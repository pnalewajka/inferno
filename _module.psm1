#Powershell for importing automation modules 
$path = (Get-Project -Name PowerShellAutomation).FullName | Split-Path | Join-Path -ChildPath Modules
Get-ChildItem -Path $path -Filter *.psm1 | % { 
	Import-Module $_.FullName -DisableNameChecking -Force
 }

$path = (Get-Project -Name PowerShellAutomation).FullName | Split-Path | Join-Path -ChildPath TabExtensions
Get-ChildItem -Path $path -Filter *.psm1 | % { 
	Import-Module $_.FullName -DisableNameChecking -Force
 }

# Enable-GitHooks # Feature disabled in Atomic by design, should be enabled in forked projects