﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Smt.Atomic.WebServices.Common.Identity
{
    public static class IdentityHeaderHelper
    {
        private const string Name = "Identity";
        private const string Namespace = "http://smtsoftware.com";

        public static MessageHeader Create(string identityToken)
        {
            var header = new MessageHeader<string>(identityToken);
            var untypedHeader = header.GetUntypedHeader(Name, Namespace);
            return untypedHeader;
        }

        public static string GetValue()
        {
            if (OperationContext.Current.IncomingMessageHeaders.FindHeader(Name, Namespace) >= 0)
            {
                return OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(Name, Namespace);
            }

            return string.Empty;
        }
    }
}
