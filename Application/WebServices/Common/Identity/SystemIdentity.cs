﻿
namespace Smt.Atomic.WebServices.Common.Identity
{
    public class SystemIdentity
    {
        public string IdentityName { get; set; }
        public long? SessionId { get; set; }
    }
}
