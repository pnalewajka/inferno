﻿using System;
using System.Security.Claims;
using System.Threading;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.WebServices.Common.Identity
{
    public class HeaderBasedAuthenticationManager : ClaimsAuthenticationManager
    {
        private readonly Func<IPrincipalBuilder> _principalBuilderFactoryMethod;
        private readonly Func<ISettingsProvider> _settingsProviderFactoryMethod;

        public HeaderBasedAuthenticationManager(
            Func<IPrincipalBuilder> principalBuilderFactoryMethod,
            Func<ISettingsProvider> settingsProviderFactoryMethod)
        {
            _principalBuilderFactoryMethod = principalBuilderFactoryMethod;
            _settingsProviderFactoryMethod = settingsProviderFactoryMethod;
        }

        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {
            var encyptedIdentity = IdentityHeaderHelper.GetValue();
            var decryptedToken = TripleDesCryptoHelper.Decrypt(encyptedIdentity, _settingsProviderFactoryMethod().EncryptionPassword);
            var identityFromToken = JsonConvert.DeserializeObject<SystemIdentity>(decryptedToken) 
                                        ?? new SystemIdentity();
            var principal = (ClaimsPrincipal) _principalBuilderFactoryMethod().BuildPrincipal(identityFromToken.IdentityName);

            Thread.CurrentPrincipal = principal;

            return principal;
        }
    }
}
