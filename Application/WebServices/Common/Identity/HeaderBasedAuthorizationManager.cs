﻿using System.Security.Claims;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebServices.Common.Identity
{
    public class HeaderBasedAuthorizationManager : ClaimsAuthorizationManager
    {
        public override bool CheckAccess(AuthorizationContext context)
        {
            return context.Principal is AtomicPrincipal;
        }
    }
}
