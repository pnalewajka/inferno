﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebServices.Common.Configuration
{
    public class ErrorLoggingBahavior : BehaviorExtensionElement, IServiceBehavior, IErrorHandler
    {
        private string _hostName;

        protected override object CreateBehavior()
        {
            return this;
        }

        public override Type BehaviorType => GetType();

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(
            ServiceDescription serviceDescription, 
            ServiceHostBase serviceHostBase, 
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            _hostName = serviceHostBase.Description.Name;

            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                if (!channelDispatcher.ErrorHandlers.Contains(this))
                {
                    channelDispatcher.ErrorHandlers.Add(this);
                }
            }
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            if (fault == null)
            {
                var localFault = new FaultException<string>(error.ToString(), new FaultReason(error.Message));
                fault = Message.CreateMessage(version, localFault.CreateMessageFault(), OperationContext.Current.IncomingMessageHeaders.Action);
            }
        }

        public bool HandleError(Exception error)
        {
            try
            {
                string contractName = null;

                if (OperationContext.Current != null
                    && OperationContext.Current.EndpointDispatcher != null)
                {
                    contractName = OperationContext.Current.EndpointDispatcher.ContractName;
                }

                var logger = ReflectionHelper.ResolveInterface<ILogger>();
                logger.ErrorFormat(error, "WCF error in {0}/{1}.", _hostName, contractName);
            }
            catch
            {
                //yes i know - but what can we do?
            }

            return true;
        }
    }
}
