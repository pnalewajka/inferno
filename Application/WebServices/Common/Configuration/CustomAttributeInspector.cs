﻿using System;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Dispatcher;
using Castle.MicroKernel;
using Smt.Atomic.WebServices.Common.Attributes;

namespace Smt.Atomic.WebServices.Common.Configuration
{
    public class CustomAttributeInspector : IParameterInspector
    {
        private readonly IKernel _kernel;
        private readonly Type _serviceType;

        public CustomAttributeInspector(IKernel kernel, Type serviceType)
        {
            _kernel = kernel;
            _serviceType = serviceType;
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            MethodInfo methodInfo = _serviceType.GetMethod(operationName);
            var attributes = Attribute.GetCustomAttributes(methodInfo).OfType<ICustomAttribute>();

            foreach (var customAttribute in attributes)
            {
                SetupAttribute(customAttribute);

                customAttribute.BeforeCall(_serviceType, operationName, inputs);
            }

            return null;
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
        }

        private void SetupAttribute(ICustomAttribute attributeInstance)
        {
            var type = attributeInstance.GetType();
            var properties = type.GetProperties()
                .Where(p => p.CanWrite && 
                    p.PropertyType.IsPublic && 
                    Attribute.IsDefined(p, typeof(SupplyFromContainerAttribute)));

            foreach (var propertyInfo in properties.Where(propertyInfo => _kernel.HasComponent(propertyInfo.PropertyType)))
            {
                propertyInfo.SetValue(attributeInstance, _kernel.Resolve(propertyInfo.PropertyType), null);
            }
        }
    }
}
