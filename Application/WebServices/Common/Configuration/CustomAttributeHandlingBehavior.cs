﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Castle.MicroKernel;

namespace Smt.Atomic.WebServices.Common.Configuration
{
    public class CustomAttributeHandlingBehavior : IEndpointBehavior
    {
        private readonly IKernel _kernel;
        private readonly Type _serviceType;

        public CustomAttributeHandlingBehavior(IKernel kernel, Type serviceType)
        {
            _kernel = kernel;
            _serviceType = serviceType;
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            foreach (DispatchOperation dispatchOperation in endpointDispatcher.DispatchRuntime.Operations)
            {
                dispatchOperation.ParameterInspectors.Add(new CustomAttributeInspector(_kernel, _serviceType));
            }
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            foreach (ClientOperation clientOperation in clientRuntime.Operations)
            {
                clientOperation.ParameterInspectors.Add(new CustomAttributeInspector(_kernel, _serviceType));
            }
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }
    }
}
