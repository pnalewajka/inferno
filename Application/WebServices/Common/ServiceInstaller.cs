﻿using System.Security.Claims;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.WebServices.Common.Identity;

namespace Smt.Atomic.WebServices.Common
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<ClaimsAuthenticationManager>()
                .UsingFactoryMethod(k => new HeaderBasedAuthenticationManager(
                    k.Resolve<IPrincipalBuilder>,
                    k.Resolve<ISettingsProvider>))
                .LifestyleTransient());

            container.Register(Component.For<ClaimsAuthorizationManager>()
                .UsingFactoryMethod(k => new HeaderBasedAuthorizationManager())
                .LifestyleTransient());
        }
    }
}