﻿using System.ServiceModel;
using Smt.Atomic.WebServices.Common.Identity;

namespace Smt.Atomic.WebServices.Common.Factories
{
    public static class OperationContextScopeFactory
    {
        public static OperationContextScope CreateWithIdentityHeader(IContextChannel channel, string identityToken)
        {
            var scope = new OperationContextScope(channel);
            OperationContext.Current.OutgoingMessageHeaders.Add(IdentityHeaderHelper.Create(identityToken));
            return scope;
        }
    }
}
