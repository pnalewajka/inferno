﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Description;
using Castle.Core;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel;
using Smt.Atomic.WebServices.Common.Configuration;

namespace Smt.Atomic.WebServices.Common.Factories
{
    public class AtomicServiceHostBuilder : DefaultServiceHostBuilder
    {
        public AtomicServiceHostBuilder(IKernel kernel)
            : base(kernel)
        {
        }

        protected override ServiceHost CreateServiceHost(ComponentModel model, DefaultServiceModel serviceModel, params Uri[] baseAddresses)
        {
            var host = (DefaultServiceHost)base.CreateServiceHost(model, serviceModel, baseAddresses);
            host.EndpointCreated += ConfigureEndpoint;
            host.Opened += ConfigureOpenedHost;

            return host;
        }

        protected override ServiceHost CreateServiceHost(ComponentModel model, Uri[] baseAddresses)
        {
            var host = (DefaultServiceHost)base.CreateServiceHost(model, baseAddresses);
            host.EndpointCreated += ConfigureEndpoint;
            host.Opened += ConfigureOpenedHost;

            return host;
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            var host = (DefaultServiceHost)base.CreateServiceHost(serviceType, baseAddresses);
            host.EndpointCreated += ConfigureEndpoint;
            host.Opened += ConfigureOpenedHost;

            return host;
        }

        private void ConfigureOpenedHost(object sender, EventArgs e)
        {
            var host = (ServiceHost)sender;
            var configuration = host.Credentials.IdentityConfiguration;
            configuration.ClaimsAuthenticationManager = Kernel.Resolve<ClaimsAuthenticationManager>();
            configuration.ClaimsAuthorizationManager = Kernel.Resolve<ClaimsAuthorizationManager>();
        }

        private void ConfigureEndpoint(object sender, EndpointCreatedArgs args)
        {
            var host = (DefaultServiceHost)sender;
            args.Endpoint.EndpointBehaviors.Add(new CustomAttributeHandlingBehavior(Kernel, host.Description.ServiceType));
        }

        private void SetupEndpoints(IEnumerable<ServiceEndpoint> collection, Type serviceType)
        {
            foreach (var serviceEndpoint in collection)
            {
                serviceEndpoint.EndpointBehaviors.Add(new CustomAttributeHandlingBehavior(Kernel, serviceType));
            }
        }
    }
}
