﻿using System;
using System.ServiceModel;
using Castle.Facilities.WcfIntegration;

namespace Smt.Atomic.WebServices.Common.Factories
{

    public class Factory : DefaultServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            var host = base.CreateServiceHost(serviceType, baseAddresses);
            return host;
        }

        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            var host = base.CreateServiceHost(constructorString, baseAddresses);
            return host;
        }
    }
}
