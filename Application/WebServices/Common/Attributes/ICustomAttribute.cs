﻿using System;

namespace Smt.Atomic.WebServices.Common.Attributes
{
    /// <summary>
    /// Implement this interface to handle custom functionality before service action is executed.
    /// </summary>
    public interface ICustomAttribute
    {
        void BeforeCall(Type serviceType, string operationName, object[] inputs);
    }
}
