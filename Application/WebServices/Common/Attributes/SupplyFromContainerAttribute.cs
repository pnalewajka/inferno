﻿using System;

namespace Smt.Atomic.WebServices.Common.Attributes
{
    /// <summary>
    /// This attribute marks property on Attribute implementing ICustomAttribute.
    /// Marked property will be supplied by IoC Container.
    /// </summary>
    public class SupplyFromContainerAttribute : Attribute
    {
    }
}
