﻿using System;
using System.Linq;
using System.ServiceModel.Security;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebServices.Common.Attributes
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class AtomicAuthorizeAttribute : Attribute, ICustomAttribute
    {
        /// <summary>
        /// It is enough to have any of provided roles
        /// </summary>
        public SecurityRoleType[] Roles { get; set; }

        [SupplyFromContainer]
        public IPrincipalProvider PrincipalProvider { get; set; }

        public AtomicAuthorizeAttribute() : this(new SecurityRoleType[] {})
        {
        }

        public AtomicAuthorizeAttribute(params SecurityRoleType[] roles)
        {
            Roles = roles;
        }

        public void BeforeCall(Type serviceType, string operationName, object[] inputs)
        {
            if (Roles == null || Roles.Length == 0)
            {
                return;
            }

            if (Roles.Any(r => PrincipalProvider.Current.IsInRole(r)))
            {
                return;
            }

            throw new SecurityAccessDeniedException($"{serviceType.Name}/{operationName}");
        }
    }
}
