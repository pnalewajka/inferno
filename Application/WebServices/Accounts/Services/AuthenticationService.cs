﻿using System.Linq;
using System.ServiceModel;
using Newtonsoft.Json;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.WebServices.Accounts.Contracts.Data;
using Smt.Atomic.WebServices.Accounts.Contracts.Service;
using Smt.Atomic.WebServices.Common.Identity;

namespace Smt.Atomic.WebServices.Accounts.Services
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single)]
    public class AuthenticationService : IAuthenticationService
    {
        private readonly ISecurityService _securityService;
        private readonly ISettingsProvider _settingsProvider;

        public AuthenticationService(
            ISecurityService securityService,
            ISettingsProvider settingsProvider)
        {
            _securityService = securityService;
            _settingsProvider = settingsProvider;
        }

        public AuthenticationResponse Authenticate(AuthenticationRequest request)
        {
            var validationResult = _securityService.ValidateUser(request.Login, request.Password);

            if (validationResult.IsUserAuthenticated)
            {
                return ProcessSuccessfulAuthorization(validationResult, request.Login);
            }

            return new AuthenticationResponse
            {
                Message = string.Join(" ",
                    validationResult.Alerts.Select(a => a.Message)),
            };
        }

        private AuthenticationResponse ProcessSuccessfulAuthorization(UserValidationResult credentialsResponse, string login)
        {
            var systemIdentity = new SystemIdentity
            {
                IdentityName = login,
            };

            return new AuthenticationResponse
            {
                Username = login,
                Token = TripleDesCryptoHelper.Encrypt(JsonConvert.SerializeObject(systemIdentity), _settingsProvider.EncryptionPassword),
            };
        }
    }
}
