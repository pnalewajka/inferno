﻿using System.Linq;
using System.ServiceModel;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.WebServices.Accounts.Contracts.Data;
using Smt.Atomic.WebServices.Accounts.Contracts.Service;
using Smt.Atomic.WebServices.Common.Attributes;

namespace Smt.Atomic.WebServices.Accounts.Services
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single)]
    public class AccountsService : IAccountsService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserService _userService;

        public AccountsService(
            IPrincipalProvider principalProvider,
            IUserService userService)
        {
            _principalProvider = principalProvider;
            _userService = userService;
        }

        public CheckUserResponse CheckUser(CheckUserRequest request)
        {
            var parsedUsername = ParsedLogin.Parse(request.Email);

            if (!parsedUsername.IsEmail)
            {
                return new CheckUserResponse { IsDomainUser = false, UserExists = false };
            }

            var result = _userService.GetUserOrDefaultByEmail(request.Email);

            if (result == null)
            {
                return new CheckUserResponse { IsDomainUser = false, UserExists = false };
            }

            return new CheckUserResponse { IsDomainUser = false, UserExists = true };
        }

        [AtomicAuthorize]
        public GetMyUserRolesResponse GetMyUserRoles(GetMyUserRolesRequest request)
        {
            var identity = _principalProvider.Current;

            if (identity.Login != request.Username)
            {
                return new GetMyUserRolesResponse
                {
                    Roles = Enumerable.Empty<string>(),
                };
            }

            return new GetMyUserRolesResponse
            {
                Roles = _userService
                            .GetUserRolesByLogin(request.Username)
                            .Select(r => r.ToString()),
            };
        }

        [AtomicAuthorize]
        public GetMyUserDataResponse GetMyUserData(GetMyUserDataRequest request)
        {
            var identity = _principalProvider.Current;

            if (identity.Login != request.Username)
            {
                return new GetMyUserDataResponse();
            }

            var user = _userService.GetUserByLogin(request.Username);

            return new GetMyUserDataResponse
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.Login,
                Email = user.Email,
            };
        }

        [AtomicAuthorize(SecurityRoleType.CanViewUsers)]
        public GetUserDataResponse GetUserData(GetUserDataRequest request)
        {
            var user = _userService.GetUserByLogin(request.Username);

            return new GetUserDataResponse
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.Login,
                Email = user.Email,
            };
        }
    }
}
