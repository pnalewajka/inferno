﻿using System.ServiceModel;
using Smt.Atomic.WebServices.Accounts.Contracts.Data;

namespace Smt.Atomic.WebServices.Accounts.Contracts.Service
{
    [ServiceContract(Namespace = "http://atomic.smtsoftware.com")]
    public interface IAuthenticationService
    {
        [OperationContract]
        AuthenticationResponse Authenticate(AuthenticationRequest request);
    }
}
 