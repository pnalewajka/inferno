﻿using System.ServiceModel;
using Smt.Atomic.WebServices.Accounts.Contracts.Data;

namespace Smt.Atomic.WebServices.Accounts.Contracts.Service
{
    [ServiceContract(Namespace = "http://atomic.smtsoftware.com")]
    public interface IAccountsService
    {
        [OperationContract]
        GetMyUserRolesResponse GetMyUserRoles(GetMyUserRolesRequest request);

        [OperationContract]
        GetMyUserDataResponse GetMyUserData(GetMyUserDataRequest request);

        [OperationContract]
        GetUserDataResponse GetUserData(GetUserDataRequest request);

        [OperationContract]
        CheckUserResponse CheckUser(CheckUserRequest request);
    }
}
