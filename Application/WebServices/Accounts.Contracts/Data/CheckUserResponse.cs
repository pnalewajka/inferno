﻿namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class CheckUserResponse
    {
        public bool UserExists { get; set; }
        public bool IsDomainUser { get; set; }
    }
}
