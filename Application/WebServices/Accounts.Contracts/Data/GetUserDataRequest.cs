﻿namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class GetUserDataRequest
    {
        public string Username { get; set; }
    }
}
