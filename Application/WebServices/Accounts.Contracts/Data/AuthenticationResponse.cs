﻿namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class AuthenticationResponse
    {
        public string Message { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}