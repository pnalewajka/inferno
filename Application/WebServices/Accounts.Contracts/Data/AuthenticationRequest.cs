﻿namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class AuthenticationRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Language { get; set; }
    }
}