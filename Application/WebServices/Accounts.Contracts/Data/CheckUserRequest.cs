﻿namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class CheckUserRequest
    {
        public string Email { get; set; }
    }
}
