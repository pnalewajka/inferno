﻿
namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class GetMyUserRolesRequest
    {
        public string Username { get; set; }
    }
}
