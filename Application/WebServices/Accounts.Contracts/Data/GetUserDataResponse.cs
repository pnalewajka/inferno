﻿using System;

namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class GetUserDataResponse
    {
        public long UserId { get; set; }
        public String UserName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String LdapName { get; set; }
        public String Email { get; set; }
    }
}
