﻿using System.Collections.Generic;

namespace Smt.Atomic.WebServices.Accounts.Contracts.Data
{
    public class GetMyUserRolesResponse
    {
        public string Message { get; set; }
        public IEnumerable<string> Roles { get; set; } 
    }
}
