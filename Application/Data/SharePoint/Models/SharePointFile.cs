﻿using System;

namespace Smt.Atomic.Data.SharePoint.Models
{
    public class SharePointFile
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public Guid UniqueId { get; set; }

        public byte[] Content { get; set; } 
    }
}
