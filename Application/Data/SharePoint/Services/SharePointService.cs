﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Microsoft.SharePoint.Client;

using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Data.SharePoint.Interfaces;
using Smt.Atomic.Data.SharePoint.Models;

namespace Smt.Atomic.Data.SharePoint.Services
{
    public class SharePointService : ISharePointService
    {
        private readonly SharePointSettings _sharePointSettings;
        private readonly ILogger _logger;

        public SharePointService(ISettingsProvider iSettingsProvider, ILogger logger)
        {
            _sharePointSettings = iSettingsProvider.SharePointSettings;
            _logger = logger;
        }

        public IEnumerable<SharePointFile> GetCandidateDocuments(IEnumerable<string> candidateSharePointLocations)
        {
            if (candidateSharePointLocations == null)
            {
                return new List<SharePointFile>();
            }

            var candidateFiles = new List<SharePointFile>();
            using (var context = CreateClientContext())
            {
                foreach (var location in candidateSharePointLocations)
                {
                    // Maybe we should get from CRM only the relative part
                    var list = context.Web.GetFolderByServerRelativeUrl(location);

                    var files = list.Files;
                    context.Load(files);
                    context.ExecuteQuery();

                    candidateFiles.AddRange(files.Select(file => new SharePointFile { Name = file.Name, Url = $"{_sharePointSettings.SharePointUrl}{file.ServerRelativeUrl}", UniqueId = file.UniqueId }).ToList());
                }
            }
            return candidateFiles;
        }

        public SharePointFile GetCandidateDocument(Guid documentUniqueId)
        {
            if (documentUniqueId == Guid.Empty)
            {
                throw new InvalidOperationException("Share point document id cannot be empty.");
            }

            try
            {
                var result = new SharePointFile { UniqueId = documentUniqueId };
                using (var context = CreateClientContext())
                {
                    var file = context.Web.GetFileById(documentUniqueId);
                    var clientResult = file.OpenBinaryStream();

                    context.Load(file);
                    context.ExecuteQuery();

                    result.Name = file.Name;

                    using (var stream = clientResult.Value)
                    {
                        result.Content = new byte[stream.Length];
                        stream.Read(result.Content, 0, result.Content.Length);

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Sharepoint document (unique id: {documentUniqueId}) could not be downloaded.", ex);
                throw;
            }
        }

        //todo: optimize
        public Task<SharePointFile> GetCandidateDocumentAsync(Guid documentUniqueId)
        {
            return Task<SharePointFile>.Factory.StartNew(() => this.GetCandidateDocument(documentUniqueId));
        }

        private ClientContext CreateClientContext()
        {
            var sharePointPassword = _sharePointSettings.Password;
            var sharePointUsername = _sharePointSettings.Username;

            var sharePointSiteUrl = _sharePointSettings.SharePointUrl + _sharePointSettings.RelativePath;

            var pass = new SecureString();
            foreach (var c in sharePointPassword)
            {
                pass.AppendChar(c);
            }

            var credentials = new SharePointOnlineCredentials(sharePointUsername, pass);
            return new ClientContext(sharePointSiteUrl) { Credentials = credentials };
        }
    }
}
