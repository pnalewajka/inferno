﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smt.Atomic.Data.SharePoint.Models;

namespace Smt.Atomic.Data.SharePoint.Interfaces
{
    public interface ISharePointService
    {
        IEnumerable<SharePointFile> GetCandidateDocuments(IEnumerable<string> candidateSharePointLocations);

        SharePointFile GetCandidateDocument(Guid documentUniqueId);

        Task<SharePointFile> GetCandidateDocumentAsync(Guid documentUniqueId);
    }
}
