﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.SharePoint.Interfaces;
using Smt.Atomic.Data.SharePoint.Services;

namespace Smt.Atomic.Data.SharePoint
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp || containerType == ContainerType.WebApi || containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ISharePointService>().ImplementedBy<SharePointService>());
            }
        }
    }
}
