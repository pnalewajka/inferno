using System;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Classes
{
    internal class ReadOnlyUnitOfWork<TDbScope> : IReadOnlyUnitOfWork<TDbScope>
        where TDbScope : IDbScope
    {
        public TDbScope Repositories { get; private set; }
        protected IDbContextWrapper DbContext;
        protected ILogger Logger { get; private set; }
        protected Guid UnitGuid { get; private set; }

        public ReadOnlyUnitOfWork(ILogger logger)
        {
            Logger = logger;
            UnitGuid = Guid.NewGuid();

            DbContext = ReflectionHelper.ResolveInterface<IDbContextWrapper>();
            DbContext.InitContext();

            Repositories = InterfaceProxyHelper.CreateDbScopeProxy<TDbScope>(DbContext);
        }

        public void Dispose()
        {
            if (Repositories != null)
            {
                Repositories.Dispose();
            }

            if (DbContext != null)
            {
                DbContext.Dispose();
                ReflectionHelper.Release(DbContext);
            }
        }
    }
}