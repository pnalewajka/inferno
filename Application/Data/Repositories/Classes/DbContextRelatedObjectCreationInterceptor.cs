﻿using Castle.DynamicProxy;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Classes
{
    /// <summary>
    /// This is base implementation of unit of work, that doesn't commit any changes back to dbcontext
    /// </summary>
    internal class DbContextRelatedObjectCreationInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var method = invocation.Method;

            if (!method.ReturnType.IsInterface)
            {
                invocation.Proceed();

                return;
            }

            var repositoryFactory = (IRepositoryFactory)invocation.Proxy;

            invocation.ReturnValue = repositoryFactory.Create(method.ReturnType);
        }
    }
}
