﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Classes
{
    internal class UnitOfWork<TDbScope>
        : ReadOnlyUnitOfWork<TDbScope>
        , IExtendedUnitOfWork<TDbScope>
        where TDbScope : class, IDbScope
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public UnitOfWork(ILogger logger, IPrincipalProvider principalProvider, ITimeService timeService)
            : base(logger)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        private void UpdateSystemFields()
        {
            var principal = _principalProvider.Current;

            HandleMassUpdateOperation<IModificationTracked>(m => SetModifiedFields(m, principal));
            HandleMassUpdateOperation<ICreationTracked>(m => SetCreatedFields(m, principal), EntityModificationScenario.AddedOnly);
            HandleMassUpdateOperation<ICreationTracked>(m => PreserveCreatedFields(m, principal), EntityModificationScenario.ModifiedOnly);
        }

        private void SetModifiedFields(IModificationTracked obj, IAtomicPrincipal atomicPrincipal)
        {
            obj.ModifiedById = atomicPrincipal.Id;
            obj.ImpersonatedById = atomicPrincipal.ImpersonatedById;
            obj.ModifiedOn = _timeService.GetCurrentTime();
        }

        private void SetCreatedFields(ICreationTracked obj, IAtomicPrincipal atomicPrincipal)
        {
            obj.CreatedOn = _timeService.GetCurrentTime();
            obj.CreatedById = atomicPrincipal.Id;
            obj.ImpersonatedCreatedById = atomicPrincipal.ImpersonatedById;
        }

        private void PreserveCreatedFields(ICreationTracked obj, IAtomicPrincipal atomicPrincipal)
        {
            var dbEntity = DbContext.Entry(obj);

            dbEntity.Property(o => o.CreatedById).IsModified = false;
            dbEntity.Property(o => o.CreatedOn).IsModified = false;
            dbEntity.Property(o => o.ImpersonatedCreatedById).IsModified = false;
        }

        public async Task CommitAsync()
        {
            try
            {
                UpdateSystemFields();

                await DbContext.SaveChangesAsync();

                Logger.DebugFormat("Core UnitOfWork commited ({0})", UnitGuid);
            }
            catch (DbEntityValidationException ex)
            {
                var translatedException = ExceptionTranslationHelper.Translate(ex);
                Logger.Error("Failed to commit: ", translatedException);

                throw translatedException;
            }
            catch (DbUpdateConcurrencyException ucex)
            {
                var translatedException = ExceptionTranslationHelper.TranslateConcurrencyException(ucex);
                Logger.Error("Failed to commit: ", translatedException);

                throw translatedException;
            }
            catch (DbUpdateException duex)
            {
                var translatedException = ExceptionTranslationHelper.Translate(duex);
                Logger.Error("Failed to commit: ", translatedException);

                throw translatedException;
            }
        }

        public void Commit()
        {
            try
            {
                UpdateSystemFields();
                DbContext.SaveChanges();

                Logger.DebugFormat("Core UnitOfWork commited ({0})", UnitGuid);
            }
            catch (DbEntityValidationException ex)
            {
                var translatedException = ExceptionTranslationHelper.Translate(ex);
                Logger.Error("Failed to commit: ", translatedException);

                throw translatedException;
            }
            catch (DbUpdateConcurrencyException ucex)
            {
                var translatedException = ExceptionTranslationHelper.TranslateConcurrencyException(ucex);
                Logger.Error("Failed to commit: ", translatedException);

                throw translatedException;
            }
            catch (DbUpdateException duex)
            {
                var translatedException = ExceptionTranslationHelper.Translate(duex);
                Logger.Error("Failed to commit: ", translatedException);

                throw translatedException;
            }
        }

        private void HandleMassUpdateOperation<TEntityInterface>(Action<TEntityInterface> updateAction, EntityModificationScenario scenario = EntityModificationScenario.Any)
        {
            foreach (var entity in GetItemsForMassUpdate<TEntityInterface>(scenario))
            {
                updateAction(entity);
            }
        }

        public int ExecuteSqlCommand(string command, params object[] parameters)
        {
            return DbContext.ExecuteSqlCommand(command, parameters);
        }

        public IEnumerable<TModel> SqlQuery<TModel>(string sql, params object[] parameters)
        {
            return DbContext.SqlQuery<TModel>(sql, parameters);
        }

        public DataSet SqlQueryToDataSet(string sql, IDictionary<string, object> parameters)
        {
            return DbContext.SqlQueryToDataSet(sql, parameters);
        }

        private IEnumerable<TEntityInterface> GetItemsForMassUpdate<TEntityInterface>(EntityModificationScenario scenario)
        {
            Func<DbEntityEntry, bool> predicate;

            switch (scenario)
            {
                case EntityModificationScenario.AddedOnly:
                    predicate = e => e.State == EntityState.Added;
                    break;

                case EntityModificationScenario.ModifiedOnly:
                    predicate = e => e.State == EntityState.Modified;
                    break;

                case EntityModificationScenario.Any:
                    predicate = e => e.State != EntityState.Detached && e.State != EntityState.Unchanged;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(scenario));
            }

            return DbContext.ChangeTrackerEntries()
                .Where(predicate)
                .Select(e => e.Entity)
                .OfType<TEntityInterface>();
        }
    }
}