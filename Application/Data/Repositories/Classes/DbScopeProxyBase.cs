using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Classes
{
    public class DbScopeProxyBase : IRepositoryFactory, IDisposable
    {
        private readonly IDictionary<Type, object> _resolvedRepositories;
        private readonly IDbContextWrapper _dbContext;

        public DbScopeProxyBase(IDbContextWrapper dbContext)
        {
            _dbContext = dbContext;
            _resolvedRepositories = new Dictionary<Type, object>();
        }

        IRepository<TEntity> IRepositoryFactory.Create<TEntity>()
        {
            return (IRepository<TEntity>)CreateRepositoryObject(typeof(IRepository<TEntity>));
        }

        IReadOnlyRepository<TEntity> IRepositoryFactory.CreateReadOnly<TEntity>()
        {
            return ((IReadOnlyRepositoryFactory)this).Create<TEntity>();
        }

        IReadOnlyRepository<TEntity> IReadOnlyRepositoryFactory.Create<TEntity>()
        {
            return (IReadOnlyRepository<TEntity>)CreateRepositoryObject(typeof(IReadOnlyRepository<TEntity>));
        }

        object IRepositoryFactory.Create(Type repositoryType)
        {
            return CreateRepositoryObject(repositoryType);
        }

        private object CreateRepositoryObject(Type repositoryInterfaceType)
        {
            if (!_resolvedRepositories.ContainsKey(repositoryInterfaceType))
            {
                if (repositoryInterfaceType.IsGenericType && repositoryInterfaceType.GetGenericTypeDefinition() == typeof(IQueryable<>))
                {
                    var entityType = TypeHelper.GetRawGenericArguments(repositoryInterfaceType, typeof(IQueryable<>))[0];
                    var querable = _dbContext.Set(entityType);
                    _resolvedRepositories.Add(repositoryInterfaceType, querable);
                }
                else
                {
                    var repository = ReflectionHelper.ResolveInterface(repositoryInterfaceType, new { dbContext = _dbContext });
                    _resolvedRepositories.Add(repositoryInterfaceType, repository);
                }
            }

            return _resolvedRepositories[repositoryInterfaceType];
        }

        public void Dispose()
        {
            var servicesForDisposal =
                _resolvedRepositories.Values
                .Where(v => v as IDisposable != null)
                .Select(v => (IDisposable)v)
                .ToList();

            foreach (var serviceForDisposal in servicesForDisposal)
            {
                serviceForDisposal.Dispose();
            }
        }
    }
}