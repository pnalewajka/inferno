﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Helpers
{
    public static class CollectionEntityExtensionMethods
    {
        /// <summary>
        /// Get specific result with selector by it's id
        /// Throws exception if entity not found
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="id"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static TResult SelectById<TEntity, TResult>(this ICollection<TEntity> source, long id, Func<TEntity, TResult> selector) where TEntity : class, IEntity
        {
            return source
                .Where(x => x.Id == id)
                .Select(selector)
                .Single();
        }
    }
}
