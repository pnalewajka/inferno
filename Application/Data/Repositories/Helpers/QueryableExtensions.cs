﻿using System.Linq;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Helpers
{
    public static class QueryableExtensions
    {
        /// <summary>
        /// Returns a new query that will not be traced by the entity framework.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> AsNoTracking<TEntity>(this IQueryable<TEntity> source) where TEntity : class, IEntity
        {
            return System.Data.Entity.QueryableExtensions.AsNoTracking(source);
        }
    }
}