﻿using Castle.DynamicProxy;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Classes;

namespace Smt.Atomic.Data.Repositories.Helpers
{
    internal class InterfaceProxyHelper
    {
        private static readonly ProxyGenerator ProxyGenerator = new ProxyGenerator();

        /// <summary>
        /// Create instance of proxy for specific interface
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <param name="interceptors"></param>
        /// <returns></returns>
        public static TInterface CreateProxy<TInterface>(params IInterceptor[] interceptors)
            where TInterface : class
        {
            var proxyGenerator = new ProxyGenerator();

            return
                proxyGenerator.CreateInterfaceProxyWithoutTarget<TInterface>(interceptors);
        }


        /// <summary>
        /// Prepare proxy for TDbScope via extension of DbScopeProxyBase
        /// </summary>
        /// <typeparam name="TDbScope"></typeparam>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static TDbScope CreateDbScopeProxy<TDbScope>(IDbContextWrapper dbContext)
        {
            var classProxyInstance = ProxyGenerator.CreateClassProxy(
                typeof (DbScopeProxyBase), 
                new[] {typeof (TDbScope)},
                ProxyGenerationOptions.Default, 
                new object[] {dbContext},
                new DbContextRelatedObjectCreationInterceptor()
                );

            return (TDbScope) classProxyInstance;
        }
    }
}