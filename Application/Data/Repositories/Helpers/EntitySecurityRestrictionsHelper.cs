﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Interfaces;
using System;
using System.Linq;

namespace Smt.Atomic.Data.Repositories.Helpers
{
    public static class EntitySecurityRestrictionsHelper
    {
        public static EntitySecurityRestriction<TEntity> GetSecurityRestriction<TEntity>() where TEntity : IEntity
        {
            return (EntitySecurityRestriction<TEntity>)GetSecurityRestriction(typeof(TEntity));
        }

        public static IEntitySecurityRestriction GetSecurityRestriction(Type type)
        {
            var restrictionGenericType = typeof(EntitySecurityRestriction<>).MakeGenericType(type);
            var restrictionType = TypeHelper.GetAllSubClassesOf(restrictionGenericType, false).FirstOrDefault();

            if (restrictionType == null)
            {
                return null;
            }

            var securityRestriction = ReflectionHelper.CreateInstanceWithIocDependencies(restrictionType);

            return (IEntitySecurityRestriction)securityRestriction;
        }
    }
}
