﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Helpers
{
    public static class DataFilterExtensions
    {
        /// <summary>
        /// Filters data query with current user filtering rules based on 
        /// the data filter definitions
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> Filtered<TEntity>(this IQueryable<TEntity> source) where TEntity : class, IEntity
        {
            var entityDataFilter = GetFilteringConditions(typeof(TEntity));

            if (entityDataFilter?.AsExpression != null)
            {
                source = source.Where((Expression<Func<TEntity, bool>>)entityDataFilter.AsExpression);
            }

            var securityRestriction = GetEntitySecurityRestriction(typeof(TEntity));

            if (securityRestriction?.AsExpression != null)
            {
                source = source.Where((Expression<Func<TEntity, bool>>)securityRestriction.AsExpression);
            }

            return source;
        }

        /// <summary>
        /// Filters data query with current user filtering rules based on 
        /// the data filter definitions
        /// </summary>
        public static TRecord FilteredOrDefault<TRecord>(this TRecord source) where TRecord : class
        {
            var dataFilter = GetFilteringConditions(typeof(TRecord));

            if (dataFilter?.AsExpression != null)
            {
                source = ((Func<TRecord, bool>)dataFilter.AsCompiled)(source) ? source : null;
            }

            var securityRestriction = GetEntitySecurityRestriction(typeof(TRecord));

            if (securityRestriction?.AsExpression != null)
            {
                source = ((Func<TRecord, bool>)securityRestriction.AsCompiled)(source) ? source : null;
            }

            return source;
        }

        /// <summary>
        /// Filters data collection with current user filtering rules based on 
        /// the data filter definitions
        /// <para>CAN'T BE USED IN SQL CONTEXT</para>
        /// </summary>
        public static TRecord FilteredOrThrow<TRecord>(this TRecord source) where TRecord : class
        {
            return new[] { source }.Filtered().Single();
        }

        /// <summary>
        /// Filters data collection with current user filtering rules based on 
        /// the data filter definitions
        /// </summary>
        /// <typeparam name="TRecord"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IEnumerable<TRecord> Filtered<TRecord>(this IEnumerable<TRecord> source) where TRecord : class
        {
            var dataFilter = GetFilteringConditions(typeof(TRecord));

            if (dataFilter?.AsExpression != null)
            {
                source = source.Where((Func<TRecord, bool>)dataFilter.AsCompiled);
            }

            var securityRestriction = GetEntitySecurityRestriction(typeof(TRecord));

            if (securityRestriction?.AsExpression != null)
            {
                source = source.Where((Func<TRecord, bool>)securityRestriction.AsCompiled);
            }

            return source;
        }

        internal static EntityDataFilter GetEntitySecurityRestriction(Type entityType)
        {
            var securityRestriction = EntitySecurityRestrictionsHelper.GetSecurityRestriction(entityType);

            if (securityRestriction == null)
            {
                return null;
            }

            var restrictionLambda = securityRestriction.GetRestriction();

            return new EntityDataFilter(restrictionLambda);
        }

        internal static EntityDataFilter GetFilteringConditions(Type entityType)
        {
            var dataPrivilegeService = ReflectionHelper.ResolveInterface<IDataFilterService>();
            var principalProvider = ReflectionHelper.ResolveInterface<IPrincipalProvider>();

            if (principalProvider.Current == null)
            {
                return null;
            }

            var userId = principalProvider.Current.Id ?? -1;

            return dataPrivilegeService.GetUserDataFilters(userId).GetEntityDataFilter(entityType);
        }
    }
}