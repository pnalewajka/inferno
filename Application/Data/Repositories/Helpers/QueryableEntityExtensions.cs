﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Helpers
{
    public static class QueryableEntityExtensions
    {
        /// <summary>
        /// Get specific entity by it's id
        /// Throws exception if entity not found
        /// </summary>
        /// <param name="source"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static TEntity GetById<TEntity>(this IQueryable<TEntity> source, long id) where TEntity : class, IEntity
        {
            return source.Single(x => x.Id == id);
        }

        /// <summary>
        /// Get specific result with selector expression by it's id
        /// Throws exception if entity not found
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="id"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static TResult SelectById<TEntity, TResult>(this IQueryable<TEntity> source, long id, Expression<Func<TEntity, TResult>> selector) where TEntity : class, IEntity
        {
            return source
                .Where(x => x.Id == id)
                .Select(selector)
                .Single();
        }

        /// <summary>
        /// Get specific entities by ids
        /// It doesn't throw the exception if any record is not found
        /// </summary>
        /// <param name="source"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> GetByIds<TEntity>(this IQueryable<TEntity> source, IEnumerable<long> ids) where TEntity : class, IEntity
        {
            return source.Where(x => ids.Contains(x.Id));
        }

        /// <summary>
        /// Get specific entities to memory by ids, maintaining id order
        /// It doesn't throw the exception if any record is not found
        /// </summary>
        /// <param name="source"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public static ICollection<TEntity> GetOrderedByIds<TEntity>(this IQueryable<TEntity> source, IList<long> ids) where TEntity : class, IEntity
        {
            return source.GetByIds(ids).ToList().OrderBy(e => ids.IndexOf(e.Id)).ToList();
        }

        /// <summary>
        /// Get specific entity by it's id
        /// </summary>
        /// <param name="source"></param>
        /// <param name="id"></param>
        /// <returns>null if entity not found</returns>
        public static TEntity GetByIdOrDefault<TEntity>(this IQueryable<TEntity> source, long id) where TEntity : class, IEntity
        {
            return source.SingleOrDefault(x => x.Id == id);
        }
    }
}