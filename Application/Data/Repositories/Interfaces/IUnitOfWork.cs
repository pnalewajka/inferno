﻿using System;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    /// <summary>
    /// Unit of work interface
    /// </summary>
    public interface IUnitOfWork<TDbScope> : IReadOnlyUnitOfWork<TDbScope>
        where TDbScope : IDbScope
    {
        /// <summary>
        /// Commit operations
        /// </summary>
        void Commit();

        /// <summary>
        /// Commit operations, async version
        /// </summary>
        /// <returns></returns>
        Task CommitAsync();
    }
}