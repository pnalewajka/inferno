﻿using System;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    /// <summary>
    /// Base interface for all scopes on dbcontext
    /// </summary>
    public interface IDbScope : IDisposable
    {
    }
}
