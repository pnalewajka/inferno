﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IDataFilterService
    {
        UserDataFilters GetUserDataFilters(long userId);

        IEnumerable<string> ValidateCondition(string entity, string condition);
    }
}