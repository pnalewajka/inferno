﻿using System;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IRepositoryFactory
        : IReadOnlyRepositoryFactory
    {
        /// <summary>
        /// Creates instance of repository for specific TEntity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        new IRepository<TEntity> Create<TEntity>() where TEntity : class, IEntity;

        /// <summary>
        /// Creates instance of readonly repository for specific TEntity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IReadOnlyRepository<TEntity> CreateReadOnly<TEntity>() where TEntity : class, IEntity;

        /// <summary>
        /// Looks for repository implementation, creates it's instance, caches it in unitofwork scope
        /// </summary>
        /// <param name="repositoryType">repository interface</param>
        /// <returns></returns>
        object Create(Type repositoryType);
    }
}