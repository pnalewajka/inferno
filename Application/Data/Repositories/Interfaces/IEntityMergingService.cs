﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IEntityMergingService
    {
        /// <summary>
        /// Create new collection to be assigned to navigation property based on the foreign entity list
        /// Records are retrieved from proper repository
        /// </summary>
        ICollection<TEntity> AttachEntities<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> foreignEntities)
            where TEntity : class, IEntity
            where TDbScope : IDbScope;

        /// <summary>
        /// Create new collection to be assigned to navigation property based on the id list
        /// Records are retrieved from proper repository
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TDbScope"></typeparam>
        /// <param name="unitOfWork"></param>
        /// <param name="ids"></param>
        ICollection<TEntity> CreateEntitiesFromIds<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, long[] ids) 
            where TEntity : class, IEntity
            where TDbScope : IDbScope;

        /// <summary>
        /// Removes and adds records in dbCollection based on the current inputCollection content
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TDbScope"></typeparam>
        /// <param name="unitOfWork"></param>
        /// <param name="dbCollection"></param>
        /// <param name="inputCollection"></param>
        void MergeCollections<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> dbCollection, ICollection<TEntity> inputCollection) 
            where TEntity : class, IEntity 
            where TDbScope : IDbScope;

        /// <summary>
        /// Removes and adds records in dbCollection based on the current inputCollection content
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TDbScope"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="unitOfWork"></param>
        /// <param name="dbCollection"></param>
        /// <param name="inputCollection"></param>
        /// <param name="discriminatorSelector"></param>
        /// <param name="removeFromRepository"></param>
        void MergeCollections<TEntity, TDbScope, TProperty>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> dbCollection, ICollection<TEntity> inputCollection, Func<TEntity, TProperty> discriminatorSelector, bool removeFromRepository)
            where TEntity : class, IEntity
            where TDbScope : IDbScope;
    }
}