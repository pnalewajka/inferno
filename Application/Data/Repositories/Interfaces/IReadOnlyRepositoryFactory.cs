﻿using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IReadOnlyRepositoryFactory
    {
        /// <summary>
        /// Creates instance of repository for specific TEntity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IReadOnlyRepository<TEntity> Create<TEntity>() where TEntity : class, IEntity;
    }
}