﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    /// <summary>
    /// Repository interface
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IReadOnlyRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Attach entity to context, mark as not modified
        /// </summary>
        /// <param name="entity"></param>
        void Attach(TEntity entity);

        /// <summary>
        /// Add entity to repository
        /// </summary>
        /// <param name="entity"></param>
        void Add(TEntity entity);

        /// <summary>
        /// Add entity range to repository
        /// </summary>
        /// <param name="entity"></param>
        void AddRange(IEnumerable<TEntity> entity);

        /// <summary>
        /// Mark entity as modified
        /// </summary>
        /// <param name="entity"></param>
        void Edit(TEntity entity);

        /// <summary>
        /// Detach entity from the context
        /// </summary>
        /// <param name="entity"></param>
        void Detach(TEntity entity);

        /// <summary>
        /// Mark specific property as modified
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="property"></param>
        void SetPropertyModified(TEntity entity, Expression<Func<TEntity, object>> property);

        /// <summary>
        /// Mark entity as to be removed
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isDeletePermanent">skip soft delete</param>
        void Delete(TEntity entity, bool isDeletePermanent = false);

        /// <summary>
        /// Mark entities as to be removed
        /// </summary>
        /// <param name="entities">Entities to be deleted</param>
        /// <param name="isDeletePermanent">skip soft delete</param>
        void DeleteRange(IEnumerable<TEntity> entities, bool isDeletePermanent = false);

        /// <summary>
        /// Remove entity from database (no other entities will be marked as deleted). If cascade delete specified it will be performed at DB side.
        /// </summary>
        /// <param name="entity"></param>
        void Remove(TEntity entity);

        /// <summary>
        /// Remove entities from database (no other entities will be marked as deleted). If cascade delete specified it will be performed at DB side.
        /// </summary>
        /// <param name="entities">Entities to be deleted</param>
        void RemoveRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Create new empty instance of specific entity
        /// </summary>
        /// <returns></returns>
        TEntity CreateEntity();

        /// <summary>
        /// Delete each record fulfilling the predicate
        /// </summary>
        /// <param name="deletePredicate"></param>
        /// <param name="isDeletePermanent">skip soft delete</param>
        void DeleteEach(Expression<Func<TEntity, bool>> deletePredicate, bool isDeletePermanent = false);

        /// <summary>
        /// Reload specific entity collection 
        /// </summary>
        /// <typeparam name="TCollectionItem"></typeparam>
        /// <param name="entity"></param>
        /// <param name="collectionExpression"></param>
        void ReloadCollection<TCollectionItem>(TEntity entity,
            Expression<Func<TEntity, ICollection<TCollectionItem>>> collectionExpression) where TCollectionItem : class;

        /// <summary>
        /// Gets all unique keys violations for a given entry
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        IEnumerable<PropertyGroup> GetUniqueKeyViolations(TEntity entity);
    }

}
