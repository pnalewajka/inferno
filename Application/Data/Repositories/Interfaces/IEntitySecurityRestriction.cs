﻿using System.Linq.Expressions;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IEntitySecurityRestriction
    {
        LambdaExpression GetRestriction();
    }
}
