﻿using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IDbContextWrapperFactory
    {
        IDbContextWrapper Create();
    }
}
