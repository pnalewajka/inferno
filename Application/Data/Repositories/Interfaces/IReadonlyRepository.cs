﻿using System.Linq;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    /// <summary>
    /// Read only repository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IReadOnlyRepository<TEntity> : IQueryable<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Specifies the related objects to include in the query results.
        /// </summary>
        /// <typeparam name="TEntity">The type of entity being queried. </typeparam>
        /// <returns>
        /// A new IQueryable&lt;T&gt; with the defined query path.
        /// </returns>
        IQueryable<TEntity> Include(string path);

        /// <summary>
        /// Specifies command timeout in seconds
        /// </summary>
        /// <param name="timeoutInSeconds">Timeout in seconds</param>
        void SetTimeout(int timeoutInSeconds);
    }
}