﻿using System.Collections.Generic;
using System.Data;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IExtendedUnitOfWork<TDbScope> : IUnitOfWork<TDbScope> 
        where TDbScope : IDbScope
    {
        /// <summary>
        /// Execute sql query
        /// </summary>
        /// <param name="command">Query</param>
        /// <param name="parameters">additional parameters</param>
        /// <returns>Number of affected rows</returns>
        int ExecuteSqlCommand(string command, params object[] parameters);

        /// <summary>
        /// Get list of TModel instances resulted from query 
        /// </summary>
        /// <typeparam name="TModel">Model type</typeparam>
        /// <param name="sql">Query</param>
        /// <param name="parameters">Additional parameters</param>
        /// <returns>List of TModel instances</returns>
        IEnumerable<TModel> SqlQuery<TModel>(string sql, params object[] parameters);

        /// <summary>
        /// Executes sql and return DataSet based on the results
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DataSet SqlQueryToDataSet(string sql, IDictionary<string, object> parameters);
    }
}