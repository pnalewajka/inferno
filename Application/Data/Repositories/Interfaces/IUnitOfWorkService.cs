﻿namespace Smt.Atomic.Data.Repositories.Interfaces
{
    /// <summary>
    /// Empty interface for proper IOC resolve of implementation of unitofworkservice that has dependencies on some IOC injected services
    /// </summary>
    public interface IUnitOfWorkService<TDbScope> : IReadOnlyUnitOfWorkService<TDbScope>
        where TDbScope : IDbScope
    {
        /// <summary>
        /// Create unit of work basing on dependencies
        /// </summary>
        /// <param name="allowAsync"></param>
        /// <returns></returns>
        new IUnitOfWork<TDbScope> Create(bool allowAsync = false);

        /// <summary>
        /// Create a readonly unit of work basing on dependencies
        /// </summary>
        /// <param name="allowAsync"></param>
        /// <returns></returns>
        IReadOnlyUnitOfWork<TDbScope> CreateReadOnly(bool allowAsync = false);

        /// <summary>
        /// Create an extended unit of work
        /// </summary>
        /// <param name="allowAsync"></param>
        /// <returns></returns>
        IExtendedUnitOfWork<TDbScope> CreateExtended(bool allowAsync = false);
    }
}