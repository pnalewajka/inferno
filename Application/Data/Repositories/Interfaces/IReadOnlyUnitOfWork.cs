﻿using System;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Repositories.Interfaces
{
    /// <summary>
    /// ReadOnly Unit of Work interface
    /// </summary>
    public interface IReadOnlyUnitOfWork<TDbScope> : IDisposable
        where TDbScope : IDbScope
    {
        /// <summary>
        /// Access to db scoped repositories
        /// </summary>
        TDbScope Repositories { get; }
    }
}