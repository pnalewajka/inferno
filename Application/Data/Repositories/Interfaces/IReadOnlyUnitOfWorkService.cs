﻿namespace Smt.Atomic.Data.Repositories.Interfaces
{
    public interface IReadOnlyUnitOfWorkService<TDbScope>
        where TDbScope : IDbScope
    {
        /// <summary>
        /// Create unit of work basing on dependencies
        /// </summary>
        /// <param name="allowAsync"></param>
        /// <returns></returns>
        IReadOnlyUnitOfWork<TDbScope> Create(bool allowAsync = false);
    }
}