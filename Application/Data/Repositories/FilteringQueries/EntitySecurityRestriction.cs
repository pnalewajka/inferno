﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public abstract class EntitySecurityRestriction<TEntity> : IEntitySecurityRestriction where TEntity : IEntity
    {
        protected readonly IAtomicPrincipal CurrentPrincipal;

        protected readonly ISet<SecurityRoleType> Roles;

        public bool RequiresAuthentication { get; protected set; } = true;

        protected static Expression<Func<TEntity, bool>> AllRecords()
        {
            return e => true;
        }

        protected static Expression<Func<TEntity, bool>> NoRecords()
        {
            return e => false;
        }

        protected EntitySecurityRestriction(IPrincipalProvider principalProvider)
        {
            CurrentPrincipal = principalProvider.Current;
            Roles = new HashSet<SecurityRoleType>();

            foreach (var roleName in CurrentPrincipal.Roles)
            {
                SecurityRoleType role;

                if (Enum.TryParse(roleName, out role))
                {
                    Roles.Add(role);
                }
            }
        }

        public abstract Expression<Func<TEntity, bool>> GetRestriction();

        LambdaExpression IEntitySecurityRestriction.GetRestriction()
        {
            if (RequiresAuthentication && (!CurrentPrincipal.IsAuthenticated || !CurrentPrincipal.Id.HasValue))
            {
                return NoRecords();
            }

            return GetRestriction();
        }
    }
}
