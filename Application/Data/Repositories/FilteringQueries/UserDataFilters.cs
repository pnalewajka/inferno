using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public class UserDataFilters
    {
        private readonly Dictionary<Type, EntityDataFilter> _conditionsForEntities = new Dictionary<Type, EntityDataFilter>();

        protected static IAtomicPrincipal Principal => new ThreadPrincipalProvider().Current;

        public void Add<T>(Expression<Func<T, bool>> condition)
        {
            _conditionsForEntities[typeof(T)] = new EntityDataFilter(condition);
        }

        public EntityDataFilter GetEntityDataFilter(Type entityType)
        {
            EntityDataFilter result;

            if (_conditionsForEntities.TryGetValue(entityType, out result))
            {
                return result;
            }

            return null;
        }
    }
}