﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Repositories.Helpers;
using System.Reflection;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public class DataFilteringVisitor : ExpressionVisitor
    {
        private static MethodInfo _whereMethod = MethodHelper.GetGenericMethodByCall(
            () => Enumerable.Where<object>(null, x => true));

        private static MethodInfo _firstOrDefaultMethod = MethodHelper.GetGenericMethodByCall(
           () => Enumerable.FirstOrDefault<object>(null));

        private static MethodInfo _filteredQueryableMethod = MethodHelper.GetGenericMethodByCall(
            () => DataFilterExtensions.Filtered(new IEntity[] { }.AsQueryable()));

        private static MethodInfo _filteredEnumerableMethod = MethodHelper.GetGenericMethodByCall(
            () => DataFilterExtensions.Filtered(new object[] { }.AsEnumerable()));

        private static MethodInfo _filteredOrDefaultMethod = MethodHelper.GetGenericMethodByCall(
            () => DataFilterExtensions.FilteredOrDefault<object>(null));

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.IsStatic)
            {
                var caller = node.Arguments.ElementAtOrDefault(0);
                var method = node.Method.IsGenericMethod
                    ? node.Method.GetGenericMethodDefinition()
                    : node.Method;

                if (caller != null)
                {
                    if (method == _filteredQueryableMethod || method == _filteredEnumerableMethod)
                    {
                        var entityType = TypeHelper.GetRawGenericArguments(caller.Type, typeof(ICollection<>))[0];
                        var whereMethod = _whereMethod.MakeGenericMethod(entityType);
                        var predicate = GetPredicate(entityType);

                        return Expression.Call(whereMethod, caller, Visit(predicate));
                    }
                    else if (method == _filteredOrDefaultMethod)
                    {
                        var entityType = node.Type;

                        var newArray = Expression.NewArrayInit(entityType, caller);
                        var castToCollection = Expression.Convert(newArray, typeof(ICollection<>).MakeGenericType(entityType));
                        var filteredCall = Expression.Call(_filteredEnumerableMethod.MakeGenericMethod(entityType), castToCollection);

                        var withFinalSelector = Expression.Call(
                            _firstOrDefaultMethod.MakeGenericMethod(entityType),
                            filteredCall
                        );

                        return Visit(withFinalSelector);
                    }
                }
            }

            return base.VisitMethodCall(node);
        }

        private Expression GetPredicate(Type entityType)
        {
            var filterExpr = DataFilterExtensions.GetFilteringConditions(entityType)?.AsExpression;
            var securityRestrictionExpr = DataFilterExtensions.GetEntitySecurityRestriction(entityType)?.AsExpression;

            if (filterExpr != null && securityRestrictionExpr != null)
            {
                return DynamicQueryHelper.And(entityType, filterExpr, securityRestrictionExpr);
            }

            return filterExpr ?? securityRestrictionExpr ?? DynamicQueryHelper.ConstantExpression(entityType, true);
        }
    }
}