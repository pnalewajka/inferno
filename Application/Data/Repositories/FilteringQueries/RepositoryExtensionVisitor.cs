﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Helpers;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public class RepositoryExtensionVisitor
        : ExpressionVisitor
    {
        private static MethodInfo _repositoryExtensionMethod = MethodHelper.GetGenericMethodByCall(
            () => EntityExtensions.Repository(null as SimpleEntity, e => new[] { new { e.Id } }));

        private static MethodInfo _asQueryableExtensionMethod = MethodHelper.GetGenericMethodByCall(
            () => Queryable.AsQueryable<int>(null));

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.IsStatic)
            {
                var caller = node.Arguments.ElementAtOrDefault(0);
                var method = node.Method.IsGenericMethod
                    ? node.Method.GetGenericMethodDefinition()
                    : node.Method;

                if (caller != null)
                {
                    if (method == _repositoryExtensionMethod)
                    {
                        var selector = PropertyHelper.ExtractLambdaExpression(node.Arguments.ElementAt(1));
                        var replacements = selector.Parameters.ToDictionary(i => i, i => caller);
                        var member = ((MemberExpression)selector.Body).Member;
                        var memberExpression = Expression.Property(caller, member.Name);
                        var collectionElementType = TypeHelper.GetCollectionElementType(memberExpression.Type);
                        var elementAsQueryable = _asQueryableExtensionMethod.MakeGenericMethod(collectionElementType);
                        var collectionAsQueryable = Expression.Call(elementAsQueryable, memberExpression);

                        return collectionAsQueryable;
                    }
                }
            }

            return base.VisitMethodCall(node);
        }
    }
}
