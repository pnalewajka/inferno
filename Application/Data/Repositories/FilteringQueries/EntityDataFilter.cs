using System.Linq.Expressions;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public class EntityDataFilter
    {
        public LambdaExpression AsExpression { get; private set; }

        private object _asCompiled;

        public object AsCompiled => _asCompiled ?? (_asCompiled = AsExpression.Compile());

        public EntityDataFilter(LambdaExpression condition)
        {
            AsExpression = condition;
        }
    }
}