﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public abstract class RelatedEntitySecurityRestriction<TEntity, TRelatedEntity> : EntitySecurityRestriction<TEntity>
        where TEntity : IEntity
        where TRelatedEntity : IEntity
    {
        private readonly EntitySecurityRestriction<TRelatedEntity> _relatedSecurityRestriction;

        protected abstract BusinessLogic<TEntity, TRelatedEntity> RelatedEntitySelector { get; }

        public RelatedEntitySecurityRestriction(
            IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            _relatedSecurityRestriction = EntitySecurityRestrictionsHelper.GetSecurityRestriction<TRelatedEntity>();
            RequiresAuthentication = _relatedSecurityRestriction.RequiresAuthentication;
        }

        public override Expression<Func<TEntity, bool>> GetRestriction()
        {
            var relatedEntityRestriction = GetRelatedEntityRestriction();
            var entityRestriction = GetEntityRestriction();

            return new BusinessLogic<TEntity, bool>(
                e => relatedEntityRestriction.Call(e) && entityRestriction.Call(e));
        }

        private BusinessLogic<TEntity, bool> GetRelatedEntityRestriction()
        {
            var relatedBusinessLogic = new BusinessLogic<TRelatedEntity, bool>(_relatedSecurityRestriction.GetRestriction());

            return new BusinessLogic<TEntity, bool>(c => relatedBusinessLogic.Call(RelatedEntitySelector.Call(c)));
        }

        protected virtual BusinessLogic<TEntity, bool> GetEntityRestriction()
        {
            return new BusinessLogic<TEntity, bool>(AllRecords());
        }
    }
}
