using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public class InterceptingProvider : IDbAsyncQueryProvider
    {
        private readonly IQueryProvider _underlyingProvider;
        private readonly Func<Expression, Expression>[] _visitors;

        /// <summary>
        /// Original provider. This readonly property must be available for InterceptedQuery
        /// </summary>
        internal IQueryProvider UnderlyingProvider => _underlyingProvider;

        private InterceptingProvider(
            IQueryProvider underlyingQueryProvider,
            params Func<Expression, Expression>[] visitors)
        {
            _underlyingProvider = underlyingQueryProvider;
            _visitors = visitors;
        }

        public static IQueryable<T> Intercept<T>(
            IQueryable<T> underlyingQuery,
            params ExpressionVisitor[] visitors)
        {
            Func<Expression, Expression>[] visitFuncs =
                visitors
                    .Select(v => (Func<Expression, Expression>)v.Visit)
                    .ToArray();
            
            return Intercept(underlyingQuery, visitFuncs);
        }

        public static IQueryable<T> Intercept<T>(
            IQueryable<T> underlyingQuery,
            params Func<Expression, Expression>[] visitors)
        {
            var provider = new InterceptingProvider(
                underlyingQuery.Provider,
                visitors);
            
            return provider
                .CreateQuery<T>(underlyingQuery.Expression);
        }

        public IQueryable<TElement> CreateNativeQuery<TElement>(
            Expression expression)
        {
            return _underlyingProvider
                .CreateQuery<TElement>(InterceptExpr(expression));
        }

        public IQueryable<TElement> CreateQuery<TElement>(
            Expression expression)
        {
            return new InterceptedQuery<TElement>(this, expression);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            Type iEnumerableType = TypeHelper.FindIEnumerable(expression.Type);
            var entityType = TypeHelper.GetRawGenericArguments(iEnumerableType, typeof (IEnumerable<>))[0];

            Type genericType = typeof(InterceptedQuery<>).MakeGenericType(entityType);
            object[] args = { this, expression };

            ConstructorInfo constructorInfo = genericType.GetConstructor(
                BindingFlags.Public | BindingFlags.Instance,
                null,
                new[] {  
                    typeof(InterceptingProvider), 
                    typeof(Expression) 
                },
                null);

            return (IQueryable)constructorInfo.Invoke(args);
        }
        
        public TResult Execute<TResult>(Expression expression)
        {
            return _underlyingProvider
                .Execute<TResult>(InterceptExpr(expression));
        }
        
        public object Execute(Expression expression)
        {
            return _underlyingProvider
                .Execute(InterceptExpr(expression));
        }
        
        private Expression InterceptExpr(Expression expression)
        {
            Expression exp = expression;
            
            foreach (var visitor in _visitors)
            {
                exp = visitor(exp);
            }
            
            return exp;
        }

        public Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            if (_underlyingProvider is IDbAsyncQueryProvider asyncProvider)
            {
                return asyncProvider.ExecuteAsync(InterceptExpr(expression), cancellationToken);
            }

            return Task.FromResult(Execute(expression));
        }

        public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            if (_underlyingProvider is IDbAsyncQueryProvider asyncProvider)
            {
                return asyncProvider.ExecuteAsync<TResult>(InterceptExpr(expression), cancellationToken);
            }

            return Task.FromResult(Execute<TResult>(expression));
        }
    }
}