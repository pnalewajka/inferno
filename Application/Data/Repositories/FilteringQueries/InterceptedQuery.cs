using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;

namespace Smt.Atomic.Data.Repositories.FilteringQueries
{
    public class InterceptedQuery<T> : IOrderedQueryable<T>, IDbAsyncEnumerable<T>
    {
        private readonly Expression _expression;
        private readonly InterceptingProvider _provider;

        public InterceptedQuery(
            InterceptingProvider provider,
            Expression expression)
        {
            _provider = provider;
            _expression = expression;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _provider.CreateNativeQuery<T>(_expression).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _provider.CreateNativeQuery<T>(_expression).GetEnumerator();
        }

        IDbAsyncEnumerator IDbAsyncEnumerable.GetAsyncEnumerator()
        {
            var query = _provider.CreateNativeQuery<T>(_expression);

            if (query is IDbAsyncEnumerable asyncQuery)
            {
                return asyncQuery.GetAsyncEnumerator();
            }

            throw new InvalidOperationException("Unable to perform async queries on mocks");
        }

        IDbAsyncEnumerator<T> IDbAsyncEnumerable<T>.GetAsyncEnumerator()
        {
            var query = _provider.CreateNativeQuery<T>(_expression);

            if (query is IDbAsyncEnumerable<T> asyncQuery)
            {
                return asyncQuery.GetAsyncEnumerator();
            }

            throw new InvalidOperationException("Unable to perform async queries on mocks");
        }

        public Type ElementType => typeof(T);

        public Expression Expression => _expression;

        public IQueryProvider Provider => _provider;

        /// <summary>
        /// Include additional objects from db, path syntax is consistent with EF Include implementation
        /// </summary>
        /// <param name="path">Objects to include</param>
        /// <returns></returns>
        public virtual InterceptedQuery<T> Include(string path)
        {
            var expression = _provider.UnderlyingProvider.CreateQuery<T>(_expression).Include(path).Expression;

            return new InterceptedQuery<T>(_provider, expression);
        }
    }
}