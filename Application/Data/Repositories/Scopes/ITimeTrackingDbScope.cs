﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ITimeTrackingDbScope : IDbScope
    {
        IRepository<TimeReport> TimeReports { get; }

        IRepository<TimeReportRow> TimeReportRows { get; }

        IRepository<TimeReportDailyEntry> DailyEntries { get; }

        IRepository<VacationBalance> VacationBalance { get; set; }

        IReadOnlyRepository<Employee> Employees { get; }

        IReadOnlyRepository<User> Users { get; }

        IReadOnlyRepository<EmploymentPeriod> EmploymentPeriods { get; }

        IRepository<Project> Projects { get; }

        IReadOnlyRepository<TimeTrackingImportSource> ImportSources { get; }

        IReadOnlyRepository<AllocationRequest> AllocationRequests { get; }

        IReadOnlyRepository<Request> Requests { get; }

        IRepository<AbsenceType> AbsenceTypes { get; }

        IRepository<CalendarAbsence> CalendarAbsences { get; }

        IRepository<ContractType> ContractTypes { get; }

        IRepository<OvertimeApproval> OvertimeApprovals { get; }

        IRepository<EmployeeAbsence> EmployeeAbsences { get; }

        IReadOnlyRepository<OrgUnit> OrgUnits { get; }

        IRepository<JiraIssueToTimeReportRowMapper> JiraIssueToTimeReportRowMappers { get; }

        IRepository<OvertimeBank> OvertimeBanks { get; }

        IRepository<Substitution> Substitutions { get; }

        IRepository<AbsenceCalendarEntry> AbsenceCalendarEntries { get; }

        IRepository<TimeReportProjectAttribute> TimeReportProjectAttributes { get; }

        IRepository<TimeReportProjectAttributeValue> TimeReportProjectAttributeValues { get; }

        IRepository<AbsenceRequest> AbsenceRequests { get; }

        IRepository<OvertimeRequest> OvertimeRequests { get; }

        IRepository<ProjectTimeReportApprovalRequest> ProjectTimeReportApprovalRequests { get; }

        IReadOnlyRepository<AddHomeOfficeRequest> HomeOfficeRequests { get; }

        IReadOnlyRepository<UtilizationCategory> UtilizationCategories { get; }

        IRepository<ClockCardDailyEntry> ClockCardDailyEntries { get; }

        IRepository<WeeklyAllocationStatus> WeeklyAllocationStatuses { get; }

        IReadOnlyRepository<Currency> Currencies { get; set; }
    }
}
