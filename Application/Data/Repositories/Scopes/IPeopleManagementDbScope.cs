﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IPeopleManagementDbScope : IDbScope
    {
        IRepository<UserAcronym> UserAcronyms { get; }

        IReadOnlyRepository<EmploymentTerminationReason> EmploymentTerminationReasons { get; }

        IReadOnlyRepository<AssetType> AssetTypes { get; }

        IReadOnlyRepository<AssetsRequest> AssetRequest { get; }

        IReadOnlyRepository<OnboardingRequest> OnboardingRequest { get; }

        IReadOnlyRepository<Request> Request { get; }

        IRepository<AssetsRequest> AssetRequests { get; }

        IReadOnlyRepository<Employee> Employee { get; }

        IRepository<OnboardingRequest> OnboardingRequests { get; }

        IReadOnlyRepository<Location> Location { get; }
    }
}