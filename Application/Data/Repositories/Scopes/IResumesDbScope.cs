﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IResumesDbScope : IDbScope
    {
        IRepository<Resume> Resumes { get; }
        IRepository<ResumeTechnicalSkill> ResumeTechnicalSkills { get; }
        IRepository<ResumeCompany> ResumeCompanies { get; }
        IRepository<ResumeProject> ResumeProjects { get; }
        IRepository<ResumeLanguage> ResumeLanguages { get; }
        IRepository<ResumeTraining> ResumeTrainings { get; }
        IRepository<ResumeEducation> ResumeEducations { get; }
        IRepository<Industry> Industries { get; }
        IRepository<Skill> Skills { get; }
        IReadOnlyRepository<Employee> Employees { get; }
        IRepository<SkillTag> SkillTags { get; }
        IRepository<ResumeAdditionalSkill> ResumeAdditionalSkills { get; }
        IRepository<ResumeShareToken> ResumeShareToken { get; }
    }
}

