﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IGDPRDbScope : IDbScope
    {
        IReadOnlyRepository<Employee> Employees { get; }
        IReadOnlyRepository<Candidate> Candidates { get; }
        IReadOnlyRepository<Company> Companies { get; }
        IRepository<RecommendingPerson> RecommendingPersons { get; }
        IReadOnlyRepository<InboundEmailDocument> InboundEmailDocuments { get; }
        IReadOnlyRepository<InboundEmail> InboundEmails { get; }
        IRepository<DataConsent> DataConsents { get; }
        IRepository<DataOwner> DataOwners { get; }
        IRepository<DataActivity> DataActivities { get; }
        IRepository<DataConsentDocument> DataConsentDocuments { get; }
        IRepository<DataConsentDocumentContent> DataConsentDocumentContents { get; }
        IRepository<DataProcessingAgreement> DataProcessingAgreements { get; }
        IRepository<DataProcessingAgreementDocument> DataProcessingAgreementDocuments { get; }
        IRepository<DataProcessingAgreementDocumentContent> DataProcessingAgreementDocumentContents { get; }
        IRepository<DataProcessingAgreementConsent> DataProcessingAgreementConsents { get; }
    }
}
