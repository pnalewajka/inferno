﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IMeetMeDbScope : IDbScope
    {
        IRepository<Meeting> Meetings { get; }
        IRepository<MeetingNote> MeetingNotes { get; }
        IRepository<MeetingAttachment> MeetingAttachments { get; }
        IRepository<MeetingAttachmentContent> MeetingAttachmentContents { get; }
        IRepository<MeetingNoteAttachment> MeetingNoteAttachments { get; }
        IRepository<MeetingNoteAttachmentContent> MeetingNoteAttachmenContents { get; }
        IRepository<Feedback> Feedbacks { get; }
        IReadOnlyRepository<Employee> Employees { get; }
        IReadOnlyRepository<AllocationRequest> AllocationRequests { get; }
        IReadOnlyRepository<EmploymentPeriod> EmploymentPeriods { get; }
        IReadOnlyRepository<EmployeeDataChangeRequest> EmployeeDataChangeRequests { get; }
    }
}
