﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Finance;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IFinanceDbScope : IDbScope
    {
        IRepository<AggregatedProjectTransaction> AggregatedProjectTransactions { get; }

        IReadOnlyRepository<Project> Projects { get; }
        IRepository<GlobalRate> GlobalRates { get; }
    }
}
