﻿using Smt.Atomic.Data.Entities.Modules.Crm;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ICrmDbScope : IDbScope
    {
        IRepository<CrmCandidate> Candidates { get; }
    }
}