﻿using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ISchedulingDbScope : IDbScope
    {
        IRepository<Pipeline> Pipelines { get; }
        IRepository<JobDefinition> JobDefinitions { get; }
        IRepository<JobDefinitionParameter> JobDefinitionParameters { get; }
        IRepository<JobLog> JobLogs { get; }
        IRepository<JobRunInfo> JobRunInfos { get; }
        IRepository<JobInternalParameter>  JobInternalParameters { get; }
    }
}