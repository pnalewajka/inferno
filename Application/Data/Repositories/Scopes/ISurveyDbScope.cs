﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ISurveyDbScope : IDbScope
    {
        IRepository<SurveyDefinition> SurveyDefinitions { get; }
        IRepository<SurveyQuestion> SurveyQuestions { get; }
        IRepository<SurveyConduct> SurveyConducts { get; }
        IRepository<SurveyConductQuestion> SurveyConductQuestions { get; }
        IRepository<OrgUnit> OrgUnits { get; set; }
        IRepository<Employee> Employees { get; set; }
        IRepository<Survey> Surveys { get; }
        IRepository<SurveyAnswer> SurveyAnswers { get; }
        IRepository<ActiveDirectoryGroup> ActiveDirectoryGroups { get; }
        IRepository<WhistleBlowing> WhistleBlowings { get; }
        IRepository<WhistleBlowingDocumentContent> WhistleBlowingDocumentContents { get; }
        IRepository<WhistleBlowingDocument> WhistleBlowingDocuments { get; }
    }
}

