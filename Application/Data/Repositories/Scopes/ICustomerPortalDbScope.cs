﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ICustomerPortalDbScope : IDbScope
    {
        IRepository<Recommendation> Recommendations { get; }
        IRepository<Challenge> Challenges { get; }
        IRepository<Document> Documents { get; }
        IRepository<DocumentContent> DocumentContents { get; }
        IRepository<NeededResource> NeededResources { get; }
        IRepository<Question> Questions { get; }
        IRepository<Note> Notes { get; }
        IRepository<ScheduleAppointment> ScheduleAppointments { get; }
        IRepository<ChallengeFile> ChallengeFiles { get; }
        IRepository<ChallengeDocument> ChallengeDocuments { get; }
        IRepository<AnswerDocument> AnswerDocuments { get; }
        IRepository<ResourceForm> ResourceForms { get; }
        IRepository<ResourceFormFile> ResourceFiles { get; }
        IRepository<LanguageOption> LanguageOptions { get; }
        IReadOnlyRepository<User> Users { get; }
        IRepository<MainTechnologyOptions> MainTechnologyOptions { get; }
        IRepository<BusinesUnitOptions> BusinesUnitOptions { get; }
        IRepository<RegionOptions> RegionOptions { get; }
        IRepository<ClientContact> ClientContacts { get; }
        IRepository<ClientData> ClientDatas { get; }
        IRepository<ActivityLog> ActivityLogs { get; }
    }
}

