﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IWorkflowsDbScope : IDbScope
    {
        IRepository<User> Users { get; }
        IReadOnlyRepository<Employee> Employees { get; }
        IRepository<EmploymentPeriod> EmploymentPeriods { get; }
        IRepository<PricingEngineer> PricingEngineers { get; }
        IRepository<TechnicalInterviewer> TechnicalInterviewers { get; }
        IReadOnlyRepository<ContractType> ContractTypes { get; }
        IReadOnlyRepository<JobTitle> JobTitles { get; }
        IReadOnlyRepository<JobProfile> JobProfiles { get; }
        IRepository<Request> Requests { get; }
        IRepository<Approval> Approvals { get; }
        IRepository<ApprovalGroup> ApprovalGroups { get; }
        IRepository<ActionSet> ActionSets { get; }
        IRepository<ActionSetTriggeringStatus> ActionSetTriggeringStatus { get; }
        IRepository<Substitution> Substitutions { get; }
        IRepository<EmployeeAbsence> EmployeeAbsences { get; }
        IRepository<VacationBalance> VacationBalances { get; }
        IReadOnlyRepository<TimeReport> TimeReports { get; }
        IRepository<TimeReportRow> TimeReportRows { get; }
        IRepository<AbsenceCalendarEntry> AbsenceCalendarEntries { get; }
        IReadOnlyRepository<AbsenceType> AbsenceTypes { get; set; }
        IRepository<RequestHistoryEntry> RequestHistoryEntries { get; }
        IRepository<SkillChangeRequest> SkillChangeRequests { get; }
        IRepository<AddEmployeeRequest> AddEmployeeRequests { get; }
        IRepository<AbsenceRequest> AbsenceRequests { get; }
        IRepository<AddHomeOfficeRequest> AddHomeOfficeRequests { get; }
        IRepository<OvertimeRequest> OvertimeRequests { get; }
        IRepository<EmploymentTerminationRequest> EmploymentTerminationRequests { get; }
        IRepository<ReopenTimeTrackingReportRequest> ReopenTimeTrackingReportRequests { get; }
        IRepository<ProjectTimeReportApprovalRequest> ProjectTimeReportApprovalRequests { get; }
        IRepository<EmployeeDataChangeRequest> EmployeeDataChangeRequests { get; }
        IRepository<EmploymentDataChangeRequest> EmploymentDataChangeRequests { get; }
        IRepository<RequestDocument> RequestDocuments { get; }
        IRepository<RequestDocumentContent> RequestDocumentContents { get; }
        IRepository<BusinessTripRequest> BusinessTripRequests { get; set; }
        IRepository<OnboardingRequest> OnboardingRequests { get; set; }
        IRepository<BusinessTrip> BusinessTrips { get; set; }
        IRepository<BusinessTripParticipant> BusinessTripParticipants { get; set; }
        IRepository<BusinessTripMessage> BusinessTripMessage { get; set; }
        IRepository<DeclaredLuggage> DeclaredLuggages { get; set; }
        IRepository<AdvancedPaymentRequest> AdvancedPaymentRequests { get; set; }
        IRepository<EmployeePhotoDocument> EmployeePhotoDocuments { get; set; }
        IRepository<EmployeePhotoDocumentContent> EmployeePhotoDocumentContents { get; set; }
        IRepository<PayrollDocument> PayrollDocuments { get; set; }
        IRepository<PayrollDocumentContent> PayrollDocumentContents { get; set; }
        IRepository<ForeignEmployeeDocument> ForeignEmployeeDocuments { get; set; }
        IRepository<ForeignEmployeeDocumentContent> ForeignEmployeeDocumentContents { get; set; }
        IRepository<AssetsRequest> AssetsRequests { get; set; }
        IRepository<FeedbackRequest> FeedbackRequests { get; }
        IRepository<BusinessTripSettlementRequest> BusinessTripSettlementRequests { get; }
        IRepository<BusinessTripSettlementRequestAbroadTimeEntry> BusinessTripSettlemenRequestAbroadTimeEntries { get; }
        IRepository<BusinessTripSettlementRequestMeal> BusinessTripSettlementRequestMeals { get; }
        IRepository<BusinessTripSettlementRequestOwnCost> BusinessTripSettlementRequestOwnCosts { get; }
        IRepository<BusinessTripSettlementRequestCompanyCost> BusinessTripSettlementRequestCompanyCosts { get; }
        IRepository<BusinessTripSettlementRequestAdvancePayment> BusinessTripSettlementRequestAdvancePayments { get; }
        IRepository<BusinessTripSettlementRequestVehicleMileage> BusinessTripSettlementRequestVehicleMileages { get; }
        IRepository<ArrangementTrackingEntry> ArrangementTrackingEntries { get; }
        IRepository<ArrangementDocument> ArrangementDocuments { get; }
        IRepository<Arrangement> Arrangements { get; }
        IRepository<BusinessTripAcceptanceConditions> BusinessTripAcceptanceConditions { get; }
        IRepository<TaxDeductibleCostRequest> TaxDeductibleCostRequests { get; }
        IRepository<BonusRequest> BonusRequests { get; }
        IRepository<ExpenseRequest> ExpenseRequests { get; }
        IRepository<RecruitmentProcess> RecruitmentProcesses { get; }
        IRepository<Bonus> Bonuses { get; }
    }
}

