﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ISkillManagementDbScope : IDbScope
    {
        IRepository<JobProfile> JobProfiles { get; }
        IRepository<Skill> Skills { get; }
        IRepository<JobMatrix> JobMatrix { get; }
        IRepository<JobMatrixLevel> JobMatrixLevels { get; }
        IRepository<SkillTag> SkillTags { get; }
        IRepository<Project> Projects { get; }
        IRepository<JobTitle> JobTitles { get; }
        IRepository<JobProfileSkillTag> JobProfileSkillTags { get; }
    }
}



