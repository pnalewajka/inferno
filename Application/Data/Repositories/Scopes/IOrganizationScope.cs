﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IOrganizationScope : IDbScope
    {
        IRepository<OrgUnit> OrgUnits { get; set; }

        IRepository<Employee> Employees { get; set; }

        IRepository<PricingEngineer> PricingEngineers { get; }

        IRepository<TechnicalInterviewer> TechnicalInterviewers { get; }

        IReadOnlyRepository<Project> Projects { get; }

        IReadOnlyRepository<SurveyConduct> SurveyConducts { get; }

        IReadOnlyRepository<SecurityProfile> SecurityProfiles { get; }
    }
}