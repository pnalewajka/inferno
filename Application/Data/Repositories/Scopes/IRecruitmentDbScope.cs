﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IRecruitmentDbScope : IDbScope
    {
        IRepository<JobOpening> JobOpenings { get; }
        IRepository<ProjectDescription> ProjectDescriptions { get; }
        IRepository<ApplicationOrigin> ApplicationOrigins { get; }
        IRepository<Candidate> Candidates { get; }
        IRepository<CandidateLanguage> CandidateLanguages { get; }
        IRepository<CandidateContactRecord> CandidateContactRecords { get; }
        IRepository<RecruitmentProcess> RecruitmentProcesses { get; }
        IRepository<RecruitmentProcessStep> RecruitmentProcessSteps { get; }
        IRepository<TechnicalReview> TechnicalReviews { get; }
        IRepository<RecommendingPerson> RecommendingPersons { get; }
        IRepository<JobApplication> JobApplications { get; }
        IRepository<DataConsent> DataConsents { get; }
        IReadOnlyRepository<Employee> Employees { get; }
        IRepository<DataOwner> DataOwner { get; }
        IRepository<CandidateNote> CandidateNotes { get; }
        IRepository<JobOpeningNote> JobOpeningNotes { get; }
        IRepository<RecruitmentProcessNote> RecruitmentProcessNotes { get; }
        IRepository<RecommendingPersonNote> RecommendingPersonNotes { get; }
        IRepository<InboundEmail> InboundEmails { get; }
        IRepository<InboundEmailDataActivity> InboundEmailDataActivities { get; }
        IRepository<InboundEmailDocument> InboundEmailDocuments { get; }
        IRepository<InboundEmailDocumentContent> InboundEmailDocumentContents { get; }
        IRepository<CandidateFileDocument> CandidateDocuments { get; }
        IRepository<CandidateFileDocumentContent> CandidateDocumentContents { get; }
        IRepository<CandidateFile> CandidateFiles { get; }
        IRepository<InboundEmailValue> InboundEmailValues { get; }
        IRepository<RecommendingPersonContactRecord> RecommendingPersonContactRecords { get; }
        IRepository<JobProfile> JobProfiles { get; }
        IRepository<Company> Companies { get; }
        IRepository<OrgUnit> OrgUnits { get; }
        IRepository<User> Users { get; }
        IRepository<JobTitle> JobTitles { get; }
        IRepository<Location> Locations { get; }
        IRepository<City> Cities { get; }
        IRepository<Customer> Customers { get; }
        IRepository<RecruitmentProcessFinal> Final { get; }
        IRepository<RecruitmentProcessOffer> Offer { get; }
        IRepository<RecruitmentProcessScreening> Screening { get; }
        IRepository<DataConsentDocument> DataConsentDocument { get; }
        IRepository<Language> Language { get; }
        IRepository<DataOwner> DataOwners { get; }
        IRepository<RecruitmentProcessHistoryEntry> RecruitmentProcessHistoryEntries { get; }
        IRepository<JobOpeningHistoryEntry> JobOpeningHistoryEntries { get; }
    }
}