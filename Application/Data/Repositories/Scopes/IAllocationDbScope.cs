﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IAllocationDbScope : IDbScope
    {
        IRepository<Employee> Employees { get; }

        IRepository<JobProfile> JobProfiles { get; }

        IRepository<Skill> Skills { get; }

        IRepository<JobMatrix> JobMatrixs { get; }

        IRepository<JobMatrixLevel> JobMatrixLevels { get; }

        IRepository<EmploymentPeriod> EmploymentPeriods { get; }

        IRepository<Project> Projects { get; }

        IRepository<StaffingDemand> StaffingDemands { get; }

        IRepository<AllocationRequest> AllocationRequests { get; }

        IRepository<AllocationDailyRecord> AllocationDailyRecords { get; }

        IRepository<WeeklyAllocationStatus> WeeklyAllocationStatus { get; }

        IRepository<WeeklyAllocationDetail> WeeklyAllocationDetails { get; }

        IRepository<OrgUnit> OrgUnits { get; }

        IRepository<Industry> Industries { get; }

        IRepository<SkillTag> SkillTags { get; }

        IRepository<ProjectPicture> ProjectPictures { get; }

        IRepository<ProjectPictureContent> ProjectPictureContents { get; }

        IRepository<SoftwareCategory> SoftwareCategories { get; }

        IRepository<ProjectTag> ProjectTags { get; }

        IRepository<EmployeeComment> EmployeeComments { get; }

        IRepository<AllocationRequestMetadata> AllocationRequestMetadatas { get; }

        IRepository<Resume> Resumes { get; }

        IRepository<ResumeTechnicalSkill> ResumeTechnicalSkills { get; }

        IRepository<ActiveDirectoryGroup> ActiveDirectoryGroups { get; }

        IRepository<User> Users { get; }

        IRepository<ProjectAttachmentContent> ProjectReferenceMaterialAttachmentContent { get; }

        IRepository<ProjectAttachment> ProjectReferenceMaterialAttachments { get; }

        IReadOnlyRepository<ContractType> ContractTypes { get; }

        IReadOnlyRepository<JobTitle> JobTitles { get; }

        IReadOnlyRepository<Location> Locations { get; }

        IReadOnlyRepository<TimeReportProjectAttribute> TimeReportProjectAttributes { get; set; }

        IReadOnlyRepository<TimeReportProjectAttributeValue> TimeReportProjectAttributeValues { get; set; }

        IReadOnlyRepository<EmployeeAbsence> EmployeeAbsences { get; }

        IRepository<AbsenceRequest> AbsenceRequests { get; }

        IRepository<ProjectInvoicingRate> ProjectInvoicingRates { get; }

        IRepository<ProjectSetup> ProjectSetups { get; }

        IRepository<DataOwner> DataOwners { get; }
    }
}


