﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ICompensationDbScope : IDbScope
    {
        IReadOnlyRepository<Employee> Employees { get; }

        IReadOnlyRepository<EmploymentPeriod> EmploymentPeriods { get; }

        IReadOnlyRepository<TimeReport> TimeReports { get; }

        IReadOnlyRepository<BusinessTripParticipant> BusinessTripParticipants { get; }

        IReadOnlyRepository<BusinessTripSettlementRequest> BusinessTripSettlementRequests { get; }

        IReadOnlyRepository<BusinessTripSettlementRequestAbroadTimeEntry> AbroadTimeEntries { get; }

        IReadOnlyRepository<DailyAllowance> DailyAllowances { get; }

        IReadOnlyRepository<ExpenseRequest> ExpenseRequests { get; }

        IReadOnlyRepository<ContractType> ContractTypes { get; }

        IRepository<InvoiceCalculation> InvoiceCalculations { get; }

        IRepository<Bonus> Bonuses { get; }

        IRepository<CurrencyExchangeRate> CurrencyExchangeRates { get; }

        IRepository<BonusRequest> BonusRequests { get; }

        IRepository<InvoiceDocument> InvoiceDocuments { get; }
    }
}
