﻿using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface INotificationsDbScope : IDbScope
    {
        IRepository<Email> Emails { get; }
        IRepository<EmailAttachment> EmailAttachments { get; }
    }
}

