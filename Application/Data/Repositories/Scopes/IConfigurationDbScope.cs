﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IConfigurationDbScope : IDbScope
    {
        IRepository<MessageTemplate> MessageTemplates { get; }
        IQueryable<DbDateTime> DbDateTime { get; }
        IRepository<SystemParameter> SystemParameters { get; }
        IRepository<Calendar> Calendars { get; }
        IRepository<CalendarEntry> CalendarEntries { get; }
        IRepository<AuditTable> AuditTables { get; }
    }
}

