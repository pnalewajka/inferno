﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IBusinessTripsDbScope : IDbScope
    {
        IRepository<AdvancedPaymentRequest> AdvancedPaymentRequests { get; }
        IRepository<BusinessTripRequest> BusinessTripRequests { get; }
        IRepository<BusinessTrip> BusinessTrips { get; }
        IRepository<BusinessTripMessage> BusinessTripMessages { get; }
        IRepository<BusinessTripParticipant> BusinessTripParticipants { get; }
        IReadOnlyRepository<BusinessTripSettlementRequest> BusinessTripSettlementRequests { get; }
        IRepository<Currency> Currencies { get; }
        IRepository<DeclaredLuggage> LuggageDeclarations { get; }
        IReadOnlyRepository<Project> Projects { get; }
        IReadOnlyRepository<Employee> Employees { get; }
        IReadOnlyRepository<EmploymentPeriod> EmploymentPeriods { get; }
        IRepository<ArrangementTrackingEntry> ArrangementTrackingEntries { get; }
        IRepository<Arrangement> Arrangements { get; }
        IRepository<ArrangementDocument> ArrangementDocuments { get; }
        IRepository<ArrangementDocumentContent> ArrangementDocumentContents { get; }
        IRepository<BusinessTripAcceptanceConditions> BusinessTripAcceptanceConditions { get; }
        IReadOnlyRepository<Country> Countries { get; }
        IReadOnlyRepository<City> Cities { get; }
        IReadOnlyRepository<Company> Companies { get; }
        IRepository<DailyAllowance> DailyAllowances { get; }
        IReadOnlyRepository<Request> Requests { get; }
    }
}


