﻿using Smt.Atomic.Data.Entities.Modules.KPI;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IKPIDbScope : IDbScope
    {
        IRepository<KpiDefinition> KpiDefinitions { get; }
    }
}
