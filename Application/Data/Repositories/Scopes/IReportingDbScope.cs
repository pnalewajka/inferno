﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IReportingDbScope : IDbScope
    {
        IRepository<ReportDefinition> ReportDefinitions { get; }
        IRepository<ReportTemplateContent> ReportTemplateContents { get; }
        IRepository<ReportTemplate> ReportTemplates { get; }
        IReadOnlyRepository<SecurityProfile> SecurityProfiles { get; }
    }
}


