﻿using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IRuntimeDbScope : IDbScope
    {
        IRepository<CacheInvalidation> CacheInvalidations { get; }
        IRepository<TemporaryDocument> TemporaryDocuments { get; }
        IReadOnlyRepository<LastJobRunInfo> LastJobRunInfos { get; }
        IRepository<JobRunInfo> JobRunInfos { get; }
    }
}
