﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Consents;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IConsentsDbScope : IDbScope
    {
        IRepository<ConsentDecision> ConsentDecisions { get; }

        IReadOnlyRepository<User> Users { get; } 
    }
}
