﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface ISalesDbScope : IDbScope
    {
        IRepository<Customer> Customers { get; }
        IRepository<Inquiry> Inquiries { get; }
        IRepository<OfferType> OfferTypes { get; }
        IRepository<InquiryNote> InquiryNotes { get; }
        IReadOnlyRepository<Employee> Employees { get; }
    }
}



