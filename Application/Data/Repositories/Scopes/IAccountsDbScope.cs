﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IAccountsDbScope : IDbScope
    {
        IRepository<User> Users { get; }
        IRepository<Credential> Credentials { get; }
        IRepository<UserDocument> UserDocuments { get; }
        IRepository<UserDocumentContent> UserDocumentContents { get; }
        IRepository<UserPicture> UserPicture { get; }
        IRepository<UserPictureContent> UserPictureContent { get; }
        IRepository<SecurityRole> SecurityRoles { get; }
        IRepository<SecurityProfile> SecurityProfiles { get; }
        IRepository<DataFilter> DataFilters { get; }
        IRepository<DataOwner> DataOwners { get; }
        IReadOnlyRepository<Employee> Employees { get; }
    }
}