﻿using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IEventSourcingDataContext : IDbScope
    {
        IRepository<ProcessedEventQueueItem> ProcessedEventQueue { get; set; }
        IRepository<PublishedEventQueueItem> PublishedEventQueue { get; set; }
    }
}