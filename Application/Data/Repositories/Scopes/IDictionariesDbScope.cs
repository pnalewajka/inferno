﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Scopes
{
    public interface IDictionariesDbScope : IDbScope
    {
        IRepository<Location> Locations { get; }
        IRepository<Industry> Industries { get; }
        IRepository<Region> Regions { get; }
        IRepository<ServiceLine> ServiceLines { get; }
        IRepository<SoftwareCategory> SoftwareCategories { get; }
        IRepository<CustomerSize> CustomerSizes { get; }
        IRepository<Company> Companies { get; }
        IReadOnlyRepository<Employee> Employees { get; }
        IRepository<Language> Languages { get; }
        IRepository<UtilizationCategory> UtilizationCategories { get; }
        IRepository<Country> Countries { get; }
        IRepository<City> Cities { get; }
        IReadOnlyRepository<BusinessTripParticipant> BusinessTripParticipants { get; }
        IRepository<CompanyAllowedAdvancePaymentCurrency> CompanyAllowedAdvancePaymentCurrencies { get; }
    }
}



