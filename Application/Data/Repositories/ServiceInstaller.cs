﻿using Castle.Facilities.TypedFactory;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Services;

namespace Smt.Atomic.Data.Repositories
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp || containerType == ContainerType.WebApi)
            {
                container.Register(Component.For<IPrincipalBuilder>().ImplementedBy<PrincipalBuilder>().LifestylePerWebRequest());

                container.Register(Component.For<IDbTimeService>().ImplementedBy<DbTimeService>().LifestylePerWebRequest());
                container.Register(Component.For<ITimeService>().UsingFactoryMethod((k, c) => k.Resolve<IDbTimeService>()).LifestylePerWebRequest());

                container.Register(Component.For<ICacheInvalidationNotifications>().ImplementedBy<DatabaseCacheInvalidationNotifications>().LifestylePerWebRequest());
                container.Register(Component.For<IEntityMergingService>().ImplementedBy<EntityMergingService>().LifestyleTransient());
                container.Register(Component.For<IDbContextWrapperFactory>().AsFactory().LifestylePerWebRequest());

                container.Register(Component.For(typeof(IReadOnlyUnitOfWorkService<>)).ImplementedBy(typeof(ReadOnlyUnitOfWorkService<>)).LifestylePerWebRequest());
                container.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestylePerWebRequest());
                container.Register(Component.For(typeof(IReadOnlyRepository<>)).ImplementedBy(typeof(ReadOnlyRepository<>)).LifestyleTransient());
                container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifestyleTransient());

                container.Register(Component.For(typeof(IDataFilterService)).ImplementedBy(typeof(DataFilterService)).LifestyleTransient());
            }

            if (containerType == ContainerType.WebServices)
            {
                container.Register(Component.For<IPrincipalBuilder>().ImplementedBy<PrincipalBuilder>().LifestylePerWcfOperation());

                container.Register(Component.For<IDbTimeService>().ImplementedBy<DbTimeService>().LifestylePerWcfOperation());
                container.Register(Component.For<ITimeService>().UsingFactoryMethod((k, c) => k.Resolve<IDbTimeService>()).LifestylePerWcfOperation());

                container.Register(Component.For<ICacheInvalidationNotifications>().ImplementedBy<DatabaseCacheInvalidationNotifications>().LifestylePerWcfOperation());
                container.Register(Component.For<IEntityMergingService>().ImplementedBy<EntityMergingService>().LifestyleTransient());
                container.Register(Component.For<IDbContextWrapperFactory>().AsFactory().LifestylePerWcfOperation());

                container.Register(Component.For(typeof(IReadOnlyUnitOfWorkService<>)).ImplementedBy(typeof(ReadOnlyUnitOfWorkService<>)).LifestylePerWcfOperation());
                container.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestylePerWcfOperation());
                container.Register(Component.For(typeof(IReadOnlyRepository<>)).ImplementedBy(typeof(ReadOnlyRepository<>)).LifestyleTransient());
                container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifestyleTransient());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IPrincipalBuilder>().ImplementedBy<PrincipalBuilder>().LifestyleTransient());
                container.Register(Component.For<IEntityMergingService>().ImplementedBy<EntityMergingService>().LifestyleTransient());
                container.Register(Component.For<IDbTimeService>().ImplementedBy<DbTimeService>().LifestylePerThread());
                container.Register(Component.For<ITimeService>().UsingFactoryMethod((k, c) => k.Resolve<IDbTimeService>()).LifestylePerThread());

                container.Register(Component.For<IDbContextWrapperFactory>().AsFactory().LifestyleTransient());


                container.Register(Component.For(typeof(IReadOnlyUnitOfWorkService<>)).ImplementedBy(typeof(ReadOnlyUnitOfWorkService<>)).LifestylePerThread());
                container.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestylePerThread());
                container.Register(Component.For(typeof(IReadOnlyRepository<>)).ImplementedBy(typeof(ReadOnlyRepository<>)).LifestyleTransient());
                container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifestyleTransient());

                container.Register(Component.For<ICacheInvalidationNotifications>().ImplementedBy<DatabaseCacheInvalidationNotifications>().LifestylePerThread());

                container.Register(Component.For(typeof(IDataFilterService)).ImplementedBy(typeof(DataFilterService)).LifestyleTransient());
            }
        }
    }
}
