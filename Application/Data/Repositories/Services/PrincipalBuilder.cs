﻿using System.Linq;
using System.Security.Claims;
using Castle.Core;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Enums;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Security.Principal.Claims;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Repositories.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class PrincipalBuilder : IPrincipalBuilder
    {
        private readonly IReadOnlyUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        private readonly ILogger _logger;

        public PrincipalBuilder(IReadOnlyUnitOfWorkService<IAccountsDbScope> unitOfWorkService, ILogger logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _logger = logger;
        }

        [Cached(CacheArea = CacheAreas.FeaturePermissions)]
        public IAtomicPrincipal BuildPrincipal(string login)
        {
            return !string.IsNullOrEmpty(login)
                ? GetAuthenticatedPrincipal(login)
                : AtomicPrincipal.Anonymous;
        }

        private IAtomicPrincipal GetAuthenticatedPrincipal(string login)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.SingleOrDefault(u => u.Login == login);

                if (user == null)
                {
                    _logger.Fatal($"User tried to access the page with username {login} stored in cookie, however, this user is not present in database. Probably tampered cookie.");

                    return AtomicPrincipal.Anonymous;
                }

                long employeeId = -1;
                var employeeCompanyName = string.Empty;
                var employeeJobTitle = new LocalizedString
                {
                    English = string.Empty,
                    Polish = string.Empty
                };

                var employee = unitOfWork.Repositories.Employees
                    .Include(nameof(Employee.Company))
                    .SingleOrDefault(u => u.UserId == user.Id);

                if (employee != null)
                {
                    employeeId = employee.Id;

                    if (employee.CompanyId.HasValue)
                    {
                        employeeCompanyName = employee.Company.Name;
                    }

                    employeeJobTitle.English = employee.JobTitle?.NameEn ?? string.Empty;
                    employeeJobTitle.Polish = employee.JobTitle?.NamePl ?? string.Empty;
                }

                var roleNames = user.GetAllRolesNames().ToArray();
                var credentials =
                    user.Credentials.Where(c => c.ProviderType == AuthorizationProviderType.Atomic)
                        .OrderByDescending(x => x.CreatedOn)
                        .ToList();

                var hasOtherCredentials = user.Credentials.Any(c => c.ProviderType != AuthorizationProviderType.Atomic);

                var passwordStatus = credentials.Count == 0
                                         ? PasswordStatus.MustSet
                                         : credentials[0].IsExpired
                                               ? PasswordStatus.MustChange
                                               : PasswordStatus.Valid;

                if (hasOtherCredentials)
                {
                    passwordStatus = PasswordStatus.Valid;
                }

                var claimsWriter = new ClaimsWriter();
                var principal = new AtomicPrincipal(user.Login);
                var claimsIdentity = (ClaimsIdentity)principal.Identity;

                foreach (var roleName in roleNames)
                {
                    claimsWriter.AddClaimValue(claimsIdentity, claimsIdentity.RoleClaimType, roleName);
                }

                foreach (var profileName in user.Profiles.Select(p => p.Name))
                {
                    claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.Profile, profileName);
                }

                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.Id, user.Id.ToInvariantString());
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.Email, user.Email);
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.FirstName, user.FirstName);
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.LastName, user.LastName);
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.PasswordStatus, passwordStatus.ToString());
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.CompanyName, employeeCompanyName);
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.JobTitle, employeeJobTitle.ToString());
                claimsWriter.AddClaimValue(claimsIdentity, AtomicClaimTypes.EmployeeId, employeeId.ToInvariantString());

                return principal;
            }
        }
    }
}