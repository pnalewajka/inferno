﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Repositories.Classes;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Services
{
    internal class UnitOfWorkService<TDbScope> : ReadOnlyUnitOfWorkService<TDbScope>, IUnitOfWorkService<TDbScope>
        where TDbScope : class, IDbScope
    {
        public new IUnitOfWork<TDbScope> Create(bool allowAsync = false)
        {
            return ReflectionHelper.CreateInstanceWithIocDependencies<UnitOfWork<TDbScope>>();
        }

        public IReadOnlyUnitOfWork<TDbScope> CreateReadOnly(bool allowAsync = false)
        {
            return base.Create();
        }

        public IExtendedUnitOfWork<TDbScope> CreateExtended(bool allowAsync = false)
        {
            return ReflectionHelper.CreateInstanceWithIocDependencies<UnitOfWork<TDbScope>>();
        }
    }
}
