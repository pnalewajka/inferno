﻿using System;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Repositories.Services
{
    internal class DatabaseCacheInvalidationNotifications : ICacheInvalidationNotifications
    {
        private readonly IUnitOfWorkService<IRuntimeDbScope> _unitOfWorkService;

        public DatabaseCacheInvalidationNotifications(
            IUnitOfWorkService<IRuntimeDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public Guid GetLastInvalidationToken(string area)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var invalidationToken = 
                    unitOfWork
                    .Repositories
                    .CacheInvalidations
                    .AsNoTracking()
                    .Where(i => i.Area == area)
                    .OrderBy(i => i.Id)
                    .Select(t => t.Token)
                    .FirstOrDefault();
                
                if (invalidationToken != null)
                {
                    return invalidationToken;
                }

                return Guid.Empty;
            }
        }

        public void NotifyAboutAreaInvalidation(string area)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var cacheInvalidationRepository = unitOfWork.Repositories.CacheInvalidations;

                foreach (var cacheInvalidation in cacheInvalidationRepository)
                {
                    cacheInvalidationRepository.Delete(cacheInvalidation);                    
                }

                var invalidation = new CacheInvalidation
                {
                    Area = area,
                    Token = Guid.NewGuid()
                };

                cacheInvalidationRepository.Add(invalidation);

                unitOfWork.Commit();
            }
        }
    }
}