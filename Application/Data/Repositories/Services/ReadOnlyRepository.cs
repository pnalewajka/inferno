﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Services
{
    internal class ReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly IDbContextWrapper DbContext;

        public ReadOnlyRepository(IDbContextWrapper dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Specifies the related objects to include in the query results.
        /// </summary>
        /// <typeparam name="TEntity">The type of entity being queried. </typeparam>
        /// <returns>
        /// A new IQueryable&lt;T&gt; with the defined query path.
        /// </returns>
        public IQueryable<TEntity> Include(string path)
        {
            IQueryable<TEntity> records = FilterAwareQuerable;
            records = records.Include(path);

            return records;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection
        /// </summary>
        /// <returns></returns>
        public IEnumerator<TEntity> GetEnumerator()
        {
            return FilterAwareQuerable.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets the expression tree that is associated with the instance of IQuerable
        /// </summary>
        public Expression Expression => FilterAwareQuerable.Expression;

        /// <summary>
        /// Get the type of the element(s) that are returned when the expression tree associated with this instance of IQuarable is executed
        /// </summary>
        public Type ElementType => FilterAwareQuerable.ElementType;

        /// <summary>
        /// Gets the provider that is associated with this data source
        /// </summary>
        public IQueryProvider Provider => FilterAwareQuerable.Provider;

        protected virtual IQueryable<TEntity> ApplyTrackingConfiguration(IQueryable<TEntity> queryable)
        {
            return queryable.AsNoTracking();
        }

        public void SetTimeout(int timeoutInSeconds)
        {
            DbContext.SetCommandTimeout(timeoutInSeconds);
        }

        /// <summary>
        /// This returns IQuerable based on the original DbContext.Set which
        /// is aware of the Filtered() extension method which can occure
        /// </summary>
        private IQueryable<TEntity> FilterAwareQuerable
        {
            get
            {
                var dbSet = DbContext.Set<TEntity>();

                return ApplyTrackingConfiguration(InterceptingProvider.Intercept(
                    dbSet,
                    new DataFilteringVisitor(),
                    new RepositoryExtensionVisitor(),
                    new BusinessLogicBase.BusinessLogicCallToInlineVisitor()));
            }
        }
    }
}
