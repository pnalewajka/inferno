﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Services
{
    internal class Repository<TEntity> : ReadOnlyRepository<TEntity>, IRepository<TEntity> where TEntity : class, IEntity
    {
        public Repository(IDbContextWrapper dbContext) : base(dbContext)
        {
        }

        public void Attach(TEntity entity)
        {
            DbContext.SetState(entity, EntityState.Unchanged);
        }

        public void Add(TEntity entity)
        {
            DbContext.SetState(entity, EntityState.Added);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }
        }

        public void Edit(TEntity entity)
        {
            DbContext.SetState(entity, EntityState.Modified);
        }

        public void Detach(TEntity entity)
        {
            DbContext.SetState(entity, EntityState.Detached);
        }

        public void ReloadCollection<TCollectionItem>(TEntity entity, Expression<Func<TEntity, ICollection<TCollectionItem>>> collectionExpression) where TCollectionItem : class
        {
            DbContext.Entry(entity).Collection(collectionExpression).Load();
        }

        public void SetPropertyModified(TEntity entity, Expression<Func<TEntity, object>> property)
        {
            DbContext.Entry(entity).Property(property).IsModified = true;
        }

        public void Delete(TEntity entity, bool isDeletePermanent = false)
        {
            var softDeletableEntity = entity as ISoftDeletable;

            if (softDeletableEntity == null || isDeletePermanent)
            {
                HardDelete(entity);
            }
            else
            {
                SoftDelete(softDeletableEntity);
            }
        }

        public void DeleteRange(IEnumerable<TEntity> entities, bool isDeletePermanent = false)
        {
            foreach (var entity in entities.ToList())
            {
                Delete(entity, isDeletePermanent);
            }
        }

        public void Remove(TEntity entity)
        {
            var entitySet = DbContext.Set(entity.GetType());
            entitySet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            var entitySet = DbContext.Set(entities.First().GetType());
            entitySet.RemoveRange(entities);
        }

        public TEntity CreateEntity()
        {
            return DbContext.Set<TEntity>().Create();
        }

        public void DeleteEach(Expression<Func<TEntity, bool>> deletePredicate, bool isDeletePermanent = false)
        {
            foreach (var record in this.Where(deletePredicate))
            {
                Delete(record, isDeletePermanent);
            }
        }

        public IEnumerable<PropertyGroup> GetUniqueKeyViolations(TEntity entity)
        {
            foreach (var uniqueKey in EntityHelper.GetUniqueKeys(entity))
            {
                IQueryable<TEntity> records = this.Where(i => i.Id != entity.Id);
                var isKeyComponentNull = false;

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (string propertyName in uniqueKey.PropertyNames)
                {
                    var property = typeof(TEntity).GetProperty(propertyName);
                    var value = property.GetValue(entity);

                    if (value == null)
                    {
                        isKeyComponentNull = true;
                        break;
                    }

                    var expression = DynamicQueryHelper.GenerateEqualPredicate<TEntity>(propertyName, value);
                    records = records.Where(expression);
                }

                if (!isKeyComponentNull && records.Any())
                {
                    yield return uniqueKey;
                }
            }

            foreach (var customUniqueKey in EntityHelper.GetCustomUniqueKeys(entity))
            {
                IQueryable<TEntity> records = this.Where(i => i.Id != entity.Id);

                var property = typeof(TEntity).GetProperty(customUniqueKey.PropertyName);
                var value = property.GetValue(entity);

                if (value == null)
                {
                    break;
                }

                var expression = DynamicQueryHelper.GenerateEqualPredicate<TEntity>(customUniqueKey.PropertyName, value);
                records = records.Where(expression);

                if (records.ToList().Any(e => customUniqueKey.Validation.Invoke(e)))
                {
                    yield return new PropertyGroup { PropertyNames = new List<string> { customUniqueKey.PropertyName } };
                }
            }
        }

        protected override IQueryable<TEntity> ApplyTrackingConfiguration(IQueryable<TEntity> queryable)
        {
            return queryable;
        }

        private void SoftDelete(ISoftDeletable entity)
        {
            entity.OnSoftDeleting();
            entity.IsDeleted = true;
            DbContext.SetState(entity, EntityState.Modified);
        }

        private void HardDelete(TEntity entity)
        {
            DbContext.SetState(entity, EntityState.Deleted);
        }
    }
}