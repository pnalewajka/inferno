﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Repositories.Classes;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Services
{
    internal class ReadOnlyUnitOfWorkService<TDbScope> : IReadOnlyUnitOfWorkService<TDbScope>
        where TDbScope : class, IDbScope
    {

        public virtual IReadOnlyUnitOfWork<TDbScope> Create(bool allowAsync = false)
        {
            return ReflectionHelper.CreateInstanceWithIocDependencies<ReadOnlyUnitOfWork<TDbScope>>();
        }
    }
}
