﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using Castle.Core.Internal;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Repositories.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class DataFilterService : IDataFilterService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public DataFilterService(IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.DataPermissions)]
        public UserDataFilters GetUserDataFilters(long userId)
        {
            var conditions = new Dictionary<Type, string>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userProfileIds = unitOfWork.Repositories.SecurityProfiles.Where(p => p.Users.Any(u => u.Id == userId)).Select(p => p.Id);
                var dataFilters = unitOfWork.Repositories.DataFilters.Where(f => f.UserId == userId || userProfileIds.Contains(f.ProfileId.Value));

                foreach (var filters in dataFilters.GroupBy(r => r.Module + "." +  r.Entity))
                {
                    var condition = string.Join(" && ", filters.Select(r => r.Condition));
                    AddEntityCondition(filters.Key, condition, conditions);                    
                }
            }

            return BuildCondition(conditions);
        }

        public IEnumerable<string> ValidateCondition(string entity, string condition)
        {
            var conditions = new Dictionary<Type, string>();
            AddEntityCondition(entity, condition, conditions);
            
            var result = new List<string>();

            try
            {
                BuildCondition(conditions);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                result.Add(invalidOperationException.Message);
            }

            return result;
        }

        private static void AddEntityCondition(string entity, string condition, Dictionary<Type, string> conditions)
        {
            var entityAssembly = typeof (User).Assembly;
            var entityType = TypeHelper.GetTypeByName(entityAssembly, entity);
            var fullCondition =
                $"{NamingConventionHelper.ConvertPascalCaseToCamelCase(entityType.Name)} => {string.Join(" && ", condition)}";

            conditions.Add(entityType, fullCondition);
        }

        private UserDataFilters BuildCondition(Dictionary<Type, string> conditions)
        {
            var references = CompilationHelper.GetLoadedReferences();

            var usings = conditions
                .Keys
                .Select(t => t.Namespace)
                .Distinct()
                .OrderBy(n => n)
                .Select(n => $"using {n};\r\n").ToList();

            usings.Add("using System.Linq;\r\n");

            var addLines = conditions.Select(p => $"Add<{p.Key.Name}>({p.Value});\r\n");

            const string codeTemplate =
@"{0}
namespace Smt.Atomic.Data.Repositories.FilteringQueries
{{
    public class UserFilters : UserDataFilters
    {{
        public UserFilters() 
        {{
            {1}
        }}
    }}
}}";
            string sourceCode = string.Format(codeTemplate,  string.Concat(usings), string.Concat(addLines));

            var assembly = CompilationHelper.Compile(sourceCode, references);

            var conditionType = TypeHelper.GetSubclassesOf(typeof(UserDataFilters), assembly).Single();

            return conditionType.CreateInstance<UserDataFilters>();
        }
    }
}