using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Data.Repositories.Services
{
    public class EntityMergingService : IEntityMergingService
    {
        public ICollection<TEntity> AttachEntities<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> foreignEntities)
            where TEntity : class, IEntity
            where TDbScope : IDbScope
        {
            return CreateEntitiesFromIds<TEntity, TDbScope>(unitOfWork, foreignEntities.GetIds());
        }

        public ICollection<TEntity> CreateEntitiesFromIds<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, long[] ids)
            where TEntity : class, IEntity
            where TDbScope: IDbScope
        {
            var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
            var repository = repositoryFactory.Create<TEntity>();

            return new Collection<TEntity>(repository.GetByIds(ids).ToList());
        }

        public void MergeCollections<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> dbCollection, ICollection<TEntity> inputCollection) 
            where TEntity : class, IEntity 
            where TDbScope : IDbScope
        {
            var dbIds = dbCollection.GetIds();
            var inputIds = inputCollection.GetIds();

            var toAddIds = inputIds.Except(dbIds).ToList();
            var toRemoveIds = dbIds.Except(inputIds).ToList();

            var toRemove = dbCollection.Where(e => toRemoveIds.Contains(e.Id)).ToList();

            foreach (var record in toRemove)
            {
                dbCollection.Remove(record);
            }

            var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
            var repository = repositoryFactory.Create<TEntity>();
            var toAdd = repository.GetByIds(toAddIds).ToList();

            if (toAddIds.Count != toAdd.Count)
            {
                throw new InvalidDataException();
            }

            foreach (var record in toAdd)
            {
                dbCollection.Add(record);
            }
        }

        public void MergeCollections<TEntity, TDbScope, TProperty>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> dbCollection, ICollection<TEntity> inputCollection, Func<TEntity, TProperty> discriminatorSelector, bool removeFromRepository)
            where TEntity : class, IEntity
            where TDbScope : IDbScope
        {
            var dbDiscriminators = dbCollection.Select(discriminatorSelector).ToList();
            var inputDiscriminators = inputCollection.Select(discriminatorSelector).ToList();

            var recordsToAdd = inputCollection.Where(e => !dbDiscriminators.Contains(discriminatorSelector(e))).ToList();
            var recordsToRemove = dbCollection.Where(e => !inputDiscriminators.Contains(discriminatorSelector(e))).ToList();

            foreach (var record in recordsToAdd)
            {
                dbCollection.Add(record);
            }

            var repository = removeFromRepository ? ((IRepositoryFactory)unitOfWork.Repositories).Create<TEntity>() : null;

            foreach (var record in recordsToRemove)
            {
                dbCollection.Remove(record);
                repository?.Delete(record);
            }
        }
    }
}