﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Repositories.Services
{
    internal class DbTimeService : ITimeServiceAdministration, IDbTimeService
    {
        private readonly bool _allowFakeTime;
        private readonly TimeSpan _dateTimeSyncPeriod = new TimeSpan(5L*TimeSpan.TicksPerMinute);
        private readonly ISystemSwitchService _systemSwitchService;
        private readonly ILogger _logger;
        private readonly IReadOnlyUnitOfWorkService<IConfigurationDbScope> _readOnlyUnitOfWorkService;

        private DateTime _lastDbDateTime = DateTime.Now;
        private DateTime _lastDbDateTimeSyncTime = DateTime.MinValue;

        public DbTimeService(
            ISystemSwitchService systemSwitchService,
            ISettingsProvider settingsProvider,
            ILogger logger,
            IReadOnlyUnitOfWorkService<IConfigurationDbScope> readOnlyUnitOfWorkService)
        {
            _systemSwitchService = systemSwitchService;
            _logger = logger;
            _readOnlyUnitOfWorkService = readOnlyUnitOfWorkService;

            _allowFakeTime = settingsProvider.IsFakeTimeAllowed;

            if (_allowFakeTime)
            {
                _logger.Warn("Fake time functionality allowed");
            }
        }

        private long OffsetInSeconds
        {
            get
            {
                if (_allowFakeTime)
                {
                    return _systemSwitchService.GetSystemTimeOffset();
                }

                return 0;
            }
        }

        private DateTime DbRealTime
        {
            get
            {
                using (var unitOfWork = _readOnlyUnitOfWorkService.Create())
                {
                    return unitOfWork.Repositories.DbDateTime.Single().CurrentDateTime;
                }
            }
        }

        public DateTime GetCurrentTime()
        {
            var diff = DateTime.Now.Subtract(_lastDbDateTimeSyncTime);

            if (diff > _dateTimeSyncPeriod)
            {
                return SyncWithDb().ToUniversalTime()
                    .AddSeconds(OffsetInSeconds)
                    .ToLocalTime();
            }

            return _lastDbDateTime.ToUniversalTime()
                .AddSeconds(OffsetInSeconds)
                .Add(diff)
                .ToLocalTime();
        }

        public DateTime GetCurrentDate()
        {
            return GetCurrentTime().Date;
        }

        public TimeSpan? GetOffset()
        {
            return OffsetInSeconds == 0L ? (TimeSpan?) null : TimeSpan.FromSeconds(OffsetInSeconds);
        }

        public bool IsTimeFake()
        {
            return OffsetInSeconds != 0L;
        }

        public bool IsFakeTimeAllowed()
        {
            return _allowFakeTime;
        }

        private DateTime SyncWithDb()
        {
            _lastDbDateTime = DbRealTime;
            _lastDbDateTimeSyncTime = DateTime.Now;

            return _lastDbDateTime;
        }
    }
}