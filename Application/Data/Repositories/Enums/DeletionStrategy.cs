﻿namespace Smt.Atomic.Business.Common.Enums
{
    public enum DeletionStrategy
    {
        /// <summary>
        /// Strategy enum to remove delete from database the entity will be marked via Entity Framework as deleted. Cascade delete won't be working, if entity is not in context.
        /// </summary>
        Delete,

        /// <summary>
        /// Strategy enum to remove entities from database (no other entities will be marked as deleted). If cascade delete specified it will be performed at DB side.
        /// </summary>
        Remove
    }
}
