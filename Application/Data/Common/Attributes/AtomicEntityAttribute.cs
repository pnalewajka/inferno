﻿using System;

namespace Smt.Atomic.Data.Common.Attributes
{
    /// <summary>
    /// Base attribute class for all attributes related to Entity meta description
    /// </summary>
    public class AtomicEntityAttribute : Attribute
    {
    }
}
