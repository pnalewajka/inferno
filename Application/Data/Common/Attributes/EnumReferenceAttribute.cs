﻿using System;

namespace Smt.Atomic.Data.Common.Attributes
{
    /// <summary>
    /// This attribute informs EF that specific property is an Enum represented in DB
    /// Entity type is used by EF convention to generate DB column name:
    /// For example
    /// [EnumReferenceAttribute]
    /// public TestType MyTestType { get; set; }
    /// Will create column in Db:  TestTypeEnumId
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class EnumReferenceAttribute : AtomicEntityAttribute
    {
    }
}
