﻿using System;

namespace Smt.Atomic.Data.Common.Attributes
{
    /// <summary>
    /// This attribute informs EF that specific property point to specific Entity
    /// Entity type is used by EF convention to generate DB column name:
    /// For example
    /// [EntityReference(typeof(User))]
    /// public long CreatedById {get; set; }
    /// Will create column in Db:  UserIdCreatedBy
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class EntityReferenceAttribute : AtomicEntityAttribute
    {
        /// <summary>
        /// Entity type
        /// </summary>
        public Type EntityType { get; private set; }

        public EntityReferenceAttribute(Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException(nameof(entityType));
            }

            EntityType = entityType;
        }
    }
}
