﻿using System;

namespace Smt.Atomic.Data.Common.Attributes
{
    /// <summary>
    /// Used to registered convention for adjusting sql string types to non -unicode  (NVARCHAR vs VARCHAR)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NonUnicodeAttribute : AtomicEntityAttribute
    {
    }
}
