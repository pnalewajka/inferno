﻿using System.Data.Entity.Spatial;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Data.Common.Helpers
{
    public static class SpatialUtils
    {
        /// <summary>
        /// Coordinate system id for WGS 84
        /// https://en.wikipedia.org/wiki/World_Geodetic_System
        /// </summary>
        private const int CoordinateSystemId = 4326;

        public static DbGeography CreateDbPoint(double latitude, double longitude)
        {
            //4326 format puts LONGITUDE first then LATITUDE
            var pointDeclaration =
                $"POINT({longitude.ToInvariantString()} {latitude.ToInvariantString()})";

            return DbGeography.PointFromText(pointDeclaration, CoordinateSystemId);
        }
    }
}
