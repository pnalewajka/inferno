﻿using System.Reflection;
using Smt.Atomic.Data.Common.Attributes;

namespace Smt.Atomic.Data.Common.Helpers
{
    public static class ObjectReferenceHelper
    {
        /// <summary>
        /// Create db column name for foreign keys by convention: (TargetTableName)Id(PropertyNameWithoutTrailingId)
        /// For example ModifiedById - foreign key to User table will be named as 'UserIdModifiedBy'
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="propertyInfo"></param>
        /// <returns>formatted column name</returns>
        public static string GetDbColumnNameByConvention(EntityReferenceAttribute attribute, PropertyInfo propertyInfo)
        {
            const string format = "{0}Id{1}";
            const string suffix = "Id";
            var propertyName = propertyInfo.Name;
            if (propertyName.EndsWith(suffix))
            {
                propertyName = propertyName.Substring(0, propertyName.Length - 2);
            }
            return string.Format(format,
                attribute.EntityType.Name,
                propertyName
                );
        }

        /// <summary>
        /// Create db column name for enums foreign keys by convention: (TargetTableName)Id(PropertyNameWithoutTrailingId)
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static string GetDbColumnNameByConvention(EnumReferenceAttribute attribute, PropertyInfo propertyInfo)
        {
            const string format = "{0}EnumId";
            var propertyType = propertyInfo.PropertyType.Name;
            return string.Format(format, propertyType);
        }
    }
}
