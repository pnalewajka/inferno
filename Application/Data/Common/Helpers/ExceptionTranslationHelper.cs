﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Common.Resources;

namespace Smt.Atomic.Data.Common.Helpers
{
    /// <summary>
    /// Helper responsible for translating one exception into another
    /// </summary>
    public static class ExceptionTranslationHelper
    {
        private static readonly List<Func<DbUpdateException, Exception>> DbUpdateExceptionTranslators = new List<Func<DbUpdateException, Exception>>
        {
            BusinessDerivedExceptionTranslator,
            DefaultDbUpdateExceptionTranslator,
        };

        /// <summary>
        /// Translates DbEntityValidationException into DatabaseOperationException
        /// DatabaseOperationException contains more user friendly description
        /// </summary>
        public static Exception Translate(DbEntityValidationException exception)
        {
            var message = new StringBuilder("Failed to commit transaction");

            foreach (var error in exception.EntityValidationErrors)
            {
                foreach (var validation in error.ValidationErrors)
                {
                    var detailLine =
                        $"  Entity: {error.Entry.GetType().Name}, property: {validation.PropertyName}, rule violated: {validation.ErrorMessage}";
                    message.AppendLine(detailLine);
                }
            }

            return new DatabaseOperationException(message.ToString(), exception);
        }

        /// <summary>
        /// Translates DbUpdateException into DatabaseOperationException
        /// DatabaseOperationException contains more user friendly description
        /// </summary>
        public static Exception Translate(DbUpdateException exception)
        {
            return DbUpdateExceptionTranslators.Select(t => t(exception)).FirstOrDefault(result => result != null);
        }

        private static Exception DefaultDbUpdateExceptionTranslator(DbUpdateException exception)
        {
            var message = new StringBuilder("Database update exception occurred");
            var sqlException = ExceptionHelper.GetFirstOfType<SqlException>(exception);

            if (sqlException != null)
            {
                message.Append(":");
                message.AppendLine();

                foreach (var error in sqlException.Errors)
                {
                    var errorLine = $"  {error}";
                    message.AppendLine(errorLine);
                }
            }

            return new DatabaseOperationException(message.ToString(), exception);
        }

        private static Exception BusinessDerivedExceptionTranslator(DbUpdateException exception)
        {
            // MessageId always returned by SqlServer where calling RAISERROR with string (no message id)
            const int messageId = 50000;

            var knownException = SqlExceptionHelper.GetFirstSqlExceptionOrDefault(exception, messageId);

            if (knownException == null)
            {
                return null;
            }

            return XmlSerializationHelper.DeserializeExceptionFromXml<BusinessException>(knownException.Message);
        }

        /// <summary>
        /// DbUpdateConcurrencyException should be treated as a known exception
        /// </summary>
        public static Exception TranslateConcurrencyException(DbUpdateConcurrencyException dbUpdateConcurrencyException)
        {
            return new DatabaseConcurrencyException(Exceptions.ConcurrencyExceptionMessage, dbUpdateConcurrencyException);
        }
    }
}