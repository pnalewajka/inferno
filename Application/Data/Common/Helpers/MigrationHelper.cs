﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Data.Common.Helpers
{
    /// <summary>
    /// Helper for processing sql from strings
    /// </summary>
    public class MigrationHelper
    {
        private static readonly Regex GoRegularExpression = new Regex(@"^\s*GO\s*$", RegexOptions.Multiline);

        /// <summary>
        /// Ado Command doesn't support context changing GO instruction. This method splits sql string into blocks looking for line containing only GO 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>ienumerable of code blocks</returns>
        public static IEnumerable<string> SplitStringByGo(string sql)
        {
            return GoRegularExpression.Split(sql).Where(s => !string.IsNullOrEmpty(s));
        }

        /// <summary>
        /// Execute sql from resources, splitting query by GO command. Executes using external action
        /// </summary>
        /// <param name="nearByType"></param>
        /// <param name="resourceName"></param>
        /// <param name="executeSqlAction"></param>
        public static void ExecuteFromResources(Type nearByType, string resourceName, Action<string> executeSqlAction)
        {
            var sql = ResourceHelper.GetResourceAsString(resourceName, nearByType);
            foreach (var sqlBlock in SplitStringByGo(sql))
            {
                executeSqlAction(sqlBlock);
            }
        }
    }
}
