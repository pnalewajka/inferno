﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Data.Common.Extensions
{
    /// <summary>
    /// Async extentesions, that are mapping directly to EntityFramework's queryable extentions.
    /// Code introduced to abstract away depenedancy on EntityFramework.dll in business/ frontend layers
    /// </summary>
    public static class AtomicQueryableExtentensions
    {
        /// <summary>
        /// While testing provider on repository mock will not support async, therefore we need to cope with this situation
        /// </summary>
        private static bool IsAsyncSupportedByProvider(this IQueryable source)
        {
            if (source == null)
            {
                return false;
            }

            return source.Provider is IDbAsyncQueryProvider;
        }

        /// <summary>
        /// Asynchronously enumerates the query such that for server queries such as those of <see cref="T:System.Data.Entity.DbSet`1" />,
        /// <see cref="T:System.Data.Entity.Core.Objects.ObjectSet`1" />
        /// ,
        /// <see cref="T:System.Data.Entity.Core.Objects.ObjectQuery`1" />, and others the results of the query will be loaded into the associated
        /// <see cref="T:System.Data.Entity.DbContext" />
        /// ,
        /// <see cref="T:System.Data.Entity.Core.Objects.ObjectContext" /> or other cache on the client.
        /// This is equivalent to calling ToList and then throwing away the list without the overhead of actually creating the list.
        /// </summary>
        /// <param name="source"> The source query. </param>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// </returns>
        public static Task LoadAsync(this IQueryable source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                source.Load();

                return TaskHelper.CompletedTask;
            }

            return QueryableExtensions.LoadAsync(source);
        }

        /// <summary>
        /// Asynchronously enumerates the query such that for server queries such as those of <see cref="T:System.Data.Entity.DbSet`1" />,
        /// <see cref="T:System.Data.Entity.Core.Objects.ObjectSet`1" />
        /// ,
        /// <see cref="T:System.Data.Entity.Core.Objects.ObjectQuery`1" />, and others the results of the query will be loaded into the associated
        /// <see cref="T:System.Data.Entity.DbContext" />
        /// ,
        /// <see cref="T:System.Data.Entity.Core.Objects.ObjectContext" /> or other cache on the client.
        /// This is equivalent to calling ToList and then throwing away the list without the overhead of actually creating the list.
        /// </summary>
        /// <param name="source"> The source query. </param>
        /// <param name="cancellationToken">
        /// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
        /// </param>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// </returns>
        public static Task LoadAsync(this IQueryable source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                source.Load();

                return TaskHelper.CompletedTask;
            }

            return QueryableExtensions.LoadAsync(source);
        }

		/// <summary>
		/// Asynchronously enumerates the query results and performs the specified action on each element.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable" /> to enumerate.
		/// </param>
		/// <param name="action"> The action to perform on each element. </param>
		/// <returns> A task that represents the asynchronous operation. </returns>
		public static Task ForEachAsync(this IQueryable source, Action<object> action)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                foreach (var item in source)
                {
                    action(item);
                }

                return TaskHelper.CompletedTask;
            }

            return QueryableExtensions.ForEachAsync(source, action);
        }

		/// <summary>
		/// Asynchronously enumerates the query results and performs the specified action on each element.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable" /> to enumerate.
		/// </param>
		/// <param name="action"> The action to perform on each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns> A task that represents the asynchronous operation. </returns>
        public static Task ForEachAsync(this IQueryable source, Action<object> action, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                foreach (var item in source)
                {
                    action(item);
                }

                return TaskHelper.CompletedTask;
            }

            return QueryableExtensions.ForEachAsync(source, action, cancellationToken);
        }

		/// <summary>
		/// Asynchronously enumerates the query results and performs the specified action on each element.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="T">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to enumerate.
		/// </param>
		/// <param name="action"> The action to perform on each element. </param>
		/// <returns> A task that represents the asynchronous operation. </returns>
        public static Task ForEachAsync<T>(this IQueryable<T> source, Action<T> action)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                foreach (var item in source)
                {
                    action(item);
                }

                return TaskHelper.CompletedTask;
            }

            return QueryableExtensions.ForEachAsync(source, action);
        }

		/// <summary>
		/// Asynchronously enumerates the query results and performs the specified action on each element.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="T">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to enumerate.
		/// </param>
		/// <param name="action"> The action to perform on each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns> A task that represents the asynchronous operation. </returns>
        public static Task ForEachAsync<T>(this IQueryable<T> source, Action<T> action, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                foreach (var item in source)
                {
                    action(item);
                }

                return TaskHelper.CompletedTask;
            }

            return QueryableExtensions.ForEachAsync(source, action, cancellationToken);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.List`1" /> from an <see cref="T:System.Linq.IQueryable" /> by enumerating it asynchronously.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable" /> to create a <see cref="T:System.Collections.Generic.List`1" /> from.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.List`1" /> that contains elements from the input sequence.
		/// </returns>
		public static Task<List<object>> ToListAsync(this IQueryable source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<object>(source).ToList());
            }

            return QueryableExtensions.ToListAsync(source);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.List`1" /> from an <see cref="T:System.Linq.IQueryable" /> by enumerating it asynchronously.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable" /> to create a <see cref="T:System.Collections.Generic.List`1" /> from.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.List`1" /> that contains elements from the input sequence.
		/// </returns>
        public static Task<List<object>> ToListAsync(this IQueryable source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<object>(source).ToList());
            }

            return QueryableExtensions.ToListAsync(source, cancellationToken);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.List`1" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.List`1" /> from.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.List`1" /> that contains elements from the input sequence.
		/// </returns>
        public static Task<List<TSource>> ToListAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToList());
            }

            return QueryableExtensions.ToListAsync(source);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.List`1" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a list from.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.List`1" /> that contains elements from the input sequence.
		/// </returns>
		public static Task<List<TSource>> ToListAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToList());
            }

            return QueryableExtensions.ToListAsync(source, cancellationToken);
        }

		/// <summary>
		/// Creates an array from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create an array from.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains an array that contains elements from the input sequence.
		/// </returns>
		public static Task<TSource[]> ToArrayAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToArray());
            }

            return QueryableExtensions.ToArrayAsync(source);
        }

		/// <summary>
		/// Creates an array from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create an array from.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains an array that contains elements from the input sequence.
		/// </returns>
		public static Task<TSource[]> ToArrayAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToArray());
            }

            return QueryableExtensions.ToArrayAsync(source, cancellationToken);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector function.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains selected keys and values.
		/// </returns>
        public static Task<Dictionary<TKey, TSource>> ToDictionaryAsync<TSource, TKey>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector function.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains selected keys and values.
		/// </returns>
        public static Task<Dictionary<TKey, TSource>> ToDictionaryAsync<TSource, TKey>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, cancellationToken);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector function and a comparer.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="comparer">
		/// An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains selected keys and values.
		/// </returns>
        public static Task<Dictionary<TKey, TSource>> ToDictionaryAsync<TSource, TKey>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector, comparer));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, comparer);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector function and a comparer.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="comparer">
		/// An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains selected keys and values.
		/// </returns>
		public static Task<Dictionary<TKey, TSource>> ToDictionaryAsync<TSource, TKey>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector, comparer));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, comparer, cancellationToken);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector and an element selector function.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <typeparam name="TElement">
		/// The type of the value returned by <paramref name="elementSelector" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="elementSelector"> A transform function to produce a result element value from each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains values of type
		/// <typeparamref name="TElement" /> selected from the input sequence.
		/// </returns>
		public static Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector, elementSelector));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, elementSelector);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector and an element selector function.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <typeparam name="TElement">
		/// The type of the value returned by <paramref name="elementSelector" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="elementSelector"> A transform function to produce a result element value from each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains values of type
		/// <typeparamref name="TElement" /> selected from the input sequence.
		/// </returns>
		public static Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector, elementSelector));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, elementSelector, cancellationToken);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector function, a comparer, and an element selector function.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <typeparam name="TElement">
		/// The type of the value returned by <paramref name="elementSelector" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="elementSelector"> A transform function to produce a result element value from each element. </param>
		/// <param name="comparer">
		/// An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains values of type
		/// <typeparamref name="TElement" /> selected from the input sequence.
		/// </returns>
		public static Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector, elementSelector, comparer));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, elementSelector, comparer);
        }

		/// <summary>
		/// Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Linq.IQueryable`1" /> by enumerating it asynchronously
		/// according to a specified key selector function, a comparer, and an element selector function.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TKey">
		/// The type of the key returned by <paramref name="keySelector" /> .
		/// </typeparam>
		/// <typeparam name="TElement">
		/// The type of the value returned by <paramref name="elementSelector" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.
		/// </param>
		/// <param name="keySelector"> A function to extract a key from each element. </param>
		/// <param name="elementSelector"> A transform function to produce a result element value from each element. </param>
		/// <param name="comparer">
		/// An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains a <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains values of type
		/// <typeparamref name="TElement" /> selected from the input sequence.
		/// </returns>
        public static Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(Enumerable.Cast<TSource>(source).ToDictionary(keySelector, elementSelector, comparer));
            }

            return QueryableExtensions.ToDictionaryAsync(source, keySelector, elementSelector, comparer, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the first element in <paramref name="source" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" /> is <c>null</c>.
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" /> doesn't implement <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />.
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">The source sequence is empty.</exception>
		public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.First());
            }

            return QueryableExtensions.FirstAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the first element in <paramref name="source" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">The source sequence is empty.</exception>
        public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.First());
            }

            return QueryableExtensions.FirstAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence that satisfies a specified condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the first element in <paramref name="source" /> that passes the test in
		/// <paramref name="predicate" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// No element satisfies the condition in
		/// <paramref name="predicate" />
		/// .
		/// </exception>
		public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.First(predicate));
            }

            return QueryableExtensions.FirstAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence that satisfies a specified condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the first element in <paramref name="source" /> that passes the test in
		/// <paramref name="predicate" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// No element satisfies the condition in
		/// <paramref name="predicate" />
		/// .
		/// </exception>
		public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.First(predicate));
            }

            return QueryableExtensions.FirstAsync(source, predicate, cancellationToken);
        }


		/// <summary>
		/// Asynchronously returns the first element of a sequence, or a default value if the sequence contains no elements.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>default</c> ( <typeparamref name="TSource" /> ) if
		/// <paramref name="source" /> is empty; otherwise, the first element in <paramref name="source" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> FirstOrDefaultAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.FirstOrDefault());
            }

            return QueryableExtensions.FirstOrDefaultAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence, or a default value if the sequence contains no elements.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>default</c> ( <typeparamref name="TSource" /> ) if
		/// <paramref name="source" /> is empty; otherwise, the first element in <paramref name="source" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> FirstOrDefaultAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.FirstOrDefault());
            }

            return QueryableExtensions.FirstOrDefaultAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence that satisfies a specified condition
		/// or a default value if no such element is found.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>default</c> ( <typeparamref name="TSource" /> ) if <paramref name="source" />
		/// is empty or if no element passes the test specified by <paramref name="predicate" /> ; otherwise, the first
		/// element in <paramref name="source" /> that passes the test specified by <paramref name="predicate" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
        /// </exception>
        public static Task<TSource> FirstOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.FirstOrDefault(predicate));
            }

            return QueryableExtensions.FirstOrDefaultAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously returns the first element of a sequence that satisfies a specified condition
		/// or a default value if no such element is found.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the first element of.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>default</c> ( <typeparamref name="TSource" /> ) if <paramref name="source" />
		/// is empty or if no element passes the test specified by <paramref name="predicate" /> ; otherwise, the first
		/// element in <paramref name="source" /> that passes the test specified by <paramref name="predicate" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// has more than one element.
		/// </exception>
		public static Task<TSource> FirstOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.FirstOrDefault(predicate));
            }

            return QueryableExtensions.FirstOrDefaultAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence, and throws an exception
		/// if there is not exactly one element in the sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">The source sequence is empty.</exception>
        public static Task<TSource> SingleAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Single());
            }

            return QueryableExtensions.SingleAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence, and throws an exception
		/// if there is not exactly one element in the sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// has more than one element.
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">The source sequence is empty.</exception>
		public static Task<TSource> SingleAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Single());
            }

            return QueryableExtensions.SingleAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence that satisfies a specified condition,
		/// and throws an exception if more than one such element exists.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the the single element of.
		/// </param>
		/// <param name="predicate"> A function to test an element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence that satisfies the condition in
		/// <paramref name="predicate" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// No element satisfies the condition in
		/// <paramref name="predicate" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// More than one element satisfies the condition in
		/// <paramref name="predicate" />
		/// .
		/// </exception>
		public static Task<TSource> SingleAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Single(predicate));
            }

            return QueryableExtensions.SingleAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence that satisfies a specified condition,
		/// and throws an exception if more than one such element exists.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="predicate"> A function to test an element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence that satisfies the condition in
		/// <paramref name="predicate" />.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// No element satisfies the condition in
		/// <paramref name="predicate" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// More than one element satisfies the condition in
		/// <paramref name="predicate" />
		/// .
		/// </exception>
		public static Task<TSource> SingleAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Single(predicate));
            }

            return QueryableExtensions.SingleAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence, or a default value if the sequence is empty;
		/// this method throws an exception if there is more than one element in the sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence, or <c>default</c> (<typeparamref name="TSource" />)
		/// if the sequence contains no elements.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// has more than one element.
		/// </exception>
		public static Task<TSource> SingleOrDefaultAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.SingleOrDefault());
            }

            return QueryableExtensions.SingleOrDefaultAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence, or a default value if the sequence is empty;
		/// this method throws an exception if there is more than one element in the sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence, or <c>default</c> (<typeparamref name="TSource" />)
		/// if the sequence contains no elements.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// has more than one element.
		/// </exception>
		public static Task<TSource> SingleOrDefaultAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.SingleOrDefault());
            }

            return QueryableExtensions.SingleOrDefaultAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence that satisfies a specified condition or
		/// a default value if no such element exists; this method throws an exception if more than one element
		/// satisfies the condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="predicate"> A function to test an element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence that satisfies the condition in
		/// <paramref name="predicate" />, or <c>default</c> ( <typeparamref name="TSource" /> ) if no such element is found.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> SingleOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.SingleOrDefault(predicate));
            }

            return QueryableExtensions.SingleOrDefaultAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously returns the only element of a sequence that satisfies a specified condition or
		/// a default value if no such element exists; this method throws an exception if more than one element
		/// satisfies the condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="predicate"> A function to test an element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the single element of the input sequence that satisfies the condition in
		/// <paramref name="predicate" />, or <c>default</c> ( <typeparamref name="TSource" /> ) if no such element is found.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> SingleOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.SingleOrDefault(predicate));
            }

            return QueryableExtensions.SingleOrDefaultAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously determines whether a sequence contains a specified element by using the default equality comparer.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="item"> The object to locate in the sequence. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if the input sequence contains the specified value; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> ContainsAsync<TSource>(this IQueryable<TSource> source, TSource item)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Contains(item));
            }

            return QueryableExtensions.ContainsAsync(source, item);
        }

		/// <summary>
		/// Asynchronously determines whether a sequence contains a specified element by using the default equality comparer.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to return the single element of.
		/// </param>
		/// <param name="item"> The object to locate in the sequence. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if the input sequence contains the specified value; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> ContainsAsync<TSource>(this IQueryable<TSource> source, TSource item, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Contains(item));
            }

            return QueryableExtensions.ContainsAsync(source, item, cancellationToken);
        }

		/// <summary>
		/// Asynchronously determines whether a sequence contains any elements.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to check for being empty.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if the source sequence contains any elements; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> AnyAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Any());
            }

            return QueryableExtensions.AnyAsync(source);
        }

		/// <summary>
		/// Asynchronously determines whether a sequence contains any elements.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> to check for being empty.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if the source sequence contains any elements; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> AnyAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Any());
            }

            return QueryableExtensions.AnyAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously determines whether any element of a sequence satisfies a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> whose elements to test for a condition.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if any elements in the source sequence pass the test in the specified predicate; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> AnyAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
		    {
                return Task.FromResult(source.Any(predicate));
            }

		    return QueryableExtensions.AnyAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously determines whether any element of a sequence satisfies a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> whose elements to test for a condition.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if any elements in the source sequence pass the test in the specified predicate; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> AnyAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Any(predicate));
            }

            return QueryableExtensions.AnyAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously determines whether all the elements of a sequence satisfy a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> whose elements to test for a condition.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if every element of the source sequence passes the test in the specified predicate; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> AllAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.All(predicate));
            }

            return QueryableExtensions.AllAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously determines whether all the elements of a sequence satisfy a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> whose elements to test for a condition.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains <c>true</c> if every element of the source sequence passes the test in the specified predicate; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<bool> AllAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.All(predicate));
            }

            return QueryableExtensions.AllAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the number of elements in a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the input sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> CountAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Count());
            }

            return QueryableExtensions.CountAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the number of elements in a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the input sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> CountAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Count());
            }

            return QueryableExtensions.CountAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the number of elements in a sequence that satisfy a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the sequence that satisfy the condition in the predicate function.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// that satisfy the condition in the predicate function
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> CountAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Count(predicate));
            }

            return QueryableExtensions.CountAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously returns the number of elements in a sequence that satisfy a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the sequence that satisfy the condition in the predicate function.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// that satisfy the condition in the predicate function
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> CountAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Count(predicate));
            }

            return QueryableExtensions.CountAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns an <see cref="T:System.Int64" /> that represents the total number of elements in a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the input sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long> LongCountAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.LongCount());
            }

            return QueryableExtensions.LongCountAsync(source);
        }

		/// <summary>
		/// Asynchronously returns an <see cref="T:System.Int64" /> that represents the total number of elements in a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the input sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long> LongCountAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.LongCount());
            }

            return QueryableExtensions.LongCountAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns an <see cref="T:System.Int64" /> that represents the number of elements in a sequence
		/// that satisfy a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the sequence that satisfy the condition in the predicate function.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// that satisfy the condition in the predicate function
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long> LongCountAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.LongCount(predicate));
            }

            return QueryableExtensions.LongCountAsync(source, predicate);
        }

		/// <summary>
		/// Asynchronously returns an <see cref="T:System.Int64" /> that represents the number of elements in a sequence
		/// that satisfy a condition.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to be counted.
		/// </param>
		/// <param name="predicate"> A function to test each element for a condition. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the number of elements in the sequence that satisfy the condition in the predicate function.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="predicate" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// that satisfy the condition in the predicate function
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
        public static Task<long> LongCountAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.LongCount(predicate));
            }

            return QueryableExtensions.LongCountAsync(source, predicate, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the minimum value of a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the minimum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the minimum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<TSource> MinAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Min());
            }

            return QueryableExtensions.MinAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the minimum value of a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the minimum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the minimum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> MinAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Min());
            }

            return QueryableExtensions.MinAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously invokes a projection function on each element of a sequence and returns the minimum resulting value.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TResult">
		/// The type of the value returned by the function represented by <paramref name="selector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the minimum of.
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the minimum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TResult> MinAsync<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Min(selector));
            }

            return QueryableExtensions.MinAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously invokes a projection function on each element of a sequence and returns the minimum resulting value.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TResult">
		/// The type of the value returned by the function represented by <paramref name="selector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the minimum of.
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the minimum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TResult> MinAsync<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Min(selector));
            }

            return QueryableExtensions.MinAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously returns the maximum value of a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the maximum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the maximum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> MaxAsync<TSource>(this IQueryable<TSource> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Max());
            }

            return QueryableExtensions.MaxAsync(source);
        }

		/// <summary>
		/// Asynchronously returns the maximum value of a sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the maximum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the maximum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TSource> MaxAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Max());
            }

            return QueryableExtensions.MaxAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously invokes a projection function on each element of a sequence and returns the maximum resulting value.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TResult">
		/// The type of the value returned by the function represented by <paramref name="selector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the maximum of.
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the maximum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TResult> MaxAsync<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Max(selector));
            }

            return QueryableExtensions.MaxAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously invokes a projection function on each element of a sequence and returns the maximum resulting value.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" />.
		/// </typeparam>
		/// <typeparam name="TResult">
		/// The type of the value returned by the function represented by <paramref name="selector" /> .
		/// </typeparam>
		/// <param name="source">
		/// An <see cref="T:System.Linq.IQueryable`1" /> that contains the elements to determine the maximum of.
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the maximum value in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<TResult> MaxAsync<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Max(selector));
            }

            return QueryableExtensions.MaxAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int32" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains  the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> SumAsync(this IQueryable<int> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int32" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> SumAsync(this IQueryable<int> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int32" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int?> SumAsync(this IQueryable<int?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int32" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int?> SumAsync(this IQueryable<int?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int64" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long> SumAsync(this IQueryable<long> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int64" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long> SumAsync(this IQueryable<long> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int64" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long?> SumAsync(this IQueryable<long?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int64" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
        public static Task<long?> SumAsync(this IQueryable<long?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Single" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<float> SumAsync(this IQueryable<float> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Single" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float> SumAsync(this IQueryable<float> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Single" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float?> SumAsync(this IQueryable<float?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Single" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float?> SumAsync(this IQueryable<float?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Double" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double> SumAsync(this IQueryable<double> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Double" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double> SumAsync(this IQueryable<double> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Double" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> SumAsync(this IQueryable<double?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Double" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> SumAsync(this IQueryable<double?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Decimal" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<decimal> SumAsync(this IQueryable<decimal> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Decimal" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<decimal> SumAsync(this IQueryable<decimal> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Decimal" /> values to calculate the sum of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<decimal?> SumAsync(this IQueryable<decimal?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the sum of a sequence of nullable <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Decimal" /> values to calculate the sum of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the values in the sequence.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Decimal.MaxValue" />
		/// .
		/// </exception>
		public static Task<decimal?> SumAsync(this IQueryable<decimal?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum());
            }

            return QueryableExtensions.SumAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
        public static Task<int?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int32.MaxValue" />
		/// .
		/// </exception>
		public static Task<int?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
        public static Task<long> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
		public static Task<long?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Int64.MaxValue" />
		/// .
		/// </exception>
        public static Task<long?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Decimal.MaxValue" />
		/// .
		/// </exception>
		public static Task<decimal> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Decimal.MaxValue" />
		/// .
		/// </exception>
        public static Task<decimal> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Decimal.MaxValue" />
		/// .
		/// </exception>
		public static Task<decimal?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the sum of the sequence of nullable <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source">
		/// A sequence of values of type <typeparamref name="TSource" /> .
		/// </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the sum of the projected values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.OverflowException">
		/// The number of elements in
		/// <paramref name="source" />
		/// is larger than
		/// <see cref="F:System.Decimal.MaxValue" />
		/// .
		/// </exception>
		public static Task<decimal?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Sum(selector));
            }

            return QueryableExtensions.SumAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int32" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<double> AverageAsync(this IQueryable<int> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int32" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync(this IQueryable<int> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int32" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync(this IQueryable<int?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int32" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int32" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync(this IQueryable<int?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int64" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<double> AverageAsync(this IQueryable<long> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Int64" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<double> AverageAsync(this IQueryable<long> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int64" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync(this IQueryable<long?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int64" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Int64" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync(this IQueryable<long?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Single" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<float> AverageAsync(this IQueryable<float> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Single" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<float> AverageAsync(this IQueryable<float> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Single" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<float?> AverageAsync(this IQueryable<float?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Single" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Single" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<float?> AverageAsync(this IQueryable<float?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Double" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync(this IQueryable<double> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Double" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync(this IQueryable<double> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Double" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> AverageAsync(this IQueryable<double?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Double" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Double" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> AverageAsync(this IQueryable<double?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Decimal" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<decimal> AverageAsync(this IQueryable<decimal> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of <see cref="T:System.Decimal" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<decimal> AverageAsync(this IQueryable<decimal> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Decimal" /> values to calculate the average of.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<decimal?> AverageAsync(this IQueryable<decimal?> source)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Decimal" /> values.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <param name="source">
		/// A sequence of nullable <see cref="T:System.Decimal" /> values to calculate the average of.
		/// </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<decimal?> AverageAsync(this IQueryable<decimal?> source, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average());
            }

            return QueryableExtensions.AverageAsync(source, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int32" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Int64" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<float> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<float> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Single" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<float?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Double" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
        public static Task<decimal> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// contains no elements.
		/// </exception>
		public static Task<decimal> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
		public static Task<decimal?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector);
        }

		/// <summary>
		/// Asynchronously computes the average of a sequence of nullable <see cref="T:System.Decimal" /> values that is obtained
		/// by invoking a projection function on each element of the input sequence.
		/// </summary>
		/// <remarks>
		/// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
		/// that any asynchronous operations have completed before calling another method on this context.
		/// </remarks>
		/// <typeparam name="TSource">
		/// The type of the elements of <paramref name="source" /> .
		/// </typeparam>
		/// <param name="source"> A sequence of values to calculate the average of. </param>
		/// <param name="selector"> A projection function to apply to each element. </param>
		/// <param name="cancellationToken">
		/// A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.
		/// </param>
		/// <returns>
		/// A task that represents the asynchronous operation.
		/// The task result contains the average of the sequence of values.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="source" />
		/// or
		/// <paramref name="selector" />
		/// is
		/// <c>null</c>
		/// .
		/// </exception>
		/// <exception cref="T:System.InvalidOperationException">
		/// <paramref name="source" />
		/// doesn't implement
		/// <see cref="T:System.Data.Entity.Infrastructure.IDbAsyncQueryProvider" />
		/// .
		/// </exception>
        public static Task<decimal?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector, CancellationToken cancellationToken)
        {
            if (!source.IsAsyncSupportedByProvider())
            {
                return Task.FromResult(source.Average(selector));
            }

            return QueryableExtensions.AverageAsync(source, selector, cancellationToken);
        }
    }
}
