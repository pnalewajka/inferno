﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.ImportHours.Interfaces;
using Smt.Atomic.Data.ImportHours.Services;

namespace Smt.Atomic.Data.ImportHours
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<IImportHoursDataService>().ImplementedBy<ImportHoursDataService>().LifestyleTransient());
        }
    }
}