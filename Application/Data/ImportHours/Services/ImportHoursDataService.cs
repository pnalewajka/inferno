﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.ImportHours.Interfaces;
using Smt.Atomic.Data.ImportHours.Resources;

namespace Smt.Atomic.Data.ImportHours.Services
{
    class ImportHoursDataService : IImportHoursDataService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISystemParameterService _systemParameterService;

        public ImportHoursDataService(
            ISettingsProvider settingsProvider,
            ISystemParameterService systemParameterService)
        {
            _settingsProvider = settingsProvider;
            _systemParameterService = systemParameterService;
        }

        private ImportHoursService.ImportHours _importHoursClient;

        private ImportHoursService.ImportHours ImportHoursClient
        {
            get
            {
                if (_importHoursClient == null)
                {
                    var timeReportImportHoursUrl = _systemParameterService.GetParameter<string>(ParameterKeys.TimeReportImportHoursUrl);
                    var importHoursSettings = _settingsProvider.AuthorizationProviderSettings.ImportHoursSettings;

                    _importHoursClient = new ImportHoursService.ImportHours
                    {
                        Credentials = new NetworkCredential(importHoursSettings.User, importHoursSettings.Password),
                        Url = timeReportImportHoursUrl
                    };
                }
                return _importHoursClient;
            }
        }    

        public BoolResult AddRegistration(TimeReport timeReport)
        {
            foreach (var timeReportRow in timeReport.Rows)
            {
                foreach (var dailyEnty in timeReportRow.DailyEntries)
                {
                    var errorMessage = SendDailyEntryRegistration(dailyEnty);

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        return ErrorResult(errorMessage);
                    }
                }
            }

            return SuccessResult();
        }

        private string SendDailyEntryRegistration(TimeReportDailyEntry dailyEnty)
        {
            var timeReportRow = dailyEnty.TimeReportRow;
            var timeReport = timeReportRow.TimeReport;

            if (string.IsNullOrEmpty(timeReportRow.Project.Apn) && timeReportRow.Project.TimeTrackingType != TimeTrackingProjectType.Absence)
            {
                return ImportHoursResources.ApnIsEmpty;
            }

            return ImportHoursClient.AddRegistration(
                        timeReport.Employee.Acronym,
                        timeReport.Employee.Company.NavisionCode,
                        dailyEnty.Day,
                        timeReportRow.Project.Apn ?? string.Empty,
                        ProjectBusinessLogic.ProjectName.Call(timeReportRow.Project),
                        StringHelper.TruncateAtWords(timeReportRow.TaskName, 199),
                        timeReportRow.OvertimeVariant.ToString(), 
                        dailyEnty.Hours,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty);
        }

        public BoolResult DeleteRegistration(TimeReport timeReport)
        {
            var errorMessage =
                ImportHoursClient.DeleteHistoricalData(
                    timeReport.Employee.Acronym,
                    DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month),
                    DateHelper.EndOfMonth(timeReport.Year, timeReport.Month));

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return ErrorResult(errorMessage);
            }

            return SuccessResult();
        }

        private BoolResult SuccessResult()
        {
            return new BoolResult { IsSuccessful = true };
        }

        private BoolResult ErrorResult(string errorMessage)
        {
            var errorResult = new BoolResult { IsSuccessful = false };
            errorResult.AddAlert(AlertType.Error, errorMessage);

            return errorResult;
        }
    }
}