﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Data.ImportHours.Interfaces
{
    public interface IImportHoursDataService
    {
        BoolResult AddRegistration(TimeReport timeReport);

        BoolResult DeleteRegistration(TimeReport timeReport);
    }
}