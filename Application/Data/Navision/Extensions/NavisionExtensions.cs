﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Linq.Expressions;

namespace Smt.Atomic.Data.Navision.Extensions
{
    public static class NavisionExtensions
    {
        private const int PageSize = 10000;

        public static IEnumerable<TElement> GetAll<TElement>(this DataServiceQuery<TElement> dataServiceQuery, Expression<Func<TElement, bool>> predicate = null)
        {
            var allElements = new List<TElement>();
            var skippedElements = 0;
            IList<TElement> page;

            do
            {
                page = dataServiceQuery.Skip(skippedElements, predicate);
                allElements.AddRange(page);
                skippedElements += PageSize;
            }
            while (page.Count == PageSize);

            return allElements;
        }

        private static IList<TElement> Skip<TElement>(this DataServiceQuery<TElement> dataServiceQuery, int pageSize, Expression<Func<TElement, bool>> predicate)
        {
            return predicate == null
                ? dataServiceQuery.AddQueryOption("$skip", pageSize).ToList()
                : dataServiceQuery.AddQueryOption("$skip", pageSize).Where(predicate).ToList();
        }
    }
}