﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Navision.Entities;
using Smt.Atomic.Data.Navision.Extensions;
using Smt.Atomic.Data.Navision.Interfaces;

namespace Smt.Atomic.Data.Navision.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class DimensionMappingsCacheService : IDimensionMappingsCacheService
    {
        private readonly INavisionUnitOfWorkService _navisionUnitOfWorkService;

        public DimensionMappingsCacheService(INavisionUnitOfWorkService navisionUnitOfWorkService)
        {
            _navisionUnitOfWorkService = navisionUnitOfWorkService;
        }

        [Cached(ExpirationInSeconds = 3600)]
        public IDictionary<string, IDictionary<string, DimensionValue>> GetDimensionNoToDimensionValueMappings()
        {
            var dimensionValues = _navisionUnitOfWorkService
                .Create()
                .DimensionValues
                .GetAll();

            var dimensionNoToDimensionValueMappings = new Dictionary<string, IDictionary<string, DimensionValue>>();
            var dimensionValueLookup = dimensionValues.ToLookup(k => k.Dimension_Code);

            foreach (var singleDimensionValues in dimensionValueLookup)
            {
                dimensionNoToDimensionValueMappings[singleDimensionValues.Key] = singleDimensionValues.ToDictionary(s => s.Code, s => new DimensionValue(s));
            }

            return dimensionNoToDimensionValueMappings;
        }
    }
}