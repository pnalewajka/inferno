﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Data.Navision.Entities;
using Smt.Atomic.Data.Navision.NavisionService;

namespace Smt.Atomic.Data.Navision.Services
{
    public static class NavisionDataContextExtensions
    {
        public static Project GetProjectByApn(this NavisionUnitOfWork unitOfWork, string apn)
        {
            var project = unitOfWork.Projects.AddQueryOption("$filter", "No eq '" + apn + "'").FirstOrDefault();

            return project;
        }

        public static Dimension_Value GetUtilizationCategoryByCode(this NavisionUnitOfWork unitOfWork, string code)
        {
            var project = unitOfWork.DimensionValues.AddQueryOption("$filter", $"Code eq '{code}' && Dimension_Code eq '{DimensionCodes.ProjectUtilization}'").FirstOrDefault();

            return project;
        }
    }
}