﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Services.Client;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text.RegularExpressions;
using Dapper;
using SimpleImpersonation;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Data.Navision.Entities;
using Smt.Atomic.Data.Navision.NavisionService;

namespace Smt.Atomic.Data.Navision.Services
{
    public class NavisionUnitOfWork
    {
        private readonly NAV _nav;
        private readonly NavisionSettings _navisionSettings;
        private readonly ISystemParameterService _systemParameterService;

        public DataServiceQuery<Dimension_Value> DimensionValues => _nav.Dimension_Value;

        public DataServiceQuery<Project> Projects => _nav.Project;

        public NavisionUnitOfWork(
            ISettingsProvider settingsProvider,
            ISystemParameterService systemParameterService)
        {
            _systemParameterService = systemParameterService;
            _navisionSettings = settingsProvider.AuthorizationProviderSettings.NavisionSettings;

            var navisionUrl = systemParameterService.GetParameter<string>(ParameterKeys.NavisionUrl);

            _nav = new NAV(new Uri(navisionUrl))
            {
                Credentials = new NetworkCredential(_navisionSettings.User, _navisionSettings.Password)
            };
        }

        public void AddProject(Project project)
        {
            _nav.AddToProject(project);
        }

        public void UpdateProject(Project project)
        {        
            _nav.UpdateObject(project);
        }

        public void Commit()
        {
            var result = _nav.SaveChanges();

            if (result == null)
            {
                throw new InvalidDataException();
            }

            foreach (var operationResponse in result)
            {
                if (operationResponse.Error != null)
                {
                    throw new InvalidOperationException("Save changes failed, see inner exception for details.", operationResponse.Error);
                }
            }
        }

        public Transaction GetTransaction(long id, IDictionary<string, IDictionary<string, DimensionValue>> dimensionNoToDimensionValueMappings)
        {
            var sql = $@"{SqlSelectPart} FROM {GetViewName()} WHERE [Entry No.] = @EntryNo";

            using (Impersonation.LogonUser(Environment.UserDomainName, _navisionSettings.User, _navisionSettings.Password, LogonType.NewCredentials))
            using (var connection = GetConnection())
            {
                return connection
                    .Query(sql, new { EntryNo = id })
                    .Select(r => new Transaction(r, dimensionNoToDimensionValueMappings))
                    .Single();
            }
        }

        public IList<Transaction> GetTransactions(DateTime from, DateTime to, IDictionary<string, IDictionary<string, DimensionValue>> dimensionNoToDimensionValueMappings)
        {
            var sql = $@"{SqlSelectPart} FROM {GetViewName()} WHERE [Posting Date] >= @From AND [Posting Date] <= @To";

            using (Impersonation.LogonUser(Environment.UserDomainName, _navisionSettings.User, _navisionSettings.Password, LogonType.NewCredentials))
            using (var connection = GetConnection())
            {
                return connection
                    .Query(sql, new { From = from, To = to })
                    .Select(r => new Transaction(r, dimensionNoToDimensionValueMappings))
                    .ToList();
            }
        }

        private IDbConnection GetConnection()
        {
            var hostName = _systemParameterService.GetParameter<string>(ParameterKeys.NavisionTransactionsHost);

            return new SqlConnection($"Data Source={hostName};Integrated Security=true");
        }

        private string GetViewName()
        {
            var viewName = _systemParameterService.GetParameter<string>(ParameterKeys.NavisionTransactionsView);

            if (!Regex.IsMatch(viewName, @"^\[[\p{L}{\p{Nd}}$#_]*\](\.\[[\p{L}{\p{Nd}}$#_]*\])*$", RegexOptions.Compiled))
            {
                throw new SecurityException("Incorrect view name: possible SQL injection attempt");
            }

            return viewName;
        }

        private const string SqlSelectPart = @"SELECT [Entry No.]                  EntryNo
                                                     ,[G/L Account No.]            GeneralLedgerAccountNo
                                                     ,[Source Type]                SourceType
                                                     ,[Source No.]                 SourceNo
                                                     ,[Source Name]                SourceName
                                                     ,[Posting Date]               PostingDate
                                                     ,[Document Date]              DocumentDate
                                                     ,[Document Type]              DocumentType
                                                     ,[Document No.]               DocumentNo
                                                     ,[External Document No.]      ExternalDocumentNo
                                                     ,[Description]                Description
                                                     ,[Amount]                     Amount
                                                     ,[Average-Currency Amount]    AverageCurrencyAmount
                                                     ,[User ID]                    UserId
                                                     ,[Source Code]                SourceCode
                                                     ,[Source Company Name]        SourceCompanyName
                                                     ,[.BUDGET POS.]               BudgetPosition
                                                     ,[.ENTITY]                    Entity
                                                     ,[.IFRS]                      Ifrs
                                                     ,[.LOCATION]                  Location
                                                     ,[.N.TAX.]                    NoTax
                                                     ,[.ORG.UNIT]                  OrgUnit
                                                     ,[.PROJECT]                   Project
                                                     ,[1. ACCOUNT]                 Account
                                                     ,[1. COST CENTRE]             CostCenter
                                                     ,[1. PM]                      ProjectManager
                                                     ,[1. PROJ.GROUP]              ProjectGroup
                                                     ,[1. PROJ.TYPE]               ProjectType
                                                     ,[1. PROJ.UTIL]               ProjectUtilization
                                                     ,[2. ACC.TYPE]                AccountType
                                                     ,[2. OM]                      OperationalManager
                                                     ,[2. REGION]                  Region
                                                     ,[2. SP]                      SalesPerson
                                                     ,[2. VERTICAL]                Vertical
                                                     ,[3. GM]                      GeneralManager
                                                     ,[3. OD]                      OperationalDirector";
    }
}