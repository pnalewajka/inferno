﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Navision.Interfaces;

namespace Smt.Atomic.Data.Navision.Services
{
    public class NavisionUnitOfWorkService : INavisionUnitOfWorkService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISystemParameterService _systemParameterService;

        public NavisionUnitOfWorkService(
            ISettingsProvider settingsProvider,
            ISystemParameterService systemParameterService)
        {
            _settingsProvider = settingsProvider;
            _systemParameterService = systemParameterService;
        }

        public NavisionUnitOfWork Create()
        {
            return new NavisionUnitOfWork(_settingsProvider, _systemParameterService);
        }
    }
}
