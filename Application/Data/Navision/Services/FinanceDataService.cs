﻿using System;
using System.Collections.Generic;
using Castle.Core;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Navision.Entities;
using Smt.Atomic.Data.Navision.Interfaces;
using Smt.Atomic.Data.Navision.Resources;

namespace Smt.Atomic.Data.Navision.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class FinanceDataService : IFinanceDataService
    {
        private readonly INavisionUnitOfWorkService _navisionUnitOfWorkService;
        private readonly IDimensionMappingsCacheService _dimensionMappingsCacheService;

        public FinanceDataService(
            INavisionUnitOfWorkService navisionUnitOfWorkService,
            IDimensionMappingsCacheService dimensionMappingsCacheService)
        {
            _navisionUnitOfWorkService = navisionUnitOfWorkService;
            _dimensionMappingsCacheService = dimensionMappingsCacheService;
        }

        [Cached(ExpirationInSeconds = 3600)]
        public IList<Transaction> GetTransactions(DateTime from, DateTime to)
        {
            var dimensionNoToDimensionValueMappings = _dimensionMappingsCacheService.GetDimensionNoToDimensionValueMappings();

            return _navisionUnitOfWorkService.Create().GetTransactions(from, to, dimensionNoToDimensionValueMappings);
        }

        [Cached(ExpirationInSeconds = 3600)]
        public Transaction GetTransactionById(long id)
        {
            var transaction = GetTransactionByIdOrDefault(id);

            if (transaction == null)
            {
                throw new ArgumentException(string.Format(Exceptions.CouldntFindTransactionWithId, id));
            }

            return transaction;
        }

        [Cached(ExpirationInSeconds = 3600)]
        public Transaction GetTransactionByIdOrDefault(long id)
        {
            var dimensionNoToDimensionValueMappings = _dimensionMappingsCacheService.GetDimensionNoToDimensionValueMappings();

            return _navisionUnitOfWorkService.Create().GetTransaction(id, dimensionNoToDimensionValueMappings);
        }
    }
}