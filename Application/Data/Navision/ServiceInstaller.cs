﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Navision.Interfaces;
using Smt.Atomic.Data.Navision.Services;

namespace Smt.Atomic.Data.Navision
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<INavisionUnitOfWorkService>().ImplementedBy<Services.NavisionUnitOfWorkService>().LifestyleTransient());
            container.Register(Component.For<IFinanceDataService>().ImplementedBy<FinanceDataService>().LifestyleTransient());
            container.Register(Component.For<IDimensionMappingsCacheService>().ImplementedBy<DimensionMappingsCacheService>().LifestyleTransient());
        }
    }
}