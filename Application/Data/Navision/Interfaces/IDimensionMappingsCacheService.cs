using System.Collections.Generic;
using Smt.Atomic.Data.Navision.Entities;

namespace Smt.Atomic.Data.Navision.Interfaces
{
    public interface IDimensionMappingsCacheService
    {
        IDictionary<string, IDictionary<string, DimensionValue>> GetDimensionNoToDimensionValueMappings();
    }
}