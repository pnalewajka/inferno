﻿using Smt.Atomic.Data.Navision.Services;

namespace Smt.Atomic.Data.Navision.Interfaces
{
    public interface INavisionUnitOfWorkService
    {
        NavisionUnitOfWork Create();
    }
}