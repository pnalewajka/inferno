using System;
using System.Collections.Generic;
using Smt.Atomic.Data.Navision.Entities;

namespace Smt.Atomic.Data.Navision.Interfaces
{
    public interface IFinanceDataService
    {
        IList<Transaction> GetTransactions(DateTime from, DateTime to);

        Transaction GetTransactionById(long id);

        Transaction GetTransactionByIdOrDefault(long id);
    }
}