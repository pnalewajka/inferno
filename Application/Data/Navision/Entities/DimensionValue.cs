﻿using System.Diagnostics;
using Smt.Atomic.Data.Navision.NavisionService;

namespace Smt.Atomic.Data.Navision.Entities
{
    [DebuggerDisplay("{" + nameof(Name) + "}")]
    public class DimensionValue
    {
        public string DimensionCode { get; private set; }
        public string Code { get; private set; }
        public string Name { get; private set; }
        public string DimensionValueType { get; set; }
        public int? GlobalDimensionNo { get; set; }

        public DimensionValue(Dimension_Value entity)
        {
            DimensionCode = entity.Dimension_Code;
            Code = entity.Code;
            Name = entity.Name;
            DimensionValueType = entity.Dimension_Value_Type;
            GlobalDimensionNo = entity.Global_Dimension_No;
        }
    }
}