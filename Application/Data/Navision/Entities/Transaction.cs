using System;
using System.Collections.Generic;
using System.Diagnostics;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Navision.Entities
{
    [DebuggerDisplay("{" + nameof(Description) + "}")]
    public class Transaction : IEntity
    {
        public long Id { get; set; }

        public string Description { get; set; }

        public decimal? AmountPln { get; set; }

        public decimal? AmountEur { get; set; }

        public DateTime? PostingDate { get; set; }        

        public DateTime? DocumentDate { get; set; }

        public string DocumentNo { get; set; }

        public string DocumentType { get; set; }

        public int EntryNo { get; set; }

        public string ExternalDocumentNo { get; set; }

        public string GeneralLedgerAccountNo { get; set; }

        public string SourceNo { get; set; }

        public string SourceName { get; set; }

        public string SourceCode { get; set; }

        public string SourceType { get; set; }

        public string SourceCompanyName { get; set; }

        public string UserId { get; set; }

        /// <summary>
        /// Connected with chart of accounts
        /// </summary>
        public DimensionValue BudgetPosition { get; set; }

        /// <summary>
        /// For consolidation
        /// </summary>
        public DimensionValue Entity { get; set; }

        /// <summary>
        /// For reporting consistent with IFRS or local rules
        /// </summary>
        public DimensionValue Ifrs { get; set; }

        /// <summary>
        /// Required for renting
        /// </summary>
        public DimensionValue Location { get; set; }

        /// <summary>
        /// For records excluded from tax reporting
        /// </summary>
        public DimensionValue NoTax { get; set; }

        /// <summary>
        /// Mapping organizational structures
        /// </summary>
        public DimensionValue OrgUnit { get; set; }

        /// <summary>
        /// Created automatically with Project object
        /// </summary>
        public DimensionValue Project { get; set; }

        /// <summary>
        /// Created automatically with Client object
        /// </summary>
        public DimensionValue Account { get; set; }

        /// <summary>
        /// (1 Proj.) Direct, indirect, oper. prod. costs
        /// </summary>
        public DimensionValue CostCenter { get; set; }

        /// <summary>
        /// Project Manager
        /// </summary>
        public DimensionValue ProjectManager { get; set; }

        /// <summary>
        /// (1 Proj.) Project Group
        /// </summary>
        public DimensionValue ProjectGroup { get; set; }

        /// <summary>
        /// According to the contract
        /// </summary>
        public DimensionValue ProjectType { get; set; }

        /// <summary>
        /// Project Utilization
        /// </summary>
        public DimensionValue ProjectUtilization { get; set; }

        /// <summary>
        /// (2 Acc.) Definition of client (key, new, regular)
        /// </summary>
        public DimensionValue AccountType { get; set; }

        /// <summary>
        /// Operational Manager
        /// </summary>
        public DimensionValue OperationalManager { get; set; }

        /// <summary>
        /// (2 Acc.) Region
        /// </summary>
        public DimensionValue Region { get; set; }

        /// <summary>
        /// (2 Acc.) Sales Person
        /// </summary>
        public DimensionValue SalesPerson { get; set; }

        /// <summary>
        /// (2 Acc.) Vertical
        /// </summary>
        public DimensionValue Vertical { get; set; }

        /// <summary>
        /// (3 SP) General Manager
        /// </summary>
        public DimensionValue GeneralManager { get; set; }

        /// <summary>
        /// (3 OM) Operational Director
        /// </summary>
        public DimensionValue OperationalDirector { get; set; }
        
        public Transaction(dynamic row, IDictionary<string, IDictionary<string, DimensionValue>> dimensionNoToDimensionValueMappings)
        {
            Id = row.EntryNo;
            DocumentNo = row.DocumentNo;
            DocumentDate = row.DocumentDate;
            DocumentType = row.DocumentType;
            Description = row.Description;
            AmountPln = (decimal?)row.Amount;
            PostingDate = row.PostingDate;
            AmountEur = (decimal?)row.AverageCurrencyAmount;
            EntryNo = row.EntryNo;
            ExternalDocumentNo = row.ExternalDocumentNo;
            GeneralLedgerAccountNo = row.GeneralLedgerAccountNo;
            SourceNo = row.SourceNo;
            SourceName = row.SourceName;
            SourceCode = row.SourceCode;
            SourceType = row.SourceType;
            SourceCompanyName = row.SourceCompanyName;
            UserId = row.UserId;
            BudgetPosition = GetDimensionValue(DimensionCodes.BudgetPosition, row.BudgetPosition, dimensionNoToDimensionValueMappings);
            Entity = GetDimensionValue(DimensionCodes.Entity, row.Entity, dimensionNoToDimensionValueMappings);
            Ifrs = GetDimensionValue(DimensionCodes.Ifrs, row.Ifrs, dimensionNoToDimensionValueMappings);
            Location = GetDimensionValue(DimensionCodes.Location, row.Location, dimensionNoToDimensionValueMappings);
            NoTax = GetDimensionValue(DimensionCodes.NoTax, row.NoTax, dimensionNoToDimensionValueMappings);
            OrgUnit = GetDimensionValue(DimensionCodes.OrgUnit, row.OrgUnit, dimensionNoToDimensionValueMappings);
            Project = GetDimensionValue(DimensionCodes.Project, row.Project, dimensionNoToDimensionValueMappings);
            Account = GetDimensionValue(DimensionCodes.Account, row.Account, dimensionNoToDimensionValueMappings);
            CostCenter = GetDimensionValue(DimensionCodes.CostCenter, row.CostCenter, dimensionNoToDimensionValueMappings);
            ProjectManager = GetDimensionValue(DimensionCodes.ProjectManager, row.ProjectManager, dimensionNoToDimensionValueMappings);
            ProjectGroup = GetDimensionValue(DimensionCodes.ProjectGroup, row.ProjectGroup, dimensionNoToDimensionValueMappings);
            ProjectType = GetDimensionValue(DimensionCodes.ProjectType, row.ProjectType, dimensionNoToDimensionValueMappings);
            ProjectUtilization = GetDimensionValue(DimensionCodes.ProjectUtilization, row.ProjectUtilization, dimensionNoToDimensionValueMappings);
            AccountType = GetDimensionValue(DimensionCodes.AccountType, row.AccountType, dimensionNoToDimensionValueMappings);
            OperationalManager = GetDimensionValue(DimensionCodes.OperationalManager, row.OperationalManager, dimensionNoToDimensionValueMappings);
            Region = GetDimensionValue(DimensionCodes.Region, row.Region, dimensionNoToDimensionValueMappings);
            SalesPerson = GetDimensionValue(DimensionCodes.SalesPerson, row.SalesPerson, dimensionNoToDimensionValueMappings);
            Vertical = GetDimensionValue(DimensionCodes.Vertical, row.Vertical, dimensionNoToDimensionValueMappings);
            GeneralManager = GetDimensionValue(DimensionCodes.GeneralManager, row.GeneralManager, dimensionNoToDimensionValueMappings);
            OperationalDirector = GetDimensionValue(DimensionCodes.OperationalDirector, row.OperationalDirector, dimensionNoToDimensionValueMappings);
        }

        private static DimensionValue GetDimensionValue(string dimension, string dimensionCode, IDictionary<string, IDictionary<string, DimensionValue>> dimensionNoToDimensionValueMappings)
        {
            if (string.IsNullOrEmpty(dimensionCode))
            {
                return null;
            }

            DimensionValue dimensionValue = null;
            dimensionNoToDimensionValueMappings.TryGetValue(dimension, out var dictionary);
            dictionary?.TryGetValue(dimensionCode, out dimensionValue);

            return dimensionValue;
        }
    }
}