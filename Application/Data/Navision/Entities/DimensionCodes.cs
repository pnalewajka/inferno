﻿namespace Smt.Atomic.Data.Navision.Entities
{
    public static class DimensionCodes
    {
        public const string BudgetPosition = ".BUDGET POS.";
        public const string Entity = ".ENTITY";
        public const string Ifrs = ".IFRS";
        public const string Location = ".LOCATION";
        public const string NoTax = ".N.TAX.";
        public const string OrgUnit = ".ORG.UNIT";
        public const string Project = ".PROJECT";
        public const string Account = "1. ACCOUNT";
        public const string CostCenter = "1. COST CENTRE";
        public const string ProjectManager = "1. PM";
        public const string ProjectGroup = "1. PROJ.GROUP";
        public const string ProjectType = "1. PROJ.TYPE";
        public const string ProjectUtilization = "1. PROJ.UTIL";
        public const string AccountType = "2. ACC.TYPE";
        public const string OperationalManager = "2. OM";
        public const string Region = "2. REGION";
        public const string SalesPerson = "2. SP";
        public const string Vertical = "2. VERTICAL";
        public const string GeneralManager = "3. GM";
        public const string OperationalDirector = "3. OD";
    }
}