﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using Smt.Atomic.Data.Jira.Interfaces;

namespace Smt.Atomic.Data.Jira.Abstract
{
    public abstract class JiraBaseIssue<TEntity> 
        : IJiraBaseIssue where TEntity : class, IJiraBaseIssue
    {
        public ProjectVersionCollection AffectsVersions { get; set; }
        public string Assignee { get; set; }
        public ProjectComponentCollection Components { get; set; }
        public DateTime? Created { get; set; }
        public string Description { get; set; }
        public DateTime? DueDate { get; set; }
        public string Environment { get; set; }
        public ProjectVersionCollection FixVersions { get; set; }
        public string JiraIdentifier { get; set; }
        public string Key { get; set; }
        public string ParentIssueKey { get; set; }
        public IssuePriority Priority { get; set; }
        public string Project { get; set; }
        public string Reporter { get; set; }
        public IssueResolution Resolution { get; set; }
        public DateTime? ResolutionDate { get; set; }
        public IssueSecurityLevel SecurityLevel { get; set; }
        public IssueStatus Status { get; set; }
        public string Summary { get; set; }
        public IssueType Type { get; set; }
        public DateTime? Updated { get; set; }
        public long? Votes { get; set; }
        public string[] Labels { get; set; }
    }
}
