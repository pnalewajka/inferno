﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Jira.Dto
{
    public class NewIssueDto
    {
        public NewIssueDto()
        {
            CustomFields = new Dictionary<string, string[]>();
        }

        public string ProjectKey { get; set; }

        public string Summary { get; set; }

        public string Description { get; set; }

        public string IssueType { get; set; }

        public string Reporter { get; set; }

        public string Security { get; set; }

        public DateTime? DueDate { get; set; }

        public IDictionary<string, string[]> CustomFields { get; private set; }
    }
}
