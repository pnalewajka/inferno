﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Data.Jira.Dto
{
    public class UpdateIssueDto
    {
        public UpdateIssueDto()
        {
            CustomFields = new Dictionary<string, string[]>();
        }

        public Optional<DateTime> DueDate { get; set; }

        public IDictionary<string, string[]> CustomFields { get; private set; }
    }   
}
