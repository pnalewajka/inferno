﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Jira.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Data.Jira
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                case ContainerType.WebApi:
                case ContainerType.WebServices:
                    container.Register(Component.For<IJiraService>().ImplementedBy<JiraService>().LifestyleTransient());
                    container.Register(Component.For<IJiraContextWrapper>().ImplementedBy<JiraContext>().LifestyleTransient());
                    break;
                case ContainerType.JobScheduler:
                    container.Register(Component.For<IJiraService>().ImplementedBy<JiraService>().LifestyleTransient());
                    container.Register(Component.For<IJiraContextWrapper>().ImplementedBy<JiraContext>().LifestyleTransient());
                    break;
            }
        }
    }
}

