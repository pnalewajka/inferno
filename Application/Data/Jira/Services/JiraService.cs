﻿using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Jira.Interfaces;

namespace Smt.Atomic.Data.Jira.Services
{
    public class JiraService : IJiraService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISystemParameterService _systemParameterService;

        public JiraService(ISettingsProvider settingsProvider, ISystemParameterService systemParameterService)
        {
            _settingsProvider = settingsProvider;
            _systemParameterService = systemParameterService;
        }

        public IJiraContextWrapper Create(string serverAddress, string username, string password)
        {
            return ReflectionHelper.CreateInstanceWithIocDependencies<JiraContext>(new { serverAddress, username, password });
        }

        public IJiraContextWrapper CreateReadOnly()
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.JiraReadOnlyUrl);
            var username = _systemParameterService.GetParameter<string>(ParameterKeys.JiraUsername);
            var password = _systemParameterService.GetParameter<string>(ParameterKeys.JiraPassword);

            return Create(serverAddress, username, password);
        }

        public IJiraContextWrapper CreateReadWrite(string username, string password)
        {
            var jiraSettings = _settingsProvider.AuthorizationProviderSettings.JiraWriterSettings;
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.JiraReadWriteUrl);

            username = username ?? jiraSettings.User;
            password = password ?? jiraSettings.Password;

            return Create(serverAddress, username, password);
        }
    }
}
