﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Atlassian.Jira;
using Atlassian.Jira.Remote;
using Castle.Core.Logging;
using RestSharp;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Jira.Abstract;
using Smt.Atomic.Data.Jira.Attributes;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Entities;
using Smt.Atomic.Data.Jira.Interfaces;

namespace Smt.Atomic.Data.Jira.Services
{
    public class JiraContext : IJiraContextWrapper
    {
        private readonly Atlassian.Jira.Jira _jira;
        private readonly ILogger _logger;

        public JiraContext(
            ILogger logger,
            string serverAddress,
            string username,
            string password)
        {
            if (string.IsNullOrEmpty(serverAddress))
            {
                throw new ArgumentNullException(nameof(serverAddress));
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            var settings = new JiraRestClientSettings();

            settings.CustomFieldSerializers.Add("com.googlecode.jira-suite-utilities:locationselect",
                new SingleObjectCustomFieldValueSerializer("value"));

            _logger = logger;
            _jira = Atlassian.Jira.Jira.CreateRestClient(serverAddress, username, password, settings);
            _jira.MaxIssuesPerRequest = int.MaxValue;
        }

        #region Base project

        public IEnumerable<Project> GetAllProjects()
        {
            return _jira.Projects.GetProjectsAsync().Result;
        }

        public Project GetProjectById(string id)
        {
            return GetAllProjects().FirstOrDefault(x => x.Id == id);
        }

        public Project GetProjectByKey(string key)
        {
            return GetAllProjects().FirstOrDefault(x => x.Key == key);
        }

        #endregion

        #region Base Issue

        public Issue GetIssueByKey(string key)
        {
            return _jira.Issues.GetIssueAsync(key).Result;
        }

        public IEnumerable<Issue> GetIssuesByJql(string jql, int? maxIssues = null, int? startAt = null)
        {
            return _jira.Issues.GetIsssuesFromJqlAsync(jql, maxIssues, startAt ?? 0).Result;
        }

        public IEnumerable<Issue> GetIssuesByQuery(Expression<Func<Issue, bool>> predicate)
        {
            return _jira.Issues.Queryable.Where(predicate);
        }

        public string CreateIssue(NewIssueDto newIssue)
        {
            var issue = _jira.CreateIssue(newIssue.ProjectKey);
            var issueTypes = Task.Run(() => _jira.IssueTypes.GetIssueTypesAsync()).Result;
            var reporter = Task.Run(() => _jira.Users.SearchUsersAsync(newIssue.Reporter)).Result.First();

            issue.Summary = newIssue.Summary;
            issue.Description = newIssue.Description;
            issue.Type = issueTypes.Single(t => t.Name == newIssue.IssueType);
            issue.Reporter = reporter.Username;
            issue.DueDate = newIssue.DueDate;

            AddOrUpdateCustomFields(issue, newIssue.CustomFields);

            issue.SaveChanges();

            if (!string.IsNullOrEmpty(newIssue.Security))
            {
                var body = new { fields = new { security = new { id = newIssue.Security } } };

                _jira.RestClient.ExecuteRequestAsync(Method.PUT, $"rest/api/2/issue/{issue.Key.Value}", body).Wait();
            }

            return issue.Key.ToString();
        }

        public void UpdateIssue(string issueKey, UpdateIssueDto updateIssue)
        {
            var issue = GetIssueByKey(issueKey);

            if (updateIssue.DueDate?.IsSet ?? false)
            {
                issue.DueDate = updateIssue.DueDate.Value;
            }

            AddOrUpdateCustomFields(issue, updateIssue.CustomFields);

            issue.SaveChanges();
        }

        public void AddCommentToIssue(string issueKey, string comment)
        {
            var issue = GetIssueByKey(issueKey);

            issue.AddCommentAsync(comment).Wait();
        }

        public string[] GetValueFromCustomField(string issueKey, string customFieldKey)
        {
            var issue = GetIssueByKey(issueKey);
            var customField = issue.CustomFields.SingleOrDefault(f => f.Id == customFieldKey);

            return customField?.Values;
        }

        private void AddOrUpdateCustomFields(Issue issue, IDictionary<string, string[]> customFields)
        {
            foreach (var pair in customFields)
            {
                var customField = issue.CustomFields.SingleOrDefault(f => f.Id == pair.Key);

                if (customField != null)
                {
                    customField.Values = pair.Value;
                }
                else
                {
                    issue.CustomFields.AddById(pair.Key, pair.Value);
                }
            }
        }

        #endregion

        #region Base users

        public JiraUser GetUserOrDefaultByUserName(string username)
        {
            try
            {
                var user = _jira.Users.GetUserAsync(username).Result;

                if (user != null)
                {
                    return user;
                }
            }
            catch(Exception e)
            {
                _logger.Warn($"Error while searching user {username} in Jira.", e);
                return null;
            }

            _logger.Warn($"Cannot find {username} user in Jira.");

            return null;
        }

        #endregion

        #region Named issue

        public TEntity GetIssueByKey<TEntity>(string key)
            where TEntity : JiraBaseIssue<TEntity>, new()
        {
            return CreateFromSource<TEntity>(GetIssueByKey(key));
        }

        public IEnumerable<TEntity> GetIssuesByJql<TEntity>(string jql)
            where TEntity : JiraBaseIssue<TEntity>, new()
        {
            return GetIssuesByJql(jql).Select(x => CreateFromSource<TEntity>(x));
        }

        public IEnumerable<TEntity> GetIssuesByJql<TEntity>(string jql, int? maxIssues = null, int? startAt = null)
            where TEntity : JiraBaseIssue<TEntity>, new()
        {
            return GetIssuesByJql(jql, maxIssues, startAt).Select(x => CreateFromSource<TEntity>(x));
        }

        public IEnumerable<TEntity> GetIssuesByQuery<TEntity>(Expression<Func<Issue, bool>> predicate)
            where TEntity : JiraBaseIssue<TEntity>, new()
        {
            return GetIssuesByQuery(predicate).Select(x => CreateFromSource<TEntity>(x));
        }

        #endregion

        #region Worklogs

        public IList<IssueWithWorklogs> GetIssuesWithWorklogs(string jiraUsername, DateTime from, DateTime to)
        {
            var request = new RestRequest
            {
                Method = Method.GET,
                Resource = "rest/api/2/search"
            };

            request.AddParameter("maxResults", -1);
            request.AddParameter("fields", string.Join(",", "worklog", "project", "summary", "parent", "issuetype", "customfield_10411", "customfield_10893"));
            request.AddParameter("jql", $"key in workedIssues('{from:yyyy-MM-dd}', '{to:yyyy-MM-dd}', '{jiraUsername}')");

            var restResponse = _jira.RestClient.RestSharpClient.Execute<IssuesWithWorklogsSearch>(request);

            var issues = restResponse.Data.Issues;

            // Filter worklogs
            issues.ForEach(i => NormalizeIssue(i, jiraUsername, from, to));

            // Merge sub-issues
            MergeSubIssues(issues);

            return issues;
        }

        private void MergeSubIssues(List<IssueWithWorklogs> issues)
        {
            var subIssues = issues.Where(i => i.IsSubIssue).ToList();

            foreach (var subIssue in subIssues)
            {
                // Assuming max nesting level == 1
                var parentIssue = issues.SingleOrDefault(i => i.Key == subIssue.Fields.Parent.Key);

                if (parentIssue == null)
                {
                    parentIssue = subIssue.Fields.Parent;
                    parentIssue.Fields.Project = subIssue.Fields.Project;
                    parentIssue.Fields.Worklog = new WorklogField();

                    issues.Add(parentIssue);
                }

                // Merge worklogs
                parentIssue.Fields.Worklog.Worklogs.AddRange(subIssue.Fields.Worklog.Worklogs);
            }

            issues.RemoveAll(i => i.IsSubIssue);
        }

        private void NormalizeIssue(IssueWithWorklogs issueWithWorklogs, string jiraUsername, DateTime from, DateTime to)
        {
            if (issueWithWorklogs.Fields.Worklog.Total > issueWithWorklogs.Fields.Worklog.MaxResults)
            {
                issueWithWorklogs.Fields.Worklog = GetAllWorkLogs(issueWithWorklogs.Id);
            }

            issueWithWorklogs.Fields.Worklog.Worklogs =
                issueWithWorklogs.Fields.Worklog.Worklogs.Where(
                    wl =>
                        wl.AuthorUsername == jiraUsername && from <= wl.StartOrCreationDate && wl.StartOrCreationDate <= to)
                    .ToList();
        }

        private WorklogField GetAllWorkLogs(long issueId)
        {
            var request = new RestRequest
            {
                Method = Method.GET,
                Resource = $"rest/api/2/issue/{issueId}/worklog"
            };

            request.AddParameter("maxResults", -1);

            var restResponse = _jira.RestClient.RestSharpClient.Execute<WorklogField>(request);

            return restResponse.Data;
        }

        #endregion

        #region Mapping & Attributes

        private TEntity CreateFromSource<TEntity>(Issue issue)
            where TEntity : JiraBaseIssue<TEntity>, IJiraBaseIssue, new()
        {
            var entity = new TEntity();

            entity.Key = issue.Key.Value;

            entity.AffectsVersions = issue.AffectsVersions;
            entity.Assignee = issue.Assignee;
            entity.Components = issue.Components;
            entity.Created = issue.Created;
            entity.Description = issue.Description;
            entity.DueDate = issue.DueDate;
            entity.Environment = issue.Environment;
            entity.FixVersions = issue.FixVersions;
            entity.JiraIdentifier = issue.JiraIdentifier;
            entity.ParentIssueKey = issue.ParentIssueKey;
            entity.Priority = issue.Priority;
            entity.Project = issue.Project;
            entity.Reporter = issue.Reporter;
            entity.Resolution = issue.Resolution;
            entity.ResolutionDate = issue.ResolutionDate;
            entity.SecurityLevel = issue.SecurityLevel;
            entity.Status = issue.Status;
            entity.Summary = issue.Summary;
            entity.Type = issue.Type;
            entity.Updated = issue.Updated;
            entity.Votes = issue.Votes;
            entity.Labels = issue.GetLabelsAsync().Result;
            UpdateJiraAttribures(ref entity, issue);

            return entity;
        }

        private void UpdateJiraAttribures<TEntity>(ref TEntity entity, Issue issue)
        {
            var propertiesWithCustomFieldAttribute =
                TypeHelper.GetPublicInstancePropertiesWithAttribute<JiraIssueCustomFieldAttribute>(typeof(TEntity));

            foreach (var property in propertiesWithCustomFieldAttribute)
            {
                var metaData = AttributeHelper.GetPropertyAttribute<JiraIssueCustomFieldAttribute>(property);
                string customField = metaData.CustomField;

                UpdatePropertyValue(issue, customField, entity, property);
            }

        }

        private void UpdatePropertyValue(Issue issue, string customField, object entity, PropertyInfo property)
        {
            var customValue = issue.CustomFields.SingleOrDefault(x => x.Id == customField);
            if (customValue != null && customValue.Values.Any())
            {
                UpdatePropertyValue(entity, property, customValue.Values.FirstOrDefault());
            }
            else
            {
                UpdatePropertyValue(entity, property, null);
            }
        }

        private void UpdatePropertyValue(object entity, PropertyInfo property, object value)
        {
            if (property.PropertyType == typeof(JiraUser))
            {
                var jiraUser = value == null ? null : GetUserOrDefaultByUserName(value.ToString());
                property.SetValue(entity, jiraUser);
            }
            else
            {
                var convertType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                var objectValue = (value == null) ? null : Convert.ChangeType(value, convertType);
                property.SetValue(entity, objectValue);
            }
        }

        #endregion

        public void Dispose()
        {

        }
    }
}
