﻿using System;

namespace Smt.Atomic.Data.Jira.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class JiraIssueCustomFieldAttribute : Attribute
    {
        public JiraIssueCustomFieldAttribute(string customfield)
        {
            CustomField = customfield;
        }

        public string CustomField { get; }
    }
}
