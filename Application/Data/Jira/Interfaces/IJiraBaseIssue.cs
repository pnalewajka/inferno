﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;

namespace Smt.Atomic.Data.Jira.Interfaces
{
    public interface IJiraBaseIssue
    {
        //
        // Summary:
        //     The versions that are affected by this issue
        ProjectVersionCollection AffectsVersions { get; set; }

        //
        // Summary:
        //     Person to whom the issue is currently assigned
        string Assignee { get; set; }

        //
        // Summary:
        //     The components associated with this issue
        ProjectComponentCollection Components { get; set; }

        //
        // Summary:
        //     Time and date on which this issue was entered into JIRA
        DateTime? Created { get; set; }

        //
        // Summary:
        //     Detailed description of the issue
        string Description { get; set; }

        //
        // Summary:
        //     Date by which this issue is scheduled to be completed
        DateTime? DueDate { get; set; }

        //
        // Summary:
        //     Hardware or software environment to which the issue relates
        string Environment { get; set; }

        //
        // Summary:
        //     The versions in which this issue is fixed
        ProjectVersionCollection FixVersions { get; set; }


        //
        // Summary:
        //     Gets the internal identifier assigned by JIRA.
        string JiraIdentifier { get; set; }

        //
        // Summary:
        //     Unique identifier for this issue
        string Key { get; set; }

        //
        // Summary:
        //     The parent key if this issue is a subtask.
        //
        // Remarks:
        //     Only available if issue was retrieved using REST API.
        string ParentIssueKey { get; set; }

        //
        // Summary:
        //     Importance of the issue in relation to other issues
        IssuePriority Priority { get; set; }

        //
        // Summary:
        //     Parent project to which the issue belongs
        string Project { get; set; }

        //
        // Summary:
        //     Person who entered the issue into the system
        string Reporter { get; set; }

        //
        // Summary:
        //     Record of the issue's resolution, if the issue has been resolved or closed
        IssueResolution Resolution { get; set; }

        //
        // Summary:
        //     Time and date on which this issue was resolved.
        //
        // Remarks:
        //     Only available if issue was retrieved using REST API, use GetResolutionDate method
        //     for SOAP clients.
        DateTime? ResolutionDate { get; set; }

        //
        // Summary:
        //     Gets the security level set on the issue.
        IssueSecurityLevel SecurityLevel { get; set; }

        //
        // Summary:
        //     The stage the issue is currently at in its lifecycle.
        IssueStatus Status { get; set; }

        //
        // Summary:
        //     Brief one-line summary of the issue
        string Summary { get; set; }

        //
        // Summary:
        //     The type of the issue
        IssueType Type { get; set; }

        //
        // Summary:
        //     Time and date on which this issue was last edited
        DateTime? Updated { get; set; }

        //
        // Summary:
        //     Number of votes the issue has
        long? Votes { get; set; }
    }
}
