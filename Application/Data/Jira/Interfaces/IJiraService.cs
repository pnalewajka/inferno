﻿namespace Smt.Atomic.Data.Jira.Interfaces
{
    public interface IJiraService
    {
        /// <summary>
        /// Creates IJiraContextWrapper by using provided serverAddress, username and password.
        /// </summary>
        IJiraContextWrapper Create(string serverAddress, string username, string password);

        /// <summary>
        /// Creates IJiraContextWrapper by using read-only server address, username and password stored in system parameters.
        /// </summary>
        /// <remarks>
        /// Read-only server address always points to production Jira regardless of environment.
        /// </remarks>
        IJiraContextWrapper CreateReadOnly();

        /// <summary>
        /// Creates IJiraContextWrapper by using read-write server address stored in system parameters.
        /// Username and password can be provided for special cases (i.e. elevated privileges), otherwise the default one is used.
        /// </summary>
        /// <remarks>
        /// Read-write server address points to production Jira in PROD environment and stage Jira in STAGE or DEV environments.
        /// </remarks>
        IJiraContextWrapper CreateReadWrite(string username = null, string password = null);
    }
}
