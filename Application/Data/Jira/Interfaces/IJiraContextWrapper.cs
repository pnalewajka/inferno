﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Atlassian.Jira;
using Smt.Atomic.Data.Jira.Abstract;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Entities;

namespace Smt.Atomic.Data.Jira.Interfaces
{
    public interface IJiraContextWrapper : IDisposable
    {
        string CreateIssue(NewIssueDto newIssue);

        void UpdateIssue(string issueKey, UpdateIssueDto updateIssue);

        void AddCommentToIssue(string issueKey, string comment);

        IEnumerable<Project> GetAllProjects();

        Project GetProjectById(string id);

        Project GetProjectByKey(string key);

        Issue GetIssueByKey(string key);

        IEnumerable<Issue> GetIssuesByJql(string jql, int? maxIssues = null, int? startAt = null);

        IEnumerable<Issue> GetIssuesByQuery(Expression<Func<Issue, bool>> predicate);

        JiraUser GetUserOrDefaultByUserName(string username);

        TEntity GetIssueByKey<TEntity>(string key)
            where TEntity : JiraBaseIssue<TEntity>, new();

        IEnumerable<TEntity> GetIssuesByJql<TEntity>(string jql)
            where TEntity : JiraBaseIssue<TEntity>, new();

        IEnumerable<TEntity> GetIssuesByJql<TEntity>(string jql, int? maxIssues = null, int? startAt = null)
            where TEntity : JiraBaseIssue<TEntity>, new();
        
        IEnumerable<TEntity> GetIssuesByQuery<TEntity>(Expression<Func<Issue, bool>> predicate)
            where TEntity : JiraBaseIssue<TEntity>, new();

        IList<IssueWithWorklogs> GetIssuesWithWorklogs(string jiraUsername, DateTime from, DateTime to);

        string[] GetValueFromCustomField(string issueKey, string customFieldKey);
    }
}
