﻿using System.Collections.Generic;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class WorklogField
    {
        public long MaxResults { get; set; }
        public long Total { get; set; }
        public List<DetailedWorklog> Worklogs { get; set; }

        public WorklogField()
        {
            Worklogs = new List<DetailedWorklog>();
        }
    }
}
