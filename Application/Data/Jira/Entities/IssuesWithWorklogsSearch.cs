﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class IssuesWithWorklogsSearch
    {
        public long MaxResults { get; set; }
        public long Total { get; set; }
        public List<IssueWithWorklogs> Issues { get; set; }
    }
}
