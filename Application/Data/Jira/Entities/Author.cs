﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class Author
    {
        public string Name { get; set; }
    }
}
