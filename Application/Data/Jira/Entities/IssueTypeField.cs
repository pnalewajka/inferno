﻿namespace Smt.Atomic.Data.Jira.Entities
{
    public class IssueTypeField
    {
        public string Id { get; set; }
        public string Self { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Subtask { get; set; }
        public string IconUrl { get; set; }
    }
}