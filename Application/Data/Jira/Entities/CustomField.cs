﻿namespace Smt.Atomic.Data.Jira.Entities
{
    public class CustomField
    {
        public string Id { get; set; }
        public string Self { get; set; }
        public string Value { get; set; }
    }
}
