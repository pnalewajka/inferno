﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class IssueWithWorklogs
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public IssueFields Fields { get; set; }

        public bool IsSubIssue => Fields.Parent != null;
    }
}
