﻿using System;
using Atlassian.Jira;
using Smt.Atomic.Data.Jira.Abstract;
using Smt.Atomic.Data.Jira.Attributes;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class ProjectIssue : JiraBaseIssue<ProjectIssue>
    {
        [JiraIssueCustomField("customfield_15394")]
        public string JiraProjectDevelopmentLink {get; set; }

        [JiraIssueCustomField("customfield_13190")]
        public string Department { get; set; }

        [JiraIssueCustomField("customfield_13191")]
        public string Region { get; set; }

        [JiraIssueCustomField("customfield_15490")]
        public string Company { get; set; }

        [JiraIssueCustomField("customfield_10116")]
        public DateTime? BeginDate { get; set; }

        [JiraIssueCustomField("customfield_10117")]
        public DateTime? EndDate { get; set; }

        [JiraIssueCustomField("customfield_10111")]
        public JiraUser ProjectManager { get; set; }

        [JiraIssueCustomField("customfield_16624")]
        public string SupervisorManagerLogin { get; set; }

        [JiraIssueCustomField("customfield_15892")]
        public JiraUser SalesAccountManager { get; set; }

        [JiraIssueCustomField("customfield_15999")]
        public string ProjectDescription { get; set; }

        [JiraIssueCustomField("customfield_16202")]
        public string CalendarCode { get; set; }

        [JiraIssueCustomField("customfield_15900")]
        public string ApnJira { get; set; }

        [JiraIssueCustomField("customfield_15894")]
        public string ApnPets { get; set; }

        [JiraIssueCustomField("customfield_16204")]
        public string KupferwerkProjectId { get; set; }

        [JiraIssueCustomField("customfield_10106")]
        public string FullCustomerName { get; set; }

        [JiraIssueCustomField("customfield_10109")]
        public string ProjectType { get; set; }
    }
}
