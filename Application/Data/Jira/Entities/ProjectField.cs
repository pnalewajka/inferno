﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class ProjectField
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
