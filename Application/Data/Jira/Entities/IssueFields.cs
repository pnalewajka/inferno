﻿namespace Smt.Atomic.Data.Jira.Entities
{
    public class IssueFields
    {
        public string Summary { get; set; }
        public ProjectField Project { get; set; }
        public WorklogField Worklog { get; set; }
        public IssueTypeField IssueType { get; set; }
        public IssueWithWorklogs Parent { get; set; }

        // Custom fields (on demand)
        public CustomField CustomField_10893 { get; set; } // IT linked project
    }
}
