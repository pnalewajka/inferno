﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Jira.Entities
{
    public class DetailedWorklog
    {
        public Author Author { protected get; set; }
        public DateTime Created { protected get; set; }
        public DateTime? Started { protected get; set; }
        public long TimeSpentSeconds { get; set; }

        public string AuthorUsername => Author.Name;
        public DateTime StartOrCreationDate => (Started ?? Created).Date;
        public decimal TimeSpentHours => decimal.Round(TimeSpentSeconds / 3600m, 2);
    }
}
