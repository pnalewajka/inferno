﻿namespace Smt.Atomic.Data.Crm.Enums
{
    internal enum CrmInterviewTypeEnum
    {
        FaceToFace = 180580000,
        Skype = 180580001,
        Phone = 180580002,
        Other = 180580003
    }
}