﻿using System;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Crm.BusinessEvents;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Crm.BusinessEventHandlers
{
    internal class ScheduleAppointmentEventHandler : BusinessEventHandler<ScheduleAppointmentEvent>
    {
        private readonly ICrmAppointmentService _crmAppointmentService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public ScheduleAppointmentEventHandler(ICrmAppointmentService crmAppointmentService, IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmAppointmentService = crmAppointmentService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(ScheduleAppointmentEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendation = unitOfWork.Repositories.Recommendations.GetById(businessEvent.RecommendationId);

                _crmAppointmentService.NotifyAppointmentService(
                    recommendation.ResourceId,
                    businessEvent.AppointmentStartDateTime, 
                    businessEvent.AppointmentEndDateTime);
            }
        }
    }
}
