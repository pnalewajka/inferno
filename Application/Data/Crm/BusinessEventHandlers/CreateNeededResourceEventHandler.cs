﻿using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Crm.BusinessEvents;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Helpers;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;

namespace Smt.Atomic.Data.Crm.BusinessEventHandlers
{
    internal class CreateNeededResourceEventHandler : BusinessEventHandler<CreateNeededResourceEvent>
    {
        private readonly ICrmNeededResourcesService _crmNeededResourcesService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public CreateNeededResourceEventHandler(
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            ICrmNeededResourcesService crmNeededResourcesService)
        {
            _unitOfWorkService = unitOfWorkService;
            _crmNeededResourcesService = crmNeededResourcesService;
        }

        public override void Handle(CreateNeededResourceEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var mainTechnology = unitOfWork.Repositories.MainTechnologyOptions.GetById(businessEvent.TechnologyDepartmentId);
                var region = unitOfWork.Repositories.RegionOptions.GetById(businessEvent.RegionId);
                var resourceForm = unitOfWork.Repositories.ResourceForms.GetById(businessEvent.ResourceFormId);
                var operationManager = unitOfWork.Repositories.Users.GetById(resourceForm.ApprovedById.Value);

                var neededResourceId = _crmNeededResourcesService.CreateNeededResource(new UpdateNeededResourceDto
                {
                    ResourceName = businessEvent.ResourceName,
                    TechnologyDepartmentGuid = mainTechnology.CrmId,
                    RegionGuid = region.CrmId,
                    ContactClientGuid = businessEvent.ContactClientGuid,
                    AccountGuid = businessEvent.AccountGuid,
                    NumberOfPeople = businessEvent.NumberOfPeople,
                    Rate = businessEvent.Rate,
                    Seniority1 = businessEvent.Seniority1,
                    Seniority2 = businessEvent.Seniority2,
                    Seniority3 = businessEvent.Seniority3,
                    Seniority4 = businessEvent.Seniority4,
                    Seniority5 = businessEvent.Seniority5,
                    Requirements = resourceForm.PositionRequirements,
                    RecruitmentProcess = resourceForm.RecruitmentPhases,
                    ProjectDescription = resourceForm.ProjectDescription,
                    RoleDescription = resourceForm.RoleDescription,
                    LanguageComment = resourceForm.LanguageComment,
                    AdditionalComment = resourceForm.AdditionalComment,
                    OperationManager = new UserDto { FirstName = operationManager.FirstName, LastName = operationManager.LastName },
                    LanguageRequirements = resourceForm.LanguageOption.Select(l => new CrmLanguageOptionDto { LanguageId = l.LanguageId, LanguageName = l.LanguageName, LevelId = l.LevelId, LevelName = l.LevelName })
                });

                resourceForm.NeededResourceCrmCode = neededResourceId;
                unitOfWork.Repositories.NeededResources.Add(new NeededResource
                {
                    CrmId = neededResourceId,
                    Name = businessEvent.ResourceName,
                    ResourceForm = resourceForm,
                });

                unitOfWork.Commit();
            }
        }
    }
}
