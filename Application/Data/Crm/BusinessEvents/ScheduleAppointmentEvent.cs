using System;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Data.Crm.BusinessEvents
{
    public class ScheduleAppointmentEvent : BusinessEvent
    {
        public long RecommendationId { get; set; }

        public DateTime AppointmentStartDateTime { get; set; }

        public DateTime AppointmentEndDateTime { get; set; }
    }
}