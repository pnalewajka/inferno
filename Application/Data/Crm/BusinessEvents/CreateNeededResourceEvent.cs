﻿using System;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Data.Crm.BusinessEvents
{
    public class CreateNeededResourceEvent : BusinessEvent
    {
        public string ResourceName { get; set; }

        public long ResourceFormId { get; set; }

        public long TechnologyDepartmentId { get; set; }

        public long RegionId { get; set; }

        public int PriorityId { get; set; }

        public Guid ContactClientGuid { get; set; }

        public Guid AccountGuid { get; set; }

        public int NumberOfPeople { get; set; }

        public bool Seniority1 { get; set; }

        public bool Seniority2 { get; set; }

        public bool Seniority3 { get; set; }

        public bool Seniority4 { get; set; }

        public bool Seniority5 { get; set; }

        public long? OperationManagerId { get; set; }

        public string Rate { get; set; }
    }
}