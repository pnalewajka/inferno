﻿namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmConnectionStringProvider
    {
        string GetConnectionString();

        string GetUrl();
    }
}
