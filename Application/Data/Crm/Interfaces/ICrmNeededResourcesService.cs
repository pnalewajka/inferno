﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Data.Crm.Dto;

namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmNeededResourcesService
    {
        Guid CreateNeededResource(UpdateNeededResourceDto dto);

        IEnumerable<Guid> LoadCustomerNeededResourcesCodes(Guid clientId);

        NeededResourceDto LoadNeededResource(Guid neededResourceId);

        IEnumerable<Guid> LoadRecruitmentActivitiesGuids(Guid neededResourceId);

        RecruitmentActivitieDto LoadRecruitmentActivitie(Guid recruitmentActivitieGuid);

        IEnumerable<string> ResolveRecruitmentActivitieDocumentUrls(Guid recruitmentActivitieGuid);
    }
}