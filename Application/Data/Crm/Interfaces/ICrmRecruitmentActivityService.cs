﻿namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmRecruitmentActivityService
    {
        void TriggerEmailAlertToCreateReminderForOwnerRA();
    }
}
