﻿using System;

namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmAppointmentService
    {
        void NotifyAppointmentService(Guid resourceId, DateTime startDateTime, DateTime endDateTime);
    }
}