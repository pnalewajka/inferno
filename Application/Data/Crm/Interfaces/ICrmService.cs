using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smt.Atomic.Data.Crm.Dto;

namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmService
    {
        CrmCandidateDto GetCandidate(Guid recruitmentActivityId, bool loadCandidateContacts = false);

        IEnumerable<CrmCandidateDto> GetCandidates(DateTime? dateFrom, int pageSize = 1000);

        IEnumerable<Guid> GetGuidsOfCandidatesToContactOn(DateTime contactOn);

        Task<CrmCandidateDto> GetCandidateAsync(Guid recruitmentActivityId, bool loadCandidateContacts = false);

        ClientContactDto[] GetClientContactsByRecruitmentActivityIds(Guid[] recruitmentActivityIds);

        CandidateActivityDto[] GetCandidatesByNeededResourceIds(Guid[] neededResourceIds);

        Task<ClientContactDto[]> GetClientContactsByRecruitmentActivityIdsAsync(Guid[] recruitmentActivityIds);

        void AddRecruitmentActivityNote(Guid recruitmentActivityId, string subject, string text);

        void UpdateRecruitmentActivityWithIntroductionDate(Guid recruitmentActivityId, DateTime date);

        void UpdateRecruitmentActivityClientDecision(Guid recruitmentActivityId, DateTime date, bool isRejected, string reason);

        void UpdateCandidateContactDateAlert(IEnumerable<Guid> candidateIds);

        ClientContactDto GetClientContactInfo(Guid clientContactId);

        IReadOnlyDictionary<Guid, NeededResourceDto> GetNeededResourcesByRecruitmentActivityIds(Guid[] recruitmentActivityIds);

        NeededResourceOwnerDto GetRecruitmentActivityOwner(Guid recruitmentActivityId);

        string RecruitmentActivityUrl(Guid recruitmentId);
    }
}
