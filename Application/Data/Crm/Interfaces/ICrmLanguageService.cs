﻿using System.Collections.Generic;
using Smt.Atomic.Data.Crm.Dto;

namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmLanguageService
    {
        IEnumerable<CrmLanguageOptionDto> GetLanguageOptions();
    }
}