﻿using System;
using Smt.Atomic.Data.Crm.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Crm.Interfaces
{
    public interface ICrmOptionService
    {
        IEnumerable<CrmDictionaryDto> GetMainTechnologyOptions();
        IEnumerable<CrmDictionaryDto> GetRegionOption();
        IEnumerable<CrmBusinessUnitOptionDto> GetBusinessUnitOptions();

        IEnumerable<CrmDictionaryDto> ClientAccounts(DateTime? createdAfter);
        IEnumerable<CrmDictionaryDto> ClientContacts(DateTime? createdAfter);
    }
}
