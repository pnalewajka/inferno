﻿using System;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class NeededResourceDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string CompanyName { get; set; }
    }
}
