﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class CrmCandidateDto
    {
        public Guid CrmId { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public List<SpDocumentLocationDto> DocumentLocations { get; set; }

        public string ContactName { get; set; }

        public string ContactFirstName { get; set; }

        public string ContactEmail { get; set; }

        public string NeededResourceName { get; set; }

        public string SdmName { get; set; }

        public string SdmFirstName { get; set; }

        public string SdmEmail { get; set; }
    }
}
