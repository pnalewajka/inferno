﻿using System;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class CrmDictionaryDto
    {
        public Guid Id { get; internal set; }

        public string Name { get; internal set; }

        public string Email { get; internal set; }

        public Guid CompanyGuidId { get; internal set; }

        public string CompanyName { get; internal set; }
    }
}
