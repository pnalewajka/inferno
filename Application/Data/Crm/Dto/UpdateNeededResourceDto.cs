﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Accounts.Dto;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class UpdateNeededResourceDto
    {
        public string ResourceName { get; set; }

        public Guid TechnologyDepartmentGuid { get; set; }

        public Guid RegionGuid { get; set; }

        public Guid ContactClientGuid { get; set; }

        public Guid AccountGuid { get; set; }

        public int NumberOfPeople { get; set; }

        public bool Seniority1 { get; set; }

        public bool Seniority2 { get; set; }

        public bool Seniority3 { get; set; }

        public bool Seniority4 { get; set; }

        public bool Seniority5 { get; set; }

        public string Requirements { get; set; }

        public string Rate { get; set; }

        public string ProjectDescription { get; set; }

        public string RecruitmentProcess { get; set; }

        public string RoleDescription { get; set; }

        public string AdditionalComment { get; set; }

        public string LanguageComment { get; set; }

        public UserDto OperationManager { get; set; }

        public IEnumerable<CrmLanguageOptionDto> LanguageRequirements { get; set; }
        
    }
}
