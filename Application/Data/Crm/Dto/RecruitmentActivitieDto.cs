﻿using System;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class RecruitmentActivitieDto
    {
        public string Name { get; set; }

        public Guid CrmId { get; set; }
    }
}
