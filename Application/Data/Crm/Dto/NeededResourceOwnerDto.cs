﻿using System;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class NeededResourceOwnerDto
    {
        public Guid NeededResourceId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}
