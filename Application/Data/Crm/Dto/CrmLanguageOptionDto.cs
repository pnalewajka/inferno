﻿namespace Smt.Atomic.Data.Crm.Dto
{
    public class CrmLanguageOptionDto
    {
        public int LevelId { get; set; }

        public int LanguageId { get; set; }

        public string LevelName { get; set; }

        public string LanguageName { get; set; }
    }
}