using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Crm;

namespace Smt.Atomic.Data.Crm.Dto
{
    class CrmCandidateDtoToCrmCandidateMapping : ClassMapping<CrmCandidateDto, CrmCandidate>
    {
        public CrmCandidateDtoToCrmCandidateMapping()
        {
            Mapping = d => new CrmCandidate
            {
                CrmId = d.CrmId,
                FirstName = d.FirstName,
                LastName = d.LastName
            };
        }
    }
}