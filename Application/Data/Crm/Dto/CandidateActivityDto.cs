﻿using System;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class CandidateActivityDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public Guid ActivityId { get; set; }

        public DateTime? DateIntroducedToTheClient { get; set; }

        public bool IsRejected { get; set; }

        public DateTime? DateOfClientDecision { get; set; }

        public bool NeededResourceActive { get; set; }

        public Guid NeededResourceId { get; set; }
    }
}
