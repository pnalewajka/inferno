﻿namespace Smt.Atomic.Data.Crm.Dto
{
    public class SpDocumentLocationDto
    {
        public string SharePointUrl { get; set; }

        public string SharePointDocPath { get; set; }

        public string RelativeUrl { get; set; }
    }
}
