﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class SmtCandidateToCrmCandidateDtoMapping : ClassMapping<smt_candidate, CrmCandidateDto>
    {
        public SmtCandidateToCrmCandidateDtoMapping()
        {
            Mapping = d => new CrmCandidateDto
            {
                CrmId = d.Id,
                ModifiedOn = d.ModifiedOn,
                FirstName = d.new_FirstName,
                LastName = d.smt_LastName2,
                FullName = d.smt_lastname
            };
        }
    }
}
