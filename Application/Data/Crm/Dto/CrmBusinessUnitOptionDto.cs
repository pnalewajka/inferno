﻿namespace Smt.Atomic.Data.Crm.Dto
{
    public class CrmBusinessUnitOptionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
