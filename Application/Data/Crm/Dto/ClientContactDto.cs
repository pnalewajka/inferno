using System;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Crm.Dto
{
    public class ClientContactDto
    {
        public ClientContactDto(Guid recruitmentActivityId, Guid contactId, string name, string firstName, string lastName, string email, string companyName)
        {
            Name = name;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            ContactId = contactId;
            CompanyName = companyName;
            RecruitmentActivityId = recruitmentActivityId;
        }

        public ClientContactDto(Guid contactId, string name, string firstName, string lastName, string email, string companyName, List<NeededResourceDto> neededResources)
        {
            Name = name;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            ContactId = contactId;
            CompanyName = companyName;
            NeededResources = neededResources;
        }

        public Guid ContactId { get; }

        public Guid RecruitmentActivityId { get; }

        public string Name { get; }

        public string FirstName { get; }

        public string LastName { get; }

        public string Login => Email;

        public string Email { get; }

        public string CompanyName { get; set; }

        public List<NeededResourceDto> NeededResources { get; set; }
    }
}