﻿using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Data.Crm.Services
{
    public sealed class CrmConnectionStringProvider : ICrmConnectionStringProvider
    {
        private const string ConnectionStringFormat = "Url={0}; Username={1}; Password={2};";

        private readonly string _connectionString;
        private readonly string _url;
        public CrmConnectionStringProvider(ISettingsProvider iSettingsProvider)
        {
            CrmSettings crmSettings = iSettingsProvider.CrmSettings;
            _url = crmSettings?.CrmUrl;
            _connectionString = string.Format(ConnectionStringFormat, crmSettings?.CrmUrl, crmSettings?.Username, crmSettings?.Password);
        }

        public string GetConnectionString()
        {
            return _connectionString;
        }

        public string GetUrl()
        {
            return _url;
        }
    }
}
