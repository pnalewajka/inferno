using System;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Data.Crm.Services
{
    internal abstract class BaseCrmService
    {
        protected readonly ICrmConnectionStringProvider ConnectionStringProvider;
        protected const int ClientDecisionApproved = 180580001;
        protected const int ClientDecisionRejected = 180580000;
        protected const int ClientCvRejectionReasonNotAvailable = 180580006;
        protected const string RecruitmentActivityCandidateColumnName = "smt_candidate";
        protected const string NeededResourceIdColumnName = "smt_neededresourceid";

        protected const int MaxPassedTimeForOutsidedRecommendations = -60;

        private string _connectionString;
        protected string ConnectitionString => _connectionString ?? (_connectionString = ConnectionStringProvider.GetConnectionString());

        protected BaseCrmService(ICrmConnectionStringProvider connectionStringProvider)
        {
            ConnectionStringProvider = connectionStringProvider;
        }


        protected void Connect(Action<CrmOrganizationServiceContext> action)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    action(context);
                }
            }
        }
    }
}