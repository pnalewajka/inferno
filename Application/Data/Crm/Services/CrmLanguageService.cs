﻿using System.Collections.Generic;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Helpers;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Data.Crm.Services
{
    internal class CrmLanguageService : BaseCrmService, ICrmLanguageService
    {
        public CrmLanguageService(ICrmConnectionStringProvider connectionStringProvider)
            : base(connectionStringProvider)
        {
        }

        public IEnumerable<CrmLanguageOptionDto> GetLanguageOptions()
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var languageLevelOptions =
                        CrmOptionValuesHelper.GetOptionSetDictionary(new_languageskill.EntityLogicalName,
                            "smt_languagelevel", context);
                    var languageOptions =
                        CrmOptionValuesHelper.GetOptionSetDictionary(new_languageskill.EntityLogicalName, "new_language",
                            context);

                    foreach (var languageOption in languageOptions)
                    {
                        foreach (var languageLevelOption in languageLevelOptions)
                        {
                            yield return new CrmLanguageOptionDto
                            {
                                LanguageId = languageOption.Key,
                                LanguageName = languageOption.Value,
                                LevelId = languageLevelOption.Key,
                                LevelName = languageLevelOption.Value
                            };
                        }
                    }
                }
            }
        }
    }
}