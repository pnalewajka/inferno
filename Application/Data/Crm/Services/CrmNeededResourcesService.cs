﻿using Castle.Core.Logging;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Helpers;
using Smt.Atomic.Data.Crm.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Data.Crm.Services
{
    internal class CrmNeededResourcesService : BaseCrmService, ICrmNeededResourcesService
    {
        private readonly ILogger _logger;

        public CrmNeededResourcesService(
            ICrmConnectionStringProvider connectionStringProvider,
            ILogger logger)
            : base(connectionStringProvider)
        {
            _logger = logger;
        }

        private int DefaultPriorityKey(CrmOrganizationServiceContext service)
        {
            return CrmOptionValuesHelper.GetOptionSetDictionary(
                        smt_neededresources.EntityLogicalName, "smt_priority", service).First().Key;
        }

        private int DefaultBusinessUnitKey(CrmOrganizationServiceContext service)
        {
            return CrmOptionValuesHelper.GetOptionSetDictionary(
                        smt_neededresources.EntityLogicalName, "smt_businessunit", service)
                        .First(bu => bu.Value == "TA").Key;
        }

        public Guid CreateNeededResource(UpdateNeededResourceDto dto)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var resourceKey = Guid.NewGuid();

                    var contactClient = new EntityReference(smt_neededresources.EntityLogicalName,
                        dto.ContactClientGuid);
                    var account = new EntityReference(smt_neededresources.EntityLogicalName, dto.AccountGuid);

                    var neededResource = new smt_neededresources
                    {
                        Id = resourceKey,
                        smt_name = dto.ResourceName,
                        smt_BusinessUnit = new OptionSetValue(DefaultBusinessUnitKey(context)),
                        smt_technologydepartmentid = new EntityReference(smt_neededresources.EntityLogicalName, dto.TechnologyDepartmentGuid),
                        smt_Region = new EntityReference(smt_neededresources.EntityLogicalName, dto.RegionGuid),
                        smt_Priority = new OptionSetValue(DefaultPriorityKey(context)),
                        smt_ContactClient = contactClient,
                        smt_Account = account,
                        smt_CustomerRate = dto.Rate,
                        smt_Seniority1 = dto.Seniority1,
                        smt_Seniority2 = dto.Seniority2,
                        smt_Seniority3 = dto.Seniority3,
                        smt_Seniority4 = dto.Seniority4,
                        smt_Seniority5 = dto.Seniority5,
                        smt_SpecificDuties = dto.ProjectDescription,
                        smt_RecruitmentProcessInformation = dto.RecruitmentProcess,
                        smt_NumberofPeople = dto.NumberOfPeople,
                        smt_Requirementsmust = dto.Requirements,
                        smt_Requirementsnicetohave = dto.RoleDescription,
                        smt_Comments = dto.AdditionalComment,
                    };

                    if (dto.LanguageRequirements.Any())
                    {
                        neededResource.smt_smt_neededresources_new_languageskill =
                            dto.LanguageRequirements.Select(l => new new_languageskill
                            {
                                smt_LanguageLevel = new OptionSetValue(l.LevelId),
                                new_Language = new OptionSetValue(l.LanguageId),
                                smt_Comment = dto.LanguageComment
                            }).ToList();
                    }

                    var operationManagerId = GetCrmOperationManagerId(dto, context);
                    if (operationManagerId.HasValue)
                    {
                        neededResource.OwnerId = new EntityReference(SystemUser.EntityLogicalName, operationManagerId.Value);
                    }

                    context.Create(neededResource);
                    context.SaveChanges();

                    return neededResource.Id;
                }
            }
        }

        private Guid? GetCrmOperationManagerId(UpdateNeededResourceDto dto, CrmOrganizationServiceContext context)
        {
            var crmOperationManagerIds = context.CreateQuery<SystemUser>()
                .Where(u => u.FirstName == dto.OperationManager.FirstName && u.LastName == dto.OperationManager.LastName)
                .Select(u => u.Id)
                .ToList();

            if (crmOperationManagerIds.Count != 1)
            {
                _logger.ErrorFormat(@"Cannot assign operation manager ({0} {1}) as owner of needed resource because expected one record but found {2}.",
                    dto.OperationManager.FirstName,
                    dto.OperationManager.LastName,
                    crmOperationManagerIds.Count);

                return null;
            }

            return crmOperationManagerIds.FirstOrDefault();
        }

        public IEnumerable<NeededResourceDto> LoadCustomerRecommendations(Guid clientId)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var clientNeededResources = context.CreateQuery<smt_neededresources>()
                        .Where(w => w.smt_ContactClient.Id == clientId)
                        .ToList();

                    return clientNeededResources
                        .Select(nr => new NeededResourceDto { Id = nr.Id, Name = nr.smt_name, CompanyName = nr.smt_Account.Name })
                        .ToList();
                }
            }
        }

        public IEnumerable<Guid> LoadRecruitmentActivitiesGuids(Guid neededResourceId)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    return context.CreateQuery<smt_recruitmentactivity>()
                        .Where(ra => ra.smt_NeededResourceId.Id == neededResourceId)
                        .Select(ra => ra.Id)
                        .ToList();
                }
            }
        }

        public RecruitmentActivitieDto LoadRecruitmentActivitie(Guid recruitmentActivitieGuid)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var activity = context.CreateQuery<smt_recruitmentactivity>()
                        .SingleOrDefault(ra => ra.Id == recruitmentActivitieGuid);

                    if (activity == null)
                    {
                        throw new BusinessException($"Can't find recruitment activity with Id={recruitmentActivitieGuid}");
                    }

                    return new RecruitmentActivitieDto { Name = activity.smt_Candidate.Name, CrmId = activity.Id };
                }
            }
        }

        public IEnumerable<string> ResolveRecruitmentActivitieDocumentUrls(Guid recruitmentActivitieGuid)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var recruitmentActivity = context.CreateQuery<smt_recruitmentactivity>()
                        .Single(ra => ra.Id == recruitmentActivitieGuid);

                    var documents = context.CreateQuery<SharePointDocumentLocation>()
                        .Where(spd => spd.smt_candidate_SharePointDocumentLocations.Id == recruitmentActivity.smt_Candidate.Id)
                        .ToList();

                    foreach (var sharePointDocumentLocation in documents)
                    {
                        var link = ResolveDocumentLocations(sharePointDocumentLocation, context);
                        if (link != null)
                        {
                            yield return $"{link.SharePointUrl}{link.SharePointDocPath}/{link.RelativeUrl}";
                        }
                    }
                }
            }
        }

        public NeededResourceDto LoadNeededResource(Guid neededResourceId)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var clientNeededResource = context.CreateQuery<smt_neededresources>()
                        .SingleOrDefault(w => w.Id == neededResourceId);

                    if (clientNeededResource == null)
                    {
                        throw new BusinessException($"Can't find needed resource with Id={neededResourceId}");
                    }

                    return new NeededResourceDto
                    {
                        Id = clientNeededResource.Id,
                        Name = clientNeededResource.smt_name,
                        CompanyName = clientNeededResource.smt_Account.Name
                    };
                }
            }
        }

        public IEnumerable<Guid> LoadCustomerNeededResourcesCodes(Guid clientId)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var clientNeededResources = context.CreateQuery<smt_neededresources>()
                        .Where(w => w.smt_ContactClient.Id == clientId)
                        .ToList();

                    return clientNeededResources
                        .Select(nr => nr.Id)
                        .ToList();
                }
            }
        }

        private SpDocumentLocationDto ResolveDocumentLocations(SharePointDocumentLocation documentLocation, CrmOrganizationServiceContext context)
        {
            var docLocation = new SpDocumentLocationDto();
            SharePointDocumentLocation parentLocation = context.CreateQuery<SharePointDocumentLocation>().FirstOrDefault(x => x.Id == documentLocation.ParentSiteOrLocation.Id);

            do
            {
                docLocation.SharePointDocPath = $"/{parentLocation?.RelativeUrl}" + docLocation.SharePointDocPath;
                if (parentLocation != null)
                {
                    var sharePointSite = context.CreateQuery<SharePointSite>().FirstOrDefault(x => x.Id == parentLocation.SiteCollectionId);
                    docLocation.SharePointUrl = $"{sharePointSite?.AbsoluteURL}";
                    parentLocation = context.CreateQuery<SharePointDocumentLocation>().FirstOrDefault(x => x.Id == parentLocation.ParentSiteOrLocation.Id);
                }
            }
            while (parentLocation != null);
            docLocation.RelativeUrl = documentLocation.RelativeUrl;

            return docLocation;
        }
    }
}
