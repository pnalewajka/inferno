using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Castle.Core.Logging;
using LinqKit;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Data.Crm.Services
{
    internal class CrmService : BaseCrmService, ICrmService
    {
        private readonly IClassMapping<smt_candidate, CrmCandidateDto> _crmToDtoCandidateMapping;
        private readonly ILogger _logger;

        public CrmService(ICrmConnectionStringProvider connectionStringProvider,
                          IClassMapping<smt_candidate, CrmCandidateDto> crmToDtoCandidateMapping,
                          ILogger logger) : base(connectionStringProvider)
        {
            _crmToDtoCandidateMapping = crmToDtoCandidateMapping;
            _logger = logger;
        }

        public CrmCandidateDto GetCandidate(Guid recruitmentActivityId, bool loadCandidateContacts = false)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException(Resources.Exceptions.EmptyRecruitmentActivityIdIsNotAllowed, nameof(recruitmentActivityId));
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var recruitmentActivity = service.Retrieve(smt_recruitmentactivity.EntityLogicalName, recruitmentActivityId,
                        new ColumnSet(RecruitmentActivityCandidateColumnName, NeededResourceIdColumnName)) as smt_recruitmentactivity;
                    if (recruitmentActivity == null)
                    {
                        throw new InvalidOperationException(string.Format(Resources.Exceptions.RecruitmentActivityWithIdCouldNotBeFound, recruitmentActivityId));
                    }

                    if (recruitmentActivity.smt_Candidate == null)
                    {
                        throw new InvalidOperationException(string.Format(Resources.Exceptions.CandidateUnderRecruitmentActivityWithIdCouldNotBeFound, recruitmentActivityId));
                    }

                    var candidateId = recruitmentActivity.smt_Candidate.Id;
                    var candidate = context.CreateQuery<smt_candidate>().FirstOrDefault(c => c.Id == candidateId);
                    var candidateDto = _crmToDtoCandidateMapping.CreateFromSource(candidate);
                    candidateDto.DocumentLocations = new List<SpDocumentLocationDto>();

                    var docLocations = context.CreateQuery<SharePointDocumentLocation>().Where(x => x.smt_candidate_SharePointDocumentLocations.Id == candidateId).ToList();

                    foreach (var location in docLocations)
                    {
                        var docLocation = new SpDocumentLocationDto();
                        SharePointDocumentLocation parentLocation = context.CreateQuery<SharePointDocumentLocation>().FirstOrDefault(x => x.Id == location.ParentSiteOrLocation.Id);

                        do
                        {
                            docLocation.SharePointDocPath = $"/{parentLocation?.RelativeUrl}" + docLocation.SharePointDocPath;

                            if (parentLocation != null)
                            {
                                var sharePointSite = context.CreateQuery<SharePointSite>().FirstOrDefault(x => x.Id == parentLocation.SiteCollectionId);
                                docLocation.SharePointUrl = $"{sharePointSite?.AbsoluteURL}";
                                parentLocation = context.CreateQuery<SharePointDocumentLocation>().FirstOrDefault(x => x.Id == parentLocation.ParentSiteOrLocation.Id);
                            }
                        }
                        while (parentLocation != null);

                        docLocation.RelativeUrl = location.RelativeUrl;
                        candidateDto.DocumentLocations.Add(docLocation);
                    }

                    if (loadCandidateContacts)
                    {
                        UpdateCandidateContacts(context, candidateDto, recruitmentActivity.smt_NeededResourceId.Id);
                    }

                    return candidateDto;
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingCandidate(ex, recruitmentActivityId, loadCandidateContacts);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingCandidate(ex, recruitmentActivityId, loadCandidateContacts);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingCandidate(ex, recruitmentActivityId, loadCandidateContacts);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
        }

        public IEnumerable<CrmCandidateDto> GetCandidates(DateTime? dateFrom = null, int pageSize = 1000)
        {
            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var candidates = context.CreateQuery<smt_candidate>();

                    if (dateFrom.HasValue)
                    {
                        candidates = candidates
                            .Where(d => d.ModifiedOn.Value >= dateFrom);
                    }

                    var result = candidates
                        .OrderBy(d => d.ModifiedOn)
                        .Take(pageSize)
                        .Select(_crmToDtoCandidateMapping.Mapping);

                    return result.ToArray();
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingCandidates(ex, dateFrom, pageSize);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingCandidates(ex, dateFrom, pageSize);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingCandidates(ex, dateFrom, pageSize);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
        }

        public IEnumerable<Guid> GetGuidsOfCandidatesToContactOn(DateTime contactOn)
        {
            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);

                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var candidates = context.CreateQuery<smt_candidate>();

                    candidates = candidates
                        .Where(c => c.statecode == smt_candidateState.Active && c.smt_prefferedcontactdate != null);

                    var result = candidates
                        .OrderBy(c => c.ModifiedOn)
                        .AsEnumerable();

                    var guidIds = result
                        .Where(c => c.smt_prefferedcontactdate.Value.ToLocalTime() == contactOn)
                        .Select(c => c.Id)
                        .AsEnumerable();

                    return guidIds;
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingCandidates(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingCandidates(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingCandidates(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
        }

        public Task<CrmCandidateDto> GetCandidateAsync(Guid recruitmentActivityId, bool loadCandidateContacts = false)
        {
            return Task<CrmCandidateDto>.Factory.StartNew(() => GetCandidate(recruitmentActivityId, loadCandidateContacts));
        }

        public Task<ClientContactDto[]> GetClientContactsByRecruitmentActivityIdsAsync(Guid[] recruitmentActivityIds)
        {
            return Task<ClientContactDto[]>.Factory.StartNew(() => GetClientContactsByRecruitmentActivityIds(recruitmentActivityIds));
        }

        public IReadOnlyDictionary<Guid, NeededResourceDto> GetNeededResourcesByRecruitmentActivityIds(Guid[] recruitmentActivityIds)
        {
            if (recruitmentActivityIds == null)
            {
                throw new ArgumentNullException(nameof(recruitmentActivityIds));
            }

            if (!recruitmentActivityIds.Any())
            {
                return new Dictionary<Guid, NeededResourceDto>();
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var neededResourcePredicate = GetPredicate<smt_recruitmentactivity>(recruitmentActivityIds);
                    var neededResources = context.CreateQuery<smt_recruitmentactivity>()
                        .AsExpandable()
                        .Where(neededResourcePredicate)
                        .Select(r =>
                            new
                            {
                                RecruitmentActivityId = r.Id,
                                NeededResource = new NeededResourceDto
                                {
                                    Id = r.smt_NeededResourceId.Id,
                                    Name = r.smt_NeededResourceId.Name
                                }
                            })
                        .ToDictionary(key => key.RecruitmentActivityId, val => val.NeededResource);

                    return neededResources;
                }
            }
            catch (Exception ex)
            {
                LogErrorWhileGettingNeededResourcesByRecruitmentActivityIds(ex, recruitmentActivityIds);

                throw new CrmException(Resources.Exceptions.NeededResourcesCouldNotBeFound, ex);
            }
        }

        public ClientContactDto[] GetClientContactsByRecruitmentActivityIds(Guid[] recruitmentActivityIds)
        {
            if (recruitmentActivityIds == null)
            {
                throw new ArgumentNullException(nameof(recruitmentActivityIds));
            }

            if (!recruitmentActivityIds.Any())
            {
                return Array.Empty<ClientContactDto>();
            }

            var contactPersons = Array.Empty<ClientContactDto>();

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var neededResourcePredicate = GetPredicate<smt_recruitmentactivity>(recruitmentActivityIds);
                    var neededResources =
                        context.CreateQuery<smt_recruitmentactivity>().AsExpandable()
                               .Where(neededResourcePredicate)
                               .Where(r => r.smt_NeededResourceId != null)
                               .Select(r => new { RecruitmentActivityId = r.Id, NeededResourceId = r.smt_NeededResourceId.Id })
                               .ToArray();

                    var neededResourceIds = neededResources.Select(n => n.NeededResourceId).Distinct().ToArray();
                    if (neededResourceIds.Length == 0)
                    {
                        return contactPersons;
                    }

                    var clientsPredicate = GetPredicate<smt_neededresources>(neededResourceIds);
                    var clients = context.CreateQuery<smt_neededresources>().AsExpandable().Where(clientsPredicate)
                            .Where(n => n.smt_Account != null)
                            .Select(n => new { NeededResourceId = n.Id, ClientContactId = n.smt_ContactClient.Id, CompanyName = n.smt_Account.Name })
                            .ToArray();

                    var clientIds = clients.Select(c => c.ClientContactId).Distinct().ToArray();
                    if (clientIds.Length == 0)
                    {
                        return contactPersons;
                    }

                    var contactsPredicate = GetPredicate<Contact>(clientIds);
                    var contacts = context.CreateQuery<Contact>().AsExpandable().Where(contactsPredicate)
                            .Select(c => new { ClientId = c.Id, Name = c.FullName, c.FirstName, c.LastName, Email = c.EMailAddress1 })
                            .ToArray();

                    var query = from contact in contacts
                        join client in clients on contact.ClientId equals client.ClientContactId
                        join neededResource in neededResources on client.NeededResourceId equals neededResource.NeededResourceId
                        join activity in recruitmentActivityIds on neededResource.RecruitmentActivityId equals activity
                        select new ClientContactDto(activity, contact.ClientId, contact.Name, contact.FirstName, contact.LastName, contact.Email, client.CompanyName);

                    contactPersons = query.ToArray();
                    return contactPersons;
                }
            }
            catch (Exception ex)
            {
                LogErrorWhileGettingClientContactsByRecruitmentActivityIds(ex, recruitmentActivityIds);

                throw new CrmException(Resources.Exceptions.ContactClientsCouldNotBeFound, ex);
            }
        }

        public CandidateActivityDto[] GetCandidatesByNeededResourceIds(Guid[] neededResourceIds)
        {
            if (neededResourceIds == null)
            {
                throw new ArgumentNullException(nameof(neededResourceIds));
            }

            if (!neededResourceIds.Any())
            {
                return Array.Empty<CandidateActivityDto>();
            }

            try
            {
                var candidates = new List<CandidateActivityDto>();
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var nrPredicate = GetPredicate<smt_neededresources>(neededResourceIds);
                    var neededResources = context.CreateQuery<smt_neededresources>().AsExpandable().Where(nrPredicate)
                            .ToArray();

                    foreach (var neededResrc in neededResources)
                    {
                        var activities = context.CreateQuery<smt_recruitmentactivity>()
                                        .AsExpandable()
                                        .Where(ra => ra.smt_NeededResourceId.Id == neededResrc.Id
                                                    && ra.smt_DateIntroducedtotheClient != null
                                                    && ra.smt_DateIntroducedtotheClient.Value > DateTime.Now.AddDays(MaxPassedTimeForOutsidedRecommendations)).ToList();

                        candidates.AddRange(from activity in activities
                            let candidateId = activity.smt_Candidate.Id
                            let candidate = context.CreateQuery<smt_candidate>().FirstOrDefault(c => c.Id == candidateId)
                            where candidate != null
                            select new CandidateActivityDto
                            {
                                FirstName = candidate.new_FirstName,
                                LastName = candidate.smt_LastName2,
                                FullName = candidate.smt_lastname,
                                ActivityId = activity.Id,
                                DateIntroducedToTheClient = activity.smt_DateIntroducedtotheClient,
                                IsRejected = activity.smt_Clientdecision?.Equals(new OptionSetValue(ClientDecisionRejected)) ?? false,
                                DateOfClientDecision = activity.smt_Dateofclientdecision,
                                NeededResourceActive = neededResrc.statecode == smt_neededresourcesState.Active,
                                NeededResourceId = neededResrc.Id
                            });
                    }

                    return candidates.ToArray();
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogErrorWhileGettingCandidatesByNeededResourceIds(ex, neededResourceIds);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogErrorWhileGettingCandidatesByNeededResourceIds(ex, neededResourceIds);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogErrorWhileGettingCandidatesByNeededResourceIds(ex, neededResourceIds);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
        }

        public void AddRecruitmentActivityNote(Guid recruitmentActivityId, string subject, string text)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException(Resources.Exceptions.EmptyRecruitmentActivityIdIsNotAllowed, nameof(recruitmentActivityId));
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                {
                    var annotation = new Annotation
                    {
                        Subject = subject,
                        NoteText = text,
                        ObjectTypeCode = "smt_recruitmentactivity",
                        ObjectId = new EntityReference(smt_recruitmentactivity.EntityLogicalName, recruitmentActivityId)
                    };
                    service.Create(annotation);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogErrorWhileAddingRecruitmentActivityNote(ex, recruitmentActivityId, subject, text);

                throw new CrmException(Resources.Exceptions.RecruitmentActivityNoteCannotBeAddedBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogErrorWhileAddingRecruitmentActivityNote(ex, recruitmentActivityId, subject, text);

                throw new CrmException(Resources.Exceptions.RecruitmentActivityNoteCannotBeAddedBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogErrorWhileAddingRecruitmentActivityNote(ex, recruitmentActivityId, subject, text);

                throw new CrmException(Resources.Exceptions.RecruitmentActivityNoteCannotBeAdded, ex);
            }
        }

        public void UpdateRecruitmentActivityWithIntroductionDate(Guid recruitmentActivityId, DateTime date)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException(Resources.Exceptions.EmptyRecruitmentActivityIdIsNotAllowed, nameof(recruitmentActivityId));
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                {
                    var recruitmentActivity = service.Retrieve(smt_recruitmentactivity.EntityLogicalName, recruitmentActivityId, new ColumnSet()) as smt_recruitmentactivity;

                    if (recruitmentActivity == null)
                    {
                        throw new InvalidOperationException(string.Format(Resources.Exceptions.RecruitmentActivityWithIdCouldNotBeFound, recruitmentActivityId));
                    }

                    recruitmentActivity.smt_DateIntroducedtotheClient = date;
                    service.Update(recruitmentActivity);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogErrorWhileUpdatingRecruitmentActivityWithIntroductionDate(ex, recruitmentActivityId, date);

                throw new CrmException(Resources.Exceptions.DateIntroducedToTheClientRecruitmentActivityCannotBeSavedBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogErrorWhileUpdatingRecruitmentActivityWithIntroductionDate(ex, recruitmentActivityId, date);

                throw new CrmException(Resources.Exceptions.DateIntroducedToTheClientRecruitmentActivityCannotBeSavedBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogErrorWhileUpdatingRecruitmentActivityWithIntroductionDate(ex, recruitmentActivityId, date);

                throw new CrmException(Resources.Exceptions.DateIntroducedToTheClientRecruitmentActivityCannotBeSaved, ex);
            }
        }

        public void UpdateCandidateContactDateAlert(IEnumerable<Guid> candidateIds)
        {
            foreach (var id in candidateIds)
            {
                if (id == Guid.Empty)
                {
                    throw new ArgumentException(Resources.Exceptions.EmptyCandidateIdIsNotAllowed, nameof(id));
                }

                try
                {
                    var connection = CrmConnection.Parse(ConnectitionString);
                    using (var service = new OrganizationService(connection))
                    {
                        var smt_candidate = service.Retrieve(CrmModel.smt_candidate.EntityLogicalName, id, new ColumnSet()) as smt_candidate;

                        if (smt_candidate == null)
                        {
                            throw new InvalidOperationException(string.Format(Resources.Exceptions.CandidateWithIdCouldNotBeFound, id));
                        }

                        smt_candidate.smt_candidatecontactdatealert = true;
                        service.Update(smt_candidate);
                    }
                }
                catch (Exception ex)
                {
                    LogErrorWhileUpdatingCandidatePreferedDateContactAlert(ex, id);

                    throw new CrmException(Resources.Exceptions.CandidateContactDateAlertCannotBeSaved, ex);
                }
            }
        }

        public void UpdateRecruitmentActivityClientDecision(Guid recruitmentActivityId, DateTime date, bool isRejected, string reason)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException(Resources.Exceptions.EmptyRecruitmentActivityIdIsNotAllowed, nameof(recruitmentActivityId));
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var recruitmentActivity = context.CreateQuery<smt_recruitmentactivity>().SingleOrDefault(r => r.Id == recruitmentActivityId);

                    if (recruitmentActivity == null)
                    {
                        throw new InvalidOperationException(string.Format(Resources.Exceptions.RecruitmentActivityWithIdCouldNotBeFound, recruitmentActivityId));
                    }

                    recruitmentActivity.smt_ClientComment = reason;
                    recruitmentActivity.smt_Clientdecision = new OptionSetValue(isRejected ? ClientDecisionRejected : ClientDecisionApproved);
                    recruitmentActivity.smt_Dateofclientdecision = date;

                    if (isRejected)
                    {
                        recruitmentActivity.smt_ClientCVRejectionReason = new OptionSetValue(ClientCvRejectionReasonNotAvailable);
                    }

                    context.UpdateObject(recruitmentActivity);
                    context.SaveChanges();
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogErrorWhileUpdatingRecruitmentActivityClientDecision(ex, recruitmentActivityId, date, isRejected, reason);

                throw new CrmException(Resources.Exceptions.RecruitmentActivityClientDecisionCannotBeSavedBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogErrorWhileUpdatingRecruitmentActivityClientDecision(ex, recruitmentActivityId, date, isRejected, reason);

                throw new CrmException(Resources.Exceptions.RecruitmentActivityClientDecisionCannotBeSavedBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogErrorWhileUpdatingRecruitmentActivityClientDecision(ex, recruitmentActivityId, date, isRejected, reason);

                throw new CrmException(Resources.Exceptions.RecruitmentActivityClientDecisionCannotBeSaved, ex);
            }
        }

        public ClientContactDto GetClientContactInfo(Guid clientContactId)
        {
            if (clientContactId == Guid.Empty)
            {
                throw new ArgumentException(nameof(clientContactId));
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var clientContact = context.CreateQuery<Contact>().AsExpandable().FirstOrDefault(c => c.Id == clientContactId);
                    if (clientContact == null)
                    {
                        return null;
                    }

                    var neededResources =
                        context.CreateQuery<smt_neededresources>()
                            .AsExpandable()
                            .Where(nr => nr.smt_ContactClient != null && nr.smt_ContactClient.Id == clientContactId && nr.smt_Account != null)
                            .Select(nr => new NeededResourceDto { Id = nr.Id, Name = nr.smt_name, CompanyName = nr.smt_Account.Name })
                                            .ToList();
                    return new ClientContactDto(clientContactId,
                                                clientContact.FullName,
                                                clientContact.FirstName,
                                                clientContact.LastName,
                                                clientContact.EMailAddress1,
                                                neededResources.Select(n => n.CompanyName).FirstOrDefault(),
                                                neededResources);
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogErrorWhileGettingClientContactInfo(ex, clientContactId);

                throw new CrmException(
                    string.Format(Resources.Exceptions.ClientContactIdCompanyNameCouldNotBeFoundBecauseOfOrganizationServiceFault, clientContactId), ex);
            }
            catch (TimeoutException ex)
            {
                LogErrorWhileGettingClientContactInfo(ex, clientContactId);

                throw new CrmException(string.Format(Resources.Exceptions.ClientContactIdCompanyNameCouldNotBeFoundBecauseOfTimeout, clientContactId), ex);
            }
            catch (Exception ex)
            {
                LogErrorWhileGettingClientContactInfo(ex, clientContactId);

                throw new CrmException(string.Format(Resources.Exceptions.ClientContactIdCompanyNameCouldNotBeFound, clientContactId), ex);
            }
        }

        public NeededResourceOwnerDto GetRecruitmentActivityOwner(Guid recruitmentActivityId)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException(nameof(recruitmentActivityId));
            }

            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var recruitmentActivity = context.CreateQuery<smt_recruitmentactivity>().FirstOrDefault(c => c.Id == recruitmentActivityId);

                    if (recruitmentActivity == null)
                    {
                        return new NeededResourceOwnerDto();
                    }

                    var owner = recruitmentActivity.OwnerId;
                    var neededResourceOwner = context.CreateQuery<SystemUser>().FirstOrDefault(c => c.Id == owner.Id);

                    return new NeededResourceOwnerDto
                    {
                        NeededResourceId = recruitmentActivity.smt_NeededResourceId.Id,
                        Name = neededResourceOwner?.FullName,
                        Email = neededResourceOwner?.InternalEMailAddress,
                        Phone = neededResourceOwner?.MobilePhone
                    };
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogErrorWhileGettingRecruitmentActivityOwner(ex, recruitmentActivityId);

                throw new CrmException(
                    string.Format(Resources.Exceptions.RecruitmentActivityOwnerCouldNotBeFoundBecauseOfOrganizationServiceFault, recruitmentActivityId), ex);
            }
            catch (TimeoutException ex)
            {
                LogErrorWhileGettingRecruitmentActivityOwner(ex, recruitmentActivityId);

                throw new CrmException(string.Format(Resources.Exceptions.RecruitmentActivityOwnerCouldNotBeFoundBecauseOfTimeout, recruitmentActivityId), ex);
            }
            catch (Exception ex)
            {
                LogErrorWhileGettingRecruitmentActivityOwner(ex, recruitmentActivityId);

                throw new CrmException(string.Format(Resources.Exceptions.RecruitmentActivityOwnerCouldNotBeFound, recruitmentActivityId), ex);
            }
        }

        public string RecruitmentActivityUrl(Guid recruitmentId)
        {
            return $"{ConnectionStringProvider.GetUrl()}/main.aspx?etn={smt_recruitmentactivity.EntityLogicalName}&id={recruitmentId.ToString("B")}&pagetype=entityrecord";
        }

        private static void UpdateCandidateContacts(OrganizationServiceContext context, CrmCandidateDto candidateDto, Guid neededResourceId)
        {
            var neededResource = context.CreateQuery<smt_neededresources>().FirstOrDefault(c => c.Id == neededResourceId);
            candidateDto.NeededResourceName = neededResource?.smt_name;

            var neededResourceOwner = context.CreateQuery<SystemUser>().FirstOrDefault(c => c.Id == neededResource.OwnerId.Id);
            candidateDto.SdmName = neededResourceOwner?.FullName;
            candidateDto.SdmFirstName = neededResourceOwner?.FirstName;
            candidateDto.SdmEmail = neededResourceOwner?.InternalEMailAddress;

            var clientContact = context.CreateQuery<Contact>().FirstOrDefault(c => c.Id == neededResource.smt_ContactClient.Id);
            candidateDto.ContactName = clientContact?.FullName;
            candidateDto.ContactFirstName = clientContact?.FirstName;
            candidateDto.ContactEmail = clientContact?.EMailAddress1;
        }

        private ExpressionStarter<T> GetPredicate<T>(Guid[] guids) where T : Entity
        {
            if (guids == null)
            {
                throw new ArgumentNullException(nameof(guids));
            }

            var predicate = PredicateBuilder.New<T>(false);
            predicate = guids.Aggregate(predicate, (current, tempGuid) => current.Or(t => t.Id == tempGuid));

            return predicate;
        }

        private void LogExceptionWhileGettingCandidate(Exception ex, Guid recruitmentActivityId, bool loadCandidateContacts)
        {
            _logger.Error($"Error while getting candidate. recruitmentActivityId = {recruitmentActivityId}, loadCandidateContacts = {loadCandidateContacts}", ex);
        }

        private void LogExceptionWhileGettingCandidates(Exception ex)
        {
            _logger.Error($"Error while getting candidates.", ex);
        }

        private void LogExceptionWhileGettingCandidates(Exception ex, DateTime? dateFrom, int pageSize)
        {
            _logger.Error($"Error while getting candidates. dateFrom = {dateFrom}, pageSize = {pageSize}", ex);
        }

        private void LogErrorWhileGettingNeededResourcesByRecruitmentActivityIds(Exception ex, Guid[] recruitmentActivityIds)
        {
            _logger.Error($"Error while getting needed resources by recruitment activity ids. Ids = {string.Join(", ", recruitmentActivityIds)}", ex);
        }

        private void LogErrorWhileGettingClientContactsByRecruitmentActivityIds(Exception ex, Guid[] recruitmentActivityIds)
        {
            _logger.Error($"Error while getting client contacts by recruitment activity ids. Ids = {string.Join(", ", recruitmentActivityIds)}", ex);
        }

        private void LogErrorWhileGettingCandidatesByNeededResourceIds(Exception ex, Guid[] neededResourceIds)
        {
            _logger.Error($"Error while getting candidates by needed resource ids. Ids = {string.Join(", ", neededResourceIds)}", ex);
        }

        private void LogErrorWhileAddingRecruitmentActivityNote(Exception ex, Guid recruitmentActivityId, string subject, string text)
        {
            _logger.Error($"Error while adding recruitment activity note. recruitmentActivityId = {recruitmentActivityId}, subject = {subject}, text = {text}", ex);
        }

        private void LogErrorWhileUpdatingRecruitmentActivityWithIntroductionDate(Exception ex, Guid recruitmentActivityId, DateTime date)
        {
            _logger.Error($"Error while updating recruitment activity with introduction date. recruitmentActivityId = {recruitmentActivityId}, date = {date}", ex);
        }

        private void LogErrorWhileUpdatingRecruitmentActivityClientDecision(Exception ex, Guid recruitmentActivityId, DateTime date, bool isRejected, string reason)
        {
            _logger.Error($"Error while updating recruitment activity client desicion. recruitmentActivityId = {recruitmentActivityId}, date = {date}, isRejected = {isRejected}, reason = {reason}", ex);
        }

        private void LogErrorWhileGettingClientContactInfo(Exception ex, Guid clientContactId)
        {
            _logger.Error($"Error while getting client contact info. clientContactId = {clientContactId}", ex);
        }

        private void LogErrorWhileGettingRecruitmentActivityOwner(Exception ex, Guid recruitmentActivityId)
        {
            _logger.Error($"Error while getting recruitment activity owner. recruitmentActivityId = {recruitmentActivityId}", ex);
        }

        private void LogErrorWhileUpdatingCandidatePreferedDateContactAlert(Exception ex, Guid candidateId)
        {
            _logger.Error($"Error while updating candidate candidate prefered date contact alert. candidateId = {candidateId}", ex);
        }
    }
}
