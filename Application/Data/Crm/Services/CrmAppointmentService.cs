﻿using System;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Enums;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Crm.Resources;

namespace Smt.Atomic.Data.Crm.Services
{
    internal class CrmAppointmentService : BaseCrmService, ICrmAppointmentService
    {
        public CrmAppointmentService(ICrmConnectionStringProvider connectionStringProvider)
            : base(connectionStringProvider)
        {
        }

        public void NotifyAppointmentService(Guid resourceId, DateTime startDateTime, DateTime endDateTime)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var interviewName = string.Format(CrmResources.ScheduleInterviewAppointmentFormat, startDateTime, endDateTime);

                    var rc =
                        context.Retrieve(smt_recruitmentactivity.EntityLogicalName, resourceId, new ColumnSet()) as
                            smt_recruitmentactivity;

                    if (rc == null)
                    {
                        throw new BusinessException($"Can't schedule appointment for recruitment {resourceId}");
                    }

                    rc.smt_interviewneeded = true;
                    rc.smt_dateinterview = startDateTime;
                    rc.smt_InterviewType = new OptionSetValue((int)CrmInterviewTypeEnum.FaceToFace);
                    rc.smt_InterviewDetails = interviewName;

                    context.Update(rc);

                    context.Create(new Appointment
                    {
                        Subject = CrmResources.ScheduleInterviewAppointment,
                        RegardingObjectId =
                            new EntityReference(smt_candidate.EntityLogicalName, resourceId),
                        ScheduledStart = startDateTime,
                        ScheduledEnd = endDateTime,
                        ScheduledDurationMinutes = (int)(endDateTime - startDateTime).TotalMinutes,
                        Description = interviewName,
                        IsAllDayEvent = false
                    });

                    context.SaveChanges();
                }
            }
        }
    }
}
