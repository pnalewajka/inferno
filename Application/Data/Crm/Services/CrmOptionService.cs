﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Helpers;
using Smt.Atomic.Data.Crm.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Data.Crm.Services
{
    internal class CrmOptionService : BaseCrmService, ICrmOptionService
    {
        public CrmOptionService(ICrmConnectionStringProvider connectionStringProvider)
            : base(connectionStringProvider)
        { }

        public IEnumerable<CrmDictionaryDto> GetMainTechnologyOptions()
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var technologyOptions =
                        CrmOptionValuesHelper.GetEntires<smt_technologydepartment>(
                            smt_technologydepartment.EntityLogicalName
                        , context);


                    return technologyOptions.Select(t => new CrmDictionaryDto
                    {
                        Id = t.Key,
                        Name = t.Value.smt_name
                    });
                }
            }
        }

        public IEnumerable<CrmDictionaryDto> GetRegionOption()
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var technologyOptions =
                        CrmOptionValuesHelper.GetEntires<Territory>(
                            Territory.EntityLogicalName
                        , context);


                    return technologyOptions.Select(t => new CrmDictionaryDto
                    {
                        Id = t.Key,
                        Name = t.Value.Name
                    });
                }
            }
        }

        public IEnumerable<CrmBusinessUnitOptionDto> GetBusinessUnitOptions()
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var technologyOptions =
                        CrmOptionValuesHelper.GetOptionSetDictionary(
                            smt_neededresources.EntityLogicalName, "smt_businessunit"
                        , context);


                    return technologyOptions.Select(t => new CrmBusinessUnitOptionDto
                    {
                        Id = t.Key,
                        Name = t.Value
                    });
                }
            }
        }

        public IEnumerable<CrmDictionaryDto> ClientAccounts(DateTime? createdAfter)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var accounts = context.CreateQuery<Account>()
                        .ToList()
                        .Where(a => !createdAfter.HasValue || a.CreatedOn > createdAfter)
                        .ToList();

                    return accounts.Select(a => new CrmDictionaryDto { Id = a.Id, Name = a.Name });
                }
            }
        }

        public IEnumerable<CrmDictionaryDto> ClientContacts(DateTime? createdAfter)
        {
            var connection = CrmConnection.Parse(ConnectitionString);
            using (var service = new OrganizationService(connection))
            {
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var accounts = context.CreateQuery<Contact>()
                        .ToList()
                        .Where(c => !string.IsNullOrEmpty(c.EMailAddress1) && c.ParentCustomerId != null)
                        .Where(a => !createdAfter.HasValue || a.CreatedOn > createdAfter)
                        .ToList();

                    return accounts.Select(a => new CrmDictionaryDto
                    {
                        Id = a.Id,
                        Name = a.FullName,
                        Email = a.EMailAddress1,
                        CompanyGuidId = a.ParentCustomerId.Id,
                        CompanyName = a.ParentCustomerId.Name
                    });
                }
            }
        }
    }
}
