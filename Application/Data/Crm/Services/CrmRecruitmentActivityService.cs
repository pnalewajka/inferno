﻿using System;
using System.Linq;
using System.ServiceModel;
using Castle.Core.Logging;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Data.Crm.Services
{
    internal class CrmRecruitmentActivityService : BaseCrmService, ICrmRecruitmentActivityService
    {
        private readonly ILogger _logger;
        private readonly ITimeService _timeService;
        private readonly string _recommendationStageName = "Recommendation";
        private readonly string _waitingForDecisionStageName = "Waiting for decision";
        private readonly int _everyNdays = 3;
        private readonly int _remainder = 0;

        public CrmRecruitmentActivityService(ICrmConnectionStringProvider connectionStringProvider,
            ILogger logger,
            ITimeService timeService) : base(connectionStringProvider)
        {
            _logger = logger;
            _timeService = timeService;
        }

        public void TriggerEmailAlertToCreateReminderForOwnerRA()
        {
            try
            {
                var connection = CrmConnection.Parse(ConnectitionString);
                using (var service = new OrganizationService(connection))
                using (var context = new CrmOrganizationServiceContext(service))
                {
                    var currentDate = _timeService.GetCurrentDate();

                    var needResourcesIds = context.CreateQuery<smt_neededresources>()
                            .Where(n => n.statecode.Value == smt_neededresourcesState.Active)
                            .Select(n => n.Id)
                            .ToList();

                    var recruitmentActivities = context.CreateQuery<smt_recruitmentactivity>()
                        .Where(r => (r.smt_stagename == _recommendationStageName || r.smt_stagename == _waitingForDecisionStageName)
                            && r.smt_DateRecommended != null
                            && r.statecode.Value == smt_recruitmentactivityState.Active
                            && r.smt_NeededResourceId != null)
                        .ToList();

                    recruitmentActivities = recruitmentActivities
                        .Where(r => needResourcesIds.Any(n => n == r.smt_NeededResourceId.Id)
                            && r.smt_DateRecommended.Value.ToLocalTime().Subtract(currentDate).Days % _everyNdays == _remainder)
                        .ToList();

                    foreach (var result in recruitmentActivities)
                    {
                        var recruitmentActivity = new smt_recruitmentactivity
                        {
                            Id = result.Id,
                            smt_triggerreminderaboutecommendation = true
                        };

                        service.Update(recruitmentActivity);
                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);
                throw new CrmException(Resources.Exceptions.RecruitmentActiviteCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);
                throw new CrmException(Resources.Exceptions.RecruitmentActiviteCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);
                throw new CrmException(Resources.Exceptions.RecruitmentActiviteCouldNotBeFound, ex);
            }
        }

        private void LogExceptionWhileGettingRecruitmentActivities(Exception ex)
        {
            _logger.Error($"Error while getting recruitment activities.", ex);
        }
    }
}
