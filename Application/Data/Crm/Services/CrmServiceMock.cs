﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Data.Crm.Services
{
    public class CrmServiceMock : ICrmService
    {
        private Dictionary<Guid, Tuple<NeededResourceDto, List<Tuple<CrmCandidateDto, ClientContactDto>>>> _dummyData =
            new Dictionary<Guid, Tuple<NeededResourceDto, List<Tuple<CrmCandidateDto, ClientContactDto>>>>
        {
            [new Guid("37D3070D-6D73-E611-80F2-5065F38B4661")] = new Tuple<NeededResourceDto, List<Tuple<CrmCandidateDto, ClientContactDto>>>(
                new NeededResourceDto
                {
                    Id = new Guid("37D3070D-6D73-E611-80F2-5065F38B4661"),
                    CompanyName = "Credit Suisse",
                    Name = "CS/ Senior Dev Ops/Wrocław/Huk/ 21315"
                }, new List<Tuple<CrmCandidateDto, ClientContactDto>>
                {
                    new Tuple<CrmCandidateDto, ClientContactDto>(new CrmCandidateDto
                        {
                            FirstName = "Pascal",
                            LastName = "Brodnicki",
                            FullName = "Pascal Brodnicki",
                            ContactName = "Huk Krzysztof",
                            ContactFirstName = "Krzysztof",
                            ContactEmail = "ta-customer-portal-list@intive.com",
                            NeededResourceName = "CS/ Senior Dev Ops/Wrocław/Huk/ 21315",
                            SdmName = "intive OM",
                            SdmFirstName = "intive",
                            SdmEmail = "ta-customer-portal-list@intive.com",
                            DocumentLocations = new List<SpDocumentLocationDto>
                            {
                                new SpDocumentLocationDto
                                {
                                    SharePointUrl = "https://smtsoftwareservices.sharepoint.com/sites/smtcrm/",
                                    SharePointDocPath = "smt_candidate/B/Brodnicki Pascal/"
                                }
                            }
                        },
                        new ClientContactDto(
                            new Guid("DAE6E659-687A-E611-80F2-5065F38B4661"),
                            new Guid("e4a10f7e-ee2e-e611-811c-3863bb35ecd0"),
                            "Huk Krzysztof",
                            "Krzysztof",
                            "Huk",
                            "ta-customer-portal-list@intive.com",
                            "Credit Suisse")),
                    new Tuple<CrmCandidateDto, ClientContactDto>(new CrmCandidateDto
                        {
                            FirstName = "Viktor",
                            LastName = "Kralevski",
                            FullName = "Viktor Kralevski",
                            ContactName = "Huk Krzysztof",
                            ContactFirstName = "Krzysztof",
                            ContactEmail = "ta-customer-portal-list@intive.com",
                            NeededResourceName = "CS/ Senior Dev Ops/Wrocław/Huk/ 21315",
                            SdmName = "intive OM",
                            SdmFirstName = "intive",
                            SdmEmail = "ta-customer-portal-list@intive.com",
                            DocumentLocations = new List<SpDocumentLocationDto>
                            {
                                new SpDocumentLocationDto
                                {
                                    SharePointUrl = "https://smtsoftwareservices.sharepoint.com/sites/smtcrm/",
                                    SharePointDocPath = "smt_candidate/K/Kralevski Viktor/"
                                }
                            }
                        },
                        new ClientContactDto(
                            new Guid("B05488EF-FC73-E611-80ED-5065F38B46C1"),
                            new Guid("e4a10f7e-ee2e-e611-811c-3863bb35ecd0"),
                            "Huk Krzysztof",
                            "Krzysztof",
                            "Huk",
                            "ta-customer-portal-list@intive.com",
                            "Credit Suisse"))
                })
        };

        public CrmCandidateDto GetCandidate(Guid recruitmentActivityId, bool loadCandidateContacts = false)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException("Empty recruitment activity id is not allowed.", nameof(recruitmentActivityId));
            }

            return _dummyData.
                SelectMany(data => data.Value.Item2).
                Single(tuple => tuple.Item2.RecruitmentActivityId == recruitmentActivityId).
                Item1;
        }

        public IEnumerable<CrmCandidateDto> GetCandidates(DateTime? dateFrom, int pageSize)
        {
            return Enumerable.Empty<CrmCandidateDto>();
        }

        public Task<CrmCandidateDto> GetCandidateAsync(Guid recruitmentActivityId, bool loadCandidateContacts = false)
        {
            return Task<CrmCandidateDto>.Factory.StartNew(() => GetCandidate(recruitmentActivityId, loadCandidateContacts));
        }

        public Task<ClientContactDto[]> GetClientContactsByRecruitmentActivityIdsAsync(Guid[] recruitmentActivityIds)
        {
            return Task<ClientContactDto[]>.Factory.StartNew(() => GetClientContactsByRecruitmentActivityIds(recruitmentActivityIds));
        }

        public IReadOnlyDictionary<Guid, NeededResourceDto> GetNeededResourcesByRecruitmentActivityIds(Guid[] recruitmentActivityIds)
        {
            if (recruitmentActivityIds == null)
            {
                throw new ArgumentNullException(nameof(recruitmentActivityIds));
            }

            if (!recruitmentActivityIds.Any())
            {
                return new Dictionary<Guid, NeededResourceDto>();
            }

            return recruitmentActivityIds.
                ToDictionary(key => key, key =>
                    _dummyData.Single(d => d.Value.Item2.Any(t => t.Item2.RecruitmentActivityId == key)).Value.Item1);
        }

        public NeededResourceOwnerDto GetRecruitmentActivityOwner(Guid recruitmentActivityResourceId)
        {
            return new NeededResourceOwnerDto
            {
                Name = "Adam Kowalski",
                Email = "ta-customer-portal-list@intive.com",
                Phone = "666666666"
            };
        }

        public ClientContactDto[] GetClientContactsByRecruitmentActivityIds(Guid[] recruitmentActivityIds)
        {
            if (recruitmentActivityIds == null)
            {
                throw new ArgumentNullException(nameof(recruitmentActivityIds));
            }

            if (!recruitmentActivityIds.Any())
            {
                return Array.Empty<ClientContactDto>();
            }

            var contactPersons = Array.Empty<ClientContactDto>();

            return _dummyData.
                SelectMany(d => d.Value.Item2.Select(e => e.Item2)).
                Where(d => recruitmentActivityIds.Contains(d.RecruitmentActivityId)).
                ToArray();

        }

        public void AddRecruitmentActivityNote(Guid recruitmentActivityId, string subject, string text)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException("Empty recruitment activity id is not allowed.", nameof(recruitmentActivityId));
            }

            Debug.WriteLine($"Added note for recruitment activity: {recruitmentActivityId}{Environment.NewLine}subject: {subject}{Environment.NewLine}text: {text}");
        }

        public void UpdateRecruitmentActivityWithIntroductionDate(Guid recruitmentActivityId, DateTime date)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException("Empty recruitment activity id is not allowed.", nameof(recruitmentActivityId));
            }

            Debug.WriteLine($"Updated recruitment activity: {recruitmentActivityId}{Environment.NewLine}introduced to the client date: {date}");
        }

        public void UpdateRecruitmentActivityClientDecision(Guid recruitmentActivityId, DateTime date, bool isRejected, string reason)
        {
            if (recruitmentActivityId == Guid.Empty)
            {
                throw new ArgumentException("Empty recruitment activity id is not allowed.", nameof(recruitmentActivityId));
            }

            Debug.WriteLine($"Updated recruitment activity: {recruitmentActivityId}{Environment.NewLine}rejected: {isRejected}{Environment.NewLine}date: {date}{Environment.NewLine}reason: {reason}");
        }

        public ClientContactDto GetClientContactInfo(Guid clientContactId)
        {
            if (clientContactId == Guid.Empty)
            {
                throw new ArgumentException(nameof(clientContactId));
            }

            return _dummyData.SelectMany(d => d.Value.Item2.Select(e => e.Item2)).FirstOrDefault(c => c.ContactId == clientContactId);
        }

        public CandidateActivityDto[] GetCandidatesByNeededResourceIds(Guid[] neededResourceIds)
        {
            return new CandidateActivityDto[]
            {
                new CandidateActivityDto
                {
                    ActivityId = new Guid("B05488EF-FC73-E611-80ED-5065F38B46C1"),
                    DateIntroducedToTheClient = null,
                    DateOfClientDecision = null,
                    FirstName = "Krzysztof",
                    LastName = "Huk",
                    FullName = "Huk Krzystof",
                    IsRejected = false,
                    NeededResourceActive = true,
                    NeededResourceId = new Guid("37D3070D-6D73-E611-80F2-5065F38B4661")
                }
            };
        }

        public string RecruitmentActivityUrl(Guid recruitmentId)
        {
            return $"/{recruitmentId}";
        }

        public void UpdateCandidateContactDateAlert(IEnumerable<Guid> candidateIds)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Guid> GetGuidsOfCandidatesToContactOn(DateTime contactOn)
        {
            throw new NotImplementedException();
        }
    }
}
