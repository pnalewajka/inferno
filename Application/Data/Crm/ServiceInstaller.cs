﻿//#define MOCK_CRM

using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Crm.BusinessEventHandlers;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Crm.Services;

namespace Smt.Atomic.Data.Crm
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp || containerType == ContainerType.WebApi || containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ICrmConnectionStringProvider>().ImplementedBy<CrmConnectionStringProvider>().LifestyleTransient());

#if MOCK_CRM && DEBUG
                // For testing in times when CRM is not working
                container.Register(Component.For<ICrmService>().ImplementedBy<CrmServiceMock>().LifestyleTransient());
#else
                container.Register(Component.For<ICrmService>().ImplementedBy<CrmService>().LifestyleTransient());
#endif
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ICrmLanguageService>().ImplementedBy<CrmLanguageService>().LifestyleTransient());
                container.Register(Component.For<ICrmOptionService>().ImplementedBy<CrmOptionService>().LifestyleTransient());
                container.Register(Component.For<ICrmNeededResourcesService>().ImplementedBy<CrmNeededResourcesService>().LifestyleTransient());
                container.Register(Component.For<ICrmAppointmentService>().ImplementedBy<CrmAppointmentService>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<ScheduleAppointmentEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<CreateNeededResourceEventHandler>().LifestyleTransient());
                container.Register(Component.For<ICrmRecruitmentActivityService>().ImplementedBy<CrmRecruitmentActivityService>().LifestyleTransient());
            }
        }
    }
}
