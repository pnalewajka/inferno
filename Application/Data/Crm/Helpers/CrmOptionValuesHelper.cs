﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System;

namespace Smt.Atomic.Data.Crm.Helpers
{
    internal static class CrmOptionValuesHelper
    {
        private static OptionMetadataCollection GetOptionSet(string entityName, string fieldName,
            IOrganizationService service, bool excludeNull)
        {
            var attReq = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = fieldName,
                RetrieveAsIfPublished = true
            };

            var attResponse = (RetrieveAttributeResponse)service.Execute(attReq);
            var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;

            var optionList = attMetadata.OptionSet.Options;
            return optionList;
        }

        public static Dictionary<int, string> GetOptionSetDictionary(string entityName, string fieldName,
            IOrganizationService service)
        {
            var optionSet = GetOptionSet(entityName, fieldName, service, true);
            var optionList = optionSet.Where(o => o.Value != null)
                .Select(o => new { key = (int)o.Value, text = o.Label.UserLocalizedLabel.Label })
                .ToDictionary(k => k.key, t => t.text);

            if (!optionList.Any())
            {
                optionList.Add(0, null);
            };

            return optionList;
        }

        public static Dictionary<Guid, T> GetEntires<T>(string entityName, IOrganizationService service) where T : Entity
        {
            return service.RetrieveMultiple(new Microsoft.Xrm.Sdk.Query.QueryExpression(entityName) { ColumnSet = new Microsoft.Xrm.Sdk.Query.ColumnSet(true) }).Entities.ToDictionary(k => k.Id, v => v.ToEntity<T>());
        }
    }
}