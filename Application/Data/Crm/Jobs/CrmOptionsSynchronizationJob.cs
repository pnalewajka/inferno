﻿using System;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Crm.Interfaces;
using System.Linq;
using System.Threading;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Data.Crm.Jobs
{
    [Identifier("Job.Crm.CrmOptionsSynchronizationJob")]
    [JobDefinition(
        Code = "CrmOptionsSynchronizationJob",
        Description = "Synchronize CRM Options Entities",
        ScheduleSettings = "0 1 * * * MON",
        MaxExecutioPeriodInSeconds = 300,
        PipelineName = PipelineCodes.CrmSynchronization)]
    public class CrmOptionsSynchronizationJob : IJob
    {
        private readonly ICrmOptionService _crmOptionService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public CrmOptionsSynchronizationJob(ICrmOptionService crmOptionService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmOptionService = crmOptionService;
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                SynchronizeMainTechnologies(unitOfWork, executionContext.CancellationToken);
                SynchronizeRegions(unitOfWork, executionContext.CancellationToken);
                SynchronizeBusinessUnits(unitOfWork, executionContext.CancellationToken);
                unitOfWork.Commit();
            }
            return JobResult.Success();
        }

        private void SynchronizeMainTechnologies(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var technologies = _crmOptionService.GetMainTechnologyOptions().ToList();
            if (technologies.Any())
            {

                foreach (var technology in technologies.TakeWhileNotCancelled(cancellationToken))
                {
                    var technologyFromDb = unitOfWork.Repositories.MainTechnologyOptions.SingleOrDefault(t => t.CrmId == technology.Id);
                    if (technologyFromDb == null)
                    {
                        technologyFromDb = new Entities.Modules.CustomerPortal.MainTechnologyOptions
                        {
                            CrmId = technology.Id,
                            DisplayValue = technology.Name
                        };
                        unitOfWork.Repositories.MainTechnologyOptions.Add(technologyFromDb);
                    }
                    else
                    {
                        technologyFromDb.DisplayValue = technology.Name;
                    }
                }

            }
        }

        private void SynchronizeRegions(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var regions = _crmOptionService.GetRegionOption().ToList();
            if (regions.Any())
            {
                foreach (var region in regions.TakeWhileNotCancelled(cancellationToken))
                {
                    var regionFromDb = unitOfWork.Repositories.RegionOptions.SingleOrDefault(t => t.CrmId == region.Id);
                    if (regionFromDb == null)
                    {
                        regionFromDb = new Entities.Modules.CustomerPortal.RegionOptions
                        {
                            CrmId = region.Id,
                            DisplayValue = region.Name
                        };
                        unitOfWork.Repositories.RegionOptions.Add(regionFromDb);
                    }
                    else
                    {
                        regionFromDb.DisplayValue = region.Name;
                    }
                }

            }
        }

        private void SynchronizeBusinessUnits(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var businessUnits = _crmOptionService.GetBusinessUnitOptions().ToList();
            if (businessUnits.Any())
            {
                foreach (var businessUnit in businessUnits.TakeWhileNotCancelled(cancellationToken))
                {
                    var businessUnitFromDb = unitOfWork.Repositories.BusinesUnitOptions.SingleOrDefault(t => t.CrmId == businessUnit.Id);
                    if (businessUnitFromDb == null)
                    {
                        businessUnitFromDb = new Entities.Modules.CustomerPortal.BusinesUnitOptions
                        {
                            CrmId = businessUnit.Id,
                            DisplayValue = businessUnit.Name
                        };
                        unitOfWork.Repositories.BusinesUnitOptions.Add(businessUnitFromDb);
                    }
                    else
                    {
                        businessUnitFromDb.DisplayValue = businessUnit.Name;
                    }
                }
            }
        }
    }
}
