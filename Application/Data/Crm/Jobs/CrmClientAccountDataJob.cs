﻿using System.Linq;
using System.Threading;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Crm.Jobs
{
    [Identifier("Job.Crm.CrmClientAccountDataJob")]
    [JobDefinition(
        Code = "CrmClientAccountDataJob",
        Description = "Synchronize CRM Client Entities",
        ScheduleSettings = "0 2 * * * *",
        MaxExecutioPeriodInSeconds = 300,
        PipelineName = PipelineCodes.CrmSynchronization)]
    public class CrmClientAccountDataJob : IJob
    {
        private readonly ICrmOptionService _crmOptionService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;

        public CrmClientAccountDataJob(ICrmOptionService crmOptionService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService, ITimeService timeService)
        {
            _crmOptionService = crmOptionService;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            if (!executionContext.CancellationToken.IsCancellationRequested)
            {
                SynchronizeClientContacts(executionContext.CancellationToken);
            }

            return JobResult.Success();
        }

        private void SynchronizeClientContacts(CancellationToken cancellationToken)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var lastDate =
                    unitOfWork.Repositories.ClientContacts.OrderByDescending(a => a.CreatedOn).FirstOrDefault();

                var contacts = _crmOptionService.ClientContacts(lastDate?.CreatedOn);

                foreach (var crmDictionaryDto in contacts.TakeWhileNotCancelled(cancellationToken))
                {
                    unitOfWork.Repositories.ClientContacts.Add(new ClientContact
                    {
                        CreatedOn = _timeService.GetCurrentTime(),
                        Name = crmDictionaryDto.Name,
                        CrmId = crmDictionaryDto.Id,
                        CompanyName = crmDictionaryDto.CompanyName,
                        Email = crmDictionaryDto.Email,
                        CompanyGuidId = crmDictionaryDto.CompanyGuidId
                    });
                }

                unitOfWork.Commit();
            }
        }
    }
}