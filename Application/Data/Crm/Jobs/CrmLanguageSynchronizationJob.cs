﻿using System.Linq;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Crm.Jobs
{
    [Identifier("Job.Crm.CrmLanguageSynchronizationJob")]
    [JobDefinition(
        Code = "CrmLanguageSynchronizationJob",
        Description = "Synchronize CRM Language Entities",
        ScheduleSettings = "0 1 * * * MON",
        MaxExecutioPeriodInSeconds = 300,
        PipelineName = PipelineCodes.CrmSynchronization)]
    public class CrmLanguageSynchronizationJob : IJob
    {
        private readonly ICrmLanguageService _crmLanguageService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public CrmLanguageSynchronizationJob(
            ICrmLanguageService crmLanguageService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmLanguageService = crmLanguageService;
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            var options = _crmLanguageService.GetLanguageOptions().ToList();

            if (options.Any())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    foreach (var crmLanguageOptionDto in options.OrderBy(r => r.LanguageId).ThenBy(r => r.LevelId).TakeWhileNotCancelled(executionContext.CancellationToken))
                    {
                        var languageOption =
                            unitOfWork.Repositories.LanguageOptions.FirstOrDefault(
                                lo =>
                                    lo.LanguageId == crmLanguageOptionDto.LanguageId &&
                                    lo.LevelId == crmLanguageOptionDto.LevelId);

                        if (languageOption == null)
                        {
                            unitOfWork.Repositories.LanguageOptions.Add(new LanguageOption
                            {
                                LanguageId = crmLanguageOptionDto.LanguageId,
                                LanguageName = crmLanguageOptionDto.LanguageName,
                                LevelId = crmLanguageOptionDto.LevelId,
                                LevelName = crmLanguageOptionDto.LevelName
                            });
                        }
                        else
                        {
                            languageOption.LanguageName = crmLanguageOptionDto.LanguageName;
                            languageOption.LevelName = crmLanguageOptionDto.LevelName;
                        }
                    }
                     
                    unitOfWork.Commit();
                }
            }

            return JobResult.Success();
        }
    }
}