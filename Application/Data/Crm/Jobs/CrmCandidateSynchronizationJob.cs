﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Jobs;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Crm;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Data.Crm.Jobs
{
    [Identifier("Job.Crm.CandidateSynchronizationJob")]
    [JobDefinition(
        Code = "CrmCandidateSynchronizationJob",
        Description = "Synchronize CRM Candidate Entities",
        ScheduleSettings = "0 2 * * * *",
        MaxExecutioPeriodInSeconds = 300,
        PipelineName = PipelineCodes.CrmSynchronization)]
    public class CrmCandidateSynchronizationJob : EntitySynchronizationJob<CrmCandidateDto, CrmCandidate>
    {
        private readonly IUnitOfWorkService<ICrmDbScope> _unitOfWorkService;
        private readonly ICrmService _crmService;
        private readonly IClassMapping<CrmCandidateDto, CrmCandidate> _classMapping;

        public CrmCandidateSynchronizationJob(IClassMappingFactory classMappingFactory,
            IUnitOfWorkService<ICrmDbScope> unitOfWorkService,
            IEntitySynchronizationConfigurationService entitySynchronizationConfigurationService, 
            ICrmService crmService,
            ILogger logger) : base(entitySynchronizationConfigurationService, logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _crmService = crmService;
            _classMapping = classMappingFactory.CreateMapping<CrmCandidateDto, CrmCandidate>();
        }

        protected override DateTime? Upsert(CrmCandidateDto externalEntity)
        {
            var mappedEntity = _classMapping.CreateFromSource(externalEntity);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidates = unitOfWork.Repositories.Candidates;

                var matchingCandidate = candidates.FirstOrDefault(c => c.CrmId == mappedEntity.CrmId);

                if (matchingCandidate != null)
                {
                    candidates.Detach(matchingCandidate);
                    mappedEntity.Id = matchingCandidate.Id;
                    candidates.Edit(mappedEntity);
                }
                else
                {
                    candidates.Add(mappedEntity);
                }

                unitOfWork.Commit();
            }

            return externalEntity.ModifiedOn;
        }

        protected override IEnumerable<CrmCandidateDto> GetModifiedData(DateTime? dateFrom)
        {
            return _crmService.GetCandidates(dateFrom);
        }
    }
}