﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using Smt.Atomic.Data.Entities.Generators;

namespace Smt.Atomic.Data.Entities
{
    public class AtomicDbConfiguration : DbConfiguration
    {
        public AtomicDbConfiguration()
        {
            var path = Path.GetDirectoryName(GetType().Assembly.Location);
            SetModelStore(new DefaultDbModelStore(path));

            SetMigrationSqlGenerator("System.Data.SqlClient", () => new AtomicMigrationSqlGenerator());
        }
    }
}
