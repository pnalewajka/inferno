﻿using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.IO;

namespace Smt.Atomic.Data.Entities.Operations
{
    public abstract class AtomicMigrationOperation
        : MigrationOperation
    {
        protected AtomicMigrationOperation()
            : base(null)
        {
        }

        public abstract TransactionalBehavior TransactionalBehavior { get; }

        public abstract void Generate(TextWriter writer);
    }
}
