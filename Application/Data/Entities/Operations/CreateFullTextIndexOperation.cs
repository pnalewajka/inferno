﻿using System;
using System.Data.Entity;
using System.IO;

namespace Smt.Atomic.Data.Entities.Operations
{
    public class CreateFullTextIndexOperation
        : AtomicMigrationOperation
    {
        private readonly string _schema;
        private readonly string _table;
        private readonly string _column;
        private readonly string _uniqueIndex;
        private readonly string _catalog;

        public CreateFullTextIndexOperation(
            string schema,
            string table,
            string column,
            string uniqueIndex = null) : base()
        {
            _schema = schema;
            _table = table;
            _column = column;
            _uniqueIndex = uniqueIndex ?? $"PK_{schema}.{table}";
            _catalog = $"FTS_{schema}_{table}_{column}";
        }

        public override bool IsDestructiveChange => true;

        public override TransactionalBehavior TransactionalBehavior => TransactionalBehavior.DoNotEnsureTransaction;

        public override void Generate(TextWriter writer)
        {
            writer.WriteLine($"EXEC sp_fulltext_database N'enable'");

            writer.WriteLine($"IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs WHERE [name] = N'FFS_Catalog')");
            writer.WriteLine($"CREATE FULLTEXT CATALOG [FFS_Catalog] AS DEFAULT");

            writer.WriteLine($"IF COLUMNPROPERTY(OBJECT_ID(N'[{_schema}].[{_table}]'), N'[{_column}]', 'IsFulltextIndexed') = 0");
            writer.WriteLine($"CREATE FULLTEXT INDEX ON [{_schema}].[{_table}] ([{_column}]) KEY INDEX [{_uniqueIndex}]");
        }
    }
}
