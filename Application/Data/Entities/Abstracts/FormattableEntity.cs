﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Entities.Abstracts
{
    public abstract class FormattableEntity : IFormattable
    {
        /// <summary>
        /// Returns string representation of entity according to format
        /// </summary>
        /// <param name="format">Format to use</param>
        /// <returns>Formatted string</returns>
        public string ToString(string format)
        {
            return ToString(format, null);
        }

        /// <summary>
        /// Returns string representation of entity according to format and formatProvider
        /// G - default format
        /// F - full rormat
        /// N - name-only format
        /// C - code-or-id format
        /// </summary>
        /// <param name="format">Format to use</param>
        /// <param name="formatProvider">Format provider to use</param>
        /// <returns>Formatted string</returns>
        public virtual string ToString(string format, IFormatProvider formatProvider)
        {
            switch (format)
            {
                case null:
                case "G":
                case "F":
                    var name = GetFormattedName(formatProvider);
                    var codeOrId = GetFormattedCodeOrId(formatProvider);
                    return $"{name} ({codeOrId.Key}={codeOrId.Value})";

                case "N":
                    return GetFormattedName(formatProvider);

                case "C":
                    return GetFormattedCodeOrId(formatProvider).Value;

                default:
                    throw new ArgumentOutOfRangeException(nameof(format), $"Format '{format}' not supported");
            }
        }

        public override sealed string ToString()
        {
            return ToString(null, null);
        }

        protected virtual string GetFormattedName(IFormatProvider formatProvider)
        {
            var property = GetType().GetProperty("Name", typeof(string)) ??
                           GetType().GetProperty("NameEn", typeof(string)) ??
                           GetType().GetProperty("FullName", typeof(string));

            return property?.GetValue(this) as string ?? GetType().Name;
        }

        protected virtual KeyValuePair<string, string> GetFormattedCodeOrId(IFormatProvider formatProvider)
        {
            var property = GetType().GetProperty("Code") ??
                           GetType().GetProperty("Id");

            var value = property?.GetValue(this) ?? GetHashCode();

            return new KeyValuePair<string, string>(
                property?.Name.ToLowerInvariant() ?? "hash",
                (value as IFormattable)?.ToString(null, formatProvider) ?? value.ToString());
        }
    }
}
