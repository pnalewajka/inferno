﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Abstracts
{
    public abstract class TrackedEntity : ModificationTrackedEntity, ICreationTracked, ISoftDeletable
    {
        [EntityReference(typeof(User))]
        public long? CreatedById { get; set; }

        [EntityReference(typeof(User))]
        public long? ImpersonatedCreatedById { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [ForeignKey(nameof(CreatedById))]
        public virtual User CreatedBy { get; set; }

        [ForeignKey(nameof(ImpersonatedCreatedById))]
        public virtual User ImpersonatedCreatedBy { get; set; }

        public bool IsDeleted { get; set; }

        public abstract void OnSoftDeleting();
    }
}
