﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Data.Common.Attributes;

namespace Smt.Atomic.Data.Entities.Abstracts
{
    public abstract class DocumentBase<TDocumentContent> : ModificationTrackedEntity
    {
        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(250)]
        [NonUnicode]
        public string ContentType { get; set; }

        public long ContentLength { get; set; }

        public virtual TDocumentContent DocumentContent { get; set; }
    }
}
