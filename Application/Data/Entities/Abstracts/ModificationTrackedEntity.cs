﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Abstracts
{
    public abstract class ModificationTrackedEntity : SimpleEntity, IModificationTracked
    {
        [EntityReference(typeof(User))]
        public long? ImpersonatedById { get; set; }

        [EntityReference(typeof(User))]
        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        [ForeignKey(nameof(ModifiedById))]
        public virtual User ModifiedBy { get; set; }

        [ForeignKey(nameof(ImpersonatedById))]
        public virtual User ImpersonatedBy { get; set; }
    }
}
