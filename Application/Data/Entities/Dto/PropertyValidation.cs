﻿
using System;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class PropertyValidation
    {
        public string PropertyName { get; }
        public Func<IEntity, bool> Validation { get; }

        public PropertyValidation(string propertyName, Func<IEntity, bool> validation)
        {
            PropertyName = propertyName;
            Validation = validation;
        }
    }
}
