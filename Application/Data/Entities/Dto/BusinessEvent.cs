using System;
using System.Runtime.Serialization;

namespace Smt.Atomic.Data.Entities.Dto
{
    [DataContract]
    public abstract class BusinessEvent
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }
    }
}