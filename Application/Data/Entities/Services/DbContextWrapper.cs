﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Services
{
    internal class DbContextWrapper : IDbContextWrapper
    {
        private DbContext _context;

        public void InitContext(string connectionString = null)
        {
            _context = connectionString == null
                ? new CoreEntitiesContext()
                : new CoreEntitiesContext(connectionString);
        }

        public DbSet Set(Type entityType)
        {
            return _context.Set(entityType);
        }

        public IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return _context.Set<TEntity>();
        }

        public int ExecuteSqlCommand(string command, params object[] parameters)
        {
            return _context.Database.ExecuteSqlCommand(command, parameters);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IEnumerable<DbEntityEntry> ChangeTrackerEntries()
        {
            return _context.ChangeTracker.Entries();
        }

        public IEnumerable<DbEntityEntry<TEntity>> ChangeTrackerEntries<TEntity>() where TEntity : class
        {
            return _context.ChangeTracker.Entries<TEntity>();
        }

        public DbEntityEntry Entry(object entity)
        {
            return _context.Entry(entity);
        }

        public DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            return _context.Entry(entity);
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            return _context.Database.SqlQuery<TElement>(sql, parameters);
        }

        public IEnumerable SqlQuery(Type elementType, string sql, params object[] parameters)
        {
            return _context.Database.SqlQuery(elementType, sql, parameters);
        }

        public DataSet SqlQueryToDataSet(string sql, IDictionary<string, object> parameters)
        {
            var connectionString = _context.Database.Connection.ConnectionString;

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    CreateParameters(parameters, command);

                    using (var dataAdapter = new SqlDataAdapter(command))
                    {
                        DataSet result = new DataSet();
                        dataAdapter.Fill(result);

                        return result;
                    }
                }
            }
        }

        private static void CreateParameters(IDictionary<string, object> parameters, SqlCommand command)
        {
            foreach (var keyValue in parameters)
            {
                var parameterName = keyValue.Key.StartsWith("@") ? keyValue.Key : "@" + keyValue.Key;
                var arrayValues = keyValue.Value as IEnumerable;

                if (arrayValues != null && !(keyValue.Value is string))
                {
                    var pascalCaseParameterName = NamingConventionHelper.ConvertCamelCaseToPascalCase(parameterName.Substring(1));
                    var isParameterNameSet = $"@Is{pascalCaseParameterName}Set";
                    var filteredArrayValues = arrayValues.OfType<object>();

                    if (filteredArrayValues.Any())
                    {
                        var names = string.Join(", ", filteredArrayValues.Select((v, i) =>
                        {
                            var indexedParameterName = parameterName + i;
                            command.Parameters.AddWithValue(indexedParameterName, v);

                            return indexedParameterName;
                        }));

                        if (command.CommandText.Contains(isParameterNameSet))
                        {
                            command.Parameters.AddWithValue(isParameterNameSet, 1);
                        }

                        command.CommandText = command.CommandText.Replace(parameterName, names);
                    }
                    else
                    {
                        if (command.CommandText.Contains(isParameterNameSet))
                        {
                            command.Parameters.AddWithValue(isParameterNameSet, 0);
                        }

                        command.Parameters.AddWithValue(parameterName, 0);
                    }
                }
                else
                {
                    command.Parameters.AddWithValue(parameterName, keyValue.Value ?? DBNull.Value);
                }
            }
        }

        public void SetCommandTimeout(int timeoutInSeconds)
        {
            var adapter = (IObjectContextAdapter)_context;
            var objectContext = adapter.ObjectContext;
            objectContext.CommandTimeout = timeoutInSeconds;
        }

        public void SetState<TEntity>(TEntity entity, EntityState state) where TEntity : class
        {
            var entry = Entry(entity);
            entry.State = state;
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}