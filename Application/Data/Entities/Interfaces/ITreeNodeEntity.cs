﻿namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface ITreeNodeEntity : IEntity
    {
        string Name { get; set; }

        string Code { get; set; }

        long? ParentId { get; set; }

        string Path { get; set; }

        long NameSortOrder { get; set; }

        long DescendantCount { get; set; }

        int? Depth { get; set; }

        long? CustomOrder { get; set; }
    }
}