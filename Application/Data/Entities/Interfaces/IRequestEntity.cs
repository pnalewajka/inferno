﻿using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface IRequestEntity : IEntity
    {
        Request Request { get; }
    }
}
