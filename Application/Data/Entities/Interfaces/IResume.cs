﻿using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface IResume
    {
        long ResumeId { get; set; }

        Resume Resume { get; set; }
    }
}
