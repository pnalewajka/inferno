﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface IBusinessEventPublisher
    {
        /// <summary>
        /// Adds business event to the business event processing queue
        /// </summary>
        /// <param name="businessEvent">A new business event</param>
        void PublishBusinessEvent(BusinessEvent businessEvent);
    }
}