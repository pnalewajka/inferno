﻿namespace Smt.Atomic.Data.Entities.Interfaces
{
    /// <summary>
    /// Entity interface
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Id that represents entity (key value)
        /// </summary>
        long Id { get; set; }
    }
}