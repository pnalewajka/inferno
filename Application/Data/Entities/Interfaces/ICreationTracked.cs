﻿using System;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    /// <summary>
    /// Interface for creation tracking
    /// </summary>
    public interface ICreationTracked
    {
        /// <summary>
        /// Id of user which created data
        /// </summary>
        long? CreatedById { get; set; }

        /// <summary>
        /// Id of impersonating user
        /// </summary>
        long? ImpersonatedCreatedById { get; set; }

        /// <summary>
        /// Date on which entity was created
        /// </summary>
        DateTime CreatedOn { get; set; }
    }
}
