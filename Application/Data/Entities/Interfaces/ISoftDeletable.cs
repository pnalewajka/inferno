﻿namespace Smt.Atomic.Data.Entities.Interfaces
{
    /// <summary>
    /// Enables soft delete
    /// </summary>
    public interface ISoftDeletable
    {
        /// <summary>
        /// Is in deleted state
        /// </summary>
        bool IsDeleted { get; set; }

        /// <summary>
        /// Method called on entity being soft deleted
        /// </summary>
        void OnSoftDeleting();
    }
}
