﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    /// <summary>
    /// Wrapping around DbContext to make it testable
    /// </summary>
    public interface IDbContextWrapper : IDisposable
    {
        /// <summary>
        /// Initialize context
        /// </summary>
        /// <param name="connectionString"></param>
        void InitContext(string connectionString = null);

        /// <summary>
        /// Get entities set
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Get entities set
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        DbSet Set(Type entityType);

        /// <summary>
        /// Execute sqlcommand
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameters"></param>
        /// <returns>Number of afected rows</returns>
        int ExecuteSqlCommand(string command, params object[] parameters);

        /// <summary>
        /// Save changes
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Save changes asynchronously
        /// </summary>
        /// <returns></returns>
        Task SaveChangesAsync();

        /// <summary>
        /// Entries from ChangeTracker
        /// </summary>
        /// <returns></returns>
        IEnumerable<DbEntityEntry> ChangeTrackerEntries();

        /// <summary>
        /// Entries from ChangeTracker - generic version
        /// </summary>
        /// <returns></returns>
        IEnumerable<DbEntityEntry<TEntity>> ChangeTrackerEntries<TEntity>() where TEntity : class;

        /// <summary>
        /// Entry
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        DbEntityEntry Entry(object entity);

        /// <summary>
        /// Entry - generic version
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Execute sql and return list of filled objects of TElement type
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters);

        /// <summary>
        /// Execute sql and return list of filled objects of TElement type - not generic version
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        IEnumerable SqlQuery(Type elementType, string sql, params object[] parameters);

        /// <summary>
        /// Executes sql and return DataSet based on the results
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DataSet SqlQueryToDataSet(string sql, IDictionary<string, object> parameters);

        /// <summary>
        /// Set command timeout
        /// </summary>
        /// <param name="timeoutInSeconds"></param>
        void SetCommandTimeout(int timeoutInSeconds);

        /// <summary>
        /// Set state of entity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="state"></param>
        void SetState<TEntity>(TEntity entity, EntityState state) where TEntity : class;
    }
}
