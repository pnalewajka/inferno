﻿namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface IBusinessTripSettlementRequestCost
    {
        string Description { get; set; }

        decimal Amount { get; set; }

        long CurrencyId { get; set; }
    }
}
