﻿namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface ITimestamp
    {
        byte[] Timestamp { get; set; }
    }
}
