﻿using System;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    /// <summary>
    /// Date of modification tracking
    /// </summary>
    public interface IModificationTracked
    {
        /// <summary>
        /// Date on which modification occurred
        /// </summary>
        DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Id of user which modified data
        /// </summary>
        long? ModifiedById { get; set; }

        /// <summary>
        /// Id of impersonating user
        /// </summary>
        long? ImpersonatedById { get; set; }
    }
}
