﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Interfaces
{
    public interface IWatchedEntity
    {
        ICollection<Employee> Watchers { get; set; }
    }
}