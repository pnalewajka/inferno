﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Infrastructure.Interception;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Fissoft.EntityFramework.Fts;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Entities.Modules.Consents;
using Smt.Atomic.Data.Entities.Modules.Crm;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Entities.Modules.Finance;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.KPI;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities
{
    public class CoreEntitiesContext : DbContext
    {
        public CoreEntitiesContext()
            : base("name=Smt.Atomic.Data.Entities.CoreEntitiesContext")
        {
            Database.SetInitializer<CoreEntitiesContext>(null);
            //DbInterception.Add(new FtsInterceptor());
        }

        public CoreEntitiesContext(string connectionString)
            : base(connectionString)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Credential> Credentials { get; set; }
        public DbSet<CacheInvalidation> CacheInvalidations { get; set; }
        public DbSet<SecurityRole> SecurityRoles { get; set; }
        public DbSet<SecurityProfile> SecurityProfiles { get; set; }
        public DbSet<DataFilter> DataFilters { get; set; }
        public DbSet<DbDateTime> CurrentDateTimes { get; set; }
        public DbSet<SystemParameter> SystemParameters { get; set; }
        public DbSet<MessageTemplate> MessageTemplate { get; set; }
        public DbSet<JobDefinition> JobDefinitions { get; set; }
        public DbSet<JobInternalParameter> EntitySynchronizationConfiguration { get; set; }
        public DbSet<JobDefinitionParameter> JobDefinitionParameters { get; set; }
        public DbSet<JobLog> JobLogs { get; set; }
        public DbSet<JobRunInfo> JobRunInfos { get; set; }
        public DbSet<TemporaryDocument> TemporaryDocuments { get; set; }
        public DbSet<TemporaryDocumentPart> TemporaryDocumentParts { get; set; }
        public DbSet<UserDocument> UserDocuments { get; set; }
        public DbSet<LastJobRunInfo> LastJobRunInfos { get; set; }
        public DbSet<Calendar> Calendars { get; set; }
        public DbSet<CalendarEntry> CalendarEntries { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<AuditTable> AuditTables { get; set; }
        public DbSet<OrgUnit> OrgUnits { get; set; }
        public DbSet<ProcessedEventQueueItem> ProcessedEventQueue { get; set; }
        public DbSet<PublishedEventQueueItem> PublishedEventQueue { get; set; }
        public DbSet<ConsentDecision> ConsentDecisions { get; set; }
        public DbSet<ReportDefinition> ReportDefinitions { get; set; }
        public DbSet<ReportTemplateContent> ReportTemplateContents { get; set; }
        public DbSet<ReportTemplate> ReportTemplates { get; set; }
        public DbSet<JobProfile> JobProfiles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<JobMatrix> JobMatrix { get; set; }
        public DbSet<EmploymentPeriod> EmploymentPeriods { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<StaffingDemand> StaffingDemands { get; set; }
        public DbSet<JobMatrixLevel> JobMatrixLevels { get; set; }
        public DbSet<ActiveDirectoryGroup> ActiveDirectoryGroups { get; set; }
        public DbSet<AllocationRequest> AllocationRequests { get; set; }
        public DbSet<AllocationDailyRecord> AllocationDailyRecords { get; set; }
        public DbSet<WeeklyAllocationStatus> WeeklyAllocationStatus { get; set; }
        public DbSet<WeeklyAllocationDetail> WeeklyAllocationDetails { get; set; }
        public DbSet<Recommendation> Recommendations { get; set; }
        public DbSet<Challenge> Challenges { get; set; }
        public DbSet<ChallengeDocument> ChallengeDocuments { get; set; }
        public DbSet<AnswerDocument> AnswerDocuments { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentContent> DocumentContents { get; set; }
        public DbSet<EmailAttachment> EmailAttachments { get; set; }
        public DbSet<SkillTag> SkillTags { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<NeededResource> NeededResources { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<ServiceLine> ServiceLines { get; set; }
        public DbSet<SoftwareCategory> SoftwareCategories { get; set; }
        public DbSet<ProjectPicture> ProjectPictures { get; set; }
        public DbSet<ProjectPictureContent> ProjectPictureContents { get; set; }
        public DbSet<CustomerSize> CustomerSizes { get; set; }
        public DbSet<ProjectTag> ProjectTags { get; set; }
        public DbSet<EmployeeComment> EmployeeComments { get; set; }
        public DbSet<CrmCandidate> Candidates { get; set; }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<ResumeTechnicalSkill> ResumeTechnicalSkills { get; set; }
        public DbSet<PricingEngineer> PricingEngineers { get; set; }
        public DbSet<TimeReport> TimeReports { get; set; }
        public DbSet<TimeReportRow> TimeReportRows { get; set; }
        public DbSet<TimeReportDailyEntry> TimeReportDailyEntries { get; set; }
        public DbSet<TimeTrackingImportSource> TimeTrackingImportSources { get; set; }
        public DbSet<VacationBalance> VacationBalances { get; set; }
        public DbSet<SurveyDefinition> SurveyDefinitions { get; set; }
        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public DbSet<SurveyConduct> SurveyConducts { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyAnswer> SurveyAnswers { get; set; }
        public DbSet<SurveyConductQuestion> SurveyConductQuestions { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Approval> Approvals { get; set; }
        public DbSet<AllocationRequestMetadata> AllocationRequestMetadatas { get; set; }
        public DbSet<TechnicalInterviewer> TechnicalInterviewers { get; set; }
        public DbSet<AbsenceType> AbsenceTypes { get; set; }
        public DbSet<CalendarAbsence> CalendarAbsences { get; set; }
        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<OvertimeApproval> OvertimeApprovals { get; set; }
        public DbSet<UserAcronym> UserAcronyms { get; set; }
        public DbSet<EmployeeAbsence> EmployeeAbsences { get; set; }
        public DbSet<JobTitle> JobTitles { get; set; }
        public DbSet<ApprovalGroup> ApprovalGroups { get; set; }
        public DbSet<EmploymentTerminationReason> EmploymentTerminationReasons { get; set; }
        public DbSet<JiraIssueToTimeReportRowMapper> JiraIssueToTimeReportRowMappers { get; set; }
        public DbSet<ActionSet> ActionSets { get; set; }
        public DbSet<ActionSetTriggeringStatus> ActionSetTriggeringStatus { get; set; }
        public DbSet<ScheduleAppointment> ScheduleAppointments { get; set; }
        public DbSet<ChallengeFile> ChallengeFiles { get; set; }
        public DbSet<ResumeCompany> ResumeCompanies { get; set; }
        public DbSet<ResumeProject> ResumeProjects { get; set; }
        public DbSet<ResumeLanguage> ResumeLanguages { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<ResumeTraining> ResumeTrainings { get; set; }
        public DbSet<ResumeEducation> ResumeEducations { get; set; }
        public DbSet<Substitution> Substitutions { get; set; }
        public DbSet<OvertimeBank> OverTimeBanks { get; set; }
        public DbSet<AbsenceCalendarEntry> AbsenceCalendarEntries { get; set; }
        public DbSet<UtilizationCategory> UtilizationCategories { get; set; }
        public DbSet<ResourceForm> ResourceForms { get; set; }
        public DbSet<ResourceFormFile> ResourceFormFiles { get; set; }
        public DbSet<ResourceFormFileContent> ResourceFormFileContents { get; set; }
        public DbSet<LanguageOption> LanguageOptions { get; set; }
        public DbSet<TimeReportProjectAttribute> TimeReportProjectAttributes { get; set; }
        public DbSet<TimeReportProjectAttributeValue> TimeReportProjectAttributeValues { get; set; }
        public DbSet<MainTechnologyOptions> MainTechnologyOptions { get; set; }
        public DbSet<BusinesUnitOptions> BusinesUnitOptions { get; set; }
        public DbSet<RegionOptions> RegionOptions { get; set; }
        public DbSet<ClientContact> ClientContacts { get; set; }
        public DbSet<ClientData> ClientDatas { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<MeetingNote> MeetingNotes { get; set; }
        public DbSet<MeetingAttachment> MeetingAttachments { get; set; }
        public DbSet<MeetingAttachmentContent> MeetingAttachmentContents { get; set; }
        public DbSet<MeetingNoteAttachment> MeetingNoteAttachments { get; set; }
        public DbSet<MeetingNoteAttachmentContent> MeetingNoteAttachmentContents { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<RequestHistoryEntry> RequestHistoryEntries { get; set; }
        public DbSet<SkillChangeRequest> SkillChangeRequests { get; set; }
        public DbSet<AddEmployeeRequest> AddEmployeeRequests { get; set; }
        public DbSet<AbsenceRequest> AbsenceRequests { get; set; }
        public DbSet<AddHomeOfficeRequest> AddHomeOfficeRequests { get; set; }
        public DbSet<OvertimeRequest> OvertimeRequests { get; set; }
        public DbSet<EmploymentTerminationRequest> EmploymentTerminationRequests { get; set; }
        public DbSet<ReopenTimeTrackingReportRequest> ReopenTimeTrackingReportRequests { get; set; }
        public DbSet<ProjectTimeReportApprovalRequest> ProjectTimeReportApprovalRequests { get; set; }
        public DbSet<EmployeeDataChangeRequest> EmployeeDataChangeRequests { get; set; }
        public DbSet<EmploymentDataChangeRequest> EmploymentDataChangeRequests { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<RequestDocument> RequestDocuments { get; set; }
        public DbSet<RequestDocumentContent> RequestDocumentContents { get; set; }
        public DbSet<BusinessTripRequest> BusinessTripRequests { get; set; }
        public DbSet<BusinessTrip> BusinessTrips { get; set; }
        public DbSet<AdvancedPaymentRequest> AdvancedPaymentRequests { get; set; }
        public DbSet<BusinessTripParticipant> BusinessTripParticipants { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<AggregatedProjectTransaction> AggregatedProjectTransactions { get; set; }
        public DbSet<DeclaredLuggage> LuggageDeclarations { get; set; }
        public DbSet<BusinessTripMessage> BusinessTripMessages { get; set; }
        public DbSet<ClockCardDailyEntry> ClockCardDailyEntries { get; set; }
        public DbSet<OnboardingRequest> OnboardingRequests { get; set; }
        public DbSet<AssetType> AssetType { get; set; }
        public DbSet<ArrangementTrackingEntry> ArrangementTrackingEntries { get; set; }
        public DbSet<EmployeePhotoDocument> EmployeePhotoDocuments { get; set; }
        public DbSet<EmployeePhotoDocumentContent> EmployeePhotoDocumentContents { get; set; }
        public DbSet<PayrollDocument> PayrollDocuments { get; set; }
        public DbSet<PayrollDocumentContent> PayrollDocumentContents { get; set; }
        public DbSet<ForeignEmployeeDocument> ForeignEmployeeDocuments { get; set; }
        public DbSet<ForeignEmployeeDocumentContent> ForeignEmployeeDocumentContents { get; set; }
        public DbSet<AssetsRequest> AssetsRequests { get; set; }
        public DbSet<JobProfileSkillTag> JobProfileSkillTags { get; set; }
        public DbSet<ResumeAdditionalSkill> ResumeAdditionalSkills { get; set; }
        public DbSet<FeedbackRequest> FeedbackRequests { get; set; }
        public DbSet<WhistleBlowing> WhistleBlowings { get; set; }
        public DbSet<WhistleBlowingDocumentContent> WhistleBlowingDocumentContents { get; set; }
        public DbSet<WhistleBlowingDocument> WhistleBlowingDocuments { get; set; }
        public DbSet<DataConsent> DataConsents { get; set; }
        public DbSet<DataConsentDocument> DataConsentDocuments { get; set; }
        public DbSet<DataConsentDocumentContent> DataConsentDocumentContents { get; set; }
        public DbSet<JobOpening> JobOpenings { get; set; }
        public DbSet<ProjectDescription> ProjectDescriptions { get; set; }
        public DbSet<ApplicationOrigin> DataOrgins { get; set; }
        public DbSet<CandidateContactRecord> CandidateContactRecords { get; set; }
        public DbSet<RecruitmentProcess> RecruitmentProcesses { get; set; }
        public DbSet<RecruitmentProcessStep> RecruitmentProcessSteps { get; set; }
        public DbSet<TechnicalReview> TechnicalReviews { get; set; }
        public DbSet<BusinessTripSettlementRequest> BusinessTripSettlementRequests { get; set; }
        public DbSet<BusinessTripAcceptanceConditions> BusinessTripAcceptanceConditions { get; set; }
        public DbSet<BusinessTripSettlementRequestAbroadTimeEntry> BusinessTripSettlemenRequestAbroadTimeEntries { get; set; }
        public DbSet<BusinessTripSettlementRequestMeal> BusinessTripSettlementRequestMeals { get; set; }
        public DbSet<BusinessTripSettlementRequestOwnCost> BusinessTripSettlementRequestOwnCosts { get; set; }
        public DbSet<BusinessTripSettlementRequestCompanyCost> BusinessTripSettlementRequestCompanyCosts { get; set; }
        public DbSet<BusinessTripSettlementRequestAdvancePayment> BusinessTripSettlementRequestAdvancePayments { get; set; }
        public DbSet<BusinessTripSettlementRequestVehicleMileage> BusinessTripSettlementRequestVehicleMileages { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<RecommendingPerson> RecommendingPersons { get; set; }
        public DbSet<DataOwner> DataOwners { get; set; }
        public DbSet<DailyAllowance> DailyAllowances { get; set; }
        public DbSet<DataActivity> DataActivities { get; set; }
        public DbSet<JobApplication> JobApplications { get; set; }
        public DbSet<InvoiceCalculation> InvoiceNotifications { get; set; }
        public DbSet<CompanyAllowedAdvancePaymentCurrency> CompanyAllowedAdvancePaymentCurrencies { get; set; }
        public DbSet<CandidateNote> CandidateNotes { get; set; }
        public DbSet<JobOpeningNote> JobOpeningNotes { get; set; }
        public DbSet<RecruitmentProcessNote> RecruitmentProcessNotes { get; set; }
        public DbSet<RecommendingPersonNote> RecommendingPersonNotes { get; set; }
        public DbSet<InboundEmail> InboundEmails { get; set; }
        public DbSet<InboundEmailDataActivity> InboundEmailActivities { get; set; }
        public DbSet<InboundEmailDocumentContent> InboundEmailDocumentContents { get; set; }
        public DbSet<InboundEmailDocument> InboundEmailDocuments { get; set; }
        public DbSet<CandidateFileDocument> CandidateDocuments { get; set; }
        public DbSet<CandidateFileDocumentContent> CandidateDocumentContents { get; set; }
        public DbSet<CandidateFile> CandidateFiles { get; set; }
        public DbSet<Bonus> Bonuses { get; set; }
        public DbSet<InboundEmailValue> InboundEmailValues { get; set; }
        public DbSet<RecommendingPersonContactRecord> RecommendingPersonContactRecords { get; set; }
        public DbSet<ProjectInvoicingRate> ProjectInvoicingRates { get; set; }
        public DbSet<TaxDeductibleCostRequest> TaxDeductibleCostRequests { get; set; }
        public DbSet<GlobalRate> GlobalRates { get; set; }
        public DbSet<BonusRequest> BonusRequests { get; set; }
        public DbSet<ResumeShareToken> ResumeShareTokens { get; set; }
        public DbSet<ProjectSetup> ProjectSetups { get; set; }
        public DbSet<ExpenseRequest> ExpenseRequests { get; set; }
        public DbSet<KpiDefinition> KpiDefinitions { get; set; }
        public DbSet<CurrencyExchangeRate> CurrencyExchangeRates { get; set; }
        public DbSet<Inquiry> Inquiries { get; set; }
        public DbSet<OfferType> OfferTypes { get; set; }
        public DbSet<InquiryNote> InquiryNotes { get; set; }
        public DbSet<RecruitmentProcessHistoryEntry> RecruitmentProcessHistoryEntries { get; set; }
        public DbSet<JobOpeningHistoryEntry> JobOpeningHistoryEntries { get; set; }
        public DbSet<InvoiceDocument> InvoiceDocuments { get; set; }
        public DbSet<InvoiceDocumentContent> InvoiceDocumentContents { get; set; }
        public DbSet<DataProcessingAgreement> DataProcessingAgreements { get; set; }
        public DbSet<DataProcessingAgreementDocument> DataProcessingAgreementDocuments { get; set; }
        public DbSet<DataProcessingAgreementDocumentContent> DataProcessingAgreementDocumentContents { get; set; }
        public DbSet<DataProcessingAgreementConsent> DataProcessingAgreementConsents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            RegisterConventions(modelBuilder);
            FixRelations(modelBuilder);
            DefineManyToManyTables(modelBuilder);
            DefineCompositeConstraints(modelBuilder);
            SetPrecisionForDecimals(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void SetPrecisionForDecimals(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrencyExchangeRate>().Property(r => r.Rate).HasPrecision(18, 6);
        }

        private void DefineCompositeConstraints(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectSetup>()
                .Property(p => p.ClientShortName)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_ProjectNamePerClient", 1) { IsClustered = false, IsUnique = true }));

            modelBuilder.Entity<ProjectSetup>()
                .Property(p => p.ProjectShortName)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_ProjectNamePerClient", 2) { IsClustered = false, IsUnique = true }));
        }

        private void DefineManyToManyTables(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
               .HasMany(u => u.Employees)
               .WithOptional(p => p.User);

            modelBuilder.Entity<User>()
               .HasMany(u => u.Profiles)
               .WithMany(p => p.Users)
               .Map(p =>
               {
                   p.ToTable("UserSecurityProfiles", "Accounts");
                   p.MapLeftKey("UserId");
                   p.MapRightKey("ProfileId");
               });

            modelBuilder.Entity<User>()
               .HasMany(u => u.Roles)
               .WithMany(p => p.Users)
               .Map(p =>
               {
                   p.ToTable("UserSecurityRoles", "Accounts");
                   p.MapLeftKey("UserId");
                   p.MapRightKey("RoleId");
               });

            modelBuilder.Entity<SecurityRole>()
               .HasMany(u => u.Profiles)
               .WithMany(p => p.Roles)
               .Map(p =>
               {
                   p.ToTable("SecurityRoleSecurityProfiles", "Accounts");
                   p.MapLeftKey("RoleId");
                   p.MapRightKey("ProfileId");
               });

            modelBuilder.Entity<Employee>()
                .HasMany(u => u.JobMatrixs)
                .WithMany(p => p.Employees)
                .Map(p =>
                {
                    p.ToTable("EmployeeJobMatrixLevel", "Allocation");
                    p.MapLeftKey("Employee_Id");
                    p.MapRightKey("JobMatrixLevel_Id");
                });

            modelBuilder.Entity<SkillTag>()
                .HasMany(u => u.Skills)
                .WithMany(p => p.SkillTags)
                .Map(p =>
                {
                    p.ToTable("SkillTagSkill", "SkillManagement");
                    p.MapLeftKey("SkillTag_Id");
                    p.MapRightKey("Skill_Id");
                });

            modelBuilder.Entity<StaffingDemand>()
                .HasMany(u => u.Skills)
                .WithMany(p => p.StaffingDemands)
                .Map(p =>
                {
                    p.ToTable("SkillsStaffingDemand", "Allocation");
                    p.MapLeftKey("StaffingDemand_Id");
                    p.MapRightKey("Skill_Id");
                });

            modelBuilder.Entity<SkillTag>()
                .HasMany(u => u.PricingEngineers)
                .WithMany(p => p.SkillTags)
                .Map(p =>
                {
                    p.ToTable("SkillTagPricingEngineer", "SkillManagement");
                    p.MapLeftKey("SkillTag_Id");
                    p.MapRightKey("PricingEngineer_Id");
                });

            modelBuilder.Entity<User>()
                .HasMany(u => u.ActiveDirectoryGroups)
                .WithMany(p => p.Users)
                .Map(p =>
                {
                    p.ToTable("ActiveDirectoryGroupsUsers", "Allocation");
                    p.MapLeftKey("User_Id");
                    p.MapRightKey("ActiveDirectoryGroup_Id");
                });

            modelBuilder.Entity<ActiveDirectoryGroup>()
                .HasMany(x => x.Managers)
                .WithMany(u => u.ActiveDirectoryGroupsManager)
                .Map(p =>
                {
                    p.ToTable("ActiveDirectoryGroupManagers", "Allocation");
                    p.MapLeftKey("ActiveDirectoryGroup_Id");
                    p.MapRightKey("User_Id");
                });

            modelBuilder.Entity<ActiveDirectoryGroup>()
                .HasOptional(a => a.Owner)
                .WithMany(u => u.ActiveDirectoryGroupsOwner)
                .HasForeignKey(a => a.OwnerId);

            modelBuilder.Entity<AllocationRequestMetadata>()
                .HasKey(a => a.AllocationRequestId);

            modelBuilder.Entity<AllocationRequest>()
                .HasRequired(a => a.AllocationRequestMetadata)
                .WithRequiredPrincipal(p => p.AllocationRequest)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ProjectSetup>()
               .HasMany(u => u.Industries)
               .WithMany(p => p.ProjectSetups)
               .Map(p =>
               {
                   p.ToTable("IndustryProject", "Allocation");
                   p.MapLeftKey("ProjectSetupId");
                   p.MapRightKey("IndustryId");
               });

            modelBuilder.Entity<Employee>()
                .HasMany(u => u.JobProfiles)
                .WithMany(p => p.UsedInEmployees)
                .Map(p =>
                {
                    p.ToTable("EmployeeJobProfile", "Allocation");
                    p.MapLeftKey("Employee_Id");
                    p.MapRightKey("JobProfile_Id");
                });

            modelBuilder.Entity<ProjectSetup>()
                .HasMany(p => p.Technologies)
                .WithMany(s => s.ProjectSetups)
                .Map(p =>
                {
                    p.ToTable("ProjectTechnologies", "Allocation");
                    p.MapLeftKey("ProjectSetupId");
                    p.MapRightKey("SkillId");
                });

            modelBuilder.Entity<ProjectSetup>()
                .HasMany(p => p.KeyTechnologies)
                .WithMany(s => s.ProjectSetups)
                .Map(p =>
                {
                    p.ToTable("ProjectKeyTechnologies", "Allocation");
                    p.MapLeftKey("ProjectSetupId");
                    p.MapRightKey("SkillTagId");

                });

            modelBuilder.Entity<ProjectSetup>()
                .HasMany(p => p.ServiceLines)
                .WithMany(s => s.ProjectSetups)
                .Map(p =>
                {
                    p.ToTable("ProjectServiceLines", "Allocation");
                    p.MapLeftKey("ProjectSetupId");
                    p.MapRightKey("ServiceLineId");
                });

            modelBuilder.Entity<ProjectSetup>()
                .HasMany(p => p.SoftwareCategories)
                .WithMany(s => s.ProjectSetups)
                .Map(p =>
                {
                    p.ToTable("ProjectSoftwareCategories", "Allocation");
                    p.MapLeftKey("ProjectSetupId");
                    p.MapRightKey("SoftwareCategoryId");

                });

            modelBuilder.Entity<ProjectSetup>()
                .HasMany(p => p.Tags)
                .WithMany(t => t.ProjectSetups)
                .Map(p =>
                {
                    p.ToTable("ProjectTagProject", "Allocation");
                    p.MapLeftKey("ProjectSetupId");
                    p.MapRightKey("ProjectTagId");
                });

            modelBuilder.Entity<SkillTag>()
                .HasMany(u => u.TechnicalInterviewers)
                .WithMany(p => p.SkillTags)
                .Map(p =>
                {
                    p.ToTable("SkillTagTechnicalInterviewer", "SkillManagement");
                    p.MapLeftKey("SkillTag_Id");
                    p.MapRightKey("TechnicalInterviewer_Id");
                });

            modelBuilder.Entity<SurveyConduct>()
                .HasMany(s => s.OrgUnits)
                .WithMany()
                .Map(m =>
                {
                    m.MapLeftKey("SurveyConductId");
                    m.MapRightKey("OrgUnitId");
                    m.ToTable("SurveyConductOrgUnits", "Survey");
                });

            modelBuilder.Entity<SurveyConduct>()
                .HasMany(s => s.Employees)
                .WithMany()
                .Map(m =>
                {
                    m.MapLeftKey("SurveyConductId");
                    m.MapRightKey("EmployeeId");
                    m.ToTable("SurveyConductEmployees", "Survey");
                });

            modelBuilder.Entity<Employee>()
               .HasMany(u => u.CalendarAbsences)
               .WithMany(p => p.EmployeesIds)
               .Map(p =>
               {
                   p.ToTable("EmployeeCalendarAbsence", "TimeTracking");
                   p.MapLeftKey("Employee_Id");
                   p.MapRightKey("CalendarAbsence_Id");
               });

            modelBuilder.Entity<SurveyConduct>()
                .HasMany(s => s.ActiveDirectoryGroups)
                .WithMany()
                .Map(m =>
                {
                    m.MapLeftKey("SurveyConductId");
                    m.MapRightKey("ActiveDirectoryGroupId");
                    m.ToTable("SurveyConductActiveDirectoryGroups", "Survey");
                });

            modelBuilder.Entity<ContractType>()
                .HasMany(ct => ct.AbsenceTypes)
                .WithMany(at => at.UsedInContractTypes)
                .Map(m =>
                {
                    m.ToTable("ContractTypeAbsenceTypes", "TimeTracking");
                    m.MapLeftKey("ContractTypeId");
                    m.MapRightKey("AbsenceTypeId");
                });

            modelBuilder.Entity<JobTitle>()
                .HasMany(t => t.DefaultProfiles)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("JobTitleSecurityProfiles", "SkillManagement");
                    m.MapLeftKey("JobTitleId");
                    m.MapRightKey("SecurityProfileId");
                });
            modelBuilder.Entity<ResumeProject>()
                .HasMany(rp => rp.TechnicalSkills)
                .WithMany(ts => ts.ResumeProjects)
                .Map(m =>
                {
                    m.ToTable("ResumeProjectTechnicalSkill", "Resumes");
                    m.MapLeftKey("ResumeProjectId");
                    m.MapRightKey("TechnicalSkillId");
                });

            modelBuilder.Entity<Resume>()
                .HasMany(r => r.ResumeRejectedSkillSuggestion)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("ResumeRejectedSkillSuggestion", "Resumes");
                    m.MapLeftKey("ResumeId");
                    m.MapRightKey("SkillId");
                });

            modelBuilder.Entity<ResumeProject>()
                .HasMany(rp => rp.Industries)
                .WithMany(ts => ts.ResumeProjects)
                .Map(m =>
                {
                    m.ToTable("ResumeProjectIndustry", "Resumes");
                    m.MapLeftKey("ResumeProjectId");
                    m.MapRightKey("IndustryId");
                });

            modelBuilder.Entity<Project>()
                .HasMany(a => a.TimeReportAcceptingPersons)
                .WithMany(p => p.AcceptingPersonProjects)
                .Map(p =>
                {
                    p.ToTable("ProjectAcceptingPersons", "TimeTracking");
                    p.MapLeftKey("ProjectId");
                    p.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<Request>()
                .HasMany(r => r.Watchers)
                .WithMany(w => w.WatchingRequests)
                .Map(p =>
                {
                    p.ToTable("RequestWatchers", "Workflows");
                    p.MapLeftKey("RequestId");
                    p.MapRightKey("UserId");
                });

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.SkillChangeRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.AddEmployeeRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.AbsenceRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.AddHomeOfficeRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.OvertimeRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.ProjectTimeReportApprovalRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.EmploymentTerminationRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.EmployeeDataChangeRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.ReopenTimeTrackingReportRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.EmploymentDataChangeRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.BusinessTripRequest)
                .WithRequired(r => r.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.AdvancedPaymentRequest)
                .WithRequired(r => r.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.OnboardingRequest)
                .WithRequired(r => r.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.AssetsRequest)
                .WithRequired(r => r.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
               .HasOptional(r => r.FeedbackRequest)
               .WithRequired(r => r.Request)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
               .HasOptional(r => r.BusinessTripSettlementRequest)
               .WithRequired(r => r.Request)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
               .HasOptional(r => r.BonusRequest)
               .WithRequired(w => w.Request)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Request>()
                .HasOptional(r => r.ExpenseRequest)
                .WithRequired(w => w.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<RequestDocumentContent>()
                .HasRequired(e => e.RequestDocument)
                .WithRequiredDependent(c => c.DocumentContent)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripRequest>()
                .HasOptional(e => e.BusinessTrip)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripSettlementRequest>()
                .HasMany(e => e.AbroadTimeEntries)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripSettlementRequest>()
                .HasMany(e => e.Meals)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripSettlementRequest>()
                .HasMany(e => e.OwnCosts)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripSettlementRequest>()
                .HasMany(e => e.CompanyCosts)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripSettlementRequest>()
                .HasMany(e => e.AdditionalAdvancePayments)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripSettlementRequest>()
                .HasOptional(e => e.PrivateVehicleMileage)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<TimeReportRow>()
                .HasMany(r => r.OvertimeBanks)
                .WithOptional(w => w.TimeReportRow)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<User>()
               .HasMany(u => u.AbsenceRequests)
               .WithRequired(r => r.AffectedUser);

            modelBuilder.Entity<TimeReport>()
                .HasMany(r => r.OvertimeBanks)
                .WithOptional(w => w.TimeReport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProjectSetup>()
                .HasMany(e => e.InvoicingRates)
                .WithRequired(e => e.ProjectSetup)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(r => r.Attributes)
                .WithMany(w => w.UsedInProjects)
                .Map(p =>
                {
                    p.ToTable("ProjectAttributesMap", "TimeTracking");
                    p.MapLeftKey("ProjectId");
                    p.MapRightKey("AttributeId");
                });

            modelBuilder.Entity<TimeReportRow>()
                .HasMany(r => r.AttributeValues)
                .WithMany(r => r.UsedInTimeReportRows)
                .Map(p =>
                {
                    p.ToTable("TimeReportRowAttributeValuesMap", "TimeTracking");
                    p.MapLeftKey("TimeReportRowId");
                    p.MapRightKey("AttributeValueId");
                });

            modelBuilder.Entity<JobProfile>()
               .HasMany(s => s.SkillTags)
               .WithRequired(s => s.JobProfile)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<SkillChangeRequest>()
                .HasMany(a => a.Skills)
                .WithMany()
                .Map(p =>
                {
                    p.ToTable("SkillChangeRequestSkillMap", "Workflows");
                    p.MapLeftKey("SkillChangeRequestId");
                    p.MapRightKey("SkillId");
                });

            modelBuilder.Entity<ReopenTimeTrackingReportRequest>()
                .HasMany(a => a.Projects)
                .WithMany()
                .Map(p =>
                {
                    p.ToTable("ReopenTimeTrackingReportRequestProjectsMap", "Workflows");
                    p.MapLeftKey("ReopenTimeTrackingReportRequestId");
                    p.MapRightKey("ProjectId");
                });

            modelBuilder.Entity<EmployeeDataChangeRequest>()
                .HasMany(r => r.Employees)
                .WithMany()
                .Map(t =>
                {
                    t.ToTable("EmployeeDataChangeRequestEmployeesMap", "Workflows");
                    t.MapLeftKey("EmployeeDataChangeRequestId");
                    t.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<BusinessTripRequest>()
                .HasMany(r => r.Projects)
                .WithMany()
                .Map(t =>
                {
                    t.ToTable("BusinessTripRequestProjectsMap", "Workflows");
                    t.MapLeftKey("BusinessTripRequestId");
                    t.MapRightKey("ProjectId");
                });

            modelBuilder.Entity<BusinessTripRequest>()
                .HasMany(r => r.Participants)
                .WithMany()
                 .Map(t =>
                 {
                     t.ToTable("BusinessTripRequestEmployeesMap", "Workflows");
                     t.MapLeftKey("BusinessTripRequestId");
                     t.MapRightKey("EmployeeId");
                 });

            modelBuilder.Entity<BusinessTrip>()
                .HasMany(r => r.Projects)
                .WithMany()
                .Map(t =>
                {
                    t.ToTable("BusinessTripProjectsMap", "BusinessTrips");
                    t.MapLeftKey("BusinessTripId");
                    t.MapRightKey("ProjectId");
                });

            modelBuilder.Entity<BusinessTrip>()
                .HasOptional(e => e.BusinessTripAcceptanceConditions)
                .WithRequired(e => e.BusinessTrip)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripRequest>()
                .HasMany(r => r.DepartureCities)
                .WithMany(c => c.UsedInBusinessTripRequestAsDeparture)
                .Map(t =>
                {
                    t.ToTable("BusinessTripRequestDepartureCityMap", "Workflows");
                    t.MapLeftKey("BusinessTripRequestId");
                    t.MapRightKey("CityId");
                });

            modelBuilder.Entity<AssetsRequest>()
                .HasMany(a => a.AssetTypes)
                .WithMany(p => p.AssetsRequests)
                .Map(p =>
                {
                    p.ToTable("AssetsRequestAssetType", "Workflows");
                    p.MapLeftKey("AssetsRequestId");
                    p.MapRightKey("AssetTypeId");
                });

            modelBuilder.Entity<Arrangement>()
               .HasMany(u => u.Participants)
               .WithMany(p => p.Arrangements)
               .Map(p =>
               {
                   p.ToTable("ArrangementParticipants", "BusinessTrips");
                   p.MapLeftKey("ParticipantId");
                   p.MapRightKey("ArrangementId");
               });

            modelBuilder.Entity<JobOpening>()
                .HasMany(o => o.JobProfiles)
                .WithMany()
                .Map(p =>
                {
                    p.ToTable("JobOpeningJobProfile", "Recruitment");
                    p.MapLeftKey("JobOpeningId");
                    p.MapRightKey("JobProfileId");
                });

            modelBuilder.Entity<JobOpening>()
               .HasMany(o => o.Cities)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobOpeningCities", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("LocationId");
               });

            modelBuilder.Entity<JobOpening>()
               .HasMany(o => o.Recruiters)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobOpeningRecruiters", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<JobOpening>()
               .HasMany(o => o.RecruitmentOwners)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobOpeningRecruitmentOwners", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<JobOpening>()
               .HasMany(o => o.TechnicalReviewers)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobOpeningTechnicalReviewers", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<Candidate>()
               .HasMany(o => o.Cities)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("CandidateCities", "Recruitment");
                   p.MapLeftKey("CandidateId");
                   p.MapRightKey("CandidateLocationId");
               });

            modelBuilder.Entity<Candidate>()
               .HasMany(o => o.JobProfiles)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("CandidateJobProfiles", "Recruitment");
                   p.MapLeftKey("CandidateId");
                   p.MapRightKey("JobProfileId");
               });

            modelBuilder.Entity<CandidateContactRecord>()
               .HasMany(o => o.RelevantJobOpenings)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("ContactRecordRelevantJobOpenings", "Recruitment");
                   p.MapLeftKey("ContactRecordId");
                   p.MapRightKey("RelevantJobOpeningId");
               });

            modelBuilder.Entity<ActiveDirectoryGroup>()
                .HasMany(g => g.Subgroups)
                .WithMany(g => g.ParentGroups)
                .Map(c =>
                {
                    c.ToTable("ActiveDirectorySubgroups", "Allocation");
                    c.MapLeftKey("ParentActiveDirectoryGroupId");
                    c.MapRightKey("ChildActiveDirectoryGroupId");
                });

            modelBuilder.Entity<TechnicalReview>()
                .HasMany(g => g.JobProfiles)
                .WithMany()
                .Map(c =>
                {
                    c.ToTable("TechnicalReviewJobProfiles", "Recruitment");
                    c.MapLeftKey("TechnicalReviewId");
                    c.MapRightKey("JobProfileId");
                });

            modelBuilder.Entity<Company>()
               .HasMany(u => u.TravelExpensesSpecialists)
               .WithMany(p => p.TravelExpensesSpecialistInCompanies)
               .Map(p =>
               {
                   p.ToTable("CompaniesTravelExpensesSpecialists", "Organization");
                   p.MapLeftKey("CompanyId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<JobOpening>()
               .HasMany(u => u.Watchers)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobOpeningWatchers", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<Candidate>()
               .HasMany(u => u.Watchers)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("CandidateWatchers", "Recruitment");
                   p.MapLeftKey("CandidateId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<InboundEmail>()
               .HasMany(u => u.Readers)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("InboundEmailReaders", "Recruitment");
                   p.MapLeftKey("InboundEmailId");
                   p.MapRightKey("UserId");
               });

            modelBuilder.Entity<RecruitmentProcess>()
               .HasMany(u => u.Watchers)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("RecruitmentProcessWatchers", "Recruitment");
                   p.MapLeftKey("RecruitmentProcesId");
                   p.MapRightKey("EmployeeId");
               });


            modelBuilder.Entity<RecruitmentProcessStep>()
               .HasMany(o => o.OtherAttendingEmployees)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("RecruitmentProcessStepOtherAttendingEmployees", "Recruitment");
                   p.MapLeftKey("RecruitmentProcessStepId");
                   p.MapRightKey("EmployeeId");
               });

            modelBuilder.Entity<JobApplication>()
               .HasMany(u => u.Cities)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobApplicationCities", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("LocationId");
               });

            modelBuilder.Entity<JobApplication>()
               .HasMany(u => u.JobProfiles)
               .WithMany()
               .Map(p =>
               {
                   p.ToTable("JobApplicationJobProfiles", "Recruitment");
                   p.MapLeftKey("JobOpeningId");
                   p.MapRightKey("JobProfileId");
               });

            modelBuilder.Entity<ReportDefinition>()
                .HasMany(s => s.AllowedForProfiles)
                .WithMany()
                .Map(m =>
                {
                    m.MapLeftKey("AllowedForProfileId");
                    m.MapRightKey("ReportDefinitionId");
                    m.ToTable("ReportDefinitionAllowedForProfiles", "Reporting");
                });

            modelBuilder.Entity<FeedbackRequest>()
                .HasMany(f => f.EvaluatorEmployees)
                .WithMany(e => e.FeedbackRequests)
                .Map(e =>
                {
                    e.ToTable("FeedbackRequestEvaluators", "Workflow");
                    e.MapLeftKey("FeedbackRequestId");
                    e.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<BusinessTripMessage>()
                .HasMany(m => m.MentionedEmployees)
                .WithMany(e => e.MentionedInBusinessTripMessages)
                .Map(e =>
                {
                    e.ToTable("BusinessTripMessageMentions", "Workflow");
                    e.MapLeftKey("BusinessTripMessageId");
                    e.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<JobOpeningNote>()
                .HasMany(m => m.MentionedEmployees)
                .WithMany()
                .Map(e =>
                {
                    e.ToTable("JobOpeningNoteMessageMentions", "Recruitment");
                    e.MapLeftKey("JobOpeningNoteId");
                    e.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<CandidateNote>()
                .HasMany(m => m.MentionedEmployees)
                .WithMany()
                .Map(e =>
                {
                    e.ToTable("CandidateNoteMessageMentions", "Recruitment");
                    e.MapLeftKey("CandidateNoteId");
                    e.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<RecommendingPersonNote>()
                .HasMany(m => m.MentionedEmployees)
                .WithMany()
                .Map(e =>
                {
                    e.ToTable("RecommendingPersonNoteMessageMentions", "Recruitment");
                    e.MapLeftKey("RecommendingPersonNoteId");
                    e.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<RecruitmentProcessNote>()
                .HasMany(m => m.MentionedEmployees)
                .WithMany()
                .Map(e =>
                {
                    e.ToTable("RecruitmentProcessNoteMessageMentions", "Recruitment");
                    e.MapLeftKey("RecruitmentProcessNoteId");
                    e.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<Request>()
               .HasOptional(r => r.TaxDeductibleCostRequest)
               .WithRequired(r => r.Request)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Candidate>()
                .HasMany(u => u.Favorites)
                .WithMany()
                .Map(p =>
                {
                    p.ToTable("CandidateFavorites", "Recruitment");
                    p.MapLeftKey("CandidateId");
                    p.MapRightKey("EmployeeId");
                });

            modelBuilder.Entity<KpiDefinition>()
                .HasMany(a => a.Roles)
                .WithMany(p => p.KpiDefinitions)
                .Map(p =>
                {
                    p.ToTable("KpiDefinitionSecurityRoles", "KPI");
                    p.MapLeftKey("KpiDefinitionId");
                    p.MapRightKey("SecurityRoleId");
                });

            ManyToMany<DataProcessingAgreement, User>(
                modelBuilder,
                "GDPR",
                nameof(DataProcessingAgreement),
                nameof(DataProcessingAgreement.RequiredUsers),
                a => a.RequiredUsers);

            ManyToMany<DataProcessingAgreement, Project>(
                modelBuilder,
                "GDPR",
                nameof(DataProcessingAgreement),
                nameof(DataProcessingAgreement.Projects),
                a => a.Projects);
        }

        private static void ManyToMany<TEntity, TCollection>(
            DbModelBuilder modelBuilder,
            string module,
            string entityName,
            string collectionName,
            Expression<Func<TEntity, ICollection<TCollection>>> collectionSelector)
            where TEntity : class, IEntity
            where TCollection : class, IEntity
        {
            modelBuilder.Entity<TEntity>()
                .HasMany(collectionSelector)
                .WithMany()
                .Map(e =>
                {
                    e.ToTable($"{entityName}{collectionName}Relation", module);
                    e.MapLeftKey(entityName + nameof(IEntity.Id));
                    e.MapRightKey(collectionName + nameof(IEntity.Id));
                });
        }

        private void FixRelations(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(e => e.ModifiedByUsers)
                .WithOptional(e => e.ModifiedBy)
                .HasForeignKey(e => e.ModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ImpersonatedByUsers)
                .WithOptional(e => e.ImpersonatedBy)
                .HasForeignKey(e => e.ImpersonatedById);

            modelBuilder.Entity<Credential>()
                .HasRequired(e => e.User)
                .WithMany(c => c.Credentials);

            modelBuilder.Entity<UserDocument>()
                .HasRequired(e => e.User)
                .WithMany(c => c.Documents);

            modelBuilder.Entity<Employee>()
                .HasOptional(e => e.LineManager).WithMany();

            modelBuilder.Entity<Employee>()
                .HasRequired(e => e.OrgUnit).WithMany();

            modelBuilder.Entity<Calendar>()
                .HasMany(c => c.Entries)
                .WithRequired(c => c.Calendar)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<TimeReportDailyEntry>()
                .HasRequired(e => e.TimeReportRow)
                .WithMany(r => r.DailyEntries)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<TimeReportRow>()
                .HasRequired(e => e.TimeReport)
                .WithMany(r => r.Rows)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ClockCardDailyEntry>()
                .HasRequired(e => e.TimeReport)
                .WithMany(r => r.ClockCardDailyEntries)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<SurveyDefinition>()
                .HasMany(s => s.SurveyQuestions)
                .WithRequired(s => s.SurveyDefinition)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Survey>()
                .HasMany(s => s.Answers)
                .WithRequired(s => s.Survey);

            modelBuilder.Entity<ReportTemplateContent>()
                .HasRequired(e => e.ReportTemplate)
                .WithRequiredDependent(c => c.DocumentContent)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ProjectSetup>()
                .HasRequired(p => p.OrgUnit)
                .WithMany();

            modelBuilder.Entity<ApprovalGroup>()
                .HasRequired(e => e.Request)
                .WithMany(e => e.ApprovalGroups)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Approval>()
                .HasRequired(e => e.ApprovalGroup)
                .WithMany(e => e.Approvals)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ActionSet>()
                .HasMany(s => s.TriggeringRequestStatuses)
                .WithRequired(s => s.ActionSet)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<TimeReportProjectAttribute>()
                .HasMany(s => s.Values)
                .WithRequired(s => s.Attribute)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Country>()
                .HasMany(c => c.Cities)
                .WithRequired(c => c.Country);

            modelBuilder.Entity<City>()
                .HasMany(c => c.Locations)
                .WithOptional(l => l.City);

            modelBuilder.Entity<EmployeePhotoDocument>()
                .HasOptional(c => c.DocumentContent)
                .WithRequired(l => l.EmployeePhotoDocument)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<PayrollDocument>()
                .HasOptional(c => c.DocumentContent)
                .WithRequired(l => l.PayrollDocument)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ForeignEmployeeDocument>()
                .HasOptional(c => c.DocumentContent)
                .WithRequired(l => l.ForeignEmployeeDocument)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripParticipant>()
                .HasMany(p => p.ArrangementTrackingEntries)
                .WithRequired(t => t.Participant)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<BusinessTripParticipant>()
                .HasMany(p => p.SettlementRequests)
                .WithRequired(t => t.BusinessTripParticipant)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DailyAllowance>()
                .HasOptional(a => a.TravelCountry)
                .WithMany(c => c.DailyAllowances);

            modelBuilder.Entity<DailyAllowance>()
                .HasOptional(a => a.TravelCity)
                .WithMany(c => c.DailyAllowances);

            modelBuilder.Entity<DataOwner>()
               .HasMany(dc => dc.DataConsents)
               .WithRequired(d => d.DataOwner)
               .HasForeignKey(d => d.DataOwnerId)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<DataConsent>()
                .HasMany(dc => dc.Attachments)
                .WithRequired(d => d.DataConsent)
                .HasForeignKey(d => d.DataConsentId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DataOwner>()
                .HasMany(dc => dc.DataActivity)
                .WithRequired(d => d.DataOwner)
                .HasForeignKey(d => d.DataOwnerId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DataConsentDocument>()
                .HasRequired(dc => dc.DocumentContent)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<InvoiceDocumentContent>()
                .HasRequired(e => e.InvoiceDocument)
                .WithRequiredDependent(c => c.DocumentContent)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DataProcessingAgreementDocument>()
                .HasRequired(e => e.Agreement)
                .WithMany(c => c.Documents)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DataProcessingAgreementDocumentContent>()
                .HasRequired(e => e.Document)
                .WithRequiredDependent(c => c.DocumentContent)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DataProcessingAgreement>()
                .HasMany(e => e.Consents)
                .WithRequired(c => c.Agreement)
                .WillCascadeOnDelete(true);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        private void RegisterConventions(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties()
                .Where(x => x.GetCustomAttributes(false).OfType<NonUnicodeAttribute>().Any())
                .Configure(c => c.IsUnicode(false));

            modelBuilder.Properties()
                .Where(x => x.GetCustomAttributes(false).OfType<EntityReferenceAttribute>().Any())
                .Configure(
                    c =>
                        c.HasColumnName(
                            ObjectReferenceHelper.GetDbColumnNameByConvention(
                                c.ClrPropertyInfo.GetCustomAttribute<EntityReferenceAttribute>(), c.ClrPropertyInfo)
                            ));

            modelBuilder.Properties()
                .Where(x => x.GetCustomAttributes(false).OfType<EnumReferenceAttribute>().Any())
                .Configure(
                    c =>
                        c.HasColumnName(
                            ObjectReferenceHelper.GetDbColumnNameByConvention(
                                c.ClrPropertyInfo.GetCustomAttribute<EnumReferenceAttribute>(), c.ClrPropertyInfo)
                            ));

            modelBuilder.Entity<MeetingAttachmentContent>()
                .HasRequired(e => e.MeetingAttachment)
                .WithRequiredDependent(c => c.DocumentContent)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<MeetingNoteAttachmentContent>()
                .HasRequired(e => e.MeetingNoteAttachment)
                .WithRequiredDependent(c => c.DocumentContent)
                .WillCascadeOnDelete(true);
        }
    }
}
