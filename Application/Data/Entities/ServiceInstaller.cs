﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Services;

namespace Smt.Atomic.Data.Entities
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                case ContainerType.WebApi:
                case ContainerType.WebServices:
                    container.Register(Component.For<IDbContextWrapper>().ImplementedBy<DbContextWrapper>().LifestyleTransient());
                    break;
                case ContainerType.JobScheduler:
                    container.Register(Component.For<IDbContextWrapper>().ImplementedBy<DbContextWrapper>().LifestyleTransient());
                    break;
            }
        }
    }
}