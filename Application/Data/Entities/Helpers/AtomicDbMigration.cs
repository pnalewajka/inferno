﻿using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using Smt.Atomic.Data.Common.Helpers;
using Smt.Atomic.Data.Entities.Operations;

namespace Smt.Atomic.Data.Entities.Helpers
{
    public abstract class AtomicDbMigration : DbMigration
    {
        protected void CreateFullTextIndex(string schema, string table, string column, string uniqueIndex = null)
        {
            ((IDbMigration)this).AddOperation(new CreateFullTextIndexOperation(schema, table, column, uniqueIndex));
        }

        public void SqlBatchFromResources(string resourceName)
        {
            MigrationHelper.ExecuteFromResources(GetType(), resourceName, s => Sql(s));
        }
    }
}