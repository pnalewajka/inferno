using System.Transactions;

namespace Smt.Atomic.Data.Entities.Helpers
{
    public static class TransactionHelper
    {
        public static TransactionScope CreateDefaultTransactionScope()
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            };

            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }
    }
}