﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Castle.Core.Internal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Helpers
{
    /// <summary>
    /// Support methods for entity 
    /// </summary>
    public static class EntityHelper
    {
        /// <summary>
        /// Get sql table name from TableAttribute on the specific TEntity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public static string GetTableName<TEntity>() where TEntity : IEntity
        {
            return AttributeHelper.GetClassAttribute<TableAttribute>(typeof(TEntity)).Name;
        }

        /// <summary>
        /// Get sql table schema name from TableAttribute on the specific TEntity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public static string GetTableSchema<TEntity>() where TEntity : IEntity
        {
            return AttributeHelper.GetClassAttribute<TableAttribute>(typeof(TEntity)).Schema;
        }

        /// <summary>
        /// Get collection of PropertyGroups with Properties from inputEntity with Index attribute with IsUnique=true parameter. If Index attribute has IsClustered=true parameter it will add clustered parameters to one group with common name.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="inputEntity"></param>
        /// <returns></returns>
        public static IEnumerable<PropertyGroup> GetUniqueKeys<TEntity>(TEntity inputEntity) where TEntity : IEntity
        {
            var entityType = inputEntity.GetType();
            var propertiesWithIndexAttribute = TypeHelper.GetPublicInstancePropertiesWithAttribute<IndexAttribute>(entityType);
            var multiKeyGroups = new Dictionary<string, PropertyGroup>();
            var singleKeyGroups = new List<PropertyGroup>();

            foreach (var property in propertiesWithIndexAttribute)
            {
                var indexAttribute = AttributeHelper.GetPropertyAttribute<IndexAttribute>(property);

                if (indexAttribute.IsUnique)
                {
                    if (indexAttribute.IsClustered)
                    {
                        var groupAlreadyExists = multiKeyGroups.ContainsKey(indexAttribute.Name);

                        if (!groupAlreadyExists)
                        {
                            multiKeyGroups[indexAttribute.Name] = new PropertyGroup();
                        }

                        multiKeyGroups[indexAttribute.Name].PropertyNames.Add(property.Name);
                    }
                    else
                    {
                        var group = new PropertyGroup();
                        group.PropertyNames.Add(property.Name);
                        singleKeyGroups.Add(group);
                    }
                }
            }

            return multiKeyGroups.Values.Concat(singleKeyGroups);
        }

        /// <summary>
        /// Get collection of Custom properties validation from inputEntity with Custom index attribute with IsUnique=true parameter.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="inputEntity"></param>
        /// <returns></returns>
        public static IEnumerable<PropertyValidation> GetCustomUniqueKeys<TEntity>(TEntity inputEntity) where TEntity : IEntity
        {
            var entityType = inputEntity.GetType();
            var propertiesWithIndexAttribute = TypeHelper.GetPublicInstancePropertiesWithAttribute<CustomIndexAttribute>(entityType);
            var singleKeyGroups = new List<PropertyValidation>();

            foreach (var property in propertiesWithIndexAttribute)
            {
                var indexAttribute = AttributeHelper.GetPropertyAttribute<CustomIndexAttribute>(property);

                if (indexAttribute != null && indexAttribute.ShouldBeUnique(inputEntity))
                {
                    var group = new PropertyValidation(property.Name, indexAttribute.UniqueValidatation);
                    singleKeyGroups.Add(group);
                }
            }

            return singleKeyGroups;
        }

        /// <summary>
        /// Retrieves max length of the given entity field based on the MaxLengthAttribute
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="propertyNameExpression"></param>
        /// <returns>Max field length</returns>
        public static int GetFieldMaxLength<TEntity>(Expression<Func<TEntity, object>> propertyNameExpression) where TEntity : IEntity
        {
            var propertyInfo = PropertyHelper.GetProperty<TEntity>(propertyNameExpression);
            var maxLength = AttributeHelper.GetPropertyAttribute<MaxLengthAttribute>(propertyInfo).Length;

            return maxLength;
        }

        public static void ShallowCopy<TEntity>(TEntity source, TEntity destination, HashSet<string> propertiesToInclude = null) where TEntity : IEntity
        {
            foreach (var propertyInfo in TypeHelper.GetPublicInstanceProperties(typeof(TEntity)))
            {
                if (propertyInfo.CanWrite && propertyInfo.CanRead
                    && (propertiesToInclude == null || propertiesToInclude.Contains(propertyInfo.Name)))
                {
                    var isTimestamp = propertyInfo.GetAttribute<TimestampAttribute>() != null;

                    var dbValue = propertyInfo.GetValue(destination);
                    var sourceValue = propertyInfo.GetValue(source);

                    if (isTimestamp && sourceValue != null && dbValue != null)
                    {
                        ArrayHelper.Rewrite(dbValue as byte[], sourceValue as byte[]);
                    }
                    else if (dbValue != sourceValue)
                    {
                        var propertyType = propertyInfo.PropertyType;

                        var isNavigationProperty = typeof(IEntity).IsAssignableFrom(propertyType)
                                                   || typeof(IEnumerable<IEntity>).IsAssignableFrom(propertyType);

                        if (!isNavigationProperty)
                        {
                            propertyInfo.SetValue(destination, sourceValue);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clone entity, without any technical properties
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns>Cloned entity</returns>
        public static TEntity ShallowClone<TEntity>(TEntity entity) where TEntity : IEntity
        {
            var type = typeof(TEntity);
            var newEntity = Activator.CreateInstance(type);

            var propertiesToSkip = GetPropertiesToSkip(type);

            foreach (var property in entity.GetType().GetProperties())
            {
                if (property.GetGetMethod().IsVirtual || propertiesToSkip.Contains(property))
                {
                    continue;
                }

                property.SetValue(newEntity, property.GetValue(entity));
            }

            return (TEntity)newEntity;
        }

        /// <summary>
        /// Creates new entities based on the ids
        /// </summary>
        public static ICollection<TOut> CreateForeignEntities<TOut>(IEnumerable<long> ids)
            where TOut : IEntity, new()
        {
            return ids == null
                ? new List<TOut>()
                : ids.Select(i => new TOut { Id = i }).ToList();
        }

        /// <summary>
        /// Creates new entities based on the dtos and mapping
        /// </summary>
        public static ICollection<TOut> CreateForeignEntities<TSource, TOut>(IEnumerable<TSource> source, IClassMapping<TSource, TOut> mapping)
            where TOut : class, IEntity
            where TSource : class
        {
            return source == null
                ? new List<TOut>()
                : source.Select(mapping.CreateFromSource).ToList();
        }

        private static PropertyInfo[] GetPropertiesToSkip(Type enitityType)
        {
            if (enitityType.IsSubclassOf(typeof(TrackedEntity)))
            {
                return typeof(TrackedEntity).GetProperties();
            }

            if (enitityType.IsSubclassOf(typeof(ModificationTrackedEntity)))
            {
                return typeof(ModificationTrackedEntity).GetProperties();
            }

            if (enitityType.IsSubclassOf(typeof(CreationTrackedEntity)))
            {
                return typeof(ModificationTrackedEntity).GetProperties();
            }

            if (enitityType.IsSubclassOf(typeof(SimpleEntity)))
            {
                return typeof(ModificationTrackedEntity).GetProperties();
            }

            if (enitityType.IsSubclassOf(typeof(ISoftDeletable)))
            {
                return typeof(ISoftDeletable).GetProperties()
                    .Union(typeof(IEntity).GetProperties())
                    .ToArray();
            }

            return typeof(IEntity).GetProperties();
        }
    }
}
