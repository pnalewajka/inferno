﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Helpers
{
    public static class EntityExtensions
    {
        public static long[] GetIds<T>(this IEnumerable<T> records) where T : IEntity
        {
            return records.Select(r => r.Id).ToArray();
        }

        private static DbEntityEntry<TEntity> GetDbEntry<TEntity>(this TEntity entity)
            where TEntity : class, IEntity
        {
            var field = entity?.GetType().GetField("_entityWrapper");
            var wrapper = field?.GetValue(entity);
            var property = wrapper.GetType().GetProperty("Context");
            var context = (ObjectContext)property.GetValue(wrapper, null);
            var dbContext = context?.TransactionHandler?.DbContext;

            if (dbContext == null)
            {
                throw new InvalidOperationException("Unable to retrieve the DbContext from a detached entity");
            }

            return dbContext.Entry(entity);
        }


        /// <summary>
        /// Gets the Query to populate a navigation property
        /// </summary>
        public static IQueryable<TElement> Repository<TEntity, TElement>(
            this TEntity entity,
            Expression<Func<TEntity, ICollection<TElement>>> navigation)
            where TEntity : class, IEntity
            where TElement : class
        {
            var dbEntry = entity.GetDbEntry();
            var collection = dbEntry.Collection(navigation);
            var isLoaded = collection.IsLoaded;

            return isLoaded
                ? collection.CurrentValue.AsQueryable()
                : collection.Query();
        }

        /// <summary>
        /// Loads a navigation property
        /// </summary>
        public static void Load<TEntity, TElement>(
            this TEntity entity,
            Expression<Func<TEntity, ICollection<TElement>>> navigation,
            Func<IQueryable<TElement>, IQueryable<TElement>> configureIncludes = null)
            where TEntity : class, IEntity
            where TElement : class
        {
            var dbEntry = entity.GetDbEntry();
            var collection = dbEntry.Collection(navigation);

            if (collection.IsLoaded)
            {
                return;
            }

            var query = collection.Query();

            if (configureIncludes != null)
            {
                query = configureIncludes(query);
            }

            collection.CurrentValue = query.ToList();
            collection.IsLoaded = true;
        }

        /// <summary>
        /// Loads a navigation property
        /// </summary>
        public static void Load<TEntity, TElement>(
            this TEntity entity,
            Expression<Func<TEntity, TElement>> navigation,
            Func<IQueryable<TElement>, IQueryable<TElement>> configureIncludes = null)
            where TEntity : class, IEntity
            where TElement : class, IEntity
        {
            var dbEntry = entity.GetDbEntry();
            var reference = dbEntry.Reference(navigation);

            if (reference.IsLoaded)
            {
                return;
            }

            var query = reference.Query();

            if (configureIncludes != null)
            {
                query = configureIncludes(query);
            }

            reference.CurrentValue = query.FirstOrDefault();
            reference.IsLoaded = true;
        }
    }
}