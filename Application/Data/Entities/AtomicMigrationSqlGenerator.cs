﻿using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;
using System.IO;
using Smt.Atomic.Data.Entities.Operations;

namespace Smt.Atomic.Data.Entities.Generators
{
    public class AtomicMigrationSqlGenerator
        : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(MigrationOperation operation)
        {
            if (operation is AtomicMigrationOperation atomicOperation)
            {
                using (var writer = new StringWriter())
                {
                    atomicOperation.Generate(writer);
                    var sql = writer.ToString();
                    Statement(sql, atomicOperation.TransactionalBehavior == TransactionalBehavior.DoNotEnsureTransaction);
                }

                return;
            }

            base.Generate(operation);
        }
    }
}
