﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.BusinessLogic
{
    public static class ProjectBusinessLogic
    {
        private const string ProjectNameSeparator = " / ";

        public static readonly BusinessLogic<Project, string> ProjectName = new BusinessLogic<Project, string>(
            p => p.ProjectSetup.GenericName != null
                ? p.ProjectSetup.GenericName
                : p.SubProjectShortName != null
                    ? p.ProjectSetup.ProjectShortName + ProjectNameSeparator + p.SubProjectShortName
                    : p.ProjectSetup.ProjectShortName);

        public static readonly BusinessLogic<Project, string> ClientProjectName = new BusinessLogic<Project, string>(
            p => p.ProjectSetup.GenericName != null
                ? p.ProjectSetup.GenericName
                : p.ProjectSetup.ClientShortName + ProjectNameSeparator + ProjectName.Call(p));

        public static readonly BusinessLogic<Project, bool> IsProjectWithKey = new BusinessLogic<Project, bool>(
            project => !string.IsNullOrEmpty(project.JiraIssueKey) && !project.Apn.Contains("n/a") && project.UtilizationCategoryId != null);

        public static readonly BusinessLogic<Project, long, IEnumerable<long>, bool> IsOwnProjectForEmployee =
            new BusinessLogic<Project, long, IEnumerable<long>, bool>((project, employeeId, managerOrgUnitsIds) =>
                project.ProjectSetup != null && (project.ProjectSetup.ProjectManagerId.HasValue && project.ProjectSetup.ProjectManagerId == employeeId
                || (project.ProjectSetup.OrgUnit != null && managerOrgUnitsIds.Any(id => id == project.ProjectSetup.OrgUnitId))));

        public static readonly BusinessLogic<Project, bool> IsSubProject =
            new BusinessLogic<Project, bool>(project => !project.IsMainProject);

        public static readonly BusinessLogic<Project, DateTime, bool> IsActiveOnDate =
            new BusinessLogic<Project, DateTime, bool>((project, date) => !project.EndDate.HasValue || project.EndDate.Value >= date);
    }
}
