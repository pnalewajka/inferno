﻿using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Attributes
{
    public class SoftDeletableIndexAttribute : CustomIndexAttribute
    {
        public SoftDeletableIndexAttribute(bool IsUnique) : base(IsUnique, e => e is ISoftDeletable soft && !soft.IsDeleted)
        {
        }
    }
}
