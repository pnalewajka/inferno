﻿using System;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class CustomIndexAttribute : Attribute
    {
        protected bool IsUnique { get; }
        public Func<IEntity, bool> UniqueValidatation { get; }

        protected CustomIndexAttribute(bool isUnique, Func<IEntity, bool> uniqueValidatation)
        {
            UniqueValidatation = uniqueValidatation;
            IsUnique = isUnique;
        }

        public bool ShouldBeUnique(IEntity entity) => IsUnique && UniqueValidatation.Invoke(entity);
    }
}
