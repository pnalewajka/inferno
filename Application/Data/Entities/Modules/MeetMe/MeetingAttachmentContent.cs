﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("MeetingAttachmentContent", Schema = "MeetMe")]
    public class MeetingAttachmentContent : IEntity
    {
        [Key]
        [ForeignKey("MeetingAttachment")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual MeetingAttachment MeetingAttachment { get; set; }
    }
}

