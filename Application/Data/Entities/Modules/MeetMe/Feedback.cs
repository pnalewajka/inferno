﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("Feedback", Schema = "MeetMe")]
    public class Feedback : ModificationTrackedEntity
    {
        [EntityReference(typeof(Employee))]
        public long AuthorId { get; set; }

        [ForeignKey(nameof(AuthorId))]
        public virtual Employee Author { get; set; }

        [EntityReference(typeof(Employee))]
        public long ReceiverId { get; set; }

        [ForeignKey(nameof(ReceiverId))]
        public virtual Employee Receiver { get; set; }

        [StringLength(4000)]
        public string FeedbackContent { get; set; }

        public FeedbackVisbility Visibility { get; set; }
    }
}

