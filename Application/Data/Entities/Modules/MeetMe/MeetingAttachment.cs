﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("MeetingAttachment", Schema = "MeetMe")]
    public class MeetingAttachment : DocumentBase<MeetingAttachmentContent>
    {
        [Column("Meeting_Id")]
        public long? MeetingId { get; set; }

        [ForeignKey(nameof(MeetingId))]
        public Meeting Meeting { get; set; }
    }
}
