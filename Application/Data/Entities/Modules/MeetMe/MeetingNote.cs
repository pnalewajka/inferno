﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("MeetingNote", Schema = "MeetMe")]
    public class MeetingNote : ModificationTrackedEntity
    {
        public MeetingNote()
        {
            Attachments = new List<MeetingNoteAttachment>();
        }

        public long MeetingId { get; set; }

        [ForeignKey(nameof(MeetingId))]
        public virtual Meeting Meeting { get; set; }

        [EntityReference(typeof(Employee))]
        public long AuthorId { get; set; }

        [ForeignKey(nameof(AuthorId))]
        public virtual Employee Employee { get; set; }        

        [StringLength(4000)]
        public string NoteBody { get; set; }
        
        public virtual ICollection<MeetingNoteAttachment> Attachments { get; set; }

        public DateTime CreatedOn { get; set; }

        public NoteStatus Status { get; set; }
    }
}

