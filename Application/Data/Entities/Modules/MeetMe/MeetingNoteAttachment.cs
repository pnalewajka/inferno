﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("MeetingNoteAttachment", Schema = "MeetMe")]
    public class MeetingNoteAttachment : DocumentBase<MeetingNoteAttachmentContent>
    {
        [Column("MeetingNote_Id")]
        public long? MeetingNoteId { get; set; }

        [ForeignKey(nameof(MeetingNoteId))]
        public MeetingNote MeetingNote { get; set; }
    }
}