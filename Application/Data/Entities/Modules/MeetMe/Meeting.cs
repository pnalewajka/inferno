﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("Meeting", Schema = "MeetMe")]
    public class Meeting : ModificationTrackedEntity
    {

        public Meeting()
        {
            Attachments = new List<MeetingAttachment>();
        }

        [EntityReference(typeof(Employee))]
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public MeetingStatus Status { get; set; }

        public DateTime? MeetingDueDate { get; set; }

        public DateTime? ScheduledMeetingDate { get; set; }

        [InverseProperty(nameof(MeetingNote.Meeting))]
        public virtual ICollection<MeetingNote> Notes { get; set; }

        [InverseProperty(nameof(MeetingAttachment.Meeting))]
        public virtual ICollection<MeetingAttachment> Attachments { get; set; }
    }
}

