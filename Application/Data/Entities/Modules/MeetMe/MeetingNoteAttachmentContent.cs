﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.MeetMe
{
    [Table("MeetingNoteAttachmentContent", Schema = "MeetMe")]
    public class MeetingNoteAttachmentContent : IEntity
    {
        [Key]
        [ForeignKey("MeetingNoteAttachment")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual MeetingNoteAttachment MeetingNoteAttachment { get; set; }
    }
}

