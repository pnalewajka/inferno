﻿namespace Smt.Atomic.Data.Entities.Modules.Finance
{
    using System.ComponentModel.DataAnnotations.Schema;

    using Smt.Atomic.Data.Entities.Abstracts;
    using Smt.Atomic.Data.Entities.Modules.Allocation;

    [Table("AggregatedProjectTransaction", Schema = "Finance")]
    public class AggregatedProjectTransaction : ModificationTrackedEntity
    {
        public decimal? EstimatedRevenue { get; set; }

        public decimal Hours { get; set; }

        public byte Month { get; set; }

        public decimal? OperationCost { get; set; }

        public decimal? OperationMargin { get; set; }

        public decimal? OperationMarginPercent { get; set; }

        public virtual Project Project { get; set; }

        public decimal? ProjectCost { get; set; }

        public long ProjectId { get; set; }

        public decimal? ProjectMargin { get; set; }

        public decimal? ProjectMarginPercent { get; set; }

        public decimal? Revenue { get; set; }

        public decimal? ServiceCost { get; set; }

        public decimal? ServiceMargin { get; set; }

        public decimal? ServiceMarginPercent { get; set; }

        public int Year { get; set; }

    }
}

