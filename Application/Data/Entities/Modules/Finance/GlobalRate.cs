﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Finance
{
    [Table("GlobalRate", Schema = "Finance")]
    public class GlobalRate : ModificationTrackedEntity
    {
        public string Name { get; set; }

        public decimal Value { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public Currency Currency { get; set; }

        public string Description { get; set; }

        public long? JobMatrixLevelId { get; set; }

        [ForeignKey(nameof(JobMatrixLevelId))]
        public JobMatrixLevel JobMatrixLevel { get; set; }

        public long? LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public Location Location { get; set; }

        public long? CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public Company Company { get; set; }
    }
}

