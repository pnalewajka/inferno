﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcessHistoryEntry", Schema = "Recruitment")]
    public class RecruitmentProcessHistoryEntry : SimpleEntity
    {
        public long RecruitmentProcessId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessId))]
        public virtual RecruitmentProcess RecruitmentProcess { get; set; }

        public DateTime PerformedOn { get; set; }

        public long PerformedByUserId { get; set; }

        [ForeignKey(nameof(PerformedByUserId))]
        public virtual User PerformedByUser { get; set; }

        public RecruitmentProcessHistoryEntryType ActivityType { get; set; }

        public string ChangesDescription { get; set; }

        public long? RecruitmentProcessStepId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessStepId))]
        public virtual RecruitmentProcessStep RecruitmentProcessStep { get; set; }
    }
}
