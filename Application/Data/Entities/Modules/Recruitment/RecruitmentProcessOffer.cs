﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcessOffer", Schema = "Recruitment")]
    public class RecruitmentProcessOffer : IEntity
    {
        [Key]
        [ForeignKey("RecruitmentProcess")]
        public long Id { get; set; }

        public virtual RecruitmentProcess RecruitmentProcess { get; set; }

        public long? PositionId { get; set; }

        [ForeignKey(nameof(PositionId))]
        public virtual JobTitle Position { get; set; }

        [MaxLength(50)]
        public string ContractType { get; set; }

        [MaxLength(150)]
        public string Salary { get; set; }

        [MaxLength(130)]
        public string StartDate { get; set; }

        public long? LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }

        [MaxLength(500)]
        public string Comment { get; set; }
    }
}
