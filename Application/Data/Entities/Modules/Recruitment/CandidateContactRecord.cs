﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("CandidateContactRecord", Schema = "Recruitment")]
    public class CandidateContactRecord : TrackedEntity
    {
        public long CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public virtual Candidate Candidate { get; set; }

        public ContactType ContactType { get; set; }

        public DateTime ContactedOn { get; set; }

        public long ContactedById { get; set; }

        [ForeignKey(nameof(ContactedById))]
        public virtual Employee ContactedBy { get; set; }

        public CandidateReaction CandidateReaction { get; set; }

        public virtual ICollection<JobOpening> RelevantJobOpenings { get; set; }

        public string Comment { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}