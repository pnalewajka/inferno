﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("TechnicalReview", Schema = "Recruitment")]
    public class TechnicalReview : TrackedEntity
    {
        public long RecruitmentProcessId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessId))]
        public virtual RecruitmentProcess RecruitmentProcess { get; set; }

        public long RecruitmentProcessStepId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessStepId))]
        public virtual RecruitmentProcessStep RecruitmentProcessStep { get; set; }

        public virtual ICollection<JobProfile> JobProfiles { get; set; }

        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        public string TechnicalKnowledgeAssessment { get; set; }

        public string TechnicalAssignmentAssessment { get; set; }

        public string EnglishUsageAssessment { get; set; }

        public string StrengthAssessment { get; set; }

        public string RiskAssessment { get; set; }

        public string TeamProjectSuitabilityAssessment { get; set; }

        public string OtherRemarks { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}