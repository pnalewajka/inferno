﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("JobOpeningNote", Schema = "Recruitment")]
    public class JobOpeningNote : TrackedEntity, IMentionedNote
    {
        public long JobOpeningId { get; set; }

        [ForeignKey(nameof(JobOpeningId))]
        public JobOpening JobOpening { get; set; }

        public NoteType NoteType { get; set; }

        public NoteVisibility NoteVisibility { get; set; }

        public string Content { get; set; }

        public virtual ICollection<Employee> MentionedEmployees { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}

