﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("CandidateNote", Schema = "Recruitment")]
    public class CandidateNote : TrackedEntity, IMentionedNote
    {
        public long CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public Candidate Candidate { get; set; }

        public NoteType NoteType { get; set; }

        public NoteVisibility NoteVisibility { get; set; }

        public string Content { get; set; }

        public virtual ICollection<Employee> MentionedEmployees { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}

