﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("InboundEmailValue", Schema = "Recruitment")]
    public class InboundEmailValue : SimpleEntity
    {
        public long InboundEmailId { get; set; }

        [ForeignKey(nameof(InboundEmailId))]
        public virtual InboundEmail InboundEmail { get; set; }

        public InboundEmailValueType Key { get; set; }

        [MaxLength(255)]
        public string ValueString { get; set; }

        public long? ValueLong { get; set; }
    }
}