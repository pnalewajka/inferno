﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("ApplicationOrigin", Schema = "Recruitment")]
    public class ApplicationOrigin : TrackedEntity
    {
        public string Name { get; set; }

        public ApplicationOriginType OriginType { get; set; }

        [MaxLength(255)]
        public string ParserIdentifier { get; set; }

        [MaxLength(255)]
        public string EmailAddress { get; set; }
        
        public override void OnSoftDeleting()
        {
        }
    }
}