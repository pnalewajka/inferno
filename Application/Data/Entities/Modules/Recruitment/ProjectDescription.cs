﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("ProjectDescription", Schema = "Recruitment")]
    public class ProjectDescription : SimpleEntity
    {
        public long JobOpeningId { get; set; }

        [ForeignKey(nameof(JobOpeningId))]
        public virtual JobOpening JobOpening { get; set; }

        [MaxLength(3072)]
        public string AboutClient { get; set; }

        [MaxLength(8192)]
        public string SpecificDuties { get; set; }

        [MaxLength(1024)]
        public string ToolsAndEnvironment { get; set; }

        [MaxLength(2048)]
        public string AboutTeam { get; set; }

        [MaxLength(2048)]
        public string ProjectTimeline { get; set; }

        [MaxLength(512)]
        public string Methodology { get; set; }

        [MaxLength(8192)]
        public string RequiredSkills { get; set; }

        [MaxLength(4096)]
        public string AdditionalSkills { get; set; }

        [MaxLength(2048)]
        public string RecruitmentDetails { get; set; }

        public bool IsTravelRequired { get; set; }

        [MaxLength(8192)]
        public string TravelAndAccomodation { get; set; }

        public bool CanWorkRemotely { get; set; }

        [MaxLength(2048)]
        public string RemoteWorkArrangements { get; set; }

        [MaxLength(3072)]
        public string Comments { get; set; }
    }
}