﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("InboundEmailDocument", Schema = "Recruitment")]
    public class InboundEmailDocument : DocumentBase<InboundEmailDocumentContent>
    {
        public long InboundEmailId { get; set; }

        [ForeignKey(nameof(InboundEmailId))]
        public virtual InboundEmail InboundEmail { get; set; }
    }
}

