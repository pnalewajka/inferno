﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("CandidateDocument", Schema = "Recruitment")]
    public class CandidateFileDocument : DocumentBase<CandidateFileDocumentContent>
    {
        public long CandidateFileId { get; set; }

        [ForeignKey(nameof(CandidateFileId))]
        public virtual CandidateFile CandidateFile { get; set; }
    }
}

