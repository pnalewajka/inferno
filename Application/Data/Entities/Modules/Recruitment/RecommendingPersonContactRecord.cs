﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecommendingPersonContactRecord", Schema = "Recruitment")]
    public class RecommendingPersonContactRecord : TrackedEntity
    {
        public long RecommendingPersonId { get; set; }

        [ForeignKey(nameof(RecommendingPersonId))]
        public virtual RecommendingPerson RecommendingPerson { get; set; }

        public ContactType ContactType { get; set; }

        public DateTime ContactedOn { get; set; }

        public long ContactedById { get; set; }

        [ForeignKey(nameof(ContactedById))]
        public virtual Employee ContactedBy { get; set; }

        public string Comment { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}

