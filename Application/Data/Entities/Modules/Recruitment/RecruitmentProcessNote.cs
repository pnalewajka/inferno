﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcessNote", Schema = "Recruitment")]
    public class RecruitmentProcessNote : TrackedEntity, IMentionedNote
    {
        public long RecruitmentProcessId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessId))]
        public RecruitmentProcess RecruitmentProcess { get; set; }

        public long? RecruitmentProcessStepId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessStepId))]
        public RecruitmentProcessStep RecruitmentProcessStep { get; set; }

        public NoteType NoteType { get; set; }

        public NoteVisibility NoteVisibility { get; set; }

        public string Content { get; set; }

        public virtual ICollection<Employee> MentionedEmployees { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}
