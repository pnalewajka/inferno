﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcessStep", Schema = "Recruitment")]
    public class RecruitmentProcessStep : TrackedEntity
    {
        public long RecruitmentProcessId { get; set; }

        [ForeignKey(nameof(RecruitmentProcessId))]
        public virtual RecruitmentProcess RecruitmentProcess { get; set; }

        public RecruitmentProcessStepType Type { get; set; }

        public DateTime? DecidedOn { get; set; }

        public DateTime? PlannedOn { get; set; }

        public DateTime? DueOn { get; set; }

        public long? AssignedEmployeeId { get; set; }

        [ForeignKey(nameof(AssignedEmployeeId))]
        public virtual Employee AssignedEmployee { get; set; }

        public virtual ICollection<Employee> OtherAttendingEmployees { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public RecruitmentProcessStepStatus Status { get; set; }

        [MaxLength(1024)]
        public string DecisionComment { get; set; }

        [MaxLength(1024)]
        public string RecruitmentHint { get; set; }

        public long? CloneOfStepId { get; set; }

        [ForeignKey(nameof(CloneOfStepId))]
        public RecruitmentProcessStep CloneOfStep { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}