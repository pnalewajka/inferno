﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcess", Schema = "Recruitment")]
    public partial class RecruitmentProcess : TrackedEntity, IWatchedEntity
    {
        public long CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public virtual Candidate Candidate { get; set; }

        public long JobOpeningId { get; set; }

        [ForeignKey(nameof(JobOpeningId))]
        public virtual JobOpening JobOpening { get; set; }

        public long RecruiterId { get; set; }

        [ForeignKey(nameof(RecruiterId))]
        public virtual Employee Recruiter { get; set; }

        public long? CityId { get; set; }

        [ForeignKey(nameof(CityId))]
        public virtual City City { get; set; }

        public long? JobApplicationId { get; set; }

        [ForeignKey(nameof(JobApplicationId))]
        public virtual JobApplication JobApplication { get; set; }

        public long? OnboardingRequestId { get; set; }

        [ForeignKey(nameof(OnboardingRequestId))]
        public virtual OnboardingRequest OnboardingRequest { get; set; }

        public RecruitmentProcessStatus Status { get; set; }

        public DateTime? ResumeShownToClientOn { get; set; }

        public DateTime? ClosedOn { get; set; }

        public RecruitmentProcessClosedReason? ClosedReason { get; set; }

        public String ClosedComment { get; set; }

        [Index]
        public Guid? CrmId { get; set; }

        public virtual ICollection<RecruitmentProcessStep> ProcessSteps { get; set; }

        public virtual ICollection<TechnicalReview> TechnicalReviews { get; set; }

        public virtual ICollection<Employee> Watchers { get; set; }

        public virtual ICollection<RecruitmentProcessNote> RecruitmentProcessNotes { get; set; }

        public virtual ICollection<RecruitmentProcessHistoryEntry> RecruitmentProcessHistories { get; set; }

        public long? SourceContactRecordId { get; set; }

        public DateTime? LastActivityOn { get; set; }

        [ForeignKey(nameof(LastActivityById))]
        public virtual Employee LastActivityBy { get; set; }

        public long? LastActivityById { get; set; }

        [ForeignKey(nameof(SourceContactRecordId))]
        public virtual CandidateContactRecord SourceContactRecord { get; set; }

        public bool IsMatureEnoughForHiringManager { get; set; }

        public virtual RecruitmentProcessOffer Offer { get; set; }

        public virtual RecruitmentProcessScreening Screening { get; set; }

        public virtual RecruitmentProcessFinal Final { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}