﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Attributes;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecommendingPerson", Schema = "Recruitment")]
    public class RecommendingPerson : TrackedEntity
    {
        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        // THIS VALUE IS UNIQUE ONLY IN CASE WHEN IS DELETED IS SET AS 0 CURRENT EF DON'T ENABLE ON SOMETHING LIKE THAT SO I ADDED CUSTOM INDEX IN 
        [SoftDeletableIndex(true)]
        [Required]
        [StringLength(255)]
        public string EmailAddress { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        public virtual ICollection<DataOwner> DataOwners { get; set; }

        public virtual ICollection<RecommendingPersonNote> RecommendingPersonNotes { get; set; }

        public bool IsAnonymized { get; set; }

        [ForeignKey(nameof(CoordinatorId))]
        public Employee Coordinator { get; set; }

        public long CoordinatorId { get; set; }

        public DateTime? DeletedOn { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}