﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Attributes;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("Candidate", Schema = "Recruitment")]
    public class Candidate : TrackedEntity, IWatchedEntity
    {
        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        public ContactRestriction ContactRestriction { get; set; }

        [StringLength(50)]
        public string MobilePhone { get; set; }

        [StringLength(50)]
        public string OtherPhone { get; set; }

        [SoftDeletableIndex(true)]
        [Required]
        [StringLength(255)]
        public string EmailAddress { get; set; }

        [StringLength(255)]
        public string OtherEmailAddress { get; set; }

        [StringLength(255)]
        public string SkypeLogin { get; set; }

        [StringLength(255)]
        public string SocialLink { get; set; }

        public DateTime? NextFollowUpDate { get; set; }

        public DateTime? LastFollowUpReminderDate { get; set; }

        public CandidateEmploymentStatus EmployeeStatus { get; set; }

        public bool CanRelocate { get; set; }

        [StringLength(1023)]
        public string RelocateDetails { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual ICollection<JobProfile> JobProfiles { get; set; }

        public virtual ICollection<CandidateLanguage> Languages { get; set; }

        public string ResumeTextContent { get; set; }

        [ForeignKey(nameof(ContactCoordinatorId))]
        public Employee ContactCoordinator { get; set; }

        public long ContactCoordinatorId { get; set; }

        [ForeignKey(nameof(ConsentCoordinatorId))]
        public Employee ConsentCoordinator { get; set; }

        public long ConsentCoordinatorId { get; set; }

        public virtual ICollection<Employee> Watchers { get; set; }

        public virtual ICollection<Employee> Favorites { get; set; }

        public virtual ICollection<DataOwner> DataOwners { get; set; }

        public virtual ICollection<CandidateNote> CandidateNotes { get; set; }

        [ForeignKey(nameof(OriginalSourceId))]
        public ApplicationOrigin OriginalSource { get; set; }

        public long OriginalSourceId { get; set; }

        [MaxLength(400)]
        public string OriginalSourceComment { get; set; }

        public virtual ICollection<RecruitmentProcess> RecruitmentProcesses { get; set; }

        public bool IsAnonymized { get; set; }

        public bool IsHighlyConfidential { get; set; }

        public Guid? CrmId { get; set; }

        public virtual ICollection<JobApplication> JobApplications { get; set; }

        public virtual ICollection<CandidateContactRecord> CandidateContactRecords { get; set; }

        public virtual ICollection<CandidateFile> CandidateFiles { get; set; }

        public DateTime? DeletedOn { get; set; }

        [ForeignKey(nameof(LastActivityById))]
        public virtual Employee LastActivityBy { get; set; }

        public long? LastActivityById { get; set; }

        public DateTime? LastActivityOn { get; set; }

        public ReferenceType? LastReferenceType { get; set; }

        public long? LastReferenceId { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}