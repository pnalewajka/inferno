﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("InboundEmailDocumentContent", Schema = "Recruitment")]
    public class InboundEmailDocumentContent : IEntity
    {
        [Key]
        [ForeignKey(nameof(InboundEmailDocument))]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual InboundEmailDocument InboundEmailDocument { get; set; }
    }
}

