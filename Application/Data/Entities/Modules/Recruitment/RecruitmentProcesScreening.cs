﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcessScreening", Schema = "Recruitment")]
    public class RecruitmentProcessScreening : IEntity
    {
        [Key]
        [ForeignKey("RecruitmentProcess")]
        public long Id { get; set; }

        public virtual RecruitmentProcess RecruitmentProcess { get; set; }

        public string GeneralOpinion { get; set; }

        public string Motivation { get; set; }

        public string WorkplaceExpectations { get; set; }

        public string Skills { get; set; }

        public string LanguageEnglish { get; set; }

        public string LanguageGerman { get; set; }

        public string LanguageOther { get; set; }

        public string ContractExpectations { get; set; }

        public string Availability { get; set; }

        public string OtherProcesses { get; set; }

        public string CounterOfferCriteria { get; set; }

        public bool? RelocationCanDo { get; set; }

        public string RelocationComment { get; set; }

        public bool? BusinessTripsCanDo { get; set; }

        public string BusinessTripsComment { get; set; }

        public bool? EveningWorkCanDo { get; set; }

        public string EveningWorkComment { get; set; }

        public bool? WorkPermitNeeded { get; set; }

        public string WorkPermitComment { get; set; }
    }
}
