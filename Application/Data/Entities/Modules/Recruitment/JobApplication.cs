﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("JobApplication", Schema = "Recruitment")]
    public class JobApplication : TrackedEntity
    {
        public long CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public virtual Candidate Candidate { get; set; }

        public long ApplicationOriginId { get; set; }

        [ForeignKey(nameof(ApplicationOriginId))]
        public virtual ApplicationOrigin ApplicationOrigin { get; set; }

        public long? RecommendingPersonId { get; set; }

        [ForeignKey(nameof(RecommendingPersonId))]
        public virtual RecommendingPerson RecommendingPerson { get; set; }

        public long? SourceEmailId { get; set; }

        [ForeignKey(nameof(SourceEmailId))]
        public InboundEmail SourceEmail { get; set; }

        [StringLength(400)]
        public string OriginComment { get; set; }

        public virtual ICollection<JobProfile> JobProfiles { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        [StringLength(250)]
        public string CandidateComment { get; set; }

        public bool IsEligibleForReferralProgram { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}