﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("InboundEmail", Schema = "Recruitment")]
    public class InboundEmail : TrackedEntity
    {
        public string To { get; set; }

        public string FromEmailAddress { get; set; }

        public string FromFullName { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public EmailBodyType BodyType { get; set; }

        public InboundEmailColorCategory? EmailColorCategory { get; set; }

        public DateTime SentOn { get; set; }

        public DateTime ReceivedOn { get; set; }

        public long? ProcessedById { get; set; }

        [ForeignKey(nameof(ProcessedById))]
        public virtual Employee ProcessedBy { get; set; }

        public virtual ICollection<User> Readers { get; set; }

        public InboundEmailStatus Status { get; set; }

        public InboundEmailCategory Category { get; set; }

        public virtual ICollection<InboundEmailDocument> Attachments { get; set; }

        public virtual ICollection<InboundEmailValue> Values { get; set; }

        public string ClosedComment { get; set; }

        public InboundEmailClosedReason? ClosedReason { get; set; }

        public DateTime? ClosedOn { get; set; }

        public long? ClosedById { get; set; }

        [ForeignKey(nameof(ClosedById))]
        public virtual Employee ClosedBy { get; set; }

        [MaxLength(300)]
        public string ResearchingComment { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}

