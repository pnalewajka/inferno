﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("JobOpeningHistoryEntry", Schema = "Recruitment")]
    public class JobOpeningHistoryEntry : SimpleEntity
    {
        public long JobOpeningId { get; set; }

        [ForeignKey(nameof(JobOpeningId))]
        public virtual JobOpening JobOpening { get; set; }

        public DateTime PerformedOn { get; set; }

        public long PerformedByUserId { get; set; }

        [ForeignKey(nameof(PerformedByUserId))]
        public virtual User PerformedByUser { get; set; }

        public string ChangesDescription { get; set; }

        public JobOpeningHistoryEntryType ActivityType { get; set; }
    }
}
