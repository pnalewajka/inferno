﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("JobOpening", Schema = "Recruitment")]
    public class JobOpening : TrackedEntity, IWatchedEntity
    {
        [MaxLength(400)]
        public string PositionName { get; set; }

        public long? CustomerId { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual Customer Customer { get; set; }

        public virtual ICollection<JobProfile> JobProfiles { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public long OrgUnitId { get; set; }

        [ForeignKey(nameof(OrgUnitId))]
        public virtual OrgUnit OrgUnit { get; set; }

        public long? AcceptingEmployeeId { get; set; }

        public long CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company { get; set; }

        public long DecisionMakerEmployeeId { get; set; }

        public Employee DecisionMakerEmployee { get; set; }

        [ForeignKey(nameof(AcceptingEmployeeId))]
        public virtual Employee AcceptingEmployee { get; set; }

        public int Priority { get; set; }

        public HiringMode HiringMode { get; set; }

        public DateTime? RoleOpenedOn { get; set; }

        public int VacancyLevel1 { get; set; }

        public int VacancyLevel2 { get; set; }

        public int VacancyLevel3 { get; set; }

        public int VacancyLevel4 { get; set; }

        public int VacancyLevel5 { get; set; }

        public int VacancyLevelNotApplicable { get; set; }

        public virtual ICollection<Employee> Recruiters { get; set; }

        public virtual ICollection<Employee> RecruitmentOwners { get; set; }

        public bool IsTechnicalVerificationRequired { get; set; }

        public virtual ICollection<Employee> TechnicalReviewers { get; set; }

        public virtual ICollection<JobOpeningHistoryEntry> JobOpeningHistories { get; set; }

        public bool IsIntiveResumeRequired { get; set; }

        public bool IsPolishSpeakerRequired { get; set; }

        [MaxLength(120)]
        public string NoticePeriod { get; set; }

        [MaxLength(130)]
        public string SalaryRate { get; set; }

        public LanguageReferenceLevel EnglishLevel { get; set; }

        [MaxLength(100)]
        public string OtherLanguages { get; set; }

        public AgencyEmploymentMode? AgencyEmploymentMode { get; set; }

        public virtual ICollection<ProjectDescription> ProjectDescriptions { get; set; }

        public JobOpeningStatus Status { get; set; }

        public virtual ICollection<Employee> Watchers { get; set; }

        public virtual ICollection<JobOpeningNote> JobOpeningNotes { get; set; }

        public virtual ICollection<RecruitmentProcess> RecruitmentProcesses { get; set; }

        public DateTime? ClosedOn { get; set; }

        public JobOpeningClosedReason? ClosedReason { get; set; }

        [MaxLength(255)]
        public string ClosedComment { get; set; }

        public bool IsHighlyConfidential { get; set; }

        [MaxLength(550)]
        public string RejectionReason { get; set; }

        [MaxLength(300)]
        public string TechnicalReviewComment { get; set; }

        [Index]
        public Guid? CrmId { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}