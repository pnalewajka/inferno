﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("CandidateLanguage", Schema = "Recruitment")]
    public class CandidateLanguage: TrackedEntity
    {
        public long CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public Candidate Candidate { get; set; }

        public long LanguageId { get; set; }

        [ForeignKey(nameof(LanguageId))]
        public Language Language { get; set; }

        public LanguageReferenceLevel Level { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}
