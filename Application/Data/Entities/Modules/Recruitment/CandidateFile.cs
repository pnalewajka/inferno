﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("CandidateFile", Schema = "Recruitment")]
    public class CandidateFile : TrackedEntity
    {
        public long CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public Candidate Candidate { get; set; }

        public virtual ICollection<CandidateFileDocument> File { get; set; }

        public RecruitmentDocumentType DocumentType { get; set; }

        [MaxLength(100)]
        public string Comment { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}