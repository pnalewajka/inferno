﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    public interface IMentionedNote
    {
        ICollection<Employee> MentionedEmployees { get; set; }
    }
}