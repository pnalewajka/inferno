﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Data.Entities.Modules.Recruitment
{
    [Table("RecruitmentProcessFinal", Schema = "Recruitment")]
    public class RecruitmentProcessFinal : IEntity
    {
        [Key]
        [ForeignKey("RecruitmentProcess")]
        public long Id { get; set; }

        public virtual RecruitmentProcess RecruitmentProcess { get; set; }

        public long? PositionId { get; set; }

        public virtual JobTitle Position { get; set; }

        public long? TrialContractTypeId { get; set; }

        [ForeignKey(nameof(TrialContractTypeId))]
        public virtual ContractType TrialContractType { get; set; }

        public long? LongTermContractTypeId { get; set; }

        [ForeignKey(nameof(LongTermContractTypeId))]
        public virtual ContractType LongTermContractType { get; set; }

        [MaxLength(50)]
        public string TrialSalary { get; set; }

        [MaxLength(50)]
        public string LongTermSalary { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? SignedDate { get; set; }

        public long? LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }

        [MaxLength(255)]
        public string Comment { get; set; }

        public long? EmploymentSourceId { get; set; }

        [ForeignKey(nameof(EmploymentSourceId))]
        public virtual ApplicationOrigin EmploymentSource { get; set; }
    }
}
