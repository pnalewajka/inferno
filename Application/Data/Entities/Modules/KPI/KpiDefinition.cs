﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.KPI
{
    [Table("KpiDefinition", Schema = "KPI")]
    public class KpiDefinition : ModificationTrackedEntity
    {
        [StringLength(35)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public KpiEntity Entity { get; set; }

        [StringLength(4000)]
        public string SqlFormula { get; set; }

        public virtual ICollection<SecurityRole> Roles { get; set; }
    }
}
