﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.CrossCutting.Business.Enums;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectTag", Schema = "Allocation")]
    public class ProjectTag : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public string Description { get; set; }

        public ProjectTagType TagType { get; set; }

        public virtual ICollection<ProjectSetup> ProjectSetups { get; set; }
    }
}

