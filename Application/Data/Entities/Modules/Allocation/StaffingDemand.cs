﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("StaffingDemand", Schema = "Allocation")]
    public class StaffingDemand : ModificationTrackedEntity
    {
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Description { get; set; }

        public decimal EmployeesNumber { get; set; }

        public long? JobMatrixLevelId { get; set; }

        [ForeignKey("JobMatrixLevelId")]
        public virtual JobMatrixLevel JobMatrixLevel { get; set; }

        public virtual ICollection<Skill> Skills { get; set; }

        public long? JobProfileId { get; set; }

        [ForeignKey("JobProfileId")]
        public virtual JobProfile JobProfile { get; set; }
  
        public long ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

    }
}

