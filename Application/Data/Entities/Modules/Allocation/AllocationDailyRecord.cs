﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Configuration;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("AllocationDailyRecord", Schema = "Allocation")]
    public class AllocationDailyRecord : SimpleEntity
    {
        //employee
        public long EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        //project
        public long ProjectId { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        //request
        public long AllocationRequestId { get; set; }

        [ForeignKey("AllocationRequestId")]
        public virtual AllocationRequest AllocationRequest { get; set; }

        //calendar
        public long CalendarId { get; set; }

        [ForeignKey("CalendarId")]
        public virtual Calendar Calendar { get; set; }
        
        //data
        public decimal Hours { get; set; }

        public DateTime Day { get; set; }

        public bool IsWorkDay { get; set; }
    }
}

