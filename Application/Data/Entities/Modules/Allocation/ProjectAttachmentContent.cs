﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectAttachmentContent", Schema = "Allocation")]
    public class ProjectAttachmentContent : IEntity
    {
        [Key]
        [ForeignKey("ProjectAttachment")]
        public long Id { get; set; }

        public byte[] Content { get; set; }
        
        public virtual ProjectAttachment ProjectAttachment { get; set; }
    }
}
