﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("EmploymentPeriod", Schema = "Allocation")]
    public class EmploymentPeriod : ModificationTrackedEntity, IWeeklyHours
    {
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime SignedOn { get; set; }

        [EntityReference(typeof(Employee))]
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        [Column(nameof(CompanyId))]
        [EntityReference(typeof(Company))]
        public long? CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company { get; set; }

        [Column(nameof(JobTitleId))]
        [EntityReference(typeof(JobTitle))]
        public long? JobTitleId { get; set; }

        [ForeignKey(nameof(JobTitleId))]
        public virtual JobTitle JobTitle { get; set; }

        public bool IsActive { get; set; }

        public EmployeeInactivityReason InactivityReason { get; set; }

        [Column(nameof(TerminationReasonId))]
        [EntityReference(typeof(EmploymentTerminationReason))]
        public long? TerminationReasonId { get; set; }

        [ForeignKey(nameof(TerminationReasonId))]
        public virtual EmploymentTerminationReason TerminationReason { get; set; }

        public decimal MondayHours { get; set; }

        public decimal TuesdayHours { get; set; }

        public decimal WednesdayHours { get; set; }

        public decimal ThursdayHours { get; set; }

        public decimal FridayHours { get; set; }

        public decimal SaturdayHours { get; set; }

        public decimal SundayHours { get; set; }

        public NoticePeriod? NoticePeriod { get; set; }

        public DateTime? TerminatedOn { get; set; }

        public long ContractTypeId { get; set; }

        [ForeignKey("ContractTypeId")]
        public virtual ContractType ContractType { get; set; }

        public ContractDurationType? ContractDuration { get; set; }

        public long? CalendarId { get; set; }

        [ForeignKey(nameof(CalendarId))]
        public virtual Calendar Calendar { get; set; }

        public decimal VacationHourToDayRatio { get; set; }

        public TimeReportingMode TimeReportingMode { get; set; }

        [EntityReference(typeof(Project))]
        public long? AbsenceOnlyDefaultProjectId { get; set; }

        [ForeignKey("AbsenceOnlyDefaultProjectId")]
        public virtual Project AbsenceOnlyDefaultProject { get; set; }

        public bool IsClockCardRequired { get; set; }

        public long? CompensationCurrencyId { get; set; }

        [ForeignKey(nameof(CompensationCurrencyId))]
        public virtual Currency CompensationCurrency { get; set; }

        [MaxLength(256)]
        public string ContractorCompanyName { get; set; }

        [MaxLength(64)]
        public string ContractorCompanyTaxIdentificationNumber { get; set; }

        [MaxLength(256)]
        public string ContractorCompanyStreetAddress { get; set; }

        [MaxLength(128)]
        public string ContractorCompanyCity { get; set; }

        [MaxLength(16)]
        public string ContractorCompanyZipCode { get; set; }

        public decimal? HourlyRate { get; set; }

        public decimal? HourlyRateDomesticOutOfOfficeBonus { get; set; }

        public decimal? HourlyRateForeignOutOfOfficeBonus { get; set; }

        public decimal? MonthlyRate { get; set; }

        public decimal? SalaryGross { get; set; }

        public decimal? AuthorRemunerationRatio { get; set; }

        [Column(nameof(LocationId))]
        [EntityReference(typeof(Location))]
        public long? LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }
    }
}