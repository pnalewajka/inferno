﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("AllocationRequestMetadata", Schema = "Allocation")]
    public class AllocationRequestMetadata : SimpleEntity
    {
        public long AllocationRequestId { get; set; }

        [ForeignKey("AllocationRequestId")]
        public virtual AllocationRequest AllocationRequest { get; set; }

        public DateTime? NextRebuildTime { get; set; }

        public ProcessingStatus ProcessingStatus { get; set; }

        public AllocationRequestMetadata()
        {
            ProcessingStatus = ProcessingStatus.Processing;
        }
    }
}

