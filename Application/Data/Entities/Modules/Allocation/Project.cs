﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("Project", Schema = "Allocation")]
    public class Project : ModificationTrackedEntity
    {
        public long ProjectSetupId { get; set; }

        [ForeignKey(nameof(ProjectSetupId))]
        public virtual ProjectSetup ProjectSetup { get; set; }

        public bool IsMainProject { get; set; }
        
        [MaxLength(128)]
        public string SubProjectShortName { get; set; }

        [MaxLength(256)]
        public string Apn { get; set; } 

        public string Description { get; set; }
        
        public DateTime? StartDate { get; set; } 

        public DateTime? EndDate { get; set; } 

        [MaxLength(256)]
        public string JiraIssueKey { get; set; } 

        [MaxLength(256)]
        public string JiraKey { get; set; } 

        [MaxLength(256)]
        public string PetsCode { get; set; } 

        [MaxLength(256)]
        public string KupferwerkProjectId { get; set; } 

        public virtual ICollection<StaffingDemand> StaffingDemands { get; set; } 

        public virtual ICollection<AllocationRequest> AllocationRequests { get; set; } 

        // TimeTracking

        public long? TimeTrackingImportSourceId { get; set; } 

        [ForeignKey(nameof(TimeTrackingImportSourceId))]
        public virtual TimeTrackingImportSource TimeTrackingImportSource { get; set; } 

        public long JiraIssueToTimeReportRowMapperId { get; set; } 

        [ForeignKey(nameof(JiraIssueToTimeReportRowMapperId))]
        public virtual JiraIssueToTimeReportRowMapper JiraIssueToTimeReportRowMapper { get; set; } 

        public OverTimeTypes AllowedOvertimeTypes { get; set; } 

        public virtual ICollection<Employee> TimeReportAcceptingPersons { get; set; }

        public bool IsApprovedByRequesterMainManager { get; set; } 

        public bool IsApprovedByMainManagerForProjectManager { get; set; } 

        public TimeTrackingProjectType TimeTrackingType { get; set; }

        public long? UtilizationCategoryId { get; set; }

        [ForeignKey(nameof(UtilizationCategoryId))]
        public virtual UtilizationCategory UtilizationCategory { get; set; } 

        public DateTime? ReportingStartsOn { get; set; } 

        public virtual ICollection<TimeReportProjectAttribute> Attributes { get; set; } 

        public virtual ICollection<TimeReportRow> TimeReportRows { get; set; } 

        [MaxLength(9)]
        [Index("IX_ProjectAcronym", IsUnique = true)]
        public string Acronym { get; set; }

        public BonusTypeSet AllowedBonusTypes { get; set; }
    }
}