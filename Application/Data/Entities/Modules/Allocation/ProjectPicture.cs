﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectPicture", Schema = "Allocation")]
    public class ProjectPicture : DocumentBase<ProjectPictureContent>
    {
    }
}
