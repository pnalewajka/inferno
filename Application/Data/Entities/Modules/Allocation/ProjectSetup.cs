﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectSetup", Schema = "Allocation")]
    public class ProjectSetup
        : SimpleEntity
        , IAcceptanceConditions
    {
        public virtual ICollection<Project> Projects { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string ClientShortName { get; set; }

        [Required]
        [MaxLength(128)]
        public string ProjectShortName { get; set; }

        public string GenericName { get; set; }

        [MaxLength(60)]
        public string Region { get; set; }

        public ProjectType? ProjectType { get; set; }

        public ProjectFeatures ProjectFeatures { get; set; }

        public long OrgUnitId { get; set; }

        [ForeignKey(nameof(OrgUnitId))]
        public virtual OrgUnit OrgUnit { get; set; }

        public long? ProjectManagerId { get; set; }

        public long? SalesAccountManagerId { get; set; }

        public long? SupervisorManagerId { get; set; }

        [ForeignKey(nameof(ProjectManagerId))]
        public virtual Employee ProjectManager { get; set; }

        [ForeignKey(nameof(SalesAccountManagerId))]
        public virtual Employee SalesAccountManager { get; set; }

        [ForeignKey(nameof(SupervisorManagerId))]
        public virtual Employee SupervisorManager { get; set; }

        public ProjectStatus ProjectStatus { get; set; }

        public long? CalendarId { get; set; }

        [ForeignKey(nameof(CalendarId))]
        public virtual Calendar Calendar { get; set; } 

        public string Color { get; set; }

        public long? PictureId { get; set; }

        [ForeignKey(nameof(PictureId))]
        public virtual ProjectPicture Picture { get; set; }

        public virtual ICollection<Industry> Industries { get; set; }

        public bool IsAvailableForSalesPresentation { get; set; }

        public virtual ICollection<Skill> Technologies { get; set; }

        public virtual ICollection<SkillTag> KeyTechnologies { get; set; }

        public virtual ICollection<ServiceLine> ServiceLines { get; set; }

        public ProjectPhase CurrentPhase { get; set; }

        public ProjectDisclosures Disclosures { get; set; }

        public bool HasClientReferences { get; set; }

        public virtual ICollection<SoftwareCategory> SoftwareCategories { get; set; }

        public long? CustomerSizeId { get; set; }

        public virtual ICollection<ProjectTag> Tags { get; set; }

        public string BusinessCase { get; set; }

        public virtual ICollection<ProjectAttachment> Attachments { get; set; }

        #region Acceptance Conditions

        public bool ReinvoicingToCustomer { get; set; }

        public long? ReinvoiceCurrencyId { get; set; }

        [ForeignKey(nameof(ReinvoiceCurrencyId))]
        public virtual Currency ReinvoiceCurrency { get; set; }

        public CostApprovalPolicy CostApprovalPolicy { get; set; }

        public long? MaxCostsCurrencyId { get; set; }

        [ForeignKey(nameof(MaxCostsCurrencyId))]
        public virtual Currency MaxCostsCurrency { get; set; }

        public decimal? MaxHotelCosts { get; set; }

        public decimal? MaxTotalFlightsCosts { get; set; }

        public decimal? MaxOtherTravelCosts { get; set; }

        #endregion

        public virtual ICollection<ProjectInvoicingRate> InvoicingRates { get; set; }

        public string InvoicingAlgorithm { get; set; }
    }
}

