﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("Employee", Schema = "Allocation")]
    public class Employee : ModificationTrackedEntity
    {
        [MaxLength(255)]
        public string FirstName { get; set; }

        [MaxLength(255)]
        public string LastName { get; set; }

        [MaxLength(15)]
        public string Acronym { get; set; }

        [NotMapped]
        public string FullName => $"{LastName} {FirstName}".Trim();

        [NotMapped]
        public string DisplayName => $"{FirstName} {LastName}".Trim();

        [MaxLength(255)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [MaxLength(32)]
        public string PersonalNumber { get; set; }

        public PersonalNumberType? PersonalNumberType { get; set; }

        [MaxLength(32)]
        public string IdCardNumber { get; set; }

        [MaxLength(32)]
        public string PhoneNumber { get; set; }

        [MaxLength(50)]
        public string SkypeName { get; set; }

        [MaxLength(128)]
        public string HomeAddress { get; set; }

        [MaxLength(128)]
        public string AvailabilityDetails { get; set; }

        [EntityReference(typeof(Location))]
        [Column("LocationId")]
        public long? LocationId { get; set; }

        [EntityReference(typeof(JobTitle))]
        [Column("JobTitleId")]
        public long? JobTitleId { get; set; }

        public EmployeeStatus CurrentEmployeeStatus { get; set; }

        public bool IsProjectContributor { get; set; }

        public bool IsCoreAsset { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        [ForeignKey("JobTitleId")]
        public virtual JobTitle JobTitle { get; set; }

        public virtual ICollection<JobProfile> JobProfiles { get; set; }

        public decimal MinimumOvertimeBankBalance { get; set; }

        public virtual ICollection<JobMatrixLevel> JobMatrixs { get; set; }

        public virtual ICollection<EmploymentPeriod> EmploymentPeriods { get; set; }

        public virtual ICollection<WeeklyAllocationStatus> WeeklyAllocationStatuses { get; set; }

        public virtual ICollection<AllocationRequest> AllocationRequests { get; set; }

        public virtual ICollection<CalendarAbsence> CalendarAbsences { get; set; }

        public virtual ICollection<Resume> Resumes { get; set; }

        public virtual ICollection<Project> AcceptingPersonProjects { get; set; }

        public virtual ICollection<VacationBalance> VacationBalances { get; set; }

        public virtual ICollection<OvertimeBank> OvertimeBanks { get; set; }

        public virtual ICollection<EmployeeAbsence> EmployeeAbsences { get; set; }

        public virtual ICollection<AllocationDailyRecord> AllocationDailyRecords { get; set; }

        public virtual ICollection<TechnicalInterviewer> TechnicalInterviewers { get; set; }

        [EntityReference(typeof(Company))]
        [Column("CompanyId")]
        public long? CompanyId { get; set; }

        [InverseProperty(nameof(Dictionaries.Company.UsedInEmployees))]
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [EntityReference(typeof(User))]
        [Column("UserId")]
        [Index(IsUnique = true)]
        public long? UserId { get; set; }

        [InverseProperty("Employees")]
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [EntityReference(typeof(Employee))]
        [Column("LineManagerId")]
        public long? LineManagerId { get; set; }

        [ForeignKey("LineManagerId")]
        public virtual Employee LineManager { get; set; }

        [EntityReference(typeof(OrgUnit))]
        [Column("OrgUnitId")]
        public long OrgUnitId { get; set; }

        [ForeignKey("OrgUnitId")]
        public virtual OrgUnit OrgUnit { get; set; }

        public virtual ICollection<TimeReport> TimeReports { get; set; }

        [InverseProperty(nameof(Meeting.Employee))]
        public virtual ICollection<Meeting> Meetings { get; set; }

        [InverseProperty(nameof(Feedback.Receiver))]
        public virtual ICollection<Feedback> Feedbacks { get; set; }

        public EmploymentType? CurrentEmploymentType { get; set; }

        public EmployeeFeatures EmployeeFeatures { get; set; }

        [InverseProperty(nameof(Dictionaries.Company.TravelExpensesSpecialists))]
        public virtual ICollection<Company> TravelExpensesSpecialistInCompanies { get; set; }

        public virtual ICollection<Bonus> Bonuses { get; set; }

        public virtual ICollection<ExpenseRequest> ExpenseRequests { get; set; }
        
        public virtual ICollection<BusinessTripMessage> MentionedInBusinessTripMessages { get; set; }

        public virtual ICollection<InvoiceCalculation> InvoiceCalculations { get; set; }

        public VacationBalance CurrentVacationBalance
        {
            get
            {
                var timeService = ReflectionHelper.ResolveInterface<ITimeService>();
                var date = timeService.GetCurrentDate();

                return VacationBalances.Where(x => x.EffectiveOn <= date)
                                        .OrderByDescending(x => x.EffectiveOn)
                                        .ThenByDescending(x => x.Id)
                                        .FirstOrDefault();
            }
        }

        public virtual ICollection<FeedbackRequest> FeedbackRequests { get; set; }

        private EmploymentPeriod _firstEmploymentPeriod;

        public EmploymentPeriod GetFirstEmploymentPeriod()
        {
            if (_firstEmploymentPeriod == null)
            {
                _firstEmploymentPeriod = EmploymentPeriods.OrderBy(p => p.StartDate).FirstOrDefault();
            }

            return _firstEmploymentPeriod;
        }

        private EmploymentPeriod _currentEmploymentPeriod;

        public EmploymentPeriod GetCurrentEmploymentPeriod()
        {
            if (_currentEmploymentPeriod == null)
            {
                var timeService = ReflectionHelper.ResolveInterface<ITimeService>();
                var date = timeService.GetCurrentDate();

                _currentEmploymentPeriod = EmploymentPeriods.SingleOrDefault(p => p.StartDate <= date && (!p.EndDate.HasValue || date <= p.EndDate));
            }

            return _currentEmploymentPeriod;
        }

        private EmploymentPeriod _lastEmploymentPeriod;

        public EmploymentPeriod GetLastEmploymentPeriod()
        {
            if (_lastEmploymentPeriod == null)
            {
                _lastEmploymentPeriod = EmploymentPeriods.OrderBy(p => p.StartDate).LastOrDefault();
            }

            return _lastEmploymentPeriod;
        }
    }
}

