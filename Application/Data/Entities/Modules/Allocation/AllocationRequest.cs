﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("AllocationRequest", Schema = "Allocation")]
    public class AllocationRequest : ModificationTrackedEntity, ICreationTracked, IWeeklyHours, IDateRange
    {
        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public long ProjectId { get; set; }

        public virtual Project Project { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public AllocationRequestContentType ContentType { get; set; }

        public AllocationCertainty AllocationCertainty { get; set; }

        public decimal MondayHours { get; set; }
        public decimal TuesdayHours { get; set; }
        public decimal WednesdayHours { get; set; }
        public decimal ThursdayHours { get; set; }
        public decimal FridayHours { get; set; }
        public decimal SaturdayHours { get; set; }
        public decimal SundayHours { get; set; }

        public long? CreatedById { get; set; }

        public long? ImpersonatedCreatedById { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual AllocationRequestMetadata AllocationRequestMetadata { get; set; }

        public AllocationChangeDemand ChangeDemand { get; set; }

        [MaxLength(200)]
        public string Comment { get; set; }
    }
}

