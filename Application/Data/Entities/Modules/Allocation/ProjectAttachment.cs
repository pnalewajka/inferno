﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectAttachment", Schema = "Allocation")]
    public class ProjectAttachment : DocumentBase<ProjectAttachmentContent>
    {
        public ProjectAttachmentType AttachmentType { get; set; }

        public long ProjectSetupId { get; set; }

        [ForeignKey(nameof(ProjectSetupId))]
        public virtual ProjectSetup ProjectSetup { get; set; }
    }
}
