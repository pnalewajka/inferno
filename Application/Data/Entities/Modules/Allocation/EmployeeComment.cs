﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("EmployeeComment", Schema = "Allocation")]
    public class EmployeeComment : ModificationTrackedEntity, ICreationTracked
    {
        public EmployeeCommentType EmployeeCommentType { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        [EntityReference(typeof(Employee))]
        public long EmployeeId { get; set; }

        [MaxLength(1024)]
        public string Comment { get; set; }

        public DateTime? RelevantOn { get; set; }

        public long? CreatedById { get; set; }

        public long? ImpersonatedCreatedById { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}

