﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectInvoicingRate", Schema = "Allocation")]
    public class ProjectInvoicingRate : SimpleEntity
    {
        [Index("IX_ProjectCodePerProject", 0, IsUnique = true)]
        public long ProjectSetupId { get; set; }

        [ForeignKey(nameof(ProjectSetupId))]
        public virtual ProjectSetup ProjectSetup { get; set; }

        [Index("IX_ProjectCodePerProject", 1, IsUnique = true)]
        [Required]
        [MaxLength(128)]
        public string Code { get; set; }

        public decimal Value { get; set; }

        public string Description { get; set; }
    }
}

