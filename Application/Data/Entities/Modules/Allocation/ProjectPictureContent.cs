﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ProjectPictureContent", Schema = "Allocation")]
    public class ProjectPictureContent : IEntity
    {
        [Key]
        [ForeignKey("ProjectPicture")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual ProjectPicture ProjectPicture { get; set; }
    }
}

