﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("WeeklyAllocationStatus", Schema = "Allocation")]
    public class WeeklyAllocationStatus : SimpleEntity
    {
        public long EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public DateTime Week { get; set; }

        public decimal AllocatedHours { get; set; }

        public decimal PlannedHours { get; set; }

        public decimal AvailableHours { get; set; }

        public decimal CalendarHours { get; set; }

        public AllocationStatus AllocationStatus { get; set; }

        public EmployeeStatus EmployeeStatus { get; set; }

        public virtual ICollection<WeeklyAllocationDetail> Details { get; set; }

        public DateTime? OnBenchSince { get; set; }

        public StaffingStatus StaffingStatus { get; set; }

        public DateTime? OnBenchUntil { get; set; }
    }
}