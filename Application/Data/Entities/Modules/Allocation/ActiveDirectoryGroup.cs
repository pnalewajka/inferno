﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("ActiveDirectoryGroup", Schema = "Allocation")]
    public class ActiveDirectoryGroup : ModificationTrackedEntity
    {
        public long? OwnerId { get; set; }

        [Index(IsUnique = true)]
        public Guid ActiveDirectoryId { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public ActiveDirectoryGroupType GroupType { get; set; }

        [MaxLength(255)]
        public string Email { get; set; }

        [MaxLength(1023)]
        public string Description { get; set; }

        [MaxLength(1023)]
        public string Info { get; set; }

        [MaxLength(1023)]
        public string Aliases { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<User> Managers { get; set; }

        public virtual User Owner { get; set; }

        public virtual ICollection<ActiveDirectoryGroup> Subgroups { get; set; }

        public virtual ICollection<ActiveDirectoryGroup> ParentGroups { get; set; }
    }
}
