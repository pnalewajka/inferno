﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Allocation
{
    [Table("WeeklyAllocationDetail", Schema = "Allocation")]
    public class WeeklyAllocationDetail : SimpleEntity
    {
        public long WeeklyAllocationStatusId { get; set; }

        [ForeignKey("WeeklyAllocationStatusId")]
        public virtual WeeklyAllocationStatus WeeklyAllocationStatus { get; set; }

        public long ProjectId { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public decimal Hours { get; set; }
    }
}

