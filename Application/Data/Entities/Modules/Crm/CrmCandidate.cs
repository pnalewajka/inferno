﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Crm
{
    [Table("CrmCandidate", Schema = "Crm")]
    public class CrmCandidate : IEntity
    {
        public long Id { get; set; }

        public Guid CrmId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}