﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Scheduling
{
    [Table("JobInternalParameter", Schema = "Scheduling")]
    public class JobInternalParameter : FormattableEntity, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string Name { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string Value { get; set; }
    }
}