﻿using System.Collections.Generic;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Modules.Runtime;

namespace Smt.Atomic.Data.Entities.Modules.Scheduling
{
    [Table("JobDefinition", Schema = "Scheduling")]
    public class JobDefinition : ModificationTrackedEntity
    {
        /// <summary>
        /// Code of job
        /// </summary>
        [MaxLength(100)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        /// <summary>
        /// More detailed job description 
        /// </summary>
        [MaxLength(500)]
        [NonUnicode]
        public string Description { get; set; }

        /// <summary>
        /// Class identifier of .net class that implements IJob interface
        /// </summary>
        [MaxLength(100)]
        [NonUnicode]
        public string ClassIdentifier { get; set; }

        /// <summary>
        /// Cron-like expression for time evaluation
        /// </summary>
        [MaxLength(200)]
        [NonUnicode]
        public string ScheduleSettings { get; set; }

        /// <summary>
        /// Last known job finish time
        /// </summary>
        public DateTime? LastRunOn { get; set; }

        /// <summary>
        /// Last known date of succesful run
        /// </summary>
        public DateTime? LastSuccessOn { get; set; }

        /// <summary>
        /// Last known date of failed run
        /// </summary>
        public DateTime? LastFailureOn { get; set; }

        /// <summary>
        /// Enable/disable job. Will be not started periodically unless enabled = true
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Force job run regardless of cron expression or enabled flag
        /// </summary>
        public bool ShouldRunNow { get; set; }

        /// <summary>
        /// Id of pipeline in which job should be run
        /// </summary>
        public long PipelineId { get; set; }

        [ForeignKey("PipelineId")]
        public virtual Pipeline Pipeline { get; set; }

        /// <summary>
        /// Maximum execution time after which job will be cancelled or terminated
        /// </summary>
        public int MaxExecutionPeriodInSeconds { get; set; }

        /// <summary>
        /// How many attempts to run job in bulk until job will be delayed
        /// </summary>
        public int MaxRunAttemptsInBulk { get; set; }

        /// <summary>
        /// Delay before next bulk attempts
        /// </summary>
        public int DelayBetweenBulkRunAttemptsInSeconds { get; set; }

        /// <summary>
        /// How many times before job should fail 
        /// </summary>
        public int MaxBulkRunAttempts { get; set; }


        public virtual ICollection<JobDefinitionParameter> JobDefinitionParameters { get; set; }
        public virtual ICollection<JobLog> JobLogs { get; set; }
        public virtual ICollection<JobRunInfo> JobRunInfos { get; set; }
    }
}
