﻿using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.Scheduling
{
    [Table("JobDefinitionParameter", Schema = "Scheduling")]
    public class JobDefinitionParameter : ModificationTrackedEntity
    {
        public long JobDefinitionId { get; set; }

        [ForeignKey("JobDefinitionId")]
        public virtual JobDefinition JobDefinition { get; set; }

        [MaxLength(100)]
        [NonUnicode]
        public string ParameterKey { get; set; }

        [MaxLength(100)]
        public string ParameterValue { get; set; }
    }
}
