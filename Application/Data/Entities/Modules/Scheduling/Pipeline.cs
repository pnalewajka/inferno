﻿using System.Collections.Generic;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.Scheduling
{
    [Table("Pipeline", Schema = "Scheduling")]
    public class Pipeline : ModificationTrackedEntity
    {
        /// <summary>
        /// Pipeline name
        /// </summary>
        [MaxLength(100)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public virtual ICollection<JobDefinition> JobDefinitions { get; set; }
    }
}
