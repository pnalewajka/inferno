﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Scheduling
{
    [Table("JobLog", Schema = "Scheduling")]
    public class JobLog : ModificationTrackedEntity
    {
        public long JobDefinitionId { get; set; }

        [ForeignKey("JobDefinitionId")]
        public virtual JobDefinition JobDefinition { get; set; }

        /// <summary>
        /// Instance of scheduler used to run this job
        /// </summary>
        [MaxLength(250)]
        [NonUnicode]
        public string SchedulerName { get; set; }

        /// <summary>
        /// IP or DNS name of computer used to run this process
        /// </summary>
        [MaxLength(250)]
        [NonUnicode]
        public string Host { get; set; }

        /// <summary>
        /// Pipeline in which process was started
        /// </summary>
        [MaxLength(100)]
        [NonUnicode]
        public string Pipeline { get; set; }

        /// <summary>
        /// Severity level of log
        /// </summary>
        [EnumReference]
        public JobLogSeverity Severity { get; set; }

        /// <summary>
        /// Job execution identifier
        /// </summary>
        [Column(TypeName = "uniqueidentifier")]
        [NotNull]
        public Guid ExecutionId { get; set; }

        /// <summary>
        /// Message reported by job (exception, status message, etc)
        /// </summary>
        public string Message { get; set; }
    }
}