﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeAdditionalSkill", Schema = "Resumes")]
    public class ResumeAdditionalSkill : ModificationTrackedEntity, IResume
    {
        public long ResumeId { get; set; }

        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }

        [MaxLength(256)]
        [NotNull]
        [Required]
        public string Name { get; set; }
    }
}

