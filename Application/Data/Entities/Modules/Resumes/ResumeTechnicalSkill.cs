﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;
using System;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeTechnicalSkill", Schema = "Resumes")]
    public class ResumeTechnicalSkill : ModificationTrackedEntity, IResume, ICreationTracked
    {
        [Index("IX_ResumeIdTechnicalSkillId", 1, IsUnique = true)]
        public long ResumeId { get; set; }

        [ForeignKey("ResumeId")]
        public virtual Resume Resume { get; set; }

        [Index("IX_ResumeIdTechnicalSkillId", 2, IsUnique = true)]
        public long TechnicalSkillId { get; set; }

        [ForeignKey(nameof(TechnicalSkillId))]
        public virtual Skill TechnicalSkill { get; set; }

        public long? SkillTagId { get; set; }

        [ForeignKey(nameof(SkillTagId))]
        public virtual SkillTag SkillTag { get; set; }

        public SkillExpertiseLevel ExpertiseLevel { get; set; }

        public SkillExpertisePeriod ExpertisePeriod { get; set; }

        public DateTime CreatedOn { get; set; }

        public long? CreatedById { get; set; }

        public long? ImpersonatedCreatedById { get; set; }

        public string GetShortExpertisePeriodDescription()
        {
            var description = "";
            switch (ExpertisePeriod)
            {
                case SkillExpertisePeriod.LessThanOne:
                    description = "(< 1)";
                    break;
                case SkillExpertisePeriod.OneTwo:
                    description = "(1-2)";
                    break;
                case SkillExpertisePeriod.TwoFive:
                    description = "(2-5)";
                    break;
                case SkillExpertisePeriod.FiveTen:
                    description = "(5-10)";
                    break;
                case SkillExpertisePeriod.MoreThanTen:
                    description = "(> 10)";
                    break;
                default:
                    description = "";
                    break;
            }

            return description;
        }
    }
}

