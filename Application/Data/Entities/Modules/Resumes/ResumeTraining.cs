﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeTraining", Schema = "Resumes")]
    public class ResumeTraining : ModificationTrackedEntity, IResume
    {
        public long ResumeId { get; set; }

        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }

        [MaxLength(256)]
        [NotNull]
        [Required]
        public string Title { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(256)]
        [NotNull]
        [Required]
        public string Institution { get; set; }

        public TrainingType Type { get; set; }
    }
}

