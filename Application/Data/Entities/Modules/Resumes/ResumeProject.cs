﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeProject", Schema = "Resumes")]
    public class ResumeProject : ModificationTrackedEntity
    {
        public long CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public virtual ResumeCompany Company { get; set; }

        public long? ProjectId { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        [MaxLength(512)]
        [NotNull]
        [Required]
        public string Roles { get; set; }

        [MaxLength(4000)]
        [NotNull]
        [Required]
        public string Duties { get; set; }

        [MaxLength(4000)]
        public string ProjectDescription { get; set; }

        [MaxLength(1024)]
        public string Name { get; set; }

        public virtual ICollection<Skill> TechnicalSkills { get; set; }

        public virtual ICollection<Industry> Industries { get; set; }
    }
}

