﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeEducation", Schema = "Resumes")]
    public class ResumeEducation : ModificationTrackedEntity, IResume
    {
        public long ResumeId { get; set; }

        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }

        [MaxLength(512)]
        [NotNull]
        public string UniversityName { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        [MaxLength(256)]
        [NotNull]
        [Required]
        public string Degree { get; set; }

        [MaxLength(256)]
        [NotNull]
        [Required]
        public string Specialization { get; set; }
    }
}
