﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeShareToken", Schema = "Resumes")]
    public class ResumeShareToken : SimpleEntity
    {
        [Index("IX_SharerOwner", 0, IsUnique = true)]
        public long SharerEmployeeId { get; set; }

        [ForeignKey(nameof(SharerEmployeeId))]
        public virtual Employee Sharer { get; set; }

        [Index("IX_SharerOwner", 1, IsUnique = true)]
        public long OwnerEmployeeId { get; set; }

        [ForeignKey(nameof(OwnerEmployeeId))]
        public virtual Employee Owner { get; set; }

        [Required]
        public string Reason { get; set; }

        [Required]
        [Index("IX_UniqueToken", IsUnique = true)]
        public Guid Token { get; set; }
    }
}

