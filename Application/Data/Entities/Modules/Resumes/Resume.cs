﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("Resume", Schema = "Resumes")]
    public class Resume : ModificationTrackedEntity
    {
        [Index(IsUnique = true)]
        public long? EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        public int AvailabilityInDays { get; set; }

        [MaxLength(4000)]
        public string Summary { get; set; }

        public long? CivoResumeVersionId { get; set; }

        public int FillPercentage { get; set; }

        public bool IsConsentClauseForDataProcessingChecked { get; set; }

        public virtual ICollection<ResumeTechnicalSkill> ResumeTechnicalSkills { get; set; }

        public virtual ICollection<ResumeCompany> ResumeCompanies { get; set; }

        public virtual ICollection<ResumeLanguage> ResumeLanguages { get; set; }

        public virtual ICollection<ResumeTraining> ResumeTrainings { get; set; }

        public virtual ICollection<ResumeEducation> ResumeEducations { get; set; }

        public virtual ICollection<Skill> ResumeRejectedSkillSuggestion { get; set; }

        public virtual ICollection<ResumeAdditionalSkill> ResumeAdditionalSkills { get; set; }

        public void CalculateResumeCompleteness()
        {
            int completness = 0;

            if (!string.IsNullOrWhiteSpace(this.Summary) && this.Summary.Length >= 80)
            {
                completness += 20;
            }

            if (this.ResumeCompanies != null && this.ResumeCompanies.Count > 0)
            {
                completness += 20;

                var projectsCount = this.ResumeCompanies.Where(c => c.ResumeProjects != null)
                    .SelectMany(c => c.ResumeProjects)
                    .Count();

                if (projectsCount > 0)
                {
                    completness += 20;
                }
            }

            if (this.ResumeTechnicalSkills != null && this.ResumeTechnicalSkills.Count > 4)
            {
                completness += 20;
            }

            if (this.ResumeEducations != null && this.ResumeEducations.Count > 0)
            {
                completness += 10;
            }

            if (this.ResumeLanguages != null && this.ResumeLanguages.Count > 0)
            {
                completness += 10;
            }

            this.FillPercentage = completness;
        }
    }
}

