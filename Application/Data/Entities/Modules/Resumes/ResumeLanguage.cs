﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeLanguage", Schema = "Resumes")]
    public class ResumeLanguage : ModificationTrackedEntity, IResume
    {
        public long ResumeId { get; set; }

        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }

        public long LanguageId { get; set; }

        [ForeignKey("LanguageId")]
        public Language Language { get; set; }

        public LanguageReferenceLevel Level { get; set; }
    }
}
