﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Resumes
{
    [Table("ResumeCompany", Schema = "Resumes")]
    public class ResumeCompany : ModificationTrackedEntity, IResume
    {
        public long ResumeId { get; set; }

        [ForeignKey("ResumeId")]
        public virtual Resume Resume { get; set; }

        [MaxLength(256)]
        [NotNull]
        [Required]
        public string Name { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public virtual ICollection<ResumeProject> ResumeProjects { get; set; }
    }
}

