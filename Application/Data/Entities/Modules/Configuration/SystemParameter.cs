﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Configuration
{
    [Table("SystemParameter", Schema = "Configuration")]
    public class SystemParameter : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Key { get; set; }

        public string Value { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string ValueType { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string ContextType { get; set; }
    }
}