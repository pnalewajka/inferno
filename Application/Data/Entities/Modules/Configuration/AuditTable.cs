﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Configuration
{
    [Table("atomic_AuditTableView", Schema = "Audit")]
    public class AuditTable : FormattableEntity, IEntity
    {
        public long Id { get; set; }
        public string TableName { get; set; }
        public string Schema { get; set; }
        public bool IsAuditEnabled { get; set; }
    }
}