﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Configuration
{
    [Table("CalendarEntry", Schema = "Configuration")]
    public class CalendarEntry : ModificationTrackedEntity
    {
        public DateTime EntryOn { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public CalendarEntryType CalendarEntryType { get; set; }

        public decimal WorkingHours { get; set; }

        public long CalendarId { get; set; }
    
        [ForeignKey("CalendarId")]
        public virtual Calendar Calendar { get; set; }
    }
}