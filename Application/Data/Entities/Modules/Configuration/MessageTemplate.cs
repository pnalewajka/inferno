﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Configuration
{
    [Table("MessageTemplate", Schema = "Configuration")]
    public class MessageTemplate : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        [EnumReference]
        public MessageTemplateContentType ContentType { get; set; }

        public string Content { get; set; }
    }
}