﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.Configuration
{
    [Table("atomic_CurrentDateTimeView", Schema = "Configuration")]
    public class DbDateTime
    {
        [Key]
        public DateTime CurrentDateTime { get; set; }
    }

}
