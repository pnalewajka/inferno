﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Configuration
{
    [Table("Calendar", Schema = "Configuration")]
    public class Calendar : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NonUnicode]
        [NotNull]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public virtual ICollection<CalendarEntry> Entries { get; set; }

        public IEnumerable<DayOfWeek> GetDefaultDaysOff()
        {
            yield return DayOfWeek.Sunday;
            yield return DayOfWeek.Saturday;
        }
    }
}