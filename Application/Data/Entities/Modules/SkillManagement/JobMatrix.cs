﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("JobMatrix", Schema = "SkillManagement")]
    public class JobMatrix : ModificationTrackedEntity
    {
        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(32)]
        [NonUnicode]
        [NotNull]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        public string Description { get; set; }

        public virtual ICollection<JobMatrixLevel> Levels { get; set; }
    }
}

