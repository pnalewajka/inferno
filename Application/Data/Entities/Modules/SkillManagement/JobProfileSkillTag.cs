﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("JobProfileSkillTag", Schema = "SkillManagement")]
    public class JobProfileSkillTag : ModificationTrackedEntity
    {
            public long JobProfileId { get; set; }

            public long SkillTagId { get; set; }

            public long CustomOrder { get; set; }

            [ForeignKey(nameof(JobProfileId))]
            public virtual JobProfile JobProfile { get; set; }

            [ForeignKey(nameof(SkillTagId))]
            public virtual SkillTag SkillTag { get; set; }        
    }
}

