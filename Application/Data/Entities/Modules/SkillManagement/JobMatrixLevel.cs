﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("JobMatrixLevel", Schema = "SkillManagement")]
    public class JobMatrixLevel : ModificationTrackedEntity
    {
        [MaxLength(32)]
        [NonUnicode]
        [NotNull]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        public int Level { get; set; }

        public string Description { get; set; }

        public long? JobMatrixId { get; set; }
        
        [ForeignKey("JobMatrixId")]
        public virtual JobMatrix JobMatrix { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}

