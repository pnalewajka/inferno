﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("JobTitle", Schema = "SkillManagement")]
    public class JobTitle : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [Index(IsUnique = true)]
        public string NameEn { get; set; }

        [MaxLength(255)]
        [Index(IsUnique = true)]
        public string NamePl { get; set; }

        public long? JobMatrixLevelId { get; set; }

        [ForeignKey("JobMatrixLevelId")]
        public virtual JobMatrixLevel JobMatrixLevel { get; set; }

        public virtual ICollection<SecurityProfile> DefaultProfiles { get; set; }

        public bool IsActive { get; set; }
    }
}