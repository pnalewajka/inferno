﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("JobProfile", Schema = "SkillManagement")]
    public class JobProfile : ModificationTrackedEntity
    {
        [Index(IsUnique = true)]
        [MaxLength(255)]
        [NotNull]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }

        [Index(IsUnique = true)]
        public long? CustomOrder { get; set; }

        public virtual ICollection<JobProfileSkillTag> SkillTags { get; set; }

        public virtual ICollection<Employee> UsedInEmployees { get; set; }

        public virtual ICollection<AddEmployeeRequest> UsedInAddEmployeeRequests { get; set; }

        public virtual ICollection<EmployeeDataChangeRequest> UsedInEmployeeDataChangeRequests { get; set; }
    }
}

