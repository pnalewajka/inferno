﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("Skill", Schema = "SkillManagement")]
    public class Skill : ModificationTrackedEntity, ICreationTracked
    {
        [MaxLength(255)]
        [NotNull]
        [Index(IsUnique = true)]
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<SkillTag> SkillTags { get; set; }
        public virtual ICollection<StaffingDemand> StaffingDemands { get; set; }
        public virtual ICollection<ProjectSetup> ProjectSetups { get; set; }
        public virtual ICollection<ResumeProject> ResumeProjects { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? CreatedById { get; set; }
        public long? ImpersonatedCreatedById { get; set; }
    }
}

