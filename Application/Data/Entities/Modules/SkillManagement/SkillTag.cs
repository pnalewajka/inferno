﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Data.Entities.Modules.SkillManagement
{
    [Table("SkillTag", Schema = "SkillManagement")]
    public class SkillTag : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NotNull]
        [Index(IsUnique = true)]
        public string NameEn { get; set; }
        public string NamePl { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Skill> Skills { get; set; }
        public SkillTagType TagType { get; set; }
        public virtual ICollection<ProjectSetup> ProjectSetups { get; set; }
        public virtual ICollection<PricingEngineer> PricingEngineers { get; set; }
        public virtual ICollection<TechnicalInterviewer> TechnicalInterviewers { get; set; }

        private LocalizedString _name = null;

        [NotMapped]
        public string Name
        {
            get
            {
                return (_name ?? new LocalizedString
                {
                    English = NameEn,
                    Polish = NamePl
                }).ToString();
            }
        }
    }
}

