﻿using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("Challenges", Schema = "CustomerPortal")]
    public class Challenge : ModificationTrackedEntity
    {
        [EntityReference(typeof(Recommendation))]
        public long RecommendationId { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }

        public DateTime Deadline { get; set; }

        public virtual ICollection<ChallengeDocument> ChallengeDocuments { get; set; }

        public virtual ICollection<AnswerDocument> AnswerDocuments { get; set; }

        public string ChallengeComment { get; set; }

        public string AnswerComment { get; set; }
    }
}
