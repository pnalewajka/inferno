﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("LanguageOption", Schema = "CustomerPortal")]
    public class LanguageOption : IEntity
    {
        public long Id { get; set; }

        public int LevelId { get; set; }

        public int LanguageId { get; set; }

        public string LevelName { get; set; }

        public string LanguageName { get; set; }

        public virtual ICollection<ResourceForm> ResourceForms { get; set; }
    }
}

