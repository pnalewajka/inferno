﻿using Smt.Atomic.Data.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    public abstract class CrmDictionaryBase : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public Guid CrmId { get; set; }

        [MaxLength(1000)]
        public string DisplayValue { get; set; }
    }
}
