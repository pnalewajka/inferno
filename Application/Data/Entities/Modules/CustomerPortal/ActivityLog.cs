﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ActivityLog", Schema = "CustomerPortal")]
    public class ActivityLog : ModificationTrackedEntity
    {
        public ActivityLogType ActivityType { get; set; }

        [EntityReference(typeof(Recommendation))]
        public long? RecommendationId { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }

        [EntityReference(typeof(ResourceForm))]
        public long? ResourceFormId { get; set; }

        [ForeignKey("ResourceFormId")]
        public virtual ResourceForm ResourceForm { get; set; }

        public DateTime? SeenByUserOn { get; set; }
    }
}
