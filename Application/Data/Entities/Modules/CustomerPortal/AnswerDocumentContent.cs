﻿using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("AnswerDocumentContents", Schema = "CustomerPortal")]
    public class AnswerDocumentContent : IEntity
    {
        [Key]
        [ForeignKey("AnswerDocument")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual AnswerDocument AnswerDocument { get; set; }
    }
}
