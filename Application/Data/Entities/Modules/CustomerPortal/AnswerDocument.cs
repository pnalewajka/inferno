﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("AnswerDocuments", Schema = "CustomerPortal")]
    public class AnswerDocument : DocumentBase<AnswerDocumentContent>
    {
        public long ChallengeId { get; set; }

        [ForeignKey(nameof(ChallengeId))]
        public virtual Challenge Challenge { get; set; }
    }
}
