﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    public abstract class CrmDictionaryOptionBase : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public int CrmId { get; set; }

        [MaxLength(1000)]
        public string DisplayValue { get; set; }
    }
}