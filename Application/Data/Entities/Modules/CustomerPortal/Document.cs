﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("Documents", Schema = "CustomerPortal")]
    public class Document : DocumentBase<DocumentContent>
    {
        public string Url { get; set; }

        public Guid UniqueId { get; set; }

        public long? RecommendationId { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }
    }
}

