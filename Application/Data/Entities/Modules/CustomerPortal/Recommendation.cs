﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("Recommendations", Schema = "CustomerPortal")]
    public class Recommendation : ModificationTrackedEntity
    {
        [RequiredGuid]
        [Index(IsUnique = true)]
        public Guid ResourceId { get; set; }

        public string Name { get; set; }

        public string Rate { get; set; }

        public DateTime AddedOn { get; set; }

        public string Availability { get; set; }

        [StringLength(500)]
        public string Note { get; set; }

        public float? Years { get; set; }

        public long? UserId { get; set; }

        public long? NeededResourceId { get; set; }

        public bool IsOutside { get; set; }

        [ForeignKey("NeededResourceId")]
        public virtual NeededResource NeededResource { get; set; }

        public virtual ICollection<Document> Documents { get; set; }

        [EnumReference]
        public RecommendationStatus Status { get; set; }

        public virtual ICollection<ScheduleAppointment> ScheduleAppointments { get; set; }

        public virtual ICollection<Challenge> Challenges { get; set; }

        public bool SeenByCustomer { get; set; }

        public DateTime? RejectedOn { get; set; }

        public virtual ICollection<Note> Notes { get; set; } 
    }
}

