﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("NeededResources", Schema = "CustomerPortal")]
    public class NeededResource : ModificationTrackedEntity
    {
        public string Name { get; set; }

        public Guid CrmId { get; set; }

        public virtual ICollection<Recommendation> Recommendations { get; set; }

        public bool IsOnHold { get; set; }

        [MaxLength(2000)]
        public string HoldReason { get; set; }

        public long? ResourceFormId { get; set; }

        [ForeignKey("ResourceFormId")]
        public virtual ResourceForm ResourceForm { get; set; }
    }
}