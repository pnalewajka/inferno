﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("BusinesUnitOptions", Schema = "CustomerPortal")]
    public class BusinesUnitOptions : CrmDictionaryOptionBase
    {
    }
}

