﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ClientData", Schema = "CustomerPortal")]
    public class ClientData : ModificationTrackedEntity
    {
        [EntityReference(typeof(User))]
        [Index(IsUnique = true)]
        public long ClientUserId { get; set; }

        [ForeignKey("ClientUserId")]
        public virtual User ClientUser { get; set; }

        [EntityReference(typeof(User))]
        public long ManagerUserId { get; set; }

        [ForeignKey("ManagerUserId")]
        public virtual User ManagerUser { get; set; }

        [EntityReference(typeof(ClientContact))]
        public long ClientContactId { get; set; }

        [ForeignKey("ClientContactId")]
        public virtual ClientContact ClientContact { get; set; }

        [MaxLength(200)]
        public string ContactPersonPhone { get; set; }
    }
}
