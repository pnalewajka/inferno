﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ResourceForm", Schema = "CustomerPortal")]
    public class ResourceForm : ModificationTrackedEntity
    {
        [EntityReference(typeof(User))]
        public long CreatedById { get; set; }

        [ForeignKey("CreatedById")]
        public virtual User CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [MaxLength(200)]
        public string ResourceName { get; set; }

        public int ResourceCount { get; set; }

        [MaxLength(2000)]
        public string RoleDescription { get; set; }

        [MaxLength(200)]
        public string PlannedStart { get; set; }

        [MaxLength(200)]
        public string ProjectDuration { get; set; }

        [MaxLength(200)]
        public string Location { get; set; }

        [MaxLength(2000)]
        public string ProjectDescription { get; set; }

        [MaxLength(2000)]
        public string PositionRequirements { get; set; }

        [Required]
        public ResourceFormState State { get; set; }

        [EntityReference(typeof(User))]
        public long? SubmittedById { get; set; }

        [ForeignKey("SubmittedById")]
        public virtual User SubmittedBy { get; set; }

        public DateTime? SubmittedOn { get; set; }

        [EntityReference(typeof(User))]
        public long? ApprovedById { get; set; }

        [ForeignKey("ApprovedById")]
        public virtual User ApprovedBy { get; set; }

        public DateTime? ApprovedOn { get; set; }

        [MaxLength(2000)]
        public string LanguageComment { get; set; }

        [MaxLength(2000)]
        public string RecruitmentPhases { get; set; }

        [MaxLength]
        public string AdditionalComment { get; set; }

        public virtual ICollection<LanguageOption> LanguageOption { get; set; }

        public Guid? NeededResourceCrmCode { get; set; }

        [MaxLength(2000)]
        public string Question { get; set; }

        public virtual ICollection<ResourceFormFile> Files { get; set; }

        public ResourceRequestType RequestType { get; set; }
    }
}
