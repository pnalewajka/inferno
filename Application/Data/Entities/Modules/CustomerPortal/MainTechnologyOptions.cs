﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("MainTechnologyOptions", Schema = "CustomerPortal")]
    public class MainTechnologyOptions : CrmDictionaryBase
    {
    }
}