﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("DocumentContent", Schema = "CustomerPortal")]
    public class DocumentContent : IEntity
    {
        [Key]
        [ForeignKey("Document")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual Document Document { get; set; }
    }
}

