using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("Questions", Schema = "CustomerPortal")]
    public class Question : ModificationTrackedEntity
    {
        [Required]
        public long RecommendationId { get; set; }

        [Required]
        public long UserId { get; set; }

        public string Query { get; set; }
        
        public DateTime QueryDate { get; set; }

        public bool WasSent { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}

