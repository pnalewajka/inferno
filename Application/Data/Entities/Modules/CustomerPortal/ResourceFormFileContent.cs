﻿using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ResourceFormFileContent", Schema = "CustomerPortal")]
    public class ResourceFormFileContent: IEntity
    {
        [Key]
        [ForeignKey("ResourceFormFile")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual ResourceFormFile ResourceFormFile { get; set; }
    }
}
