﻿using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ChallengeDocumentContents", Schema = "CustomerPortal")]
    public class ChallengeDocumentContent : IEntity
    {
        [Key]
        [ForeignKey("ChallengeDocument")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual ChallengeDocument ChallengeDocument { get; set; }
    }
}
