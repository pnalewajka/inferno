﻿using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ResourceFormFile", Schema = "CustomerPortal")]
    public class ResourceFormFile : DocumentBase<ResourceFormFileContent>, IEntity
    {
        public long ResourceFormId { get; set; }

        [ForeignKey("ResourceFormId")]
        public virtual ResourceForm ResourceForm { get; set; }
    }
}
