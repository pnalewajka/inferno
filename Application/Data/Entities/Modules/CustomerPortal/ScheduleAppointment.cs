﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ScheduleAppointment", Schema = "CustomerPortal")]
    public class ScheduleAppointment : ModificationTrackedEntity
    {
        public DateTime AppointmentDate { get; set; }

        public DateTime AppointmentEndDate { get; set; }

        public AppointmentStatusType AppointmentStatus { get; set; }

        [EntityReference(typeof(Recommendation))]
        public long RecommendationId { get; set; }

        public InterviewType InterviewType { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }

        public DateTime? ReminderTriggered { get; set; }
    }
}

