﻿using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("Note", Schema = "CustomerPortal")]
    public class Note : ModificationTrackedEntity
    {
        [StringLength(1000)]
        public string Content { get; set; }

        public long? RecommendationId { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }

        public bool SavedInCRM { get; set; }

        [EntityReference(typeof(User))]
        public long? UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}

