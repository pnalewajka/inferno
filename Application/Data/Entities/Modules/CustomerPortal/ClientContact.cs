﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ClientContact", Schema = "CustomerPortal")]
    public class ClientContact : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public Guid CrmId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public Guid CompanyGuidId { get; set; }

        public string CompanyName { get; set; }
    }
}
