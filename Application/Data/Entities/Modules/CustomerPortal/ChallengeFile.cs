using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.CustomerPortal
{
    [Table("ChallengeFile", Schema = "CustomerPortal")]
    public class ChallengeFile : ModificationTrackedEntity
    {
        [MaxLength(1024)]
        public string Name { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string ContentType { get; set; }

        [Index]
        public Guid Identifier { get; set; }

        public long ContentLength { get; set; }

        public byte[] Data { get; set; }

        [EntityReference(typeof(Recommendation))]
        public long? RecommendationId { get; set; }

        [ForeignKey("RecommendationId")]
        public virtual Recommendation Recommendation { get; set; }
    }
}