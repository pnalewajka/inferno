﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Consents
{
    [Table("ConsentDecision", Schema = "Consents")]
    public class ConsentDecision : ModificationTrackedEntity
    {
        [EntityReference(typeof(User))]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        public string EnumIdentifier { get; set; }

        public string EnumValue { get; set; }

        public ConsentDecisionType ConsentDecisionType { get; set; }
    }
}

