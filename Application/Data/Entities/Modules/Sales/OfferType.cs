﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Sales
{
    [Table("OfferType", Schema = "Sales")]
    public class OfferType : SimpleEntity
    {

        [MaxLength(100)]
        [Required]
        public string OfferName { get; set; }

        public virtual ICollection<Inquiry> Inquiries { get; set; }
    }
}

