﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Sales
{
    [Table("InquiryNote", Schema = "Sales")]
    public class InquiryNote : ModificationTrackedEntity
    {
        public long InquiryId { get; set; }

        [ForeignKey(nameof(InquiryId))]
        public Inquiry Inquiry { get; set; }

        [StringLength(3000)]
        [Required]
        public string Content { get; set; }

        public virtual ICollection<Employee> MentionedEmployees { get; set; }
    }
}

