﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Sales
{
    [Table("Customer", Schema = "Sales")]
    public class Customer : SimpleEntity
    {
        [MaxLength(250)]
        public string LegalName { get; set; }

        [MaxLength(50)]
        public string ShortName { get; set; }

        public long? Priority { get; set; }

        public virtual ICollection<Inquiry> Inquiries { get; set; }
    }
}
