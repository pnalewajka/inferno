﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Organization;

namespace Smt.Atomic.Data.Entities.Modules.Sales
{
    [Table("Inquiry", Schema = "Sales")]
    public class Inquiry : ModificationTrackedEntity
    {
        public long CustomerId { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public Customer Customer { get; set; }

        [MaxLength(500)]
        public string DisplayName { get; set; }

        public long? BusinessLineOrgUnitId { get; set; }

        [ForeignKey(nameof(BusinessLineOrgUnitId))]
        public OrgUnit BusinessLineOrgUnit { get; set; }

        [MaxLength(1000)]
        public string Description {get;set;}

        public long SalesPersonId { get; set; }

        [ForeignKey(nameof(SalesPersonId))]
        public Employee SalesPerson { get; set; }

        public InquiryStatus ProcessStatus { get; set; }

        public int? ProjectPropability { get; set; }

        public ImportanceType ProjectImportance { get; set; }

        public decimal? ExpectedRevenue { get; set; }

        public long? ExpectedRevenueCurrencyId { get; set; }

        public Currency ExpectedRevenueCurrency { get; set; }

        public decimal? ExpectedMargin { get; set; }

        public decimal? MaxPreSalesSpending { get; set; }

        public DateTime? OfferDueDate { get; set; }

        public long OfferTypeId { get; set; }

        [ForeignKey(nameof(OfferTypeId))]
        public OfferType OfferType { get; set; }

        public virtual ICollection<InquiryNote> Notes { get; set; }

        public virtual ICollection<Employee> Watchers { get; set; }
    }
}
