﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Organization
{
    [Table("PricingEngineer", Schema = "Organization")]
    public class PricingEngineer : ModificationTrackedEntity
    {
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        [EntityReference(typeof(Employee))]
        public long EmployeeId { get; set; }

        public virtual ICollection<SkillTag> SkillTags { get; set; }
    }
}