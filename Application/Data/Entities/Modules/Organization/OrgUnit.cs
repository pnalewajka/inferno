﻿using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Data.Entities.Modules.Organization
{
    [Table("OrgUnit", Schema = "Organization")]
    public class OrgUnit : ModificationTrackedEntity, ITreeNodeEntity
    {
        [MaxLength(32)]
        [NonUnicode]
        [NotNull]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        [MaxLength(900)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Path { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(64)]
        [Index(IsUnique = true)]
        public string FlatName { get; set; }

        [Index(IsUnique = false)]
        public long NameSortOrder { get; set; }

        public long DescendantCount { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public long? ParentId { get; set; }

        [ForeignKey("ParentId")]
        public virtual OrgUnit Parent { get; set; }

        public int? Depth { get; set; }

        [ForeignKey("CalendarId")]
        public virtual Calendar Calendar { get; set; }

        public long? CalendarId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public long? EmployeeId { get; set; }

        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }

        public long? RegionId { get; set; }

        public OrgUnitFeatures OrgUnitFeatures { get; set; }

        public long? CustomOrder { get; set; }

        public InheritableBool DefaultProjectContributionMode { get; set; }

        public TimeReportingMode? TimeReportingMode { get; set; }

        public long? AbsenceOnlyDefaultProjectId { get; set; }

        [ForeignKey("AbsenceOnlyDefaultProjectId")]
        public virtual Project AbsenceOnlyDefaultProject { get; set; }

        [EnumReference]
        [Column("OrgUnitManagerRole")]
        public OrgUnitManagerRole? OrgUnitManagerRole { get; set; }
    }
}
