﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Organization
{
    [Table("TechnicalInterviewer", Schema = "Organization")]
    public class TechnicalInterviewer : ModificationTrackedEntity
    {
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        [EntityReference(typeof(Employee))]
        public long EmployeeId { get; set; }

        public int CandidateMaxLevel { get; set; }

        public bool CanInterviewForeignCandidates { get; set; }

        [MaxLength(128)]
        public string Comments { get; set; }

        public virtual ICollection<SkillTag> SkillTags { get; set; }
    }
}

