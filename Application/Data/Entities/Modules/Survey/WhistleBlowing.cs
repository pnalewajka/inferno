﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("WhistleBlowing", Schema = "Survey")]
    public class WhistleBlowing : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }

        [MaxLength(2048)]
        [Required]
        public string Message { get; set; }

        public bool ShowUserName { get; set; }

        public virtual ICollection<WhistleBlowingDocument> Documents { get; set; }

        public WhistleBlowingMessageStatus MessageStatus { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}

