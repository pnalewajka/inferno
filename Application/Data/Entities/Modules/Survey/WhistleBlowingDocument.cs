﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("WhistleBlowingDocument", Schema = "Survey")]
    public class WhistleBlowingDocument : DocumentBase<WhistleBlowingDocumentContent>
    {
        public long WhistleBlowingId { get; set; }

        [ForeignKey("WhistleBlowingId")]
        public virtual WhistleBlowing WhistleBlowing { get; set; }
    }
}

