﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("WhistleBlowingDocumentContent", Schema = "Survey")]
    public class WhistleBlowingDocumentContent : FormattableEntity, IEntity
    {
        [Key]
        [ForeignKey("WhistleBlowingDocument")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual WhistleBlowingDocument WhistleBlowingDocument { get; set; }
    }
}

