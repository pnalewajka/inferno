﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("SurveyConductQuestion", Schema = "Survey")]
    public class SurveyConductQuestion : ModificationTrackedEntity
    {
        public long SurveyConductId { get; set; }

        [MaxLength(255)]
        public string Text { get; set; }

        public SurveyQuestionType Type { get; set; }

        public long Order { get; set; }

        public string Items { get; set; }

        public string DefaultAnswer { get; set; }

        public bool IsRequired { get; set; }

        public SurveyFieldSize FieldSize { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public string VisibilityCondition { get; set; }

        [MaxLength(255)]
        public string FieldName { get; set; }

        public virtual SurveyConduct SurveyConduct { get; set; }
    }
}

