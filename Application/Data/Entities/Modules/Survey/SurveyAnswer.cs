﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("SurveyAnswer", Schema = "Survey")]
    public class SurveyAnswer : ModificationTrackedEntity
    {
        public long SurveyId { get; set; }

        [ForeignKey("SurveyId")]
        public virtual Survey Survey { get; set; }

        public long QuestionId { get; set; }

        [NotMapped]
        public string QuestionText { get { return Questions != null ? Questions.Text : null; } }

        [ForeignKey("QuestionId")]
        public virtual SurveyConductQuestion Questions { get; set; }

        public string Answer { get; set; }
    }
}

