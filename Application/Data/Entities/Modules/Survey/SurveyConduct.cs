﻿using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Organization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("SurveyConduct", Schema = "Survey")]
    public class SurveyConduct : ModificationTrackedEntity
    {
        public long? SurveyDefinitionId { get; set; }

        [ForeignKey("SurveyDefinitionId")]
        public virtual SurveyDefinition SurveyDefinition { get; set; }

        public string SurveyName { get; set; }

        public string SurveyDescription { get; set; }

        public bool ShouldAutoClose { get; set; }
        
        public string SurveyWelcomeInstruction { get; set; }

        public string SurveyThankYouMessage { get; set; }

        public DateTime? OpenedOn { get; set; }

        [NotMapped]
        public string PollsterFullName { get { return OpenedBy != null ? $"{OpenedBy.FirstName} {OpenedBy.LastName}" : null; } }

        public long? OpenedById { get; set; }

        [ForeignKey("OpenedById")]
        public virtual User OpenedBy { get; set; }

        public bool IsClosed { get; set; }

        public virtual ICollection<OrgUnit> OrgUnits { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }

        public virtual ICollection<SurveyConductQuestion> Questions { get; set; }

        public virtual ICollection<ActiveDirectoryGroup> ActiveDirectoryGroups { get; set; }
    }
}

