﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using System.Collections.Generic;
using System;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("Survey", Schema = "Survey")]
    public class Survey : ModificationTrackedEntity
    {
        public long SurveyConductId { get; set; }

        [ForeignKey("SurveyConductId")]
        public virtual SurveyConduct SurveyConduct { get; set; }

        public long RespondentId { get; set; }

        public DateTime? LastEditedAt { get; set; }

        public bool IsCompleted { get; set; }

        [ForeignKey("RespondentId")]
        public virtual User Respondent { get; set; }

        public bool IsConfirmationSent { get; set; }

        public virtual ICollection<SurveyAnswer> Answers { get; set; }
    }
}

