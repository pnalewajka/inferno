﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("SurveyQuestion", Schema = "Survey")]
    public class SurveyQuestion : ModificationTrackedEntity
    {
        [Index("IX_SurveyDefinitionIdFieldName", 1, IsClustered = true, IsUnique = true)]
        public long SurveyDefinitionId { get; set; }

        [MaxLength(255)]
        public string Text { get; set; }

        public SurveyQuestionType Type { get; set; }

        public long Order { get; set; }

        public string Items { get; set; }

        public string DefaultAnswer { get; set; }

        public bool IsRequired { get; set; }

        public SurveyFieldSize FieldSize { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public string VisibilityCondition { get; set; }

        [MaxLength(255)]
        [Index("IX_SurveyDefinitionIdFieldName", 2, IsClustered = true, IsUnique = true)]
        public string FieldName { get; set; }

        public virtual SurveyDefinition SurveyDefinition { get; set; }
    }
}

