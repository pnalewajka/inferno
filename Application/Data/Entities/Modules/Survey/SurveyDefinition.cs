﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Entities.Modules.Survey
{
    [Table("SurveyDefinition", Schema = "Survey")]
    public class SurveyDefinition : ModificationTrackedEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool ShouldAutoClose { get; set; }
        
        public string WelcomeInstruction { get; set; }

        public string ThankYouMessage { get; set; }

        public virtual ICollection<SurveyQuestion> SurveyQuestions { get; set; }
    }
}

