﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Notifications
{
    [Table("EmailAttachment", Schema = "Notifications")]
    public class EmailAttachment : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }

        public byte[] Content { get; set; }

        public long EmailId { get; set; }

        public virtual Email Email { get; set; }
    }
}

