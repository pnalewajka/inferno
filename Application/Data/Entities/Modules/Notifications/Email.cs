﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Notifications
{
    [Table("Email", Schema = "Notifications")]
    public class Email : FormattableEntity, IEntity, ICreationTracked
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [Index("EmailSmartSearchIndex", Order = 1)]
        [MaxLength(384)]
        public string To { get; set; }

        [Required]
        [Index("EmailSmartSearchIndex", Order = 2)]
        [MaxLength(128)]
        public string From { get; set; }

        [Required]
        [Index("EmailSmartSearchIndex", Order = 3)]
        [MaxLength(256)]
        public string Subject { get; set; }

        [Required]
        [EnumReference]
        public EmailStatus Status { get; set; }

        public DateTime? SentOn { get; set; }

        [Required]
        public string Content { get; set; }

        public string Exception { get; set; }

        [EntityReference(typeof(User))]
        public long? CreatedById { get; set; }

        [EntityReference(typeof(User))]
        public long? ImpersonatedCreatedById { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [ForeignKey(nameof(CreatedById))]
        public virtual User CreatedBy { get; set; }

        [ForeignKey(nameof(ImpersonatedCreatedById))]
        public virtual User ImpersonatedCreatedBy { get; set; }

        public virtual ICollection<EmailAttachment> Attachments { get; set; }
    }
}
