﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("BusinessTripParticipant", Schema = "BusinessTrips")]
    public class BusinessTripParticipant : SimpleEntity
    {
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime EndedOn { get; set; }

        [MaxLength(32)]
        public string IdCardNumber { get; set; }

        [MaxLength(32)]
        public string PhoneNumber { get; set; }

        [MaxLength(128)]
        public string HomeAddress { get; set; }

        public MeansOfTransport TransportationPreferences { get; set; }

        public AccommodationType AccommodationPreferences { get; set; }

        public int DepartureCityTaxiVouchers { get; set; }

        public int DestinationCityTaxiVouchers { get; set; }

        public virtual ICollection<DeclaredLuggage> LuggageDeclarations { get; set; }

        public string ItineraryDetails { get; set; }

        public long BusinessTripId { get; set; }

        [ForeignKey(nameof(BusinessTripId))]
        public virtual BusinessTrip BusinessTrip { get; set; }

        public long DepartureCityId { get; set; }

        [ForeignKey(nameof(DepartureCityId))]
        public virtual City DepartureCity { get; set; }

        public virtual ICollection<ArrangementTrackingEntry> ArrangementTrackingEntries { get; set; }

        public virtual ICollection<Arrangement> Arrangements { get; set; }

        public virtual ICollection<BusinessTripSettlementRequest> SettlementRequests { get; set; }

        public virtual ICollection<AdvancedPaymentRequest> AdvancedPaymentRequests { get; set; }
    }
}
