﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("DailyAllowance", Schema = "BusinessTrips")]
    public class DailyAllowance : ModificationTrackedEntity
    {
        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public long EmploymentCountryId { get; set; }

        [ForeignKey(nameof(EmploymentCountryId))]
        public virtual Country EmploymentCountry { get; set; }

        public long? TravelCityId { get; set; }

        [ForeignKey(nameof(TravelCityId))]
        public virtual City TravelCity { get; set; }

        public long? TravelCountryId { get; set; }

        [ForeignKey(nameof(TravelCountryId))]
        public virtual Country TravelCountry { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }

        public decimal Allowance { get; set; }

        public decimal? DepartureAndArrivalDayAllowance { get; set; }

        public decimal AccommodationLumpSum { get; set; }
    }
}
