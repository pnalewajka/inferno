﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("Arrangement", Schema = "BusinessTrips")]
    public class Arrangement : SimpleEntity
    {
        public long BusinessTripId { get; set; }

        [ForeignKey(nameof(BusinessTripId))]
        public virtual BusinessTrip BusinessTrip { get; set; }

        public ArrangementType ArrangementType { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        public DateTime IssuedOn { get; set; }

        public virtual ICollection<BusinessTripParticipant> Participants { get; set; }

        [MaxLength(2000)]
        public string Details { get; set; }

        public long? CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public Currency Currency { get; set; }

        public decimal? Value { get; set; }

        public virtual ICollection<ArrangementDocument> ArrangementDocuments { get; set; }

        public long CreatedByUserId { get; set; }

        [ForeignKey(nameof(CreatedByUserId))]
        public virtual User CreatedByUser { get; set; }
    }
}