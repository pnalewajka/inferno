﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("BusinessTripAcceptanceConditions", Schema = "BusinessTrips")]
    public class BusinessTripAcceptanceConditions : IEntity, IAcceptanceConditions
    {
        [Key]
        public long Id { get; set; }
        
        public virtual BusinessTrip BusinessTrip { get; set; }

        public bool ReinvoicingToCustomer { get; set; }

        public long? ReinvoiceCurrencyId { get; set; }

        [ForeignKey(nameof(ReinvoiceCurrencyId))]
        public virtual Currency ReinvoiceCurrency { get; set; }

        public CostApprovalPolicy CostApprovalPolicy { get; set; }

        public long? MaxCostsCurrencyId { get; set; }

        [ForeignKey(nameof(MaxCostsCurrencyId))]
        public virtual Currency MaxCostsCurrency { get; set; }

        public decimal? MaxHotelCosts { get; set; }

        public decimal? MaxTotalFlightsCosts { get; set; }

        public decimal? MaxOtherTravelCosts { get; set; }
    }
}

