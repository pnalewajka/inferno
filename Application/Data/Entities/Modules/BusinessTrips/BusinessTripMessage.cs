﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("BusinessTripMessage", Schema = "BusinessTrips")]
    public class BusinessTripMessage : SimpleEntity
    {
        public long AuthorEmployeeId { get; set; }

        [ForeignKey(nameof(AuthorEmployeeId))]
        public virtual Employee Author { get; set; }

        public DateTime AddedOn { get; set; }

        public string Content { get; set; }

        public long BusinessTripId { get; set; }

        [ForeignKey(nameof(BusinessTripId))]
        public virtual BusinessTrip BusinessTrip { get; set; }

        public virtual ICollection<Employee> MentionedEmployees { get; set; }
    }
}
