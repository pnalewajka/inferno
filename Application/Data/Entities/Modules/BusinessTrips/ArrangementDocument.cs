﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("ArrangementDocument", Schema = "BusinessTrips")]
    public class ArrangementDocument : ModificationTrackedEntity
    {
        [MaxLength(255)]
        public string Name { get; set; }

        public Guid UniqueId { get; set; }

        [MaxLength(250)]
        [NonUnicode]
        public string ContentType { get; set; }

        public long ContentLength { get; set; }

        public long ArrangementId { get; set; }

        [ForeignKey("ArrangementId")]
        public virtual Arrangement Arrangement { get; set; }

        public virtual ArrangementDocumentContent DocumentContent { get; set; }
    }
}