﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("ArrangementDocumentContent", Schema = "BusinessTrips")]
    public class ArrangementDocumentContent : IEntity
    {
        [Key]
        [ForeignKey("Document")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual ArrangementDocument Document { get; set; }
    }
}