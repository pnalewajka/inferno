﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("BusinessTrip", Schema = "BusinessTrips")]
    public class BusinessTrip : IEntity
    {
        [Key]
        public long Id { get; set; }
        
        [ForeignKey(nameof(Id))]
        public virtual BusinessTripRequest Request { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

        public virtual ICollection<BusinessTripParticipant> Participants { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime EndedOn { get; set; }

        public long DestinationCityId { get; set; }

        [ForeignKey(nameof(DestinationCityId))]
        public virtual City DestinationCity { get; set; }

        [MaxLength(2000)]
        public string TravelExplanation { get; set; }

        public string ItineraryDetails { get; set; }

        public string ArrangementTrackingRemarks { get; set; }

        public long? FrontDeskAssigneeEmployeeId { get; set; }

        [ForeignKey(nameof(FrontDeskAssigneeEmployeeId))]
        public virtual Employee FrontDeskAssignee { get; set; }

        public virtual ICollection<Arrangement> Arrangements { get; set; }

        public virtual BusinessTripAcceptanceConditions BusinessTripAcceptanceConditions { get; set; }

        public virtual ICollection<BusinessTripMessage> Messages { get; set; }
    }
}
