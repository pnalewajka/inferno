﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.BusinessTrips
{
    [Table("ArrangementTrackingEntry", Schema = "BusinessTrips")]
    public class ArrangementTrackingEntry : SimpleEntity
    {
        [Index("IX_ArrangementTypePerParticipant", 1, IsUnique = true)]
        public long ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual BusinessTripParticipant Participant { get; set; }

        [Index("IX_ArrangementTypePerParticipant", 2, IsUnique = true)]
        public TrackedArrangement ArrangementType { get; set; }

        public ArrangementTrackingStatus Status { get; set; }
    }
}

