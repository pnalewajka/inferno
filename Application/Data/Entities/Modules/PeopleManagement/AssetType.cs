﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.PeopleManagement
{
    [Table("AssetType", Schema = "PeopleManagement")]
    public class AssetType : SimpleEntity
    {
        [Index(IsUnique = true)]
        [MaxLength(255)]
        [NotNull]
        public string NameEn { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(255)]
        [NotNull]
        public string NamePl { get; set; }

        public AssetTypeCategory Category { get; set; }

        public bool DetailsRequired { get; set; }

        [MaxLength(255)]
        public string DetailsHintEn { get; set; }

        [MaxLength(255)]
        public string DetailsHintPl { get; set; }

        public bool IsActive { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public virtual ICollection<AssetsRequest> AssetsRequests { get; set; }
    }
}
