﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.PeopleManagement
{
    [Table("EmploymentTerminationReason", Schema = "PeopleManagement")]
    public class EmploymentTerminationReason : SimpleEntity
    {
        [MaxLength(255)]
        [NotNull]
        [Required(AllowEmptyStrings = false)]
        public string NameEn { get; set; }

        [MaxLength(255)]
        [NotNull]
        [Required(AllowEmptyStrings = false)]
        public string NamePl { get; set; }

        [MaxLength(512)]
        public string DescriptionEn { get; set; }

        [MaxLength(512)]
        public string DescriptionPl { get; set; }

        public bool IsVoluntarilyTurnover { get; set; }

        public bool IsOtherDepartureReasons { get; set; }
    }
}