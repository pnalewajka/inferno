﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.PeopleManagement
{
    [Table("UserAcronym", Schema = "PeopleManagement")]
    public class UserAcronym : SimpleEntity
    {
        [MaxLength(20)]
        [Index(IsUnique = true)]
        public string Acronym { get; set; }
    }
}