﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequestCompanyCost", Schema = "Workflows")]
    public class BusinessTripSettlementRequestCompanyCost : SimpleEntity, IBusinessTripSettlementRequestCost
    {
        public long BusinessTripSettlementRequestId { get; set; }

        [ForeignKey(nameof(BusinessTripSettlementRequestId))]
        public virtual BusinessTripSettlementRequest Request { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime TransactionDate { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }

        public decimal Amount { get; set; }

        public BusinessTripSettlementPaymentMethod PaymentMethod { get; set; }

        public long? CardHolderEmployeeId { get; set; }

        [ForeignKey(nameof(CardHolderEmployeeId))]
        public virtual Employee CardHolder { get; set; }

        public bool HasInvoiceIssued { get; set; }
    }
}
