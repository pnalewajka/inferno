﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("DeclaredLuggage", Schema = "Workflows")]
    public class DeclaredLuggage : SimpleEntity
    {
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public int HandLuggageItems { get; set; }

        public int RegisteredLuggageItems { get; set; }

        public long? RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual BusinessTripRequest Request { get; set; }

        public long? ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual BusinessTripParticipant Participant { get; set; }
    }
}
