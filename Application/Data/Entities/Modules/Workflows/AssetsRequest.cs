﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("AssetsRequest", Schema = "Workflows")]
    public class AssetsRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long? OnboardingRequestId { get; set; }

        [ForeignKey(nameof(OnboardingRequestId))]
        public virtual OnboardingRequest OnboardingRequest { get; set; }

        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public virtual ICollection<AssetType> AssetTypes { get; set; }

        [MaxLength(1024)]
        public string AssetTypeHardwareSetComment { get; set; }

        [MaxLength(1024)]
        public string AssetTypeHeadsetComment { get; set; }

        [MaxLength(1024)]
        public string AssetTypeBackpackComment { get; set; }

        [MaxLength(1024)]
        public string AssetTypeOptionalHardwareComment { get; set; }

        [MaxLength(1024)]
        public string AssetTypeSoftwareComment { get; set; }

        [MaxLength(1024)]
        public string AssetTypeOtherComment { get; set; }

        [MaxLength(1024)]
        public string CommentForIT { get; set; }
    }
}
