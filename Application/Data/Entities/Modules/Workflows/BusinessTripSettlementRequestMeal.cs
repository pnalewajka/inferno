﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequestMeal", Schema = "Workflows")]
    public class BusinessTripSettlementRequestMeal : SimpleEntity
    {
        public long BusinessTripSettlementRequestId { get; set; }

        [ForeignKey(nameof(BusinessTripSettlementRequestId))]
        public virtual BusinessTripSettlementRequest Request { get; set; }

        public long? CountryId { get; set; }

        [ForeignKey(nameof(CountryId))]
        public virtual Country Country { get; set; }

        public DateTime? Day { get; set; }

        public long BreakfastCount { get; set; }

        public long LunchCount { get; set; }

        public long DinnerCount { get; set; }
    }
}
