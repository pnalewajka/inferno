﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequestAdvancePayment", Schema = "Workflows")]
    public class BusinessTripSettlementRequestAdvancePayment : SimpleEntity
    {   
        public long RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual BusinessTripSettlementRequest Request { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }

        public decimal Amount { get; set; }

        public DateTime WithdrawnOn { get; set; }
    }
}
