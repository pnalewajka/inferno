﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using System;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Configuration;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("AddEmployeeRequest", Schema = "Workflows")]
    public class AddEmployeeRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public long JobTitleId { get; set; }

        [ForeignKey(nameof(JobTitleId))]
        public virtual JobTitle JobTitle { get; set; }

        public string CrmId { get; set; }

        public long JobProfileId { get; set; }

        [ForeignKey(nameof(JobProfileId))]
        public virtual JobProfile JobProfile { get; set; }

        public long OrgUnitId { get; set; }

        [ForeignKey(nameof(OrgUnitId))]
        public virtual OrgUnit OrgUnit { get; set; }

        public long LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }

        public PlaceOfWork PlaceOfWork { get; set; }

        public long CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company { get; set; }

        public long LineManagerId { get; set; }

        [ForeignKey(nameof(LineManagerId))]
        public virtual Employee LineManager { get; set; }

        public DateTime ContractFrom { get; set; }

        public DateTime? ContractTo { get; set; }

        public DateTime ContractSignDate { get; set; }

        public decimal MondayHours { get; set; }

        public decimal TuesdayHours { get; set; }

        public decimal WednesdayHours { get; set; }

        public decimal ThursdayHours { get; set; }

        public decimal FridayHours { get; set; }

        public decimal SaturdayHours { get; set; }

        public decimal SundayHours { get; set; }

        public long ContractTypeId { get; set; }

        public ContractDurationType? ContractDuration { get; set; }

        [ForeignKey(nameof(ContractTypeId))]
        public virtual ContractType ContractType { get; set; }

        public decimal VacationHourToDayRatio { get; set; }

        public TimeReportingMode TimeReportingMode { get; set; }

        public long? AbsenceOnlyDefaultProjectId { get; set; }

        [ForeignKey(nameof(AbsenceOnlyDefaultProjectId))]
        public virtual Project AbsenceOnlyDefaultProject { get; set; }

        public long? CalendarId { get; set; }

        [ForeignKey(nameof(CalendarId))]
        public virtual Calendar Calendar { get; set; }
    }
}

