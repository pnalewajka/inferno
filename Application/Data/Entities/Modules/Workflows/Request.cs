﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("Request", Schema = "Workflows")]
    public class Request : ModificationTrackedEntity, ICreationTracked
    {
        public long RequestingUserId { get; set; }

        [ForeignKey("RequestingUserId")]
        public virtual User RequestingUser { get; set; }

        public RequestStatus Status { get; set; }

        public virtual RequestType RequestType { get; set; }

        [MaxLength(512)]
        public string DescriptionEn { get; set; }

        [MaxLength(512)]
        public string DescriptionPl { get; set; }

        [MaxLength(255)]
        public string RejectionReason { get; set; }

        public virtual ICollection<ApprovalGroup> ApprovalGroups { get; set; }

        public virtual ICollection<User> Watchers { get; set; }

        public DateTime? DeferredUntil { get; set; }

        public virtual ICollection<RequestDocument> Documents { get; set; }

        public virtual ICollection<RequestHistoryEntry> HistoryEntries { get; set; }

        [MaxLength(512)]
        public string Exception { get; set; }

        public long? CreatedById { get; set; }

        public long? ImpersonatedCreatedById { get; set; }

        public DateTime CreatedOn { get; set; }

        // RequestData

        [NotMapped]
        public IRequestEntity RequestObject
        {
            get
            {
                switch (RequestType)
                {
                    case RequestType.SkillChangeRequest: return SkillChangeRequest;
                    case RequestType.AddEmployeeRequest: return AddEmployeeRequest;
                    case RequestType.AbsenceRequest: return AbsenceRequest;
                    case RequestType.AddHomeOfficeRequest: return AddHomeOfficeRequest;
                    case RequestType.OvertimeRequest: return OvertimeRequest;
                    case RequestType.ProjectTimeReportApprovalRequest: return ProjectTimeReportApprovalRequest;
                    case RequestType.EmploymentTerminationRequest: return EmploymentTerminationRequest;
                    case RequestType.EmployeeDataChangeRequest: return EmployeeDataChangeRequest;
                    case RequestType.ReopenTimeTrackingReportRequest: return ReopenTimeTrackingReportRequest;
                    case RequestType.EmploymentDataChangeRequest: return EmploymentDataChangeRequest;
                    case RequestType.BusinessTripRequest: return BusinessTripRequest;
                    case RequestType.OnboardingRequest: return OnboardingRequest;
                    case RequestType.AssetsRequest: return AssetsRequest;
                    case RequestType.FeedbackRequest: return FeedbackRequest;
                    case RequestType.BusinessTripSettlementRequest: return BusinessTripSettlementRequest;
                    case RequestType.TaxDeductibleCostRequest: return TaxDeductibleCostRequest;
                    case RequestType.BonusRequest: return BonusRequest;
                    case RequestType.ExpenseRequest: return ExpenseRequest;
                    default: throw new InvalidOperationException($"RequestData mapping not found for type: {RequestType}");
                }
            }
        }

        public virtual SkillChangeRequest SkillChangeRequest { get; set; }
        public virtual AddEmployeeRequest AddEmployeeRequest { get; set; }
        public virtual AbsenceRequest AbsenceRequest { get; set; }
        public virtual AddHomeOfficeRequest AddHomeOfficeRequest { get; set; }
        public virtual OvertimeRequest OvertimeRequest { get; set; }
        public virtual ProjectTimeReportApprovalRequest ProjectTimeReportApprovalRequest { get; set; }
        public virtual EmploymentTerminationRequest EmploymentTerminationRequest { get; set; }
        public virtual EmployeeDataChangeRequest EmployeeDataChangeRequest { get; set; }
        public virtual ReopenTimeTrackingReportRequest ReopenTimeTrackingReportRequest { get; set; }
        public virtual EmploymentDataChangeRequest EmploymentDataChangeRequest { get; set; }
        public virtual BusinessTripRequest BusinessTripRequest { get; set; }
        public virtual AdvancedPaymentRequest AdvancedPaymentRequest { get; set; }
        public virtual OnboardingRequest OnboardingRequest { get; set; }
        public virtual AssetsRequest AssetsRequest { get; set; }
        public virtual FeedbackRequest FeedbackRequest { get; set; }
        public virtual BusinessTripSettlementRequest BusinessTripSettlementRequest { get; set; }
        public virtual TaxDeductibleCostRequest TaxDeductibleCostRequest { get; set; }
        public virtual BonusRequest BonusRequest { get; set; }
        public virtual ExpenseRequest ExpenseRequest { get; set; }
    }
}
