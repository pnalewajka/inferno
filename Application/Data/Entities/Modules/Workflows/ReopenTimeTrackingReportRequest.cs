﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ReopenTimeTrackingReportRequest", Schema = "Workflows")]
    public class ReopenTimeTrackingReportRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

        public string Reason { get; set; }
    }
}

