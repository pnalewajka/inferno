﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("Substitution", Schema = "Workflows")]
    public class Substitution : SimpleEntity
    {
        public long ReplacedUserId { get; set; }

        [ForeignKey(nameof(ReplacedUserId))]
        public virtual User ReplacedUser { get; set; }

        public long SubstituteUserId { get; set; }

        [ForeignKey(nameof(SubstituteUserId))]
        public virtual User SubstituteUser { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public long? RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }
    }
}

