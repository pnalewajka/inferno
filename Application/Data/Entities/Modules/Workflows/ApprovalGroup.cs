﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ApprovalGroup", Schema = "Workflows")]
    public class ApprovalGroup : ModificationTrackedEntity
    {
        [Index("IX_UniqueOrderByRequest", 0, IsUnique = true)]
        public long RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }

        [Index("IX_UniqueOrderByRequest", 1, IsUnique = true)]
        public long Order { get; set; }

        public ApprovalGroupMode Mode { get; set; }

        public virtual ICollection<Approval> Approvals { get; set; }
    }
}

