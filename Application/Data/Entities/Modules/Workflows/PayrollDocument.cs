﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("PayrollDocument", Schema = "Workflows")]
    public class PayrollDocument : DocumentBase<PayrollDocumentContent>
    {
        public long OnboardingRequestId { get; set; }

        [ForeignKey(nameof(OnboardingRequestId))]
        public virtual OnboardingRequest OnboardingRequest { get; set; }
    }
}
