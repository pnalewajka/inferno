﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("OvertimeRequest", Schema = "Workflows")]
    public class OvertimeRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public virtual Project Project { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal HourLimit { get; set; }

        public string Comment { get; set; }
    }
}

