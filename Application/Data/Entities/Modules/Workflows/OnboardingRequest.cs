﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("OnboardingRequest", Schema = "Workflows")]
    public class OnboardingRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long? LineManagerEmployeeId { get; set; }

        [ForeignKey(nameof(LineManagerEmployeeId))]
        public virtual Employee LineManager { get; set; }

        public long RecruiterEmployeeId { get; set; }

        [ForeignKey(nameof(RecruiterEmployeeId))]
        public virtual Employee Recruiter { get; set; }

        [MaxLength(255)]
        public string FirstName { get; set; }

        [MaxLength(255)]
        public string LastName { get; set; }

        [MaxLength(20)]
        public string Login { get; set; }

        public DateTime? SignedOn { get; set; }

        public DateTime StartDate { get; set; }

        public long LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public long CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company { get; set; }

        public long JobTitleId { get; set; }

        [ForeignKey(nameof(JobTitleId))]
        public virtual JobTitle JobTitle { get; set; }

        public long JobProfileId { get; set; }

        [ForeignKey(nameof(JobProfileId))]
        public virtual JobProfile JobProfile { get; set; }

        public long ContractTypeId { get; set; }

        [ForeignKey(nameof(ContractTypeId))]
        public virtual ContractType ContractType { get; set; }

        public long ProbationPeriodContractTypeId { get; set; }

        [ForeignKey(nameof(ProbationPeriodContractTypeId))]
        public virtual ContractType ProbationPeriodContractType { get; set; }

        [MaxLength(128)]
        public string Compensation { get; set; }

        [MaxLength(128)]
        public string ProbationPeriodCompensation { get; set; }

        [MaxLength(2048)]
        public string CommentsForPayroll { get; set; }

        public long OrgUnitId { get; set; }

        [ForeignKey(nameof(OrgUnitId))]
        public virtual OrgUnit OrgUnit { get; set; }

        [MaxLength(2048)]
        public string CommentsForHiringManager { get; set; }

        public bool IsForeignEmployee { get; set; }

        public bool IsComingFromReferralProgram { get; set; }

        [MaxLength(2048)]
        public string ReferralProgramDetails { get; set; }

        public bool IsRelocationPackageNeeded { get; set; }

        [MaxLength(2048)]
        public string RelocationPackageDetails { get; set; }

        public DateTime? OnboardingDate { get; set; }

        public long? OnboardingLocationId { get; set; }

        [ForeignKey(nameof(OnboardingLocationId))]
        public virtual Location OnboardingLocation { get; set; }

        [MaxLength(2048)]
        public string WelcomeAnnounceEmailInformation { get; set; }

        public virtual ICollection<EmployeePhotoDocument> WelcomeAnnounceEmailPhotos { get; set; }

        public virtual ICollection<PayrollDocument> PayrollDocuments { get; set; }

        public virtual ICollection<ForeignEmployeeDocument> ForeignEmployeeDocuments { get; set; }

        public Guid WelcomeAnnounceEmailPhotosSecret { get; set; }

        public Guid ForeignEmployeeDocumentsSecret { get; set; }

        public Guid PayrollDocumentsSecret { get; set; }

        // EF doesn't handle 0 .. 1 FK relations. Use FirstOrDefault to follow it.
        public virtual ICollection<AssetsRequest> AssetsRequest { get; set; }

        public long? ResultingUserId { get; set; }

        [ForeignKey(nameof(ResultingUserId))]
        public virtual User ResultingUser { get; set; }
    }
}
