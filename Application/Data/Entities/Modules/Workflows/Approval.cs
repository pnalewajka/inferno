﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("Approvals", Schema = "Workflows")]
    public class Approval : ModificationTrackedEntity
    {
        public ApprovalStatus Status { get; set; }

        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public long ApprovalGroupId { get; set; }

        [ForeignKey("ApprovalGroupId")]
        public virtual ApprovalGroup ApprovalGroup { get; set; }

        public long? ForcedByUserId { get; set; }

        [ForeignKey("ForcedByUserId")]
        public virtual User ForcedByUser { get; set; }
    }
}