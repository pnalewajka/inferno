﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("AbsenceRequest", Schema = "Workflows")]
    public class AbsenceRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long AffectedUserId { get; set; }

        [ForeignKey(nameof(AffectedUserId))]
        public virtual User AffectedUser { get; set; }

        public long AbsenceTypeId { get; set; }

        [ForeignKey(nameof(AbsenceTypeId))]
        public virtual AbsenceType AbsenceType { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal Hours { get; set; }

        public long? SubstituteUserId { get; set; }

        [ForeignKey(nameof(SubstituteUserId))]
        public virtual User Substitute { get; set; }

        [MaxLength(255)]
        public string Comment { get; set; }
    }
}

