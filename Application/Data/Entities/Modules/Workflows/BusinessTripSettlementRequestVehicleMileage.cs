﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequestVehicleMileage", Schema = "Workflows")]
    public class BusinessTripSettlementRequestVehicleMileage : IEntity
    {
        [Key]
        public long Id { get; set; }

        public virtual BusinessTripSettlementRequest Request { get; set; }

        public VehicleType VehicleType { get; set; }

        public bool IsCarEngineCapacityOver900cc { get; set; }

        [Required]
        [MaxLength(25)]
        public string RegistrationNumber { get; set; }

        #region Outbound

        public long OutboundFromCityId { get; set; }

        [ForeignKey(nameof(OutboundFromCityId))]
        public virtual City OutboundFromCity { get; set; }

        public long OutboundToCityId { get; set; }

        [ForeignKey(nameof(OutboundToCityId))]
        public virtual City OutboundToCity { get; set; }

        public decimal OutboundKilometers { get; set; }

        #endregion

        #region Inbound

        public long InboundFromCityId { get; set; }

        [ForeignKey(nameof(InboundFromCityId))]
        public virtual City InboundFromCity { get; set; }

        public long InboundToCityId { get; set; }

        [ForeignKey(nameof(InboundToCityId))]
        public virtual City InboundToCity { get; set; }

        public decimal InboundKilometers { get; set; }

        #endregion
    }
}

