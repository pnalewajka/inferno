﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ForeignEmployeeDocument", Schema = "Workflows")]
    public class ForeignEmployeeDocument : DocumentBase<ForeignEmployeeDocumentContent>
    {
        public long OnboardingRequestId { get; set; }

        [ForeignKey(nameof(OnboardingRequestId))]
        public virtual OnboardingRequest OnboardingRequest { get; set; }
    }
}
