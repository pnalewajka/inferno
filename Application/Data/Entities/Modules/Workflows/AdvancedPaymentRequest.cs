using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("AdvancedPaymentRequest", Schema = "Workflows")]
    public class AdvancedPaymentRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long  BusinessTripId { get; set; }

        [ForeignKey(nameof(BusinessTripId))]
        public virtual BusinessTrip BusinessTrip { get; set; }

        public long ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual BusinessTripParticipant Participant { get; set; }

        public decimal Amount { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }
    }
}
