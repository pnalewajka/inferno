﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("EmploymentDataChangeRequest", Schema = "Workflows")]
    public class EmploymentDataChangeRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public DateTime EffectiveOn { get; set; }

        // Salary

        public bool IsSetSalary { get; set; }

        public string Salary { get; set; }

        // Contract type

        public bool IsSetContractType { get; set; }

        public long? ContractTypeId { get; set; }

        [ForeignKey(nameof(ContractTypeId))]
        public virtual ContractType ContractType { get; set; }

        // Contract duration

        public bool IsSetContractDurationType { get; set; }

        public ContractDurationType? ContractDurationType { get; set; }

        // Company

        public bool IsSetCompany { get; set; }

        public long? CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company { get; set; }

        // JobTitle

        public bool IsSetJobTitle { get; set; }

        public long? JobTitleId { get; set; }

        [ForeignKey(nameof(JobTitleId))]
        public virtual JobTitle JobTitle { get; set; }

        // +

        public string CommentsForPayroll { get; set; }

        public string Justification { get; set; }
    }
}

