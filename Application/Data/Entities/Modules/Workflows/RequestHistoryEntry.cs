﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("RequestHistoryEntry", Schema = "Workflows")]
    public class RequestHistoryEntry : SimpleEntity
    {
        public DateTime Date { get; set; }

        public RequestHistoryEntryType Type { get; set; }

        [Column(nameof(ExecutingUserId))]
        [EntityReference(typeof(User))]
        public long? ExecutingUserId { get; set; }

        [ForeignKey(nameof(ExecutingUserId))]
        public virtual User ExecutingUser { get; set; }

        [Column(nameof(ApprovingUserId))]
        [EntityReference(typeof(User))]
        public long? ApprovingUserId { get; set; }

        [ForeignKey(nameof(ApprovingUserId))]
        public virtual User ApprovingUser { get; set; }

        [MaxLength(128)]
        public string ActionIdentifier { get; set; }

        [MaxLength(64)]
        public string JiraIssueKey { get; set; }

        [MaxLength(256)]
        public string EmailSubject { get; set; }

        [MaxLength(1024)]
        public string EmailRecipients { get; set; }

        [MaxLength(256)]
        public string WatcherNames { get; set; }

        [MaxLength(256)]
        public string RejectionReason { get; set; }

        public bool? IsActionActive { get; set; }

        [MaxLength(256)]
        public string UpdatesOn { get; set; }

        [Column(nameof(RequestId))]
        [EntityReference(typeof(Request))]
        public long RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }
    }
}