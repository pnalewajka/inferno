﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequestOwnCost", Schema = "Workflows")]
    public class BusinessTripSettlementRequestOwnCost : SimpleEntity, IBusinessTripSettlementRequestCost
    {
        public long BusinessTripSettlementRequestId { get; set; }

        [ForeignKey(nameof(BusinessTripSettlementRequestId))]
        public virtual BusinessTripSettlementRequest Request { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime TransactionDate { get; set; }

        public decimal Amount { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }

        public bool HasInvoiceIssued { get; set; }
    }
}
