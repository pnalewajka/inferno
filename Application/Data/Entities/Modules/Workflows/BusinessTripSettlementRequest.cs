﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequest", Schema = "Workflows")]
    public class BusinessTripSettlementRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        [Index(IsUnique = true)]
        public long BusinessTripParticipantId { get; set; }

        [ForeignKey(nameof(BusinessTripParticipantId))]
        public virtual BusinessTrips.BusinessTripParticipant BusinessTripParticipant { get; set; }

        public bool IsEmpty { get; set; }

        public DateTime? SettlementDate { get; set; }

        public DateTime? DepartureDateTime { get; set; }

        public DateTime? ArrivalDateTime { get; set; }

        public virtual ICollection<BusinessTripSettlementRequestAbroadTimeEntry> AbroadTimeEntries { get; set; }

        public decimal HoursWorkedForClient { get; set; }

        public decimal? SimplifiedPrivateVehicleMileage { get; set; }

        public MileageUnit? PreferredDistanceUnit { get; set; }

        public virtual ICollection<BusinessTripSettlementRequestMeal> Meals { get; set; }

        public virtual ICollection<BusinessTripSettlementRequestOwnCost> OwnCosts { get; set; }

        public virtual ICollection<BusinessTripSettlementRequestCompanyCost> CompanyCosts { get; set; }

        public virtual ICollection<BusinessTripSettlementRequestAdvancePayment> AdditionalAdvancePayments { get; set; }

        public virtual BusinessTripSettlementRequestVehicleMileage PrivateVehicleMileage { get; set; }

        public string CalculationResult { get; set; }

        public BusinessTripSettlementControllingStatus ControllingStatus { get; set; }
    }
}
