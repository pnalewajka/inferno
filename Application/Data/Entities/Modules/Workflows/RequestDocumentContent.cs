﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("RequestDocumentContent", Schema = "Workflows")]
    public class RequestDocumentContent : IEntity
    {
        [Key]
        [ForeignKey("RequestDocument")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual RequestDocument RequestDocument { get; set; }
    }
}

