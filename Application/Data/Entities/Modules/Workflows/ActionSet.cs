﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ActionSet", Schema = "Workflows")]
    public class ActionSet : SimpleEntity
    {
        [Required]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public RequestType? TriggeringRequestType { get; set; }

        public virtual ICollection<ActionSetTriggeringStatus> TriggeringRequestStatuses { get; set; }

        public string Definition { get; set; }
    }
}
