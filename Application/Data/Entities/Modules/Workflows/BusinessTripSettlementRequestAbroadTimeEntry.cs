﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripSettlementRequestAbroadTimeEntry", Schema = "Workflows")]
    public class BusinessTripSettlementRequestAbroadTimeEntry : SimpleEntity
    {
        public long BusinessTripSettlementRequestId { get; set; }

        [ForeignKey(nameof(BusinessTripSettlementRequestId))]
        public virtual BusinessTripSettlementRequest Request { get; set; }

        public DateTime DepartureDateTime { get; set; }

        public long DepartureCountryId { get; set; }

        [ForeignKey(nameof(DepartureCountryId))]
        public virtual Country DepartureCountry { get; set; }

        public long? DepartureCityId { get; set; }

        [ForeignKey(nameof(DepartureCityId))]
        public virtual City DepartureCity { get; set; }

        public BusinessTripDestinationType? DestinationType { get; set; }
    }
}
