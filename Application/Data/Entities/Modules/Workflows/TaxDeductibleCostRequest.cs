﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("TaxDeductibleCostRequest", Schema = "Workflows")]
    public class TaxDeductibleCostRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long AffectedUserId { get; set; }

        [ForeignKey(nameof(AffectedUserId))]
        public virtual User AffectedUser { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public long ProjectId { get; set; }

        public string WorkName { get; set; }

        public decimal Hours { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public string UrlField { get; set; }
    }
}