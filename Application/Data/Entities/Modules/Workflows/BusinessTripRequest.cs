﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("BusinessTripRequest", Schema = "Workflows")]
    public class BusinessTripRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

        public virtual ICollection<Employee> Participants { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime EndedOn { get; set; }

        public virtual ICollection<City> DepartureCities { get; set; }

        public long DestinationCityId { get; set; }

        [ForeignKey(nameof(DestinationCityId))]
        public virtual City DestinationCity { get; set; }

        [MaxLength(2000)]
        public string TravelExplanation { get; set; }

        public bool AreTravelArrangementsNeeded { get; set; }

        public MeansOfTransport TransportationPreferences { get; set; }

        public AccommodationType AccommodationPreferences { get; set; }

        public int DepartureCityTaxiVouchers { get; set; }

        public int DestinationCityTaxiVouchers { get; set; }

        public virtual ICollection<DeclaredLuggage> LuggageDeclarations { get; set; }

        public string ItineraryDetails { get; set; }

        public virtual BusinessTrip BusinessTrip { get; set; }
    }
}
