﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using System;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ProjectTimeReportApprovalRequest", Schema = "Workflows")]
    public class ProjectTimeReportApprovalRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public long ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public virtual Project Project { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public string Tasks { get; set; }
    }
}

