﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("RequestDocument", Schema = "Workflows")]
    public class RequestDocument : DocumentBase<RequestDocumentContent>
    {
        public long RequestId { get; set; }

        [ForeignKey("RequestId")]
        public virtual Request Request { get; set; }
    }
}

