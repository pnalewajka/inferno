﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("FeedbackRequest", Schema = "Workflows")]
    public class FeedbackRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long EvaluatedEmployeeId { get; set; }

        [ForeignKey(nameof(EvaluatedEmployeeId))]
        public virtual Employee EvaluatedEmployee { get; set; }

        public long EvaluatorEmployeeId { get; set; }

        [ForeignKey(nameof(EvaluatorEmployeeId))]
        public virtual Employee EvaluatorEmployee { get; set; }

        [StringLength(4000)]
        public string FeedbackContent { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        public FeedbackVisbility Visibility { get; set; }

        public FeedbackOriginScenario OriginScenario { get; set; }

        public virtual ICollection<Employee> EvaluatorEmployees { get; set; }
    }
}

