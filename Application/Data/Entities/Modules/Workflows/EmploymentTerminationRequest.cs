﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("EmploymentTerminationRequest", Schema = "Workflows")]
    public class EmploymentTerminationRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public DateTime TerminationDate { get; set; }

        public DateTime EndDate { get; set; }

        public long ReasonId { get; set; }

        [ForeignKey(nameof(ReasonId))]
        public virtual EmploymentTerminationReason Reason { get; set; }

        public string BackupData { get; set; }
    }
}

