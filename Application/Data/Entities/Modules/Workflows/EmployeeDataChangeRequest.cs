﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("EmployeeDataChangeRequest", Schema = "Workflows")]
    public class EmployeeDataChangeRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }

        public DateTime? EffectiveOn { get; set; }

        // LineManager

        public bool IsSetLineManager { get; set; }

        public long? LineManagerId { get; set; }

        [ForeignKey(nameof(LineManagerId))]
        public virtual Employee LineManager { get; set; }

        // OrgUnit

        public bool IsSetOrgUnit { get; set; }

        public long? OrgUnitId { get; set; }

        [ForeignKey(nameof(OrgUnitId))]
        public virtual OrgUnit OrgUnit { get; set; }

        // JobProfile

        public bool IsSetJobProfile { get; set; }

        public long? JobProfileId { get; set; }

        [ForeignKey(nameof(JobProfileId))]
        public virtual JobProfile JobProfile { get; set; }

        // Location

        public bool IsSetLocation { get; set; }

        public long? LocationId { get; set; }

        [ForeignKey(nameof(LocationId))]
        public virtual Location Location { get; set; }

        // PlaceOfWork

        public bool IsSetPlaceOfWork { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }
    }
}
