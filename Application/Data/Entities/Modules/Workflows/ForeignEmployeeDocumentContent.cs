﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ForeignEmployeeDocumentContent", Schema = "Workflows")]
    public class ForeignEmployeeDocumentContent : IEntity
    {
        [Key]
        [ForeignKey(nameof(ForeignEmployeeDocument))]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual ForeignEmployeeDocument ForeignEmployeeDocument { get; set; }
    }
}
