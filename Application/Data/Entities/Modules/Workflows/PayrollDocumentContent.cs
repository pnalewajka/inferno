﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("PayrollDocumentContent", Schema = "Workflows")]
    public class PayrollDocumentContent : IEntity
    {
        [Key]
        [ForeignKey(nameof(PayrollDocument))]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual PayrollDocument PayrollDocument { get; set; }
    }
}
