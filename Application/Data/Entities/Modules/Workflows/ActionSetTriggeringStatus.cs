﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("ActionSetTriggeringStatus", Schema = "Workflows")]
    public class ActionSetTriggeringStatus : SimpleEntity
    {
        public long ActionSetId { get; set; }

        [ForeignKey("ActionSetId")]
        public virtual ActionSet ActionSet { get; set; }

        public RequestStatus RequestStatus { get; set; }
    }
}
