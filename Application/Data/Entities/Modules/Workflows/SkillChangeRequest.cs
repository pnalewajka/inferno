﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.Workflows
{
    [Table("SkillChangeRequest", Schema = "Workflows")]
    public class SkillChangeRequest : IRequestEntity
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Id))]
        public virtual Request Request { get; set; }

        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public virtual ICollection<Skill> Skills { get; set; }
    }
}
