﻿using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Industry", Schema = "Dictionaries")]
    public class Industry : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NotNull]
        [Index(IsUnique = true)]
        public string NameEn { get; set; }
        public string NamePl { get; set; }
        public virtual ICollection<ProjectSetup> ProjectSetups { get; set; }
        public virtual ICollection<ResumeProject> ResumeProjects { get; set; }

        private LocalizedString _name = null;

        [NotMapped]
        public string Name
        {
            get
            {
                return (_name ?? new LocalizedString
                {
                    English = NameEn,
                    Polish = NamePl
                }).ToString();
            }
        }
    }
}
