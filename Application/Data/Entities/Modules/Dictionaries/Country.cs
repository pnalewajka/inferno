﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Country", Schema = "Dictionaries")]
    public class Country : SimpleEntity
    {
        [Required]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [Required]
        [MaxLength(2)]
        public string IsoCode { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual ICollection<DailyAllowance> DailyAllowances { get; set; }
    }
}
