﻿using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Region", Schema = "Dictionaries")]
    public class Region : ModificationTrackedEntity
    {
        [MaxLength(100)]
        [NotNull]
        [Index(IsUnique = true)]
        public string Name { get; set; }
    }
}

