﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Language", Schema = "Dictionaries")]
    public class Language : ModificationTrackedEntity
    {
        [MaxLength(64)]
        [NotNull]
        [Index(IsUnique = true)]
        public string NameEn { get; set; }

        [MaxLength(64)]
        public string NamePl { get; set; }

        public int? Priority { get; set; }

        [NotMapped]
        public LocalizedString Name => new LocalizedString
        {
            English = NameEn,
            Polish = NamePl
        };
    }
}

