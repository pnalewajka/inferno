﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("ServiceLine", Schema = "Dictionaries")]
    public class ServiceLine : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public virtual ICollection<ProjectSetup> ProjectSetups { get; set; }
    }
}

