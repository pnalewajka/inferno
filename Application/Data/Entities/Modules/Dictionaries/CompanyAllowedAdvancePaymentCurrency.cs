﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("CompanyAllowedAdvancePaymentCurrency", Schema = "Dictionaries")]
    public class CompanyAllowedAdvancePaymentCurrency : SimpleEntity
    {
        [Index("IX_CurrencyPerCompany", 0, IsUnique = true)]
        public long CompanyId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company { get; set; }

        [Index("IX_CurrencyPerCompany", 1, IsUnique = true)]
        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }

        public decimal MinimumValue { get; set; }
    }
}

