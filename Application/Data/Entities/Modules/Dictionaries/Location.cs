﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Location", Schema = "Dictionaries")]
    public class Location : ModificationTrackedEntity
    {
        [MaxLength(100)]
        [NotNull]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Address { get; set; }

        public long? CityId { get; set; }

        [ForeignKey(nameof(CityId))]
        public virtual City City { get; set; }

        public long? CountryId { get; set; }

        [ForeignKey(nameof(CountryId))]
        public virtual Country Country { get; set; }

        public virtual ICollection<Employee> UsedInEmployees { get; set; }

        public virtual ICollection<AddEmployeeRequest> UsedInAddEmployeeRequests { get; set; }
    }
}

