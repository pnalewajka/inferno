﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Collections.Generic;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("UtilizationCategory", Schema = "Dictionaries")]
    public class UtilizationCategory : ModificationTrackedEntity
    {
        [MaxLength(64)]
        [Index(IsUnique = true)]
        public string NavisionCode { get; set; }

        [MaxLength(64)]
        public string NavisionName { get; set; }

        [MaxLength(64)]
        public string DisplayName { get; set; }

        public int? Order { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<Project> UsedInProjects { get; set; }
    }
}

