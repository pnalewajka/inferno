﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Currency", Schema = "Dictionaries")]
    public class Currency : ModificationTrackedEntity
    {
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [MaxLength(12)]
        [Required]
        public string IsoCode { get; set; }

        [MaxLength(12)]
        [Required]
        public string Symbol { get; set; }

        [NotMapped]
        public string DisplayName => $"{Symbol} ({IsoCode})";
    }
}

