﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("City", Schema = "Dictionaries")]
    public class City : SimpleEntity
    {
        [Required]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public long CountryId { get; set; }

        [ForeignKey(nameof(CountryId))]
        public virtual Country Country { get; set; }

        public bool IsCompanyApartmentAvailable { get; set; }

        public bool IsVoucherServiceAvailable { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        public virtual ICollection<DailyAllowance> DailyAllowances { get; set; }

        [ForeignKey(nameof(BusinessTrip.DestinationCityId))]
        public virtual ICollection<BusinessTrip> UsedInBusinessTripAsDestination { get; set; }

        public virtual ICollection<BusinessTripParticipant> UsedInBusinessTripParticipantAsDeparture { get; set; }

        [ForeignKey(nameof(BusinessTripRequest.DestinationCityId))]
        public virtual ICollection<BusinessTripRequest> UsedInBusinessTripRequestAsDestination { get; set; }

        public virtual ICollection<BusinessTripRequest> UsedInBusinessTripRequestAsDeparture { get; set; }
    }
}
