﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("Company", Schema = "Dictionaries")]
    public class Company : ModificationTrackedEntity
    {
        [MaxLength(100)]
        [NotNull]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public string Description { get; set; }

        [MaxLength(40)]
        [NotNull]
        public string EmailDomain { get; set; }

        [MaxLength(100)]
        [NotNull]
        [Index(IsUnique = true)]
        public string ActiveDirectoryCode { get; set; }

        [MaxLength(100)]
        public string NavisionCode { get; set; }

        [Index(IsUnique = true)]
        public long? CustomOrder { get; set; }

        public bool IsDataAdministrator { get; set; }

        public virtual ICollection<Employee> UsedInEmployees { get; set; }

        public virtual ICollection<EmploymentPeriod> UsedInEmploymentPeriods { get; set; }

        public virtual ICollection<AddEmployeeRequest> UsedInAddEmployeeRequests { get; set; }

        public virtual ICollection<EmploymentDataChangeRequest> UsedInEmploymentDataChangeRequests { get; set; }

        public virtual ICollection<Employee> TravelExpensesSpecialists { get; set; }

        public virtual ICollection<CompanyAllowedAdvancePaymentCurrency> AllowedAdvancedPaymentCurrencies { get; set; }

        public long DefaultAdvancedPaymentCurrencyId { get; set; }

        [ForeignKey(nameof(DefaultAdvancedPaymentCurrencyId))]
        public virtual Currency DefaultAdvancedPaymentCurrency { get; set; }

        public decimal DefaultAdvancedPaymentDailyAmount { get; set; }

        public long? AdvancePaymentAcceptingEmployeeId { get; set; }

        [ForeignKey(nameof(AdvancePaymentAcceptingEmployeeId))]
        public virtual Employee AdvancePaymentAcceptingEmployee { get; set; }

        public string InvoiceInformation { get; set; }

        public long? FrontDeskAssigneeEmployeeId { get; set; }

        public AccommodationType AllowedAccomodations { get; set; }

        public MeansOfTransport AllowedMeansOfTransport { get; set; }

        [ForeignKey(nameof(FrontDeskAssigneeEmployeeId))]
        public virtual Employee FrontDeskAssignee { get; set; }
    }
}