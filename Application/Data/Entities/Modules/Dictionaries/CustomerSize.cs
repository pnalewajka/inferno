﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.Dictionaries
{
    [Table("CustomerSize", Schema = "Dictionaries")]
    public class CustomerSize : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}

