﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Runtime
{
    [Table("atomic_LastJobRunInfoView", Schema = "Runtime")]
    public class LastJobRunInfo : FormattableEntity, IEntity
    {
        /// <summary>
        /// Job id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Last job run info id
        /// </summary>
        public long? JobRunInfoId { get; set; }

        public JobStatus? Status { get; set; }

        public DateTime? StartedOn { get; set; }

        public DateTime? FinishedOn { get; set; }

        public string Pipeline { get; set; }

        public int? TimesFailedInRow { get; set; }

    }
}

