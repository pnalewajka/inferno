﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Runtime
{
    [Table("TemporaryDocumentPart", Schema = "Runtime")]
    public class TemporaryDocumentPart : FormattableEntity, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long TemporaryDocumentId { get; set; }

        public byte[] Data { get; set; }

        [ForeignKey("TemporaryDocumentId")]
        public virtual TemporaryDocument DocumentHeader { get; set; }
    }
}

