﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Runtime
{
    [Table("TemporaryDocument", Schema = "Runtime")]
    public class TemporaryDocument : ModificationTrackedEntity
    {
        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(1024)]
        [NonUnicode]
        public string ContentType { get; set; }

        [EnumReference]
        public TemporaryDocumentState State { get; set; }

        [Index]
        public Guid Identifier { get; set; }

        public long ContentLength { get; set; }

        public virtual ICollection<TemporaryDocumentPart> DocumentParts { get; set; }
    }
}

