﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Runtime
{
    [Table("CacheInvalidation", Schema = "Runtime")]
    public class CacheInvalidation : FormattableEntity, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(255)]
        [Column(TypeName = "VARCHAR")]
        public string Area { get; set; }

        public Guid Token { get; set; }
    }
}