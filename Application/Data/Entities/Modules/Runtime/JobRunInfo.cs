﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Scheduling;

namespace Smt.Atomic.Data.Entities.Modules.Runtime
{
    [Table("JobRunInfo", Schema = "Runtime")]
    public class JobRunInfo : ModificationTrackedEntity
    {
        public long JobDefinitionId { get; set; }

        [ForeignKey("JobDefinitionId")]
        public virtual JobDefinition JobDefinition { get; set; }

        /// <summary>
        /// Job start time
        /// </summary>
        public DateTime StartedOn { get; set; }

        /// <summary>
        /// Job finish time
        /// </summary>
        public DateTime? FinishedOn { get; set; }

        /// <summary>
        /// Next scheduled run
        /// </summary>
        public DateTime? NextRunOn { get; set; }

        /// <summary>
        /// Job last status
        /// </summary>
        [EnumReference]
        public JobStatus Status { get; set; }

        /// <summary>
        /// Ip or DNS name of computer on which job was started
        /// </summary>
        [MaxLength(250)]
        [NonUnicode]
        public string Host { get; set; }

        /// <summary>
        /// Scheduler name that've picked up job
        /// </summary>
        [MaxLength(250)]
        [NonUnicode]
        public string SchedulerName { get; set; }

        /// <summary>
        /// Number of failed attempts to run specific job
        /// </summary>
        public int? TimesFailedInRow { get; set; }
    }
}
