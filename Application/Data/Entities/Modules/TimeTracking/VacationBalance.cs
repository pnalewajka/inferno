﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("VacationBalance", Schema = "TimeTracking")]
    public class VacationBalance : SimpleEntity
    {
        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public DateTime EffectiveOn { get; set; }

        public long? RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }

        public VacationOperation Operation { get; set; }

        [MaxLength(255)]
        public string Comment { get; set; }

        public decimal HourChange { get; set; }

        /// <summary>
        ///  Readonly
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal TotalHourBalance { get; set; }
    }
}