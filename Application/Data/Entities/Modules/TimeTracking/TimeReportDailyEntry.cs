﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("TimeReportDailyEntry", Schema = "TimeTracking")]
    public class TimeReportDailyEntry : SimpleEntity
    {
        public long TimeReportRowId { get; set; }

        public virtual TimeReportRow TimeReportRow { get; set; }

        public DateTime Day { get; set; }

        public decimal Hours { get; set; }
    }
}