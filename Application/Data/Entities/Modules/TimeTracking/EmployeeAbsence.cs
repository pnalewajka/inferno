﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("EmployeeAbsence", Schema = "TimeTracking")]
    public class EmployeeAbsence : ModificationTrackedEntity
    {
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public long AbsenceTypeId { get; set; }

        [ForeignKey(nameof(AbsenceTypeId))]
        public virtual AbsenceType AbsenceType { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public DateTime ApprovedOn { get; set; }

        public decimal Hours { get; set; }

        public long RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }
    }
}

