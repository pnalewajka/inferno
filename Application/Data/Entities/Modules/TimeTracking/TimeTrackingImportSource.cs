﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("TimeTrackingImportSource", Schema = "TimeTracking")]
    public class TimeTrackingImportSource : SimpleEntity
    {
        [NotNull]
        [Index(IsUnique = true)]
        [MaxLength(255)]
        public string Code { get; set; }

        [NotNull]
        public string NamePl { get; set; }

        [NotNull]
        public string NameEn { get; set; }

        [NotNull]
        [MaxLength(255)]
        public string ImportStrategyIdentifier { get; set; }

        /// <summary>
        /// JSON serialized configuration
        /// </summary>
        public string Configuration { get; set; }
    }
}