﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("TimeReport", Schema = "TimeTracking")]
    public class TimeReport : ModificationTrackedEntity
    {
        [Index("IX_EmployeeYearMonth", 1, IsUnique = false)]
        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        [Index("IX_EmployeeYearMonth", 2, IsUnique = false)]
        public int Year { get; set; }

        [Index("IX_EmployeeYearMonth", 3, IsUnique = false)]
        public byte Month { get; set; }

        public bool IsTemporary { get; set; }

        public decimal ContractedHours { get; set; }

        public decimal ReportedNormalHours { get; set; }

        public decimal NoServiceHours { get; set; }

        public TimeReportStatus Status { get; set; }

        public ControllingStatus ControllingStatus { get; set; }

        [MaxLength(256)]
        public string ControllingErrorMessage { get; set; }

        public InvoiceNotificationStatus InvoiceNotificationStatus { get; set; }

        [MaxLength(256)]
        public string InvoiceNotificationErrorMessage { get; set; }

        public InvoiceStatus InvoiceStatus { get; set; }

        public InvoiceIssues? InvoiceIssues { get; set; }

        [MaxLength(256)]
        public string InvoiceRejectionReason { get; set; }

        public virtual ICollection<TimeReportRow> Rows { get; set; }

        public virtual ICollection<ClockCardDailyEntry> ClockCardDailyEntries { get; set; }

        public virtual ICollection<OvertimeBank> OvertimeBanks { get; set; }

        public long? InvoiceDocumentId { get; set; }

        [ForeignKey(nameof(InvoiceDocumentId))]
        public virtual InvoiceDocument InvoiceDocument { get; set; }

        [NotMapped]
        public bool IsReadOnly => Status == TimeReportStatus.Submitted || Status == TimeReportStatus.Accepted;

        [MaxLength(200)]
        public string TimeZoneDisplayName { get; set; }

        public int TimeZoneOffsetMinutes { get; set; }

        public decimal GetReportedNormalHours()
        {
            return Rows?.Where(r => r.OvertimeVariant == HourlyRateType.Regular)
                .SelectMany(r => r.DailyEntries)
                .Sum(e => e.Hours) ?? 0;
        }

        public bool IsFullyAccepted()
        {
            return Rows.All(r => r.Status == TimeReportStatus.Accepted);
        }
    }
}