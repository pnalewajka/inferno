﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("JiraIssueToTimeReportRowMapper", Schema = "TimeTracking")]
    public class JiraIssueToTimeReportRowMapper : ModificationTrackedEntity
    {
        [NotNull]
        [Index(IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        [NotNull]
        public string SourceCode { get; set; }
    }
}

