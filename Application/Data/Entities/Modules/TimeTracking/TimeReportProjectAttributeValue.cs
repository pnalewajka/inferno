﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("TimeReportProjectAttributeValue", Schema = "TimeTracking")]
    public class TimeReportProjectAttributeValue : ModificationTrackedEntity, ISoftDeletable
    {
        public bool IsDeleted { get; set; }

        public long AttributeId { get; set; }

        [ForeignKey(nameof(AttributeId))]
        public virtual TimeReportProjectAttribute Attribute { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public AttributeValueFeature Features { get; set; }

        public virtual ICollection<TimeReportRow> UsedInTimeReportRows { get; set; }

        public void OnSoftDeleting()
        {
        }
    }
}

