﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("ContractType", Schema = "TimeTracking")]
    public class ContractType : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NotNull]
        [Required(AllowEmptyStrings = false)]
        public string NameEn { get; set; }

        [MaxLength(255)]
        [NotNull]
        [Required(AllowEmptyStrings = false)]
        public string NamePl { get; set; }

        [MaxLength(512)]
        public string DescriptionEn { get; set; }

        [MaxLength(512)]
        public string DescriptionPl { get; set; }

        public EmploymentType? EmploymentType { get; set; }

        public bool IsActive { get; set; }

        public decimal? DailyWorkingLimit { get; set; }

        [MaxLength(255)]
        [NotNull]
        [Index(IsUnique = true)]
        public string ActiveDirectoryCode { get; set; }

        public TimeTrackingCompensationCalculators? TimeTrackingCompensationCalculator { get; set; }

        public BusinessTripCompensationCalculators? BusinessTripCompensationCalculator { get; set; }

        public OverTimeTypes AllowedOverTimeTypes { get; set; }

        public bool HasPaidVacation { get; set; }

        public bool UseAutomaticOvertimeBankCalculation { get; set; }

        public virtual ICollection<AbsenceType> AbsenceTypes { get; set; }

        public virtual ICollection<EmploymentPeriod> UsedInEmploymentPeriods { get; set; }

        public virtual ICollection<EmploymentDataChangeRequest> UsedInEmploymentDataChangeRequests { get; set; }

        public bool ShouldNotifyOnAbsenceRemoval { get; set; }

        public decimal MinimunOvertimeBankBalance { get; set; }

        public SettlementSectionsFeatureType SettlementSectionsFeatures { get; set; }

        public bool NotifyInvoiceAmount { get; set; }

        public bool NotifyMissingBusinessTripSettlement { get; set; }

        public bool UseDailySettlementMeals { get; set; }

        public bool HasToSpecifyDestinationType { get; set; }

        [NotMapped]
        public LocalizedString Name => new LocalizedString
        {
            English = NameEn,
            Polish = NamePl
        };

        public bool IsTaxDeductibleCostEnabled { get; set; }
    }
}

