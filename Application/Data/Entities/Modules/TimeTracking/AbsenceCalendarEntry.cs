﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("AbsenceCalendarEntry", Schema = "TimeTracking")]
    public class AbsenceCalendarEntry : SimpleEntity
    {
        [NotNull]
        [MaxLength(255)]
        [Index(IsUnique = true)]
        public string ExchangeId { get; set; }

        public long? RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }
    }
}

