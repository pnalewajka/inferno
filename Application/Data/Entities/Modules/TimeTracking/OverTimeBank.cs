﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("OvertimeBank", Schema = "TimeTracking")]
    public class OvertimeBank : SimpleEntity
    {
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }
        
        public long? RequestId { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }

        public long? TimeReportRowId { get; set; }

        [ForeignKey(nameof(TimeReportRowId))]
        public virtual TimeReportRow TimeReportRow { get; set; }

        public long? TimeReportId { get; set; }

        [ForeignKey(nameof(TimeReportId))]
        public virtual TimeReport TimeReport { get; set; }

        public decimal OvertimeChange { get; set; }

        public DateTime EffectiveOn { get; set; }

        /// <summary>
        ///  Readonly
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal OvertimeBalance { get; set; }

        [MaxLength(255)]
        public string Comment { get; set; }

        [NotMapped]
        public bool IsAutomatic => RequestId.HasValue || TimeReportId.HasValue || TimeReportRowId.HasValue;
    }
}

