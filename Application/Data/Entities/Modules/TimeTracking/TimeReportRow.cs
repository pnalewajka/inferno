﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using System.Linq;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("TimeReportRow", Schema = "TimeTracking")]
    public class TimeReportRow : SimpleEntity
    {
        public long TimeReportId { get; set; }

        public virtual TimeReport TimeReport { get; set; }

        public long ProjectId { get; set; }

        public virtual Project Project { get; set; }

        public string TaskName { get; set; }

        public long DisplayOrder { get; set; }

        public long? ImportSourceId { get; set; }

        [ForeignKey("ImportSourceId")]
        public virtual TimeTrackingImportSource ImportSource { get; set; }

        public string ImportedTaskCode { get; set; }

        [MaxLength(128)]
        public string ImportedTaskType { get; set; }

        public HourlyRateType OvertimeVariant { get; set; }

        public virtual ICollection<TimeReportDailyEntry> DailyEntries { get; set; }

        public TimeReportStatus Status { get; set; }

        [ForeignKey(nameof(RequestId))]
        public virtual Request Request { get; set; }

        public virtual long? RequestId { get; set; }

        public long? AbsenceTypeId { get; set; }

        public virtual ICollection<TimeReportProjectAttributeValue> AttributeValues { get; set; }

        [ForeignKey(nameof(AbsenceTypeId))]
        public virtual AbsenceType AbsenceType { get; set; }

        public virtual ICollection<OvertimeBank> OvertimeBanks { get; set; }

        [NotMapped]
        public bool IsReadOnly => Status == TimeReportStatus.Submitted || Status == TimeReportStatus.Accepted || TimeReport.IsReadOnly;

        [NotMapped]
        public bool IsBillable => AttributeValues.All(v => !v.Features.HasFlag(AttributeValueFeature.NonBillable));
    }
}