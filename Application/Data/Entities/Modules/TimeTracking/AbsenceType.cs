﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("AbsenceType", Schema = "TimeTracking")]
    public class AbsenceType : ModificationTrackedEntity
    {
        [MaxLength(32)]
        [NonUnicode]
        [NotNull]
        [Index(IsUnique = true)]
        [Required(AllowEmptyStrings = false)]
        public string Code { get; set; }

        [MaxLength(255)]
        [NotNull]
        [Required(AllowEmptyStrings = false)]
        public string NameEn { get; set; }

        [MaxLength(255)]
        [NotNull]
        [Required(AllowEmptyStrings = false)]
        public string NamePl { get; set; }

        public string DescriptionEn { get; set; }

        public string DescriptionPl { get; set; }

        [MaxLength(512)]
        public string EmployeeInstructionEn { get; set; }

        [MaxLength(512)]
        public string EmployeeInstructionPl { get; set; }

        [MaxLength(255)]
        public string KbrCode { get; set; }

        public bool IsVacation { get; set; }

        public bool IsTimeOffForOvertime { get; set; }

        public bool RequiresHRApproval { get; set; }

        public bool AllowHourlyReporting { get; set; }

        public int Order { get; set; }

        public virtual ICollection<ContractType> UsedInContractTypes { get; set; }

        public virtual ICollection<TimeReportRow> UsedInTimeReportRow { get; set; }

        public virtual ICollection<AbsenceRequest> UsedInAbsenceRequest { get; set; }

        public long? UtilizationCategoryId { get; set; }

        public long? AbsenceProjectId { get; set; }

        [ForeignKey("AbsenceProjectId")]
        public virtual Project AbsenceProject { get; set; }
    }
}

