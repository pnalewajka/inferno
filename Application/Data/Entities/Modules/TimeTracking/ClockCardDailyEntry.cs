﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("ClockCardDailyEntry", Schema = "TimeTracking")]
    public class ClockCardDailyEntry : SimpleEntity
    {
        public long TimeReportId { get; set; }

        [ForeignKey(nameof(TimeReportId))]
        public virtual TimeReport TimeReport { get; set; }

        public DateTime Day { get; set; }

        public DateTime? InTime { get; set; }

        public DateTime? OutTime { get; set; }

        public TimeSpan? BreakDuration { get; set; }
    }
}
