﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("CalendarAbsence", Schema = "TimeTracking")]
    public class CalendarAbsence : ModificationTrackedEntity, ICreationTracked
    {
        [Index(IsUnique = true)]
        [MaxLength(255)]
        [NotNull]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(255)]
        [NotNull]
        [EmailAddress]
        public string OfficeGroupEmail { get; set; }

        public virtual ICollection<Employee> EmployeesIds { get; set; }

        [EntityReference(typeof(User))]
        public long? CreatedById { get; set; }

        [EntityReference(typeof(User))]
        public long? ImpersonatedCreatedById { get; set; }

        public DateTime CreatedOn { get; set; }

        [ForeignKey("CreatedById")]
        public virtual User CreatedBy { get; set; }

        [ForeignKey("ImpersonatedCreatedById")]
        public virtual User ImpersonatedCreatedBy { get; set; }
    }
}

