﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("OvertimeApproval", Schema = "TimeTracking")]
    public class OvertimeApproval : SimpleEntity
    {
        public long EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public long ProjectId { get; set; }

        public virtual Project Project { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public decimal HourLimit { get; set; }

        public long RequestId { get; set; }

        public virtual Request Request { get; set; }
    }
}

