﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.TimeTracking
{
    [Table("TimeReportProjectAttribute", Schema = "TimeTracking")]
    public class TimeReportProjectAttribute : ModificationTrackedEntity, ISoftDeletable
    {
        public bool IsDeleted { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long? DefaultValueId { get; set; }

        [ForeignKey(nameof(DefaultValueId))]
        public virtual TimeReportProjectAttributeValue DefaultValue { get; set; }

        public virtual ICollection<TimeReportProjectAttributeValue> Values { get; set; }

        public virtual ICollection<Project> UsedInProjects { get; set; }

        public void OnSoftDeleting()
        {
        }
    }
}

