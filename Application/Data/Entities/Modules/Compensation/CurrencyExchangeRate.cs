﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Compensation
{
    [Table("CurrencyExchangeRate", Schema = "Compensation")]
    public class CurrencyExchangeRate : SimpleEntity
    {
        public DateTime Date { get; set; }

        public long BaseCurrencyId { get; set; }

        [ForeignKey(nameof(BaseCurrencyId))]
        public Currency BaseCurrency { get; set; }

        public long QuotedCurrencyId { get; set; }

        [ForeignKey(nameof(QuotedCurrencyId))]
        public Currency QuotedCurrency { get; set; }

        public decimal Rate { get; set; }

        public CurrencyExchangeRateSourceType SourceType { get; set; }

        [MaxLength(32)]
        public string TableNumber { get; set; }
    }
}
