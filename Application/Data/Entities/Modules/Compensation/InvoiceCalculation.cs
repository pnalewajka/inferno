﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.Compensation
{
    [Table("InvoiceCalculation", Schema = "Compensation")]
    public class InvoiceCalculation : SimpleEntity
    {
        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }

        public string CalculationResult { get; set; }

        public DateTime NotifiedOn { get; set; }

        public bool WasEmployeeNotified { get; set; }

        public bool IsValid { get; set; }
    }
}

