﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Compensation
{
    [Table("InvoiceDocumentContent", Schema = "Compensation")]
    public class InvoiceDocumentContent : IEntity
    {
        [Key]
        [ForeignKey(nameof(InvoiceDocument))]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual InvoiceDocument InvoiceDocument { get; set; }
    }
}
