﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Compensation
{
    [Table("InvoiceDocument", Schema = "Compensation")]
    public class InvoiceDocument : DocumentBase<InvoiceDocumentContent>
    {
    }
}
