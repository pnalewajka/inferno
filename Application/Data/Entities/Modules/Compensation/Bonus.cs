﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.Compensation
{
    [Table("Bonus", Schema = "Compensation")]
    public class Bonus : TrackedEntity
    {
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public long ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public virtual Project Project { get; set; }

        public decimal BonusAmount { get; set; }

        public long CurrencyId { get; set; }

        [ForeignKey(nameof(CurrencyId))]
        public virtual Currency Currency { get; set; }

        [MaxLength(255)]
        public string Justification { get; set; }

        public BonusType Type { get; set; }

        public bool Reinvoice { get; set; }

        public override void OnSoftDeleting()
        {
        }
    }
}

