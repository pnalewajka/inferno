﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Reporting
{
    [Table("ReportTemplate", Schema = "Reporting")]
    public class ReportTemplate : DocumentBase<ReportTemplateContent>
    {
        public long ReportDefinitionId { get; set; }

        [ForeignKey(nameof(ReportDefinitionId))]
        public virtual ReportDefinition ReportDefinition { get; set; }
    }
}

