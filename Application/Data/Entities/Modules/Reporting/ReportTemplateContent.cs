﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Reporting
{
    [Table("ReportTemplateContent", Schema = "Reporting")]
    public class ReportTemplateContent : FormattableEntity, IEntity
    {
        [Key]
        [ForeignKey("ReportTemplate")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual ReportTemplate ReportTemplate { get; set; }
    }
}

