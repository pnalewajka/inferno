﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.Reporting
{
    [Table("ReportDefinition", Schema = "Reporting")]
    public class ReportDefinition : ModificationTrackedEntity
    {
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(255)]
        public string Code { get; set; }

        [Required]
        public string NameEn { get; set; }

        [Required]
        public string NamePl { get; set; }

        public string DescriptionEn { get; set; }

        public string DescriptionPl { get; set; }

        [MaxLength(255)]
        public string OutputFileNameTemplate { get; set; }

        [Required]
        public string ReportingEngineIdentifier { get; set; }

        [Required]
        public string DataSourceTypeIdentifier { get; set; }

        public string Area { get; set; }

        public string Group { get; set; }

        public virtual ICollection<ReportTemplate> ReportTemplates { get; set; }

        public virtual ICollection<SecurityProfile> AllowedForProfiles { get; set; }
    }
}

