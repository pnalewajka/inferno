﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataProcessingAgreementConsent", Schema = "GDPR")]
    public class DataProcessingAgreementConsent : ModificationTrackedEntity
    {
        [Index("IX_Unique_Agreement_User", IsUnique = true, Order = 0)]
        public long AgreementId { get; set; }

        [ForeignKey(nameof(AgreementId))]
        public virtual DataProcessingAgreement Agreement { get; set; }

        [Index("IX_Unique_Agreement_User", IsUnique = true, Order = 1)]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }

        [Index("IX_IsApproved", IsClustered = false)]
        public bool IsApproved { get; set; }
    }
}

