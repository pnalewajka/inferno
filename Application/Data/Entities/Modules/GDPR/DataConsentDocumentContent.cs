﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataConsentDocumentContent", Schema = "GDPR")]
    public class DataConsentDocumentContent : IEntity
    {
        [Key]
        [ForeignKey(nameof(DataConsentDocument))]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual DataConsentDocument DataConsentDocument { get; set; }
    }
}