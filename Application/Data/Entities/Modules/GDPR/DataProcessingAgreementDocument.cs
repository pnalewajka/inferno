﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataProcessingAgreementDocument", Schema = "GDPR")]
    public class DataProcessingAgreementDocument : DocumentBase<DataProcessingAgreementDocumentContent>
    {
        public long DataProcessingAgreementId { get; set; }

        [ForeignKey(nameof(DataProcessingAgreementId))]
        public virtual DataProcessingAgreement Agreement { get; set; }
    }
}

