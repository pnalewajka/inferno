﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataConsentDocument", Schema = "GDPR")]
    public class DataConsentDocument : DocumentBase<DataConsentDocumentContent>
    {
        public long DataConsentId { get; set; }

        [ForeignKey(nameof(DataConsentId))]
        public virtual DataConsent DataConsent { get; set; }
    }
}