﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataActivity", Schema = "GDPR")]
    public class DataActivity : ModificationTrackedEntity
    {
        public long DataOwnerId { get; set; }

        [ForeignKey(nameof(DataOwnerId))]
        public virtual DataOwner DataOwner { get; set; }

        public DateTime PerformedOn { get; set; }

        public long PerformedByUserId { get; set; }

        [ForeignKey(nameof(PerformedByUserId))]
        public virtual User PerformedByUser { get; set; }

        public ActivityType ActivityType { get; set; }

        public long? ReferenceId { get; set; }

        public ReferenceType? ReferenceType { get; set; }

        [MaxLength(500)]
        public string S1 { get; set; }

        [MaxLength(500)]
        public string S2 { get; set; }

        [MaxLength(2000)]
        public string S3 { get; set; }
    }
}

