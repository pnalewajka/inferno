﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataConsent", Schema = "GDPR")]
    public class DataConsent : ModificationTrackedEntity
    {
        public DataConsentType Type { get; set; }

        public DateTime? ExpiresOn { get; set; }

        public long DataAdministratorId { get; set; }

        [ForeignKey(nameof(DataAdministratorId))]
        public virtual Company DataAdministrator { get; set; }

        [StringLength(250)]
        public string Scope { get; set; }

        public virtual ICollection<DataConsentDocument> Attachments { get; set; }

        public long DataOwnerId { get; set; }

        [ForeignKey(nameof(DataOwnerId))]
        public DataOwner DataOwner { get; set; }
    }
}