﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataOwner", Schema = "GDPR")]
    public class DataOwner : ModificationTrackedEntity
    {
        public long? UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        public long? EmployeeId { get; set; }

        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }

        public long? CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        public virtual Candidate Candidate { get; set; }

        public long? RecommendingPersonId { get; set; }

        [ForeignKey(nameof(RecommendingPersonId))]
        public virtual RecommendingPerson RecommendingPerson { get; set; }

        public virtual ICollection<DataConsent> DataConsents { get; set; }

        public virtual ICollection<DataActivity> DataActivity { get; set; }

        public bool IsPotentialDuplicate { get; set; }
    }
}

