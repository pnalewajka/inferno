﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataProcessingAgreement", Schema = "GDPR")]
    public class DataProcessingAgreement : ModificationTrackedEntity
    {
        public virtual ICollection<Project> Projects { get; set; }

        public virtual ICollection<User> RequiredUsers { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidTo { get; set; }

        public virtual ICollection<DataProcessingAgreementDocument> Documents { get; set; }

        public virtual ICollection<DataProcessingAgreementConsent> Consents { get; set; }
    }
}
