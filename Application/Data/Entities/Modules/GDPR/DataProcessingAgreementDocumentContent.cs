﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.GDPR
{
    [Table("DataProcessingAgreementDocumentContent", Schema = "GDPR")]
    public class DataProcessingAgreementDocumentContent : FormattableEntity, IEntity
    {
        [Key]
        [ForeignKey(nameof(Document))]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual DataProcessingAgreementDocument Document { get; set; }
    }
}
