﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("User", Schema = "Accounts")]
    public class User : FormattableEntity, IEntity, IModificationTracked, ITimestamp, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(255)]
        public string FirstName { get; set; }

        [MaxLength(255)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}".Trim();

        [MaxLength(255)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Login { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        [ForeignKey("ModifiedById")]
        public virtual User ModifiedBy { get; set; }

        [ForeignKey("ImpersonatedById")]
        public virtual User ImpersonatedBy { get; set; }

        public bool IsActive { get; set; }

        [EntityReference(typeof(User))]
        public long? ImpersonatedById { get; set; }

        [EntityReference(typeof(User))]
        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public DateTime? LastLoggedOn { get; set; }

        [EntityReference(typeof(UserLoginToken))]
        public long? LoginTokenId { get; set; }

        [ForeignKey("LoginTokenId")]
        public virtual UserLoginToken LoginToken { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public long? PictureId { get; set; }

        public virtual ICollection<User> ModifiedByUsers { get; set; }

        public virtual ICollection<User> ImpersonatedByUsers { get; set; }

        public virtual ICollection<Credential> Credentials { get; set; }
        public virtual ICollection<SecurityRole> Roles { get; set; }
        public virtual ICollection<SecurityProfile> Profiles { get; set; }

        public virtual ICollection<UserDocument> Documents { get; set; }

        public virtual ICollection<AbsenceRequest> AbsenceRequests { get; set; }

        [InverseProperty(nameof(TaxDeductibleCostRequest.AffectedUser))]
        public virtual ICollection<TaxDeductibleCostRequest> TaxDeductibleCostRequests { get; set; }

        public virtual ICollection<ActiveDirectoryGroup> ActiveDirectoryGroups { get; set; }
        public virtual ICollection<ActiveDirectoryGroup> ActiveDirectoryGroupsOwner { get; set; }
        public virtual ICollection<ActiveDirectoryGroup> ActiveDirectoryGroupsManager { get; set; }

        public DbGeography Location { get; set; }

        public int? LocationRadius { get; set; }

        [ForeignKey("PictureId")]
        public virtual UserPicture Picture { get; set; }

        /// <summary>
        /// Use .FirstOrDefault()
        /// </summary>
        public virtual ICollection<Employee> Employees { get; set; }

        public virtual ICollection<Request> WatchingRequests { get; set; }

        public IEnumerable<SecurityRoleType> GetAllRoleTypes()
        {
            return Profiles.SelectMany(p => p.Roles)
                .Concat(Roles)
                .Select(r => EnumHelper.GetEnumValue<SecurityRoleType>(r.Name));
        }

        public IEnumerable<string> GetAllRolesNames()
        {
            return Profiles.SelectMany(p => p.Roles)
                .Concat(Roles)
                .Select(r => r.Name);
        }

        public IEnumerable<SecurityRole> GetAllRoles()
        {
            return Profiles.SelectMany(p => p.Roles)
                .Concat(Roles);
        }

        public bool IsDeleted { get; set; }

        [Index(IsUnique = true)]
        public Guid? ActiveDirectoryId { get; set; }

        [MaxLength(64)]
        public string CrmId { get; set; }

        [ForeignKey(nameof(Substitution.ReplacedUserId))]
        public virtual ICollection<Substitution> Substitutions { get; set; }

        public void OnSoftDeleting()
        {
            var suffix = $"_SoftDeleted-{DateTime.UtcNow:s}";
            Login = $"{Login}{suffix}";
            Email = $"{Email}{suffix}";
        }

        protected override string GetFormattedName(IFormatProvider formatProvider)
        {
            return $"{FirstName} {LastName}".Trim();
        }
    }
}