﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("UserPictureContent", Schema = "Accounts")]
    public class UserPictureContent : FormattableEntity, IEntity
    {
        [Key]
        [ForeignKey("UserPicture")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public byte[] Thumbnail { get; set; }

        public virtual UserPicture UserPicture { get; set; }
    }
}

