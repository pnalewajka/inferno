﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("UserDocument", Schema = "Accounts")]
    public class UserDocument : DocumentBase<UserDocumentContent>
    {
        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}