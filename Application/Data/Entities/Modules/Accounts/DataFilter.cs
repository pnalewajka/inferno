﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("DataFilter", Schema = "Accounts")]
    public class DataFilter : ModificationTrackedEntity
    {
        public long? UserId { get; set; }

        public long? ProfileId { get; set; }

        [MaxLength(100)]
        [NonUnicode]
        public string Entity { get; set; }

        [MaxLength(300)]
        [NonUnicode]
        public string Module { get; set; }

        [MaxLength(2048)]
        [NonUnicode]
        public string Condition { get; set; }

        public virtual User User { get; set; }
    }
}