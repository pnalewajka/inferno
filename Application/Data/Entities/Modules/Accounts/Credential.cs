﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("Credential", Schema = "Accounts")]
    public class Credential : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NonUnicode]
        public string PasswordHash { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string Salt { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? LastUsedOn { get; set; }

        public bool IsExpired { get; set; }

        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string Email { get; set; }

        [EnumReference]
        public AuthorizationProviderType ProviderType { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string ExternalId { get; set; }
    }
}