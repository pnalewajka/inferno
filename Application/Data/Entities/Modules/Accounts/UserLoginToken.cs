﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("UserLoginToken", Schema = "Accounts")]
    public class UserLoginToken : ModificationTrackedEntity
    {
        [MaxLength(64)]
        [NonUnicode]
        public string Content { get; set; }

        public DateTime ExpiresOn { get; set; }
    }
}

