﻿using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("UserPicture", Schema = "Accounts")]
    public class UserPicture : DocumentBase<UserPictureContent>
    {

        public string ETag { get; set; }
    }
}
