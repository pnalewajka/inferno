﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("UserDocumentContent", Schema = "Accounts")]
    public class UserDocumentContent : FormattableEntity, IEntity
    {
        [Key]
        [ForeignKey("UserDocument")]
        public long Id { get; set; }

        public byte[] Content { get; set; }

        public virtual UserDocument UserDocument { get; set; }
    }
}

