﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.KPI;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("SecurityRole", Schema = "Accounts")]
    public class SecurityRole : FormattableEntity, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [MaxLength(255)]
        [NonUnicode]
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<SecurityProfile> Profiles { get; set; }

        public virtual ICollection<KpiDefinition> KpiDefinitions { get; set; }
    }
}