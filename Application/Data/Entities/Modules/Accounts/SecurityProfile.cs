﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Common.Attributes;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Data.Entities.Modules.Accounts
{
    [Table("SecurityProfile", Schema = "Accounts")]
    public class SecurityProfile : ModificationTrackedEntity
    {
        [MaxLength(255)]
        [NonUnicode]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string ActiveDirectoryGroupName { get; set; }

        public virtual ICollection<SecurityRole> Roles { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}