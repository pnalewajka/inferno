﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Data.Entities.Modules.EventSourcing
{
    [Table("PublishedEventQueue", Schema = "Runtime")]
    public class PublishedEventQueueItem : FormattableEntity, IEntity, ITimestamp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public Guid BusinessEventId { get; set; }

        public DateTime PublishedOn { get; set; }

        public DateTime? LeasedOn { get; set; }

        public string BusinessEvent { get; set; }

        [MaxLength(2048)]
        public string Exception { get; set; }
    }
}