﻿SET XACT_ABORT ON
GO

UPDATE [Workflows].[OnboardingRequest]
   SET [ResultingUserId] = (SELECT TOP (1) [Id] FROM [Accounts].[User] WHERE [Login] = [Workflows].[OnboardingRequest].[Login])
