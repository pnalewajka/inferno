namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTravelExplanationMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("BusinessTrips.BusinessTrip", "TravelExplanation", c => c.String(maxLength: 2000));
            AddColumn("Workflows.BusinessTripRequest", "TravelExplanation", c => c.String(maxLength: 2000));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripRequest", "TravelExplanation");
            DropColumn("BusinessTrips.BusinessTrip", "TravelExplanation");
        }
    }
}
