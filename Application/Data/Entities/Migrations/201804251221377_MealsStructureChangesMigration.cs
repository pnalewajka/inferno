namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MealsStructureChangesMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", "IX_SingleMealsEntryPerCountry");
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "Day", c => c.DateTime());
            AlterColumn("Workflows.BusinessTripSettlementRequestMeal", "CountryId", c => c.Long());
            CreateIndex("Workflows.BusinessTripSettlementRequestMeal", "BusinessTripSettlementRequestId");
            CreateIndex("Workflows.BusinessTripSettlementRequestMeal", "CountryId");
        }
        
        public override void Down()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", new[] { "CountryId" });
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", new[] { "BusinessTripSettlementRequestId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestMeal", "CountryId", c => c.Long(nullable: false));
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "Day");
            CreateIndex("Workflows.BusinessTripSettlementRequestMeal", new[] { "BusinessTripSettlementRequestId", "CountryId" }, unique: true, name: "IX_SingleMealsEntryPerCountry");
        }
    }
}
