﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class FixProjectAcceptanceConditionsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802201355468_FixProjectAcceptanceConditionsMigration.UpdateProjectsConditionsSql.Up.sql");
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "ReinvoiceCurrencyId" });
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "MaxCostsCurrencyId" });
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId", c => c.Long());
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId", c => c.Long());
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxHotelCosts", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxTotalFlightsCosts", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxOtherTravelCosts", c => c.Decimal(precision: 18, scale: 2));
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId");
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId");
        }
        
        public override void Down()
        {
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "MaxCostsCurrencyId" });
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "ReinvoiceCurrencyId" });
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxOtherTravelCosts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxTotalFlightsCosts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxHotelCosts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId", c => c.Long(nullable: false));
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId", c => c.Long(nullable: false));
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId");
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId");
        }
    }
}

