namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMentionedEmployeesToRecruitmentModuleMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Recruitment.CandidateNoteMessageMentions",
                c => new
                    {
                        CandidateNoteId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateNoteId, t.EmployeeId })
                .ForeignKey("Recruitment.CandidateNote", t => t.CandidateNoteId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.CandidateNoteId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.RecommendingPersonNoteMessageMentions",
                c => new
                    {
                        RecommendingPersonNoteId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.RecommendingPersonNoteId, t.EmployeeId })
                .ForeignKey("Recruitment.RecommendingPersonNote", t => t.RecommendingPersonNoteId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.RecommendingPersonNoteId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.JobOpeningNoteMessageMentions",
                c => new
                    {
                        JobOpeningNoteId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningNoteId, t.EmployeeId })
                .ForeignKey("Recruitment.JobOpeningNote", t => t.JobOpeningNoteId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.JobOpeningNoteId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.RecruitmentProcessNoteMessageMentions",
                c => new
                    {
                        RecruitmentProcessNoteId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.RecruitmentProcessNoteId, t.EmployeeId })
                .ForeignKey("Recruitment.RecruitmentProcessNote", t => t.RecruitmentProcessNoteId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.RecruitmentProcessNoteId)
                .Index(t => t.EmployeeId);
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcessNoteMessageMentions", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.RecruitmentProcessNoteMessageMentions", "RecruitmentProcessNoteId", "Recruitment.RecruitmentProcessNote");
            DropForeignKey("Recruitment.JobOpeningNoteMessageMentions", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.JobOpeningNoteMessageMentions", "JobOpeningNoteId", "Recruitment.JobOpeningNote");
            DropForeignKey("Recruitment.RecommendingPersonNoteMessageMentions", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.RecommendingPersonNoteMessageMentions", "RecommendingPersonNoteId", "Recruitment.RecommendingPersonNote");
            DropForeignKey("Recruitment.CandidateNoteMessageMentions", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.CandidateNoteMessageMentions", "CandidateNoteId", "Recruitment.CandidateNote");
            DropIndex("Recruitment.RecruitmentProcessNoteMessageMentions", new[] { "EmployeeId" });
            DropIndex("Recruitment.RecruitmentProcessNoteMessageMentions", new[] { "RecruitmentProcessNoteId" });
            DropIndex("Recruitment.JobOpeningNoteMessageMentions", new[] { "EmployeeId" });
            DropIndex("Recruitment.JobOpeningNoteMessageMentions", new[] { "JobOpeningNoteId" });
            DropIndex("Recruitment.RecommendingPersonNoteMessageMentions", new[] { "EmployeeId" });
            DropIndex("Recruitment.RecommendingPersonNoteMessageMentions", new[] { "RecommendingPersonNoteId" });
            DropIndex("Recruitment.CandidateNoteMessageMentions", new[] { "EmployeeId" });
            DropIndex("Recruitment.CandidateNoteMessageMentions", new[] { "CandidateNoteId" });
            DropTable("Recruitment.RecruitmentProcessNoteMessageMentions");
            DropTable("Recruitment.JobOpeningNoteMessageMentions");
            DropTable("Recruitment.RecommendingPersonNoteMessageMentions");
            DropTable("Recruitment.CandidateNoteMessageMentions");
        }
    }
}
