namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ContractUseDailySettlementMealsMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.ContractType", "UseDailySettlementMeals", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("TimeTracking.ContractType", "UseDailySettlementMeals");
        }
    }
}
