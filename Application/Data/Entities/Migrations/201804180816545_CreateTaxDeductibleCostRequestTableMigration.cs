namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateTaxDeductibleCostRequestTableMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Workflows.TaxDeductibleCostRequest",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Month = c.Int(nullable: false),
                    Year = c.Int(nullable: false),
                    ProjectId = c.Long(nullable: false),
                    WorkName = c.String(),
                    Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
                    Description = c.String(maxLength: 1000),
                    UrlField = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("Workflows.TaxDeductibleCostRequest", "Id", "Workflows.Request");
            DropIndex("Workflows.TaxDeductibleCostRequest", new[] { "Id" });
            DropTable("Workflows.TaxDeductibleCostRequest");
        }
    }
}