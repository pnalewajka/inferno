namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentAddWatchersToJobPostingAndCandidateMigration : DbMigration
    {
        public override void Up()
        {           
            CreateTable(
                "Recruitment.CandidateWatchers",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.EmployeeId })
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.JobOpeningWatchers",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.EmployeeId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.EmployeeId);
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.JobOpeningWatchers", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.JobOpeningWatchers", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.CandidateWatchers", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.CandidateWatchers", "CandidateId", "Recruitment.Candidate");
            DropIndex("Recruitment.JobOpeningWatchers", new[] { "EmployeeId" });
            DropIndex("Recruitment.JobOpeningWatchers", new[] { "JobOpeningId" });
            DropIndex("Recruitment.CandidateWatchers", new[] { "EmployeeId" });
            DropIndex("Recruitment.CandidateWatchers", new[] { "CandidateId" });
            DropTable("Recruitment.JobOpeningWatchers");
            DropTable("Recruitment.CandidateWatchers");
        }
    }
}
