﻿SET XACT_ABORT ON
GO

CREATE SCHEMA [Enum]
GO

CREATE PROCEDURE [Runtime].[ClearSqlEnum]
    @schemaName VARCHAR(MAX),
	@tableName VARCHAR(MAX)
AS BEGIN
	IF (EXISTS (SELECT * 
		FROM [INFORMATION_SCHEMA].[TABLES]
		WHERE TABLE_SCHEMA = @schemaName
		AND TABLE_NAME = @tableName))
	BEGIN
		DECLARE @deleteValuesSql NVARCHAR(MAX) = 'DELETE FROM [' + @schemaName + '].[' + @tableName + ']'
		EXECUTE sp_executesql @deleteValuesSql
	END
END
GO

CREATE PROCEDURE [Runtime].[InsertSqlEnumValue]
    @schemaName VARCHAR(MAX),
	@tableName VARCHAR(MAX),
	@value BIGINT,
	@code VARCHAR(MAX),
	@descriptionEn NVARCHAR(MAX),
	@descriptionPl NVARCHAR(MAX)
AS BEGIN
	IF NOT (EXISTS (SELECT * 
		FROM [INFORMATION_SCHEMA].[TABLES]
		WHERE TABLE_SCHEMA = @schemaName
		AND TABLE_NAME = @tableName))
	BEGIN
		DECLARE @createTableSql NVARCHAR(MAX) = 'CREATE TABLE [' + @schemaName + '].[' + @tableName + '] ('
			+ '[Value] BIGINT NOT NULL UNIQUE,'
			+ '[Code] VARCHAR(256) NOT NULL UNIQUE,'
			+ '[DescriptionEn] NVARCHAR(MAX),'
			+ '[DescriptionPl] NVARCHAR(MAX)'
			+ ')'

		EXECUTE sp_executesql @createTableSql
	END

	DECLARE @insertValueSql NVARCHAR(MAX) = 'INSERT INTO [' + @schemaName + '].[' + @tableName + '] '
		+ '([Value], [Code], [DescriptionEn], [DescriptionPl]) '
		+ 'VALUES ('
			+ CAST(@value AS VARCHAR(MAX)) + ','
			+ '''' + @code + ''','
			+ '''' + @descriptionEn + ''','
			+ '''' + @descriptionPl + ''''
		+ ')'

	EXECUTE sp_executesql @insertValueSql
END