namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AddEmployeeRelatedDataMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805221107477_AddEmployeeRelatedDataMigration.Batch.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
