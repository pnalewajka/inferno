namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataConsentToGDPRModuleMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "GDPR.DataConsentDocumentContent",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("GDPR.DataConsentDocument", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
            CreateTable(
                "GDPR.DataConsentDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataConsentId = c.Long(nullable: false),
                        Name = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 250, unicode: false),
                        ContentLength = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("GDPR.DataConsent", t => t.DataConsentId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.DataConsentId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "GDPR.DataConsent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        ExpiresOn = c.DateTime(),
                        DataAdministratorId = c.Long(nullable: false),
                        Scope = c.String(maxLength: 250),
                        DataOwnerId = c.Long(nullable: false),
                        ApplicantId = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Applicant", t => t.ApplicantId)
                .ForeignKey("Dictionaries.Company", t => t.DataAdministratorId)
                .ForeignKey("GDPR.DataOwner", t => t.DataOwnerId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.DataAdministratorId)
                .Index(t => t.DataOwnerId)
                .Index(t => t.ApplicantId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument");
            DropForeignKey("GDPR.DataConsentDocument", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataConsentDocument", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataConsent", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataConsent", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            DropForeignKey("GDPR.DataConsent", "DataAdministratorId", "Dictionaries.Company");
            DropForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent");
            DropForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant");
            DropIndex("GDPR.DataConsent", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataConsent", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataConsent", new[] { "ApplicantId" });
            DropIndex("GDPR.DataConsent", new[] { "DataOwnerId" });
            DropIndex("GDPR.DataConsent", new[] { "DataAdministratorId" });
            DropIndex("GDPR.DataConsentDocument", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataConsentDocument", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataConsentDocument", new[] { "DataConsentId" });
            DropIndex("GDPR.DataConsentDocumentContent", new[] { "Id" });
            DropTable("GDPR.DataConsent");
            DropTable("GDPR.DataConsentDocument");
            DropTable("GDPR.DataConsentDocumentContent");
        }
    }
}
