namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration010320181513 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("Workflows.BusinessTripSettlementRequest", new[] { "BusinessTripParticipantId" });
            //CreateTable(
            //    "Compensation.InvoiceNotification",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            EmployeeId = c.Long(nullable: false),
            //            Year = c.Int(nullable: false),
            //            Month = c.Byte(nullable: false),
            //            CalculationResult = c.String(),
            //            NotifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Allocation.Employee", t => t.EmployeeId)
            //    .Index(t => t.EmployeeId);
            
            //AddColumn("Dictionaries.Company", "InvoiceInformation", c => c.String());
            //AddColumn("TimeTracking.ContractType", "NotifyInvoiceAmount", c => c.Boolean(nullable: false));
            //CreateIndex("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", unique: true);
        }
        
        public override void Down()
        {
            //DropForeignKey("Compensation.InvoiceNotification", "EmployeeId", "Allocation.Employee");
            //DropIndex("Compensation.InvoiceNotification", new[] { "EmployeeId" });
            //DropIndex("Workflows.BusinessTripSettlementRequest", new[] { "BusinessTripParticipantId" });
            //DropColumn("TimeTracking.ContractType", "NotifyInvoiceAmount");
            //DropColumn("Dictionaries.Company", "InvoiceInformation");
            //DropTable("Compensation.InvoiceNotification");
            //CreateIndex("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId");
        }
    }
}
