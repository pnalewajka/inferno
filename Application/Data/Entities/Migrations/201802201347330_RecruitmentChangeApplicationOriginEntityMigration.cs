namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentChangeApplicationOriginEntityMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.ApplicationOrigin", "OriginType", c => c.Int(nullable: false));
            DropColumn("Recruitment.ApplicationOrigin", "IsRecommendingPersonRequired");
            DropColumn("Recruitment.ApplicationOrigin", "IsCommentRequired");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.ApplicationOrigin", "IsCommentRequired", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.ApplicationOrigin", "IsRecommendingPersonRequired", c => c.Boolean(nullable: false));
            DropColumn("Recruitment.ApplicationOrigin", "OriginType");
        }
    }
}
