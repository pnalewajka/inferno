namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterRecruitmentInboundEmailMigration : DbMigration
    {
        public override void Up()
        {          
            AddColumn("Recruitment.InboundEmail", "BodyType", c => c.Int(nullable: false));
            AddColumn("Recruitment.InboundEmail", "ReceivedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.InboundEmail", "ReceivedOn");
            DropColumn("Recruitment.InboundEmail", "BodyType");
        }
    }
}
