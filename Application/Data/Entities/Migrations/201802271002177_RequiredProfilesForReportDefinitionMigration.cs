namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredProfilesForReportDefinitionMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Reporting.ReportDefinitionRequiredProfiles",
                c => new
                    {
                        RequiredProfileId = c.Long(nullable: false),
                        ReportDefinitionId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.RequiredProfileId, t.ReportDefinitionId })
                .ForeignKey("Reporting.ReportDefinition", t => t.RequiredProfileId, cascadeDelete: true)
                .ForeignKey("Accounts.SecurityProfile", t => t.ReportDefinitionId, cascadeDelete: true)
                .Index(t => t.RequiredProfileId)
                .Index(t => t.ReportDefinitionId);            
        }
        
        public override void Down()
        {
            DropForeignKey("Reporting.ReportDefinitionRequiredProfiles", "ReportDefinitionId", "Accounts.SecurityProfile");
            DropForeignKey("Reporting.ReportDefinitionRequiredProfiles", "RequiredProfileId", "Reporting.ReportDefinition");
            DropIndex("Reporting.ReportDefinitionRequiredProfiles", new[] { "ReportDefinitionId" });
            DropIndex("Reporting.ReportDefinitionRequiredProfiles", new[] { "RequiredProfileId" });
            DropTable("Reporting.ReportDefinitionRequiredProfiles");
        }
    }
}
