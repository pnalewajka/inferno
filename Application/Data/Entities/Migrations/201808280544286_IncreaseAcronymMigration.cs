namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreaseAcronymMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Allocation.Project", "IX_ProjectAcronym");
            AlterColumn("Allocation.Project", "Acronym", c => c.String(maxLength: 9));
            CreateIndex("Allocation.Project", "Acronym", unique: true, name: "IX_ProjectAcronym");
        }
        
        public override void Down()
        {
            DropIndex("Allocation.Project", "IX_ProjectAcronym");
            AlterColumn("Allocation.Project", "Acronym", c => c.String(maxLength: 4));
            CreateIndex("Allocation.Project", "Acronym", unique: true, name: "IX_ProjectAcronym");
        }
    }
}
