namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreaseFeedbackAndNoteContentLengthMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Workflows.FeedbackRequest", "FeedbackContent", c => c.String(maxLength: 4000));
            AlterColumn("MeetMe.Feedback", "FeedbackContent", c => c.String(maxLength: 4000));
            AlterColumn("MeetMe.MeetingNote", "NoteBody", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("MeetMe.MeetingNote", "NoteBody", c => c.String(maxLength: 1000));
            AlterColumn("MeetMe.Feedback", "FeedbackContent", c => c.String(maxLength: 1000));
            AlterColumn("Workflows.FeedbackRequest", "FeedbackContent", c => c.String(maxLength: 1000));
        }
    }
}
