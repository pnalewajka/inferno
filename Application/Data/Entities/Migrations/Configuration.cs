using System.Data.Entity.Migrations;
using Smt.Atomic.Data.Entities.Migrations.Seeds;

namespace Smt.Atomic.Data.Entities.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CoreEntitiesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            CommandTimeout = int.MaxValue;
        }

        protected override void Seed(CoreEntitiesContext context)
        {
            SystemParametersSeed.Seed(context);
            SecurityRolesSeed.Seed(context);
            SqlEnumSeed.Seed(context);
            FullTextIndexSeed.Seed(context);
        }
    }
}
