﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class InvoiceStatusMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.TimeReport", "InvoiceStatus", c => c.Int(nullable: false));
            AddColumn("TimeTracking.TimeReport", "InvoiceErrorMessage", c => c.String(maxLength: 256));

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808210829520_InvoiceStatusMigration.Script.Up.sql");
        }

        public override void Down()
        {
            DropColumn("TimeTracking.TimeReport", "InvoiceErrorMessage");
            DropColumn("TimeTracking.TimeReport", "InvoiceStatus");
        }
    }
}
