namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DataOwnerCascadeDeleteMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            DropForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent");

            AddForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id", cascadeDelete: true);
            AddForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent", "Id", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent");
            DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");

            AddForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent", "Id");
            AddForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id");
        }
    }
}
