﻿SET XACT_ABORT ON
GO

ALTER TABLE [Allocation].[Project]
DROP CONSTRAINT [CHK_NoMainProjectWithSubprojectName]
