namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRecruitmentProjectDescriptionMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.ProjectDescription", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdModifiedBy", "Accounts.User");
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdModifiedBy" });
            DropColumn("Recruitment.ProjectDescription", "UserIdCreatedBy");
            DropColumn("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.ProjectDescription", "CreatedOn");
            DropColumn("Recruitment.ProjectDescription", "IsDeleted");
            DropColumn("Recruitment.ProjectDescription", "UserIdImpersonatedBy");
            DropColumn("Recruitment.ProjectDescription", "UserIdModifiedBy");
            DropColumn("Recruitment.ProjectDescription", "ModifiedOn");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.ProjectDescription", "ModifiedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.ProjectDescription", "UserIdModifiedBy", c => c.Long());
            AddColumn("Recruitment.ProjectDescription", "UserIdImpersonatedBy", c => c.Long());
            AddColumn("Recruitment.ProjectDescription", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.ProjectDescription", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.ProjectDescription", "UserIdCreatedBy", c => c.Long());
            CreateIndex("Recruitment.ProjectDescription", "UserIdModifiedBy");
            CreateIndex("Recruitment.ProjectDescription", "UserIdImpersonatedBy");
            CreateIndex("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.ProjectDescription", "UserIdCreatedBy");
            AddForeignKey("Recruitment.ProjectDescription", "UserIdModifiedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ProjectDescription", "UserIdCreatedBy", "Accounts.User", "Id");
        }
    }
}
