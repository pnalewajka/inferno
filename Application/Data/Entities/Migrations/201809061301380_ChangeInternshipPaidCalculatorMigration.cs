namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class ChangeInternshipPaidCalculatorMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201809061301380_ChangeInternshipPaidCalculatorMigration.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
