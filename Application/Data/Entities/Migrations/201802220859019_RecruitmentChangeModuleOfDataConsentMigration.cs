﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class RecruitmentChangeModuleOfDataConsentMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802220859019_RecruitmentChangeModuleOfDataConsentMigration.RemoveDataFromDataConsent.Up.sql");
            //DropForeignKey("Recruitment.DataConsent", "ApplicationOriginId", "Recruitment.ApplicationOrigin"); -Added manually by SQL
            DropForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant");
            DropIndex("Recruitment.DataConsent", new[] { "WhoRecommendedApplicantId" });
            DropIndex("Recruitment.DataConsent", new[] { "ApplicationOriginId" });
            AddColumn("Recruitment.DataConsent", "DataOwnerId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.DataConsent", "DataOwnerId");
            AddForeignKey("Recruitment.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id");
            DropColumn("Recruitment.DataConsent", "WhoRecommendedApplicantId");
            DropColumn("Recruitment.DataConsent", "Comment");
            DropColumn("Recruitment.DataConsent", "ApplicationOriginId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.DataConsent", "ApplicationOriginId", c => c.Long(nullable: false));
            AddColumn("Recruitment.DataConsent", "Comment", c => c.String());
            AddColumn("Recruitment.DataConsent", "WhoRecommendedApplicantId", c => c.Long(nullable: false));
            DropForeignKey("Recruitment.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            DropIndex("Recruitment.DataConsent", new[] { "DataOwnerId" });
            DropColumn("Recruitment.DataConsent", "DataOwnerId");
            CreateIndex("Recruitment.DataConsent", "ApplicationOriginId");
            CreateIndex("Recruitment.DataConsent", "WhoRecommendedApplicantId");
            AddForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant", "Id");
            //AddForeignKey("Recruitment.DataConsent", "ApplicationOriginId", "Recruitment.ApplicationOrigin", "Id"); -Added manually by SQL
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802220859019_RecruitmentChangeModuleOfDataConsentMigration.RemoveDataFromDataConsent.Down.sql");
        }
    }
}

