﻿INSERT INTO Accounts.SecurityProfile
(
    --Id - this column value is auto-generated
    Name,
    UserIdModifiedBy,
    ModifiedOn,
    UserIdImpersonatedBy,
    Timestamp,
    ActiveDirectoryGroupName
)
VALUES
(
    'Technical Interviewer', -- Name - varchar
    NULL, -- UserIdModifiedBy - bigint
    GETDATE(), -- ModifiedOn - datetime
    NULL, -- UserIdImpersonatedBy - bigint
    NULL, -- Timestamp - timestamp
    NULL -- ActiveDirectoryGroupName - nvarchar
)

DECLARE @profileId int
SET @profileId = (SELECT sp.Id FROM Accounts.SecurityProfile sp WHERE sp.Name = 'Technical Interviewer')

ALTER TABLE Accounts.UserSecurityProfiles DISABLE TRIGGER [atomic_UserSecurityProfilesSecurityAreaRoleViolation]

INSERT INTO Accounts.UserSecurityProfiles
(
    UserId,
    ProfileId
)
(SELECT DISTINCT e.UserId, @profileId FROM Allocation.Employee e WHERE e.UserId IS NOT NULL AND e.Id IN (SELECT DISTINCT ti.EmployeeIdEmployee FROM Organization.TechnicalInterviewer ti)) -- UserId - bigint

ALTER TABLE Accounts.UserSecurityProfiles ENABLE TRIGGER [atomic_UserSecurityProfilesSecurityAreaRoleViolation]
