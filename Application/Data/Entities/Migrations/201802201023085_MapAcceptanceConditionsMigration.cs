﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class MapAcceptanceConditionsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "BusinessTrip_Id" });
            RenameColumn(table: "BusinessTrips.BusinessTripAcceptanceConditions", name: "BusinessTrip_Id", newName: "BusinessTripId");
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId", c => c.Long(nullable: false));
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802201023085_MapAcceptanceConditionsMigration.DefaultAcceptanceConditionsSql.Up.sql");
        }

        public override void Down()
        {
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "BusinessTripId" });
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId", c => c.Long());
            RenameColumn(table: "BusinessTrips.BusinessTripAcceptanceConditions", name: "BusinessTripId", newName: "BusinessTrip_Id");
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id");
        }
    }
}

