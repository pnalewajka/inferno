namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AddMeetingForMissingEmployeeFromImportMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805110921426_AddMeetingForMissingEmployeeFromImportMigration.Up.sql");
        }

        public override void Down()
        {
        }
    }
}
