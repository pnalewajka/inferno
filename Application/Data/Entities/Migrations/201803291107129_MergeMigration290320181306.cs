namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration290320181306 : DbMigration
    {
        public override void Up()
        {
            //RenameTable(name: "Compensation.InvoiceNotification", newName: "InvoiceCalculation");
            //AddColumn("Workflows.FeedbackRequest", "OriginScenario", c => c.Int(nullable: false));
            //AddColumn("Compensation.InvoiceCalculation", "WasEmployeeNotified", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            //DropColumn("Compensation.InvoiceCalculation", "WasEmployeeNotified");
            //DropColumn("Workflows.FeedbackRequest", "OriginScenario");
            //RenameTable(name: "Compensation.InvoiceCalculation", newName: "InvoiceNotification");
        }
    }
}
