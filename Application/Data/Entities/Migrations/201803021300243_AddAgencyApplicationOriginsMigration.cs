﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AddAgencyApplicationOriginsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803021300243_AddAgencyApplicationOriginsMigration.AgencyListing.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

