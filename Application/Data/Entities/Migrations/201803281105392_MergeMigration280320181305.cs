namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration280320181305 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Compensation.Bonus",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            StartDate = c.DateTime(nullable: false),
            //            EndDate = c.DateTime(),
            //            EmployeeId = c.Long(nullable: false),
            //            BonusAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            CurrencyId = c.Long(nullable: false),
            //            Justification = c.String(maxLength: 255),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Dictionaries.Currency", t => t.CurrencyId)
            //    .ForeignKey("Allocation.Employee", t => t.EmployeeId)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .Index(t => t.EmployeeId)
            //    .Index(t => t.CurrencyId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
        }
        
        public override void Down()
        {
            //DropForeignKey("Compensation.Bonus", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Compensation.Bonus", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Compensation.Bonus", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Compensation.Bonus", "EmployeeId", "Allocation.Employee");
            //DropForeignKey("Compensation.Bonus", "CurrencyId", "Dictionaries.Currency");
            //DropForeignKey("Compensation.Bonus", "UserIdCreatedBy", "Accounts.User");
            //DropIndex("Compensation.Bonus", new[] { "UserIdModifiedBy" });
            //DropIndex("Compensation.Bonus", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Compensation.Bonus", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Compensation.Bonus", new[] { "UserIdCreatedBy" });
            //DropIndex("Compensation.Bonus", new[] { "CurrencyId" });
            //DropIndex("Compensation.Bonus", new[] { "EmployeeId" });
            //DropTable("Compensation.Bonus");
        }
    }
}
