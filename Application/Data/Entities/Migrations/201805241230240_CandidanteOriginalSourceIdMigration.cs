namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CandidanteOriginalSourceIdMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "OriginalSourceId", c => c.Long());
            Sql("UPDATE Recruitment.Candidate SET OriginalSourceId = (SELECT TOP 1 Id FROM Recruitment.ApplicationOrigin WHERE OriginType = 7)");
            AlterColumn("Recruitment.Candidate", "OriginalSourceId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.Candidate", "OriginalSourceId");
            AddForeignKey("Recruitment.Candidate", "OriginalSourceId", "Recruitment.ApplicationOrigin", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.Candidate", "OriginalSourceId", "Recruitment.ApplicationOrigin");
            DropIndex("Recruitment.Candidate", new[] { "OriginalSourceId" });
            DropColumn("Recruitment.Candidate", "OriginalSourceId");
        }
    }
}
