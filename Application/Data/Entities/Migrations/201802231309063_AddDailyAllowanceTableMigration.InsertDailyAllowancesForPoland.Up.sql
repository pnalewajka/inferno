﻿IF NOT EXISTS (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'EUR')
BEGIN
  INSERT INTO BusinessTrips.Currency
  (
      Name,
      IsoCode,
      Symbol,
      UserIdImpersonatedBy,
      UserIdModifiedBy,
      ModifiedOn,
      Timestamp
  )
  VALUES
  ( 'euro', 'EUR', N'€', NULL, NULL, '2018-02-23 00:00:00',NULL)
  
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'SEK')
BEGIN
  INSERT INTO BusinessTrips.Currency
  (
      Name,
      IsoCode,
      Symbol,
      UserIdImpersonatedBy,
      UserIdModifiedBy,
      ModifiedOn,
      Timestamp
  )
  VALUES
  ( 'korona szwedzka', 'SEK', 'kr', NULL, NULL, '2018-02-23 00:00:00',NULL)
  
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'GBP')
BEGIN
  INSERT INTO BusinessTrips.Currency
  (
      Name,
      IsoCode,
      Symbol,
      UserIdImpersonatedBy,
      UserIdModifiedBy,
      ModifiedOn,
      Timestamp
  )
  VALUES
  ( 'funt', 'GBP', N'£', NULL, NULL, '2018-02-23 00:00:00',NULL)
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'PLN')
BEGIN
  INSERT INTO BusinessTrips.Currency
  (
      Name,
      IsoCode,
      Symbol,
      UserIdImpersonatedBy,
      UserIdModifiedBy,
      ModifiedOn,
      Timestamp
  )
  VALUES
  ( N'złoty', 'PLN', N'zł', NULL, NULL, '2018-03-12 00:00:00',NULL)
  
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'USD')
BEGIN
  INSERT INTO BusinessTrips.Currency
  (
      Name,
      IsoCode,
      Symbol,
      UserIdImpersonatedBy,
      UserIdModifiedBy,
      ModifiedOn,
      Timestamp
  )
  VALUES
  ( N'dollar', 'USD', N'$', NULL, NULL, '2018-03-12 00:00:00',NULL)

END

----Country Insert
IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'AU')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'Austria', 'AU')
END


IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'FR')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'France', 'FR')
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'DE')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'Germany', 'DE')
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'PL')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'Poland', 'PL')
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'SE')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'Sweden', 'SE')
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'GB')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'United Kingdom', 'GB')
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'US')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'United States', 'US')
END

IF NOT EXISTS (SELECT TOP 1 Id FROM [Dictionaries].Country where IsoCode = 'ES')
BEGIN
  INSERT INTO Dictionaries.Country
  (
      Name,
      IsoCode
  )
  VALUES
  ( N'Spain', 'ES')
END

declare @auCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'AU')
declare @frCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'FR')
declare @deCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'DE')
declare @plCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'PL')
declare @seCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'SE')
declare @gbCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'GB')
declare @usCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'US')
declare @esCountry bigint = (SELECT TOP 1 Id FROM [Dictionaries].[Country] where IsoCode = 'ES')
----Country Insert End


declare @plCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'PLN')
declare @usdCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'USD')
declare @eurCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'EUR')
declare @sekCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'SEK')
declare @gbpCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'GBP')


  INSERT INTO BusinessTrips.DailyAllowance
  (
		ValidFrom,
		ValidTo,
		EmploymentCountryId,
		TravelCountryId,
		CurrencyId,
		Allowance,
		UserIdImpersonatedBy,
		UserIdModifiedBy,
		ModifiedOn,
		Timestamp
  )
  VALUES
  ( '2013-03-01', NULL, @plCountry, @auCountry, @eurCurrency, 52, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @frCountry, @eurCurrency, 50, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @deCountry, @eurCurrency, 49, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @plCountry, @plCurrency, 30, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @seCountry, @sekCurrency, 459, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @gbCountry, @gbpCurrency, 35, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @usCountry, @usdCurrency, 59, NULL, NULL, '2018-02-23 00:00:00',NULL ),
  ( '2013-03-01', NULL, @plCountry, @esCountry, @eurCurrency, 50, NULL, NULL, '2018-02-23 00:00:00',NULL )