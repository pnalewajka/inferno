﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class UpdatePromoteTemporaryDocumentToTupleMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201809050837588_UpdatePromoteTemporaryDocumentToTupleMigration.AlterProc.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201809050837588_UpdatePromoteTemporaryDocumentToTupleMigration.AlterProc.Down.sql");
        }
    }
}

