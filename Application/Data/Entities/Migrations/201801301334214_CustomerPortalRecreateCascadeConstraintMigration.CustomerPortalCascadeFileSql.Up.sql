﻿ALTER TABLE CustomerPortal.ResourceFormFileContent
	DROP CONSTRAINT [FK_CustomerPortal.ResourceFormFileContent_CustomerPortal.ResourceFormFile_Id]

ALTER TABLE CustomerPortal.ResourceFormFileContent ADD CONSTRAINT
	[FK_CustomerPortal.ResourceFormFileContent_CustomerPortal.ResourceFormFile_Id] FOREIGN KEY
	(
	Id
	) REFERENCES CustomerPortal.ResourceFormFile
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 

