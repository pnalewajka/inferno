﻿SET XACT_ABORT ON
GO

DELETE FROM [Recruitment].[JobApplicationTechnologies];
GO

DELETE FROM [Recruitment].[JobApplicationLocations];
GO

DELETE FROM [Recruitment].[JobApplication];
GO

DELETE FROM [Recruitment].[ApplicationOrigin];

INSERT INTO [Recruitment].[ApplicationOrigin] ([Name], [OriginType], [ModifiedOn])
VALUES
('Recommendation', 4, GETDATE()),
('Job Fair / College', 5, GETDATE()),
('Application Without Notice', 6, GETDATE()),
('Other', 7, GETDATE()),
('LinkedIn', 2, GETDATE()),
('Goldenline', 2, GETDATE()),
('Facebook', 2, GETDATE()),
('pracuj.pl', 3, GETDATE()),
('praca.pl', 3, GETDATE()),
('LinkedIn', 3, GETDATE()),
('intive website', 3, GETDATE()),
('Gowork', 3, GETDATE()),
('Crossjob', 3, GETDATE()),
('infopraca', 3, GETDATE())
GO