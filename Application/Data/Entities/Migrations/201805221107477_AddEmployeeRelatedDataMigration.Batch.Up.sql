﻿DECLARE @cursor CURSOR
DECLARE @identity BIGINT
DECLARE @firstName NVARCHAR(255)
DECLARE @lastName NVARCHAR(255)
DECLARE @email NVARCHAR(255)
DECLARE @employeeId BIGINT
DECLARE @dataOwnerId BIGINT
DECLARE @userId BIGINT
DECLARE @candidateId BIGINT
DECLARE @recommendingPersonId BIGINT
DECLARE @dataConsentId BIGINT
DECLARE @companyId BIGINT
DECLARE @defaultCompanyId BIGINT
DECLARE @defaultCoordinatorId BIGINT
DECLARE @defaultCompanyActiveDirectoryCode NVARCHAR(255)
DECLARE @recruitmentDefaultCoordinatorEmail NVARCHAR(255)
DECLARE @currentDate DateTime = GETDATE()
DECLARE @RecommendingEmployeeConsent INT = 7
DECLARE @Active INT = 2

SELECT @defaultCoordinatorId = Id FROM Allocation.Employee WHERE Email = 'anna.jarco@intive.com'

SELECT @defaultCompanyActiveDirectoryCode = Value FROM Configuration.SystemParameter WHERE [Key] = 'Recruitment.DefaultCompany.ActiveDirectoryCode'
SELECT @defaultCompanyId = Id FROM Dictionaries.Company WHERE ActiveDirectoryCode = @defaultCompanyActiveDirectoryCode

SET @cursor = CURSOR FOR
SELECT Id, FirstName, LastName, Email, UserId, CompanyId FROM Allocation.Employee
WHERE CurrentEmployeeStatus = @Active

OPEN @cursor
FETCH NEXT FROM @cursor
INTO @identity, @firstName, @lastName, @email, @userId, @companyId

WHILE @@FETCH_STATUS = 0
BEGIN
    SET @employeeId = NULL
    SET @dataOwnerId = NULL
    SET @recommendingPersonId  = NULL
    SET @dataConsentId  = NULL
    SET @candidateId = NULL
      
    SELECT @dataOwnerId = Id FROM GDPR.DataOwner WHERE EmployeeId = @identity

    IF @dataOwnerId IS NULL
    BEGIN        
        IF @email IS NOT NULL
        BEGIN
            IF @userId IS NULL
            BEGIN
                SELECT @userId = Id FROM Accounts.[User] WHERE Email = @email
            END

            SELECT @candidateId = Id FROM Recruitment.Candidate WHERE EmailAddress = @email OR OtherEmailAddress = @email
            SELECT @recommendingPersonId = Id FROM Recruitment.RecommendingPerson WHERE EmailAddress = @email
        END

        SELECT @dataOwnerId = Id, @employeeId = EmployeeId FROM GDPR.DataOwner
            WHERE (UserId IS NOT NULL AND UserId = @userId)
            OR (CandidateId IS NOT NULL AND CandidateId = @candidateId)
            OR (RecommendingPersonId IS NOT NULL AND RecommendingPersonId = @recommendingPersonId)

        IF @dataOwnerId IS NULL
        BEGIN
            INSERT INTO GDPR.DataOwner 
                (EmployeeId,
                UserId,
                CandidateId,
                RecommendingPersonId,
                ModifiedOn) 
            VALUES
                (@identity,
                @userId,
                @candidateId,
                @recommendingPersonId,
                @currentDate)

            SET @dataOwnerId = SCOPE_IDENTITY()
        END
        ELSE
        BEGIN
            IF @employeeId IS NULL
            BEGIN
                UPDATE GDPR.DataOwner SET EmployeeId = @identity WHERE Id = @dataOwnerId
            END
            ELSE
            BEGIN
                INSERT INTO GDPR.DataOwner
                    (EmployeeId,
                    IsPotentialDuplicate,
                    ModifiedOn)
                VALUES
                    (@identity,
                    1,
                    @currentDate)

                SET @dataOwnerId = SCOPE_IDENTITY()
            END
        END
    END

    IF @email IS NOT NULL
    BEGIN
        SELECT @recommendingPersonId = RecommendingPersonId FROM GDPR.DataOwner WHERE Id = @dataOwnerId

        IF @recommendingPersonId IS NULL
        BEGIN
            SELECT @recommendingPersonId = Id FROM Recruitment.RecommendingPerson WHERE EmailAddress IS NOT NULL AND EmailAddress = @email

            IF @recommendingPersonId IS NULL
            BEGIN
                INSERT INTO Recruitment.RecommendingPerson ( FirstName, LastName, EmailAddress, ModifiedOn, CreatedOn, CoordinatorId )
                VALUES ( @firstName, @lastName, @email, @currentDate, @currentDate, @defaultCoordinatorId)

                SET @recommendingPersonId = SCOPE_IDENTITY()
            END

            UPDATE GDPR.DataOwner SET RecommendingPersonId = @recommendingPersonId WHERE Id = @dataOwnerId
        END

        SELECT @dataConsentId = Id FROM GDPR.DataConsent WHERE DataOwnerId = @dataOwnerId

        IF @dataConsentId IS NULL
        BEGIN
            IF @companyId IS NULL
            BEGIN
                SET @companyId = @defaultCompanyId
            END

            INSERT INTO GDPR.DataConsent ( [Type], DataAdministratorId, DataOwnerId, ModifiedOn )
            VALUES ( @RecommendingEmployeeConsent, @companyId, @dataOwnerId, @currentDate)
        END
    END

    FETCH NEXT FROM @cursor
    INTO @identity, @firstName, @lastName, @email, @userId, @companyId
END

CLOSE @cursor
DEALLOCATE @cursor
