namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CurrencyExchangeRateMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Compensation.CurrencyExchangeRate",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        BaseCurrencyId = c.Long(nullable: false),
                        QuotedCurrencyId = c.Long(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 6),
                        SourceType = c.Int(nullable: false),
                        TableNumber = c.String(maxLength: 32),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.Currency", t => t.BaseCurrencyId)
                .ForeignKey("Dictionaries.Currency", t => t.QuotedCurrencyId)
                .Index(t => t.BaseCurrencyId)
                .Index(t => t.QuotedCurrencyId);
        }
        
        public override void Down()
        {
            DropForeignKey("Compensation.CurrencyExchangeRate", "QuotedCurrencyId", "Dictionaries.Currency");
            DropForeignKey("Compensation.CurrencyExchangeRate", "BaseCurrencyId", "Dictionaries.Currency");
            DropIndex("Compensation.CurrencyExchangeRate", new[] { "QuotedCurrencyId" });
            DropIndex("Compensation.CurrencyExchangeRate", new[] { "BaseCurrencyId" });
            DropTable("Compensation.CurrencyExchangeRate");
        }
    }
}
