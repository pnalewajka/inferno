namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CompensationParametersMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.ContractType", "TimeTrackingCompensationCalculator", c => c.Int());
            AddColumn("TimeTracking.ContractType", "BusinessTripCompensationCalculator", c => c.Int());
            AddColumn("Allocation.EmploymentPeriod", "ContractorCompanyName", c => c.String(maxLength: 255));
            AddColumn("Allocation.EmploymentPeriod", "HourlyRate", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Allocation.EmploymentPeriod", "HourlyRateDomesticOutOfOfficeBonus", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Allocation.EmploymentPeriod", "HourlyRateForeignOutOfOfficeBonus", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Allocation.EmploymentPeriod", "MonthlyRate", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("Allocation.EmploymentPeriod", "MonthlyRate");
            DropColumn("Allocation.EmploymentPeriod", "HourlyRateForeignOutOfOfficeBonus");
            DropColumn("Allocation.EmploymentPeriod", "HourlyRateDomesticOutOfOfficeBonus");
            DropColumn("Allocation.EmploymentPeriod", "HourlyRate");
            DropColumn("Allocation.EmploymentPeriod", "ContractorCompanyName");
            DropColumn("TimeTracking.ContractType", "BusinessTripCompensationCalculator");
            DropColumn("TimeTracking.ContractType", "TimeTrackingCompensationCalculator");
        }
    }
}
