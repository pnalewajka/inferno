namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDecisionMakerToRecruitmentProcessStepMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId", c => c.Long(nullable: true));
            Sql("UPDATE Recruitment.JobOpening SET DecisionMakerEmployeeId = (SELECT TOP 1 ID FROM Allocation.Employee)");
            AlterColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.JobOpening", "DecisionMakerEmployeeId");
            AddForeignKey("Recruitment.JobOpening", "DecisionMakerEmployeeId", "Allocation.Employee", "Id");
        }

        public override void Down()
        {
            DropForeignKey("Recruitment.JobOpening", "DecisionMakerEmployeeId", "Allocation.Employee");
            DropIndex("Recruitment.JobOpening", new[] { "DecisionMakerEmployeeId" });
            DropColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId");
        }
    }
}
