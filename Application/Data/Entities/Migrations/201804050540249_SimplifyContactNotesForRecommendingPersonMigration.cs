namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SimplifyContactNotesForRecommendingPersonMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "RecommendingPersonContactRecordId", "Recruitment.RecommendingPersonContactRecord");
            DropIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", new[] { "JobOpeningId" });
            DropIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", new[] { "RecommendingPersonContactRecordId" });
            DropColumn("Recruitment.RecommendingPersonContactRecord", "RecommendingPersonReaction");
            DropTable("Recruitment.RecommendingPersonContactRecordJobOpenings");
        }
        
        public override void Down()
        {
            CreateTable(
                "Recruitment.RecommendingPersonContactRecordJobOpenings",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        RecommendingPersonContactRecordId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.RecommendingPersonContactRecordId });
            
            AddColumn("Recruitment.RecommendingPersonContactRecord", "RecommendingPersonReaction", c => c.Int(nullable: false));
            CreateIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", "RecommendingPersonContactRecordId");
            CreateIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", "JobOpeningId");
            AddForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "RecommendingPersonContactRecordId", "Recruitment.RecommendingPersonContactRecord", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "JobOpeningId", "Recruitment.JobOpening", "Id", cascadeDelete: true);
        }
    }
}
