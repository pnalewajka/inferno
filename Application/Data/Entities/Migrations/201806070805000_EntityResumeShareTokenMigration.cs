namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityResumeShareTokenMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Resume.ResumeShareToken",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SharerEmployeeId = c.Long(nullable: false),
                        ResumeOwnerEmployeeId = c.Long(nullable: false),
                        Reason = c.String(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Allocation.Employee", t => t.ResumeOwnerEmployeeId)
                .ForeignKey("Allocation.Employee", t => t.SharerEmployeeId)
                .Index(t => new { t.SharerEmployeeId, t.ResumeOwnerEmployeeId }, unique: true, name: "IX_SharerOwner");
        }
        
        public override void Down()
        {
            DropForeignKey("Resume.ResumeShareToken", "SharerEmployeeId", "Allocation.Employee");
            DropForeignKey("Resume.ResumeShareToken", "ResumeOwnerEmployeeId", "Allocation.Employee");
            DropIndex("Resume.ResumeShareToken", "IX_SharerOwner");
            DropTable("Resume.ResumeShareToken");
        }
    }
}
