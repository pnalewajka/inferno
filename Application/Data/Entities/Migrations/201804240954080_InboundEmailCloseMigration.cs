namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InboundEmailCloseMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.InboundEmail", "ClosedComment", c => c.String());
            AddColumn("Recruitment.InboundEmail", "ClosedReason", c => c.Int());
            AddColumn("Recruitment.InboundEmail", "ClosedOn", c => c.DateTime());
            AddColumn("Recruitment.InboundEmail", "ClosedById", c => c.Long()); 
            CreateIndex("Recruitment.InboundEmail", "ClosedById");
            AddForeignKey("Recruitment.InboundEmail", "ClosedById", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.InboundEmail", "ClosedById", "Allocation.Employee");
            DropIndex("Recruitment.InboundEmail", new[] { "ClosedById" });
            DropColumn("Recruitment.InboundEmail", "ClosedById");
            DropColumn("Recruitment.InboundEmail", "ClosedOn");
            DropColumn("Recruitment.InboundEmail", "ClosedReason");
            DropColumn("Recruitment.InboundEmail", "ClosedComment");
        }
    }
}
