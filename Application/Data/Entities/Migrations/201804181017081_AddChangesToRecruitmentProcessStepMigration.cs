namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddChangesToRecruitmentProcessStepMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcessStep", "DecisionComment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.RecruitmentProcessStep", "RecruitmentHint", c => c.String(maxLength: 255));
            DropColumn("Recruitment.RecruitmentProcessStep", "Comment");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.RecruitmentProcessStep", "Comment", c => c.String());
            DropColumn("Recruitment.RecruitmentProcessStep", "RecruitmentHint");
            DropColumn("Recruitment.RecruitmentProcessStep", "DecisionComment");
        }
    }
}
