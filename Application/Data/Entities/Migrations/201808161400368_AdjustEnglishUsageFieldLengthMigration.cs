namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AdjustEnglishUsageFieldLengthMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.TechnicalReview", "EnglishUsageAssessment", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.TechnicalReview", "EnglishUsageAssessment", c => c.String(maxLength: 400));
        }
    }
}
