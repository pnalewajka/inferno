namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration100420181245 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("Allocation.ProjectInvoicingRate", "IX_ProjectCodePerProject");
            //AddColumn("Compensation.Bonus", "ProjectId", c => c.Long(nullable: false));
            //AddColumn("Recruitment.RecruitmentProcess", "FinalPositionId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "FinalStartDate", c => c.DateTime());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalComment", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferPositionId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "OfferContractType", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferSalary", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferStartDate", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferLocationId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "OfferComment", c => c.String(maxLength: 255));
            //CreateIndex("Compensation.Bonus", "ProjectId");
            //CreateIndex("Allocation.ProjectInvoicingRate", new[] { "ProjectId", "Code" }, unique: true, name: "IX_ProjectCodePerProject");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalPositionId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            //CreateIndex("Recruitment.RecruitmentProcess", "OfferPositionId");
            //CreateIndex("Recruitment.RecruitmentProcess", "OfferLocationId");
            //AddForeignKey("Compensation.Bonus", "ProjectId", "Allocation.Project", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle");
            //DropForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            //DropForeignKey("Compensation.Bonus", "ProjectId", "Allocation.Project");
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferLocationId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferPositionId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLongTermContractTypeId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalTrialContractTypeId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalPositionId" });
            //DropIndex("Allocation.ProjectInvoicingRate", "IX_ProjectCodePerProject");
            //DropIndex("Compensation.Bonus", new[] { "ProjectId" });
            //DropColumn("Recruitment.RecruitmentProcess", "OfferComment");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferLocationId");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferStartDate");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferContractType");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferPositionId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalComment");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalStartDate");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalPositionId");
            //DropColumn("Compensation.Bonus", "ProjectId");
            //CreateIndex("Allocation.ProjectInvoicingRate", new[] { "ProjectId", "Code" }, unique: true, clustered: true, name: "IX_ProjectCodePerProject");
        }
    }
}
