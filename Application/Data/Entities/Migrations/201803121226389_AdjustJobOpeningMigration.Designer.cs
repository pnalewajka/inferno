// <auto-generated />
namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AdjustJobOpeningMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AdjustJobOpeningMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201803121226389_AdjustJobOpeningMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
