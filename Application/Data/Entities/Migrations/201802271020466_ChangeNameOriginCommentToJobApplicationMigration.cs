namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNameOriginCommentToJobApplicationMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.JobApplication", "OriginComment", c => c.String(maxLength: 250));
            DropColumn("Recruitment.JobApplication", "SourceComment");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.JobApplication", "SourceComment", c => c.String(maxLength: 250));
            DropColumn("Recruitment.JobApplication", "OriginComment");
        }
    }
}
