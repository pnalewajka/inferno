namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddReinvoiceToBonusTableMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Compensation.Bonus", "Reinvoice", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("Compensation.Bonus", "Reinvoice");
        }
    }
}
