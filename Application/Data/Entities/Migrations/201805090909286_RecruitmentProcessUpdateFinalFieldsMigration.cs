namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecruitmentProcessUpdateFinalFieldsMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "JobApplicationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalJobTitleId", c => c.Long());
            AddColumn("Recruitment.JobOpening", "CompanyId", c => c.Long());
            Sql("UPDATE Recruitment.JobOpening SET CompanyId = (SELECT TOP 1 Id FROM Dictionaries.Company)");
            AlterColumn("Recruitment.JobOpening", "CompanyId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.RecruitmentProcess", "JobApplicationId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalJobTitleId");
            CreateIndex("Recruitment.JobOpening", "CompanyId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalJobTitleId", "SkillManagement.JobTitle", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "JobApplicationId", "Recruitment.JobApplication", "Id");
            AddForeignKey("Recruitment.JobOpening", "CompanyId", "Dictionaries.Company", "Id");
        }

        public override void Down()
        {
            DropForeignKey("Recruitment.JobOpening", "CompanyId", "Dictionaries.Company");
            DropForeignKey("Recruitment.RecruitmentProcess", "JobApplicationId", "Recruitment.JobApplication");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalJobTitleId", "SkillManagement.JobTitle");
            DropIndex("Recruitment.JobOpening", new[] { "CompanyId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalJobTitleId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "JobApplicationId" });
            DropColumn("Recruitment.JobOpening", "CompanyId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalJobTitleId");
            DropColumn("Recruitment.RecruitmentProcess", "JobApplicationId");
        }
    }
}
