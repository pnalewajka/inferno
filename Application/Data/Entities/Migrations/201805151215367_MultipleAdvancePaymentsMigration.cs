namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class MultipleAdvancePaymentsMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id", "Workflows.BusinessTripSettlementRequest");
            DropIndex("Workflows.BusinessTripSettlementRequestAdvancePayment", new[] { "Id" });
            DropPrimaryKey("Workflows.BusinessTripSettlementRequestAdvancePayment");
            RenameColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id", "RequestId");
            AddColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id", c => c.Long(nullable: false, identity: true));
            AddColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Timestamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddPrimaryKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id");
            CreateIndex("Workflows.BusinessTripSettlementRequestAdvancePayment", "RequestId");
            AddForeignKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "RequestId", "Workflows.BusinessTripSettlementRequest", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "RequestId", "Workflows.BusinessTripSettlementRequest");
            DropIndex("Workflows.BusinessTripSettlementRequestAdvancePayment", new[] { "RequestId" });
            DropPrimaryKey("Workflows.BusinessTripSettlementRequestAdvancePayment");
            DropColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Timestamp");
            DropColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id");
            RenameColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "RequestId", "Id");
            AddPrimaryKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id");
            CreateIndex("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id");
            AddForeignKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id", "Workflows.BusinessTripSettlementRequest", "Id", cascadeDelete: true);
        }
    }
}
