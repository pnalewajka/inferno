namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration100520181623 : DbMigration
    {
        public override void Up()
        {
            /*DropForeignKey("Recruitment.RecruitmentProcess", "FinalCityId", "Dictionaries.City");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalCityId" });
            AddColumn("Recruitment.RecruitmentProcess", "JobApplicationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "OnboardingRequestId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "ResumeShownToClientOn", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalSignedDate", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork", c => c.Int());
            AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkComment", c => c.String(maxLength: 50));
            AddColumn("Recruitment.JobOpening", "CompanyId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.RecruitmentProcess", "JobApplicationId");
            CreateIndex("Recruitment.RecruitmentProcess", "OnboardingRequestId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
            CreateIndex("Recruitment.JobOpening", "CompanyId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "JobApplicationId", "Recruitment.JobApplication", "Id");
            AddForeignKey("Recruitment.JobOpening", "CompanyId", "Dictionaries.Company", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "OnboardingRequestId", "Workflows.OnboardingRequest", "Id");
            DropColumn("Recruitment.RecruitmentProcess", "CvShownToClient");
            DropColumn("Recruitment.RecruitmentProcess", "CvShownToClientOn");
            DropColumn("Recruitment.RecruitmentProcess", "HiredOn");
            DropColumn("Recruitment.RecruitmentProcess", "FinalCityId");
            DropColumn("Recruitment.TechnicalReview", "SeniorityLevelExpectation");*/
        }
        
        public override void Down()
        {
            /*AddColumn("Recruitment.TechnicalReview", "SeniorityLevelExpectation", c => c.Int(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "FinalCityId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "HiredOn", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "CvShownToClientOn", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "CvShownToClient", c => c.Boolean(nullable: false));
            DropForeignKey("Recruitment.RecruitmentProcess", "OnboardingRequestId", "Workflows.OnboardingRequest");
            DropForeignKey("Recruitment.JobOpening", "CompanyId", "Dictionaries.Company");
            DropForeignKey("Recruitment.RecruitmentProcess", "JobApplicationId", "Recruitment.JobApplication");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            DropIndex("Recruitment.JobOpening", new[] { "CompanyId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalOnboardingLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OnboardingRequestId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "JobApplicationId" });
            DropColumn("Recruitment.JobOpening", "CompanyId");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork");
            DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate");
            DropColumn("Recruitment.RecruitmentProcess", "FinalSignedDate");
            DropColumn("Recruitment.RecruitmentProcess", "ResumeShownToClientOn");
            DropColumn("Recruitment.RecruitmentProcess", "OnboardingRequestId");
            DropColumn("Recruitment.RecruitmentProcess", "JobApplicationId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalCityId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalCityId", "Dictionaries.City", "Id");*/
        }
    }
}
