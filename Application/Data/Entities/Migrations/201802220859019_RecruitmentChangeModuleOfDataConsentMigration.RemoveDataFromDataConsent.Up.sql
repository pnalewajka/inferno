﻿SET XACT_ABORT ON
GO

DELETE FROM [Recruitment].[DataConsentDocumentContent]
DELETE FROM [Recruitment].[DataConsentDocument]
DELETE FROM [Recruitment].[DataConsent]

ALTER TABLE [Recruitment].[DataConsent]   
DROP CONSTRAINT [FK_Recruitment.DataConsent_Recruitment.DataOrigin_DataOriginId];