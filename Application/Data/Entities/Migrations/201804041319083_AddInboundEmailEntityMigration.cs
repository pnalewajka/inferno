namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddInboundEmailEntityMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "Recruitment.InboundEmailValue",
                    c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        InboundEmailId = c.Long(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(maxLength: 255),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true,
                            storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.InboundEmail", t => t.InboundEmailId)
                .Index(t => t.InboundEmailId);
        }

        public override void Down()
        {
            DropForeignKey("Recruitment.InboundEmailValue", "InboundEmailId", "Recruitment.InboundEmail");
            DropIndex("Recruitment.InboundEmailValue", new[] {"InboundEmailId"});
            DropTable("Recruitment.InboundEmailValue");
        }
    }
}
