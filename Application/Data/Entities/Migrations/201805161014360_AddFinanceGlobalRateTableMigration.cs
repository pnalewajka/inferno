namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFinanceGlobalRateTableMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Finance.GlobalRate",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Long(nullable: false),
                        Description = c.String(),
                        JobMatrixLevelId = c.Long(),
                        LocationId = c.Long(),
                        CompanyId = c.Long(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.Company", t => t.CompanyId)
                .ForeignKey("Dictionaries.Currency", t => t.CurrencyId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("SkillManagement.JobMatrixLevel", t => t.JobMatrixLevelId)
                .ForeignKey("Dictionaries.Location", t => t.LocationId)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.CurrencyId)
                .Index(t => t.JobMatrixLevelId)
                .Index(t => t.LocationId)
                .Index(t => t.CompanyId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Finance.GlobalRate", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Finance.GlobalRate", "LocationId", "Dictionaries.Location");
            DropForeignKey("Finance.GlobalRate", "JobMatrixLevelId", "SkillManagement.JobMatrixLevel");
            DropForeignKey("Finance.GlobalRate", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Finance.GlobalRate", "CurrencyId", "Dictionaries.Currency");
            DropForeignKey("Finance.GlobalRate", "CompanyId", "Dictionaries.Company");
            DropIndex("Finance.GlobalRate", new[] { "UserIdModifiedBy" });
            DropIndex("Finance.GlobalRate", new[] { "UserIdImpersonatedBy" });
            DropIndex("Finance.GlobalRate", new[] { "CompanyId" });
            DropIndex("Finance.GlobalRate", new[] { "LocationId" });
            DropIndex("Finance.GlobalRate", new[] { "JobMatrixLevelId" });
            DropIndex("Finance.GlobalRate", new[] { "CurrencyId" });
            DropTable("Finance.GlobalRate");
        }
    }
}
