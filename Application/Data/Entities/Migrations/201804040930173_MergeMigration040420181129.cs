namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration040420181129 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Recruitment.RecommendingPersonContactRecord",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            RecommendingPersonId = c.Long(nullable: false),
            //            ContactType = c.Int(nullable: false),
            //            ContactedOn = c.DateTime(nullable: false),
            //            ContactedById = c.Long(nullable: false),
            //            RecommendingPersonReaction = c.Int(nullable: false),
            //            Comment = c.String(),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Allocation.Employee", t => t.ContactedById)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .ForeignKey("Recruitment.RecommendingPerson", t => t.RecommendingPersonId)
            //    .Index(t => t.RecommendingPersonId)
            //    .Index(t => t.ContactedById)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.RecommendingPersonContactRecordJobOpenings",
            //    c => new
            //        {
            //            JobOpeningId = c.Long(nullable: false),
            //            RecommendingPersonContactRecordId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.JobOpeningId, t.RecommendingPersonContactRecordId })
            //    .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
            //    .ForeignKey("Recruitment.RecommendingPersonContactRecord", t => t.RecommendingPersonContactRecordId, cascadeDelete: true)
            //    .Index(t => t.JobOpeningId)
            //    .Index(t => t.RecommendingPersonContactRecordId);
            
            //AddColumn("Recruitment.InboundEmail", "Category", c => c.Int(nullable: false));
            //DropColumn("Recruitment.InboundEmail", "TriageResult");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.InboundEmail", "TriageResult", c => c.Int(nullable: false));
            //DropForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "RecommendingPersonContactRecordId", "Recruitment.RecommendingPersonContactRecord");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "JobOpeningId", "Recruitment.JobOpening");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecord", "RecommendingPersonId", "Recruitment.RecommendingPerson");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonContactRecord", "ContactedById", "Allocation.Employee");
            //DropIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", new[] { "RecommendingPersonContactRecordId" });
            //DropIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", new[] { "JobOpeningId" });
            //DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "ContactedById" });
            //DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "RecommendingPersonId" });
            //DropColumn("Recruitment.InboundEmail", "Category");
            //DropTable("Recruitment.RecommendingPersonContactRecordJobOpenings");
            //DropTable("Recruitment.RecommendingPersonContactRecord");
        }
    }
}
