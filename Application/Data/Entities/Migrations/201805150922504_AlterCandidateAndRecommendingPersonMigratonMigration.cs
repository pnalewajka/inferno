namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterCandidateAndRecommendingPersonMigratonMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "DeletedOn", c => c.DateTime());
            AddColumn("Recruitment.RecommendingPerson", "DeletedOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.RecommendingPerson", "DeletedOn");
            DropColumn("Recruitment.Candidate", "DeletedOn");
        }
    }
}
