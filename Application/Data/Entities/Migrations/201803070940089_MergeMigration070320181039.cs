namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration070320181039 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            //DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            //AddColumn("Dictionaries.Company", "FrontDeskAssigneeEmployeeId", c => c.Long());
            //AddColumn("Recruitment.Candidate", "SocialLink", c => c.String(maxLength: 255));
            //AlterColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", c => c.Long());
            //AlterColumn("Recruitment.Candidate", "FirstName", c => c.String(nullable: false, maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "LastName", c => c.String(nullable: false, maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String(maxLength: 20));
            //AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String(maxLength: 20));
            //AlterColumn("Recruitment.Candidate", "EmailAddreess", c => c.String(nullable: false, maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "EmailAddreess2", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "SkypeLogin", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "RelocateDetails", c => c.String(maxLength: 1023));
            //CreateIndex("Dictionaries.Company", "FrontDeskAssigneeEmployeeId");
            //CreateIndex("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId");
            //CreateIndex("Recruitment.Candidate", "EmailAddreess", unique: true);
            //CreateIndex("GDPR.DataOwner", "CandidateId", unique: true);
            //AddForeignKey("Dictionaries.Company", "FrontDeskAssigneeEmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Dictionaries.Company", "FrontDeskAssigneeEmployeeId", "Allocation.Employee");
            //DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            //DropIndex("Recruitment.Candidate", new[] { "EmailAddreess" });
            //DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            //DropIndex("Dictionaries.Company", new[] { "FrontDeskAssigneeEmployeeId" });
            //AlterColumn("Recruitment.Candidate", "RelocateDetails", c => c.String());
            //AlterColumn("Recruitment.Candidate", "SkypeLogin", c => c.String());
            //AlterColumn("Recruitment.Candidate", "EmailAddreess2", c => c.String());
            //AlterColumn("Recruitment.Candidate", "EmailAddreess", c => c.String());
            //AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String());
            //AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String());
            //AlterColumn("Recruitment.Candidate", "LastName", c => c.String());
            //AlterColumn("Recruitment.Candidate", "FirstName", c => c.String());
            //AlterColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", c => c.Long(nullable: false));
            //DropColumn("Recruitment.Candidate", "SocialLink");
            //DropColumn("Dictionaries.Company", "FrontDeskAssigneeEmployeeId");
            //CreateIndex("GDPR.DataOwner", "CandidateId");
            //CreateIndex("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId");
        }
    }
}
