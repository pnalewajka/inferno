﻿UPDATE [TimeTracking].[TimeReport]
   SET [InvoiceStatus] = 1
 WHERE EXISTS
 (
     SELECT [EP].[Id]
       FROM [Allocation].[EmploymentPeriod] AS [EP]
       JOIN [TimeTracking].[ContractType] AS [CT] ON [EP].[ContractTypeId] = [CT].[Id]
      WHERE [CT].[NotifyInvoiceAmount] = 1
        AND [EP].[EmployeeIdEmployee] = [EmployeeId]
        AND [EP].[StartDate] <= DATEFROMPARTS([Year], [Month], 1)
        AND ([EP].[EndDate] IS NULL OR [EP].[EndDate] >= DATEADD(DAY, 1, EOMONTH(DATEFROMPARTS([Year], [Month], 1), -1)))
 )