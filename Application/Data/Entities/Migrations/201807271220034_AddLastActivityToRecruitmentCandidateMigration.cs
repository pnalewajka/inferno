namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLastActivityToRecruitmentCandidateMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "LastActivityById", c => c.Long());
            AddColumn("Recruitment.Candidate", "LastActivityOn", c => c.DateTime());
            CreateIndex("Recruitment.Candidate", "LastActivityById");
            AddForeignKey("Recruitment.Candidate", "LastActivityById", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.Candidate", "LastActivityById", "Allocation.Employee");
            DropIndex("Recruitment.Candidate", new[] { "LastActivityById" });
            DropColumn("Recruitment.Candidate", "LastActivityOn");
            DropColumn("Recruitment.Candidate", "LastActivityById");
        }
    }
}
