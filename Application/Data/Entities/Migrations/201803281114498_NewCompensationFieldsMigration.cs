﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class NewCompensationFieldsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803281114498_NewCompensationFieldsMigration.Script.Up.sql");
        }

        public override void Down()
        {
            DropColumn("Allocation.EmploymentPeriod", "AuthorRemunerationRatio");
            DropColumn("Allocation.EmploymentPeriod", "SalaryGross");
        }
    }
}
