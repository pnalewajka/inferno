﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RenameSalarySecurityRoleMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807231245328_RenameSalarySecurityRoleMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
