namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecommendingPersonUniqueEmailIndexMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.RecommendingPerson", new[] { "EmailAddress" });
            Sql("CREATE UNIQUE INDEX IX_Recruitment_RecommendingPerson_EmailAddress ON Recruitment.RecommendingPerson(EmailAddress) WHERE IsDeleted = 0");
        }

        public override void Down()
        {
            Sql("DROP INDEX IX_Recruitment_RecommendingPerson_EmailAddress ON Recruitment.RecommendingPerson");
            CreateIndex("Recruitment.RecommendingPerson", "EmailAddress", unique: true);
        }
    }
}
