namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInResumeTokenEntityMigration : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "Resume.ResumeShareToken", newSchema: "Resumes");
            RenameColumn(table: "Resumes.ResumeShareToken", name: "ResumeOwnerEmployeeId", newName: "OwnerEmployeeId");
            AddColumn("Resumes.ResumeShareToken", "Token", c => c.Guid(nullable: false));
            CreateIndex("Resumes.ResumeShareToken", "Token", unique: true, name: "IX_UniqueToken");
        }
        
        public override void Down()
        {
            DropIndex("Resumes.ResumeShareToken", "IX_UniqueToken");
            DropColumn("Resumes.ResumeShareToken", "Token");
            RenameColumn(table: "Resumes.ResumeShareToken", name: "OwnerEmployeeId", newName: "ResumeOwnerEmployeeId");
            MoveTable(name: "Resumes.ResumeShareToken", newSchema: "Resume");
        }
    }
}
