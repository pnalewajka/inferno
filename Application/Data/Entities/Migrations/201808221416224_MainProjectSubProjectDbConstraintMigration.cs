﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class MainProjectSubProjectDbConstraintMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropIndex("Allocation.ProjectSetup", "IX_ProjectNamePerClient");
            AlterColumn("Allocation.ProjectSetup", "ClientShortName", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("Allocation.ProjectSetup", "ProjectShortName", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("Allocation.ProjectSetup", new[] { "ClientShortName", "ProjectShortName" }, unique: true, name: "IX_ProjectNamePerClient");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808221416224_MainProjectSubProjectDbConstraintMigration.CreateConstraint.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808221416224_MainProjectSubProjectDbConstraintMigration.CreateConstraint.Down.sql");

            DropIndex("Allocation.ProjectSetup", "IX_ProjectNamePerClient");
            AlterColumn("Allocation.ProjectSetup", "ProjectShortName", c => c.String(maxLength: 128));
            AlterColumn("Allocation.ProjectSetup", "ClientShortName", c => c.String(maxLength: 128));
            CreateIndex("Allocation.ProjectSetup", new[] { "ClientShortName", "ProjectShortName" }, unique: true, name: "IX_ProjectNamePerClient");
        }
    }
}

