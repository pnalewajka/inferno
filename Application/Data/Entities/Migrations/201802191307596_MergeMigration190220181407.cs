namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration190220181407 : DbMigration
    {
        public override void Up()
        {
            /*DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.Arrangement", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripParticipant", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripProjectsMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("Workflows.AdvancedPaymentRequest", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripMessage", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropIndex("BusinessTrips.BusinessTrip", new[] { "RequestId" });
            DropColumn("BusinessTrips.BusinessTrip", "Id");
            RenameColumn(table: "BusinessTrips.BusinessTrip", name: "RequestId", newName: "Id");
            DropPrimaryKey("BusinessTrips.BusinessTrip");
            CreateTable(
                "Recruitment.RecommendingPerson",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmailAdress = c.String(),
                        PhoneNumber = c.String(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            AddColumn("TimeTracking.ContractType", "SettlementSectionsFeatures", c => c.Int(nullable: false));
            AlterColumn("BusinessTrips.BusinessTrip", "Id", c => c.Long(nullable: false));
            AlterColumn("Workflows.BusinessTripSettlementRequest", "DepartureDateTime", c => c.DateTime());
            AlterColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDateTime", c => c.DateTime());
            AddPrimaryKey("BusinessTrips.BusinessTrip", "Id");
            CreateIndex("BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
            AddForeignKey("BusinessTrips.Arrangement", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripParticipant", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripProjectsMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
            AddForeignKey("Workflows.AdvancedPaymentRequest", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripMessage", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            DropColumn("BusinessTrips.BusinessTrip", "Timestamp");*/
        }
        
        public override void Down()
        {
            /*AddColumn("BusinessTrips.BusinessTrip", "Timestamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            DropForeignKey("BusinessTrips.BusinessTripMessage", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip");
            DropForeignKey("Workflows.AdvancedPaymentRequest", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripProjectsMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripParticipant", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.Arrangement", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("Recruitment.RecommendingPerson", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPerson", "UserIdImpersonatedBy", "Accounts.User");
            DropIndex("Recruitment.RecommendingPerson", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.RecommendingPerson", new[] { "UserIdImpersonatedBy" });
            DropIndex("BusinessTrips.BusinessTrip", new[] { "Id" });
            DropPrimaryKey("BusinessTrips.BusinessTrip");
            AlterColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDateTime", c => c.DateTime(nullable: false));
            AlterColumn("Workflows.BusinessTripSettlementRequest", "DepartureDateTime", c => c.DateTime(nullable: false));
            AlterColumn("BusinessTrips.BusinessTrip", "Id", c => c.Long(nullable: false, identity: true));
            DropColumn("TimeTracking.ContractType", "SettlementSectionsFeatures");
            DropTable("Recruitment.RecommendingPerson");
            AddPrimaryKey("BusinessTrips.BusinessTrip", "Id");
            RenameColumn(table: "BusinessTrips.BusinessTrip", name: "Id", newName: "RequestId");
            AddColumn("BusinessTrips.BusinessTrip", "Id", c => c.Long(nullable: false, identity: true));
            CreateIndex("BusinessTrips.BusinessTrip", "RequestId");
            AddForeignKey("BusinessTrips.BusinessTripMessage", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("Workflows.AdvancedPaymentRequest", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripProjectsMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
            AddForeignKey("BusinessTrips.BusinessTripParticipant", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.Arrangement", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);*/
        }
    }
}
