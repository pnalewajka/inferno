namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeRecommendingPersonNotesLongerMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.RecommendingPersonNote", "Content", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.RecommendingPersonNote", "Content", c => c.String(maxLength: 1024));
        }
    }
}
