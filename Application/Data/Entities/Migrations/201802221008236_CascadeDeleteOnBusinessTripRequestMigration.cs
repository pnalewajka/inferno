namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeDeleteOnBusinessTripRequestMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest");
            AddForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest");
            AddForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest", "Id");
        }
    }
}
