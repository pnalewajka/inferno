namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreatedByOnArrangementMigration : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM [BusinessTrips].[ArrangementDocumentContent]; DELETE FROM [BusinessTrips].[ArrangementDocument]; DELETE FROM [BusinessTrips].[Arrangement]");

            AddColumn("BusinessTrips.Arrangement", "CreatedByUserId", c => c.Long(nullable: false));
            CreateIndex("BusinessTrips.Arrangement", "CreatedByUserId");
            AddForeignKey("BusinessTrips.Arrangement", "CreatedByUserId", "Accounts.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("BusinessTrips.Arrangement", "CreatedByUserId", "Accounts.User");
            DropIndex("BusinessTrips.Arrangement", new[] { "CreatedByUserId" });
            DropColumn("BusinessTrips.Arrangement", "CreatedByUserId");
        }
    }
}
