namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OneToZeroRelationRecruitmentProcessRefactorMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("Recruitment.RecruitmentProcess", "OfferId");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalId", c => c.Long(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningId", c => c.Long(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "OfferId", c => c.Long(nullable: false));
        }
    }
}
