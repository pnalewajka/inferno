﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class ExtendDataActivityDescriptionFieldsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201809110958013_ExtendDataActivityDescriptionFieldsMigration.S1S2S3.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

