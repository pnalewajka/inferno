namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration160320180927 : DbMigration
    {
        public override void Up()
        {
        //    CreateTable(
        //        "Recruitment.InboundEmailDocumentContent",
        //        c => new
        //            {
        //                Id = c.Long(nullable: false),
        //                Content = c.Binary(),
        //            })
        //        .PrimaryKey(t => t.Id)
        //        .ForeignKey("Recruitment.InboundEmailDocument", t => t.Id)
        //        .Index(t => t.Id);
            
        //    CreateTable(
        //        "Recruitment.InboundEmailDocument",
        //        c => new
        //            {
        //                Id = c.Long(nullable: false, identity: true),
        //                InboundEmailId = c.Long(nullable: false),
        //                Name = c.String(maxLength: 255),
        //                ContentType = c.String(maxLength: 250, unicode: false),
        //                ContentLength = c.Long(nullable: false),
        //                UserIdImpersonatedBy = c.Long(),
        //                UserIdModifiedBy = c.Long(),
        //                ModifiedOn = c.DateTime(nullable: false),
        //                Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
        //            })
        //        .PrimaryKey(t => t.Id)
        //        .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
        //        .ForeignKey("Recruitment.InboundEmail", t => t.InboundEmailId)
        //        .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
        //        .Index(t => t.InboundEmailId)
        //        .Index(t => t.UserIdImpersonatedBy)
        //        .Index(t => t.UserIdModifiedBy);
            
        //    CreateTable(
        //        "Recruitment.InboundEmail",
        //        c => new
        //            {
        //                Id = c.Long(nullable: false, identity: true),
        //                To = c.String(),
        //                From = c.String(),
        //                Subject = c.String(),
        //                Body = c.String(),
        //                SentOn = c.DateTime(nullable: false),
        //                Status = c.Int(nullable: false),
        //                IsAboutCandidate = c.Boolean(nullable: false),
        //                IsAboutRecommendingPerson = c.Boolean(nullable: false),
        //                UserIdCreatedBy = c.Long(),
        //                UserIdImpersonatedCreatedBy = c.Long(),
        //                CreatedOn = c.DateTime(nullable: false),
        //                IsDeleted = c.Boolean(nullable: false),
        //                UserIdImpersonatedBy = c.Long(),
        //                UserIdModifiedBy = c.Long(),
        //                ModifiedOn = c.DateTime(nullable: false),
        //                Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
        //                Recruiter_Id = c.Long(),
        //            })
        //        .PrimaryKey(t => t.Id)
        //        .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
        //        .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
        //        .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
        //        .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
        //        .ForeignKey("Allocation.Employee", t => t.Recruiter_Id)
        //        .Index(t => t.UserIdCreatedBy)
        //        .Index(t => t.UserIdImpersonatedCreatedBy)
        //        .Index(t => t.UserIdImpersonatedBy)
        //        .Index(t => t.UserIdModifiedBy)
        //        .Index(t => t.Recruiter_Id);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.InboundEmailDocumentContent", "Id", "Recruitment.InboundEmailDocument");
            //DropForeignKey("Recruitment.InboundEmailDocument", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.InboundEmail", "Recruiter_Id", "Allocation.Employee");
            //DropForeignKey("Recruitment.InboundEmail", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.InboundEmail", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.InboundEmail", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.InboundEmail", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.InboundEmailDocument", "InboundEmailId", "Recruitment.InboundEmail");
            //DropForeignKey("Recruitment.InboundEmailDocument", "UserIdImpersonatedBy", "Accounts.User");
            //DropIndex("Recruitment.InboundEmail", new[] { "Recruiter_Id" });
            //DropIndex("Recruitment.InboundEmail", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.InboundEmail", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.InboundEmail", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.InboundEmail", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.InboundEmailDocument", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.InboundEmailDocument", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.InboundEmailDocument", new[] { "InboundEmailId" });
            //DropIndex("Recruitment.InboundEmailDocumentContent", new[] { "Id" });
            //DropTable("Recruitment.InboundEmail");
            //DropTable("Recruitment.InboundEmailDocument");
            //DropTable("Recruitment.InboundEmailDocumentContent");
        }
    }
}
