namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJobDefinitionForRecruitmentsMigration : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Scheduling.JobDefinition SET ScheduleSettings = '0 0 22 * * *' WHERE Code = 'InboundEmailFetcher'");
            Sql("UPDATE Scheduling.JobDefinition SET ScheduleSettings = '0 30 21 * * *' WHERE Code = 'CandidateNextFollowUpMissedJob'");
            Sql("UPDATE Scheduling.JobDefinition SET ScheduleSettings = '0 30 22 * * *' WHERE Code = 'DataConsentExpiringJob'");
        }
        
        public override void Down()
        {
        }
    }
}
