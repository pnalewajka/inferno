namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddEvaluatorsEmployeeToFeedbackRequestMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Workflow.FeedbackRequestEvaluators",
                c => new
                    {
                        FeedbackRequestId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.FeedbackRequestId, t.EmployeeId })
                .ForeignKey("Workflows.FeedbackRequest", t => t.FeedbackRequestId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.FeedbackRequestId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Workflow.FeedbackRequestEvaluators", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Workflow.FeedbackRequestEvaluators", "FeedbackRequestId", "Workflows.FeedbackRequest");
            DropIndex("Workflow.FeedbackRequestEvaluators", new[] { "EmployeeId" });
            DropIndex("Workflow.FeedbackRequestEvaluators", new[] { "FeedbackRequestId" });
            DropTable("Workflow.FeedbackRequestEvaluators");
        }
    }
}
