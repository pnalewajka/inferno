﻿ALTER PROCEDURE [Runtime].[atomic_PromoteTemporaryDocumentToTuple]
    @CurrentUserId BIGINT,
    @TargetMetaTableSchema NVARCHAR(128),
    @TargetMetaTableName NVARCHAR(128),
    @TargetMetaTableId BIGINT,
    @TargetDataTableSchema NVARCHAR(128),
    @TargetDataTableName NVARCHAR(128),
    @TargetDataTableId BIGINT,
    @DataColumnName NVARCHAR(128),
    @MimeTypeColumnName NVARCHAR(128),
    @ContentLengthColumnName NVARCHAR(128),
    @NameColumnName NVARCHAR(128),
    @TemporaryDocumentIdentifier UNIQUEIDENTIFIER
AS
BEGIN
    DECLARE @sqlUpdateContents NVARCHAR(MAX)

    SELECT @sqlUpdateContents = STUFF((
		SELECT 
			' + ' + CONCAT('(SELECT Data From [Runtime].[TemporaryDocumentPart] tdp WHERE tdp.Id=', cast(tdp.Id as varchar(max)), ')') 
		FROM 
			[Runtime].[TemporaryDocumentPart] tdp
			JOIN [Runtime].[TemporaryDocument] td ON tdp.TemporaryDocumentId = td.Id
			WHERE td.Identifier = @TemporaryDocumentIdentifier
			FOR XML PATH ('')
		 ), 1, 3, '')

    BEGIN TRANSACTION

    IF NOT EXISTS (
        SELECT TOP 1 Id from [Runtime].[TemporaryDocument] 
        WHERE UserIdModifiedBy = @CurrentUserId AND Identifier = @TemporaryDocumentIdentifier
    )
    THROW 51000, 'The record does not exist.', 1;  


    DECLARE @params NVARCHAR(MAX) = N'@targetDocumentId BIGINT, @temporaryDocumentIdentifier uniqueidentifier'
    DECLARE @sql NVARCHAR(MAX) = N'
    UPDATE [' + @TargetMetaTableSchema+'].['+@TargetMetaTableName+N']
        SET 
            [' + @NameColumnName + '] = source.Name,
            [' + @MimeTypeColumnName + '] = source.ContentType,
            [' + @ContentLengthColumnName + '] = source.ContentLength
    FROM
        [' + @TargetMetaTableSchema + '].[' + @TargetMetaTableName + N'] target
        JOIN [Runtime].[TemporaryDocument] source ON source.Identifier = @temporaryDocumentIdentifier
    WHERE 
        target.ID = @targetDocumentId
    '

    EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId, @temporaryDocumentIdentifier = @TemporaryDocumentIdentifier

    IF (@TargetDataTableId IS NULL)
    BEGIN
        --insert, using PK from meta
        SET @params = N'@targetDocumentId BIGINT'
        --update using provided id
        SET @sql = N'
        INSERT INTO ['+ @TargetDataTableSchema+'].['+@TargetDataTableName+N']
        (ID)
        VALUES
        (
            @targetDocumentId
        )
        '
        EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId
    END
    
    SET @params = N'@targetDocumentId BIGINT'
    --update using provided id
    SET @sql = N'
    UPDATE [' + @TargetDataTableSchema + '].[' + @TargetDataTableName + N']
        SET 
            [' + @DataColumnName + '] = (' + @sqlUpdateContents + N')
    WHERE 
        ID = @targetDocumentId
    '
    EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId

    COMMIT
END
