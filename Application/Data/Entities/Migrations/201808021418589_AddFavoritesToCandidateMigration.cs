namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddFavoritesToCandidateMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Recruitment.CandidateFavorites",
                c => new
                {
                    CandidateId = c.Long(nullable: false),
                    EmployeeId = c.Long(nullable: false),
                })
                .PrimaryKey(t => new { t.CandidateId, t.EmployeeId })
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.EmployeeId);
        }

        public override void Down()
        {
            DropTable("Recruitment.CandidateFavorites");
        }
    }
}
