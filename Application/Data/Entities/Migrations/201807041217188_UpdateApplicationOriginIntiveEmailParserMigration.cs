namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UpdateApplicationOriginIntiveEmailParserMigration : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Recruitment.ApplicationOrigin SET ParserIdentifier = 'Recruitment.IntiveEmailParser', EmailAddress = 'sender@intive.com' WHERE Id = 11");
        }

        public override void Down()
        {
        }
    }
}
