﻿SET XACT_ABORT ON
GO

UPDATE TimeTracking.ContractType SET 
	TimeTrackingCompensationCalculator = NULL,
	BusinessTripCompensationCalculator = NULL,
	SettlementSectionsFeatures = 0