namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveNullableFieldsInSettlementRequest : DbMigration
    {
        public override void Up()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", new[] { "DepartureCountryId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureDateTime", c => c.DateTime(nullable: false));
            AlterColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCountryId", c => c.Long(nullable: false));
            CreateIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCountryId");
        }
        
        public override void Down()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", new[] { "DepartureCountryId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCountryId", c => c.Long());
            AlterColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureDateTime", c => c.DateTime());
            CreateIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCountryId");
        }
    }
}
