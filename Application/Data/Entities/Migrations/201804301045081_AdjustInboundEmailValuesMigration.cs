namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AdjustInboundEmailValuesMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Recruitment.InboundEmailValue", "Value", "ValueString");
            AddColumn("Recruitment.InboundEmailValue", "ValueLong", c => c.Long());
        }
        
        public override void Down()
        {
            RenameColumn("Recruitment.InboundEmailValue", "ValueString", "Value");
            DropColumn("Recruitment.InboundEmailValue", "ValueLong");
        }
    }
}
