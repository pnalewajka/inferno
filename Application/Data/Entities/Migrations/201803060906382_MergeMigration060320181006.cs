namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration060320181006 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            //AddColumn("Recruitment.Candidate", "SocialLink", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "FirstName", c => c.String(nullable: false, maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "LastName", c => c.String(nullable: false, maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String(maxLength: 20));
            //AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String(maxLength: 20));
            //AlterColumn("Recruitment.Candidate", "EmailAddreess", c => c.String(nullable: false, maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "EmailAddreess2", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "SkypeLogin", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.Candidate", "RelocateDetails", c => c.String(maxLength: 1023));
            //CreateIndex("Recruitment.Candidate", "EmailAddreess", unique: true);
            //CreateIndex("GDPR.DataOwner", "CandidateId");
        }
        
        public override void Down()
        {
            //DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            //DropIndex("Recruitment.Candidate", new[] { "EmailAddreess" });
            //AlterColumn("Recruitment.Candidate", "RelocateDetails", c => c.String());
            //AlterColumn("Recruitment.Candidate", "SkypeLogin", c => c.String());
            //AlterColumn("Recruitment.Candidate", "EmailAddreess2", c => c.String());
            //AlterColumn("Recruitment.Candidate", "EmailAddreess", c => c.String());
            //AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String());
            //AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String());
            //AlterColumn("Recruitment.Candidate", "LastName", c => c.String());
            //AlterColumn("Recruitment.Candidate", "FirstName", c => c.String());
            //DropColumn("Recruitment.Candidate", "SocialLink");
            //CreateIndex("GDPR.DataOwner", "CandidateId", unique: true);
        }
    }
}
