﻿SET XACT_ABORT ON
GO

update [Inferno].[PeopleManagement].[EmploymentTerminationReason] set IsAttrition = 1 where Id = 1;

update [Inferno].[PeopleManagement].[EmploymentTerminationReason] set IsTurnover = 1 where Id = 2;

update [Inferno].[PeopleManagement].[EmploymentTerminationReason] set IsTurnover = 1 where Id = 3;

update [Inferno].[PeopleManagement].[EmploymentTerminationReason] set IsAttrition = 1 where Id = 4;

update [Inferno].[PeopleManagement].[EmploymentTerminationReason] set IsTurnover = 1 where Id = 5;

update [Inferno].[PeopleManagement].[EmploymentTerminationReason] set  IsAttrition = 1, NameEn = N'Expiry of contract by employee', NamePl = N'Z upływem czasu, na jaki umowa była zawarta - ze strony pracownika' where Id = 6;

IF NOT EXISTS (SELECT Id FROM [Inferno].[PeopleManagement].[EmploymentTerminationReason] WHERE [NameEn] = N'Expiry of contract by intive')
BEGIN

	insert into [Inferno].[PeopleManagement].[EmploymentTerminationReason] (NameEn, NamePl, IsTurnover) VALUES (N'Expiry of contract by intive', N'Z upływem czasu, na jaki umowa była zawarta - ze strony pracodawcy', 1)

END
