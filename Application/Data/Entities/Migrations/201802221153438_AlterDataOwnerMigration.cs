namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AlterDataOwnerMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropIndex("GDPR.DataOwner", new[] { "UserId" });
            DropIndex("GDPR.DataOwner", new[] { "EmployeeId" });
            DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            DropIndex("GDPR.DataOwner", new[] { "RecommendingPersonId" });
            AlterColumn("GDPR.DataOwner", "UserId", c => c.Long());
            AlterColumn("GDPR.DataOwner", "EmployeeId", c => c.Long());
            AlterColumn("GDPR.DataOwner", "CandidateId", c => c.Long());
            AlterColumn("GDPR.DataOwner", "RecommendingPersonId", c => c.Long());
            CreateIndex("GDPR.DataOwner", "UserId");
            CreateIndex("GDPR.DataOwner", "EmployeeId");
            CreateIndex("GDPR.DataOwner", "CandidateId");
            CreateIndex("GDPR.DataOwner", "RecommendingPersonId");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802221153438_AlterDataOwnerMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
            DropIndex("GDPR.DataOwner", new[] { "RecommendingPersonId" });
            DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            DropIndex("GDPR.DataOwner", new[] { "EmployeeId" });
            DropIndex("GDPR.DataOwner", new[] { "UserId" });
            AlterColumn("GDPR.DataOwner", "RecommendingPersonId", c => c.Long(nullable: false));
            AlterColumn("GDPR.DataOwner", "CandidateId", c => c.Long(nullable: false));
            AlterColumn("GDPR.DataOwner", "EmployeeId", c => c.Long(nullable: false));
            AlterColumn("GDPR.DataOwner", "UserId", c => c.Long(nullable: false));
            CreateIndex("GDPR.DataOwner", "RecommendingPersonId");
            CreateIndex("GDPR.DataOwner", "CandidateId");
            CreateIndex("GDPR.DataOwner", "EmployeeId");
            CreateIndex("GDPR.DataOwner", "UserId");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802221153438_AlterDataOwnerMigration.Script.Down.sql");
        }
    }
}
