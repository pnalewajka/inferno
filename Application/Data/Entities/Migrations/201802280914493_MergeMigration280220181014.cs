namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration280220181014 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("Recruitment.Applicant", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.Applicant", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant");
            //DropForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant");
            //DropIndex("Recruitment.Applicant", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.Applicant", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.Candidate", new[] { "ApplicantId" });
            //DropIndex("GDPR.DataOwner", new[] { "UserId" });
            //DropIndex("GDPR.DataOwner", new[] { "EmployeeId" });
            //DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            //DropIndex("GDPR.DataOwner", new[] { "RecommendingPersonId" });
            //DropIndex("GDPR.DataConsent", new[] { "ApplicantId" });
            //CreateTable(
            //    "Recruitment.JobApplication",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            CandidateID = c.Long(nullable: false),
            //            OriginType = c.Int(nullable: false),
            //            ApplicationOriginId = c.Long(nullable: false),
            //            RecommendingPersonId = c.Long(),
            //            OriginComment = c.String(maxLength: 250),
            //            CandidateComment = c.String(maxLength: 250),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.ApplicationOrigin", t => t.ApplicationOriginId)
            //    .ForeignKey("Recruitment.Candidate", t => t.CandidateID)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .ForeignKey("Recruitment.RecommendingPerson", t => t.RecommendingPersonId)
            //    .Index(t => t.CandidateID)
            //    .Index(t => t.ApplicationOriginId)
            //    .Index(t => t.RecommendingPersonId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.JobApplicationLocations",
            //    c => new
            //        {
            //            JobOpeningId = c.Long(nullable: false),
            //            LocationId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.JobOpeningId, t.LocationId })
            //    .ForeignKey("Recruitment.JobApplication", t => t.JobOpeningId, cascadeDelete: true)
            //    .ForeignKey("Dictionaries.Location", t => t.LocationId, cascadeDelete: true)
            //    .Index(t => t.JobOpeningId)
            //    .Index(t => t.LocationId);
            
            //CreateTable(
            //    "Recruitment.JobApplicationTechnologies",
            //    c => new
            //        {
            //            JobOpeningId = c.Long(nullable: false),
            //            SkillId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.JobOpeningId, t.SkillId })
            //    .ForeignKey("Recruitment.JobApplication", t => t.JobOpeningId, cascadeDelete: true)
            //    .ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
            //    .Index(t => t.JobOpeningId)
            //    .Index(t => t.SkillId);
            
            //AlterColumn("GDPR.DataOwner", "UserId", c => c.Long());
            //AlterColumn("GDPR.DataOwner", "EmployeeId", c => c.Long());
            //AlterColumn("GDPR.DataOwner", "CandidateId", c => c.Long());
            //AlterColumn("GDPR.DataOwner", "RecommendingPersonId", c => c.Long());
            //CreateIndex("GDPR.DataOwner", "UserId");
            //CreateIndex("GDPR.DataOwner", "EmployeeId");
            //CreateIndex("GDPR.DataOwner", "CandidateId");
            //CreateIndex("GDPR.DataOwner", "RecommendingPersonId");
            //DropColumn("Recruitment.Candidate", "ApplicantId");
            //DropColumn("GDPR.DataConsent", "ApplicantId");
            //DropTable("Recruitment.Applicant");
        }
        
        public override void Down()
        {
            //CreateTable(
            //    "Recruitment.Applicant",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            FirstName = c.String(),
            //            LastName = c.String(),
            //            EmailAdress = c.String(),
            //            PhoneNumber = c.String(),
            //            ApplicantType = c.Int(nullable: false),
            //            CanBeCandidate = c.Boolean(nullable: false),
            //            CanBeRecommendingPerson = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //AddColumn("GDPR.DataConsent", "ApplicantId", c => c.Long(nullable: false));
            //AddColumn("Recruitment.Candidate", "ApplicantId", c => c.Long(nullable: false));
            //DropForeignKey("Recruitment.JobApplication", "RecommendingPersonId", "Recruitment.RecommendingPerson");
            //DropForeignKey("Recruitment.JobApplication", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplicationTechnologies", "SkillId", "SkillManagement.Skill");
            //DropForeignKey("Recruitment.JobApplicationTechnologies", "JobOpeningId", "Recruitment.JobApplication");
            //DropForeignKey("Recruitment.JobApplicationLocations", "LocationId", "Dictionaries.Location");
            //DropForeignKey("Recruitment.JobApplicationLocations", "JobOpeningId", "Recruitment.JobApplication");
            //DropForeignKey("Recruitment.JobApplication", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplication", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplication", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplication", "CandidateID", "Recruitment.Candidate");
            //DropForeignKey("Recruitment.JobApplication", "ApplicationOriginId", "Recruitment.ApplicationOrigin");
            //DropIndex("Recruitment.JobApplicationTechnologies", new[] { "SkillId" });
            //DropIndex("Recruitment.JobApplicationTechnologies", new[] { "JobOpeningId" });
            //DropIndex("Recruitment.JobApplicationLocations", new[] { "LocationId" });
            //DropIndex("Recruitment.JobApplicationLocations", new[] { "JobOpeningId" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "RecommendingPersonId" });
            //DropIndex("Recruitment.JobApplication", new[] { "ApplicationOriginId" });
            //DropIndex("Recruitment.JobApplication", new[] { "CandidateID" });
            //DropIndex("GDPR.DataOwner", new[] { "RecommendingPersonId" });
            //DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            //DropIndex("GDPR.DataOwner", new[] { "EmployeeId" });
            //DropIndex("GDPR.DataOwner", new[] { "UserId" });
            //AlterColumn("GDPR.DataOwner", "RecommendingPersonId", c => c.Long(nullable: false));
            //AlterColumn("GDPR.DataOwner", "CandidateId", c => c.Long(nullable: false));
            //AlterColumn("GDPR.DataOwner", "EmployeeId", c => c.Long(nullable: false));
            //AlterColumn("GDPR.DataOwner", "UserId", c => c.Long(nullable: false));
            //DropTable("Recruitment.JobApplicationTechnologies");
            //DropTable("Recruitment.JobApplicationLocations");
            //DropTable("Recruitment.JobApplication");
            //CreateIndex("GDPR.DataConsent", "ApplicantId");
            //CreateIndex("GDPR.DataOwner", "RecommendingPersonId");
            //CreateIndex("GDPR.DataOwner", "CandidateId");
            //CreateIndex("GDPR.DataOwner", "EmployeeId");
            //CreateIndex("GDPR.DataOwner", "UserId");
            //CreateIndex("Recruitment.Candidate", "ApplicantId");
            //CreateIndex("Recruitment.Applicant", "UserIdModifiedBy");
            //CreateIndex("Recruitment.Applicant", "UserIdImpersonatedBy");
            //AddForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant", "Id");
            //AddForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant", "Id");
            //AddForeignKey("Recruitment.Applicant", "UserIdModifiedBy", "Accounts.User", "Id");
            //AddForeignKey("Recruitment.Applicant", "UserIdImpersonatedBy", "Accounts.User", "Id");
        }
    }
}
