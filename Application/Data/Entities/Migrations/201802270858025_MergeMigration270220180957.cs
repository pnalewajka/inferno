namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration270220180957 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Recruitment.Applicant",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            FirstName = c.String(),
            //            LastName = c.String(),
            //            EmailAdress = c.String(),
            //            PhoneNumber = c.String(),
            //            ApplicantType = c.Int(nullable: false),
            //            CanBeCandidate = c.Boolean(nullable: false),
            //            CanBeRecommendingPerson = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.JobApplication",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            CandidateID = c.Long(nullable: false),
            //            OriginType = c.Int(nullable: false),
            //            SourceId = c.Long(nullable: false),
            //            RecommendingPersonId = c.Long(),
            //            SourceComment = c.String(maxLength: 250),
            //            CandidateComment = c.String(maxLength: 250),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.Candidate", t => t.CandidateID)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .ForeignKey("Recruitment.RecommendingPerson", t => t.RecommendingPersonId)
            //    .Index(t => t.CandidateID)
            //    .Index(t => t.RecommendingPersonId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.JobApplicationLocations",
            //    c => new
            //        {
            //            JobOpeningId = c.Long(nullable: false),
            //            LocationId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.JobOpeningId, t.LocationId })
            //    .ForeignKey("Recruitment.JobApplication", t => t.JobOpeningId, cascadeDelete: true)
            //    .ForeignKey("Dictionaries.Location", t => t.LocationId, cascadeDelete: true)
            //    .Index(t => t.JobOpeningId)
            //    .Index(t => t.LocationId);
            
            //CreateTable(
            //    "Recruitment.JobApplicationTechnologies",
            //    c => new
            //        {
            //            JobOpeningId = c.Long(nullable: false),
            //            SkillId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.JobOpeningId, t.SkillId })
            //    .ForeignKey("Recruitment.JobApplication", t => t.JobOpeningId, cascadeDelete: true)
            //    .ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
            //    .Index(t => t.JobOpeningId)
            //    .Index(t => t.SkillId);
            
            //AddColumn("Recruitment.Candidate", "ApplicantId", c => c.Long(nullable: false));
            //AddColumn("GDPR.DataConsent", "ApplicantId", c => c.Long(nullable: false));
            //CreateIndex("Recruitment.Candidate", "ApplicantId");
            //CreateIndex("GDPR.DataConsent", "ApplicantId");
            //AddForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant", "Id");
            //AddForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.JobApplication", "RecommendingPersonId", "Recruitment.RecommendingPerson");
            //DropForeignKey("Recruitment.JobApplication", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplicationTechnologies", "SkillId", "SkillManagement.Skill");
            //DropForeignKey("Recruitment.JobApplicationTechnologies", "JobOpeningId", "Recruitment.JobApplication");
            //DropForeignKey("Recruitment.JobApplicationLocations", "LocationId", "Dictionaries.Location");
            //DropForeignKey("Recruitment.JobApplicationLocations", "JobOpeningId", "Recruitment.JobApplication");
            //DropForeignKey("Recruitment.JobApplication", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplication", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplication", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobApplication", "CandidateID", "Recruitment.Candidate");
            //DropForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant");
            //DropForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant");
            //DropForeignKey("Recruitment.Applicant", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.Applicant", "UserIdImpersonatedBy", "Accounts.User");
            //DropIndex("Recruitment.JobApplicationTechnologies", new[] { "SkillId" });
            //DropIndex("Recruitment.JobApplicationTechnologies", new[] { "JobOpeningId" });
            //DropIndex("Recruitment.JobApplicationLocations", new[] { "LocationId" });
            //DropIndex("Recruitment.JobApplicationLocations", new[] { "JobOpeningId" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.JobApplication", new[] { "RecommendingPersonId" });
            //DropIndex("Recruitment.JobApplication", new[] { "CandidateID" });
            //DropIndex("GDPR.DataConsent", new[] { "ApplicantId" });
            //DropIndex("Recruitment.Candidate", new[] { "ApplicantId" });
            //DropIndex("Recruitment.Applicant", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.Applicant", new[] { "UserIdImpersonatedBy" });
            //DropColumn("GDPR.DataConsent", "ApplicantId");
            //DropColumn("Recruitment.Candidate", "ApplicantId");
            //DropTable("Recruitment.JobApplicationTechnologies");
            //DropTable("Recruitment.JobApplicationLocations");
            //DropTable("Recruitment.JobApplication");
            //DropTable("Recruitment.Applicant");
        }
    }
}
