namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDueDateToStepMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Recruitment.RecruitmentProcessStep", name: "DecisionMakerId", newName: "AssignedEmployeeId");
            RenameIndex(table: "Recruitment.RecruitmentProcessStep", name: "IX_DecisionMakerId", newName: "IX_AssignedEmployeeId");
            AddColumn("Recruitment.RecruitmentProcessStep", "DueOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.RecruitmentProcessStep", "DueOn");
            RenameIndex(table: "Recruitment.RecruitmentProcessStep", name: "IX_AssignedEmployeeId", newName: "IX_DecisionMakerId");
            RenameColumn(table: "Recruitment.RecruitmentProcessStep", name: "AssignedEmployeeId", newName: "DecisionMakerId");
        }
    }
}
