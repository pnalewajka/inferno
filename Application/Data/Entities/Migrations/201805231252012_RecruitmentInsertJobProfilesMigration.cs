﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RecruitmentInsertJobProfilesMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805231252012_RecruitmentInsertJobProfilesMigration.InsertJobProfiles.Up.sql");
        }

        public override void Down()
        {
        }
    }
}

