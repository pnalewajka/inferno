namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectAcceptanceConditionsMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Allocation.Project", "ReinvoicingToCustomer", c => c.Boolean(nullable: false));
            AddColumn("Allocation.Project", "ReinvoiceCurrencyId", c => c.Long());
            AddColumn("Allocation.Project", "CostApprovalPolicy", c => c.Int(nullable: false));
            AddColumn("Allocation.Project", "MaxCostsCurrencyId", c => c.Long());
            AddColumn("Allocation.Project", "MaxHotelCosts", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Allocation.Project", "MaxTotalFlightsCosts", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Allocation.Project", "MaxOtherTravelCosts", c => c.Decimal(precision: 18, scale: 2));
            CreateIndex("Allocation.Project", "ReinvoiceCurrencyId");
            CreateIndex("Allocation.Project", "MaxCostsCurrencyId");
            AddForeignKey("Allocation.Project", "MaxCostsCurrencyId", "BusinessTrips.Currency", "Id");
            AddForeignKey("Allocation.Project", "ReinvoiceCurrencyId", "BusinessTrips.Currency", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Allocation.Project", "ReinvoiceCurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Allocation.Project", "MaxCostsCurrencyId", "BusinessTrips.Currency");
            DropIndex("Allocation.Project", new[] { "MaxCostsCurrencyId" });
            DropIndex("Allocation.Project", new[] { "ReinvoiceCurrencyId" });
            DropColumn("Allocation.Project", "MaxOtherTravelCosts");
            DropColumn("Allocation.Project", "MaxTotalFlightsCosts");
            DropColumn("Allocation.Project", "MaxHotelCosts");
            DropColumn("Allocation.Project", "MaxCostsCurrencyId");
            DropColumn("Allocation.Project", "CostApprovalPolicy");
            DropColumn("Allocation.Project", "ReinvoiceCurrencyId");
            DropColumn("Allocation.Project", "ReinvoicingToCustomer");
        }
    }
}
