namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ReplaceMainTechnologiesWithJobProfilesMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.JobApplicationTechnologies", "JobOpeningId", "Recruitment.JobApplication");
            DropForeignKey("Recruitment.JobApplicationTechnologies", "SkillId", "SkillManagement.Skill");
            DropForeignKey("Recruitment.CandidateMainTechnologies", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.CandidateMainTechnologies", "MainTechnologyId", "SkillManagement.Skill");
            DropForeignKey("Recruitment.JobOpeningMainTechnologies", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpeningMainTechnologies", "SkillId", "SkillManagement.Skill");
            DropForeignKey("Recruitment.TechnicalReviewMainTechnologies", "TechnicalReviewId", "Recruitment.TechnicalReview");
            DropForeignKey("Recruitment.TechnicalReviewMainTechnologies", "MainTechnologyId", "SkillManagement.Skill");
            DropIndex("Recruitment.JobApplicationTechnologies", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobApplicationTechnologies", new[] { "SkillId" });
            DropIndex("Recruitment.CandidateMainTechnologies", new[] { "CandidateId" });
            DropIndex("Recruitment.CandidateMainTechnologies", new[] { "MainTechnologyId" });
            DropIndex("Recruitment.JobOpeningMainTechnologies", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningMainTechnologies", new[] { "SkillId" });
            DropIndex("Recruitment.TechnicalReviewMainTechnologies", new[] { "TechnicalReviewId" });
            DropIndex("Recruitment.TechnicalReviewMainTechnologies", new[] { "MainTechnologyId" });
            CreateTable(
                "Recruitment.JobApplicationJobProfiles",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        JobProfileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.JobProfileId })
                .ForeignKey("Recruitment.JobApplication", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.JobProfileId);
            
            CreateTable(
                "Recruitment.CandidateJobProfiles",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        JobProfileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.JobProfileId })
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.JobProfileId);
            
            CreateTable(
                "Recruitment.JobOpeningJobProfile",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        JobProfileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.JobProfileId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.JobProfileId);
            
            CreateTable(
                "Recruitment.TechnicalReviewJobProfiles",
                c => new
                    {
                        TechnicalReviewId = c.Long(nullable: false),
                        JobProfileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.TechnicalReviewId, t.JobProfileId })
                .ForeignKey("Recruitment.TechnicalReview", t => t.TechnicalReviewId, cascadeDelete: true)
                .ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId, cascadeDelete: true)
                .Index(t => t.TechnicalReviewId)
                .Index(t => t.JobProfileId);
            
            DropTable("Recruitment.JobApplicationTechnologies");
            DropTable("Recruitment.CandidateMainTechnologies");
            DropTable("Recruitment.JobOpeningMainTechnologies");
            DropTable("Recruitment.TechnicalReviewMainTechnologies");
        }
        
        public override void Down()
        {
            CreateTable(
                "Recruitment.TechnicalReviewMainTechnologies",
                c => new
                    {
                        TechnicalReviewId = c.Long(nullable: false),
                        MainTechnologyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.TechnicalReviewId, t.MainTechnologyId });
            
            CreateTable(
                "Recruitment.JobOpeningMainTechnologies",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        SkillId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.SkillId });
            
            CreateTable(
                "Recruitment.CandidateMainTechnologies",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        MainTechnologyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.MainTechnologyId });
            
            CreateTable(
                "Recruitment.JobApplicationTechnologies",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        SkillId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.SkillId });
            
            DropForeignKey("Recruitment.TechnicalReviewJobProfiles", "JobProfileId", "SkillManagement.JobProfile");
            DropForeignKey("Recruitment.TechnicalReviewJobProfiles", "TechnicalReviewId", "Recruitment.TechnicalReview");
            DropForeignKey("Recruitment.JobOpeningJobProfile", "JobProfileId", "SkillManagement.JobProfile");
            DropForeignKey("Recruitment.JobOpeningJobProfile", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.CandidateJobProfiles", "JobProfileId", "SkillManagement.JobProfile");
            DropForeignKey("Recruitment.CandidateJobProfiles", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.JobApplicationJobProfiles", "JobProfileId", "SkillManagement.JobProfile");
            DropForeignKey("Recruitment.JobApplicationJobProfiles", "JobOpeningId", "Recruitment.JobApplication");
            DropIndex("Recruitment.TechnicalReviewJobProfiles", new[] { "JobProfileId" });
            DropIndex("Recruitment.TechnicalReviewJobProfiles", new[] { "TechnicalReviewId" });
            DropIndex("Recruitment.JobOpeningJobProfile", new[] { "JobProfileId" });
            DropIndex("Recruitment.JobOpeningJobProfile", new[] { "JobOpeningId" });
            DropIndex("Recruitment.CandidateJobProfiles", new[] { "JobProfileId" });
            DropIndex("Recruitment.CandidateJobProfiles", new[] { "CandidateId" });
            DropIndex("Recruitment.JobApplicationJobProfiles", new[] { "JobProfileId" });
            DropIndex("Recruitment.JobApplicationJobProfiles", new[] { "JobOpeningId" });
            DropTable("Recruitment.TechnicalReviewJobProfiles");
            DropTable("Recruitment.JobOpeningJobProfile");
            DropTable("Recruitment.CandidateJobProfiles");
            DropTable("Recruitment.JobApplicationJobProfiles");
            CreateIndex("Recruitment.TechnicalReviewMainTechnologies", "MainTechnologyId");
            CreateIndex("Recruitment.TechnicalReviewMainTechnologies", "TechnicalReviewId");
            CreateIndex("Recruitment.JobOpeningMainTechnologies", "SkillId");
            CreateIndex("Recruitment.JobOpeningMainTechnologies", "JobOpeningId");
            CreateIndex("Recruitment.CandidateMainTechnologies", "MainTechnologyId");
            CreateIndex("Recruitment.CandidateMainTechnologies", "CandidateId");
            CreateIndex("Recruitment.JobApplicationTechnologies", "SkillId");
            CreateIndex("Recruitment.JobApplicationTechnologies", "JobOpeningId");
            AddForeignKey("Recruitment.TechnicalReviewMainTechnologies", "MainTechnologyId", "SkillManagement.Skill", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.TechnicalReviewMainTechnologies", "TechnicalReviewId", "Recruitment.TechnicalReview", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobOpeningMainTechnologies", "SkillId", "SkillManagement.Skill", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobOpeningMainTechnologies", "JobOpeningId", "Recruitment.JobOpening", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.CandidateMainTechnologies", "MainTechnologyId", "SkillManagement.Skill", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.CandidateMainTechnologies", "CandidateId", "Recruitment.Candidate", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobApplicationTechnologies", "SkillId", "SkillManagement.Skill", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobApplicationTechnologies", "JobOpeningId", "Recruitment.JobApplication", "Id", cascadeDelete: true);
        }
    }
}
