namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeRecruitmentEntitiesTrackedMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.ContactRecord", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.ContactRecord", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.ContactRecord", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.ContactRecord", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.Candidate", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.Candidate", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.Candidate", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.Candidate", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.CandidateLanguage", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.CandidateLanguage", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.CandidateLanguage", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.CandidateLanguage", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.ProjectDescription", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.ProjectDescription", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.ProjectDescription", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.RecommendingPerson", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.RecommendingPerson", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.RecommendingPerson", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.RecommendingPerson", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.ApplicationOrigin", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.ApplicationOrigin", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.ApplicationOrigin", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.ApplicationOrigin", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.RecruitmentProcessStep", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcessStep", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcessStep", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.RecruitmentProcessStep", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.TechnicalReview", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.TechnicalReview", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.TechnicalReview", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.TechnicalReview", "IsDeleted", c => c.Boolean(nullable: false));
            CreateIndex("Recruitment.ContactRecord", "UserIdCreatedBy");
            CreateIndex("Recruitment.ContactRecord", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.Candidate", "UserIdCreatedBy");
            CreateIndex("Recruitment.Candidate", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.CandidateLanguage", "UserIdCreatedBy");
            CreateIndex("Recruitment.CandidateLanguage", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.ProjectDescription", "UserIdCreatedBy");
            CreateIndex("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.RecommendingPerson", "UserIdCreatedBy");
            CreateIndex("Recruitment.RecommendingPerson", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.ApplicationOrigin", "UserIdCreatedBy");
            CreateIndex("Recruitment.ApplicationOrigin", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.RecruitmentProcess", "UserIdCreatedBy");
            CreateIndex("Recruitment.RecruitmentProcess", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.RecruitmentProcessStep", "UserIdCreatedBy");
            CreateIndex("Recruitment.RecruitmentProcessStep", "UserIdImpersonatedCreatedBy");
            CreateIndex("Recruitment.TechnicalReview", "UserIdCreatedBy");
            CreateIndex("Recruitment.TechnicalReview", "UserIdImpersonatedCreatedBy");
            AddForeignKey("Recruitment.Candidate", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.Candidate", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.CandidateLanguage", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.CandidateLanguage", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ContactRecord", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ContactRecord", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ProjectDescription", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.RecommendingPerson", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.RecommendingPerson", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ApplicationOrigin", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.ApplicationOrigin", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.RecruitmentProcessStep", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.RecruitmentProcessStep", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.TechnicalReview", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.TechnicalReview", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.TechnicalReview", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.TechnicalReview", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcessStep", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcessStep", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcess", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcess", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ApplicationOrigin", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ApplicationOrigin", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPerson", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPerson", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ContactRecord", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ContactRecord", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.CandidateLanguage", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.CandidateLanguage", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.Candidate", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.Candidate", "UserIdCreatedBy", "Accounts.User");
            DropIndex("Recruitment.TechnicalReview", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.TechnicalReview", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.ApplicationOrigin", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.ApplicationOrigin", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.RecommendingPerson", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.RecommendingPerson", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.CandidateLanguage", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.CandidateLanguage", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.Candidate", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.Candidate", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.ContactRecord", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.ContactRecord", new[] { "UserIdCreatedBy" });
            DropColumn("Recruitment.TechnicalReview", "IsDeleted");
            DropColumn("Recruitment.TechnicalReview", "CreatedOn");
            DropColumn("Recruitment.TechnicalReview", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.TechnicalReview", "UserIdCreatedBy");
            DropColumn("Recruitment.RecruitmentProcessStep", "IsDeleted");
            DropColumn("Recruitment.RecruitmentProcessStep", "CreatedOn");
            DropColumn("Recruitment.RecruitmentProcessStep", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.RecruitmentProcessStep", "UserIdCreatedBy");
            DropColumn("Recruitment.RecruitmentProcess", "IsDeleted");
            DropColumn("Recruitment.RecruitmentProcess", "CreatedOn");
            DropColumn("Recruitment.RecruitmentProcess", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.RecruitmentProcess", "UserIdCreatedBy");
            DropColumn("Recruitment.ApplicationOrigin", "IsDeleted");
            DropColumn("Recruitment.ApplicationOrigin", "CreatedOn");
            DropColumn("Recruitment.ApplicationOrigin", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.ApplicationOrigin", "UserIdCreatedBy");
            DropColumn("Recruitment.RecommendingPerson", "IsDeleted");
            DropColumn("Recruitment.RecommendingPerson", "CreatedOn");
            DropColumn("Recruitment.RecommendingPerson", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.RecommendingPerson", "UserIdCreatedBy");
            DropColumn("Recruitment.ProjectDescription", "IsDeleted");
            DropColumn("Recruitment.ProjectDescription", "CreatedOn");
            DropColumn("Recruitment.ProjectDescription", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.ProjectDescription", "UserIdCreatedBy");
            DropColumn("Recruitment.CandidateLanguage", "IsDeleted");
            DropColumn("Recruitment.CandidateLanguage", "CreatedOn");
            DropColumn("Recruitment.CandidateLanguage", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.CandidateLanguage", "UserIdCreatedBy");
            DropColumn("Recruitment.Candidate", "IsDeleted");
            DropColumn("Recruitment.Candidate", "CreatedOn");
            DropColumn("Recruitment.Candidate", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.Candidate", "UserIdCreatedBy");
            DropColumn("Recruitment.ContactRecord", "IsDeleted");
            DropColumn("Recruitment.ContactRecord", "CreatedOn");
            DropColumn("Recruitment.ContactRecord", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.ContactRecord", "UserIdCreatedBy");
        }
    }
}
