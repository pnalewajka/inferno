namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class SimplifiedMileageMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "SimplifiedPrivateVehicleMileage", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "SimplifiedPrivateVehicleMileage");
        }
    }
}
