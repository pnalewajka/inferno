namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AttritionTurnoverAdjustmentMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("PeopleManagement.EmploymentTerminationReason", "IsTurnover", "IsOtherDepartureReasons");
            RenameColumn("PeopleManagement.EmploymentTerminationReason", "IsAttrition", "IsVoluntarilyTurnover");
        }
        
        public override void Down()
        {
            RenameColumn("PeopleManagement.EmploymentTerminationReason", "IsOtherDepartureReasons", "IsTurnover");
            RenameColumn("PeopleManagement.EmploymentTerminationReason", "IsVoluntarilyTurnover", "IsAttrition");
        }
    }
}
