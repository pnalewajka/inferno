namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration060420182348 : DbMigration
    {
        public override void Up()
        {
            //RenameColumn(table: "Recruitment.Candidate", name: "CoordinatorId", newName: "ConsentCoordinatorId");
            //RenameIndex(table: "Recruitment.Candidate", name: "IX_CoordinatorId", newName: "IX_ConsentCoordinatorId");
            //AddColumn("Recruitment.Candidate", "ContactCoordinatorId", c => c.Long(nullable: false));
            //CreateIndex("Recruitment.Candidate", "ContactCoordinatorId");
            //AddForeignKey("Recruitment.Candidate", "ContactCoordinatorId", "Allocation.Employee", "Id");
        }

        public override void Down()
        {
            //DropForeignKey("Recruitment.Candidate", "ContactCoordinatorId", "Allocation.Employee");
            //DropIndex("Recruitment.Candidate", new[] { "ContactCoordinatorId" });
            //DropColumn("Recruitment.Candidate", "ContactCoordinatorId");
            //RenameIndex(table: "Recruitment.Candidate", name: "IX_ConsentCoordinatorId", newName: "IX_CoordinatorId");
            //RenameColumn(table: "Recruitment.Candidate", name: "ConsentCoordinatorId", newName: "CoordinatorId");
        }
    }
}
