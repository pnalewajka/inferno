namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecruitmentProcessRemoveStringLimitsMigration : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AboutClient VARCHAR (2048);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN SpecificDuties VARCHAR(8000);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN ToolsAndEnvironment VARCHAR(1024);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AboutTeam VARCHAR(2048);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN ProjectTimeline VARCHAR(2048);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN Methodology VARCHAR(512);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN RequiredSkills VARCHAR(8000);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AdditionalSkills VARCHAR(4096);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN RecruitmentDetails VARCHAR(2048);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN TravelAndAccomodation VARCHAR(8000);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN RemoteWorkArrangements VARCHAR(2048);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN Comments VARCHAR(3072);" +
                "ALTER TABLE Recruitment.JobOpening  ALTER COLUMN NoticePeriod VARCHAR(1024);" +
                "ALTER TABLE Recruitment.JobOpening  ALTER COLUMN SalaryRate VARCHAR(300);");
        }

        public override void Down()
        {
            Sql("ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AboutClient VARCHAR (100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN SpecificDuties VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN ToolsAndEnvironment VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AboutTeam VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN ProjectTimeline VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN Methodology VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN RequiredSkills VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AdditionalSkills VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN RecruitmentDetails VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN TravelAndAccomodation VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN RemoteWorkArrangements VARCHAR(100);" +
                "ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN Comments VARCHAR(255);" +
                "ALTER TABLE Recruitment.JobOpening  ALTER COLUMN NoticePeriod VARCHAR(50);" +
                "ALTER TABLE Recruitment.JobOpening  ALTER COLUMN SalaryRate VARCHAR(50);");
        }
    }
}
