namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentChangeDataOriginToApplicationOriginMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Recruitment.DataOrigin", newName: "ApplicationOrigin");
            RenameColumn(table: "Recruitment.DataConsent", name: "DataOriginId", newName: "ApplicationOriginId");
            RenameIndex(table: "Recruitment.DataConsent", name: "IX_DataOriginId", newName: "IX_ApplicationOriginId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "Recruitment.DataConsent", name: "IX_ApplicationOriginId", newName: "IX_DataOriginId");
            RenameColumn(table: "Recruitment.DataConsent", name: "ApplicationOriginId", newName: "DataOriginId");
            RenameTable(name: "Recruitment.ApplicationOrigin", newName: "DataOrigin");
        }
    }
}
