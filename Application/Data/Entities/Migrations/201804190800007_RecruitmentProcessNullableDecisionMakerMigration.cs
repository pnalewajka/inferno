namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecruitmentProcessNullableDecisionMakerMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionMakerId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcessStep", "DecisionMakerId");
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionMakerId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.RecruitmentProcessStep", "DecisionMakerId");
        }
    }
}
