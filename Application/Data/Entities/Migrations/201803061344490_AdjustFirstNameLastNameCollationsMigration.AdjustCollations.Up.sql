﻿IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'missing_index_1526_1525_User')
BEGIN
	DROP INDEX [missing_index_1526_1525_User] ON [Accounts].[User]
END
GO

ALTER TABLE [Accounts].[User]
ALTER COLUMN [FirstName] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL 
GO

ALTER TABLE [Accounts].[User]
ALTER COLUMN [LastName] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL 
GO

IF NOT EXISTS(SELECT Name FROM sysindexes WHERE Name = 'IX_IsDeletedFirstNameLastNameLoginEmail') 
BEGIN
CREATE NONCLUSTERED INDEX [IX_IsDeletedFirstNameLastNameLoginEmail] ON [Accounts].[User]
(
	[IsDeleted] ASC
)
INCLUDE ([Id],
	[FirstName],
	[LastName],
	[Login],
	[Email]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
END
GO


ALTER TABLE [Recruitment].[Candidate]
ALTER COLUMN [FirstName] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL 
GO

ALTER TABLE [Recruitment].[Candidate]
ALTER COLUMN [LastName] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL 
GO

ALTER TABLE [Recruitment].[RecommendingPerson]
ALTER COLUMN [FirstName] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL 
GO

ALTER TABLE [Recruitment].[RecommendingPerson]
ALTER COLUMN [LastName] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL 
GO
