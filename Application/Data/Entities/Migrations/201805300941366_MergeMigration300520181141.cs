namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration300520181141 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location");
            //DropIndex("Workflows.OnboardingRequest", new[] { "OnboardingLocationId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalOnboardingLocationId" });
            //AddColumn("Recruitment.Candidate", "OriginalSourceId", c => c.Long(nullable: false));
            //AddColumn("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", c => c.Long());
            //AlterColumn("Workflows.OnboardingRequest", "PlaceOfWork", c => c.Int());
            //AlterColumn("Workflows.OnboardingRequest", "OnboardingDate", c => c.DateTime());
            //AlterColumn("Workflows.OnboardingRequest", "OnboardingLocationId", c => c.Long());
            //CreateIndex("Workflows.OnboardingRequest", "OnboardingLocationId");
            //CreateIndex("Recruitment.Candidate", "OriginalSourceId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId");
            //AddForeignKey("Recruitment.Candidate", "OriginalSourceId", "Recruitment.ApplicationOrigin", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin", "Id");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork", c => c.Int());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate", c => c.DateTime());
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin");
            //DropForeignKey("Recruitment.Candidate", "OriginalSourceId", "Recruitment.ApplicationOrigin");
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalEmploymentSourceId" });
            //DropIndex("Recruitment.Candidate", new[] { "OriginalSourceId" });
            //DropIndex("Workflows.OnboardingRequest", new[] { "OnboardingLocationId" });
            //AlterColumn("Workflows.OnboardingRequest", "OnboardingLocationId", c => c.Long(nullable: false));
            //AlterColumn("Workflows.OnboardingRequest", "OnboardingDate", c => c.DateTime(nullable: false));
            //AlterColumn("Workflows.OnboardingRequest", "PlaceOfWork", c => c.Int(nullable: false));
            //DropColumn("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId");
            //DropColumn("Recruitment.Candidate", "OriginalSourceId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
            //CreateIndex("Workflows.OnboardingRequest", "OnboardingLocationId");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location", "Id");
        }
    }
}
