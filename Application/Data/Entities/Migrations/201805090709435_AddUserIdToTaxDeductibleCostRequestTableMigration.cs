namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserIdToTaxDeductibleCostRequestTableMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.TaxDeductibleCostRequest", "AffectedUserId", c => c.Long(nullable: false));
            CreateIndex("Workflows.TaxDeductibleCostRequest", "AffectedUserId");
            AddForeignKey("Workflows.TaxDeductibleCostRequest", "AffectedUserId", "Accounts.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.TaxDeductibleCostRequest", "AffectedUserId", "Accounts.User");
            DropIndex("Workflows.TaxDeductibleCostRequest", new[] { "AffectedUserId" });
            DropColumn("Workflows.TaxDeductibleCostRequest", "AffectedUserId");
        }
    }
}
