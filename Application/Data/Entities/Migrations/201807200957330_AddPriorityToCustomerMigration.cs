﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AddPriorityToCustomerMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Sales.Customer", "Priority", c => c.Long());
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807200957330_AddPriorityToCustomerMigration.AddPriorityToCustomer.Up.sql");
        }

        public override void Down()
        {
            DropColumn("Sales.Customer", "Priority");
        }
    }
}

