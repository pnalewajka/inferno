namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustTechnicalReviewEntityMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.TechnicalReview", "RecruitmentProcessStepId", c => c.Long(nullable: false));
            AddColumn("Recruitment.TechnicalReview", "SeniorityLevelExpectation", c => c.Int(nullable: false));
            AddColumn("Recruitment.TechnicalReview", "SeniorityLevelAssessment", c => c.Int(nullable: false));
            AddColumn("Recruitment.TechnicalReview", "TechnicalAssignmentAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "SoftwareEngineeringAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "ExperienceAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "EnglishUsageAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "RiskAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "StrengthAssessment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.TechnicalReview", "OtherRemarks", c => c.String(maxLength: 255));
            CreateIndex("Recruitment.TechnicalReview", "RecruitmentProcessStepId");
            AddForeignKey("Recruitment.TechnicalReview", "RecruitmentProcessStepId", "Recruitment.RecruitmentProcessStep", "Id");
            DropColumn("Recruitment.TechnicalReview", "SeniorityLevel");
            DropColumn("Recruitment.TechnicalReview", "Message");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.TechnicalReview", "Message", c => c.String());
            AddColumn("Recruitment.TechnicalReview", "SeniorityLevel", c => c.Int(nullable: false));
            DropForeignKey("Recruitment.TechnicalReview", "RecruitmentProcessStepId", "Recruitment.RecruitmentProcessStep");
            DropIndex("Recruitment.TechnicalReview", new[] { "RecruitmentProcessStepId" });
            DropColumn("Recruitment.TechnicalReview", "OtherRemarks");
            DropColumn("Recruitment.TechnicalReview", "StrengthAssessment");
            DropColumn("Recruitment.TechnicalReview", "RiskAssessment");
            DropColumn("Recruitment.TechnicalReview", "EnglishUsageAssessment");
            DropColumn("Recruitment.TechnicalReview", "ExperienceAssessment");
            DropColumn("Recruitment.TechnicalReview", "SoftwareEngineeringAssessment");
            DropColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment");
            DropColumn("Recruitment.TechnicalReview", "TechnicalAssignmentAssessment");
            DropColumn("Recruitment.TechnicalReview", "SeniorityLevelAssessment");
            DropColumn("Recruitment.TechnicalReview", "SeniorityLevelExpectation");
            DropColumn("Recruitment.TechnicalReview", "RecruitmentProcessStepId");
        }
    }
}
