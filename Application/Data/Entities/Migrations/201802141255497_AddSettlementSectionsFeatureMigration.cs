namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSettlementSectionsFeatureMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.ContractType", "SettlementSectionsFeatures", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("TimeTracking.ContractType", "SettlementSectionsFeatures");
        }
    }
}
