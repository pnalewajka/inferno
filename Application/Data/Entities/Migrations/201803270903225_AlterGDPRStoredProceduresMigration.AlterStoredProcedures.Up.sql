﻿SET XACT_ABORT ON
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [GDPR].[LogCandidateDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()
	DECLARE @userId BIGINT = NULL
	DECLARE @employeeId BIGINT = NULL
	DECLARE @recommendingPersonId BIGINT = NULL
	DECLARE @email NVARCHAR(50) = NULL
	DECLARE @otherEmail NVARCHAR(50) = NULL
	DECLARE @candidateId BIGINT = NULL

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE CandidateId = @identity

	IF @dataOwnerId IS NULL
	BEGIN
		SELECT @email = EmailAddress, @otherEmail = OtherEmailAddress FROM [Recruitment].[Candidate] WHERE Id = @identity
		SELECT @userId = Id FROM [Accounts].[User] WHERE Email IS NOT NULL AND (Email = @email OR Email = @otherEmail)
		SELECT @employeeId = Id FROM [Allocation].[Employee] WHERE Email IS NOT NULL AND (Email = @email OR Email = @otherEmail)
		SELECT @recommendingPersonId = Id FROM [Recruitment].[RecommendingPerson] WHERE EmailAddress = @email OR EmailAddress = @otherEmail	
		
		SELECT @dataOwnerId = Id, @candidateId = CandidateId FROM [GDPR].[DataOwner] 
		WHERE (EmployeeId IS NOT NULL AND EmployeeId = @employeeId) 
			OR (UserId IS NOT NULL AND UserId = @userId)
			OR (RecommendingPersonId IS NOT NULL AND RecommendingPersonId = @recommendingPersonId)

		IF @dataOwnerId IS NULL
		BEGIN
			INSERT INTO [GDPR].[DataOwner]
				(CandidateId,
				EmployeeId,
				UserId,
				RecommendingPersonId,
				ModifiedOn)
			VALUES
				(@identity,
				@employeeId,
				@userId,
				@recommendingPersonId,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			IF @candidateId IS NULL
			BEGIN
				UPDATE [GDPR].[DataOwner] SET CandidateId = @identity WHERE Id = @dataOwnerId
			END
			ELSE
			BEGIN
				INSERT INTO [GDPR].[DataOwner]
					(CandidateId,
					IsPotentialDuplicate,
					ModifiedOn)
				VALUES
					(@identity,
					1,
					@currentDate)

				SET @dataOwnerId = SCOPE_IDENTITY()
			END
		END
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

ALTER PROCEDURE [GDPR].[LogEmployeeDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @userId BIGINT = NULL
	DECLARE @candidateId BIGINT = NULL
	DECLARE @recommendingPersonId BIGINT = NULL
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()
	DECLARE @email NVARCHAR(50)
	DECLARE @employeeId BIGINT = NULL

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE EmployeeId = @identity

	IF @dataOwnerId IS NULL
	BEGIN
		SELECT @userId = UserId, @email = Email FROM [Allocation].[Employee] WHERE Id = @identity

		IF @email IS NOT NULL
		BEGIN
			IF @userId IS NULL
			BEGIN
				SELECT @userId = Id FROM [Accounts].[User] WHERE Email = @email
			END

			SELECT @candidateId = Id FROM [Recruitment].[Candidate] WHERE EmailAddress = @email OR OtherEmailAddress = @email
			SELECT @recommendingPersonId = Id FROM [Recruitment].[RecommendingPerson] WHERE EmailAddress = @email
		END

		SELECT @dataOwnerId = Id, @employeeId = EmployeeId FROM [GDPR].[DataOwner]
			WHERE (UserId IS NOT NULL AND UserId = @userId)
			OR (CandidateId IS NOT NULL AND CandidateId = @candidateId)
			OR (RecommendingPersonId IS NOT NULL AND RecommendingPersonId = @recommendingPersonId)

		IF @dataOwnerId IS NULL
		BEGIN
			INSERT INTO [GDPR].[DataOwner] 
				(EmployeeId,
				UserId,
				CandidateId,
				RecommendingPersonId,
				ModifiedOn) 
			VALUES
				(@identity,
				@userId,
				@candidateId,
				@recommendingPersonId,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			IF @employeeId IS NULL
			BEGIN
				UPDATE [GDPR].[DataOwner] SET EmployeeId = @identity WHERE Id = @dataOwnerId
			END
			ELSE
			BEGIN
				INSERT INTO [GDPR].[DataOwner]
					(EmployeeId,
					IsPotentialDuplicate,
					ModifiedOn)
				VALUES
					(@identity,
					1,
					@currentDate)

				SET @dataOwnerId = SCOPE_IDENTITY()
			END
		END
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

ALTER PROCEDURE [GDPR].[LogRecommendingPersonDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()
	DECLARE @userId BIGINT = NULL
	DECLARE @employeeId BIGINT = NULL
	DECLARE @recommendingPersonId BIGINT = NULL
	DECLARE @email NVARCHAR(50) = NULL
	DECLARE @candidateId BIGINT = NULL

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE RecommendingPersonId = @identity

	IF @dataOwnerId IS NULL
	BEGIN
		SELECT @email = EmailAddress FROM [Recruitment].[RecommendingPerson] WHERE Id = @identity
		
		IF @email IS NOT NULL
		BEGIN
			SELECT @userId = Id FROM [Accounts].[User] WHERE Email = @email
			SELECT @employeeId = Id FROM [Allocation].[Employee] WHERE Email = @email
			SELECT @candidateId = Id FROM [Recruitment].[Candidate] WHERE EmailAddress = @email
		
			SELECT @dataOwnerId = Id, @recommendingPersonId = RecommendingPersonId FROM [GDPR].[DataOwner] 
			WHERE (EmployeeId IS NOT NULL AND EmployeeId = @employeeId) 
				OR (UserId IS NOT NULL AND UserId = @userId)
				OR (CandidateId IS NOT NULL AND CandidateId = @candidateId)
		END

		IF @dataOwnerId IS NULL
		BEGIN
			INSERT INTO [GDPR].[DataOwner]
				(RecommendingPersonId,
				EmployeeId,
				UserId,
				CandidateId,
				ModifiedOn)
			VALUES
				(@identity,
				@employeeId,
				@userId,
				@candidateId,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			IF @recommendingPersonId IS NULL
			BEGIN
				UPDATE [GDPR].[DataOwner] SET RecommendingPersonId = @identity WHERE Id = @dataOwnerId
			END
			ELSE
			BEGIN
				INSERT INTO [GDPR].[DataOwner]
					(RecommendingPersonId,
					IsPotentialDuplicate,
					ModifiedOn)
				VALUES
					(@identity,
					1,
					@currentDate)

				SET @dataOwnerId = SCOPE_IDENTITY()
			END
		END
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

ALTER PROCEDURE [GDPR].[LogUserDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @employeeId BIGINT
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()
	DECLARE @userId BIGINT
	DECLARE @candidateId BIGINT
	DECLARE @recommendingPersonId BIGINT
	DECLARE @email NVARCHAR(50)

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE UserId = @identity

	IF @dataOwnerId IS NULL
	BEGIN

		SELECT @employeeId = e.Id, @email = u.Email FROM [Allocation].[Employee] as e
			RIGHT JOIN [Accounts].[User] as u
			ON e.UserId = u.Id
			WHERE u.Id = @identity

		IF @email IS NOT NULL
		BEGIN
			IF @employeeId IS NULL
			BEGIN
				SELECT @employeeId = Id FROM [Allocation].[Employee] WHERE Email = @email
			END

			SELECT @candidateId = Id FROM [Recruitment].[Candidate] WHERE EmailAddress = @email OR OtherEmailAddress = @email
			SELECT @recommendingPersonId = Id FROM [Recruitment].[RecommendingPerson] WHERE EmailAddress = @email
		END

		SELECT @dataOwnerId = Id, @userId = UserId FROM [GDPR].[DataOwner] WHERE 
			(EmployeeId IS NOT NULL AND EmployeeId = @employeeId)
			OR (CandidateId IS NOT NULL AND CandidateId = @candidateId)
			OR (RecommendingPersonId IS NOT NULL AND RecommendingPersonId = @recommendingPersonId)

		IF @dataOwnerId IS NULL
		BEGIN
			INSERT INTO [GDPR].[DataOwner]
				(EmployeeId,
				UserId,
				RecommendingPersonId,
				CandidateId,
				ModifiedOn) 
			VALUES
				(@employeeId,
				@identity,
				@recommendingPersonId,
				@candidateId,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			IF @userId IS NULL
			BEGIN
				UPDATE [GDPR].[DataOwner] SET UserId = @identity WHERE Id = @dataOwnerId
			END
			ELSE
			BEGIN
				INSERT INTO [GDPR].[DataOwner]
					(UserId,
					IsPotentialDuplicate,
					ModifiedOn)
				VALUES
					(@identity,
					1,
					@currentDate)

				SET @dataOwnerId = SCOPE_IDENTITY()
			END
		END
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO