namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveFeedbackStatusMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("MeetMe.Feedback", "Status");
        }
        
        public override void Down()
        {
            AddColumn("MeetMe.Feedback", "Status", c => c.Int(nullable: false));
        }
    }
}
