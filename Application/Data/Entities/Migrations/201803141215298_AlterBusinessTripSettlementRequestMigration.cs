namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterBusinessTripSettlementRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "HasInvoiceIssued", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "HasInvoiceIssued");
        }
    }
}
