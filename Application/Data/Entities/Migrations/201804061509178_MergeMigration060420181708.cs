namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration060420181708 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Recruitment.InboundEmailValue",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            InboundEmailId = c.Long(nullable: false),
            //            Key = c.Int(nullable: false),
            //            Value = c.String(maxLength: 255),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.InboundEmail", t => t.InboundEmailId)
            //    .Index(t => t.InboundEmailId);
            
            //AddColumn("Recruitment.RecommendingPerson", "IsAnonymized", c => c.Boolean(nullable: false));
            //AddColumn("Recruitment.RecommendingPerson", "CoordinatorId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.TechnicalReview", "VerifiedOn", c => c.DateTime());
            //CreateIndex("Recruitment.RecommendingPerson", "CoordinatorId");
            //AddForeignKey("Recruitment.RecommendingPerson", "CoordinatorId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.InboundEmailValue", "InboundEmailId", "Recruitment.InboundEmail");
            //DropForeignKey("Recruitment.RecommendingPerson", "CoordinatorId", "Allocation.Employee");
            //DropIndex("Recruitment.InboundEmailValue", new[] { "InboundEmailId" });
            //DropIndex("Recruitment.RecommendingPerson", new[] { "CoordinatorId" });
            //AlterColumn("Recruitment.TechnicalReview", "VerifiedOn", c => c.DateTime(nullable: false));
            //DropColumn("Recruitment.RecommendingPerson", "CoordinatorId");
            //DropColumn("Recruitment.RecommendingPerson", "IsAnonymized");
            //DropTable("Recruitment.InboundEmailValue");
        }
    }
}
