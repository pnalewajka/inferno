﻿SET XACT_ABORT ON
GO

DECLARE @DepartureArrival INT = 1;
DECLARE @OwnCosts INT = 2;
DECLARE @CompanyCosts INT = 4;
DECLARE @AdditionalAdvancePayment INT = 8;
DECLARE @PrivateVehicleMileage INT = 16;
DECLARE @HoursWorkedForCustomer INT = 32;
DECLARE @Meals INT = 64;

UPDATE TimeTracking.ContractType SET 
	TimeTrackingCompensationCalculator = NULL,
	BusinessTripCompensationCalculator = NULL,
	SettlementSectionsFeatures = 0

--Employment contract Sales - USA
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 3,
    SettlementSectionsFeatures = 0
WHERE NameEn = 'Employment contract Sales - USA'

--Employment Contract - USA
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 3,
    SettlementSectionsFeatures = 0
WHERE NameEn = 'Employment Contract - USA'

--Employment Contract Production - UK
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 4,
    SettlementSectionsFeatures = @OwnCosts
WHERE NameEn = 'Employment Contract Production - UK'

--Employment Contract Sales - UK
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 4,
    SettlementSectionsFeatures = @OwnCosts
WHERE NameEn = 'Employment Contract Sales - UK'

--Employment contract - UK
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 4,
    SettlementSectionsFeatures = @OwnCosts
WHERE NameEn = 'Employment contract - UK'

--Service contract (A type, ZUS incl.) - Poland
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@PrivateVehicleMileage
WHERE NameEn = 'Service contract (A type, ZUS incl.) - Poland'

--Service contract (A type, ZUS excl.) - Poland
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@PrivateVehicleMileage
WHERE NameEn = 'Service contract (A type, ZUS excl.) - Poland'

--Specified task contract (E type) - Poland
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@PrivateVehicleMileage
WHERE NameEn = 'Specified task contract (E type) - Poland'

--Permanent employment with 50% CDE - Poland
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'Permanent employment with 50% CDE - Poland'

--University required internship - Germany
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 5,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'University required internship - Germany'

--Voluntary internship - Germany
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 5,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'Voluntary internship - Germany'

--Working students - Germany
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 5,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'Working students - Germany'

--Permanent employment - Germany
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 5,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'Permanent employment - Germany'

--Permanent employment - Poland
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = NULL,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'Permanent employment - Poland'

--B2B (E1 type, ex SMT)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 6,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'B2B (E1 type, ex SMT)'

--B2B (E type, ex SMT)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 7,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'B2B (E type, ex SMT)'

--B2B (B1 type, ex SMT)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 5,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'B2B (B1 type, ex SMT)'

--B2B (B type, ex SMT)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 4,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'B2B (B type, ex SMT)'

--B2B (A1 type, ex SMT)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 2,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'B2B (A1 type, ex SMT)'

--B2B (A type, ex SMT)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 1,
    BusinessTripCompensationCalculator = 2,
    SettlementSectionsFeatures = @DepartureArrival|@Meals|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage
WHERE NameEn = 'B2B (A type, ex SMT)'

--B2B (N type)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 1,
    BusinessTripCompensationCalculator = 1,
    SettlementSectionsFeatures = @DepartureArrival|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage|@HoursWorkedForCustomer
WHERE NameEn = 'B2B (N type)'

--External B2B
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 1,
    BusinessTripCompensationCalculator = 1,
    SettlementSectionsFeatures = @DepartureArrival|@OwnCosts|@PrivateVehicleMileage|@HoursWorkedForCustomer
WHERE NameEn = 'External B2B'

--B2B Germany (N type) 
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 3,
    BusinessTripCompensationCalculator = NULL,
    SettlementSectionsFeatures = @OwnCosts|@CompanyCosts|@HoursWorkedForCustomer
WHERE NameEn = 'B2B Germany (N type) '

--B2B (Sales)
UPDATE TimeTracking.ContractType SET
    TimeTrackingCompensationCalculator = 1,
    BusinessTripCompensationCalculator = 1,
    SettlementSectionsFeatures = @DepartureArrival|@OwnCosts|@CompanyCosts|@AdditionalAdvancePayment|@PrivateVehicleMileage|@HoursWorkedForCustomer
WHERE NameEn = 'B2B (Sales)'