namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration260220181449 : DbMigration
    {
        public override void Up()
        {
            //MoveTable(name: "Recruitment.DataConsentDocumentContent", newSchema: "GDPR");
            //MoveTable(name: "Recruitment.DataConsentDocument", newSchema: "GDPR");
            //MoveTable(name: "Recruitment.DataConsent", newSchema: "GDPR");
            //DropForeignKey("Recruitment.DataConsent", "ApplicationOriginId", "Recruitment.ApplicationOrigin");
            //DropForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant");
            //DropIndex("GDPR.DataConsent", new[] { "WhoRecommendedApplicantId" });
            //DropIndex("GDPR.DataConsent", new[] { "ApplicationOriginId" });
            //AddColumn("GDPR.DataConsent", "DataOwnerId", c => c.Long(nullable: false));
            //CreateIndex("GDPR.DataConsent", "DataOwnerId");
            //AddForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id");
            //DropColumn("GDPR.DataConsent", "WhoRecommendedApplicantId");
            //DropColumn("GDPR.DataConsent", "Comment");
            //DropColumn("GDPR.DataConsent", "ApplicationOriginId");
        }
        
        public override void Down()
        {
            //AddColumn("GDPR.DataConsent", "ApplicationOriginId", c => c.Long(nullable: false));
            //AddColumn("GDPR.DataConsent", "Comment", c => c.String());
            //AddColumn("GDPR.DataConsent", "WhoRecommendedApplicantId", c => c.Long(nullable: false));
            //DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            //DropIndex("GDPR.DataConsent", new[] { "DataOwnerId" });
            //DropColumn("GDPR.DataConsent", "DataOwnerId");
            //CreateIndex("GDPR.DataConsent", "ApplicationOriginId");
            //CreateIndex("GDPR.DataConsent", "WhoRecommendedApplicantId");
            //AddForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant", "Id");
            //AddForeignKey("Recruitment.DataConsent", "ApplicationOriginId", "Recruitment.ApplicationOrigin", "Id");
            //MoveTable(name: "GDPR.DataConsent", newSchema: "Recruitment");
            //MoveTable(name: "GDPR.DataConsentDocument", newSchema: "Recruitment");
            //MoveTable(name: "GDPR.DataConsentDocumentContent", newSchema: "Recruitment");
        }
    }
}
