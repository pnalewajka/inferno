// <auto-generated />
namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ExtendInquiryEntityMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ExtendInquiryEntityMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201808090752067_ExtendInquiryEntityMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
