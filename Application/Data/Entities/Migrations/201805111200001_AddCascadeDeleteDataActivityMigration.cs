namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddCascadeDeleteDataActivityMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner");

            AddForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner", "Id", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner");

            AddForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner", "Id");
        }
    }
}
