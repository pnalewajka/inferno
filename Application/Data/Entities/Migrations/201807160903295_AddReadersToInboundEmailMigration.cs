namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReadersToInboundEmailMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Recruitment.InboundEmailReaders",
                c => new
                    {
                        InboundEmailId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.InboundEmailId, t.UserId })
                .ForeignKey("Recruitment.InboundEmail", t => t.InboundEmailId, cascadeDelete: true)
                .ForeignKey("Accounts.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.InboundEmailId)
                .Index(t => t.UserId);
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.InboundEmailReaders", "UserId", "Accounts.User");
            DropForeignKey("Recruitment.InboundEmailReaders", "InboundEmailId", "Recruitment.InboundEmail");
            DropIndex("Recruitment.InboundEmailReaders", new[] { "UserId" });
            DropIndex("Recruitment.InboundEmailReaders", new[] { "InboundEmailId" });
            DropTable("Recruitment.InboundEmailReaders");
        }
    }
}