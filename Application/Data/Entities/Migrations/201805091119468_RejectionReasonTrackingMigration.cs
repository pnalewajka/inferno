namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RejectionReasonTrackingMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.RequestHistoryEntry", "RejectionReason", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.RequestHistoryEntry", "RejectionReason");
        }
    }
}
