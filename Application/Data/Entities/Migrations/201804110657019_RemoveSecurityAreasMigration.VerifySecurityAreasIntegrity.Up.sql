﻿SET XACT_ABORT ON
GO

ALTER TABLE [Accounts].[User] DISABLE TRIGGER [atomic_UserSecurityAreaRoleViolation]
GO

ALTER TABLE [Accounts].[SecurityAreaSecurityRoles] DISABLE TRIGGER [atomic_SecurityAreaSecurityRolesSecurityAreaRoleViolation]
GO

ALTER TABLE [Accounts].[SecurityRoleSecurityProfiles] DISABLE TRIGGER [atomic_SecurityRoleSecurityProfilesSecurityAreaRoleViolation]
GO

ALTER TABLE [Accounts].[UserSecurityProfiles] DISABLE TRIGGER [atomic_UserSecurityProfilesSecurityAreaRoleViolation]
GO

ALTER TABLE [Accounts].[UserSecurityRoles] DISABLE TRIGGER [atomic_UserSecurityRolesSecurityAreaRoleViolation]
GO


DROP TRIGGER [Accounts].[atomic_UserSecurityAreaRoleViolation]
GO

DROP TRIGGER [Accounts].[atomic_SecurityAreaSecurityRolesSecurityAreaRoleViolation]
GO

DROP TRIGGER [Accounts].[atomic_SecurityRoleSecurityProfilesSecurityAreaRoleViolation]
GO

DROP TRIGGER [Accounts].[atomic_UserSecurityProfilesSecurityAreaRoleViolation]
GO

DROP TRIGGER [Accounts].[atomic_UserSecurityRolesSecurityAreaRoleViolation]
GO


DROP PROCEDURE [Accounts].[atomic_VerifySecurityAreasIntegrity]
GO