namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KpiDefinitionTableMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "KPI.KpiDefinition",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 35),
                        Description = c.String(maxLength: 1000),
                        Entity = c.Int(nullable: false),
                        SqlFormula = c.String(maxLength: 4000),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("KPI.KpiDefinition", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("KPI.KpiDefinition", "UserIdImpersonatedBy", "Accounts.User");
            DropIndex("KPI.KpiDefinition", new[] { "UserIdModifiedBy" });
            DropIndex("KPI.KpiDefinition", new[] { "UserIdImpersonatedBy" });
            DropTable("KPI.KpiDefinition");
        }
    }
}
