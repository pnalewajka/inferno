﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class RemoveMeetMeSystemParametersMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807250600553_RemoveMeetMeSystemParametersMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

