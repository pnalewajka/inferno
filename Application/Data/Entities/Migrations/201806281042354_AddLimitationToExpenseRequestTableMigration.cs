namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddLimitationToExpenseRequestTableMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Workflows.ExpenseRequest", "Justification", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("Workflows.ExpenseRequest", "Justification", c => c.String());
        }
    }
}