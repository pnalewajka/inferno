﻿MERGE SkillManagement.JobProfile AS Target
USING (
		SELECT *
		 FROM ( 
		VALUES	('Recruitment', 'Recruitment', GETDATE()),
				('Xamarin', 'Xamarin', GETDATE()),
				('Node.JS', 'Node.JS', GETDATE()))
            AS s ([Name], [Description], [ModifiedOn])
     ) AS Source
ON Target.[Name] = Source.[Name]
WHEN NOT MATCHED THEN
    INSERT ([Name], [Description], [ModifiedOn])
    VALUES (Source.[Name], Source.[Description], Source.[ModifiedOn]);
