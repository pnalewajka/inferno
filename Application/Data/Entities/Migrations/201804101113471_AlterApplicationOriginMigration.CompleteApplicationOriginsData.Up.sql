﻿UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.RecommendationInboundEmailParser' WHERE [Name] = 'Recommendation' AND [OriginType] = 4
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Job Fair / College' AND [OriginType] = 5
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Application Without Notice' AND [OriginType] = 6
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Other' AND [OriginType] = 7
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser', [EmailAddress] = 'job-apps@linkedin.com' WHERE [Name] = 'LinkedIn' AND [OriginType] = 1002
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Goldenline' AND [OriginType] = 1002
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Facebook' AND [OriginType] = 1002
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser', [EmailAddress] = 'cv@aplikacje.pracuj.pl' WHERE [Name] = 'pracuj.pl' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser', [EmailAddress] = 'cvonline@praca.pl' WHERE [Name] = 'praca.pl' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser', [EmailAddress] = ' job-apps@linkedin.com' WHERE [Name] = 'LinkedIn' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser', [EmailAddress] = 'noreply@intive-projects.com' WHERE [Name] = 'intive website' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' , [EmailAddress] = 'gowork-no_reply@gowork.pl' WHERE [Name] = 'Gowork' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser', [EmailAddress] = 'job@crossweb.pl' WHERE [Name] = 'Crossjob' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'infopraca' AND [OriginType] = 1003
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Edge1Solution' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'EUVIC' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'HAYS' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'MAKE MY MIND' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Antal' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Altimi Solutions' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Manpower' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Powermedia' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Transition Technologies' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Geosolution' AND [OriginType] = 1001
UPDATE [Recruitment].[ApplicationOrigin] SET [ParserIdentifier] = 'Recruitment.JobApplicationEmailParser' WHERE [Name] = 'Enginar' AND [OriginType] = 1001
GO
