﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class ChangeRecommendingPersonEntityMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803081259451_ChangeRecommendingPersonEntityMigration.DeleteRecommendingPersonData.Up.sql");
            DropIndex("Recruitment.RecommendingPerson", new[] { "EmailAdress" });
            AddColumn("Recruitment.RecommendingPerson", "EmailAddress", c => c.String(nullable: false, maxLength: 255));
            CreateIndex("Recruitment.RecommendingPerson", "EmailAddress", unique: true);
            DropColumn("Recruitment.RecommendingPerson", "EmailAdress");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.RecommendingPerson", "EmailAdress", c => c.String(nullable: false, maxLength: 255));
            DropIndex("Recruitment.RecommendingPerson", new[] { "EmailAddress" });
            DropColumn("Recruitment.RecommendingPerson", "EmailAddress");
            CreateIndex("Recruitment.RecommendingPerson", "EmailAdress", unique: true);
        }
    }
}

