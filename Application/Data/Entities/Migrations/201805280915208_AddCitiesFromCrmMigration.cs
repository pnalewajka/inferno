﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AddCitiesFromCrmMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805280915208_AddCitiesFromCrmMigration.AddCitiesFromCrmMigration.Up.sql");
        }

        public override void Down()
        {
        }
    }
}

