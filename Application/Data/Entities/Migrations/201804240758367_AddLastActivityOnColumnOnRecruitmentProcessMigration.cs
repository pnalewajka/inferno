namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddLastActivityOnColumnOnRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "LastActivityOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.RecruitmentProcess", "LastActivityOn");
        }
    }
}
