﻿SET XACT_ABORT ON
GO

UPDATE [TimeTracking].[JiraIssueToTimeReportRowMapper]
SET [SourceCode] = 'return issues.Select(i =>
        {
            if (i.Project == null)
            {
                return null;
            }

            return new MyTimeReportRowDto
            {
                ProjectId = i.Project.Id,
                ProjectName = i.Project.ClientProjectName,
                TaskName = i.Fields.Summary,
                ImportedTaskCode = i.Key,
                ImportedTaskType = i.Fields.IssueType != null ? i.Fields.IssueType.Name : null,
                Days =
                    i.Fields.Worklog.Worklogs.GroupBy(wl => wl.StartOrCreationDate)
                        .Select(wl => new MyTimeReportDailyEntryDto
                        {
                            Day = wl.Key,
                            Hours = wl.Sum(wld => wld.TimeSpentHours)
                        }).ToList()
            };
        }).Where(i => i != null).ToList();'
WHERE [Name] = 'Default'

UPDATE [TimeTracking].[JiraIssueToTimeReportRowMapper]
SET [SourceCode] = 'return issues.Select(i =>
        {
            if (i.Project == null)
            {
                return null;
            }

            var project = GetProjectByJiraIdOrDefault((i.Fields != null && i.Fields.CustomField_10893!=null && !string.IsNullOrEmpty(i.Fields.CustomField_10893.Id)) ? i.Fields.CustomField_10893.Id : "10180");

            if (project == null)
            {
                project = i.Project;
            }
            
            return new MyTimeReportRowDto
            {
                ProjectId = project.Id,
                ProjectName = project.ClientProjectName,
                TaskName =  i.Key + ": " + i.Fields.Summary,
                ImportedTaskCode = i.Key,
                ImportedTaskType = i.Fields.IssueType != null ? i.Fields.IssueType.Name : null,
                Days =
                    i.Fields.Worklog.Worklogs.GroupBy(wl => wl.StartOrCreationDate)
                       .Select(wl => new MyTimeReportDailyEntryDto
                        {
                            Day = wl.Key,
                            Hours = wl.Sum(wld => wld.TimeSpentHours)
                        }).ToList()
            };
        }).Where(i => i != null).ToList();'
WHERE [Name] = 'ITProject'