namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AdjustJobOpeningProcessNameMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805071249529_AdjustJobOpeningProcessNameMigration.AdjustCollation.Up.sql");
        }

        public override void Down()
        {
        }
    }
}
