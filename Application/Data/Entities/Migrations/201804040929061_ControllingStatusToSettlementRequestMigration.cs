namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ControllingStatusToSettlementRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "ControllingStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "ControllingStatus");
        }
    }
}
