namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AlterRecruitmentRecommendingPersonTableMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecommendingPerson", "IsAnonymized", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.RecommendingPerson", "CoordinatorId", c => c.Long(nullable: false));

            Sql("UPDATE Recruitment.RecommendingPerson SET CoordinatorId = (SELECT TOP 1 ID FROM Allocation.Employee)");

            CreateIndex("Recruitment.RecommendingPerson", "CoordinatorId");
            AddForeignKey("Recruitment.RecommendingPerson", "CoordinatorId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecommendingPerson", "CoordinatorId", "Allocation.Employee");
            DropIndex("Recruitment.RecommendingPerson", new[] { "CoordinatorId" });
            DropColumn("Recruitment.RecommendingPerson", "CoordinatorId");
            DropColumn("Recruitment.RecommendingPerson", "IsAnonymized");
        }
    }
}
