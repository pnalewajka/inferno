﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class CleanRecruitmentProcessStepMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804200937078_CleanRecruitmentProcessStepMigration.CleanRecruitmentProcessStep.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
