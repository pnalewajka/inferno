﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RemoveSecurityAreasMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804110657019_RemoveSecurityAreasMigration.VerifySecurityAreasIntegrity.Up.sql");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804110657019_RemoveSecurityAreasMigration.AddRoleToProfile.Up.sql");

            DropForeignKey("Accounts.SecurityArea", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Accounts.SecurityArea", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Accounts.SecurityAreaSecurityRoles", "AreaId", "Accounts.SecurityArea");
            DropForeignKey("Accounts.SecurityAreaSecurityRoles", "RoleId", "Accounts.SecurityRole");
            DropForeignKey("Accounts.User", "FK_Accounts.User_Accounts.SecurityArea_SecurityAreaIdSecurityArea");
            DropIndex("Accounts.User", new[] { "SecurityAreaId" });
            DropIndex("Accounts.SecurityArea", new[] { "Code" });
            DropIndex("Accounts.SecurityArea", new[] { "UserIdImpersonatedBy" });
            DropIndex("Accounts.SecurityArea", new[] { "UserIdModifiedBy" });
            DropIndex("Accounts.SecurityAreaSecurityRoles", new[] { "AreaId" });
            DropIndex("Accounts.SecurityAreaSecurityRoles", new[] { "RoleId" });
            DropTable("Accounts.SecurityAreaSecurityRoles");
            DropColumn("Accounts.User", "SecurityAreaId");
            DropTable("Accounts.SecurityArea");
        }

        public override void Down()
        {
            CreateTable(
                "Accounts.SecurityAreaSecurityRoles",
                c => new
                    {
                        AreaId = c.Long(nullable: false),
                        RoleId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.AreaId, t.RoleId });
            
            CreateTable(
                "Accounts.SecurityArea",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Code = c.String(maxLength: 255, unicode: false),
                        Description = c.String(maxLength: 500),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("Accounts.User", "SecurityAreaId", c => c.Long(nullable: false));
            CreateIndex("Accounts.SecurityAreaSecurityRoles", "RoleId");
            CreateIndex("Accounts.SecurityAreaSecurityRoles", "AreaId");
            CreateIndex("Accounts.SecurityArea", "UserIdModifiedBy");
            CreateIndex("Accounts.SecurityArea", "UserIdImpersonatedBy");
            CreateIndex("Accounts.SecurityArea", "Code", unique: true);
            CreateIndex("Accounts.User", "SecurityAreaId");
            AddForeignKey("Accounts.User", "SecurityAreaId", "Accounts.SecurityArea", "Id", name: "FK_Accounts.User_Accounts.SecurityArea_SecurityAreaIdSecurityArea");
            AddForeignKey("Accounts.SecurityAreaSecurityRoles", "RoleId", "Accounts.SecurityRole", "Id", cascadeDelete: true);
            AddForeignKey("Accounts.SecurityAreaSecurityRoles", "AreaId", "Accounts.SecurityArea", "Id", cascadeDelete: true);
            AddForeignKey("Accounts.SecurityArea", "UserIdModifiedBy", "Accounts.User", "Id");
            AddForeignKey("Accounts.SecurityArea", "UserIdImpersonatedBy", "Accounts.User", "Id");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804110657019_RemoveSecurityAreasMigration.AddRoleToProfile.Down.sql");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804110657019_RemoveSecurityAreasMigration.VerifySecurityAreasIntegrity.Down.sql");
        }
    }
}


