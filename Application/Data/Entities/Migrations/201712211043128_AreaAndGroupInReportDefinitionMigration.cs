namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AreaAndGroupInReportDefinitionMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Reporting.ReportDefinition", "Area", c => c.String());
            AddColumn("Reporting.ReportDefinition", "Group", c => c.String());
            DropColumn("Reporting.ReportDefinition", "PathEn");
            DropColumn("Reporting.ReportDefinition", "PathPl");
        }
        
        public override void Down()
        {
            AddColumn("Reporting.ReportDefinition", "PathPl", c => c.String());
            AddColumn("Reporting.ReportDefinition", "PathEn", c => c.String());
            DropColumn("Reporting.ReportDefinition", "Group");
            DropColumn("Reporting.ReportDefinition", "Area");
        }
    }
}
