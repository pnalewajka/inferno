namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AdjustCollationOnPositionNameInJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE Recruitment.JobOpening ALTER COLUMN PositionName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AI NULL");
        }
        
        public override void Down()
        {
        }
    }
}
