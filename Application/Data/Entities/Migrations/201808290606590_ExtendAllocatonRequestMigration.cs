namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ExtendAllocatonRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Allocation.AllocationRequest", "ChangeDemand", c => c.Int(nullable: false));
            AddColumn("Allocation.AllocationRequest", "Comment", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("Allocation.AllocationRequest", "Comment");
            DropColumn("Allocation.AllocationRequest", "ChangeDemand");
        }
    }
}
