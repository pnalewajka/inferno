namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixEmailAddressFieldsOnCandidateMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Recruitment.Candidate", name: "EmailAddreess", newName: "EmailAddress");
            RenameColumn(table: "Recruitment.Candidate", name: "EmailAddreess2", newName: "EmailAddress2");
            RenameIndex(table: "Recruitment.Candidate", name: "IX_EmailAddreess", newName: "IX_EmailAddress");
        }
        
        public override void Down()
        {
            RenameColumn(table: "Recruitment.Candidate", name: "EmailAddress", newName: "EmailAddreess");
            RenameColumn(table: "Recruitment.Candidate", name: "EmailAddress2", newName: "EmailAddreess2");
            RenameIndex(table: "Recruitment.Candidate", name: "IX_EmailAddress", newName: "IX_EmailAddreess");
        }
    }
}
