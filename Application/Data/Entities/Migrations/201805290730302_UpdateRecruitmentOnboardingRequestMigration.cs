namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRecruitmentOnboardingRequestMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location");
            DropIndex("Workflows.OnboardingRequest", new[] { "OnboardingLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalOnboardingLocationId" });
            AlterColumn("Workflows.OnboardingRequest", "PlaceOfWork", c => c.Int());
            AlterColumn("Workflows.OnboardingRequest", "OnboardingDate", c => c.DateTime());
            AlterColumn("Workflows.OnboardingRequest", "OnboardingLocationId", c => c.Long());
            CreateIndex("Workflows.OnboardingRequest", "OnboardingLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate");
            DropColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork");
            DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork", c => c.Int());
            AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate", c => c.DateTime());
            DropIndex("Workflows.OnboardingRequest", new[] { "OnboardingLocationId" });
            AlterColumn("Workflows.OnboardingRequest", "OnboardingLocationId", c => c.Long(nullable: false));
            AlterColumn("Workflows.OnboardingRequest", "OnboardingDate", c => c.DateTime(nullable: false));
            AlterColumn("Workflows.OnboardingRequest", "PlaceOfWork", c => c.Int(nullable: false));
            CreateIndex("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
            CreateIndex("Workflows.OnboardingRequest", "OnboardingLocationId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location", "Id");
        }
    }
}
