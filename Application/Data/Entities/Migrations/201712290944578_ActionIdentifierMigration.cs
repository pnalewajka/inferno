﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class ActionIdentifierMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201712290944578_ActionIdentifierMigration.Script.Up.sql");
        }

        public override void Down()
        {
            AlterColumn("Workflows.RequestHistoryEntry", "JiraIssueKey", c => c.String(maxLength: 256));
            DropColumn("Workflows.RequestHistoryEntry", "IsActionActive");
            DropColumn("Workflows.RequestHistoryEntry", "ActionIdentifier");
        }
    }
}
