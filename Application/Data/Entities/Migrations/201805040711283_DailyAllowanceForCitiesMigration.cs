﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class DailyAllowanceForCitiesMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805040711283_DailyAllowanceForCitiesMigration.UpdateLumpSum.Up.sql");

            RenameColumn("BusinessTrips.DailyAllowance", "AccommodationLimit", "AccommodationLumpSum");
            DropIndex("BusinessTrips.DailyAllowance", new[] { "TravelCountryId" });
            AddColumn("BusinessTrips.DailyAllowance", "TravelCityId", c => c.Long());
            AlterColumn("BusinessTrips.DailyAllowance", "TravelCountryId", c => c.Long());
            CreateIndex("BusinessTrips.DailyAllowance", "TravelCityId");
            CreateIndex("BusinessTrips.DailyAllowance", "TravelCountryId");
            AddForeignKey("BusinessTrips.DailyAllowance", "TravelCityId", "Dictionaries.City", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("BusinessTrips.DailyAllowance", "TravelCityId", "Dictionaries.City");
            DropIndex("BusinessTrips.DailyAllowance", new[] { "TravelCountryId" });
            DropIndex("BusinessTrips.DailyAllowance", new[] { "TravelCityId" });
            AlterColumn("BusinessTrips.DailyAllowance", "TravelCountryId", c => c.Long(nullable: false));
            DropColumn("BusinessTrips.DailyAllowance", "TravelCityId");
            CreateIndex("BusinessTrips.DailyAllowance", "TravelCountryId");
            RenameColumn("BusinessTrips.DailyAllowance", "AccommodationLumpSum", "AccommodationLimit");
        }
    }
}
