﻿ALTER TABLE Recruitment.CandidateNote  ALTER COLUMN Content NVARCHAR (4000);
ALTER TABLE Recruitment.JobOpeningNote  ALTER COLUMN Content NVARCHAR (4000);
ALTER TABLE Recruitment.RecruitmentProcessNote  ALTER COLUMN Content NVARCHAR (4000);