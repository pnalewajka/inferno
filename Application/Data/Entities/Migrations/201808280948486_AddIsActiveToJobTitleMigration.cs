namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsActiveToJobTitleMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("SkillManagement.JobTitle", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("SkillManagement.JobTitle", "IsActive");
        }
    }
}