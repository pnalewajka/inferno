namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AbsenceRequestCommentMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Workflows.AbsenceRequest", "Comment", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("Workflows.AbsenceRequest", "Comment", c => c.String());
        }
    }
}
