﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AdjustSecurityVerificationProcedureMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803070007489_AdjustSecurityVerificationProcedureMigration.AdjustStoredProcedure.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

