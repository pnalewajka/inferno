namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AdjustTechnicalReviewsMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.TechnicalReview", "VerifiedByEmployeeId", "Allocation.Employee");
            DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            DropColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            DropColumn("Recruitment.TechnicalReview", "VerifiedOn");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.TechnicalReview", "VerifiedOn", c => c.DateTime());
            AddColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long());
            CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            AddForeignKey("Recruitment.TechnicalReview", "VerifiedByEmployeeId", "Allocation.Employee", "Id");
        }
    }
}
