﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class CountryInLocationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803221243484_CountryInLocationMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
            DropForeignKey("Dictionaries.Location", "CountryId", "Dictionaries.Country");
            DropIndex("Dictionaries.Location", new[] { "CountryId" });
            DropColumn("Dictionaries.Location", "CountryId");
        }
    }
}
