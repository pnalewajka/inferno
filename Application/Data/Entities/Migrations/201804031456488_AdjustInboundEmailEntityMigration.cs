namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AdjustInboundEmailEntityMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Recruitment.InboundEmail", "TriageResult", "Category");
        }
        
        public override void Down()
        {
            RenameColumn("Recruitment.InboundEmail", "Category", "TriageResult");
        }
    }
}
