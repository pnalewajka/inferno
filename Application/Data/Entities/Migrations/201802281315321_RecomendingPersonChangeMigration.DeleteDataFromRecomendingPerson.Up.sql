﻿SET XACT_ABORT ON
GO

DELETE FROM [GDPR].[DataActivity]
DELETE FROM [GDPR].[DataOwner]
DELETE FROM [Recruitment].[RecommendingPerson]