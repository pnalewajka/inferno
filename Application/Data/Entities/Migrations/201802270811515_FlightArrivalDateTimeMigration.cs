namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FlightArrivalDateTimeMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "SettledOn");
            AddColumn("Workflows.BusinessTripSettlementRequest", "SettlementDate", c => c.DateTime());
            AddColumn("Workflows.BusinessTripSettlementRequest", "FlightArrivalDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "FlightArrivalDateTime");
            DropColumn("Workflows.BusinessTripSettlementRequest", "SettlementDate");
            AddColumn("Workflows.BusinessTripSettlementRequest", "SettledOn", c => c.DateTime(nullable: false));
        }
    }
}
