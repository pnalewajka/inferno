﻿INSERT INTO [Workflows].[BusinessTripSettlementRequestAbroadTimeEntry]
           ([BusinessTripSettlementRequestId]
           ,[DepartureDateTime]
           ,[DepartureCountryId])
     SELECT
            [Id]
           ,[FlightArrivalDateTime]
           ,(    SELECT COALESCE([c].[CountryId], [l].[CountryId])
                   FROM [BusinessTrips].[BusinessTripParticipant] [p]
                   JOIN [Allocation].[Employee] [e] ON [e].[Id] = [p].[EmployeeId]
                   JOIN [Dictionaries].[Location] [l] ON [l].[Id] = [e].[LocationId]
              LEFT JOIN [Dictionaries].[City] [c] ON [c].[Id] = [l].[CityId]
			      WHERE [p].[Id] = [s].[BusinessTripParticipantId])
       FROM [Workflows].[BusinessTripSettlementRequest] [s]
      WHERE [FlightArrivalDateTime] IS NOT NULL
