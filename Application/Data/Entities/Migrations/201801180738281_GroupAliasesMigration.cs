namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class GroupAliasesMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Allocation.ActiveDirectoryGroup", "Aliases", c => c.String(maxLength: 1023));
        }
        
        public override void Down()
        {
            DropColumn("Allocation.ActiveDirectoryGroup", "Aliases");
        }
    }
}
