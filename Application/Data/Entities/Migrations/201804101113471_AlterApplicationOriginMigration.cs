namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AlterApplicationOriginMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.ApplicationOrigin", "ParserIdentifier", c => c.String(maxLength: 255));
            AddColumn("Recruitment.ApplicationOrigin", "EmailAddress", c => c.String(maxLength: 255));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804101113471_AlterApplicationOriginMigration.CompleteApplicationOriginsData.Up.sql");
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.ApplicationOrigin", "EmailAddress");
            DropColumn("Recruitment.ApplicationOrigin", "ParserIdentifier");
        }
    }
}
