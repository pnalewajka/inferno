namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentProcessPlaceOfWorkNullableMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork", c => c.Int());
        }
        
        public override void Down()
        {        
            AlterColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork", c => c.Int(nullable: false));
        }
    }
}
