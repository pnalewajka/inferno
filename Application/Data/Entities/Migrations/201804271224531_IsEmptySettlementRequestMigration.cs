namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsEmptySettlementRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "IsEmpty", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "IsEmpty");
        }
    }
}
