﻿ALTER PROCEDURE [Runtime].[atomic_PromoteTemporaryDocumentToTuple]
	@CurrentUserId BIGINT,
	@TargetMetaTableSchema NVARCHAR(128),
	@TargetMetaTableName NVARCHAR(128),
	@TargetMetaTableId BIGINT,
	@TargetDataTableSchema NVARCHAR(128),
	@TargetDataTableName NVARCHAR(128),
	@TargetDataTableId BIGINT,
	@DataColumnName NVARCHAR(128),
	@MimeTypeColumnName NVARCHAR(128),
	@ContentLengthColumnName NVARCHAR(128),
	@NameColumnName NVARCHAR(128),
	@TemporaryDocumentIdentifier UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @documentContents VARBINARY(MAX) = 0x

	DECLARE @tempData VARBINARY(MAX)

	DECLARE Contents CURSOR FOR 
	SELECT 
		tfp.Data
	FROM
		[Runtime].[TemporaryDocumentPart] tfp
		JOIN [Runtime].[TemporaryDocument] tf ON tfp.TemporaryDocumentId = tf.Id
	WHERE 
		tf.Identifier = @TemporaryDocumentIdentifier
	ORDER BY tfp.Id

	OPEN Contents
	FETCH NEXT FROM Contents INTO @tempData

	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @documentContents = @documentContents + @tempData
		FETCH NEXT FROM Contents INTO @tempData
	END

	CLOSE Contents
	DEALLOCATE Contents

	BEGIN TRANSACTION

	IF NOT EXISTS (
		SELECT TOP 1 Id from [Runtime].[TemporaryDocument] 
		WHERE UserIdModifiedBy = @CurrentUserId AND Identifier = @TemporaryDocumentIdentifier
	)
	THROW 51000, 'The record does not exist.', 1;  


	DECLARE @params NVARCHAR(MAX) = N'@targetDocumentId BIGINT, @temporaryDocumentIdentifier uniqueidentifier'
	DECLARE @sql NVARCHAR(MAX) = N'
	UPDATE ['+ @TargetMetaTableSchema+'].['+@TargetMetaTableName+N']
		SET 
			['+@NameColumnName+'] = source.Name,
			['+@MimeTypeColumnName+'] = source.ContentType,
			['+@ContentLengthColumnName+'] = source.ContentLength
	FROM
		['+ @TargetMetaTableSchema+'].['+@TargetMetaTableName+N'] target
		JOIN [Runtime].[TemporaryDocument] source ON source.Identifier = @temporaryDocumentIdentifier
	WHERE 
		target.ID = @targetDocumentId
	'

	EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId, @temporaryDocumentIdentifier = @TemporaryDocumentIdentifier

	IF(@TargetDataTableId IS NULL)
	BEGIN
		--insert, using PK from meta
		SET @params = N'@targetDocumentId BIGINT, @documentContents VARBINARY(MAX)'
		--update using provided id
		SET @sql = N'
		INSERT INTO ['+ @TargetDataTableSchema+'].['+@TargetDataTableName+N']
		(ID, ['+@DataColumnName+'])
		VALUES
		(
			@targetDocumentId,
			@documentContents
		)
		'
		EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId, @documentContents = @documentContents
	END
	ELSE
	BEGIN
		SET @params = N'@targetDocumentId BIGINT, @documentContents VARBINARY(MAX)'
		--update using provided id
		SET @sql = N'
		UPDATE ['+ @TargetDataTableSchema+'].['+@TargetDataTableName+N']
			SET 
				['+@DataColumnName+'] = @documentContents
		WHERE 
			ID = @targetDocumentId
		'
		EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetDataTableId, @documentContents = @documentContents
	END

	COMMIT
END
