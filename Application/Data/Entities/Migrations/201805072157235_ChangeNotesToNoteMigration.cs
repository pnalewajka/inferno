namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ChangeNotesToNoteMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Recruitment.CandidateNotes", newName: "CandidateNote");
            RenameTable(name: "Recruitment.RecommendingPersonNotes", newName: "RecommendingPersonNote");
            RenameTable(name: "Recruitment.JobOpeningNotes", newName: "JobOpeningNote");
            RenameTable(name: "Recruitment.RecruitmentProcessNotes", newName: "RecruitmentProcessNote");
        }
        
        public override void Down()
        {
            RenameTable(name: "Recruitment.RecruitmentProcessNote", newName: "RecruitmentProcessNotes");
            RenameTable(name: "Recruitment.JobOpeningNote", newName: "JobOpeningNotes");
            RenameTable(name: "Recruitment.RecommendingPersonNote", newName: "RecommendingPersonNotes");
            RenameTable(name: "Recruitment.CandidateNote", newName: "CandidateNotes");
        }
    }
}
