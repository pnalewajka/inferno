namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddOriginSenarioToFeedbackRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.FeedbackRequest", "OriginScenario", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.FeedbackRequest", "OriginScenario");
        }
    }
}
