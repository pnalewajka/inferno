// <auto-generated />
namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CompanyAdvancedPaymentConfigurationMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CompanyAdvancedPaymentConfigurationMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201803021234117_CompanyAdvancedPaymentConfigurationMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
