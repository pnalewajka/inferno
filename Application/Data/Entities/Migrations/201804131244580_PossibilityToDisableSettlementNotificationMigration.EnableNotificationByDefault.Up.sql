﻿SET XACT_ABORT ON
GO

UPDATE TimeTracking.ContractType SET NotifyMissingBusinessTripSettlement = 1
