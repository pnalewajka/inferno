﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RefactorRecruitmentProcessMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType");
            DropForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalEmploymentSourceId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalPositionId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalTrialContractTypeId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLongTermContractTypeId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferPositionId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferLocationId" });

            CreateTable(
                "Recruitment.RecruitmentProcessFinal",
                c => new
                {
                    Id = c.Long(nullable: false),
                    FinalPositionId = c.Long(),
                    FinalTrialContractTypeId = c.Long(),
                    FinalLongTermContractTypeId = c.Long(),
                    FinalTrialSalary = c.String(maxLength: 50),
                    FinalLongTermSalary = c.String(maxLength: 50),
                    FinalStartDate = c.DateTime(),
                    FinalSignedDate = c.DateTime(),
                    FinalLocationId = c.Long(),
                    FinalComment = c.String(maxLength: 255),
                    FinalEmploymentSourceId = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.ApplicationOrigin", t => t.FinalEmploymentSourceId)
                .ForeignKey("Dictionaries.Location", t => t.FinalLocationId)
                .ForeignKey("TimeTracking.ContractType", t => t.FinalLongTermContractTypeId)
                .ForeignKey("SkillManagement.JobTitle", t => t.FinalPositionId)
                .ForeignKey("TimeTracking.ContractType", t => t.FinalTrialContractTypeId)
                .ForeignKey("Recruitment.RecruitmentProcess", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.FinalPositionId)
                .Index(t => t.FinalTrialContractTypeId)
                .Index(t => t.FinalLongTermContractTypeId)
                .Index(t => t.FinalLocationId)
                .Index(t => t.FinalEmploymentSourceId);

            CreateTable(
                "Recruitment.RecruitmentProcessOffer",
                c => new
                {
                    Id = c.Long(nullable: false),
                    OfferPositionId = c.Long(),
                    OfferContractType = c.String(maxLength: 50),
                    OfferSalary = c.String(maxLength: 150),
                    OfferStartDate = c.String(maxLength: 130),
                    OfferLocationId = c.Long(),
                    OfferComment = c.String(maxLength: 500),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.Location", t => t.OfferLocationId)
                .ForeignKey("SkillManagement.JobTitle", t => t.OfferPositionId)
                .ForeignKey("Recruitment.RecruitmentProcess", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.OfferPositionId)
                .Index(t => t.OfferLocationId);

            CreateTable(
                "Recruitment.RecruitmentProcessScreening",
                c => new
                {
                    Id = c.Long(nullable: false),
                    ScreeningGeneralOpinion = c.String(maxLength: 500),
                    ScreeningMotivation = c.String(maxLength: 500),
                    ScreeningWorkplaceExpectations = c.String(maxLength: 500),
                    ScreeningSkills = c.String(maxLength: 500),
                    ScreeningLanguageEnglish = c.String(maxLength: 300),
                    ScreeningLanguageGerman = c.String(maxLength: 250),
                    ScreeningLanguageOther = c.String(maxLength: 250),
                    ScreeningContractExpectations = c.String(maxLength: 500),
                    ScreeningAvailability = c.String(maxLength: 400),
                    ScreeningOtherProcesses = c.String(maxLength: 400),
                    ScreeningCounterOfferCriteria = c.String(maxLength: 400),
                    ScreeningRelocationCanDo = c.Boolean(),
                    ScreeningRelocationComment = c.String(maxLength: 400),
                    ScreeningBusinessTripsCanDo = c.Boolean(),
                    ScreeningBusinessTripsComment = c.String(maxLength: 250),
                    ScreeningEveningWorkCanDo = c.Boolean(),
                    ScreeningEveningWorkComment = c.String(maxLength: 500),
                    ScreeningWorkPermitNeeded = c.Boolean(),
                    ScreeningWorkPermitComment = c.String(maxLength: 250),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.RecruitmentProcess", t => t.Id)
                .Index(t => t.Id);

            AddColumn("Recruitment.RecruitmentProcess", "OfferId", c => c.Long(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningId", c => c.Long(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "FinalId", c => c.Long(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806280729369_RefactorRecruitmentProcessMigration.RecruitmentProcessRefactorMigratin.Up.sql");
            DropColumn("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalPositionId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary");
            DropColumn("Recruitment.RecruitmentProcess", "FinalStartDate");
            DropColumn("Recruitment.RecruitmentProcess", "FinalSignedDate");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalComment");
            DropColumn("Recruitment.RecruitmentProcess", "OfferPositionId");
            DropColumn("Recruitment.RecruitmentProcess", "OfferContractType");
            DropColumn("Recruitment.RecruitmentProcess", "OfferSalary");
            DropColumn("Recruitment.RecruitmentProcess", "OfferStartDate");
            DropColumn("Recruitment.RecruitmentProcess", "OfferLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "OfferComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningGeneralOpinion");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningMotivation");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkplaceExpectations");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningSkills");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageEnglish");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageGerman");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageOther");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningContractExpectations");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningAvailability");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningOtherProcesses");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningCounterOfferCriteria");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitNeeded");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitComment");
        }

        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806280729369_RefactorRecruitmentProcessMigration.RecruitmentProcessRefactorMigratin.Down.sql");
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitComment", c => c.String(maxLength: 250));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitNeeded", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkComment", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsComment", c => c.String(maxLength: 250));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationComment", c => c.String(maxLength: 400));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningCounterOfferCriteria", c => c.String(maxLength: 400));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningOtherProcesses", c => c.String(maxLength: 400));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningAvailability", c => c.String(maxLength: 400));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningContractExpectations", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageOther", c => c.String(maxLength: 250));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageGerman", c => c.String(maxLength: 250));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageEnglish", c => c.String(maxLength: 300));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningSkills", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkplaceExpectations", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningMotivation", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningGeneralOpinion", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "OfferComment", c => c.String(maxLength: 500));
            AddColumn("Recruitment.RecruitmentProcess", "OfferLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "OfferStartDate", c => c.String(maxLength: 130));
            AddColumn("Recruitment.RecruitmentProcess", "OfferSalary", c => c.String(maxLength: 150));
            AddColumn("Recruitment.RecruitmentProcess", "OfferContractType", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "OfferPositionId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalComment", c => c.String(maxLength: 255));
            AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalSignedDate", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalStartDate", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalPositionId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", c => c.Long());
            DropForeignKey("Recruitment.RecruitmentProcessScreening", "Id", "Recruitment.RecruitmentProcess");
            DropForeignKey("Recruitment.RecruitmentProcessOffer", "Id", "Recruitment.RecruitmentProcess");
            DropForeignKey("Recruitment.RecruitmentProcessOffer", "OfferPositionId", "SkillManagement.JobTitle");
            DropForeignKey("Recruitment.RecruitmentProcessOffer", "OfferLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcessFinal", "Id", "Recruitment.RecruitmentProcess");
            DropForeignKey("Recruitment.RecruitmentProcessFinal", "FinalTrialContractTypeId", "TimeTracking.ContractType");
            DropForeignKey("Recruitment.RecruitmentProcessFinal", "FinalPositionId", "SkillManagement.JobTitle");
            DropForeignKey("Recruitment.RecruitmentProcessFinal", "FinalLongTermContractTypeId", "TimeTracking.ContractType");
            DropForeignKey("Recruitment.RecruitmentProcessFinal", "FinalLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcessFinal", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin");
            DropIndex("Recruitment.RecruitmentProcessScreening", new[] { "Id" });
            DropIndex("Recruitment.RecruitmentProcessOffer", new[] { "OfferLocationId" });
            DropIndex("Recruitment.RecruitmentProcessOffer", new[] { "OfferPositionId" });
            DropIndex("Recruitment.RecruitmentProcessOffer", new[] { "Id" });
            DropIndex("Recruitment.RecruitmentProcessFinal", new[] { "FinalEmploymentSourceId" });
            DropIndex("Recruitment.RecruitmentProcessFinal", new[] { "FinalLocationId" });
            DropIndex("Recruitment.RecruitmentProcessFinal", new[] { "FinalLongTermContractTypeId" });
            DropIndex("Recruitment.RecruitmentProcessFinal", new[] { "FinalTrialContractTypeId" });
            DropIndex("Recruitment.RecruitmentProcessFinal", new[] { "FinalPositionId" });
            DropIndex("Recruitment.RecruitmentProcessFinal", new[] { "Id" });
            DropColumn("Recruitment.RecruitmentProcess", "FinalId");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningId");
            DropColumn("Recruitment.RecruitmentProcess", "OfferId");
            DropTable("Recruitment.RecruitmentProcessScreening");
            DropTable("Recruitment.RecruitmentProcessOffer");
            DropTable("Recruitment.RecruitmentProcessFinal");
            CreateIndex("Recruitment.RecruitmentProcess", "OfferLocationId");
            CreateIndex("Recruitment.RecruitmentProcess", "OfferPositionId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalPositionId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId");
            AddForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin", "Id");
        }
    }
}

