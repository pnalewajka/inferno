﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AddPriorityToLanguageMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Dictionaries.Language", "Priority", c => c.Int(nullable: true));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807200740428_AddPriorityToLanguageMigration.AddPrioritToLanguge.Up.sql");
        }

        public override void Down()
        {
            DropColumn("Dictionaries.Language", "Priority");
        }
    }
}

