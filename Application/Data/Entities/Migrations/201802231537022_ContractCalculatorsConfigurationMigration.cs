﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class ContractCalculatorsConfigurationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802231537022_ContractCalculatorsConfigurationMigration.ContractCalculatorsConfiguration.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802231537022_ContractCalculatorsConfigurationMigration.ContractCalculatorsConfiguration.Down.sql");
        }
    }
}

