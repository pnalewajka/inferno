﻿declare @plCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'PLN')
declare @usdCurrency bigint = (SELECT TOP 1 Id FROM [BusinessTrips].[Currency] where IsoCode = 'USD')

UPDATE [Allocation].[Project]
SET
      [ReinvoicingToCustomer] = 0
      ,[ReinvoiceCurrencyId] = @plCurrency
      ,[CostApprovalPolicy] = 1
      ,[MaxCostsCurrencyId] = @plCurrency
      ,[MaxHotelCosts] = null
      ,[MaxTotalFlightsCosts] = null
      ,[MaxOtherTravelCosts] = null
  WHERE Region <> 'USA' or Region is null


UPDATE [Allocation].[Project]
SET
      [ReinvoicingToCustomer] = 0
      ,[ReinvoiceCurrencyId] = @usdCurrency
      ,[CostApprovalPolicy] = 1
      ,[MaxCostsCurrencyId] = @plCurrency
      ,[MaxHotelCosts] = null
      ,[MaxTotalFlightsCosts] = null
      ,[MaxOtherTravelCosts] = null
  WHERE Region = 'USA' 