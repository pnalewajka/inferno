namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddCrmIdsMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "CrmId", c => c.Guid());
            CreateIndex("Recruitment.RecruitmentProcess", "CrmId");
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.RecruitmentProcess", new[] { "CrmId" });
            DropColumn("Recruitment.RecruitmentProcess", "CrmId");
        }
    }
}
