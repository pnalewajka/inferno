namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecruitmentProcessNullableLocationMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            AlterColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            AlterColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
        }
    }
}
