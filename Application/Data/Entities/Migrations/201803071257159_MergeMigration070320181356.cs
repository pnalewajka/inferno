namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration070320181356 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("Recruitment.JobOpening", new[] { "AcceptingEmployeeId" });
            //AddColumn("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId", c => c.Long());
            //AlterColumn("Recruitment.JobOpening", "AcceptingEmployeeId", c => c.Long());
            //CreateIndex("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId");
            //CreateIndex("Recruitment.JobOpening", "AcceptingEmployeeId");
            //AddForeignKey("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId", "Allocation.Employee");
            //DropIndex("Recruitment.JobOpening", new[] { "AcceptingEmployeeId" });
            //DropIndex("Dictionaries.Company", new[] { "AdvancePaymentAcceptingEmployeeId" });
            //AlterColumn("Recruitment.JobOpening", "AcceptingEmployeeId", c => c.Long(nullable: false));
            //DropColumn("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId");
            //CreateIndex("Recruitment.JobOpening", "AcceptingEmployeeId");
        }
    }
}
