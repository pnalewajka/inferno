namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentNotesLimitlessMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.CandidateNote", "Content", c => c.String());
            AlterColumn("Recruitment.RecommendingPersonNote", "Content", c => c.String());
            AlterColumn("Recruitment.JobOpeningNote", "Content", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessNote", "Content", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.RecruitmentProcessNote", "Content", c => c.String(maxLength: 4000));
            AlterColumn("Recruitment.JobOpeningNote", "Content", c => c.String(maxLength: 4000));
            AlterColumn("Recruitment.RecommendingPersonNote", "Content", c => c.String(maxLength: 4000));
            AlterColumn("Recruitment.CandidateNote", "Content", c => c.String(maxLength: 4000));
        }
    }
}
