namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration210220181435 : DbMigration
    {
        public override void Up()
        {
            //RenameTable(name: "Recruitment.DataOrigin", newName: "ApplicationOrigin");
            //RenameColumn(table: "Recruitment.DataConsent", name: "DataOriginId", newName: "ApplicationOriginId");
            //RenameIndex(table: "Recruitment.DataConsent", name: "IX_DataOriginId", newName: "IX_ApplicationOriginId");
            //AddColumn("Recruitment.ApplicationOrigin", "OriginType", c => c.Int(nullable: false));
            //DropColumn("Recruitment.ApplicationOrigin", "IsRecommendingPersonRequired");
            //DropColumn("Recruitment.ApplicationOrigin", "IsCommentRequired");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.ApplicationOrigin", "IsCommentRequired", c => c.Boolean(nullable: false));
            //AddColumn("Recruitment.ApplicationOrigin", "IsRecommendingPersonRequired", c => c.Boolean(nullable: false));
            //DropColumn("Recruitment.ApplicationOrigin", "OriginType");
            //RenameIndex(table: "Recruitment.DataConsent", name: "IX_ApplicationOriginId", newName: "IX_DataOriginId");
            //RenameColumn(table: "Recruitment.DataConsent", name: "ApplicationOriginId", newName: "DataOriginId");
            //RenameTable(name: "Recruitment.ApplicationOrigin", newName: "DataOrigin");
        }
    }
}
