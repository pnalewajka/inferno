﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;
    
    public partial class AssetsV2Migration : AtomicDbMigration
    {
        public override void Up()
        {
            DropForeignKey("Workflows.AssetsRequest", "ProjectId", "Allocation.Project");
            DropIndex("Workflows.AssetsRequest", new[] { "ProjectId" });
            AddColumn("Workflows.AssetsRequest", "AssetTypeHardwareSetComment", c => c.String(maxLength: 1024));
            AddColumn("Workflows.AssetsRequest", "AssetTypeHeadsetComment", c => c.String(maxLength: 1024));
            AddColumn("Workflows.AssetsRequest", "AssetTypeBackpackComment", c => c.String(maxLength: 1024));
            AddColumn("Workflows.AssetsRequest", "AssetTypeOptionalHardwareComment", c => c.String(maxLength: 1024));
            AddColumn("Workflows.AssetsRequest", "CommentForIT", c => c.String(maxLength: 1024));
            DropColumn("Workflows.AssetsRequest", "ProjectId");
            DropColumn("Workflows.AssetsRequest", "AssetTypeSystemAccessComment");
            DropColumn("Workflows.AssetsRequest", "AssetTypeHardwareComment");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805161225181_AssetsV2Migration.Categories.Up.sql");
        }

        public override void Down()
        {
            AddColumn("Workflows.AssetsRequest", "AssetTypeHardwareComment", c => c.String(maxLength: 1024));
            AddColumn("Workflows.AssetsRequest", "AssetTypeSystemAccessComment", c => c.String(maxLength: 1024));
            AddColumn("Workflows.AssetsRequest", "ProjectId", c => c.Long());
            DropColumn("Workflows.AssetsRequest", "CommentForIT");
            DropColumn("Workflows.AssetsRequest", "AssetTypeOptionalHardwareComment");
            DropColumn("Workflows.AssetsRequest", "AssetTypeBackpackComment");
            DropColumn("Workflows.AssetsRequest", "AssetTypeHeadsetComment");
            DropColumn("Workflows.AssetsRequest", "AssetTypeHardwareSetComment");
            CreateIndex("Workflows.AssetsRequest", "ProjectId");
            AddForeignKey("Workflows.AssetsRequest", "ProjectId", "Allocation.Project", "Id");
        }
    }
}
