namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTaxiVouchersFromBusinessTripMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("BusinessTrips.BusinessTrip", "DepartureCityTaxiVouchers");
            DropColumn("BusinessTrips.BusinessTrip", "DestinationCityTaxiVouchers");
        }
        
        public override void Down()
        {
            AddColumn("BusinessTrips.BusinessTrip", "DestinationCityTaxiVouchers", c => c.Int(nullable: false));
            AddColumn("BusinessTrips.BusinessTrip", "DepartureCityTaxiVouchers", c => c.Int(nullable: false));
        }
    }
}
