namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterTechnicalReviewMigration : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN TechnicalAssignmentAssessment NVARCHAR (500);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN SoftwareEngineeringAssessment NVARCHAR (500);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN ExperienceAssessment NVARCHAR (500);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN EnglishUsageAssessment NVARCHAR (400);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN RiskAssessment NVARCHAR (500);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN StrengthAssessment NVARCHAR (500);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN OtherRemarks NVARCHAR (500);");
        }

        public override void Down()
        {
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN TechnicalAssignmentAssessment NVARCHAR (255);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN SoftwareEngineeringAssessment NVARCHAR (255);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN ExperienceAssessment NVARCHAR (255);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN EnglishUsageAssessment NVARCHAR (255);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN RiskAssessment NVARCHAR (255);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN StrengthAssessment NVARCHAR (255);" +
                "ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN OtherRemarks NVARCHAR (255);");
        }
    }
}
