namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGDPRDataActivityMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "GDPR.DataActivity",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataOwnerId = c.Long(nullable: false),
                        PerformedOn = c.DateTime(nullable: false),
                        PerformedByUserId = c.Long(nullable: false),
                        ActivityType = c.Int(nullable: false),
                        ReferenceId = c.Long(),
                        ReferenceType = c.Int(),
                        S1 = c.String(maxLength: 50),
                        S2 = c.String(maxLength: 50),
                        S3 = c.String(maxLength: 50),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("GDPR.DataOwner", t => t.DataOwnerId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Accounts.User", t => t.PerformedByUserId)
                .Index(t => t.DataOwnerId)
                .Index(t => t.PerformedByUserId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("GDPR.DataActivity", "PerformedByUserId", "Accounts.User");
            DropForeignKey("GDPR.DataActivity", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataActivity", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner");
            DropIndex("GDPR.DataActivity", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataActivity", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataActivity", new[] { "PerformedByUserId" });
            DropIndex("GDPR.DataActivity", new[] { "DataOwnerId" });
            DropTable("GDPR.DataActivity");
        }
    }
}
