namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AlterAndUpdateToEmploymentTerminationReasonMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("PeopleManagement.EmploymentTerminationReason", "IsAttrition", c => c.Boolean(nullable: false));
            AddColumn("PeopleManagement.EmploymentTerminationReason", "IsTurnover", c => c.Boolean(nullable: false));

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201712221339188_AlterAndUpdateToEmploymentTerminationReasonMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
            DropColumn("PeopleManagement.EmploymentTerminationReason", "IsTurnover");
            DropColumn("PeopleManagement.EmploymentTerminationReason", "IsAttrition");
        }
    }
}
