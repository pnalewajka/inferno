﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class UpdateNbpUrlMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803270816166_UpdateNbpUrlMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
