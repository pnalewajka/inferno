namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameMoreColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "ContractType", c => c.Int(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("Workflows.BusinessTripSettlementRequestOwnCost", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("Workflows.BusinessTripSettlementRequest", "Type");
            DropColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "Value");
            DropColumn("Workflows.BusinessTripSettlementRequestOwnCost", "Value");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripSettlementRequestOwnCost", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("Workflows.BusinessTripSettlementRequest", "Type", c => c.Int(nullable: false));
            DropColumn("Workflows.BusinessTripSettlementRequestOwnCost", "Amount");
            DropColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "Amount");
            DropColumn("Workflows.BusinessTripSettlementRequest", "ContractType");
        }
    }
}
