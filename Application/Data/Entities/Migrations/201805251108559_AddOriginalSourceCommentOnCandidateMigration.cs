namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOriginalSourceCommentOnCandidateMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "OriginalSourceComment", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.Candidate", "OriginalSourceComment");
        }
    }
}
