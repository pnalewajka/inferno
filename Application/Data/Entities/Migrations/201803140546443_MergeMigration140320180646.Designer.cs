// <auto-generated />
namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class MergeMigration140320180646 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MergeMigration140320180646));
        
        string IMigrationMetadata.Id
        {
            get { return "201803140546443_MergeMigration140320180646"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
