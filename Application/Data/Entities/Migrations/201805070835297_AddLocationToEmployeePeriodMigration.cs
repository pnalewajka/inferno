namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AddLocationToEmployeePeriodMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Allocation.EmploymentPeriod", "LocationId", c => c.Long());
            CreateIndex("Allocation.EmploymentPeriod", "LocationId");
            AddForeignKey("Allocation.EmploymentPeriod", "LocationId", "Dictionaries.Location", "Id");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805070835297_AddLocationToEmployeePeriodMigration.Up.sql");
        }
        
        public override void Down()
        {
            DropForeignKey("Allocation.EmploymentPeriod", "LocationId", "Dictionaries.Location");
            DropIndex("Allocation.EmploymentPeriod", new[] { "LocationId" });
            DropColumn("Allocation.EmploymentPeriod", "LocationId");
        }
    }
}
