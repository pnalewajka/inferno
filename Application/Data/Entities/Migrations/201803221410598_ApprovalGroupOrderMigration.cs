﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class ApprovalGroupOrderMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropIndex("Workflows.ApprovalGroup", new[] { "RequestId" });
            AddColumn("Workflows.ApprovalGroup", "Order", c => c.Long(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803221410598_ApprovalGroupOrderMigration.FixOldData.Up.sql");
            CreateIndex("Workflows.ApprovalGroup", new[] { "RequestId", "Order" }, unique: true, name: "IX_UniqueOrderByRequest");
        }
        
        public override void Down()
        {
            DropIndex("Workflows.ApprovalGroup", "IX_UniqueOrderByRequest");
            DropColumn("Workflows.ApprovalGroup", "Order");
            CreateIndex("Workflows.ApprovalGroup", "RequestId");
        }
    }
}

