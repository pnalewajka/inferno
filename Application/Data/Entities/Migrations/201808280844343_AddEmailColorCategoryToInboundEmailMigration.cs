namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailColorCategoryToInboundEmailMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.InboundEmail", "EmailColorCategory", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.InboundEmail", "EmailColorCategory");
        }
    }
}
