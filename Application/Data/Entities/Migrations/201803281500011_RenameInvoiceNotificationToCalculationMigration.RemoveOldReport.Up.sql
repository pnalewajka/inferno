﻿SET XACT_ABORT ON
GO

DELETE FROM [Reporting].[ReportTemplate]
WHERE [ReportDefinitionId] IN (SELECT [Id] FROM [Reporting].[ReportDefinition] WHERE [Code] = 'InvoiceNotificationsPivot')

DELETE FROM [Reporting].[ReportDefinition]
WHERE [Code] = 'InvoiceNotificationsPivot'

UPDATE [Accounts].[SecurityRole]
SET [Name] = 'CanGenerateInvoiceCalculationsReport'
WHERE [Name] = 'CanGenerateInvoiceNotificationsReport'