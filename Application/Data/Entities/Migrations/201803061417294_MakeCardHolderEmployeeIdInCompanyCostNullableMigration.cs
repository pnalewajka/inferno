namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeCardHolderEmployeeIdInCompanyCostNullableMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", c => c.Long());
            CreateIndex("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId");
        }
        
        public override void Down()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", c => c.Long(nullable: false));
            CreateIndex("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId");
        }
    }
}
