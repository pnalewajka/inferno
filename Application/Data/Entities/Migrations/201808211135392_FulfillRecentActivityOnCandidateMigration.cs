﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class FulfillRecentActivityOnCandidateMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808211135392_FulfillRecentActivityOnCandidateMigration.FulFillRecentActivity.Up.sql");
        }

        public override void Down()
        {
        }
    }
}

