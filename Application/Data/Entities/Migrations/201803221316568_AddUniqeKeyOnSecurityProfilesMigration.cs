namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUniqeKeyOnSecurityProfilesMigration : DbMigration
    {
        public override void Up()
        {
            CreateIndex("Accounts.SecurityProfile", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("Accounts.SecurityProfile", new[] { "Name" });
        }
    }
}
