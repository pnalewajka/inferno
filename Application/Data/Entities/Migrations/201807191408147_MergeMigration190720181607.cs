namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration190720181607 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Recruitment.RecruitmentProcessStepOtherAttendingEmployees",
            //    c => new
            //        {
            //            RecruitmentProcessStepId = c.Long(nullable: false),
            //            EmployeeId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.RecruitmentProcessStepId, t.EmployeeId })
            //    .ForeignKey("Recruitment.RecruitmentProcessStep", t => t.RecruitmentProcessStepId, cascadeDelete: true)
            //    .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
            //    .Index(t => t.RecruitmentProcessStepId)
            //    .Index(t => t.EmployeeId);
        }

        public override void Down()
        {
            //DropForeignKey("Recruitment.RecruitmentProcessStepOtherAttendingEmployees", "EmployeeId", "Allocation.Employee");
            //DropForeignKey("Recruitment.RecruitmentProcessStepOtherAttendingEmployees", "RecruitmentProcessStepId", "Recruitment.RecruitmentProcessStep");
            //DropIndex("Recruitment.RecruitmentProcessStepOtherAttendingEmployees", new[] { "EmployeeId" });
            //DropIndex("Recruitment.RecruitmentProcessStepOtherAttendingEmployees", new[] { "RecruitmentProcessStepId" });
            //DropTable("Recruitment.RecruitmentProcessStepOtherAttendingEmployees");
        }
    }
}
