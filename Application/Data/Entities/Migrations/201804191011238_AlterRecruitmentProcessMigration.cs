namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AlterRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningGeneralOpinion", c => c.String(maxLength: 255));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningMotivation", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkplaceExpectations", c => c.String(maxLength: 255));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningSkills", c => c.String(maxLength: 255));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageEnglish", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageGerman", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageOther", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningContractExpectations", c => c.String(maxLength: 255));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningAvailability", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningOtherProcesses", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningCounterOfferCriteria", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationComment", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsComment", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitNeeded", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitComment", c => c.String(maxLength: 50));
            DropColumn("Recruitment.RecruitmentProcess", "ExpectedSalary");
            DropColumn("Recruitment.RecruitmentProcess", "CanWorkInEvenings");
            DropColumn("Recruitment.RecruitmentProcess", "CanTravel");
            DropColumn("Recruitment.RecruitmentProcess", "Seniority");
        }
        
        public override void Down()
        {           
            AddColumn("Recruitment.RecruitmentProcess", "Seniority", c => c.Int(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "CanTravel", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "CanWorkInEvenings", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "ExpectedSalary", c => c.String());
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitNeeded");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningCounterOfferCriteria");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningOtherProcesses");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningAvailability");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningContractExpectations");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageOther");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageGerman");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageEnglish");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningSkills");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkplaceExpectations");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningMotivation");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningGeneralOpinion");
        }
    }
}
