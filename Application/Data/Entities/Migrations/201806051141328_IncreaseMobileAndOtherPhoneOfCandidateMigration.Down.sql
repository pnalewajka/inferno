﻿ALTER TABLE Recruitment.Candidate  ALTER COLUMN MobilePhone NVARCHAR (20);
ALTER TABLE Recruitment.Candidate  ALTER COLUMN OtherPhone NVARCHAR (20);