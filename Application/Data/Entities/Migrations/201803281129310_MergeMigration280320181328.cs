namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration280320181328 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("Recruitment.Candidate", "LastContactByEmployeeId", c => c.Long());
            //AddColumn("Recruitment.Candidate", "LastContactOn", c => c.DateTime());
            //AddColumn("Recruitment.Candidate", "LastContactType", c => c.Int());
            //CreateIndex("Recruitment.Candidate", "LastContactByEmployeeId");
            //AddForeignKey("Recruitment.Candidate", "LastContactByEmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.Candidate", "LastContactByEmployeeId", "Allocation.Employee");
            //DropIndex("Recruitment.Candidate", new[] { "LastContactByEmployeeId" });
            //DropColumn("Recruitment.Candidate", "LastContactType");
            //DropColumn("Recruitment.Candidate", "LastContactOn");
            //DropColumn("Recruitment.Candidate", "LastContactByEmployeeId");
        }
    }
}
