﻿BEGIN TRANSACTION

INSERT INTO [KPI].[KpiDefinition] ([Name], [Description], [Entity], [SqlFormula], [ModifiedOn]) VALUES
('FTE Rate', 'FTE Rate', 1,
'SELECT (ep.MondayHours + ep.TuesdayHours + ep.WednesdayHours + ep.ThursdayHours + ep.FridayHours + ep.SaturdayHours + ep.SundayHours) / 40 FROM Allocation.EmploymentPeriod ep
    WHERE ep.EmployeeIdEmployee = e.Id 
        AND ep.StartDate <= @date 
        AND (ep.EndDate IS NULL OR ep.EndDate >= @date)', GETDATE()),
('Calculated Monthly Value (PLN)', 'Calculated Monthly Value (PLN)', 1,
'SELECT
    IIF(ep.SalaryGross IS NOT NULL, 
        ep.SalaryGross, 
        IIF(ep.MonthlyRate IS NOT NULL, 
            ep.MonthlyRate, 
            IIF(ep.HourlyRate IS NOT NULL, ep.HourlyRate * 168, NULL))) FROM Allocation.EmploymentPeriod ep
    WHERE ep.EmployeeIdEmployee = e.Id 
        AND ep.StartDate <= @date 
        AND (ep.EndDate IS NULL OR ep.EndDate >= @date)', GETDATE()),
('Employer Cost (PLN)', 'Employer Cost (PLN)', 1,
'SELECT
        IIF(ep.SalaryGross IS NOT NULL, 
            ep.SalaryGross * 1.1948, 
            IIF(ep.MonthlyRate IS NOT NULL, 
                ep.MonthlyRate, 
                IIF(ep.HourlyRate IS NOT NULL, ep.HourlyRate * 168, NULL))) FROM Allocation.EmploymentPeriod ep 
        WHERE ep.EmployeeIdEmployee = e.Id 
            AND ep.StartDate <= @date 
            AND (ep.EndDate IS NULL OR ep.EndDate >= @date)', GETDATE()),
('Years of service', 'Years of service', 1,
'(SELECT SUM(DATEDIFF(day, p.StartDate, COALESCE(p.EndDate, @date))) FROM Allocation.EmploymentPeriod p WHERE p.EmployeeIdEmployee = e.Id) / 365', GETDATE()),
('Daily Allocation Density', 'Daily Allocation Density', 1,
'SELECT COUNT(DISTINCT(ar.ProjectId)) FROM Allocation.AllocationRequest ar WHERE ar.EmployeeId = e.Id 
        AND ar.StartDate <= @date 
        AND (ar.EndDate IS NULL OR ar.EndDate >= @date)', GETDATE()),
('Monthly Allocation Density', 'Monthly Allocation Density', 1,
'SELECT COUNT(DISTINCT(ar.ProjectId)) FROM Allocation.AllocationRequest ar WHERE ar.EmployeeId = e.Id 
        AND ar.StartDate <= EOMONTH(@date) 
        AND (ar.EndDate IS NULL OR ar.EndDate >= DATEADD(DAY, 1, EOMONTH(@date, -1)))', GETDATE()),
('Vacation Left (Days)', 'Vacation Left (Days)', 1,
'(SELECT TOP 1 vb.TotalHourBalance FROM TimeTracking.VacationBalance vb WHERE vb.EmployeeId = e.Id ORDER BY vb.EffectiveOn DESC) / 8', GETDATE()),
('Overtime Bank (Hours)', 'Overtime Bank (Hours)', 1,
'SELECT TOP 1 ob.OvertimeBalance FROM TimeTracking.OvertimeBank ob WHERE ob.EmployeeId = e.Id ORDER BY ob.EffectiveOn DESC', GETDATE()),
('Daily Allocation Density', 'Daily Allocation Density', 0,
'SELECT COUNT(DISTINCT(ar.EmployeeId)) FROM Allocation.AllocationRequest ar WHERE ar.ProjectId = p.Id 
        AND ar.StartDate <= @date 
        AND (ar.EndDate IS NULL OR ar.EndDate >= @date)', GETDATE()),
('Monthly Allocation Density', 'Monthly Allocation Density', 0,
'SELECT COUNT(DISTINCT(ar.EmployeeId)) FROM Allocation.AllocationRequest ar WHERE ar.ProjectId = p.Id 
        AND ar.StartDate <= EOMONTH(@date) 
        AND (ar.EndDate IS NULL OR ar.EndDate >= DATEADD(DAY, 1, EOMONTH(@date, -1)))', GETDATE()),
('Reported Hours Monthly', 'Reported Hours Monthly', 0,
'SELECT SUM(de.Hours) FROM TimeTracking.TimeReportRow r 
     JOIN TimeTracking.TimeReportDailyEntry de ON de.TimeReportRowId = r.Id
     WHERE r.ProjectId = p.Id', GETDATE()),
('Reported Overtime Hours Monthly', 'Reported Overtime Hours Monthly', 0,
'SELECT SUM(de.Hours) FROM TimeTracking.TimeReportRow r 
     JOIN TimeTracking.TimeReportDailyEntry de ON de.TimeReportRowId = r.Id
     WHERE r.ProjectId = p.Id AND r.OvertimeVariant != 0', GETDATE())
GO

DROP VIEW IF EXISTS KPI.EmployeeKPI
DROP VIEW IF EXISTS KPI.ProjectKPI
DROP FUNCTION IF EXISTS KPI.CalculateEmployeeKPIs
DROP FUNCTION IF EXISTS KPI.CalculateProjectKPIs
GO

CREATE FUNCTION KPI.CalculateEmployeeKPIs
(
    @date DATETIME
) 
RETURNS TABLE AS RETURN 
SELECT
    e.Id,
    e.Acronym, 
    e.FirstName, 
    e.LastName,
    (SELECT (ep.MondayHours + ep.TuesdayHours + ep.WednesdayHours + ep.ThursdayHours + ep.FridayHours + ep.SaturdayHours + ep.SundayHours) / 40 FROM Allocation.EmploymentPeriod ep
        WHERE ep.EmployeeIdEmployee = e.Id 
            AND ep.StartDate <= @date 
            AND (ep.EndDate IS NULL OR ep.EndDate >= @date)) as 'FTE Rate',
    (SELECT
        IIF(ep.SalaryGross IS NOT NULL, 
            ep.SalaryGross, 
            IIF(ep.MonthlyRate IS NOT NULL, 
                ep.MonthlyRate, 
                IIF(ep.HourlyRate IS NOT NULL, ep.HourlyRate * 168, NULL))) FROM Allocation.EmploymentPeriod ep
        WHERE ep.EmployeeIdEmployee = e.Id 
            AND ep.StartDate <= @date 
            AND (ep.EndDate IS NULL OR ep.EndDate >= @date)) as 'Calculated Monthly Value (PLN)',
    (SELECT
        IIF(ep.SalaryGross IS NOT NULL, 
            ep.SalaryGross * 1.1948, 
            IIF(ep.MonthlyRate IS NOT NULL, 
                ep.MonthlyRate, 
                IIF(ep.HourlyRate IS NOT NULL, ep.HourlyRate * 168, NULL))) FROM Allocation.EmploymentPeriod ep 
        WHERE ep.EmployeeIdEmployee = e.Id 
            AND ep.StartDate <= @date 
            AND (ep.EndDate IS NULL OR ep.EndDate >= @date)) as 'Employer Cost (PLN)',
    (SELECT SUM(DATEDIFF(day, p.StartDate, COALESCE(p.EndDate, @date))) FROM Allocation.EmploymentPeriod p WHERE p.EmployeeIdEmployee = e.Id) / 365 as 'Years of service',
    (SELECT COUNT(DISTINCT(ar.ProjectId)) FROM Allocation.AllocationRequest ar WHERE ar.EmployeeId = e.Id 
        AND ar.StartDate <= @date 
        AND (ar.EndDate IS NULL OR ar.EndDate >= @date)) as 'Daily Allocation Density',
    (SELECT COUNT(DISTINCT(ar.ProjectId)) FROM Allocation.AllocationRequest ar WHERE ar.EmployeeId = e.Id 
        AND ar.StartDate <= EOMONTH(@date) 
        AND (ar.EndDate IS NULL OR ar.EndDate >= DATEADD(DAY, 1, EOMONTH(@date, -1)))) 
        as 'Monthly Allocation Density',
    (SELECT TOP 1 vb.TotalHourBalance FROM TimeTracking.VacationBalance vb WHERE vb.EmployeeId = e.Id ORDER BY vb.EffectiveOn DESC) / 8 as 'Vacation Left (Days)',
    (SELECT TOP 1 ob.OvertimeBalance FROM TimeTracking.OvertimeBank ob WHERE ob.EmployeeId = e.Id ORDER BY ob.EffectiveOn DESC) as 'Overtime Bank (Hours)'
FROM Allocation.Employee e
GO

CREATE VIEW KPI.EmployeeKPI AS SELECT * FROM CalculateEmployeeKPIs(GETDATE()) 
GO

DROP PROCEDURE IF EXISTS KPI.CalculateEmployeeKPIsForDateRange

GO

CREATE PROCEDURE KPI.CalculateEmployeeKPIsForDateRange
    @startDate DATE,
    @endDate DATE,
    @entityIds CollectionItems READONLY
AS
BEGIN
    DROP TABLE IF EXISTS ##CalculateEmployeeKPIsForDateRangeResult
    SELECT *, @startDate AS ReportRecordDate INTO ##CalculateEmployeeKPIsForDateRangeResult FROM KPI.CalculateEmployeeKPIs(@startDate) WHERE Id IN (SELECT * FROM @entityIds)

    WHILE (@startDate < @endDate)
    BEGIN
        SET @startDate = DATEADD(day, 1, @startDate)
        INSERT INTO ##CalculateEmployeeKPIsForDateRangeResult SELECT *, @startDate AS ReportRecordDate FROM KPI.CalculateEmployeeKPIs(@startDate) WHERE Id IN (SELECT * FROM @entityIds)
    END
END

GO

CREATE FUNCTION KPI.CalculateProjectKPIs
(
    @date DATETIME
) 
RETURNS TABLE AS RETURN 
SELECT
    p.Id,
    s.ProjectShortName + ' / ' + p.SubProjectShortName as 'ProjectShortName',
    (SELECT COUNT(DISTINCT(ar.EmployeeId)) FROM Allocation.AllocationRequest ar WHERE ar.ProjectId = p.Id 
        AND ar.StartDate <= @date 
        AND (ar.EndDate IS NULL OR ar.EndDate >= @date)) as 'Daily Allocation Density',
    (SELECT COUNT(DISTINCT(ar.EmployeeId)) FROM Allocation.AllocationRequest ar WHERE ar.ProjectId = p.Id 
        AND ar.StartDate <= EOMONTH(@date) 
        AND (ar.EndDate IS NULL OR ar.EndDate >= DATEADD(DAY, 1, EOMONTH(@date, -1)))) 
        as 'Monthly Allocation Density',
    (SELECT SUM(de.Hours) FROM TimeTracking.TimeReportRow r 
     JOIN TimeTracking.TimeReportDailyEntry de ON de.TimeReportRowId = r.Id
     WHERE r.ProjectId = p.Id) as 'Reported Hours Monthly',
    (SELECT SUM(de.Hours) FROM TimeTracking.TimeReportRow r 
     JOIN TimeTracking.TimeReportDailyEntry de ON de.TimeReportRowId = r.Id
     WHERE r.ProjectId = p.Id AND r.OvertimeVariant != 0) as 'Reported Overtime Hours Monthly'
FROM
    Allocation.Project p
JOIN Allocation.ProjectSetup s ON s.Id = p.ProjectSetupId
GO

CREATE VIEW KPI.ProjectKPI AS SELECT * FROM KPI.CalculateProjectKPIs(GETDATE())

GO

DROP PROCEDURE IF EXISTS KPI.CalculateProjectKPIsForDateRange

GO

CREATE PROCEDURE KPI.CalculateProjectKPIsForDateRange
    @startDate DATE,
    @endDate DATE,
    @entityIds CollectionItems READONLY
AS
BEGIN
    DROP TABLE IF EXISTS ##CalculateProjectKPIsForDateRangeResult
    SELECT *, @startDate AS ReportRecordDate INTO ##CalculateProjectKPIsForDateRangeResult FROM KPI.CalculateProjectKPIs(@startDate) WHERE Id IN (SELECT * FROM @entityIds)

    WHILE (@startDate < @endDate)
    BEGIN
        SET @startDate = DATEADD(day, 1, @startDate)
        INSERT INTO ##CalculateProjectKPIsForDateRangeResult SELECT *, @startDate AS ReportRecordDate FROM KPI.CalculateProjectKPIs(@startDate) WHERE Id IN (SELECT * FROM @entityIds)
    END
END

GO

COMMIT