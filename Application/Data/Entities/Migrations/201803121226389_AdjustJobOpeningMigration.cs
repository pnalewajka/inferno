namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.JobOpeningRecruitmentHelpers", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpeningRecruitmentHelpers", "EmployeeId", "Allocation.Employee");
            DropIndex("Recruitment.JobOpeningRecruitmentHelpers", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningRecruitmentHelpers", new[] { "EmployeeId" });
            
            AddColumn("Recruitment.JobOpening", "IsHighlyConfidential", c => c.Boolean(nullable: false));
            AlterColumn("Recruitment.JobOpening", "RoleOpenedOn", c => c.DateTime());
            DropTable("Recruitment.JobOpeningRecruitmentHelpers");
        }
        
        public override void Down()
        {
            CreateTable(
                "Recruitment.JobOpeningRecruitmentHelpers",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.EmployeeId });
            
            DropForeignKey("Recruitment.RecruitmentProcessWatchers", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.RecruitmentProcessWatchers", "RecruitmentProcesId", "Recruitment.RecruitmentProcess");
            DropIndex("Recruitment.RecruitmentProcessWatchers", new[] { "EmployeeId" });
            DropIndex("Recruitment.RecruitmentProcessWatchers", new[] { "RecruitmentProcesId" });
            AlterColumn("Recruitment.JobOpening", "RoleOpenedOn", c => c.DateTime(nullable: false));
            DropColumn("Recruitment.JobOpening", "IsHighlyConfidential");

            CreateIndex("Recruitment.JobOpeningRecruitmentHelpers", "EmployeeId");
            CreateIndex("Recruitment.JobOpeningRecruitmentHelpers", "JobOpeningId");
            AddForeignKey("Recruitment.JobOpeningRecruitmentHelpers", "EmployeeId", "Allocation.Employee", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobOpeningRecruitmentHelpers", "JobOpeningId", "Recruitment.JobOpening", "Id", cascadeDelete: true);
        }
    }
}
