﻿UPDATE Dictionaries.Language SET Priority = 1
UPDATE Dictionaries.Language SET Priority = 100 WHERE NameEn = 'English'
UPDATE Dictionaries.Language SET Priority = 95 WHERE NameEn = 'German'
UPDATE Dictionaries.Language SET Priority = 90 WHERE NameEn = 'French'
UPDATE Dictionaries.Language SET Priority = 85 WHERE NameEn = 'Russian'
UPDATE Dictionaries.Language SET Priority = 80 WHERE NameEn = 'Polish'