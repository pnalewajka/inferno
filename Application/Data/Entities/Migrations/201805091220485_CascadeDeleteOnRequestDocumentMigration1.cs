namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeDeleteOnRequestDocumentMigration1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Workflows.RequestDocumentContent", "Id", "Workflows.RequestDocument");
            AddForeignKey("Workflows.RequestDocumentContent", "Id", "Workflows.RequestDocument", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.RequestDocumentContent", "Id", "Workflows.RequestDocument");
            AddForeignKey("Workflows.RequestDocumentContent", "Id", "Workflows.RequestDocument", "Id");
        }
    }
}
