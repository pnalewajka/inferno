namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropOldFKsMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("BusinessTrips.BusinessTrip", new[] { "RequestId" });
            RenameColumn(table: "BusinessTrips.BusinessTrip", name: "RequestId", newName: "Request_Id");
            AlterColumn("BusinessTrips.BusinessTrip", "Request_Id", c => c.Long());
            CreateIndex("BusinessTrips.BusinessTrip", "Request_Id");
        }
        
        public override void Down()
        {
            DropIndex("BusinessTrips.BusinessTrip", new[] { "Request_Id" });
            AlterColumn("BusinessTrips.BusinessTrip", "Request_Id", c => c.Long(nullable: false));
            RenameColumn(table: "BusinessTrips.BusinessTrip", name: "Request_Id", newName: "RequestId");
            CreateIndex("BusinessTrips.BusinessTrip", "RequestId");
        }
    }
}
