namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApplicationOriginToJobApplicationMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.JobApplication", "ApplicationOriginId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.JobApplication", "ApplicationOriginId");
            AddForeignKey("Recruitment.JobApplication", "ApplicationOriginId", "Recruitment.ApplicationOrigin", "Id");
            DropColumn("Recruitment.JobApplication", "SourceId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.JobApplication", "SourceId", c => c.Long(nullable: false));
            DropForeignKey("Recruitment.JobApplication", "ApplicationOriginId", "Recruitment.ApplicationOrigin");
            DropIndex("Recruitment.JobApplication", new[] { "ApplicationOriginId" });
            DropColumn("Recruitment.JobApplication", "ApplicationOriginId");
        }
    }
}
