namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveDataConsentFromRecruitmentModuleMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.DataConsent", "ApplicantId", "Recruitment.Applicant");
            DropForeignKey("Recruitment.DataConsentDocument", "DataConsentId", "Recruitment.DataConsent");
            DropForeignKey("Recruitment.DataConsent", "DataAdministratorId", "Dictionaries.Company");
            DropForeignKey("Recruitment.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            DropForeignKey("Recruitment.DataConsent", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsent", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsentDocument", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsentDocument", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsentDocumentContent", "Id", "Recruitment.DataConsentDocument");
            DropIndex("Recruitment.DataConsentDocumentContent", new[] { "Id" });
            DropIndex("Recruitment.DataConsentDocument", new[] { "DataConsentId" });
            DropIndex("Recruitment.DataConsentDocument", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.DataConsentDocument", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.DataConsent", new[] { "DataAdministratorId" });
            DropIndex("Recruitment.DataConsent", new[] { "DataOwnerId" });
            DropIndex("Recruitment.DataConsent", new[] { "ApplicantId" });
            DropIndex("Recruitment.DataConsent", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.DataConsent", new[] { "UserIdModifiedBy" });
            DropTable("Recruitment.DataConsentDocumentContent");
            DropTable("Recruitment.DataConsentDocument");
            DropTable("Recruitment.DataConsent");
        }
        
        public override void Down()
        {
            CreateTable(
                "Recruitment.DataConsent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        ExpiresOn = c.DateTime(),
                        DataAdministratorId = c.Long(nullable: false),
                        Scope = c.String(maxLength: 250),
                        DataOwnerId = c.Long(nullable: false),
                        ApplicantId = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Recruitment.DataConsentDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataConsentId = c.Long(nullable: false),
                        Name = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 250, unicode: false),
                        ContentLength = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Recruitment.DataConsentDocumentContent",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("Recruitment.DataConsent", "UserIdModifiedBy");
            CreateIndex("Recruitment.DataConsent", "UserIdImpersonatedBy");
            CreateIndex("Recruitment.DataConsent", "ApplicantId");
            CreateIndex("Recruitment.DataConsent", "DataOwnerId");
            CreateIndex("Recruitment.DataConsent", "DataAdministratorId");
            CreateIndex("Recruitment.DataConsentDocument", "UserIdModifiedBy");
            CreateIndex("Recruitment.DataConsentDocument", "UserIdImpersonatedBy");
            CreateIndex("Recruitment.DataConsentDocument", "DataConsentId");
            CreateIndex("Recruitment.DataConsentDocumentContent", "Id");
            AddForeignKey("Recruitment.DataConsentDocumentContent", "Id", "Recruitment.DataConsentDocument", "Id");
            AddForeignKey("Recruitment.DataConsentDocument", "UserIdModifiedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.DataConsentDocument", "UserIdImpersonatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.DataConsent", "UserIdModifiedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.DataConsent", "UserIdImpersonatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id");
            AddForeignKey("Recruitment.DataConsent", "DataAdministratorId", "Dictionaries.Company", "Id");
            AddForeignKey("Recruitment.DataConsentDocument", "DataConsentId", "Recruitment.DataConsent", "Id");
            AddForeignKey("Recruitment.DataConsent", "ApplicantId", "Recruitment.Applicant", "Id");
        }
    }
}
