namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AlterRecruitmentProjectDescriptionMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.ProjectDescription", "AboutClient", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "SpecificDuties", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "ToolsAndEnvironment", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "AboutTeam", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "ProjectTimeline", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "Methodology", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "RequiredSkills", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "AdditionalSkills", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "RecruitmentDetails", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "TravelAndAccomodation", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "RemoteWorkArrangements", c => c.String(maxLength: 100));
            AlterColumn("Recruitment.ProjectDescription", "Comments", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.ProjectDescription", "Comments", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "RemoteWorkArrangements", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "TravelAndAccomodation", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "RecruitmentDetails", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "AdditionalSkills", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "RequiredSkills", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "Methodology", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "ProjectTimeline", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "AboutTeam", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "ToolsAndEnvironment", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "SpecificDuties", c => c.String());
            AlterColumn("Recruitment.ProjectDescription", "AboutClient", c => c.String());
        }
    }
}
