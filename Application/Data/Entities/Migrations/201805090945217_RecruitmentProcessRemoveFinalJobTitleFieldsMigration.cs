namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentProcessRemoveFinalJobTitleFieldsMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalJobTitleId", "SkillManagement.JobTitle");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalJobTitleId" });
            AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate", c => c.DateTime());
            DropColumn("Recruitment.RecruitmentProcess", "FinalJobTitleId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalJobTitleId", c => c.Long());
            DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingDate");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalJobTitleId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalJobTitleId", "SkillManagement.JobTitle", "Id");
        }
    }
}
