namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration140820181446 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("Allocation.EmploymentPeriod", "CompensationCurrencyId", c => c.Long());
            //AddColumn("Allocation.EmploymentPeriod", "ContractorCompanyTaxIdentificationNumber", c => c.String(maxLength: 64));
            //AddColumn("Allocation.EmploymentPeriod", "ContractorCompanyStreetAddress", c => c.String(maxLength: 256));
            //AddColumn("Allocation.EmploymentPeriod", "ContractorCompanyCity", c => c.String(maxLength: 128));
            //AddColumn("Allocation.EmploymentPeriod", "ContractorCompanyZipCode", c => c.String(maxLength: 16));
            //AlterColumn("Allocation.EmploymentPeriod", "ContractorCompanyName", c => c.String(maxLength: 256));
            //CreateIndex("Allocation.EmploymentPeriod", "CompensationCurrencyId");
            //AddForeignKey("Allocation.EmploymentPeriod", "CompensationCurrencyId", "Dictionaries.Currency", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Allocation.EmploymentPeriod", "CompensationCurrencyId", "Dictionaries.Currency");
            //DropIndex("Allocation.EmploymentPeriod", new[] { "CompensationCurrencyId" });
            //AlterColumn("Allocation.EmploymentPeriod", "ContractorCompanyName", c => c.String(maxLength: 255));
            //DropColumn("Allocation.EmploymentPeriod", "ContractorCompanyZipCode");
            //DropColumn("Allocation.EmploymentPeriod", "ContractorCompanyCity");
            //DropColumn("Allocation.EmploymentPeriod", "ContractorCompanyStreetAddress");
            //DropColumn("Allocation.EmploymentPeriod", "ContractorCompanyTaxIdentificationNumber");
            //DropColumn("Allocation.EmploymentPeriod", "CompensationCurrencyId");
        }
    }
}
