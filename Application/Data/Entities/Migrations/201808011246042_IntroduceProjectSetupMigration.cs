namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class IntroduceProjectSetupMigration : AtomicDbMigration
    {
        public override void Up()
        {
            //DropForeignKey("Allocation.ProjectAttachment", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.Project", "CalendarId", "Configuration.Calendar");
            //DropForeignKey("Allocation.IndustryProject", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.IndustryProject", "IndustryId", "Dictionaries.Industry");
            //DropForeignKey("Allocation.ProjectInvoicingRate", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.ProjectKeyTechnologies", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.ProjectKeyTechnologies", "SkillTagId", "SkillManagement.SkillTag");
            //DropForeignKey("Allocation.Project", "MaxCostsCurrencyId", "Dictionaries.Currency");
            //DropForeignKey("Allocation.Project", "OrgUnitId", "Organization.OrgUnit");
            //DropForeignKey("Allocation.Project", "PictureId", "Allocation.ProjectPicture");
            //DropForeignKey("Allocation.Project", "ProjectManagerId", "Allocation.Employee");
            //DropForeignKey("Allocation.Project", "ReinvoiceCurrencyId", "Dictionaries.Currency");
            //DropForeignKey("Allocation.Project", "SalesAccountManagerId", "Allocation.Employee");
            //DropForeignKey("Allocation.ProjectServiceLines", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.ProjectServiceLines", "ServiceLineId", "Dictionaries.ServiceLine");
            //DropForeignKey("Allocation.ProjectSoftwareCategories", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.ProjectSoftwareCategories", "SoftwareCategoryId", "Dictionaries.SoftwareCategory");
            //DropForeignKey("Allocation.Project", "SupervisorManagerId", "Allocation.Employee");
            //DropForeignKey("Allocation.ProjectTagProject", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.ProjectTagProject", "ProjectTagId", "Allocation.ProjectTag");
            //DropForeignKey("Allocation.ProjectTechnologies", "ProjectId", "Allocation.Project");
            //DropForeignKey("Allocation.ProjectTechnologies", "SkillId", "SkillManagement.Skill");
            //DropIndex("Allocation.Project", "IX_ProjectNamePerClient");
            //DropIndex("Allocation.Project", new[] { "OrgUnitId" });
            //DropIndex("Allocation.Project", new[] { "ProjectManagerId" });
            //DropIndex("Allocation.Project", new[] { "SalesAccountManagerId" });
            //DropIndex("Allocation.Project", new[] { "SupervisorManagerId" });
            //DropIndex("Allocation.Project", new[] { "CalendarId" });
            //DropIndex("Allocation.Project", new[] { "PictureId" });
            //DropIndex("Allocation.Project", new[] { "ReinvoiceCurrencyId" });
            //DropIndex("Allocation.Project", new[] { "MaxCostsCurrencyId" });
            //DropIndex("Allocation.ProjectAttachment", new[] { "ProjectId" });
            //DropIndex("Allocation.IndustryProject", new[] { "ProjectId" });
            //DropIndex("Allocation.ProjectKeyTechnologies", new[] { "ProjectId" });
            //DropIndex("Allocation.ProjectServiceLines", new[] { "ProjectId" });
            //DropIndex("Allocation.ProjectSoftwareCategories", new[] { "ProjectId" });
            //DropIndex("Allocation.ProjectTagProject", new[] { "ProjectId" });
            //DropIndex("Allocation.ProjectTechnologies", new[] { "ProjectId" });
            //CreateTable(
            //    "Allocation.ProjectSetup",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            ClientShortName = c.String(maxLength: 128),
            //            ProjectShortName = c.String(maxLength: 128),
            //            GenericName = c.String(),
            //            Region = c.String(maxLength: 60),
            //            ProjectType = c.Int(),
            //            ProjectFeatures = c.Int(nullable: false),
            //            OrgUnitId = c.Long(nullable: false),
            //            ProjectManagerId = c.Long(),
            //            SalesAccountManagerId = c.Long(),
            //            SupervisorManagerId = c.Long(),
            //            ProjectStatus = c.Int(nullable: false),
            //            CalendarId = c.Long(),
            //            Color = c.String(),
            //            PictureId = c.Long(),
            //            IsAvailableForSalesPresentation = c.Boolean(nullable: false),
            //            CurrentPhase = c.Int(nullable: false),
            //            Disclosures = c.Int(nullable: false),
            //            HasClientReferences = c.Boolean(nullable: false),
            //            CustomerSizeId = c.Long(),
            //            BusinessCase = c.String(),
            //            ReinvoicingToCustomer = c.Boolean(nullable: false),
            //            ReinvoiceCurrencyId = c.Long(),
            //            CostApprovalPolicy = c.Int(nullable: false),
            //            MaxCostsCurrencyId = c.Long(),
            //            MaxHotelCosts = c.Decimal(precision: 18, scale: 2),
            //            MaxTotalFlightsCosts = c.Decimal(precision: 18, scale: 2),
            //            MaxOtherTravelCosts = c.Decimal(precision: 18, scale: 2),
            //            InvoicingAlgorithm = c.String(),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Configuration.Calendar", t => t.CalendarId)
            //    .ForeignKey("Dictionaries.Currency", t => t.MaxCostsCurrencyId)
            //    .ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
            //    .ForeignKey("Allocation.ProjectPicture", t => t.PictureId)
            //    .ForeignKey("Allocation.Employee", t => t.ProjectManagerId)
            //    .ForeignKey("Dictionaries.Currency", t => t.ReinvoiceCurrencyId)
            //    .ForeignKey("Allocation.Employee", t => t.SalesAccountManagerId)
            //    .ForeignKey("Allocation.Employee", t => t.SupervisorManagerId)
            //    .Index(t => new { t.ClientShortName, t.ProjectShortName }, unique: true, clustered: true, name: "IX_ProjectNamePerClient")
            //    .Index(t => t.OrgUnitId)
            //    .Index(t => t.ProjectManagerId)
            //    .Index(t => t.SalesAccountManagerId)
            //    .Index(t => t.SupervisorManagerId)
            //    .Index(t => t.CalendarId)
            //    .Index(t => t.PictureId)
            //    .Index(t => t.ReinvoiceCurrencyId)
            //    .Index(t => t.MaxCostsCurrencyId);

            //CreateTable(
            //    "Allocation.IndustryProject",
            //    c => new
            //        {
            //            ProjectSetupId = c.Long(nullable: false),
            //            IndustryId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.ProjectSetupId, t.IndustryId })
            //    .ForeignKey("Allocation.ProjectSetup", t => t.ProjectSetupId, cascadeDelete: true)
            //    .ForeignKey("Dictionaries.Industry", t => t.IndustryId, cascadeDelete: true)
            //    .Index(t => t.ProjectSetupId);

            //CreateTable(
            //    "Allocation.ProjectKeyTechnologies",
            //    c => new
            //        {
            //            ProjectSetupId = c.Long(nullable: false),
            //            SkillTagId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.ProjectSetupId, t.SkillTagId })
            //    .ForeignKey("Allocation.ProjectSetup", t => t.ProjectSetupId, cascadeDelete: true)
            //    .ForeignKey("SkillManagement.SkillTag", t => t.SkillTagId, cascadeDelete: true)
            //    .Index(t => t.ProjectSetupId);

            //CreateTable(
            //    "Allocation.ProjectServiceLines",
            //    c => new
            //        {
            //            ProjectSetupId = c.Long(nullable: false),
            //            ServiceLineId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.ProjectSetupId, t.ServiceLineId })
            //    .ForeignKey("Allocation.ProjectSetup", t => t.ProjectSetupId, cascadeDelete: true)
            //    .ForeignKey("Dictionaries.ServiceLine", t => t.ServiceLineId, cascadeDelete: true)
            //    .Index(t => t.ProjectSetupId);

            //CreateTable(
            //    "Allocation.ProjectSoftwareCategories",
            //    c => new
            //        {
            //            ProjectSetupId = c.Long(nullable: false),
            //            SoftwareCategoryId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.ProjectSetupId, t.SoftwareCategoryId })
            //    .ForeignKey("Allocation.ProjectSetup", t => t.ProjectSetupId, cascadeDelete: true)
            //    .ForeignKey("Dictionaries.SoftwareCategory", t => t.SoftwareCategoryId, cascadeDelete: true)
            //    .Index(t => t.ProjectSetupId);

            //CreateTable(
            //    "Allocation.ProjectTagProject",
            //    c => new
            //        {
            //            ProjectSetupId = c.Long(nullable: false),
            //            ProjectTagId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.ProjectSetupId, t.ProjectTagId })
            //    .ForeignKey("Allocation.ProjectSetup", t => t.ProjectSetupId, cascadeDelete: true)
            //    .ForeignKey("Allocation.ProjectTag", t => t.ProjectTagId, cascadeDelete: true)
            //    .Index(t => t.ProjectSetupId);

            //CreateTable(
            //    "Allocation.ProjectTechnologies",
            //    c => new
            //        {
            //            ProjectSetupId = c.Long(nullable: false),
            //            SkillId = c.Long(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.ProjectSetupId, t.SkillId })
            //    .ForeignKey("Allocation.ProjectSetup", t => t.ProjectSetupId, cascadeDelete: true)
            //    .ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
            //    .Index(t => t.ProjectSetupId);

            //AddColumn("Allocation.Project", "ProjectSetupId", c => c.Long(nullable: false));
            //AddColumn("Allocation.Project", "IsMainProject", c => c.Boolean(nullable: false));
            //AddColumn("Allocation.ProjectAttachment", "ProjectSetupId", c => c.Long(nullable: false));
            //CreateIndex("Allocation.Project", "ProjectSetupId");
            //CreateIndex("Allocation.Project", "ProjectShortName", unique: true, clustered: true, name: "IX_ProjectNamePerClient");
            //CreateIndex("Allocation.ProjectAttachment", "ProjectSetupId");
            //AddForeignKey("Allocation.ProjectAttachment", "ProjectSetupId", "Allocation.ProjectSetup", "Id");
            //AddForeignKey("Allocation.ProjectInvoicingRate", "ProjectId", "Allocation.ProjectSetup", "Id", cascadeDelete: true);
            //AddForeignKey("Allocation.Project", "ProjectSetupId", "Allocation.ProjectSetup", "Id");
            //DropColumn("Allocation.Project", "Region");
            //DropColumn("Allocation.Project", "ProjectType");
            //DropColumn("Allocation.Project", "ProjectFeatures");
            //DropColumn("Allocation.Project", "ClientShortName");
            //DropColumn("Allocation.Project", "OrgUnitId");
            //DropColumn("Allocation.Project", "ProjectManagerId");
            //DropColumn("Allocation.Project", "SalesAccountManagerId");
            //DropColumn("Allocation.Project", "SupervisorManagerId");
            //DropColumn("Allocation.Project", "ProjectStatus");
            //DropColumn("Allocation.Project", "CalendarId");
            //DropColumn("Allocation.Project", "Color");
            //DropColumn("Allocation.Project", "GenericName");
            //DropColumn("Allocation.Project", "PictureId");
            //DropColumn("Allocation.Project", "IsAvailableForSalesPresentation");
            //DropColumn("Allocation.Project", "CurrentPhase");
            //DropColumn("Allocation.Project", "Disclosures");
            //DropColumn("Allocation.Project", "HasClientReferences");
            //DropColumn("Allocation.Project", "CustomerSizeId");
            //DropColumn("Allocation.Project", "BusinessCase");
            //DropColumn("Allocation.Project", "ReinvoicingToCustomer");
            //DropColumn("Allocation.Project", "ReinvoiceCurrencyId");
            //DropColumn("Allocation.Project", "CostApprovalPolicy");
            //DropColumn("Allocation.Project", "MaxCostsCurrencyId");
            //DropColumn("Allocation.Project", "MaxHotelCosts");
            //DropColumn("Allocation.Project", "MaxTotalFlightsCosts");
            //DropColumn("Allocation.Project", "MaxOtherTravelCosts");
            //DropColumn("Allocation.Project", "InvoicingAlgorithm");
            //DropColumn("Allocation.ProjectAttachment", "ProjectId");
            //DropTable("Allocation.IndustryProject");
            //DropTable("Allocation.ProjectKeyTechnologies");
            //DropTable("Allocation.ProjectServiceLines");
            //DropTable("Allocation.ProjectSoftwareCategories");
            //DropTable("Allocation.ProjectTagProject");
            //DropTable("Allocation.ProjectTechnologies");

            DropForeignKey("Allocation.Project", "CalendarId", "Configuration.Calendar");
            DropForeignKey("Allocation.IndustryProject", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.ProjectInvoicingRate", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.ProjectKeyTechnologies", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.Project", "MaxCostsCurrencyId", "Dictionaries.Currency");
            DropForeignKey("Allocation.Project", "OrgUnitId", "Organization.OrgUnit");
            DropForeignKey("Allocation.Project", "PictureId", "Allocation.ProjectPicture");
            DropForeignKey("Allocation.Project", "ProjectManagerId", "Allocation.Employee");
            DropForeignKey("Allocation.Project", "ReinvoiceCurrencyId", "Dictionaries.Currency");
            DropForeignKey("Allocation.Project", "SalesAccountManagerId", "Allocation.Employee");
            DropForeignKey("Allocation.ProjectServiceLines", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.ProjectSoftwareCategories", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.Project", "SupervisorManagerId", "Allocation.Employee");
            DropForeignKey("Allocation.ProjectTagProject", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.ProjectTechnologies", "ProjectId", "Allocation.Project");
            DropForeignKey("Allocation.Project", "FK_Allocation.Project_BusinessTrips.Currency_ReinvoiceCurrencyId");
            DropForeignKey("Allocation.Project", "FK_Allocation.Project_BusinessTrips.Currency_MaxCostsCurrencyId");
            DropIndex("Allocation.Project", "IX_ProjectNamePerClient");
            DropIndex("Allocation.ProjectAttachment", new[] { "ProjectId" });
            DropIndex("Allocation.Project", new[] { "OrgUnitId" });
            DropIndex("Allocation.Project", new[] { "ProjectManagerId" });
            DropIndex("Allocation.Project", new[] { "SalesAccountManagerId" });
            DropIndex("Allocation.Project", new[] { "SupervisorManagerId" });
            DropIndex("Allocation.Project", new[] { "CalendarId" });
            DropIndex("Allocation.Project", new[] { "PictureId" });
            DropIndex("Allocation.Project", new[] { "ReinvoiceCurrencyId" });
            DropIndex("Allocation.Project", new[] { "MaxCostsCurrencyId" });
            DropIndex("Allocation.Project", "IX_Project_Apn");
            DropIndex("Allocation.Project", "IX_Project_StartDate_EndDate_JiraIssueKey");
            DropIndex("Allocation.Project", "IX_Project_StartDate_EndDate_JiraIssueKey_TimeTrackingType_UtilizationCategoryId");
            DropIndex("Allocation.IndustryProject", new[] { "ProjectId" });
            DropIndex("Allocation.ProjectKeyTechnologies", new[] { "ProjectId" });
            DropIndex("Allocation.ProjectServiceLines", new[] { "ProjectId" });
            DropIndex("Allocation.ProjectSoftwareCategories", new[] { "ProjectId" });
            DropIndex("Allocation.ProjectTagProject", new[] { "ProjectId" });
            DropIndex("Allocation.ProjectTechnologies", new[] { "ProjectId" });

            CreateTable(
                "Allocation.ProjectSetup",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    ClientShortName = c.String(maxLength: 128),
                    ProjectShortName = c.String(maxLength: 128),
                    GenericName = c.String(),
                    Region = c.String(maxLength: 60),
                    ProjectType = c.Int(),
                    ProjectFeatures = c.Int(nullable: false),
                    OrgUnitId = c.Long(nullable: false),
                    ProjectManagerId = c.Long(),
                    SalesAccountManagerId = c.Long(),
                    SupervisorManagerId = c.Long(),
                    ProjectStatus = c.Int(nullable: false),
                    CalendarId = c.Long(),
                    Color = c.String(),
                    PictureId = c.Long(),
                    IsAvailableForSalesPresentation = c.Boolean(nullable: false),
                    CurrentPhase = c.Int(nullable: false),
                    Disclosures = c.Int(nullable: false),
                    HasClientReferences = c.Boolean(nullable: false),
                    CustomerSizeId = c.Long(),
                    BusinessCase = c.String(),
                    ReinvoicingToCustomer = c.Boolean(nullable: false),
                    ReinvoiceCurrencyId = c.Long(),
                    CostApprovalPolicy = c.Int(nullable: false),
                    MaxCostsCurrencyId = c.Long(),
                    MaxHotelCosts = c.Decimal(precision: 18, scale: 2),
                    MaxTotalFlightsCosts = c.Decimal(precision: 18, scale: 2),
                    MaxOtherTravelCosts = c.Decimal(precision: 18, scale: 2),
                    InvoicingAlgorithm = c.String(),
                    Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Configuration.Calendar", t => t.CalendarId)
                .ForeignKey("Dictionaries.Currency", t => t.MaxCostsCurrencyId)
                .ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
                .ForeignKey("Allocation.ProjectPicture", t => t.PictureId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.ProjectManagerId)
                .ForeignKey("Dictionaries.Currency", t => t.ReinvoiceCurrencyId)
                .ForeignKey("Allocation.Employee", t => t.SalesAccountManagerId)
                .ForeignKey("Allocation.Employee", t => t.SupervisorManagerId)
                .Index(t => new { t.ClientShortName, t.ProjectShortName }, unique: true, clustered: false, name: "IX_ProjectNamePerClient")
                .Index(t => t.OrgUnitId)
                .Index(t => t.ProjectManagerId)
                .Index(t => t.SalesAccountManagerId)
                .Index(t => t.SupervisorManagerId)
                .Index(t => t.CalendarId)
                .Index(t => t.PictureId)
                .Index(t => t.ReinvoiceCurrencyId)
                .Index(t => t.MaxCostsCurrencyId);

            FixIntermediateTable("Allocation.IndustryProject");
            FixIntermediateTable("Allocation.ProjectKeyTechnologies");
            FixIntermediateTable("Allocation.ProjectServiceLines");
            FixIntermediateTable("Allocation.ProjectSoftwareCategories");
            FixIntermediateTable("Allocation.ProjectTagProject");
            FixIntermediateTable("Allocation.ProjectTechnologies");
            FixIntermediateTable("Allocation.ProjectAttachment");
            FixIntermediateTable("Allocation.ProjectInvoicingRate");

            AddColumn("Allocation.Project", "ProjectSetupId", c => c.Long(nullable: true));
            AddColumn("Allocation.Project", "IsMainProject", c => c.Boolean(nullable: false));

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808011246042_IntroduceProjectSetupMigration.PortLegacyData.Up.sql");

            AlterColumn("Allocation.Project", "ProjectSetupId", c => c.Long(nullable: false));
            CreateIndex("Allocation.Project", "ProjectSetupId");
            AddForeignKey("Allocation.Project", "ProjectSetupId", "Allocation.ProjectSetup", "Id");

            FixIntermediateTableStep2("Allocation.IndustryProject");
            FixIntermediateTableStep2("Allocation.ProjectKeyTechnologies");
            FixIntermediateTableStep2("Allocation.ProjectServiceLines");
            FixIntermediateTableStep2("Allocation.ProjectSoftwareCategories");
            FixIntermediateTableStep2("Allocation.ProjectTagProject");
            FixIntermediateTableStep2("Allocation.ProjectTechnologies");
            FixIntermediateTableStep2("Allocation.ProjectAttachment");
            FixIntermediateTableStep2("Allocation.ProjectInvoicingRate");

            DropColumn("Allocation.Project", "Region");
            DropColumn("Allocation.Project", "ProjectType");
            DropColumn("Allocation.Project", "ProjectFeatures");
            DropColumn("Allocation.Project", "ClientShortName");
            DropColumn("Allocation.Project", "OrgUnitId");
            DropColumn("Allocation.Project", "ProjectManagerId");
            DropColumn("Allocation.Project", "SalesAccountManagerId");
            DropColumn("Allocation.Project", "SupervisorManagerId");
            DropColumn("Allocation.Project", "ProjectStatus");
            DropColumn("Allocation.Project", "CalendarId");
            DropColumn("Allocation.Project", "Color");
            DropColumn("Allocation.Project", "GenericName");
            DropColumn("Allocation.Project", "PictureId");
            DropColumn("Allocation.Project", "IsAvailableForSalesPresentation");
            DropColumn("Allocation.Project", "CurrentPhase");
            DropColumn("Allocation.Project", "Disclosures");
            DropColumn("Allocation.Project", "HasClientReferences");
            DropColumn("Allocation.Project", "CustomerSizeId");
            DropColumn("Allocation.Project", "BusinessCase");
            DropColumn("Allocation.Project", "ReinvoicingToCustomer");
            DropColumn("Allocation.Project", "ReinvoiceCurrencyId");
            DropColumn("Allocation.Project", "CostApprovalPolicy");
            DropColumn("Allocation.Project", "MaxCostsCurrencyId");
            DropColumn("Allocation.Project", "MaxHotelCosts");
            DropColumn("Allocation.Project", "MaxTotalFlightsCosts");
            DropColumn("Allocation.Project", "MaxOtherTravelCosts");
            DropColumn("Allocation.Project", "InvoicingAlgorithm");
        }

        private void FixIntermediateTable(string tableName)
        {
            DropForeignKey(tableName, "ProjectId", "Allocation.Project");
            RenameColumn(tableName, "ProjectId", "ProjectSetupId");
        }

        private void FixIntermediateTableStep2(string tableName)
        {
            AddForeignKey(tableName, "ProjectSetupId", "Allocation.ProjectSetup", cascadeDelete: true);
        }

        public override void Down()
        {
            throw new NotImplementedException("nope");
        }
    }
}
