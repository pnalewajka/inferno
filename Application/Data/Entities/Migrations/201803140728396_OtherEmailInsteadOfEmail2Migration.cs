namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OtherEmailInsteadOfEmail2Migration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "OtherEmailAddress", c => c.String(maxLength: 255));
            DropColumn("Recruitment.Candidate", "EmailAddress2");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.Candidate", "EmailAddress2", c => c.String(maxLength: 255));
            DropColumn("Recruitment.Candidate", "OtherEmailAddress");
        }
    }
}
