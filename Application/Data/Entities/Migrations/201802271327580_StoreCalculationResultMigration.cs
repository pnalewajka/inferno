namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreCalculationResultMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "CalculationResult", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "CalculationResult");
        }
    }
}
