﻿IF (SELECT COL_LENGTH('[Allocation].[EmploymentPeriod]', 'SalaryGross')) IS NULL
BEGIN
    ALTER TABLE [Allocation].[EmploymentPeriod] ADD [SalaryGross] [decimal](18, 2)
    ALTER TABLE [Allocation].[EmploymentPeriod] ADD [AuthorRemunerationRatio] [decimal](18, 2)

    UPDATE [TimeTracking].[ContractType]
       SET [TimeTrackingCompensationCalculator] = 8,
           [NotifyInvoiceAmount] = 0
     WHERE [NameEn] = N'Permanent employment - Poland'

    UPDATE [TimeTracking].[ContractType]
       SET [TimeTrackingCompensationCalculator] = 9,
           [NotifyInvoiceAmount] = 0
     WHERE [NameEn] = N'Permanent employment with 50% CDE - Poland'
END
