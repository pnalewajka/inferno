﻿UPDATE [Accounts].[SecurityRole]
   SET [Name] = N'CanViewEmployeeSalaryData'
 WHERE [Name] = N'CanViewAllEmployeeSalaryData'
