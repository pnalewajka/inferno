﻿ALTER FUNCTION [KPI].[CalculateProjectKPIs]
(
    @date DATETIME
)
RETURNS TABLE
AS
RETURN
(
    SELECT
    p.Id,
    p.ProjectShortName,
    p.ClientShortName,
    (SELECT SUM(de.Hours) FROM TimeTracking.TimeReportRow r 
            JOIN TimeTracking.TimeReportDailyEntry de ON de.TimeReportRowId = r.Id
            WHERE r.ProjectId = p.Id AND r.OvertimeVariant != 0) AS 'Reported Overtime Hours Monthly',
    (SELECT SUM(de.Hours) FROM TimeTracking.TimeReportRow r 
            JOIN TimeTracking.TimeReportDailyEntry de ON de.TimeReportRowId = r.Id
            WHERE r.ProjectId = p.Id) AS 'Reported Hours Monthly',
    (SELECT COUNT(DISTINCT(ar.EmployeeId)) FROM Allocation.AllocationRequest ar WHERE ar.ProjectId = p.Id 
            AND ar.StartDate <= EOMONTH(@date) 
            AND (ar.EndDate IS NULL OR ar.EndDate >= DATEADD(DAY, 1, EOMONTH(@date, -1)))) AS 'Monthly Allocation Density',
    (SELECT COUNT(DISTINCT(ar.EmployeeId)) FROM Allocation.AllocationRequest ar WHERE ar.ProjectId = p.Id 
            AND ar.StartDate <= @date 
            AND (ar.EndDate IS NULL OR ar.EndDate >= @date)) AS 'Daily Allocation Density'
    FROM
    Allocation.FullProject p
)