namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CandidateSoftDeletableIndexEmailMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.Candidate", new[] { "EmailAddress" });
            Sql("CREATE UNIQUE INDEX IX_Recruitment_Candidate_EmailAddress ON Recruitment.Candidate(EmailAddress) WHERE IsDeleted = 0");
        }
        
        public override void Down()
        {
            Sql("DROP INDEX IX_Recruitment_Candidate_EmailAddress ON Recruitment.Candidate");
            CreateIndex("Recruitment.Candidate", "EmailAddress", unique: true);
        }
    }
}
