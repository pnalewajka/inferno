namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration180720181016 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Compensation.CurrencyExchangeRate",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            Date = c.DateTime(nullable: false),
            //            BaseCurrencyId = c.Long(nullable: false),
            //            QuotedCurrencyId = c.Long(nullable: false),
            //            Rate = c.Decimal(nullable: false, precision: 18, scale: 6),
            //            SourceType = c.Int(nullable: false),
            //            TableNumber = c.String(maxLength: 32),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Dictionaries.Currency", t => t.BaseCurrencyId)
            //    .ForeignKey("Dictionaries.Currency", t => t.QuotedCurrencyId)
            //    .Index(t => t.BaseCurrencyId)
            //    .Index(t => t.QuotedCurrencyId);
            
            //AddColumn("Recruitment.InboundEmail", "ResearchingComment", c => c.String(maxLength: 300));
            //AddColumn("Recruitment.RecruitmentProcessStep", "CloneOfStepId", c => c.Long());
            //AddColumn("Recruitment.TechnicalReview", "TechnicalKnowledgeAssessment", c => c.String());
            //AddColumn("Recruitment.TechnicalReview", "TeamProjectSuitabilityAssessment", c => c.String());
            //CreateIndex("Recruitment.RecruitmentProcessStep", "CloneOfStepId");
            //AddForeignKey("Recruitment.RecruitmentProcessStep", "CloneOfStepId", "Recruitment.RecruitmentProcessStep", "Id");
            //DropColumn("Recruitment.TechnicalReview", "SoftwareEngineeringAssessment");
            //DropColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment");
            //DropColumn("Recruitment.TechnicalReview", "ExperienceAssessment");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.TechnicalReview", "ExperienceAssessment", c => c.String());
            //AddColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment", c => c.String());
            //AddColumn("Recruitment.TechnicalReview", "SoftwareEngineeringAssessment", c => c.String());
            //DropForeignKey("Compensation.CurrencyExchangeRate", "QuotedCurrencyId", "Dictionaries.Currency");
            //DropForeignKey("Compensation.CurrencyExchangeRate", "BaseCurrencyId", "Dictionaries.Currency");
            //DropForeignKey("Recruitment.RecruitmentProcessStep", "CloneOfStepId", "Recruitment.RecruitmentProcessStep");
            //DropIndex("Compensation.CurrencyExchangeRate", new[] { "QuotedCurrencyId" });
            //DropIndex("Compensation.CurrencyExchangeRate", new[] { "BaseCurrencyId" });
            //DropIndex("Recruitment.RecruitmentProcessStep", new[] { "CloneOfStepId" });
            //DropColumn("Recruitment.TechnicalReview", "TeamProjectSuitabilityAssessment");
            //DropColumn("Recruitment.TechnicalReview", "TechnicalKnowledgeAssessment");
            //DropColumn("Recruitment.RecruitmentProcessStep", "CloneOfStepId");
            //DropColumn("Recruitment.InboundEmail", "ResearchingComment");
            //DropTable("Compensation.CurrencyExchangeRate");
        }
    }
}
