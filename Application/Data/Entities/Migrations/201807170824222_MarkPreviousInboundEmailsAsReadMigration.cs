﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class MarkPreviousInboundEmailsAsReadMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807170824222_MarkPreviousInboundEmailsAsReadMigration.MarkPreviousInboundEmailsAsRead.Up.sql");
        }
    }
}