﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AdjustRecruitmentCandidateEntityMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803021012598_AdjustRecruitmentCandidateEntityMigration.DeleteCandidate.Up.sql");
            AddColumn("Recruitment.Candidate", "SocialLink", c => c.String(maxLength: 255));
            AlterColumn("Recruitment.Candidate", "FirstName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("Recruitment.Candidate", "LastName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String(maxLength: 20));
            AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String(maxLength: 20));
            AlterColumn("Recruitment.Candidate", "EmailAddreess", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("Recruitment.Candidate", "EmailAddreess2", c => c.String(maxLength: 255));
            AlterColumn("Recruitment.Candidate", "SkypeLogin", c => c.String(maxLength: 255));
            AlterColumn("Recruitment.Candidate", "RelocateDetails", c => c.String(maxLength: 1023));
            CreateIndex("Recruitment.Candidate", "EmailAddreess", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.Candidate", new[] { "EmailAddreess" });
            AlterColumn("Recruitment.Candidate", "RelocateDetails", c => c.String());
            AlterColumn("Recruitment.Candidate", "SkypeLogin", c => c.String());
            AlterColumn("Recruitment.Candidate", "EmailAddreess2", c => c.String());
            AlterColumn("Recruitment.Candidate", "EmailAddreess", c => c.String());
            AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String());
            AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String());
            AlterColumn("Recruitment.Candidate", "LastName", c => c.String());
            AlterColumn("Recruitment.Candidate", "FirstName", c => c.String());
            DropColumn("Recruitment.Candidate", "SocialLink");
        }
    }
}

