﻿IF (SELECT COL_LENGTH('[Dictionaries].[Location]', 'CountryId')) IS NULL
BEGIN

  ALTER TABLE [Dictionaries].[Location]
          ADD [CountryId] [bigint]

  EXEC('CREATE INDEX [IX_CountryId] ON [Dictionaries].[Location]([CountryId])')

  ALTER TABLE [Dictionaries].[Location]
  ADD CONSTRAINT [FK_Dictionaries.Location_Dictionaries.Country_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [Dictionaries].[Country] ([Id])

  EXEC('INSERT INTO [Dictionaries].[Location] ( [Name], [ModifiedOn], [CountryId] ) VALUES ( N''Germany'', ''2018-03-22'', (SELECT [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N''DE'') )')

  UPDATE [Allocation].[Employee]
     SET [LocationId] = (SELECT [Id] FROM [Dictionaries].[Location] WHERE [Name] = N'Germany')
   WHERE [Acronym] = N'lga'
END
