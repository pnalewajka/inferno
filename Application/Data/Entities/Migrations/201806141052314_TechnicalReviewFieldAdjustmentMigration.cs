namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class TechnicalReviewFieldAdjustmentMigration : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN TechnicalAssignmentAssessment NVARCHAR(MAX)");
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN SolutionDesignAssessment NVARCHAR(MAX)");
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN SoftwareEngineeringAssessment NVARCHAR(MAX)");
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN ExperienceAssessment NVARCHAR(MAX)");
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN RiskAssessment NVARCHAR(MAX)");
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN StrengthAssessment NVARCHAR(MAX)");
            Sql("ALTER TABLE Recruitment.TechnicalReview ALTER COLUMN OtherRemarks NVARCHAR(MAX)");
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.TechnicalReview", "OtherRemarks", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.TechnicalReview", "StrengthAssessment", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.TechnicalReview", "RiskAssessment", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.TechnicalReview", "ExperienceAssessment", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.TechnicalReview", "SoftwareEngineeringAssessment", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment", c => c.String(maxLength: 255));
            AlterColumn("Recruitment.TechnicalReview", "TechnicalAssignmentAssessment", c => c.String(maxLength: 500));
        }
    }
}
