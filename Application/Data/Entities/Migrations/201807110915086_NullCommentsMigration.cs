﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;
    
    public partial class NullCommentsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807110915086_NullCommentsMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
