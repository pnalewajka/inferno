﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;
    
    public partial class ResultingUserInOnboardingRequestMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.OnboardingRequest", "ResultingUserId", c => c.Long());
            CreateIndex("Workflows.OnboardingRequest", "ResultingUserId");
            AddForeignKey("Workflows.OnboardingRequest", "ResultingUserId", "Accounts.User", "Id");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201801171251238_ResultingUserInOnboardingRequestMigration.Script.Up.sql");
        }

        public override void Down()
        {
            DropForeignKey("Workflows.OnboardingRequest", "ResultingUserId", "Accounts.User");
            DropIndex("Workflows.OnboardingRequest", new[] { "ResultingUserId" });
            DropColumn("Workflows.OnboardingRequest", "ResultingUserId");
        }
    }
}

