namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class TruncateOrExtendJobOpeningFieldsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806061243215_TruncateOrExtendJobOpeningFieldsMigration.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806061243215_TruncateOrExtendJobOpeningFieldsMigration.Down.sql");
        }
    }
}
