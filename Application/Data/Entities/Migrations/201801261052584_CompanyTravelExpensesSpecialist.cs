namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanyTravelExpensesSpecialist : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Organization.CompaniesTravelExpensesSpecialists",
                c => new
                    {
                        CompanyId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.EmployeeId })
                .ForeignKey("Dictionaries.Company", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Organization.CompaniesTravelExpensesSpecialists", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Organization.CompaniesTravelExpensesSpecialists", "CompanyId", "Dictionaries.Company");
            DropIndex("Organization.CompaniesTravelExpensesSpecialists", new[] { "EmployeeId" });
            DropIndex("Organization.CompaniesTravelExpensesSpecialists", new[] { "CompanyId" });
            DropTable("Organization.CompaniesTravelExpensesSpecialists");
        }
    }
}
