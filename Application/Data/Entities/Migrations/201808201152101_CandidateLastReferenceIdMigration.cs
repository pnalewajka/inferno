namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CandidateLastReferenceIdMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "LastReferenceType", c => c.Int());
            AddColumn("Recruitment.Candidate", "LastReferenceId", c => c.Long());
        }

        public override void Down()
        {
            DropColumn("Recruitment.Candidate", "LastReferenceId");
            DropColumn("Recruitment.Candidate", "LastReferenceType");
        }
    }
}
