﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class CustomerPortalResourceFileMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201801301236261_CustomerPortalResourceFileMigration.DropCustomerPortalFileTableSql.Up.sql");

            CreateTable(
                "CustomerPortal.ResourceFormFile",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    ResourceFormId = c.Long(nullable: false),
                    Name = c.String(maxLength: 255),
                    ContentType = c.String(maxLength: 250, unicode: false),
                    ContentLength = c.Long(nullable: false),
                    UserIdImpersonatedBy = c.Long(),
                    UserIdModifiedBy = c.Long(),
                    ModifiedOn = c.DateTime(nullable: false),
                    Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("CustomerPortal.ResourceForm", t => t.ResourceFormId)
                .Index(t => t.ResourceFormId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);

            CreateTable(
                "CustomerPortal.ResourceFormFileContent",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Content = c.Binary(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("CustomerPortal.ResourceFormFile", t => t.Id)
                .Index(t => t.Id);
        }

        public override void Down()
        {

        }
    }
}


