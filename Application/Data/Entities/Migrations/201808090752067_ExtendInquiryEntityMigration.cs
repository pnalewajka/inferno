namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendInquiryEntityMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Sales.Inquiry", new[] { "CustomerId" });
            CreateTable(
                "Sales.InquiryNote",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        InquiryId = c.Long(nullable: false),
                        Content = c.String(nullable: false, maxLength: 3000),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Sales.Inquiry", t => t.InquiryId)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.InquiryId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Sales.OfferType",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OfferName = c.String(nullable: false, maxLength: 100),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("Allocation.Employee", "InquiryNote_Id", c => c.Long());
            AddColumn("Allocation.Employee", "Inquiry_Id", c => c.Long());
            AddColumn("Sales.Inquiry", "BusinessLineOrgUnitId", c => c.Long());
            AddColumn("Sales.Inquiry", "Description", c => c.String(maxLength: 1000));
            AddColumn("Sales.Inquiry", "SalesPersonId", c => c.Long(nullable: false));
            AddColumn("Sales.Inquiry", "ProjectPropability", c => c.Int());
            AddColumn("Sales.Inquiry", "ProjectImportance", c => c.Int(nullable: false));
            AddColumn("Sales.Inquiry", "ExpectedRevenue", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Sales.Inquiry", "ExpectedRevenueCurrencyId", c => c.Long());
            AddColumn("Sales.Inquiry", "ExpectedMargin", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Sales.Inquiry", "MaxPreSalesSpending", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Sales.Inquiry", "OfferDueDate", c => c.DateTime());
            AddColumn("Sales.Inquiry", "OfferTypeId", c => c.Long(nullable: false));
            AlterColumn("Sales.Inquiry", "CustomerId", c => c.Long(nullable: false));
            CreateIndex("Allocation.Employee", "InquiryNote_Id");
            CreateIndex("Allocation.Employee", "Inquiry_Id");
            CreateIndex("Sales.Inquiry", "CustomerId");
            CreateIndex("Sales.Inquiry", "BusinessLineOrgUnitId");
            CreateIndex("Sales.Inquiry", "SalesPersonId");
            CreateIndex("Sales.Inquiry", "ExpectedRevenueCurrencyId");
            CreateIndex("Sales.Inquiry", "OfferTypeId");
            AddForeignKey("Sales.Inquiry", "BusinessLineOrgUnitId", "Organization.OrgUnit", "Id");
            AddForeignKey("Sales.Inquiry", "ExpectedRevenueCurrencyId", "Dictionaries.Currency", "Id");
            AddForeignKey("Allocation.Employee", "InquiryNote_Id", "Sales.InquiryNote", "Id");
            AddForeignKey("Sales.Inquiry", "OfferTypeId", "Sales.OfferType", "Id");
            AddForeignKey("Sales.Inquiry", "SalesPersonId", "Allocation.Employee", "Id");
            AddForeignKey("Allocation.Employee", "Inquiry_Id", "Sales.Inquiry", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Allocation.Employee", "Inquiry_Id", "Sales.Inquiry");
            DropForeignKey("Sales.Inquiry", "SalesPersonId", "Allocation.Employee");
            DropForeignKey("Sales.Inquiry", "OfferTypeId", "Sales.OfferType");
            DropForeignKey("Sales.InquiryNote", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Allocation.Employee", "InquiryNote_Id", "Sales.InquiryNote");
            DropForeignKey("Sales.InquiryNote", "InquiryId", "Sales.Inquiry");
            DropForeignKey("Sales.InquiryNote", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Sales.Inquiry", "ExpectedRevenueCurrencyId", "Dictionaries.Currency");
            DropForeignKey("Sales.Inquiry", "BusinessLineOrgUnitId", "Organization.OrgUnit");
            DropIndex("Sales.InquiryNote", new[] { "UserIdModifiedBy" });
            DropIndex("Sales.InquiryNote", new[] { "UserIdImpersonatedBy" });
            DropIndex("Sales.InquiryNote", new[] { "InquiryId" });
            DropIndex("Sales.Inquiry", new[] { "OfferTypeId" });
            DropIndex("Sales.Inquiry", new[] { "ExpectedRevenueCurrencyId" });
            DropIndex("Sales.Inquiry", new[] { "SalesPersonId" });
            DropIndex("Sales.Inquiry", new[] { "BusinessLineOrgUnitId" });
            DropIndex("Sales.Inquiry", new[] { "CustomerId" });
            DropIndex("Allocation.Employee", new[] { "Inquiry_Id" });
            DropIndex("Allocation.Employee", new[] { "InquiryNote_Id" });
            AlterColumn("Sales.Inquiry", "CustomerId", c => c.Long());
            DropColumn("Sales.Inquiry", "OfferTypeId");
            DropColumn("Sales.Inquiry", "OfferDueDate");
            DropColumn("Sales.Inquiry", "MaxPreSalesSpending");
            DropColumn("Sales.Inquiry", "ExpectedMargin");
            DropColumn("Sales.Inquiry", "ExpectedRevenueCurrencyId");
            DropColumn("Sales.Inquiry", "ExpectedRevenue");
            DropColumn("Sales.Inquiry", "ProjectImportance");
            DropColumn("Sales.Inquiry", "ProjectPropability");
            DropColumn("Sales.Inquiry", "SalesPersonId");
            DropColumn("Sales.Inquiry", "Description");
            DropColumn("Sales.Inquiry", "BusinessLineOrgUnitId");
            DropColumn("Allocation.Employee", "Inquiry_Id");
            DropColumn("Allocation.Employee", "InquiryNote_Id");
            DropTable("Sales.OfferType");
            DropTable("Sales.InquiryNote");
            CreateIndex("Sales.Inquiry", "CustomerId");
        }
    }
}
