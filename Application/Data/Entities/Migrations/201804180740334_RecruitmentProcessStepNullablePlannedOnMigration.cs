namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecruitmentProcessStepNullablePlannedOnMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.RecruitmentProcessStep", "PlannedOn", c => c.DateTime());
        }

        public override void Down()
        {
            AlterColumn("Recruitment.RecruitmentProcessStep", "PlannedOn", c => c.DateTime(nullable: false));
        }
    }
}
