namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RemoveNotNeededColumnMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "ContractType");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "ContractType", c => c.Int(nullable: false));
        }
    }
}
