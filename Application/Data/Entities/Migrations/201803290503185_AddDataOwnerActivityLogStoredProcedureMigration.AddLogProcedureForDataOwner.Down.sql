﻿SET XACT_ABORT ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogDataOwnerDataActivity]'))
   DROP PROCEDURE [GDPR].[LogDataOwnerDataActivity]
GO