namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustTimeReportMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.TimeReport", "UserIdImpersonatedBy", c => c.Long());
            AddColumn("TimeTracking.TimeReport", "UserIdModifiedBy", c => c.Long());
            CreateIndex("TimeTracking.TimeReport", "UserIdImpersonatedBy");
            CreateIndex("TimeTracking.TimeReport", "UserIdModifiedBy");
            AddForeignKey("TimeTracking.TimeReport", "UserIdImpersonatedBy", "Accounts.User", "Id");
            AddForeignKey("TimeTracking.TimeReport", "UserIdModifiedBy", "Accounts.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("TimeTracking.TimeReport", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("TimeTracking.TimeReport", "UserIdImpersonatedBy", "Accounts.User");
            DropIndex("TimeTracking.TimeReport", new[] { "UserIdModifiedBy" });
            DropIndex("TimeTracking.TimeReport", new[] { "UserIdImpersonatedBy" });
            DropColumn("TimeTracking.TimeReport", "UserIdModifiedBy");
            DropColumn("TimeTracking.TimeReport", "UserIdImpersonatedBy");
        }
    }
}
