namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixMinimumOvertimeBankBalanceTypoMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Allocation.Employee", "MinimunOvertimeBankBalance", "MinimumOvertimeBankBalance");
        }
        
        public override void Down()
        {
            RenameColumn("Allocation.Employee", "MinimumOvertimeBankBalance", "MinimunOvertimeBankBalance");
        }
    }
}
