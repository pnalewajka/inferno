namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterRecruitmentProcessMigration1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin", "Id");

            Sql("INSERT INTO [Recruitment].[ApplicationOrigin] (Name, OriginType, ModifiedOn) VALUES ('Researching', 8, GETDATE())");
            Sql("INSERT INTO [Recruitment].[ApplicationOrigin] (Name, OriginType, ModifiedOn) VALUES ('Direct Search', 9, GETDATE())");
            Sql("INSERT INTO [Recruitment].[ApplicationOrigin] (Name, OriginType, ModifiedOn) VALUES ('Other', 1001, GETDATE())");
            Sql("INSERT INTO [Recruitment].[ApplicationOrigin] (Name, OriginType, ModifiedOn) VALUES ('Other', 1002, GETDATE())");
            Sql("INSERT INTO [Recruitment].[ApplicationOrigin] (Name, OriginType, ModifiedOn) VALUES ('Other', 1003, GETDATE())");
            Sql("UPDATE[Recruitment].[ApplicationOrigin] SET Name = 'Event' WHERE Name = 'Job Fair / College'");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId", "Recruitment.ApplicationOrigin");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalEmploymentSourceId" });
            DropColumn("Recruitment.RecruitmentProcess", "FinalEmploymentSourceId");

            Sql("DELETE FROM[Recruitment].[ApplicationOrigin] WHERE Name = 'Researching' AND OriginType = 8");
            Sql("DELETE FROM[Recruitment].[ApplicationOrigin] WHERE Name = 'Direct Search' AND OriginType = 9");
            Sql("DELETE FROM[Recruitment].[ApplicationOrigin] WHERE Name = 'Other' AND OriginType = 1001");
            Sql("DELETE FROM[Recruitment].[ApplicationOrigin] WHERE Name = 'Other' AND OriginType = 1002");
            Sql("DELETE FROM[Recruitment].[ApplicationOrigin] WHERE Name = 'Other' AND OriginType = 1003");
            Sql("UPDATE[Recruitment].[ApplicationOrigin] SET Name = 'Job Fair / College' WHERE Name = 'Event'");
        }
    }
}
