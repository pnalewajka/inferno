namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FrontDeskAssigneeMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId", c => c.Long());
            CreateIndex("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId");
            AddForeignKey("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId", "Allocation.Employee");
            DropIndex("BusinessTrips.BusinessTrip", new[] { "FrontDeskAssigneeEmployeeId" });
            DropColumn("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId");
        }
    }
}
