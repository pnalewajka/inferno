namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationToDataOwnerMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            CreateIndex("GDPR.DataOwner", "CandidateId", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            CreateIndex("GDPR.DataOwner", "CandidateId");
        }
    }
}
