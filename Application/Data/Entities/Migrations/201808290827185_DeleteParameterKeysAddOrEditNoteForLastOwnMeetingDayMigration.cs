namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;
    
    public partial class DeleteParameterKeysAddOrEditNoteForLastOwnMeetingDayMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808290827185_DeleteParameterKeysAddOrEditNoteForLastOwnMeetingDayMigration.DeleteRowSql.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}