﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AccommodationLimitMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("BusinessTrips.DailyAllowance", "AccommodationLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803191045020_AccommodationLimitMigration.InitialValues.Up.sql");
        }

        public override void Down()
        {
            DropColumn("BusinessTrips.DailyAllowance", "AccommodationLimit");
        }
    }
}

