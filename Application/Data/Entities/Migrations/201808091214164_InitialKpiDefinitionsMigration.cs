namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialKpiDefinitionsMigration : DbMigration
    {
        public override void Up()
        {
            SqlResource("Smt.Atomic.Data.Entities.Migrations.201808091214164_InitialKpiDefinitionsMigration.Up.sql");
        }

        public override void Down()
        {

        }
    }
}
