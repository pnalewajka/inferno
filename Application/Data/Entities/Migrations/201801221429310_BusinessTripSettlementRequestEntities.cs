namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusinessTripSettlementRequestEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Workflows.BusinessTripSettlementRequest",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        DepartureDate = c.DateTime(),
                        ArrivalDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
            CreateTable(
                "Workflows.BusinessTripSettlementRequestAbroadTimeEntry",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BusinessTripSettlementRequestId = c.Long(nullable: false),
                        DepartureDateTime = c.DateTime(),
                        DepartureCountryId = c.Long(),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.Country", t => t.DepartureCountryId)
                .ForeignKey("Workflows.BusinessTripSettlementRequest", t => t.BusinessTripSettlementRequestId, cascadeDelete: true)
                .Index(t => t.BusinessTripSettlementRequestId)
                .Index(t => t.DepartureCountryId);
            
            CreateTable(
                "Workflows.BusinessTripSettlementRequestAdvancePayment",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("BusinessTrips.Currency", t => t.CurrencyId)
                .ForeignKey("Workflows.BusinessTripSettlementRequest", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "Workflows.BusinessTripSettlementRequestCompanyCost",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BusinessTripSettlementRequestId = c.Long(nullable: false),
                        Description = c.String(nullable: false),
                        TransactionDate = c.DateTime(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentMethod = c.Int(nullable: false),
                        CardHolderEmployeeId = c.Long(),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Allocation.Employee", t => t.CardHolderEmployeeId)
                .ForeignKey("BusinessTrips.Currency", t => t.CurrencyId)
                .ForeignKey("Workflows.BusinessTripSettlementRequest", t => t.BusinessTripSettlementRequestId, cascadeDelete: true)
                .Index(t => t.BusinessTripSettlementRequestId)
                .Index(t => t.CurrencyId)
                .Index(t => t.CardHolderEmployeeId);
            
            CreateTable(
                "Workflows.BusinessTripSettlementRequestMeal",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BusinessTripSettlementRequestId = c.Long(nullable: false),
                        CountryId = c.Long(nullable: false),
                        Breakfast = c.Long(nullable: false),
                        Lunch = c.Long(nullable: false),
                        Dinner = c.Long(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.Country", t => t.CountryId)
                .ForeignKey("Workflows.BusinessTripSettlementRequest", t => t.BusinessTripSettlementRequestId, cascadeDelete: true)
                .Index(t => new { t.BusinessTripSettlementRequestId, t.CountryId }, unique: true, name: "IX_SingleMealsEntryPerCountry");
            
            CreateTable(
                "Workflows.BusinessTripSettlementRequestOwnCost",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BusinessTripSettlementRequestId = c.Long(nullable: false),
                        Description = c.String(nullable: false),
                        TransactionDate = c.DateTime(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Long(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("BusinessTrips.Currency", t => t.CurrencyId)
                .ForeignKey("Workflows.BusinessTripSettlementRequest", t => t.BusinessTripSettlementRequestId, cascadeDelete: true)
                .Index(t => t.BusinessTripSettlementRequestId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "Workflows.BusinessTripSettlementRequestVehicleMileage",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        VehicleType = c.Int(nullable: false),
                        IsCarEngineCapacityOver900ccc = c.Boolean(nullable: false),
                        RegistrationNumber = c.String(nullable: false),
                        OutboundFromCityId = c.Long(nullable: false),
                        OutboundToCityId = c.Long(nullable: false),
                        OutboundKilometers = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InboundFromCityId = c.Long(nullable: false),
                        InboundToCityId = c.Long(nullable: false),
                        InboundKilometers = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.City", t => t.InboundFromCityId)
                .ForeignKey("Dictionaries.City", t => t.InboundToCityId)
                .ForeignKey("Dictionaries.City", t => t.OutboundFromCityId)
                .ForeignKey("Dictionaries.City", t => t.OutboundToCityId)
                .ForeignKey("Workflows.BusinessTripSettlementRequest", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.OutboundFromCityId)
                .Index(t => t.OutboundToCityId)
                .Index(t => t.InboundFromCityId)
                .Index(t => t.InboundToCityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequest", "Id", "Workflows.Request");
            DropForeignKey("Workflows.BusinessTripSettlementRequestVehicleMileage", "Id", "Workflows.BusinessTripSettlementRequest");
            DropForeignKey("Workflows.BusinessTripSettlementRequestVehicleMileage", "OutboundToCityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripSettlementRequestVehicleMileage", "OutboundFromCityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripSettlementRequestVehicleMileage", "InboundToCityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripSettlementRequestVehicleMileage", "InboundFromCityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripSettlementRequestOwnCost", "BusinessTripSettlementRequestId", "Workflows.BusinessTripSettlementRequest");
            DropForeignKey("Workflows.BusinessTripSettlementRequestOwnCost", "CurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Workflows.BusinessTripSettlementRequestMeal", "BusinessTripSettlementRequestId", "Workflows.BusinessTripSettlementRequest");
            DropForeignKey("Workflows.BusinessTripSettlementRequestMeal", "CountryId", "Dictionaries.Country");
            DropForeignKey("Workflows.BusinessTripSettlementRequestCompanyCost", "BusinessTripSettlementRequestId", "Workflows.BusinessTripSettlementRequest");
            DropForeignKey("Workflows.BusinessTripSettlementRequestCompanyCost", "CurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", "Allocation.Employee");
            DropForeignKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "Id", "Workflows.BusinessTripSettlementRequest");
            DropForeignKey("Workflows.BusinessTripSettlementRequestAdvancePayment", "CurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "BusinessTripSettlementRequestId", "Workflows.BusinessTripSettlementRequest");
            DropForeignKey("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCountryId", "Dictionaries.Country");
            DropIndex("Workflows.BusinessTripSettlementRequestVehicleMileage", new[] { "InboundToCityId" });
            DropIndex("Workflows.BusinessTripSettlementRequestVehicleMileage", new[] { "InboundFromCityId" });
            DropIndex("Workflows.BusinessTripSettlementRequestVehicleMileage", new[] { "OutboundToCityId" });
            DropIndex("Workflows.BusinessTripSettlementRequestVehicleMileage", new[] { "OutboundFromCityId" });
            DropIndex("Workflows.BusinessTripSettlementRequestVehicleMileage", new[] { "Id" });
            DropIndex("Workflows.BusinessTripSettlementRequestOwnCost", new[] { "CurrencyId" });
            DropIndex("Workflows.BusinessTripSettlementRequestOwnCost", new[] { "BusinessTripSettlementRequestId" });
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", "IX_SingleMealsEntryPerCountry");
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CurrencyId" });
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "BusinessTripSettlementRequestId" });
            DropIndex("Workflows.BusinessTripSettlementRequestAdvancePayment", new[] { "CurrencyId" });
            DropIndex("Workflows.BusinessTripSettlementRequestAdvancePayment", new[] { "Id" });
            DropIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", new[] { "DepartureCountryId" });
            DropIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", new[] { "BusinessTripSettlementRequestId" });
            DropIndex("Workflows.BusinessTripSettlementRequest", new[] { "Id" });
            DropTable("Workflows.BusinessTripSettlementRequestVehicleMileage");
            DropTable("Workflows.BusinessTripSettlementRequestOwnCost");
            DropTable("Workflows.BusinessTripSettlementRequestMeal");
            DropTable("Workflows.BusinessTripSettlementRequestCompanyCost");
            DropTable("Workflows.BusinessTripSettlementRequestAdvancePayment");
            DropTable("Workflows.BusinessTripSettlementRequestAbroadTimeEntry");
            DropTable("Workflows.BusinessTripSettlementRequest");
        }
    }
}
