namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeCustomerOptionalForJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.JobOpening", new[] { "CustomerId" });
            AlterColumn("Recruitment.JobOpening", "CustomerId", c => c.Long());
            CreateIndex("Recruitment.JobOpening", "CustomerId");
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.JobOpening", new[] { "CustomerId" });
            AlterColumn("Recruitment.JobOpening", "CustomerId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.JobOpening", "CustomerId");
        }
    }
}
