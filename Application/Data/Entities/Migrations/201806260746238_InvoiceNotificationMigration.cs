namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InvoiceNotificationMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.TimeReport", "InvoiceNotificationStatus", c => c.Int(nullable: false));
            AddColumn("TimeTracking.TimeReport", "InvoiceNotificationErrorMessage", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("TimeTracking.TimeReport", "InvoiceNotificationErrorMessage");
            DropColumn("TimeTracking.TimeReport", "InvoiceNotificationStatus");
        }
    }
}
