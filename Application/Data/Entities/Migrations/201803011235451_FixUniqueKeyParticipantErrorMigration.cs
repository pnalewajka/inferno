﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class FixUniqueKeyParticipantErrorMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803011235451_FixUniqueKeyParticipantErrorMigration.RemoveSettlementRequestSql.Up.sql");
            DropIndex("Workflows.BusinessTripSettlementRequest", new[] { "BusinessTripParticipantId" });
            CreateIndex("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("Workflows.BusinessTripSettlementRequest", new[] { "BusinessTripParticipantId" });
            CreateIndex("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803011235451_FixUniqueKeyParticipantErrorMigration.RemoveSettlementRequestSql.Down.sql");
        }
    }
}

