namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdvancePaymentAcceptingEmployeeOnCompanyMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId", c => c.Long());
            CreateIndex("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId");
            AddForeignKey("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId", "Allocation.Employee");
            DropIndex("Dictionaries.Company", new[] { "AdvancePaymentAcceptingEmployeeId" });
            DropColumn("Dictionaries.Company", "AdvancePaymentAcceptingEmployeeId");
        }
    }
}
