﻿INSERT INTO Recruitment.InboundEmailReaders(InboundEmailId, UserId)
SELECT IE.Id AS InboundEmailId, U.Id AS UserId
FROM Recruitment.InboundEmail AS IE
INNER JOIN Accounts.[User] AS U
ON IE.[To] = U.Email
WHERE IE.ReceivedOn < DATEADD(DAY,-1, CAST(GETDATE() AS DATE)) AND  
NOT EXISTS
(SELECT InboundEmailId, UserId
FROM Recruitment.InboundEmailReaders AS IER
WHERE IER.InboundEmailId = IE.Id AND IER.UserId = U.Id)