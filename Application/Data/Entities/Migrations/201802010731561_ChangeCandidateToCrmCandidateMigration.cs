namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCandidateToCrmCandidateMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Crm.Candidate", newName: "CrmCandidate");
        }
        
        public override void Down()
        {
            RenameTable(name: "Crm.CrmCandidate", newName: "Candidate");
        }
    }
}
