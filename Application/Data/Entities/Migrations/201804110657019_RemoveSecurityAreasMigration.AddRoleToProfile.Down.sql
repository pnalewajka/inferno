﻿SET XACT_ABORT ON
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [Accounts].[atomic_AddRoleToProfile]
 	@roleName VARCHAR(255),
	@profileName VARCHAR(255)
AS
BEGIN
	DECLARE @roleId BIGINT
	SELECT @roleId = [Id] FROM [Accounts].[SecurityRole] WHERE [Name] = @roleName
	DECLARE @profileId BIGINT
	SELECT @profileId = [Id] FROM [Accounts].[SecurityProfile] WHERE [Name] = @profileName
	IF @profileId IS NULL
	BEGIN
		DECLARE @errorMsg VARCHAR(700) = ''
		SET @errorMsg = @errorMsg + CASE WHEN (LEN(@errorMsg)>0) THEN ' ' ELSE '' END + 'Profile: ' + ISNULL(@roleName,'') + ' is missing'
		RAISERROR(@errorMsg, 16, 1);
	END
	IF @roleId IS NULL
	BEGIN
		SELECT @roleId = COALESCE(MAX([Id]), 0) + 1 FROM [Accounts].[SecurityRole]
		INSERT INTO [Accounts].[SecurityRole] ([Id], [Name]) VALUES (@roleId, @roleName)
		INSERT INTO [Accounts].[SecurityAreaSecurityRoles] ([AreaId], [RoleId])
			SELECT DISTINCT sasr.[AreaId] AS [AreaId], @roleId AS [RoleId]
				FROM [Accounts].[SecurityAreaSecurityRoles] sasr
				JOIN [Accounts].[SecurityRoleSecurityProfiles] srsp ON sasr.RoleId = srsp.RoleId
				WHERE srsp.[ProfileId] = @profileId
		IF NOT EXISTS (SELECT * FROM [Accounts].[SecurityAreaSecurityRoles] WHERE [RoleId] = @roleId)
		BEGIN
			INSERT INTO [Accounts].[SecurityAreaSecurityRoles] ([AreaId], [RoleId])
				SELECT [Id] AS [AreaId], @roleId AS [RoleId] FROM [Accounts].[SecurityArea]
		END
	END

	MERGE 
		INTO [Accounts].[SecurityRoleSecurityProfiles] AS target
		USING ( VALUES(@roleId, @profileId) ) AS source ([RoleId],[ProfileId])
		ON target.[RoleId] = source.[RoleId] AND target.[ProfileId] = source.[ProfileId]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ([RoleId], [ProfileId]) VALUES (source.[RoleId], source.[ProfileId]) ;

END
GO

