namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCloneOfStepMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcessStep", "CloneOfStepId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcessStep", "CloneOfStepId");
            AddForeignKey("Recruitment.RecruitmentProcessStep", "CloneOfStepId", "Recruitment.RecruitmentProcessStep", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcessStep", "CloneOfStepId", "Recruitment.RecruitmentProcessStep");
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "CloneOfStepId" });
            DropColumn("Recruitment.RecruitmentProcessStep", "CloneOfStepId");
        }
    }
}
