namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLastActivityByToRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "LastActivityById", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcess", "LastActivityById");
            AddForeignKey("Recruitment.RecruitmentProcess", "LastActivityById", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "LastActivityById", "Allocation.Employee");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "LastActivityById" });
            DropColumn("Recruitment.RecruitmentProcess", "LastActivityById");
        }
    }
}
