﻿DELETE FROM [Configuration].[SystemParameter]
      WHERE [Key] IN
            (
                N'MeetMe.MeetingSynchronization.JiraImportDateFormats',
                N'MeetMe.MeetingSynchronization.JiraMinimumDayDiffrenceBetweenMeetingAndComments',
                N'MeetMe.MeetingSynchronization.JiraEmployeeIdsForCommentsIgnorance',
                N'MeetMe.MeetingSynchronization.JiraPhrasesForCommentsIgnorance',
                N'MeetMe.MeetingSynchronization.JiraMaxCommentLengthForIgnorance',
                N'MeetMe.MeetingSynchronization.JiraDiffrenceDayBetweenMinAndMaxOfComments'
            )

DECLARE @jobId BIGINT = (SELECT [Id] FROM [Scheduling].[JobDefinition] WHERE [Code] = N'JiraMeetMeSynchronizationJob')

DELETE FROM [Runtime].[JobRunInfo] WHERE [JobDefinitionId] = @jobId
DELETE FROM [Scheduling].[JobLog] WHERE [JobDefinitionId] = @jobId
DELETE FROM [Scheduling].[JobDefinition] WHERE [Id] = @jobId
DELETE FROM [Scheduling].[Pipeline] WHERE [Name] = N'MeetMe Synchronization'
