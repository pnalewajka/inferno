﻿SET XACT_ABORT ON
GO

DELETE FROM [GDPR].[DataConsent] WHERE DataOwnerId IN
(SELECT Id FROM [GDPR].[DataOwner] WHERE RecommendingPersonId IS NOT NULL)
GO

DELETE FROM [GDPR].[DataActivity] WHERE DataOwnerId IN
(SELECT Id FROM [GDPR].[DataOwner] WHERE RecommendingPersonId IS NOT NULL)
GO

DELETE FROM [GDPR].[DataOwner] WHERE RecommendingPersonId IS NOT NULL
GO

DELETE FROM [Recruitment].[RecommendingPerson]
GO