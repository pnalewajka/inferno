namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CitiesInAbroadTimeEntriesMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.ContractType", "HasToSpecifyDestinationType", c => c.Boolean(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCityId", c => c.Long());
            AddColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DestinationType", c => c.Int());
            CreateIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCityId");
            AddForeignKey("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCityId", "Dictionaries.City", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCityId", "Dictionaries.City");
            DropIndex("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", new[] { "DepartureCityId" });
            DropColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DestinationType");
            DropColumn("Workflows.BusinessTripSettlementRequestAbroadTimeEntry", "DepartureCityId");
            DropColumn("TimeTracking.ContractType", "HasToSpecifyDestinationType");
        }
    }
}
