namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration140620181357 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("Recruitment.TechnicalReview", "VerifiedByEmployeeId", "Allocation.Employee");
            //DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            //RenameColumn(table: "Recruitment.RecruitmentProcessStep", name: "DecisionMakerId", newName: "AssignedEmployeeId");
            //RenameIndex(table: "Recruitment.RecruitmentProcessStep", name: "IX_DecisionMakerId", newName: "IX_AssignedEmployeeId");
            //CreateTable(
            //    "Recruitment.InboundEmailDataActivity",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            InboundEmailId = c.Long(nullable: false),
            //            PerformedOn = c.DateTime(nullable: false),
            //            PerformedByUserId = c.Long(nullable: false),
            //            ActivityType = c.Int(nullable: false),
            //            ReferenceId = c.Long(),
            //            ReferenceType = c.Int(),
            //            S1 = c.String(maxLength: 50),
            //            S2 = c.String(maxLength: 50),
            //            S3 = c.String(maxLength: 300),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.InboundEmail", t => t.InboundEmailId)
            //    .ForeignKey("Accounts.User", t => t.PerformedByUserId)
            //    .Index(t => t.InboundEmailId)
            //    .Index(t => t.PerformedByUserId);
            
            //CreateTable(
            //    "Resumes.ResumeShareToken",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            SharerEmployeeId = c.Long(nullable: false),
            //            OwnerEmployeeId = c.Long(nullable: false),
            //            Reason = c.String(nullable: false),
            //            Token = c.Guid(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Allocation.Employee", t => t.OwnerEmployeeId)
            //    .ForeignKey("Allocation.Employee", t => t.SharerEmployeeId)
            //    .Index(t => new { t.SharerEmployeeId, t.OwnerEmployeeId }, unique: true, name: "IX_SharerOwner")
            //    .Index(t => t.Token, unique: true, name: "IX_UniqueToken");
            
            //AddColumn("Recruitment.JobOpening", "TechnicalReviewComment", c => c.String(maxLength: 300));
            //AddColumn("Recruitment.RecruitmentProcessStep", "DueOn", c => c.DateTime());
            //AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String(maxLength: 50));
            //AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String(maxLength: 50));
            //AlterColumn("Recruitment.Candidate", "OriginalSourceComment", c => c.String(maxLength: 400));
            //AlterColumn("Recruitment.CandidateNote", "Content", c => c.String(maxLength: 4000));
            //AlterColumn("GDPR.DataActivity", "S3", c => c.String(maxLength: 300));
            //AlterColumn("Recruitment.JobApplication", "OriginComment", c => c.String(maxLength: 400));
            //AlterColumn("Recruitment.RecruitmentProcess", "OfferSalary", c => c.String(maxLength: 150));
            //AlterColumn("Recruitment.RecruitmentProcess", "OfferStartDate", c => c.String(maxLength: 130));
            //AlterColumn("Recruitment.RecruitmentProcess", "OfferComment", c => c.String(maxLength: 500));
            //AlterColumn("Recruitment.JobOpening", "PositionName", c => c.String(maxLength: 400));
            //AlterColumn("Recruitment.JobOpening", "NoticePeriod", c => c.String(maxLength: 120));
            //AlterColumn("Recruitment.JobOpening", "SalaryRate", c => c.String(maxLength: 130));
            //AlterColumn("Recruitment.JobOpening", "RejectionReason", c => c.String(maxLength: 550));
            //AlterColumn("Recruitment.JobOpeningNote", "Content", c => c.String(maxLength: 4000));
            //AlterColumn("Recruitment.RecruitmentProcessNote", "Content", c => c.String(maxLength: 4000));
            //DropColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            //DropColumn("Recruitment.TechnicalReview", "VerifiedOn");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.TechnicalReview", "VerifiedOn", c => c.DateTime());
            //AddColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long());
            //DropForeignKey("Resumes.ResumeShareToken", "SharerEmployeeId", "Allocation.Employee");
            //DropForeignKey("Resumes.ResumeShareToken", "OwnerEmployeeId", "Allocation.Employee");
            //DropForeignKey("Recruitment.InboundEmailDataActivity", "PerformedByUserId", "Accounts.User");
            //DropForeignKey("Recruitment.InboundEmailDataActivity", "InboundEmailId", "Recruitment.InboundEmail");
            //DropIndex("Resumes.ResumeShareToken", "IX_UniqueToken");
            //DropIndex("Resumes.ResumeShareToken", "IX_SharerOwner");
            //DropIndex("Recruitment.InboundEmailDataActivity", new[] { "PerformedByUserId" });
            //DropIndex("Recruitment.InboundEmailDataActivity", new[] { "InboundEmailId" });
            //AlterColumn("Recruitment.RecruitmentProcessNote", "Content", c => c.String(maxLength: 1024));
            //AlterColumn("Recruitment.JobOpeningNote", "Content", c => c.String(maxLength: 1024));
            //AlterColumn("Recruitment.JobOpening", "RejectionReason", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.JobOpening", "SalaryRate", c => c.String(maxLength: 300));
            //AlterColumn("Recruitment.JobOpening", "NoticePeriod", c => c.String(maxLength: 1024));
            //AlterColumn("Recruitment.JobOpening", "PositionName", c => c.String(maxLength: 250));
            //AlterColumn("Recruitment.RecruitmentProcess", "OfferComment", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.RecruitmentProcess", "OfferStartDate", c => c.String(maxLength: 50));
            //AlterColumn("Recruitment.RecruitmentProcess", "OfferSalary", c => c.String(maxLength: 50));
            //AlterColumn("Recruitment.JobApplication", "OriginComment", c => c.String(maxLength: 250));
            //AlterColumn("GDPR.DataActivity", "S3", c => c.String(maxLength: 50));
            //AlterColumn("Recruitment.CandidateNote", "Content", c => c.String(maxLength: 1024));
            //AlterColumn("Recruitment.Candidate", "OriginalSourceComment", c => c.String(maxLength: 50));
            //AlterColumn("Recruitment.Candidate", "OtherPhone", c => c.String(maxLength: 20));
            //AlterColumn("Recruitment.Candidate", "MobilePhone", c => c.String(maxLength: 20));
            //DropColumn("Recruitment.RecruitmentProcessStep", "DueOn");
            //DropColumn("Recruitment.JobOpening", "TechnicalReviewComment");
            //DropTable("Resumes.ResumeShareToken");
            //DropTable("Recruitment.InboundEmailDataActivity");
            //RenameIndex(table: "Recruitment.RecruitmentProcessStep", name: "IX_AssignedEmployeeId", newName: "IX_DecisionMakerId");
            //RenameColumn(table: "Recruitment.RecruitmentProcessStep", name: "AssignedEmployeeId", newName: "DecisionMakerId");
            //CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            //AddForeignKey("Recruitment.TechnicalReview", "VerifiedByEmployeeId", "Allocation.Employee", "Id");
        }
    }
}
