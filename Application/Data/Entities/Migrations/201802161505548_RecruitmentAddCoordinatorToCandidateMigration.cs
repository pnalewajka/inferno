namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentAddCoordinatorToCandidateMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "CoordinatorId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.Candidate", "CoordinatorId");
            AddForeignKey("Recruitment.Candidate", "CoordinatorId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.Candidate", "CoordinatorId", "Allocation.Employee");
            DropIndex("Recruitment.Candidate", new[] { "CoordinatorId" });
            DropColumn("Recruitment.Candidate", "CoordinatorId");
        }
    }
}
