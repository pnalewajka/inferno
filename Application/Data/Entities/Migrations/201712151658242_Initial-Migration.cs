namespace Smt.Atomic.Data.Entities.Migrations
{
	using Smt.Atomic.Data.Entities.Helpers;
	using System;
	using System.Data.Entity.Migrations;

	public partial class InitialMigration : AtomicDbMigration
	{
		public override void Up()
		{
			CreateTable(
				"TimeTracking.AbsenceCalendarEntry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ExchangeId = c.String(maxLength: 255),
					RequestId = c.Long(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.ExchangeId, unique: true)
				.Index(t => t.RequestId);

			CreateTable(
				"Workflows.Request",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					RequestingUserId = c.Long(nullable: false),
					Status = c.Int(nullable: false),
					RequestType = c.Int(nullable: false),
					DescriptionEn = c.String(maxLength: 512),
					DescriptionPl = c.String(maxLength: 512),
					RejectionReason = c.String(maxLength: 255),
					DeferredUntil = c.DateTime(),
					Exception = c.String(maxLength: 512),
					CreatedById = c.Long(),
					ImpersonatedCreatedById = c.Long(),
					CreatedOn = c.DateTime(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.RequestingUserId)
				.Index(t => t.RequestingUserId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.AbsenceRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					AffectedUserId = c.Long(nullable: false),
					AbsenceTypeId = c.Long(nullable: false),
					From = c.DateTime(nullable: false),
					To = c.DateTime(nullable: false),
					Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SubstituteUserId = c.Long(),
					Comment = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.AbsenceType", t => t.AbsenceTypeId)
				.ForeignKey("Accounts.User", t => t.AffectedUserId)
				.ForeignKey("Accounts.User", t => t.SubstituteUserId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.AffectedUserId)
				.Index(t => t.AbsenceTypeId)
				.Index(t => t.SubstituteUserId);

			CreateTable(
				"TimeTracking.AbsenceType",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(nullable: false, maxLength: 32, unicode: false),
					NameEn = c.String(nullable: false, maxLength: 255),
					NamePl = c.String(nullable: false, maxLength: 255),
					DescriptionEn = c.String(),
					DescriptionPl = c.String(),
					EmployeeInstructionEn = c.String(maxLength: 512),
					EmployeeInstructionPl = c.String(maxLength: 512),
					KbrCode = c.String(maxLength: 255),
					IsVacation = c.Boolean(nullable: false),
					IsTimeOffForOvertime = c.Boolean(nullable: false),
					RequiresHRApproval = c.Boolean(nullable: false),
					AllowHourlyReporting = c.Boolean(nullable: false),
					Order = c.Int(nullable: false),
					UtilizationCategoryId = c.Long(),
					AbsenceProjectId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Project", t => t.AbsenceProjectId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.AbsenceProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.Project",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					JiraIssueKey = c.String(maxLength: 256),
					JiraKey = c.String(maxLength: 256),
					PetsCode = c.String(maxLength: 256),
					Apn = c.String(maxLength: 256),
					Region = c.String(maxLength: 60),
					KupferwerkProjectId = c.String(maxLength: 256),
					ProjectType = c.Int(),
					ProjectFeatures = c.Int(nullable: false),
					Description = c.String(),
					ClientShortName = c.String(maxLength: 128),
					ProjectShortName = c.String(maxLength: 128),
					OrgUnitId = c.Long(nullable: false),
					ProjectManagerId = c.Long(),
					SalesAccountManagerId = c.Long(),
					SupervisorManagerId = c.Long(),
					StartDate = c.DateTime(),
					EndDate = c.DateTime(),
					ProjectStatus = c.Int(nullable: false),
					CalendarId = c.Long(),
					Color = c.String(),
					GenericName = c.String(),
					PictureId = c.Long(),
					IsAvailableForSalesPresentation = c.Boolean(nullable: false),
					CurrentPhase = c.Int(nullable: false),
					Disclosures = c.Int(nullable: false),
					HasClientReferences = c.Boolean(nullable: false),
					CustomerSizeId = c.Long(),
					BusinessCase = c.String(),
					TimeTrackingImportSourceId = c.Long(),
					JiraIssueToTimeReportRowMapperId = c.Long(nullable: false),
					AllowedOvertimeTypes = c.Int(nullable: false),
					IsApprovedByRequesterMainManager = c.Boolean(nullable: false),
					IsApprovedByMainManagerForProjectManager = c.Boolean(nullable: false),
					TimeTrackingType = c.Int(nullable: false),
					UtilizationCategoryId = c.Long(),
					ReportingStartsOn = c.DateTime(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Configuration.Calendar", t => t.CalendarId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("TimeTracking.JiraIssueToTimeReportRowMapper", t => t.JiraIssueToTimeReportRowMapperId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
				.ForeignKey("Allocation.ProjectPicture", t => t.PictureId)
				.ForeignKey("Allocation.Employee", t => t.ProjectManagerId)
				.ForeignKey("Allocation.Employee", t => t.SalesAccountManagerId)
				.ForeignKey("Allocation.Employee", t => t.SupervisorManagerId)
				.ForeignKey("TimeTracking.TimeTrackingImportSource", t => t.TimeTrackingImportSourceId)
				.ForeignKey("Dictionaries.UtilizationCategory", t => t.UtilizationCategoryId)
				.Index(t => new { t.ClientShortName, t.ProjectShortName }, unique: true, clustered: false, name: "IX_ProjectNamePerClient")
				.Index(t => t.OrgUnitId)
				.Index(t => t.ProjectManagerId)
				.Index(t => t.SalesAccountManagerId)
				.Index(t => t.SupervisorManagerId)
				.Index(t => t.CalendarId)
				.Index(t => t.PictureId)
				.Index(t => t.TimeTrackingImportSourceId)
				.Index(t => t.JiraIssueToTimeReportRowMapperId)
				.Index(t => t.UtilizationCategoryId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.AllocationRequest",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					StartDate = c.DateTime(nullable: false),
					EndDate = c.DateTime(),
					ContentType = c.Int(nullable: false),
					AllocationCertainty = c.Int(nullable: false),
					MondayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					TuesdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					WednesdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					ThursdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					FridayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SaturdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SundayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					CreatedById = c.Long(),
					ImpersonatedCreatedById = c.Long(),
					CreatedOn = c.DateTime(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.Index(t => t.EmployeeId)
				.Index(t => t.ProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.AllocationRequestMetadata",
				c => new
				{
					AllocationRequestId = c.Long(nullable: false),
					NextRebuildTime = c.DateTime(),
					ProcessingStatus = c.Int(nullable: false),
					Id = c.Long(nullable: false, identity: true),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.AllocationRequestId)
				.ForeignKey("Allocation.AllocationRequest", t => t.AllocationRequestId, cascadeDelete: true)
				.Index(t => t.AllocationRequestId);

			CreateTable(
				"Allocation.Employee",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					FirstName = c.String(maxLength: 255),
					LastName = c.String(maxLength: 255),
					Acronym = c.String(maxLength: 15),
					Email = c.String(maxLength: 255, unicode: false),
					DateOfBirth = c.DateTime(),
					PersonalNumber = c.String(maxLength: 32),
					PersonalNumberType = c.Int(),
					IdCardNumber = c.String(maxLength: 32),
					PhoneNumber = c.String(maxLength: 32),
					SkypeName = c.String(maxLength: 50),
					HomeAddress = c.String(maxLength: 128),
					AvailabilityDetails = c.String(maxLength: 128),
					LocationId = c.Long(),
					JobTitleId = c.Long(),
					CurrentEmployeeStatus = c.Int(nullable: false),
					IsProjectContributor = c.Boolean(nullable: false),
					IsCoreAsset = c.Boolean(nullable: false),
					PlaceOfWork = c.Int(),
					MinimunOvertimeBankBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
					CompanyId = c.Long(),
					UserId = c.Long(),
					LineManagerId = c.Long(),
					OrgUnitId = c.Long(nullable: false),
					CurrentEmploymentType = c.Int(),
					EmployeeFeatures = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.Location", t => t.LocationId)
				.ForeignKey("Dictionaries.Company", t => t.CompanyId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("SkillManagement.JobTitle", t => t.JobTitleId)
				.ForeignKey("Allocation.Employee", t => t.LineManagerId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
				.ForeignKey("Accounts.User", t => t.UserId)
				.Index(t => t.Email, unique: true)
				.Index(t => t.LocationId)
				.Index(t => t.JobTitleId)
				.Index(t => t.CompanyId)
				.Index(t => t.UserId, unique: true)
				.Index(t => t.LineManagerId)
				.Index(t => t.OrgUnitId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.AllocationDailyRecord",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					AllocationRequestId = c.Long(nullable: false),
					CalendarId = c.Long(nullable: false),
					Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
					Day = c.DateTime(nullable: false),
					IsWorkDay = c.Boolean(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.AllocationRequest", t => t.AllocationRequestId)
				.ForeignKey("Configuration.Calendar", t => t.CalendarId)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.Index(t => t.EmployeeId)
				.Index(t => t.ProjectId)
				.Index(t => t.AllocationRequestId)
				.Index(t => t.CalendarId);

			CreateTable(
				"Configuration.Calendar",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 255, unicode: false),
					Description = c.String(maxLength: 1024),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Configuration.CalendarEntry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EntryOn = c.DateTime(nullable: false),
					Description = c.String(maxLength: 1024),
					CalendarEntryType = c.Int(nullable: false),
					WorkingHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					CalendarId = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Configuration.Calendar", t => t.CalendarId, cascadeDelete: true)
				.Index(t => t.CalendarId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.User",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					FirstName = c.String(maxLength: 255),
					LastName = c.String(maxLength: 255),
					Login = c.String(maxLength: 255, unicode: false),
					Email = c.String(maxLength: 255, unicode: false),
					IsActive = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					LastLoggedOn = c.DateTime(),
					UserLoginTokenIdLoginToken = c.Long(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
					SecurityAreaId = c.Long(nullable: false),
					PictureId = c.Long(),
					Location = c.Geography(),
					LocationRadius = c.Int(),
					IsDeleted = c.Boolean(nullable: false),
					ActiveDirectoryId = c.Guid(),
					CrmId = c.String(maxLength: 64),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.UserLoginToken", t => t.UserLoginTokenIdLoginToken)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.UserPicture", t => t.PictureId)
				.ForeignKey("Accounts.SecurityArea", t => t.SecurityAreaId)
				.Index(t => t.Login, unique: true)
				.Index(t => t.Email, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy)
				.Index(t => t.UserLoginTokenIdLoginToken)
				.Index(t => t.SecurityAreaId)
				.Index(t => t.PictureId)
				.Index(t => t.ActiveDirectoryId, unique: true);

			CreateTable(
				"Allocation.ActiveDirectoryGroup",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					OwnerId = c.Long(),
					ActiveDirectoryId = c.Guid(nullable: false),
					Name = c.String(maxLength: 255),
					GroupType = c.Int(nullable: false),
					Email = c.String(maxLength: 255),
					Description = c.String(maxLength: 1023),
					Info = c.String(maxLength: 1023),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.OwnerId)
				.Index(t => t.OwnerId)
				.Index(t => t.ActiveDirectoryId, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.Credential",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					PasswordHash = c.String(maxLength: 255, unicode: false),
					Salt = c.String(maxLength: 255, unicode: false),
					CreatedOn = c.DateTime(nullable: false),
					LastUsedOn = c.DateTime(),
					IsExpired = c.Boolean(nullable: false),
					UserId = c.Long(nullable: false),
					Email = c.String(maxLength: 255, unicode: false),
					AuthorizationProviderTypeEnumId = c.Int(nullable: false),
					ExternalId = c.String(maxLength: 255, unicode: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.UserId)
				.Index(t => t.UserId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.UserDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					UserId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.UserId)
				.Index(t => t.UserId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.UserDocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.UserDocument", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"Accounts.UserLoginToken",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Content = c.String(maxLength: 64, unicode: false),
					ExpiresOn = c.DateTime(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.UserPicture",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ETag = c.String(),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.UserPictureContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
					Thumbnail = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.UserPicture", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"Accounts.SecurityProfile",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255, unicode: false),
					ActiveDirectoryGroupName = c.String(maxLength: 255),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.SecurityRole",
				c => new
				{
					Id = c.Long(nullable: false),
					Name = c.String(maxLength: 255, unicode: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Accounts.SecurityArea",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 255, unicode: false),
					Description = c.String(maxLength: 500),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.Substitution",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ReplacedUserId = c.Long(nullable: false),
					SubstituteUserId = c.Long(nullable: false),
					From = c.DateTime(nullable: false),
					To = c.DateTime(nullable: false),
					RequestId = c.Long(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.ReplacedUserId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.ForeignKey("Accounts.User", t => t.SubstituteUserId)
				.Index(t => t.ReplacedUserId)
				.Index(t => t.SubstituteUserId)
				.Index(t => t.RequestId);

			CreateTable(
				"TimeTracking.CalendarAbsence",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					Description = c.String(maxLength: 255),
					OfficeGroupEmail = c.String(maxLength: 255),
					UserIdCreatedBy = c.Long(),
					UserIdImpersonatedCreatedBy = c.Long(),
					CreatedOn = c.DateTime(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.OfficeGroupEmail, unique: true)
				.Index(t => t.UserIdCreatedBy)
				.Index(t => t.UserIdImpersonatedCreatedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.Company",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 100),
					Description = c.String(),
					EmailDomain = c.String(maxLength: 40),
					ActiveDirectoryCode = c.String(maxLength: 100),
					NavisionCode = c.String(maxLength: 100),
					CustomOrder = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.ActiveDirectoryCode, unique: true)
				.Index(t => t.CustomOrder, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.AddEmployeeRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					FirstName = c.String(),
					LastName = c.String(),
					Email = c.String(),
					Login = c.String(),
					JobTitleId = c.Long(nullable: false),
					CrmId = c.String(),
					JobProfileId = c.Long(nullable: false),
					OrgUnitId = c.Long(nullable: false),
					LocationId = c.Long(nullable: false),
					PlaceOfWork = c.Int(nullable: false),
					CompanyId = c.Long(nullable: false),
					LineManagerId = c.Long(nullable: false),
					ContractFrom = c.DateTime(nullable: false),
					ContractTo = c.DateTime(),
					ContractSignDate = c.DateTime(nullable: false),
					MondayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					TuesdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					WednesdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					ThursdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					FridayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SaturdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SundayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					ContractTypeId = c.Long(nullable: false),
					ContractDuration = c.Int(),
					VacationHourToDayRatio = c.Decimal(nullable: false, precision: 18, scale: 2),
					TimeReportingMode = c.Int(nullable: false),
					AbsenceOnlyDefaultProjectId = c.Long(),
					CalendarId = c.Long(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Project", t => t.AbsenceOnlyDefaultProjectId)
				.ForeignKey("Configuration.Calendar", t => t.CalendarId)
				.ForeignKey("Dictionaries.Company", t => t.CompanyId)
				.ForeignKey("TimeTracking.ContractType", t => t.ContractTypeId)
				.ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId)
				.ForeignKey("Dictionaries.Location", t => t.LocationId)
				.ForeignKey("SkillManagement.JobTitle", t => t.JobTitleId)
				.ForeignKey("Allocation.Employee", t => t.LineManagerId)
				.ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.JobTitleId)
				.Index(t => t.JobProfileId)
				.Index(t => t.OrgUnitId)
				.Index(t => t.LocationId)
				.Index(t => t.CompanyId)
				.Index(t => t.LineManagerId)
				.Index(t => t.ContractTypeId)
				.Index(t => t.AbsenceOnlyDefaultProjectId)
				.Index(t => t.CalendarId);

			CreateTable(
				"TimeTracking.ContractType",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(nullable: false, maxLength: 255),
					NamePl = c.String(nullable: false, maxLength: 255),
					DescriptionEn = c.String(maxLength: 512),
					DescriptionPl = c.String(maxLength: 512),
					EmploymentType = c.Int(),
					IsActive = c.Boolean(nullable: false),
					DailyWorkingLimit = c.Decimal(precision: 18, scale: 2),
					ActiveDirectoryCode = c.String(maxLength: 255),
					AllowedOverTimeTypes = c.Int(nullable: false),
					HasPaidVacation = c.Boolean(nullable: false),
					UseAutomaticOvertimeBankCalculation = c.Boolean(nullable: false),
					ShouldNotifyOnAbsenceRemoval = c.Boolean(nullable: false),
					MinimunOvertimeBankBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.ActiveDirectoryCode, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.EmploymentDataChangeRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
					EffectiveOn = c.DateTime(nullable: false),
					IsSetSalary = c.Boolean(nullable: false),
					Salary = c.String(),
					IsSetContractType = c.Boolean(nullable: false),
					ContractTypeId = c.Long(),
					IsSetContractDurationType = c.Boolean(nullable: false),
					ContractDurationType = c.Int(),
					IsSetCompany = c.Boolean(nullable: false),
					CompanyId = c.Long(),
					IsSetJobTitle = c.Boolean(nullable: false),
					JobTitleId = c.Long(),
					CommentsForPayroll = c.String(),
					Justification = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.Company", t => t.CompanyId)
				.ForeignKey("TimeTracking.ContractType", t => t.ContractTypeId)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("SkillManagement.JobTitle", t => t.JobTitleId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.EmployeeId)
				.Index(t => t.ContractTypeId)
				.Index(t => t.CompanyId)
				.Index(t => t.JobTitleId);

			CreateTable(
				"SkillManagement.JobTitle",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(),
					NamePl = c.String(),
					JobMatrixLevelId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("SkillManagement.JobMatrixLevel", t => t.JobMatrixLevelId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.JobMatrixLevelId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"SkillManagement.JobMatrixLevel",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 32, unicode: false),
					Level = c.Int(nullable: false),
					Description = c.String(),
					JobMatrixId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("SkillManagement.JobMatrix", t => t.JobMatrixId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.JobMatrixId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"SkillManagement.JobMatrix",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					Code = c.String(maxLength: 32, unicode: false),
					Description = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.EmploymentPeriod",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					StartDate = c.DateTime(nullable: false),
					EndDate = c.DateTime(),
					SignedOn = c.DateTime(nullable: false),
					EmployeeIdEmployee = c.Long(nullable: false),
					CompanyId = c.Long(),
					JobTitleId = c.Long(),
					IsActive = c.Boolean(nullable: false),
					InactivityReason = c.Int(nullable: false),
					TerminationReasonId = c.Long(),
					MondayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					TuesdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					WednesdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					ThursdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					FridayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SaturdayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					SundayHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					NoticePeriod = c.Int(),
					TerminatedOn = c.DateTime(),
					ContractTypeId = c.Long(nullable: false),
					ContractDuration = c.Int(),
					CalendarId = c.Long(),
					VacationHourToDayRatio = c.Decimal(nullable: false, precision: 18, scale: 2),
					TimeReportingMode = c.Int(nullable: false),
					ProjectIdAbsenceOnlyDefaultProject = c.Long(),
					IsClockCardRequired = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Project", t => t.ProjectIdAbsenceOnlyDefaultProject)
				.ForeignKey("Configuration.Calendar", t => t.CalendarId)
				.ForeignKey("Dictionaries.Company", t => t.CompanyId)
				.ForeignKey("TimeTracking.ContractType", t => t.ContractTypeId)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdEmployee)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("SkillManagement.JobTitle", t => t.JobTitleId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("PeopleManagement.EmploymentTerminationReason", t => t.TerminationReasonId)
				.Index(t => t.EmployeeIdEmployee)
				.Index(t => t.CompanyId)
				.Index(t => t.JobTitleId)
				.Index(t => t.TerminationReasonId)
				.Index(t => t.ContractTypeId)
				.Index(t => t.CalendarId)
				.Index(t => t.ProjectIdAbsenceOnlyDefaultProject)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"PeopleManagement.EmploymentTerminationReason",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(nullable: false, maxLength: 255),
					NamePl = c.String(nullable: false, maxLength: 255),
					DescriptionEn = c.String(maxLength: 512),
					DescriptionPl = c.String(maxLength: 512),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"SkillManagement.JobProfile",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					Description = c.String(maxLength: 255),
					CustomOrder = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.CustomOrder, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"SkillManagement.JobProfileSkillTag",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					JobProfileId = c.Long(nullable: false),
					SkillTagId = c.Long(nullable: false),
					CustomOrder = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("SkillManagement.SkillTag", t => t.SkillTagId)
				.ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId, cascadeDelete: true)
				.Index(t => t.JobProfileId)
				.Index(t => t.SkillTagId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"SkillManagement.SkillTag",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(maxLength: 255),
					NamePl = c.String(),
					Description = c.String(),
					TagType = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.NameEn, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Organization.PricingEngineer",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeIdEmployee = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdEmployee)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.EmployeeIdEmployee)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"SkillManagement.Skill",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					Description = c.String(),
					CreatedOn = c.DateTime(nullable: false),
					CreatedById = c.Long(),
					ImpersonatedCreatedById = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeProject",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					CompanyId = c.Long(nullable: false),
					ProjectId = c.Long(),
					From = c.DateTime(nullable: false),
					To = c.DateTime(),
					Roles = c.String(nullable: false, maxLength: 512),
					Duties = c.String(nullable: false, maxLength: 4000),
					ProjectDescription = c.String(maxLength: 4000),
					Name = c.String(maxLength: 1024),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Resumes.ResumeCompany", t => t.CompanyId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.Index(t => t.CompanyId)
				.Index(t => t.ProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeCompany",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResumeId = c.Long(nullable: false),
					Name = c.String(nullable: false, maxLength: 256),
					From = c.DateTime(nullable: false),
					To = c.DateTime(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Resumes.Resume", t => t.ResumeId)
				.Index(t => t.ResumeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.Resume",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(),
					AvailabilityInDays = c.Int(nullable: false),
					Summary = c.String(maxLength: 4000),
					CivoResumeVersionId = c.Long(),
					FillPercentage = c.Int(nullable: false),
					IsConsentClauseForDataProcessingChecked = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.EmployeeId, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeAdditionalSkill",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResumeId = c.Long(nullable: false),
					Name = c.String(nullable: false, maxLength: 256),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Resumes.Resume", t => t.ResumeId)
				.Index(t => t.ResumeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeEducation",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResumeId = c.Long(nullable: false),
					UniversityName = c.String(maxLength: 512),
					From = c.DateTime(nullable: false),
					To = c.DateTime(),
					Degree = c.String(nullable: false, maxLength: 256),
					Specialization = c.String(nullable: false, maxLength: 256),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Resumes.Resume", t => t.ResumeId)
				.Index(t => t.ResumeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeLanguage",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResumeId = c.Long(nullable: false),
					LanguageId = c.Long(nullable: false),
					Level = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Dictionaries.Language", t => t.LanguageId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Resumes.Resume", t => t.ResumeId)
				.Index(t => t.ResumeId)
				.Index(t => t.LanguageId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.Language",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(maxLength: 64),
					NamePl = c.String(maxLength: 64),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.NameEn, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeTechnicalSkill",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResumeId = c.Long(nullable: false),
					TechnicalSkillId = c.Long(nullable: false),
					ExpertiseLevel = c.Int(nullable: false),
					ExpertisePeriod = c.Int(nullable: false),
					CreatedOn = c.DateTime(nullable: false),
					CreatedById = c.Long(),
					ImpersonatedCreatedById = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Resumes.Resume", t => t.ResumeId)
				.ForeignKey("SkillManagement.Skill", t => t.TechnicalSkillId)
				.Index(t => new { t.ResumeId, t.TechnicalSkillId }, unique: true, name: "IX_ResumeIdTechnicalSkillId")
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Resumes.ResumeTraining",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResumeId = c.Long(nullable: false),
					Title = c.String(nullable: false, maxLength: 256),
					Date = c.DateTime(nullable: false),
					Institution = c.String(nullable: false, maxLength: 256),
					Type = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Resumes.Resume", t => t.ResumeId)
				.Index(t => t.ResumeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.Industry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(maxLength: 255),
					NamePl = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.NameEn, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.StaffingDemand",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					StartDate = c.DateTime(nullable: false),
					EndDate = c.DateTime(),
					Description = c.String(),
					EmployeesNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
					JobMatrixLevelId = c.Long(),
					JobProfileId = c.Long(),
					ProjectId = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("SkillManagement.JobMatrixLevel", t => t.JobMatrixLevelId)
				.ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.Index(t => t.JobMatrixLevelId)
				.Index(t => t.JobProfileId)
				.Index(t => t.ProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Organization.TechnicalInterviewer",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeIdEmployee = c.Long(nullable: false),
					CandidateMaxLevel = c.Int(nullable: false),
					CanInterviewForeignCandidates = c.Boolean(nullable: false),
					Comments = c.String(maxLength: 128),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdEmployee)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.EmployeeIdEmployee)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.EmployeeDataChangeRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					EffectiveOn = c.DateTime(),
					IsSetLineManager = c.Boolean(nullable: false),
					LineManagerId = c.Long(),
					IsSetOrgUnit = c.Boolean(nullable: false),
					OrgUnitId = c.Long(),
					IsSetJobProfile = c.Boolean(nullable: false),
					JobProfileId = c.Long(),
					IsSetLocation = c.Boolean(nullable: false),
					LocationId = c.Long(),
					IsSetPlaceOfWork = c.Boolean(nullable: false),
					PlaceOfWork = c.Int(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId)
				.ForeignKey("Allocation.Employee", t => t.LineManagerId)
				.ForeignKey("Dictionaries.Location", t => t.LocationId)
				.ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.LineManagerId)
				.Index(t => t.OrgUnitId)
				.Index(t => t.JobProfileId)
				.Index(t => t.LocationId);

			CreateTable(
				"Dictionaries.Location",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 100),
					Address = c.String(maxLength: 255),
					CityId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.City", t => t.CityId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.CityId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.City",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(nullable: false, maxLength: 100),
					CountryId = c.Long(nullable: false),
					IsCompanyApartmentAvailable = c.Boolean(nullable: false),
					IsVoucherServiceAvailable = c.Boolean(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.Country", t => t.CountryId)
				.Index(t => t.Name, unique: true)
				.Index(t => t.CountryId);

			CreateTable(
				"Dictionaries.Country",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(nullable: false, maxLength: 100),
					IsoCode = c.String(nullable: false, maxLength: 2),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.Index(t => t.Name, unique: true);

			CreateTable(
				"BusinessTrips.BusinessTrip",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					RequestId = c.Long(nullable: false),
					StartedOn = c.DateTime(nullable: false),
					EndedOn = c.DateTime(nullable: false),
					DepartureCityId = c.Long(nullable: false),
					DestinationCityId = c.Long(nullable: false),
					DepartureCityTaxiVouchers = c.Int(nullable: false),
					DestinationCityTaxiVouchers = c.Int(nullable: false),
					ItineraryDetails = c.String(),
					ArrangementTrackingRemarks = c.String(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.City", t => t.DepartureCityId)
				.ForeignKey("Dictionaries.City", t => t.DestinationCityId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.RequestId)
				.Index(t => t.DepartureCityId)
				.Index(t => t.DestinationCityId);

			CreateTable(
				"BusinessTrips.BusinessTripParticipant",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					StartedOn = c.DateTime(nullable: false),
					EndedOn = c.DateTime(nullable: false),
					IdCardNumber = c.String(maxLength: 32),
					PhoneNumber = c.String(maxLength: 32),
					HomeAddress = c.String(maxLength: 128),
					TransportationPreferences = c.Int(nullable: false),
					AccommodationPreferences = c.Int(nullable: false),
					DepartureCityTaxiVouchers = c.Int(nullable: false),
					DestinationCityTaxiVouchers = c.Int(nullable: false),
					ItineraryDetails = c.String(),
					BusinessTripId = c.Long(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTripId)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.Index(t => t.EmployeeId)
				.Index(t => t.BusinessTripId);

			CreateTable(
				"BusinessTrips.Arrangement",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					BusinessTripId = c.Long(nullable: false),
					ArrangementType = c.Int(nullable: false),
					Name = c.String(nullable: false, maxLength: 250),
					Details = c.String(maxLength: 2000),
					CurrencyId = c.Long(),
					Value = c.Decimal(precision: 18, scale: 2),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTripId)
				.ForeignKey("BusinessTrips.Currency", t => t.CurrencyId)
				.Index(t => t.BusinessTripId)
				.Index(t => t.CurrencyId);

			CreateTable(
				"BusinessTrips.ArrangementDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					UniqueId = c.Guid(nullable: false),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					ArrangementId = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("BusinessTrips.Arrangement", t => t.ArrangementId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.ArrangementId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"BusinessTrips.ArrangementDocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("BusinessTrips.ArrangementDocument", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"BusinessTrips.Currency",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(nullable: false, maxLength: 100),
					IsoCode = c.String(nullable: false, maxLength: 12),
					Symbol = c.String(nullable: false, maxLength: 12),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"BusinessTrips.ArrangementTrackingEntry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ParticipantId = c.Long(nullable: false),
					ArrangementType = c.Int(nullable: false),
					Status = c.Int(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("BusinessTrips.BusinessTripParticipant", t => t.ParticipantId, cascadeDelete: true)
				.Index(t => new { t.ParticipantId, t.ArrangementType }, unique: true, name: "IX_ArrangementTypePerParticipant");

			CreateTable(
				"Workflows.DeclaredLuggage",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					LargeCabinItems = c.Int(nullable: false),
					RegisteredUnder15Items = c.Int(nullable: false),
					Registered15To23Items = c.Int(nullable: false),
					RegisteredOver23Items = c.Int(nullable: false),
					RequestId = c.Long(),
					ParticipantId = c.Long(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("BusinessTrips.BusinessTripParticipant", t => t.ParticipantId)
				.ForeignKey("Workflows.BusinessTripRequest", t => t.RequestId)
				.Index(t => t.EmployeeId)
				.Index(t => t.RequestId)
				.Index(t => t.ParticipantId);

			CreateTable(
				"Workflows.BusinessTripRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					StartedOn = c.DateTime(nullable: false),
					EndedOn = c.DateTime(nullable: false),
					DepartureCityId = c.Long(nullable: false),
					DestinationCityId = c.Long(nullable: false),
					AreTravelArrangementsNeeded = c.Boolean(nullable: false),
					TransportationPreferences = c.Int(nullable: false),
					AccommodationPreferences = c.Int(nullable: false),
					DepartureCityTaxiVouchers = c.Int(nullable: false),
					DestinationCityTaxiVouchers = c.Int(nullable: false),
					ItineraryDetails = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.City", t => t.DepartureCityId)
				.ForeignKey("Dictionaries.City", t => t.DestinationCityId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.DepartureCityId)
				.Index(t => t.DestinationCityId);

			CreateTable(
				"Organization.OrgUnit",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 32, unicode: false),
					Path = c.String(maxLength: 900, unicode: false),
					Name = c.String(maxLength: 255),
					FlatName = c.String(maxLength: 64),
					NameSortOrder = c.Long(nullable: false),
					DescendantCount = c.Long(nullable: false),
					Description = c.String(maxLength: 1024),
					ParentId = c.Long(),
					Depth = c.Int(),
					CalendarId = c.Long(),
					EmployeeId = c.Long(),
					RegionId = c.Long(),
					OrgUnitFeatures = c.Int(nullable: false),
					CustomOrder = c.Long(),
					DefaultProjectContributionMode = c.Int(nullable: false),
					TimeReportingMode = c.Int(),
					AbsenceOnlyDefaultProjectId = c.Long(),
					OrgUnitManagerRole = c.Int(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Project", t => t.AbsenceOnlyDefaultProjectId)
				.ForeignKey("Configuration.Calendar", t => t.CalendarId)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Organization.OrgUnit", t => t.ParentId)
				.ForeignKey("Dictionaries.Region", t => t.RegionId)
				.Index(t => t.Code, unique: true)
				.Index(t => t.Path, unique: true)
				.Index(t => t.FlatName, unique: true)
				.Index(t => t.NameSortOrder)
				.Index(t => t.ParentId)
				.Index(t => t.CalendarId)
				.Index(t => t.EmployeeId)
				.Index(t => t.RegionId)
				.Index(t => t.AbsenceOnlyDefaultProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.Region",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 100),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"TimeTracking.EmployeeAbsence",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					AbsenceTypeId = c.Long(nullable: false),
					From = c.DateTime(nullable: false),
					To = c.DateTime(nullable: false),
					ApprovedOn = c.DateTime(nullable: false),
					Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
					RequestId = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.AbsenceType", t => t.AbsenceTypeId)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.EmployeeId)
				.Index(t => t.AbsenceTypeId)
				.Index(t => t.RequestId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"MeetMe.Feedback",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeIdAuthor = c.Long(nullable: false),
					PositiveFeedback = c.String(maxLength: 1000),
					NegativeFeedback = c.String(maxLength: 1000),
					Remarks = c.String(maxLength: 1000),
					Status = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdAuthor)
				.Index(t => t.EmployeeIdAuthor)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"MeetMe.Meeting",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeIdEmployee = c.Long(nullable: false),
					EmployeeIdLineManager = c.Long(),
					LocationIdLocation = c.Long(nullable: false),
					Status = c.Int(nullable: false),
					MeetingDueDate = c.DateTime(),
					ScheduledMeetingDate = c.DateTime(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdLineManager)
				.ForeignKey("Dictionaries.Location", t => t.LocationIdLocation)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdEmployee)
				.Index(t => t.EmployeeIdEmployee)
				.Index(t => t.EmployeeIdLineManager)
				.Index(t => t.LocationIdLocation)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"MeetMe.MeetingAttachment",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
					Meeting_Id = c.Long(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("MeetMe.Meeting", t => t.Meeting_Id)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy)
				.Index(t => t.Meeting_Id);

			CreateTable(
				"MeetMe.MeetingAttachmentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("MeetMe.MeetingAttachment", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"MeetMe.MeetingNote",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					MeetingId = c.Long(nullable: false),
					EmployeeIdAuthor = c.Long(nullable: false),
					NoteBody = c.String(maxLength: 1000),
					CreatedOn = c.DateTime(nullable: false),
					Status = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdAuthor)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("MeetMe.Meeting", t => t.MeetingId)
				.Index(t => t.MeetingId)
				.Index(t => t.EmployeeIdAuthor)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"MeetMe.MeetingNoteAttachment",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
					MeetingNote_Id = c.Long(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("MeetMe.MeetingNote", t => t.MeetingNote_Id)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy)
				.Index(t => t.MeetingNote_Id);

			CreateTable(
				"MeetMe.MeetingNoteAttachmentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("MeetMe.MeetingNoteAttachment", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"TimeTracking.OvertimeBank",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					RequestId = c.Long(),
					TimeReportRowId = c.Long(),
					TimeReportId = c.Long(),
					OvertimeChange = c.Decimal(nullable: false, precision: 18, scale: 2),
					EffectiveOn = c.DateTime(nullable: false),
					OvertimeBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
					Comment = c.String(maxLength: 255),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.ForeignKey("TimeTracking.TimeReport", t => t.TimeReportId)
				.ForeignKey("TimeTracking.TimeReportRow", t => t.TimeReportRowId, cascadeDelete: true)
				.Index(t => t.EmployeeId)
				.Index(t => t.RequestId)
				.Index(t => t.TimeReportRowId)
				.Index(t => t.TimeReportId);

			CreateTable(
				"TimeTracking.TimeReport",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					Year = c.Int(nullable: false),
					Month = c.Byte(nullable: false),
					ContractedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					ReportedNormalHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					Status = c.Int(nullable: false),
					ControllingStatus = c.Int(nullable: false),
					ControllingErrorMessage = c.String(maxLength: 256),
					ModifiedOn = c.DateTime(nullable: false),
					TimeZoneDisplayName = c.String(maxLength: 200),
					TimeZoneOffsetMinutes = c.Int(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.Index(t => new { t.EmployeeId, t.Year, t.Month }, unique: true, name: "IX_EmployeeYearMonth");

			CreateTable(
				"TimeTracking.ClockCardDailyEntry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					TimeReportId = c.Long(nullable: false),
					Day = c.DateTime(nullable: false),
					InTime = c.DateTime(),
					OutTime = c.DateTime(),
					BreakDuration = c.Time(precision: 7),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.TimeReport", t => t.TimeReportId, cascadeDelete: true)
				.Index(t => t.TimeReportId);

			CreateTable(
				"TimeTracking.TimeReportRow",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					TimeReportId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					TaskName = c.String(),
					DisplayOrder = c.Long(nullable: false),
					ImportSourceId = c.Long(),
					ImportedTaskCode = c.String(),
					ImportedTaskType = c.String(maxLength: 128),
					OvertimeVariant = c.Int(nullable: false),
					Status = c.Int(nullable: false),
					RequestId = c.Long(),
					AbsenceTypeId = c.Long(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.AbsenceType", t => t.AbsenceTypeId)
				.ForeignKey("TimeTracking.TimeTrackingImportSource", t => t.ImportSourceId)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.ForeignKey("TimeTracking.TimeReport", t => t.TimeReportId, cascadeDelete: true)
				.Index(t => t.TimeReportId)
				.Index(t => t.ProjectId)
				.Index(t => t.ImportSourceId)
				.Index(t => t.RequestId)
				.Index(t => t.AbsenceTypeId);

			CreateTable(
				"TimeTracking.TimeReportProjectAttributeValue",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					IsDeleted = c.Boolean(nullable: false),
					AttributeId = c.Long(nullable: false),
					Name = c.String(),
					Description = c.String(),
					Features = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.TimeReportProjectAttribute", t => t.AttributeId, cascadeDelete: true)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.AttributeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"TimeTracking.TimeReportProjectAttribute",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					IsDeleted = c.Boolean(nullable: false),
					Name = c.String(),
					Description = c.String(),
					DefaultValueId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.TimeReportProjectAttributeValue", t => t.DefaultValueId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.DefaultValueId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"TimeTracking.TimeReportDailyEntry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					TimeReportRowId = c.Long(nullable: false),
					Day = c.DateTime(nullable: false),
					Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("TimeTracking.TimeReportRow", t => t.TimeReportRowId, cascadeDelete: true)
				.Index(t => t.TimeReportRowId);

			CreateTable(
				"TimeTracking.TimeTrackingImportSource",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 255),
					NamePl = c.String(),
					NameEn = c.String(),
					ImportStrategyIdentifier = c.String(maxLength: 255),
					Configuration = c.String(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.Index(t => t.Code, unique: true);

			CreateTable(
				"TimeTracking.VacationBalance",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					EffectiveOn = c.DateTime(nullable: false),
					RequestId = c.Long(),
					Operation = c.Int(nullable: false),
					Comment = c.String(maxLength: 255),
					HourChange = c.Decimal(nullable: false, precision: 18, scale: 2),
					TotalHourBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.EmployeeId)
				.Index(t => t.RequestId);

			CreateTable(
				"Allocation.WeeklyAllocationStatus",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					Week = c.DateTime(nullable: false),
					AllocatedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					PlannedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					AvailableHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					CalendarHours = c.Decimal(nullable: false, precision: 18, scale: 2),
					AllocationStatus = c.Int(nullable: false),
					EmployeeStatus = c.Int(nullable: false),
					OnBenchSince = c.DateTime(),
					StaffingStatus = c.Int(nullable: false),
					OnBenchUntil = c.DateTime(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.Index(t => t.EmployeeId);

			CreateTable(
				"Allocation.WeeklyAllocationDetail",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					WeeklyAllocationStatusId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.ForeignKey("Allocation.WeeklyAllocationStatus", t => t.WeeklyAllocationStatusId)
				.Index(t => t.WeeklyAllocationStatusId)
				.Index(t => t.ProjectId);

			CreateTable(
				"Allocation.ProjectAttachment",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					AttachmentType = c.Int(nullable: false),
					ProjectId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.Index(t => t.ProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.ProjectAttachmentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.ProjectAttachment", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"TimeTracking.JiraIssueToTimeReportRowMapper",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 100),
					SourceCode = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.ProjectPicture",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.ProjectPictureContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.ProjectPicture", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"Dictionaries.ServiceLine",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.SoftwareCategory",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Allocation.ProjectTag",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					Description = c.String(),
					TagType = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Dictionaries.UtilizationCategory",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NavisionCode = c.String(maxLength: 64),
					NavisionName = c.String(maxLength: 64),
					DisplayName = c.String(maxLength: 64),
					Order = c.Int(),
					IsActive = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.NavisionCode, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.AddHomeOfficeRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					From = c.DateTime(nullable: false),
					To = c.DateTime(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"Workflows.AdvancedPaymentRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					BusinessTripId = c.Long(nullable: false),
					Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
					CurrencyId = c.Long(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTripId)
				.ForeignKey("BusinessTrips.Currency", t => t.CurrencyId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.BusinessTripId)
				.Index(t => t.CurrencyId);

			CreateTable(
				"Workflows.ApprovalGroup",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					RequestId = c.Long(nullable: false),
					Mode = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Workflows.Request", t => t.RequestId, cascadeDelete: true)
				.Index(t => t.RequestId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.Approvals",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Status = c.Int(nullable: false),
					UserId = c.Long(nullable: false),
					ApprovalGroupId = c.Long(nullable: false),
					ForcedByUserId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.ApprovalGroup", t => t.ApprovalGroupId, cascadeDelete: true)
				.ForeignKey("Accounts.User", t => t.ForcedByUserId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.UserId)
				.Index(t => t.UserId)
				.Index(t => t.ApprovalGroupId)
				.Index(t => t.ForcedByUserId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.AssetsRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					OnboardingRequestId = c.Long(),
					ProjectId = c.Long(),
					EmployeeId = c.Long(nullable: false),
					AssetTypeSoftwareComment = c.String(maxLength: 1024),
					AssetTypeSystemAccessComment = c.String(maxLength: 1024),
					AssetTypeHardwareComment = c.String(maxLength: 1024),
					AssetTypeOtherComment = c.String(maxLength: 1024),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Workflows.OnboardingRequest", t => t.OnboardingRequestId)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.OnboardingRequestId)
				.Index(t => t.ProjectId)
				.Index(t => t.EmployeeId);

			CreateTable(
				"PeopleManagement.AssetType",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					NameEn = c.String(maxLength: 255),
					NamePl = c.String(maxLength: 255),
					Category = c.Int(nullable: false),
					DetailsRequired = c.Boolean(nullable: false),
					DetailsHintEn = c.String(maxLength: 255),
					DetailsHintPl = c.String(maxLength: 255),
					IsActive = c.Boolean(nullable: false),
					Description = c.String(maxLength: 1024),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.Index(t => t.NameEn, unique: true)
				.Index(t => t.NamePl, unique: true);

			CreateTable(
				"Workflows.OnboardingRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					LineManagerEmployeeId = c.Long(),
					RecruiterEmployeeId = c.Long(nullable: false),
					FirstName = c.String(maxLength: 255),
					LastName = c.String(maxLength: 255),
					Login = c.String(maxLength: 20),
					SignedOn = c.DateTime(),
					StartDate = c.DateTime(nullable: false),
					LocationId = c.Long(nullable: false),
					PlaceOfWork = c.Int(nullable: false),
					CompanyId = c.Long(nullable: false),
					JobTitleId = c.Long(nullable: false),
					JobProfileId = c.Long(nullable: false),
					ContractTypeId = c.Long(nullable: false),
					ProbationPeriodContractTypeId = c.Long(nullable: false),
					Compensation = c.String(maxLength: 128),
					ProbationPeriodCompensation = c.String(maxLength: 128),
					CommentsForPayroll = c.String(maxLength: 2048),
					OrgUnitId = c.Long(nullable: false),
					CommentsForHiringManager = c.String(maxLength: 2048),
					IsForeignEmployee = c.Boolean(nullable: false),
					IsComingFromReferralProgram = c.Boolean(nullable: false),
					IsRelocationPackageNeeded = c.Boolean(nullable: false),
					OnboardingDate = c.DateTime(nullable: false),
					OnboardingLocationId = c.Long(nullable: false),
					WelcomeAnnounceEmailInformation = c.String(maxLength: 2048),
					WelcomeAnnounceEmailPhotosSecret = c.Guid(nullable: false),
					ForeignEmployeeDocumentsSecret = c.Guid(nullable: false),
					PayrollDocumentsSecret = c.Guid(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Dictionaries.Company", t => t.CompanyId)
				.ForeignKey("TimeTracking.ContractType", t => t.ContractTypeId)
				.ForeignKey("SkillManagement.JobProfile", t => t.JobProfileId)
				.ForeignKey("SkillManagement.JobTitle", t => t.JobTitleId)
				.ForeignKey("Allocation.Employee", t => t.LineManagerEmployeeId)
				.ForeignKey("Dictionaries.Location", t => t.LocationId)
				.ForeignKey("Dictionaries.Location", t => t.OnboardingLocationId)
				.ForeignKey("Organization.OrgUnit", t => t.OrgUnitId)
				.ForeignKey("TimeTracking.ContractType", t => t.ProbationPeriodContractTypeId)
				.ForeignKey("Allocation.Employee", t => t.RecruiterEmployeeId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.LineManagerEmployeeId)
				.Index(t => t.RecruiterEmployeeId)
				.Index(t => t.LocationId)
				.Index(t => t.CompanyId)
				.Index(t => t.JobTitleId)
				.Index(t => t.JobProfileId)
				.Index(t => t.ContractTypeId)
				.Index(t => t.ProbationPeriodContractTypeId)
				.Index(t => t.OrgUnitId)
				.Index(t => t.OnboardingLocationId);

			CreateTable(
				"Workflows.ForeignEmployeeDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					OnboardingRequestId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Workflows.OnboardingRequest", t => t.OnboardingRequestId)
				.Index(t => t.OnboardingRequestId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.ForeignEmployeeDocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.ForeignEmployeeDocument", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"Workflows.PayrollDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					OnboardingRequestId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Workflows.OnboardingRequest", t => t.OnboardingRequestId)
				.Index(t => t.OnboardingRequestId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.PayrollDocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.PayrollDocument", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"Workflows.EmployeePhotoDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					OnboardingRequestId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Workflows.OnboardingRequest", t => t.OnboardingRequestId)
				.Index(t => t.OnboardingRequestId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.EmployeePhotoDocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.EmployeePhotoDocument", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"Workflows.RequestDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					RequestId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.RequestId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Workflows.RequestDocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.RequestDocument", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"Workflows.EmploymentTerminationRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
					TerminationDate = c.DateTime(nullable: false),
					EndDate = c.DateTime(nullable: false),
					ReasonId = c.Long(nullable: false),
					BackupData = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("PeopleManagement.EmploymentTerminationReason", t => t.ReasonId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.EmployeeId)
				.Index(t => t.ReasonId);

			CreateTable(
				"Workflows.FeedbackRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					EvaluatedEmployeeId = c.Long(nullable: false),
					EvaluatorEmployeeId = c.Long(nullable: false),
					PositiveFeedback = c.String(maxLength: 1000),
					NegativeFeedback = c.String(maxLength: 1000),
					Remarks = c.String(maxLength: 1000),
					Comment = c.String(maxLength: 1000),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EvaluatedEmployeeId)
				.ForeignKey("Allocation.Employee", t => t.EvaluatorEmployeeId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.EvaluatedEmployeeId)
				.Index(t => t.EvaluatorEmployeeId);

			CreateTable(
				"Workflows.RequestHistoryEntry",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Date = c.DateTime(nullable: false),
					Type = c.Int(nullable: false),
					ExecutingUserId = c.Long(),
					ApprovingUserId = c.Long(),
					JiraIssueKey = c.String(maxLength: 256),
					EmailSubject = c.String(maxLength: 256),
					EmailRecipients = c.String(maxLength: 1024),
					WatcherNames = c.String(maxLength: 256),
					UpdatesOn = c.String(maxLength: 256),
					RequestId = c.Long(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.ApprovingUserId)
				.ForeignKey("Accounts.User", t => t.ExecutingUserId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.ExecutingUserId)
				.Index(t => t.ApprovingUserId)
				.Index(t => t.RequestId);

			CreateTable(
				"Workflows.OvertimeRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					From = c.DateTime(nullable: false),
					To = c.DateTime(nullable: false),
					HourLimit = c.Decimal(nullable: false, precision: 18, scale: 2),
					Comment = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.ProjectId);

			CreateTable(
				"Workflows.ProjectTimeReportApprovalRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					From = c.DateTime(nullable: false),
					To = c.DateTime(nullable: false),
					Tasks = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.EmployeeId)
				.Index(t => t.ProjectId);

			CreateTable(
				"Workflows.ReopenTimeTrackingReportRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					Month = c.Int(nullable: false),
					Year = c.Int(nullable: false),
					Reason = c.String(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"Workflows.SkillChangeRequest",
				c => new
				{
					Id = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.EmployeeId);

			CreateTable(
				"Workflows.ActionSet",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(nullable: false, maxLength: 100),
					Description = c.String(maxLength: 500),
					TriggeringRequestType = c.Int(),
					Definition = c.String(),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.Index(t => t.Code, unique: true);

			CreateTable(
				"Workflows.ActionSetTriggeringStatus",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ActionSetId = c.Long(nullable: false),
					RequestStatus = c.Int(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Workflows.ActionSet", t => t.ActionSetId, cascadeDelete: true)
				.Index(t => t.ActionSetId);

			CreateTable(
				"CustomerPortal.ActivityLog",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ActivityType = c.Int(nullable: false),
					RecommendationIdRecommendation = c.Long(),
					ResourceFormIdResourceForm = c.Long(),
					SeenByUserOn = c.DateTime(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationIdRecommendation)
				.ForeignKey("CustomerPortal.ResourceForm", t => t.ResourceFormIdResourceForm)
				.Index(t => t.RecommendationIdRecommendation)
				.Index(t => t.ResourceFormIdResourceForm)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.Recommendations",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ResourceId = c.Guid(nullable: false),
					Name = c.String(),
					Rate = c.String(),
					AddedOn = c.DateTime(nullable: false),
					Availability = c.String(),
					Note = c.String(maxLength: 500),
					Years = c.Single(),
					UserId = c.Long(),
					NeededResourceId = c.Long(),
					IsOutside = c.Boolean(nullable: false),
					RecommendationStatusEnumId = c.Int(nullable: false),
					SeenByCustomer = c.Boolean(nullable: false),
					RejectedOn = c.DateTime(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.NeededResources", t => t.NeededResourceId)
				.Index(t => t.ResourceId, unique: true)
				.Index(t => t.NeededResourceId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.Challenges",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					RecommendationIdRecommendation = c.Long(nullable: false),
					Deadline = c.DateTime(nullable: false),
					ChallengeComment = c.String(),
					AnswerComment = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationIdRecommendation)
				.Index(t => t.RecommendationIdRecommendation)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.AnswerDocuments",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ChallengeId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("CustomerPortal.Challenges", t => t.ChallengeId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.ChallengeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.AnswerDocumentContents",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("CustomerPortal.AnswerDocuments", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"CustomerPortal.ChallengeDocuments",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ChallengeId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("CustomerPortal.Challenges", t => t.ChallengeId)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.ChallengeId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.ChallengeDocumentContents",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("CustomerPortal.ChallengeDocuments", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"CustomerPortal.Documents",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Url = c.String(),
					UniqueId = c.Guid(nullable: false),
					RecommendationId = c.Long(),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationId)
				.Index(t => t.RecommendationId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.DocumentContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("CustomerPortal.Documents", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"CustomerPortal.NeededResources",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(),
					CrmId = c.Guid(nullable: false),
					IsOnHold = c.Boolean(nullable: false),
					HoldReason = c.String(maxLength: 2000),
					ResourceFormId = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.ResourceForm", t => t.ResourceFormId)
				.Index(t => t.ResourceFormId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.ResourceForm",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					UserIdCreatedBy = c.Long(nullable: false),
					CreatedOn = c.DateTime(nullable: false),
					ResourceName = c.String(maxLength: 200),
					ResourceCount = c.Int(nullable: false),
					RoleDescription = c.String(maxLength: 2000),
					PlannedStart = c.String(maxLength: 200),
					ProjectDuration = c.String(maxLength: 200),
					Location = c.String(maxLength: 200),
					ProjectDescription = c.String(maxLength: 2000),
					PositionRequirements = c.String(maxLength: 2000),
					State = c.Int(nullable: false),
					UserIdSubmittedBy = c.Long(),
					SubmittedOn = c.DateTime(),
					UserIdApprovedBy = c.Long(),
					ApprovedOn = c.DateTime(),
					LanguageComment = c.String(maxLength: 2000),
					RecruitmentPhases = c.String(maxLength: 2000),
					AdditionalComment = c.String(),
					NeededResourceCrmCode = c.Guid(),
					Question = c.String(maxLength: 2000),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdApprovedBy)
				.ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.UserIdSubmittedBy)
				.Index(t => t.UserIdCreatedBy)
				.Index(t => t.UserIdSubmittedBy)
				.Index(t => t.UserIdApprovedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.LanguageOption",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					LevelId = c.Int(nullable: false),
					LanguageId = c.Int(nullable: false),
					LevelName = c.String(),
					LanguageName = c.String(),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"CustomerPortal.Note",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Content = c.String(maxLength: 1000),
					RecommendationId = c.Long(),
					SavedInCRM = c.Boolean(nullable: false),
					UserIdUser = c.Long(),
					CreatedOn = c.DateTime(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationId)
				.ForeignKey("Accounts.User", t => t.UserIdUser)
				.Index(t => t.RecommendationId)
				.Index(t => t.UserIdUser)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.ScheduleAppointment",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					AppointmentDate = c.DateTime(nullable: false),
					AppointmentEndDate = c.DateTime(nullable: false),
					AppointmentStatus = c.Int(nullable: false),
					RecommendationIdRecommendation = c.Long(nullable: false),
					InterviewType = c.Int(nullable: false),
					ReminderTriggered = c.DateTime(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationIdRecommendation)
				.Index(t => t.RecommendationIdRecommendation)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Finance.AggregatedProjectTransaction",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EstimatedRevenue = c.Decimal(precision: 18, scale: 2),
					Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
					Month = c.Byte(nullable: false),
					OperationCost = c.Decimal(precision: 18, scale: 2),
					OperationMargin = c.Decimal(precision: 18, scale: 2),
					OperationMarginPercent = c.Decimal(precision: 18, scale: 2),
					ProjectCost = c.Decimal(precision: 18, scale: 2),
					ProjectId = c.Long(nullable: false),
					ProjectMargin = c.Decimal(precision: 18, scale: 2),
					ProjectMarginPercent = c.Decimal(precision: 18, scale: 2),
					Revenue = c.Decimal(precision: 18, scale: 2),
					ServiceCost = c.Decimal(precision: 18, scale: 2),
					ServiceMargin = c.Decimal(precision: 18, scale: 2),
					ServiceMarginPercent = c.Decimal(precision: 18, scale: 2),
					Year = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.Index(t => t.ProjectId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Audit.atomic_AuditTableView",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					TableName = c.String(),
					Schema = c.String(),
					IsAuditEnabled = c.Boolean(nullable: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"BusinessTrips.BusinessTripMessage",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					AuthorEmployeeId = c.Long(nullable: false),
					AddedOn = c.DateTime(nullable: false),
					Content = c.String(),
					BusinessTripId = c.Long(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.AuthorEmployeeId)
				.ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTripId)
				.Index(t => t.AuthorEmployeeId)
				.Index(t => t.BusinessTripId);

			CreateTable(
				"CustomerPortal.BusinesUnitOptions",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					CrmId = c.Int(nullable: false),
					DisplayValue = c.String(maxLength: 1000),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Runtime.CacheInvalidation",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Area = c.String(maxLength: 255, unicode: false),
					Token = c.Guid(nullable: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Crm.Candidate",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					CrmId = c.Guid(nullable: false),
					FirstName = c.String(),
					LastName = c.String(),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"CustomerPortal.ChallengeFile",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 1024),
					ContentType = c.String(maxLength: 255, unicode: false),
					Identifier = c.Guid(nullable: false),
					ContentLength = c.Long(nullable: false),
					Data = c.Binary(),
					RecommendationIdRecommendation = c.Long(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationIdRecommendation)
				.Index(t => t.Identifier)
				.Index(t => t.RecommendationIdRecommendation)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.ClientContact",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					CreatedOn = c.DateTime(nullable: false),
					CrmId = c.Guid(nullable: false),
					Name = c.String(),
					Email = c.String(),
					CompanyGuidId = c.Guid(nullable: false),
					CompanyName = c.String(),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"CustomerPortal.ClientData",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					UserIdClientUser = c.Long(nullable: false),
					UserIdManagerUser = c.Long(nullable: false),
					ClientContactIdClientContact = c.Long(nullable: false),
					ContactPersonPhone = c.String(maxLength: 200),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("CustomerPortal.ClientContact", t => t.ClientContactIdClientContact)
				.ForeignKey("Accounts.User", t => t.UserIdClientUser)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdManagerUser)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdClientUser, unique: true)
				.Index(t => t.UserIdManagerUser)
				.Index(t => t.ClientContactIdClientContact)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Consents.ConsentDecision",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					UserIdUser = c.Long(nullable: false),
					EnumIdentifier = c.String(),
					EnumValue = c.String(),
					ConsentDecisionType = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.UserIdUser)
				.Index(t => t.UserIdUser)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Configuration.atomic_CurrentDateTimeView",
				c => new
				{
					CurrentDateTime = c.DateTime(nullable: false),
				})
				.PrimaryKey(t => t.CurrentDateTime);

			CreateTable(
				"Dictionaries.CustomerSize",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					Description = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Accounts.DataFilter",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					UserId = c.Long(),
					ProfileId = c.Long(),
					Entity = c.String(maxLength: 100, unicode: false),
					Module = c.String(maxLength: 300, unicode: false),
					Condition = c.String(maxLength: 2048, unicode: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.UserId)
				.Index(t => t.UserId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Notifications.EmailAttachment",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(),
					Content = c.Binary(),
					EmailId = c.Long(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Notifications.Email", t => t.EmailId)
				.Index(t => t.EmailId);

			CreateTable(
				"Notifications.Email",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					To = c.String(nullable: false, maxLength: 384),
					From = c.String(nullable: false, maxLength: 128),
					Subject = c.String(nullable: false, maxLength: 256),
					EmailStatusEnumId = c.Int(nullable: false),
					SentOn = c.DateTime(),
					Content = c.String(nullable: false),
					Exception = c.String(),
					UserIdCreatedBy = c.Long(),
					UserIdImpersonatedCreatedBy = c.Long(),
					CreatedOn = c.DateTime(nullable: false),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
				.Index(t => new { t.To, t.From, t.Subject }, name: "EmailSmartSearchIndex")
				.Index(t => t.UserIdCreatedBy)
				.Index(t => t.UserIdImpersonatedCreatedBy);

			CreateTable(
				"Allocation.EmployeeComment",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeCommentType = c.Int(nullable: false),
					EmployeeIdEmployee = c.Long(nullable: false),
					Comment = c.String(maxLength: 1024),
					RelevantOn = c.DateTime(),
					CreatedById = c.Long(),
					ImpersonatedCreatedById = c.Long(),
					CreatedOn = c.DateTime(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeIdEmployee)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.EmployeeIdEmployee)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Scheduling.JobInternalParameter",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255, unicode: false),
					Value = c.String(maxLength: 255, unicode: false),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Scheduling.JobDefinitionParameter",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					JobDefinitionId = c.Long(nullable: false),
					ParameterKey = c.String(maxLength: 100, unicode: false),
					ParameterValue = c.String(maxLength: 100),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Scheduling.JobDefinition", t => t.JobDefinitionId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.JobDefinitionId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Scheduling.JobDefinition",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 100, unicode: false),
					Description = c.String(maxLength: 500, unicode: false),
					ClassIdentifier = c.String(maxLength: 100, unicode: false),
					ScheduleSettings = c.String(maxLength: 200, unicode: false),
					LastRunOn = c.DateTime(),
					LastSuccessOn = c.DateTime(),
					LastFailureOn = c.DateTime(),
					IsEnabled = c.Boolean(nullable: false),
					ShouldRunNow = c.Boolean(nullable: false),
					PipelineId = c.Long(nullable: false),
					MaxExecutionPeriodInSeconds = c.Int(nullable: false),
					MaxRunAttemptsInBulk = c.Int(nullable: false),
					DelayBetweenBulkRunAttemptsInSeconds = c.Int(nullable: false),
					MaxBulkRunAttempts = c.Int(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Scheduling.Pipeline", t => t.PipelineId)
				.Index(t => t.Code, unique: true)
				.Index(t => t.PipelineId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Scheduling.JobLog",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					JobDefinitionId = c.Long(nullable: false),
					SchedulerName = c.String(maxLength: 250, unicode: false),
					Host = c.String(maxLength: 250, unicode: false),
					Pipeline = c.String(maxLength: 100, unicode: false),
					JobLogSeverityEnumId = c.Int(nullable: false),
					ExecutionId = c.Guid(nullable: false),
					Message = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Scheduling.JobDefinition", t => t.JobDefinitionId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.JobDefinitionId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Runtime.JobRunInfo",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					JobDefinitionId = c.Long(nullable: false),
					StartedOn = c.DateTime(nullable: false),
					FinishedOn = c.DateTime(),
					NextRunOn = c.DateTime(),
					JobStatusEnumId = c.Int(nullable: false),
					Host = c.String(maxLength: 250, unicode: false),
					SchedulerName = c.String(maxLength: 250, unicode: false),
					TimesFailedInRow = c.Int(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Scheduling.JobDefinition", t => t.JobDefinitionId)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.JobDefinitionId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Scheduling.Pipeline",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 100, unicode: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Name, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Runtime.atomic_LastJobRunInfoView",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					JobRunInfoId = c.Long(),
					Status = c.Int(),
					StartedOn = c.DateTime(),
					FinishedOn = c.DateTime(),
					Pipeline = c.String(),
					TimesFailedInRow = c.Int(),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"CustomerPortal.MainTechnologyOptions",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					CrmId = c.Guid(nullable: false),
					DisplayValue = c.String(maxLength: 1000),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Configuration.MessageTemplate",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(maxLength: 255, unicode: false),
					Description = c.String(maxLength: 1024),
					MessageTemplateContentTypeEnumId = c.Int(nullable: false),
					Content = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"TimeTracking.OvertimeApproval",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					EmployeeId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
					DateFrom = c.DateTime(nullable: false),
					DateTo = c.DateTime(nullable: false),
					HourLimit = c.Decimal(nullable: false, precision: 18, scale: 2),
					RequestId = c.Long(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId)
				.ForeignKey("Allocation.Project", t => t.ProjectId)
				.ForeignKey("Workflows.Request", t => t.RequestId)
				.Index(t => t.EmployeeId)
				.Index(t => t.ProjectId)
				.Index(t => t.RequestId);

			CreateTable(
				"Runtime.ProcessedEventQueue",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
					BusinessEventId = c.Guid(nullable: false),
					PublishedOn = c.DateTime(nullable: false),
					ProcessingStartedOn = c.DateTime(nullable: false),
					ProcessedFinishedOn = c.DateTime(nullable: false),
					BusinessEvent = c.String(),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Runtime.PublishedEventQueue",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
					BusinessEventId = c.Guid(nullable: false),
					PublishedOn = c.DateTime(nullable: false),
					LeasedOn = c.DateTime(),
					BusinessEvent = c.String(),
					Exception = c.String(maxLength: 2048),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"CustomerPortal.Questions",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					RecommendationId = c.Long(nullable: false),
					UserId = c.Long(nullable: false),
					Query = c.String(),
					QueryDate = c.DateTime(nullable: false),
					WasSent = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("CustomerPortal.Recommendations", t => t.RecommendationId)
				.ForeignKey("Accounts.User", t => t.UserId)
				.Index(t => t.RecommendationId)
				.Index(t => t.UserId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"CustomerPortal.RegionOptions",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					CrmId = c.Guid(nullable: false),
					DisplayValue = c.String(maxLength: 1000),
				})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"Reporting.ReportDefinition",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Code = c.String(nullable: false, maxLength: 255),
					NameEn = c.String(nullable: false),
					NamePl = c.String(nullable: false),
					DescriptionEn = c.String(),
					DescriptionPl = c.String(),
					OutputFileNameTemplate = c.String(maxLength: 255),
					ReportingEngineIdentifier = c.String(nullable: false),
					DataSourceTypeIdentifier = c.String(nullable: false),
					PathEn = c.String(),
					PathPl = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Code, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Reporting.ReportTemplate",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					ReportDefinitionId = c.Long(nullable: false),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 250, unicode: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Reporting.ReportDefinition", t => t.ReportDefinitionId)
				.Index(t => t.ReportDefinitionId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Reporting.ReportTemplateContent",
				c => new
				{
					Id = c.Long(nullable: false),
					Content = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Reporting.ReportTemplate", t => t.Id, cascadeDelete: true)
				.Index(t => t.Id);

			CreateTable(
				"Survey.SurveyAnswer",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					SurveyId = c.Long(nullable: false),
					QuestionId = c.Long(nullable: false),
					Answer = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Survey.Survey", t => t.SurveyId)
				.ForeignKey("Survey.SurveyConductQuestion", t => t.QuestionId)
				.Index(t => t.SurveyId)
				.Index(t => t.QuestionId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Survey.SurveyConductQuestion",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					SurveyConductId = c.Long(nullable: false),
					Text = c.String(maxLength: 255),
					Type = c.Int(nullable: false),
					Order = c.Long(nullable: false),
					Items = c.String(),
					DefaultAnswer = c.String(),
					IsRequired = c.Boolean(nullable: false),
					FieldSize = c.Int(nullable: false),
					Description = c.String(maxLength: 1000),
					VisibilityCondition = c.String(),
					FieldName = c.String(maxLength: 255),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Survey.SurveyConduct", t => t.SurveyConductId)
				.Index(t => t.SurveyConductId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Survey.SurveyConduct",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					SurveyDefinitionId = c.Long(),
					SurveyName = c.String(),
					SurveyDescription = c.String(),
					ShouldAutoClose = c.Boolean(nullable: false),
					SurveyWelcomeInstruction = c.String(),
					SurveyThankYouMessage = c.String(),
					OpenedOn = c.DateTime(),
					OpenedById = c.Long(),
					IsClosed = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.OpenedById)
				.ForeignKey("Survey.SurveyDefinition", t => t.SurveyDefinitionId)
				.Index(t => t.SurveyDefinitionId)
				.Index(t => t.OpenedById)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Survey.SurveyDefinition",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(),
					Description = c.String(),
					ShouldAutoClose = c.Boolean(nullable: false),
					WelcomeInstruction = c.String(),
					ThankYouMessage = c.String(),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Survey.SurveyQuestion",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					SurveyDefinitionId = c.Long(nullable: false),
					Text = c.String(maxLength: 255),
					Type = c.Int(nullable: false),
					Order = c.Long(nullable: false),
					Items = c.String(),
					DefaultAnswer = c.String(),
					IsRequired = c.Boolean(nullable: false),
					FieldSize = c.Int(nullable: false),
					Description = c.String(maxLength: 1000),
					VisibilityCondition = c.String(),
					FieldName = c.String(maxLength: 255),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Survey.SurveyDefinition", t => t.SurveyDefinitionId, cascadeDelete: true)
				.Index(t => new { t.SurveyDefinitionId, t.FieldName }, unique: true, clustered: false, name: "IX_SurveyDefinitionIdFieldName")
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Survey.Survey",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					SurveyConductId = c.Long(nullable: false),
					RespondentId = c.Long(nullable: false),
					LastEditedAt = c.DateTime(),
					IsCompleted = c.Boolean(nullable: false),
					IsConfirmationSent = c.Boolean(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.ForeignKey("Accounts.User", t => t.RespondentId)
				.ForeignKey("Survey.SurveyConduct", t => t.SurveyConductId)
				.Index(t => t.SurveyConductId)
				.Index(t => t.RespondentId)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Configuration.SystemParameter",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Key = c.String(maxLength: 255, unicode: false),
					Value = c.String(),
					ValueType = c.String(maxLength: 255, unicode: false),
					ContextType = c.String(maxLength: 255, unicode: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Key, unique: true)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"Runtime.TemporaryDocumentPart",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					TemporaryDocumentId = c.Long(nullable: false),
					Data = c.Binary(),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Runtime.TemporaryDocument", t => t.TemporaryDocumentId)
				.Index(t => t.TemporaryDocumentId);

			CreateTable(
				"Runtime.TemporaryDocument",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(maxLength: 255),
					ContentType = c.String(maxLength: 1024, unicode: false),
					TemporaryDocumentStateEnumId = c.Int(nullable: false),
					Identifier = c.Guid(nullable: false),
					ContentLength = c.Long(nullable: false),
					UserIdImpersonatedBy = c.Long(),
					UserIdModifiedBy = c.Long(),
					ModifiedOn = c.DateTime(nullable: false),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
				.ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
				.Index(t => t.Identifier)
				.Index(t => t.UserIdImpersonatedBy)
				.Index(t => t.UserIdModifiedBy);

			CreateTable(
				"PeopleManagement.UserAcronym",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Acronym = c.String(maxLength: 20),
					Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
				})
				.PrimaryKey(t => t.Id)
				.Index(t => t.Acronym, unique: true);

			CreateTable(
				"Allocation.ActiveDirectoryGroupManagers",
				c => new
				{
					ActiveDirectoryGroup_Id = c.Long(nullable: false),
					User_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ActiveDirectoryGroup_Id, t.User_Id })
				.ForeignKey("Allocation.ActiveDirectoryGroup", t => t.ActiveDirectoryGroup_Id, cascadeDelete: true)
				.ForeignKey("Accounts.User", t => t.User_Id, cascadeDelete: true)
				.Index(t => t.ActiveDirectoryGroup_Id)
				.Index(t => t.User_Id);

			CreateTable(
				"Allocation.ActiveDirectoryGroupsUsers",
				c => new
				{
					User_Id = c.Long(nullable: false),
					ActiveDirectoryGroup_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.User_Id, t.ActiveDirectoryGroup_Id })
				.ForeignKey("Accounts.User", t => t.User_Id, cascadeDelete: true)
				.ForeignKey("Allocation.ActiveDirectoryGroup", t => t.ActiveDirectoryGroup_Id, cascadeDelete: true)
				.Index(t => t.User_Id)
				.Index(t => t.ActiveDirectoryGroup_Id);

			CreateTable(
				"Accounts.SecurityAreaSecurityRoles",
				c => new
				{
					AreaId = c.Long(nullable: false),
					RoleId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.AreaId, t.RoleId })
				.ForeignKey("Accounts.SecurityArea", t => t.AreaId, cascadeDelete: true)
				.ForeignKey("Accounts.SecurityRole", t => t.RoleId, cascadeDelete: true)
				.Index(t => t.AreaId)
				.Index(t => t.RoleId);

			CreateTable(
				"Accounts.SecurityRoleSecurityProfiles",
				c => new
				{
					RoleId = c.Long(nullable: false),
					ProfileId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.RoleId, t.ProfileId })
				.ForeignKey("Accounts.SecurityRole", t => t.RoleId, cascadeDelete: true)
				.ForeignKey("Accounts.SecurityProfile", t => t.ProfileId, cascadeDelete: true)
				.Index(t => t.RoleId)
				.Index(t => t.ProfileId);

			CreateTable(
				"Accounts.UserSecurityProfiles",
				c => new
				{
					UserId = c.Long(nullable: false),
					ProfileId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.UserId, t.ProfileId })
				.ForeignKey("Accounts.User", t => t.UserId, cascadeDelete: true)
				.ForeignKey("Accounts.SecurityProfile", t => t.ProfileId, cascadeDelete: true)
				.Index(t => t.UserId)
				.Index(t => t.ProfileId);

			CreateTable(
				"Accounts.UserSecurityRoles",
				c => new
				{
					UserId = c.Long(nullable: false),
					RoleId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.UserId, t.RoleId })
				.ForeignKey("Accounts.User", t => t.UserId, cascadeDelete: true)
				.ForeignKey("Accounts.SecurityRole", t => t.RoleId, cascadeDelete: true)
				.Index(t => t.UserId)
				.Index(t => t.RoleId);

			CreateTable(
				"TimeTracking.EmployeeCalendarAbsence",
				c => new
				{
					Employee_Id = c.Long(nullable: false),
					CalendarAbsence_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.Employee_Id, t.CalendarAbsence_Id })
				.ForeignKey("Allocation.Employee", t => t.Employee_Id, cascadeDelete: true)
				.ForeignKey("TimeTracking.CalendarAbsence", t => t.CalendarAbsence_Id, cascadeDelete: true)
				.Index(t => t.Employee_Id)
				.Index(t => t.CalendarAbsence_Id);

			CreateTable(
				"TimeTracking.ContractTypeAbsenceTypes",
				c => new
				{
					ContractTypeId = c.Long(nullable: false),
					AbsenceTypeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ContractTypeId, t.AbsenceTypeId })
				.ForeignKey("TimeTracking.ContractType", t => t.ContractTypeId, cascadeDelete: true)
				.ForeignKey("TimeTracking.AbsenceType", t => t.AbsenceTypeId, cascadeDelete: true)
				.Index(t => t.ContractTypeId)
				.Index(t => t.AbsenceTypeId);

			CreateTable(
				"SkillManagement.JobTitleSecurityProfiles",
				c => new
				{
					JobTitleId = c.Long(nullable: false),
					SecurityProfileId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.JobTitleId, t.SecurityProfileId })
				.ForeignKey("SkillManagement.JobTitle", t => t.JobTitleId, cascadeDelete: true)
				.ForeignKey("Accounts.SecurityProfile", t => t.SecurityProfileId, cascadeDelete: true)
				.Index(t => t.JobTitleId)
				.Index(t => t.SecurityProfileId);

			CreateTable(
				"SkillManagement.SkillTagPricingEngineer",
				c => new
				{
					SkillTag_Id = c.Long(nullable: false),
					PricingEngineer_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SkillTag_Id, t.PricingEngineer_Id })
				.ForeignKey("SkillManagement.SkillTag", t => t.SkillTag_Id, cascadeDelete: true)
				.ForeignKey("Organization.PricingEngineer", t => t.PricingEngineer_Id, cascadeDelete: true)
				.Index(t => t.SkillTag_Id)
				.Index(t => t.PricingEngineer_Id);

			CreateTable(
				"Resumes.ResumeRejectedSkillSuggestion",
				c => new
				{
					ResumeId = c.Long(nullable: false),
					SkillId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ResumeId, t.SkillId })
				.ForeignKey("Resumes.Resume", t => t.ResumeId, cascadeDelete: true)
				.ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
				.Index(t => t.ResumeId)
				.Index(t => t.SkillId);

			CreateTable(
				"Resumes.ResumeProjectIndustry",
				c => new
				{
					ResumeProjectId = c.Long(nullable: false),
					IndustryId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ResumeProjectId, t.IndustryId })
				.ForeignKey("Resumes.ResumeProject", t => t.ResumeProjectId, cascadeDelete: true)
				.ForeignKey("Dictionaries.Industry", t => t.IndustryId, cascadeDelete: true)
				.Index(t => t.ResumeProjectId)
				.Index(t => t.IndustryId);

			CreateTable(
				"Resumes.ResumeProjectTechnicalSkill",
				c => new
				{
					ResumeProjectId = c.Long(nullable: false),
					TechnicalSkillId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ResumeProjectId, t.TechnicalSkillId })
				.ForeignKey("Resumes.ResumeProject", t => t.ResumeProjectId, cascadeDelete: true)
				.ForeignKey("SkillManagement.Skill", t => t.TechnicalSkillId, cascadeDelete: true)
				.Index(t => t.ResumeProjectId)
				.Index(t => t.TechnicalSkillId);

			CreateTable(
				"Allocation.SkillsStaffingDemand",
				c => new
				{
					StaffingDemand_Id = c.Long(nullable: false),
					Skill_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.StaffingDemand_Id, t.Skill_Id })
				.ForeignKey("Allocation.StaffingDemand", t => t.StaffingDemand_Id, cascadeDelete: true)
				.ForeignKey("SkillManagement.Skill", t => t.Skill_Id, cascadeDelete: true)
				.Index(t => t.StaffingDemand_Id)
				.Index(t => t.Skill_Id);

			CreateTable(
				"SkillManagement.SkillTagSkill",
				c => new
				{
					SkillTag_Id = c.Long(nullable: false),
					Skill_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SkillTag_Id, t.Skill_Id })
				.ForeignKey("SkillManagement.SkillTag", t => t.SkillTag_Id, cascadeDelete: true)
				.ForeignKey("SkillManagement.Skill", t => t.Skill_Id, cascadeDelete: true)
				.Index(t => t.SkillTag_Id)
				.Index(t => t.Skill_Id);

			CreateTable(
				"SkillManagement.SkillTagTechnicalInterviewer",
				c => new
				{
					SkillTag_Id = c.Long(nullable: false),
					TechnicalInterviewer_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SkillTag_Id, t.TechnicalInterviewer_Id })
				.ForeignKey("SkillManagement.SkillTag", t => t.SkillTag_Id, cascadeDelete: true)
				.ForeignKey("Organization.TechnicalInterviewer", t => t.TechnicalInterviewer_Id, cascadeDelete: true)
				.Index(t => t.SkillTag_Id)
				.Index(t => t.TechnicalInterviewer_Id);

			CreateTable(
				"Workflows.EmployeeDataChangeRequestEmployeesMap",
				c => new
				{
					EmployeeDataChangeRequestId = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.EmployeeDataChangeRequestId, t.EmployeeId })
				.ForeignKey("Workflows.EmployeeDataChangeRequest", t => t.EmployeeDataChangeRequestId, cascadeDelete: true)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
				.Index(t => t.EmployeeDataChangeRequestId)
				.Index(t => t.EmployeeId);

			CreateTable(
				"BusinessTrips.ArrangementParticipants",
				c => new
				{
					ParticipantId = c.Long(nullable: false),
					ArrangementId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ParticipantId, t.ArrangementId })
				.ForeignKey("BusinessTrips.Arrangement", t => t.ParticipantId, cascadeDelete: true)
				.ForeignKey("BusinessTrips.BusinessTripParticipant", t => t.ArrangementId, cascadeDelete: true)
				.Index(t => t.ParticipantId)
				.Index(t => t.ArrangementId);

			CreateTable(
				"Workflows.BusinessTripRequestEmployeesMap",
				c => new
				{
					BusinessTripRequestId = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.BusinessTripRequestId, t.EmployeeId })
				.ForeignKey("Workflows.BusinessTripRequest", t => t.BusinessTripRequestId, cascadeDelete: true)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
				.Index(t => t.BusinessTripRequestId)
				.Index(t => t.EmployeeId);

			CreateTable(
				"Workflows.BusinessTripRequestProjectsMap",
				c => new
				{
					BusinessTripRequestId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.BusinessTripRequestId, t.ProjectId })
				.ForeignKey("Workflows.BusinessTripRequest", t => t.BusinessTripRequestId, cascadeDelete: true)
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.Index(t => t.BusinessTripRequestId)
				.Index(t => t.ProjectId);

			CreateTable(
				"BusinessTrips.BusinessTripProjectsMap",
				c => new
				{
					BusinessTripId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.BusinessTripId, t.ProjectId })
				.ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTripId, cascadeDelete: true)
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.Index(t => t.BusinessTripId)
				.Index(t => t.ProjectId);

			CreateTable(
				"Allocation.EmployeeJobMatrixLevel",
				c => new
				{
					Employee_Id = c.Long(nullable: false),
					JobMatrixLevel_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.Employee_Id, t.JobMatrixLevel_Id })
				.ForeignKey("Allocation.Employee", t => t.Employee_Id, cascadeDelete: true)
				.ForeignKey("SkillManagement.JobMatrixLevel", t => t.JobMatrixLevel_Id, cascadeDelete: true)
				.Index(t => t.Employee_Id)
				.Index(t => t.JobMatrixLevel_Id);

			CreateTable(
				"Allocation.EmployeeJobProfile",
				c => new
				{
					Employee_Id = c.Long(nullable: false),
					JobProfile_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.Employee_Id, t.JobProfile_Id })
				.ForeignKey("Allocation.Employee", t => t.Employee_Id, cascadeDelete: true)
				.ForeignKey("SkillManagement.JobProfile", t => t.JobProfile_Id, cascadeDelete: true)
				.Index(t => t.Employee_Id)
				.Index(t => t.JobProfile_Id);

			CreateTable(
				"TimeTracking.TimeReportRowAttributeValuesMap",
				c => new
				{
					TimeReportRowId = c.Long(nullable: false),
					AttributeValueId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.TimeReportRowId, t.AttributeValueId })
				.ForeignKey("TimeTracking.TimeReportRow", t => t.TimeReportRowId, cascadeDelete: true)
				.ForeignKey("TimeTracking.TimeReportProjectAttributeValue", t => t.AttributeValueId, cascadeDelete: true)
				.Index(t => t.TimeReportRowId)
				.Index(t => t.AttributeValueId);

			CreateTable(
				"TimeTracking.ProjectAttributesMap",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					AttributeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.AttributeId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("TimeTracking.TimeReportProjectAttribute", t => t.AttributeId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.AttributeId);

			CreateTable(
				"Allocation.IndustryProject",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					IndustryId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.IndustryId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("Dictionaries.Industry", t => t.IndustryId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.IndustryId);

			CreateTable(
				"Allocation.ProjectKeyTechnologies",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					SkillTagId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.SkillTagId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("SkillManagement.SkillTag", t => t.SkillTagId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.SkillTagId);

			CreateTable(
				"Allocation.ProjectServiceLines",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					ServiceLineId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.ServiceLineId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("Dictionaries.ServiceLine", t => t.ServiceLineId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.ServiceLineId);

			CreateTable(
				"Allocation.ProjectSoftwareCategories",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					SoftwareCategoryId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.SoftwareCategoryId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("Dictionaries.SoftwareCategory", t => t.SoftwareCategoryId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.SoftwareCategoryId);

			CreateTable(
				"Allocation.ProjectTagProject",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					ProjectTagId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.ProjectTagId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("Allocation.ProjectTag", t => t.ProjectTagId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.ProjectTagId);

			CreateTable(
				"Allocation.ProjectTechnologies",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					SkillId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.SkillId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.SkillId);

			CreateTable(
				"TimeTracking.ProjectAcceptingPersons",
				c => new
				{
					ProjectId = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ProjectId, t.EmployeeId })
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
				.Index(t => t.ProjectId)
				.Index(t => t.EmployeeId);

			CreateTable(
				"Workflows.AssetsRequestAssetType",
				c => new
				{
					AssetsRequestId = c.Long(nullable: false),
					AssetTypeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.AssetsRequestId, t.AssetTypeId })
				.ForeignKey("Workflows.AssetsRequest", t => t.AssetsRequestId, cascadeDelete: true)
				.ForeignKey("PeopleManagement.AssetType", t => t.AssetTypeId, cascadeDelete: true)
				.Index(t => t.AssetsRequestId)
				.Index(t => t.AssetTypeId);

			CreateTable(
				"Workflows.ReopenTimeTrackingReportRequestProjectsMap",
				c => new
				{
					ReopenTimeTrackingReportRequestId = c.Long(nullable: false),
					ProjectId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.ReopenTimeTrackingReportRequestId, t.ProjectId })
				.ForeignKey("Workflows.ReopenTimeTrackingReportRequest", t => t.ReopenTimeTrackingReportRequestId, cascadeDelete: true)
				.ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
				.Index(t => t.ReopenTimeTrackingReportRequestId)
				.Index(t => t.ProjectId);

			CreateTable(
				"Workflows.SkillChangeRequestSkillMap",
				c => new
				{
					SkillChangeRequestId = c.Long(nullable: false),
					SkillId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SkillChangeRequestId, t.SkillId })
				.ForeignKey("Workflows.SkillChangeRequest", t => t.SkillChangeRequestId, cascadeDelete: true)
				.ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
				.Index(t => t.SkillChangeRequestId)
				.Index(t => t.SkillId);

			CreateTable(
				"Workflows.RequestWatchers",
				c => new
				{
					RequestId = c.Long(nullable: false),
					UserId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.RequestId, t.UserId })
				.ForeignKey("Workflows.Request", t => t.RequestId, cascadeDelete: true)
				.ForeignKey("Accounts.User", t => t.UserId, cascadeDelete: true)
				.Index(t => t.RequestId)
				.Index(t => t.UserId);

			CreateTable(
				"dbo.LanguageOptionResourceForms",
				c => new
				{
					LanguageOption_Id = c.Long(nullable: false),
					ResourceForm_Id = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.LanguageOption_Id, t.ResourceForm_Id })
				.ForeignKey("CustomerPortal.LanguageOption", t => t.LanguageOption_Id, cascadeDelete: true)
				.ForeignKey("CustomerPortal.ResourceForm", t => t.ResourceForm_Id, cascadeDelete: true)
				.Index(t => t.LanguageOption_Id)
				.Index(t => t.ResourceForm_Id);

			CreateTable(
				"Survey.SurveyConductActiveDirectoryGroups",
				c => new
				{
					SurveyConductId = c.Long(nullable: false),
					ActiveDirectoryGroupId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SurveyConductId, t.ActiveDirectoryGroupId })
				.ForeignKey("Survey.SurveyConduct", t => t.SurveyConductId, cascadeDelete: true)
				.ForeignKey("Allocation.ActiveDirectoryGroup", t => t.ActiveDirectoryGroupId, cascadeDelete: true)
				.Index(t => t.SurveyConductId)
				.Index(t => t.ActiveDirectoryGroupId);

			CreateTable(
				"Survey.SurveyConductEmployees",
				c => new
				{
					SurveyConductId = c.Long(nullable: false),
					EmployeeId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SurveyConductId, t.EmployeeId })
				.ForeignKey("Survey.SurveyConduct", t => t.SurveyConductId, cascadeDelete: true)
				.ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
				.Index(t => t.SurveyConductId)
				.Index(t => t.EmployeeId);

			CreateTable(
				"Survey.SurveyConductOrgUnits",
				c => new
				{
					SurveyConductId = c.Long(nullable: false),
					OrgUnitId = c.Long(nullable: false),
				})
				.PrimaryKey(t => new { t.SurveyConductId, t.OrgUnitId })
				.ForeignKey("Survey.SurveyConduct", t => t.SurveyConductId, cascadeDelete: true)
				.ForeignKey("Organization.OrgUnit", t => t.OrgUnitId, cascadeDelete: true)
				.Index(t => t.SurveyConductId)
				.Index(t => t.OrgUnitId);


			SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201712151658242_Initial-Migration.Script.Up.sql");
		}

		public override void Down()
		{
			DropForeignKey("Runtime.TemporaryDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Runtime.TemporaryDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Runtime.TemporaryDocumentPart", "TemporaryDocumentId", "Runtime.TemporaryDocument");
			DropForeignKey("Configuration.SystemParameter", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Configuration.SystemParameter", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyAnswer", "QuestionId", "Survey.SurveyConductQuestion");
			DropForeignKey("Survey.Survey", "SurveyConductId", "Survey.SurveyConduct");
			DropForeignKey("Survey.Survey", "RespondentId", "Accounts.User");
			DropForeignKey("Survey.Survey", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Survey.Survey", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyAnswer", "SurveyId", "Survey.Survey");
			DropForeignKey("Survey.SurveyConduct", "SurveyDefinitionId", "Survey.SurveyDefinition");
			DropForeignKey("Survey.SurveyQuestion", "SurveyDefinitionId", "Survey.SurveyDefinition");
			DropForeignKey("Survey.SurveyQuestion", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyQuestion", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyDefinition", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyDefinition", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyConductQuestion", "SurveyConductId", "Survey.SurveyConduct");
			DropForeignKey("Survey.SurveyConductOrgUnits", "OrgUnitId", "Organization.OrgUnit");
			DropForeignKey("Survey.SurveyConductOrgUnits", "SurveyConductId", "Survey.SurveyConduct");
			DropForeignKey("Survey.SurveyConduct", "OpenedById", "Accounts.User");
			DropForeignKey("Survey.SurveyConduct", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyConduct", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyConductEmployees", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Survey.SurveyConductEmployees", "SurveyConductId", "Survey.SurveyConduct");
			DropForeignKey("Survey.SurveyConductActiveDirectoryGroups", "ActiveDirectoryGroupId", "Allocation.ActiveDirectoryGroup");
			DropForeignKey("Survey.SurveyConductActiveDirectoryGroups", "SurveyConductId", "Survey.SurveyConduct");
			DropForeignKey("Survey.SurveyConductQuestion", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyConductQuestion", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyAnswer", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Survey.SurveyAnswer", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Reporting.ReportTemplate", "ReportDefinitionId", "Reporting.ReportDefinition");
			DropForeignKey("Reporting.ReportTemplate", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Reporting.ReportTemplate", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Reporting.ReportTemplateContent", "Id", "Reporting.ReportTemplate");
			DropForeignKey("Reporting.ReportDefinition", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Reporting.ReportDefinition", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Questions", "UserId", "Accounts.User");
			DropForeignKey("CustomerPortal.Questions", "RecommendationId", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.Questions", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Questions", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.OvertimeApproval", "RequestId", "Workflows.Request");
			DropForeignKey("TimeTracking.OvertimeApproval", "ProjectId", "Allocation.Project");
			DropForeignKey("TimeTracking.OvertimeApproval", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Configuration.MessageTemplate", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Configuration.MessageTemplate", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobDefinitionParameter", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Scheduling.Pipeline", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobDefinition", "PipelineId", "Scheduling.Pipeline");
			DropForeignKey("Scheduling.Pipeline", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobDefinition", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Runtime.JobRunInfo", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Runtime.JobRunInfo", "JobDefinitionId", "Scheduling.JobDefinition");
			DropForeignKey("Runtime.JobRunInfo", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobLog", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobLog", "JobDefinitionId", "Scheduling.JobDefinition");
			DropForeignKey("Scheduling.JobLog", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobDefinitionParameter", "JobDefinitionId", "Scheduling.JobDefinition");
			DropForeignKey("Scheduling.JobDefinition", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Scheduling.JobDefinitionParameter", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.EmployeeComment", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.EmployeeComment", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.EmployeeComment", "EmployeeIdEmployee", "Allocation.Employee");
			DropForeignKey("Notifications.Email", "UserIdImpersonatedCreatedBy", "Accounts.User");
			DropForeignKey("Notifications.Email", "UserIdCreatedBy", "Accounts.User");
			DropForeignKey("Notifications.EmailAttachment", "EmailId", "Notifications.Email");
			DropForeignKey("Accounts.DataFilter", "UserId", "Accounts.User");
			DropForeignKey("Accounts.DataFilter", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.DataFilter", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Dictionaries.CustomerSize", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.CustomerSize", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Consents.ConsentDecision", "UserIdUser", "Accounts.User");
			DropForeignKey("Consents.ConsentDecision", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Consents.ConsentDecision", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ClientData", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ClientData", "UserIdManagerUser", "Accounts.User");
			DropForeignKey("CustomerPortal.ClientData", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ClientData", "UserIdClientUser", "Accounts.User");
			DropForeignKey("CustomerPortal.ClientData", "ClientContactIdClientContact", "CustomerPortal.ClientContact");
			DropForeignKey("CustomerPortal.ChallengeFile", "RecommendationIdRecommendation", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.ChallengeFile", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ChallengeFile", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("BusinessTrips.BusinessTripMessage", "BusinessTripId", "BusinessTrips.BusinessTrip");
			DropForeignKey("BusinessTrips.BusinessTripMessage", "AuthorEmployeeId", "Allocation.Employee");
			DropForeignKey("Finance.AggregatedProjectTransaction", "ProjectId", "Allocation.Project");
			DropForeignKey("Finance.AggregatedProjectTransaction", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Finance.AggregatedProjectTransaction", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ActivityLog", "ResourceFormIdResourceForm", "CustomerPortal.ResourceForm");
			DropForeignKey("CustomerPortal.ActivityLog", "RecommendationIdRecommendation", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.ScheduleAppointment", "RecommendationIdRecommendation", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.ScheduleAppointment", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ScheduleAppointment", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Note", "UserIdUser", "Accounts.User");
			DropForeignKey("CustomerPortal.Note", "RecommendationId", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.Note", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Note", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.NeededResources", "ResourceFormId", "CustomerPortal.ResourceForm");
			DropForeignKey("CustomerPortal.ResourceForm", "UserIdSubmittedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ResourceForm", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("dbo.LanguageOptionResourceForms", "ResourceForm_Id", "CustomerPortal.ResourceForm");
			DropForeignKey("dbo.LanguageOptionResourceForms", "LanguageOption_Id", "CustomerPortal.LanguageOption");
			DropForeignKey("CustomerPortal.ResourceForm", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ResourceForm", "UserIdCreatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ResourceForm", "UserIdApprovedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Recommendations", "NeededResourceId", "CustomerPortal.NeededResources");
			DropForeignKey("CustomerPortal.NeededResources", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.NeededResources", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Recommendations", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Recommendations", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Documents", "RecommendationId", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.Documents", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Documents", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.DocumentContent", "Id", "CustomerPortal.Documents");
			DropForeignKey("CustomerPortal.Challenges", "RecommendationIdRecommendation", "CustomerPortal.Recommendations");
			DropForeignKey("CustomerPortal.Challenges", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.Challenges", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ChallengeDocuments", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ChallengeDocuments", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ChallengeDocumentContents", "Id", "CustomerPortal.ChallengeDocuments");
			DropForeignKey("CustomerPortal.ChallengeDocuments", "ChallengeId", "CustomerPortal.Challenges");
			DropForeignKey("CustomerPortal.AnswerDocuments", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.AnswerDocuments", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.AnswerDocumentContents", "Id", "CustomerPortal.AnswerDocuments");
			DropForeignKey("CustomerPortal.AnswerDocuments", "ChallengeId", "CustomerPortal.Challenges");
			DropForeignKey("CustomerPortal.ActivityLog", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("CustomerPortal.ActivityLog", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.ActionSetTriggeringStatus", "ActionSetId", "Workflows.ActionSet");
			DropForeignKey("TimeTracking.AbsenceCalendarEntry", "RequestId", "Workflows.Request");
			DropForeignKey("Workflows.RequestWatchers", "UserId", "Accounts.User");
			DropForeignKey("Workflows.RequestWatchers", "RequestId", "Workflows.Request");
			DropForeignKey("Workflows.SkillChangeRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.SkillChangeRequestSkillMap", "SkillId", "SkillManagement.Skill");
			DropForeignKey("Workflows.SkillChangeRequestSkillMap", "SkillChangeRequestId", "Workflows.SkillChangeRequest");
			DropForeignKey("Workflows.SkillChangeRequest", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.Request", "RequestingUserId", "Accounts.User");
			DropForeignKey("Workflows.ReopenTimeTrackingReportRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.ReopenTimeTrackingReportRequestProjectsMap", "ProjectId", "Allocation.Project");
			DropForeignKey("Workflows.ReopenTimeTrackingReportRequestProjectsMap", "ReopenTimeTrackingReportRequestId", "Workflows.ReopenTimeTrackingReportRequest");
			DropForeignKey("Workflows.ProjectTimeReportApprovalRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.ProjectTimeReportApprovalRequest", "ProjectId", "Allocation.Project");
			DropForeignKey("Workflows.ProjectTimeReportApprovalRequest", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.OvertimeRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.OvertimeRequest", "ProjectId", "Allocation.Project");
			DropForeignKey("Workflows.OnboardingRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.Request", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.Request", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.RequestHistoryEntry", "RequestId", "Workflows.Request");
			DropForeignKey("Workflows.RequestHistoryEntry", "ExecutingUserId", "Accounts.User");
			DropForeignKey("Workflows.RequestHistoryEntry", "ApprovingUserId", "Accounts.User");
			DropForeignKey("Workflows.FeedbackRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.FeedbackRequest", "EvaluatorEmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.FeedbackRequest", "EvaluatedEmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.EmploymentTerminationRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.EmploymentTerminationRequest", "ReasonId", "PeopleManagement.EmploymentTerminationReason");
			DropForeignKey("Workflows.EmploymentTerminationRequest", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.EmploymentDataChangeRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.EmployeeDataChangeRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.RequestDocument", "RequestId", "Workflows.Request");
			DropForeignKey("Workflows.RequestDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.RequestDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.RequestDocumentContent", "Id", "Workflows.RequestDocument");
			DropForeignKey("Workflows.BusinessTripRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.AssetsRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.AssetsRequest", "ProjectId", "Allocation.Project");
			DropForeignKey("Workflows.EmployeePhotoDocument", "OnboardingRequestId", "Workflows.OnboardingRequest");
			DropForeignKey("Workflows.EmployeePhotoDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.EmployeePhotoDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.EmployeePhotoDocumentContent", "Id", "Workflows.EmployeePhotoDocument");
			DropForeignKey("Workflows.OnboardingRequest", "RecruiterEmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.OnboardingRequest", "ProbationPeriodContractTypeId", "TimeTracking.ContractType");
			DropForeignKey("Workflows.PayrollDocument", "OnboardingRequestId", "Workflows.OnboardingRequest");
			DropForeignKey("Workflows.PayrollDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.PayrollDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.PayrollDocumentContent", "Id", "Workflows.PayrollDocument");
			DropForeignKey("Workflows.OnboardingRequest", "OrgUnitId", "Organization.OrgUnit");
			DropForeignKey("Workflows.OnboardingRequest", "OnboardingLocationId", "Dictionaries.Location");
			DropForeignKey("Workflows.OnboardingRequest", "LocationId", "Dictionaries.Location");
			DropForeignKey("Workflows.OnboardingRequest", "LineManagerEmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.OnboardingRequest", "JobTitleId", "SkillManagement.JobTitle");
			DropForeignKey("Workflows.OnboardingRequest", "JobProfileId", "SkillManagement.JobProfile");
			DropForeignKey("Workflows.ForeignEmployeeDocument", "OnboardingRequestId", "Workflows.OnboardingRequest");
			DropForeignKey("Workflows.ForeignEmployeeDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.ForeignEmployeeDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.ForeignEmployeeDocumentContent", "Id", "Workflows.ForeignEmployeeDocument");
			DropForeignKey("Workflows.OnboardingRequest", "ContractTypeId", "TimeTracking.ContractType");
			DropForeignKey("Workflows.OnboardingRequest", "CompanyId", "Dictionaries.Company");
			DropForeignKey("Workflows.AssetsRequest", "OnboardingRequestId", "Workflows.OnboardingRequest");
			DropForeignKey("Workflows.AssetsRequest", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.AssetsRequestAssetType", "AssetTypeId", "PeopleManagement.AssetType");
			DropForeignKey("Workflows.AssetsRequestAssetType", "AssetsRequestId", "Workflows.AssetsRequest");
			DropForeignKey("Workflows.ApprovalGroup", "RequestId", "Workflows.Request");
			DropForeignKey("Workflows.ApprovalGroup", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.ApprovalGroup", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.Approvals", "UserId", "Accounts.User");
			DropForeignKey("Workflows.Approvals", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Workflows.Approvals", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.Approvals", "ForcedByUserId", "Accounts.User");
			DropForeignKey("Workflows.Approvals", "ApprovalGroupId", "Workflows.ApprovalGroup");
			DropForeignKey("Workflows.AdvancedPaymentRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.AdvancedPaymentRequest", "CurrencyId", "BusinessTrips.Currency");
			DropForeignKey("Workflows.AdvancedPaymentRequest", "BusinessTripId", "BusinessTrips.BusinessTrip");
			DropForeignKey("Workflows.AddHomeOfficeRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.AddEmployeeRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.AbsenceRequest", "Id", "Workflows.Request");
			DropForeignKey("Workflows.AbsenceRequest", "SubstituteUserId", "Accounts.User");
			DropForeignKey("Workflows.AbsenceRequest", "AffectedUserId", "Accounts.User");
			DropForeignKey("Workflows.AbsenceRequest", "AbsenceTypeId", "TimeTracking.AbsenceType");
			DropForeignKey("TimeTracking.AbsenceType", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.AbsenceType", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.AbsenceType", "AbsenceProjectId", "Allocation.Project");
			DropForeignKey("Allocation.Project", "UtilizationCategoryId", "Dictionaries.UtilizationCategory");
			DropForeignKey("Dictionaries.UtilizationCategory", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.UtilizationCategory", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.Project", "TimeTrackingImportSourceId", "TimeTracking.TimeTrackingImportSource");
			DropForeignKey("TimeTracking.ProjectAcceptingPersons", "EmployeeId", "Allocation.Employee");
			DropForeignKey("TimeTracking.ProjectAcceptingPersons", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.ProjectTechnologies", "SkillId", "SkillManagement.Skill");
			DropForeignKey("Allocation.ProjectTechnologies", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.ProjectTagProject", "ProjectTagId", "Allocation.ProjectTag");
			DropForeignKey("Allocation.ProjectTagProject", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.ProjectTag", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectTag", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.Project", "SupervisorManagerId", "Allocation.Employee");
			DropForeignKey("Allocation.ProjectSoftwareCategories", "SoftwareCategoryId", "Dictionaries.SoftwareCategory");
			DropForeignKey("Allocation.ProjectSoftwareCategories", "ProjectId", "Allocation.Project");
			DropForeignKey("Dictionaries.SoftwareCategory", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.SoftwareCategory", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectServiceLines", "ServiceLineId", "Dictionaries.ServiceLine");
			DropForeignKey("Allocation.ProjectServiceLines", "ProjectId", "Allocation.Project");
			DropForeignKey("Dictionaries.ServiceLine", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.ServiceLine", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.Project", "SalesAccountManagerId", "Allocation.Employee");
			DropForeignKey("Allocation.Project", "ProjectManagerId", "Allocation.Employee");
			DropForeignKey("Allocation.Project", "PictureId", "Allocation.ProjectPicture");
			DropForeignKey("Allocation.ProjectPicture", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectPicture", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectPictureContent", "Id", "Allocation.ProjectPicture");
			DropForeignKey("Allocation.Project", "OrgUnitId", "Organization.OrgUnit");
			DropForeignKey("Allocation.Project", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectKeyTechnologies", "SkillTagId", "SkillManagement.SkillTag");
			DropForeignKey("Allocation.ProjectKeyTechnologies", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.Project", "JiraIssueToTimeReportRowMapperId", "TimeTracking.JiraIssueToTimeReportRowMapper");
			DropForeignKey("TimeTracking.JiraIssueToTimeReportRowMapper", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.JiraIssueToTimeReportRowMapper", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.IndustryProject", "IndustryId", "Dictionaries.Industry");
			DropForeignKey("Allocation.IndustryProject", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.Project", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.Project", "CalendarId", "Configuration.Calendar");
			DropForeignKey("TimeTracking.ProjectAttributesMap", "AttributeId", "TimeTracking.TimeReportProjectAttribute");
			DropForeignKey("TimeTracking.ProjectAttributesMap", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.ProjectAttachment", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.ProjectAttachment", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectAttachment", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.ProjectAttachmentContent", "Id", "Allocation.ProjectAttachment");
			DropForeignKey("Allocation.AllocationRequest", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.AllocationRequest", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.AllocationRequest", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.WeeklyAllocationStatus", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Allocation.WeeklyAllocationDetail", "WeeklyAllocationStatusId", "Allocation.WeeklyAllocationStatus");
			DropForeignKey("Allocation.WeeklyAllocationDetail", "ProjectId", "Allocation.Project");
			DropForeignKey("TimeTracking.VacationBalance", "RequestId", "Workflows.Request");
			DropForeignKey("TimeTracking.VacationBalance", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Allocation.Employee", "UserId", "Accounts.User");
			DropForeignKey("TimeTracking.TimeReportRow", "TimeReportId", "TimeTracking.TimeReport");
			DropForeignKey("TimeTracking.TimeReportRow", "RequestId", "Workflows.Request");
			DropForeignKey("TimeTracking.TimeReportRow", "ProjectId", "Allocation.Project");
			DropForeignKey("TimeTracking.OvertimeBank", "TimeReportRowId", "TimeTracking.TimeReportRow");
			DropForeignKey("TimeTracking.TimeReportRow", "ImportSourceId", "TimeTracking.TimeTrackingImportSource");
			DropForeignKey("TimeTracking.TimeReportDailyEntry", "TimeReportRowId", "TimeTracking.TimeReportRow");
			DropForeignKey("TimeTracking.TimeReportRowAttributeValuesMap", "AttributeValueId", "TimeTracking.TimeReportProjectAttributeValue");
			DropForeignKey("TimeTracking.TimeReportRowAttributeValuesMap", "TimeReportRowId", "TimeTracking.TimeReportRow");
			DropForeignKey("TimeTracking.TimeReportProjectAttributeValue", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.TimeReportProjectAttributeValue", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.TimeReportProjectAttributeValue", "AttributeId", "TimeTracking.TimeReportProjectAttribute");
			DropForeignKey("TimeTracking.TimeReportProjectAttribute", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.TimeReportProjectAttribute", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.TimeReportProjectAttribute", "DefaultValueId", "TimeTracking.TimeReportProjectAttributeValue");
			DropForeignKey("TimeTracking.TimeReportRow", "AbsenceTypeId", "TimeTracking.AbsenceType");
			DropForeignKey("TimeTracking.OvertimeBank", "TimeReportId", "TimeTracking.TimeReport");
			DropForeignKey("TimeTracking.TimeReport", "EmployeeId", "Allocation.Employee");
			DropForeignKey("TimeTracking.ClockCardDailyEntry", "TimeReportId", "TimeTracking.TimeReport");
			DropForeignKey("TimeTracking.OvertimeBank", "RequestId", "Workflows.Request");
			DropForeignKey("TimeTracking.OvertimeBank", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Allocation.Employee", "OrgUnitId", "Organization.OrgUnit");
			DropForeignKey("Allocation.Employee", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("MeetMe.Meeting", "EmployeeIdEmployee", "Allocation.Employee");
			DropForeignKey("MeetMe.MeetingNote", "MeetingId", "MeetMe.Meeting");
			DropForeignKey("MeetMe.MeetingNote", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingNote", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingNote", "EmployeeIdAuthor", "Allocation.Employee");
			DropForeignKey("MeetMe.MeetingNoteAttachment", "MeetingNote_Id", "MeetMe.MeetingNote");
			DropForeignKey("MeetMe.MeetingNoteAttachment", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingNoteAttachment", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingNoteAttachmentContent", "Id", "MeetMe.MeetingNoteAttachment");
			DropForeignKey("MeetMe.Meeting", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("MeetMe.Meeting", "LocationIdLocation", "Dictionaries.Location");
			DropForeignKey("MeetMe.Meeting", "EmployeeIdLineManager", "Allocation.Employee");
			DropForeignKey("MeetMe.Meeting", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingAttachment", "Meeting_Id", "MeetMe.Meeting");
			DropForeignKey("MeetMe.MeetingAttachment", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingAttachment", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("MeetMe.MeetingAttachmentContent", "Id", "MeetMe.MeetingAttachment");
			DropForeignKey("Allocation.Employee", "LineManagerId", "Allocation.Employee");
			DropForeignKey("Allocation.Employee", "JobTitleId", "SkillManagement.JobTitle");
			DropForeignKey("Allocation.EmployeeJobProfile", "JobProfile_Id", "SkillManagement.JobProfile");
			DropForeignKey("Allocation.EmployeeJobProfile", "Employee_Id", "Allocation.Employee");
			DropForeignKey("Allocation.EmployeeJobMatrixLevel", "JobMatrixLevel_Id", "SkillManagement.JobMatrixLevel");
			DropForeignKey("Allocation.EmployeeJobMatrixLevel", "Employee_Id", "Allocation.Employee");
			DropForeignKey("Allocation.Employee", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("MeetMe.Feedback", "EmployeeIdAuthor", "Allocation.Employee");
			DropForeignKey("MeetMe.Feedback", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("MeetMe.Feedback", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.EmployeeAbsence", "RequestId", "Workflows.Request");
			DropForeignKey("TimeTracking.EmployeeAbsence", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.EmployeeAbsence", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.EmployeeAbsence", "EmployeeId", "Allocation.Employee");
			DropForeignKey("TimeTracking.EmployeeAbsence", "AbsenceTypeId", "TimeTracking.AbsenceType");
			DropForeignKey("Allocation.Employee", "CompanyId", "Dictionaries.Company");
			DropForeignKey("Workflows.AddEmployeeRequest", "OrgUnitId", "Organization.OrgUnit");
			DropForeignKey("Workflows.AddEmployeeRequest", "LineManagerId", "Allocation.Employee");
			DropForeignKey("Workflows.AddEmployeeRequest", "JobTitleId", "SkillManagement.JobTitle");
			DropForeignKey("Workflows.EmployeeDataChangeRequest", "OrgUnitId", "Organization.OrgUnit");
			DropForeignKey("Organization.OrgUnit", "RegionId", "Dictionaries.Region");
			DropForeignKey("Dictionaries.Region", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.Region", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Organization.OrgUnit", "ParentId", "Organization.OrgUnit");
			DropForeignKey("Organization.OrgUnit", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Organization.OrgUnit", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Organization.OrgUnit", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Organization.OrgUnit", "CalendarId", "Configuration.Calendar");
			DropForeignKey("Organization.OrgUnit", "AbsenceOnlyDefaultProjectId", "Allocation.Project");
			DropForeignKey("Workflows.EmployeeDataChangeRequest", "LocationId", "Dictionaries.Location");
			DropForeignKey("Allocation.Employee", "LocationId", "Dictionaries.Location");
			DropForeignKey("Workflows.AddEmployeeRequest", "LocationId", "Dictionaries.Location");
			DropForeignKey("Dictionaries.Location", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.Location", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("BusinessTrips.BusinessTrip", "RequestId", "Workflows.Request");
			DropForeignKey("BusinessTrips.BusinessTripProjectsMap", "ProjectId", "Allocation.Project");
			DropForeignKey("BusinessTrips.BusinessTripProjectsMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
			DropForeignKey("Workflows.BusinessTripRequestProjectsMap", "ProjectId", "Allocation.Project");
			DropForeignKey("Workflows.BusinessTripRequestProjectsMap", "BusinessTripRequestId", "Workflows.BusinessTripRequest");
			DropForeignKey("Workflows.BusinessTripRequestEmployeesMap", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.BusinessTripRequestEmployeesMap", "BusinessTripRequestId", "Workflows.BusinessTripRequest");
			DropForeignKey("Workflows.DeclaredLuggage", "RequestId", "Workflows.BusinessTripRequest");
			DropForeignKey("Workflows.BusinessTripRequest", "DestinationCityId", "Dictionaries.City");
			DropForeignKey("Workflows.BusinessTripRequest", "DepartureCityId", "Dictionaries.City");
			DropForeignKey("Workflows.DeclaredLuggage", "ParticipantId", "BusinessTrips.BusinessTripParticipant");
			DropForeignKey("Workflows.DeclaredLuggage", "EmployeeId", "Allocation.Employee");
			DropForeignKey("BusinessTrips.BusinessTripParticipant", "EmployeeId", "Allocation.Employee");
			DropForeignKey("BusinessTrips.BusinessTripParticipant", "BusinessTripId", "BusinessTrips.BusinessTrip");
			DropForeignKey("BusinessTrips.ArrangementTrackingEntry", "ParticipantId", "BusinessTrips.BusinessTripParticipant");
			DropForeignKey("BusinessTrips.ArrangementParticipants", "ArrangementId", "BusinessTrips.BusinessTripParticipant");
			DropForeignKey("BusinessTrips.ArrangementParticipants", "ParticipantId", "BusinessTrips.Arrangement");
			DropForeignKey("BusinessTrips.Arrangement", "CurrencyId", "BusinessTrips.Currency");
			DropForeignKey("BusinessTrips.Currency", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("BusinessTrips.Currency", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("BusinessTrips.Arrangement", "BusinessTripId", "BusinessTrips.BusinessTrip");
			DropForeignKey("BusinessTrips.ArrangementDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("BusinessTrips.ArrangementDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("BusinessTrips.ArrangementDocumentContent", "Id", "BusinessTrips.ArrangementDocument");
			DropForeignKey("BusinessTrips.ArrangementDocument", "ArrangementId", "BusinessTrips.Arrangement");
			DropForeignKey("BusinessTrips.BusinessTrip", "DestinationCityId", "Dictionaries.City");
			DropForeignKey("BusinessTrips.BusinessTrip", "DepartureCityId", "Dictionaries.City");
			DropForeignKey("Dictionaries.Location", "CityId", "Dictionaries.City");
			DropForeignKey("Dictionaries.City", "CountryId", "Dictionaries.Country");
			DropForeignKey("Workflows.EmployeeDataChangeRequest", "LineManagerId", "Allocation.Employee");
			DropForeignKey("Workflows.EmployeeDataChangeRequest", "JobProfileId", "SkillManagement.JobProfile");
			DropForeignKey("Workflows.EmployeeDataChangeRequestEmployeesMap", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.EmployeeDataChangeRequestEmployeesMap", "EmployeeDataChangeRequestId", "Workflows.EmployeeDataChangeRequest");
			DropForeignKey("Workflows.AddEmployeeRequest", "JobProfileId", "SkillManagement.JobProfile");
			DropForeignKey("SkillManagement.JobProfileSkillTag", "JobProfileId", "SkillManagement.JobProfile");
			DropForeignKey("SkillManagement.JobProfileSkillTag", "SkillTagId", "SkillManagement.SkillTag");
			DropForeignKey("SkillManagement.SkillTagTechnicalInterviewer", "TechnicalInterviewer_Id", "Organization.TechnicalInterviewer");
			DropForeignKey("SkillManagement.SkillTagTechnicalInterviewer", "SkillTag_Id", "SkillManagement.SkillTag");
			DropForeignKey("Organization.TechnicalInterviewer", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Organization.TechnicalInterviewer", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Organization.TechnicalInterviewer", "EmployeeIdEmployee", "Allocation.Employee");
			DropForeignKey("SkillManagement.SkillTagSkill", "Skill_Id", "SkillManagement.Skill");
			DropForeignKey("SkillManagement.SkillTagSkill", "SkillTag_Id", "SkillManagement.SkillTag");
			DropForeignKey("Allocation.SkillsStaffingDemand", "Skill_Id", "SkillManagement.Skill");
			DropForeignKey("Allocation.SkillsStaffingDemand", "StaffingDemand_Id", "Allocation.StaffingDemand");
			DropForeignKey("Allocation.StaffingDemand", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.StaffingDemand", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.StaffingDemand", "JobProfileId", "SkillManagement.JobProfile");
			DropForeignKey("Allocation.StaffingDemand", "JobMatrixLevelId", "SkillManagement.JobMatrixLevel");
			DropForeignKey("Allocation.StaffingDemand", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeProjectTechnicalSkill", "TechnicalSkillId", "SkillManagement.Skill");
			DropForeignKey("Resumes.ResumeProjectTechnicalSkill", "ResumeProjectId", "Resumes.ResumeProject");
			DropForeignKey("Resumes.ResumeProject", "ProjectId", "Allocation.Project");
			DropForeignKey("Resumes.ResumeProject", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeProjectIndustry", "IndustryId", "Dictionaries.Industry");
			DropForeignKey("Resumes.ResumeProjectIndustry", "ResumeProjectId", "Resumes.ResumeProject");
			DropForeignKey("Dictionaries.Industry", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.Industry", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeProject", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeProject", "CompanyId", "Resumes.ResumeCompany");
			DropForeignKey("Resumes.ResumeTraining", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeTraining", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeTraining", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeTechnicalSkill", "TechnicalSkillId", "SkillManagement.Skill");
			DropForeignKey("Resumes.ResumeTechnicalSkill", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeTechnicalSkill", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeTechnicalSkill", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeRejectedSkillSuggestion", "SkillId", "SkillManagement.Skill");
			DropForeignKey("Resumes.ResumeRejectedSkillSuggestion", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeLanguage", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeLanguage", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeLanguage", "LanguageId", "Dictionaries.Language");
			DropForeignKey("Dictionaries.Language", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.Language", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeLanguage", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeEducation", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeEducation", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeEducation", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeCompany", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeAdditionalSkill", "ResumeId", "Resumes.Resume");
			DropForeignKey("Resumes.ResumeAdditionalSkill", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeAdditionalSkill", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.Resume", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.Resume", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Resumes.Resume", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Resumes.ResumeCompany", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Resumes.ResumeCompany", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.Skill", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.Skill", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.SkillTagPricingEngineer", "PricingEngineer_Id", "Organization.PricingEngineer");
			DropForeignKey("SkillManagement.SkillTagPricingEngineer", "SkillTag_Id", "SkillManagement.SkillTag");
			DropForeignKey("Organization.PricingEngineer", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Organization.PricingEngineer", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Organization.PricingEngineer", "EmployeeIdEmployee", "Allocation.Employee");
			DropForeignKey("SkillManagement.SkillTag", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.SkillTag", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobProfileSkillTag", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobProfileSkillTag", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobProfile", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobProfile", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.AddEmployeeRequest", "ContractTypeId", "TimeTracking.ContractType");
			DropForeignKey("Allocation.EmploymentPeriod", "TerminationReasonId", "PeopleManagement.EmploymentTerminationReason");
			DropForeignKey("Allocation.EmploymentPeriod", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.EmploymentPeriod", "JobTitleId", "SkillManagement.JobTitle");
			DropForeignKey("Allocation.EmploymentPeriod", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.EmploymentPeriod", "EmployeeIdEmployee", "Allocation.Employee");
			DropForeignKey("Allocation.EmploymentPeriod", "ContractTypeId", "TimeTracking.ContractType");
			DropForeignKey("Allocation.EmploymentPeriod", "CompanyId", "Dictionaries.Company");
			DropForeignKey("Allocation.EmploymentPeriod", "CalendarId", "Configuration.Calendar");
			DropForeignKey("Allocation.EmploymentPeriod", "ProjectIdAbsenceOnlyDefaultProject", "Allocation.Project");
			DropForeignKey("Workflows.EmploymentDataChangeRequest", "JobTitleId", "SkillManagement.JobTitle");
			DropForeignKey("SkillManagement.JobTitle", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobTitle", "JobMatrixLevelId", "SkillManagement.JobMatrixLevel");
			DropForeignKey("SkillManagement.JobMatrixLevel", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobMatrix", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobMatrixLevel", "JobMatrixId", "SkillManagement.JobMatrix");
			DropForeignKey("SkillManagement.JobMatrix", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobMatrixLevel", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobTitle", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("SkillManagement.JobTitleSecurityProfiles", "SecurityProfileId", "Accounts.SecurityProfile");
			DropForeignKey("SkillManagement.JobTitleSecurityProfiles", "JobTitleId", "SkillManagement.JobTitle");
			DropForeignKey("Workflows.EmploymentDataChangeRequest", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Workflows.EmploymentDataChangeRequest", "ContractTypeId", "TimeTracking.ContractType");
			DropForeignKey("Workflows.EmploymentDataChangeRequest", "CompanyId", "Dictionaries.Company");
			DropForeignKey("TimeTracking.ContractType", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.ContractType", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.ContractTypeAbsenceTypes", "AbsenceTypeId", "TimeTracking.AbsenceType");
			DropForeignKey("TimeTracking.ContractTypeAbsenceTypes", "ContractTypeId", "TimeTracking.ContractType");
			DropForeignKey("Workflows.AddEmployeeRequest", "CompanyId", "Dictionaries.Company");
			DropForeignKey("Workflows.AddEmployeeRequest", "CalendarId", "Configuration.Calendar");
			DropForeignKey("Workflows.AddEmployeeRequest", "AbsenceOnlyDefaultProjectId", "Allocation.Project");
			DropForeignKey("Dictionaries.Company", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Dictionaries.Company", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.EmployeeCalendarAbsence", "CalendarAbsence_Id", "TimeTracking.CalendarAbsence");
			DropForeignKey("TimeTracking.EmployeeCalendarAbsence", "Employee_Id", "Allocation.Employee");
			DropForeignKey("TimeTracking.CalendarAbsence", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("TimeTracking.CalendarAbsence", "UserIdImpersonatedCreatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.CalendarAbsence", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("TimeTracking.CalendarAbsence", "UserIdCreatedBy", "Accounts.User");
			DropForeignKey("Allocation.AllocationRequest", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Allocation.AllocationDailyRecord", "ProjectId", "Allocation.Project");
			DropForeignKey("Allocation.AllocationDailyRecord", "EmployeeId", "Allocation.Employee");
			DropForeignKey("Allocation.AllocationDailyRecord", "CalendarId", "Configuration.Calendar");
			DropForeignKey("Configuration.Calendar", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Configuration.Calendar", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Configuration.CalendarEntry", "CalendarId", "Configuration.Calendar");
			DropForeignKey("Configuration.CalendarEntry", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Configuration.CalendarEntry", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Workflows.Substitution", "SubstituteUserId", "Accounts.User");
			DropForeignKey("Workflows.Substitution", "RequestId", "Workflows.Request");
			DropForeignKey("Workflows.Substitution", "ReplacedUserId", "Accounts.User");
			DropForeignKey("Accounts.User", "SecurityAreaId", "Accounts.SecurityArea");
			DropForeignKey("Accounts.UserSecurityRoles", "RoleId", "Accounts.SecurityRole");
			DropForeignKey("Accounts.UserSecurityRoles", "UserId", "Accounts.User");
			DropForeignKey("Accounts.UserSecurityProfiles", "ProfileId", "Accounts.SecurityProfile");
			DropForeignKey("Accounts.UserSecurityProfiles", "UserId", "Accounts.User");
			DropForeignKey("Accounts.SecurityRoleSecurityProfiles", "ProfileId", "Accounts.SecurityProfile");
			DropForeignKey("Accounts.SecurityRoleSecurityProfiles", "RoleId", "Accounts.SecurityRole");
			DropForeignKey("Accounts.SecurityAreaSecurityRoles", "RoleId", "Accounts.SecurityRole");
			DropForeignKey("Accounts.SecurityAreaSecurityRoles", "AreaId", "Accounts.SecurityArea");
			DropForeignKey("Accounts.SecurityArea", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.SecurityArea", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Accounts.SecurityProfile", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.SecurityProfile", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Accounts.User", "PictureId", "Accounts.UserPicture");
			DropForeignKey("Accounts.UserPicture", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.UserPicture", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Accounts.UserPictureContent", "Id", "Accounts.UserPicture");
			DropForeignKey("Accounts.User", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.User", "UserLoginTokenIdLoginToken", "Accounts.UserLoginToken");
			DropForeignKey("Accounts.UserLoginToken", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.UserLoginToken", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Accounts.User", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Accounts.UserDocument", "UserId", "Accounts.User");
			DropForeignKey("Accounts.UserDocument", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.UserDocument", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Accounts.UserDocumentContent", "Id", "Accounts.UserDocument");
			DropForeignKey("Accounts.Credential", "UserId", "Accounts.User");
			DropForeignKey("Accounts.Credential", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Accounts.Credential", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.ActiveDirectoryGroupsUsers", "ActiveDirectoryGroup_Id", "Allocation.ActiveDirectoryGroup");
			DropForeignKey("Allocation.ActiveDirectoryGroupsUsers", "User_Id", "Accounts.User");
			DropForeignKey("Allocation.ActiveDirectoryGroup", "OwnerId", "Accounts.User");
			DropForeignKey("Allocation.ActiveDirectoryGroup", "UserIdModifiedBy", "Accounts.User");
			DropForeignKey("Allocation.ActiveDirectoryGroupManagers", "User_Id", "Accounts.User");
			DropForeignKey("Allocation.ActiveDirectoryGroupManagers", "ActiveDirectoryGroup_Id", "Allocation.ActiveDirectoryGroup");
			DropForeignKey("Allocation.ActiveDirectoryGroup", "UserIdImpersonatedBy", "Accounts.User");
			DropForeignKey("Allocation.AllocationDailyRecord", "AllocationRequestId", "Allocation.AllocationRequest");
			DropForeignKey("Allocation.AllocationRequestMetadata", "AllocationRequestId", "Allocation.AllocationRequest");
			DropIndex("Survey.SurveyConductOrgUnits", new[] { "OrgUnitId" });
			DropIndex("Survey.SurveyConductOrgUnits", new[] { "SurveyConductId" });
			DropIndex("Survey.SurveyConductEmployees", new[] { "EmployeeId" });
			DropIndex("Survey.SurveyConductEmployees", new[] { "SurveyConductId" });
			DropIndex("Survey.SurveyConductActiveDirectoryGroups", new[] { "ActiveDirectoryGroupId" });
			DropIndex("Survey.SurveyConductActiveDirectoryGroups", new[] { "SurveyConductId" });
			DropIndex("dbo.LanguageOptionResourceForms", new[] { "ResourceForm_Id" });
			DropIndex("dbo.LanguageOptionResourceForms", new[] { "LanguageOption_Id" });
			DropIndex("Workflows.RequestWatchers", new[] { "UserId" });
			DropIndex("Workflows.RequestWatchers", new[] { "RequestId" });
			DropIndex("Workflows.SkillChangeRequestSkillMap", new[] { "SkillId" });
			DropIndex("Workflows.SkillChangeRequestSkillMap", new[] { "SkillChangeRequestId" });
			DropIndex("Workflows.ReopenTimeTrackingReportRequestProjectsMap", new[] { "ProjectId" });
			DropIndex("Workflows.ReopenTimeTrackingReportRequestProjectsMap", new[] { "ReopenTimeTrackingReportRequestId" });
			DropIndex("Workflows.AssetsRequestAssetType", new[] { "AssetTypeId" });
			DropIndex("Workflows.AssetsRequestAssetType", new[] { "AssetsRequestId" });
			DropIndex("TimeTracking.ProjectAcceptingPersons", new[] { "EmployeeId" });
			DropIndex("TimeTracking.ProjectAcceptingPersons", new[] { "ProjectId" });
			DropIndex("Allocation.ProjectTechnologies", new[] { "SkillId" });
			DropIndex("Allocation.ProjectTechnologies", new[] { "ProjectId" });
			DropIndex("Allocation.ProjectTagProject", new[] { "ProjectTagId" });
			DropIndex("Allocation.ProjectTagProject", new[] { "ProjectId" });
			DropIndex("Allocation.ProjectSoftwareCategories", new[] { "SoftwareCategoryId" });
			DropIndex("Allocation.ProjectSoftwareCategories", new[] { "ProjectId" });
			DropIndex("Allocation.ProjectServiceLines", new[] { "ServiceLineId" });
			DropIndex("Allocation.ProjectServiceLines", new[] { "ProjectId" });
			DropIndex("Allocation.ProjectKeyTechnologies", new[] { "SkillTagId" });
			DropIndex("Allocation.ProjectKeyTechnologies", new[] { "ProjectId" });
			DropIndex("Allocation.IndustryProject", new[] { "IndustryId" });
			DropIndex("Allocation.IndustryProject", new[] { "ProjectId" });
			DropIndex("TimeTracking.ProjectAttributesMap", new[] { "AttributeId" });
			DropIndex("TimeTracking.ProjectAttributesMap", new[] { "ProjectId" });
			DropIndex("TimeTracking.TimeReportRowAttributeValuesMap", new[] { "AttributeValueId" });
			DropIndex("TimeTracking.TimeReportRowAttributeValuesMap", new[] { "TimeReportRowId" });
			DropIndex("Allocation.EmployeeJobProfile", new[] { "JobProfile_Id" });
			DropIndex("Allocation.EmployeeJobProfile", new[] { "Employee_Id" });
			DropIndex("Allocation.EmployeeJobMatrixLevel", new[] { "JobMatrixLevel_Id" });
			DropIndex("Allocation.EmployeeJobMatrixLevel", new[] { "Employee_Id" });
			DropIndex("BusinessTrips.BusinessTripProjectsMap", new[] { "ProjectId" });
			DropIndex("BusinessTrips.BusinessTripProjectsMap", new[] { "BusinessTripId" });
			DropIndex("Workflows.BusinessTripRequestProjectsMap", new[] { "ProjectId" });
			DropIndex("Workflows.BusinessTripRequestProjectsMap", new[] { "BusinessTripRequestId" });
			DropIndex("Workflows.BusinessTripRequestEmployeesMap", new[] { "EmployeeId" });
			DropIndex("Workflows.BusinessTripRequestEmployeesMap", new[] { "BusinessTripRequestId" });
			DropIndex("BusinessTrips.ArrangementParticipants", new[] { "ArrangementId" });
			DropIndex("BusinessTrips.ArrangementParticipants", new[] { "ParticipantId" });
			DropIndex("Workflows.EmployeeDataChangeRequestEmployeesMap", new[] { "EmployeeId" });
			DropIndex("Workflows.EmployeeDataChangeRequestEmployeesMap", new[] { "EmployeeDataChangeRequestId" });
			DropIndex("SkillManagement.SkillTagTechnicalInterviewer", new[] { "TechnicalInterviewer_Id" });
			DropIndex("SkillManagement.SkillTagTechnicalInterviewer", new[] { "SkillTag_Id" });
			DropIndex("SkillManagement.SkillTagSkill", new[] { "Skill_Id" });
			DropIndex("SkillManagement.SkillTagSkill", new[] { "SkillTag_Id" });
			DropIndex("Allocation.SkillsStaffingDemand", new[] { "Skill_Id" });
			DropIndex("Allocation.SkillsStaffingDemand", new[] { "StaffingDemand_Id" });
			DropIndex("Resumes.ResumeProjectTechnicalSkill", new[] { "TechnicalSkillId" });
			DropIndex("Resumes.ResumeProjectTechnicalSkill", new[] { "ResumeProjectId" });
			DropIndex("Resumes.ResumeProjectIndustry", new[] { "IndustryId" });
			DropIndex("Resumes.ResumeProjectIndustry", new[] { "ResumeProjectId" });
			DropIndex("Resumes.ResumeRejectedSkillSuggestion", new[] { "SkillId" });
			DropIndex("Resumes.ResumeRejectedSkillSuggestion", new[] { "ResumeId" });
			DropIndex("SkillManagement.SkillTagPricingEngineer", new[] { "PricingEngineer_Id" });
			DropIndex("SkillManagement.SkillTagPricingEngineer", new[] { "SkillTag_Id" });
			DropIndex("SkillManagement.JobTitleSecurityProfiles", new[] { "SecurityProfileId" });
			DropIndex("SkillManagement.JobTitleSecurityProfiles", new[] { "JobTitleId" });
			DropIndex("TimeTracking.ContractTypeAbsenceTypes", new[] { "AbsenceTypeId" });
			DropIndex("TimeTracking.ContractTypeAbsenceTypes", new[] { "ContractTypeId" });
			DropIndex("TimeTracking.EmployeeCalendarAbsence", new[] { "CalendarAbsence_Id" });
			DropIndex("TimeTracking.EmployeeCalendarAbsence", new[] { "Employee_Id" });
			DropIndex("Accounts.UserSecurityRoles", new[] { "RoleId" });
			DropIndex("Accounts.UserSecurityRoles", new[] { "UserId" });
			DropIndex("Accounts.UserSecurityProfiles", new[] { "ProfileId" });
			DropIndex("Accounts.UserSecurityProfiles", new[] { "UserId" });
			DropIndex("Accounts.SecurityRoleSecurityProfiles", new[] { "ProfileId" });
			DropIndex("Accounts.SecurityRoleSecurityProfiles", new[] { "RoleId" });
			DropIndex("Accounts.SecurityAreaSecurityRoles", new[] { "RoleId" });
			DropIndex("Accounts.SecurityAreaSecurityRoles", new[] { "AreaId" });
			DropIndex("Allocation.ActiveDirectoryGroupsUsers", new[] { "ActiveDirectoryGroup_Id" });
			DropIndex("Allocation.ActiveDirectoryGroupsUsers", new[] { "User_Id" });
			DropIndex("Allocation.ActiveDirectoryGroupManagers", new[] { "User_Id" });
			DropIndex("Allocation.ActiveDirectoryGroupManagers", new[] { "ActiveDirectoryGroup_Id" });
			DropIndex("PeopleManagement.UserAcronym", new[] { "Acronym" });
			DropIndex("Runtime.TemporaryDocument", new[] { "UserIdModifiedBy" });
			DropIndex("Runtime.TemporaryDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("Runtime.TemporaryDocument", new[] { "Identifier" });
			DropIndex("Runtime.TemporaryDocumentPart", new[] { "TemporaryDocumentId" });
			DropIndex("Configuration.SystemParameter", new[] { "UserIdModifiedBy" });
			DropIndex("Configuration.SystemParameter", new[] { "UserIdImpersonatedBy" });
			DropIndex("Configuration.SystemParameter", new[] { "Key" });
			DropIndex("Survey.Survey", new[] { "UserIdModifiedBy" });
			DropIndex("Survey.Survey", new[] { "UserIdImpersonatedBy" });
			DropIndex("Survey.Survey", new[] { "RespondentId" });
			DropIndex("Survey.Survey", new[] { "SurveyConductId" });
			DropIndex("Survey.SurveyQuestion", new[] { "UserIdModifiedBy" });
			DropIndex("Survey.SurveyQuestion", new[] { "UserIdImpersonatedBy" });
			DropIndex("Survey.SurveyQuestion", "IX_SurveyDefinitionIdFieldName");
			DropIndex("Survey.SurveyDefinition", new[] { "UserIdModifiedBy" });
			DropIndex("Survey.SurveyDefinition", new[] { "UserIdImpersonatedBy" });
			DropIndex("Survey.SurveyConduct", new[] { "UserIdModifiedBy" });
			DropIndex("Survey.SurveyConduct", new[] { "UserIdImpersonatedBy" });
			DropIndex("Survey.SurveyConduct", new[] { "OpenedById" });
			DropIndex("Survey.SurveyConduct", new[] { "SurveyDefinitionId" });
			DropIndex("Survey.SurveyConductQuestion", new[] { "UserIdModifiedBy" });
			DropIndex("Survey.SurveyConductQuestion", new[] { "UserIdImpersonatedBy" });
			DropIndex("Survey.SurveyConductQuestion", new[] { "SurveyConductId" });
			DropIndex("Survey.SurveyAnswer", new[] { "UserIdModifiedBy" });
			DropIndex("Survey.SurveyAnswer", new[] { "UserIdImpersonatedBy" });
			DropIndex("Survey.SurveyAnswer", new[] { "QuestionId" });
			DropIndex("Survey.SurveyAnswer", new[] { "SurveyId" });
			DropIndex("Reporting.ReportTemplateContent", new[] { "Id" });
			DropIndex("Reporting.ReportTemplate", new[] { "UserIdModifiedBy" });
			DropIndex("Reporting.ReportTemplate", new[] { "UserIdImpersonatedBy" });
			DropIndex("Reporting.ReportTemplate", new[] { "ReportDefinitionId" });
			DropIndex("Reporting.ReportDefinition", new[] { "UserIdModifiedBy" });
			DropIndex("Reporting.ReportDefinition", new[] { "UserIdImpersonatedBy" });
			DropIndex("Reporting.ReportDefinition", new[] { "Code" });
			DropIndex("CustomerPortal.Questions", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.Questions", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.Questions", new[] { "UserId" });
			DropIndex("CustomerPortal.Questions", new[] { "RecommendationId" });
			DropIndex("TimeTracking.OvertimeApproval", new[] { "RequestId" });
			DropIndex("TimeTracking.OvertimeApproval", new[] { "ProjectId" });
			DropIndex("TimeTracking.OvertimeApproval", new[] { "EmployeeId" });
			DropIndex("Configuration.MessageTemplate", new[] { "UserIdModifiedBy" });
			DropIndex("Configuration.MessageTemplate", new[] { "UserIdImpersonatedBy" });
			DropIndex("Configuration.MessageTemplate", new[] { "Code" });
			DropIndex("Scheduling.Pipeline", new[] { "UserIdModifiedBy" });
			DropIndex("Scheduling.Pipeline", new[] { "UserIdImpersonatedBy" });
			DropIndex("Scheduling.Pipeline", new[] { "Name" });
			DropIndex("Runtime.JobRunInfo", new[] { "UserIdModifiedBy" });
			DropIndex("Runtime.JobRunInfo", new[] { "UserIdImpersonatedBy" });
			DropIndex("Runtime.JobRunInfo", new[] { "JobDefinitionId" });
			DropIndex("Scheduling.JobLog", new[] { "UserIdModifiedBy" });
			DropIndex("Scheduling.JobLog", new[] { "UserIdImpersonatedBy" });
			DropIndex("Scheduling.JobLog", new[] { "JobDefinitionId" });
			DropIndex("Scheduling.JobDefinition", new[] { "UserIdModifiedBy" });
			DropIndex("Scheduling.JobDefinition", new[] { "UserIdImpersonatedBy" });
			DropIndex("Scheduling.JobDefinition", new[] { "PipelineId" });
			DropIndex("Scheduling.JobDefinition", new[] { "Code" });
			DropIndex("Scheduling.JobDefinitionParameter", new[] { "UserIdModifiedBy" });
			DropIndex("Scheduling.JobDefinitionParameter", new[] { "UserIdImpersonatedBy" });
			DropIndex("Scheduling.JobDefinitionParameter", new[] { "JobDefinitionId" });
			DropIndex("Allocation.EmployeeComment", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.EmployeeComment", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.EmployeeComment", new[] { "EmployeeIdEmployee" });
			DropIndex("Notifications.Email", new[] { "UserIdImpersonatedCreatedBy" });
			DropIndex("Notifications.Email", new[] { "UserIdCreatedBy" });
			DropIndex("Notifications.Email", "EmailSmartSearchIndex");
			DropIndex("Notifications.EmailAttachment", new[] { "EmailId" });
			DropIndex("Accounts.DataFilter", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.DataFilter", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.DataFilter", new[] { "UserId" });
			DropIndex("Dictionaries.CustomerSize", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.CustomerSize", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.CustomerSize", new[] { "Name" });
			DropIndex("Consents.ConsentDecision", new[] { "UserIdModifiedBy" });
			DropIndex("Consents.ConsentDecision", new[] { "UserIdImpersonatedBy" });
			DropIndex("Consents.ConsentDecision", new[] { "UserIdUser" });
			DropIndex("CustomerPortal.ClientData", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.ClientData", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.ClientData", new[] { "ClientContactIdClientContact" });
			DropIndex("CustomerPortal.ClientData", new[] { "UserIdManagerUser" });
			DropIndex("CustomerPortal.ClientData", new[] { "UserIdClientUser" });
			DropIndex("CustomerPortal.ChallengeFile", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.ChallengeFile", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.ChallengeFile", new[] { "RecommendationIdRecommendation" });
			DropIndex("CustomerPortal.ChallengeFile", new[] { "Identifier" });
			DropIndex("BusinessTrips.BusinessTripMessage", new[] { "BusinessTripId" });
			DropIndex("BusinessTrips.BusinessTripMessage", new[] { "AuthorEmployeeId" });
			DropIndex("Finance.AggregatedProjectTransaction", new[] { "UserIdModifiedBy" });
			DropIndex("Finance.AggregatedProjectTransaction", new[] { "UserIdImpersonatedBy" });
			DropIndex("Finance.AggregatedProjectTransaction", new[] { "ProjectId" });
			DropIndex("CustomerPortal.ScheduleAppointment", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.ScheduleAppointment", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.ScheduleAppointment", new[] { "RecommendationIdRecommendation" });
			DropIndex("CustomerPortal.Note", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.Note", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.Note", new[] { "UserIdUser" });
			DropIndex("CustomerPortal.Note", new[] { "RecommendationId" });
			DropIndex("CustomerPortal.ResourceForm", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.ResourceForm", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.ResourceForm", new[] { "UserIdApprovedBy" });
			DropIndex("CustomerPortal.ResourceForm", new[] { "UserIdSubmittedBy" });
			DropIndex("CustomerPortal.ResourceForm", new[] { "UserIdCreatedBy" });
			DropIndex("CustomerPortal.NeededResources", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.NeededResources", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.NeededResources", new[] { "ResourceFormId" });
			DropIndex("CustomerPortal.DocumentContent", new[] { "Id" });
			DropIndex("CustomerPortal.Documents", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.Documents", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.Documents", new[] { "RecommendationId" });
			DropIndex("CustomerPortal.ChallengeDocumentContents", new[] { "Id" });
			DropIndex("CustomerPortal.ChallengeDocuments", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.ChallengeDocuments", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.ChallengeDocuments", new[] { "ChallengeId" });
			DropIndex("CustomerPortal.AnswerDocumentContents", new[] { "Id" });
			DropIndex("CustomerPortal.AnswerDocuments", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.AnswerDocuments", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.AnswerDocuments", new[] { "ChallengeId" });
			DropIndex("CustomerPortal.Challenges", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.Challenges", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.Challenges", new[] { "RecommendationIdRecommendation" });
			DropIndex("CustomerPortal.Recommendations", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.Recommendations", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.Recommendations", new[] { "NeededResourceId" });
			DropIndex("CustomerPortal.Recommendations", new[] { "ResourceId" });
			DropIndex("CustomerPortal.ActivityLog", new[] { "UserIdModifiedBy" });
			DropIndex("CustomerPortal.ActivityLog", new[] { "UserIdImpersonatedBy" });
			DropIndex("CustomerPortal.ActivityLog", new[] { "ResourceFormIdResourceForm" });
			DropIndex("CustomerPortal.ActivityLog", new[] { "RecommendationIdRecommendation" });
			DropIndex("Workflows.ActionSetTriggeringStatus", new[] { "ActionSetId" });
			DropIndex("Workflows.ActionSet", new[] { "Code" });
			DropIndex("Workflows.SkillChangeRequest", new[] { "EmployeeId" });
			DropIndex("Workflows.SkillChangeRequest", new[] { "Id" });
			DropIndex("Workflows.ReopenTimeTrackingReportRequest", new[] { "Id" });
			DropIndex("Workflows.ProjectTimeReportApprovalRequest", new[] { "ProjectId" });
			DropIndex("Workflows.ProjectTimeReportApprovalRequest", new[] { "EmployeeId" });
			DropIndex("Workflows.ProjectTimeReportApprovalRequest", new[] { "Id" });
			DropIndex("Workflows.OvertimeRequest", new[] { "ProjectId" });
			DropIndex("Workflows.OvertimeRequest", new[] { "Id" });
			DropIndex("Workflows.RequestHistoryEntry", new[] { "RequestId" });
			DropIndex("Workflows.RequestHistoryEntry", new[] { "ApprovingUserId" });
			DropIndex("Workflows.RequestHistoryEntry", new[] { "ExecutingUserId" });
			DropIndex("Workflows.FeedbackRequest", new[] { "EvaluatorEmployeeId" });
			DropIndex("Workflows.FeedbackRequest", new[] { "EvaluatedEmployeeId" });
			DropIndex("Workflows.FeedbackRequest", new[] { "Id" });
			DropIndex("Workflows.EmploymentTerminationRequest", new[] { "ReasonId" });
			DropIndex("Workflows.EmploymentTerminationRequest", new[] { "EmployeeId" });
			DropIndex("Workflows.EmploymentTerminationRequest", new[] { "Id" });
			DropIndex("Workflows.RequestDocumentContent", new[] { "Id" });
			DropIndex("Workflows.RequestDocument", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.RequestDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.RequestDocument", new[] { "RequestId" });
			DropIndex("Workflows.EmployeePhotoDocumentContent", new[] { "Id" });
			DropIndex("Workflows.EmployeePhotoDocument", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.EmployeePhotoDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.EmployeePhotoDocument", new[] { "OnboardingRequestId" });
			DropIndex("Workflows.PayrollDocumentContent", new[] { "Id" });
			DropIndex("Workflows.PayrollDocument", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.PayrollDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.PayrollDocument", new[] { "OnboardingRequestId" });
			DropIndex("Workflows.ForeignEmployeeDocumentContent", new[] { "Id" });
			DropIndex("Workflows.ForeignEmployeeDocument", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.ForeignEmployeeDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.ForeignEmployeeDocument", new[] { "OnboardingRequestId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "OnboardingLocationId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "OrgUnitId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "ProbationPeriodContractTypeId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "ContractTypeId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "JobProfileId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "JobTitleId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "CompanyId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "LocationId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "RecruiterEmployeeId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "LineManagerEmployeeId" });
			DropIndex("Workflows.OnboardingRequest", new[] { "Id" });
			DropIndex("PeopleManagement.AssetType", new[] { "NamePl" });
			DropIndex("PeopleManagement.AssetType", new[] { "NameEn" });
			DropIndex("Workflows.AssetsRequest", new[] { "EmployeeId" });
			DropIndex("Workflows.AssetsRequest", new[] { "ProjectId" });
			DropIndex("Workflows.AssetsRequest", new[] { "OnboardingRequestId" });
			DropIndex("Workflows.AssetsRequest", new[] { "Id" });
			DropIndex("Workflows.Approvals", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.Approvals", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.Approvals", new[] { "ForcedByUserId" });
			DropIndex("Workflows.Approvals", new[] { "ApprovalGroupId" });
			DropIndex("Workflows.Approvals", new[] { "UserId" });
			DropIndex("Workflows.ApprovalGroup", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.ApprovalGroup", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.ApprovalGroup", new[] { "RequestId" });
			DropIndex("Workflows.AdvancedPaymentRequest", new[] { "CurrencyId" });
			DropIndex("Workflows.AdvancedPaymentRequest", new[] { "BusinessTripId" });
			DropIndex("Workflows.AdvancedPaymentRequest", new[] { "Id" });
			DropIndex("Workflows.AddHomeOfficeRequest", new[] { "Id" });
			DropIndex("Dictionaries.UtilizationCategory", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.UtilizationCategory", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.UtilizationCategory", new[] { "NavisionCode" });
			DropIndex("Allocation.ProjectTag", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.ProjectTag", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.ProjectTag", new[] { "Name" });
			DropIndex("Dictionaries.SoftwareCategory", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.SoftwareCategory", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.SoftwareCategory", new[] { "Name" });
			DropIndex("Dictionaries.ServiceLine", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.ServiceLine", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.ServiceLine", new[] { "Name" });
			DropIndex("Allocation.ProjectPictureContent", new[] { "Id" });
			DropIndex("Allocation.ProjectPicture", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.ProjectPicture", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.JiraIssueToTimeReportRowMapper", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.JiraIssueToTimeReportRowMapper", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.JiraIssueToTimeReportRowMapper", new[] { "Name" });
			DropIndex("Allocation.ProjectAttachmentContent", new[] { "Id" });
			DropIndex("Allocation.ProjectAttachment", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.ProjectAttachment", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.ProjectAttachment", new[] { "ProjectId" });
			DropIndex("Allocation.WeeklyAllocationDetail", new[] { "ProjectId" });
			DropIndex("Allocation.WeeklyAllocationDetail", new[] { "WeeklyAllocationStatusId" });
			DropIndex("Allocation.WeeklyAllocationStatus", new[] { "EmployeeId" });
			DropIndex("TimeTracking.VacationBalance", new[] { "RequestId" });
			DropIndex("TimeTracking.VacationBalance", new[] { "EmployeeId" });
			DropIndex("TimeTracking.TimeTrackingImportSource", new[] { "Code" });
			DropIndex("TimeTracking.TimeReportDailyEntry", new[] { "TimeReportRowId" });
			DropIndex("TimeTracking.TimeReportProjectAttribute", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.TimeReportProjectAttribute", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.TimeReportProjectAttribute", new[] { "DefaultValueId" });
			DropIndex("TimeTracking.TimeReportProjectAttributeValue", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.TimeReportProjectAttributeValue", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.TimeReportProjectAttributeValue", new[] { "AttributeId" });
			DropIndex("TimeTracking.TimeReportRow", new[] { "AbsenceTypeId" });
			DropIndex("TimeTracking.TimeReportRow", new[] { "RequestId" });
			DropIndex("TimeTracking.TimeReportRow", new[] { "ImportSourceId" });
			DropIndex("TimeTracking.TimeReportRow", new[] { "ProjectId" });
			DropIndex("TimeTracking.TimeReportRow", new[] { "TimeReportId" });
			DropIndex("TimeTracking.ClockCardDailyEntry", new[] { "TimeReportId" });
			DropIndex("TimeTracking.TimeReport", "IX_EmployeeYearMonth");
			DropIndex("TimeTracking.OvertimeBank", new[] { "TimeReportId" });
			DropIndex("TimeTracking.OvertimeBank", new[] { "TimeReportRowId" });
			DropIndex("TimeTracking.OvertimeBank", new[] { "RequestId" });
			DropIndex("TimeTracking.OvertimeBank", new[] { "EmployeeId" });
			DropIndex("MeetMe.MeetingNoteAttachmentContent", new[] { "Id" });
			DropIndex("MeetMe.MeetingNoteAttachment", new[] { "MeetingNote_Id" });
			DropIndex("MeetMe.MeetingNoteAttachment", new[] { "UserIdModifiedBy" });
			DropIndex("MeetMe.MeetingNoteAttachment", new[] { "UserIdImpersonatedBy" });
			DropIndex("MeetMe.MeetingNote", new[] { "UserIdModifiedBy" });
			DropIndex("MeetMe.MeetingNote", new[] { "UserIdImpersonatedBy" });
			DropIndex("MeetMe.MeetingNote", new[] { "EmployeeIdAuthor" });
			DropIndex("MeetMe.MeetingNote", new[] { "MeetingId" });
			DropIndex("MeetMe.MeetingAttachmentContent", new[] { "Id" });
			DropIndex("MeetMe.MeetingAttachment", new[] { "Meeting_Id" });
			DropIndex("MeetMe.MeetingAttachment", new[] { "UserIdModifiedBy" });
			DropIndex("MeetMe.MeetingAttachment", new[] { "UserIdImpersonatedBy" });
			DropIndex("MeetMe.Meeting", new[] { "UserIdModifiedBy" });
			DropIndex("MeetMe.Meeting", new[] { "UserIdImpersonatedBy" });
			DropIndex("MeetMe.Meeting", new[] { "LocationIdLocation" });
			DropIndex("MeetMe.Meeting", new[] { "EmployeeIdLineManager" });
			DropIndex("MeetMe.Meeting", new[] { "EmployeeIdEmployee" });
			DropIndex("MeetMe.Feedback", new[] { "UserIdModifiedBy" });
			DropIndex("MeetMe.Feedback", new[] { "UserIdImpersonatedBy" });
			DropIndex("MeetMe.Feedback", new[] { "EmployeeIdAuthor" });
			DropIndex("TimeTracking.EmployeeAbsence", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.EmployeeAbsence", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.EmployeeAbsence", new[] { "RequestId" });
			DropIndex("TimeTracking.EmployeeAbsence", new[] { "AbsenceTypeId" });
			DropIndex("TimeTracking.EmployeeAbsence", new[] { "EmployeeId" });
			DropIndex("Dictionaries.Region", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.Region", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.Region", new[] { "Name" });
			DropIndex("Organization.OrgUnit", new[] { "UserIdModifiedBy" });
			DropIndex("Organization.OrgUnit", new[] { "UserIdImpersonatedBy" });
			DropIndex("Organization.OrgUnit", new[] { "AbsenceOnlyDefaultProjectId" });
			DropIndex("Organization.OrgUnit", new[] { "RegionId" });
			DropIndex("Organization.OrgUnit", new[] { "EmployeeId" });
			DropIndex("Organization.OrgUnit", new[] { "CalendarId" });
			DropIndex("Organization.OrgUnit", new[] { "ParentId" });
			DropIndex("Organization.OrgUnit", new[] { "NameSortOrder" });
			DropIndex("Organization.OrgUnit", new[] { "FlatName" });
			DropIndex("Organization.OrgUnit", new[] { "Path" });
			DropIndex("Organization.OrgUnit", new[] { "Code" });
			DropIndex("Workflows.BusinessTripRequest", new[] { "DestinationCityId" });
			DropIndex("Workflows.BusinessTripRequest", new[] { "DepartureCityId" });
			DropIndex("Workflows.BusinessTripRequest", new[] { "Id" });
			DropIndex("Workflows.DeclaredLuggage", new[] { "ParticipantId" });
			DropIndex("Workflows.DeclaredLuggage", new[] { "RequestId" });
			DropIndex("Workflows.DeclaredLuggage", new[] { "EmployeeId" });
			DropIndex("BusinessTrips.ArrangementTrackingEntry", "IX_ArrangementTypePerParticipant");
			DropIndex("BusinessTrips.Currency", new[] { "UserIdModifiedBy" });
			DropIndex("BusinessTrips.Currency", new[] { "UserIdImpersonatedBy" });
			DropIndex("BusinessTrips.ArrangementDocumentContent", new[] { "Id" });
			DropIndex("BusinessTrips.ArrangementDocument", new[] { "UserIdModifiedBy" });
			DropIndex("BusinessTrips.ArrangementDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("BusinessTrips.ArrangementDocument", new[] { "ArrangementId" });
			DropIndex("BusinessTrips.Arrangement", new[] { "CurrencyId" });
			DropIndex("BusinessTrips.Arrangement", new[] { "BusinessTripId" });
			DropIndex("BusinessTrips.BusinessTripParticipant", new[] { "BusinessTripId" });
			DropIndex("BusinessTrips.BusinessTripParticipant", new[] { "EmployeeId" });
			DropIndex("BusinessTrips.BusinessTrip", new[] { "DestinationCityId" });
			DropIndex("BusinessTrips.BusinessTrip", new[] { "DepartureCityId" });
			DropIndex("BusinessTrips.BusinessTrip", new[] { "RequestId" });
			DropIndex("Dictionaries.Country", new[] { "Name" });
			DropIndex("Dictionaries.City", new[] { "CountryId" });
			DropIndex("Dictionaries.City", new[] { "Name" });
			DropIndex("Dictionaries.Location", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.Location", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.Location", new[] { "CityId" });
			DropIndex("Dictionaries.Location", new[] { "Name" });
			DropIndex("Workflows.EmployeeDataChangeRequest", new[] { "LocationId" });
			DropIndex("Workflows.EmployeeDataChangeRequest", new[] { "JobProfileId" });
			DropIndex("Workflows.EmployeeDataChangeRequest", new[] { "OrgUnitId" });
			DropIndex("Workflows.EmployeeDataChangeRequest", new[] { "LineManagerId" });
			DropIndex("Workflows.EmployeeDataChangeRequest", new[] { "Id" });
			DropIndex("Organization.TechnicalInterviewer", new[] { "UserIdModifiedBy" });
			DropIndex("Organization.TechnicalInterviewer", new[] { "UserIdImpersonatedBy" });
			DropIndex("Organization.TechnicalInterviewer", new[] { "EmployeeIdEmployee" });
			DropIndex("Allocation.StaffingDemand", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.StaffingDemand", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.StaffingDemand", new[] { "ProjectId" });
			DropIndex("Allocation.StaffingDemand", new[] { "JobProfileId" });
			DropIndex("Allocation.StaffingDemand", new[] { "JobMatrixLevelId" });
			DropIndex("Dictionaries.Industry", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.Industry", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.Industry", new[] { "NameEn" });
			DropIndex("Resumes.ResumeTraining", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeTraining", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeTraining", new[] { "ResumeId" });
			DropIndex("Resumes.ResumeTechnicalSkill", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeTechnicalSkill", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeTechnicalSkill", "IX_ResumeIdTechnicalSkillId");
			DropIndex("Dictionaries.Language", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.Language", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.Language", new[] { "NameEn" });
			DropIndex("Resumes.ResumeLanguage", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeLanguage", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeLanguage", new[] { "LanguageId" });
			DropIndex("Resumes.ResumeLanguage", new[] { "ResumeId" });
			DropIndex("Resumes.ResumeEducation", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeEducation", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeEducation", new[] { "ResumeId" });
			DropIndex("Resumes.ResumeAdditionalSkill", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeAdditionalSkill", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeAdditionalSkill", new[] { "ResumeId" });
			DropIndex("Resumes.Resume", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.Resume", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.Resume", new[] { "EmployeeId" });
			DropIndex("Resumes.ResumeCompany", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeCompany", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeCompany", new[] { "ResumeId" });
			DropIndex("Resumes.ResumeProject", new[] { "UserIdModifiedBy" });
			DropIndex("Resumes.ResumeProject", new[] { "UserIdImpersonatedBy" });
			DropIndex("Resumes.ResumeProject", new[] { "ProjectId" });
			DropIndex("Resumes.ResumeProject", new[] { "CompanyId" });
			DropIndex("SkillManagement.Skill", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.Skill", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.Skill", new[] { "Name" });
			DropIndex("Organization.PricingEngineer", new[] { "UserIdModifiedBy" });
			DropIndex("Organization.PricingEngineer", new[] { "UserIdImpersonatedBy" });
			DropIndex("Organization.PricingEngineer", new[] { "EmployeeIdEmployee" });
			DropIndex("SkillManagement.SkillTag", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.SkillTag", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.SkillTag", new[] { "NameEn" });
			DropIndex("SkillManagement.JobProfileSkillTag", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.JobProfileSkillTag", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.JobProfileSkillTag", new[] { "SkillTagId" });
			DropIndex("SkillManagement.JobProfileSkillTag", new[] { "JobProfileId" });
			DropIndex("SkillManagement.JobProfile", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.JobProfile", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.JobProfile", new[] { "CustomOrder" });
			DropIndex("SkillManagement.JobProfile", new[] { "Name" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "ProjectIdAbsenceOnlyDefaultProject" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "CalendarId" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "ContractTypeId" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "TerminationReasonId" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "JobTitleId" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "CompanyId" });
			DropIndex("Allocation.EmploymentPeriod", new[] { "EmployeeIdEmployee" });
			DropIndex("SkillManagement.JobMatrix", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.JobMatrix", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.JobMatrix", new[] { "Code" });
			DropIndex("SkillManagement.JobMatrixLevel", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.JobMatrixLevel", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.JobMatrixLevel", new[] { "JobMatrixId" });
			DropIndex("SkillManagement.JobMatrixLevel", new[] { "Code" });
			DropIndex("SkillManagement.JobTitle", new[] { "UserIdModifiedBy" });
			DropIndex("SkillManagement.JobTitle", new[] { "UserIdImpersonatedBy" });
			DropIndex("SkillManagement.JobTitle", new[] { "JobMatrixLevelId" });
			DropIndex("Workflows.EmploymentDataChangeRequest", new[] { "JobTitleId" });
			DropIndex("Workflows.EmploymentDataChangeRequest", new[] { "CompanyId" });
			DropIndex("Workflows.EmploymentDataChangeRequest", new[] { "ContractTypeId" });
			DropIndex("Workflows.EmploymentDataChangeRequest", new[] { "EmployeeId" });
			DropIndex("Workflows.EmploymentDataChangeRequest", new[] { "Id" });
			DropIndex("TimeTracking.ContractType", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.ContractType", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.ContractType", new[] { "ActiveDirectoryCode" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "CalendarId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "AbsenceOnlyDefaultProjectId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "ContractTypeId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "LineManagerId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "CompanyId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "LocationId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "OrgUnitId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "JobProfileId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "JobTitleId" });
			DropIndex("Workflows.AddEmployeeRequest", new[] { "Id" });
			DropIndex("Dictionaries.Company", new[] { "UserIdModifiedBy" });
			DropIndex("Dictionaries.Company", new[] { "UserIdImpersonatedBy" });
			DropIndex("Dictionaries.Company", new[] { "CustomOrder" });
			DropIndex("Dictionaries.Company", new[] { "ActiveDirectoryCode" });
			DropIndex("Dictionaries.Company", new[] { "Name" });
			DropIndex("TimeTracking.CalendarAbsence", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.CalendarAbsence", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.CalendarAbsence", new[] { "UserIdImpersonatedCreatedBy" });
			DropIndex("TimeTracking.CalendarAbsence", new[] { "UserIdCreatedBy" });
			DropIndex("TimeTracking.CalendarAbsence", new[] { "OfficeGroupEmail" });
			DropIndex("TimeTracking.CalendarAbsence", new[] { "Name" });
			DropIndex("Workflows.Substitution", new[] { "RequestId" });
			DropIndex("Workflows.Substitution", new[] { "SubstituteUserId" });
			DropIndex("Workflows.Substitution", new[] { "ReplacedUserId" });
			DropIndex("Accounts.SecurityArea", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.SecurityArea", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.SecurityArea", new[] { "Code" });
			DropIndex("Accounts.SecurityProfile", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.SecurityProfile", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.UserPictureContent", new[] { "Id" });
			DropIndex("Accounts.UserPicture", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.UserPicture", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.UserLoginToken", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.UserLoginToken", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.UserDocumentContent", new[] { "Id" });
			DropIndex("Accounts.UserDocument", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.UserDocument", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.UserDocument", new[] { "UserId" });
			DropIndex("Accounts.Credential", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.Credential", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.Credential", new[] { "UserId" });
			DropIndex("Allocation.ActiveDirectoryGroup", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.ActiveDirectoryGroup", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.ActiveDirectoryGroup", new[] { "ActiveDirectoryId" });
			DropIndex("Allocation.ActiveDirectoryGroup", new[] { "OwnerId" });
			DropIndex("Accounts.User", new[] { "ActiveDirectoryId" });
			DropIndex("Accounts.User", new[] { "PictureId" });
			DropIndex("Accounts.User", new[] { "SecurityAreaId" });
			DropIndex("Accounts.User", new[] { "UserLoginTokenIdLoginToken" });
			DropIndex("Accounts.User", new[] { "UserIdModifiedBy" });
			DropIndex("Accounts.User", new[] { "UserIdImpersonatedBy" });
			DropIndex("Accounts.User", new[] { "Email" });
			DropIndex("Accounts.User", new[] { "Login" });
			DropIndex("Configuration.CalendarEntry", new[] { "UserIdModifiedBy" });
			DropIndex("Configuration.CalendarEntry", new[] { "UserIdImpersonatedBy" });
			DropIndex("Configuration.CalendarEntry", new[] { "CalendarId" });
			DropIndex("Configuration.Calendar", new[] { "UserIdModifiedBy" });
			DropIndex("Configuration.Calendar", new[] { "UserIdImpersonatedBy" });
			DropIndex("Configuration.Calendar", new[] { "Code" });
			DropIndex("Allocation.AllocationDailyRecord", new[] { "CalendarId" });
			DropIndex("Allocation.AllocationDailyRecord", new[] { "AllocationRequestId" });
			DropIndex("Allocation.AllocationDailyRecord", new[] { "ProjectId" });
			DropIndex("Allocation.AllocationDailyRecord", new[] { "EmployeeId" });
			DropIndex("Allocation.Employee", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.Employee", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.Employee", new[] { "OrgUnitId" });
			DropIndex("Allocation.Employee", new[] { "LineManagerId" });
			DropIndex("Allocation.Employee", new[] { "UserId" });
			DropIndex("Allocation.Employee", new[] { "CompanyId" });
			DropIndex("Allocation.Employee", new[] { "JobTitleId" });
			DropIndex("Allocation.Employee", new[] { "LocationId" });
			DropIndex("Allocation.Employee", new[] { "Email" });
			DropIndex("Allocation.AllocationRequestMetadata", new[] { "AllocationRequestId" });
			DropIndex("Allocation.AllocationRequest", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.AllocationRequest", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.AllocationRequest", new[] { "ProjectId" });
			DropIndex("Allocation.AllocationRequest", new[] { "EmployeeId" });
			DropIndex("Allocation.Project", new[] { "UserIdModifiedBy" });
			DropIndex("Allocation.Project", new[] { "UserIdImpersonatedBy" });
			DropIndex("Allocation.Project", new[] { "UtilizationCategoryId" });
			DropIndex("Allocation.Project", new[] { "JiraIssueToTimeReportRowMapperId" });
			DropIndex("Allocation.Project", new[] { "TimeTrackingImportSourceId" });
			DropIndex("Allocation.Project", new[] { "PictureId" });
			DropIndex("Allocation.Project", new[] { "CalendarId" });
			DropIndex("Allocation.Project", new[] { "SupervisorManagerId" });
			DropIndex("Allocation.Project", new[] { "SalesAccountManagerId" });
			DropIndex("Allocation.Project", new[] { "ProjectManagerId" });
			DropIndex("Allocation.Project", new[] { "OrgUnitId" });
			DropIndex("Allocation.Project", "IX_ProjectNamePerClient");
			DropIndex("TimeTracking.AbsenceType", new[] { "UserIdModifiedBy" });
			DropIndex("TimeTracking.AbsenceType", new[] { "UserIdImpersonatedBy" });
			DropIndex("TimeTracking.AbsenceType", new[] { "AbsenceProjectId" });
			DropIndex("TimeTracking.AbsenceType", new[] { "Code" });
			DropIndex("Workflows.AbsenceRequest", new[] { "SubstituteUserId" });
			DropIndex("Workflows.AbsenceRequest", new[] { "AbsenceTypeId" });
			DropIndex("Workflows.AbsenceRequest", new[] { "AffectedUserId" });
			DropIndex("Workflows.AbsenceRequest", new[] { "Id" });
			DropIndex("Workflows.Request", new[] { "UserIdModifiedBy" });
			DropIndex("Workflows.Request", new[] { "UserIdImpersonatedBy" });
			DropIndex("Workflows.Request", new[] { "RequestingUserId" });
			DropIndex("TimeTracking.AbsenceCalendarEntry", new[] { "RequestId" });
			DropIndex("TimeTracking.AbsenceCalendarEntry", new[] { "ExchangeId" });
			DropTable("Survey.SurveyConductOrgUnits");
			DropTable("Survey.SurveyConductEmployees");
			DropTable("Survey.SurveyConductActiveDirectoryGroups");
			DropTable("dbo.LanguageOptionResourceForms");
			DropTable("Workflows.RequestWatchers");
			DropTable("Workflows.SkillChangeRequestSkillMap");
			DropTable("Workflows.ReopenTimeTrackingReportRequestProjectsMap");
			DropTable("Workflows.AssetsRequestAssetType");
			DropTable("TimeTracking.ProjectAcceptingPersons");
			DropTable("Allocation.ProjectTechnologies");
			DropTable("Allocation.ProjectTagProject");
			DropTable("Allocation.ProjectSoftwareCategories");
			DropTable("Allocation.ProjectServiceLines");
			DropTable("Allocation.ProjectKeyTechnologies");
			DropTable("Allocation.IndustryProject");
			DropTable("TimeTracking.ProjectAttributesMap");
			DropTable("TimeTracking.TimeReportRowAttributeValuesMap");
			DropTable("Allocation.EmployeeJobProfile");
			DropTable("Allocation.EmployeeJobMatrixLevel");
			DropTable("BusinessTrips.BusinessTripProjectsMap");
			DropTable("Workflows.BusinessTripRequestProjectsMap");
			DropTable("Workflows.BusinessTripRequestEmployeesMap");
			DropTable("BusinessTrips.ArrangementParticipants");
			DropTable("Workflows.EmployeeDataChangeRequestEmployeesMap");
			DropTable("SkillManagement.SkillTagTechnicalInterviewer");
			DropTable("SkillManagement.SkillTagSkill");
			DropTable("Allocation.SkillsStaffingDemand");
			DropTable("Resumes.ResumeProjectTechnicalSkill");
			DropTable("Resumes.ResumeProjectIndustry");
			DropTable("Resumes.ResumeRejectedSkillSuggestion");
			DropTable("SkillManagement.SkillTagPricingEngineer");
			DropTable("SkillManagement.JobTitleSecurityProfiles");
			DropTable("TimeTracking.ContractTypeAbsenceTypes");
			DropTable("TimeTracking.EmployeeCalendarAbsence");
			DropTable("Accounts.UserSecurityRoles");
			DropTable("Accounts.UserSecurityProfiles");
			DropTable("Accounts.SecurityRoleSecurityProfiles");
			DropTable("Accounts.SecurityAreaSecurityRoles");
			DropTable("Allocation.ActiveDirectoryGroupsUsers");
			DropTable("Allocation.ActiveDirectoryGroupManagers");
			DropTable("PeopleManagement.UserAcronym");
			DropTable("Runtime.TemporaryDocument");
			DropTable("Runtime.TemporaryDocumentPart");
			DropTable("Configuration.SystemParameter");
			DropTable("Survey.Survey");
			DropTable("Survey.SurveyQuestion");
			DropTable("Survey.SurveyDefinition");
			DropTable("Survey.SurveyConduct");
			DropTable("Survey.SurveyConductQuestion");
			DropTable("Survey.SurveyAnswer");
			DropTable("Reporting.ReportTemplateContent");
			DropTable("Reporting.ReportTemplate");
			DropTable("Reporting.ReportDefinition");
			DropTable("CustomerPortal.RegionOptions");
			DropTable("CustomerPortal.Questions");
			DropTable("Runtime.PublishedEventQueue");
			DropTable("Runtime.ProcessedEventQueue");
			DropTable("TimeTracking.OvertimeApproval");
			DropTable("Configuration.MessageTemplate");
			DropTable("CustomerPortal.MainTechnologyOptions");
			DropTable("Runtime.atomic_LastJobRunInfoView");
			DropTable("Scheduling.Pipeline");
			DropTable("Runtime.JobRunInfo");
			DropTable("Scheduling.JobLog");
			DropTable("Scheduling.JobDefinition");
			DropTable("Scheduling.JobDefinitionParameter");
			DropTable("Scheduling.JobInternalParameter");
			DropTable("Allocation.EmployeeComment");
			DropTable("Notifications.Email");
			DropTable("Notifications.EmailAttachment");
			DropTable("Accounts.DataFilter");
			DropTable("Dictionaries.CustomerSize");
			DropTable("Configuration.atomic_CurrentDateTimeView");
			DropTable("Consents.ConsentDecision");
			DropTable("CustomerPortal.ClientData");
			DropTable("CustomerPortal.ClientContact");
			DropTable("CustomerPortal.ChallengeFile");
			DropTable("Crm.Candidate");
			DropTable("Runtime.CacheInvalidation");
			DropTable("CustomerPortal.BusinesUnitOptions");
			DropTable("BusinessTrips.BusinessTripMessage");
			DropTable("Audit.atomic_AuditTableView");
			DropTable("Finance.AggregatedProjectTransaction");
			DropTable("CustomerPortal.ScheduleAppointment");
			DropTable("CustomerPortal.Note");
			DropTable("CustomerPortal.LanguageOption");
			DropTable("CustomerPortal.ResourceForm");
			DropTable("CustomerPortal.NeededResources");
			DropTable("CustomerPortal.DocumentContent");
			DropTable("CustomerPortal.Documents");
			DropTable("CustomerPortal.ChallengeDocumentContents");
			DropTable("CustomerPortal.ChallengeDocuments");
			DropTable("CustomerPortal.AnswerDocumentContents");
			DropTable("CustomerPortal.AnswerDocuments");
			DropTable("CustomerPortal.Challenges");
			DropTable("CustomerPortal.Recommendations");
			DropTable("CustomerPortal.ActivityLog");
			DropTable("Workflows.ActionSetTriggeringStatus");
			DropTable("Workflows.ActionSet");
			DropTable("Workflows.SkillChangeRequest");
			DropTable("Workflows.ReopenTimeTrackingReportRequest");
			DropTable("Workflows.ProjectTimeReportApprovalRequest");
			DropTable("Workflows.OvertimeRequest");
			DropTable("Workflows.RequestHistoryEntry");
			DropTable("Workflows.FeedbackRequest");
			DropTable("Workflows.EmploymentTerminationRequest");
			DropTable("Workflows.RequestDocumentContent");
			DropTable("Workflows.RequestDocument");
			DropTable("Workflows.EmployeePhotoDocumentContent");
			DropTable("Workflows.EmployeePhotoDocument");
			DropTable("Workflows.PayrollDocumentContent");
			DropTable("Workflows.PayrollDocument");
			DropTable("Workflows.ForeignEmployeeDocumentContent");
			DropTable("Workflows.ForeignEmployeeDocument");
			DropTable("Workflows.OnboardingRequest");
			DropTable("PeopleManagement.AssetType");
			DropTable("Workflows.AssetsRequest");
			DropTable("Workflows.Approvals");
			DropTable("Workflows.ApprovalGroup");
			DropTable("Workflows.AdvancedPaymentRequest");
			DropTable("Workflows.AddHomeOfficeRequest");
			DropTable("Dictionaries.UtilizationCategory");
			DropTable("Allocation.ProjectTag");
			DropTable("Dictionaries.SoftwareCategory");
			DropTable("Dictionaries.ServiceLine");
			DropTable("Allocation.ProjectPictureContent");
			DropTable("Allocation.ProjectPicture");
			DropTable("TimeTracking.JiraIssueToTimeReportRowMapper");
			DropTable("Allocation.ProjectAttachmentContent");
			DropTable("Allocation.ProjectAttachment");
			DropTable("Allocation.WeeklyAllocationDetail");
			DropTable("Allocation.WeeklyAllocationStatus");
			DropTable("TimeTracking.VacationBalance");
			DropTable("TimeTracking.TimeTrackingImportSource");
			DropTable("TimeTracking.TimeReportDailyEntry");
			DropTable("TimeTracking.TimeReportProjectAttribute");
			DropTable("TimeTracking.TimeReportProjectAttributeValue");
			DropTable("TimeTracking.TimeReportRow");
			DropTable("TimeTracking.ClockCardDailyEntry");
			DropTable("TimeTracking.TimeReport");
			DropTable("TimeTracking.OvertimeBank");
			DropTable("MeetMe.MeetingNoteAttachmentContent");
			DropTable("MeetMe.MeetingNoteAttachment");
			DropTable("MeetMe.MeetingNote");
			DropTable("MeetMe.MeetingAttachmentContent");
			DropTable("MeetMe.MeetingAttachment");
			DropTable("MeetMe.Meeting");
			DropTable("MeetMe.Feedback");
			DropTable("TimeTracking.EmployeeAbsence");
			DropTable("Dictionaries.Region");
			DropTable("Organization.OrgUnit");
			DropTable("Workflows.BusinessTripRequest");
			DropTable("Workflows.DeclaredLuggage");
			DropTable("BusinessTrips.ArrangementTrackingEntry");
			DropTable("BusinessTrips.Currency");
			DropTable("BusinessTrips.ArrangementDocumentContent");
			DropTable("BusinessTrips.ArrangementDocument");
			DropTable("BusinessTrips.Arrangement");
			DropTable("BusinessTrips.BusinessTripParticipant");
			DropTable("BusinessTrips.BusinessTrip");
			DropTable("Dictionaries.Country");
			DropTable("Dictionaries.City");
			DropTable("Dictionaries.Location");
			DropTable("Workflows.EmployeeDataChangeRequest");
			DropTable("Organization.TechnicalInterviewer");
			DropTable("Allocation.StaffingDemand");
			DropTable("Dictionaries.Industry");
			DropTable("Resumes.ResumeTraining");
			DropTable("Resumes.ResumeTechnicalSkill");
			DropTable("Dictionaries.Language");
			DropTable("Resumes.ResumeLanguage");
			DropTable("Resumes.ResumeEducation");
			DropTable("Resumes.ResumeAdditionalSkill");
			DropTable("Resumes.Resume");
			DropTable("Resumes.ResumeCompany");
			DropTable("Resumes.ResumeProject");
			DropTable("SkillManagement.Skill");
			DropTable("Organization.PricingEngineer");
			DropTable("SkillManagement.SkillTag");
			DropTable("SkillManagement.JobProfileSkillTag");
			DropTable("SkillManagement.JobProfile");
			DropTable("PeopleManagement.EmploymentTerminationReason");
			DropTable("Allocation.EmploymentPeriod");
			DropTable("SkillManagement.JobMatrix");
			DropTable("SkillManagement.JobMatrixLevel");
			DropTable("SkillManagement.JobTitle");
			DropTable("Workflows.EmploymentDataChangeRequest");
			DropTable("TimeTracking.ContractType");
			DropTable("Workflows.AddEmployeeRequest");
			DropTable("Dictionaries.Company");
			DropTable("TimeTracking.CalendarAbsence");
			DropTable("Workflows.Substitution");
			DropTable("Accounts.SecurityArea");
			DropTable("Accounts.SecurityRole");
			DropTable("Accounts.SecurityProfile");
			DropTable("Accounts.UserPictureContent");
			DropTable("Accounts.UserPicture");
			DropTable("Accounts.UserLoginToken");
			DropTable("Accounts.UserDocumentContent");
			DropTable("Accounts.UserDocument");
			DropTable("Accounts.Credential");
			DropTable("Allocation.ActiveDirectoryGroup");
			DropTable("Accounts.User");
			DropTable("Configuration.CalendarEntry");
			DropTable("Configuration.Calendar");
			DropTable("Allocation.AllocationDailyRecord");
			DropTable("Allocation.Employee");
			DropTable("Allocation.AllocationRequestMetadata");
			DropTable("Allocation.AllocationRequest");
			DropTable("Allocation.Project");
			DropTable("TimeTracking.AbsenceType");
			DropTable("Workflows.AbsenceRequest");
			DropTable("Workflows.Request");
			DropTable("TimeTracking.AbsenceCalendarEntry");
		}
	}
}
