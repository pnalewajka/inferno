namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustAgencyModeForJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.JobOpening", "AgencyEmploymentMode", c => c.Int());
            DropColumn("Recruitment.JobOpening", "IsAgencyEmploymentAllowed");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.JobOpening", "IsAgencyEmploymentAllowed", c => c.Boolean(nullable: false));
            AlterColumn("Recruitment.JobOpening", "AgencyEmploymentMode", c => c.Int(nullable: false));
        }
    }
}
