namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration280320181352 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("Allocation.EmploymentPeriod", "SalaryGross", c => c.Decimal(precision: 18, scale: 2));
            //AddColumn("Allocation.EmploymentPeriod", "AuthorRemunerationRatio", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            //DropColumn("Allocation.EmploymentPeriod", "AuthorRemunerationRatio");
            //DropColumn("Allocation.EmploymentPeriod", "SalaryGross");
        }
    }
}
