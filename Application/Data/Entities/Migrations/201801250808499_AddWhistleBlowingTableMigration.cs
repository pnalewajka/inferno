namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWhistleBlowingTableMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Survey.WhistleBlowingDocumentContent",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Survey.WhistleBlowingDocument", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "Survey.WhistleBlowingDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        WhistleBlowingId = c.Long(nullable: false),
                        Name = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 250, unicode: false),
                        ContentLength = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Survey.WhistleBlowing", t => t.WhistleBlowingId)
                .Index(t => t.WhistleBlowingId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Survey.WhistleBlowing",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 255),
                        Message = c.String(nullable: false, maxLength: 2048),
                        ShowUserName = c.Boolean(nullable: false),
                        MessageStatus = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Survey.WhistleBlowingDocumentContent", "Id", "Survey.WhistleBlowingDocument");
            DropForeignKey("Survey.WhistleBlowing", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Survey.WhistleBlowing", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Survey.WhistleBlowingDocument", "WhistleBlowingId", "Survey.WhistleBlowing");
            DropForeignKey("Survey.WhistleBlowingDocument", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Survey.WhistleBlowingDocument", "UserIdImpersonatedBy", "Accounts.User");
            DropIndex("Survey.WhistleBlowing", new[] { "UserIdModifiedBy" });
            DropIndex("Survey.WhistleBlowing", new[] { "UserIdImpersonatedBy" });
            DropIndex("Survey.WhistleBlowingDocument", new[] { "UserIdModifiedBy" });
            DropIndex("Survey.WhistleBlowingDocument", new[] { "UserIdImpersonatedBy" });
            DropIndex("Survey.WhistleBlowingDocument", new[] { "WhistleBlowingId" });
            DropIndex("Survey.WhistleBlowingDocumentContent", new[] { "Id" });
            DropTable("Survey.WhistleBlowing");
            DropTable("Survey.WhistleBlowingDocument");
            DropTable("Survey.WhistleBlowingDocumentContent");
        }
    }
}
