namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InvoiceDocumentMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Compensation.InvoiceDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 250, unicode: false),
                        ContentLength = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Compensation.InvoiceDocumentContent",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Compensation.InvoiceDocument", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
            AddColumn("TimeTracking.TimeReport", "InvoiceDocumentId", c => c.Long());
            CreateIndex("TimeTracking.TimeReport", "InvoiceDocumentId");
            AddForeignKey("TimeTracking.TimeReport", "InvoiceDocumentId", "Compensation.InvoiceDocument", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("TimeTracking.TimeReport", "InvoiceDocumentId", "Compensation.InvoiceDocument");
            DropForeignKey("Compensation.InvoiceDocument", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Compensation.InvoiceDocument", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Compensation.InvoiceDocumentContent", "Id", "Compensation.InvoiceDocument");
            DropIndex("Compensation.InvoiceDocumentContent", new[] { "Id" });
            DropIndex("Compensation.InvoiceDocument", new[] { "UserIdModifiedBy" });
            DropIndex("Compensation.InvoiceDocument", new[] { "UserIdImpersonatedBy" });
            DropIndex("TimeTracking.TimeReport", new[] { "InvoiceDocumentId" });
            DropColumn("TimeTracking.TimeReport", "InvoiceDocumentId");
            DropTable("Compensation.InvoiceDocumentContent");
            DropTable("Compensation.InvoiceDocument");
        }
    }
}
