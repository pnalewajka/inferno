﻿MERGE Dictionaries.City AS Target
USING (
		SELECT *
		 FROM ( 
		VALUES	
			    ('Ottawa', 45, 0, 0),
				('Oslo', 46, 0, 0),				
				('Seul', 47, 0, 0),
				('Helsinki', 18, 0, 0),
				('Southampton', 6, 0, 0),				
				('Amsterdam', 28, 0, 0),
				('Augsburg', 3, 0, 0),
				('Wien', 1, 0, 0),
				('Dattwil', 3, 0, 0),
				('Basel', 34, 0, 0),		
				('Bydgoszcz', 4, 0, 0),
				('Chorzow', 4, 0, 0),				
				('Cluj-Napoca', 30, 0, 0),
				('Copenhagen', 16, 0, 0),				
				('Darmstadt', 3, 0, 0),
				('Dublin', 21, 0, 0),
				('Duisburg', 3, 0, 0),
				('Dzierzoniow', 4, 0, 0),				
				('Ennis', 21, 0, 0),
				('Gdansk', 4, 0, 0),				
				('Gdynia', 4, 0, 0),
				('Genova', 22, 0, 0),
				('Gliwice', 4, 0, 0),
				('Gorlitz', 3, 0, 0),		
				('Hannover', 3, 0, 0),
				('Heidelberg', 3, 0, 0),				
				('Jelenia Gora', 4, 0, 0),
				('Katowice', 4, 0, 0),
				('Stockholm', 5, 0, 0),
				('Koblenz', 3, 0, 0),
				('Bonn', 3, 0, 0),				
				('Kopenhaga', 16, 0, 0),
				('Leeds', 6, 0, 0),
				('Lodz', 4, 0, 0),			
				('Malmo', 5, 0, 0),				
				('Madryt', 8, 0, 0),
				('Mannheim', 3, 0, 0),
				('Mediolan', 22, 0, 0),
				('Mons', 11, 0, 0),				
				('Moskwa', 31, 0, 0),
				('Nassau', 3, 0, 0),
				('Noordwijk', 28, 0, 0),
				('Pila', 4, 0, 0),				
				('Plock', 4, 0, 0),
				('Hamburg', 3, 0, 0),			
				('Stuttgart', 3, 0, 0),
				('Sophia', 12, 0, 0),
				('Sopot', 4, 0, 0),
				('Edynburg', 6, 0, 0),		
				('Berno', 34, 0, 0),				
				('Talin', 17, 0, 0),
				('Torun', 4, 0, 0),
				('Koszalin', 4, 0, 0),
				('Wangen im Allgau', 3, 0, 0),		
				('Rome', 22, 0, 0),				
				('Wolfsburg', 3, 0, 0),
				('Wuppertal', 3, 0, 0),				
				('Zabkowice Slaskie', 4, 0, 0),
				('Zdunska Wola', 4, 0, 0),
				('Echternach', 25, 0, 0)
				)
            AS s (Name, CountryId, IsCompanyApartmentAvailable, IsVoucherServiceAvailable)
     ) AS Source
ON Target.[Name] = Source.[Name]
WHEN NOT MATCHED THEN
    INSERT (Name, CountryId, IsCompanyApartmentAvailable, IsVoucherServiceAvailable)
    VALUES (Source.[Name], Source.CountryId, Source.IsCompanyApartmentAvailable, Source.IsVoucherServiceAvailable);


