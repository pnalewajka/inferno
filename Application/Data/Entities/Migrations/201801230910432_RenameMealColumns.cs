namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameMealColumns : DbMigration
    {
        public override void Up()
        {
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "BreakfastCount", c => c.Long(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "LunchCount", c => c.Long(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "DinnerCount", c => c.Long(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestVehicleMileage", "IsCarEngineCapacityOver900cc", c => c.Boolean(nullable: false));
            AlterColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", c => c.Long(nullable: false));
            CreateIndex("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "Breakfast");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "Lunch");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "Dinner");
            DropColumn("Workflows.BusinessTripSettlementRequestVehicleMileage", "IsCarEngineCapacityOver900ccc");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripSettlementRequestVehicleMileage", "IsCarEngineCapacityOver900ccc", c => c.Boolean(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "Dinner", c => c.Long(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "Lunch", c => c.Long(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "Breakfast", c => c.Long(nullable: false));
            DropIndex("Workflows.BusinessTripSettlementRequestCompanyCost", new[] { "CardHolderEmployeeId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId", c => c.Long());
            DropColumn("Workflows.BusinessTripSettlementRequestVehicleMileage", "IsCarEngineCapacityOver900cc");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "DinnerCount");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "LunchCount");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "BreakfastCount");
            CreateIndex("Workflows.BusinessTripSettlementRequestCompanyCost", "CardHolderEmployeeId");
        }
    }
}
