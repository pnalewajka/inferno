namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration230420181225 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            //DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            //DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningGeneralOpinion", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningMotivation", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkplaceExpectations", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningSkills", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageEnglish", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageGerman", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageOther", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningContractExpectations", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningAvailability", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningOtherProcesses", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningCounterOfferCriteria", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationCanDo", c => c.Boolean());
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationComment", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsCanDo", c => c.Boolean());
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsComment", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitNeeded", c => c.Boolean());
            //AddColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitComment", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long());
            //AlterColumn("Recruitment.ProjectDescription", "AboutClient", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "SpecificDuties", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "ToolsAndEnvironment", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "AboutTeam", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "ProjectTimeline", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "Methodology", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "RequiredSkills", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "AdditionalSkills", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "RecruitmentDetails", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "TravelAndAccomodation", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "RemoteWorkArrangements", c => c.String(maxLength: 100));
            //AlterColumn("Recruitment.ProjectDescription", "Comments", c => c.String(maxLength: 255));
            //AlterColumn("Recruitment.RecruitmentProcessStep", "PlannedOn", c => c.DateTime());
            //AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionMakerId", c => c.Long());
            //AlterColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long());
            //CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
            //CreateIndex("Recruitment.JobOpening", "DecisionMakerEmployeeId");
            //CreateIndex("Recruitment.RecruitmentProcessStep", "DecisionMakerId");
            //CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            //AddForeignKey("Recruitment.JobOpening", "DecisionMakerEmployeeId", "Allocation.Employee", "Id");
            //DropColumn("Recruitment.RecruitmentProcess", "ExpectedSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "CanWorkInEvenings");
            //DropColumn("Recruitment.RecruitmentProcess", "CanTravel");
            //DropColumn("Recruitment.RecruitmentProcess", "Seniority");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.RecruitmentProcess", "Seniority", c => c.Int(nullable: false));
            //AddColumn("Recruitment.RecruitmentProcess", "CanTravel", c => c.Boolean(nullable: false));
            //AddColumn("Recruitment.RecruitmentProcess", "CanWorkInEvenings", c => c.Boolean(nullable: false));
            //AddColumn("Recruitment.RecruitmentProcess", "ExpectedSalary", c => c.String());
            //DropForeignKey("Recruitment.JobOpening", "DecisionMakerEmployeeId", "Allocation.Employee");
            //DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            //DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            //DropIndex("Recruitment.JobOpening", new[] { "DecisionMakerEmployeeId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            //AlterColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionMakerId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcessStep", "PlannedOn", c => c.DateTime(nullable: false));
            //AlterColumn("Recruitment.ProjectDescription", "Comments", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "RemoteWorkArrangements", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "TravelAndAccomodation", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "RecruitmentDetails", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "AdditionalSkills", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "RequiredSkills", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "Methodology", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "ProjectTimeline", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "AboutTeam", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "ToolsAndEnvironment", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "SpecificDuties", c => c.String());
            //AlterColumn("Recruitment.ProjectDescription", "AboutClient", c => c.String());
            //AlterColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long(nullable: false));
            //DropColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitComment");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkPermitNeeded");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsComment");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningBusinessTripsCanDo");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationComment");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningRelocationCanDo");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningCounterOfferCriteria");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningOtherProcesses");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningAvailability");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningContractExpectations");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageOther");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageGerman");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningLanguageEnglish");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningSkills");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningWorkplaceExpectations");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningMotivation");
            //DropColumn("Recruitment.RecruitmentProcess", "ScreeningGeneralOpinion");
            //CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            //CreateIndex("Recruitment.RecruitmentProcessStep", "DecisionMakerId");
            //CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
        }
    }
}
