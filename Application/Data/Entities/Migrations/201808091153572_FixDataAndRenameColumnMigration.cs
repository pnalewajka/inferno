﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    using Helpers;

    public partial class FixDataAndRenameColumnMigration : AtomicDbMigration
    {
        public override void Up()
        {
            RenameColumn("Allocation.Project", "ProjectShortName", "SubProjectShortName");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808091153572_FixDataAndRenameColumnMigration.RemoveOldSubProjectNames.Up.sql");
        }
        
        public override void Down()
        {
            RenameColumn("Allocation.Project", "SubProjectShortName", "ProjectShortName");
        }
    }
}

