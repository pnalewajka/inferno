namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ChangesForCalculatorsMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "SettledOn", c => c.DateTime(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "WithdrawnOn", c => c.DateTime(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestOwnCost", "HasInvoiceIssued", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequestOwnCost", "HasInvoiceIssued");
            DropColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "WithdrawnOn");
            DropColumn("Workflows.BusinessTripSettlementRequest", "SettledOn");
        }
    }
}
