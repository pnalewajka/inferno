namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSomeFieldsToExpenseRequestTableMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.ExpenseRequest", "EmployeeId", c => c.Long(nullable: false));
            AddColumn("Workflows.ExpenseRequest", "Date", c => c.DateTime(nullable: false));
            CreateIndex("Workflows.ExpenseRequest", "EmployeeId");
            CreateIndex("Workflows.ExpenseRequest", "ProjectId");
            CreateIndex("Workflows.ExpenseRequest", "CurrencyId");
            AddForeignKey("Workflows.ExpenseRequest", "CurrencyId", "Dictionaries.Currency", "Id");
            AddForeignKey("Workflows.ExpenseRequest", "ProjectId", "Allocation.Project", "Id");
            AddForeignKey("Workflows.ExpenseRequest", "EmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.ExpenseRequest", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Workflows.ExpenseRequest", "ProjectId", "Allocation.Project");
            DropForeignKey("Workflows.ExpenseRequest", "CurrencyId", "Dictionaries.Currency");
            DropIndex("Workflows.ExpenseRequest", new[] { "CurrencyId" });
            DropIndex("Workflows.ExpenseRequest", new[] { "ProjectId" });
            DropIndex("Workflows.ExpenseRequest", new[] { "EmployeeId" });
            DropColumn("Workflows.ExpenseRequest", "Date");
            DropColumn("Workflows.ExpenseRequest", "EmployeeId");
        }
    }
}
