namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration210320180804 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Recruitment.JobApplication", name: "CandidateID", newName: "CandidateId");
            RenameIndex(table: "Recruitment.JobApplication", name: "IX_CandidateID", newName: "IX_CandidateId");
            DropColumn("Recruitment.JobApplication", "OriginType");

            //AddColumn("Recruitment.RecruitmentProcess", "SourceContactRecordId", c => c.Long());
            //CreateIndex("Recruitment.RecruitmentProcess", "SourceContactRecordId");
            //AddForeignKey("Recruitment.RecruitmentProcess", "SourceContactRecordId", "Recruitment.ContactRecord", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.RecruitmentProcess", "SourceContactRecordId", "Recruitment.ContactRecord");
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "SourceContactRecordId" });
            //DropColumn("Recruitment.RecruitmentProcess", "SourceContactRecordId");

            AddColumn("Recruitment.JobApplication", "OriginType", c => c.Int(nullable: false));
            RenameIndex(table: "Recruitment.JobApplication", name: "IX_CandidateId", newName: "IX_CandidateID");
            RenameColumn(table: "Recruitment.JobApplication", name: "CandidateId", newName: "CandidateID");
        }
    }
}
