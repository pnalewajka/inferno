﻿ALTER TABLE Recruitment.Candidate  ALTER COLUMN OriginalSourceComment NVARCHAR (50)
ALTER TABLE Recruitment.JobApplication  ALTER COLUMN OriginComment NVARCHAR (250)