namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveLastContactOnFromCandidateEntityMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.Candidate", "LastContactByEmployeeId", "Allocation.Employee");
            DropIndex("Recruitment.Candidate", new[] { "LastContactByEmployeeId" });
            DropColumn("Recruitment.Candidate", "LastContactByEmployeeId");
            DropColumn("Recruitment.Candidate", "LastContactOn");
            DropColumn("Recruitment.Candidate", "LastContactType");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.Candidate", "LastContactType", c => c.Int());
            AddColumn("Recruitment.Candidate", "LastContactOn", c => c.DateTime());
            AddColumn("Recruitment.Candidate", "LastContactByEmployeeId", c => c.Long());
            CreateIndex("Recruitment.Candidate", "LastContactByEmployeeId");
            AddForeignKey("Recruitment.Candidate", "LastContactByEmployeeId", "Allocation.Employee", "Id");
        }
    }
}
