namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentProcessUpdateFieldsMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalCityId", "Dictionaries.City");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalCityId" });
            AddColumn("Recruitment.RecruitmentProcess", "ResumeShownToClientOn", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalSignedDate", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork", c => c.Int(nullable: false));
            AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkCanDo", c => c.Boolean());
            AddColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkComment", c => c.String(maxLength: 50));
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            DropColumn("Recruitment.RecruitmentProcess", "CvShownToClient");
            DropColumn("Recruitment.RecruitmentProcess", "CvShownToClientOn");
            DropColumn("Recruitment.RecruitmentProcess", "HiredOn");
            DropColumn("Recruitment.RecruitmentProcess", "FinalCityId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalCityId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "HiredOn", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "CvShownToClientOn", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "CvShownToClient", c => c.Boolean(nullable: false));
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkComment");
            DropColumn("Recruitment.RecruitmentProcess", "ScreeningEveningWorkCanDo");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalPlaceOfWork");
            DropColumn("Recruitment.RecruitmentProcess", "FinalSignedDate");
            DropColumn("Recruitment.RecruitmentProcess", "ResumeShownToClientOn");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalCityId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalCityId", "Dictionaries.City", "Id");
        }
    }
}
