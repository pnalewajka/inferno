namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class IncreaseMobileAndOtherPhoneOfCandidateMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806051141328_IncreaseMobileAndOtherPhoneOfCandidateMigration.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806051141328_IncreaseMobileAndOtherPhoneOfCandidateMigration.Down.sql");
        }
    }
}
