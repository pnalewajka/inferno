namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameCompanyToTravelExpensesSpecialistTableMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", "BusinessTrips.BusinessTripParticipant");
            AddForeignKey("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", "BusinessTrips.BusinessTripParticipant", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", "BusinessTrips.BusinessTripParticipant");
            AddForeignKey("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", "BusinessTrips.BusinessTripParticipant", "Id");
        }
    }
}
