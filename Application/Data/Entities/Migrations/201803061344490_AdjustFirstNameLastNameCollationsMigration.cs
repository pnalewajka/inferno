﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AdjustFirstNameLastNameCollationsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803061344490_AdjustFirstNameLastNameCollationsMigration.AdjustCollations.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

