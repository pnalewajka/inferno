﻿---- LATEST PROCESS  
UPDATE Recruitment.Candidate 
SET
    Recruitment.Candidate.LastActivityOn = MostRecentProcesses.LastModifiedOn,
    Recruitment.Candidate.LastActivityById = MostRecentProcesses.LastModifiedBy,
    Recruitment.Candidate.LastReferenceType = 16,
    Recruitment.Candidate.LastReferenceId = MostRecentProcesses.ReferenceId
 FROM (
        SELECT
                RANK() OVER (PARTITION BY CandidateId ORDER BY rp.ModifiedOn DESC) AS #Rank,
                rp.CandidateId,
                rp.Id AS ReferenceId,
                rp.ModifiedOn AS LastModifiedOn,
                e.Id AS LastModifiedBy
          FROM Recruitment.RecruitmentProcess as rp 
          JOIN Allocation.Employee as e ON e.UserId = rp.UserIdModifiedBy
          ) AS MostRecentProcesses
WHERE
     (Recruitment.Candidate.LastActivityOn IS NULL 
     OR Recruitment.Candidate.LastActivityOn < MostRecentProcesses.LastModifiedOn)
     AND Recruitment.Candidate.Id = MostRecentProcesses.CandidateId
     AND MostRecentProcesses.#Rank = 1
     
---- LATEST STEP
UPDATE Recruitment.Candidate 
SET
    Recruitment.Candidate.LastActivityOn = MostRecentStep.LastModifiedOn,
    Recruitment.Candidate.LastActivityById = MostRecentStep.LastModifiedBy,
    Recruitment.Candidate.LastReferenceType = 17,
    Recruitment.Candidate.LastReferenceId = MostRecentStep.ReferenceId
 FROM (
        SELECT	
                RANK() OVER (PARTITION BY rp.CandidateId ORDER BY rps.ModifiedOn DESC) AS #Rank,
                rp.CandidateId,
                rps.Id AS ReferenceId,
                rps.ModifiedOn AS LastModifiedOn,
                e.Id AS LastModifiedBy
           FROM Recruitment.RecruitmentProcessStep AS rps
           JOIN Recruitment.RecruitmentProcess AS rp ON rps.RecruitmentProcessId = rp.Id
           JOIN Allocation.Employee as e ON e.UserId = rps.UserIdModifiedBy
          ) AS MostRecentStep
WHERE
     (Recruitment.Candidate.LastActivityOn IS NULL 
     OR Recruitment.Candidate.LastActivityOn < MostRecentStep.LastModifiedOn)
     AND Recruitment.Candidate.Id = MostRecentStep.CandidateId
     AND MostRecentStep.#Rank = 1
     
----- LATEST FILE
UPDATE Recruitment.Candidate 
SET
    Recruitment.Candidate.LastActivityOn = MostRecentFile.LastModifiedOn,
    Recruitment.Candidate.LastActivityById = MostRecentFile.LastModifiedBy,
    Recruitment.Candidate.LastReferenceType = 14,
    Recruitment.Candidate.LastReferenceId = MostRecentFile.ReferenceId
 FROM (
        SELECT	
                RANK() OVER (PARTITION BY cf.CandidateId ORDER BY cf.ModifiedOn DESC) AS #Rank,
                cf.CandidateId,
                cf.Id AS ReferenceId,
                cf.ModifiedOn AS LastModifiedOn,
                e.Id AS LastModifiedBy
           FROM Recruitment.CandidateFile AS cf
           JOIN Allocation.Employee as e ON e.UserId = cf.UserIdModifiedBy
          ) AS MostRecentFile
WHERE
     (Recruitment.Candidate.LastActivityOn IS NULL 
     OR Recruitment.Candidate.LastActivityOn < MostRecentFile.LastModifiedOn)
     AND Recruitment.Candidate.Id = MostRecentFile.CandidateId
     AND MostRecentFile.#Rank = 1
         
----- LATEST NOTE
UPDATE Recruitment.Candidate 
SET
    Recruitment.Candidate.LastActivityOn = MostRecentNote.LastModifiedOn,
    Recruitment.Candidate.LastActivityById = MostRecentNote.LastModifiedBy,
    Recruitment.Candidate.LastReferenceType = 7,
    Recruitment.Candidate.LastReferenceId = MostRecentNote.ReferenceId
 FROM (
        SELECT	
                RANK() OVER (PARTITION BY cf.CandidateId ORDER BY cf.ModifiedOn DESC) AS #Rank,
                cf.CandidateId,
                cf.Id AS ReferenceId,
                cf.ModifiedOn AS LastModifiedOn,
                e.Id AS LastModifiedBy
           FROM Recruitment.CandidateNote AS cf
           JOIN Allocation.Employee as e ON e.UserId = cf.UserIdModifiedBy
          ) AS MostRecentNote
WHERE
     (Recruitment.Candidate.LastActivityOn IS NULL 
     OR Recruitment.Candidate.LastActivityOn < MostRecentNote.LastModifiedOn)
     AND Recruitment.Candidate.Id = MostRecentNote.CandidateId
     AND MostRecentNote.#Rank = 1
         
----- LATEST CONTACT
UPDATE Recruitment.Candidate 
SET
    Recruitment.Candidate.LastActivityOn = MostRecentContact.LastModifiedOn,
    Recruitment.Candidate.LastActivityById = MostRecentContact.LastModifiedBy,
    Recruitment.Candidate.LastReferenceType = 5,
    Recruitment.Candidate.LastReferenceId = MostRecentContact.ReferenceId
 FROM (
        SELECT	
                RANK() OVER (PARTITION BY ccr.CandidateId ORDER BY ccr.ModifiedOn DESC) AS #Rank,
                ccr.CandidateId,
                ccr.Id AS ReferenceId,
                ccr.ModifiedOn AS LastModifiedOn,
                e.Id AS LastModifiedBy
           FROM Recruitment.CandidateContactRecord AS ccr		   
           JOIN Allocation.Employee as e ON e.UserId = ccr.UserIdModifiedBy
          ) AS MostRecentContact
WHERE
     (Recruitment.Candidate.LastActivityOn IS NULL 
     OR Recruitment.Candidate.LastActivityOn < MostRecentContact.LastModifiedOn)
     AND Recruitment.Candidate.Id = MostRecentContact.CandidateId
     AND MostRecentContact.#Rank = 1