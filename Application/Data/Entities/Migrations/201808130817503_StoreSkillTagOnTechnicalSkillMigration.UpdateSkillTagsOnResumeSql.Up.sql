﻿UPDATE [Resumes].[ResumeTechnicalSkill]
  SET [SkillTagId] = (
	SELECT TOP 1 SkillTag_Id FROM SkillManagement.SkillTagSkill sts 
	LEFT JOIN SkillManagement.SkillTag st ON st.Id = sts.SkillTag_Id
	WHERE sts.Skill_Id = TechnicalSkillId AND st.TagType & 32 <> 0
	)