﻿SET XACT_ABORT ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogEmployeeDataActivity]'))
   DROP PROCEDURE [GDPR].[LogEmployeeDataActivity]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogUserDataActivity]'))
   DROP PROCEDURE [GDPR].[LogUserDataActivity]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogCandidateDataActivity]'))
   DROP PROCEDURE [GDPR].[LogCandidateDataActivity]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogRecommendingPersonDataActivity]'))
   DROP PROCEDURE [GDPR].[LogRecommendingPersonDataActivity]
GO