SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Accounts].[atomic_AddRoleToProfile]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Accounts].[atomic_AddRoleToProfile]
END
GO

CREATE PROCEDURE [Accounts].[atomic_AddRoleToProfile]
 	@roleName VARCHAR(255),
	@profileName VARCHAR(255)
AS
BEGIN
	DECLARE @roleId BIGINT
	SELECT @roleId = [Id] FROM [Accounts].[SecurityRole] WHERE [Name] = @roleName
	DECLARE @profileId BIGINT
	SELECT @profileId = [Id] FROM [Accounts].[SecurityProfile] WHERE [Name] = @profileName
	IF @profileId IS NULL
	BEGIN
		DECLARE @errorMsg VARCHAR(700) = ''
		SET @errorMsg = @errorMsg + CASE WHEN (LEN(@errorMsg)>0) THEN ' ' ELSE '' END + 'Profile: ' + ISNULL(@roleName,'') + ' is missing'
		RAISERROR(@errorMsg, 16, 1);
	END
	IF @roleId IS NULL
	BEGIN
		SELECT @roleId = COALESCE(MAX([Id]), 0) + 1 FROM [Accounts].[SecurityRole]
		INSERT INTO [Accounts].[SecurityRole] ([Id], [Name]) VALUES (@roleId, @roleName)
		INSERT INTO [Accounts].[SecurityAreaSecurityRoles] ([AreaId], [RoleId])
			SELECT DISTINCT sasr.[AreaId] AS [AreaId], @roleId AS [RoleId]
				FROM [Accounts].[SecurityAreaSecurityRoles] sasr
				JOIN [Accounts].[SecurityRoleSecurityProfiles] srsp ON sasr.RoleId = srsp.RoleId
				WHERE srsp.[ProfileId] = @profileId
		IF NOT EXISTS (SELECT * FROM [Accounts].[SecurityAreaSecurityRoles] WHERE [RoleId] = @roleId)
		BEGIN
			INSERT INTO [Accounts].[SecurityAreaSecurityRoles] ([AreaId], [RoleId])
				SELECT [Id] AS [AreaId], @roleId AS [RoleId] FROM [Accounts].[SecurityArea]
		END
	END

	MERGE 
		INTO [Accounts].[SecurityRoleSecurityProfiles] AS target
		USING ( VALUES(@roleId, @profileId) ) AS source ([RoleId],[ProfileId])
		ON target.[RoleId] = source.[RoleId] AND target.[ProfileId] = source.[ProfileId]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ([RoleId], [ProfileId]) VALUES (source.[RoleId], source.[ProfileId]) ;

END
GO
---------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Accounts].[atomic_AddRoleToAdministrativeProfile]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Accounts].[atomic_AddRoleToAdministrativeProfile]
END
GO

CREATE PROCEDURE [Accounts].[atomic_AddRoleToAdministrativeProfile]
	 	@roleName varchar(255)
	AS
	BEGIN
		EXECUTE [Accounts].[atomic_AddRoleToProfile] @roleName = @roleName, @profileName = "Super Administrator"
	END
GO
----------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Accounts].[atomic_VerifySecurityAreasIntegrity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Accounts].[atomic_VerifySecurityAreasIntegrity]
END
GO

CREATE PROCEDURE [Accounts].[atomic_VerifySecurityAreasIntegrity]
	@UserId BIGINT = NULL
AS 
BEGIN
	DECLARE @output VARCHAR(MAX) = '<BusinessException xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="SecurityAreaRoleViolationException"><Violations>'

	SELECT TOP 5
		@output = @output + '<ViolationDetail>' +
			'<UserName>' + (UserName COLLATE DATABASE_DEFAULT) + '</UserName>' +
			CASE WHEN ProfileName IS NULL THEN '' ELSE '<Profile>' + ProfileName + '</Profile>' END +
			'<Role>' + SecurityRoleName + '</Role>' +
			'</ViolationDetail>'
	FROM
	(
		SELECT
			*,
			MAX(AreaMatched) OVER (PARTITION BY UserId, RoleId) AS RangeMatched
		FROM
		(
			SELECT 
				EffectiveRoles.*,
				sasr.AreaId, 
				sa.Code AS RoleAreaName,
				CASE WHEN EffectiveRoles.SecurityAreaId = sasr.AreaId THEN 1 ELSE 0 END AS AreaMatched
			FROM
			(
				SELECT 
					u.Id AS UserId, u.FirstName + ' ' + u.LastName AS UserName, u.SecurityAreaId, sa.Code AS Area, spProfile.Id as ProfileId, spProfile.Name AS ProfileName, srspProfile.RoleId, sr.Name AS SecurityRoleName
				FROM
					[Accounts].[User] u
				JOIN
					([Accounts].[UserSecurityProfiles] uspProfile 
						JOIN [Accounts].[SecurityProfile] spProfile ON uspProfile.ProfileId = spProfile.Id
						JOIN [Accounts].[SecurityRoleSecurityProfiles] srspProfile ON uspProfile.ProfileId = srspProfile.ProfileId
					)
					ON u.ID = uspProfile.UserId
				JOIN [Accounts].[SecurityArea] sa ON u.SecurityAreaId = sa.Id
				JOIN [Accounts].[SecurityRole] sr ON sr.ID =  srspProfile.RoleId
				UNION ALL
				SELECT 
					u.Id AS UserId, u.FirstName + ' ' + u.LastName AS UserName, u.SecurityAreaId, sa.Code AS Area, NULL AS ProfileId, NULL AS ProfileName, usrRole.RoleId, sr.Name AS SecurityRoleName
				FROM
					[Accounts].[User] u
				JOIN [Accounts].[UserSecurityRoles] usrRole ON u.ID = usrRole.UserId
				JOIN [Accounts].[SecurityArea] sa ON u.SecurityAreaId = sa.Id
				JOIN [Accounts].[SecurityRole] sr ON sr.ID =  usrRole.RoleId
			) AS EffectiveRoles
			JOIN [Accounts].[SecurityAreaSecurityRoles] sasr ON sasr.RoleId = EffectiveRoles.RoleId
			JOIN [Accounts].[SecurityArea] sa ON sa.Id = sasr.AreaId
		) AS EffectiveRolesAndAreas
	) AS ResolvedTable
	WHERE 
	RangeMatched = 0
	AND AreaMatched = 0
	AND (@UserId IS NULL OR UserId = @UserId)

	DECLARE @count BIGINT
	SELECT @count = COUNT(1)
	FROM
	(
		SELECT
			*,
			MAX(AreaMatched) OVER (PARTITION BY UserId, RoleId) AS RangeMatched
		FROM
		(
			SELECT 
				EffectiveRoles.*,
				sasr.AreaId, 
				CASE WHEN EffectiveRoles.SecurityAreaId = sasr.AreaId THEN 1 ELSE 0 END AS AreaMatched
			FROM
			(
				SELECT 
					u.Id AS UserId, u.SecurityAreaId, spProfile.Id as ProfileId, srspProfile.RoleId
				FROM
					[Accounts].[User] u
				JOIN
					([Accounts].[UserSecurityProfiles] uspProfile 
						JOIN [Accounts].[SecurityProfile] spProfile ON uspProfile.ProfileId = spProfile.Id
						JOIN [Accounts].[SecurityRoleSecurityProfiles] srspProfile ON uspProfile.ProfileId = srspProfile.ProfileId
					)
					ON u.ID = uspProfile.UserId
				UNION ALL
				SELECT 
					u.Id AS UserId, u.SecurityAreaId, NULL as ProfileId, usrRole.RoleId
				FROM
					[Accounts].[User] u
				JOIN
					[Accounts].[UserSecurityRoles] usrRole ON u.ID = usrRole.UserId
			) AS EffectiveRoles
			JOIN 
				[Accounts].[SecurityAreaSecurityRoles] sasr ON sasr.RoleId = EffectiveRoles.RoleId
		) AS EffectiveRolesAndAreas
	) AS ResolvedTable
	WHERE 
	RangeMatched = 0
	AND AreaMatched = 0
	AND (@UserId IS NULL OR UserId = @UserId)

	SET @output = @output + '</Violations><ViolationCount>' + CAST(@count AS VARCHAR(10)) + '</ViolationCount></BusinessException>'
	IF(@count > 0)
	BEGIN
		RAISERROR('%s', 16, 1, @output)
		IF(@@TRANCOUNT>0)
		BEGIN
			ROLLBACK
		END
	END
END
GO  
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_AmendAlterSchema]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_AmendAlterSchema]
END
GO

CREATE PROCEDURE [Audit].[atomic_AmendAlterSchema] 
	@tableName VARCHAR(100),
	@schemaName VARCHAR(200),
	@newSchemaName VARCHAR(200)
AS 
BEGIN
	DECLARE @logTableName VARCHAR(200)='[Audit].[' + @schemaName + '_' + @tableName + '_Log]'
	DECLARE @newLogTableName VARCHAR(400)
	SET @newLogTableName = @newSchemaName + '_' + @tableName + '_Log'
	EXEC sys.sp_rename @logTableName,  @newLogTableName
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_AmendRename]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_AmendRename]
END
GO

CREATE PROCEDURE [Audit].[atomic_AmendRename] 
	@tableName VARCHAR(100),
	@columnName VARCHAR(100),
	@newName VARCHAR(100),
	@schemaName VARCHAR(200)
AS 
BEGIN
	DECLARE @logTableName VARCHAR(200)='[Audit].[' + @schemaName + '_' + @tableName + '_Log]'
	DECLARE @newLogTableName VARCHAR(400)
	DECLARE @fullColumnName VARCHAR(400) = @logTableName + '.'+@columnName
	IF @columnName IS NOT NULL
	BEGIN
		EXEC sys.sp_rename @fullColumnName,  @newName, 'COLUMN'
	END
	ELSE
	BEGIN
		SET @newLogTableName = @schemaName + '_' + @newName + '_Log'
		EXEC sys.sp_rename @logTableName,  @newLogTableName
	END
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_AmendTrigger]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_AmendTrigger]
END
GO

CREATE PROCEDURE [Audit].[atomic_AmendTrigger]
	@tableName varchar(100),
	@schemaName varchar(100),
	@removeTableFromDisabled bit = 0
AS
BEGIN
	DECLARE @fullTableName VARCHAR(200)='[' + @schemaName + '].[' + @tableName + ']'
	DECLARE @logTableName VARCHAR(200)='[Audit].[' + @schemaName + '_' + @tableName + '_Log]'
	DECLARE @triggerName VARCHAR(200) = '[' + @schemaName + '].[' + @tableName + '_Dml]'
	DECLARE @createScript NVARCHAR(MAX) = ''
	DECLARE @columnsScript NVARCHAR(MAX) = ''
	DECLARE @resultScript NVARCHAR(MAX) = ''
	DECLARE @dropScript NVARCHAR(MAX) = ''

	IF EXISTS(SELECT 1 FROM [Audit].[Exclusions] WHERE ExcludeObject = OBJECT_ID(@fullTableName) OR LOWER(ExcludeTable) = LOWER(@fullTableName))
	BEGIN
		RETURN;
	END
	EXEC [Audit].[atomic_DropTrigger] @tableName=@tableName, @schemaName=@schemaName

	IF(@removeTableFromDisabled = 1)
	BEGIN
		DELETE FROM [Audit].[DisabledTables]
		WHERE ExcludeObject = OBJECT_ID(@fullTableName) OR LOWER(ExcludeTable) = LOWER(@fullTableName)
	END

	SELECT 
		@columnsScript = @columnsScript+ CASE WHEN LEN(@columnsScript)>0 THEN ',' ELSE '' END + '['+name+']'
	FROM 
		sys.all_columns 
	WHERE 
		object_id = OBJECT_ID(@fullTableName) 
		AND system_type_id != TYPE_ID('timestamp')
		AND is_computed = 0;
	
	SET @createScript = 'CREATE TRIGGER '+@triggerName+'
ON '+@fullTableName+'
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @deletedCount INT = 0
	DECLARE @insertedCount INT = 0
	SELECT @deletedCount = COUNT(1) FROM DELETED;
	SELECT @insertedCount = COUNT(1) FROM INSERTED;
	IF(@insertedCount>0 AND @deletedCount=0)
	BEGIN
		INSERT INTO ' + @logTableName + '
		(
			_ModifiedOn,_OperationType,_RemoteAddress,_DbUser,' + @columnsScript + '
		)
		SELECT GETDATE(), ''I'', CONVERT(VARCHAR(30), CONNECTIONPROPERTY(''client_net_address'') ),CAST(SYSTEM_USER AS NVARCHAR(100)),' + @columnsScript + ' FROM INSERTED
	END
	IF(@insertedCount>0 AND @deletedCount>0)
	BEGIN
		INSERT INTO ' + @logTableName + '
		(
			_ModifiedOn,_OperationType,_RemoteAddress,_DbUser,' + @columnsScript + ' 
		)
		SELECT GETDATE(), ''U'',CONVERT(VARCHAR(30), CONNECTIONPROPERTY(''client_net_address'') ), CAST(SYSTEM_USER AS NVARCHAR(100)),' + @columnsScript + ' FROM INSERTED
	END
	IF(@insertedCount=0 AND @deletedCount>0)
	BEGIN
		INSERT INTO ' + @logTableName + '
		(
			_ModifiedOn,_OperationType,_RemoteAddress,_DbUser,' + @columnsScript + '
		)
		SELECT GETDATE(), ''D'',CONVERT(VARCHAR(30), CONNECTIONPROPERTY(''client_net_address'') ),CAST(SYSTEM_USER AS NVARCHAR(100)), ' + @columnsScript + ' FROM DELETED
	END
END'
	EXEC sys.sp_executesql @createScript
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_DisableAudit]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_DisableAudit]
END
GO

CREATE PROCEDURE [Audit].[atomic_DisableAudit] 
	@dropAuditTables bit = 0
AS
BEGIN

	BEGIN TRANSACTION

	EXEC sp_executesql N'DISABLE TRIGGER [atomic_TableSchemaUpdateTrigger] ON DATABASE'
	EXEC sp_executesql N'DISABLE TRIGGER [atomic_SchemaUpdateTrigger] ON DATABASE'
	EXEC sp_executesql N'DISABLE TRIGGER [atomic_TableAlterSchemaTrigger] ON DATABASE'
	EXEC sp_executesql N'DISABLE TRIGGER [atomic_TableSchemaRenameTrigger] ON DATABASE'

	DECLARE @sourceTableName VARCHAR(200)
	DECLARE @schemaName VARCHAR(200)
	DECLARE @logTableName VARCHAR(200)
	DECLARE @oldFullTableName VARCHAR(400)
	DECLARE @newTableName VARCHAR(400)
	DECLARE @sql NVARCHAR(4000)
	DECLARE @localTableName VARCHAR(200)
	DECLARE @localSchemaName VARCHAR(200)
	DECLARE @stringIndex BIGINT
	DECLARE @triggerName VARCHAR(200)

	DECLARE LogTablePairs CURSOR FOR
	SELECT st.name As SourceTableName, SCHEMA_NAME(st.schema_id) as SchemaName, st_log.name AS LogTableName FROM sys.tables st
	JOIN sys.tables st_log 
		ON st_log.name = SCHEMA_NAME(st.schema_id) + '_' + st.name + '_Log' 
		AND st_log.schema_id = SCHEMA_ID('Audit')
	WHERE 
		st.schema_id <> SCHEMA_ID('Audit') 
		AND st.schema_id <> SCHEMA_ID('dbo')

	OPEN LogTablePairs

	FETCH NEXT FROM LogTablePairs INTO @sourceTableName, @schemaName, @logTableName
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC [Audit].[atomic_DropTrigger] @tableName =  @sourceTableName, @schemaName = @schemaName

		IF @dropAuditTables = 1
		BEGIN
			SET @sql = 'DROP TABLE [Audit].[' + @logTableName + ']'
			EXEC sp_executesql @sql
		END

		FETCH NEXT FROM LogTablePairs INTO @sourceTableName, @schemaName, @LogTableName
	END

	CLOSE LogTablePairs
	DEALLOCATE LogTablePairs

	DECLARE LogTables CURSOR FOR
	SELECT st.name As SourceTableName FROM sys.tables st
		WHERE st.schema_id = SCHEMA_ID('Audit')  AND st.name NOT IN ('Ddl_Log', 'Exclusions')

	OPEN LogTables

	FETCH NEXT FROM LogTables INTO @logTableName
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @localTableName = REPLACE(@logTableName, '_Log','')
		SET @stringIndex  = CHARINDEX('_',@localTableName)

		IF(@stringIndex > 0 )
		BEGIN
			SET @localTableName = SUBSTRING(@localTableName, @stringIndex + 1, LEN(@localTableName) - @stringIndex)
		END

		SET @triggerName = NULL
		SELECT  
			@triggerName = tr.name, 
			@localSchemaName = SCHEMA_NAME(t.schema_id) 
		FROM 
			sys.triggers tr 
		JOIN 
			sys.tables t ON tr.parent_id = t.object_id
		WHERE tr.name = @localTableName +'_Dml'

		IF @triggerName IS NOT NULL 
		BEGIN
			EXEC [Audit].[atomic_DropTrigger] @tableName = @localTableName, @schemaName = @localSchemaName
		END

		IF @dropAuditTables = 1
		BEGIN
			SET @sql = 'DROP TABLE [Audit].[' + @logTableName + ']'
			EXEC sp_executesql @sql
		END

		FETCH NEXT FROM LogTables INTO @logTableName
	END

	CLOSE LogTables
	DEALLOCATE LogTables

	COMMIT	
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_DisableAuditOnTable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_DisableAuditOnTable]
END
GO

CREATE PROCEDURE [Audit].[atomic_DisableAuditOnTable]
	@Id bigint,
	@dropAuditTable bit

AS
BEGIN

	BEGIN TRANSACTION

	DECLARE @tableName VARCHAR(200)
	DECLARE @schemaName VARCHAR(200) 

	DECLARE @sql NVARCHAR(4000)

	SELECT @schemaName = SCHEMA_NAME(st_log.schema_id), @tableName = st_log.Name
	FROM sys.tables st_log
	WHERE st_log.object_id = @Id

	EXEC [Audit].[atomic_DropTrigger] @tableName =  @tableName, @schemaName = @schemaName, @addTableToDisabled = 1

	IF @dropAuditTable = 1
	BEGIN
		SET @sql = 'DROP TABLE ' + '[' + @schemaName + '].[' + @tableName + ']'
		EXEC sp_executesql @sql
	END

	COMMIT
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_DropTrigger]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_DropTrigger]
END
GO

CREATE PROCEDURE [Audit].[atomic_DropTrigger]
	@tableName VARCHAR(100),
	@schemaName varchar(100),
	@addTableToDisabled bit = 0
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @dmlTrigger NVARCHAR(200) = '['+@schemaName+'].['+@tableName +'_Dml]'
	DECLARE @fullTableName NVARCHAR(200) = '['+@schemaName+'].['+@tableName +']'

	IF OBJECT_ID(@dmlTrigger) IS NOT NULL
	BEGIN
		DECLARE @sql NVARCHAR(300) = N'DROP TRIGGER '+@dmlTrigger

		IF (@addTableToDisabled = 1)
		BEGIN
			INSERT INTO [Audit].[DisabledTables] (ExcludeObject, ExcludeTable)
			VALUES(OBJECT_ID(@fullTableName), @fullTableName)
		END

		EXEC sys.sp_executesql @sql
	END
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_EnableAudit]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_EnableAudit]
END
GO

CREATE PROCEDURE [Audit].[atomic_EnableAudit] 
AS
BEGIN

	BEGIN TRANSACTION

	DECLARE @localTableName VARCHAR(200)
	DECLARE @localSchemaName VARCHAR(200)
	DECLARE @fullTableName VARCHAR(200)

	DECLARE LogTablePairs CURSOR FOR
	SELECT 
		st.name As TableName, 
		SCHEMA_NAME(st.schema_id) as SchemaName
	FROM sys.tables st
	WHERE 
		st.schema_id <> SCHEMA_ID('Audit') 
		AND st.schema_id <> SCHEMA_ID('dbo')

	OPEN LogTablePairs

	FETCH NEXT FROM LogTablePairs INTO @localTableName, @localSchemaName
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @fullTableName = '[' + @localSchemaName + '].[' + @localTableName + ']'

		EXEC [Audit].[atomic_AmendAuditStructure] @tableName = @localTableName, @schemaName = @localSchemaName

		IF NOT EXISTS(SELECT 1 FROM [Audit].[DisabledTables] WHERE ExcludeObject = OBJECT_ID(@fullTableName) OR LOWER(ExcludeTable) = LOWER(@fullTableName))
		BEGIN
			EXEC [Audit].[atomic_AmendTrigger] @tableName = @localTableName, @schemaName = @localSchemaName
		END

		FETCH NEXT FROM LogTablePairs INTO @localTableName, @localSchemaName
	END

	CLOSE LogTablePairs
	DEALLOCATE LogTablePairs


	EXEC sp_executesql N'ENABLE TRIGGER [atomic_TableSchemaUpdateTrigger] ON DATABASE'
	EXEC sp_executesql N'ENABLE TRIGGER [atomic_TableAlterSchemaTrigger] ON DATABASE'
	EXEC sp_executesql N'ENABLE TRIGGER [atomic_TableSchemaRenameTrigger] ON DATABASE'
	EXEC sp_executesql N'ENABLE TRIGGER [atomic_SchemaUpdateTrigger] ON DATABASE'

	COMMIT	
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_EnableAuditOnTable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_EnableAuditOnTable]
END
GO

CREATE PROCEDURE [Audit].[atomic_EnableAuditOnTable]
	@Id bigint

AS
BEGIN

	BEGIN TRANSACTION

	DECLARE @tableName VARCHAR(200)
	DECLARE @schemaName VARCHAR(200) 

	DECLARE @sql NVARCHAR(4000)

	SELECT @schemaName = SCHEMA_NAME(st_log.schema_id), @tableName = st_log.Name
	FROM sys.tables st_log
	WHERE st_log.object_id = @Id

	EXEC [Audit].[atomic_AmendAuditStructure] @tableName = @tableName, @schemaName = @schemaName
	EXEC [Audit].[atomic_AmendTrigger] @tableName =  @tableName, @schemaName = @schemaName, @removeTableFromDisabled = 1

	COMMIT
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_ExcludeTable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_ExcludeTable]
END
GO

CREATE PROCEDURE [Audit].[atomic_ExcludeTable]
	@tableName NVARCHAR(100),
	@schemaName NVARCHAR(100)
AS 
BEGIN
	SET NOCOUNT ON
END
DECLARE @fullName VARCHAR(255)
SET @fullName = '['+@schemaName+'].['+@tableName+']'
IF( NOT EXISTS (SELECT 1 FROM [Audit].[Exclusions] WHERE LOWER(ExcludeTable) = LOWER(@fullName)) )
BEGIN
	INSERT INTO [Audit].[Exclusions] (ExcludeTable) VALUES (@fullName);
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Audit].[atomic_IsAuditEnabled]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Audit].[atomic_IsAuditEnabled]
END
GO

CREATE PROCEDURE [Audit].[atomic_IsAuditEnabled]
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @isTableSchemaUpdateTriggerDisabled bit
	DECLARE @isTableAlterSchemaTriggerDisabled bit
	DECLARE @isTableSchemaRenameTriggerDisabled bit
	DECLARE @isSchemaUpdateTriggerDisabled bit

	SELECT @isTableSchemaUpdateTriggerDisabled = tr.is_disabled
	FROM sys.triggers tr
	WHERE tr.name = 'atomic_TableSchemaUpdateTrigger'

	SELECT @isTableAlterSchemaTriggerDisabled = tr.is_disabled
	FROM sys.triggers tr
	WHERE tr.name = 'atomic_TableAlterSchemaTrigger'

	SELECT @isTableSchemaRenameTriggerDisabled = tr.is_disabled
	FROM sys.triggers tr
	WHERE tr.name = 'atomic_TableSchemaRenameTrigger'

	SELECT @isSchemaUpdateTriggerDisabled = tr.is_disabled
	FROM sys.triggers tr
	WHERE tr.name = 'atomic_SchemaUpdateTrigger'
	
	SELECT CASE WHEN @isTableSchemaUpdateTriggerDisabled = 0
			AND @isTableAlterSchemaTriggerDisabled = 0
			AND @isTableSchemaRenameTriggerDisabled = 0
			AND @isSchemaUpdateTriggerDisabled = 0
		THEN
			CAST(1 AS bit)
		ELSE
			CAST(0 AS bit)
		END
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_AcquireJobRunInfo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_AcquireJobRunInfo]
END
GO

CREATE PROCEDURE [Runtime].[atomic_AcquireJobRunInfo] 
	@jobDefinitionId BIGINT,
	@startedOn DATETIME,
	@host VARCHAR(250),
	@schedulerName VARCHAR(250),
	@newJobRunInfoId BIGINT OUTPUT
AS
BEGIN
	DECLARE @lastRunInfoId BIGINT;
	DECLARE @currentTime DATETIME;
	DECLARE @lastStatus BIGINT;
	DECLARE @isRunning BIT;
	DECLARE @failuresInRow INT;
	DECLARE @jobTerminationGracePeriod INT;
	DECLARE @pipeline VARCHAR(100)

	SELECT @currentTime = CurrentDateTime FROM [Configuration].[atomic_CurrentDateTimeView]
	SELECT @jobTerminationGracePeriod = CAST(Value AS INT) FROM [Configuration].[SystemParameter] WHERE [Key]='System.Scheduling.JobTerminationGracePeriodInSeconds'

	SELECT @pipeline = p.Name FROM [Scheduling].[JobDefinition] jd JOIN [Scheduling].[Pipeline] p ON jd.PipelineId = p.Id WHERE jd.Id = @jobDefinitionId

	EXEC [Runtime].[atomic_CleanupExpiredRunInfos] @pipeline


	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION
		UPDATE 
			[Runtime].[JobRunInfo] 
		SET
			[TimesFailedInRow] = items.TimesFailedInRow,
			NextRunOn = items.NextRunOn,
			JobStatusEnumId = items.AbortedStatusId,
			FinishedOn = @currentTime
		FROM
			[Runtime].[JobRunInfo] ujri WITH (TABLOCKX, HOLDLOCK)
			JOIN (
				SELECT 
					jri.Id, 
					(SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Aborted' ) AS AbortedStatusId, 
					ISNULL(jri.TimesFailedInRow, 0) + 1 AS TimesFailedInRow, 
					DATEADD(ss, 
					 CASE 
						WHEN (ISNULL(jri.TimesFailedInRow, 0) + 1) % CASE WHEN jd.MaxRunAttemptsInBulk IS NULL OR jd.MaxRunAttemptsInBulk = 0 THEN 1 ELSE jd.MaxRunAttemptsInBulk END = 0 THEN jd.[DelayBetweenBulkRunAttemptsInSeconds]
						ELSE
						 0
					 END, @currentTime ) AS NextRunOn
				FROM 
					[Runtime].[JobRunInfo] jri
					JOIN [Scheduling].[JobDefinition] jd ON jri.JobDefinitionId = jd.Id
					JOIN (
						SELECT 
							MAX(jri.Id) AS JobRunInfoId
						FROM 
							[Runtime].[JobRunInfo] jri
							JOIN [Scheduling].[JobDefinition] jd ON jd.Id = jri.JobDefinitionId
							JOIN [Scheduling].[Pipeline] p ON jd.PipelineId = p.Id 
						WHERE
							jd.Id = @jobDefinitionId
						GROUP BY
							jd.Id
					) lastRunInfos ON jri.Id = lastRunInfos.JobRunInfoId
				WHERE
					CASE 
						WHEN 
							jri.JobStatusEnumId = (SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Starting' ) 
							AND DATEADD(s, jd.[MaxExecutionPeriodInSeconds] + @jobTerminationGracePeriod, jri.StartedOn) < @currentTime 
						THEN
							1
					ELSE
						0
					END = 1
			) items ON items.Id = ujri.Id


		SELECT 
			@lastRunInfoId = jri.Id, 
			@lastStatus = jri.JobStatusEnumId,
			@isRunning = CASE WHEN jri.JobStatusEnumId = (SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Starting' ) THEN
				1
			ELSE
				0
			END,
			@failuresInRow = jri.[TimesFailedInRow]
		FROM 
			[Runtime].[JobRunInfo] jri WITH (TABLOCKX, HOLDLOCK)
			JOIN [Scheduling].[JobDefinition] jd ON jri.JobDefinitionId = jd.Id
		WHERE jri.Id = (
			SELECT 
				MAX(Id) AS JobRunInfoId
			FROM 
				[Runtime].[JobRunInfo]
			WHERE
				[JobDefinitionId] = @jobDefinitionId
		)

		IF @isRunning = 1
		BEGIN
			COMMIT
			SET TRANSACTION ISOLATION LEVEL READ COMMITTED
			SET @newJobRunInfoId = NULL
			RETURN
		END

		INSERT INTO 
			[Runtime].[JobRunInfo] 
			(ModifiedOn, JobDefinitionId, StartedOn, JobStatusEnumId, Host, SchedulerName, TimesFailedInRow )
		VALUES
			(@currentTime, @jobDefinitionId, @startedOn, (SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Starting' ), @host, @schedulerName, @failuresInRow)

		SET @newJobRunInfoId = SCOPE_IDENTITY()

	COMMIT

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_CleanupExpiredRunInfos]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_CleanupExpiredRunInfos]
END
GO

CREATE PROCEDURE [Runtime].[atomic_CleanupExpiredRunInfos] 
	@pipelineName VARCHAR(100)
AS
BEGIN

	DECLARE @currentTime DATETIME;
	DECLARE @jobTerminationGracePeriod INT;

	SELECT @currentTime = CurrentDateTime FROM [Configuration].[atomic_CurrentDateTimeView]
	SELECT @jobTerminationGracePeriod = CAST(Value AS INT) FROM [Configuration].[SystemParameter] WHERE [Key]='System.Scheduling.JobTerminationGracePeriodInSeconds'

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION

		UPDATE 
			[Runtime].[JobRunInfo] 
		SET
			[TimesFailedInRow] = items.TimesFailedInRow,
			NextRunOn = items.NextRunOn,
			JobStatusEnumId = items.AbortedStatusId,
			FinishedOn = @currentTime
		FROM
			[Runtime].[JobRunInfo] ujri 
			JOIN (
				SELECT 
					jri.Id, 
					(SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Aborted' ) AS AbortedStatusId, 
					ISNULL(jri.TimesFailedInRow, 0) + 1 AS TimesFailedInRow, 
					DATEADD(ss, 
					 CASE 
						WHEN (ISNULL(jri.TimesFailedInRow, 0) + 1) % CASE WHEN jd.MaxRunAttemptsInBulk IS NULL OR jd.MaxRunAttemptsInBulk = 0 THEN 1 ELSE jd.MaxRunAttemptsInBulk END = 0 THEN jd.[DelayBetweenBulkRunAttemptsInSeconds]
						ELSE
						 0
					 END, @currentTime ) AS NextRunOn
				FROM 
					[Runtime].[JobRunInfo] jri
					JOIN [Scheduling].[JobDefinition] jd ON jri.JobDefinitionId = jd.Id
					JOIN (
						SELECT 
							MAX(jri.Id) AS JobRunInfoId
						FROM 
							[Runtime].[JobRunInfo] jri
							JOIN [Scheduling].[JobDefinition] jd ON jd.Id = jri.JobDefinitionId
							JOIN [Scheduling].[Pipeline] p ON jd.PipelineId = p.Id 
						WHERE
							p.Name = @pipelineName
						GROUP BY
							jd.Id
					) lastRunInfos ON jri.Id = lastRunInfos.JobRunInfoId
				WHERE
					CASE 
						WHEN 
							jri.JobStatusEnumId = (SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Starting' ) 
							AND DATEADD(s, jd.[MaxExecutionPeriodInSeconds] + @jobTerminationGracePeriod, jri.StartedOn) < @currentTime 
						THEN
							1
					ELSE
						0
					END = 1
			) items ON items.Id = ujri.Id

	COMMIT TRANSACTION

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_CleanUpTemporaryDocuments]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_CleanUpTemporaryDocuments]
END
GO

CREATE PROCEDURE [Runtime].[atomic_CleanUpTemporaryDocuments]
	@AgeInSeconds INT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE 
		FROM
		[Runtime].[TemporaryDocument]
	WHERE 
		DATEDIFF(ss, ModifiedOn, GETDATE()) > @AgeInSeconds	
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_IsDescendantOf]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_IsDescendantOf]
END
GO

CREATE PROCEDURE [Runtime].[atomic_IsDescendantOf]
		@tableName varchar(100),
		@schemaName varchar(100),
		@treeNodeId int,
		@possibleDescenantTreeNodeId int,
		@res bit output

AS
BEGIN
	DECLARE @fullTableName VARCHAR(200) = '[' + @schemaName + '].[' + @tableName + ']'
	DECLARE @refreshScript NVARCHAR(MAX)

	DECLARE @requiredColumnsCount int
	
	
	SELECT @requiredColumnsCount=count(*)
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_SCHEMA = @schemaName AND
			     TABLE_NAME = @tableName
                 AND (
						(COLUMN_NAME = 'Id' and DATA_TYPE = 'bigint') OR
						(COLUMN_NAME = 'ParentId' and DATA_TYPE = 'bigint')
					)

	if @requiredColumnsCount<>2 
	BEGIN
		RAISERROR
		(N'Provided table :%s.%s has no required columns', 16, 1, @schemaName, @tableName);
	END

	SET @refreshScript = 
	'WITH Descendants (Id, ParentId)
	AS
	(
		-- Anchor member definition
			SELECT [Id],[ParentId]        
			FROM '+@fullTableName+'    
			WHERE ParentId='+CAST(@treeNodeId as nvarchar(20))+'
			UNION ALL
		-- Recursive member definition
			SELECT o.Id,o.ParentId
			FROM '+@fullTableName+' As o
			INNER JOIN Descendants AS d
				ON o.ParentId = d.Id
	)

	select @res=count(*) from Descendants where Id=@possibleDescenantTreeNodeId OPTION(MAXRECURSION 100)
	'
	EXEC sys.sp_executesql @refreshScript, N'@treeNodeId int,@possibleDescenantTreeNodeId int,@res bit output', @treeNodeId,@possibleDescenantTreeNodeId, @res=@res output

	return;

END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_PromoteTemporaryDocumentToTuple]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_PromoteTemporaryDocumentToTuple]
END
GO

CREATE PROCEDURE [Runtime].[atomic_PromoteTemporaryDocumentToTuple]
	@TargetMetaTableSchema NVARCHAR(128),
	@TargetMetaTableName NVARCHAR(128),
	@TargetMetaTableId BIGINT,
	@TargetDataTableSchema NVARCHAR(128),
	@TargetDataTableName NVARCHAR(128),
	@TargetDataTableId BIGINT,
	@DataColumnName NVARCHAR(128),
	@MimeTypeColumnName NVARCHAR(128),
	@ContentLengthColumnName NVARCHAR(128),
	@NameColumnName NVARCHAR(128),
	@TemporaryDocumentIdentifier UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @documentContents VARBINARY(MAX) = 0x

	DECLARE @tempData VARBINARY(MAX)

	DECLARE Contents CURSOR FOR 
	SELECT 
		tfp.Data
	FROM
		[Runtime].[TemporaryDocumentPart] tfp
		JOIN [Runtime].[TemporaryDocument] tf ON tfp.TemporaryDocumentId = tf.Id
	WHERE 
		tf.Identifier = @TemporaryDocumentIdentifier
	ORDER BY tfp.Id

	OPEN Contents
	FETCH NEXT FROM Contents INTO @tempData

	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @documentContents = @documentContents + @tempData
		FETCH NEXT FROM Contents INTO @tempData
	END

	CLOSE Contents
	DEALLOCATE Contents

	BEGIN TRANSACTION

	DECLARE @params NVARCHAR(MAX) = N'@targetDocumentId BIGINT, @temporaryDocumentIdentifier uniqueidentifier'
	DECLARE @sql NVARCHAR(MAX) = N'
	UPDATE ['+ @TargetMetaTableSchema+'].['+@TargetMetaTableName+N']
		SET 
			['+@NameColumnName+'] = source.Name,
			['+@MimeTypeColumnName+'] = source.ContentType,
			['+@ContentLengthColumnName+'] = source.ContentLength
	FROM
		['+ @TargetMetaTableSchema+'].['+@TargetMetaTableName+N'] target
		JOIN [Runtime].[TemporaryDocument] source ON source.Identifier = @temporaryDocumentIdentifier
	WHERE 
		target.ID = @targetDocumentId
	'

	EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId, @temporaryDocumentIdentifier = @TemporaryDocumentIdentifier

	IF(@TargetDataTableId IS NULL)
	BEGIN
		--insert, using PK from meta
		SET @params = N'@targetDocumentId BIGINT, @documentContents VARBINARY(MAX)'
		--update using provided id
		SET @sql = N'
		INSERT INTO ['+ @TargetDataTableSchema+'].['+@TargetDataTableName+N']
		(ID, ['+@DataColumnName+'])
		VALUES
		(
			@targetDocumentId,
			@documentContents
		)
		'
		EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetMetaTableId, @documentContents = @documentContents
	END
	ELSE
	BEGIN
		SET @params = N'@targetDocumentId BIGINT, @documentContents VARBINARY(MAX)'
		--update using provided id
		SET @sql = N'
		UPDATE ['+ @TargetDataTableSchema+'].['+@TargetDataTableName+N']
			SET 
				['+@DataColumnName+'] = @documentContents
		WHERE 
			ID = @targetDocumentId
		'
		EXEC sp_executesql @sql, @params, @targetDocumentId = @TargetDataTableId, @documentContents = @documentContents
	END

	COMMIT
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_RefreshTreeStructure]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_RefreshTreeStructure]
END
GO

CREATE PROCEDURE [Runtime].[atomic_RefreshTreeStructure]
	@tableName varchar(100),
	@schemaName varchar(100)
AS
BEGIN
	DECLARE @fullTableName VARCHAR(205) = '[' + @schemaName + '].[' + @tableName + ']'
	DECLARE @refreshScript NVARCHAR(MAX)
	DECLARE @requiredColumnsCount int

	SELECT @requiredColumnsCount=count(*)
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_SCHEMA = @schemaName AND
			     TABLE_NAME = @tableName
                 AND (
						(COLUMN_NAME = 'Id' and DATA_TYPE = 'bigint') OR
						(COLUMN_NAME = 'ParentId' and DATA_TYPE = 'bigint') OR
						(COLUMN_NAME = 'Path' and DATA_TYPE = 'varchar' and CHARACTER_MAXIMUM_LENGTH >= 900) OR
						(COLUMN_NAME = 'NameSortOrder' and DATA_TYPE = 'bigint') OR
						(COLUMN_NAME = 'DescendantCount' and DATA_TYPE = 'bigint') OR
						(COLUMN_NAME = 'Depth' and DATA_TYPE = 'int') OR
						(COLUMN_NAME = 'Name' and DATA_TYPE = 'nvarchar')						
					 )

	IF @requiredColumnsCount <> 7 
	BEGIN
		RAISERROR
		(N'Provided table :%s.%s has no required columns', 16, 1, @schemaName, @tableName);
	END

	SET @refreshScript = 
'WITH Hierarchy (NodeId, NodeName, ParentId, NodePath, NamePath, Depth)
AS
(
    SELECT 
		Id, 
		Name, 
		ParentId, 
		CAST(''/'' + CAST(Id AS VARCHAR(MAX)) + ''/'' AS VARCHAR(900)), 
		CAST(''/'' + CAST(FORMAT(ISNULL(CustomOrder, 99999), ''00000'') as VARCHAR(5)) + ''|'' +  Name + ''/'' AS NVARCHAR(MAX)),
		CAST(0 as int)
    FROM 
		' + @fullTableName + '
    WHERE 
		ParentId IS NULL        
    UNION ALL
    SELECT 
		NextNode.Id, 
		NextNode.Name, 
		Parent.NodeId, 
		CAST(Parent.NodePath + CAST(NextNode.Id AS VARCHAR(MAX)) + ''/'' AS VARCHAR(900)), 
		CAST(Parent.NamePath  + CAST(FORMAT(ISNULL(CustomOrder, 99999), ''00000'') as VARCHAR(5)) + ''|'' + NextNode.Name + ''/'' AS NVARCHAR(MAX)),
		Parent.Depth+1
     FROM 
		' + @fullTableName + ' AS NextNode
        INNER JOIN Hierarchy AS Parent ON NextNode.ParentId = Parent.NodeId   
),
AllNodes (NodeId, NodePath, NameSortOrder, BranchCount, Depth) 
AS
(
	SELECT
		NodeId, 
		NodePath, 
		ROW_NUMBER() OVER (ORDER BY NamePath) AS NameSortOrder, 
		(SELECT COUNT(*) FROM Hierarchy Sub WHERE Sub.NodePath LIKE Hierarchy.NodePath + ''%'') AS BranchCount,
		Depth
	FROM
		Hierarchy
)
UPDATE
	A 
SET
	A.Path = B.NodePath, 
	A.Depth= B.Depth,
	A.NameSortOrder = B.NameSortOrder, 
	A.DescendantCount = B.BranchCount - 1
FROM 
	' + @fullTableName + ' AS A
	INNER JOIN AllNodes AS B
ON A.Id = B.NodeId
    OPTION(MAXRECURSION 100)';

	EXEC sys.sp_executesql @refreshScript
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Runtime].[atomic_ReleaseJobRunInfo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Runtime].[atomic_ReleaseJobRunInfo]
END
GO

CREATE PROCEDURE [Runtime].[atomic_ReleaseJobRunInfo] 
	@jobDefinitionId BIGINT,
	@jobRunInfoId BIGINT,
	@jobStatusEnumId BIGINT,
	@finishedOn DATETIME,
	@nextRunOn DATETIME,
	@timesFailedInRow INT
AS
BEGIN
	IF( @jobStatusEnumId = (SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE [Name]='Finished') AND EXISTS (SELECT 1 FROM [Scheduling].[JobDefinition] WHERE [Id] = @jobDefinitionId AND [ShouldRunNow]=1))
	BEGIN
		UPDATE [Scheduling].[JobDefinition] SET [ShouldRunNow]=0  WHERE [Id] = @jobDefinitionId AND [ShouldRunNow]=1
	END

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION
		UPDATE 
			[Runtime].[JobRunInfo]
		SET
			[FinishedOn] = @finishedOn,
			[JobStatusEnumId] = @jobStatusEnumId,
			[NextRunOn] = @nextRunOn,
			[TimesFailedInRow] = @timesFailedInRow
		WHERE
			Id = @jobRunInfoId AND JobDefinitionId = @jobDefinitionId AND JobStatusEnumId = (SELECT Id FROM [Scheduling].[JobStatusEnum] WHERE Name='Starting' )
	COMMIT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Scheduling].[atomic_MaintainIndexAndStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Scheduling].[atomic_MaintainIndexAndStatistics]
END
GO

CREATE PROCEDURE [Scheduling].[atomic_MaintainIndexAndStatistics]
AS
BEGIN
	DECLARE @table NVARCHAR(200), @index NVARCHAR(200), @indexType INT, @schema NVARCHAR(100)
	DECLARE CUR_IDX CURSOR FOR
	SELECT SCHEMA_NAME(sta.schema_id) as [Schema], OBJECT_NAME(si.OBJECT_ID), si.name as [index], si.type as [indexType] FROM 
	sys.dm_db_index_physical_stats(DB_ID(),  NULL, NULL, NULL, 'sampled') st
	JOIN sys.indexes si ON si.index_id = st.index_id AND si.object_id = st.object_id
	JOIN sys.tables sta ON sta.object_id = si.object_id
	WHERE avg_fragmentation_in_percent >= 30.0
	DECLARE @sql NVARCHAR(4000)
	OPEN CUR_IDX
	FETCH NEXT FROM CUR_IDX INTO @schema, @table, @index, @indexType
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF(@indexType = 1)
		BEGIN
			SET @sql = 'ALTER INDEX ['+@index +'] ON [' + @schema + '].[' + @table + '] REBUILD WITH (SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON);'
		END
		ELSE
		BEGIN
			SET @sql = 'ALTER INDEX ['+@index +'] ON [' + @schema + '].[' + @table + '] REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON);'
		END
		PRINT @sql
		exec sp_sqlexec @sql
		FETCH NEXT FROM CUR_IDX INTO @schema, @table, @index, @indexType
	END
	CLOSE CUR_IDX
	DEALLOCATE CUR_IDX

	EXEC sp_updatestats
END
GO
------------------
IF EXISTS ( SELECT * FROM   sysobjects 	WHERE  id = object_id(N'[Scheduling].[InsertJobLog]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [Scheduling].[InsertJobLog]
END
GO

CREATE PROCEDURE [Scheduling].[InsertJobLog]
	@ModifiedOn AS DATETIME, @Message AS NVARCHAR(2048), @JobDefinitionId AS BIGINT, @SchedulerName AS VARCHAR(250), @Host AS VARCHAR(250), @Severity AS VARCHAR(50), @Pipeline AS VARCHAR(250), @ExecutionId AS UNIQUEIDENTIFIER
AS
BEGIN
	INSERT INTO [Scheduling].[JobLog]
		([ModifiedOn],[Message], [JobDefinitionId], [SchedulerName], [Host], [JobLogSeverityEnumId], [Pipeline], [ExecutionId])
	SELECT
		@ModifiedOn,
		@Message,
		@JobDefinitionId,
		@SchedulerName,
		@Host,
		CASE @Severity
			WHEN 'Fatal' THEN 1
			WHEN 'Error' THEN 2
			WHEN 'Warn' THEN 3
			WHEN 'Info' THEN 4
			WHEN 'Debug' THEN 5
			WHEN 'Trace' THEN 6
			ELSE 0
		END,
		@Pipeline,
		@ExecutionId
END
GO

------Views------
IF EXISTS(SELECT * FROM sys.views where name = 'UsersNotLogedEvenOnce')
BEGIN
	DROP VIEW [Accounts].[UsersNotLogedEvenOnce]	
END
GO

CREATE VIEW [Accounts].[UsersNotLogedEvenOnce] 
AS
	SELECT e.FirstName, e.LastName, u.LastLoggedOn from Accounts.[User] u
	JOIN Allocation.Employee e ON e.UserId = u.Id
	WHERE LastLoggedOn is NULL 
GO
-----------------
IF EXISTS(SELECT * FROM sys.views where name = 'atomic_CurrentDateTimeView')
BEGIN
	DROP VIEW [Configuration].[atomic_CurrentDateTimeView]
END
GO

CREATE VIEW [Configuration].[atomic_CurrentDateTimeView]
AS
	SELECT GETDATE() AS CurrentDateTime
GO
-----------------
IF EXISTS(SELECT * FROM sys.views where name = 'atomic_LastJobRunInfoView')
BEGIN
	DROP VIEW [Runtime].[atomic_LastJobRunInfoView]
END
GO

CREATE VIEW [Runtime].[atomic_LastJobRunInfoView] 
AS 
	SELECT 
		jd.Id, ri.JobStatusEnumId AS Status, ri.StartedOn, ri.FinishedOn, ri.Id AS JobRunInfoId, p.Name AS Pipeline, ri.TimesFailedInRow
	FROM
		[Scheduling].[JobDefinition] jd
		JOIN [Scheduling].[Pipeline] p ON jd.PipelineId = p.Id
		LEFT JOIN ((
			SELECT 
				MAX(Id) AS Id, 
				[JobDefinitionId] 
			FROM 
				[Runtime].[JobRunInfo]
			GROUP BY 
				[JobDefinitionId]
		) newestEntries JOIN 
			[Runtime].[JobRunInfo] ri ON ri.Id = newestEntries.Id
		) ON ri.JobDefinitionId = jd.Id
GO
-----------------
IF EXISTS(SELECT * FROM sys.views where name = 'ReportedHours')
BEGIN
	DROP VIEW [TimeTracking].[ReportedHours]
END
GO

CREATE VIEW [TimeTracking].[ReportedHours] 
AS
	SELECT 
		tr.Id as TimeReportId, 
		tr.Status as ReportStatus,
		rr.Id as TimeReportRowId,
		rr.ProjectId,
		tr.EmployeeId,
		tr.Year,
		tr.Month,
		e.FirstName,
		e.LastName,
		rr.TaskName,
		rr.ImportSourceId,
		rr.RequestId,
		(SELECT SUM(Hours) FROM TimeTracking.TimeReportDailyEntry WHERE TimeReportRowId = rr.Id) as Hours
	FROM TimeTracking.TimeReportRow rr
	JOIN TimeTracking.TimeReport tr ON tr.Id = rr.TimeReportId
	JOIN Allocation.Employee e ON e.Id = tr.EmployeeId
GO
-----------------
IF EXISTS(SELECT * FROM sys.views where name = 'ReportedTasks')
BEGIN
	DROP VIEW [TimeTracking].[ReportedTasks]
END
GO

CREATE VIEW [TimeTracking].[ReportedTasks] 
AS
	SELECT 
		r.Id as ReportId, 
		rr.Id as RowId, 
		e.FirstName + ' ' + e.LastName as Employee, 
		rr.TaskName, 
		p.ProjectShortName, 
		p.APN,
		p.JiraKey as ProjectJiraKey,
		p.JiraIssueKey as ProjectJiraIssueKey, 
		(SELECT SUM(Hours) FROM TimeTracking.TimeReportDailyEntry re WHERE re.TimeReportRowId = rr.Id) as HOURS
	FROM TimeTracking.TimeReport r
	JOIN Allocation.Employee e ON e.Id = r.EmployeeId
	JOIN TimeTracking.TimeReportRow rr ON rr.TimeReportId = r.Id
	JOIN Allocation.Project p on rr.ProjectId = p.Id
GO
-----------------
IF EXISTS(SELECT * FROM sys.views where name = 'PendingRequests')
BEGIN
	DROP VIEW [Workflows].[PendingRequests]
END
GO

CREATE VIEW [Workflows].[PendingRequests] 
AS
	SELECT  
		DATEDIFF(day, createdOn, GETDATE()) as 'Days Pending', 
		e.FirstName + ' ' + e.LastName as Requestor,
		ae.FirstName + ' ' + ae.LastName as BlockingRequestor,
		'https://dante.intive.org/Workflows/Request/View/' +  CAST(r.id as varchar(max)) as Link
	FROM Workflows.Request r
	JOIN Allocation.Employee e on r.RequestingUserId = e.UserId
	JOIN Workflows.ApprovalGroup g on g.RequestId = r.Id
	JOIN Workflows.Approvals a on a.ApprovalGroupId = g.Id
	JOIN Allocation.Employee ae on ae.UserId = a.UserId
	WHERE r.[Status] = 1 AND a.[Status] = 0
GO