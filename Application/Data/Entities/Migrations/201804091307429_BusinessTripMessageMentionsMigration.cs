namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class BusinessTripMessageMentionsMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Workflow.BusinessTripMessageMentions",
                c => new
                    {
                        BusinessTripMessageId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BusinessTripMessageId, t.EmployeeId })
                .ForeignKey("BusinessTrips.BusinessTripMessage", t => t.BusinessTripMessageId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.BusinessTripMessageId)
                .Index(t => t.EmployeeId);
        }
        
        public override void Down()
        {
            DropForeignKey("Workflow.BusinessTripMessageMentions", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Workflow.BusinessTripMessageMentions", "BusinessTripMessageId", "BusinessTrips.BusinessTripMessage");
            DropIndex("Workflow.BusinessTripMessageMentions", new[] { "EmployeeId" });
            DropIndex("Workflow.BusinessTripMessageMentions", new[] { "BusinessTripMessageId" });
            DropTable("Workflow.BusinessTripMessageMentions");
        }
    }
}
