namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MatchDbAndCodeModelsMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip");
            CreateIndex("BusinessTrips.BusinessTrip", "Id");

            AddForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
            AddForeignKey("BusinessTrips.Arrangement", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripParticipant", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripProjectsMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
            AddForeignKey("Workflows.AdvancedPaymentRequest", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip", "Id");
            AddForeignKey("BusinessTrips.BusinessTripMessage", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
        }
        
        public override void Down()
        {
            throw new Exception("Unrecoverable change");
        }
    }
}
