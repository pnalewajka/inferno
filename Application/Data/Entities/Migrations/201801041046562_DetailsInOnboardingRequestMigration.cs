﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class DetailsInOnboardingRequestMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201801041046562_DetailsInOnboardingRequestMigration.Script.Up.sql");
        }

        public override void Down()
        {
            DropColumn("Workflows.OnboardingRequest", "RelocationPackageDetails");
            DropColumn("Workflows.OnboardingRequest", "ReferralProgramDetails");
        }
    }
}
