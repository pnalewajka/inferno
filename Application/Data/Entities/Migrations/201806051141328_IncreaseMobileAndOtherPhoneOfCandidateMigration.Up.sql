﻿ALTER TABLE Recruitment.Candidate  ALTER COLUMN MobilePhone NVARCHAR (50);
ALTER TABLE Recruitment.Candidate  ALTER COLUMN OtherPhone NVARCHAR (50);