namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveApplicantEntityMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.Applicant", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.Applicant", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant");
            DropForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant");
            DropIndex("Recruitment.Applicant", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.Applicant", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.Candidate", new[] { "ApplicantId" });
            DropIndex("GDPR.DataConsent", new[] { "ApplicantId" });
            DropColumn("Recruitment.Candidate", "ApplicantId");
            DropColumn("GDPR.DataConsent", "ApplicantId");
            DropTable("Recruitment.Applicant");
        }
        
        public override void Down()
        {
            CreateTable(
                "Recruitment.Applicant",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmailAdress = c.String(),
                        PhoneNumber = c.String(),
                        ApplicantType = c.Int(nullable: false),
                        CanBeCandidate = c.Boolean(nullable: false),
                        CanBeRecommendingPerson = c.Boolean(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("GDPR.DataConsent", "ApplicantId", c => c.Long(nullable: false));
            AddColumn("Recruitment.Candidate", "ApplicantId", c => c.Long(nullable: false));
            CreateIndex("GDPR.DataConsent", "ApplicantId");
            CreateIndex("Recruitment.Candidate", "ApplicantId");
            CreateIndex("Recruitment.Applicant", "UserIdModifiedBy");
            CreateIndex("Recruitment.Applicant", "UserIdImpersonatedBy");
            AddForeignKey("GDPR.DataConsent", "ApplicantId", "Recruitment.Applicant", "Id");
            AddForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant", "Id");
            AddForeignKey("Recruitment.Applicant", "UserIdModifiedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.Applicant", "UserIdImpersonatedBy", "Accounts.User", "Id");
        }
    }
}
