﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class FixAcceptanceConditionsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "BusinessTripId" });
            DropPrimaryKey("BusinessTrips.BusinessTripAcceptanceConditions");
            DropColumn("BusinessTrips.BusinessTripAcceptanceConditions", "Id");
            RenameColumn(table: "BusinessTrips.BusinessTripAcceptanceConditions", name: "BusinessTripId", newName: "Id");
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "Id", c => c.Long(nullable: false));
            AddPrimaryKey("BusinessTrips.BusinessTripAcceptanceConditions", "Id");
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "Id");
            AddForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "Id", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "Id", "BusinessTrips.BusinessTrip");
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "Id" });
            DropPrimaryKey("BusinessTrips.BusinessTripAcceptanceConditions");
            AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("BusinessTrips.BusinessTripAcceptanceConditions", "Id");
            RenameColumn(table: "BusinessTrips.BusinessTripAcceptanceConditions", name: "Id", newName: "BusinessTripId");
            AddColumn("BusinessTrips.BusinessTripAcceptanceConditions", "Id", c => c.Long(nullable: false, identity: true));
            CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId");
            AddForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id");
        }
    }
}

