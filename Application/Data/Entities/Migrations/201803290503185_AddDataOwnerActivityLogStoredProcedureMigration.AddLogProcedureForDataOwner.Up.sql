﻿SET XACT_ABORT ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogDataOwnerDataActivity]'))
   DROP PROCEDURE [GDPR].[LogDataOwnerDataActivity]
GO

CREATE PROCEDURE [GDPR].[LogDataOwnerDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @currentDate DateTime = GETDATE()

	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@identity, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

