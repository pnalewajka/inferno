﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class CompanyAdvancedPaymentDefaultsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Organization.CompanyAllowedAdvancedPaymentCurrencies",
                c => new
                    {
                        CompanyId = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.CurrencyId })
                .ForeignKey("Dictionaries.Company", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("BusinessTrips.Currency", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.CurrencyId);
            
            AddColumn("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId", c => c.Long(nullable: false));
            AddColumn("Dictionaries.Company", "DefaultAdvancedPaymentDailyAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802221449063_CompanyAdvancedPaymentDefaultsMigration.SetDefaultAdvancedPaymentCurrency.Up.sql");
            AddForeignKey("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId", "BusinessTrips.Currency", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CompanyId", "Dictionaries.Company");
            DropIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", new[] { "CurrencyId" });
            DropIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", new[] { "CompanyId" });
            DropIndex("Dictionaries.Company", new[] { "DefaultAdvancedPaymentCurrencyId" });
            DropColumn("Dictionaries.Company", "DefaultAdvancedPaymentDailyAmount");
            DropColumn("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId");
            DropTable("Organization.CompanyAllowedAdvancedPaymentCurrencies");
        }
    }
}

