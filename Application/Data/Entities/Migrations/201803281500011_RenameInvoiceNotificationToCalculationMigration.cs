﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RenameInvoiceNotificationToCalculationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Compensation.InvoiceNotification", newName: "InvoiceCalculation");
            AddColumn("Compensation.InvoiceCalculation", "WasEmployeeNotified", c => c.Boolean(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803281500011_RenameInvoiceNotificationToCalculationMigration.RemoveOldReport.Up.sql");
        }
        
        public override void Down()
        {
            DropColumn("Compensation.InvoiceCalculation", "WasEmployeeNotified");
            RenameTable(name: "Compensation.InvoiceCalculation", newName: "InvoiceNotification");
        }
    }
}

