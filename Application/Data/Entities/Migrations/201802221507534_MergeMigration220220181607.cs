namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration220220181607 : DbMigration
    {
        public override void Up()
        {
            //MoveTable(name: "GDPR.DataConsentDocumentContent", newSchema: "Recruitment");
            //MoveTable(name: "GDPR.DataConsentDocument", newSchema: "Recruitment");
            //MoveTable(name: "GDPR.DataConsent", newSchema: "Recruitment");
            //DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            //DropForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest");
            //DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "ReinvoiceCurrencyId" });
            //DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "MaxCostsCurrencyId" });
            //DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "BusinessTrip_Id" });
            //DropIndex("Recruitment.DataConsent", new[] { "DataOwnerId" });
            //RenameColumn(table: "BusinessTrips.BusinessTripAcceptanceConditions", name: "BusinessTrip_Id", newName: "BusinessTripId");
            //AddColumn("Allocation.Project", "ReinvoicingToCustomer", c => c.Boolean(nullable: false));
            //AddColumn("Allocation.Project", "ReinvoiceCurrencyId", c => c.Long());
            //AddColumn("Allocation.Project", "CostApprovalPolicy", c => c.Int(nullable: false));
            //AddColumn("Allocation.Project", "MaxCostsCurrencyId", c => c.Long());
            //AddColumn("Allocation.Project", "MaxHotelCosts", c => c.Decimal(precision: 18, scale: 2));
            //AddColumn("Allocation.Project", "MaxTotalFlightsCosts", c => c.Decimal(precision: 18, scale: 2));
            //AddColumn("Allocation.Project", "MaxOtherTravelCosts", c => c.Decimal(precision: 18, scale: 2));
            //AddColumn("Recruitment.DataConsent", "WhoRecommendedApplicantId", c => c.Long(nullable: false));
            //AddColumn("Recruitment.DataConsent", "Comment", c => c.String());
            //AddColumn("Recruitment.DataConsent", "ApplicationOriginId", c => c.Long(nullable: false));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId", c => c.Long());
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId", c => c.Long());
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxHotelCosts", c => c.Decimal(precision: 18, scale: 2));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxTotalFlightsCosts", c => c.Decimal(precision: 18, scale: 2));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxOtherTravelCosts", c => c.Decimal(precision: 18, scale: 2));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId", c => c.Long(nullable: false));
            //CreateIndex("Allocation.Project", "ReinvoiceCurrencyId");
            //CreateIndex("Allocation.Project", "MaxCostsCurrencyId");
            //CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId");
            //CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId");
            //CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId");
            //CreateIndex("Recruitment.DataConsent", "WhoRecommendedApplicantId");
            //CreateIndex("Recruitment.DataConsent", "ApplicationOriginId");
            //AddForeignKey("Allocation.Project", "MaxCostsCurrencyId", "BusinessTrips.Currency", "Id");
            //AddForeignKey("Allocation.Project", "ReinvoiceCurrencyId", "BusinessTrips.Currency", "Id");
            //AddForeignKey("Recruitment.DataConsent", "ApplicationOriginId", "Recruitment.ApplicationOrigin", "Id");
            //AddForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant", "Id");
            //AddForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest", "Id", cascadeDelete: true);
            //DropColumn("Recruitment.DataConsent", "DataOwnerId");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.DataConsent", "DataOwnerId", c => c.Long(nullable: false));
            //DropForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest");
            //DropForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant");
            //DropForeignKey("Recruitment.DataConsent", "ApplicationOriginId", "Recruitment.ApplicationOrigin");
            //DropForeignKey("Allocation.Project", "ReinvoiceCurrencyId", "BusinessTrips.Currency");
            //DropForeignKey("Allocation.Project", "MaxCostsCurrencyId", "BusinessTrips.Currency");
            //DropIndex("Recruitment.DataConsent", new[] { "ApplicationOriginId" });
            //DropIndex("Recruitment.DataConsent", new[] { "WhoRecommendedApplicantId" });
            //DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "MaxCostsCurrencyId" });
            //DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "ReinvoiceCurrencyId" });
            //DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "BusinessTripId" });
            //DropIndex("Allocation.Project", new[] { "MaxCostsCurrencyId" });
            //DropIndex("Allocation.Project", new[] { "ReinvoiceCurrencyId" });
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTripId", c => c.Long());
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxOtherTravelCosts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxTotalFlightsCosts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxHotelCosts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId", c => c.Long(nullable: false));
            //AlterColumn("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId", c => c.Long(nullable: false));
            //DropColumn("Recruitment.DataConsent", "ApplicationOriginId");
            //DropColumn("Recruitment.DataConsent", "Comment");
            //DropColumn("Recruitment.DataConsent", "WhoRecommendedApplicantId");
            //DropColumn("Allocation.Project", "MaxOtherTravelCosts");
            //DropColumn("Allocation.Project", "MaxTotalFlightsCosts");
            //DropColumn("Allocation.Project", "MaxHotelCosts");
            //DropColumn("Allocation.Project", "MaxCostsCurrencyId");
            //DropColumn("Allocation.Project", "CostApprovalPolicy");
            //DropColumn("Allocation.Project", "ReinvoiceCurrencyId");
            //DropColumn("Allocation.Project", "ReinvoicingToCustomer");
            //RenameColumn(table: "BusinessTrips.BusinessTripAcceptanceConditions", name: "BusinessTripId", newName: "BusinessTrip_Id");
            //CreateIndex("Recruitment.DataConsent", "DataOwnerId");
            //CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id");
            //CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId");
            //CreateIndex("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId");
            //AddForeignKey("BusinessTrips.BusinessTrip", "Id", "Workflows.BusinessTripRequest", "Id");
            //AddForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id");
            //MoveTable(name: "Recruitment.DataConsent", newSchema: "GDPR");
            //MoveTable(name: "Recruitment.DataConsentDocument", newSchema: "GDPR");
            //MoveTable(name: "Recruitment.DataConsentDocumentContent", newSchema: "GDPR");
        }
    }
}
