namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class ChangeInAssetsRequestActionsetMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201712210800416_ChangeInAssetsRequestActionset-Migration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
