namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RenameColumnsOnRelatedEntitiesMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "FinalEmploymentSourceId", newName: "EmploymentSourceId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "FinalLocationId", newName: "LocationId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "FinalLongTermContractTypeId", newName: "LongTermContractTypeId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "FinalPositionId", newName: "PositionId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "FinalTrialContractTypeId", newName: "TrialContractTypeId");
            RenameColumn(table: "Recruitment.RecruitmentProcessOffer", name: "OfferLocationId", newName: "LocationId");
            RenameColumn(table: "Recruitment.RecruitmentProcessOffer", name: "OfferPositionId", newName: "PositionId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_FinalPositionId", newName: "IX_PositionId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_FinalTrialContractTypeId", newName: "IX_TrialContractTypeId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_FinalLongTermContractTypeId", newName: "IX_LongTermContractTypeId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_FinalLocationId", newName: "IX_LocationId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_FinalEmploymentSourceId", newName: "IX_EmploymentSourceId");
            RenameIndex(table: "Recruitment.RecruitmentProcessOffer", name: "IX_OfferPositionId", newName: "IX_PositionId");
            RenameIndex(table: "Recruitment.RecruitmentProcessOffer", name: "IX_OfferLocationId", newName: "IX_LocationId");

            RenameColumn("Recruitment.RecruitmentProcessFinal", "FinalTrialSalary", "TrialSalary");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "FinalLongTermSalary", "LongTermSalary");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "FinalStartDate", "StartDate");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "FinalSignedDate", "SignedDate");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "FinalComment", "Comment");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "OfferContractType", "ContractType");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "OfferSalary", "Salary");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "OfferStartDate", "StartDate");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "OfferComment", "Comment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningGeneralOpinion", "GeneralOpinion");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningMotivation", "Motivation");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningWorkplaceExpectations", "WorkplaceExpectations");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningSkills", "Skills");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningLanguageEnglish", "LanguageEnglish");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningLanguageGerman", "LanguageGerman");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningLanguageOther", "LanguageOther");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningContractExpectations", "ContractExpectations");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningAvailability", "Availability");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningOtherProcesses", "OtherProcesses");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningCounterOfferCriteria", "CounterOfferCriteria");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningRelocationCanDo", "RelocationCanDo");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningRelocationComment", "RelocationComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningBusinessTripsCanDo", "BusinessTripsCanDo");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningBusinessTripsComment", "BusinessTripsComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningEveningWorkCanDo", "EveningWorkCanDo");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningEveningWorkComment", "EveningWorkComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningWorkPermitNeeded", "WorkPermitNeeded");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ScreeningWorkPermitComment", "WorkPermitComment");
        }

        public override void Down()
        {
            RenameColumn("Recruitment.RecruitmentProcessFinal", "TrialSalary", "FinalTrialSalary");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "LongTermSalary", "FinalLongTermSalary");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "StartDate", "FinalStartDate");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "SignedDate", "FinalSignedDate");
            RenameColumn("Recruitment.RecruitmentProcessFinal", "Comment", "FinalComment");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "ContractType", "OfferContractType");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "Salary", "OfferSalary");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "StartDate", "OfferStartDate");
            RenameColumn("Recruitment.RecruitmentProcessOffer", "Comment", "OfferComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "GeneralOpinion", "ScreeningGeneralOpinion");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "Motivation", "ScreeningMotivation");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "WorkplaceExpectations", "ScreeningWorkplaceExpectations");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "Skills", "ScreeningSkills");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "LanguageEnglish", "ScreeningLanguageEnglish");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "LanguageGerman", "ScreeningLanguageGerman");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "LanguageOther", "ScreeningLanguageOther");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "ContractExpectations", "ScreeningContractExpectations");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "Availability", "ScreeningAvailability");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "OtherProcesses", "ScreeningOtherProcesses");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "CounterOfferCriteria", "ScreeningCounterOfferCriteria");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "RelocationCanDo", "ScreeningRelocationCanDo");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "RelocationComment", "ScreeningRelocationComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "BusinessTripsCanDo", "ScreeningBusinessTripsCanDo");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "BusinessTripsComment", "ScreeningBusinessTripsComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "EveningWorkCanDo", "ScreeningEveningWorkCanDo");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "EveningWorkComment", "ScreeningEveningWorkComment");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "WorkPermitNeeded", "ScreeningWorkPermitNeeded");
            RenameColumn("Recruitment.RecruitmentProcessScreening", "WorkPermitComment", "ScreeningWorkPermitComment");
            RenameIndex(table: "Recruitment.RecruitmentProcessOffer", name: "IX_LocationId", newName: "IX_OfferLocationId");
            RenameIndex(table: "Recruitment.RecruitmentProcessOffer", name: "IX_PositionId", newName: "IX_OfferPositionId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_EmploymentSourceId", newName: "IX_FinalEmploymentSourceId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_LocationId", newName: "IX_FinalLocationId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_LongTermContractTypeId", newName: "IX_FinalLongTermContractTypeId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_TrialContractTypeId", newName: "IX_FinalTrialContractTypeId");
            RenameIndex(table: "Recruitment.RecruitmentProcessFinal", name: "IX_PositionId", newName: "IX_FinalPositionId");
            RenameColumn(table: "Recruitment.RecruitmentProcessOffer", name: "PositionId", newName: "OfferPositionId");
            RenameColumn(table: "Recruitment.RecruitmentProcessOffer", name: "LocationId", newName: "OfferLocationId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "TrialContractTypeId", newName: "FinalTrialContractTypeId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "PositionId", newName: "FinalPositionId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "LongTermContractTypeId", newName: "FinalLongTermContractTypeId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "LocationId", newName: "FinalLocationId");
            RenameColumn(table: "Recruitment.RecruitmentProcessFinal", name: "EmploymentSourceId", newName: "FinalEmploymentSourceId");
        }
    }
}
