﻿SET XACT_ABORT ON
GO

SET IDENTITY_INSERT [Allocation].[ProjectSetup] ON

INSERT INTO [Allocation].[ProjectSetup]
    (Id, ClientShortName, ProjectShortName, GenericName, Region, ProjectType, ProjectFeatures, OrgUnitId, ProjectManagerId,
    SalesAccountManagerId, SupervisorManagerId, ProjectStatus, CalendarId, Color, PictureId, IsAvailableForSalesPresentation,
    CurrentPhase, Disclosures, HasClientReferences, CustomerSizeId, BusinessCase, ReinvoicingToCustomer, ReinvoiceCurrencyId,
    CostApprovalPolicy, MaxCostsCurrencyId, MaxHotelCosts, MaxTotalFlightsCosts, MaxOtherTravelCosts, InvoicingAlgorithm)
SELECT Id, ClientShortName, ProjectShortName, GenericName, Region, ProjectType, ProjectFeatures, OrgUnitId, ProjectManagerId,
    SalesAccountManagerId, SupervisorManagerId, ProjectStatus, CalendarId, Color, PictureId, IsAvailableForSalesPresentation,
    CurrentPhase, Disclosures, HasClientReferences, CustomerSizeId, BusinessCase, ReinvoicingToCustomer, ReinvoiceCurrencyId,
    CostApprovalPolicy, MaxCostsCurrencyId, MaxHotelCosts, MaxTotalFlightsCosts, MaxOtherTravelCosts, InvoicingAlgorithm
    FROM Allocation.Project

SET IDENTITY_INSERT [Allocation].[ProjectSetup] OFF

UPDATE Allocation.Project
    SET ProjectSetupId = Id, IsMainProject = 1
