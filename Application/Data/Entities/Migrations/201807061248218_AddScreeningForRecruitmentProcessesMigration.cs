﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AddScreeningForRecruitmentProcessesMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807061248218_AddScreeningForRecruitmentProcessesMigration.AddScreenningForRecruitmentProcesses.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807061248218_AddScreeningForRecruitmentProcessesMigration.AddScreenningForRecruitmentProcesses.Down.sql");
        }
    }
}

