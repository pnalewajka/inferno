namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AlterGDPRStoredProceduresMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("GDPR.DataOwner", "IsPotentialDuplicate", c => c.Boolean(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.201803270903225_AlterGDPRStoredProceduresMigration.AlterStoredProcedures.Up.sql");
        }
        
        public override void Down()
        {
            DropColumn("GDPR.DataOwner", "IsPotentialDuplicate");
        }
    }
}
