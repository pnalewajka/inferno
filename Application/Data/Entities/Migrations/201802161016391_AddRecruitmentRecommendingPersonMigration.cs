namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRecruitmentRecommendingPersonMigration : DbMigration
    {
        public override void Up()
        {          
            CreateTable(
                "Recruitment.RecommendingPerson",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmailAdress = c.String(),
                        PhoneNumber = c.String(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);                        
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecommendingPerson", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPerson", "UserIdImpersonatedBy", "Accounts.User");
            DropIndex("Recruitment.RecommendingPerson", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.RecommendingPerson", new[] { "UserIdImpersonatedBy" });
            DropTable("Recruitment.RecommendingPerson");
        }
    }
}
