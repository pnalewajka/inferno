﻿UPDATE [Workflows].[BusinessTripSettlementRequest] SET [PreferredDistanceUnit] = 0
WHERE [Id] IN (
	SELECT R.Id FROM [Workflows].[BusinessTripSettlementRequest] R
	WHERE
	[SimplifiedPrivateVehicleMileage] IS NOT NULL
	OR EXISTS (SELECT * FROM [Workflows].[BusinessTripSettlementRequestVehicleMileage] M WHERE M.Id = R.Id))