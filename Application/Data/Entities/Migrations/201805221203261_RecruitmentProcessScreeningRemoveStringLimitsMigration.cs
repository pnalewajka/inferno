namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecruitmentProcessScreeningRemoveStringLimitsMigration : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN ScreeningGeneralOpinion VARCHAR (500);"
                + "ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN ScreeningMotivation VARCHAR(500);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningWorkplaceExpectations VARCHAR(500);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningSkills VARCHAR(500);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningLanguageEnglish VARCHAR(300);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningLanguageGerman VARCHAR(250);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningLanguageOther VARCHAR(250);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningContractExpectations VARCHAR(500);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningAvailability VARCHAR(400);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningOtherProcesses VARCHAR(400);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningCounterOfferCriteria VARCHAR(400);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningRelocationComment VARCHAR(400);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningBusinessTripsComment VARCHAR(250);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningEveningWorkComment VARCHAR(500);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningWorkPermitComment VARCHAR(250);");
        }

        public override void Down()
        {
            Sql("ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningGeneralOpinion VARCHAR (255);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningMotivation VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningWorkplaceExpectations VARCHAR(255);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningSkills VARCHAR(255);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningLanguageEnglish VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningLanguageGerman VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningLanguageOther VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningContractExpectations VARCHAR(255);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningAvailability VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningOtherProcesses VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningCounterOfferCriteria VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningRelocationComment VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningBusinessTripsComment VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningEveningWorkComment VARCHAR(50);"
                + "ALTER TABLE Recruitment.RecruitmentProcess ALTER COLUMN ScreeningWorkPermitComment VARCHAR(50);");
        }
    }
}
