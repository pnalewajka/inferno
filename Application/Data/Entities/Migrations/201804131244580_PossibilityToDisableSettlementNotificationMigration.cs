﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class PossibilityToDisableSettlementNotificationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.ContractType", "NotifyMissingBusinessTripSettlement", c => c.Boolean(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804131244580_PossibilityToDisableSettlementNotificationMigration.EnableNotificationByDefault.Up.sql");
        }
        
        public override void Down()
        {
            DropColumn("TimeTracking.ContractType", "NotifyMissingBusinessTripSettlement");
        }
    }
}

