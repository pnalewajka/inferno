namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveIrrelevantIndicesMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Allocation.Project", "IX_ProjectNamePerClient");
            DropIndex("Allocation.ProjectSetup", "IX_ProjectNamePerClient");
        }
        
        public override void Down()
        {
        }
    }
}
