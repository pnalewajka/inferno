namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration140320180942 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Recruitment.CandidateNotes",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            CandidateId = c.Long(nullable: false),
            //            NoteType = c.Int(nullable: false),
            //            NoteVisibility = c.Int(nullable: false),
            //            Content = c.String(maxLength: 1024),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.Candidate", t => t.CandidateId)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .Index(t => t.CandidateId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.RecommendingPersonNotes",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            RecommendingPersonId = c.Long(nullable: false),
            //            NoteType = c.Int(nullable: false),
            //            NoteVisibility = c.Int(nullable: false),
            //            Content = c.String(maxLength: 1024),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .ForeignKey("Recruitment.RecommendingPerson", t => t.RecommendingPersonId)
            //    .Index(t => t.RecommendingPersonId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.JobOpeningNotes",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            JobOpeningId = c.Long(nullable: false),
            //            NoteType = c.Int(nullable: false),
            //            NoteVisibility = c.Int(nullable: false),
            //            Content = c.String(maxLength: 1024),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .Index(t => t.JobOpeningId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.RecruitmentProcessNotes",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            RecruitmentProcessId = c.Long(nullable: false),
            //            NoteType = c.Int(nullable: false),
            //            NoteVisibility = c.Int(nullable: false),
            //            Content = c.String(maxLength: 1024),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .ForeignKey("Recruitment.RecruitmentProcess", t => t.RecruitmentProcessId)
            //    .Index(t => t.RecruitmentProcessId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.RecruitmentProcessNotes", "RecruitmentProcessId", "Recruitment.RecruitmentProcess");
            //DropForeignKey("Recruitment.RecruitmentProcessNotes", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecruitmentProcessNotes", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecruitmentProcessNotes", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecruitmentProcessNotes", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobOpeningNotes", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobOpeningNotes", "JobOpeningId", "Recruitment.JobOpening");
            //DropForeignKey("Recruitment.JobOpeningNotes", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobOpeningNotes", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.JobOpeningNotes", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateNotes", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateNotes", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateNotes", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateNotes", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonNotes", "RecommendingPersonId", "Recruitment.RecommendingPerson");
            //DropForeignKey("Recruitment.RecommendingPersonNotes", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonNotes", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonNotes", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.RecommendingPersonNotes", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateNotes", "CandidateId", "Recruitment.Candidate");
            //DropIndex("Recruitment.RecruitmentProcessNotes", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.RecruitmentProcessNotes", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.RecruitmentProcessNotes", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.RecruitmentProcessNotes", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.RecruitmentProcessNotes", new[] { "RecruitmentProcessId" });
            //DropIndex("Recruitment.JobOpeningNotes", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.JobOpeningNotes", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.JobOpeningNotes", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.JobOpeningNotes", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.JobOpeningNotes", new[] { "JobOpeningId" });
            //DropIndex("Recruitment.RecommendingPersonNotes", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.RecommendingPersonNotes", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.RecommendingPersonNotes", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.RecommendingPersonNotes", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.RecommendingPersonNotes", new[] { "RecommendingPersonId" });
            //DropIndex("Recruitment.CandidateNotes", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.CandidateNotes", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.CandidateNotes", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.CandidateNotes", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.CandidateNotes", new[] { "CandidateId" });
            //DropTable("Recruitment.RecruitmentProcessNotes");
            //DropTable("Recruitment.JobOpeningNotes");
            //DropTable("Recruitment.RecommendingPersonNotes");
            //DropTable("Recruitment.CandidateNotes");
        }
    }
}
