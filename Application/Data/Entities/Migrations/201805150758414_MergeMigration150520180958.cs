namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration150520180958 : DbMigration
    {
        public override void Up()
        {
            //AlterColumn("Recruitment.JobOpening", "AgencyEmploymentMode", c => c.Int());
            //DropColumn("Recruitment.RecruitmentProcess", "HiredOn");
            //DropColumn("Recruitment.JobOpening", "IsAgencyEmploymentAllowed");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.JobOpening", "IsAgencyEmploymentAllowed", c => c.Boolean(nullable: false));
            //AddColumn("Recruitment.RecruitmentProcess", "HiredOn", c => c.DateTime());
            //AlterColumn("Recruitment.JobOpening", "AgencyEmploymentMode", c => c.Int(nullable: false));
        }
    }
}
