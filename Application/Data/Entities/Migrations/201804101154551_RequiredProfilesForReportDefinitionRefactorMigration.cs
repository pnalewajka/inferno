namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RequiredProfilesForReportDefinitionRefactorMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Reporting.ReportDefinitionRequiredProfiles", newName: "ReportDefinitionAllowedForProfiles");
            RenameColumn(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "RequiredProfileId", newName: "AllowedForProfileId");
            RenameIndex(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "IX_RequiredProfileId", newName: "IX_AllowedForProfileId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "IX_AllowedForProfileId", newName: "IX_RequiredProfileId");
            RenameColumn(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "AllowedForProfileId", newName: "RequiredProfileId");
            RenameTable(name: "Reporting.ReportDefinitionAllowedForProfiles", newName: "ReportDefinitionRequiredProfiles");
        }
    }
}
