﻿CREATE VIEW Allocation.FullProject AS
    SELECT
        p.Id,
        IIF(p.IsMainProject = 1, s.ProjectShortName, CONCAT(s.ProjectShortName, ' / ' + p.SubProjectShortName)) AS 'ProjectShortName',
        s.ClientShortName
    FROM
        Allocation.Project p
    JOIN Allocation.ProjectSetup s ON s.Id = p.ProjectSetupId