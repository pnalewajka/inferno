namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterBusinessTripAndBusinessTripRequestMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("BusinessTrips.BusinessTrip", "DepartureCityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripRequest", "DepartureCityId", "Dictionaries.City");
            DropIndex("BusinessTrips.BusinessTrip", new[] { "DepartureCityId" });
            DropIndex("Workflows.BusinessTripRequest", new[] { "DepartureCityId" });
            CreateTable(
                "BusinessTrips.BusinessTripDepartureCityMap",
                c => new
                    {
                        BusinessTripId = c.Long(nullable: false),
                        CityId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BusinessTripId, t.CityId })
                .ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTripId, cascadeDelete: true)
                .ForeignKey("Dictionaries.City", t => t.CityId, cascadeDelete: true)
                .Index(t => t.BusinessTripId)
                .Index(t => t.CityId);
            
            CreateTable(
                "Workflows.BusinessTripRequestDepartureCityMap",
                c => new
                    {
                        BusinessTripRequestId = c.Long(nullable: false),
                        CityId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BusinessTripRequestId, t.CityId })
                .ForeignKey("Workflows.BusinessTripRequest", t => t.BusinessTripRequestId, cascadeDelete: true)
                .ForeignKey("Dictionaries.City", t => t.CityId, cascadeDelete: true)
                .Index(t => t.BusinessTripRequestId)
                .Index(t => t.CityId);
            
            DropColumn("BusinessTrips.BusinessTrip", "DepartureCityId");
            DropColumn("Workflows.BusinessTripRequest", "DepartureCityId");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripRequest", "DepartureCityId", c => c.Long(nullable: false));
            AddColumn("BusinessTrips.BusinessTrip", "DepartureCityId", c => c.Long(nullable: false));
            DropForeignKey("Workflows.BusinessTripRequestDepartureCityMap", "CityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripRequestDepartureCityMap", "BusinessTripRequestId", "Workflows.BusinessTripRequest");
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "CityId", "Dictionaries.City");
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropIndex("Workflows.BusinessTripRequestDepartureCityMap", new[] { "CityId" });
            DropIndex("Workflows.BusinessTripRequestDepartureCityMap", new[] { "BusinessTripRequestId" });
            DropIndex("BusinessTrips.BusinessTripDepartureCityMap", new[] { "CityId" });
            DropIndex("BusinessTrips.BusinessTripDepartureCityMap", new[] { "BusinessTripId" });
            DropTable("Workflows.BusinessTripRequestDepartureCityMap");
            DropTable("BusinessTrips.BusinessTripDepartureCityMap");
            CreateIndex("Workflows.BusinessTripRequest", "DepartureCityId");
            CreateIndex("BusinessTrips.BusinessTrip", "DepartureCityId");
            AddForeignKey("Workflows.BusinessTripRequest", "DepartureCityId", "Dictionaries.City", "Id");
            AddForeignKey("BusinessTrips.BusinessTrip", "DepartureCityId", "Dictionaries.City", "Id");
        }
    }
}
