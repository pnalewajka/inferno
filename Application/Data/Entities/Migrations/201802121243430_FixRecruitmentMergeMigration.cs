namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixRecruitmentMergeMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("BusinessTrips.BusinessTrip", "DepartureCityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripRequest", "DepartureCityId", "Dictionaries.City");
            DropIndex("BusinessTrips.BusinessTrip", new[] { "DepartureCityId" });
            DropIndex("Workflows.BusinessTripRequest", new[] { "DepartureCityId" });
            CreateTable(
                "Recruitment.Applicant",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmailAdress = c.String(),
                        PhoneNumber = c.String(),
                        ApplicantType = c.Int(nullable: false),
                        CanBeCandidate = c.Boolean(nullable: false),
                        CanBeRecommendingPerson = c.Boolean(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "BusinessTrips.BusinessTripAcceptanceConditions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReinvoicingToCustomer = c.Boolean(nullable: false),
                        ReinvoiceCurrencyId = c.Long(nullable: false),
                        CostApprovalPolicy = c.Int(nullable: false),
                        MaxCostsCurrencyId = c.Long(nullable: false),
                        MaxHotelCosts = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxTotalFlightsCosts = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxOtherTravelCosts = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BusinessTrip_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("BusinessTrips.BusinessTrip", t => t.BusinessTrip_Id)
                .ForeignKey("BusinessTrips.Currency", t => t.MaxCostsCurrencyId)
                .ForeignKey("BusinessTrips.Currency", t => t.ReinvoiceCurrencyId)
                .Index(t => t.ReinvoiceCurrencyId)
                .Index(t => t.MaxCostsCurrencyId)
                .Index(t => t.BusinessTrip_Id);
            
            CreateTable(
                "Recruitment.ContactRecord",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CandidateId = c.Long(nullable: false),
                        ContactType = c.Int(nullable: false),
                        ContactedOn = c.DateTime(nullable: false),
                        ContactedById = c.Long(nullable: false),
                        CandidateReaction = c.Int(nullable: false),
                        Comment = c.String(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId)
                .ForeignKey("Allocation.Employee", t => t.ContactedById)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.CandidateId)
                .Index(t => t.ContactedById)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.Candidate",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        ContactRestriction = c.Int(nullable: false),
                        MobilePhone = c.String(),
                        OtherPhone = c.String(),
                        EmailAddreess = c.String(),
                        EmailAddreess2 = c.String(),
                        SkypeLogin = c.String(),
                        NextFollowUpDate = c.DateTime(),
                        EmployeeStatus = c.Int(nullable: false),
                        CanRelocate = c.Boolean(nullable: false),
                        RelocateDetails = c.String(),
                        CvFullText = c.String(),
                        ApplicantId = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Applicant", t => t.ApplicantId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.ApplicantId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.CandidateLanguage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CandidateId = c.Long(nullable: false),
                        LanguageId = c.Long(nullable: false),
                        Level = c.Int(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Dictionaries.Language", t => t.LanguageId)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.CandidateId)
                .Index(t => t.LanguageId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.JobOpening",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PositionName = c.String(),
                        CustomerId = c.Long(nullable: false),
                        ServiceLineId = c.Long(nullable: false),
                        BusinessLineId = c.Long(nullable: false),
                        AcceptingEmployeeId = c.Long(nullable: false),
                        Priority = c.Int(nullable: false),
                        HiringMode = c.Int(nullable: false),
                        RoleOpenedOn = c.DateTime(nullable: false),
                        VacancyLevel1 = c.Int(nullable: false),
                        VacancyLevel2 = c.Int(nullable: false),
                        VacancyLevel3 = c.Int(nullable: false),
                        VacancyLevel4 = c.Int(nullable: false),
                        VacancyLevel5 = c.Int(nullable: false),
                        VacancyLevelNotApplicable = c.Int(nullable: false),
                        IsTechnicalVerificationRequired = c.Boolean(nullable: false),
                        IsIntiveResumeRequired = c.Boolean(nullable: false),
                        IsPolishSpeakerRequired = c.Boolean(nullable: false),
                        NoticePeriod = c.String(),
                        SalaryRate = c.String(),
                        EnglishLevel = c.Int(nullable: false),
                        OtherLanguages = c.String(),
                        IsAgencyEmploymentAllowed = c.Boolean(nullable: false),
                        AgencyEmploymentMode = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Allocation.Employee", t => t.AcceptingEmployeeId)
                .ForeignKey("Organization.OrgUnit", t => t.BusinessLineId)
                .ForeignKey("Sales.Customer", t => t.CustomerId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Organization.OrgUnit", t => t.ServiceLineId)
                .Index(t => t.CustomerId)
                .Index(t => t.ServiceLineId)
                .Index(t => t.BusinessLineId)
                .Index(t => t.AcceptingEmployeeId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Sales.Customer",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LegalName = c.String(maxLength: 250),
                        ShortName = c.String(maxLength: 50),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Recruitment.ProjectDescription",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        JobOpeningId = c.Long(nullable: false),
                        AboutClient = c.String(),
                        SpecificDuties = c.String(),
                        ToolsAndEnvironment = c.String(),
                        AboutTeam = c.String(),
                        ProjectTimeline = c.String(),
                        Methodology = c.String(),
                        RequiredSkills = c.String(),
                        AdditionalSkills = c.String(),
                        RecruitmentDetails = c.String(),
                        IsTravelRequired = c.Boolean(nullable: false),
                        TravelAndAccomodation = c.String(),
                        CanWorkRemotely = c.Boolean(nullable: false),
                        RemoteWorkArrangements = c.String(),
                        Comments = c.String(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.JobOpeningId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.DataConsentDocumentContent",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.DataConsentDocument", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "Recruitment.DataConsentDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataConsentId = c.Long(nullable: false),
                        Name = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 250, unicode: false),
                        ContentLength = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.DataConsent", t => t.DataConsentId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.DataConsentId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.DataConsent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        ApplicantId = c.Long(nullable: false),
                        ExpiresOn = c.DateTime(),
                        DataAdministratorId = c.Long(nullable: false),
                        Scope = c.String(maxLength: 250),
                        WhoRecommendedApplicantId = c.Long(nullable: false),
                        Comment = c.String(),
                        DataOriginId = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Applicant", t => t.ApplicantId)
                .ForeignKey("Dictionaries.Company", t => t.DataAdministratorId)
                .ForeignKey("Recruitment.DataOrigin", t => t.DataOriginId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Recruitment.Applicant", t => t.WhoRecommendedApplicantId)
                .Index(t => t.ApplicantId)
                .Index(t => t.DataAdministratorId)
                .Index(t => t.WhoRecommendedApplicantId)
                .Index(t => t.DataOriginId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.DataOrigin",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        IsRecommendingPersonRequired = c.Boolean(nullable: false),
                        IsCommentRequired = c.Boolean(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.RecruitmentProcess",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CandidateId = c.Long(nullable: false),
                        JobOpeningId = c.Long(nullable: false),
                        RecruiterId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                        ExpectedSalary = c.String(),
                        CanWorkInEvenings = c.Boolean(nullable: false),
                        CanTravel = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                        CvShownToClient = c.Boolean(nullable: false),
                        CvShownToClientOn = c.DateTime(),
                        ClosedOn = c.DateTime(),
                        ClosedReason = c.Int(),
                        ClosedComment = c.String(),
                        HiredOn = c.DateTime(),
                        Seniority = c.Int(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId)
                .ForeignKey("Dictionaries.Location", t => t.LocationId)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Allocation.Employee", t => t.RecruiterId)
                .Index(t => t.CandidateId)
                .Index(t => t.JobOpeningId)
                .Index(t => t.RecruiterId)
                .Index(t => t.LocationId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.RecruitmentProcessStep",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RecruitmentProcessId = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        PlannedOn = c.DateTime(nullable: false),
                        DecisionMakerId = c.Long(nullable: false),
                        Decision = c.Int(nullable: false),
                        Comment = c.String(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Allocation.Employee", t => t.DecisionMakerId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Recruitment.RecruitmentProcess", t => t.RecruitmentProcessId)
                .Index(t => t.RecruitmentProcessId)
                .Index(t => t.DecisionMakerId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.TechnicalReview",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RecruitmentProcessId = c.Long(nullable: false),
                        SeniorityLevel = c.Int(nullable: false),
                        VerifiedByEmployeeId = c.Long(nullable: false),
                        VerifiedOn = c.DateTime(nullable: false),
                        Message = c.String(),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Recruitment.RecruitmentProcess", t => t.RecruitmentProcessId)
                .ForeignKey("Allocation.Employee", t => t.VerifiedByEmployeeId)
                .Index(t => t.RecruitmentProcessId)
                .Index(t => t.VerifiedByEmployeeId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "Recruitment.CandidateLocations",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        CandidateLocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.CandidateLocationId })
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("Dictionaries.Location", t => t.CandidateLocationId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.CandidateLocationId);
            
            CreateTable(
                "Recruitment.CandidateMainTechnologies",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        MainTechnologyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.MainTechnologyId })
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("SkillManagement.Skill", t => t.MainTechnologyId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.MainTechnologyId);
            
            CreateTable(
                "Recruitment.JobOpeningLocations",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.LocationId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Dictionaries.Location", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "Recruitment.JobOpeningMainTechnologies",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        SkillId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.SkillId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("SkillManagement.Skill", t => t.SkillId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.SkillId);
            
            CreateTable(
                "Recruitment.JobOpeningRecruiters",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.EmployeeId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.JobOpeningRecruitmentHelpers",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.EmployeeId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.JobOpeningRecruitmentOwners",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.EmployeeId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.JobOpeningTechnicalReviewers",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.EmployeeId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Recruitment.ContactRecordRelevantJobOpenings",
                c => new
                    {
                        ContactRecordId = c.Long(nullable: false),
                        RelevantJobOpeningId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContactRecordId, t.RelevantJobOpeningId })
                .ForeignKey("Recruitment.ContactRecord", t => t.ContactRecordId, cascadeDelete: true)
                .ForeignKey("Recruitment.JobOpening", t => t.RelevantJobOpeningId, cascadeDelete: true)
                .Index(t => t.ContactRecordId)
                .Index(t => t.RelevantJobOpeningId);
            
            CreateTable(
                "Recruitment.TechnicalReviewMainTechnologies",
                c => new
                    {
                        TechnicalReviewId = c.Long(nullable: false),
                        MainTechnologyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.TechnicalReviewId, t.MainTechnologyId })
                .ForeignKey("Recruitment.TechnicalReview", t => t.TechnicalReviewId, cascadeDelete: true)
                .ForeignKey("SkillManagement.Skill", t => t.MainTechnologyId, cascadeDelete: true)
                .Index(t => t.TechnicalReviewId)
                .Index(t => t.MainTechnologyId);
            
            AddColumn("Dictionaries.Company", "IsDataAdministrator", c => c.Boolean(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequest", "HoursWorkedForClient", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripRequest", "DepartureCityId", c => c.Long(nullable: false));
            AddColumn("BusinessTrips.BusinessTrip", "DepartureCityId", c => c.Long(nullable: false));
            DropForeignKey("Recruitment.TechnicalReview", "VerifiedByEmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.TechnicalReview", "RecruitmentProcessId", "Recruitment.RecruitmentProcess");
            DropForeignKey("Recruitment.TechnicalReview", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.TechnicalReviewMainTechnologies", "MainTechnologyId", "SkillManagement.Skill");
            DropForeignKey("Recruitment.TechnicalReviewMainTechnologies", "TechnicalReviewId", "Recruitment.TechnicalReview");
            DropForeignKey("Recruitment.TechnicalReview", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcess", "RecruiterId", "Allocation.Employee");
            DropForeignKey("Recruitment.RecruitmentProcessStep", "RecruitmentProcessId", "Recruitment.RecruitmentProcess");
            DropForeignKey("Recruitment.RecruitmentProcessStep", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcessStep", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcessStep", "DecisionMakerId", "Allocation.Employee");
            DropForeignKey("Recruitment.RecruitmentProcess", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcess", "LocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.RecruitmentProcess", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecruitmentProcess", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.DataConsentDocumentContent", "Id", "Recruitment.DataConsentDocument");
            DropForeignKey("Recruitment.DataConsentDocument", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsentDocument", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsent", "WhoRecommendedApplicantId", "Recruitment.Applicant");
            DropForeignKey("Recruitment.DataConsent", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsent", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsent", "DataOriginId", "Recruitment.DataOrigin");
            DropForeignKey("Recruitment.DataOrigin", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataOrigin", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.DataConsent", "DataAdministratorId", "Dictionaries.Company");
            DropForeignKey("Recruitment.DataConsentDocument", "DataConsentId", "Recruitment.DataConsent");
            DropForeignKey("Recruitment.DataConsent", "ApplicantId", "Recruitment.Applicant");
            DropForeignKey("Recruitment.ContactRecordRelevantJobOpenings", "RelevantJobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.ContactRecordRelevantJobOpenings", "ContactRecordId", "Recruitment.ContactRecord");
            DropForeignKey("Recruitment.JobOpeningTechnicalReviewers", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.JobOpeningTechnicalReviewers", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpening", "ServiceLineId", "Organization.OrgUnit");
            DropForeignKey("Recruitment.JobOpeningRecruitmentOwners", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.JobOpeningRecruitmentOwners", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpeningRecruitmentHelpers", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.JobOpeningRecruitmentHelpers", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpeningRecruiters", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.JobOpeningRecruiters", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.ProjectDescription", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.ProjectDescription", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.JobOpening", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.JobOpeningMainTechnologies", "SkillId", "SkillManagement.Skill");
            DropForeignKey("Recruitment.JobOpeningMainTechnologies", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpeningLocations", "LocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.JobOpeningLocations", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpening", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.JobOpening", "CustomerId", "Sales.Customer");
            DropForeignKey("Recruitment.JobOpening", "BusinessLineId", "Organization.OrgUnit");
            DropForeignKey("Recruitment.JobOpening", "AcceptingEmployeeId", "Allocation.Employee");
            DropForeignKey("Recruitment.ContactRecord", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.ContactRecord", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.ContactRecord", "ContactedById", "Allocation.Employee");
            DropForeignKey("Recruitment.ContactRecord", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.Candidate", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.CandidateMainTechnologies", "MainTechnologyId", "SkillManagement.Skill");
            DropForeignKey("Recruitment.CandidateMainTechnologies", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.CandidateLocations", "CandidateLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.CandidateLocations", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.CandidateLanguage", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.CandidateLanguage", "LanguageId", "Dictionaries.Language");
            DropForeignKey("Recruitment.CandidateLanguage", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.CandidateLanguage", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.Candidate", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.Candidate", "ApplicantId", "Recruitment.Applicant");
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "ReinvoiceCurrencyId", "BusinessTrips.Currency");
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "MaxCostsCurrencyId", "BusinessTrips.Currency");
            DropForeignKey("BusinessTrips.BusinessTripAcceptanceConditions", "BusinessTrip_Id", "BusinessTrips.BusinessTrip");
            DropForeignKey("Recruitment.Applicant", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.Applicant", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Workflows.BusinessTripRequestDepartureCityMap", "CityId", "Dictionaries.City");
            DropForeignKey("Workflows.BusinessTripRequestDepartureCityMap", "BusinessTripRequestId", "Workflows.BusinessTripRequest");
            DropForeignKey("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId", "Allocation.Employee");
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "CityId", "Dictionaries.City");
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropIndex("Recruitment.TechnicalReviewMainTechnologies", new[] { "MainTechnologyId" });
            DropIndex("Recruitment.TechnicalReviewMainTechnologies", new[] { "TechnicalReviewId" });
            DropIndex("Recruitment.ContactRecordRelevantJobOpenings", new[] { "RelevantJobOpeningId" });
            DropIndex("Recruitment.ContactRecordRelevantJobOpenings", new[] { "ContactRecordId" });
            DropIndex("Recruitment.JobOpeningTechnicalReviewers", new[] { "EmployeeId" });
            DropIndex("Recruitment.JobOpeningTechnicalReviewers", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningRecruitmentOwners", new[] { "EmployeeId" });
            DropIndex("Recruitment.JobOpeningRecruitmentOwners", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningRecruitmentHelpers", new[] { "EmployeeId" });
            DropIndex("Recruitment.JobOpeningRecruitmentHelpers", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningRecruiters", new[] { "EmployeeId" });
            DropIndex("Recruitment.JobOpeningRecruiters", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningMainTechnologies", new[] { "SkillId" });
            DropIndex("Recruitment.JobOpeningMainTechnologies", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningLocations", new[] { "LocationId" });
            DropIndex("Recruitment.JobOpeningLocations", new[] { "JobOpeningId" });
            DropIndex("Recruitment.CandidateMainTechnologies", new[] { "MainTechnologyId" });
            DropIndex("Recruitment.CandidateMainTechnologies", new[] { "CandidateId" });
            DropIndex("Recruitment.CandidateLocations", new[] { "CandidateLocationId" });
            DropIndex("Recruitment.CandidateLocations", new[] { "CandidateId" });
            DropIndex("Workflows.BusinessTripRequestDepartureCityMap", new[] { "CityId" });
            DropIndex("Workflows.BusinessTripRequestDepartureCityMap", new[] { "BusinessTripRequestId" });
            DropIndex("BusinessTrips.BusinessTripDepartureCityMap", new[] { "CityId" });
            DropIndex("BusinessTrips.BusinessTripDepartureCityMap", new[] { "BusinessTripId" });
            DropIndex("Recruitment.TechnicalReview", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.TechnicalReview", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            DropIndex("Recruitment.TechnicalReview", new[] { "RecruitmentProcessId" });
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            DropIndex("Recruitment.RecruitmentProcessStep", new[] { "RecruitmentProcessId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "RecruiterId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "JobOpeningId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "CandidateId" });
            DropIndex("Recruitment.DataOrigin", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.DataOrigin", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.DataConsent", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.DataConsent", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.DataConsent", new[] { "DataOriginId" });
            DropIndex("Recruitment.DataConsent", new[] { "WhoRecommendedApplicantId" });
            DropIndex("Recruitment.DataConsent", new[] { "DataAdministratorId" });
            DropIndex("Recruitment.DataConsent", new[] { "ApplicantId" });
            DropIndex("Recruitment.DataConsentDocument", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.DataConsentDocument", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.DataConsentDocument", new[] { "DataConsentId" });
            DropIndex("Recruitment.DataConsentDocumentContent", new[] { "Id" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.ProjectDescription", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpening", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.JobOpening", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.JobOpening", new[] { "AcceptingEmployeeId" });
            DropIndex("Recruitment.JobOpening", new[] { "BusinessLineId" });
            DropIndex("Recruitment.JobOpening", new[] { "ServiceLineId" });
            DropIndex("Recruitment.JobOpening", new[] { "CustomerId" });
            DropIndex("Recruitment.CandidateLanguage", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.CandidateLanguage", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.CandidateLanguage", new[] { "LanguageId" });
            DropIndex("Recruitment.CandidateLanguage", new[] { "CandidateId" });
            DropIndex("Recruitment.Candidate", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.Candidate", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.Candidate", new[] { "ApplicantId" });
            DropIndex("Recruitment.ContactRecord", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.ContactRecord", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.ContactRecord", new[] { "ContactedById" });
            DropIndex("Recruitment.ContactRecord", new[] { "CandidateId" });
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "BusinessTrip_Id" });
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "MaxCostsCurrencyId" });
            DropIndex("BusinessTrips.BusinessTripAcceptanceConditions", new[] { "ReinvoiceCurrencyId" });
            DropIndex("Recruitment.Applicant", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.Applicant", new[] { "UserIdImpersonatedBy" });
            DropIndex("BusinessTrips.BusinessTrip", new[] { "FrontDeskAssigneeEmployeeId" });
            DropColumn("Workflows.BusinessTripSettlementRequest", "HoursWorkedForClient");
            DropColumn("BusinessTrips.BusinessTrip", "FrontDeskAssigneeEmployeeId");
            DropColumn("Dictionaries.Company", "IsDataAdministrator");
            DropTable("Recruitment.TechnicalReviewMainTechnologies");
            DropTable("Recruitment.ContactRecordRelevantJobOpenings");
            DropTable("Recruitment.JobOpeningTechnicalReviewers");
            DropTable("Recruitment.JobOpeningRecruitmentOwners");
            DropTable("Recruitment.JobOpeningRecruitmentHelpers");
            DropTable("Recruitment.JobOpeningRecruiters");
            DropTable("Recruitment.JobOpeningMainTechnologies");
            DropTable("Recruitment.JobOpeningLocations");
            DropTable("Recruitment.CandidateMainTechnologies");
            DropTable("Recruitment.CandidateLocations");
            DropTable("Workflows.BusinessTripRequestDepartureCityMap");
            DropTable("BusinessTrips.BusinessTripDepartureCityMap");
            DropTable("Recruitment.TechnicalReview");
            DropTable("Recruitment.RecruitmentProcessStep");
            DropTable("Recruitment.RecruitmentProcess");
            DropTable("Recruitment.DataOrigin");
            DropTable("Recruitment.DataConsent");
            DropTable("Recruitment.DataConsentDocument");
            DropTable("Recruitment.DataConsentDocumentContent");
            DropTable("Recruitment.ProjectDescription");
            DropTable("Sales.Customer");
            DropTable("Recruitment.JobOpening");
            DropTable("Recruitment.CandidateLanguage");
            DropTable("Recruitment.Candidate");
            DropTable("Recruitment.ContactRecord");
            DropTable("BusinessTrips.BusinessTripAcceptanceConditions");
            DropTable("Recruitment.Applicant");
            CreateIndex("Workflows.BusinessTripRequest", "DepartureCityId");
            CreateIndex("BusinessTrips.BusinessTrip", "DepartureCityId");
            AddForeignKey("Workflows.BusinessTripRequest", "DepartureCityId", "Dictionaries.City", "Id");
            AddForeignKey("BusinessTrips.BusinessTrip", "DepartureCityId", "Dictionaries.City", "Id");
        }
    }
}
