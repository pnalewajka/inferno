namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddCrmIdOnCandidateAndJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "CrmId", c => c.Guid(nullable: true));
            AddColumn("Recruitment.JobOpening", "CrmId", c => c.Guid(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.JobOpening", "CrmId");
            DropColumn("Recruitment.Candidate", "CrmId");

        }
    }
}
