namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration110420181842 : DbMigration
    {
        public override void Up()
        {
            //RenameTable(name: "Reporting.ReportDefinitionRequiredProfiles", newName: "ReportDefinitionAllowedForProfiles");
            //RenameColumn(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "RequiredProfileId", newName: "AllowedForProfileId");
            //RenameIndex(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "IX_RequiredProfileId", newName: "IX_AllowedForProfileId");
            //AddColumn("Recruitment.ApplicationOrigin", "ParserIdentifier", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.ApplicationOrigin", "EmailAddress", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            //DropColumn("Recruitment.ApplicationOrigin", "EmailAddress");
            //DropColumn("Recruitment.ApplicationOrigin", "ParserIdentifier");
            //RenameIndex(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "IX_AllowedForProfileId", newName: "IX_RequiredProfileId");
            //RenameColumn(table: "Reporting.ReportDefinitionAllowedForProfiles", name: "AllowedForProfileId", newName: "RequiredProfileId");
            //RenameTable(name: "Reporting.ReportDefinitionAllowedForProfiles", newName: "ReportDefinitionRequiredProfiles");
        }
    }
}
