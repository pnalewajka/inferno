namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFrontDeskAssigneeToCompanyMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Dictionaries.Company", "FrontDeskAssigneeEmployeeId", c => c.Long());
            CreateIndex("Dictionaries.Company", "FrontDeskAssigneeEmployeeId");
            AddForeignKey("Dictionaries.Company", "FrontDeskAssigneeEmployeeId", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Dictionaries.Company", "FrontDeskAssigneeEmployeeId", "Allocation.Employee");
            DropIndex("Dictionaries.Company", new[] { "FrontDeskAssigneeEmployeeId" });
            DropColumn("Dictionaries.Company", "FrontDeskAssigneeEmployeeId");
        }
    }
}
