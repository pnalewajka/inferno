﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class IsValidForInvoiceNotificationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Compensation.InvoiceNotification", "IsValid", c => c.Boolean(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803260950371_IsValidForInvoiceNotificationMigration.SetOldInvoiceNotificationsAsActive.Up.sql");
        }
        
        public override void Down()
        {
            DropColumn("Compensation.InvoiceNotification", "IsValid");
        }
    }
}

