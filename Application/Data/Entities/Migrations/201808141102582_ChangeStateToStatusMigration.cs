namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStateToStatusMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Sales.Inquiry", "ProcessState", "ProcessStatus");
        }
        
        public override void Down()
        {
            RenameColumn("Sales.Inquiry", "ProcessStatus", "ProcessState");
        }
    }
}
