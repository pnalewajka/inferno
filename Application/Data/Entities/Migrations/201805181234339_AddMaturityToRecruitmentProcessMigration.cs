namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMaturityToRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "IsMatureEnoughForHiringManager", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.RecruitmentProcess", "IsMatureEnoughForHiringManager");
        }
    }
}
