namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class ResourceRequestTypeAddMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("CustomerPortal.ResourceForm", "RequestType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("CustomerPortal.ResourceForm", "RequestType");
        }
    }
}
