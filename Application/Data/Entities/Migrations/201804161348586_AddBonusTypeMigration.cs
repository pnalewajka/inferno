namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddBonusTypeMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Compensation.Bonus", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Compensation.Bonus", "Type");
        }
    }
}
