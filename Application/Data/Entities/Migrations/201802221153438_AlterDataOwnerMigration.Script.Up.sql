﻿SET XACT_ABORT ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogEmployeeDataActivity]'))
   DROP PROCEDURE [GDPR].[LogEmployeeDataActivity]
GO

CREATE PROCEDURE [GDPR].[LogEmployeeDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @userId BIGINT
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()

	SELECT @userId = u.Id FROM [Allocation].[Employee] as e
		JOIN [Accounts].[User] as u
		ON e.Id = u.Id
		WHERE e.Id = @identity

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE EmployeeId = @identity OR UserId = @userId

	IF @dataOwnerId IS NULL
	BEGIN
			INSERT INTO [GDPR].[DataOwner] 
				(EmployeeId,
				UserId,
				ModifiedOn) 
			VALUES
				(@identity,
				@userId,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogUserDataActivity]'))
   DROP PROCEDURE [GDPR].[LogUserDataActivity]
GO

CREATE PROCEDURE [GDPR].[LogUserDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @employeeId BIGINT
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()

	SELECT @employeeId = e.Id FROM [Allocation].[Employee] as e
		JOIN [Accounts].[User] as u
		ON e.Id = u.Id
		WHERE u.Id = @identity

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE UserId = @identity OR EmployeeId = @employeeId

	IF @dataOwnerId IS NULL
	BEGIN
			INSERT INTO [GDPR].[DataOwner] 
				(EmployeeId,
				UserId,
				ModifiedOn) 
			VALUES
				(@employeeId,
				@identity,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogCandidateDataActivity]'))
   DROP PROCEDURE [GDPR].[LogCandidateDataActivity]
GO

CREATE PROCEDURE [GDPR].[LogCandidateDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE CandidateId = @identity

	IF @dataOwnerId IS NULL
	BEGIN
			INSERT INTO [GDPR].[DataOwner] 
				(CandidateId,
				ModifiedOn) 
			VALUES
				(@identity,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[GDPR].[LogRecommendingPersonDataActivity]'))
   DROP PROCEDURE [GDPR].[LogRecommendingPersonDataActivity]
GO

CREATE PROCEDURE [GDPR].[LogRecommendingPersonDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @dataOwnerId BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()

	SELECT @dataOwnerId = Id FROM [GDPR].[DataOwner] WHERE RecommendingPersonId = @identity

	IF @dataOwnerId IS NULL
	BEGIN
			INSERT INTO [GDPR].[DataOwner] 
				(RecommendingPersonId,
				ModifiedOn) 
			VALUES
				(@identity,
				@currentDate)

			SET @dataOwnerId = SCOPE_IDENTITY()
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwnerId, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO


