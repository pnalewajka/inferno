﻿SET XACT_ABORT ON
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [Accounts].[atomic_VerifySecurityAreasIntegrity]
	@UserId BIGINT = NULL
AS 
BEGIN
	DECLARE @output VARCHAR(MAX) = '<BusinessException xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="SecurityAreaRoleViolationException"><Violations>'

	SELECT TOP 5
		@output = @output + '<ViolationDetail>' +
			'<UserName>' + (UserName COLLATE DATABASE_DEFAULT) + '</UserName>' +
			CASE WHEN ProfileName IS NULL THEN '' ELSE '<Profile>' + ProfileName + '</Profile>' END +
			'<Role>' + SecurityRoleName + '</Role>' +
			'</ViolationDetail>'
	FROM
	(
		SELECT
			*,
			MAX(AreaMatched) OVER (PARTITION BY UserId, RoleId) AS RangeMatched
		FROM
		(
			SELECT 
				EffectiveRoles.*,
				sasr.AreaId, 
				sa.Code AS RoleAreaName,
				CASE WHEN EffectiveRoles.SecurityAreaId = sasr.AreaId THEN 1 ELSE 0 END AS AreaMatched
			FROM
			(
				SELECT 
					u.Id AS UserId, u.FirstName + ' ' + u.LastName AS UserName, u.SecurityAreaId, sa.Code AS Area, spProfile.Id as ProfileId, spProfile.Name AS ProfileName, srspProfile.RoleId, sr.Name AS SecurityRoleName
				FROM
					[Accounts].[User] u
				JOIN
					([Accounts].[UserSecurityProfiles] uspProfile 
						JOIN [Accounts].[SecurityProfile] spProfile ON uspProfile.ProfileId = spProfile.Id
						JOIN [Accounts].[SecurityRoleSecurityProfiles] srspProfile ON uspProfile.ProfileId = srspProfile.ProfileId
					)
					ON u.ID = uspProfile.UserId
				JOIN [Accounts].[SecurityArea] sa ON u.SecurityAreaId = sa.Id
				JOIN [Accounts].[SecurityRole] sr ON sr.ID =  srspProfile.RoleId
				UNION ALL
				SELECT 
					u.Id AS UserId, u.FirstName + ' ' + u.LastName AS UserName, u.SecurityAreaId, sa.Code AS Area, NULL AS ProfileId, NULL AS ProfileName, usrRole.RoleId, sr.Name AS SecurityRoleName
				FROM
					[Accounts].[User] u
				JOIN [Accounts].[UserSecurityRoles] usrRole ON u.ID = usrRole.UserId
				JOIN [Accounts].[SecurityArea] sa ON u.SecurityAreaId = sa.Id
				JOIN [Accounts].[SecurityRole] sr ON sr.ID =  usrRole.RoleId
			) AS EffectiveRoles
			JOIN [Accounts].[SecurityAreaSecurityRoles] sasr ON sasr.RoleId = EffectiveRoles.RoleId
			JOIN [Accounts].[SecurityArea] sa ON sa.Id = sasr.AreaId
		) AS EffectiveRolesAndAreas
	) AS ResolvedTable
	WHERE 
	RangeMatched = 0
	AND AreaMatched = 0
	AND (@UserId IS NULL OR UserId = @UserId)

	DECLARE @count BIGINT
	SELECT @count = COUNT(1)
	FROM
	(
		SELECT
			*,
			MAX(AreaMatched) OVER (PARTITION BY UserId, RoleId) AS RangeMatched
		FROM
		(
			SELECT 
				EffectiveRoles.*,
				sasr.AreaId, 
				CASE WHEN EffectiveRoles.SecurityAreaId = sasr.AreaId THEN 1 ELSE 0 END AS AreaMatched
			FROM
			(
				SELECT 
					u.Id AS UserId, u.SecurityAreaId, spProfile.Id as ProfileId, srspProfile.RoleId
				FROM
					[Accounts].[User] u
				JOIN
					([Accounts].[UserSecurityProfiles] uspProfile 
						JOIN [Accounts].[SecurityProfile] spProfile ON uspProfile.ProfileId = spProfile.Id
						JOIN [Accounts].[SecurityRoleSecurityProfiles] srspProfile ON uspProfile.ProfileId = srspProfile.ProfileId
					)
					ON u.ID = uspProfile.UserId
				UNION ALL
				SELECT 
					u.Id AS UserId, u.SecurityAreaId, NULL as ProfileId, usrRole.RoleId
				FROM
					[Accounts].[User] u
				JOIN
					[Accounts].[UserSecurityRoles] usrRole ON u.ID = usrRole.UserId
			) AS EffectiveRoles
			JOIN 
				[Accounts].[SecurityAreaSecurityRoles] sasr ON sasr.RoleId = EffectiveRoles.RoleId
		) AS EffectiveRolesAndAreas
	) AS ResolvedTable
	WHERE 
	RangeMatched = 0
	AND AreaMatched = 0
	AND (@UserId IS NULL OR UserId = @UserId)

	SET @output = @output + '</Violations><ViolationCount>' + CAST(@count AS VARCHAR(10)) + '</ViolationCount></BusinessException>'
	IF(@count > 0)
	BEGIN
		RAISERROR('%s', 16, 1, @output)
		IF(@@TRANCOUNT>0)
		BEGIN
			ROLLBACK
		END
	END
END
GO


