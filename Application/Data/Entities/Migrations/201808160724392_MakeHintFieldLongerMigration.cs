namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MakeHintFieldLongerMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.RecruitmentProcessStep", "RecruitmentHint", c => c.String(maxLength: 1024));
        }

        public override void Down()
        {
            AlterColumn("Recruitment.RecruitmentProcessStep", "RecruitmentHint", c => c.String(maxLength: 255));
        }
    }
}
