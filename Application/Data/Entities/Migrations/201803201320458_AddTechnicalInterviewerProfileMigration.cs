﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AddTechnicalInterviewerProfileMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803201320458_AddTechnicalInterviewerProfileMigration.AddTechnicalInterviewerProfile.Up.sql");
        }
    }
}

