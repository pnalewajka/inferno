﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class PromoteTemporaryDocumentMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807240730249_PromoteTemporaryDocumentMigration.ModifyPromoteTempDocumentSqlMigration.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201807240730249_PromoteTemporaryDocumentMigration.ModifyPromoteTempDocumentSqlMigration.Down.sql");
        }
    }
}

