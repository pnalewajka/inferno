namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RenameAndAddCandidateFieldMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Recruitment.Candidate", "CvFullText", "ResumeTextContent");
            AddColumn("Recruitment.Candidate", "IsHighlyConfidential", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            RenameColumn("Recruitment.Candidate", "ResumeTextContent", "CvFullText");
            DropColumn("Recruitment.Candidate", "IsHighlyConfidential");
        }
    }
}
