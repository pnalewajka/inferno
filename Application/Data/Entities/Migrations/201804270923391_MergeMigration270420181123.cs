namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration270420181123 : DbMigration
    {
        public override void Up()
        {
            /*DropForeignKey("Recruitment.JobApplicationLocations", "JobOpeningId", "Recruitment.JobApplication");
            DropForeignKey("Recruitment.JobApplicationLocations", "LocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.CandidateLocations", "CandidateId", "Recruitment.Candidate");
            DropForeignKey("Recruitment.CandidateLocations", "CandidateLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.JobOpeningLocations", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.JobOpeningLocations", "LocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "LocationId", "Dictionaries.Location");
            DropForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location");
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", "IX_SingleMealsEntryPerCountry");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferLocationId" });
            DropIndex("Recruitment.JobApplicationLocations", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobApplicationLocations", new[] { "LocationId" });
            DropIndex("Recruitment.CandidateLocations", new[] { "CandidateId" });
            DropIndex("Recruitment.CandidateLocations", new[] { "CandidateLocationId" });
            DropIndex("Recruitment.JobOpeningLocations", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobOpeningLocations", new[] { "LocationId" });
            CreateTable(
                "Recruitment.CandidateCities",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        CandidateLocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.CandidateLocationId })
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("Dictionaries.City", t => t.CandidateLocationId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.CandidateLocationId);
            
            CreateTable(
                "Recruitment.JobApplicationCities",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.LocationId })
                .ForeignKey("Recruitment.JobApplication", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Dictionaries.City", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "Recruitment.JobOpeningCities",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.LocationId })
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .ForeignKey("Dictionaries.City", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.JobOpeningId)
                .Index(t => t.LocationId);
            
            AddColumn("TimeTracking.ContractType", "UseDailySettlementMeals", c => c.Boolean(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestMeal", "Day", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "CityId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalCityId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "OfferCityId", c => c.Long());
            AlterColumn("Workflows.BusinessTripSettlementRequestMeal", "CountryId", c => c.Long());
            CreateIndex("Workflows.BusinessTripSettlementRequestMeal", "BusinessTripSettlementRequestId");
            CreateIndex("Workflows.BusinessTripSettlementRequestMeal", "CountryId");
            CreateIndex("Recruitment.RecruitmentProcess", "CityId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalCityId");
            CreateIndex("Recruitment.RecruitmentProcess", "OfferCityId");
            AddForeignKey("Recruitment.RecruitmentProcess", "CityId", "Dictionaries.City", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalCityId", "Dictionaries.City", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "OfferCityId", "Dictionaries.City", "Id");
            DropColumn("Recruitment.RecruitmentProcess", "LocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "OfferLocationId");
            DropTable("Recruitment.JobApplicationLocations");
            DropTable("Recruitment.CandidateLocations");
            DropTable("Recruitment.JobOpeningLocations");*/
        }
        
        public override void Down()
        {
            /*CreateTable(
                "Recruitment.JobOpeningLocations",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.LocationId });
            
            CreateTable(
                "Recruitment.CandidateLocations",
                c => new
                    {
                        CandidateId = c.Long(nullable: false),
                        CandidateLocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.CandidateLocationId });
            
            CreateTable(
                "Recruitment.JobApplicationLocations",
                c => new
                    {
                        JobOpeningId = c.Long(nullable: false),
                        LocationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.JobOpeningId, t.LocationId });
            
            AddColumn("Recruitment.RecruitmentProcess", "OfferLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long());
            DropForeignKey("Recruitment.RecruitmentProcess", "OfferCityId", "Dictionaries.City");
            DropForeignKey("Recruitment.JobOpeningCities", "LocationId", "Dictionaries.City");
            DropForeignKey("Recruitment.JobOpeningCities", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalCityId", "Dictionaries.City");
            DropForeignKey("Recruitment.RecruitmentProcess", "CityId", "Dictionaries.City");
            DropForeignKey("Recruitment.JobApplicationCities", "LocationId", "Dictionaries.City");
            DropForeignKey("Recruitment.JobApplicationCities", "JobOpeningId", "Recruitment.JobApplication");
            DropForeignKey("Recruitment.CandidateCities", "CandidateLocationId", "Dictionaries.City");
            DropForeignKey("Recruitment.CandidateCities", "CandidateId", "Recruitment.Candidate");
            DropIndex("Recruitment.JobOpeningCities", new[] { "LocationId" });
            DropIndex("Recruitment.JobOpeningCities", new[] { "JobOpeningId" });
            DropIndex("Recruitment.JobApplicationCities", new[] { "LocationId" });
            DropIndex("Recruitment.JobApplicationCities", new[] { "JobOpeningId" });
            DropIndex("Recruitment.CandidateCities", new[] { "CandidateLocationId" });
            DropIndex("Recruitment.CandidateCities", new[] { "CandidateId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferCityId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalCityId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "CityId" });
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", new[] { "CountryId" });
            DropIndex("Workflows.BusinessTripSettlementRequestMeal", new[] { "BusinessTripSettlementRequestId" });
            AlterColumn("Workflows.BusinessTripSettlementRequestMeal", "CountryId", c => c.Long(nullable: false));
            DropColumn("Recruitment.RecruitmentProcess", "OfferCityId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalCityId");
            DropColumn("Recruitment.RecruitmentProcess", "CityId");
            DropColumn("Workflows.BusinessTripSettlementRequestMeal", "Day");
            DropColumn("TimeTracking.ContractType", "UseDailySettlementMeals");
            DropTable("Recruitment.JobOpeningCities");
            DropTable("Recruitment.JobApplicationCities");
            DropTable("Recruitment.CandidateCities");
            CreateIndex("Recruitment.JobOpeningLocations", "LocationId");
            CreateIndex("Recruitment.JobOpeningLocations", "JobOpeningId");
            CreateIndex("Recruitment.CandidateLocations", "CandidateLocationId");
            CreateIndex("Recruitment.CandidateLocations", "CandidateId");
            CreateIndex("Recruitment.JobApplicationLocations", "LocationId");
            CreateIndex("Recruitment.JobApplicationLocations", "JobOpeningId");
            CreateIndex("Recruitment.RecruitmentProcess", "OfferLocationId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
            CreateIndex("Workflows.BusinessTripSettlementRequestMeal", new[] { "BusinessTripSettlementRequestId", "CountryId" }, unique: true, name: "IX_SingleMealsEntryPerCountry");
            AddForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "LocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.JobOpeningLocations", "LocationId", "Dictionaries.Location", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobOpeningLocations", "JobOpeningId", "Recruitment.JobOpening", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.CandidateLocations", "CandidateLocationId", "Dictionaries.Location", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.CandidateLocations", "CandidateId", "Recruitment.Candidate", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobApplicationLocations", "LocationId", "Dictionaries.Location", "Id", cascadeDelete: true);
            AddForeignKey("Recruitment.JobApplicationLocations", "JobOpeningId", "Recruitment.JobApplication", "Id", cascadeDelete: true);*/
        }
    }
}
