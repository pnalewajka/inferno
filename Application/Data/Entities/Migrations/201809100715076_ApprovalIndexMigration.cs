namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApprovalIndexMigration : DbMigration
    {
        public override void Up()
        {
            CreateIndex("GDPR.DataProcessingAgreementConsent", "IsApproved");
        }
        
        public override void Down()
        {
            DropIndex("GDPR.DataProcessingAgreementConsent", new[] { "IsApproved" });
        }
    }
}
