namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentProcessAddOnboardingRequestIdMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "OnboardingRequestId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcess", "OnboardingRequestId");
            AddForeignKey("Recruitment.RecruitmentProcess", "OnboardingRequestId", "Workflows.OnboardingRequest", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "OnboardingRequestId", "Workflows.OnboardingRequest");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OnboardingRequestId" });
            DropColumn("Recruitment.RecruitmentProcess", "OnboardingRequestId");
        }
    }
}
