namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ProjectDetailsAboutClientMaxLengthMigration : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AboutClient NVARCHAR (3072)");
        }

        public override void Down()
        {
            Sql("ALTER TABLE Recruitment.ProjectDescription  ALTER COLUMN AboutClient NVARCHAR (2048)");
        }
    }
}
