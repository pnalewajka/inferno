namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplaceBusinessLineServiceLineWithOrgUnitInJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Recruitment.JobOpening", "ServiceLineId", "Organization.OrgUnit");
            DropIndex("Recruitment.JobOpening", new[] { "ServiceLineId" });
            RenameColumn(table: "Recruitment.JobOpening", name: "BusinessLineId", newName: "OrgUnitId");
            RenameIndex(table: "Recruitment.JobOpening", name: "IX_BusinessLineId", newName: "IX_OrgUnitId");
            DropColumn("Recruitment.JobOpening", "ServiceLineId");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.JobOpening", "ServiceLineId", c => c.Long(nullable: false));
            RenameIndex(table: "Recruitment.JobOpening", name: "IX_OrgUnitId", newName: "IX_BusinessLineId");
            RenameColumn(table: "Recruitment.JobOpening", name: "OrgUnitId", newName: "BusinessLineId");
            CreateIndex("Recruitment.JobOpening", "ServiceLineId");
            AddForeignKey("Recruitment.JobOpening", "ServiceLineId", "Organization.OrgUnit", "Id");
        }
    }
}
