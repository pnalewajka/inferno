﻿SET XACT_ABORT ON
GO

DELETE FROM [GDPR].[DataActivity]
DELETE FROM [GDPR].[DataConsent]
DELETE FROM [GDPR].[DataOwner]
DELETE FROM [Recruitment].[TechnicalReview]
DELETE FROM [Recruitment].[RecruitmentProcessStep]
DELETE FROM [Recruitment].[RecruitmentProcess]
DELETE FROM [Recruitment].[JobApplication]
DELETE FROM [Recruitment].[Candidate]