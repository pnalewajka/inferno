namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSourceContactRecordToRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "SourceContactRecordId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcess", "SourceContactRecordId");
            AddForeignKey("Recruitment.RecruitmentProcess", "SourceContactRecordId", "Recruitment.ContactRecord", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "SourceContactRecordId", "Recruitment.ContactRecord");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "SourceContactRecordId" });
            DropColumn("Recruitment.RecruitmentProcess", "SourceContactRecordId");
        }
    }
}
