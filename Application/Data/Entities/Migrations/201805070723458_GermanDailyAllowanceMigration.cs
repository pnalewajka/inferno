﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;
    
    public partial class GermanDailyAllowanceMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("BusinessTrips.DailyAllowance", "DepartureAndArrivalDayAllowance", c => c.Decimal(precision: 18, scale: 2));

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805070723458_GermanDailyAllowanceMigration.InitialValues.Up.sql");
        }

        public override void Down()
        {
            DropColumn("BusinessTrips.DailyAllowance", "DepartureAndArrivalDayAllowance");
        }
    }
}

