﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AddDailyAllowanceTableMigration : AtomicDbMigration
    {
        public override void Up()
        {
            CreateTable(
                "BusinessTrips.DailyAllowance",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ValidFrom = c.DateTime(),
                        ValidTo = c.DateTime(),
                        EmploymentCountryId = c.Long(nullable: false),
                        TravelCountryId = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                        Allowance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("BusinessTrips.Currency", t => t.CurrencyId)
                .ForeignKey("Dictionaries.Country", t => t.EmploymentCountryId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Dictionaries.Country", t => t.TravelCountryId)
                .Index(t => t.EmploymentCountryId)
                .Index(t => t.TravelCountryId)
                .Index(t => t.CurrencyId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802231309063_AddDailyAllowanceTableMigration.InsertDailyAllowancesForPoland.Up.sql");
        }

        public override void Down()
        {
            DropForeignKey("BusinessTrips.DailyAllowance", "TravelCountryId", "Dictionaries.Country");
            DropForeignKey("BusinessTrips.DailyAllowance", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("BusinessTrips.DailyAllowance", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("BusinessTrips.DailyAllowance", "EmploymentCountryId", "Dictionaries.Country");
            DropForeignKey("BusinessTrips.DailyAllowance", "CurrencyId", "BusinessTrips.Currency");
            DropIndex("BusinessTrips.DailyAllowance", new[] { "UserIdModifiedBy" });
            DropIndex("BusinessTrips.DailyAllowance", new[] { "UserIdImpersonatedBy" });
            DropIndex("BusinessTrips.DailyAllowance", new[] { "CurrencyId" });
            DropIndex("BusinessTrips.DailyAllowance", new[] { "TravelCountryId" });
            DropIndex("BusinessTrips.DailyAllowance", new[] { "EmploymentCountryId" });
            DropTable("BusinessTrips.DailyAllowance");
        }
    }
}


