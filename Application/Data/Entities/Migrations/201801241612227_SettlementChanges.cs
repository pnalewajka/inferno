namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SettlementChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "DepartureDateTime", c => c.DateTime(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDateTime", c => c.DateTime(nullable: false));
            AddColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("Workflows.BusinessTripSettlementRequest", "DepartureDate");
            DropColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDate");
            DropColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Value");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDate", c => c.DateTime());
            AddColumn("Workflows.BusinessTripSettlementRequest", "DepartureDate", c => c.DateTime());
            DropColumn("Workflows.BusinessTripSettlementRequestAdvancePayment", "Amount");
            DropColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDateTime");
            DropColumn("Workflows.BusinessTripSettlementRequest", "DepartureDateTime");
        }
    }
}
