namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveCurrencyToDictionariesMigration : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "BusinessTrips.Currency", newSchema: "Dictionaries");
        }
        
        public override void Down()
        {
            MoveTable(name: "Dictionaries.Currency", newSchema: "BusinessTrips");
        }
    }
}
