namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtedDecisonCommentOnRPSMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionComment", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionComment", c => c.String(maxLength: 255));
        }
    }
}
