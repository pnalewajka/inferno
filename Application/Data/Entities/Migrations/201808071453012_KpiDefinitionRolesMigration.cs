namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KpiDefinitionRolesMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "KPI.KpiDefinitionSecurityRoles",
                c => new
                    {
                        KpiDefinitionId = c.Long(nullable: false),
                        SecurityRoleId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.KpiDefinitionId, t.SecurityRoleId })
                .ForeignKey("KPI.KpiDefinition", t => t.KpiDefinitionId, cascadeDelete: true)
                .ForeignKey("Accounts.SecurityRole", t => t.SecurityRoleId, cascadeDelete: true)
                .Index(t => t.KpiDefinitionId)
                .Index(t => t.SecurityRoleId);            
        }
        
        public override void Down()
        {
            DropForeignKey("KPI.KpiDefinitionSecurityRoles", "SecurityRoleId", "Accounts.SecurityRole");
            DropForeignKey("KPI.KpiDefinitionSecurityRoles", "KpiDefinitionId", "KPI.KpiDefinition");
            DropIndex("KPI.KpiDefinitionSecurityRoles", new[] { "SecurityRoleId" });
            DropIndex("KPI.KpiDefinitionSecurityRoles", new[] { "KpiDefinitionId" });
            DropTable("KPI.KpiDefinitionSecurityRoles");
        }
    }
}
