﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class RemoveBTRequestFKMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802151612409_RemoveBTRequestFKMigration.FixPkAndFkOnBt.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

