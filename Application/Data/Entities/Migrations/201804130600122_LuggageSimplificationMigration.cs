﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class LuggageSimplificationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            RenameColumn("Workflows.DeclaredLuggage", "LargeCabinItems", "HandLuggageItems");
            RenameColumn("Workflows.DeclaredLuggage", "RegisteredUnder15Items", "RegisteredLuggageItems");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804130600122_LuggageSimplificationMigration.Script.Up.sql");

            DropColumn("Workflows.DeclaredLuggage", "Registered15To23Items");
            DropColumn("Workflows.DeclaredLuggage", "RegisteredOver23Items");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.DeclaredLuggage", "RegisteredOver23Items", c => c.Int(nullable: false));
            AddColumn("Workflows.DeclaredLuggage", "Registered15To23Items", c => c.Int(nullable: false));
            RenameColumn("Workflows.DeclaredLuggage", "HandLuggageItems", "LargeCabinItems");
            RenameColumn("Workflows.DeclaredLuggage", "RegisteredLuggageItems", "RegisteredUnder15Items");
        }
    }
}
