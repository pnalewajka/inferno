namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateBonusRequestTableMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Workflows.BonusRequest",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        AffectedUserId = c.Long(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        ProjectId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                        BonusAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Long(nullable: false),
                        Justification = c.String(maxLength: 255),
                        Type = c.Int(nullable: false),
                        Reinvoice = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.AffectedUserId)
                .ForeignKey("Dictionaries.Currency", t => t.CurrencyId)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId)
                .ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.AffectedUserId)
                .Index(t => t.EmployeeId)
                .Index(t => t.CurrencyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BonusRequest", "Id", "Workflows.Request");
            DropForeignKey("Workflows.BonusRequest", "EmployeeId", "Allocation.Employee");
            DropForeignKey("Workflows.BonusRequest", "CurrencyId", "Dictionaries.Currency");
            DropForeignKey("Workflows.BonusRequest", "AffectedUserId", "Accounts.User");
            DropIndex("Workflows.BonusRequest", new[] { "CurrencyId" });
            DropIndex("Workflows.BonusRequest", new[] { "EmployeeId" });
            DropIndex("Workflows.BonusRequest", new[] { "AffectedUserId" });
            DropIndex("Workflows.BonusRequest", new[] { "Id" });
            DropTable("Workflows.BonusRequest");
        }
    }
}
