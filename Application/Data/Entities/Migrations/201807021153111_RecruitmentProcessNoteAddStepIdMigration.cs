namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentProcessNoteAddStepIdMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcessNote", "RecruitmentProcessStepId", c => c.Long(nullable: true));
            CreateIndex("Recruitment.RecruitmentProcessNote", "RecruitmentProcessStepId");
            AddForeignKey("Recruitment.RecruitmentProcessNote", "RecruitmentProcessStepId", "Recruitment.RecruitmentProcessStep", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcessNote", "RecruitmentProcessStepId", "Recruitment.RecruitmentProcessStep");
            DropIndex("Recruitment.RecruitmentProcessNote", new[] { "RecruitmentProcessStepId" });
            DropColumn("Recruitment.RecruitmentProcessNote", "RecruitmentProcessStepId");
        }
    }
}
