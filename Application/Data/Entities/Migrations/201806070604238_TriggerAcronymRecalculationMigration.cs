﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class TriggerAcronymRecalculationMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806070604238_TriggerAcronymRecalculationMigration.TriggerAcronymRecalculationSql.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

