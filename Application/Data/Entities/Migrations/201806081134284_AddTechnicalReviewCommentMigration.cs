namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddTechnicalReviewCommentMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.JobOpening", "TechnicalReviewComment", c => c.String(maxLength: 300));
        }

        public override void Down()
        {
            AddColumn("Recruitment.JobOpening", "TechnicalReviewComment", c => c.String(maxLength: 300));
        }
    }
}
