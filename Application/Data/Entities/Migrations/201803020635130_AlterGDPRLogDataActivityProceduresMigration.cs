namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AlterGDPRLogDataActivityProceduresMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803020635130_AlterGDPRLogDataActivityProceduresMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
