namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RemoveIncludeAdvancePaymentMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("TimeTracking.ContractType", "IncludeAdvancedPaymentInBuinessTripCosts");
        }
        
        public override void Down()
        {
            AddColumn("TimeTracking.ContractType", "IncludeAdvancedPaymentInBuinessTripCosts", c => c.Boolean(nullable: false));
        }
    }
}
