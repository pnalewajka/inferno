﻿SET XACT_ABORT ON
GO

DELETE n FROM [Compensation].[InvoiceNotification] n
WHERE [Id] != (
	SELECT TOP 1 [Id]
	FROM [Compensation].[InvoiceNotification] t
	WHERE t.[EmployeeId] = n.[EmployeeId] AND t.[Year] = n.[Year] AND t.[Month] = n.[Month]
	ORDER BY t.[NotifiedOn] DESC)

UPDATE [Compensation].[InvoiceNotification]
SET [IsValid] = 1
