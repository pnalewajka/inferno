﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RecomendingPersonChangeMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201802281315321_RecomendingPersonChangeMigration.DeleteDataFromRecomendingPerson.Up.sql");
            AlterColumn("Recruitment.RecommendingPerson", "FirstName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("Recruitment.RecommendingPerson", "LastName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("Recruitment.RecommendingPerson", "EmailAdress", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("Recruitment.RecommendingPerson", "PhoneNumber", c => c.String(maxLength: 20));
            CreateIndex("Recruitment.RecommendingPerson", "EmailAdress", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.RecommendingPerson", new[] { "EmailAdress" });
            AlterColumn("Recruitment.RecommendingPerson", "PhoneNumber", c => c.String());
            AlterColumn("Recruitment.RecommendingPerson", "EmailAdress", c => c.String());
            AlterColumn("Recruitment.RecommendingPerson", "LastName", c => c.String());
            AlterColumn("Recruitment.RecommendingPerson", "FirstName", c => c.String());
        }
    }
}

