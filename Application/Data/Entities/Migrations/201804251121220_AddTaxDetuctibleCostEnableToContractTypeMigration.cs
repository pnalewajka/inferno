namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class AddTaxDetuctibleCostEnableToContractTypeMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.ContractType", "IsTaxDeductibleCostEnabled", c => c.Boolean(nullable: false));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804251121220_AddTaxDetuctibleCostEnableToContractTypeMigration.sql");
        }

        public override void Down()
        {
            DropColumn("TimeTracking.ContractType", "IsTaxDeductibleCostEnabled");
        }
    }
}
