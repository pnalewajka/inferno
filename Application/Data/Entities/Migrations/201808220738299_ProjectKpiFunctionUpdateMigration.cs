﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class ProjectKpiFunctionUpdateMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808220738299_ProjectKpiFunctionUpdateMigration.CalculateProjectKpis.Up.sql");
        }

        public override void Down()
        {
        }
    }
}