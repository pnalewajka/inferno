namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InvoiceIssuesMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.TimeReport", "InvoiceIssues", c => c.Int());
            RenameColumn("TimeTracking.TimeReport", "InvoiceErrorMessage", "InvoiceRejectionReason");
        }
        
        public override void Down()
        {
            RenameColumn("TimeTracking.TimeReport", "InvoiceRejectionReason", "InvoiceErrorMessage");
            DropColumn("TimeTracking.TimeReport", "InvoiceIssues");
        }
    }
}
