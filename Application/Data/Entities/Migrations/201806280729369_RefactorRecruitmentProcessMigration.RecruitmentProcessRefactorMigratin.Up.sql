﻿MERGE INTO Recruitment.RecruitmentProcessScreening  AS target
USING ( 
 SELECT Id, 
		ScreeningGeneralOpinion,
		ScreeningMotivation,
		ScreeningWorkplaceExpectations,
		ScreeningSkills,
		ScreeningLanguageEnglish,
		ScreeningLanguageGerman,
		ScreeningLanguageOther,
		ScreeningContractExpectations,
		ScreeningAvailability,
		ScreeningOtherProcesses,
		ScreeningCounterOfferCriteria,
		ScreeningRelocationCanDo,
		ScreeningRelocationComment,
		ScreeningBusinessTripsCanDo,
		ScreeningBusinessTripsComment,
		ScreeningEveningWorkCanDo,
		ScreeningEveningWorkComment,
		ScreeningWorkPermitNeeded,
		ScreeningWorkPermitComment
 FROM Recruitment.RecruitmentProcess
 ) AS Source 
	(   RecruitmentProcessId, 
		ScreeningGeneralOpinion,
		ScreeningMotivation,
		ScreeningWorkplaceExpectations,
		ScreeningSkills,
		ScreeningLanguageEnglish,
		ScreeningLanguageGerman,
		ScreeningLanguageOther,
		ScreeningContractExpectations,
		ScreeningAvailability,
		ScreeningOtherProcesses,
		ScreeningCounterOfferCriteria,
		ScreeningRelocationCanDo,
		ScreeningRelocationComment,
		ScreeningBusinessTripsCanDo,
		ScreeningBusinessTripsComment,
		ScreeningEveningWorkCanDo,
		ScreeningEveningWorkComment,
		ScreeningWorkPermitNeeded,
		ScreeningWorkPermitComment)
	ON source.RecruitmentProcessId = target.Id 
	AND 
	((
		source.ScreeningGeneralOpinion IS NOT NULL OR
		source.ScreeningMotivation IS NOT NULL OR
		source.ScreeningWorkplaceExpectations IS NOT NULL OR
		source.ScreeningSkills IS NOT NULL OR
		source.ScreeningLanguageEnglish IS NOT NULL OR
		source.ScreeningLanguageGerman IS NOT NULL OR
		source.ScreeningLanguageOther IS NOT NULL OR
		source.ScreeningContractExpectations IS NOT NULL OR
		source.ScreeningAvailability IS NOT NULL OR
		source.ScreeningOtherProcesses IS NOT NULL OR
		source.ScreeningCounterOfferCriteria IS NOT NULL OR
		source.ScreeningRelocationCanDo IS NOT NULL OR
		source.ScreeningRelocationComment IS NOT NULL OR
		source.ScreeningBusinessTripsCanDo IS NOT NULL OR
		source.ScreeningBusinessTripsComment IS NOT NULL OR
		source.ScreeningEveningWorkCanDo IS NOT NULL OR
		source.ScreeningEveningWorkComment IS NOT NULL OR
		source.ScreeningWorkPermitNeeded IS NOT NULL OR
		source.ScreeningWorkPermitComment IS NOT NULL 
	) OR (
		1 <= (	SELECT Count(1) FROM Recruitment.RecruitmentProcessStep WHERE Type IN (100,200)	 AND RecruitmentProcessId = source.RecruitmentProcessId
	))
	)
	WHEN NOT MATCHED BY TARGET THEN 
  INSERT	(Id, 
			ScreeningGeneralOpinion,
			ScreeningMotivation,
			ScreeningWorkplaceExpectations,
			ScreeningSkills,
			ScreeningLanguageEnglish,
			ScreeningLanguageGerman,
			ScreeningLanguageOther,
			ScreeningContractExpectations,
			ScreeningAvailability,
			ScreeningOtherProcesses,
			ScreeningCounterOfferCriteria,
			ScreeningRelocationCanDo,
			ScreeningRelocationComment,
			ScreeningBusinessTripsCanDo,
			ScreeningBusinessTripsComment,
			ScreeningEveningWorkCanDo,
			ScreeningEveningWorkComment,
			ScreeningWorkPermitNeeded,
			ScreeningWorkPermitComment)
	VALUES (RecruitmentProcessId, 
			ScreeningGeneralOpinion,
			ScreeningMotivation,
			ScreeningWorkplaceExpectations,
			ScreeningSkills,
			ScreeningLanguageEnglish,
			ScreeningLanguageGerman,
			ScreeningLanguageOther,
			ScreeningContractExpectations,
			ScreeningAvailability,
			ScreeningOtherProcesses,
			ScreeningCounterOfferCriteria,
			ScreeningRelocationCanDo,
			ScreeningRelocationComment,
			ScreeningBusinessTripsCanDo,
			ScreeningBusinessTripsComment,
			ScreeningEveningWorkCanDo,
			ScreeningEveningWorkComment,
			ScreeningWorkPermitNeeded,
			ScreeningWorkPermitComment);

			
MERGE INTO Recruitment.RecruitmentProcessFinal  AS target
USING ( 
 SELECT Id, 
		FinalPositionId,
		FinalTrialContractTypeId,
		FinalLongTermContractTypeId,
		FinalTrialSalary,
		FinalLongTermSalary,
		FinalStartDate,
		FinalSignedDate,
		FinalLocationId,
		FinalComment,
		FinalEmploymentSourceId
 FROM Recruitment.RecruitmentProcess
 ) as SOURCE 
	(   RecruitmentProcessId, 
		FinalPositionId,
		FinalTrialContractTypeId,
		FinalLongTermContractTypeId,
		FinalTrialSalary,
		FinalLongTermSalary,
		FinalStartDate,
		FinalSignedDate,
		FinalLocationId,
		FinalComment,
		FinalEmploymentSourceId)
	ON source.RecruitmentProcessId = target.Id
	AND 
	(
		source.FinalPositionId IS NOT NULL OR
		source.FinalTrialContractTypeId IS NOT NULL OR
		source.FinalLongTermContractTypeId IS NOT NULL OR
		source.FinalTrialSalary IS NOT NULL OR
		source.FinalLongTermSalary IS NOT NULL OR
		source.FinalStartDate IS NOT NULL OR
		source.FinalSignedDate IS NOT NULL OR
		source.FinalLocationId IS NOT NULL OR
		source.FinalComment IS NOT NULL OR
		source.FinalEmploymentSourceId IS NOT NULL 
	) OR (
		1 <= (	SELECT Count(1) FROM Recruitment.RecruitmentProcessStep WHERE Type IN (900) AND RecruitmentProcessId = source.RecruitmentProcessId
	))
  WHEN NOT MATCHED BY TARGET THEN 
  INSERT (Id, 
		FinalPositionId,
		FinalTrialContractTypeId,
		FinalLongTermContractTypeId,
		FinalTrialSalary,
		FinalLongTermSalary,
		FinalStartDate,
		FinalSignedDate,
		FinalLocationId,
		FinalComment,
		FinalEmploymentSourceId)
		VALUES (RecruitmentProcessId, 
		FinalPositionId,
		FinalTrialContractTypeId,
		FinalLongTermContractTypeId,
		FinalTrialSalary,
		FinalLongTermSalary,
		FinalStartDate,
		FinalSignedDate,
		FinalLocationId,
		FinalComment,
		FinalEmploymentSourceId);

		
MERGE INTO Recruitment.RecruitmentProcessOffer  AS target
USING ( 
 SELECT Id, 
		OfferPositionId,
		OfferContractType,
		OfferSalary,
		OfferStartDate,
		OfferLocationId,
		OfferComment
 FROM Recruitment.RecruitmentProcess
 ) as SOURCE 
	(   RecruitmentProcessId, 
		OfferPositionId,
		OfferContractType,
		OfferSalary,
		OfferStartDate,
		OfferLocationId,
		OfferComment)
	ON source.RecruitmentProcessId = target.Id
	AND 
	(
		source.OfferPositionId IS NOT NULL OR
		source.OfferContractType IS NOT NULL OR
		source.OfferSalary IS NOT NULL OR
		source.OfferStartDate IS NOT NULL OR
		source.OfferLocationId IS NOT NULL OR
		source.OfferComment IS NOT NULL 
	) OR (
		1 <= (	SELECT Count(1) FROM Recruitment.RecruitmentProcessStep WHERE Type IN (900)	 AND RecruitmentProcessId = source.RecruitmentProcessId
	))
  WHEN NOT MATCHED BY TARGET THEN 
  INSERT (Id, 
		OfferPositionId,
		OfferContractType,
		OfferSalary,
		OfferStartDate,
		OfferLocationId,
		OfferComment)
		VALUES (RecruitmentProcessId, 
		OfferPositionId,
		OfferContractType,
		OfferSalary,
		OfferStartDate,
		OfferLocationId,
		OfferComment);