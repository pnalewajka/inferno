namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AdjustInboundEmailMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Recruitment.InboundEmail", name: "RecruiterId", newName: "ProcessedById");
            RenameIndex(table: "Recruitment.InboundEmail", name: "IX_RecruiterId", newName: "IX_ProcessedById");
            RenameColumn(table: "Recruitment.InboundEmail", name: "From", newName: "FromEmailAddress");
            AddColumn("Recruitment.InboundEmail", "FromFullName", c => c.String());
            AddColumn("Recruitment.InboundEmail", "TriageResult", c => c.Int(nullable: false));
            DropColumn("Recruitment.InboundEmail", "IsAboutCandidate");
            DropColumn("Recruitment.InboundEmail", "IsAboutRecommendingPerson");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.InboundEmail", "IsAboutRecommendingPerson", c => c.Boolean(nullable: false));
            AddColumn("Recruitment.InboundEmail", "IsAboutCandidate", c => c.Boolean(nullable: false));
            DropColumn("Recruitment.InboundEmail", "TriageResult");
            DropColumn("Recruitment.InboundEmail", "FromFullName");
            RenameColumn(table: "Recruitment.InboundEmail", name: "FromEmailAddress", newName: "From");
            RenameIndex(table: "Recruitment.InboundEmail", name: "IX_ProcessedById", newName: "IX_RecruiterId");
            RenameColumn(table: "Recruitment.InboundEmail", name: "ProcessedById", newName: "RecruiterId");
        }
    }
}
