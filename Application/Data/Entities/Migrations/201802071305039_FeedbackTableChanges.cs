namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FeedbackTableChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("MeetMe.Feedback", "FeedbackContent", c => c.String(maxLength: 1000));
            AddColumn("Workflows.FeedbackRequest", "FeedbackContent", c => c.String(maxLength: 1000));
            DropColumn("MeetMe.Feedback", "PositiveFeedback");
            DropColumn("MeetMe.Feedback", "NegativeFeedback");
            DropColumn("MeetMe.Feedback", "Remarks");
            DropColumn("Workflows.FeedbackRequest", "PositiveFeedback");
            DropColumn("Workflows.FeedbackRequest", "NegativeFeedback");
            DropColumn("Workflows.FeedbackRequest", "Remarks");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.FeedbackRequest", "Remarks", c => c.String(maxLength: 1000));
            AddColumn("Workflows.FeedbackRequest", "NegativeFeedback", c => c.String(maxLength: 1000));
            AddColumn("Workflows.FeedbackRequest", "PositiveFeedback", c => c.String(maxLength: 1000));
            AddColumn("MeetMe.Feedback", "Remarks", c => c.String(maxLength: 1000));
            AddColumn("MeetMe.Feedback", "NegativeFeedback", c => c.String(maxLength: 1000));
            AddColumn("MeetMe.Feedback", "PositiveFeedback", c => c.String(maxLength: 1000));
            DropColumn("Workflows.FeedbackRequest", "FeedbackContent");
            DropColumn("MeetMe.Feedback", "FeedbackContent");
        }
    }
}
