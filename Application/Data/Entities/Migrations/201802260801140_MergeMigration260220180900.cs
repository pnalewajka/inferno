namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration260220180900 : DbMigration
    {
        public override void Up()
        {
            /*DropIndex("Workflows.AdvancedPaymentRequest", new[] { "BusinessTripParticipant_Id" });
            RenameColumn(table: "Workflows.AdvancedPaymentRequest", name: "BusinessTripParticipant_Id", newName: "ParticipantId");
            CreateTable(
                "Organization.CompanyAllowedAdvancedPaymentCurrencies",
                c => new
                    {
                        CompanyId = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.CurrencyId })
                .ForeignKey("Dictionaries.Company", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("BusinessTrips.Currency", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.CurrencyId);
            
            AddColumn("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId", c => c.Long(nullable: false));
            AddColumn("Dictionaries.Company", "DefaultAdvancedPaymentDailyAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("Workflows.AdvancedPaymentRequest", "ParticipantId", c => c.Long(nullable: false));
            CreateIndex("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId");
            CreateIndex("Workflows.AdvancedPaymentRequest", "ParticipantId");
            AddForeignKey("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId", "BusinessTrips.Currency", "Id");*/
        }
        
        public override void Down()
        {
            /*DropForeignKey("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CurrencyId", "BusinessTrips.Currency");
            DropForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CompanyId", "Dictionaries.Company");
            DropIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", new[] { "CurrencyId" });
            DropIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", new[] { "CompanyId" });
            DropIndex("Workflows.AdvancedPaymentRequest", new[] { "ParticipantId" });
            DropIndex("Dictionaries.Company", new[] { "DefaultAdvancedPaymentCurrencyId" });
            AlterColumn("Workflows.AdvancedPaymentRequest", "ParticipantId", c => c.Long());
            DropColumn("Dictionaries.Company", "DefaultAdvancedPaymentDailyAmount");
            DropColumn("Dictionaries.Company", "DefaultAdvancedPaymentCurrencyId");
            DropTable("Organization.CompanyAllowedAdvancedPaymentCurrencies");
            RenameColumn(table: "Workflows.AdvancedPaymentRequest", name: "ParticipantId", newName: "BusinessTripParticipant_Id");
            CreateIndex("Workflows.AdvancedPaymentRequest", "BusinessTripParticipant_Id");*/
        }
    }
}
