﻿SET XACT_ABORT ON
GO

IF (SELECT COL_LENGTH('[Workflows].[RequestHistoryEntry]','ActionIdentifier')) IS NULL
BEGIN
    ALTER TABLE [Workflows].[RequestHistoryEntry] ADD [ActionIdentifier] [nvarchar](128)
    ALTER TABLE [Workflows].[RequestHistoryEntry] ADD [IsActionActive] [bit]
    ALTER TABLE [Workflows].[RequestHistoryEntry] ALTER COLUMN [JiraIssueKey] [nvarchar](64) NULL

    EXEC('UPDATE [Workflows].[RequestHistoryEntry] SET [IsActionActive] = 1 WHERE [JiraIssueKey] IS NOT NULL OR [EmailSubject] IS NOT NULL')
END
