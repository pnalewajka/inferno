﻿SET XACT_ABORT ON
GO

ALTER TABLE [BusinessTrips].[BusinessTrip]
DROP CONSTRAINT [FK_BusinessTrips.BusinessTrip_Workflows.Request_RequestId]

ALTER TABLE [BusinessTrips].[BusinessTripAcceptanceConditions]
DROP CONSTRAINT [FK_BusinessTrips.BusinessTripAcceptanceConditions_BusinessTrips.BusinessTrip_BusinessTrip_Id]

ALTER TABLE [BusinessTrips].[BusinessTripDepartureCityMap]
DROP CONSTRAINT [FK_BusinessTrips.BusinessTripDepartureCityMap_BusinessTrips.BusinessTrip_BusinessTripId]

ALTER TABLE [BusinessTrips].[BusinessTripParticipant]
DROP CONSTRAINT [FK_BusinessTrips.BusinessTripParticipant_BusinessTrips.BusinessTrip_BusinessTripId]

ALTER TABLE [BusinessTrips].[BusinessTripMessage]
DROP CONSTRAINT [FK_BusinessTrips.BusinessTripMessage_BusinessTrips.BusinessTrip_BusinessTripId]

ALTER TABLE [Workflows].[AdvancedPaymentRequest]
DROP CONSTRAINT [FK_Workflows.AdvancedPaymentRequest_BusinessTrips.BusinessTrip_BusinessTripId]

ALTER TABLE [BusinessTrips].[BusinessTripProjectsMap]
DROP CONSTRAINT [FK_BusinessTrips.BusinessTripProjectsMap_BusinessTrips.BusinessTrip_BusinessTripId]

ALTER TABLE [BusinessTrips].[Arrangement]
DROP CONSTRAINT [FK_BusinessTrips.Arrangement_BusinessTrips.BusinessTrip_BusinessTripId]

ALTER TABLE [BusinessTrips].[BusinessTrip]
DROP CONSTRAINT [PK_BusinessTrips.BusinessTrip]

ALTER TABLE [BusinessTrips].[BusinessTrip]
DROP COLUMN [Id]

EXEC sp_rename 'BusinessTrips.BusinessTrip.Request_Id', 'Id', 'COLUMN';

DROP INDEX [IX_Request_Id] ON [BusinessTrips].[BusinessTrip]

ALTER TABLE [BusinessTrips].[BusinessTrip]
ALTER COLUMN [Id] BIGINT NOT NULL

ALTER TABLE [BusinessTrips].[BusinessTrip]
ADD CONSTRAINT [PK_BusinessTrips.BusinessTrip] PRIMARY KEY ([Id])
