namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AlterRecruitmentCandidateMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.Candidate", "LastFollowUpReminderDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.Candidate", "LastFollowUpReminderDate");
        }
    }
}
