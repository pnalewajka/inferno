﻿SET XACT_ABORT ON
GO

IF NOT EXISTS (SELECT [Id] FROM [BusinessTrips].[Currency] WHERE [IsoCode] = 'EUR')
BEGIN
	INSERT INTO [BusinessTrips].[Currency] ([Name], [IsoCode], [Symbol], [ModifiedOn])
	VALUES ('euro', 'EUR', '€', GETDATE())
END

DECLARE @euro BIGINT = (SELECT [Id] FROM [BusinessTrips].[Currency] WHERE [IsoCode] = 'EUR')
DECLARE @pln BIGINT = (SELECT [Id] FROM [BusinessTrips].[Currency] WHERE [IsoCode] = 'PLN')

UPDATE [Dictionaries].[Company] SET [DefaultAdvancedPaymentCurrencyId] = IIF([ActiveDirectoryCode] IN ('BlStreamInc', 'SmtSoftware', 'BlStream'), @pln, @euro)
UPDATE [Dictionaries].[Company] SET [DefaultAdvancedPaymentDailyAmount] = '30' WHERE [DefaultAdvancedPaymentCurrencyId] = @pln
UPDATE [Dictionaries].[Company] SET [DefaultAdvancedPaymentDailyAmount] = '7.5' WHERE [DefaultAdvancedPaymentCurrencyId] = @euro