namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ActiveDirectorySubgroupsMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Allocation.ActiveDirectorySubgroups",
                c => new
                    {
                        ParentActiveDirectoryGroupId = c.Long(nullable: false),
                        ChildActiveDirectoryGroupId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ParentActiveDirectoryGroupId, t.ChildActiveDirectoryGroupId })
                .ForeignKey("Allocation.ActiveDirectoryGroup", t => t.ParentActiveDirectoryGroupId)
                .ForeignKey("Allocation.ActiveDirectoryGroup", t => t.ChildActiveDirectoryGroupId)
                .Index(t => t.ParentActiveDirectoryGroupId)
                .Index(t => t.ChildActiveDirectoryGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Allocation.ActiveDirectorySubgroups", "ChildActiveDirectoryGroupId", "Allocation.ActiveDirectoryGroup");
            DropForeignKey("Allocation.ActiveDirectorySubgroups", "ParentActiveDirectoryGroupId", "Allocation.ActiveDirectoryGroup");
            DropIndex("Allocation.ActiveDirectorySubgroups", new[] { "ChildActiveDirectoryGroupId" });
            DropIndex("Allocation.ActiveDirectorySubgroups", new[] { "ParentActiveDirectoryGroupId" });
            DropTable("Allocation.ActiveDirectorySubgroups");
        }
    }
}
