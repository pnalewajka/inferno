﻿SET XACT_ABORT ON
GO

DELETE FROM [Inferno].[GDPR].[DataActivity]
GO

DELETE FROM [Inferno].[GDPR].[DataOwner]
GO

ALTER PROCEDURE [GDPR].[LogUserDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @employeeId BIGINT
	DECLARE @dataOwner BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()

	SELECT @employeeId = e.Id FROM [Allocation].[Employee] as e
		JOIN [Accounts].[User] as u
		ON e.UserId = u.Id
		WHERE u.Id = @identity

	SELECT @dataOwner = Id FROM [GDPR].[DataOwner] WHERE UserId = @identity OR EmployeeId = @employeeId

	IF @dataOwner IS NULL
	BEGIN
		INSERT INTO [GDPR].[DataOwner] 
			(EmployeeId, UserId, ModifiedOn) 
		VALUES
			(@employeeId, @identity, @currentDate)

		SET @dataOwner = SCOPE_IDENTITY()
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwner, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO

ALTER PROCEDURE [GDPR].[LogEmployeeDataActivity]
	@identity BIGINT,
	@activityType INT,
	@referenceType INT,
	@referenceId BIGINT,
	@detail1 NVARCHAR(50),
	@detail2 NVARCHAR(50),
	@detail3 NVARCHAR(50),
	@currentUserId BIGINT
AS
BEGIN
	DECLARE @userId BIGINT
	DECLARE @dataOwner BIGINT = NULL
	DECLARE @currentDate DateTime = GETDATE()

	SELECT @userId = UserId FROM [Allocation].[Employee] WHERE Id = @identity

	SELECT @dataOwner = Id FROM [GDPR].[DataOwner] WHERE EmployeeId = @identity OR UserId = @userId

	IF @dataOwner IS NULL
	BEGIN
		INSERT INTO [GDPR].[DataOwner] 
			(EmployeeId, UserId, ModifiedOn) 
		VALUES
			(@identity,	@userId,@currentDate)

		SET @dataOwner = SCOPE_IDENTITY()
	END
	
	INSERT INTO [GDPR].[DataActivity] (DataOwnerId, ActivityType, ReferenceType, ReferenceId, PerformedOn, PerformedByUserId, S1, S2, S3, ModifiedOn)
	VALUES (@dataOwner, @activityType, @referenceType, @referenceId, @currentDate, @currentUserId, @detail1, @detail2, @detail3, @currentDate);
END
GO