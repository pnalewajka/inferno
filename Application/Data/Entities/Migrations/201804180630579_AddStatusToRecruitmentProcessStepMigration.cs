namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddStatusToRecruitmentProcessStepMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcessStep", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.RecruitmentProcessStep", "Status");
        }
    }
}
