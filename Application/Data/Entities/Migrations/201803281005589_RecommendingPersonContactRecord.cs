namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RecommendingPersonContactRecord : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Recruitment.RecommendingPersonContactRecord",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    RecommendingPersonId = c.Long(nullable: false),
                    ContactType = c.Int(nullable: false),
                    ContactedOn = c.DateTime(nullable: false),
                    ContactedById = c.Long(nullable: false),
                    RecommendingPersonReaction = c.Int(nullable: false),
                    Comment = c.String(),
                    UserIdCreatedBy = c.Long(),
                    UserIdImpersonatedCreatedBy = c.Long(),
                    CreatedOn = c.DateTime(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    UserIdImpersonatedBy = c.Long(),
                    UserIdModifiedBy = c.Long(),
                    ModifiedOn = c.DateTime(nullable: false),
                    Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Allocation.Employee", t => t.ContactedById)
                .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Recruitment.RecommendingPerson", t => t.RecommendingPersonId)
                .Index(t => t.RecommendingPersonId)
                .Index(t => t.ContactedById)
                .Index(t => t.UserIdCreatedBy)
                .Index(t => t.UserIdImpersonatedCreatedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);

            CreateTable(
                "Recruitment.RecommendingPersonContactRecordJobOpenings",
                c => new
                {
                    RecommendingPersonContactRecordId = c.Long(nullable: false),
                    JobOpeningId = c.Long(nullable: false),
                })
                .PrimaryKey(t => new { t.RecommendingPersonContactRecordId, t.JobOpeningId })
                .ForeignKey("Recruitment.RecommendingPersonContactRecord", t => t.RecommendingPersonContactRecordId, cascadeDelete: true)
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId, cascadeDelete: true)
                .Index(t => t.RecommendingPersonContactRecordId)
                .Index(t => t.JobOpeningId);

        }

        public override void Down()
        {
            DropForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "JobOpeningId", "Recruitment.JobOpening");
            DropForeignKey("Recruitment.RecommendingPersonContactRecordJobOpenings", "RecommendingPersonContactRecordId", "Recruitment.RecommendingPersonContactRecord");
            DropForeignKey("Recruitment.RecommendingPersonContactRecord", "RecommendingPersonId", "Recruitment.RecommendingPerson");
            DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPersonContactRecord", "UserIdCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.RecommendingPersonContactRecord", "ContactedById", "Allocation.Employee");
            DropIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", new[] { "JobOpeningId" });
            DropIndex("Recruitment.RecommendingPersonContactRecordJobOpenings", new[] { "RecommendingPersonContactRecordId" });
            DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdModifiedBy" });
            DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdImpersonatedBy" });
            DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "UserIdCreatedBy" });
            DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "ContactedById" });
            DropIndex("Recruitment.RecommendingPersonContactRecord", new[] { "RecommendingPersonId" });
            DropTable("Recruitment.RecommendingPersonContactRecordJobOpenings");
            DropTable("Recruitment.RecommendingPersonContactRecord");
        }
    }
}
