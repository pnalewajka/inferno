namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddFinalToRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalPositionId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "FinalStartDate", c => c.DateTime());
            AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "FinalComment", c => c.String(maxLength: 255));
            CreateIndex("Recruitment.RecruitmentProcess", "FinalPositionId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType");
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLongTermContractTypeId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalTrialContractTypeId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalPositionId" });
            DropColumn("Recruitment.RecruitmentProcess", "FinalComment");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalStartDate");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary");
            DropColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary");
            DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            DropColumn("Recruitment.RecruitmentProcess", "FinalPositionId");
        }
    }
}
