﻿-- Currencies

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Currency] WHERE [IsoCode] = N'CHF')
BEGIN
  INSERT INTO [Dictionaries].[Currency]
  (
    [Name],
    [IsoCode],
    [Symbol],
    [ModifiedOn]
  )
  VALUES
  (
    N'Swiss franc',
    N'CHF',
    N'Fr',
    '2018-03-20 00:00:00'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Currency] WHERE [IsoCode] = N'DKK')
BEGIN
  INSERT INTO [Dictionaries].[Currency]
  (
    [Name],
    [IsoCode],
    [Symbol],
    [ModifiedOn]
  )
  VALUES
  (
    N'Danish krone',
    N'DKK',
    N'kr',
    '2018-03-20 00:00:00'
  )
END

UPDATE [Dictionaries].[Currency]
   SET [Name] = N'Polish złoty'
 WHERE [IsoCode] = N'PLN'

UPDATE [Dictionaries].[Currency]
   SET [Name] = N'United States dollar'
 WHERE [IsoCode] = N'USD'

UPDATE [Dictionaries].[Currency]
   SET [Name] = N'Swedish krona'
 WHERE [IsoCode] = N'SEK'

UPDATE [Dictionaries].[Currency]
   SET [Name] = N'Pound Sterling'
 WHERE [IsoCode] = N'GBP'

 -- Countries

UPDATE [Dictionaries].[Country]
   SET [IsoCode] = N'AT'
 WHERE [IsoCode] = N'AU'

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'AR')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Argentina',
    N'AR'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'BY')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Belarus',
    N'BY'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'BE')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Belgium',
    N'BE'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'BG')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Bulgaria',
    N'BG'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'HR')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Croatia',
    N'HR'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'CY')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Cyprus',
    N'CY'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'CZ')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Czech Republic',
    N'CZ'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'DK')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Denmark',
    N'DK'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'EE')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Estonia',
    N'EE'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'FI')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Finland',
    N'FI'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'GR')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Greece',
    N'GR'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'HU')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Hungary',
    N'HU'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'IE')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Ireland',
    N'IE'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'IT')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Italy',
    N'IT'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LV')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Latvia',
    N'LV'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LI')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Liechtenstein',
    N'LI'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LT')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Lithuania',
    N'LT'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LU')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Luxembourg',
    N'LU'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'MT')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Malta',
    N'MT'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'NL')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Netherlands',
    N'NL'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'PT')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Portugal',
    N'PT'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'RO')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Romania',
    N'RO'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'RU')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Russia',
    N'RU'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'SK')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Slovakia',
    N'SK'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'SI')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Slovenia',
    N'SI'
  )
END

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'CH')
BEGIN
  INSERT INTO [Dictionaries].[Country]
  (
      [Name],
      [IsoCode]
  )
  VALUES
  (
    N'Switzerland',
    N'CH'
  )
END

 -- Daily allowances

DECLARE @pl bigint = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'PL')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 150
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'AR')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 130
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'AT')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 130
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'BY')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 160
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'BE')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 120
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'BG')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 125
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'HR')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 160
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'CY')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 120
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'CZ')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 1300
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'DK')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 100
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'EE')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 160
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'FI')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 180
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'FR')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 150
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'DE')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 140
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'GR')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 130
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'HU')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 160
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'IE')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 174
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'IT')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 132
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'LV')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 200
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'LI')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 130
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'LT')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 160
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'LU')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 180
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'MT')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 130
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'NL')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 600
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'PL')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 120
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'PT')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 100
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'RO')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 200
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'RU')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 120
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'SK')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 130
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'SI')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 160
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'ES')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 1800
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'SE')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 200
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'CH')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 200
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'GB')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 200
 WHERE [ValidFrom] = '2013-03-01'
   AND [EmploymentCountryId] = @pl
   AND [TravelCountryId] = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] Where [IsoCode] = N'US')
