﻿declare @roleId bigint = (
SELECT top 1 [Id]
  FROM [Accounts].[SecurityRole]
  where [Name] = 'CanDeleteBusinessTripRequests'
);

delete from [Accounts].[SecurityRoleSecurityProfiles] where RoleId = @roleId;
delete from [Accounts].[UserSecurityRoles] where RoleId = @roleId;
delete from [Accounts].[SecurityRole] where Id = @roleId;

delete from [Workflows].[BusinessTripSettlementRequestAdvancePayment]
delete from [Workflows].[BusinessTripSettlementRequestAbroadTimeEntry]
delete from [Workflows].[BusinessTripSettlementRequestCompanyCost]
delete from [Workflows].[BusinessTripSettlementRequestMeal]
delete from [Workflows].[BusinessTripSettlementRequestOwnCost]
delete from [Workflows].[BusinessTripSettlementRequestVehicleMileage]

delete from [Workflows].RequestHistoryEntry
where RequestId in (
	select Id from [Workflows].[BusinessTripSettlementRequest]
)

delete from [Workflows].[Approvals]
where Id in (
	select Id from [Workflows].ApprovalGroup d
	where d.RequestId in ( select Id from [Workflows].[BusinessTripSettlementRequest] )
)

delete from [Workflows].ApprovalGroup
where RequestId in (
	select Id from [Workflows].[BusinessTripSettlementRequest]
)

delete from [Workflows].[RequestDocumentContent]
where Id in (
	select Id from [Workflows].RequestDocument d
	where d.RequestId in ( select Id from [Workflows].[BusinessTripSettlementRequest] )
)

delete from [Workflows].RequestDocument
where RequestId in (
	select Id from [Workflows].[BusinessTripSettlementRequest]
)

delete from [Workflows].[Request]
where Id in (
	select Id from [Workflows].[BusinessTripSettlementRequest]
)

delete from [Workflows].[BusinessTripSettlementRequest]
