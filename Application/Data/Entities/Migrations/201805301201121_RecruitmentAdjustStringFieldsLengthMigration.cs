﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RecruitmentAdjustStringFieldsLengthMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805301201121_RecruitmentAdjustStringFieldsLengthMigration.AdjustStringFieldsLength.Up.sql");
        }

        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805301201121_RecruitmentAdjustStringFieldsLengthMigration.AdjustStringFieldsLength.Down.sql");
        }
    }
}


