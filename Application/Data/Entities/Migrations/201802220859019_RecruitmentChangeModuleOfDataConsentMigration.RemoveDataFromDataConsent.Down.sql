﻿SET XACT_ABORT ON
GO

ALTER TABLE [Recruitment].[DataConsent]    
ADD CONSTRAINT [FK_Recruitment.DataConsent_Recruitment.DataOrigin_DataOriginId] FOREIGN KEY (ApplicationOriginId)     
    REFERENCES [Recruitment].[ApplicationOrigin] ([Id])  
