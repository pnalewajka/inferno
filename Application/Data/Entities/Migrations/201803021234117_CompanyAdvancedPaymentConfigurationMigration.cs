namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanyAdvancedPaymentConfigurationMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CompanyId", "Dictionaries.Company");
            DropForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CurrencyId", "Dictionaries.Currency");
            DropIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", new[] { "CompanyId" });
            DropIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", new[] { "CurrencyId" });
            CreateTable(
                "Dictionaries.CompanyAllowedAdvancePaymentCurrency",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CompanyId = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                        MinimumValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Dictionaries.Company", t => t.CompanyId)
                .ForeignKey("Dictionaries.Currency", t => t.CurrencyId)
                .Index(t => new { t.CompanyId, t.CurrencyId }, unique: true, name: "IX_CurrencyPerCompany");
            
            AddColumn("Dictionaries.Company", "AllowedAccomodations", c => c.Int(nullable: false));
            AddColumn("Dictionaries.Company", "AllowedMeansOfTransport", c => c.Int(nullable: false));
            DropTable("Organization.CompanyAllowedAdvancedPaymentCurrencies");
        }
        
        public override void Down()
        {
            CreateTable(
                "Organization.CompanyAllowedAdvancedPaymentCurrencies",
                c => new
                    {
                        CompanyId = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.CurrencyId });
            
            DropForeignKey("Dictionaries.CompanyAllowedAdvancePaymentCurrency", "CurrencyId", "Dictionaries.Currency");
            DropForeignKey("Dictionaries.CompanyAllowedAdvancePaymentCurrency", "CompanyId", "Dictionaries.Company");
            DropIndex("Dictionaries.CompanyAllowedAdvancePaymentCurrency", "IX_CurrencyPerCompany");
            DropColumn("Dictionaries.Company", "AllowedMeansOfTransport");
            DropColumn("Dictionaries.Company", "AllowedAccomodations");
            DropTable("Dictionaries.CompanyAllowedAdvancePaymentCurrency");
            CreateIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CurrencyId");
            CreateIndex("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CompanyId");
            AddForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CurrencyId", "Dictionaries.Currency", "Id", cascadeDelete: true);
            AddForeignKey("Organization.CompanyAllowedAdvancedPaymentCurrencies", "CompanyId", "Dictionaries.Company", "Id", cascadeDelete: true);
        }
    }
}
