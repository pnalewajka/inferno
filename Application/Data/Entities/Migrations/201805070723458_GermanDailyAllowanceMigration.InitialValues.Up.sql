﻿DECLARE @poland bigint = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'PL')
DECLARE @usd bigint = (SELECT TOP 1 [Id] FROM [Dictionaries].[Currency] WHERE [IsoCode] = N'USD')

INSERT INTO [Dictionaries].[City]
            ([Name] 
            ,[CountryId]
            ,[IsCompanyApartmentAvailable]
            ,[IsVoucherServiceAvailable])
     VALUES (N'Washington', (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'US'), 0, 0)

INSERT INTO [BusinessTrips].[DailyAllowance]
           ([ValidFrom]
           ,[ValidTo]
           ,[EmploymentCountryId]
           ,[TravelCityId]
           ,[CurrencyId]
           ,[Allowance]
           ,[AccommodationLumpSum]
           ,[UserIdImpersonatedBy]
           ,[UserIdModifiedBy]
           ,[ModifiedOn])
    VALUES ('2013-03-01', NULL, @poland, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'New York City'), @usd, 59, 87.5, NULL, NULL, '2018-05-07 00:00:00'),
           ('2013-03-01', NULL, @poland, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Washington'),    @usd, 59, 75,   NULL, NULL, '2018-05-07 00:00:00')

DECLARE @germany bigint = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'DE')
DECLARE @eur bigint = (SELECT TOP 1 [Id] FROM [Dictionaries].[Currency] WHERE [IsoCode] = N'EUR')

UPDATE [Dictionaries].[City]
   SET [Name] = N'Milan'
 WHERE [Name] = N'Mediolan'

INSERT INTO [BusinessTrips].[DailyAllowance]
           ([ValidFrom]
           ,[ValidTo]
           ,[EmploymentCountryId]
           ,[TravelCountryId]
           ,[CurrencyId]
           ,[Allowance]
           ,[DepartureAndArrivalDayAllowance]
           ,[AccommodationLumpSum]
           ,[UserIdImpersonatedBy]
           ,[UserIdModifiedBy]
           ,[ModifiedOn])
    VALUES ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'AR'), @eur, 34, 23, 144, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'AU'), @eur, 51, 34, 158, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'AT'), @eur, 36, 24, 104, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'BY'), @eur, 20, 13, 98,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'BE'), @eur, 42, 28, 135, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'BG'), @eur, 22, 15, 90,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'HR'), @eur, 28, 19, 75,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'CY'), @eur, 45, 30, 116, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'CZ'), @eur, 35, 24, 94,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'DK'), @eur, 58, 39, 143, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'EE'), @eur, 27, 18, 71,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'FI'), @eur, 50, 33, 136, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'FR'), @eur, 44, 29, 115, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'DE'), @eur, 24, 12, 20,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'GR'), @eur, 36, 24, 89,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'HU'), @eur, 22, 15, 63,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'IE'), @eur, 44, 29, 92,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'IT'), @eur, 34, 23, 126, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'JP'), @eur, 51, 34, 156, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LV'), @eur, 30, 20, 80,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LI'), @eur, 53, 36, 180, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LT'), @eur, 24, 16, 68,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'LU'), @eur, 47, 32, 130, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'MT'), @eur, 45, 30, 112, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'NL'), @eur, 46, 31, 119, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'PL'), @eur, 27, 18, 50,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'PT'), @eur, 36, 24, 102, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'RO'), @eur, 26, 17, 62,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'RU'), @eur, 24, 16, 58,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'SK'), @eur, 24, 16, 85,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'SI'), @eur, 33, 22, 95,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'ES'), @eur, 29, 20, 88,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'SE'), @eur, 50, 33, 168, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'CH'), @eur, 62, 41, 169, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'UA'), @eur, 32, 21, 98,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'GB'), @eur, 45, 30, 115, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'US'), @eur, 51, 34, 138, NULL, NULL, '2018-05-07 00:00:00')

INSERT INTO [BusinessTrips].[DailyAllowance]
           ([ValidFrom]
           ,[ValidTo]
           ,[EmploymentCountryId]
           ,[TravelCityId]
           ,[CurrencyId]
           ,[Allowance]
           ,[DepartureAndArrivalDayAllowance]
           ,[AccommodationLumpSum]
           ,[UserIdImpersonatedBy]
           ,[UserIdModifiedBy]
           ,[ModifiedOn])
    VALUES ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Canberra'),         @eur, 51, 34, 158, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Paris'),            @eur, 58, 39, 152, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Rome'),             @eur, 52, 35, 160, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Milan'),            @eur, 39, 26, 156, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Tokyo'),            @eur, 66, 44, 233, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Wroclaw'),          @eur, 33, 22, 92,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Gdansk'),           @eur, 29, 20, 77,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Krakow'),           @eur, 28, 19, 88,  NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Warszawa'),         @eur, 30, 20, 105, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Saint Petersburg'), @eur, 26, 17, 114, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Barcelona'),        @eur, 32, 21, 118, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'London'),           @eur, 62, 41, 224, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'Los Angeles'),      @eur, 56, 37, 274, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'San Franciso'),     @eur, 51, 34, 314, NULL, NULL, '2018-05-07 00:00:00'),
           ('2018-01-01', NULL, @germany, (SELECT TOP 1 [Id] FROM [Dictionaries].[City] WHERE [Name] = N'New York City'),    @eur, 58, 39, 282, NULL, NULL, '2018-05-07 00:00:00')
