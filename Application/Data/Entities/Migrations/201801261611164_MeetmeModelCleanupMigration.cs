namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MeetmeModelCleanupMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("MeetMe.Meeting", "EmployeeIdLineManager", "Allocation.Employee");
            DropForeignKey("MeetMe.Meeting", "LocationIdLocation", "Dictionaries.Location");
            DropIndex("MeetMe.Meeting", new[] { "EmployeeIdLineManager" });
            DropIndex("MeetMe.Meeting", new[] { "LocationIdLocation" });
            DropColumn("MeetMe.Meeting", "EmployeeIdLineManager");
            DropColumn("MeetMe.Meeting", "LocationIdLocation");
        }
        
        public override void Down()
        {
            AddColumn("MeetMe.Meeting", "LocationIdLocation", c => c.Long(nullable: false));
            AddColumn("MeetMe.Meeting", "EmployeeIdLineManager", c => c.Long());
            CreateIndex("MeetMe.Meeting", "LocationIdLocation");
            CreateIndex("MeetMe.Meeting", "EmployeeIdLineManager");
            AddForeignKey("MeetMe.Meeting", "LocationIdLocation", "Dictionaries.Location", "Id");
            AddForeignKey("MeetMe.Meeting", "EmployeeIdLineManager", "Allocation.Employee", "Id");
        }
    }
}
