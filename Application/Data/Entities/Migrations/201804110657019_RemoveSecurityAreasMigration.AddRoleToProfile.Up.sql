﻿SET XACT_ABORT ON
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [Accounts].[atomic_AddRoleToProfile]
 	@roleName VARCHAR(255),
	@profileName VARCHAR(255)
AS
BEGIN
	DECLARE @roleId BIGINT
	DECLARE @profileId BIGINT

	SELECT @roleId = [Id] FROM [Accounts].[SecurityRole] WHERE [Name] = @roleName
	SELECT @profileId = [Id] FROM [Accounts].[SecurityProfile] WHERE [Name] = @profileName

	IF @profileId IS NULL
	BEGIN
		DECLARE @errorMsg VARCHAR(700) = ''
		SET @errorMsg = @errorMsg + CASE WHEN (LEN(@errorMsg)>0) THEN ' ' ELSE '' END + 'Profile: ' + ISNULL(@roleName,'') + ' is missing'
		RAISERROR(@errorMsg, 16, 1);
	END

	IF @roleId IS NULL
	BEGIN
		SELECT @roleId = COALESCE(MAX([Id]), 0) + 1 FROM [Accounts].[SecurityRole]
		INSERT INTO [Accounts].[SecurityRole] ([Id], [Name]) VALUES (@roleId, @roleName)
	END
END
GO

