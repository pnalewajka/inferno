namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ProjectInvoicingConfigurationMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Allocation.ProjectInvoicingRate",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProjectId = c.Long(nullable: false),
                        Code = c.String(nullable: false, maxLength: 128),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
                .Index(t => new { t.ProjectId, t.Code }, unique: true, name: "IX_ProjectCodePerProject");

            AddColumn("Allocation.Project", "InvoicingAlgorithm", c => c.String());

            DropIndex("Allocation.ProjectInvoicingRate", "IX_ProjectCodePerProject");
            CreateIndex("Allocation.ProjectInvoicingRate", new[] { "ProjectId", "Code" }, unique: true, name: "IX_ProjectCodePerProject");
        }
        
        public override void Down()
        {
            DropIndex("Allocation.ProjectInvoicingRate", "IX_ProjectCodePerProject");
            CreateIndex("Allocation.ProjectInvoicingRate", new[] { "ProjectId", "Code" }, unique: true, clustered: true, name: "IX_ProjectCodePerProject");

            DropForeignKey("Allocation.ProjectInvoicingRate", "ProjectId", "Allocation.Project");
            DropIndex("Allocation.ProjectInvoicingRate", "IX_ProjectCodePerProject");
            DropColumn("Allocation.Project", "InvoicingAlgorithm");
            DropTable("Allocation.ProjectInvoicingRate");
        }
    }
}
