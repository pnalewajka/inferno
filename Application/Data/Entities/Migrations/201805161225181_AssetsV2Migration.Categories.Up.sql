﻿UPDATE [PeopleManagement].[AssetType]
   SET [Category] = 3
 WHERE [NameEn] LIKE N'Headset:%';

UPDATE [PeopleManagement].[AssetType]
   SET [Category] = 4
 WHERE [NameEn] LIKE N'Bag %' OR [NameEn] LIKE N'Backpack %';

UPDATE [PeopleManagement].[AssetType]
   SET [Category] = 5
 WHERE [Category] = 2 AND [NameEn] NOT LIKE N'Set for %';
