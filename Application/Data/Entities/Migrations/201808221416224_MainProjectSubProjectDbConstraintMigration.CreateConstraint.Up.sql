﻿SET XACT_ABORT ON
GO

ALTER TABLE [Allocation].[Project]
ADD CONSTRAINT [CHK_NoMainProjectWithSubprojectName] CHECK
	(1 = ([IsMainProject] ^ IIF([SubProjectShortName] IS NOT NULL, 1, 0)))