﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RemoveFlightArrivalMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804170827539_RemoveFlightArrivalMigration.Script.Up.sql");

            DropColumn("Workflows.BusinessTripSettlementRequest", "FlightArrivalDateTime");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "FlightArrivalDateTime", c => c.DateTime());
        }
    }
}
