﻿ALTER TABLE Recruitment.JobOpening  ALTER COLUMN PositionName NVARCHAR (250);
ALTER TABLE Recruitment.JobOpening  ALTER COLUMN NoticePeriod NVARCHAR (1024);
ALTER TABLE Recruitment.JobOpening  ALTER COLUMN SalaryRate NVARCHAR (300);
ALTER TABLE Recruitment.JobOpening  ALTER COLUMN RejectionReason NVARCHAR (255);