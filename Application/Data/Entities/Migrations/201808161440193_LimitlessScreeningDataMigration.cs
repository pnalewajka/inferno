namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class LimitlessScreeningDataMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.RecruitmentProcessScreening", "GeneralOpinion", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "Motivation", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "WorkplaceExpectations", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "Skills", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "LanguageEnglish", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "LanguageGerman", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "LanguageOther", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "ContractExpectations", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "Availability", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "OtherProcesses", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "CounterOfferCriteria", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "RelocationComment", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "BusinessTripsComment", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "EveningWorkComment", c => c.String());
            AlterColumn("Recruitment.RecruitmentProcessScreening", "WorkPermitComment", c => c.String());
        }

        public override void Down()
        {
            AlterColumn("Recruitment.RecruitmentProcessScreening", "WorkPermitComment", c => c.String(maxLength: 250));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "EveningWorkComment", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "BusinessTripsComment", c => c.String(maxLength: 250));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "RelocationComment", c => c.String(maxLength: 400));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "CounterOfferCriteria", c => c.String(maxLength: 400));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "OtherProcesses", c => c.String(maxLength: 400));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "Availability", c => c.String(maxLength: 400));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "ContractExpectations", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "LanguageOther", c => c.String(maxLength: 250));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "LanguageGerman", c => c.String(maxLength: 250));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "LanguageEnglish", c => c.String(maxLength: 300));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "Skills", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "WorkplaceExpectations", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "Motivation", c => c.String(maxLength: 500));
            AlterColumn("Recruitment.RecruitmentProcessScreening", "GeneralOpinion", c => c.String(maxLength: 500));
        }
    }
}
