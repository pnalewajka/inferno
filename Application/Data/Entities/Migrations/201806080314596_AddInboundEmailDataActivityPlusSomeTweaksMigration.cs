namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddInboundEmailDataActivityPlusSomeTweaksMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Recruitment.InboundEmailDataActivity",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        InboundEmailId = c.Long(nullable: false),
                        PerformedOn = c.DateTime(nullable: false),
                        PerformedByUserId = c.Long(nullable: false),
                        ActivityType = c.Int(nullable: false),
                        ReferenceId = c.Long(),
                        ReferenceType = c.Int(),
                        S1 = c.String(maxLength: 50),
                        S2 = c.String(maxLength: 50),
                        S3 = c.String(maxLength: 300),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.InboundEmail", t => t.InboundEmailId)
                .ForeignKey("Accounts.User", t => t.PerformedByUserId)
                .Index(t => t.InboundEmailId)
                .Index(t => t.PerformedByUserId);
            
            AlterColumn("GDPR.DataActivity", "S3", c => c.String(maxLength: 300));
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.InboundEmailDataActivity", "PerformedByUserId", "Accounts.User");
            DropForeignKey("Recruitment.InboundEmailDataActivity", "InboundEmailId", "Recruitment.InboundEmail");
            DropIndex("Recruitment.InboundEmailDataActivity", new[] { "PerformedByUserId" });
            DropIndex("Recruitment.InboundEmailDataActivity", new[] { "InboundEmailId" });
            AlterColumn("GDPR.DataActivity", "S3", c => c.String(maxLength: 50));
            DropTable("Recruitment.InboundEmailDataActivity");
        }
    }
}
