namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterJobOpeningMigration : DbMigration
    {
        public override void Up()
        {          
            AddColumn("Recruitment.JobOpening", "UserIdCreatedBy", c => c.Long());
            AddColumn("Recruitment.JobOpening", "UserIdImpersonatedCreatedBy", c => c.Long());
            AddColumn("Recruitment.JobOpening", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("Recruitment.JobOpening", "IsDeleted", c => c.Boolean(nullable: false));
            CreateIndex("Recruitment.JobOpening", "UserIdCreatedBy");
            CreateIndex("Recruitment.JobOpening", "UserIdImpersonatedCreatedBy");
            AddForeignKey("Recruitment.JobOpening", "UserIdCreatedBy", "Accounts.User", "Id");
            AddForeignKey("Recruitment.JobOpening", "UserIdImpersonatedCreatedBy", "Accounts.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.JobOpening", "UserIdImpersonatedCreatedBy", "Accounts.User");
            DropForeignKey("Recruitment.JobOpening", "UserIdCreatedBy", "Accounts.User");
            DropIndex("Recruitment.JobOpening", new[] { "UserIdImpersonatedCreatedBy" });
            DropIndex("Recruitment.JobOpening", new[] { "UserIdCreatedBy" });
            DropColumn("Recruitment.JobOpening", "IsDeleted");
            DropColumn("Recruitment.JobOpening", "CreatedOn");
            DropColumn("Recruitment.JobOpening", "UserIdImpersonatedCreatedBy");
            DropColumn("Recruitment.JobOpening", "UserIdCreatedBy");
        }
    }
}
