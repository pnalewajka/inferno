namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDecisionDateToProcessStepMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcessStep", "DecidedOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.RecruitmentProcessStep", "DecidedOn");
        }
    }
}
