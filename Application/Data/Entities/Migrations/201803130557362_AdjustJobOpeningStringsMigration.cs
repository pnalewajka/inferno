namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustJobOpeningStringsMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.JobOpening", "PositionName", c => c.String(maxLength: 250));
            AlterColumn("Recruitment.JobOpening", "NoticePeriod", c => c.String(maxLength: 50));
            AlterColumn("Recruitment.JobOpening", "SalaryRate", c => c.String(maxLength: 50));
            AlterColumn("Recruitment.JobOpening", "OtherLanguages", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.JobOpening", "OtherLanguages", c => c.String());
            AlterColumn("Recruitment.JobOpening", "SalaryRate", c => c.String());
            AlterColumn("Recruitment.JobOpening", "NoticePeriod", c => c.String());
            AlterColumn("Recruitment.JobOpening", "PositionName", c => c.String());
        }
    }
}
