namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsToMeetMeFeedback : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("MeetMe.Feedback", "EmployeeIdAuthor", "Allocation.Employee");
            AddColumn("MeetMe.Feedback", "EmployeeIdReceiver", c => c.Long(nullable: false));
            CreateIndex("MeetMe.Feedback", "EmployeeIdReceiver");
            AddForeignKey("MeetMe.Feedback", "EmployeeIdAuthor", "Allocation.Employee", "Id");
            AddForeignKey("MeetMe.Feedback", "EmployeeIdReceiver", "Allocation.Employee", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("MeetMe.Feedback", "EmployeeIdReceiver", "Allocation.Employee");
            DropForeignKey("MeetMe.Feedback", "EmployeeIdAuthor", "Allocation.Employee");
            DropIndex("MeetMe.Feedback", new[] { "EmployeeIdReceiver" });
            DropColumn("MeetMe.Feedback", "EmployeeIdReceiver");
            AddForeignKey("MeetMe.Feedback", "EmployeeIdAuthor", "Allocation.Employee", "Id");
        }
    }
}
