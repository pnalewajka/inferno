using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal class SystemParametersSeed
    {
        public static void Seed(CoreEntitiesContext context)
        {
            var existingParameterKeys = context.SystemParameters.Select(s => s.Key).ToHashSet();
            var newMissingParameters = GetNewParameterValues().Where(s => !existingParameterKeys.Contains(s.Key));
            
            context.SystemParameters.AddRange(newMissingParameters);
            context.SaveChanges();
        }

        public static IEnumerable<SystemParameter> GetNewParameterValues()
        {
            var propertyInfos = typeof(ParameterKeys).GetFields();

            foreach (var propertyInfo in propertyInfos)
            {
                var attribute = propertyInfo.GetCustomAttributes(typeof (SystemParameterDefinitionAttribute)).Single() as SystemParameterDefinitionAttribute;
                var key = (string)propertyInfo.GetValue(null);

                if (attribute == null)
                {
                    throw new InvalidOperationException($"SystemParameter with key: `{key}` has not SystemParameterSeed attribute");
                }

                yield return new SystemParameter
                {
                    Key = key,
                    ValueType = attribute.ValueType,
                    Value = attribute.DefaultValue,
                    ContextType = attribute.ContextType,
                    ModifiedOn = DateTime.Now
                };
            }
        }
    }
}