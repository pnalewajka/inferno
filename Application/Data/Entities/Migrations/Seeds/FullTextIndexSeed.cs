﻿using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Operations;

namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal static class FullTextIndexSeed
    {
        public static void Seed(CoreEntitiesContext context)
        {
            var entities = TypeHelper.GetAllSubClassesOf(typeof(IEntity), false);

            foreach (var entity in entities)
            {
                var tableAttribute = AttributeHelper.GetClassAttribute<TableAttribute>(entity);
                var properties = TypeHelper.GetPublicInstancePropertiesWithAttribute<FullTextSearchAttribute>(entity);

                foreach (var property in properties)
                {
                    var fullTextAttribute = AttributeHelper.GetPropertyAttribute<FullTextSearchAttribute>(property);

                    var operation = new CreateFullTextIndexOperation(
                        tableAttribute.Schema,
                        tableAttribute.Name,
                        property.Name);

                    using (var sql = new StringWriter())
                    {
                        operation.Generate(sql);
                        context.Database.ExecuteSqlCommand(operation.TransactionalBehavior, sql.ToString());
                    }

                    context.SaveChanges();
                }
            }
        }
    }
}
