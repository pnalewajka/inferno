﻿namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal static partial class SecurityRolesSeed
    {
        private struct SecurityRoleDefinition
        {
            public long Id { get; set; }
            public string Name { get; set; }
        }
    }
}
