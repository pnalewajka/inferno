﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal class SqlEnumSeed
    {
        private struct SqlEnumMetadata
        {
            public string SchemaName { get; set; }

            public string TableName { get; set; }
        }

        public static void Seed(CoreEntitiesContext context)
        {
            var sqlEnums = TypeHelper.GetLoadedTypes()
                .Where(t => t.GetCustomAttributes(typeof(SqlEnumAttribute), true).Any());

            CheckConflictingNames(sqlEnums);

            foreach (var enumType in sqlEnums)
            {
                var metadata = GetMetadata(enumType);

                // clear old table (if exists)
                context.Database.ExecuteSqlCommand("EXEC [Runtime].[ClearSqlEnum] @p0, @p1", metadata.SchemaName, metadata.TableName);

                // insert new values
                foreach (var value in EnumHelper.GetEnumValues(enumType))
                {
                    var code = value.ToString();
                    var description = value.GetLocalizedDescription();
                    var enDescription = description?.English ?? code;
                    var plDescription = description?.Polish ?? enDescription;

                    context.Database.ExecuteSqlCommand(
                        "EXEC [Runtime].[InsertSqlEnumValue] @p0, @p1, @p2, @p3, @p4, @p5",
                        new SqlParameter("@p0", SqlDbType.VarChar) { Value = metadata.SchemaName },
                        new SqlParameter("@p1", SqlDbType.VarChar) { Value = metadata.TableName },
                        new SqlParameter("@p2", SqlDbType.BigInt) { Value = Convert.ToInt64(value) },
                        new SqlParameter("@p3", SqlDbType.VarChar) { Value = code },
                        new SqlParameter("@p4", SqlDbType.NVarChar) { Value = enDescription.Replace("'", "''") },
                        new SqlParameter("@p5", SqlDbType.NVarChar) { Value = plDescription.Replace("'", "''") });
                }
            }

            context.SaveChanges();
        }

        private static void CheckConflictingNames(IEnumerable<Type> sqlEnumTypes)
        {
            var enumTables = sqlEnumTypes.Select(t => GetMetadata(t)).Select(m => $"[{m.SchemaName}].[{m.TableName}]");
            var conflictingTableNames = enumTables.GroupBy(i => i).Where(i => i.Count() > 1);

            if (conflictingTableNames.Any())
            {
                var errorMessage = "Conflicting enum names detected, consider using different constructor in SqlEnumAttribute to force a table name."
                    + "\n Conflicting names: [{0}]";

                throw new InvalidOperationException(
                    string.Format(errorMessage, string.Join(", ", conflictingTableNames.Select(i => i.Key))));
            }
        }

        private static SqlEnumMetadata GetMetadata(Type sqlEnumType)
        {
            var attribute = (SqlEnumAttribute)sqlEnumType
                .GetCustomAttributes(typeof(SqlEnumAttribute), true)
                .Single();

            return new SqlEnumMetadata
            {
                SchemaName = attribute.SchemaName,
                TableName = attribute.TableName ?? sqlEnumType.Name,
            };
        }
    }
}
