﻿using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal static partial class SecurityRolesSeed
    {
        private class IdUpdateOrderer
        {
            private readonly Dictionary<long, long> _targetToSourceIdMapping;
            private readonly Queue<long> _targetIdsReadyToAssign;
            private long _lastTemporaryId;

            public IdUpdateOrderer(IReadOnlyCollection<IdRemappingDescription> idRemapping)
            {
                var occupiedIds = new HashSet<long>(idRemapping.Select(r => r.SourceId));
                var allIdsInUse = occupiedIds.Concat(idRemapping.Select(r => r.TargetId));

                _lastTemporaryId = idRemapping.Count > 0 ? allIdsInUse.Max() : 0;

                _targetToSourceIdMapping = idRemapping
                    .Where(r => r.TargetId != r.SourceId)
                    .ToDictionary(r => r.TargetId, r => r.SourceId);

                _targetIdsReadyToAssign =
                    new Queue<long>(_targetToSourceIdMapping.Keys.Where(id => !occupiedIds.Contains(id)));
            }

            public IEnumerable<IdRemappingDescription> GetOrderedUpdates()
            {
                while (HasMoreUpdatesToDo())
                {
                    var idUpdate = GetNextUpdateToDo();

                    yield return idUpdate;

                    MakeAnotherUpdateReadyForTarget(idUpdate.SourceId);
                }
            }

            private bool HasMoreUpdatesToDo()
            {
                return _targetToSourceIdMapping.Count > 0;
            }

            private IdRemappingDescription GetNextUpdateToDo()
            {
                return TryGetNextReadyUpdate() ?? GetUpdateToTemporaryPosition();
            }

            private IdRemappingDescription? TryGetNextReadyUpdate()
            {
                if (_targetIdsReadyToAssign.Count == 0)
                {
                    return null;
                }

                var targetId = _targetIdsReadyToAssign.Dequeue();
                var sourceId = _targetToSourceIdMapping[targetId];
                _targetToSourceIdMapping.Remove(targetId);

                return new IdRemappingDescription
                {
                    TargetId = targetId,
                    SourceId = sourceId
                };
            }

            private IdRemappingDescription GetUpdateToTemporaryPosition()
            {
                var anyMapping = _targetToSourceIdMapping.First();
                var targetId = anyMapping.Key;
                var sourceId = anyMapping.Value;
                var temporaryId = ++_lastTemporaryId;

                _targetToSourceIdMapping[targetId] = temporaryId;

                return new IdRemappingDescription
                {
                    TargetId = temporaryId,
                    SourceId = sourceId
                };
            }

            private void MakeAnotherUpdateReadyForTarget(long targetId)
            {
                if (_targetToSourceIdMapping.ContainsKey(targetId))
                {
                    _targetIdsReadyToAssign.Enqueue(targetId);
                }
            }
        }
    }
}
