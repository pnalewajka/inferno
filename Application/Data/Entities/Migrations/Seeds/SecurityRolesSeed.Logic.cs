﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal static partial class SecurityRolesSeed
    {
        private static IEnumerable<SecurityRoleDefinition> GetRolesDefinedInEnum()
        {
            return Enum.GetValues(typeof(SecurityRoleType))
                .Cast<SecurityRoleType>()
                .Select(srt => new SecurityRoleDefinition
                {
                    Id = (long)srt,
                    Name = srt.ToString()
                });
        }

        private static IEnumerable<SecurityRoleDefinition> GetRolesFromDb(Database database)
        {
            // Loading entities from db before fixing their ids will make Entity Framework go crazy.
            // That's why custom SQL is used
            return database.SqlQuery<SecurityRoleDefinition>("SELECT Id, Name FROM Accounts.SecurityRole");
        }

        private static void EnsureThatEnumHasDisctinctValues()
        {
            var enumValues = Enum.GetValues(typeof(SecurityRoleType));
            var uniqueValues = enumValues.Cast<SecurityRoleType>().Distinct();

            if (enumValues.Length != uniqueValues.Count())
            {
                throw new InvalidOperationException("Enum SecurityRoleType must contain unique values");
            }
        }

        private static void EnsureThatDatabaseHasNoUnknownRolesWithConflictingIds(
            IEnumerable<SecurityRoleDefinition> rolesInDb,
            IReadOnlyCollection<SecurityRoleDefinition> rolesDefinedInEnum)
        {
            var knownNames = new HashSet<string>(rolesDefinedInEnum.Select(r => r.Name));
            var knownIds = new HashSet<long>(rolesDefinedInEnum.Select(r => r.Id));
            var unknownRoles = rolesInDb.Where(r => !knownNames.Contains(r.Name));
            var conflictingIds = unknownRoles.Where(r => knownIds.Contains(r.Id)).ToList();

            if (conflictingIds.Any())
            {
                throw new InvalidOperationException(
                    "There are some conflicting SecurityRoles in database not defined in SecurityRoleType enum:\n\t>> "
                    + string.Join(", ", conflictingIds.Select(r => r.Name)));
            }
        }

        private static void FixRolesIds(
            Database database,
            IEnumerable<SecurityRoleDefinition> rolesInDb,
            IEnumerable<SecurityRoleDefinition> rolesDefinedInEnum)
        {
            var idRemapping = CreateRequiredIdRemapping(rolesInDb, rolesDefinedInEnum).ToList();
            var idUpdateActions = TransformToIdUpdateActions(idRemapping);

            ApplyIdUpdateActions(database, idUpdateActions);
        }

        private static IEnumerable<IdRemappingDescription> CreateRequiredIdRemapping(
            IEnumerable<SecurityRoleDefinition> rolesInDb,
            IEnumerable<SecurityRoleDefinition> rolesDefinedInEnum)
        {
            var nameToIdMapping = rolesDefinedInEnum.ToDictionary(r => r.Name, r => r.Id);

            return rolesInDb.Select(r =>
            {
                long requiredId;

                if (!nameToIdMapping.TryGetValue(r.Name, out requiredId))
                {
                    requiredId = r.Id;
                }

                return new IdRemappingDescription
                {
                    TargetId = requiredId,
                    SourceId = r.Id
                };
            });
        }

        private static IEnumerable<IdRemappingDescription> TransformToIdUpdateActions(
            IReadOnlyCollection<IdRemappingDescription> idRemapping)
        {
            var orderer = new IdUpdateOrderer(idRemapping);

            return orderer.GetOrderedUpdates();
        }

        private static void ApplyIdUpdateActions(Database database, IEnumerable<IdRemappingDescription> idUpdateActions)
        {
            foreach (var action in idUpdateActions)
            {
                var sourceIdParam = new SqlParameter("@sourceId", action.SourceId);
                var targetIdParam = new SqlParameter("@targetId", action.TargetId);

                database.ExecuteSqlCommand(
                    "UPDATE Accounts.SecurityRole SET Id = @targetId WHERE Id = @sourceId",
                    targetIdParam,
                    sourceIdParam);
            }
        }

        private static void InsertNewRoles(
            CoreEntitiesContext context,
            IEnumerable<SecurityRoleDefinition> rolesInDb,
            IEnumerable<SecurityRoleDefinition> rolesDefinedInEnum)
        {
            var existingRolesNames = new HashSet<string>(rolesInDb.Select(r => r.Name));
            var rolesToInsert = rolesDefinedInEnum.Where(r => !existingRolesNames.Contains(r.Name));

            CreateAndInsertRoles(context, rolesToInsert);
            context.SaveChanges();
        }

        private static void CreateAndInsertRoles(
            CoreEntitiesContext context,
            IEnumerable<SecurityRoleDefinition> rolesToInsert)
        {
            var securityRoles = rolesToInsert.Select(r => new SecurityRole
            {
                Id = r.Id,
                Name = r.Name
            });

            context.SecurityRoles.AddRange(securityRoles);
        }
    }
}
