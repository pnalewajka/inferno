﻿using System.Linq;

namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal static partial class SecurityRolesSeed
    {
        public static void Seed(CoreEntitiesContext context)
        {
            var rolesDefinedInEnum = GetRolesDefinedInEnum().ToList();
            var rolesInDb = GetRolesFromDb(context.Database).ToList();

            EnsureThatEnumHasDisctinctValues();
            EnsureThatDatabaseHasNoUnknownRolesWithConflictingIds(rolesInDb, rolesDefinedInEnum);
            FixRolesIds(context.Database, rolesInDb, rolesDefinedInEnum);
            InsertNewRoles(context, rolesInDb, rolesDefinedInEnum);
        }
    }
}
