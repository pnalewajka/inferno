﻿namespace Smt.Atomic.Data.Entities.Migrations.Seeds
{
    internal static partial class SecurityRolesSeed
    {
        private struct IdRemappingDescription
        {
            public long SourceId { get; set; }
            public long TargetId { get; set; }
        }
    }
}
