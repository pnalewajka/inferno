namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ChangeContactRecordToCandidateContactRecordMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Recruitment.ContactRecord", newName: "CandidateContactRecord");
        }
        
        public override void Down()
        {
            RenameTable(name: "Recruitment.CandidateContactRecord", newName: "ContactRecord");
        }
    }
}
