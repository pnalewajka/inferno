namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration230420181107 : DbMigration
    {
        public override void Up()
        {
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            //DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            //DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            //AddColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long());
            //AlterColumn("Recruitment.RecruitmentProcessStep", "PlannedOn", c => c.DateTime());
            //AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionMakerId", c => c.Long());
            //AlterColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long());
            //CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
            //CreateIndex("Recruitment.JobOpening", "DecisionMakerEmployeeId");
            //CreateIndex("Recruitment.RecruitmentProcessStep", "DecisionMakerId");
            //CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            //AddForeignKey("Recruitment.JobOpening", "DecisionMakerEmployeeId", "Allocation.Employee", "Id");
        }

        public override void Down()
        {
            //DropForeignKey("Recruitment.JobOpening", "DecisionMakerEmployeeId", "Allocation.Employee");
            //DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            //DropIndex("Recruitment.RecruitmentProcessStep", new[] { "DecisionMakerId" });
            //DropIndex("Recruitment.JobOpening", new[] { "DecisionMakerEmployeeId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "LocationId" });
            //AlterColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcessStep", "DecisionMakerId", c => c.Long(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcessStep", "PlannedOn", c => c.DateTime(nullable: false));
            //AlterColumn("Recruitment.RecruitmentProcess", "LocationId", c => c.Long(nullable: false));
            //DropColumn("Recruitment.JobOpening", "DecisionMakerEmployeeId");
            //CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
            //CreateIndex("Recruitment.RecruitmentProcessStep", "DecisionMakerId");
            //CreateIndex("Recruitment.RecruitmentProcess", "LocationId");
        }
    }
}
