namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Data.SqlTypes;

    public partial class ArrangementIssuedOnDateMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("BusinessTrips.Arrangement", "IssuedOn", c => c.DateTime(nullable: false, defaultValue: SqlDateTime.MinValue.Value));
        }
        
        public override void Down()
        {
            DropColumn("BusinessTrips.Arrangement", "IssuedOn");
        }
    }
}
