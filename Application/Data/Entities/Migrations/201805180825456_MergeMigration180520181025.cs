namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration180520181025 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("Recruitment.RecruitmentProcess", "OfferCityId", "Dictionaries.City");
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferCityId" });
            //AddColumn("Recruitment.RecruitmentProcess", "OfferLocationId", c => c.Long());
            //CreateIndex("Recruitment.RecruitmentProcess", "OfferLocationId");
            //AddForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location", "Id");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferCityId");
        }
        
        public override void Down()
        {
            //AddColumn("Recruitment.RecruitmentProcess", "OfferCityId", c => c.Long());
            //DropForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location");
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferLocationId" });
            //DropColumn("Recruitment.RecruitmentProcess", "OfferLocationId");
            //CreateIndex("Recruitment.RecruitmentProcess", "OfferCityId");
            //AddForeignKey("Recruitment.RecruitmentProcess", "OfferCityId", "Dictionaries.City", "Id");
        }
    }
}
