namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataProcessingAgreementEntitiesMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "GDPR.DataProcessingAgreementConsent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AgreementId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("GDPR.DataProcessingAgreement", t => t.AgreementId, cascadeDelete: true)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Accounts.User", t => t.UserId)
                .Index(t => new { t.AgreementId, t.UserId }, unique: true, name: "IX_Unique_Agreement_User")
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "GDPR.DataProcessingAgreement",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ValidFrom = c.DateTime(nullable: false),
                        ValidTo = c.DateTime(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "GDPR.DataProcessingAgreementDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataProcessingAgreementId = c.Long(nullable: false),
                        Name = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 250, unicode: false),
                        ContentLength = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("GDPR.DataProcessingAgreement", t => t.DataProcessingAgreementId, cascadeDelete: true)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.DataProcessingAgreementId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
            CreateTable(
                "GDPR.DataProcessingAgreementDocumentContent",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Content = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("GDPR.DataProcessingAgreementDocument", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
            CreateTable(
                "GDPR.DataProcessingAgreementProjectsRelation",
                c => new
                    {
                        DataProcessingAgreementId = c.Long(nullable: false),
                        ProjectsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.DataProcessingAgreementId, t.ProjectsId })
                .ForeignKey("GDPR.DataProcessingAgreement", t => t.DataProcessingAgreementId, cascadeDelete: true)
                .ForeignKey("Allocation.Project", t => t.ProjectsId, cascadeDelete: true)
                .Index(t => t.DataProcessingAgreementId)
                .Index(t => t.ProjectsId);
            
            CreateTable(
                "GDPR.DataProcessingAgreementRequiredUsersRelation",
                c => new
                    {
                        DataProcessingAgreementId = c.Long(nullable: false),
                        RequiredUsersId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.DataProcessingAgreementId, t.RequiredUsersId })
                .ForeignKey("GDPR.DataProcessingAgreement", t => t.DataProcessingAgreementId, cascadeDelete: true)
                .ForeignKey("Accounts.User", t => t.RequiredUsersId, cascadeDelete: true)
                .Index(t => t.DataProcessingAgreementId)
                .Index(t => t.RequiredUsersId);
        }
        
        public override void Down()
        {
            DropForeignKey("GDPR.DataProcessingAgreementConsent", "UserId", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementConsent", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementConsent", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementRequiredUsersRelation", "RequiredUsersId", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementRequiredUsersRelation", "DataProcessingAgreementId", "GDPR.DataProcessingAgreement");
            DropForeignKey("GDPR.DataProcessingAgreementProjectsRelation", "ProjectsId", "Allocation.Project");
            DropForeignKey("GDPR.DataProcessingAgreementProjectsRelation", "DataProcessingAgreementId", "GDPR.DataProcessingAgreement");
            DropForeignKey("GDPR.DataProcessingAgreement", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreement", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementDocument", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementDocument", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataProcessingAgreementDocumentContent", "Id", "GDPR.DataProcessingAgreementDocument");
            DropForeignKey("GDPR.DataProcessingAgreementDocument", "DataProcessingAgreementId", "GDPR.DataProcessingAgreement");
            DropForeignKey("GDPR.DataProcessingAgreementConsent", "AgreementId", "GDPR.DataProcessingAgreement");
            DropIndex("GDPR.DataProcessingAgreementRequiredUsersRelation", new[] { "RequiredUsersId" });
            DropIndex("GDPR.DataProcessingAgreementRequiredUsersRelation", new[] { "DataProcessingAgreementId" });
            DropIndex("GDPR.DataProcessingAgreementProjectsRelation", new[] { "ProjectsId" });
            DropIndex("GDPR.DataProcessingAgreementProjectsRelation", new[] { "DataProcessingAgreementId" });
            DropIndex("GDPR.DataProcessingAgreementDocumentContent", new[] { "Id" });
            DropIndex("GDPR.DataProcessingAgreementDocument", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataProcessingAgreementDocument", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataProcessingAgreementDocument", new[] { "DataProcessingAgreementId" });
            DropIndex("GDPR.DataProcessingAgreement", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataProcessingAgreement", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataProcessingAgreementConsent", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataProcessingAgreementConsent", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataProcessingAgreementConsent", "IX_Unique_Agreement_User");
            DropTable("GDPR.DataProcessingAgreementRequiredUsersRelation");
            DropTable("GDPR.DataProcessingAgreementProjectsRelation");
            DropTable("GDPR.DataProcessingAgreementDocumentContent");
            DropTable("GDPR.DataProcessingAgreementDocument");
            DropTable("GDPR.DataProcessingAgreement");
            DropTable("GDPR.DataProcessingAgreementConsent");
        }
    }
}
