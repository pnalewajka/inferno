namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPreferredDistanceUnitToBusinessTripSettlementRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "PreferredDistanceUnit", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("Workflows.BusinessTripSettlementRequest", "PreferredDistanceUnit");
        }
    }
}
