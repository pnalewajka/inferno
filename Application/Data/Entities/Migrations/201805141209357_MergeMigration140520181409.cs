namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration140520181409 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner");
            //DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            //DropForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent");
            //DropForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument");
            //AddForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner", "Id");
            //AddForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id");
            //AddForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent", "Id");
            //AddForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument", "Id");
        }

        public override void Down()
        {
            //DropForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument");
            //DropForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent");
            //DropForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner");
            //DropForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner");
            //AddForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument", "Id", cascadeDelete: true);
            //AddForeignKey("GDPR.DataConsentDocument", "DataConsentId", "GDPR.DataConsent", "Id", cascadeDelete: true);
            //AddForeignKey("GDPR.DataConsent", "DataOwnerId", "GDPR.DataOwner", "Id", cascadeDelete: true);
            //AddForeignKey("GDPR.DataActivity", "DataOwnerId", "GDPR.DataOwner", "Id", cascadeDelete: true);
        }
    }
}
