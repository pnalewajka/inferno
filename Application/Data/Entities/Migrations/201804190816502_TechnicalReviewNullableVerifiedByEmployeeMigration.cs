namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class TechnicalReviewNullableVerifiedByEmployeeMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            AlterColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long());
            CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.TechnicalReview", new[] { "VerifiedByEmployeeId" });
            AlterColumn("Recruitment.TechnicalReview", "VerifiedByEmployeeId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.TechnicalReview", "VerifiedByEmployeeId");
        }
    }
}
