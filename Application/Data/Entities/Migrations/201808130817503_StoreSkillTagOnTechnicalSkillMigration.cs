﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class StoreSkillTagOnTechnicalSkillMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Resumes.ResumeTechnicalSkill", "SkillTagId", c => c.Long(nullable: true));
            CreateIndex("Resumes.ResumeTechnicalSkill", "SkillTagId");
            AddForeignKey("Resumes.ResumeTechnicalSkill", "SkillTagId", "SkillManagement.SkillTag", "Id");
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808130817503_StoreSkillTagOnTechnicalSkillMigration.UpdateSkillTagsOnResumeSql.Up.sql");
        }

        public override void Down()
        {
            DropForeignKey("Resumes.ResumeTechnicalSkill", "SkillTagId", "SkillManagement.SkillTag");
            DropIndex("Resumes.ResumeTechnicalSkill", new[] { "SkillTagId" });
            DropColumn("Resumes.ResumeTechnicalSkill", "SkillTagId");
        }
    }
}

