namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVisibilityRuleToFeedbackMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("MeetMe.Feedback", "Visibility", c => c.Int(nullable: false));
            AddColumn("Workflows.FeedbackRequest", "Visibility", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("MeetMe.Feedback", "Visibility");
            DropColumn("Workflows.FeedbackRequest", "Visibility");
        }
    }
}
