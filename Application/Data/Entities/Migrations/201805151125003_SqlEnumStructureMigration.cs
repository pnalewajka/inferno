﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class SqlEnumStructureMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805151125003_SqlEnumStructureMigration.CreateSchemaAndProcedures.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

