namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TechnicalReviewRemoveSeniorityLevelExpectationMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("Recruitment.TechnicalReview", "SeniorityLevelExpectation");
        }
        
        public override void Down()
        {
            AddColumn("Recruitment.TechnicalReview", "SeniorityLevelExpectation", c => c.Int(nullable: false));
        }
    }
}
