namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqueJobTitleNamesMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("SkillManagement.JobTitle", "NameEn", c => c.String(maxLength: 255));
            AlterColumn("SkillManagement.JobTitle", "NamePl", c => c.String(maxLength: 255));
            CreateIndex("SkillManagement.JobTitle", "NameEn", unique: true);
            CreateIndex("SkillManagement.JobTitle", "NamePl", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("SkillManagement.JobTitle", new[] { "NamePl" });
            DropIndex("SkillManagement.JobTitle", new[] { "NameEn" });
            AlterColumn("SkillManagement.JobTitle", "NamePl", c => c.String());
            AlterColumn("SkillManagement.JobTitle", "NameEn", c => c.String());
        }
    }
}
