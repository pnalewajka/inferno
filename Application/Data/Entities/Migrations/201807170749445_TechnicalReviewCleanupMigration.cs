namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class TechnicalReviewCleanupMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Recruitment.TechnicalReview", "SoftwareEngineeringAssessment", "TechnicalKnowledgeAssessment");
            RenameColumn("Recruitment.TechnicalReview", "ExperienceAssessment", "TeamProjectSuitabilityAssessment");
            DropColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment");
        }

        public override void Down()
        {
            RenameColumn("Recruitment.TechnicalReview", "TechnicalKnowledgeAssessment", "SoftwareEngineeringAssessment");
            RenameColumn("Recruitment.TechnicalReview", "TeamProjectSuitabilityAssessment", "ExperienceAssessment");
            AddColumn("Recruitment.TechnicalReview", "SolutionDesignAssessment", c => c.String());
        }
    }
}
