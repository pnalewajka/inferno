﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class TemporaryTimeReportMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropIndex("TimeTracking.TimeReport", "IX_EmployeeYearMonth");
            AddColumn("TimeTracking.TimeReport", "IsTemporary", c => c.Boolean(nullable: false));
            CreateIndex("TimeTracking.TimeReport", new[] { "EmployeeId", "Year", "Month" }, name: "IX_EmployeeYearMonth");

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805101112483_TemporaryTimeReportMigration.AddUniqueConditionalIndex.Up.sql");
        }
        
        public override void Down()
        {
            DropIndex("TimeTracking.TimeReport", "IX_EmployeeYearMonth");
            DropColumn("TimeTracking.TimeReport", "IsTemporary");
            CreateIndex("TimeTracking.TimeReport", new[] { "EmployeeId", "Year", "Month" }, unique: true, name: "IX_EmployeeYearMonth");
        }
    }
}

