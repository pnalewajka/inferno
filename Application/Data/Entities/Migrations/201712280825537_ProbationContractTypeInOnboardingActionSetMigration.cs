﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class ProbationContractTypeInOnboardingActionSetMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201712280825537_ProbationContractTypeInOnboardingActionSetMigration.Script.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
