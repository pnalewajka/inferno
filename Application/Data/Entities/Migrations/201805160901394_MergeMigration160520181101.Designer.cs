// <auto-generated />
namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class MergeMigration160520181101 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MergeMigration160520181101));
        
        string IMigrationMetadata.Id
        {
            get { return "201805160901394_MergeMigration160520181101"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
