namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddResearchingCommentMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.InboundEmail", "ResearchingComment", c => c.String(maxLength: 300));
        }

        public override void Down()
        {
            DropColumn("Recruitment.InboundEmail", "ResearchingComment");
        }
    }
}
