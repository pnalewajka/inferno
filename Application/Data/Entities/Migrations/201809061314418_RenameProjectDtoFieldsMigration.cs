﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class RenameProjectDtoFieldsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201809061314418_RenameProjectDtoFieldsMigration.UpdateJiraIssueToTimeReportRowMappers.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

