namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReasonToRecruitmentJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.JobOpening", "RejectionReason", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("Recruitment.JobOpening", "RejectionReason");
        }
    }
}
