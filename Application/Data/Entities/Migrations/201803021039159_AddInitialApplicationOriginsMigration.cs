﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class AddInitialApplicationOriginsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803021039159_AddInitialApplicationOriginsMigration.InitalizeApplicationOrigins.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

