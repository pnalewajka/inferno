﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    using Helpers;

    public partial class IncreaseLengthOfFieldsMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806070816549_IncreaseLengthOfFieldsMigration.IncreaseLengthOfFields.Up.sql");
        }

        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201806070816549_IncreaseLengthOfFieldsMigration.IncreaseLengthOfFields.Down.sql");
        }
    }
}

