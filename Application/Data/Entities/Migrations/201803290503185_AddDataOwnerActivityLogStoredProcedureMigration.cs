﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AddDataOwnerActivityLogStoredProcedureMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803290503185_AddDataOwnerActivityLogStoredProcedureMigration.AddLogProcedureForDataOwner.Up.sql");
        }
        
        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201803290503185_AddDataOwnerActivityLogStoredProcedureMigration.AddLogProcedureForDataOwner.Down.sql");
        }
    }
}

