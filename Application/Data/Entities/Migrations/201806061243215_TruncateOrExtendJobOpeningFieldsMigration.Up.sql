﻿ALTER TABLE Recruitment.JobOpening  ALTER COLUMN PositionName NVARCHAR (400);
ALTER TABLE Recruitment.JobOpening  ALTER COLUMN NoticePeriod NVARCHAR (120);
ALTER TABLE Recruitment.JobOpening  ALTER COLUMN SalaryRate NVARCHAR (130);
ALTER TABLE Recruitment.JobOpening  ALTER COLUMN RejectionReason NVARCHAR (400);