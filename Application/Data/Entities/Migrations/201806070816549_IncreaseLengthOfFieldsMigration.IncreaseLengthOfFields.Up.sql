﻿ALTER TABLE Recruitment.JobOpening  ALTER COLUMN RejectionReason NVARCHAR (550);
ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN OfferStartDate NVARCHAR (130);
ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN OfferSalary NVARCHAR (150);
ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN OfferComment NVARCHAR (500);
