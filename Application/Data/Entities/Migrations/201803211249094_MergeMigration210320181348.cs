namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergeMigration210320181348 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Recruitment.CandidateDocumentContent",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false),
            //            Content = c.Binary(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.CandidateDocument", t => t.Id)
            //    .Index(t => t.Id);
            
            //CreateTable(
            //    "Recruitment.CandidateDocument",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            CandidateFileId = c.Long(nullable: false),
            //            Name = c.String(maxLength: 255),
            //            ContentType = c.String(maxLength: 250, unicode: false),
            //            ContentLength = c.Long(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.CandidateFile", t => t.CandidateFileId)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .Index(t => t.CandidateFileId)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
            //CreateTable(
            //    "Recruitment.CandidateFile",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            CandidateId = c.Long(nullable: false),
            //            DocumentType = c.Int(nullable: false),
            //            Comment = c.String(maxLength: 100),
            //            UserIdCreatedBy = c.Long(),
            //            UserIdImpersonatedCreatedBy = c.Long(),
            //            CreatedOn = c.DateTime(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            UserIdImpersonatedBy = c.Long(),
            //            UserIdModifiedBy = c.Long(),
            //            ModifiedOn = c.DateTime(nullable: false),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Recruitment.Candidate", t => t.CandidateId)
            //    .ForeignKey("Accounts.User", t => t.UserIdCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdImpersonatedCreatedBy)
            //    .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
            //    .Index(t => t.CandidateId)
            //    .Index(t => t.UserIdCreatedBy)
            //    .Index(t => t.UserIdImpersonatedCreatedBy)
            //    .Index(t => t.UserIdImpersonatedBy)
            //    .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("Recruitment.CandidateDocumentContent", "Id", "Recruitment.CandidateDocument");
            //DropForeignKey("Recruitment.CandidateDocument", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateDocument", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateFile", "UserIdModifiedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateFile", "UserIdImpersonatedCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateFile", "UserIdImpersonatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateDocument", "CandidateFileId", "Recruitment.CandidateFile");
            //DropForeignKey("Recruitment.CandidateFile", "UserIdCreatedBy", "Accounts.User");
            //DropForeignKey("Recruitment.CandidateFile", "CandidateId", "Recruitment.Candidate");
            //DropIndex("Recruitment.CandidateFile", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.CandidateFile", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.CandidateFile", new[] { "UserIdImpersonatedCreatedBy" });
            //DropIndex("Recruitment.CandidateFile", new[] { "UserIdCreatedBy" });
            //DropIndex("Recruitment.CandidateFile", new[] { "CandidateId" });
            //DropIndex("Recruitment.CandidateDocument", new[] { "UserIdModifiedBy" });
            //DropIndex("Recruitment.CandidateDocument", new[] { "UserIdImpersonatedBy" });
            //DropIndex("Recruitment.CandidateDocument", new[] { "CandidateFileId" });
            //DropIndex("Recruitment.CandidateDocumentContent", new[] { "Id" });
            //DropTable("Recruitment.CandidateFile");
            //DropTable("Recruitment.CandidateDocument");
            //DropTable("Recruitment.CandidateDocumentContent");
        }
    }
}
