namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddAllowedBonusTypesToProjectMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Allocation.Project", "AllowedBonusTypes", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Allocation.Project", "AllowedBonusTypes");
        }
    }
}