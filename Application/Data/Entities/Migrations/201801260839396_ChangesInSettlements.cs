namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInSettlements : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", c => c.Long(nullable: false));
            CreateIndex("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId");
            AddForeignKey("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", "BusinessTrips.BusinessTripParticipant", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId", "BusinessTrips.BusinessTripParticipant");
            DropIndex("Workflows.BusinessTripSettlementRequest", new[] { "BusinessTripParticipantId" });
            DropColumn("Workflows.BusinessTripSettlementRequest", "BusinessTripParticipantId");
        }
    }
}
