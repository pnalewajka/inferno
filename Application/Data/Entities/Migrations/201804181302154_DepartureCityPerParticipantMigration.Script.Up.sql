﻿UPDATE [BusinessTrips].[BusinessTripParticipant]
   SET [DepartureCityId] = (SELECT [CityId] FROM [Allocation].[Employee] [e]
                              JOIN [Dictionaries].[Location] [l] ON [l].[Id] = [e].[LocationId] 
                             WHERE [e].[Id] = [EmployeeId])
