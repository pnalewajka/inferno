namespace Smt.Atomic.Data.Entities.Migrations
{
    using Smt.Atomic.Data.Entities.Helpers;

    public partial class UpdateBusinessTripSettlementRequestMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201809180902038_UpdateBusinessTripSettlementRequestMigration.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}
