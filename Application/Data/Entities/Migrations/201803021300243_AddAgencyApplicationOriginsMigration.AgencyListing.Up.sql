﻿SET XACT_ABORT ON
GO

INSERT INTO [Recruitment].[ApplicationOrigin] ([Name], [OriginType], [ModifiedOn])
VALUES
('Edge1Solution', 1, GETDATE()),
('EUVIC', 1, GETDATE()),
('HAYS', 1, GETDATE()),
('MAKE MY MIND', 1, GETDATE()),
('Antal', 1, GETDATE()),
('Altimi Solutions', 1, GETDATE()),
('Manpower', 1, GETDATE()),
('Powermedia', 1, GETDATE()),
('Transition Technologies', 1, GETDATE()),
('Geosolution', 1, GETDATE()),
('Enginar', 1, GETDATE())
GO

UPDATE [Recruitment].[ApplicationOrigin] SET [OriginType] = [OriginType] + 1000 WHERE [OriginType] < 4
GO