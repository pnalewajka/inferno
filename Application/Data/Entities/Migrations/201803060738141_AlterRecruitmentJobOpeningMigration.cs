namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterRecruitmentJobOpeningMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("Recruitment.JobOpening", new[] { "AcceptingEmployeeId" });
            AlterColumn("Recruitment.JobOpening", "AcceptingEmployeeId", c => c.Long());
            CreateIndex("Recruitment.JobOpening", "AcceptingEmployeeId");
        }
        
        public override void Down()
        {
            DropIndex("Recruitment.JobOpening", new[] { "AcceptingEmployeeId" });
            AlterColumn("Recruitment.JobOpening", "AcceptingEmployeeId", c => c.Long(nullable: false));
            CreateIndex("Recruitment.JobOpening", "AcceptingEmployeeId");
        }
    }
}
