﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AddProjectAcronymMigraionMigration : AtomicDbMigration
    {
        public override void Up()
        {
            AddColumn("Allocation.Project", "Acronym", c => c.String(maxLength: 4));
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201805281033435_AddProjectAcronymMigraionMigration.AddProjectAcronymIndexSqlMigration.Up.sql");
        }

        public override void Down()
        {
            DropIndex("Allocation.Project", "IX_ProjectAcronym");
            DropColumn("Allocation.Project", "Acronym");
        }
    }
}

