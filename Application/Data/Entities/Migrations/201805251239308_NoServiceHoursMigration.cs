namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class NoServiceHoursMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("TimeTracking.TimeReport", "NoServiceHours", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("TimeTracking.TimeReport", "NoServiceHours");
        }
    }
}
