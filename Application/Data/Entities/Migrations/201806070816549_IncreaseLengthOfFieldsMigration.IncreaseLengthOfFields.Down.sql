﻿ALTER TABLE Recruitment.RejectionReason  ALTER COLUMN PositionName NVARCHAR (400);
ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN OfferStartDate NVARCHAR (50);
ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN OfferSalary NVARCHAR (50);
ALTER TABLE Recruitment.RecruitmentProcess  ALTER COLUMN OfferComment NVARCHAR (255);
