﻿SET XACT_ABORT ON
GO

CREATE UNIQUE INDEX IX_UQ_EmployeeYearMonth
ON [TimeTracking].[TimeReport] ([EmployeeId], [Year], [Month])
WHERE [IsTemporary] = 0
