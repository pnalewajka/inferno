namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ChangeVerifiedOnTechnicalReviewMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Recruitment.TechnicalReview", "VerifiedOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("Recruitment.TechnicalReview", "VerifiedOn", c => c.DateTime(nullable: false));
        }
    }
}
