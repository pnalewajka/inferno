﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class FullProjectViewMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808210825301_FullProjectViewMigration.FullProjectView.Up.sql");
        }

        public override void Down()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201808210825301_FullProjectViewMigration.FullProjectView.Down.sql");
        }
    }
}


