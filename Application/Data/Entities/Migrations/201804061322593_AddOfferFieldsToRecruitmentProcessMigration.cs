namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddOfferFieldsToRecruitmentProcessMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "OfferPositionId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "OfferContractType", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "OfferSalary", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "OfferStartDate", c => c.String(maxLength: 50));
            AddColumn("Recruitment.RecruitmentProcess", "OfferLocationId", c => c.Long());
            AddColumn("Recruitment.RecruitmentProcess", "OfferComment", c => c.String(maxLength: 255));
            CreateIndex("Recruitment.RecruitmentProcess", "OfferPositionId");
            CreateIndex("Recruitment.RecruitmentProcess", "OfferLocationId");
            AddForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location", "Id");
            AddForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle");
            DropForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferLocationId" });
            DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferPositionId" });
            DropColumn("Recruitment.RecruitmentProcess", "OfferComment");
            DropColumn("Recruitment.RecruitmentProcess", "OfferLocationId");
            DropColumn("Recruitment.RecruitmentProcess", "OfferStartDate");
            DropColumn("Recruitment.RecruitmentProcess", "OfferSalary");
            DropColumn("Recruitment.RecruitmentProcess", "OfferContractType");
            DropColumn("Recruitment.RecruitmentProcess", "OfferPositionId");
        }
    }
}
