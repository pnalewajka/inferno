﻿DECLARE @polandId bigint = (SELECT TOP 1 [Id] FROM [Dictionaries].[Country] WHERE [IsoCode] = N'PL')

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = [AccommodationLimit] * 0.25
 WHERE [EmploymentCountryId] = @polandId AND [TravelCountryId] != @polandId

UPDATE [BusinessTrips].[DailyAllowance]
   SET [AccommodationLimit] = 45
 WHERE [EmploymentCountryId] = @polandId AND [TravelCountryId] = @polandId
