namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixInboundEmailEntityMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Recruitment.InboundEmail", name: "Recruiter_Id", newName: "RecruiterId");
            RenameIndex(table: "Recruitment.InboundEmail", name: "IX_Recruiter_Id", newName: "IX_RecruiterId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "Recruitment.InboundEmail", name: "IX_RecruiterId", newName: "IX_Recruiter_Id");
            RenameColumn(table: "Recruitment.InboundEmail", name: "RecruiterId", newName: "Recruiter_Id");
        }
    }
}
