namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInquiryEntityMigrtionMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Sales.Inquiry",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CustomerId = c.Long(),
                        DisplayName = c.String(maxLength: 500),
                        ProcessState = c.Int(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Sales.Customer", t => t.CustomerId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .Index(t => t.CustomerId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Sales.Inquiry", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("Sales.Inquiry", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("Sales.Inquiry", "CustomerId", "Sales.Customer");
            DropIndex("Sales.Inquiry", new[] { "UserIdModifiedBy" });
            DropIndex("Sales.Inquiry", new[] { "UserIdImpersonatedBy" });
            DropIndex("Sales.Inquiry", new[] { "CustomerId" });
            DropTable("Sales.Inquiry");
        }
    }
}
