namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHistoryEntriesToJobOpeningAndProcessMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Recruitment.JobOpeningHistoryEntry",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        JobOpeningId = c.Long(nullable: false),
                        PerformedOn = c.DateTime(nullable: false),
                        PerformedByUserId = c.Long(nullable: false),
                        ChangesDescription = c.String(),
                        ActivityType = c.Int(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.JobOpening", t => t.JobOpeningId)
                .ForeignKey("Accounts.User", t => t.PerformedByUserId)
                .Index(t => t.JobOpeningId)
                .Index(t => t.PerformedByUserId);
            
            CreateTable(
                "Recruitment.RecruitmentProcessHistoryEntry",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RecruitmentProcessId = c.Long(nullable: false),
                        PerformedOn = c.DateTime(nullable: false),
                        PerformedByUserId = c.Long(nullable: false),
                        ActivityType = c.Int(nullable: false),
                        ChangesDescription = c.String(),
                        RecruitmentProcessStepId = c.Long(),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Accounts.User", t => t.PerformedByUserId)
                .ForeignKey("Recruitment.RecruitmentProcess", t => t.RecruitmentProcessId)
                .ForeignKey("Recruitment.RecruitmentProcessStep", t => t.RecruitmentProcessStepId)
                .Index(t => t.RecruitmentProcessId)
                .Index(t => t.PerformedByUserId)
                .Index(t => t.RecruitmentProcessStepId);            
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcessHistoryEntry", "RecruitmentProcessStepId", "Recruitment.RecruitmentProcessStep");
            DropForeignKey("Recruitment.RecruitmentProcessHistoryEntry", "RecruitmentProcessId", "Recruitment.RecruitmentProcess");
            DropForeignKey("Recruitment.RecruitmentProcessHistoryEntry", "PerformedByUserId", "Accounts.User");
            DropForeignKey("Recruitment.JobOpeningHistoryEntry", "PerformedByUserId", "Accounts.User");
            DropForeignKey("Recruitment.JobOpeningHistoryEntry", "JobOpeningId", "Recruitment.JobOpening");
            DropIndex("Recruitment.RecruitmentProcessHistoryEntry", new[] { "RecruitmentProcessStepId" });
            DropIndex("Recruitment.RecruitmentProcessHistoryEntry", new[] { "PerformedByUserId" });
            DropIndex("Recruitment.RecruitmentProcessHistoryEntry", new[] { "RecruitmentProcessId" });
            DropIndex("Recruitment.JobOpeningHistoryEntry", new[] { "PerformedByUserId" });
            DropIndex("Recruitment.JobOpeningHistoryEntry", new[] { "JobOpeningId" });
            DropTable("Recruitment.RecruitmentProcessHistoryEntry");
            DropTable("Recruitment.JobOpeningHistoryEntry");
        }
    }
}
