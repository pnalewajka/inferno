﻿DECLARE @CurrentDate DATETIME
SET @CurrentDate = GETDATE()

INSERT INTO [MeetMe].[Meeting]
           ([EmployeeIdEmployee]
           ,[Status]
           ,[MeetingDueDate]
           ,[ScheduledMeetingDate]
           ,[UserIdImpersonatedBy]
           ,[UserIdModifiedBy]
           ,[ModifiedOn])
SELECT 
	E.Id
	,0
	,DATEADD(WEEK,2,ISNULL((SELECT P.StartDate 
								FROM Allocation.EmploymentPeriod P 
							WHERE P.EmployeeIdEmployee = E.Id
									AND P.StartDate <= @CurrentDate 
									AND (P.EndDate IS NULL OR P.EndDate >= @CurrentDate))
							,@CurrentDate))
	,NULL
	,NULL
	,1
	,@CurrentDate
		FROM Allocation.Employee E 
	WHERE E.CurrentEmployeeStatus != 5 AND E.Id NOT IN 
		(SELECT M.EmployeeIdEmployee 
			FROM MeetMe.Meeting M 
		 WHERE M.EmployeeIdEmployee = E.Id)

GO