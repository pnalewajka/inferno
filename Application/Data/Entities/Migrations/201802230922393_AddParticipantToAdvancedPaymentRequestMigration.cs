namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParticipantToAdvancedPaymentRequestMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.AdvancedPaymentRequest", "ParticipantId", c => c.Long(nullable: false));
            CreateIndex("Workflows.AdvancedPaymentRequest", "ParticipantId");
            AddForeignKey("Workflows.AdvancedPaymentRequest", "ParticipantId", "BusinessTrips.BusinessTripParticipant", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.AdvancedPaymentRequest", "ParticipantId", "BusinessTrips.BusinessTripParticipant");
            DropIndex("Workflows.AdvancedPaymentRequest", new[] { "ParticipantId" });
            DropColumn("Workflows.AdvancedPaymentRequest", "ParticipantId");
        }
    }
}
