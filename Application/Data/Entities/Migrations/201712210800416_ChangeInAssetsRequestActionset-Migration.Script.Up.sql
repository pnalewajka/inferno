﻿SET XACT_ABORT ON
GO

IF EXISTS (SELECT Id FROM [Workflows].[ActionSet] WHERE [Code] = N'Assets')
BEGIN

	UPDATE [Workflows].[ActionSet]
	SET Definition = N'@model Smt.Atomic.WebApp.Areas.PeopleManagement.Models.AssetsRequestActionSetModel

@functions
{     
    string ToITLocation(string location)
    {
        switch (location)
        {
            case "Szczecin":
            case "Warszawa":
            case "Lublin":
            case "Poznań":
            case "Białystok":
            case "Katowice":
            case "Kraków":
            case "Łódź":
            case "Regensburg":
                return location;

            case "Berlin":
                return "Germany";

            case "Munchen":
                return "München";

            case "Wrocław Kościuszki":
                return "Wrocław - Kościuszki";

            case "Wrocław Prosta":
                return "Wrocław - Prosta";

            default:
                return "Other";
        }
    }
}

@{ var dueDate = Model.OnboardingDate.ToString("dd/MMM/yy", System.Globalization.CultureInfo.InvariantCulture); }
@{ var startDate = Model.StartDate.ToString("dd/MMM/yy", System.Globalization.CultureInfo.InvariantCulture); }

<Actions>
    <CreateJiraIssue Summary="Onboarding - @Model.Employee.FullName (@Model.Employee.Login), @startDate" Project="IT" Type="Task" DueDate="@dueDate" Security="10131" UpdatesOn="StartDate;OnboardingDate;OnboardingLocationId;LocationId;LineManagerEmployeeId">
        <CustomFields>
            <CustomField Id="10411" Name="Location" Value="@ToITLocation(Model.Employee.LocationName)" />
            <CustomField Id="10893" Name="Project name" Value="IT" />
            <CustomField Id="10260" Name="Viewers" Value="[[@Model.Recruiter.Acronym]],[[@Model.LineManager.Acronym]]" />
        </CustomFields>
Hi there,

New employee @Model.Employee.FullName (@Model.Employee.Login) is going to start his/hers intive journey from @startDate. Please find details below:

Onboarding location: @Model.Employee.LocationName
@if (!String.IsNullOrEmpty(Model.Hardware.English))
{
@:Hardware: @Model.Hardware.English
}
@if (!String.IsNullOrEmpty(Model.HardwareComments))
{
@:Hardware Comments: @Model.HardwareComments
}
@if (!String.IsNullOrEmpty(Model.Software.English))
{
@:Software: @Model.Software.English
}
@if (!String.IsNullOrEmpty(Model.SoftwareComments))
{
@:Software Comments: @Model.SoftwareComments
}
@if (!String.IsNullOrEmpty(Model.SystemAccess.English))
{
@:SystemAccess: @Model.SystemAccess.English
}
@if (!String.IsNullOrEmpty(Model.SystemAccessComments))
{
@:SystemAccess Comments: @Model.SystemAccessComments
}
@if (!String.IsNullOrEmpty(Model.Other.English))
{
@:Other: @Model.Other.English
}
@if (!String.IsNullOrEmpty(Model.OtherComments))
{
@:Other Comments: @Model.OtherComments
}

All hardware need to be prepared before @dueDate

In case of any questions please contact his/her recruiter @Model.Recruiter.FullName (@Model.Recruiter.Login).
    </CreateJiraIssue>
</Actions>'
	WHERE Code = N'Assets'

END