namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveaffectedUserFromBonusRequestEntityMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Workflows.BonusRequest", "AffectedUserId", "Accounts.User");
            DropIndex("Workflows.BonusRequest", new[] { "AffectedUserId" });
            DropColumn("Workflows.BonusRequest", "AffectedUserId");
        }
        
        public override void Down()
        {
            AddColumn("Workflows.BonusRequest", "AffectedUserId", c => c.Long(nullable: false));
            CreateIndex("Workflows.BonusRequest", "AffectedUserId");
            AddForeignKey("Workflows.BonusRequest", "AffectedUserId", "Accounts.User", "Id");
        }
    }
}
