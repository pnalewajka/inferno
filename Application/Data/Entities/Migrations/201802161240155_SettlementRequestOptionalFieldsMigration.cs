namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SettlementRequestOptionalFieldsMigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Workflows.BusinessTripSettlementRequest", "DepartureDateTime", c => c.DateTime());
            AlterColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("Workflows.BusinessTripSettlementRequest", "ArrivalDateTime", c => c.DateTime(nullable: false));
            AlterColumn("Workflows.BusinessTripSettlementRequest", "DepartureDateTime", c => c.DateTime(nullable: false));
        }
    }
}
