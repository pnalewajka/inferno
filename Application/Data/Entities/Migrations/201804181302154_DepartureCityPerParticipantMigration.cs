﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class DepartureCityPerParticipantMigration : AtomicDbMigration
    {
        public override void Up()
        {
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip");
            DropForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "CityId", "Dictionaries.City");
            DropIndex("BusinessTrips.BusinessTripDepartureCityMap", new[] { "BusinessTripId" });
            DropIndex("BusinessTrips.BusinessTripDepartureCityMap", new[] { "CityId" });
            AddColumn("BusinessTrips.BusinessTripParticipant", "DepartureCityId", c => c.Long(nullable: false));

            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804181302154_DepartureCityPerParticipantMigration.Script.Up.sql");

            CreateIndex("BusinessTrips.BusinessTripParticipant", "DepartureCityId");
            AddForeignKey("BusinessTrips.BusinessTripParticipant", "DepartureCityId", "Dictionaries.City", "Id");
            DropTable("BusinessTrips.BusinessTripDepartureCityMap");
        }
        
        public override void Down()
        {
            CreateTable(
                "BusinessTrips.BusinessTripDepartureCityMap",
                c => new
                    {
                        BusinessTripId = c.Long(nullable: false),
                        CityId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BusinessTripId, t.CityId });
            
            DropForeignKey("BusinessTrips.BusinessTripParticipant", "DepartureCityId", "Dictionaries.City");
            DropIndex("BusinessTrips.BusinessTripParticipant", new[] { "DepartureCityId" });
            DropColumn("BusinessTrips.BusinessTripParticipant", "DepartureCityId");
            CreateIndex("BusinessTrips.BusinessTripDepartureCityMap", "CityId");
            CreateIndex("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId");
            AddForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "CityId", "Dictionaries.City", "Id", cascadeDelete: true);
            AddForeignKey("BusinessTrips.BusinessTripDepartureCityMap", "BusinessTripId", "BusinessTrips.BusinessTrip", "Id", cascadeDelete: true);
        }
    }
}

