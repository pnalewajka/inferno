namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCascadeDeleteDataContentMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument");
            AddForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument");
            AddForeignKey("GDPR.DataConsentDocumentContent", "Id", "GDPR.DataConsentDocument", "Id");
        }
    }
}
