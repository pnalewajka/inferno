namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateExpenseRequestMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Workflows.ExpenseRequest",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        ProjectId = c.Long(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Long(nullable: false),
                        Justification = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Workflows.Request", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.ExpenseRequest", "Id", "Workflows.Request");
            DropIndex("Workflows.ExpenseRequest", new[] { "Id" });
            DropTable("Workflows.ExpenseRequest");
        }
    }
}
