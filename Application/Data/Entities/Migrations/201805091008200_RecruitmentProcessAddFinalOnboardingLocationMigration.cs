namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecruitmentProcessAddFinalOnboardingLocationMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", c => c.Long());
            CreateIndex("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
            AddForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId", "Dictionaries.Location");
            DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalOnboardingLocationId" });
            DropColumn("Recruitment.RecruitmentProcess", "FinalOnboardingLocationId");
        }
    }
}
