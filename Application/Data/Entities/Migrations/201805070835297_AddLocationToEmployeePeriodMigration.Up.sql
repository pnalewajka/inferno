﻿UPDATE P
	SET P.LocationId = E.LocationId
	FROM [Allocation].EmploymentPeriod P 
		INNER JOIN [Allocation].Employee E 
			ON E.Id = P.EmployeeIdEmployee 
	WHERE P.StartDate <= GETDATE() AND (P.EndDate >= GETDATE() or p.EndDate is null)