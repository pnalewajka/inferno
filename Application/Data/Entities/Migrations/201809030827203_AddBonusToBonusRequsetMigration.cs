namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBonusToBonusRequsetMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("Workflows.BonusRequest", "BonusId", c => c.Long());
            CreateIndex("Workflows.BonusRequest", "BonusId");
            AddForeignKey("Workflows.BonusRequest", "BonusId", "Compensation.Bonus", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Workflows.BonusRequest", "BonusId", "Compensation.Bonus");
            DropIndex("Workflows.BonusRequest", new[] { "BonusId" });
            DropColumn("Workflows.BonusRequest", "BonusId");
        }
    }
}
