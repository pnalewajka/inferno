namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGDPRDataOwnerMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "GDPR.DataOwner",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        EmployeeId = c.Long(nullable: false),
                        CandidateId = c.Long(nullable: false),
                        RecommendingPersonId = c.Long(nullable: false),
                        UserIdImpersonatedBy = c.Long(),
                        UserIdModifiedBy = c.Long(),
                        ModifiedOn = c.DateTime(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Recruitment.Candidate", t => t.CandidateId)
                .ForeignKey("Allocation.Employee", t => t.EmployeeId)
                .ForeignKey("Accounts.User", t => t.UserIdImpersonatedBy)
                .ForeignKey("Accounts.User", t => t.UserIdModifiedBy)
                .ForeignKey("Recruitment.RecommendingPerson", t => t.RecommendingPersonId)
                .ForeignKey("Accounts.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.EmployeeId)
                .Index(t => t.CandidateId)
                .Index(t => t.RecommendingPersonId)
                .Index(t => t.UserIdImpersonatedBy)
                .Index(t => t.UserIdModifiedBy);            
            
        }
        
        public override void Down()
        {
            DropForeignKey("GDPR.DataOwner", "UserId", "Accounts.User");
            DropForeignKey("GDPR.DataOwner", "RecommendingPersonId", "Recruitment.RecommendingPerson");
            DropForeignKey("GDPR.DataOwner", "UserIdModifiedBy", "Accounts.User");
            DropForeignKey("GDPR.DataOwner", "UserIdImpersonatedBy", "Accounts.User");
            DropForeignKey("GDPR.DataOwner", "EmployeeId", "Allocation.Employee");
            DropForeignKey("GDPR.DataOwner", "CandidateId", "Recruitment.Candidate");
            DropIndex("GDPR.DataOwner", new[] { "UserIdModifiedBy" });
            DropIndex("GDPR.DataOwner", new[] { "UserIdImpersonatedBy" });
            DropIndex("GDPR.DataOwner", new[] { "RecommendingPersonId" });
            DropIndex("GDPR.DataOwner", new[] { "CandidateId" });
            DropIndex("GDPR.DataOwner", new[] { "EmployeeId" });
            DropIndex("GDPR.DataOwner", new[] { "UserId" });
            DropTable("GDPR.DataOwner");
        }
    }
}
