﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Helpers;
    
    public partial class CustomerPortalRecreateCascadeConstraintMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201801301334214_CustomerPortalRecreateCascadeConstraintMigration.CustomerPortalCascadeFileSql.Up.sql");
        }
        
        public override void Down()
        {
        }
    }
}

