namespace Smt.Atomic.Data.Entities.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MergeMigration100420181503 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "Allocation.ProjectInvoicingRate",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            ProjectId = c.Long(nullable: false),
            //            Code = c.String(nullable: false, maxLength: 128),
            //            Value = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            Description = c.String(),
            //            Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("Allocation.Project", t => t.ProjectId, cascadeDelete: true)
            //    .Index(t => new { t.ProjectId, t.Code }, unique: true, name: "IX_ProjectCodePerProject");
            
            //AddColumn("Allocation.Project", "InvoicingAlgorithm", c => c.String());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalPositionId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "FinalStartDate", c => c.DateTime());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalLocationId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "FinalComment", c => c.String(maxLength: 255));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferPositionId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "OfferContractType", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferSalary", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferStartDate", c => c.String(maxLength: 50));
            //AddColumn("Recruitment.RecruitmentProcess", "OfferLocationId", c => c.Long());
            //AddColumn("Recruitment.RecruitmentProcess", "OfferComment", c => c.String(maxLength: 255));
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalPositionId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            //CreateIndex("Recruitment.RecruitmentProcess", "FinalLocationId");
            //CreateIndex("Recruitment.RecruitmentProcess", "OfferPositionId");
            //CreateIndex("Recruitment.RecruitmentProcess", "OfferLocationId");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location", "Id");
            //AddForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle", "Id");
            //DropColumn("Allocation.Employee", "IsCertifiedToWorkWithRecruitmentData");
        }
        
        public override void Down()
        {
            //AddColumn("Allocation.Employee", "IsCertifiedToWorkWithRecruitmentData", c => c.Boolean(nullable: false));
            //DropForeignKey("Recruitment.RecruitmentProcess", "OfferPositionId", "SkillManagement.JobTitle");
            //DropForeignKey("Recruitment.RecruitmentProcess", "OfferLocationId", "Dictionaries.Location");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId", "TimeTracking.ContractType");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalPositionId", "SkillManagement.JobTitle");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId", "TimeTracking.ContractType");
            //DropForeignKey("Recruitment.RecruitmentProcess", "FinalLocationId", "Dictionaries.Location");
            //DropForeignKey("Allocation.ProjectInvoicingRate", "ProjectId", "Allocation.Project");
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferLocationId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "OfferPositionId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLocationId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalLongTermContractTypeId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalTrialContractTypeId" });
            //DropIndex("Recruitment.RecruitmentProcess", new[] { "FinalPositionId" });
            //DropIndex("Allocation.ProjectInvoicingRate", "IX_ProjectCodePerProject");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferComment");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferLocationId");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferStartDate");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferContractType");
            //DropColumn("Recruitment.RecruitmentProcess", "OfferPositionId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalComment");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalLocationId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalStartDate");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalTrialSalary");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalLongTermContractTypeId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalTrialContractTypeId");
            //DropColumn("Recruitment.RecruitmentProcess", "FinalPositionId");
            //DropColumn("Allocation.Project", "InvoicingAlgorithm");
            //DropTable("Allocation.ProjectInvoicingRate");
        }
    }
}
