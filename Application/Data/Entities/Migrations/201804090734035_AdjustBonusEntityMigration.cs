﻿namespace Smt.Atomic.Data.Entities.Migrations
{
    using Helpers;

    public partial class AdjustBonusEntityMigration : AtomicDbMigration
    {
        public override void Up()
        {
            SqlBatchFromResources("Smt.Atomic.Data.Entities.Migrations.201804090734035_AdjustBonusEntityMigration.ClearBonusTable.Up.sql");
            AddColumn("Compensation.Bonus", "ProjectId", c => c.Long(nullable: false));
            CreateIndex("Compensation.Bonus", "ProjectId");
            AddForeignKey("Compensation.Bonus", "ProjectId", "Allocation.Project", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Compensation.Bonus", "ProjectId", "Allocation.Project");
            DropIndex("Compensation.Bonus", new[] { "ProjectId" });
            DropColumn("Compensation.Bonus", "ProjectId");
        }
    }
}

