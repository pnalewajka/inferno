﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Exchange.Interfaces;
using Smt.Atomic.Data.Exchange.Services;

namespace Smt.Atomic.Data.Exchange
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp || containerType == ContainerType.WebApi || containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ICalendarOfficeService>().ImplementedBy<CalendarOfficeService>());
                container.Register(Component.For<IEmailOfficeService>().ImplementedBy<EmailOfficeService>());
                container.Register(Component.For<IInboundEmailOfficeService>().ImplementedBy<InboundEmailOfficeService>());
            }
        }
    }
}
