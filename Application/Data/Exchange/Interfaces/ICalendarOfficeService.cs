﻿using System;
using System.Collections.Generic;
using Microsoft.Exchange.WebServices.Data;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Data.Exchange.Interfaces
{
    public interface ICalendarOfficeService
    {
        BoolResult IsCalendarAvailable(string groupEmail);

        string CreateAppointment(string groupEmail, CalendarAppointmentType calendarAppointmentType,
            string appointmentName,
            DateTime startDate, DateTime endDate, string description, string requesterName, IList<string> attendeeEmails);

        string CreateAppointment(string groupEmail, CalendarAppointmentType calendarAppointmentType,
            DateTime startDate, DateTime endDate, string subject, string body, IList<string> attendeeEmails);

        void EditAppointment(string itemId, DateTime startDate, DateTime endDate);

        void DeleteAppointment(string itemId);

        FindItemsResults<Appointment> LoadAppointments(string groupEmail, DateTime startDate, DateTime endDate);
    }
}


