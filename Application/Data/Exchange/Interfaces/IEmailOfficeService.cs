﻿using System;
using Microsoft.Exchange.WebServices.Data;

namespace Smt.Atomic.Data.Exchange.Interfaces
{
    public interface IEmailOfficeService
    {
        void ReadEmails(string mailBox, DateTime receivedAfter, int pageSize, Action<EmailMessage> action);

        byte[] GetAttachmentFileContent(Attachment attachment);
    }
}