﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Exchange.WebServices.Data;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Exchange.Interfaces;
using Smt.Atomic.Data.Exchange.Resources;

namespace Smt.Atomic.Data.Exchange.Services
{
    public class CalendarOfficeService : OfficeServiceBase, ICalendarOfficeService
    {
        public CalendarOfficeService(
            ISystemParameterService systemParameterService,
            ISettingsProvider settingsProvider)
            : base(systemParameterService, settingsProvider)
        {
        }

        public BoolResult IsCalendarAvailable(string groupEmail)
        {
            var result = new BoolResult(true);
            var service = CreateService();
            var danteUser = _systemParameterService.GetParameter<string>(ParameterKeys.ExchangeSyncUser);

            var groupCalendar = GetGroupCalendar(service, groupEmail);

            if (groupCalendar == null || !groupCalendar.EffectiveRights.HasFlag(EffectiveRights.Read))
            {
                result.IsSuccessful = false;
                result.AddAlert(AlertType.Error, string.Format(OfficeCalendarResources.MissingPermissionForCalendar, EffectiveRights.Read, danteUser));

                return result;
            }

            if (!groupCalendar.EffectiveRights.HasFlag(EffectiveRights.CreateContents))
            {
                result.IsSuccessful = false;
                result.AddAlert(AlertType.Error, string.Format(OfficeCalendarResources.MissingPermissionForCalendar, EffectiveRights.CreateContents, danteUser));
            }

            return result;
        }

        public string CreateAppointment(string groupEmail, CalendarAppointmentType calendarAppointmentType, string appointmentName,
            DateTime startDate, DateTime endDate, string description, string requesterName, IList<string> attendeeEmails)
        {
            var subject = $"{requesterName}: {appointmentName}";
            var body = $"{requesterName}: {description}";

            return CreateAppointment(groupEmail, calendarAppointmentType, startDate, endDate, subject, body, attendeeEmails);
        }

        public string CreateAppointment(string groupEmail, CalendarAppointmentType calendarAppointmentType, DateTime startDate, DateTime endDate, string subject, string body, IList<string> attendeeEmails)
        {
            var service = CreateService();
            var appointment = new Appointment(service)
            {
                Subject = subject,
                Body = body,
                Start = startDate,
                End = endDate,
                LegacyFreeBusyStatus = GetStatusByTypeAppointment(calendarAppointmentType),
            };

            foreach (var email in attendeeEmails)
            {
                appointment.RequiredAttendees.Add(email);
            }

            var calendar = GetGroupCalendar(service, groupEmail);

            if (calendar == null)
            {
                throw new NullReferenceException("Calendar cannot be found");
            }

            appointment.Save(calendar.Id, SendInvitationsMode.SendToNone);

            return appointment.Id.UniqueId;
        }

        private LegacyFreeBusyStatus GetStatusByTypeAppointment(CalendarAppointmentType calendarAppointment)
        {
            switch (calendarAppointment)
            {
                case CalendarAppointmentType.Absence:
                    return LegacyFreeBusyStatus.OOF;
                case CalendarAppointmentType.HomeOffice:
                    return LegacyFreeBusyStatus.WorkingElsewhere;
                case CalendarAppointmentType.Meeting:
                    return LegacyFreeBusyStatus.Busy;
                default:
                    return LegacyFreeBusyStatus.Busy;
            }
        }

        public void EditAppointment(string itemId, DateTime startDate, DateTime endDate)
        {
            var service = CreateService();
            var appointment = Appointment.Bind(service, new ItemId(itemId));

            appointment.Start = startDate;
            appointment.End = endDate;
            appointment.StartTimeZone = TimeZoneInfo.Local;
            appointment.EndTimeZone = TimeZoneInfo.Local;

            appointment.Update(ConflictResolutionMode.AlwaysOverwrite);
        }

        public void DeleteAppointment(string itemId)
        {
            var service = CreateService();
            var appointment = Appointment.Bind(service, new ItemId(itemId));
            appointment.Delete(DeleteMode.HardDelete);
        }

        public FindItemsResults<Appointment> LoadAppointments(string groupEmail, DateTime startDate, DateTime endDate)
        {
            var service = CreateService();
            var calendar = GetGroupCalendar(service, groupEmail);

            if (calendar == null)
            {
                throw new NullReferenceException("Calendar cannot be found");
            }

            var cView = new CalendarView(startDate, endDate, 1000)
            {
                PropertySet = new PropertySet(
                    ItemSchema.Id,
                    ItemSchema.Subject,
                    AppointmentSchema.Start,
                    AppointmentSchema.End)
            };

            var appointments = calendar.FindAppointments(cView);

            return appointments;
        }

        private CalendarFolder GetGroupCalendar(ExchangeService service, string groupEmail)
        {
            try
            {
                var groupFolder = new FolderId(WellKnownFolderName.Root, groupEmail);

                // Check https://msdn.microsoft.com/en-us/library/hh354773(v=exchg.80).aspx
                var sfSearchFilter = new SearchFilter.IsEqualTo(FolderSchema.FolderClass, "IPF.Appointment");
                var view = new FolderView(1000)
                {
                    PropertySet = new PropertySet(BasePropertySet.IdOnly) { FolderSchema.DisplayName, FolderSchema.EffectiveRights },
                    Traversal = FolderTraversal.Deep
                };

                var findFolderResults = service.FindFolders(groupFolder, sfSearchFilter, view).Cast<CalendarFolder>().ToList();

                var mainCalendar = findFolderResults.FirstOrDefault(c => c.DisplayName.ToLower() == "calendar")
                                   ?? findFolderResults.FirstOrDefault();

                return mainCalendar;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}