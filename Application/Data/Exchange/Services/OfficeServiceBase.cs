﻿using System;
using Microsoft.Exchange.WebServices.Data;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.Data.Exchange.Services
{
    public class OfficeServiceBase
    {
        protected readonly ISettingsProvider _settingsProvider;
        protected readonly ISystemParameterService _systemParameterService;

        public OfficeServiceBase(
            ISystemParameterService systemParameterService,
            ISettingsProvider settingsProvider)
        {
            _systemParameterService = systemParameterService;
            _settingsProvider = settingsProvider;
        }

        protected virtual ExchangeService CreateService()
        {
            var login = _systemParameterService.GetParameter<string>(ParameterKeys.ExchangeSyncUser);
            var password = _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.Password;
            var url = _systemParameterService.GetParameter<string>(ParameterKeys.ExchangeOutlookUrl);

            return CreateExchangeService(login, password, url);
        }

        protected ExchangeService CreateExchangeService(string login, string password, string url)
        {
            var service = new ExchangeService(ExchangeVersion.Exchange2010)
            {
                Credentials = new WebCredentials(login, password),
                Url = new Uri(url)
            };

            return service;
        }
    }
}
