﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Exchange.WebServices.Data;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Exchange.Interfaces;

namespace Smt.Atomic.Data.Exchange.Services
{
    public class EmailOfficeService : OfficeServiceBase, IEmailOfficeService
    {
        public EmailOfficeService(
            ISystemParameterService systemParameterService,
            ISettingsProvider settingsProvider)
            : base(systemParameterService, settingsProvider)
        {
        }

        public void ReadEmails(string mailBox, DateTime receivedAfter, int pageSize, Action<EmailMessage> action)
        {
            var service = CreateService();

            ReadEmails(service, mailBox, receivedAfter, pageSize, action);
        }

        public byte[] GetAttachmentFileContent(Attachment attachment)
        {
            if (attachment is FileAttachment fileAttachment)
            {
                using (var memoryStream = new MemoryStream())
                {
                    fileAttachment.Load(memoryStream);

                    return memoryStream.ToArray();
                }
            }

            return null;
        }

        private void ReadEmails(ExchangeService serviceInstance, string groupEmail, DateTime receivedAfter, int pageSize, Action<EmailMessage> processEmail)
        {
            var offset = 0;
            var moreEmailsAvailable = true;

            Folder inbox = Folder.Bind(serviceInstance, new FolderId(WellKnownFolderName.Inbox, groupEmail));

            SearchFilter searchFilter = new SearchFilter.IsGreaterThan(EmailMessageSchema.DateTimeReceived, receivedAfter.AddSeconds(1));
            ItemView view = new ItemView(pageSize, offset, OffsetBasePoint.Beginning);
            PropertySet propSet = new PropertySet(
                BasePropertySet.IdOnly,
                EmailMessageSchema.ToRecipients,
                EmailMessageSchema.From,
                EmailMessageSchema.Subject,
                EmailMessageSchema.Body,
                EmailMessageSchema.DateTimeSent,
                EmailMessageSchema.DateTimeReceived,
                EmailMessageSchema.Attachments);

            while (moreEmailsAvailable)
            {
                FindItemsResults<Item> findResults = serviceInstance.FindItems(inbox.Id, searchFilter, view);

                if (!findResults.Any())
                {
                    return;
                }

                var itemIds = findResults.Select(m => m.Id);

                ServiceResponseCollection<GetItemResponse> response = serviceInstance.BindToItems(itemIds, propSet);

                if (response.OverallResult == ServiceResult.Error)
                {
                    throw new Exception("Error while collecting emails");
                }

                foreach (GetItemResponse getItemResponse in response)
                {
                    Item item = getItemResponse.Item;
                    EmailMessage message = (EmailMessage)item;

                    processEmail.Invoke(message);
                }

                moreEmailsAvailable = findResults.MoreAvailable;

                if (moreEmailsAvailable)
                {
                    view.Offset += pageSize;
                }
            }
        }
    }
}