﻿using Microsoft.Exchange.WebServices.Data;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Exchange.Interfaces;

namespace Smt.Atomic.Data.Exchange.Services
{
    public class InboundEmailOfficeService : EmailOfficeService, IInboundEmailOfficeService
    {
        public InboundEmailOfficeService(
            ISystemParameterService systemParameterService,
            ISettingsProvider settingsProvider)
            : base(systemParameterService, settingsProvider)
        {
        }

        protected override ExchangeService CreateService()
        {
            if (_settingsProvider.AuthorizationProviderSettings.RecruitmentExchangeCredentialSettings != null
                && !string.IsNullOrWhiteSpace(_settingsProvider.AuthorizationProviderSettings.RecruitmentExchangeCredentialSettings.Login))
            {
                var login = _settingsProvider.AuthorizationProviderSettings.RecruitmentExchangeCredentialSettings.Login;
                var password = _settingsProvider.AuthorizationProviderSettings.RecruitmentExchangeCredentialSettings.Password;
                var url = _systemParameterService.GetParameter<string>(ParameterKeys.ExchangeOutlookUrl);

                return CreateExchangeService(login, password, url);
            }

            return base.CreateService();
        }
    }
}