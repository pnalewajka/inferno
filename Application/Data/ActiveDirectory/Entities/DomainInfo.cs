﻿using System;
using System.DirectoryServices.Linq.Attributes;
using System.DirectoryServices.Linq.EntryObjects;

namespace Smt.Atomic.Data.ActiveDirectory.Entities
{
    [DirectoryType("msSFU30DomainInfo")]
    public class DomainInfo : EntryObject
    {
        [DirectoryProperty("objectGUID", true)]
        public Guid Id { get; set; }

        [DirectoryProperty("distinguishedName", true)]
        public string Path { get; set; }

        [DirectoryProperty("msSFU30MaxUidNumber", true)]
        public int? MaxUidNumber { get; set; }
    }
}
