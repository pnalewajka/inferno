﻿using System;
using System.DirectoryServices.Linq.Attributes;
using System.DirectoryServices.Linq.EntryObjects;

namespace Smt.Atomic.Data.ActiveDirectory.Entities
{
    [DirectoryType("organizationalUnit")]
    public class OrgUnit : EntryObject
    {
        [DirectoryProperty("objectGUID", true)]
        public Guid Id { get; set; }

        [DirectoryProperty("distinguishedName", true)]
        public string Path { get; set; }
    }
}