using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.Linq;
using System.DirectoryServices.Linq.Attributes;
using System.DirectoryServices.Linq.EntryObjects;
using System.Linq;

namespace Smt.Atomic.Data.ActiveDirectory.Entities
{
    [DebuggerDisplay("{Name}")]
    [DirectoryType("group", "OU=Groups")]
    public class Group : EntryObject
    {
        private string _ownerId;
        private List<string> _managersIds = new List<string>();
        private string _description;
        private string _info;
        private string _ownerDn;

        [DirectoryProperty("objectGUID", true)]
        public Guid Id { get; set; }

        [DirectoryProperty("sAMAccountName")]
        public string Name { get; set; }

        [DirectoryProperty("mail")]
        public string Mail { get; set; }

        [DirectoryProperty("description")]
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    NotifyPropertyChanged(nameof(Description));
                }
            }
        }

        [DirectoryProperty("info")]
        public string Info
        {
            get
            {
                return _info;
            }
            set
            {
                if (value != _info)
                {
                    _info = value;
                    NotifyPropertyChanged(nameof(Info));
                }
            }
        }

        [DirectoryProperty("proxyAddresses")]
        public IEnumerable<string> Aliases { get; set; }

        [DirectoryProperty("managedBy")]
        public string OwnerDn
        {
            get
            {
                return _ownerDn;
            }
            set
            {
                if (value != _ownerDn)
                {
                    _ownerDn = value;
                    NotifyPropertyChanged(nameof(OwnerDn));
                }
            }
        }

        [DirectoryProperty("xOwner")]
        public string OwnerId
        {
            get
            {
                return _ownerId;
            }
            set
            {
                if (value != _ownerId)
                {
                    _ownerId = value;
                    NotifyPropertyChanged(nameof(OwnerId));
                }
            }
        }

        [DirectoryProperty("xManagers")]
        public IEnumerable<string> ManagersIds
        {
            get { return _managersIds; }
            set { _managersIds = value.ToList(); }
        }

        [EntryCollectionProperty("memberOf", MatchingRule = MatchingRuleType.InChain)]
        public EntryCollection<User> Users => ((IEntryWithRelationships)this).RelationshipManager.GetEntryCollection<User>(nameof(Users));

        [EntryCollectionProperty("memberOf")]
        public EntryCollection<Group> Subgroups => ((IEntryWithRelationships)this).RelationshipManager.GetEntryCollection<Group>(nameof(Subgroups));

        public void AddManagerId(string id)
        {
            if (!_managersIds.Contains(id))
            {
                _managersIds.Add(id);
                NotifyPropertyChanged(nameof(ManagersIds));
            }
        }

        public void ClearManagersIds()
        {
            _managersIds.Clear();
            NotifyPropertyChanged(nameof(ManagersIds));
        }
    }
}