﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.Linq.Attributes;
using System.DirectoryServices.Linq.EntryObjects;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Smt.Atomic.Data.ActiveDirectory.Entities
{
    [DirectoryType("User")]
    [DebuggerDisplay("{FirstName} {LastName}")]
    public class User : UserEntryObject
    {
        private string _path;
        private string _department;
        private string _location;
        private string _title;
        private string _firstName;
        private string _lastName;
        private string _email;
        private string _skypeName;
        private string _company;
        private string _companyOfOrigin;
        private string _jobMatrixLevel;
        private string _mainCompetence;
        private string _lineManager;
        private string _crmId;
        private string _displayName;
        private string _userName;
        private string _principalName;
        private string _startDate;
        private string _endDate;
        private string _contractType;
        private string _profileType;
        private DateTime? _effectiveDate;
        private string _acronym;

        [DirectoryProperty("objectGUID", true)]
        public Guid Id { get; set; }

        [DirectoryProperty("displayName")]
        public string DisplayName
        {
            get { return _displayName; }
            set { SetField(ref _displayName, value); }
        }

        [DirectoryProperty("sAMAccountName")]
        public string UserName
        {
            get { return _userName; }
            set { SetField(ref _userName, value); }
        }

        [DirectoryProperty("userPrincipalName")]
        public string PrincipalName
        {
            get { return _principalName; }
            set { SetField(ref _principalName, value); }
        }

        [DirectoryProperty("givenName")]
        public string FirstName
        {
            get { return _firstName; }
            set { SetField(ref _firstName, value); }
        }

        [DirectoryProperty("sn")]
        public string LastName
        {
            get { return _lastName; }
            set { SetField(ref _lastName, value); }
        }

        [DirectoryProperty("mail")]
        public string Email
        {
            get { return _email; }
            set { SetField(ref _email, value); }
        }

        [DirectoryProperty("skype")]
        public string SkypeName
        {
            get { return _skypeName; }
            set { SetField(ref _skypeName, value); }
        }

        [DirectoryProperty("title")]
        public string Title
        {
            get { return _title; }
            set { SetField(ref _title, value); }
        }

        [DirectoryProperty("department")]
        public string Department
        {
            get { return _department; }
            set { SetField(ref _department, value); }
        }

        [DirectoryProperty("l")]
        public string Location
        {
            get { return _location; }
            set { SetField(ref _location, value); }
        }

        [DirectoryProperty("xCRMId")]
        public string CrmId
        {
            get { return _crmId; }
            set { SetField(ref _crmId, value); }
        }

        [DirectoryProperty("company")]
        public string Company
        {
            get { return _company; }
            set { SetField(ref _company, value); }
        }

        [DirectoryProperty("xCompanyOfOrigin")]
        public string CompanyOfOrigin
        {
            get { return _companyOfOrigin; }
            set { SetField(ref _companyOfOrigin, value); }
        }

        [DirectoryProperty("xPlaceOfWork")]
        public string PlaceOfWork { get; set; }

        [DirectoryProperty("xLineManager")]
        public string LineManager
        {
            get { return _lineManager; }
            set { SetField(ref _lineManager, value); }
        }

        [DirectoryProperty("xJobMatrixLevel")]
        public string JobMatrixLevel
        {
            get { return _jobMatrixLevel; }
            set { SetField(ref _jobMatrixLevel, value); }
        }

        [DirectoryProperty("xMainCompetence")]
        public string MainCompetence
        {
            get { return _mainCompetence; }
            set { SetField(ref _mainCompetence, value); }
        }

        [DirectoryProperty("xStartDate")]
        public string StartDate
        {
            get { return _startDate; }
            set { SetField(ref _startDate, value); }
        }

        [DirectoryProperty("xEndDate")]
        public string EndDate
        {
            get { return _endDate; }
            set { SetField(ref _endDate, value); }
        }

        [DirectoryProperty("xContractType")]
        public string ContractType
        {
            get { return _contractType; }
            set { SetField(ref _contractType, value); }
        }

        [DirectoryProperty("xProfileType")]
        public string ProfileType
        {
            get { return _profileType; }
            set { SetField(ref _profileType, value); }
        }

        [DirectoryProperty("xEffectiveDate")]
        public DateTime? EffectiveDate
        {
            get { return _effectiveDate; }
            set { SetField(ref _effectiveDate, value); }
        }

        [DirectoryProperty("xAcronym")]
        public string Acronym
        {
            get { return _acronym; }
            set { SetField(ref _acronym, value); }
        }

        [DirectoryProperty("xradiusTunnelType")]
        public string RadiusTunnelType { get; set; }

        [DirectoryProperty("xradiustunnelmediumtype")]
        public string RadiusTunnelMediumType { get; set; }

        [DirectoryProperty("xradiustunnelprivategroupid")]
        public string RadiusTunnelPrivateGroupId { get; set; }

        [DirectoryProperty("gidNumber")]
        public string Gid { get; set; }

        [DirectoryProperty("homeDirectory")]
        public string HomeDirectory { get; set; }

        [DirectoryProperty("loginShell")]
        public string LoginShell { get; set; }

        [DirectoryProperty("msSFU30Name")]
        public string MSSFU30Name { get; set; }

        [DirectoryProperty("msSFU30NisDomain")]
        public string MSSFU30NisDomain { get; set; }

        [DirectoryProperty("uid")]
        public string Uid { get; set; }

        [DirectoryProperty("unixHomeDirectory")]
        public string UnixHomeDirectory { get; set; }

        [DirectoryProperty("uidNumber")]
        public int? UidNumber { get; set; }

        [DirectoryProperty("distinguishedName")]
        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                if (_path != value)
                {
                    _path = value;
                    TreePath = _path.Split(',').ToList();
                }
            }
        }

        [DirectoryProperty("treePath")]
        public List<string> TreePath { get; private set; }

        [DirectoryProperty("physicalDeliveryOfficeName")]
        public string Office { get; set; }

        [EntryCollectionProperty("member")]
        public EntryCollection<Group> Groups => ((IEntryWithRelationships)this).RelationshipManager.GetEntryCollection<Group>(nameof(Groups));

        protected void SetField<T>(ref T field, T value, [CallerMemberName]string propertyName = null)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                NotifyPropertyChanged(propertyName);
            }        
        }
    }
}