﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Linq;
using System.DirectoryServices.Linq.ChangeTracking;
using System.DirectoryServices.Linq.EntryObjects;
using System.IO;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Data.ActiveDirectory.Entities;

namespace Smt.Atomic.Data.ActiveDirectory
{
    public class CoreDirectoryContext : DirectoryContext
    {
        private readonly ILogger _logger;
        private readonly ISet<Guid> _allowedAllowedWritableObjects;
        private IEntrySet<User> _users;
        private IEntrySet<Group> _groups;
        private IEntrySet<OrgUnit> _orgUnits;
        private IEntrySet<DomainInfo> _domainInfos;
        private readonly ActiveDirectorySettings _activeDirectorySettings;
        private readonly ActiveDirectoryWriterSettings _activeDirectoryWriterSettings;
        private readonly bool _useCustomResultMapper;
        private readonly bool _useCustomChangeTracker;
        private readonly bool _isWritable;

        public IEntrySet<User> Users => _users ?? (_users = CreateEntrySet<User>());

        public IEntrySet<Group> Groups => _groups ?? (_groups = CreateEntrySet<Group>());

        public IEntrySet<OrgUnit> OrgUnits => _orgUnits ?? (_orgUnits = CreateEntrySet<OrgUnit>());

        public IEntrySet<DomainInfo> DomainInfos => _domainInfos ?? (_domainInfos = CreateEntrySet<DomainInfo>());

        public CoreDirectoryContext(ISettingsProvider settingsProvider, bool isWritable = false, ISet<Guid> allowedWritableObjects = null, ILogger logger = null, bool useCustomResultMapper = false, bool useCustomChangeTracker = false)
            : base(string.Empty)
        {
            _useCustomResultMapper = useCustomResultMapper;
            _useCustomChangeTracker = useCustomChangeTracker;
            _isWritable = isWritable;

            if (_isWritable && logger == null)
            {
                throw new InvalidDataException("Wrong configuration provided, logger expected for a writable context");
            }

            _logger = logger;
            _allowedAllowedWritableObjects = allowedWritableObjects;

            _activeDirectorySettings = settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings;
            _activeDirectoryWriterSettings = settingsProvider.AuthorizationProviderSettings.ActiveDirectoryWriterSettings;
            ConnectionString = _activeDirectorySettings.ConnectionString;

            if (_isWritable)
            {
                RootEntry.Username = _activeDirectoryWriterSettings.User;
                RootEntry.Password = _activeDirectoryWriterSettings.Password;
            }
            else
            {
                if (!_activeDirectorySettings.Username.IsNullOrEmpty())
                {
                    RootEntry.Username = _activeDirectorySettings.Username;
                    RootEntry.Password = _activeDirectorySettings.Password;
                }
            }
        }

        public new void SubmitChanges()
        {
            SkipNonWritableObjects();
            base.SubmitChanges();
        }

        public void AddMembersToGroup(Guid groupId, IEnumerable<Guid> membersIds)
        {
            if (_isWritable)
            {
                using (var principalContext = new PrincipalContext(ContextType.Domain, 
                    _activeDirectorySettings.Domain, 
                    _activeDirectorySettings.GroupsContainer, 
                    ContextOptions.SimpleBind,
                    _activeDirectoryWriterSettings.User, 
                    _activeDirectoryWriterSettings.Password))
                {
                    var principalGroup = GroupPrincipal.FindByIdentity(principalContext, IdentityType.Guid, groupId.ToString());

                    if (principalGroup != null)
                    {
                        foreach (var memberId in membersIds)
                        {
                            principalGroup.Members.Add(principalContext, IdentityType.Guid, memberId.ToString());
                        }

                        principalGroup.Save();
                    }
                }
            }
        }

        public void RemoveMembersFromGroup(Guid groupId, IEnumerable<Guid> membersIds)
        {
            if (_isWritable)
            {
                using (var principalContext = new PrincipalContext(ContextType.Domain,
                    _activeDirectorySettings.Domain,
                    _activeDirectorySettings.GroupsContainer,
                    ContextOptions.SimpleBind,
                    _activeDirectoryWriterSettings.User,
                    _activeDirectoryWriterSettings.Password))
                {
                    var principalGroup = GroupPrincipal.FindByIdentity(principalContext, IdentityType.Guid, groupId.ToString());

                    if (principalGroup != null)
                    {
                        foreach (var memberId in membersIds)
                        {
                            principalGroup.Members.Remove(principalContext, IdentityType.Guid, memberId.ToString());
                        }

                        principalGroup.Save();
                    }
                }
            }
        }

        private void SkipNonWritableObjects()
        {
            if (_allowedAllowedWritableObjects == null)
            {
                return;
            }

            var toBeSkipped = new List<EntryObject>();
            var changeTracker = (ChangeTracker) ChangeTracker;

            foreach (var entryObject in changeTracker.Changes.Keys)
            {
                var user = entryObject as User;

                if (user != null)
                {
                    if (!_allowedAllowedWritableObjects.Contains(user.Id))
                    {
                        toBeSkipped.Add(entryObject);
                        _logger.Info($"Skipping AD write for user {user.UserName}");
                    }
                    else
                    {
                        _logger.Info($"Writing AD changes for user {user.UserName}");
                    }
                }
            }

            foreach (var o in toBeSkipped)
            {
                changeTracker.Changes.Remove(o);
            }
        }

        protected override IResultMapper GetResultMapper()
        {
            return _useCustomResultMapper 
                ? new CoreResultMapper(this) 
                : base.GetResultMapper();
        }

        protected override IChangeTracker GetChangeTracker()
        {
            return _useCustomChangeTracker
                ? new CoreChangeTracker(this)
                : base.GetChangeTracker();
        }
    }
}