﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.Linq;
using System.DirectoryServices.Linq.Attributes;
using System.DirectoryServices.Linq.ChangeTracking;
using System.DirectoryServices.Linq.EntryObjects;
using System.Linq;
using System.Reflection;
using Castle.Core.Internal;

namespace Smt.Atomic.Data.ActiveDirectory
{
    public class CoreChangeTracker : ChangeTracker
    {
        public CoreChangeTracker(DirectoryContext context) 
            : base(context)
        {
        }

        public override void SubmitChanges()
        {
            foreach (var item in Changes)
            {
                var entry = item.Key;

                if (GetPropertyValue<ChangeState>(entry, "ChangeState") == ChangeState.Insert)
                {
                    GetPropertyValue<DirectoryEntry>(entry, "Entry").RefreshCache();
                    SetPropertyValue(entry, "ChangeState", ChangeState.Update);
                }

                if (GetPropertyValue<ChangeState>(entry, "ChangeState") == ChangeState.Update)
                {
                    UpdateEntry(entry, item.Value);
                    GetPropertyValue<DirectoryEntry>(entry, "Entry").CommitChanges();
                }
                else if (GetPropertyValue<ChangeState>(entry, "ChangeState") == ChangeState.Delete)
                {
                    var ent = GetPropertyValue<DirectoryEntry>(entry, "Entry");
                    ent.Parent.Children.Remove(ent);
                }
            }

            Changes.Clear();
        }

        private T GetPropertyValue<T>(EntryObject entry, string propertyName)
        {
            return (T)entry.GetType()
                .GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(entry);
        }

        private void SetPropertyValue(EntryObject entry, string propertyName, object value)
        {
            entry.GetType()
                .GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance)
                .SetValue(entry, value);
        }

        private void UpdateEntry(EntryObject entry, IEnumerable<string> properties)
        {
            var entryType = entry.GetType();

            foreach (var name in properties)
            {
                var property = entryType.GetProperty(name);
                var attribute = property.GetAttribute<DirectoryPropertyAttribute>();

                if (attribute == null || attribute.IsReadOnly)
                {
                    continue;
                }

                var value = GetPropertyValue(entry, property);

                if (value != null)
                {
                    var directoryEntry = GetPropertyValue<DirectoryEntry>(entry, "Entry");
                    var propertyCollection = directoryEntry.Properties[attribute.Name];

                    if (property.PropertyType != typeof (string) && typeof (IEnumerable).IsAssignableFrom(property.PropertyType))
                    {
                        var enumerable = (IEnumerable) value;
                        var array = enumerable.Cast<object>().ToArray();
                        propertyCollection.Clear();
                        propertyCollection.AddRange(array);
                    }
                    else
                    {
                        propertyCollection.Value = value;
                    }
                }
                else
                {
                    GetPropertyValue<DirectoryEntry>(entry, "Entry").Properties[attribute.Name].Clear();
                }
            }
        }

        private static object GetPropertyValue(EntryObject entry, PropertyInfo property)
        {
            var value = property.GetValue(entry, null);

            if (value is Guid)
            {
                return ((Guid)value).ToByteArray();
            }

            return value;
        }
    }
}
