using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.Linq;
using System.DirectoryServices.Linq.Attributes;
using System.DirectoryServices.Linq.EntryObjects;
using System.Reflection;
using Castle.Core.Internal;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Data.ActiveDirectory
{
    public class CoreResultMapper : ResultMapper
    {
        public CoreResultMapper(DirectoryContext context)
            : base(context)
        {
        }

        public override T Map<T>(SearchResult result)
        {
            return (T)Map(typeof(T), result);
        }

        public override object Map(Type type, SearchResult result)
        {
            if (result != null)
            {
                var mappedObject = Activator.CreateInstance(type);

                foreach (PropertyInfo property in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                {
                    MapProperty(mappedObject, property, result);
                }

                if (typeof(EntryObject).IsAssignableFrom(type))
                {
                    var entryObject = (EntryObject)mappedObject;
                    var directoryEntry = result.GetDirectoryEntry();

                    SetEntryObjectPropertyValue(entryObject, "Context", Context);
                    SetEntryObjectPropertyValue(entryObject, "ADPath", result.Path);
                    SetEntryObjectPropertyValue(entryObject, "Entry", directoryEntry);
                    entryObject.SetParent(directoryEntry.Parent);
                    Context.ChangeTracker.TrackChanges(entryObject);
                }

                return mappedObject;
            }

            return null;
        }

        private void SetEntryObjectPropertyValue(EntryObject entryObject, string propertyName, object value)
        {
            entryObject.GetType()
                .GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance)
                .SetValue(entryObject, value);
        }

        private static void MapProperty(object mappedObject, PropertyInfo property, SearchResult result)
        {
            string attributeName = GetADFieldName(property);

            if (property.CanWrite && result.Properties.Contains(attributeName))
            {
                var resultPropertyCollection = result.Properties[attributeName];

                if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
                {
                    var value = MapCollectionProperty(resultPropertyCollection, property);
                    property.SetValue(mappedObject, value, null);
                }
                else
                {
                    object value = resultPropertyCollection[0];
                    value = ConvertTo(value, property.PropertyType);
                    property.SetValue(mappedObject, value, null);
                }
            }
        }

        private static object ConvertTo(object value, Type targetType)
        {
            if (value != null)
            {
                var valueType = value.GetType();

                if (valueType != targetType)
                {
                    if (valueType == typeof(byte[]) && targetType == typeof(Guid))
                    {
                        return new Guid((byte[])value);
                    }

                    if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return Activator.CreateInstance(targetType, value);// new[] { value });
                    }

                    return Convert.ChangeType(value, targetType);
                }
            }

            return value;
        }

        private static object MapCollectionProperty(ResultPropertyValueCollection resultPropertyCollection, PropertyInfo property)
        {
            return resultPropertyCollection.ToCollection(typeof(List<>), property.PropertyType.GenericTypeArguments[0]);
        }

        private static string GetADFieldName(MemberInfo info)
        {
            var attribute = info.GetAttribute<DirectoryPropertyAttribute>();

            if (!string.IsNullOrEmpty(attribute?.Name))
            {
                return attribute.Name;
            }

            return info.Name;
        }
    }
}