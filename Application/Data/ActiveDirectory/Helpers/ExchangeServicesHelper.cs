﻿using System;
using System.Net;
using Microsoft.Exchange.WebServices.Data;

namespace Smt.Atomic.Data.ActiveDirectory.Helpers
{
    /// <summary>
    /// Helper responsible for connection with Exchange services
    /// </summary>
    public class ExchangeServicesHelper
    {
        /// <summary>
        /// Sets autoresponder according to provided configuration
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="serviceUrl"></param>
        /// <param name="configuration"></param>
        public static void SetAutoResponderConfiguration(ICredentials credentials, Uri serviceUrl, AutoresponderConfiguration configuration)
        {
            var service = new ExchangeService(ExchangeVersion.Exchange2013_SP1)
            {
                Credentials = new WebCredentials(credentials),
                Url = serviceUrl
            };

            var settings = new OofSettings
            {
                State = (OofState)configuration.State,
                Duration = new TimeWindow(configuration.StartDate, configuration.EndDate),
                ExternalAudience = OofExternalAudience.All,
                InternalReply = new OofReply(configuration.InternalMessage),
                ExternalReply = new OofReply(configuration.ExternalMessage),
            };

            service.SetUserOofSettings(configuration.Email, settings);
        }

        /// <summary>
        /// Gets autoresponder configuration
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="serviceUrl"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public static AutoresponderConfiguration GetAutoResponderConfiguration(ICredentials credentials, Uri serviceUrl, string email)
        {
            var service = new ExchangeService(ExchangeVersion.Exchange2013_SP1)
            {
                Credentials = new WebCredentials(credentials),
                Url = serviceUrl
            };

            var settings = service.GetUserOofSettings(email);

            return new AutoresponderConfiguration
            {
                Email = email,
                StartDate = settings.Duration.StartTime,
                EndDate = settings.Duration.EndTime,
                InternalMessage = settings.InternalReply,
                ExternalMessage = settings.ExternalReply,
                State = (AutoresponderState)settings.State
            };
        }
    }
}
