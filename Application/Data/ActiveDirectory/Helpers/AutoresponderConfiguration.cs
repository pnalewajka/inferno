﻿using System;

namespace Smt.Atomic.Data.ActiveDirectory.Helpers
{
    public class AutoresponderConfiguration
    {
        public string Email { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string InternalMessage { get; set; }

        public string ExternalMessage { get; set; }

        public AutoresponderState State { get; set; }

        public bool IsActive(DateTime date)
        {
            return State == AutoresponderState.Enabled
                                || (State == AutoresponderState.Scheduled && StartDate >= date && EndDate <= date);
        }
    }
}
