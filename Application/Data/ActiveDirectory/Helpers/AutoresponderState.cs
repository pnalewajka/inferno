﻿namespace Smt.Atomic.Data.ActiveDirectory.Helpers
{
    public enum AutoresponderState
    {
        Disabled,
        Enabled,
        Scheduled
    }
}
