using System;
using System.DirectoryServices.Linq.EntryObjects;
using System.Reflection;

namespace Smt.Atomic.Data.ActiveDirectory.Extensions
{
    public static class EntryObjectExtensions
    {
        public static DateTime? GetDateAttribute(this EntryObject entryObject, string attributeName)
        {
            var value = entryObject.GetAttribute<object>(attributeName);

            if (value == null || value is DateTime)
            {
                return value as DateTime?;
            }

            try
            {
                // Try treating value as IADsLargeInteger
                // https://msdn.microsoft.com/en-us/library/aa706037(v=vs.85).aspx

                var type = value.GetType();
                var highPart = (int) type.InvokeMember("HighPart", BindingFlags.GetProperty, null, value, null);
                var lowPart = (int) type.InvokeMember("LowPart", BindingFlags.GetProperty, null, value, null);

                var timestamp = (long) highPart << 32 | (uint) lowPart;

                return timestamp != 0
                    ? DateTime.FromFileTime(timestamp)
                    : default(DateTime?);
            }
            catch (Exception)
            {
                // value is not of expected type
            }

            return default(DateTime?);
        }
    }
}
