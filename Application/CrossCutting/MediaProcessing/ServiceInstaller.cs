﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.MediaProcessing.Interfaces;
using Smt.Atomic.CrossCutting.MediaProcessing.Services;

namespace Smt.Atomic.CrossCutting.MediaProcessing
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<IImageProcessingService>().ImplementedBy<ImageProcessingService>().LifestyleTransient());
        }
    }
}