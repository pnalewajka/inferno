﻿using System.IO;
using Smt.Atomic.CrossCutting.MediaProcessing.Models;

namespace Smt.Atomic.CrossCutting.MediaProcessing.Interfaces
{
    public interface IImageProcessingService
    {
        /// <summary>
        /// Get size of image from stream
        /// </summary>
        /// <param name="imageData"></param>
        /// <returns></returns>
        ImageSize GetImageSize(Stream imageData);

        /// <summary>
        /// Crop image from stream
        /// </summary>
        /// <param name="imageData"></param>
        /// <param name="imageCropOptions"></param>
        /// <returns></returns>
        CroppedImage CropImage(Stream imageData, ImageCropOptions imageCropOptions);

        /// <summary>
        /// Resize image, preserving aspect ratio
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        byte[] ResizeImage(byte[] image, int width);

        /// <summary>
        /// Get image mime type (format)
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        string GetImageMimeType(byte[] image);
    }
}
