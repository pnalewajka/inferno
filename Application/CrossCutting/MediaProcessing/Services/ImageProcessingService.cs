﻿using System;
using System.Drawing;
using System.IO;
using ImageProcessor;
using Smt.Atomic.CrossCutting.MediaProcessing.Interfaces;
using Smt.Atomic.CrossCutting.MediaProcessing.Models;

namespace Smt.Atomic.CrossCutting.MediaProcessing.Services
{
    internal class ImageProcessingService : IImageProcessingService
    {
        public ImageSize GetImageSize(Stream imageData)
        {
            using (var imageFactory = new ImageFactory())
            {
                var imageSize = imageFactory.Load(imageData).Image.Size;

                return new ImageSize(imageSize.Width, imageSize.Height);
            }
        }

        public CroppedImage CropImage(Stream imageData, ImageCropOptions imageCropOptions)
        {
            using (var imageFactory = new ImageFactory())
            {
                imageFactory.Load(imageData);

                var cropStartingPoint = new Point(imageCropOptions.Left, imageCropOptions.Top);
                var cropSize = new Size(imageCropOptions.Width, imageCropOptions.Height);

                using (var newImageFactory = imageFactory.Crop(new Rectangle(cropStartingPoint, cropSize)))
                {
                    var imageDataStream = new MemoryStream();
                    newImageFactory.Save(imageDataStream);
                    var size = newImageFactory.Image.Size;

                    return new CroppedImage(imageDataStream, new ImageSize(size.Width, size.Height),
                        imageDataStream.Length);
                }
            }
        }

        public byte[] ResizeImage(byte[] image, int width)
        {
            if (width <= 0)
            {
                throw  new ArgumentOutOfRangeException(nameof(width));
            }

            using (var imageFactory = new ImageFactory())
            {
                imageFactory.Load(image);

                var scalingRatio = (1.0*width)/imageFactory.Image.Width;
                var newSize = new Size(width, (int) (imageFactory.Image.Height*scalingRatio));

                using (var newImageFactory = imageFactory.Resize(newSize))
                {
                    using (var imageDataStream = new MemoryStream())
                    {
                        newImageFactory.Save(imageDataStream);

                        return imageDataStream.GetBuffer();
                    }
                }
            }
        }

        public string GetImageMimeType(byte[] image)
        {
            using (var imageFactory = new ImageFactory())
            {
                imageFactory.Load(image);

                return imageFactory.CurrentImageFormat.MimeType;
            }
        }
    }
}
