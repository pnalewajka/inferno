﻿namespace Smt.Atomic.CrossCutting.MediaProcessing.Models
{
    /// <summary>
    /// Image crop options
    /// </summary>
    public class ImageCropOptions
    {
        /// <summary>
        /// Left portion of position of top-left point of cropping rectangle
        /// </summary>
        public int Left { get; private set; }

        /// <summary>
        /// Top portion of position of top-left point of cropping rectangle
        /// </summary>
        public int Top { get; private set; }

        /// <summary>
        /// Cropping rectangle width
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Cropping rectangle height
        /// </summary>
        public int Height { get; private set; }

        public ImageCropOptions(int left, int top, int width, int height)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }
    }
}

