﻿namespace Smt.Atomic.CrossCutting.MediaProcessing.Models
{
    /// <summary>
    /// Image size
    /// </summary>
    public class ImageSize
    {
        /// <summary>
        /// Image width in pixels
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Image height in pixels
        /// </summary>
        public int Height { get; private set; }

        public ImageSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
}
