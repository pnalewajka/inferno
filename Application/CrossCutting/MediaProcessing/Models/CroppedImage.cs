﻿using System;
using System.IO;

namespace Smt.Atomic.CrossCutting.MediaProcessing.Models
{
    /// <summary>
    /// Image content & metadata after cropping
    /// </summary>
    public class CroppedImage : IDisposable 
    {
        /// <summary>
        /// Image contents stream
        /// </summary>
        public Stream Content { get; private set; }

        /// <summary>
        /// New size
        /// </summary>
        public ImageSize Size { get; private set; }

        /// <summary>
        /// Stream length
        /// </summary>
        public long ContentLength { get; private set; }

        public CroppedImage(Stream content, ImageSize size, long contentLength)
        {
            Content = content;
            Size = size;
            ContentLength = contentLength;
        }

        public void Dispose()
        {
            if (Content != null)
            {
                Content.Dispose();
            }
        }
    }
}
