﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.CrossCutting.Business.Extensions
{
    public static class HourlyRateTypeExtensions
    {
        public static bool IsAllowedBy(this HourlyRateType hourlyRateType, OverTimeTypes overTimeTypes)
        {
            return overTimeTypes.HasFlag(hourlyRateType.ToOverTimeTypes());
        }

        public static OverTimeTypes ToOverTimeTypes(this HourlyRateType hourlyRateType)
        {
            return hourlyRateType == HourlyRateType.Regular
                ? 0
                : (OverTimeTypes) (1 << (int) hourlyRateType);
        }
    }
}
