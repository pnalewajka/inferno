﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.CrossCutting.Business.Extensions
{
    public static class NoticePeriodDateExtensions
    {
        public static DateTime AddNoticePeriod(this DateTime date, NoticePeriod period)
        {
            //pattern mmwwdd
            var periodValue = (int) period;
            var days = periodValue%100;
            var weeks = ((periodValue - days)/100)%100;
            var months = ((periodValue - days - weeks*100)/10000);

            return date.AddMonths(months).AddDays(days + weeks*7);
        }

        public static DateTime AddNoticePeriod(this DateTime date, NoticePeriod? period)
        {
            if (period.HasValue)
            {
                return date.AddNoticePeriod(period.Value);
            }

            return date;
        }

        public static DateTime SubstractNoticePeriod(this DateTime date, NoticePeriod period)
        {
            //pattern mmwwdd
            var periodValue = (int)period;
            var days = periodValue % 100;
            var weeks = ((periodValue - days) / 100) % 100;
            var months = ((periodValue - days - weeks * 100) / 10000);

            return date.AddMonths(-months).AddDays(-(days + weeks*7));
        }

        public static DateTime SubstractNoticePeriod(this DateTime date, NoticePeriod? period)
        {
            if (period.HasValue)
            {
                return date.SubstractNoticePeriod(period.Value);
            }

            return date;
        }
    }
}
