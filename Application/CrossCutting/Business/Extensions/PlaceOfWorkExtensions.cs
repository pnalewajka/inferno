﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Business.Extensions
{
    public static class PlaceOfWorkExtensions
    {
        private static readonly IDictionary<string, string> Cache = new ConcurrentDictionary<string, string>();

        public static string GetActiveDirectoryCode(this PlaceOfWork enumValue)
        {
            string result;

            var cacheKey = $"{enumValue.GetType().FullName},{enumValue}";

            if (!Cache.TryGetValue(cacheKey, out result))
            {
                var activeDirectoryCodeAttribute = AttributeHelper.GetEnumFieldAttribute<ActiveDirectoryCodeAttribute>(enumValue);

                result = activeDirectoryCodeAttribute?.Code;
                Cache[cacheKey] = result;
            }

            return result;
        }
    }
}