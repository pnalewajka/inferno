﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.CrossCutting.Business.Extensions
{
    public static class InheritableBoolExtensions
    {
        public static bool ToBool(this InheritableBool inheritableBool)
        {
            switch (inheritableBool)
            {
                case InheritableBool.No:
                    return false;

                case InheritableBool.Yes:
                    return true;

                case InheritableBool.Inherited:
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}