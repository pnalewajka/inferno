﻿using Smt.Atomic.CrossCutting.Business.Interfaces;

namespace Smt.Atomic.CrossCutting.Business.Extensions
{
    public static class AcceptanceConditionsExtensions
    {
        public static T CopyFields<T>(this T destination, IAcceptanceConditions source) where T : IAcceptanceConditions
        {
            destination.ReinvoicingToCustomer = source.ReinvoicingToCustomer;
            destination.ReinvoiceCurrencyId = source.ReinvoiceCurrencyId;
            destination.CostApprovalPolicy = source.CostApprovalPolicy;
            destination.MaxCostsCurrencyId = source.MaxCostsCurrencyId;
            destination.MaxHotelCosts = source.MaxHotelCosts;
            destination.MaxTotalFlightsCosts = source.MaxTotalFlightsCosts;
            destination.MaxOtherTravelCosts = source.MaxOtherTravelCosts;

            return destination;
        }
    }
}
