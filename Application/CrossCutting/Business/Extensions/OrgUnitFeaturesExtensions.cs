﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.CrossCutting.Business.Extensions
{
    public static class OrgUnitFeaturesExtensions
    {
        public static OrgUnitFeatures Add(this OrgUnitFeatures currentVallue, OrgUnitFeatures toAdd)
        {
            return currentVallue | toAdd;
        }

        public static OrgUnitFeatures Remove(this OrgUnitFeatures currentVallue, OrgUnitFeatures toRemove)
        {
            return currentVallue &= ~toRemove;
        }

        public static bool Has(this OrgUnitFeatures currentVallue, OrgUnitFeatures toCheck)
        {
            return (currentVallue & toCheck) == toCheck;
        }
    }
}
