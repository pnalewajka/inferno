﻿using System;

namespace Smt.Atomic.CrossCutting.Business.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class KpiEntityAttribute : Attribute
    {
        public string[] Columns { get; }

        public string Table { get; }

        public string Alias { get; }

        public KpiEntityAttribute(string table, string alias, params string[] columns)
        {
            Columns = columns;
            Table = table;
            Alias = alias;
        }
    }
}
