﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.CrossCutting.Business.Attributes
{
    public class CompensationCalculatorRequirementAttribute : Attribute
    {
        public IEnumerable<TimeTrackingCompensationCalculators> TimeTrackingCalculatorTypes { get; }

        public IEnumerable<BusinessTripCompensationCalculators> BusinessTripCalculatorTypes { get; }

        public CompensationCalculatorRequirementAttribute(params object[] calculatorTypes)
        {
            TimeTrackingCalculatorTypes = calculatorTypes.OfType<TimeTrackingCompensationCalculators>();
            BusinessTripCalculatorTypes = calculatorTypes.OfType<BusinessTripCompensationCalculators>();
        }
    }
}
