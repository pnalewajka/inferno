﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.CrossCutting.Business.Interfaces
{
    public interface IAcceptanceConditions
    {
        bool ReinvoicingToCustomer { get; set; }

        long? ReinvoiceCurrencyId { get; set; }

        CostApprovalPolicy CostApprovalPolicy { get; set; }

        long? MaxCostsCurrencyId { get; set; }

        decimal? MaxHotelCosts { get; set; }

        decimal? MaxTotalFlightsCosts { get; set; }

        decimal? MaxOtherTravelCosts { get; set; }
    }
}
