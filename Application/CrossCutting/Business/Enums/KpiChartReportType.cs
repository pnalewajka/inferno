﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum KpiChartReportType
    {
        [DescriptionLocalized(nameof(AggregatedKpisForManyEntitiesOverPeriods), typeof(EnumResources))]
        AggregatedKpisForManyEntitiesOverPeriods,

        [DescriptionLocalized(nameof(OneKpiForManyEntitiesOverPeriods), typeof(EnumResources))]
        OneKpiForManyEntitiesOverPeriods,

        [DescriptionLocalized(nameof(AggregatedKpisForManyPeriodsOverEntities), typeof(EnumResources))]
        AggregatedKpisForManyPeriodsOverEntities,
    }
}
