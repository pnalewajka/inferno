﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.MeetMe)]
    public enum NoteStatus
    {
        [DescriptionLocalized("Draft", typeof(EnumResources))]
        Draft = 0,

        [DescriptionLocalized("Done", typeof(EnumResources))]
        Done = 1,
    }
}