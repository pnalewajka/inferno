﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum AgencyEmploymentMode
    {
        [DescriptionLocalized(nameof(EnumResources.Outsourcing), typeof(EnumResources))]
        Outsourcing = 0,

        [DescriptionLocalized(nameof(EnumResources.PermanentRecruitment), typeof(EnumResources))]
        PermanentRecruitment = 1,

        [DescriptionLocalized(nameof(EnumResources.Both), typeof(EnumResources))]
        Both = 2
    }
}
