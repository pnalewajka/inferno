﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum HiringMode
    {
        [DescriptionLocalized(nameof(EnumResources.Intive), typeof(EnumResources))]
        Intive = 0,

        [DescriptionLocalized(nameof(EnumResources.Project), typeof(EnumResources))]
        Project = 1,
    }
}
