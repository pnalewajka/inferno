﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ReportScopeEnum
    {
        [DescriptionLocalized("AllEmployeesText", typeof(EnumResources))]
        AllEmployees,
        [DescriptionLocalized("MyEmployeesText", typeof(EnumResources))]
        MyEmployees
    }
}
