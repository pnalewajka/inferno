﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Identifier("Allocations.Enums.EmploymentType")]
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum EmploymentType
    {
        [DescriptionLocalized("Employee", typeof(EnumResources))]
        Employee = 1,

        [DescriptionLocalized("Contractor", typeof(EnumResources))]
        Contractor = 2,

        [DescriptionLocalized("External", typeof(EnumResources))]
        External = 3
    }
}
