﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum ProjectStatus
    {
        [DescriptionLocalized("Planned", typeof(EnumResources))]
        Planned = 0,

        [DescriptionLocalized("Fixed", typeof(EnumResources))]
        Fixed = 1,
    }
}