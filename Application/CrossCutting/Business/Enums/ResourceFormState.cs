﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ResourceFormState
    {
        [DescriptionLocalized("Draft", typeof(EnumResources))]
        Draft = 0,
        [DescriptionLocalized("Submitted", typeof(EnumResources))]
        Submitted = 1,
        [DescriptionLocalized("Approved", typeof(EnumResources))]
        Approved = 2

    }
}