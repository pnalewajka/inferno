﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum RecruitmentProcessStatus
    {
        [DescriptionLocalized(nameof(EnumResources.Draft), typeof(EnumResources))]
        Draft = 0,

        [DescriptionLocalized(nameof(EnumResources.Active), typeof(EnumResources))]
        Active = 1,

        [DescriptionLocalized(nameof(EnumResources.OnHold), typeof(EnumResources))]
        OnHold = 2,

        [DescriptionLocalized(nameof(EnumResources.Closed), typeof(EnumResources))]
        Closed = 3
    }
}