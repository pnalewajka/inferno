﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    ///<summary>
    ///Employee Notice Period Value
    ///</summary>
    ///<remarks>
    ///Employee NoticePeriod allow us to use 'smart-code'.
    ///Ten-thousands are for months, hundreds for weeks (assuming we might have silly 10 week notices), ones are for days
    ///</remarks>
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum NoticePeriod
    {
        [DescriptionLocalized("NoticePeriodTwoWeeks", typeof(EnumResources))]
        TwoWeeks = 200,

        [DescriptionLocalized("NoticePeriodSixWeeks", typeof(EnumResources))]
        SixWeeks = 600,

        [DescriptionLocalized("NoticePeriodOneMonth", typeof(EnumResources))]
        OneMonth = 10000,

        [DescriptionLocalized("NoticePeriodTwoMonths", typeof(EnumResources))]
        TwoMonths = 20000,

        [DescriptionLocalized("NoticePeriodThreeMonths", typeof(EnumResources))]
        ThreeMonths = 30000,
    }
}
