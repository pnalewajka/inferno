﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum NoteVisibility
    {
        [DescriptionLocalized(nameof(EnumResources.Public), typeof(EnumResources))]
        Public = 1,

        [DescriptionLocalized(nameof(EnumResources.RecruitmentTeamOnly), typeof(EnumResources))]
        RecruitmentTeamOnly = 2,

        [DescriptionLocalized(nameof(EnumResources.HiringManagerOnly), typeof(EnumResources))]
        HiringManagerOnly = 3,

        [DescriptionLocalized(nameof(EnumResources.TechnicalReviewerOnly), typeof(EnumResources))]
        TechnicalReviewerOnly = 4
    }
}
