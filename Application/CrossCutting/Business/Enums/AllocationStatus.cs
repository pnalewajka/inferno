﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum AllocationStatus
    {
        [DescriptionLocalized("AllocationStatusAllocated", typeof(EnumResources))]
        Allocated  = 1,

        [DescriptionLocalized("AllocationStatusPlanned", typeof(EnumResources))]
        Planned = 2,

        [DescriptionLocalized("AllocationStatusFree", typeof(EnumResources))]
        Free = 3,

        [DescriptionLocalized("AllocationStatusUnderAllocated", typeof(EnumResources))]
        UnderAllocated = 4,

        [DescriptionLocalized("AllocationStatusUnderPlanned", typeof(EnumResources))]
        UnderPlanned = 5,

        [DescriptionLocalized("AllocationStatusAlmostFree", typeof(EnumResources))]
        AlmostFree = 6,

        [DescriptionLocalized("AllocationStatusMixed", typeof(EnumResources))]
        Mixed = 7,

        [DescriptionLocalized("AllocationStatusUnavailable", typeof(EnumResources))]
        Unavailable = 8,

        [DescriptionLocalized("AllocationStatusInActive", typeof(EnumResources))]
        InActive = 9,

        [DescriptionLocalized("AllocationStatusProcessing", typeof(EnumResources))]
        Processing = 10,
    }
}
