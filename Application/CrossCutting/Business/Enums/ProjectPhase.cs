﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum ProjectPhase
    {
        [DescriptionLocalized("Design", typeof(EnumResources))]
        Design = 1,

        [DescriptionLocalized("Consulting", typeof(EnumResources))]
        Consulting = 2,

        [DescriptionLocalized("Development", typeof(EnumResources))]
        Development = 3,

        [DescriptionLocalized("Maintenance", typeof(EnumResources))]
        Maintenance = 4,

        [DescriptionLocalized("Outsourcing", typeof(EnumResources))]
        Outsourcing = 5,

        [DescriptionLocalized("Finished", typeof(EnumResources))]
        Finished = 6,
    }
}