﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum PersonalNumberType
    {
        [DescriptionLocalized("PersonalNumberPesel", typeof(EnumResources))]
        Pesel = 0,

        [DescriptionLocalized("PersonalSteuerIdNr", typeof(EnumResources))]
        SteuerIdNr = 1,

        [DescriptionLocalized("PersonalNumberNationalInsuranceNumber", typeof(EnumResources))]
        NationalInsuranceNumber = 2,

        [DescriptionLocalized("PersonalNumberSocialSecurityNumber", typeof(EnumResources))]
        SocialSecurityNumber = 3,

        [DescriptionLocalized("PersonalNumberDocumentoNacionalDeIdentidad", typeof(EnumResources))]
        DocumentoNacionalDeIdentidad = 4
    }
}
