﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum EmployeeCommentType
    {
        [DescriptionLocalized("EmployeeCommentTypeStaffingComment", typeof(EnumResources))]
        StaffingComment = 1,

        [DescriptionLocalized("EmployeeCommentTypePositiveFeedback", typeof(EnumResources))]
        PositiveFeedback = 2,

        [DescriptionLocalized("EmployeeCommentTypeNegativeFeedback", typeof(EnumResources))]
        NegativeFeedback = 3,
    }
}
