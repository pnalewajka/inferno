﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum InboundEmailValueType
    {
        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.MessageType), typeof(InboundEmailValueTypeResources))]
        MessageType = 1,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.ApplicationOriginName), typeof(InboundEmailValueTypeResources))]
        ApplicationOriginName = 2,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.ApplicationOriginId), typeof(InboundEmailValueTypeResources))]
        ApplicationOriginId = 3,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidateFirstName), typeof(InboundEmailValueTypeResources))]
        CandidateFirstName = 4,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidateLastName), typeof(InboundEmailValueTypeResources))]
        CandidateLastName = 5,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidateEmailAddress), typeof(InboundEmailValueTypeResources))]
        CandidateEmailAddress = 6,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidatePhoneNumber), typeof(InboundEmailValueTypeResources))]
        CandidatePhoneNumber = 7,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CityId), typeof(InboundEmailValueTypeResources))]
        CityId = 8,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.JobProfileId), typeof(InboundEmailValueTypeResources))]
        JobProfileId = 9,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.ReferenceNumber), typeof(InboundEmailValueTypeResources))]
        ReferenceNumber = 10,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidateConsentType), typeof(InboundEmailValueTypeResources))]
        CandidateConsentType = 11,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.MatchingCandidateId), typeof(InboundEmailValueTypeResources))]
        MatchingCandidateId = 12,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.RecommendingPersonFirstName), typeof(InboundEmailValueTypeResources))]
        RecommendingPersonFirstName = 13,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.RecommendingPersonLastName), typeof(InboundEmailValueTypeResources))]
        RecommendingPersonLastName = 14,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.RecommendingPersonEmailAddress), typeof(InboundEmailValueTypeResources))]
        RecommendingPersonEmailAddress = 15,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.RecommendingPersonPhoneNumber), typeof(InboundEmailValueTypeResources))]
        RecommendingPersonPhoneNumber = 16,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.RecommendingPersonConsentType), typeof(InboundEmailValueTypeResources))]
        RecommendingPersonConsentType = 17,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.MatchingRecommendingPersonId), typeof(InboundEmailValueTypeResources))]
        MatchingRecommendingPersonId = 18,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.ParserMessage), typeof(InboundEmailValueTypeResources))]
        ParserMessage = 19,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.IsEligibleForReferralProgram), typeof(InboundEmailValueTypeResources))]
        IsEligibleForReferralProgram = 20,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.JobApplicationId), typeof(InboundEmailValueTypeResources))]
        JobApplicationId = 21,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidateAreaOfInterest), typeof(InboundEmailValueTypeResources))]
        CandidateAreaOfInterest = 22,

        [DescriptionLocalized(nameof(InboundEmailValueTypeResources.CandidatePreferedLocation), typeof(InboundEmailValueTypeResources))]
        CandidatePreferedLocation = 23
    }
}
