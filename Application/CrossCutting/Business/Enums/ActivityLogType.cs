﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ActivityLogType
    {
        NewRecommendation = 1,
        RequestApproved = 2,
        RequestQuestion = 3,
        AppointmentScheduled = 4,
        ChallengeAnswered = 5
    }
}