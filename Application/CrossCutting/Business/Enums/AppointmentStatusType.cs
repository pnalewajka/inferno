﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.CustomerPortal)]
    public enum AppointmentStatusType
    {
        Pending = 1,
        Accepted = 2,
        NotUsed = 3
    }
}