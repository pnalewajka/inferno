﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Sales)]
    public enum ImportanceType
    {
        [DescriptionLocalized(nameof(EnumResources.Low), typeof(EnumResources))]
        Low = 1,

        [DescriptionLocalized(nameof(EnumResources.High), typeof(EnumResources))]
        High = 100,
    }
}
