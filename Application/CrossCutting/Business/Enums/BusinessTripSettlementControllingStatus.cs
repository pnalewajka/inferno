﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.BusinessTrips, "SettlementControllingStatus")]
    public enum BusinessTripSettlementControllingStatus
    {
        [DescriptionLocalized(nameof(EnumResources.NotSent), typeof(EnumResources))]
        NotSent = 0,

        [DescriptionLocalized(nameof(EnumResources.InAccounting), typeof(EnumResources))]
        InAccounting = 1,

        [DescriptionLocalized(nameof(EnumResources.ClaimBack), typeof(EnumResources))]
        ClaimBack = 2,

        [DescriptionLocalized(nameof(EnumResources.Paid), typeof(EnumResources))]
        Paid = 3,
    }
}
