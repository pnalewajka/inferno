﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.BusinessTrips)]
    public enum SettlementSectionsFeatureType
    {
        [DescriptionLocalized(nameof(EnumResources.DepartureArrival), typeof(EnumResources))]
        DepartureArrival = 1 << 0,

        [DescriptionLocalized(nameof(EnumResources.OwnCosts), typeof(EnumResources))]
        OwnCosts = 1 << 1,

        [DescriptionLocalized(nameof(EnumResources.CompanyCosts), typeof(EnumResources))]
        CompanyCosts = 1 << 2,

        [DescriptionLocalized(nameof(EnumResources.AdditionalAdvancePayments), typeof(EnumResources))]
        AdditionalAdvancePayments = 1 << 3,

        [DescriptionLocalized(nameof(EnumResources.PrivateVehicleMileage), typeof(EnumResources))]
        PrivateVehicleMileage = 1 << 4,

        [DescriptionLocalized(nameof(EnumResources.HoursWorkedForCustomer), typeof(EnumResources))]
        HoursWorkedForCustomer = 1 << 5,

        [DescriptionLocalized(nameof(EnumResources.Meals), typeof(EnumResources))]
        Meals = 1 << 6,

        [DescriptionLocalized(nameof(EnumResources.SimplifiedPrivateVehicleMileage), typeof(EnumResources))]
        SimplifiedPrivateVehicleMileage = 1 << 7,
    }
}
