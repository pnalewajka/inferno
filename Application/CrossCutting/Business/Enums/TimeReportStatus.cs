﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum TimeReportStatus
    {
        [DescriptionLocalized("TimeReportStatusNotStarted", typeof(EnumResources))]
        NotStarted = -1,

        [DescriptionLocalized("TimeReportStatusDraft", typeof(EnumResources))]
        Draft = 0,

        [DescriptionLocalized("TimeReportStatusSubmitted", typeof(EnumResources))]
        Submitted = 1,

        [DescriptionLocalized("TimeReportStatusDraftAcepted", typeof(EnumResources))]
        Accepted = 2,

        [DescriptionLocalized("TimeReportStatusDraftRejected", typeof(EnumResources))]
        Rejected = 3
    }
}