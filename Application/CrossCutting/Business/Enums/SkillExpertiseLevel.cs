﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Resumes)]
    public enum SkillExpertiseLevel
    {
        [DescriptionLocalized("None", typeof(EnumResources))]
        None = 0,

        [DescriptionLocalized("Basic", typeof(EnumResources))]
        Basic = 1,

        [DescriptionLocalized("Medium", typeof(EnumResources))]
        Medium = 2,

        [DescriptionLocalized("VeryGood", typeof(EnumResources))]
        VeryGood = 3,

        [DescriptionLocalized("Expert", typeof(EnumResources))]
        Expert = 4,
    }
}
