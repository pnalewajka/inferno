﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum InboundEmailClosedReason
    {
        [DescriptionLocalized(nameof(EnumResources.NotRelevant), typeof(EnumResources))]
        NotRelevant = 1,

        [DescriptionLocalized(nameof(EnumResources.NewApplication), typeof(EnumResources))]
        NewApplication = 2,
    }
}
