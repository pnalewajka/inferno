﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Compensation)]
    public enum CurrencyExchangeRateSourceType
    {
        Unknown = 0,

        NationalBankOfPoland = 1,

        EuropeanCentralBank = 2,

        OpenExchangeRates = 3,
    }
}
