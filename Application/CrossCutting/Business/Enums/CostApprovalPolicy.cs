﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum CostApprovalPolicy
    {
        [DescriptionLocalized(nameof(EnumResources.ManualApprovalAll), typeof(EnumResources))]
        ManualApprovalAll = 0,

        [DescriptionLocalized(nameof(EnumResources.ManualApprovalIfExceedsCompanyPolicy), typeof(EnumResources))]
        ManualApprovalIfExceedsCompanyPolicy = 1,

        [DescriptionLocalized(nameof(EnumResources.OwnThresholds), typeof(EnumResources))]
        OwnThresholds = 2,
    }
}
