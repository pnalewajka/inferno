﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum InboundEmailCategory
    {
        [DescriptionLocalized(nameof(EnumResources.Unknown), typeof(EnumResources))]
        Unknown = 0,

        [DescriptionLocalized(nameof(EnumResources.Spam), typeof(EnumResources))]
        Spam = 1,

        [DescriptionLocalized(nameof(EnumResources.JobApplication), typeof(EnumResources))]
        JobApplication = 2,

        [DescriptionLocalized(nameof(EnumResources.Recommendation), typeof(EnumResources))]
        Recommendation = 3,

        [DescriptionLocalized(nameof(EnumResources.DataOwnerInquiry), typeof(EnumResources))]
        DataOwnerInquiry = 4
    }
}
