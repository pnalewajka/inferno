﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum JobOpeningClosedReason
    {
        [DescriptionLocalized(nameof(EnumResources.Fullfied), typeof(EnumResources))]
        Fullfied = 1,

        [DescriptionLocalized(nameof(EnumResources.Lost), typeof(EnumResources))]
        Lost = 2,

        [DescriptionLocalized(nameof(EnumResources.PositionCanceled), typeof(EnumResources))]
        PositionCanceled = 3
    }
}
