﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum CandidateReaction
    {
        [DescriptionLocalized(nameof(EnumResources.Interested), typeof(EnumResources))]
        Interested = 1,

        [DescriptionLocalized(nameof(EnumResources.ContactLater), typeof(EnumResources))]
        ContactLater = 2,

        [DescriptionLocalized(nameof(EnumResources.NotInterested), typeof(EnumResources))]
        NotInterested = 3,

        [DescriptionLocalized(nameof(EnumResources.NotApplicable), typeof(EnumResources))]
        NotApplicable = 4
    }
}
