﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum EmployeeInactivityReason
    {
        [DescriptionLocalized("EmploymentPeriodStatusNonApplicable", typeof(EnumResources))]
        NonApplicable = 0,

        [DescriptionLocalized("EmploymentPeriodStatusSickness", typeof(EnumResources))]
        Sickness = 1,

        [DescriptionLocalized("EmploymentPeriodStatusMaternityLeave", typeof(EnumResources))]
        MaternityLeave = 2,

        [DescriptionLocalized("EmploymentPeriodStatusVacation", typeof(EnumResources))]
        Vacation = 3,
    }
}
