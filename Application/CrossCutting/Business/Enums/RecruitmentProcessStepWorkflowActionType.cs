﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum RecruitmentProcessStepWorkflowActionType
    {
        ApproveCandidate = 1,
        RejectCandidate = 2,
        PutProcessOnHold = 3,

        CancelProcessStep = 4,
        AddHrInterview = 5,
        AddTechnicalReview = 6,
        AddClientInterview = 7,
        AddHiringManagerInterview = 8,
        AddHiringManagerDecision = 9,
        SendResumeToHiringManager = 10,
        SendResumeToClient = 11,
        StartContractNegotations = 12,

        RefreshAndRunScript = 128
    }
}
