﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ResourceRequestType
    {
        [DescriptionLocalized(nameof(EnumResources.ResourceRequestFile), typeof(EnumResources))]
        FileRequest = 0,

        [DescriptionLocalized(nameof(EnumResources.ResourceRequestAdvanced), typeof(EnumResources))]
        AdvancedRequest = 1,
    }
}
