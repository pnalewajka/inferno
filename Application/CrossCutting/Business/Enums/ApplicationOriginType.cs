﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ApplicationOriginType
    {
        [DescriptionLocalized(nameof(EnumResources.Agency), typeof(EnumResources))]
        Agency = 1001,

        [DescriptionLocalized(nameof(EnumResources.SocialNetwork), typeof(EnumResources))]
        SocialNetwork = 1002,

        [DescriptionLocalized(nameof(EnumResources.JobAdvertisement), typeof(EnumResources))]
        JobAdvertisement = 1003,

        [DescriptionLocalized(nameof(EnumResources.Recommendation), typeof(EnumResources))]
        Recommendation = 4,

        [DescriptionLocalized(nameof(EnumResources.Event), typeof(EnumResources))]
        Event = 5,

        [DescriptionLocalized(nameof(EnumResources.DropOff), typeof(EnumResources))]
        DropOff = 6,

        [DescriptionLocalized(nameof(EnumResources.Other), typeof(EnumResources))]
        Other = 7,

        [DescriptionLocalized(nameof(EnumResources.Researching), typeof(EnumResources))]
        Researching = 8,

        [DescriptionLocalized(nameof(EnumResources.DirectSearch), typeof(EnumResources))]
        DirectSearch = 9,
    }
}
