﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.BusinessTrips, "SettlementPaymentMethod")]
    public enum BusinessTripSettlementPaymentMethod
    {
        [DescriptionLocalized(nameof(EnumResources.CreditCard), typeof(EnumResources))]
        CreditCard = 0,

        [DescriptionLocalized(nameof(EnumResources.MoneyTransfer), typeof(EnumResources))]
        MoneyTransfer = 1,

        [DescriptionLocalized(nameof(EnumResources.Voucher), typeof(EnumResources))]
        Voucher = 2,
    }
}
