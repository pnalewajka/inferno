﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    public enum AllocationChangeDemand
    {
        None = 0,

        [DescriptionLocalized("AllocationChangeDemandEmployee", typeof(EnumResources))]
        EmployeeDemand = 1 << 0,

        [DescriptionLocalized("AllocationChangeDemandCompany", typeof(EnumResources))]
        CompanyDemand = 1 << 1,

        [DescriptionLocalized("AllocationChangeDemandClient", typeof(EnumResources))]
        ClientDemand = 1 << 2,
    }
}