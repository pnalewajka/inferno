﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Workflows)]
    public enum RequestHistoryEntryType
    {
        [DescriptionLocalized("RequestHistoryEntryCreated", typeof(EnumResources))]
        Created = 0,

        [DescriptionLocalized("RequestHistoryEntryEdited", typeof(EnumResources))]
        Edited = 1,

        [DescriptionLocalized("RequestHistoryEntryApproved", typeof(EnumResources))]
        Approved = 2,

        [DescriptionLocalized("RequestHistoryEntryRejected", typeof(EnumResources))]
        Rejected = 3,

        [DescriptionLocalized("RequestHistoryEntryExecuted", typeof(EnumResources))]
        Executed = 4,

        [DescriptionLocalized("RequestHistoryEntryMailSent", typeof(EnumResources))]
        MailSent = 5,

        [DescriptionLocalized("RequestHistoryEntryJiraIssueCreated", typeof(EnumResources))]
        JiraIssueCreated = 6,

        [DescriptionLocalized("RequestHistoryEntryErrorOccurred", typeof(EnumResources))]
        ErrorOccurred = 7,

        [DescriptionLocalized("RequestHistoryEntryWatchersAdded", typeof(EnumResources))]
        WatchersAdded = 8
    }
}
