﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum OrgUnitType
    {
        [DescriptionLocalized("BusinessUnit", typeof(EnumResources))]
        BusinessUnit = 1,

        [DescriptionLocalized("Operation", typeof(EnumResources))]
        Operation = 2,

        [DescriptionLocalized("SubOperation", typeof(EnumResources))]
        SubOperation = 3
    }
}
