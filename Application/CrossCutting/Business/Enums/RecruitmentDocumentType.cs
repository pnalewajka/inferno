﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum RecruitmentDocumentType
    {
        [DescriptionLocalized(nameof(EnumResources.OriginalResume), typeof(EnumResources))]
        OriginalResume = 1,

        [DescriptionLocalized(nameof(EnumResources.CompanyResume), typeof(EnumResources))]
        CompanyResume = 2,

        [DescriptionLocalized(nameof(EnumResources.CoverLetter), typeof(EnumResources))]
        CoverLetter = 3,

        [DescriptionLocalized(nameof(EnumResources.Certificate), typeof(EnumResources))]
        Certificate = 4,

        [DescriptionLocalized(nameof(EnumResources.JobReference), typeof(EnumResources))]
        JobReference = 5,

        [DescriptionLocalized(nameof(EnumResources.Portfolio), typeof(EnumResources))]
        Portfolio = 6,

        [DescriptionLocalized(nameof(EnumResources.Consent), typeof(EnumResources))]
        Consent = 7,

        [DescriptionLocalized(nameof(EnumResources.TechnicalReview), typeof(EnumResources))]
        TechnicalReview = 8,

        [DescriptionLocalized(nameof(EnumResources.Other), typeof(EnumResources))]
        Other = 99
    }
}
