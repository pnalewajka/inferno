﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{   public enum AllocationRequestContentType
    {
        [DescriptionLocalized("Simple", typeof(EnumResources))]
        Simple = 1,

        [DescriptionLocalized("Advanced", typeof(EnumResources))]
        Advanced = 2
    }
}
