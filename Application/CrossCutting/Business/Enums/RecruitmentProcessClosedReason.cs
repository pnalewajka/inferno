﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum RecruitmentProcessClosedReason
    {
        [DescriptionLocalized(nameof(EnumResources.CandidateResigned), typeof(EnumResources))]
        CandidateResigned = 0,

        [DescriptionLocalized(nameof(EnumResources.ClientResigned), typeof(EnumResources))]
        ClientResigned = 1,

        [DescriptionLocalized(nameof(EnumResources.Hired), typeof(EnumResources))]
        Hired = 3,

        [DescriptionLocalized(nameof(EnumResources.RejectedByHiringManager), typeof(EnumResources))]
        RejectedByHiringManager = 4,

        [DescriptionLocalized(nameof(EnumResources.RejectedByClient), typeof(EnumResources))]
        RejectedByClient = 5,

        [DescriptionLocalized(nameof(EnumResources.RejectedByRecruiter), typeof(EnumResources))]
        RejectedByRecruiter = 6,

        [DescriptionLocalized(nameof(EnumResources.RejectedByCandidate), typeof(EnumResources))]
        RejectedByCandidate = 7,

        [DescriptionLocalized(nameof(EnumResources.FailedTechnicalReview), typeof(EnumResources))]
        FailedTechnicalReview = 8
    }
}
