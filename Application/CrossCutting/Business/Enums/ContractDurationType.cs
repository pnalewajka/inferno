﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum ContractDurationType
    {

        [DescriptionLocalized(nameof(EnumResources.ContractDurationPermanentTrial), typeof(EnumResources))]
        PermanentTrial = 1,

        [DescriptionLocalized(nameof(EnumResources.ContractDurationFixedTerm), typeof(EnumResources))]
        FixedTerm = 2,

        [DescriptionLocalized(nameof(EnumResources.ContractDurationPermanent), typeof(EnumResources))]
        Permanent = 3,

        [DescriptionLocalized(nameof(EnumResources.ContractDurationReplacement), typeof(EnumResources))]
        Replacement = 4
    }
}