﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum TimeReportCycle
    {
        [DescriptionLocalized("TimeReportCycleMonthly", typeof(EnumResources))]
        Monthly = 0,

        [DescriptionLocalized("TimeReportCycleWeekly", typeof(EnumResources))]
        Weekly = 1
    }
}
