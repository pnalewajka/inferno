﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum InvoiceIssues
    {
        [DescriptionLocalized(nameof(EnumResources.None), typeof(EnumResources))]
        None = 0,

        [DescriptionLocalized(nameof(EnumResources.ContractorCompanyNameNotFound), typeof(EnumResources))]
        ContractorCompanyNameNotFound = 1 << 0,

        [DescriptionLocalized(nameof(EnumResources.ContractorCompanyStreetAddressNotFound), typeof(EnumResources))]
        ContractorCompanyStreetAddressNotFound = 1 << 1,

        [DescriptionLocalized(nameof(EnumResources.ContractorCompanyZipCodeNotFound), typeof(EnumResources))]
        ContractorCompanyZipCodeNotFound = 1 << 2,

        [DescriptionLocalized(nameof(EnumResources.ContractorCompanyCityNotFound), typeof(EnumResources))]
        ContractorCompanyCityNotFound = 1 << 3,

        [DescriptionLocalized(nameof(EnumResources.ContractorCompanyTaxIdentificationNumberNotFound), typeof(EnumResources))]
        ContractorCompanyTaxIdentificationNumberNotFound = 1 << 4,

        [DescriptionLocalized(nameof(EnumResources.TotalAmountNotFound), typeof(EnumResources))]
        TotalAmountNotFound = 1 << 5,
    }
}
