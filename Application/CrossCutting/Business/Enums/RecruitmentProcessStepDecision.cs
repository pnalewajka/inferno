﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum RecruitmentProcessStepDecision
    {
        [DescriptionLocalized(nameof(EnumResources.None), typeof(EnumResources))]
        None = 0,

        [DescriptionLocalized(nameof(EnumResources.Positive), typeof(EnumResources))]
        Positive = 1,

        [DescriptionLocalized(nameof(EnumResources.Negative), typeof(EnumResources))]
        Negative = 2
    }
}