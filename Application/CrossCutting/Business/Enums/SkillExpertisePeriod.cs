﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Resumes)]
    public enum SkillExpertisePeriod
    {
        [DescriptionLocalized("None", typeof(EnumResources))]
        None = 0,

        [DescriptionLocalized("LessThanOne", typeof(EnumResources))]
        LessThanOne = 1,

        [DescriptionLocalized("OneTwo", typeof(EnumResources))]
        OneTwo = 2,

        [DescriptionLocalized("TwoFive", typeof(EnumResources))]
        TwoFive = 3,

        [DescriptionLocalized("FiveTen", typeof(EnumResources))]
        FiveTen = 4,

        [DescriptionLocalized("MoreThanTen", typeof(EnumResources))]
        MoreThanTen = 5,
    }
}
