﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment, "SeniorityLevel")]
    public enum SeniorityLevelEnum
    {
        [DescriptionLocalized("Seniority1", typeof(EnumResources))]
        Seniority1,
        [DescriptionLocalized("Seniority2", typeof(EnumResources))]
        Seniority2,
        [DescriptionLocalized("Seniority3", typeof(EnumResources))]
        Seniority3,
        [DescriptionLocalized("Seniority4", typeof(EnumResources))]
        Seniority4,
        [DescriptionLocalized("Seniority5", typeof(EnumResources))]
        Seniority5
    }
}