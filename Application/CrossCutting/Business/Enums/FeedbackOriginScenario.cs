﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.MeetMe)]
    public enum FeedbackOriginScenario
    {
        OwnInitiative = 1 << 0,

        Requested = 1 << 1
    }
}
