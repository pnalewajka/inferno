﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum InboundEmailWorkflowAction
    {
        Assign = 1,
        Close = 2,
        Refresh = 3,
        SendForResearch = 4,
        ChangeCategory = 5,
        Unassign = 6,
    }
}
