﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum BusinessTripStatus
    {
        [DescriptionLocalized(nameof(EnumResources.ArrangementInProgress), typeof(EnumResources))]
        ArrangementInProgress = 0,

        [DescriptionLocalized(nameof(EnumResources.Arranged), typeof(EnumResources))]
        Arranged = 1,

        [DescriptionLocalized(nameof(EnumResources.Ongoing), typeof(EnumResources))]
        Ongoing = 2,

        [DescriptionLocalized(nameof(EnumResources.Finished), typeof(EnumResources))]
        Finished = 3,

        [DescriptionLocalized(nameof(EnumResources.Accounted), typeof(EnumResources))]
        Accounted = 4,
    }
}
