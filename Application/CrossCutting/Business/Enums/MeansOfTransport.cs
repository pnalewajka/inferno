﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.BusinessTrips)]
    public enum MeansOfTransport
    {
        [DescriptionLocalized(nameof(EnumResources.TransportTrain), typeof(EnumResources))]
        Train = 1 << 0,

        [DescriptionLocalized(nameof(EnumResources.TransportPrivateCar), typeof(EnumResources))]
        PrivateCar = 1 << 1,

        [DescriptionLocalized(nameof(EnumResources.TransportCompanyCar), typeof(EnumResources))]
        CompanyCar = 1 << 2,

        [DescriptionLocalized(nameof(EnumResources.TransportPlane), typeof(EnumResources))]
        Plane = 1 << 3,

        [DescriptionLocalized(nameof(EnumResources.TransportTaxi), typeof(EnumResources))]
        Taxi = 1 << 4,

        [DescriptionLocalized(nameof(EnumResources.TransportOther), typeof(EnumResources))]
        Other = 1 << 5,

        [DescriptionLocalized(nameof(EnumResources.TransportShip), typeof(EnumResources))]
        Ship = 1 << 6,

        [DescriptionLocalized(nameof(EnumResources.AirportTransport), typeof(EnumResources))]
        AirportTransport = 1 << 7,
    }
}
