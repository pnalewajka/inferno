﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum HourlyRateType
    {
        [DescriptionLocalized("HourlyRateTypeRegular", typeof(EnumResources))]
        Regular = 0,

        [DescriptionLocalized("HourlyRateTypeOverTimeNormal", typeof(EnumResources))]
        OverTimeNormal = 1,

        [DescriptionLocalized("HourlyRateTypeOverTimeLateNight", typeof(EnumResources))]
        OverTimeLateNight = 2,

        [DescriptionLocalized("HourlyRateTypeOverTimeWeekend", typeof(EnumResources))]
        OverTimeWeekend = 3,

        [DescriptionLocalized("HourlyRateTypeOverTimeHoliday", typeof(EnumResources))]
        OverTimeHoliday = 4,

        [DescriptionLocalized("HourlyRateTypeOverTimeBank", typeof(EnumResources))]
        OverTimeBank = 5
    }
}