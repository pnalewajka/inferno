﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum ProjectType
    {
        [ProjectTypeJiraIdentifier("unknown")]
        [DescriptionLocalized("ProjectTypeUnknown", typeof(EnumResources))]
        Unknown = -1,

        [ProjectTypeJiraIdentifier("fix")]
        [DescriptionLocalized("ProjectTypeFixedPrice", typeof(EnumResources))]
        FixedPrice = 0,

        [ProjectTypeJiraIdentifier("t&m")]
        [DescriptionLocalized("ProjectTypeTimeAndMaterial", typeof(EnumResources))]
        TimeAndMaterial = 1,

        [ProjectTypeJiraIdentifier("internal")]
        [DescriptionLocalized("ProjectTypeInternal", typeof(EnumResources))]
        Internal = 2,

        [ProjectTypeJiraIdentifier("agile fp")]
        [DescriptionLocalized("ProjectTypeAgileFp", typeof(EnumResources))]
        AgileFixedPrice = 3,

        [ProjectTypeJiraIdentifier("t&m (budget cap)")]
        [DescriptionLocalized("ProjectTypeTimeAndMaterialBudgetCap", typeof(EnumResources))]
        TimeAndMaterialWithBudgetCap = 4,
    }
}
