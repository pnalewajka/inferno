﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum TimeReportingMode
    {
        [DescriptionLocalized("TimeReportingNoTracking", typeof(EnumResources))]
        NoTracking = 0,

        [DescriptionLocalized("TimeReportingAbsenceOnly", typeof(EnumResources))]
        AbsenceOnly = 1,

        [DescriptionLocalized("TimeReportingDetailed", typeof(EnumResources))]
        Detailed = 2
    }
}