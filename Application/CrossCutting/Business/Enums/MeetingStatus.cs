﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.MeetMe)]
    public enum MeetingStatus
    {
        [DescriptionLocalized("PendingMeeting", typeof(EnumResources))]
        Pending = 0,

        [DescriptionLocalized("Scheduled", typeof(EnumResources))]
        Scheduled = 1,

        [DescriptionLocalized("Done", typeof(EnumResources))]
        Done = 2,

    }
}