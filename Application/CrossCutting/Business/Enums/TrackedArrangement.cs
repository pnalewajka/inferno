﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum TrackedArrangement
    {
        [DescriptionLocalized(nameof(EnumResources.TransportTrain), typeof(EnumResources))]
        Train = 0,

        [DescriptionLocalized(nameof(EnumResources.TransportShip), typeof(EnumResources))]
        Ship = 1,

        [DescriptionLocalized(nameof(EnumResources.TransportPlane), typeof(EnumResources))]
        Plane = 2,

        [DescriptionLocalized(nameof(EnumResources.AccommodationHotel), typeof(EnumResources))]
        Hotel = 3,

        [DescriptionLocalized(nameof(EnumResources.Insurance), typeof(EnumResources))]
        Insurance = 4,

        [DescriptionLocalized(nameof(EnumResources.Taxi), typeof(EnumResources))]
        Taxi = 5,

        [DescriptionLocalized(nameof(EnumResources.CompanyApartment), typeof(EnumResources))]
        CompanyApartment = 6,

        [DescriptionLocalized(nameof(EnumResources.Other), typeof(EnumResources))]
        Other = 7,

        [DescriptionLocalized(nameof(EnumResources.AirportTransport), typeof(EnumResources))]
        AirportTransport = 8,
    }
}
