﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ApplicantType
    {
        [DescriptionLocalized(nameof(EnumResources.Recommending), typeof(EnumResources))]
        Recommending = 0,

        [DescriptionLocalized(nameof(EnumResources.Candidate), typeof(EnumResources))]
        Candidate = 1
    }
}
