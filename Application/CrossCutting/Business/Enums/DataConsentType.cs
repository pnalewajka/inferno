﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum DataConsentType
    {
        [DescriptionLocalized(nameof(EnumResources.CandidateConsentGeneral), typeof(EnumResources))]
        CandidateConsentGeneral = 1,

        [DescriptionLocalized(nameof(EnumResources.CandidateConsentSpecificPosition), typeof(EnumResources))]
        CandidateConsentSpecificPosition = 2,

        [DescriptionLocalized(nameof(EnumResources.RecommendingPersonConsentRecommendation), typeof(EnumResources))]
        RecommendingPersonConsentRecommendation = 3,

        [DescriptionLocalized(nameof(EnumResources.CandidateConsentLegacy), typeof(EnumResources))]
        CandidateConsentLegacy = 4,

        [DescriptionLocalized(nameof(EnumResources.CandidateConsentSocialNetwork), typeof(EnumResources))]
        CandidateConsentSocialNetwork = 5,

        [DescriptionLocalized(nameof(EnumResources.CandidateConsentInitialContact), typeof(EnumResources))]
        CandidateConsentInitialContact = 6,

        [DescriptionLocalized(nameof(EnumResources.RecommendingEmployeeConsent), typeof(EnumResources))]
        RecommendingEmployeeConsent = 7,
    }
}