﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Compensation)]
    public enum BusinessTripCompensationCalculators
    {
        [DescriptionLocalized(nameof(EnumResources.CalculatorHourlyRateBasedPL), typeof(EnumResources))]
        HourlyRateBasedPL = 1,

        [DescriptionLocalized(nameof(EnumResources.CalculatorEmploymentContractPL), typeof(EnumResources))]
        EmploymentContractPL = 2,

        [DescriptionLocalized(nameof(EnumResources.CalculatorEmploymentContractUS), typeof(EnumResources))]
        EmploymentContractUS = 3,

        [DescriptionLocalized(nameof(EnumResources.CalculatorEmploymentContractUK), typeof(EnumResources))]
        EmploymentContractUK = 4,

        [DescriptionLocalized(nameof(EnumResources.CalculatorEmploymentContractDE), typeof(EnumResources))]
        EmploymentContractDE = 5,
    }
}
