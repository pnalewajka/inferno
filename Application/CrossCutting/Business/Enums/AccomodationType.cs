﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.BusinessTrips)]
    public enum AccommodationType
    {
        [DescriptionLocalized("AccommodationCompanyApartment", typeof(EnumResources))]
        CompanyApartment = 1 << 0,

        [DescriptionLocalized("AccommodationHotel", typeof(EnumResources))]
        Hotel = 1 << 1,

        [DescriptionLocalized("AccommodationLumpSum", typeof(EnumResources))]
        LumpSum = 1 << 2,

        [DescriptionLocalized("AccommodationLackOfAccommodation", typeof(EnumResources))]
        LackOfAccommodation = 1 << 3,

        [DescriptionLocalized("AccommodationOther", typeof(EnumResources))]
        Other = 1 << 4
    }
}
