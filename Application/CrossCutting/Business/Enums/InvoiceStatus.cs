﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum InvoiceStatus
    {
        [DescriptionLocalized(nameof(EnumResources.InvoiceNotApplicable), typeof(EnumResources))]
        InvoiceNotApplicable = 0,

        [DescriptionLocalized(nameof(EnumResources.InvoiceNotSent), typeof(EnumResources))]
        InvoiceNotSent = 1,

        [DescriptionLocalized(nameof(EnumResources.InvoiceSent), typeof(EnumResources))]
        InvoiceSent = 2,

        [DescriptionLocalized(nameof(EnumResources.InvoiceSentWithIssues), typeof(EnumResources))]
        InvoiceSentWithIssues = 3,

        [DescriptionLocalized(nameof(EnumResources.InvoiceRejected), typeof(EnumResources))]
        InvoiceRejected = 4,

        [DescriptionLocalized(nameof(EnumResources.InvoiceAccepted), typeof(EnumResources))]
        InvoiceAccepted = 5,
    }
}
