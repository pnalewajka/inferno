﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum EmployeeStatus
    {
        [DescriptionLocalized("NewHire", typeof(EnumResources))]
        NewHire = 1,

        [DescriptionLocalized("Active", typeof(EnumResources))]
        Active = 2,

        [DescriptionLocalized("Inactive", typeof(EnumResources))]
        Inactive = 3,

        [DescriptionLocalized("Leave", typeof(EnumResources))]
        Leave = 4,

        [DescriptionLocalized("Terminated", typeof(EnumResources))]
        Terminated = 5,
    }
}
