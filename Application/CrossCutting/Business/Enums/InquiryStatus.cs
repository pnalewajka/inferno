﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Sales)]
    public enum InquiryStatus
    {
        [DescriptionLocalized(nameof(EnumResources.Lead), typeof(EnumResources))]
        Lead = 0,

        [DescriptionLocalized(nameof(EnumResources.Opportunity), typeof(EnumResources))]
        Opportunity = 1,

        [DescriptionLocalized(nameof(EnumResources.Proposal), typeof(EnumResources))]
        Proposal = 2,

        [DescriptionLocalized(nameof(EnumResources.Shortlisted), typeof(EnumResources))]
        Shortlisted = 3,

        [DescriptionLocalized(nameof(EnumResources.Negotiations), typeof(EnumResources))]
        Negotiations = 4,

        [DescriptionLocalized(nameof(EnumResources.ClosedWon), typeof(EnumResources))]
        ClosedWon = 5,

        [DescriptionLocalized(nameof(EnumResources.ClosedLost), typeof(EnumResources))]
        ClosedLost = 6,
    }
}
