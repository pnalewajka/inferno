﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum ProjectFeatures
    {
        [DescriptionLocalized("IsFake", typeof(EnumResources))]
        IsFake = 1,
    }
}
