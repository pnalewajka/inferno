﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum TimelineDetailType
    {
        [DescriptionLocalized(nameof(EnumResources.File), typeof(EnumResources))]
        File = 1,

        [DescriptionLocalized(nameof(EnumResources.CandidateNote), typeof(EnumResources))]
        CandidateNote = 2,

        [DescriptionLocalized(nameof(EnumResources.ContactRecord), typeof(EnumResources))]
        ContactRecord = 3,

        [DescriptionLocalized(nameof(EnumResources.JobApplication), typeof(EnumResources))]
        JobApplication = 4,

        [DescriptionLocalized(nameof(EnumResources.TechnicalReview), typeof(EnumResources))]
        TechnicalReview = 5
    }
}
