﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Compensation)]
    public enum TimeTrackingCompensationCalculators
    {
        [DescriptionLocalized(nameof(EnumResources.CalculatorHourlyRateBasedPL), typeof(EnumResources))]
        HourlyRateBasedPL = 1,

        [DescriptionLocalized(nameof(EnumResources.CalculatorHourlyRateBasedWithOvertimePL), typeof(EnumResources))]
        HourlyRateBasedWithOvertimePL = 2,

        [DescriptionLocalized(nameof(EnumResources.CalculatorHourlyRateBasedDE), typeof(EnumResources))]
        HourlyRateBasedDE = 3,

        [DescriptionLocalized(nameof(EnumResources.CalculatorMonthlyRateBasedPL), typeof(EnumResources))]
        MonthlyRateBasedPL = 4,

        [DescriptionLocalized(nameof(EnumResources.CalculatorMonthlyRateBasedWithOvertimePL), typeof(EnumResources))]
        MonthlyRateBasedWithOvertimePL = 5,

        [DescriptionLocalized(nameof(EnumResources.CalculatorMonthlyRateBasedWithSickLeavePL), typeof(EnumResources))]
        MonthlyRateBasedWithSickLeavePL = 6,

        [DescriptionLocalized(nameof(EnumResources.CalculatorMonthlyRateBasedWithOvertimeAndSickLeavePL), typeof(EnumResources))]
        MonthlyRateBasedWithOvertimeAndSickLeavePL = 7,

        [DescriptionLocalized(nameof(EnumResources.CalculatorEmploymentContractPL), typeof(EnumResources))]
        EmploymentContractPL = 8,

        [DescriptionLocalized(nameof(EnumResources.CalculatorEmploymentContractWithCdePL), typeof(EnumResources))]
        EmploymentContractWithCdePL = 9,

        [DescriptionLocalized(nameof(EnumResources.CalculatorMonthlyRateEmpty), typeof(EnumResources))]
        MonthlyRateEmpty = 10,
    }
}
