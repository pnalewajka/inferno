﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum TimeColumnReportType
    {
        ActiveInactiveEmployee = 1,

        NewHiredLeavingEmployee = 2
    }
}
