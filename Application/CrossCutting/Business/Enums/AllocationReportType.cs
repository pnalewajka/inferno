﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum AllocationReportType
    {
        [DescriptionLocalized("PerOrgUnitText", typeof(EnumResources))]
        PerOrgUnit = 1,

        [DescriptionLocalized("PerLocationText", typeof(EnumResources))]
        PerLocation = 2,

        [DescriptionLocalized("PerJobProfileText", typeof(EnumResources))]
        PerJobProfile = 3
    }
}