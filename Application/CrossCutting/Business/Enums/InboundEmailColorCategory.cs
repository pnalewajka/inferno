﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum InboundEmailColorCategory
    {
        [DescriptionLocalized(nameof(EnumResources.Blue), typeof(EnumResources))]
        Blue = 1,

        [DescriptionLocalized(nameof(EnumResources.Green), typeof(EnumResources))]
        Green = 2,

        [DescriptionLocalized(nameof(EnumResources.Orange), typeof(EnumResources))]
        Orange = 3,

        [DescriptionLocalized(nameof(EnumResources.Purple), typeof(EnumResources))]
        Purple = 4,

        [DescriptionLocalized(nameof(EnumResources.Red), typeof(EnumResources))]
        Red = 5,

        [DescriptionLocalized(nameof(EnumResources.Yellow), typeof(EnumResources))]
        Yellow = 6
    }
}
