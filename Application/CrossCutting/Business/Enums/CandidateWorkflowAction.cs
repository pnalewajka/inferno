﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum CandidateWorkflowAction
    {
        Watch = 1,
        Unwatch = 2,
    }
}
