﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ProcessingStatus
    {
        [DescriptionLocalized("ProcessingStatusProcessing", typeof(EnumResources))]
        Processing = 1,

        [DescriptionLocalized("ProcessingStatusProcessed", typeof(EnumResources))]
        Processed = 2
    }
}
