﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum JobMatrixLevels
    {
        [DescriptionLocalized("Level1", typeof(EnumResources))]
        Level1 = 1,

        [DescriptionLocalized("Level2", typeof(EnumResources))]
        Level2 = 2,

        [DescriptionLocalized("Level3", typeof(EnumResources))]
        Level3 = 3,

        [DescriptionLocalized("Level4", typeof(EnumResources))]
        Level4 = 4,

        [DescriptionLocalized("Level5", typeof(EnumResources))]
        Level5 = 5,
    }
}
