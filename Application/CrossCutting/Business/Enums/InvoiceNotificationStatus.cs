﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum InvoiceNotificationStatus
    {
        [DescriptionLocalized(nameof(EnumResources.NotSent), typeof(EnumResources))]
        NotificationNotSent = 0,

        [DescriptionLocalized(nameof(EnumResources.InProgress), typeof(EnumResources))]
        NotificationInProgress = 1,

        [DescriptionLocalized(nameof(EnumResources.Sent), typeof(EnumResources))]
        NotificationSent = 2,

        [DescriptionLocalized(nameof(EnumResources.Skipped), typeof(EnumResources))]
        NotificationSkipped = 3,

        [DescriptionLocalized(nameof(EnumResources.Outdated), typeof(EnumResources))]
        NotificationOutdated = 4,

        [DescriptionLocalized(nameof(EnumResources.Blocked), typeof(EnumResources))]
        NotificationBlocked = 5,
    }
}