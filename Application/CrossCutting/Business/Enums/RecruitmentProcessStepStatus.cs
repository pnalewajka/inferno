﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum RecruitmentProcessStepStatus
    {
        [DescriptionLocalized(nameof(EnumResources.Draft), typeof(EnumResources))]
        Draft = 0,

        [DescriptionLocalized(nameof(EnumResources.ToDo), typeof(EnumResources))]
        ToDo = 1,

        [DescriptionLocalized(nameof(EnumResources.Done), typeof(EnumResources))]
        Done = 2,

        [DescriptionLocalized(nameof(EnumResources.Cancelled), typeof(EnumResources))]
        Cancelled = 3
    }
}
