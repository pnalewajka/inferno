﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum ControllingStatus
    {
        [DescriptionLocalized(nameof(EnumResources.NotSent), typeof(EnumResources))]
        NotSent = 0,

        [DescriptionLocalized(nameof(EnumResources.InProgress), typeof(EnumResources))]
        InProgress = 1,

        [DescriptionLocalized(nameof(EnumResources.Sent), typeof(EnumResources))]
        Sent = 2,

        [DescriptionLocalized(nameof(EnumResources.Error), typeof(EnumResources))]
        Error = 3,
    }
}