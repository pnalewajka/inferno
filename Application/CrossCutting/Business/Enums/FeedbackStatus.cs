﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum FeedbackStatus
    {
        [DescriptionLocalized("Draft", typeof(EnumResources))]
        Draft = 0,

        [DescriptionLocalized("Done", typeof(EnumResources))]
        Done = 1,
    }
}