﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.SkillManagement)]
    public enum SkillTagType
    {
        [DescriptionLocalized("KeyTechnology", typeof(EnumResources))]
        KeyTechnology = 1,

        [DescriptionLocalized("DomainKnowledge", typeof(EnumResources))]
        DomainKnowledge = 2,

        [DescriptionLocalized("MainTaxonomy", typeof(EnumResources))]
        MainTaxonomy = 4,

        [DescriptionLocalized("PricingEngineer", typeof(EnumResources))]
        PricingEngineer = 8,

        [DescriptionLocalized("TechnicalInterviewer", typeof(EnumResources))]
        TechnicalInterviewer = 16,

        [DescriptionLocalized("ResumeTechnicalSkillsGroup", typeof(EnumResources))]
        ResumeTechnicalSkillsGroup = 32,
    }
}
