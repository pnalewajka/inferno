﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum PlaceOfWork
    {
        [ActiveDirectoryCode("Company office")]
        [DescriptionLocalized("PlaceOfWorkCompanyOffice", typeof(EnumResources))]
        CompanyOffice = 0,

        [ActiveDirectoryCode("Client office")]
        [DescriptionLocalized("PlaceOfWorkClientOffice", typeof(EnumResources))]
        ClientOffice = 1,

        [ActiveDirectoryCode("Home office")]
        [DescriptionLocalized("PlaceOfWorkHomeOffice", typeof(EnumResources))]
        HomeOffice = 2
    }
}