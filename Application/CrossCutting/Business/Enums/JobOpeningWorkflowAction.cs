﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum JobOpeningWorkflowAction
    {
        SendForApproval = 1,
        Approve = 2,
        Reject = 3,
        Close = 4,
        Watch = 5,
        Unwatch = 6,
    }
}
