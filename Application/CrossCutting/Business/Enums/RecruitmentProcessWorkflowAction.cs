﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum RecruitmentProcessWorkflowAction
    {
        Watch = 1,
        Unwatch = 2,
        Close = 3
    }
}
