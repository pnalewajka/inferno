﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Identifier("Allocations.Enums.ActiveDirectoryGroupType")]
    public enum ActiveDirectoryGroupType
    {
        [DescriptionLocalized("Security", typeof(EnumResources))]
        Security = 1,

        [DescriptionLocalized("Mailing", typeof(EnumResources))]
        Mailing = 2,
    }
}
