﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    public enum ProjectAttachmentType
    {
        [DescriptionLocalized("ReferenceMaterial", typeof(EnumResources))]
        ReferenceMaterial = 1,

        [DescriptionLocalized("ProjectInformation", typeof(EnumResources))]
        ProjectInformation = 2,

        [DescriptionLocalized("Other", typeof(EnumResources))]
        Other = 3
    }
}
