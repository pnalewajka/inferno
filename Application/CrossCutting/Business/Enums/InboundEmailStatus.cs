﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum InboundEmailStatus
    {
        [DescriptionLocalized(nameof(EnumResources.New), typeof(EnumResources))]
        New,

        [DescriptionLocalized(nameof(EnumResources.Assigned), typeof(EnumResources))]
        Assigned,

        [DescriptionLocalized(nameof(EnumResources.Closed), typeof(EnumResources))]
        Closed,

        [DescriptionLocalized(nameof(EnumResources.SentForResearch), typeof(EnumResources))]
        SentForResearch
    }
}
