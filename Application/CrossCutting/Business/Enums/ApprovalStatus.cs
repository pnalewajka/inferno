﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    // Update ActionOnBehalf.ts too!
    [SqlEnum(DatabaseSchemes.Workflows)]
    public enum ApprovalStatus
    {
        [DescriptionLocalized("Pending", typeof(EnumResources))]
        Pending = 0,

        [DescriptionLocalized("Approved", typeof(EnumResources))]
        Approved = 1,

        [DescriptionLocalized("Rejected", typeof(EnumResources))]
        Rejected = 2
    }
}