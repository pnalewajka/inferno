﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ContactRestriction
    {
        [DescriptionLocalized("None", typeof(EnumResources))]
        None = 0,

        [DescriptionLocalized("AgencyRestrictions", typeof(EnumResources))]
        AgencyRestrictions = 1,

        [DescriptionLocalized("Other", typeof(EnumResources))]
        Other = 2        
    }
}
