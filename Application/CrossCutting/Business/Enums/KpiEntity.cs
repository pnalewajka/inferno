using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum KpiEntity
    {
        [DescriptionLocalized(nameof(Project), typeof(EnumResources))]
        [KpiEntity("Allocation.FullProject", "p", "Id", "ProjectShortName", "ClientShortName")]
        Project,

        [DescriptionLocalized(nameof(Employee), typeof(EnumResources))]
        [KpiEntity("Allocation.Employee", "e", "Id", "Acronym", "FirstName", "LastName")]
        Employee,

        [DescriptionLocalized(nameof(OrgUnit), typeof(EnumResources))]
        [KpiEntity("Organization.OrgUnit", "ou", "Id", "Name", "FlatName", "EmployeeId")]
        OrgUnit
    }
}
