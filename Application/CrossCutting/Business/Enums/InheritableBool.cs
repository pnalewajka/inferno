﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum InheritableBool
    {
        [DescriptionLocalized("Inherited", typeof(EnumResources))]
        Inherited = 0,

        [DescriptionLocalized("No", typeof(EnumResources))]
        No = 1,

        [DescriptionLocalized("Yes", typeof(EnumResources))]
        Yes = 2,
    }
}