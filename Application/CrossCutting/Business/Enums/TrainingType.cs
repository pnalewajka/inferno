﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Resumes)]
    public enum TrainingType
    {
        [DescriptionLocalized("Course", typeof(EnumResources))]
        Course = 0,

        [DescriptionLocalized("Certificate", typeof(EnumResources))]
        Certificate = 1,
    }
}
