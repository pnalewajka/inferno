﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum MileageUnit
    {
        [DescriptionLocalized(nameof(EnumResources.Kilometers), typeof(EnumResources))]
        Kilometers = 0,

        [DescriptionLocalized(nameof(EnumResources.Miles), typeof(EnumResources))]
        Miles = 1,
    }
}
