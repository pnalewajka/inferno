﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.BusinessTrips)]
    public enum ArrangementType
    {
        [DescriptionLocalized(nameof(EnumResources.Unknown), typeof(EnumResources))]
        Unknown = 0,

        [DescriptionLocalized(nameof(EnumResources.Flight), typeof(EnumResources))]
        Flight = 1,

        [DescriptionLocalized(nameof(EnumResources.Train), typeof(EnumResources))]
        Train = 2,

        [DescriptionLocalized(nameof(EnumResources.InvoiceFlight), typeof(EnumResources))]
        InvoiceFlight = 3,

        [DescriptionLocalized(nameof(EnumResources.InvoiceTrain), typeof(EnumResources))]
        InvoiceTrain = 4,

        [DescriptionLocalized(nameof(EnumResources.Taxi), typeof(EnumResources))]
        Taxi = 5,

        [DescriptionLocalized(nameof(EnumResources.InvoiceTaxi), typeof(EnumResources))]
        InvoiceTaxi = 6,

        [DescriptionLocalized(nameof(EnumResources.Hotel), typeof(EnumResources))]
        Hotel = 7,

        [DescriptionLocalized(nameof(EnumResources.InvoiceHotel), typeof(EnumResources))]
        InvoiceHotel = 8,

        [DescriptionLocalized(nameof(EnumResources.Other), typeof(EnumResources))]
        Other = 9,

        [DescriptionLocalized(nameof(EnumResources.InvoiceOther), typeof(EnumResources))]
        InvoiceOther = 10,
    }
}