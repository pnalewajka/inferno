﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Identifier("Survey.Enums.WhistleBlowingMessageStatus")]
    public enum WhistleBlowingMessageStatus
    {
        [DescriptionLocalized("MessageUnreadLabel", typeof(EnumResources))]
        Unread,

        [DescriptionLocalized("MessageReadLabel", typeof(EnumResources))]
        Read,
    }
}
