﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum SurveyFieldSize
    {
        [DescriptionLocalized("SurveySizeMedium", typeof(EnumResources))]
        Medium = 0,
        [DescriptionLocalized("SurveySizeSmall", typeof(EnumResources))]
        Small = 1,
        [DescriptionLocalized("SurveySizeLarge", typeof(EnumResources))]
        Large = 2
    }
}
