﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum ContactType
    {
        [DescriptionLocalized(nameof(EnumResources.Email), typeof(EnumResources))]
        Email = 1,

        [DescriptionLocalized(nameof(EnumResources.Telephone), typeof(EnumResources))]
        Telephone = 2,

        [DescriptionLocalized(nameof(EnumResources.Skype), typeof(EnumResources))]
        Skype = 3,

        [DescriptionLocalized(nameof(EnumResources.SocialNetwork), typeof(EnumResources))]
        SocialNetwork = 4,

        [DescriptionLocalized(nameof(EnumResources.Meeting), typeof(EnumResources))]
        Meeting = 5,

        [DescriptionLocalized(nameof(EnumResources.Other), typeof(EnumResources))]
        Other = 6,
    }
}