﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.BusinessTrips, "DestinationType")]
    public enum BusinessTripDestinationType
    {
        [DescriptionLocalized(nameof(EnumResources.BusinessTripDestinationTypeCompanyOffice), typeof(EnumResources))]
        CompanyOffice = 1,

        [DescriptionLocalized(nameof(EnumResources.BusinessTripDestinationTypeCustomer), typeof(EnumResources))]
        Customer = 2,
    }
}
