﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum BoolFilterOptionsEnum
    {
        [DescriptionLocalized("Yes", typeof(EnumResources))]
        Yes = 1,

        [DescriptionLocalized("No", typeof(EnumResources))]
        No = 2,

        [DescriptionLocalized("Either", typeof(EnumResources))]
        Either = 3,
    }
}
