﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum BillabilityFiltering
    {
        [DescriptionLocalized("BillabilityAllRecords", typeof(EnumResources))]
        All,

        [DescriptionLocalized("BillabilityBillable", typeof(EnumResources))]
        Billable,

        [DescriptionLocalized("BillabilityNoBillable", typeof(EnumResources))]
        NoBillable
    }
}
