﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum AllocationStatusType
    {
        [DescriptionLocalized("Weekly", typeof(EnumResources))]
        Weekly = 1,

        [DescriptionLocalized("ImmediateFuture", typeof(EnumResources))]
        ImmediateFuture = 2
    }
}
