﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.BusinessTrips)]
    public enum VehicleType
    {
        [DescriptionLocalized(nameof(Car), typeof(EnumResources))]
        Car = 0,

        [DescriptionLocalized(nameof(Motorcycle), typeof(EnumResources))]
        Motorcycle = 1,

        [DescriptionLocalized(nameof(Moped), typeof(EnumResources))]
        Moped = 2,
    }
}
