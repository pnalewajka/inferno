﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.Organization)]
    public enum OrgUnitFeatures
    {
        [DescriptionLocalized("IsJiraDepartment", typeof(EnumResources))]
        IsJiraDepartment = 1 << 0,

        [DescriptionLocalized("IsDepartment", typeof(EnumResources))]
        IsDepartment = 1 << 1,

        [DescriptionLocalized("IsBusinessUnit", typeof(EnumResources))]
        IsBusinessUnit = 1 << 2,

        [DescriptionLocalized("ShowChart", typeof(EnumResources))]
        ShowChart = 1 << 3,

        [DescriptionLocalized("ShowInUtilizationReport", typeof(EnumResources))]
        ShowInUtilizationReport = 1 << 4,

        [DescriptionLocalized("AutonomousOrgUnit", typeof(EnumResources))]
        AutonomousOrgUnit = 1 << 5,
    }
}
