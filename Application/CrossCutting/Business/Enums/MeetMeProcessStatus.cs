﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum MeetMeProcessStatus
    {
        [DescriptionLocalized(nameof(EnumResources.NoMeetingCalculated), typeof(EnumResources))]
        NoMeetingCalculated = 0,

        [DescriptionLocalized(nameof(EnumResources.Scheduled), typeof(EnumResources))]
        Scheduled = 1,

        [DescriptionLocalized(nameof(EnumResources.Overdue), typeof(EnumResources))]
        Overdue = 2
    }
}
