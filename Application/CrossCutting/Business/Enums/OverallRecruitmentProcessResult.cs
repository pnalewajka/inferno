﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum OverallRecruitmentProcessResult
    {
        [DescriptionLocalized(nameof(EnumResources.Positive), typeof(EnumResources))]
        Positive = 1,

        [DescriptionLocalized(nameof(EnumResources.Negative), typeof(EnumResources))]
        Negative = 2
    }
}
