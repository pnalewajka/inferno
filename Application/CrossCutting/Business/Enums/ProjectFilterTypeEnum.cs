﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum ProjectFilterTypeEnum
    {
        [DescriptionLocalized("AllProjectsText", typeof(EnumResources))]
        AllProjects,
        [DescriptionLocalized("MyProjectsText", typeof(EnumResources))]
        MyProjects
    }
}
