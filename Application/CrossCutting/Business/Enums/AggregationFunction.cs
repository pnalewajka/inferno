﻿namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum AggregationFunction
    {
        Sum = 1,
        Min = 2,
        Max = 3,
        Avg = 4,
    }
}
