﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum RecruitmentProcessStepType
    {
        [DescriptionLocalized(nameof(EnumResources.HrCall), typeof(EnumResources))]
        HrCall = 100,

        [DescriptionLocalized(nameof(EnumResources.HrInterview), typeof(EnumResources))]
        HrInterview = 200,

        [DescriptionLocalized(nameof(EnumResources.TechnicalReview), typeof(EnumResources))]
        TechnicalReview = 300,

        [DescriptionLocalized(nameof(EnumResources.HiringManagerResumeApproval), typeof(EnumResources))]
        HiringManagerResumeApproval = 400,

        [DescriptionLocalized(nameof(EnumResources.ClientResumeApproval), typeof(EnumResources))]
        ClientResumeApproval = 500,

        [DescriptionLocalized(nameof(EnumResources.HiringManagerInterview), typeof(EnumResources))]
        HiringManagerInterview = 600,

        [DescriptionLocalized(nameof(EnumResources.ClientInterview), typeof(EnumResources))]
        ClientInterview = 700,

        [DescriptionLocalized(nameof(EnumResources.HiringDecision), typeof(EnumResources))]
        HiringDecision = 800,

        [DescriptionLocalized(nameof(EnumResources.ContractNegotiations), typeof(EnumResources))]
        ContractNegotiations = 900,
    }
}
