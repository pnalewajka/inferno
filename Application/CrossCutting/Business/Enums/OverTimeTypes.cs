﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum OverTimeTypes
    {
        [DescriptionLocalized("HourlyRateTypeNoOverTime", typeof(EnumResources))]
        NoOverTime = 0,

        [DescriptionLocalized("HourlyRateTypeOverTimeNormal", typeof(EnumResources))]
        OverTimeNormal = 1 << HourlyRateType.OverTimeNormal,

        [DescriptionLocalized("HourlyRateTypeOverTimeLateNight", typeof(EnumResources))]
        OverTimeLateNight = 1 << HourlyRateType.OverTimeLateNight,

        [DescriptionLocalized("HourlyRateTypeOverTimeWeekend", typeof(EnumResources))]
        OverTimeWeekend = 1 << HourlyRateType.OverTimeWeekend,

        [DescriptionLocalized("HourlyRateTypeOverTimeHoliday", typeof(EnumResources))]
        OverTimeHoliday = 1 << HourlyRateType.OverTimeHoliday,

        [DescriptionLocalized("HourlyRateTypeOverTimeBank", typeof(EnumResources))]
        OverTimeBank = 1 << HourlyRateType.OverTimeBank,
    }
}