﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum NoteType
    {
        [DescriptionLocalized(nameof(EnumResources.UserNote), typeof(EnumResources))]
        UserNote = 1,

        [DescriptionLocalized(nameof(EnumResources.SystemNote), typeof(EnumResources))]
        SystemNote = 2
    }
}
