﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum FileSource
    {
        [DescriptionLocalized(nameof(EnumResources.Disk), typeof(EnumResources))]
        Disk,

        [DescriptionLocalized(nameof(EnumResources.InboundEmailAttachment), typeof(EnumResources))]
        InboundEmailAttachment,

        [DescriptionLocalized(nameof(EnumResources.InboundEmailContent), typeof(EnumResources))]
        InboundEmailContent,
    }
}
