﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.PeopleManagement)]
    public enum EmployeeFeatures
    {
        [DescriptionLocalized("ShowInUtilizationReport", typeof(EnumResources))]
        ShowInUtilizationReport = 1 << 0
    }
}
