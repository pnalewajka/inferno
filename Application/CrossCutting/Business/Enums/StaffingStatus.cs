﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Allocation)]
    public enum StaffingStatus
    {
        [DescriptionLocalized("BenchFree", typeof(EnumResources))]
        Free = 1,

        [DescriptionLocalized("BenchPlanned", typeof(EnumResources))]
        Planned = 2,

        [DescriptionLocalized("BenchPlannedAndConfirmed", typeof(EnumResources))]
        PlannedAndConfirmed = 3,

        [DescriptionLocalized("BenchAllocated", typeof(EnumResources))]
        Allocated = 4,

        [DescriptionLocalized("BenchNotAvaliable", typeof(EnumResources))]
        NotAvaliable = 5,
    }
}
