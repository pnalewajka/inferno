﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    public enum FeedbackVisbility
    {
        [DescriptionLocalized(nameof(EnumResources.VisibleForReceiver), typeof(EnumResources))]
        VisibleForReceiver = 1 << 0,

        [DescriptionLocalized(nameof(EnumResources.AnonymousForReceiver), typeof(EnumResources))]
        AnonymousForReceiver = 1 << 1
    }
}
