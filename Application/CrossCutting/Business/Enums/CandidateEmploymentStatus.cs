﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum CandidateEmploymentStatus
    {
        [DescriptionLocalized(nameof(EnumResources.NotEmployee), typeof(EnumResources))]
        NotEmployee = 0,

        [DescriptionLocalized(nameof(EnumResources.CurrentEmployee), typeof(EnumResources))]
        CurrentEmployee = 1,

        [DescriptionLocalized(nameof(EnumResources.PastEmployee), typeof(EnumResources))]
        PastEmployee = 2
    }
}