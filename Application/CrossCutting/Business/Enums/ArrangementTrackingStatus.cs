﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.BusinessTrips)]
    public enum ArrangementTrackingStatus
    {
        [DescriptionLocalized(nameof(EnumResources.ToDo), typeof(EnumResources))]
        ToDo = 0,

        [DescriptionLocalized(nameof(EnumResources.RequestedExternal), typeof(EnumResources))]
        RequestedExternal = 1,

        [DescriptionLocalized(nameof(EnumResources.InProgress), typeof(EnumResources))]
        InProgress = 2,

        [DescriptionLocalized(nameof(EnumResources.Done), typeof(EnumResources))]
        Done = 3,
    }
}
