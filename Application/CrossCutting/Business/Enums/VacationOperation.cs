﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum VacationOperation
    {
        [DescriptionLocalized("YearlyCalculation", typeof(EnumResources))]
        YearlyCalculation = 0,

        [DescriptionLocalized("Vacation", typeof(EnumResources))]
        Vacation = 1,

        [DescriptionLocalized("ManualCorrection", typeof(EnumResources))]
        ManualCorrection = 2,
    }
}