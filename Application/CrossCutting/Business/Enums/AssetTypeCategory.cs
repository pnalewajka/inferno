﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Identifier("PeopleManagement.Enums.AssetCategoryType")]
    [SqlEnum(DatabaseSchemes.PeopleManagement)]
    public enum AssetTypeCategory
    {
        [DescriptionLocalized(nameof(EnumResources.Other), typeof(EnumResources))]
        Other = 0,

        [DescriptionLocalized(nameof(EnumResources.Software), typeof(EnumResources))]
        Software = 1,

        [DescriptionLocalized(nameof(EnumResources.HardwareSet), typeof(EnumResources))]
        HardwareSet = 2,

        [DescriptionLocalized(nameof(EnumResources.Headset), typeof(EnumResources))]
        Headset = 3,

        [DescriptionLocalized(nameof(EnumResources.Backpack), typeof(EnumResources))]
        Backpack = 4,

        [DescriptionLocalized(nameof(EnumResources.OptionalHardware), typeof(EnumResources))]
        OptionalHardware = 5,
    }
}
