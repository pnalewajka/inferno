﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Recruitment)]
    public enum JobOpeningStatus
    {
        [DescriptionLocalized(nameof(EnumResources.Draft), typeof(EnumResources))]
        Draft = 0,

        [DescriptionLocalized(nameof(EnumResources.PendingApproval), typeof(EnumResources))]
        PendingApproval = 1,

        [DescriptionLocalized(nameof(EnumResources.Active), typeof(EnumResources))]
        Active = 2,

        [DescriptionLocalized(nameof(EnumResources.Rejected), typeof(EnumResources))]
        Rejected = 3,

        [DescriptionLocalized(nameof(EnumResources.Closed), typeof(EnumResources))]
        Closed = 4,

        [DescriptionLocalized(nameof(EnumResources.OnHold), typeof(EnumResources))]
        OnHold = 5
    }
}