﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum SurveyQuestionType
    {
        [DescriptionLocalized("SurveyQuestionTypeCombo", typeof(EnumResources))]
        Combo = 1,

        [DescriptionLocalized("SurveyQuestionTypePicker", typeof(EnumResources))]
        Picker = 2,

        [DescriptionLocalized("SurveyQuestionTypeCheckbox", typeof(EnumResources))]
        Checkbox = 3,

        [DescriptionLocalized("SurveyQuestionTypeRadio", typeof(EnumResources))]
        Radio = 4,

        [DescriptionLocalized("SurveyQuestionTypeOpenQuestion", typeof(EnumResources))]
        OpenQuestion = 5,
    }
}
