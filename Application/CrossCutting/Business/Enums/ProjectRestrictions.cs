﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    public enum ProjectDisclosures
    {
        [DescriptionLocalized("WeCanTalkAboutClient", typeof(EnumResources))]
        WeCanTalkAboutClient = 1,

        [DescriptionLocalized("WeCanTalkAboutProject", typeof(EnumResources))]
        WeCanTalkAboutProject = 2,

        [DescriptionLocalized("WeCanUseClientLogo", typeof(EnumResources))]
        WeCanUseClientLogo = 4,
    }
}
