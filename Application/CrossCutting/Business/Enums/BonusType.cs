﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.Compensation)]
    public enum BonusType
    {
        [DescriptionLocalized(nameof(EnumResources.Bonus), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestBonusType)]
        Bonus = 0,

        [DescriptionLocalized(nameof(EnumResources.Oncall), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestOncallBonusType)]
        Oncall = 1,

        [DescriptionLocalized(nameof(EnumResources.ShortTerm), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestShortTermBonusType)]
        ShortTerm = 2,

        [DescriptionLocalized(nameof(EnumResources.Reallocation), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestReallocationBonusType)]
        Reallocation = 3,

        [DescriptionLocalized(nameof(EnumResources.Recommendation), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestRecommendationBonusType)]
        Recommendation = 4,

        [DescriptionLocalized(nameof(EnumResources.Benefits), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestBenefitsBonusType)]
        Benefits = 5,

        [DescriptionLocalized(nameof(EnumResources.Regular), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestRegularBonusType)]
        Regular = 6,

        [DescriptionLocalized(nameof(EnumResources.Holiday), typeof(EnumResources))]
        [RoleRequired(SecurityRoleType.CanRequestHolidayBonusType)]
        Holiday = 7,
    }
}
