﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum TimeTrackingProjectType
    {
        [DescriptionLocalized("TimeTrackingProjectNormal", typeof(EnumResources))]
        Normal = 0,

        [DescriptionLocalized("TimeTrackingProjectAbsence", typeof(EnumResources))]
        Absence = 1,

        [DescriptionLocalized("TimeTrackingProjectHoliday", typeof(EnumResources))]
        Holiday = 2,

        [DescriptionLocalized("TimeTrackingProjectTraining", typeof(EnumResources))]
        Training = 3,

        [DescriptionLocalized("TimeTrackingProjectGuild", typeof(EnumResources))]
        Guild = 4,
    }
}
