﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    [SqlEnum(DatabaseSchemes.TimeTracking)]
    public enum AttributeValueFeature : long
    {
        None = 0,
        NonBillable = 1 << 0,
        PassiveWorkingTime = 1 << 1,
    }
}
