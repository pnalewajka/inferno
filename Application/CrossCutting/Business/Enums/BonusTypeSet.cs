﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    public enum BonusTypeSet
    {
        None = 0,

        [DescriptionLocalized(nameof(EnumResources.Bonus), typeof(EnumResources))]
        Bonus = 1,

        [DescriptionLocalized(nameof(EnumResources.Oncall), typeof(EnumResources))]
        Oncall = 2,

        [DescriptionLocalized(nameof(EnumResources.ShortTerm), typeof(EnumResources))]
        ShortTerm = 4,

        [DescriptionLocalized(nameof(EnumResources.Reallocation), typeof(EnumResources))]
        Reallocation = 8,

        [DescriptionLocalized(nameof(EnumResources.Recommendation), typeof(EnumResources))]
        Recommendation = 16,

        [DescriptionLocalized(nameof(EnumResources.Benefits), typeof(EnumResources))]
        Benefits = 32,

        [DescriptionLocalized(nameof(EnumResources.Regular), typeof(EnumResources))]
        Regular = 64,

        [DescriptionLocalized(nameof(EnumResources.Holiday), typeof(EnumResources))]
        Holiday = 128,
    }
}
