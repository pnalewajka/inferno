﻿using System;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    [Flags]
    public enum ProjectTagType
    {
        [DescriptionLocalized("Sales", typeof(EnumResources))]
        Sales = 1,

        [DescriptionLocalized("Jira", typeof(EnumResources))]
        Jira = 2,
    }
}
