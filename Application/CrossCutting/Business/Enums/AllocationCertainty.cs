﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum AllocationCertainty
    {
        [DescriptionLocalized("AllocationCertaintyTypePlanned", typeof(EnumResources))]
        Planned = 0,

        [DescriptionLocalized("AllocationCertaintyTypeCertain", typeof(EnumResources))]
        Certain = 1,
    }
}