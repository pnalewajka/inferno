﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum EmailBodyType
    {
        [DescriptionLocalized(nameof(EnumResources.EmailBodyTypeText), typeof(EnumResources))]
        Text = 1,

        [DescriptionLocalized(nameof(EnumResources.EmailBodyTypeHtml), typeof(EnumResources))]
        Html = 2,
    }
}
