﻿using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Business.Enums
{
    public enum LanguageReferenceLevel
    {
        [DescriptionLocalized("None", typeof(EnumResources))]
        None = 0,

        [DescriptionLocalized("A1", typeof(EnumResources))]
        A1 = 1,

        [DescriptionLocalized("A2", typeof(EnumResources))]
        A2 = 2,

        [DescriptionLocalized("B1", typeof(EnumResources))]
        B1 = 3,

        [DescriptionLocalized("B2", typeof(EnumResources))]
        B2 = 4,

        [DescriptionLocalized("C1", typeof(EnumResources))]
        C1 = 5,

        [DescriptionLocalized("C2", typeof(EnumResources))]
        C2 = 6
    }
}
