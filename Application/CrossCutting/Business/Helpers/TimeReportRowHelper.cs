﻿using Castle.Core.Internal;
using System.Text.RegularExpressions;

namespace Smt.Atomic.CrossCutting.Business.Helpers
{
    public class TimeReportRowHelper
    {
        public static string CleanTaskName(string taskName)
        {
            return (taskName ?? string.Empty).Trim();
        }

        public static string RemoveNotAllowedCharacters(string value)
        {
            if (value.IsNullOrEmpty())
            {
                return value;
            }

            // Same as MyTimeReport.ts : cleanupTaskName()
            var replaceRegex = new Regex("(?i:[^a-z0-9äéöüßąćęłńóśźż\\(\\)\\s,.:\\'\"\\*\\-\\+\\/\\\\]+)");
            var multipleSpaces = new Regex(@"[\s]{2,}");

            value = replaceRegex.Replace(value, string.Empty);
            value = multipleSpaces.Replace(value, " ");

            return value;
        }
    }
}
