﻿namespace Smt.Atomic.CrossCutting.Business.Helpers
{
    public static class BusinessLogicHelper
    {
        public const long NonExistingId = default(long);
        public const string NonExistingOrAnonymousUserName = "Unknown";
    }
}
