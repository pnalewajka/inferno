﻿using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using JobSchedulerSettings = Smt.Atomic.CrossCutting.Settings.Models.JobSchedulerSettings;

namespace Smt.Atomic.CrossCutting.Settings.Configuration
{
    internal class SettingsProvider : SettingsProviderBase, ISettingsProvider
    {
        private const string FakeTimeAllowedParameter = "FakeTimeAllowed";
        private const string ReportNewIssueUrlParameter = "ReportNewIssueUrl";
        private const string ExceptionDetailsAvailableParameter = "ExceptionDetailsAvailable";
        private const string ExceptionFormDetailsAvailableParameter = "ExceptionFormDetailsAvailable";
        private const string EncryptionPasswordParameter = "EncryptionPassword";
        private const string DefaultUserPictureFilePathParameter = "DefaultUserPicture.FilePath";
        private const string DefaultProjectPictureFilePathParameter = "DefaultProjectPicture.FilePath";

        public string ReportNewIssueUrl => GetString(ReportNewIssueUrlParameter);

        public bool IsFakeTimeAllowed => GetProductionReadyBool(FakeTimeAllowedParameter);

        public JobSchedulerSettings JobSchedulerSettings => GetSection<JobSchedulerSettings>();

        public AuthorizationProviderSettings AuthorizationProviderSettings => GetSection<AuthorizationProviderSettings>();

        public bool AreExceptionDetailsAvailable => GetBool(ExceptionDetailsAvailableParameter, false);

        public bool AreExceptionFormDetailsAvailable => GetBool(ExceptionFormDetailsAvailableParameter, false);

        public string EncryptionPassword => GetString(EncryptionPasswordParameter);

        public string DefaultUserPictureFilePath => GetString(DefaultUserPictureFilePathParameter);

        public string DefaultProjectPictureFilePath => GetString(DefaultProjectPictureFilePathParameter);

        public GoogleMapsSettings GoogleMapsSettings => GetSection<GoogleMapsSettings>();

        public JiraSettings JiraSettings => GetSection<JiraSettings>();

        public CrmSettings CrmSettings => GetSection<CrmSettings>();

        public CrmImportSettings CrmImportSettings => GetSection<CrmImportSettings>();

        public SharePointSettings SharePointSettings => GetSection<SharePointSettings>();

        public MiscellaneousSettings MiscellaneousSettings => GetSection<MiscellaneousSettings>();

        public SettingsProvider(ILogger logger)
            : base(logger)
        {
        }
    }
}
