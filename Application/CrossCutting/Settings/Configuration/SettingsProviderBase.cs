﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml.Serialization;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Settings.Configuration
{
    public abstract class SettingsProviderBase
    {
        protected readonly ILogger Logger;

        protected SettingsProviderBase(ILogger logger)
        {
            Logger = logger;
        }

        protected string GetString(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        protected int GetInt(string key, int fallback)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (value == null)
            {
                Logger.DebugFormat("Key [{0}] value is not present in appSettings. Using fallback value [{1}] instead.", key, fallback);

                return fallback;
            }

            int defaultValue;
            var parseResult = int.TryParse(value, out defaultValue);

            if (parseResult)
            {
                return defaultValue;
            }

            Logger.DebugFormat("Key [{0}] value is not parsable as Integer. Using fallback value [{1}] instead.", key, fallback);

            return fallback;
        }

        protected long GetLong(string key, int fallback)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (value == null)
            {
                Logger.DebugFormat("Key [{0}] value is not present in appSettings. Using fallback value [{1}] instead.", key, fallback);

                return fallback;
            }

            long defaultValue;
            var parseResult = long.TryParse(value, out defaultValue);

            if (parseResult)
            {
                return defaultValue;
            }

            Logger.DebugFormat("Key [{0}] value is not parsable as Integer. Using fallback value [{1}] instead.", key, fallback);

            return fallback;
        }

        protected bool GetBool(string key, bool fallback)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (value == null)
            {
                Logger.DebugFormat("Key [{0}] value is not present in appSettings. Using fallback value [{1}] instead.", key, fallback);

                return fallback;
            }

            bool defaultValue;
            var parseResult = bool.TryParse(value, out defaultValue);

            if (parseResult)
            {
                return defaultValue;
            }

            Logger.DebugFormat("Key [{0}] value is not parsable as Boolean. Using fallback value [{1}] instead.", key, fallback);

            return fallback;
        }

        protected bool GetProductionReadyBool(string key)
        {
#if DEBUG
            return GetBool(key, false);
#else
            return false;
#endif
        }

        protected IEnumerable<string> GetSequence(string key, char separator)
        {
            var value = ConfigurationManager.AppSettings[key];

            if (value == null)
            {
                Logger.DebugFormat("Key [{0}] value is not present in appSettings. Using empty list instead.", key);

                return new List<string>();
            }

            return value.Split(separator);
        }

        protected string GetConnectionString(string connectionStringKey)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringKey];

            if (connectionString == null)
            {
                throw new ArgumentException("Could not found connection string: " + connectionStringKey, nameof(connectionStringKey));
            }

            return connectionString.ConnectionString;
        }

        protected TSectionModel GetSection<TSectionModel>() where TSectionModel : class
        {
            var attribute = AttributeHelper.GetClassAttribute<XmlRootAttribute>(typeof(TSectionModel));
            var sectionName = attribute == null ? typeof(TSectionModel).Name : attribute.ElementName;

            return ConfigurationManager.GetSection(sectionName) as TSectionModel;
        }
    }
}