﻿using Smt.Atomic.CrossCutting.Settings.Models;

namespace Smt.Atomic.CrossCutting.Settings.Interfaces
{
    /// <summary>
    /// Interface for system wide access to configuration parameters
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// Is fake time allowed - for time changing feature
        /// </summary>
        bool IsFakeTimeAllowed { get; }

        /// <summary>
        /// Url which should be used for error reporting
        /// </summary>
        string ReportNewIssueUrl { get; }

        /// <summary>
        /// Scheduler of settings
        /// </summary>
        JobSchedulerSettings JobSchedulerSettings { get; }

        /// <summary>
        /// Authorization providers settings
        /// </summary>
        AuthorizationProviderSettings AuthorizationProviderSettings { get; }

        /// <summary>
        /// True if the end user should be able to see the details of the exception
        /// </summary>
        bool AreExceptionDetailsAvailable { get; }

        /// <summary>
        /// True if the end user should be able to see the details of the exception form
        /// </summary>
        bool AreExceptionFormDetailsAvailable { get; }

        /// <summary>
        /// Password used for encryption
        /// </summary>
        string EncryptionPassword { get; }

        /// <summary>
        /// Path to default user picture (for profile)
        /// </summary>
        string DefaultUserPictureFilePath { get; }

        /// <summary>
        /// Path to default user picture (for profile)
        /// </summary>
        string DefaultProjectPictureFilePath { get; }

        /// <summary>
        /// Google maps settings
        /// </summary>
        GoogleMapsSettings GoogleMapsSettings { get; }

        /// <summary>
        /// Google maps settings
        /// </summary>
        JiraSettings JiraSettings { get; }

        CrmSettings CrmSettings { get;  }

        CrmImportSettings CrmImportSettings { get; }

        SharePointSettings SharePointSettings { get; }

        MiscellaneousSettings MiscellaneousSettings { get; }
    }
}
