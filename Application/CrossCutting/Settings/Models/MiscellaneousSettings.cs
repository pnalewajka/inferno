﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("Miscellaneous", IsNullable = false)]
    public class MiscellaneousSettings
    {
        public string MasterPassword { get; set; }
    }
}