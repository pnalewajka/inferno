﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("OneTimeToken", IsNullable = false)]
    public class OneTimeTokenSettings
    {
        /// <summary>
        /// Is provided enabled?
        /// </summary>
        public bool IsEnabled { get; set; }
    }
}