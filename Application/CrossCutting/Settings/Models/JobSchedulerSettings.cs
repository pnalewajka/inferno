﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("JobScheduler", IsNullable = false)]
    public class JobSchedulerSettings
    {
        /// <summary>
        /// Service name part (will be used to construct service name_
        /// </summary>
        public string Name { get; set; }

        public string DisplayName { get; set; }

        /// <summary>
        /// Service description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Maximum number of simultaneous execution pipelines 
        /// </summary>
        public int MaxPipelines { get; set; }

        /// <summary>
        /// List of accepted pipelines
        /// </summary>
        [XmlArrayItem("Pipeline")]
        public string[] Pipelines { get; set; }

        /// <summary>
        /// Specific scheduler name (will be used to construct service name_
        /// </summary>
        public string SchedulerName { get; set; }

        /// <summary>
        /// Job scheduler account
        /// </summary>
        public string UserName { get; set; }
    }
}
