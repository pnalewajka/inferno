﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("CrmImport", IsNullable = true)]
    public class CrmImportSettings
    {
        public string DefaultDanteDataConsentAdministratorCompanyAdCode { get; set; }

        public string DefaultDanteConsentCoordinatorEmployeeEmail { get; set; }

        public string DefaultDanteContactCoordinatorEmployeeEmail { get; set; }

        public string DefaultDanteCandidateEmailAddress { get; set; }

        public string DefaultDanteJobOpeningPositionName { get; set; }

        public string DefaultDanteJobOpeningCompanyAdCode { get; set; }

        public string DefaultDanteOrgUnitCode { get; set; }

        public string DefaultDanteDecisionMakerEmployeeEmail { get; set; }

        public string DefaultDanteOwningUserLogin { get; set; }
    }
}
