﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("OpenExchangeRates", IsNullable = false)]
    public class OpenExchangeRatesSettings
    {
        /// <summary>
        /// Unique AppId
        /// </summary>
        public string AppId { get; set; }
    }
}
