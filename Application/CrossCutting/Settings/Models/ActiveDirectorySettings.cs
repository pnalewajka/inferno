﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("ActiveDirectory", IsNullable = false)]
    public class ActiveDirectorySettings
    {
        /// <summary>
        /// Active mode: user needs to enter login/password and will be authenticated using his credentials from server side
        /// </summary>
        public bool IsActiveModeEnabled { get; set; }

        /// <summary>
        /// NTLM WWW-Authenticate challenge will be used to authenticate user client-side
        /// </summary>
        public bool IsPassiveModeEnabled { get; set; }

        /// <summary>
        /// Active directory connection string
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Active directory domain name
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Active directory groups container
        /// </summary>
        public string GroupsContainer { get; set; }

        /// <summary>
        /// Entering login screen triggers NTLM challenge
        /// </summary>
        public bool AutoLogin { get; set; }

        /// <summary>
        /// User name to use when authenticating the client
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password to use when authenticating the client
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Active directory path
        /// </summary>
        public string Path { get; set; }
    }
}