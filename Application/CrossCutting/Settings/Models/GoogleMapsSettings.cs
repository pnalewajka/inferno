﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("GoogleMaps", IsNullable = true)]
    public class GoogleMapsSettings
    {
        [XmlElement(IsNullable = false)]
        public string ApiKey { get; set; }

        [XmlElement(IsNullable = false)]
        public bool IsSearchEnabled { get; set; }
    }
}