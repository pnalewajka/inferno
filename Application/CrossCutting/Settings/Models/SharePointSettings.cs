﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("SharePoint", IsNullable = false)]
    public class SharePointSettings
    {
        /// <summary>
        /// SharePoint username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// SharePoint password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// RelativePath
        /// </summary>
        public string RelativePath { get; set; }

        /// <summary>
        /// SharePoint URL
        /// </summary>
        public string SharePointUrl { get; set; }
    }
}
