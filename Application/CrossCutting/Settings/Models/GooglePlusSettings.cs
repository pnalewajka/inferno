﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("GooglePlus", IsNullable = false)]
    public class GooglePlusSettings
    {
        /// <summary>
        /// Is provided enabled?
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Client id from API
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Secret from API
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// URL of OAuth endpoint
        /// </summary>
        public string AuthorizationEndpoint { get; set; }

        /// <summary>
        /// Data access scopes
        /// </summary>
        [XmlArrayItem("Scope")]
        public string[] Scopes { get; set; }

        /// <summary>
        /// Redirect url (user will return to this address after interacting with google)
        /// </summary>
        public string RedirectUri { get; set; }

        /// <summary>
        /// URL of user info endpoint
        /// </summary>
        public string UserInfoEndpoint { get; set; }

        /// <summary>
        /// Url of token exchange endpoint (auth token to access_token exchange)
        /// </summary>
        public string TokenExchangeEndpoint { get; set; }
    }
}