﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("Crm", IsNullable = true)]
    public class CrmSettings
    {
        /// <summary>
        /// Crm username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Crm password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Crm url
        /// </summary>
        public string CrmUrl { get; set; }
    }
}
