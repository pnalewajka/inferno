using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("Twitter", IsNullable = false)]
    public class TwitterSettings
    {
        /// <summary>
        /// Is provided enabled?
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Callback url
        /// </summary>
        public string CallbackUrl { get; set; }

        /// <summary>
        /// Secret from API
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// URL of OAuth endpoint
        /// </summary>
        public string AuthorizationEndpoint { get; set; }

        /// <summary>
        /// Consumer key
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Request token endpoint
        /// </summary>
        public string RequestTokenEndpoint { get; set; }
    }
}