﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("Navision", IsNullable = false)]
    public class NavisionSettings
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}