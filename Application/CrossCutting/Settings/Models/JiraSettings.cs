﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("Jira", IsNullable = true)]
    public class JiraSettings
    {
        /// <summary>
        /// Jira username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Jira password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Jira url
        /// </summary>
        public string JiraUrl { get; set; }

    }
}