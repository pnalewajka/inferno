﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("Facebook", IsNullable = false)]
    public class FacebookSettings
    {
        /// <summary>
        /// Is provided enabled?
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Client id from facebook API
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Secret from facebook API
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// URL of OAuth endpoint
        /// </summary>
        public string AuthorizationEndpoint { get; set; }

        public string RedirectUri { get; set; }

        /// <summary>
        /// URL of user info endpoint
        /// </summary>
        public string UserInfoEndpoint { get; set; }

        /// <summary>
        /// Url of token exchange endpoint (auth token to access_token exchange)
        /// </summary>
        public string TokenExchangeEndpoint { get; set; }

        /// <summary>
        /// Data access scopes
        /// </summary>
        [XmlArrayItem("Scope")]
        public string[] Scopes { get; set; }

        /// <summary>
        /// Token debug endpoint
        /// </summary>
        public string DebugTokenEndpoint { get; set; }
    }
}