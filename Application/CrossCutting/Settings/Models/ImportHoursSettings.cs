﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("ImportHours", IsNullable = false)]
    public class ImportHoursSettings
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}