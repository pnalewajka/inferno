﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("AuthorizationProviders", IsNullable = true)]
    public class AuthorizationProviderSettings
    {
        [XmlElement(IsNullable = false)]
        public GooglePlusSettings GooglePlusSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public TwitterSettings TwitterSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public ActiveDirectorySettings ActiveDirectorySettings { get; set; }

        [XmlElement(IsNullable = false)]
        public ActiveDirectoryWriterSettings ActiveDirectoryWriterSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public JiraWriterSettings JiraWriterSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public NavisionSettings NavisionSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public ImportHoursSettings ImportHoursSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public MiscellaneousSettings MiscellaneousSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public OneTimeTokenSettings OneTimeTokenSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public OpenExchangeRatesSettings OpenExchangeRatesSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public FacebookSettings FacebookSettings { get; set; }

        [XmlElement(IsNullable = false)]
        public bool IsAtomicAuthenticationEnabled { get; set; }

        [XmlElement(IsNullable = false)]
        public RecruitmentExchangeCredentialSettings RecruitmentExchangeCredentialSettings { get; set; }
    }
}