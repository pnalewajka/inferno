﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("JiraWriterSettings", IsNullable = false)]
    public class JiraWriterSettings
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}