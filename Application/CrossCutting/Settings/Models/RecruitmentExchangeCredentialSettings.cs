﻿using System;
using System.Xml.Serialization;

namespace Smt.Atomic.CrossCutting.Settings.Models
{
    [Serializable]
    [XmlRoot("RecruitmentExchangeCredentialSettings", IsNullable = false)]
    public class RecruitmentExchangeCredentialSettings
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}