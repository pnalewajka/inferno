﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Settings.Configuration;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.CrossCutting.Settings
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                case ContainerType.WebApi:
                case ContainerType.WebServices:
                    container.Register(
                        Component.For<ISettingsProvider>().ImplementedBy<SettingsProvider>().LifestyleSingleton());
                    break;

                case ContainerType.JobScheduler:
                    container.Register(
                        Component.For<ISettingsProvider>().ImplementedBy<SettingsProvider>().LifestylePerThread()
                        );
                    break;
            }
        }
    }
}