﻿using NCrontab;

namespace Smt.Atomic.CrossCutting.Common.SemanticTypes
{
    public class CronExpression : SemanticType<string>
    {
        private static readonly CrontabSchedule.ParseOptions ParseOptions = new CrontabSchedule.ParseOptions { IncludingSeconds = true };

        private CrontabSchedule _schedule;

        public CrontabSchedule Schedule => _schedule ?? (_schedule = CrontabSchedule.Parse(Value, ParseOptions));

        public CronExpression()
            : base(IsValid)
        {
        }

        public CronExpression(string cronExpression) : base(IsValid, cronExpression)
        {
        }

        public static bool IsValid(string value)
        {
            return CrontabSchedule.TryParse(value, ParseOptions) != null;
        }
    }
}
