﻿using System;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.SemanticTypes
{
    /// <summary>
    /// Class representing clr namespace
    /// </summary>
    [Serializable]
    public class Namespace : SemanticType<string>
    {
        public Namespace()
            : base(IsValid)
        {
        }

        public Namespace(string value)
            : base(IsValid, value)
        {
        }

        public static bool IsValid(string value)
        {
            return ReflectionHelper.FindNamespaceInLoadedAssemblies(value);
        }
    }
}
