﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.SemanticTypes
{
    /// <summary>
    /// Implementation for base of semantic type
    /// </summary>
    /// <typeparam name="TUnderlyingType"></typeparam>
    public abstract class SemanticType<TUnderlyingType> : IEquatable<SemanticType<TUnderlyingType>>,
        IComparable<SemanticType<TUnderlyingType>> where TUnderlyingType : IComparable<TUnderlyingType>
    {
        private readonly Func<TUnderlyingType, bool> _isValidPredicate;

        protected SemanticType(Func<TUnderlyingType, bool> isValidPredicate)
        {
            _isValidPredicate = isValidPredicate;
        }

        protected SemanticType(Func<TUnderlyingType, bool> isValidPredicate, TUnderlyingType value)
            : this(isValidPredicate)
        {
            if (_isValidPredicate != null && !_isValidPredicate(value))
            {
                throw new ArgumentException($"Trying to set a {GetType()} to {value} which is invalid");
            }

            Value = value;
        }

        private TUnderlyingType _value;

        /// <summary>
        /// Get value of semantic type
        /// </summary>
        public TUnderlyingType Value
        {
            get { return _value; }
            set
            {
                if (!EqualityComparer<TUnderlyingType>.Default.Equals(value, default(TUnderlyingType)) 
                    && _isValidPredicate != null && !_isValidPredicate(value))
                {
                    throw new ArgumentException($"Trying to set a {GetType()} to {value} which is invalid");
                }
                _value = value;
            }
        }

        /// <summary>
        /// ICompareable implementation
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(SemanticType<TUnderlyingType> other)
        {
            return other == null ? 1 : Value.CompareTo(other.Value);
        }

        /// <summary>
        /// IEquatable implementation
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SemanticType<TUnderlyingType> other)
        {
            return other != null && (Value.Equals(other.Value));
        }

        /// <summary>
        /// Overriden equals implementation to match underlaying value
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            return (Value.Equals(((SemanticType<TUnderlyingType>) obj).Value));
        }

        /// <summary>
        /// Overridden GetHashCode to match underlaying value
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        /// <summary>
        /// == operator implementation
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(SemanticType<TUnderlyingType> a, SemanticType<TUnderlyingType> b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if (((object) a == null) || ((object) b == null))
            {
                return false;
            }

            return a.Equals(b);
        }

        /// <summary>
        /// != operator implementation
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(SemanticType<TUnderlyingType> a, SemanticType<TUnderlyingType> b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Overridden ToString implementation to match underlaying value
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Value.ToString();
        }
    }
}