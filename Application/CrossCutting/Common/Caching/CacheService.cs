﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public class CacheService : ICacheService, ICacheInvalidationService
    {
        private const string WindsorInterceptorCacheName = "WindsorInterceptorCache";

        private static IDictionary<string, CacheAreaDescription> _cacheAreas = new ConcurrentDictionary<string, CacheAreaDescription>();
        private static ObjectCache _cache = new MemoryCache(WindsorInterceptorCacheName); // thread-safe

        public static int MaxInvalidationCheckIntervalInSeconds { get; set; }

        private readonly ICacheInvalidationNotifications _cacheInvalidationNotifications;
        private readonly static object Lock = new object();

        public CacheService(ICacheInvalidationNotifications cacheInvalidationNotifications)
        {
            _cacheInvalidationNotifications = cacheInvalidationNotifications;
            MaxInvalidationCheckIntervalInSeconds = 10;
        }

        public bool Contains(CacheKey cacheKey)
        {
            return _cache.Contains(cacheKey.ToString());
        }

        public object Get(CacheKey cacheKey)
        {
            InvalidateAreaIfNeeded(cacheKey.Area);

            return _cache.Get(cacheKey.ToString());
        }

        private void InvalidateAreaIfNeeded(string area)
        {
            if (!_cacheAreas.ContainsKey(area))
            {
                _cacheAreas[area] = new CacheAreaDescription();
            }

            var cacheAreaDescription = _cacheAreas[area];

            if (cacheAreaDescription.LastCheckForNotificationOn.AddSeconds(MaxInvalidationCheckIntervalInSeconds) < DateTime.Now)
            {
                var lastInvalidationToken = _cacheInvalidationNotifications.GetLastInvalidationToken(area);
                cacheAreaDescription.LastCheckForNotificationOn = DateTime.Now;

                if (lastInvalidationToken != cacheAreaDescription.LastKnownInvalidationToken)
                {
                    cacheAreaDescription.LastKnownInvalidationToken = lastInvalidationToken;
                    InvalidateArea(area, false);
                }
            }
        }

        public void Add(CacheKey cacheKey, object value)
        {
            var key = cacheKey.ToString();
            AddToArea(cacheKey.Area, key);
            _cache.Add(key, value, new CacheItemPolicy());
        }

        public void Add(CacheKey cacheKey, object value, int expirationInSeconds)
        {
            var key = cacheKey.ToString();
            AddToArea(cacheKey.Area, key);

            var timeSpan = new TimeSpan(0, 0, expirationInSeconds);
            var dateTime = DateTime.Now.Add(timeSpan);
            var dateTimeOffset = new DateTimeOffset(dateTime);
            _cache.Add(key, value, dateTimeOffset);
        }

        public void Invalidate()
        {
            lock (Lock)
            {
                _cacheAreas = new ConcurrentDictionary<string, CacheAreaDescription>();
                _cache = new MemoryCache(WindsorInterceptorCacheName);
            }
        }

        public void Invalidate(CacheKey cacheKey)
        {
            _cache.Remove(cacheKey.ToString());
        }

        public void InvalidateArea(string cacheArea)
        {
            InvalidateArea(cacheArea, true);
        }

        private void InvalidateArea(string cacheArea, bool notifyOtherInstances)
        {
            if (notifyOtherInstances)
            {
                _cacheInvalidationNotifications.NotifyAboutAreaInvalidation(cacheArea);
            }

            lock (Lock)
            {
                if (!_cacheAreas.ContainsKey(cacheArea))
                {
                    return;
                }

                foreach (var cacheKey in _cacheAreas[cacheArea].CachedKeys)
                {
                    _cache.Remove(cacheKey);
                }
            }
        }

        private void AddToArea(string cacheArea, string key)
        {
            lock (Lock)
            {
                if (!_cacheAreas.ContainsKey(cacheArea))
                {
                    _cacheAreas[cacheArea] = new CacheAreaDescription();
                }

                _cacheAreas[cacheArea].CachedKeys.Add(key);
            }
        }
    }
}
