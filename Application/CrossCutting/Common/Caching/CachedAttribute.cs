﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CachedAttribute : Attribute
    {
        public string CacheArea { get; set; }
        public int ExpirationInSeconds { get; set; }

        /// <summary>
        /// True if the value calculation should be done in background and exception should be thrown on call when no cached value available
        /// </summary>
        public bool ShouldProcessInBackground { get; set; }

        public CachedAttribute()
        {
            CacheArea = string.Empty;
            ShouldProcessInBackground = false;
        }
    }
}
