﻿namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public interface ICacheInvalidationService
    {
        void Invalidate();
        void Invalidate(CacheKey cacheKey);
        void InvalidateArea(string cacheArea);
    }
}