namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public interface ICacheService
    {
        bool Contains(CacheKey cacheKey);
        object Get(CacheKey cacheKey);
        void Add(CacheKey cacheKey, object value);
        void Add(CacheKey cacheKey, object value, int expirationInSeconds);
    }
}