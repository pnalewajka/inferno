﻿namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public static class CacheAreas
    {
        public const string FeaturePermissions = "FeaturePermissions";
        public const string DataPermissions = "DataPermissions";
        public const string SystemParameters = "SystemParameters";
        public const string SystemParametersContext = "SystemParametersContext";
        public const string Calendars = "Calendars";
        public const string ReportDefinitions = "ReportDefinitions";
        public const string DefaultOrgUnits = "DefaultOrgUnits";
        public const string ManagerOrgUnitIds = "ManagerOrgUnitIds";
        public const string HasEmployees = "HasEmployees";
        public const string HasProjects = "HasProjects";
        public const string OrgUnits = "OrgUnits";
        public const string OrgChart = "OrgChart";
        public const string ContractTypes = "ContractTypes";
        public const string JobTitles = "JobTitles";
        public const string JobProfiles = "JobProfiles";
        public const string Locations = "Locations";
        public const string Companies = "Companies";
        public const string Projects = "Projects";
        public const string Users = "Users";
        public const string NavisionData = "NavisionData";
        public const string JiraProjects = "JiraProjects";
        public const string EmployeeDeductibleCost = "EmployeeDeductibleCost";
        public const string Currencies = "Currencies";
        public const string Absences = "Absences";
    }
}