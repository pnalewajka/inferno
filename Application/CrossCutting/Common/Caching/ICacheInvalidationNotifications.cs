﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public interface ICacheInvalidationNotifications
    {
        Guid GetLastInvalidationToken(string area);
        void NotifyAboutAreaInvalidation(string area);
    }
}