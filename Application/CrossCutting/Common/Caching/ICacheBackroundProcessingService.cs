using System;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public interface ICacheBackroundProcessingService
    {
        void EnqueueProcessing(Type serviceContract, string method, object[] arguments, CacheKey cacheKey);
        void StartProcessing(Guid token);
    }
}