﻿using System.Collections.Concurrent;
using System.Reflection;
using Castle.DynamicProxy;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public class CacheInterceptor : IInterceptor
    {
        private readonly ICacheService _cacheService;
        private readonly ICacheBackroundProcessingService _cacheBackroundProcessingService;
        private readonly ConcurrentDictionary<MethodInfo, MethodCacheSettings> _scannedMethods = new ConcurrentDictionary<MethodInfo, MethodCacheSettings>();

        public CacheInterceptor(ICacheService cacheService, ICacheBackroundProcessingService cacheBackroundProcessingService)
        {
            _cacheService = cacheService;
            _cacheBackroundProcessingService = cacheBackroundProcessingService;
        }

        public void Intercept(IInvocation invocation)
        {
            var cacheability = _scannedMethods.GetOrAdd(invocation.MethodInvocationTarget, ScanMethod);

            if (cacheability.IsCached)
            {
                var cacheKey = new CacheKey(cacheability.Area, invocation.Method, invocation.Arguments);
                var returnValue = _cacheService.Get(cacheKey);

                if (returnValue == null)
                {
                    if (cacheability.ShouldProcessInBackground)
                    {
                        _cacheBackroundProcessingService.EnqueueProcessing(invocation.TargetType, invocation.Method.Name, invocation.Arguments, cacheKey);

                        throw new CachedValueNotAvailableException();
                    }

                    invocation.Proceed();

                    var invocationReturnValue = invocation.ReturnValue;
                    if (invocationReturnValue == null) return;

                    if (cacheability.ExpirationTimeInSeconds > 0)
                    {
                        _cacheService.Add(cacheKey, invocationReturnValue, cacheability.ExpirationTimeInSeconds);
                    }
                    else
                    {
                        _cacheService.Add(cacheKey, invocationReturnValue);
                    }
                }
                else
                {
                    invocation.ReturnValue = returnValue;
                }
            }
            else
            {
                invocation.Proceed();
            }
        }

        private static MethodCacheSettings ScanMethod(MethodInfo methodInfo)
        {
            var cacheAttributes = methodInfo.GetCustomAttributes(typeof(CachedAttribute), true);
            
            if (cacheAttributes.Length > 0)
            {
                var cachedAttribute = (CachedAttribute)cacheAttributes[0];
                return new MethodCacheSettings
                {
                    IsCached = true,
                    Area = cachedAttribute.CacheArea,
                    ExpirationTimeInSeconds = cachedAttribute.ExpirationInSeconds,
                    ShouldProcessInBackground = cachedAttribute.ShouldProcessInBackground
                };
            }
            
            return new MethodCacheSettings
            {
                IsCached = false,
            };
        }

        private struct MethodCacheSettings
        {
            public bool IsCached;
            public string Area;
            public int ExpirationTimeInSeconds;
            public bool ShouldProcessInBackground { get; set; }
        }
    }
}
