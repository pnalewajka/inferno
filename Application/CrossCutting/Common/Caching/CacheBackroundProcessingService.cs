using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Hosting;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public partial class CacheBackroundProcessingService : ICacheBackroundProcessingService
    {
        private const int WebRequestTimeout = 600;
        private const int ProcessingTimeout = 600;

        private readonly ICacheService _cacheService;

        private static readonly IDictionary<Guid, BackgroundProcessingDetails> BackgroundProcessing = new Dictionary<Guid, BackgroundProcessingDetails>();

        public CacheBackroundProcessingService(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        public void EnqueueProcessing(Type serviceContract, string method, object[] arguments, CacheKey cacheKey)
        {
            var existingProcessing = 
                BackgroundProcessing
                .Values
                .OrderByDescending(p => p.StartTime)
                .FirstOrDefault(p => p.CacheKey == cacheKey && p.Method == method && p.ServiceContract == serviceContract);

            if (existingProcessing!= null && (DateTime.Now - existingProcessing.StartTime).TotalSeconds <= ProcessingTimeout)
            {
                return;
            }

            var token = Guid.NewGuid();

            BackgroundProcessing[token] = new BackgroundProcessingDetails(serviceContract, method, arguments, cacheKey);
            var url = CalculateProcessingRequestUrl(token);

            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken =>
            {
                var webRequest = WebRequest.Create(url);
                webRequest.Timeout = WebRequestTimeout;
                webRequest.GetResponse().GetResponseStream();
            });
        }

        private static string CalculateProcessingRequestUrl(Guid token)
        {
            var uriBuilder = new UriBuilder(System.Web.HttpContext.Current.Request.Url)
            {
                Path = "/CacheBackgroundProcessing/StartProcessing",
                Query = $"token={token}"
            };
            var url = uriBuilder.ToString();

            return url;
        }

        public void StartProcessing(Guid token)
        {
            if (BackgroundProcessing[token].IsProcessingStarted)
            {
                throw new InvalidOperationException("Cache background processing for a given token already started");
            }

            BackgroundProcessing[token].ProcessAndCache(_cacheService);
        }
    }
}