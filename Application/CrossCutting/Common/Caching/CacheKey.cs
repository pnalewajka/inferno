using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public class CacheKey
    {
        private readonly string _area;
        private readonly MethodInfo _calledMethod;
        private readonly object[] _arguments;

        // types, for which ToString() returns unique value
        private static readonly Type[] TypeWhitelist =
        {
            typeof(Boolean),
            typeof(SByte),
            typeof(Int16),
            typeof(Int32),
            typeof(Int64),
            typeof(Byte),
            typeof(UInt16),
            typeof(UInt32),
            typeof(UInt64),
            typeof(Single),
            typeof(Double),
            typeof(Decimal),
            typeof(String),
            typeof(Guid),
            typeof(DateTime),
            typeof(IPAddress)
        };

        public string Area => _area;

        public CacheKey(string area, MethodInfo calledMethod, object[] arguments)
        {
            _area = area;
            _calledMethod = calledMethod;
            _arguments = arguments;
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder(80 + 24 * _arguments.Length);

            if (_calledMethod.DeclaringType == null)
            {
                throw new InvalidDataException();
            }

            stringBuilder
                .Append(_calledMethod.DeclaringType.FullName)
                .Append('.')
                .Append(_calledMethod.Name)
                .Append(',');

            foreach (var argument in _arguments)
            {
                if (argument == null)
                {
                    stringBuilder.Append("null,");
                }
                else
                {
                    var argumentType = argument.GetType();
                    var isSerializable = AttributeHelper.GetClassAttribute<JsonObjectAttribute>(argument) != null;

                    if (!isSerializable && !(TypeWhitelist.Contains(argumentType) || argumentType.IsEnum))
                    {
                        string message = $"Type {argumentType.Name} cannot be used as argument for cached method";
                        throw new NotSupportedException(message);
                    }

                    var argumentString = isSerializable
                        ? JsonConvert.SerializeObject(argument, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:00:00" })
                        : argument.ToString();

                    stringBuilder
                        .Append(argumentType.FullName)
                        .Append('@')
                        .Append(argumentString.Length)
                        .Append(':')
                        .Append(argumentString)
                        .Append(',');
                }
            }

            return stringBuilder.ToString();
        }
    }
}