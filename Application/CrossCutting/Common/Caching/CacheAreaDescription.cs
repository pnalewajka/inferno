using System;
using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public class CacheAreaDescription
    {
        public HashSet<string> CachedKeys { get; private set; }
        
        /// <summary>
        /// Date when we last checked for the new invalidation notifiaction was made 
        /// </summary>
        public DateTime LastCheckForNotificationOn { get; set; }
        
        public Guid LastKnownInvalidationToken { get; set; }

        public CacheAreaDescription()
        {
            CachedKeys = new HashSet<string>();
            LastCheckForNotificationOn = DateTime.MinValue;
            LastKnownInvalidationToken = Guid.Empty;
        }
    }
}