using System;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Caching
{
    public partial class CacheBackroundProcessingService
    {
        private class BackgroundProcessingDetails
        {
            public BackgroundProcessingDetails(Type serviceContract, string method, object[] arguments, CacheKey cacheKey)
            {
                ServiceContract = serviceContract;
                Method = method;
                Arguments = arguments;
                CacheKey = cacheKey;
                StartTime = DateTime.Now;
            }

            private object[] Arguments { get; }
            public Type ServiceContract { get; }
            public string Method { get; }
            public CacheKey CacheKey { get; }
            public DateTime StartTime { get; }
            public bool IsProcessingStarted { get; private set; }

            public void ProcessAndCache(ICacheService cacheService)
            {
                IsProcessingStarted = true;

                var service = ReflectionHelper.CreateInstanceWithIocDependencies(ServiceContract);
                var serviceType = service.GetType();
                var result = serviceType.GetMethod(Method).Invoke(service, Arguments);

                cacheService.Add(CacheKey, result);
            }
        }
    }
}