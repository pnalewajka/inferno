﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Visitors;

namespace Smt.Atomic.CrossCutting.Common.Abstacts
{
    /// <summary>
    /// Class to class mappings
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TDestination"></typeparam>
    public abstract class ClassMapping<TSource, TDestination> : IClassMapping<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        private IDictionary<string, List<string>> _fieldMappings;
        private Func<TSource, TDestination> _compiledMapping;

        public Expression<Func<TSource, TDestination>> Mapping { get; protected set; }

        public virtual IEnumerable<string> NavigationProperties => new Lazy<IEnumerable<string>>(GetNavigationProperties()).Value;

        private Func<IEnumerable<string>> GetNavigationProperties()
        {
            return () =>
            {
                var visitor = new IncludeVisitor();
                visitor.VisitExpression(Mapping);

                return visitor.Includes;
            };
        }

        public virtual IDictionary<string, List<string>> GetFieldMappings(Type sourceType, Type destinationType)
        {
            if (_fieldMappings == null)
            {
                var visitor = new PropertySearchVisitor(destinationType);
                visitor.VisitExpression(Mapping);
                _fieldMappings = visitor.RelatedProperties;
            }

            return _fieldMappings;
        }

        public object CreateFromSource(object o)
        {
            return CreateFromSourceWithAssuredContext((TSource)o, null);
        }

        public object CreateFromSource(object o, CreationContext context)
        {
            return CreateFromSourceWithAssuredContext((TSource)o, context);
        }

        public virtual TDestination CreateFromSource(TSource o)
        {
            return CreateFromSourceWithAssuredContext(o, null);
        }

        public IEnumerable<TDestination> CreateFromSource(IEnumerable<TSource> sourceObjects)
        {
            return CreateFromSource(sourceObjects, null);
        }

        public IEnumerable<TDestination> CreateFromSource(IEnumerable<TSource> sourceObjects, CreationContext context)
        {
            foreach (var sourceObject in sourceObjects)
            {
                yield return CreateFromSourceWithAssuredContext(sourceObject, context);
            }
        }

        private TDestination CreateFromSourceWithAssuredContext(TSource sourceObject, CreationContext context)
        {
            if (context == null)
            {
                context = new CreationContext() { RetrievalScenario = RetrievalScenario.MultipleRecords };
            }

            return CreateFromSource(sourceObject, context);
        }

        /// <summary>
        /// Create new TDestination object and fill it with data according to Mappings
        /// </summary>
        /// <param name="sourceObject"></param>
        /// <param name="scenario"></param>
        /// <returns></returns>
        public virtual TDestination CreateFromSource(TSource sourceObject, CreationContext context)
        {
            if (_compiledMapping == null)
            {
                _compiledMapping = Mapping.Compile();
            }

            return _compiledMapping.Invoke(sourceObject);
        }

        /// <summary>
        /// Visitor implementation for generating necessary includes
        /// </summary>
        private class IncludeVisitor : AtomicExpressionVisitor
        {
            private readonly IDictionary<Expression, string> _mappings;
            private string _currentMapping;
            public IEnumerable<string> Includes { get; private set; }

            public IncludeVisitor()
            {
                _mappings = new Dictionary<Expression, string>();
                _currentMapping = "";
                Includes = new List<string>();
            }

            protected override Expression VisitLambda(LambdaExpression lambda)
            {
                var newMappings = lambda.Parameters.ToDictionary(p => p, p => _currentMapping);

                foreach (var mapping in newMappings)
                {
                    _mappings[mapping.Key] = mapping.Value;
                }

                return base.VisitLambda(lambda);
            }

            protected override Expression VisitMemberAccess(MemberExpression memberExpression)
            {
                var baseResult = base.VisitMemberAccess(memberExpression);

                string prefixMapping;

                if (memberExpression.Expression == null || !_mappings.TryGetValue(memberExpression.Expression, out prefixMapping))
                {
                    prefixMapping = "";
                }

                _currentMapping = prefixMapping + $".{memberExpression.Member.Name}";
                _mappings[memberExpression] = _currentMapping;
                GenerateIncludeIfNeeded(memberExpression);

                return baseResult;
            }

            private void GenerateIncludeIfNeeded(MemberExpression memberExpression)
            {
                var type = memberExpression.Type;

                if (type.IsGenericType
                    && type.GetGenericTypeDefinition() == typeof(ICollection<>))
                {
                    Includes = Includes.Union(new[] { _currentMapping.Trim('.') });
                }
            }

            public void VisitExpression(Expression expressionToVisit)
            {
                Visit(expressionToVisit);
            }
        }

        /// <summary>
        /// visitor implementation for searching of property names used in member initializer lambda
        /// </summary>
        private class PropertySearchVisitor : AtomicExpressionVisitor
        {
            private string _currentMember;
            private string _currentItem;
            private readonly Type _destinationType;

            public PropertySearchVisitor(Type destinationType)
            {
                _destinationType = destinationType;
                RelatedProperties = new Dictionary<string, List<string>>();
            }

            public IDictionary<string, List<string>> RelatedProperties { get; private set; }

            protected override MemberBinding VisitBinding(MemberBinding binding)
            {
                var oldMember = _currentMember;

                _currentItem = GetMemberName(binding.Member);
                _currentMember = _currentItem;

                if (!RelatedProperties.ContainsKey(_currentItem))
                {
                    RelatedProperties.Add(_currentItem, new List<string>());
                }

                var baseBinding = base.VisitBinding(binding);

                _currentMember = oldMember;

                return baseBinding;
            }

            private string GetMemberName(MemberInfo memberInfo)
            {
                var name = memberInfo.Name;
                var isMemberOfComplexType = memberInfo.ReflectedType != null
                                            && memberInfo.ReflectedType != _destinationType
                                            && memberInfo.ReflectedType != _destinationType.BaseType
                                            && !memberInfo.ReflectedType.IsAbstract
                                            && memberInfo.ReflectedType.FullName != null
                                            && !memberInfo.ReflectedType.FullName.StartsWith("System.");

                if (isMemberOfComplexType)
                {
                    return PropertyHelper.CombinePropertyPath(_currentMember, name);
                }

                _currentMember = name;

                return name;
            }

            protected override Expression VisitMemberAccess(MemberExpression memberExpression)
            {
                if (_currentMember != null)
                {
                    RelatedProperties[_currentMember].Add(memberExpression.Member.Name);
                    var shouldBeAddedToSubMember = _currentMember != _currentItem && RelatedProperties.ContainsKey(_currentItem);

                    if (shouldBeAddedToSubMember)
                    {
                        RelatedProperties[_currentItem].Add(memberExpression.Member.Name);
                    }
                }

                return base.VisitMemberAccess(memberExpression);
            }

            protected override Expression VisitConditionForConditional(ConditionalExpression c)
            {
                var result = base.VisitConditionForConditional(c);
                ClearPath();

                return result;
            }

            public override Expression VisitTruePathForConditional(ConditionalExpression c)
            {
                var result = base.VisitTruePathForConditional(c);

                if (IsStringEmptyExpression(c.IfTrue))
                {
                    ClearPath();
                }

                return result;
            }

            protected override Expression VisitFalsePathForConditional(ConditionalExpression c)
            {
                var result = base.VisitFalsePathForConditional(c);

                if (IsStringEmptyExpression(c.IfFalse))
                {
                    ClearPath();
                }

                return result;
            }

            private void ClearPath()
            {
                RelatedProperties[_currentMember].Clear();
            }

            private static bool IsStringEmptyExpression(Expression expression)
            {
                var memberExpression = expression as MemberExpression;

                return memberExpression != null
                       && memberExpression.Member.Name == "Empty"
                       && memberExpression.Member.ReflectedType == typeof(string);
            }

            public void VisitExpression(Expression expressionToVisit)
            {
                Visit(expressionToVisit);
            }
        }
    }
}