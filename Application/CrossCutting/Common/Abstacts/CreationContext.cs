﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Abstacts
{
    public class CreationContext
    {
        public object Context { get; set; }
        public RetrievalScenario RetrievalScenario { get; set; }

        public CreationContext()
        {
        }

        public CreationContext(RetrievalScenario retrievalScenario)
        {
            RetrievalScenario = retrievalScenario;
        }

        public CreationContext(RetrievalScenario retrievalScenario, object retrievalContext)
        {
            RetrievalScenario = retrievalScenario;
            Context = retrievalContext;
        }
    }
}
