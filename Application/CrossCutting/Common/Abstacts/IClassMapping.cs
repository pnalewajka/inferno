using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Smt.Atomic.CrossCutting.Common.Abstacts
{
    public interface IClassMapping
    {
        /// <summary>
        /// Gets mapping dictionary describing field dependencies between source and destination
        /// </summary>
        /// <param name="sourceType">Source type</param>
        /// <param name="destinationType">Destination type</param>
        /// <returns>Mapping dictionary: name of the source field (key) and the list of the dependent destination fields (value)</returns>
        IDictionary<string, List<string>> GetFieldMappings(Type sourceType, Type destinationType);

        /// <summary>
        /// Creates destination record based on the source record
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        object CreateFromSource(object o);

        /// <summary>
        /// Creates destination record based on the source record
        /// </summary>
        /// <param name="o"></param>
        /// <param name="scenario"></param>
        /// <returns></returns>
        object CreateFromSource(object o, CreationContext context);
    }

    public interface IClassMapping<TSource, TDestination> : IClassMapping
        where TSource : class
        where TDestination : class
    {
        Expression<Func<TSource, TDestination>> Mapping { get; }

        TDestination CreateFromSource(TSource sourceObject);
        TDestination CreateFromSource(TSource sourceObject, CreationContext context);

        IEnumerable<TDestination> CreateFromSource(IEnumerable<TSource> sourceObjects);
        IEnumerable<TDestination> CreateFromSource(IEnumerable<TSource> sourceObjects, CreationContext context);

        /// <summary>
        /// Gets mappings for all nested navigation properties to include them in eager loading
        /// </summary>
        /// <returns>Collection of strings representing all nested navigation properties</returns>
        IEnumerable<string> NavigationProperties { get; }
    }
}