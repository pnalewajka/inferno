﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Comparers
{
    /// <summary>
    /// Compares two objects by comparing their keys
    /// </summary>
    /// <typeparam name="T">Values's type</typeparam>
    /// <typeparam name="TKey">Key's type</typeparam>
    public sealed class ByKeyEqualityComparer<T, TKey> : IEqualityComparer<T>
    {
        private readonly Func<T, TKey> _keySelector;
        private readonly IEqualityComparer<TKey> _keyEqualityComparer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keySelector">Delegate extracting the key of value</param>
        /// <param name="keyEqualityComparer">Optional key's equality comparer</param>
        public ByKeyEqualityComparer([NotNull] Func<T, TKey> keySelector, IEqualityComparer<TKey> keyEqualityComparer = null)
        {
            if (keySelector == null)
            {
                throw new ArgumentNullException(nameof(keySelector));
            }

            _keySelector = keySelector;
            _keyEqualityComparer = keyEqualityComparer ?? EqualityComparer<TKey>.Default;
        }

        /// <summary>
        /// Compares two values
        /// </summary>
        /// <param name="x">Value to compare</param>
        /// <param name="y">Value to compare</param>
        /// <returns>True if both values have equals keys. False otherwise.</returns>
        public bool Equals(T x, T y)
        {
            var xKey = _keySelector(x);
            var yKey = _keySelector(y);

            return _keyEqualityComparer.Equals(xKey, yKey);
        }

        /// <summary>
        /// Gets value's hash code
        /// </summary>
        /// <param name="obj">Value to extract hash from.</param>
        /// <returns>Hash code of value's key</returns>
        public int GetHashCode(T obj)
        {
            var key = _keySelector(obj);

            return _keyEqualityComparer.GetHashCode(key);
        }
    }
}
