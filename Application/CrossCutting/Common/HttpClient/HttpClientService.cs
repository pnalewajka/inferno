﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Flurl;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;

namespace Smt.Atomic.CrossCutting.Common.HttpClient
{
    internal class HttpClientService : IHttpClientService
    {
        public HttpResponseData RetrieveHtmlPage(string url, IDictionary<string, string> headers, IDictionary<string, string> getData, IDictionary<string, string> postData)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                using (var requestMessage = new HttpRequestMessage())
                {
                    var flUrl = new Url(url);

                    if (getData != null)
                    {
                        flUrl.SetQueryParams(getData);
                    }

                    requestMessage.RequestUri = new Uri(flUrl.ToString());

                    if (headers != null)
                    {
                        foreach (var header in headers)
                        {
                            requestMessage.Headers.Add(header.Key, header.Value);
                        }
                    }

                    if (postData != null)
                    {
                        requestMessage.Method = new HttpMethod("POST");
                        requestMessage.Content = new FormUrlEncodedContent(postData);
                    }

                    using (var response = client.SendAsync(requestMessage).Result)
                    {
                        var responseData = new HttpResponseData
                        {
                            ContentType = response.Content.Headers.ContentType.MediaType,
                            Body = response.Content.ReadAsStringAsync().Result,
                            Headers = response.Content.Headers.ToDictionary(h => h.Key, h => h.Value.FirstOrDefault()),
                            StatusCode = response.StatusCode
                        };

                        return responseData;
                    }
                }
            }
        }
    }
}