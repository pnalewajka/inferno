using System.Collections.Generic;
using System.Net;

namespace Smt.Atomic.CrossCutting.Common.Services
{
    public class HttpResponseData
    {
        public string ContentType { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string Body { get; set; }
        public IDictionary<string, string> Headers { get; set; }
    }
}