﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.HttpClient;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.CrossCutting.Common
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<CacheInterceptor>().LifestyleTransient().Named("Cache"));
            container.Register(Component.For<ICacheService, ICacheInvalidationService>().ImplementedBy<CacheService>().LifestyleTransient());
            container.Register(Component.For<ICacheBackroundProcessingService>().ImplementedBy<CacheBackroundProcessingService>().LifestyleTransient());
            container.Register(Component.For<IDnsService>().ImplementedBy<DnsService>().LifestyleTransient());
            container.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleSingleton());
            container.Register(Component.For<IHttpClientService>().ImplementedBy<HttpClientService>().LifestyleTransient());
            container.Register(Component.For<ITextExtractionService>().ImplementedBy<TextExtractionService>().LifestyleTransient());
            container.Register(Classes.FromThisAssembly().BasedOn<IUrlTokenResolver>().WithServiceFromInterface().LifestyleTransient());
        }
    }
}