﻿namespace Smt.Atomic.CrossCutting.Common.Results
{
    /// <summary>
    /// Boolean result with state (as object)
    /// </summary>
    /// <typeparam name="TObject"></typeparam>
    public class BoolResultWithState<TObject> 
    {
        /// <summary>
        /// Result state
        /// </summary>
        public TObject State { get; private set; }

        /// <summary>
        /// Result value
        /// </summary>
        public bool Result { get; private set; }

        public BoolResultWithState(bool result, TObject state)
        {
            Result = result;
            State = state;
        }

        public static implicit operator bool(BoolResultWithState<TObject> boolResult)
        {
            return boolResult != null && boolResult.Result;
        }
    }
}
