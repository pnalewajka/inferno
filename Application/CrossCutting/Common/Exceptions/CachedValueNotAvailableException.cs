using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class CachedValueNotAvailableException : BusinessException
    {
        public CachedValueNotAvailableException()
            : base(LocalizedExceptionResources.CachedValueNotAvailable)
        {
        }
    }
}