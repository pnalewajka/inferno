﻿using System;
using System.Runtime.Serialization;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception for handling lack of authorization
    /// </summary>
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException()
        {
        }

        public UnauthorizedException(string message) : base(message)
        {
        }

        public UnauthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnauthorizedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
