﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception for handling lack of authorization to use specific filter
    /// </summary>
    public class UnauthorizedFilterUsageException : UnauthorizedException
    {
        protected UnauthorizedFilterUsageException()
        {
        }

        public UnauthorizedFilterUsageException(string filterCode, string missingRole)
            : base(BuildMessage(filterCode, missingRole))
        {
        }

        private static string BuildMessage(string filterCode, string missingRole)
        {
            var exceptionFormat = LocalizedExceptionResources.UnauthorizedFilterUsageExceptionFormat;

            return string.Format(exceptionFormat, filterCode, missingRole);
        }
    }
}
