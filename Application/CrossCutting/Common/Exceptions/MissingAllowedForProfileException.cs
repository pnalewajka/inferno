﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class MissingAllowedForProfileException : BusinessException
    {
        public MissingAllowedForProfileException() : base(LocalizedExceptionResources.MissingProfileException)
        {
        }
    }
}
