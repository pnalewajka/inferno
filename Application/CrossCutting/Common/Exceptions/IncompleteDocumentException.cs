﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class IncompleteDocumentException : BusinessException
    {
        protected IncompleteDocumentException()
        {
        }

        public IncompleteDocumentException(string documentName) : base(BuildMessage(documentName))
        {
        }

        private static string BuildMessage(string documentName)
        {
            var exceptionFormat = LocalizedExceptionResources.IncompleteDocumentExceptionFormat;

            return string.Format(exceptionFormat, documentName);
        }
    }
}