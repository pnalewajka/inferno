using System;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class DatabaseConcurrencyException : BusinessException
    {
        protected DatabaseConcurrencyException()
        {
        }

        public DatabaseConcurrencyException(string message) : base(message)
        {
        }

        protected DatabaseConcurrencyException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DatabaseConcurrencyException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}