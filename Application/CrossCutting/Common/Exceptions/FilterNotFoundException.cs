﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class FilterNotFoundException : BusinessException
    {
        protected FilterNotFoundException()
        {
        }

        public FilterNotFoundException(string filterCode)
            : base(BuildMessage(filterCode))
        {
        }

        private static string BuildMessage(string filterCode)
        {
            var exceptionFormat = LocalizedExceptionResources.FilterNotFoundExceptionFormat;

            return string.Format(exceptionFormat, filterCode);
        }
    }
}