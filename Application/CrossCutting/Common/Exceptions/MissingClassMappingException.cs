﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    [Serializable]
    public class MissingClassMappingException : Exception
    {
        private const string MessageFormat = "Missing class mapping: {0} ";

        /// <summary>
        /// Missing mapping class name
        /// </summary>
        public string MissingMappingKey { get; protected set; }

        public MissingClassMappingException()
        {
        }

        public MissingClassMappingException(string missingMappingKey)
            : base(string.Format(MessageFormat, missingMappingKey))
        {
            MissingMappingKey = missingMappingKey;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected MissingClassMappingException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            MissingMappingKey = info.GetString(PropertyHelper.GetPropertyName<MissingClassMappingException>(e => e.MissingMappingKey));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<MissingClassMappingException>(e => e.MissingMappingKey), MissingMappingKey);
            base.GetObjectData(info, context);
        }

    }
}
