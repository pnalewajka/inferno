﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class OvertimeBalanceLessThanMinimunDbException : BusinessException
    {
        public long MinimunBalance { get; set; }

        public override string Message => string.Format(
            OvertimeBankResources.UpdatingOvertimeFailedBalanceLessThanMinimun, MinimunBalance);
    }
}
