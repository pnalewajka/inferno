﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class MissingRequiredRoleException : BusinessException
    {
        protected MissingRequiredRoleException()
        {
        }

        public MissingRequiredRoleException(IEnumerable<string> missingRoles, bool allRolesRequired = true)
            : base(BuildMessage(missingRoles, allRolesRequired))
        {
        }

        private static string BuildMessage(IEnumerable<string> missingRoles, bool allRolesRequired)
        {
            var exceptionFormat = allRolesRequired
                ? LocalizedExceptionResources.MissingRequiredRoleExceptionFormat
                : LocalizedExceptionResources.MissingOneOfRoleExceptionFormat;

            return string.Format(exceptionFormat, string.Join(", ", missingRoles));
        }
    }
}
