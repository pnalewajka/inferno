﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class MissingRequiredProfileException : BusinessException
    {
        public MissingRequiredProfileException() : base(LocalizedExceptionResources.MissingProfileException)
        {
        }
    }
}
