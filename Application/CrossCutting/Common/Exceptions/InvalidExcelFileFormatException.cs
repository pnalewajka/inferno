﻿using System;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class InvalidExcelFileFormatException : Exception
    {
        public InvalidExcelFileFormatException(Exception innerException)
            : base(LocalizedExceptionResources.InvalidExcelFileFormatExceptionFormat, innerException)
        {
        }
    }
}