﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Very basic configuration exception message
    /// </summary>
    public class BasicConfigurationException : Exception
    {
        public BasicConfigurationException()
        {
        }

        public BasicConfigurationException(string message) : base(message)
        {
        }

        public BasicConfigurationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
