﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class FilterParameterFormatException : BusinessException
    {
        protected FilterParameterFormatException()
        {
        }

        public FilterParameterFormatException(string parameterName)
            : base(BuildMessage(parameterName))
        {
        }

        private static string BuildMessage(string parameterName)
        {
            var exceptionFormat = LocalizedExceptionResources.FilterParameterFormatExceptionFormat;

            return string.Format(exceptionFormat, parameterName);
        }
    }
}