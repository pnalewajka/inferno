using System;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception intented for business rule failures. All those exceptions will be 
    /// handled globaly and displayed as a message to user
    /// </summary>
    [Serializable]
    public class BusinessException : Exception
    {
        protected BusinessException()
        {
        }

        public BusinessException(string message) : base(message)
        {
        }

        public BusinessException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BusinessException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}