﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// System parameter not found exception
    /// </summary>
    [Serializable]
    public class SystemParameterNotFoundException : Exception
    {
        public string Key { get; protected set; }
        public string ValueTypeId { get; protected set; }

        public SystemParameterNotFoundException(string key)
            : base($"Parameter {key} not found.")
        {
            Key = key;
        }

        public SystemParameterNotFoundException(string key, string valueTypeId)
            : base($"Parameter {key} of type {valueTypeId} not found.")
        {
            Key = key;
            ValueTypeId = valueTypeId;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected SystemParameterNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Key = info.GetString(PropertyHelper.GetPropertyName<SystemParameterNotFoundException>(e => e.Key));
            var value = info.GetString(PropertyHelper.GetPropertyName<SystemParameterNotFoundException>(e => e.ValueTypeId));

            if (!string.IsNullOrEmpty(value))
            {
                ValueTypeId = value;
            }
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<SystemParameterNotFoundException>(e => e.Key), Key);
            info.AddValue(PropertyHelper.GetPropertyName<SystemParameterNotFoundException>(e => e.ValueTypeId), ValueTypeId);
            base.GetObjectData(info, context);
        }
    }
}
