using System;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class DatabaseOperationException : Exception
    {
        public DatabaseOperationException()
        {
        }

        public DatabaseOperationException(string message) : base(message)
        {
        }

        public DatabaseOperationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DatabaseOperationException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}