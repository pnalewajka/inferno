﻿using System;
using System.Runtime.Serialization;

using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception intented for business rule failures. All those exceptions will be 
    /// handled globaly and displayed as a message to user
    /// </summary>
    [Serializable]
    public class CrmException : Exception
    {
        protected CrmException()
        {
        }

        public CrmException(string message) : base(message)
        {
        }

        public CrmException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CrmException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}