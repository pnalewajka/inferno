﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception that describes details of missing resource in assembly
    /// </summary>
    [Serializable]
    public class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException()
        {
        }

        public ResourceNotFoundException(string message) : base(message)
        {
        }

        public ResourceNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ResourceNotFoundException(string message, string resourceName, string assemblyName) : this(message)
        {
            ResourceName = resourceName;
            AssemblyName = assemblyName;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected ResourceNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            AssemblyName = info.GetString(PropertyHelper.GetPropertyName<ResourceNotFoundException>(e => e.AssemblyName));
            ResourceName = info.GetString(PropertyHelper.GetPropertyName<ResourceNotFoundException>(e => e.ResourceName));
        }

        /// <summary>
        /// Resource name for system was looking for
        /// </summary>
        public string ResourceName { get; protected set; }

        /// <summary>
        /// Assembly name in which system was looking for
        /// </summary>
        public string AssemblyName { get; protected set; }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<ResourceNotFoundException>(e => e.ResourceName), ResourceName);
            info.AddValue(PropertyHelper.GetPropertyName<ResourceNotFoundException>(e => e.AssemblyName), AssemblyName);
            base.GetObjectData(info, context);
        }
    }
}