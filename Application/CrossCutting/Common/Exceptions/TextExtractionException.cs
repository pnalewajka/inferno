﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class TextExtractionException : Exception
    {
        public TextExtractionException(string message) : base(message)
        {
        }
    }
}
