﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception that describes details of generic parameter mismatch
    /// </summary>
    [Serializable]
    public class GenericParameterException : Exception
    {
        private const string MessageFormat = "Incorrect generic parameter type {0}, expected base type was {1}";

        public GenericParameterException()
        {
        }

        public GenericParameterException(string message) : base(message)
        {
        }

        public GenericParameterException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public GenericParameterException(Type genericParameterType, Type expectedType)
            : this(string.Format(MessageFormat, genericParameterType.FullName, expectedType.FullName))
        {
            GenericParameterType = genericParameterType.FullName;
            ExpectedParameterType = expectedType.FullName;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected GenericParameterException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            GenericParameterType = info.GetString(PropertyHelper.GetPropertyName<GenericParameterException>(e=>e.GenericParameterType));
            ExpectedParameterType = info.GetString(PropertyHelper.GetPropertyName<GenericParameterException>(e => e.ExpectedParameterType));
        }

        /// <summary>
        /// Bad generic parameter type
        /// </summary>
        public string GenericParameterType { get; protected set; }

        /// <summary>
        /// Expected parameter type
        /// </summary>
        public string ExpectedParameterType { get; protected set; }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<GenericParameterException>(e => e.GenericParameterType), GenericParameterType);
            info.AddValue(PropertyHelper.GetPropertyName<GenericParameterException>(e => e.ExpectedParameterType), ExpectedParameterType);
            base.GetObjectData(info, context);
        }
    }
}