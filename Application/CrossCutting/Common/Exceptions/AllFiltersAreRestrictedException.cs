﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Exception is thrown when user request to display card index data cannot be performed because all filters are restricted
    /// </summary>
    public class AllFiltersAreRestrictedException : BusinessException
    {
        public AllFiltersAreRestrictedException()
            : base(LocalizedExceptionResources.AllFiltersAreRestricted)
        {
        }
    }
}