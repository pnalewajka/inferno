﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    /// <summary>
    /// Required attribute wasn't found
    /// </summary>
    public class AttributeNotFoundException<TAttribute> : Exception
        where TAttribute : Attribute
    {
        private const string MessageFormat = @"Attibute {0} wasn't found";

        public string AttributeClassName { get; set; }

        public AttributeNotFoundException() : base(string.Format(MessageFormat, typeof(TAttribute).Name))
        {
            AttributeClassName = typeof(TAttribute).Name;
        }
    }
}
