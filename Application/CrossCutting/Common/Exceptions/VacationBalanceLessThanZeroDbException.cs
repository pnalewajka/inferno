﻿using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Exceptions
{
    public class VacationBalanceLessThanZeroDbException : BusinessException
    {
        public override string Message => VacationBalanceDbExceptionResources.NegativeInFuture;
    }
}
