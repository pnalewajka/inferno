﻿using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.MemberAccess.Binders;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess
{
    public class PropertyAccessElement : MemberAccessElement
    {
        private static readonly Regex PropertyPattern = new Regex(
            @"^\.?(?<Name>[A-Za-z_][A-Za-z0-9_]*)$",
            RegexOptions.Compiled);

        private static readonly ConcurrentDictionary<string, CallSite<Func<CallSite, object, object>>> CallSiteCache =
            new ConcurrentDictionary<string, CallSite<Func<CallSite, object, object>>>();

        private readonly CallSite<Func<CallSite, object, object>> _callSite;

        public string PropertyName { get; }

        public PropertyAccessElement([NotNull] string propertyName)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            PropertyName = propertyName;
            _callSite = CallSiteCache.GetOrAdd(propertyName,
                n => CallSite<Func<CallSite, object, object>>.Create(new PropertyBinder(propertyName)));
        }

        public override object GetValueOf(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            return _callSite.Target(_callSite, obj);
        }

        public override string ToString()
        {
            return PropertyName;
        }

        public static PropertyAccessElement TryParse(string elementName)
        {
            var match = PropertyPattern.Match(elementName);

            if (!match.Success)
            {
                return null;
            }

            var propertyName = match.Groups["Name"].Value;

            return new PropertyAccessElement(propertyName);
        }
    }
}
