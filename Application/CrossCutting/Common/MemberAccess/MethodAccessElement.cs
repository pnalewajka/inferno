﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.MemberAccess.Binders;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess
{
    public class MethodAccessElement : MemberAccessElement
    {
        private static readonly Regex MethodPattern = new Regex(
            @"^\.?(?<Name>[A-Za-z_][A-Za-z0-9_]*)\((?<Parameters>[A-Za-z0-9_ ,]*)\)$",
            RegexOptions.Compiled);

        private static readonly ConcurrentDictionary<string, CallSite<Func<CallSite, object, object>>> CallSiteCache =
            new ConcurrentDictionary<string, CallSite<Func<CallSite, object, object>>>();

        private readonly CallSite<Func<CallSite, object, object>> _callSite;

        public string MethodName { get; }

        public string[] Arguments { get; set; }

        public MethodAccessElement([NotNull] string methodName, string[] arguments)
        {
            if (methodName == null)
            {
                throw new ArgumentNullException(nameof(methodName));
            }

            MethodName = methodName;
            Arguments = arguments ?? Array.Empty<string>();

            _callSite = CallSiteCache.GetOrAdd(ToString(),
                n => CallSite<Func<CallSite, object, object>>.Create(new MethodBinder(MethodName, arguments)));
        }

        public override object GetValueOf(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            return _callSite.Target(_callSite, obj);
        }

        public override string ToString()
        {
            return $"{MethodName}({string.Join(",", Arguments)})";
        }

        public static MethodAccessElement TryParse(string elementName)
        {
            var match = MethodPattern.Match(elementName);

            if (!match.Success)
            {
                return null;
            }

            var methodName = match.Groups["Name"].Value;
            var unparsedParameters = match.Groups["Parameters"].Value;
            var parameters = unparsedParameters == string.Empty ? Array.Empty<string>() : unparsedParameters
                .Split(',')
                .Select(p => p.Trim())
                .ToArray();

            return new MethodAccessElement(methodName, parameters);
        }
    }
}
