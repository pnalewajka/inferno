﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess
{
    internal static class MemberAccessExpressionHelper
    {
        private static readonly Func<string, MemberAccessElement>[] Parsers = {
            PropertyAccessElement.TryParse,
            IndexerAccessElement.TryParse,
            MethodAccessElement.TryParse,
        };

        public static MemberAccessElement ParseElement([NotNull] string elementName)
        {
            if (elementName == null)
            {
                throw new ArgumentNullException(nameof(elementName));
            }

            var result = Parsers.Select(p => p(elementName)).FirstOrDefault(e => e != null);

            if (result == null)
            {
                throw new InvalidOperationException($"Cannot parse element {elementName}");
            }

            return result;
        }

        public static IEnumerable<string> SplitPathToElementNames([NotNull] string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }

            var startIndex = 0;

            while (true)
            {
                var nextSplitIndex = startIndex + 1 < path.Length
                    ? path.IndexOfAny(new[] {'.', '['}, startIndex + 1)
                    : -1;

                if (nextSplitIndex < 0)
                {
                    yield return path.Substring(startIndex);

                    yield break;
                }

                yield return path.Substring(startIndex, nextSplitIndex - startIndex);

                startIndex = nextSplitIndex;
            }
        }
    }
}
