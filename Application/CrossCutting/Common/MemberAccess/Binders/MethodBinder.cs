﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess.Binders
{
    internal class MethodBinder : CallSiteBinder
    {
        private readonly string _methodName;
        private readonly IReadOnlyList<string> _arguments; 

        public MethodBinder(string methodName, IReadOnlyList<string> arguments)
        {
            _methodName = methodName;
            _arguments = arguments;
        }

        public override Expression Bind(object[] args, ReadOnlyCollection<ParameterExpression> parameters, LabelTarget returnLabel)
        {
            var targetParameter = parameters.Single();
            var target = args.Single();
            var targetType = target.GetType();

            var method = targetType.GetMethods(BindingFlags.Instance | BindingFlags.Public)
                .FirstOrDefault(m => m.Name == _methodName && m.GetParameters().Length == _arguments.Count);

            Expression result;

            try
            {
                if (method == null || method.ReturnType == typeof(void))
                {
                    throw new InvalidOperationException(
                        $"Method '{_methodName}' taking {_arguments.Count} parameters not found in class {targetType.Name}");
                }

                var arguments = CreateArgumentExpressions(method.GetParameters(), targetType);
                var typedParameter = Expression.Convert(targetParameter, targetType);
                result = Expression.Call(typedParameter, method, arguments);
                result = Expression.Convert(result, typeof(object));
            }
            catch (Exception e)
            {
                result = Expression.Throw(
                    Expression.New(
                        e.GetType().GetConstructor(new[] {typeof(string)}),
                        Expression.Constant(e.Message)),
                    typeof(object));
            }

            return WrapWithGuard(targetParameter, result, targetType, returnLabel);
        }

        private static Expression WrapWithGuard(Expression target, Expression result, Type targetType, LabelTarget returnLabel)
        {
            return Expression.IfThen(Expression.TypeIs(target, targetType), Expression.Return(returnLabel, result));
        }

        private IEnumerable<Expression> CreateArgumentExpressions(IReadOnlyList<ParameterInfo> expectedParameters, Type targetType)
        {
            for (var i = 0; i < _arguments.Count; i++)
            {
                var argumentType = expectedParameters[i].ParameterType;
                object argument;

                try
                {
                    argument = Convert.ChangeType(_arguments[i], argumentType);
                }
                catch (InvalidCastException e)
                {
                    throw new InvalidOperationException(
                        $"Cannot apply argument '{_arguments[i]}' to method '{targetType.Name}.{_methodName}' at position {i}",
                        e);
                }
                catch (FormatException e)
                {
                    throw new InvalidOperationException(
                        $"Cannot apply argument '{_arguments[i]}' to method '{targetType.Name}.{_methodName}' at position {i}",
                        e);
                }

                yield return Expression.Constant(argument, argumentType);
            }
        }
    }
}
