﻿using System;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess.Binders
{
    internal class IndexerBinder : GetIndexBinder
    {
        public IndexerBinder()
            : base(new CallInfo(1, "index"))
        {
        }

        public override DynamicMetaObject FallbackGetIndex(DynamicMetaObject target, DynamicMetaObject[] indexes,
            DynamicMetaObject errorSuggestion)
        {
            var targetType = target.LimitType;

            var targetExpression = Expression.Convert(target.Expression, targetType);
            Expression expression;

            if (targetType.IsArray && targetType.GetArrayRank() == 1)
            {
                expression = Expression.ArrayIndex(targetExpression, indexes.Single().Expression);
            }
            else
            {
                var defaultMemberAttribute = targetType.GetCustomAttribute<DefaultMemberAttribute>();

                if (defaultMemberAttribute == null)
                {
                    return errorSuggestion ?? CannotIndex(target);
                }

                var indexTypes = indexes.Select(d => d.LimitType).ToArray();

                var indexer = targetType.GetProperty(defaultMemberAttribute.MemberName, indexTypes);

                if (indexer == null)
                {
                    return errorSuggestion ?? CannotIndex(target);
                }

                expression = Expression.MakeIndex(targetExpression, indexer, indexes.Select(d => d.Expression));
            }

            expression = Expression.Convert(expression, typeof(object));

            return new DynamicMetaObject(expression,
                BindingRestrictions.GetTypeRestriction(target.Expression, targetType));
        }

        private static DynamicMetaObject CannotIndex(DynamicMetaObject target)
        {
            var expression = Expression.Throw(
                    Expression.New(
                        typeof(InvalidOperationException).GetConstructor(new[] { typeof(string) }),
                        Expression.Constant($"Cannot index class {target.LimitType.Name}")),
                    typeof(object));

            return new DynamicMetaObject(expression,
                BindingRestrictions.GetTypeRestriction(target.Expression, target.LimitType));
        }
    }
}
