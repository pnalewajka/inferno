﻿using System;
using System.Dynamic;
using System.Linq.Expressions;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess.Binders
{
    internal class PropertyBinder : GetMemberBinder
    {
        public PropertyBinder(string name)
            : base(name, false)
        {
        }

        public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
        {
            var targetType = target.LimitType;

            var property = targetType.GetProperty(Name);

            Expression expression;

            if (property == null || !property.CanRead || property.GetIndexParameters().Length > 0)
            {
                if (errorSuggestion != null)
                {
                    return errorSuggestion;
                }

                expression = Expression.Throw(
                    Expression.New(
                        typeof (InvalidOperationException).GetConstructor(new[] {typeof (string)}),
                        Expression.Constant($"Property '{Name}' not found in class {targetType.Name}")),
                    typeof(object));
            }
            else
            {
                expression = Expression.Convert(
                    Expression.MakeMemberAccess(
                        Expression.Convert(target.Expression, targetType),
                        property),
                    typeof(object));
            }

            return new DynamicMetaObject(expression,
                BindingRestrictions.GetTypeRestriction(target.Expression, targetType));
        }
    }
}
