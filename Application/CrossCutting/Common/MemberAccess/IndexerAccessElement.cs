﻿using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.MemberAccess.Binders;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess
{
    public class IndexerAccessElement : MemberAccessElement
    {
        private static readonly Regex IndexerPattern = new Regex(@"^\[(?<Index>[0-9]+)\]$", RegexOptions.Compiled);

        private static readonly CallSite<Func<CallSite, object, int, object>> CallSite =
            CallSite<Func<CallSite, object, int, object>>.Create(new IndexerBinder());

        public int Index { get; }

        public IndexerAccessElement(int index)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }
            
            Index = index;
        }

        public override object GetValueOf(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            return CallSite.Target(CallSite, obj, Index);
        }

        public override void AppendTo(StringBuilder stringBuilder)
        {
            stringBuilder.Append(ToString());
        }

        public override string ToString()
        {
            return $"[{Index}]";
        }

        public static IndexerAccessElement TryParse(string elementName)
        {
            var match = IndexerPattern.Match(elementName);

            if (!match.Success)
            {
                return null;
            }
            
            var index = int.Parse(match.Groups["Index"].Value);

            return new IndexerAccessElement(index);
        }
    }
}
