﻿using System.Text;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess
{
    public abstract class MemberAccessElement
    {
        public abstract object GetValueOf(object obj);

        public virtual void AppendTo(StringBuilder stringBuilder)
        {
            if (stringBuilder.Length > 0)
            {
                stringBuilder.Append('.');
            }

            stringBuilder.Append(ToString());
        }
    }
}
