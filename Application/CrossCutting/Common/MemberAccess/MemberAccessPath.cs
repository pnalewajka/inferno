﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.MemberAccess
{
    public class MemberAccessPath
    {
        public class ExtendedPropertyInfo
        {
            public PropertyInfo PropertyInfo { get; set; }
            public object ParentObject { get; set; }

            public ExtendedPropertyInfo(PropertyInfo info, object parent)
            {
                PropertyInfo = info;
                ParentObject = parent;
            }
        }

        public IReadOnlyList<MemberAccessElement> Elements { get; }

        public MemberAccessPath([NotNull] IEnumerable<MemberAccessElement> elements)
        {
            Elements = elements.ToList();
        }

        public MemberAccessPath([NotNull] IEnumerable<string> elements)
            : this(elements.Select(MemberAccessExpressionHelper.ParseElement))
        {
        }

        public MemberAccessPath([NotNull] string path)
            : this(MemberAccessExpressionHelper.SplitPathToElementNames(path))
        {
        }

        public MemberAccessPath Join([NotNull] MemberAccessElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            return new MemberAccessPath(Elements.Concat(new[] { element }));
        }

        public MemberAccessPath Join([NotNull] MemberAccessPath path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            return new MemberAccessPath(Elements.Concat(path.Elements));
        }

        public MemberAccessPath Join([NotNull] string path)
        {
            return Join(new MemberAccessPath(path));
        }

        public object GetValueOf(object obj)
        {
            return Elements.Aggregate(obj, (o, e) => e.GetValueOf(o));
        }

        public ExtendedPropertyInfo GetPropertyInfo(object @object)
        {
            var lastParentPath = new MemberAccessPath(Elements.Take(Elements.Count - 1));
            var lastParent = lastParentPath.GetValueOf(@object);

            var propertyName = Elements.Last().ToString();
            var propertyInfo = lastParent.GetType().GetProperty(propertyName);

            if (propertyInfo == null)
            {
                return new ExtendedPropertyInfo(null, null);
            }

            return new ExtendedPropertyInfo(propertyInfo, lastParent);
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            foreach (var element in Elements)
            {
                element.AppendTo(stringBuilder);
            }

            return stringBuilder.ToString();
        }
    }
}
