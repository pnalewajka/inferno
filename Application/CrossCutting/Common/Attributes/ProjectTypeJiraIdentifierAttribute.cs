﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class ProjectTypeJiraIdentifierAttribute : Attribute
    {
        public string Identifier { get; private set; }

        public ProjectTypeJiraIdentifierAttribute(string identifier)
        {
            Identifier = identifier;
        }
    }
}