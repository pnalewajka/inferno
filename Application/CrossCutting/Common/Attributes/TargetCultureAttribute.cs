﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class TargetCultureAttribute : Attribute
    {
        /// <summary>
        /// Name of culture to match. Match all child cultures.
        /// Example: "en" matches "en-GB" and "en-US", "" matches everything
        /// </summary>
        public string CultureName { get; }

        /// <summary>
        /// </summary>
        /// <param name="cultureName">Name of culture to match. Match all child cultures.</param>
        public TargetCultureAttribute(string cultureName)
        {
            CultureName = cultureName;
        }
    }
}
