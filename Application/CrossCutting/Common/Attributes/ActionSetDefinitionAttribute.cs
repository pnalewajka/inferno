﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class ActionSetDefinitionAttribute : Attribute
    {
        public ActionSetDefinitionAttribute(string description, RequestType triggeringRequestType, RequestStatus[] triggeringRequestStatuses)
        {
            Description = description;
            TriggeringRequestType = triggeringRequestType;
            TriggeringRequestStatuses = triggeringRequestStatuses;
        }

        public string Description { get; private set; }

        public RequestType TriggeringRequestType { get; private set; }

        public RequestStatus[] TriggeringRequestStatuses { get; private set; }
    }
}
