﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class SystemParameterDefinitionAttribute : Attribute
    {
        public string ValueType { get; set; }

        public string DefaultValue { get; set; }

        public string ContextType { get; set; }
    }
}
