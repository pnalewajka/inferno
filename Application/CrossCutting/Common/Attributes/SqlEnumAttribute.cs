﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Enum)]
    public class SqlEnumAttribute : Attribute
    {
        public string SchemaName { get; private set; }

        public string TableName { get; private set; }

        public SqlEnumAttribute(string schemaName, string tableName)
        {
            SchemaName = schemaName;
            TableName = tableName;
        }

        public SqlEnumAttribute(string schemaName)
            : this(schemaName, null)
        {
        }
    }
}
