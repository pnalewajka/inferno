﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class JobDefinitionAttribute : Attribute
    {
        /// <summary>
        /// Code of job
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// More detailed job description 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Enable/disable job. Will be not started periodically unless job is enabled
        /// </summary>
        public bool IsEnabled { get; set; } = true;

        /// <summary>
        /// Force job run when is created
        /// </summary>
        public bool ShouldRunAfterCreation { get; set; } = false;

        /// <summary>
        /// Name of pipeline in which job should be run
        /// </summary>
        public string PipelineName { get; set; }

        /// <summary>
        /// Maximum execution time after which job will be cancelled or terminated
        /// </summary>
        public int MaxExecutioPeriodInSeconds { get; set; } = 60;

        /// <summary>
        /// Cron-like expression for time evaluation
        /// </summary>
        public string ScheduleSettings { get; set; }

        /// <summary>
        /// How many attempts to run job in bulk until job will be delayed
        /// </summary>
        public int MaxRunAttemptsInBulk { get; set; } = 2;

        /// <summary>
        /// Delay before next bulk attempts
        /// </summary>
        public int DelayBetweenBulkRunAttemptsInSeconds { get; set; } = 10;

        /// <summary>
        /// How many times before job should fail 
        /// </summary>
        public int MaxBulkRunAttempts { get; set; } = 2;
    }
}
