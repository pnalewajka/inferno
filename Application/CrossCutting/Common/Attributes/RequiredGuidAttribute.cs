using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class RequiredGuidAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            Guid guid;

            if (value == null || !Guid.TryParse(value.ToString(), out guid))
            {
                guid = default(Guid);
            }

            return !Equals(guid, default(Guid));
        }
    }
}
