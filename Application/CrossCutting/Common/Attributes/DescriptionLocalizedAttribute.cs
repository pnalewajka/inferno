﻿using System;
using System.ComponentModel;
using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class DescriptionLocalizedAttribute : DescriptionAttribute
    {
        public readonly Type ResourceType;

        public DescriptionLocalizedAttribute()
        {
        }

        public DescriptionLocalizedAttribute(string description, Type resourceType = null)
            : base(description)
        {
            ResourceType = resourceType;
        }

        public override string Description => GetDescription();

        public string GetDescription(CultureInfo culture = null)
        {
            if (ResourceType != null)
            {
                var resourceManager = ResourceManagerHelper.GetResourceManager(ResourceType);

                return culture != null
                    ? resourceManager.GetString(base.Description, culture)
                    : resourceManager.GetString(base.Description);
            }

            return base.Description;
        }
    }
}