﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FullTextSearchAttribute : Attribute
    {
        // see FullTextIndexSeed
    }
}
