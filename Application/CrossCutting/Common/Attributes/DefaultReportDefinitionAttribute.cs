﻿using System;
using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DefaultReportDefinitionAttribute : Attribute
    {
        public DefaultReportDefinitionAttribute(
            string code,
            string name,
            string description,
            string area,
            string group,
            string reportingEngineIdentifier,
            string[] reportTemplateResources,
            Type resourceType)
        {
            Code = code;
            Name = name;
            Description = description;
            Area = area;
            Group = group;
            ReportingEngineIdentifier = reportingEngineIdentifier;
            ReportTemplateResources = reportTemplateResources;
            ResourceType = resourceType;
        }

        public DefaultReportDefinitionAttribute(
            string code,
            string name,
            string description,
            string area,
            string group,
            string reportingEngineIdentifier,
            string reportTemplateResource,
            Type resourceType)
            : this(code, name, description, area, group, reportingEngineIdentifier, new[] { reportTemplateResource }, resourceType)
        {
        }

        public DefaultReportDefinitionAttribute(
            string code,
            string name,
            string description,
            string area,
            string group,
            string reportingEngineIdentifier,
            string reportTemplateResource,
            string fileName,
            Type resourceType)
            : this(code, name, description, area, group, reportingEngineIdentifier, reportTemplateResource, resourceType)
        {
            FileName = fileName;
        }

        public DefaultReportDefinitionAttribute(
            string code,
            string name,
            string description,
            string area,
            string group,
            string reportingEngineIdentifier,
            string[] reportTemplateResources,
            string fileName,
            Type resourceType)
            : this(code, name, description, area, group, reportingEngineIdentifier, reportTemplateResources, resourceType)
        {
            FileName = fileName;
        }

        public string Code { get; }

        public string Name { get; }

        public string Description { get; }

        public string ReportingEngineIdentifier { get; }

        public string[] ReportTemplateResources { get; }

        public string FileName { get; }

        public string Area { get; }

        public string Group { get; }

        public Type ResourceType { get; }

        public string GetName(CultureInfo cultureInfo) => ResourceHelper.GetString(ResourceType, Name, cultureInfo);

        public string GetDescription(CultureInfo cultureInfo) => ResourceHelper.GetString(ResourceType, Description, cultureInfo);
    }
}
