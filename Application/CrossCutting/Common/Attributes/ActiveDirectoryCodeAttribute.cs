﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class ActiveDirectoryCodeAttribute : Attribute
    {
        public string Code { get; private set; }

        public ActiveDirectoryCodeAttribute(string code)
        {
            Code = code;
        }
    }
}