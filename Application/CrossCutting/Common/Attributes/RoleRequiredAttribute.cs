﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    /// <summary>
    /// Attribute that defines Role requirement for specific property.
    /// In enums, it can be applied to each field.
    /// Field will be hidden if the user doesn't have the permission to pick it
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class RoleRequiredAttribute : Attribute
    {
        /// <summary>
        /// Read permission role
        /// </summary>
        public SecurityRoleType ForRead { get; private set; }

        /// <summary>
        /// Write permission role
        /// </summary>
        public SecurityRoleType ForWrite { get; private set; }

        public RoleRequiredAttribute(SecurityRoleType accessRole)
            : this(accessRole, accessRole)
        {
        }

        public RoleRequiredAttribute(SecurityRoleType forRead, SecurityRoleType forWrite)
        {
            ForRead = forRead;
            ForWrite = forWrite;
        }
    }
}
