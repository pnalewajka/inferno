﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class TemplateDefinitionAttribute : Attribute
    {
        public TemplateDefinitionAttribute(MessageTemplateContentType contentType, string description, string templateLocation)
        {
            ContentType = contentType;
            Description = description;
            TemplateLocation = templateLocation;
        }

        public MessageTemplateContentType ContentType { get; private set; }

        public string Description { get; private set; }

        public string TemplateLocation { get; private set; }
    }
}
