﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    /// <summary>
    /// Defines universal unique identifier which should be used to refer to given type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum, AllowMultiple = false)]
    public class IdentifierAttribute : Attribute
    {
        private readonly string _value;

        public string Value => _value;

        public IdentifierAttribute(string value)
        {
            _value = value;
        }
    }
}