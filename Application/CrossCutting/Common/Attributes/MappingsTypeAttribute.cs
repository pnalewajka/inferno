﻿using System;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    /// <summary>
    /// Defines type for maintaining properties mappings between 2 classes
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class MappingsTypeAttribute : Attribute
    {
        public MappingsTypeAttribute(Type mappingClass)
        {
            MappingType = mappingClass;
        }

        /// <summary>
        /// Mappings class 
        /// </summary>
        public Type MappingType { get; protected set; }

        /// <summary>
        /// Determines if specified mapping type can be used to map between specified types
        /// </summary>
        /// <returns>true if specified type can be used for mapping</returns>
        public bool IsValidForMapping(Type sourceType, Type destinationType)
        {
            var classMappingType = typeof (ClassMapping<,>).MakeGenericType(sourceType, destinationType);
            
            return classMappingType.IsAssignableFrom(MappingType);
        }
    }
}
