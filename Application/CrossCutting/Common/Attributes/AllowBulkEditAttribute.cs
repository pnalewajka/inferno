﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    /// <summary>
    /// Mark property as allowed in bulk edit action
    /// </summary>
    public class AllowBulkEditAttribute : Attribute
    {
    }
}
