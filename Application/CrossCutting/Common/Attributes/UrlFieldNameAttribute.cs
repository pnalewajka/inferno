﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    /// <summary>
    /// Url field name. Actual property name will be converted to provided name.
    /// example: ContextParentId can be converte to url field parent-id
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class UrlFieldNameAttribute : Attribute
    {
        /// <summary>
        /// Url field name
        /// </summary>
        public string Name { get; private set; }

        public UrlFieldNameAttribute(string name)
        {
            Name = name;
        }
    }
}
