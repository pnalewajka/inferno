﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Attributes
{
    public class SecurityRoleDescriptionAttribute : DescriptionLocalizedAttribute
    {
        public readonly SecurityRoleModule Module;

        public SecurityRoleDescriptionAttribute()
        {
        }

        public SecurityRoleDescriptionAttribute(string description, Type resourceType = null, SecurityRoleModule module = SecurityRoleModule.Default)
            : base(description, resourceType)
        {
            Module = module;
        }
    }
}
