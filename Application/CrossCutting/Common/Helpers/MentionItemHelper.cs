﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public class MentionItemHelper
    {
        private static Lazy<Regex> TokenRegex = new Lazy<Regex>(() => new Regex(@"\@\{([^}]*)\}", RegexOptions.Compiled));

        public static string ReplaceTokens(string input, Func<MentionItem, string> replaceMethod)
        {
            var costam = TokenRegex.Value.Replace(input, match =>
            {
                try
                {
                    var json = match.Value.Substring(1);
                    var item = JsonConvert.DeserializeObject<MentionItem>(HttpUtility.HtmlDecode(json));

                    return replaceMethod(item);
                }
                catch
                {
                    return match.Value;
                }
            });

            return costam.Trim();
        }

        public static IEnumerable<MentionItem> GetTokens(string input)
        {
            var resultList = new List<MentionItem>();

            foreach (Match match in TokenRegex.Value.Matches(input))
            {
                try
                {
                    var item = JsonConvert.DeserializeObject<MentionItem>(HttpUtility.HtmlDecode(match.Value.Substring(1)));

                    if (item != null && !string.IsNullOrEmpty(item.Label))
                    {
                        resultList.Add(item);
                    }
                }
                catch
                {
                    /* do nothing, just return */
                }
            }

            return resultList;
        }

        public static IEnumerable<long> GetTokenIds(string input)
        {
            return GetTokens(input).Select(i => i.Id);
        }
    }
}
