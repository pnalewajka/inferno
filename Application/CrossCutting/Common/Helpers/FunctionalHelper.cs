﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class FunctionalHelper
    {
        public static T Try<T>(Func<T> tryFunction, T fallbackValue)
        {
            return Try<T, Exception>(tryFunction, () => fallbackValue);
        }

        public static T Try<T>(Func<T> tryFunction, Func<T> fallbackFunction)
        {
            return Try<T, Exception>(tryFunction, fallbackFunction);
        }

        public static T Try<T, TException>(Func<T> tryFunction, Func<T> fallbackFunction)
            where TException : Exception
        {
            try
            {
                return tryFunction();
            }
            catch (TException)
            {
                return fallbackFunction();
            }
        }
    }
}
