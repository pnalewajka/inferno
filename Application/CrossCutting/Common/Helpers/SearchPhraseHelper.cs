using System.Collections.Generic;
using System.Text;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class SearchPhraseHelper
    {
        private const char QuotationCharacter = '"';
        private const char SpaceCharacter = ' ';

        public static IEnumerable<string> GetPhrases(string searchText)
        {
            var isInQuotation = false;
            var buffer = new StringBuilder();

            foreach (var character in searchText)
            {
                if (character == QuotationCharacter)
                {
                    isInQuotation = !isInQuotation;

                    if (!isInQuotation && buffer.Length > 0)
                    {
                        yield return buffer.ToString().ToLower();

                        buffer.Length = 0;
                    }
                }
                else if (character == SpaceCharacter)
                {
                    if (!isInQuotation && buffer.Length > 0)
                    {
                        yield return buffer.ToString().ToLower();

                        buffer.Length = 0;
                    }
                    else if (isInQuotation)
                    {
                        buffer.Append(char.ToLower(character));                        
                    }
                }           
                else
                {
                    buffer.Append(char.ToLower(character));
                }
            }

            if (buffer.Length != 0)
            {
                yield return buffer.ToString().ToLower();                
            }
        }
    }
}