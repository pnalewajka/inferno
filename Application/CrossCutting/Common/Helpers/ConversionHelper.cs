﻿using System;
using System.Text;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Methods for conversion
    /// </summary>
    public class ConversionHelper
    {
        static readonly char[] ConversionArray36 = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        /// <summary>
        /// Convert long base 36 system
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertTo36Base(long value)
        {
            var conversionBase = ConversionArray36.Length;
            var result = new StringBuilder();
            
            do
            {
                result.Append(ConversionArray36[value % conversionBase]);
                value = value / conversionBase;
            }
            while (value > 0);
            
            var resultReversed = new StringBuilder();
            for (var i = result.Length - 1; i > -1; i--)
            {
                resultReversed.Append(result[i]);
            }

            return resultReversed.ToString();
        }

        /// <summary>
        /// Convert value from base 36 string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ConvertFrom36Base(string value)
        {
            var conversionBase = ConversionArray36.Length;
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }

            value = value.ToUpper().Trim();
            long result = 0;
            long itemBase = 1;

            for (var i = value.Length - 1; i > -1; i--)
            {
                var index = Array.IndexOf(ConversionArray36, value[i]);

                if (index == -1)
                {
                    throw new InvalidCastException();
                }

                result += index * itemBase;
                itemBase *= conversionBase;
            }
            
            return result;
        }


        /// <summary>
        /// Converts file size into human readable string like : 100MB, 1GB, 150KB, etc
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string ConvertFileSizeToHumanReadableString(int size)
        {
            if (size < 1024)
            {
                return $"{size} B";
            }

            if (size < (1024 * 1024))
            {
                return $"{size/1024} KB";
            }

            if (size < (1024 * 1024 * 1024))
            {
                var mb = size / (1024 * 1024);
                var kb = (size - (mb * 1024 * 1024)) / 1024m;
                var x = (int)Math.Floor(0.5M + ((100M * kb) / 1024M));

                return (x == 0)
                    ? $"{mb} MB"
                    : $"{mb}.{x.ToString("D2")} MB";
            }
            {
                var gb = size / (1024 * 1024 * 1024);
                var mb = (size - (gb * 1024 * 1024 * 1024)) / 1024m;
                var x = (int)Math.Floor(0.5M + ((100M * mb) / 1024M));

                return (x == 0)
                    ? $"{size/(1024*1024*1024)} GB"
                    : $"{gb}.{x.ToString("D2")} GB";
            }
        }

        public static bool? TryConvertZeroOrOneToBool(string value)
        {
            switch (value)
            {
                case "1":
                    return true;

                case "0":
                    return false;

                default:
                    return null;
            }
        }
    }
}
