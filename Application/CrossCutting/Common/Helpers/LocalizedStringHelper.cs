﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public class LocalizedStringHelper
    {
        private static readonly ConcurrentDictionary<TargetTypeAndCulture, PropertyInfo> PropertyCache =
            new ConcurrentDictionary<TargetTypeAndCulture, PropertyInfo>();

        /// <summary>
        /// Returns representation in specified culture of object's virtual property (or object itself if null)
        /// </summary>
        /// <param name="object"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public static string GetLocalizedString([NotNull]object @object, CultureInfo cultureInfo = null)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            var propertyInfo = GetLocalizedPropertyInfo(@object.GetType(), cultureInfo);

            return (string)propertyInfo.GetValue(@object);
        }

        /// <summary>
        /// Returns property containing representation in specified culture of object's virtual property (or object itself if null)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public static PropertyInfo GetLocalizedPropertyInfo(Type type, CultureInfo cultureInfo = null)
        {
            cultureInfo = cultureInfo ?? CultureInfo.CurrentUICulture;

            var key = new TargetTypeAndCulture(type, cultureInfo);
            var property = PropertyCache.GetOrAdd(key, FindLocalizedPropertyInfo);

            if (property == null)
            {
                throw new InvalidOperationException($"{type.FullName} doesn't have property for specified culture");
            }

            return property;
        }

        /// <summary>
        /// Returns mapping from culture names to properties for specified virtual property
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IDictionary<string, PropertyInfo> GetCultureToPropertyMapping(Type type)
        {
            return type.GetProperties()
                .Where(p => p.CanRead && p.PropertyType == typeof(string))
                .Select(p => new
                {
                    Attrubute = AttributeHelper.GetPropertyAttribute<TargetCultureAttribute>(p),
                    Property = p
                })
                .Where(x => x.Attrubute != null)
                .ToDictionary(x => x.Attrubute.CultureName, x => x.Property);
        }

        /// <summary>
        /// Return two-letter description for current culture taken from DescriptionLocalizedAttribute of type member
        /// or null if current culture is not present in type DescriptionLocalizedAttribute attributes list
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetCultureTwoLetterISOCode(Type type)
        {
            var culturePropertyMapping = GetCultureToPropertyMapping(type);
            PropertyInfo property = null;

            if (culturePropertyMapping.ContainsKey(CultureInfo.CurrentUICulture.TwoLetterISOLanguageName))
            {
                property = culturePropertyMapping[CultureInfo.CurrentUICulture.TwoLetterISOLanguageName];
            }
            else if (culturePropertyMapping.ContainsKey(LocalizedStringBase.DefaultLanguageKey))
            {
                property = culturePropertyMapping[LocalizedStringBase.DefaultLanguageKey];
            }

            if (property != null)
            {
                var descriptionLocalize = AttributeHelper.GetPropertyAttribute<DescriptionLocalizedAttribute>(property);

                if (descriptionLocalize != null)
                {
                    var description = descriptionLocalize.GetDescription();

                    if (description != null && description.Length != 2)
                    {
                        throw new InvalidOperationException($"Property ({property.Name}) description localize value ({description}) must be of length=2");
                    }

                    return description;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns Expression with renamed last propery depending on current culture (e.g. Name -> NamePl )
        /// </summary>
        /// <typeparam name="TLocalizedString"></typeparam>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public static Expression<Func<TEntity, string>> GetOrderByExpressionForSpecificCulture<TLocalizedString, TEntity>(Expression<Func<TEntity, string>> orderByExpression)
            where TLocalizedString : LocalizedStringBase
        {
            var propertyName = PropertyHelper.GetPropertyPath<TEntity, string>(orderByExpression)?.Split('.').Last();

            if (!string.IsNullOrEmpty(propertyName))
            {
                var visitor = new LocalizedStringPropertyReplacingVisitor<TLocalizedString>(propertyName);

                return (Expression<Func<TEntity, string>>)visitor.Visit(orderByExpression);
            }

            return orderByExpression;
        }

        private static PropertyInfo FindLocalizedPropertyInfo(TargetTypeAndCulture targetTypeAndCulture)
        {
            var properties = GetCultureToPropertyMapping(targetTypeAndCulture.TargetType);
            var cultureInfo = targetTypeAndCulture.CultureInfo;

            do
            {
                PropertyInfo result;

                if (properties.TryGetValue(cultureInfo.Name, out result))
                {
                    return result;
                }

                cultureInfo = GetParentCulture(cultureInfo);
            } while (cultureInfo != null);

            return null;
        }

        private static CultureInfo GetParentCulture(CultureInfo cultureInfo)
        {
            return cultureInfo.Name == string.Empty ? null : cultureInfo.Parent;
        }

        private struct TargetTypeAndCulture : IEquatable<TargetTypeAndCulture>
        {
            public Type TargetType { get; }

            public CultureInfo CultureInfo { get; }

            public TargetTypeAndCulture(Type targetType, CultureInfo cultureName)
            {
                TargetType = targetType;
                CultureInfo = cultureName;
            }

            public bool Equals(TargetTypeAndCulture other)
            {
                return TargetType == other.TargetType && CultureInfo.Name == other.CultureInfo.Name;
            }

            public override bool Equals(object obj)
            {
                return obj is TargetTypeAndCulture && Equals((TargetTypeAndCulture)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (TargetType.GetHashCode() * 397) ^ CultureInfo.Name.GetHashCode();
                }
            }
        }

        /// <summary>
        /// Class searching Expression body for members with name equal to propertName 
        /// and replacing found members names adding culture description at the end of member name
        /// ( e.g Name -> NamePL )
        /// </summary>
        /// <typeparam name="TLocalizedString"></typeparam>
        private class LocalizedStringPropertyReplacingVisitor<TLocalizedString> : DynamicExpressionVisitor
            where TLocalizedString : LocalizedStringBase
        {
            private string _propertyName;

            public LocalizedStringPropertyReplacingVisitor(string propertyName)
            {
                _propertyName = propertyName;
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                if (node.Member.Name == _propertyName)
                {
                    var twoLetterCultureDescription = LocalizedStringHelper.GetCultureTwoLetterISOCode(typeof(TLocalizedString));

                    if (!string.IsNullOrEmpty(twoLetterCultureDescription))
                    {
                        twoLetterCultureDescription = $"{char.ToUpper(twoLetterCultureDescription[0])}{char.ToLower(twoLetterCultureDescription[1])}";
                        var otherMember = node.Member.ReflectedType.GetProperty($"{node.Member.Name}{twoLetterCultureDescription}");

                        if (otherMember != null)
                        {
                            var inner = Visit(node.Expression);

                            return Expression.Property(inner, otherMember);
                        }
                    }
                }

                return base.VisitMember(node);
            }
        }
    }
}
