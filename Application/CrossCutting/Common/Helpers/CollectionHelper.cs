﻿using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class CollectionHelper
    {
        public static bool IsNullOrEmpty<T>(this ICollection<T> items)
        {
            if (items != null && items.Count > 0)
            {
                return false;
            }

            return true;
        }
    }
}
