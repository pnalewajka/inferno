﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Week of specific month
    /// </summary>
    public class WeekFromMonth
    {
        public int WeekNumber { get; set; }

        public int DaysInWeek { get; set; }
    }
}
