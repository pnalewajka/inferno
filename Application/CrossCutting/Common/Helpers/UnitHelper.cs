﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class UnitHelper
    {
        private const decimal MilesToKilometersConversionRate = 1.60934m;

        public static decimal ConvertMilesToKilometers(decimal kilometers)
        {
            return Math.Round((MilesToKilometersConversionRate * kilometers), 2, MidpointRounding.AwayFromZero);
        }

        public static decimal ConvertKilometersToMiles(decimal miles)
        {
            return Math.Round((miles / MilesToKilometersConversionRate), 2, MidpointRounding.AwayFromZero);
        }
    }
}