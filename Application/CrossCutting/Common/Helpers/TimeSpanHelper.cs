﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class TimeSpanHelper
    {
        public static bool ApproximatelyEquals(TimeSpan timeSpan1, TimeSpan timeSpan2, long thresholdInTicks = TimeSpan.TicksPerMinute)
        {
            return Math.Abs(timeSpan1.Ticks - timeSpan2.Ticks) < thresholdInTicks;
        }

        public static TimeSpan RemoveDatePart(TimeSpan timeSpan)
        {
            return TimeSpan.FromTicks(timeSpan.Ticks % TimeSpan.TicksPerDay);
        }
    }
}
