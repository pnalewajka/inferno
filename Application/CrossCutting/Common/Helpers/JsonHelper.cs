﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class JsonHelper
    {
        /// <summary>
        /// Serializes the specified object to a JSON string.
        /// </summary>
        /// <param name="value">The object to serialize.</param>
        /// <param name="forceLowerCase">if true the property names will be converted to lower case</param>
        /// <returns>A JSON string representation of the object.</returns>
        public static string Serialize(object value, bool forceLowerCase = false)
        {
            var settings = new JsonSerializerSettings();

            if (forceLowerCase)
            {
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }

            return Serialize(value, settings);
        }

        /// <summary>
        /// Serializes the specified object to a JSON string.
        /// </summary>
        /// <param name="value">The object to serialize.</param>
        /// <param name="forceLowerCase">if true the property names will be converted to lower case</param>
        /// <returns>A JSON string representation of the object.</returns>
        public static string Serialize(object value, JsonSerializerSettings settings)
        {
            return JsonConvert.SerializeObject(value, settings);
        }

        /// <summary>
        /// Deserializes the specified JSON string into an object
        /// </summary>
        /// <param name="value">Value represented as JSON string</param>
        /// <returns>Deserialized object</returns>
        public static object Deserialize(string value)
        {
            return JsonConvert.DeserializeObject(value);
        }

        /// <summary>
        /// Deserializes the specified JSON string into an object
        /// </summary>
        /// <param name="value">Value represented as JSON string</param>
        /// <param name="expectedType">Expected resulting type</param>
        /// <returns>Deserialized object</returns>
        public static object Deserialize(string value, Type expectedType)
        {
            return JsonConvert.DeserializeObject(value, expectedType);
        }

        /// <summary>
        /// Deserializes the specified JSON string into an object of type T
        /// </summary>
        /// <param name="value">Value represented as JSON string</param>
        /// <returns>Deserialized object as T</returns>
        public static T Deserialize<T>(string value)
        {
            return (T)Deserialize(value, typeof(T));
        }

        /// <summary>
        /// Returns if the provided JSON string is a valid one
        /// </summary>
        public static bool IsJson(string value)
        {
            return value[0] == '{'
                || value[0] == '['
                || value == "null";
        }

        /// <summary>
        /// Configuration to be used for ajax calls
        /// All dates are serialized as iso strings
        /// </summary>
        public static JsonSerializerSettings DefaultAjaxSettings => new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
        };

        /// <summary>
        /// Configuration to be used when embedding object initializer
        /// in <script> tags. All dates are serialized using "new Date()" approach
        /// </summary>
        public static JsonSerializerSettings DefaultObjectInitializerSettings => new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new JsonConverter[] { new JavaScriptDateTimeConverter() }
        };
    }
}
