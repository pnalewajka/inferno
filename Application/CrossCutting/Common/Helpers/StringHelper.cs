﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// String helper methods
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Concatenates the specified collection of strings, skipping empty items.
        /// </summary>
        /// <param name="separator">The string to use as a separator.</param>
        /// <param name="items">A collection that contains the strings to concatenate.</param>
        /// <returns>A string that consists of the members of values delimited by the separator string. 
        /// If there are no items specified, the method returns System.String.Empty.</returns>
        public static string Join(string separator, params string[] items)
        {
            if (separator == null)
            {
                throw new ArgumentNullException(nameof(separator));
            }

            items = items ?? new string[0];

            return String.Join(separator, items.Where(s => !String.IsNullOrWhiteSpace(s)));
        }

        /// <summary>
        /// Returns a random string of specified length, made of allowed characters
        /// </summary>
        /// <param name="length">Length of the string to generate</param>
        /// <param name="allowedCharacters">Allowed characters. When null, A-Za-z0-9 are used.</param>
        /// <returns>Random string of given length</returns>
        public static string GetRandomString(int length, IEnumerable<char> allowedCharacters = null)
        {
            const string defaultCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            allowedCharacters = allowedCharacters ?? defaultCharacters;
            var allowedCharacterArray = allowedCharacters.ToArray();

            if (allowedCharacterArray.Length == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(allowedCharacters));
            }

            var random = new Random();
            var chars = new char[length];

            for (var i = 0; i < length; i++)
            {
                var index = random.Next(0, allowedCharacterArray.Length);
                chars[i] = allowedCharacterArray[index];
            }

            return new string(chars);
        }

        /// <summary>
        /// Returns a random string of length within specified bounds, made of allowed characters
        /// </summary>
        /// <param name="minLength">The inclusive lower bound of the random length used</param>
        /// <param name="maxLength">The exclusive upper bound of the random length. maxLength must be greater than or equal to minValue.</param>
        /// <param name="allowedCharacters">Allowed characters. When null, A-Z0-9 are used.</param>
        /// <returns>Random string</returns>
        public static string GetRandomString(int minLength, int maxLength, IEnumerable<char> allowedCharacters = null)
        {
            if (minLength < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(minLength));
            }

            if (maxLength < minLength || maxLength < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxLength));
            }

            var random = new Random();
            var length = random.Next(minLength, maxLength);

            return GetRandomString(length, allowedCharacters);
        }

        /// <summary>
        /// Break string into array of string according to new line marks
        /// </summary>
        /// <param name="content"></param>
        /// <returns>array of strings</returns>
        public static string[] BreakString(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return new string[0];
            }

            return Regex.Split(content, @"\r\n?|\n");
        }

        /// <summary>
        /// Replaces diacritics from value with ASCII-based standard character (ISO-8859-8).
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Value with diacritics</returns>
        public static string RemoveDiacritics(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value), @"Value can not be null");
            }

            value = value.Replace("ß", "ss")
                .Replace("œ", "oe")
                .Replace("æ", "ae");

            var tempBytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(value);

            return Encoding.UTF8.GetString(tempBytes);
        }

        /// <summary>
        /// Reduce length of the given text to the specified limit and trying
        /// not to cut words and also add '…' at the end. Example: "This is test"
        /// with limit 10 would return "This is…"
        /// </summary>
        /// <param name="text">Given text to truncate</param>
        /// <param name="limit">Maximum number of characters that we want to present</param>
        /// <returns>Truncated text</returns>
        public static string TruncateAtWords(string text, int limit)
        {
            if (limit <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit));
            }

            if (text == null)
            {
                return null;
            }

            if (text.Length > limit)
            {
                const char ellipsis = '…';
                const char wordSeparator = ' ';

                var wordEndIndex = text.LastIndexOf(wordSeparator, limit, limit);

                return wordEndIndex > 0
                    ? text.Substring(0, wordEndIndex) + ellipsis
                    : text.Substring(0, limit - 1) + ellipsis;
            }

            return text;
        }

        public static IEnumerable<Match> GetRegexMatches(string regex, string text)
        {
            return new Regex(regex)
                    .Matches(text)
                    .Cast<Match>();
        }
    }
}