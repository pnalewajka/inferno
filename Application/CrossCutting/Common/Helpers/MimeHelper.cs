﻿namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class MimeHelper
    {
        public const string AnyImage = "image/*";
        public const string OfficeDocumentExcel = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string CommaSeparatedValuesFile = "text/csv";
        public const string ZipFile = "application/zip, application/octet-stream";
        public const string OfficeDocumentPowerPoint = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        public const string OfficeDocumentWord = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        public const string OldOfficeDocumentWord = "application/msword";
        public const string PngImageFile = "image/png";
        public const string JpegImageFile = "image/jpeg";
        public const string PdfFile = "application/pdf";
        public const string JavaScriptFile = "application/javascript";
        public const string BinaryFile = "application/octet-stream";
        public const string CompressedZipFile = "application/x-zip-compressed";
        public const string TextFile = "text/plain";
        public const string HtmlFile = "text/html";
        public const string XmlFile = "text/xml";
    }
}