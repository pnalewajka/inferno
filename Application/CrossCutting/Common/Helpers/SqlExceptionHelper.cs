﻿using System;
using System.Data.SqlClient;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public class SqlExceptionHelper
    {
        /// <summary>
        /// Get first occurence of exception of sql exception with specific message id and traversing through InnerException
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static SqlException GetFirstSqlExceptionOrDefault(Exception ex, long messageId) 
        {
            while (ex != null)
            {
                var result = ex as SqlException;

                if (result != null && result.Number == messageId)
                {
                    return result;
                }

                ex = ex.InnerException;
            }

            return null;
        }
    }
}
