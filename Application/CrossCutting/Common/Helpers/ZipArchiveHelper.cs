﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Zip compression helper methods
    /// </summary>
    public static class ZipArchiveHelper
    {
        /// <summary>
        /// Returns file name followed with proper zip file extension.
        /// </summary>
        public static string GetZipFileName(string name)
        {
            const string zipFileExtension = "zip";
            const char fileNameSeperator = '.';

            return $"{name}{fileNameSeperator}{zipFileExtension}";
        }

        /// <summary>
        /// Creates zip file converted to bytes array containing provided files.
        /// </summary>
        /// <typeparam name="TArchivable"></typeparam>
        /// <param name="files">Files to compress</param>
        /// <returns>Zip file converted to bytes array</returns>
        public static byte[] GetZipArchiveBytes<TArchivable>(IEnumerable<TArchivable> files)
            where TArchivable : IArchivable
        {
            using (var memoryStream = new MemoryStream())
            {
                var zipArchive = GetZipArchive(memoryStream, files);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Creates zip file containing provided files.
        /// </summary>
        /// <typeparam name="TIArchivable"></typeparam>
        /// <param name="stream"></param>
        /// <param name="files">Files to compress</param>
        /// <returns></returns>
        public static ZipArchive GetZipArchive<TIArchivable>(Stream stream, IEnumerable<TIArchivable> files)
            where TIArchivable : IArchivable
        {
            using (var zipArchive = new ZipArchive(stream, ZipArchiveMode.Create, true))
            {
                foreach (var file in files)
                {
                    var entry = zipArchive.CreateEntry(file.FileName);

                    using (var entryStream = entry.Open())
                    using (var streamWriter = new BinaryWriter(entryStream))
                    {
                        streamWriter.Write(file.Content);
                    }
                }

                return zipArchive;
            }
        }

        /// <summary>
        /// Unzip file and return files. Based on bytes is created Memory Stream, which is disposed at the end of method.
        /// </summary>
        /// <param name="zippedArray">Zipped content</param>
        /// <returns>Files inside zipped</returns>
        public static IEnumerable<UnzippedFile> UnzipFilesFromByteArray(byte[] zippedArray)
        {
            using (var stream = new MemoryStream(zippedArray))
            {
                return UnzipFilesFromByteArray(stream);
            }
        }

        /// <summary>
        /// Unzip file and return files. 
        /// </summary>
        /// <param name="stream">Zipped content stream</param>
        /// <returns>Files inside zipped</returns>
        public static IEnumerable<UnzippedFile> UnzipFilesFromByteArray(Stream stream)
        {
            using (var archive = new ZipArchive(stream))
            {
                return archive.Entries.Select(entry =>
                {
                    var bytes = GetBytesFromZipArchiveEntry(entry);
                    var fileName = entry.Name;
                    var mimeType = MimeMapping.GetMimeMapping(fileName);

                    return new UnzippedFile { Content = bytes, FileName = fileName, MimeType = mimeType };
                }).ToList();
            }
        }

        private static byte[] GetBytesFromZipArchiveEntry(ZipArchiveEntry entry)
        {
            using (var zipFileStream = entry.Open())
            using (var copyStream = new MemoryStream())
            {
                zipFileStream.CopyTo(copyStream);

                return copyStream.ToArray();
            }
        }
    }
}
