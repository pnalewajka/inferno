﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public class DateRangeHelper
    {
        private const DateComparer DefaultDateComparer = DateComparer.LeftClosed | DateComparer.RightClosed;

        public static TimeSpan? GetOverlapingInterval(DateTime firstFrom, DateTime firstTo, DateTime secondFrom, DateTime secondTo)
        {
            var interval = DateHelper.Min(firstTo, secondTo) - DateHelper.Max(firstFrom, secondFrom);

            return interval > TimeSpan.Zero ? interval : (TimeSpan?)null;
        }

        public static bool IsOverlapingDateRange(
            DateTime recordFrom,
            DateTime? recordTo,
            DateTime checkFrom,
            DateTime checkTo,
            DateComparer comparer = DefaultDateComparer)
        {
            var comparerExpression = GetOverlapingRangesComparer(comparer);

            return comparerExpression.Call(checkFrom, checkTo, recordFrom, recordTo);
        }

        public static Expression<Func<TEntity, bool>> OverlapingDateRange<TEntity>(
            Expression<Func<TEntity, DateTime>> fromPropertyExpression,
            Expression<Func<TEntity, DateTime?>> toPropertyExpression,
            DateTime from,
            DateTime to,
            DateComparer comparer = DefaultDateComparer)
            where TEntity : class
        {

            var fromSelector = new BusinessLogic<TEntity, DateTime>(fromPropertyExpression);
            var toSelector = new BusinessLogic<TEntity, DateTime?>(toPropertyExpression);
            var comparerExpression = GetOverlapingRangesComparer(comparer);

            var predicate = new BusinessLogic<TEntity, bool>(e =>
                comparerExpression.Call(from, to, fromSelector.Call(e), toSelector.Call(e)));

            return predicate;
        }

        private static BusinessLogic<DateTime, DateTime, DateTime, DateTime?, bool> GetOverlapingRangesComparer(DateComparer type)
        {
            BusinessLogic<DateTime, DateTime, bool> left;
            BusinessLogic<DateTime, DateTime?, bool> right;

            left = type.HasFlag(DateComparer.LeftClosed)
                ? new BusinessLogic<DateTime, DateTime, bool>((d, l) => l <= d)
                : new BusinessLogic<DateTime, DateTime, bool>((d, l) => l < d);

            right = type.HasFlag(DateComparer.RightClosed)
                ? new BusinessLogic<DateTime, DateTime?, bool>((d, r) => !r.HasValue || d <= r.Value)
                : new BusinessLogic<DateTime, DateTime?, bool>((d, r) => !r.HasValue || d < r.Value);

            return new BusinessLogic<DateTime, DateTime, DateTime, DateTime?, bool>(
                (l0, r0, l1, r1) => left.Call(r0, l1) && right.Call(l0, r1));
        }

        private static BusinessLogic<DateTime, DateTime, DateTime?, bool> GetDateVsRangeComparer(DateComparer type)
        {
            BusinessLogic<DateTime, DateTime, bool> left;
            BusinessLogic<DateTime, DateTime?, bool> right;

            left = type.HasFlag(DateComparer.LeftClosed)
                ? new BusinessLogic<DateTime, DateTime, bool>((d, l) => l <= d)
                : new BusinessLogic<DateTime, DateTime, bool>((d, l) => l < d);

            right = type.HasFlag(DateComparer.RightClosed)
                ? new BusinessLogic<DateTime, DateTime?, bool>((d, r) => !r.HasValue || d <= r.Value)
                : new BusinessLogic<DateTime, DateTime?, bool>((d, r) => !r.HasValue || d < r.Value);

            return new BusinessLogic<DateTime, DateTime, DateTime?, bool>(
                (d, l, r) => left.Call(d, l) && right.Call(d, r));
        }

        public static BusinessLogic<TEntity, DateTime, bool> IsDateInRange<TEntity>(
            Expression<Func<TEntity, DateTime>> fromSelector,
            Expression<Func<TEntity, DateTime?>> toSelector,
            DateComparer comparer = DefaultDateComparer)
        {
            var from = new BusinessLogic<TEntity, DateTime>(fromSelector);
            var to = new BusinessLogic<TEntity, DateTime?>(toSelector);

            var comparerExpression = GetDateVsRangeComparer(comparer);

            return new BusinessLogic<TEntity, DateTime, bool>(
                (e, date) => comparerExpression.Call(date, from.Call(e), to.Call(e)));
        }

        public static BusinessLogic<DateTime, bool> GetIsDateInRangeBusinessLogic(
            DateTime from,
            DateTime? to,
            DateComparer comparer = DefaultDateComparer)
        {
            var comparerExpression = GetDateVsRangeComparer(comparer);

            return new BusinessLogic<DateTime, bool>(d => comparerExpression.Call(d, from, to));
        }

        public static BusinessLogic<TEntity, bool> IsDateInRange<TEntity>(
            Expression<Func<TEntity, DateTime>> fromSelector,
            Expression<Func<TEntity, DateTime?>> toSelector,
            DateTime date,
            DateComparer comparer = DefaultDateComparer)
        {
            return IsDateInRange(fromSelector, toSelector, comparer).Parametrize(date);
        }

        public static BusinessLogic<TEntity, bool> IsDateInRange<TEntity>(
            DateTime from,
            DateTime? to,
            Expression<Func<TEntity, DateTime>> dateSelector,
            DateComparer comparer = DefaultDateComparer)
        {
            var date = new BusinessLogic<TEntity, DateTime>(dateSelector);
            var comparerExpression = GetDateVsRangeComparer(comparer);

            return new BusinessLogic<TEntity, bool>(
                e => comparerExpression.Call(date.Call(e), from, to));
        }
    }
}