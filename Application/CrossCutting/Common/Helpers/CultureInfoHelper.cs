﻿using System;
using System.Globalization;
using System.Threading;

// ReSharper disable InconsistentNaming
namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class CultureInfoHelper
    {
        public static string GetJavaScriptDateFormat(CultureInfo cultureInfo = null)
        {
            if (cultureInfo == null)
            {
                cultureInfo = Thread.CurrentThread.CurrentCulture;
            }

            var result = new DateTime(2013, 2, 1).ToString("d", cultureInfo);

            result = result.Replace("2013", "yyyy");
            result = result.Replace("02", "mm");
            result = result.Replace("2", "m");
            result = result.Replace("01", "dd");
            result = result.Replace("1", "d");

            return result;
        }

        public static string GetDecimalSeparator(CultureInfo cultureInfo = null)
        {
            if (cultureInfo == null)
            {
                cultureInfo = Thread.CurrentThread.CurrentCulture;
            }

            return cultureInfo.NumberFormat.NumberDecimalSeparator;
        }

        public static IDisposable SetCurrentCulture(CultureInfo newCulture)
        {
            var previousCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = newCulture;

            return new CurrentCultureSnapshot(previousCulture);
        }

        public static IDisposable SetCurrentUICulture(CultureInfo newUICulture)
        {
            var previousCulture = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = newUICulture;

            return new CurrentUICultureSnapshot(previousCulture);
        }

        private sealed class CurrentCultureSnapshot : IDisposable
        {
            private readonly CultureInfo _cultureInfo;

            public CurrentCultureSnapshot(CultureInfo cultureInfo)
            {
                _cultureInfo = cultureInfo;
            }

            public void Dispose()
            {
                Thread.CurrentThread.CurrentCulture = _cultureInfo;
            }
        }

        private sealed class CurrentUICultureSnapshot : IDisposable
        {
            private readonly CultureInfo _cultureInfo;

            public CurrentUICultureSnapshot(CultureInfo cultureInfo)
            {
                _cultureInfo = cultureInfo;
            }

            public void Dispose()
            {
                Thread.CurrentThread.CurrentUICulture = _cultureInfo;
            }
        }
    }
}