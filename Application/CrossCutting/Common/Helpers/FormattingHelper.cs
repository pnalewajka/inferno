﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Commonly used formatting aides
    /// </summary>
    public class FormattingHelper
    {
        /// <summary>
        /// Returns a list of prefix-style separators that can be used for user-friendly collection formatting ('or' scenario).
        /// </summary>
        /// <param name="collectionCount">Number of items in the collection</param>
        /// <returns>Separators that need to be inserted before each element</returns>
        public static string[] GetOrSeparators(int collectionCount)
        {
            if (collectionCount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(collectionCount));
            }

            switch (collectionCount)
            {
                case 0:
                    return new string[0];

                case 1:
                    return new[] {string.Empty};

                case 2:
                    return new[] {string.Empty, FormattingResources.TwoItemOr};

                default:
                    var separators = new List<string> {string.Empty};

                    for (var i = 1; i < collectionCount - 1; i++)
                    {
                        separators.Add(", ");
                    }

                    separators.Add(FormattingResources.ManyItemOr);

                    return separators.ToArray();
            }
        }
    }
}