﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    using System.Data.SqlTypes;

    /// <summary>
    /// Date helpers
    /// </summary>
    public static class DateHelper
    {
        private const int WeeksLengthInDays = 7;

        /// <summary>
        /// Convert unix epoch time to datetime
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public static DateTime FromUnixEpochTime(long unixTime)
        {
            var d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            d = d.AddSeconds(unixTime);

            return d.ToLocalTime();
        }

        /// <summary>
        /// Parse timespan from ISO 8601 duration format
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static TimeSpan FromIso8601Duration(string duration)
        {
            try
            {
                return XmlConvert.ToTimeSpan(duration);
            }
            catch (FormatException)
            {
                return TimeSpan.Zero;
            }
        }

        /// <summary>
        /// Convert date time to unix epoch, please ensure using UTC as input datetime
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int ToUnixEpochTime(DateTime date)
        {
            var timeSpan = date - new DateTime(1970, 1, 1);

            return (int)timeSpan.TotalSeconds;
        }

        /// <summary>
        /// Calculate befinning of month
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime BeginningOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Calculate befinning of month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static DateTime BeginningOfMonth(int year, byte month)
        {
            try
            {
                return new DateTime(year, month, 1);
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"Given year ({year}) and month ({month}) are invalid and can't be used to create valid date.", ex);
            }
        }

        /// <summary>
        /// Calculate end of month
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime EndOfMonth(DateTime date)
        {
            return BeginningOfMonth(date).AddMonths(1).AddSeconds(-1);
        }

        /// <summary>
        /// Calculate end of month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static DateTime EndOfMonth(int year, byte month)
        {
            return BeginningOfMonth(year, month).AddMonths(1).AddSeconds(-1);
        }

        /// <summary>
        /// Returns all days in a given month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);

            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        /// <summary>
        /// Returns all weeks in given month where week number is a key and value contains count of days within this week
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static IEnumerable<WeekFromMonth> AllWeeksInMonth(int year, int month)
        {
            var days = 0;
            var dates = AllDatesInMonth(year, month).ToArray();
            var lastDayOfMonth = dates.Last();

            foreach (var date in dates)
            {
                days++;

                if (date.DayOfWeek == DayOfWeek.Sunday || date == lastDayOfMonth)
                {
                    yield return new WeekFromMonth()
                    {
                        WeekNumber = date.GetWeekOfYear(),
                        DaysInWeek = days,
                    };

                    days = 0;
                }
            }
        }

        /// <summary>
        /// Returns number of months since 1 AD
        /// </summary>
        public static int MonthsInDate(DateTime date)
        {
            return date.Year * 12 + date.Month;
        }

        /// <summary>
        /// Returns number of months between two dates
        /// </summary>
        /// <param name="start">Start date</param>
        /// <param name="end">End date</param>
        /// <returns></returns>
        public static int MonthsInRange(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new ArgumentException($"{start} can't be greater than e {end}");
            }

            return (end.Month - start.Month + 1) + 12 * (end.Year - start.Year);
        }

        /// <summary>
        /// Returns the earliest of given dates
        /// </summary>
        public static DateTime Min(params DateTime[] dates) => dates.Min();

        /// <summary>
        /// Returns the latest of given dates
        /// </summary>
        public static DateTime Max(params DateTime[] dates) => dates.Max();

        public static DateTime AddWeeks(DateTime dateTime, int weeks)
        {
            return dateTime.AddDays(weeks * WeeksLengthInDays);
        }

        public static bool IsBetweenDates(this DateTime? date, DateTime from, DateTime to)
        {
            return from <= date.GetMinDateIfDefault() && to >= date.GetMinDateIfDefault();
        }

        public static DateTime GetMinDateIfDefault(this DateTime? date)
        {
            return date ?? (DateTime)SqlDateTime.MinValue;
        }

        public static IEnumerable<DateTime> GetDaysInRange(DateTime from, DateTime to, bool includeWeekends = true)
        {
            var count = to.Date.Subtract(from.Date).Days + 1;

            for (var offset = 0; offset < count; offset++)
            {
                var date = from.Date.AddDays(offset);
                var skipDateBecauseOfWeekend = !includeWeekends && date.IsWeekend();

                if (skipDateBecauseOfWeekend)
                {
                    continue;
                }

                yield return date;
            }
        }

        public static List<int> WeeksInRange(DateTime start, DateTime end)
        {
            return GetDaysInRange(start, end).Select(d => d.GetWeekOfYear()).Distinct().ToList();
        }

        public static IEnumerable<DateTime> AllMonthsInRange(DateTime start, DateTime end)
        {
            var iterator = start.GetFirstDayInMonth();
            var iteratorEnd = end.GetFirstDayInMonth();

            while (iterator <= iteratorEnd)
            {
                yield return iterator;
                iterator = iterator.AddMonths(1);
            }
        }
    }
}
