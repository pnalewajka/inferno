﻿using System;
using System.IO;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Enum helper methods
    /// </summary>
    public class EnumHelper
    {
        /// <summary>
        /// TryParse for dynamically typed enum
        /// </summary>
        public static bool TryParse(Type enumType, string value, out Enum result)
        {
            var tryParseMethodArgs = new object[] { value, null };
            var tryParseMethod = typeof(Enum).GetMethods()
                .Where(m => m.Name == nameof(Enum.TryParse))
                .Single(m => m.GetParameters().Length == 2)
                .MakeGenericMethod(enumType);

            var didParseSucceed = (bool)tryParseMethod.Invoke(null, tryParseMethodArgs);
            var parseResult = tryParseMethodArgs[1]; // out value

            // flagged enums don't require to be explicitly defined
            didParseSucceed &= TypeHelper.IsFlaggedEnum(enumType) || Enum.IsDefined(enumType, parseResult);

            result = didParseSucceed
                ? (Enum)parseResult
                : default(Enum);

            return didParseSucceed;
        }

        /// <summary>
        /// TryParse for dynamically typed enum
        /// </summary>
        public static bool TryParse<TEnum>(string value, out TEnum result)
        {
            var succeeded = TryParse(typeof(TEnum), value, out var interalResult);

            result = succeeded
                ? (TEnum)(object)interalResult
                : default(TEnum);

            return succeeded;
        }

        /// <summary>
        /// Parse enum value from string
        /// </summary>
        /// <typeparam name="TEnum">Enum type</typeparam>
        /// <param name="stringValue">Value to parse</param>
        /// <returns>Enum value matching given string</returns>
        public static TEnum GetEnumValue<TEnum>([NotNull] string stringValue)
        {
            if (stringValue == null)
            {
                throw new ArgumentNullException(nameof(stringValue));
            }

            return (TEnum)Enum.Parse(typeof(TEnum), stringValue, false);
        }

        /// <summary>
        /// Get enum value based on the field name. If not valid, returns null.
        /// Field name can be given in a transformed form. It makes sense to provide the transformation function then, to allow consistent name matching.
        /// </summary>
        /// <typeparam name="TEnum">Enum type</typeparam>
        /// <param name="fieldName">Enum field name</param>
        /// <param name="nameTransformation">Name transformation to be applied on field names (e.g. hyphenation)</param>
        /// <returns>Enum field value or null</returns>
        public static TEnum? GetEnumValueOrDefault<TEnum>(string fieldName, Func<string, string> nameTransformation = null)
            where TEnum : struct 
        {
            if (fieldName == null)
            {
                return null;
            }

            nameTransformation = nameTransformation ?? SameNameTransformation;

            var allowedValues = GetEnumValues<TEnum>()
                .Select(v => new
                    {
                        Value = v,
                        Name = nameTransformation(v.ToString())
                    })
                .ToDictionary
                (
                    v => v.Name,
                    v => v.Value
                );
            
            if (!allowedValues.ContainsKey(fieldName))
            {
                return null;
            }

            return allowedValues[fieldName];
        }

        /// <summary>
        /// Get all values of specified enum
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns>Array of enum values</returns>
        public static TEnum[] GetEnumValues<TEnum>()
        {
            var enumType = typeof(TEnum);
            
            return (TEnum[])Enum.GetValues(enumType);
        }

        /// <summary>
        /// Get names of all enum values
        /// </summary>
        /// <typeparam name="TEnum">Enum to examine</typeparam>
        /// <returns>Array of enum field names</returns>
        public static IEnumerable<string> GetEnumNames<TEnum>()
        {
            var values = GetEnumValues<TEnum>();

            return values.Select(v => v.ToString());
        }

        /// <summary>
        /// Get names of all enum values
        /// </summary>
        /// <param name="enumType">Enum to examine</param>
        /// <returns>Array of enum field names</returns>   
        public static IEnumerable<string> GetEnumNames(Type enumType)
        {
            var values = Enum.GetValues(enumType).OfType<object>();
            
            return values.Select(v => v.ToString());
        }

        /// <summary>
        /// Get all values of specified enum
        /// </summary>
        /// <param name="enumType">type of the enum</param>
        /// <returns></returns>
        public static Enum[] GetEnumValues([NotNull] Type enumType)
        {
            if (enumType == null)
            {
                throw new ArgumentNullException(nameof(enumType));
            }

            return Enum.GetValues(enumType).Cast<Enum>().ToArray();
        }

        /// <summary>
        /// Gets the value which is out of range for enum
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <returns>Value which is not covered by enum values</returns>
        public static T GetOutOfRangeValue<T>()
        {
            return (T) GetOutOfRangeValue(typeof (T));
        }

        /// <summary>
        /// Gets the value which is out of range for enum
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static object GetOutOfRangeValue(Type enumType)
        {
            var existingEnumValues = GetEnumValues(enumType);
            var min = existingEnumValues.Min();

            if (Convert.ToInt64(min) != long.MinValue)
            {
                var result = Convert.ToInt64(min) - 1;

                return Enum.ToObject(enumType, result);
            }

            var max = existingEnumValues.Max();

            if (Convert.ToInt64(min) != long.MaxValue)
            {
                var result = Convert.ToInt64(max) + 1;

                return Enum.ToObject(enumType, result);
            }

            throw new InvalidDataException();
        }

        /// <summary>
        /// Get enum value from attribute based on predicate
        /// </summary>
        /// <typeparam name="TEnum">Enum type to get</typeparam>
        /// <typeparam name="TAttribute">Attribute type</typeparam>
        /// <param name="searchPredicate">A function to detect enum value</param>
        /// <returns>Enum value satisfying predicate</returns>
        /// <exception cref="Smt.Atomic.CrossCutting.Common.Exceptions.GenericParameterException">
        /// <typeparamref name="TEnum" />
        /// parameter is not an <see cref="Enum"/> type
        /// </exception>
        public static TEnum GetEnumByAttribute<TEnum, TAttribute>([NotNull] Func<TAttribute, bool> searchPredicate)
            where TEnum : struct, IComparable, IFormattable, IConvertible
            where TAttribute : Attribute
        {
            if (searchPredicate == null)
            {
                throw new ArgumentNullException(nameof(searchPredicate));
            }

            if (false == typeof(TEnum).IsEnum)
            {
                throw new GenericParameterException(typeof(TEnum), typeof(Enum));
            }

            return GetEnumValues<TEnum>()
                .Single(ev => searchPredicate(AttributeHelper.GetEnumFieldAttribute<TAttribute>(ev as Enum)));
        }

        /// <summary>
        /// Get value associated with specified enum value and attribute
        /// </summary>
        /// <typeparam name="TValueType">Type of value expected to get</typeparam>
        /// <typeparam name="TAttribute">Type of attribute</typeparam>
        /// <param name="enumValue">Enum to get associated value with</param>
        /// <param name="extractFunction">Method extracting value from attribute</param>
        /// <returns>Value associated with enum</returns>
        /// <exception cref="T:Smt.Atomic.CrossCutting.Common.Exceptions.AttributeNotFoundException">
        /// <paramref name="enumValue" />
        /// does not contain
        /// <typeparamref name="TAttribute"/>
        /// </exception>
        public static TValueType GetValueAssignedToEnum<TValueType, TAttribute>(Enum enumValue, [NotNull] Func<TAttribute, TValueType> extractFunction)
            where TAttribute : Attribute
        {
            if (extractFunction == null)
            {
                throw new ArgumentNullException(nameof(extractFunction));
            }

            var attr = AttributeHelper.GetEnumFieldAttribute<TAttribute>(enumValue);

            if (attr == null)
            {
                throw new AttributeNotFoundException<TAttribute>();
            }

            return extractFunction(attr);
        }

        public static string ToUrlString(Enum enumValue)
        {
            if (enumValue == null)
            {
                return string.Empty;
            }

            var stringValue = enumValue.ToString();
            return NamingConventionHelper.ConvertPascalCaseToHyphenated(stringValue);
        }

        private static string SameNameTransformation(string name)
        {
            return name;
        }

        /// <summary>
        /// Gets a list of enum values from a given enum value.
        /// For enums with FlagsAttribute it will split their value on its parts.
        /// </summary>
        /// <param name="enumValue">Given enum value </param>
        /// <returns>List of the enum flags that are set for a given value.</returns>
        public static IEnumerable<Enum> GetFlags(Enum enumValue)
        {
            var enumType = enumValue.GetType();
            if (TypeHelper.IsFlaggedEnum(enumType))
            {
                foreach (Enum value in Enum.GetValues(enumType))
                {
                    if (Convert.ToInt64(value) != 0 && enumValue.HasFlag(value))
                    {
                        yield return value;
                    }
                }
            }
            else
            {
                yield return enumValue;
            }
        }

        /// <summary>
        /// Gets aggregated enum value for a given list of enum flags and enum type
        /// </summary>
        /// <param name="enumType">Enum type</param>
        /// <param name="enumFlags">List of enum flag names to aggregate</param>
        /// <returns>Aggregated enum value</returns>
        public static object GetEnumValue(Type enumType, IEnumerable<string> enumFlags)
        {
            var enumValues = new List<long>();

            foreach (var flag in enumFlags)
            {
                object enumFlag = Enum.Parse(enumType, flag);
                
                if (Enum.GetUnderlyingType(enumType) == typeof(long))
                {
                    enumValues.Add((long)enumFlag);
                }
                else if (Enum.GetUnderlyingType(enumType) == typeof(int))
                {
                    enumValues.Add((int)enumFlag);
                }
            }

            var flags = enumValues.Aggregate<long, long>(0, (current, flag) => current | flag);

            return Enum.ToObject(enumType, flags);
        }
    }
}
