﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class NamingConventionHelper
    {
        /// <summary>
        /// Convert from PascalCaseName to pascal-case-name
        /// </summary>
        /// <param name="pascalCaseInput">input in PascalCase</param>
        /// <returns>input converted to hyphenated-input</returns>
        public static string ConvertPascalCaseToHyphenated([NotNull] string pascalCaseInput)
        {
            var result = new StringBuilder(pascalCaseInput.Length);

            foreach (var character in pascalCaseInput)
            {
                if (char.IsUpper(character))
                {
                    result.Append('-');
                    result.Append(char.ToLower(character));
                }
                else
                {
                    result.Append(character);
                }
            }

            return result.ToString().Trim('-');
        }

        /// <summary>
        /// Convert from PascalCaseName to camelCaseName
        /// </summary>
        /// <param name="pascalCaseInput">input in PascalCase</param>
        /// <returns>input converted to camel case</returns>
        public static string ConvertPascalCaseToCamelCase([NotNull] string pascalCaseInput)
        {
            if (pascalCaseInput == null)
            {
                throw new ArgumentNullException(nameof(pascalCaseInput));
            }

            if (pascalCaseInput.Length < 2)
            {
                return pascalCaseInput;
            }

            return pascalCaseInput.Substring(0, 1).ToLowerInvariant() + pascalCaseInput.Substring(1);
        }

        /// <summary>
        /// Generate label based on the pascal case input
        /// </summary>
        /// <param name="pascalCaseInput">input in pascal case</param>
        /// <returns>input split into separate words</returns>
        public static string ConvertPascalCaseToLabel(string pascalCaseInput)
        {
            var result = new StringBuilder(pascalCaseInput.Length);

            foreach (var character in pascalCaseInput)
            {
                if (char.IsUpper(character))
                {
                    result.Append(' ');
                }

                result.Append(character);
            }

            return result.ToString().Trim();
        }

        /// <summary>
        /// Converts hyphenated-input to PascalCase
        /// </summary>
        /// <param name="hypenatedInput">input in hyphanated-format</param>
        /// <returns>result in PascalCaseFormat</returns>
        public static string ConvertHyphenatedToPascalCase(string hypenatedInput)
        {
            var result = new StringBuilder(hypenatedInput.Length);
            bool shouldCapitalise = true;

            foreach (var c in hypenatedInput)
            {
                if (c == '-')
                {
                    shouldCapitalise = true;
                }
                else
                {
                    if (shouldCapitalise)
                    {
                        result.Append(char.ToUpper(c));
                        shouldCapitalise = false;
                    }
                    else
                    {
                        result.Append(c);
                    }
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Converts hyphenated-input to camelCase
        /// </summary>
        /// <param name="hypenatedInput">input in hyphanated-format</param>
        /// <returns>result in camelCaseFormat</returns>
        public static string ConvertHyphenatedToCamelCase(string hypenatedInput)
        {
            var result = new StringBuilder(hypenatedInput.Length);
            bool shouldCapitalise = false;

            foreach (var c in hypenatedInput)
            {
                if (c == '-')
                {
                    shouldCapitalise = true;
                }
                else
                {
                    if (shouldCapitalise)
                    {
                        result.Append(char.ToUpper(c));
                        shouldCapitalise = false;
                    }
                    else
                    {
                        result.Append(c);
                    }
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Converts given string to class-plausible identifier
        /// </summary>
        /// <param name="s">String to convert</param>
        /// <returns>String that can be used as class name</returns>
        public static string ConvertToClassName([NotNull] string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            s = StringHelper.RemoveDiacritics(s);
            s = s.Replace(' ', '-');
            s = s.Replace('.', '-');
            s = ConvertHyphenatedToPascalCase(s);

            s = Regex.Replace(s, "\\W", string.Empty);

            if (s.Length == 0)
            {
                throw new ArgumentException(@"Cannot convert to a valid name.", nameof(s));
            }

            // make sure the name does not start with a number
            if (s[0] >= '0' && s[1] <= '9')
            {
                return $"_{s}";
            }

            return s;
        }

        /// <summary>
        /// Convert given string to pascal case
        /// </summary>
        /// <param name="camelCaseInput"></param>
        /// <returns></returns>
        public static string ConvertCamelCaseToPascalCase([NotNull] string camelCaseInput)
        {
            if (camelCaseInput.IsNullOrEmpty())
            {
                return camelCaseInput;
            }

            char[] pascalCaseString = camelCaseInput.ToCharArray();
            pascalCaseString[0] = char.ToUpper(pascalCaseString[0]);

            return new string(pascalCaseString);
        }
    }
}
