﻿using System.Drawing;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class ColorHelper
    {
        public static bool IsDark(string color, float thresholdValue = 0.5f)
        {
            if (string.IsNullOrEmpty(color))
            {
                return false;
            }

            var rgbColor = ColorTranslator.FromHtml(color);
            var brightness = 1 - (0.299 * rgbColor.R + 0.587 * rgbColor.G + 0.114 * rgbColor.B) / 255;

            return brightness >= thresholdValue;
        }
    }
}