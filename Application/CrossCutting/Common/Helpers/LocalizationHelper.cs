﻿using System;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class LocalizationHelper
    {
        /// <summary>
        /// Get localized string from specific resources
        /// </summary>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        public static string GetLocalizedString<TResourceType>([NotNull] string resourceKey)
        {
            if (resourceKey == null)
            {
                throw new ArgumentNullException(nameof(resourceKey));
            }

            return GetLocalizedStringOrDefault(typeof(TResourceType), resourceKey);
        }

        /// <summary>
        /// Get localized string from specific resources, falling back to returning resource key value if resource type not provided
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        public static string GetLocalizedStringOrDefault(Type resourceType, string resourceKey)
        {
            if (resourceType == null)
            {
                return resourceKey;
            }

            if (resourceKey == null)
            {
                return null;
            }

            var resourceManager = ResourceManagerHelper.GetResourceManager(resourceType);

            return resourceManager.GetString(resourceKey);
        }
    }
}
