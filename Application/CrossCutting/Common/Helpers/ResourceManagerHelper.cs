﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class ResourceManagerHelper
    {
        private const string ResourceManagerPropertyName = "ResourceManager";

        /// <summary>
        /// Returns all strings from a given resource file for a given culture, 
        /// optionally filtered by a regular expression applied to the key
        /// </summary>
        /// <param name="resourceType">Resource manager type to retrieve strings from</param>
        /// <param name="culture">Culture info to be used for string retrieval</param>
        /// <param name="regex">Optional pattern to be used for resource keys</param>
        /// <returns>Dictionary with resource keys mapped to their string values</returns>
        public static IDictionary<string, string> GetAllStrings(Type resourceType, CultureInfo culture, string regex = null)
        {
            if (resourceType == null)
            {
                throw new ArgumentNullException(nameof(resourceType));
            }

            var resourceManager = GetResourceManager(resourceType);

            if (resourceManager == null)
            {
                throw new ArgumentNullException(nameof(resourceManager));
            }

            if (culture == null)
            {
                throw new ArgumentNullException(nameof(culture));
            }

            var set = resourceManager.GetResourceSet(culture, true, true);

            return set.Cast<DictionaryEntry>()
                .Where(kv => regex == null || kv.Key.ToString().Matches(regex))
                      .ToDictionary
                (
                    item => (string) item.Key,
                    item => (string) item.Value
                );
        }

        /// <summary>
        /// Retrieves resource manager from the provided resources class.
        /// </summary>
        /// <typeparam name="TResources">Resources class</typeparam>
        /// <returns>Resource manager for the specified resources</returns>
        public static ResourceManager GetResourceManager<TResources>()
        {
            return PropertyHelper.GetStaticPropertyValue<TResources, ResourceManager>(ResourceManagerPropertyName, true);
        }

        /// <summary>
        /// Retrieves resource manager from the provided resources class.
        /// </summary>
        /// <param name="resourcesType">Resources class</param>
        /// <returns>Resource manager for the specified resources</returns>
        public static ResourceManager GetResourceManager(Type resourcesType)
        {
            return PropertyHelper.GetStaticPropertyValue<ResourceManager>(resourcesType, ResourceManagerPropertyName, true);
        }
    }
}
