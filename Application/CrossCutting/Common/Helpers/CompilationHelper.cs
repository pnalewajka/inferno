﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Castle.Core.Logging;
using Microsoft.CSharp;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Provides facility for on-demand code compilation
    /// </summary>
    public static class CompilationHelper
    {
        private static readonly IDictionary<string, Assembly> Assemblies = new Dictionary<string, Assembly>();

        /// <summary>
        /// Returns an assembly compiled from the specified source files
        /// </summary>
        /// <param name="fileNames">Source files to be included in compilation</param>
        /// <param name="referencedAssemblies">Names of assemblies to reference in compilation</param>
        /// <returns>Compiled assembly</returns>
        public static Assembly Compile([NotNull] string[] fileNames, [NotNull] IEnumerable<string> referencedAssemblies)
        {
            if (fileNames == null)
            {
                throw new ArgumentNullException(nameof(fileNames));
            }

            if (referencedAssemblies == null)
            {
                throw new ArgumentNullException(nameof(referencedAssemblies));
            }

            var hash = CalculateHash(fileNames);

            if (Assemblies.ContainsKey(hash))
            {
                return Assemblies[hash];
            }

            var provider = ConfigureCSharpProvider();
            var compilerParams = GetCompilerParameters(referencedAssemblies);

            var result = provider.CompileAssemblyFromFile(compilerParams, fileNames.ToArray());

            return GetCachedAssembly(result, hash);
        }

        /// <summary>
        /// Returns an assembly compiled from the specified source code
        /// </summary>
        /// <param name="sourceCode">Source code to be included in compilation</param>
        /// <param name="referencedAssemblies">Names of assemblies to reference in compilation</param>
        /// <returns>Compiled assembly</returns>
        public static Assembly Compile(string sourceCode, IEnumerable<string> referencedAssemblies)
        {
            var hash = HashHelper.CalculateMd5Hash(sourceCode);

            if (Assemblies.ContainsKey(hash))
            {
                return Assemblies[hash];
            }

            var codeProvider = ConfigureCSharpProvider();
            var compilerParameters = GetCompilerParameters(referencedAssemblies);

            var result = codeProvider.CompileAssemblyFromSource(compilerParameters, sourceCode);

            return GetCachedAssembly(result, hash);
        }

        /// <summary>
        /// Lists locations of currently loaded assemblies that can be used as references for compilation methods.
        /// By defaults only System and Smt assemblies are taken.
        /// </summary>
        /// <returns>Locations of currently loaded assemblies</returns>
        public static IEnumerable<string> GetLoadedReferences(Func<Assembly, bool> assemblyFilteringPredicate = null)
        {
            IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain.GetAssemblies();

            if (assemblyFilteringPredicate == null)
            {
                assemblies = assemblies.Where(IsStandardAssembly);
            }

            return assemblies.Select(a => a.Location).ToList();
        }

        public static bool IsStandardAssembly(Assembly assembly)
        {
            return !assembly.IsDynamic                 
                && (assembly.FullName.StartsWith("System.") 
                    || assembly.FullName.StartsWith("Smt.Atomic")
                    || assembly.FullName.StartsWith("UnitTests"));
        }

        private static string CalculateHash(IEnumerable<string> fileNames)
        {
            var allFiles = new StringBuilder();

            foreach (var fileName in fileNames)
            {
                var fileContent = File.ReadAllText(fileName);
                allFiles.Append(fileContent);
            }

            var wholeContent = allFiles.ToString();

            return HashHelper.CalculateMd5Hash(wholeContent);
        }

        private static CSharpCodeProvider ConfigureCSharpProvider()
        {
            var providerOptions = new Dictionary<string, string>
                {
                    {"CompilerVersion", "v4.0"}
                };

            var provider = new CSharpCodeProvider(providerOptions);

            return provider;
        }

        private static CompilerParameters GetCompilerParameters(IEnumerable<string> referencedAssemblies)
        {
            var compilerParams = new CompilerParameters();

            foreach (var assembly in referencedAssemblies)
            {
                compilerParams.ReferencedAssemblies.Add(assembly);
            }

            compilerParams.GenerateInMemory = true;
            compilerParams.GenerateExecutable = false;
            compilerParams.OutputAssembly = Path.GetTempFileName();
#if DEBUG
            compilerParams.IncludeDebugInformation = true;
#endif

            var tempPath = Path.GetTempPath();
            compilerParams.TempFiles = new TempFileCollection(tempPath, false);

            return compilerParams;
        }

        private static Assembly GetCachedAssembly(CompilerResults result, string hash)
        {
            if (result.Errors.HasErrors)
            {
                var invalidOperationException = new InvalidOperationException(result.Errors[0].ErrorText);
                invalidOperationException.Data["errors"] = result.Errors;
                
                throw invalidOperationException;
            }

            Assemblies[hash] = result.CompiledAssembly;

            var logger = ReflectionHelper.ResolveInterface<ILogger>();
            logger.Warn($"Assembly compiled, number of assemblies: {Assemblies.Count}");

            return result.CompiledAssembly;
        }
    }
}