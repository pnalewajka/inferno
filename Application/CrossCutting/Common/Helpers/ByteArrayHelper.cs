﻿using System.Linq;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper methods for array of bytes
    /// </summary>
    public static class ByteArrayHelper
    {
        /// <summary>
        /// Splice array
        /// </summary>
        /// <param name="source"></param>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static byte[] Splice(byte[] source, int offset, int length)
        {
            if (source == null || source.Length == 0)
            {
                return null;
            }

            return source.Skip(offset).Take(length).ToArray();
        }
    }
}
