﻿using System.Linq;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class FullTextSearchHelper
    {
        private const string FullTextFreeText = "-FTSFULLTEXTFREETEXT-";

        public static string FreeText(string search, bool matchAllWords = false)
        {
            return $"({FullTextFreeText}{Convert(search, matchAllWords)})";
        }

        private static string Convert(string search, bool matchAllWords = false)
        {
            if (string.IsNullOrWhiteSpace(search)
                || !search.Contains(" ")
                || (search.StartsWith("\"") && search.EndsWith("\"")))
            {
                return search;
            }

            var words = search.Split(' ', '　');

            return matchAllWords
                ? string.Join(" and ", words.Where(c => c != "and"))
                : string.Join(" or ", words.Where(c => c != "or"));
        }
    }
}
