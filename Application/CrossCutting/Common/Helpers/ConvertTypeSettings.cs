using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Conversion settings used for serializing/deserializing
    /// </summary>
    public struct ConvertTypeSettings
    {
        public EnumNameConversion EnumNameConversion { get; set; }

        public IFormatProvider FormatProvider { get; set; }
    }
}