﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// 3DES crypt/encrypt methods
    /// </summary>
    public class TripleDesCryptoHelper
    {
        /// <summary>
        /// Encrypt string using provided password
        /// </summary>
        /// <param name="toEncrypt">Content for encryption</param>
        /// <param name="password">Password</param>
        /// <param name="useHashing">Indicates if password should be hashed, default true</param>
        /// <returns>Base64 encoded encrypted content</returns>
        public static string Encrypt(string toEncrypt, string password, bool useHashing = true)
        {
            if (toEncrypt == null)
            {
                return null;
            }

            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = GetCipherKey(password, useHashing),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// Decrypt string using provided password
        /// </summary>
        /// <param name="cipherString">Base64 encoded encrypted string</param>
        /// <param name="password">Password</param>
        /// <param name="useHashing">Indicates if password should be hashed, default true</param>
        /// <returns>Decrypted content. Returns empty string on problems </returns>
        public static string Decrypt(string cipherString, string password, bool useHashing = true)
        {
            if (cipherString == null)
            {
                return null;
            }

            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = GetCipherKey(password, useHashing),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            try
            {
                var cTransform = tdes.CreateDecryptor();
                var toEncryptArray = Convert.FromBase64String(cipherString);
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();

                return Encoding.UTF8.GetString(resultArray);
            }
            catch (CryptographicException)
            {
                return string.Empty;
            }
            catch (FormatException)
            {
                return string.Empty;
            }
        }

        private static byte[] GetCipherKey(string password, bool useHashing)
        {
            if (!useHashing)
            {
                return Encoding.UTF8.GetBytes(password);
            }

            var hashmd5 = new MD5CryptoServiceProvider();
            var keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(password));
            hashmd5.Clear();

            return keyArray;
        }
    }
}