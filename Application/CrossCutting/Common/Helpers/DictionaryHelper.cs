﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.MemberAccess;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helpers for Dictionary
    /// </summary>
    public static class DictionaryHelper
    {
        public static readonly ConvertTypeSettings DefaultConvertTypeSettings = new ConvertTypeSettings
        {
            EnumNameConversion = EnumNameConversion.None,
            FormatProvider = CultureInfo.CurrentCulture,
        };

        /// <summary>
        /// Converts name value collection to dictionary
        /// </summary>
        /// <param name="nameValueCollection">name value collection to convert</param>
        /// <returns>Dictionary of string => object which is accepted by RouteValueDictionary ctor</returns>
        public static IDictionary<string, object> ToDictionary(this NameValueCollection nameValueCollection)
        {
            return nameValueCollection.Keys.Cast<string>().ToDictionary(k => k, v => (object)nameValueCollection[v]);
        }

        /// <summary>
        /// Extract all field values from namevaluecollection
        /// </summary>
        /// <param name="nameValueCollection"></param>
        /// <returns></returns>
        public static IDictionary<string, string[]> ToFieldValuesDictionary(NameValueCollection nameValueCollection)
        {
            const char delimiter = ',';

            return nameValueCollection.Keys.Cast<string>().ToDictionary(k => k, v => nameValueCollection.Get(v).Split(delimiter));
        }


        /// <summary>
        /// Generates dictionary from POCO object
        /// </summary>
        /// <typeparam name="TKey">Type of the dictionary key</typeparam>
        /// <typeparam name="TValue">Type of the dictionary value</typeparam>
        /// <param name="object">object to be converted to dictionary</param>
        /// <param name="keyFunc">Function transforming property name to key</param>
        /// <param name="valueFunc">Function transforming property value to dictionary value</param>
        /// <returns>Dictionary or null if object is null</returns>
        public static IDictionary<TKey, TValue> ToDictionary<TKey, TValue>(object @object, Func<string, TKey> keyFunc, Func<object, TValue> valueFunc)
        {
            if (@object == null)
            {
                return null;
            }

            var properties = GetProperties(@object);

            return properties.ToDictionary(i => keyFunc(i.Name), i => valueFunc(i.GetValue(@object)));
        }

        /// <summary>
        /// Generates dictionary from POCO object
        /// </summary>
        /// <typeparam name="TKey">Type of the dictionary key</typeparam>
        /// <typeparam name="TValue">Type of the dictionary value</typeparam>
        /// <typeparam name="TObject">Type of the object value</typeparam>
        /// <param name="object">object to be converted to dictionary</param>
        /// <param name="getKey">Function transforming property name to key</param>
        /// <param name="getValue">Function transforming property value to dictionary value</param>
        /// <returns>Dictionary or null if object is null</returns>
        public static IDictionary<TKey, TValue> ToDictionary<TKey, TValue, TObject>(object @object, Func<string, TKey> getKey, Func<PropertyInfo, TObject, TValue> getValue)
        {
            if (@object == null)
            {
                return null;
            }

            var properties = GetProperties(@object);

            return properties.ToDictionary(x => getKey(x.Name),
                                           x => getValue(x, (TObject)x.GetValue(@object)));
        }

        /// <summary>
        /// Turns an array of consecutive key-value pairs into a dictionary. 
        /// Overwrites values of the same key with the latter instance.
        /// Array elements are expected to be of even number.
        /// Null yields null.
        /// Key-value pairs should be listed as key0, value0, key1, value1, ..., keyN, valueN.
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="keysAndValues">Key-value elements to be transformed into dictionary</param>
        /// <returns>Dictionary made of key-value pairs</returns>
        public static IDictionary<T, T> ToDictionary<T>(T[] keysAndValues)
        {
            if (keysAndValues == null)
            {
                return null;
            }

            if (keysAndValues.Length % 2 != 0)
            {
                throw new InvalidDataException("Parameter keysAndValues must have an even number of elements");
            }

            var result = new Dictionary<T, T>();

            for (var i = 0; i < keysAndValues.Length; i += 2)
            {
                var key = keysAndValues[i];
                var value = keysAndValues[i + 1];

                result[key] = value;
            }

            return result;
        }

        /// <summary>
        /// Set values based on the propertyValues dictionary to object properties
        /// </summary>
        /// <param name="object">Object whose values should be set</param>
        /// <param name="propertyValues">Dictionary containing values to be set</param>
        /// <param name="convertTypeSettings">Convert type settings</param>
        public static void PopulateObjectProperties(
            [NotNull] object @object,
            IDictionary<string, object> propertyValues,
            ConvertTypeSettings? convertTypeSettings = null)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            if (convertTypeSettings == null)
            {
                convertTypeSettings = DefaultConvertTypeSettings;
            }

            foreach (var property in propertyValues)
            {
                var memberAccessPath = new MemberAccessPath(property.Key);

                var memberProperty = memberAccessPath.GetPropertyInfo(@object);
                var propertyInfo = memberProperty.PropertyInfo;
                var parent = memberProperty.ParentObject;

                if (propertyInfo != null)
                {
                    var resolvedPropertyType = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;
                    var value = property.Value == null ? null : ConvertType(property.Value, resolvedPropertyType, convertTypeSettings.Value);
                    propertyInfo.SetValue(parent, value);
                }
            }
        }

        /// <summary>
        ///     Convert value to given type with support for enum values.
        /// </summary>
        /// <param name="value">Object value to convert</param>
        /// <param name="type">Convert to this type</param>
        /// <param name="convertTypeSettings">Settings for conversion</param>
        /// <returns></returns>
        private static object ConvertType(object value, Type type, ConvertTypeSettings convertTypeSettings)
        {
            if (type.IsEnum && !value.GetType().IsEnum)
            {
                if (convertTypeSettings.EnumNameConversion == EnumNameConversion.None)
                {
                    return Enum.Parse(type, (string)value);
                }

                try
                {
                    var pascalCaseValue = NamingConventionHelper.ConvertHyphenatedToPascalCase((string)value);

                    return Enum.Parse(type, pascalCaseValue);
                }
                catch (ArgumentException)
                {
                    //falling back to default behavior
                }

                return Enum.Parse(type, (string)value);
            }

            var elementType = type.GetElementType();

            if (type.IsArray && value is string)
            {
                var values = SplitByComma(value.ToString());
                var result = Array.CreateInstance(elementType, values.Length);

                for (int i = 0; i < values.Length; i++)
                {
                    result.SetValue(Convert.ChangeType(values[i], elementType), i);
                }

                return result;
            }

            return Convert.ChangeType(value, type, convertTypeSettings.FormatProvider);
        }

        private static IEnumerable<PropertyInfo> GetProperties(object @object)
        {
            var type = @object.GetType();

            return TypeHelper.GetPublicInstanceProperties(type);
        }

        private static string[] SplitByComma(string stringToSplit)
        {
            return string.IsNullOrEmpty(stringToSplit) ? new string[0] : stringToSplit.Split(',');
        }
    }
}