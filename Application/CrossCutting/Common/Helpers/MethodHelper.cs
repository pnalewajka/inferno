using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper methods related to method processing via reflection
    /// </summary>
    public class MethodHelper
    {
        /// <summary>
        /// Gets method by a name
        /// </summary>
        /// <typeparam name="TObject">Type to work with</typeparam>
        /// <param name="methodName">Name to retrieve the method</param>
        /// <returns></returns>
        public static MethodInfo GetMethod<TObject>(string methodName)
        {
            var type = typeof(TObject);

            return type.GetMethod(methodName);
        }

        /// <summary>
        /// Gets method by a name and parameter types
        /// </summary>
        public static MethodInfo GetMethodByParamTypes<TObject>(string methodName, params Type[] args)
        {
            var method = GetMethodByParamTypesOrDefault<TObject>(methodName, args);

            if (method == null)
            {
                throw new Exception($"Cant find any method with name '{methodName}' and provided parameters");
            }

            return method;
        }

        /// <summary>
        /// Gets method by a name and parameter types
        /// </summary>
        public static MethodInfo GetMethodByParamTypesOrDefault<TObject>(string methodName, params Type[] args)
        {
            var type = typeof(TObject);
            var methods = type.GetMethods();

            foreach (var method in methods)
            {
                if (method.Name != methodName)
                {
                    continue;
                }

                var parameters = method.GetParameters();
                var doArgumentsMatch = args
                    .Select((arg, index) => new { arg, index })
                    .All(a => a.arg == null || parameters[a.index].ParameterType == a.arg);

                if (doArgumentsMatch)
                {
                    return method;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets method by a expression that calls it
        /// </summary>
        public static MethodInfo GetMethodByCall(Expression<Func<object>> callExpression)
        {
            return ((MethodCallExpression)callExpression.Body).Method;
        }

        /// <summary>
        /// Gets a generic method by a expression that calls it
        /// </summary>
        public static MethodInfo GetGenericMethodByCall(Expression<Func<object>> callExpression)
        {
            return GetMethodByCall(callExpression).GetGenericMethodDefinition();
        }

        /// <summary>
        /// Gets method by a lambda expression
        /// </summary>
        /// <typeparam name="TObject">Type to work with</typeparam>
        /// <param name="methodNameExpression">Lambda to retrieve the method</param>
        /// <returns></returns>
        public static MethodInfo GetMethod<TObject>(Expression<Func<TObject, object>> methodNameExpression)
        {
            if (methodNameExpression == null)
            {
                throw new ArgumentNullException(nameof(methodNameExpression));
            }

            var lambda = methodNameExpression as LambdaExpression;

            return GetMethodInfo(lambda);
        }

        /// <summary>
        /// Gets name of method specified by lambda
        /// </summary>
        /// <typeparam name="TObject">Type to work with</typeparam>
        /// <param name="methodNameExpression">Lambda to retrieve the method</param>
        /// <returns></returns>
        public static string GetMethodName<TObject>(Expression<Func<TObject, object>> methodNameExpression)
        {
            return GetMethod(methodNameExpression).Name;
        }

        /// <summary>
        /// Gets name of method specified by a lambda
        /// </summary>
        /// <param name="methodNameExpression">Lambda to retrieve the method</param>
        /// <returns></returns>
        public static string GetMethodName(Expression<Func<object>> methodNameExpression)
        {
            return GetMethodInfo(methodNameExpression).Name;
        }

        private static MethodInfo GetMethodInfo(LambdaExpression lambda)
        {
            var callExpression = lambda.Body as MethodCallExpression;

            if (callExpression == null)
            {
                throw new ArgumentException(@"Not a valid method expression", nameof(lambda));
            }

            var methodInfo = callExpression.Method;

            return methodInfo;
        }

        /// <summary>
        /// Gets the lambda expression of getting method with certain name
        /// </summary>
        /// <typeparam name="TClass">Type of class which contains the method</typeparam>
        /// <param name="methodInfo">Name of the method</param>
        public static Expression<Func<TClass, object>> GetLambdaFromMethod<TClass>(MethodInfo methodInfo)
        {
            var p = Expression.Parameter(typeof(TClass));
            var expr = Expression.Lambda<Func<TClass, object>>(
                Expression.Convert(
                    Expression.Call(
                        Expression.Convert(p, typeof(TClass)), methodInfo), typeof(object)), p);

            return expr;
        }
    }
}