﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Attributes processing helper methods
    /// </summary>
    public static class AttributeHelper
    {
        private struct EnumAttributesKey : IEquatable<EnumAttributesKey>
        {
            public Type enumType { get; set; }
            public MemberInfo member { get; set; }
            public Type attributeType { get; set; }

            public bool Equals(EnumAttributesKey other)
            {
                return enumType == other.enumType
                    && member == other.member
                    && attributeType == other.attributeType;
            }

            public override int GetHashCode()
            {
                return enumType.GetHashCode()
                    + member.GetHashCode()
                    + attributeType.GetHashCode();
            }
        }

        private static IDictionary<EnumAttributesKey, Attribute[]> _enumMemberAttributesCache
            = new ConcurrentDictionary<EnumAttributesKey, Attribute[]>();

        /// <summary>
        /// Return specific attribute for given type, basing on specific instance
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="instance">instance of object</param>
        /// <param name="defaultValue">Default value to return if no attributes of requested type are found</param>
        /// <returns>Requested attribute or default value</returns>
        public static TAttribute GetClassAttribute<TAttribute>(object instance, TAttribute defaultValue = null) where TAttribute : Attribute
        {
            return GetClassAttributes<TAttribute>(instance).SingleOrDefault() ?? defaultValue;
        }

        /// <summary>
        /// Return specific attribute for given type
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="type">Type to be examined</param>
        /// <param name="defaultValue">Default value to return if no attributes of requested type are found</param>
        /// <returns>Requested attribute or default value</returns>
        public static TAttribute GetClassAttribute<TAttribute>(Type type, TAttribute defaultValue = null) where TAttribute : Attribute
        {
            return GetClassAttributes<TAttribute>(type).SingleOrDefault() ?? defaultValue;
        }

        /// <summary>
        /// Get specific TAttribute of TClass, or null if attribute wasn't present
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <typeparam name="TAttribute"></typeparam>
        /// <returns></returns>
        public static TAttribute GetClassAttribute<TClass, TAttribute>() where TAttribute : Attribute
            where TClass : class
        {
            return typeof(TClass).GetCustomAttributes<TAttribute>(true).FirstOrDefault();
        }

        /// <summary>
        /// Return list of specific attribute for given type, basing on specific instance
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="instance">instance of object</param>
        /// <returns>List of requested attributes</returns>
        public static IEnumerable<TAttribute> GetClassAttributes<TAttribute>([NotNull] object instance) where TAttribute : Attribute
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            var type = instance.GetType();

            return type.GetCustomAttributes<TAttribute>(true);
        }

        /// <summary>
        /// Return list of specific attribute for given type
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="type">Type to be examined</param>
        /// <returns>List of requested attributes</returns>
        public static IEnumerable<TAttribute> GetClassAttributes<TAttribute>([NotNull] Type type) where TAttribute : Attribute
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.GetCustomAttributes<TAttribute>(true);
        }

        /// <summary>
        /// Return specific attribute for given property
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="propertyInfo">Property info</param>
        /// <param name="defaultValue">Default value, returned if no attribute is found</param>
        /// <returns>Attribute or default value if not found</returns>
        public static TAttribute GetPropertyAttribute<TAttribute>([NotNull] PropertyInfo propertyInfo, TAttribute defaultValue = null) where TAttribute : Attribute
        {
            if (propertyInfo == null)
            {
                throw new ArgumentNullException(nameof(propertyInfo));
            }

            return propertyInfo.GetCustomAttribute<TAttribute>(true) ?? defaultValue;
        }

        /// <summary>
        /// Returns specific attribute for given property expression
        /// </summary>
        /// <typeparam name="TObject">Requested property type</typeparam>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="propertyNameExpression">Expression to extract property from type</param>
        /// <returns>Attribute</returns>
        public static TAttribute GetPropertyAttribute<TObject, TAttribute>([NotNull] Expression<Func<TObject, object>> propertyNameExpression)
            where TAttribute : Attribute
        {
            if (propertyNameExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyNameExpression));
            }

            return GetPropertyAttribute<TAttribute>(PropertyHelper.GetProperty(propertyNameExpression));
        }

        /// <summary>
        /// Return specific attribute for given member
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="memberInfo">Member info</param>
        /// <param name="defaultValue">Default value, returned if no attribute is found</param>
        /// <returns>Attribute or default value if not found</returns>
        public static TAttribute GetMemberAttribute<TAttribute>([NotNull] MemberInfo memberInfo,
            TAttribute defaultValue = null) where TAttribute : Attribute
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            return memberInfo.GetCustomAttribute<TAttribute>(true) ?? defaultValue;
        }

        /// <summary>
        /// Return specific attribute for given property
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="memberInfo">Property info</param>
        /// <param name="defaultValue">Expression used to calculate default value</param>
        /// <returns>Attribute or default value if not found</returns>
        public static TAttribute GetMemberAttribute<TAttribute>([NotNull] MemberInfo memberInfo, Func<TAttribute> defaultValue) where TAttribute : Attribute
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            return memberInfo.GetCustomAttribute<TAttribute>(true) ?? defaultValue();
        }

        /// <summary>
        /// Return specific attribute for given property
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="propertyInfo">Property info</param>
        /// <param name="defaultValue">Expression used to calculate default value</param>
        /// <returns>Attribute or default value if not found</returns>
        public static TAttribute GetPropertyAttribute<TAttribute>([NotNull] PropertyInfo propertyInfo, Func<TAttribute> defaultValue) where TAttribute : Attribute
        {
            if (propertyInfo == null)
            {
                throw new ArgumentNullException(nameof(propertyInfo));
            }

            return propertyInfo.GetCustomAttribute<TAttribute>(true) ?? defaultValue();
        }

        /// <summary>
        /// Return specific attribute for given method
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="methodInfo">Method info</param>
        /// <param name="defaultValue">Default value, returned if no </param>
        /// <returns>Attribute or default value if not found</returns>
        public static TAttribute GetMethodAttribute<TAttribute>([NotNull] MethodInfo methodInfo, TAttribute defaultValue = null) where TAttribute : Attribute
        {
            if (methodInfo == null)
            {
                throw new ArgumentNullException(nameof(methodInfo));
            }

            return methodInfo.GetCustomAttribute<TAttribute>(true) ?? defaultValue;
        }

        /// <summary>
        /// Return list of attribute for given method
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="methodInfo">Method info</param>
        /// <returns>List of requested attributes</returns>
        public static IEnumerable<TAttribute> GetMethodAttributes<TAttribute>([NotNull] MethodInfo methodInfo) where TAttribute : Attribute
        {
            if (methodInfo == null)
            {
                throw new ArgumentNullException(nameof(methodInfo));
            }

            return methodInfo.GetCustomAttributes<TAttribute>(true);
        }

        /// <summary>
        /// Get specific attribute for enum value
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute</typeparam>
        /// <param name="enumVal">enum value</param>
        /// <returns>Attribute or null if not found</returns>
        public static TAttribute GetEnumFieldAttribute<TAttribute>(Enum enumVal) where TAttribute : Attribute
        {
            return GetEnumFieldAttributes<TAttribute>(enumVal).SingleOrDefault();
        }

        /// <summary>
        /// Get specific attributes for enum value
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute</typeparam>
        /// <param name="enumVal">enum value</param>
        /// <returns></returns>
        public static IEnumerable<TAttribute> GetEnumFieldAttributes<TAttribute>(Enum enumVal) where TAttribute : Attribute
        {
            var type = enumVal.GetType();
            var member = type.GetMember(enumVal.ToString()).FirstOrDefault();

            if (member == null)
            {
                return Enumerable.Empty<TAttribute>();
            }

            var cacheKey = new EnumAttributesKey
            {
                enumType = type,
                member = member,
                attributeType = typeof(TAttribute),
            };

            if (!_enumMemberAttributesCache.ContainsKey(cacheKey))
            {
                _enumMemberAttributesCache[cacheKey] = member.GetCustomAttributes<TAttribute>(false).ToArray();
            }

            return (TAttribute[])_enumMemberAttributesCache[cacheKey];
        }

        /// <summary>
        /// Check if specific attribute is applied for given property
        /// </summary>
        /// <typeparam name="TAttribute">Requested attribute type</typeparam>
        /// <param name="propertyInfo">Property info</param>
        /// <param name="defaultValue">Default value, returned if no attribute is found</param>
        /// <returns>bool</returns>
        public static bool HasPropertyAttribute<TAttribute>([NotNull] PropertyInfo propertyInfo, TAttribute defaultValue = null) where TAttribute : Attribute
        {
            if (propertyInfo == null)
            {
                throw new ArgumentNullException(nameof(propertyInfo));
            }

            return propertyInfo.IsDefined(typeof(TAttribute));
        }
    }
}
