﻿using System;
using System.Linq;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper for arrays
    /// </summary>
    public static class ArrayHelper
    {
        /// <summary>
        /// Compare two arrays
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool AreEqual(Array a, Array b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if ((a == null || b == null) || a.Length != b.Length)
            {
                return false;
            }

            return !a.Cast<object>().Where((t, i) => !a.GetValue(i).Equals(b.GetValue(i))).Any();
        }

        /// <summary>
        /// Rewrite each value from source to target array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="target"></param>
        /// <param name="source"></param>
        /// <exception cref="System.ArgumentNullException">Thrown when target or source paramter is null</exception>
        /// <exception cref="System.ArgumentException">Thrown when source array length doesn't match target array length</exception>
        public static void Rewrite<T>(T[] target, T[] source)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (target.Length != source.Length)
            {
                throw new ArgumentException("Source array length doesn't match target array length.");
            }

            for (var i = 0; i < target.Length; i++)
            {
                target[i] = source[i];
            }
        }
    }
}