﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Data contract Seriazation helper class
    /// </summary>
    public class DataContractSerializationHelper
    {
        /// <summary>
        /// Serialize given object and type to xml
        /// </summary>
        /// <param name="dataObj">Object to serialize</param>
        /// <param name="type">Type of object</param>
        /// <param name="knownTypes"></param>
        /// <returns></returns>
        public static string SerializeToXml(object dataObj, Type type, IEnumerable<Type> knownTypes = null)
        {
            using (var writer = new MemoryStream())
            {
                var ser = new DataContractSerializer(type, knownTypes);

                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };

                using (var w = XmlWriter.Create(writer, settings))
                {
                    ser.WriteObject(w, dataObj);
                }

                return Encoding.UTF8.GetString(writer.ToArray());
            }
        }

        /// <summary>
        /// Serialize given object and type to xml. Generic version.
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeToXml<TClass>(TClass obj) where TClass : class
        {
            return SerializeToXml(obj, typeof (TClass));
        }

        /// <summary>
        /// Deserialize object from xml using specific type
        /// </summary>
        /// <param name="xml">Xml with serialized object</param>
        /// <param name="type">Type of object</param>
        /// <param name="knownTypes"></param>
        /// <returns></returns>
        public static object DeserializeFromXml(string xml, Type type, IEnumerable<Type> knownTypes = null)
        {
            var buff = Encoding.UTF8.GetBytes(xml);

            using (var ms = new MemoryStream(buff))
            {
                using (
                    var reader = XmlDictionaryReader.CreateTextReader(ms,
                        new XmlDictionaryReaderQuotas {MaxStringContentLength = int.MaxValue}))
                {
                    var ser = new DataContractSerializer(type, knownTypes);
                    var obj = ser.ReadObject(reader);
                    return obj;
                }
            }
        }

        /// <summary>
        /// Deserialize object from xml using specific type. Generic version.
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static TClass DeserializeFromXml<TClass>(string xml) where TClass : class
        {
            return (TClass) DeserializeFromXml(xml, typeof (TClass));
        }
    }
}