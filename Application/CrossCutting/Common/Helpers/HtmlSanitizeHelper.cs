﻿using System;
using System.Text.RegularExpressions;
using Ganss.XSS;
using Microsoft.Security.Application;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class HtmlSanitizeHelper
    {
        public static string Sanitize(string input, HtmlSanitizeMethod htmlSanitizeMethod)
        {
            switch (htmlSanitizeMethod)
            {
                case HtmlSanitizeMethod.Default:
                    return SanitizeDefault(input);

                case HtmlSanitizeMethod.Basic:
                    return SanitizeBasic(input);

                case HtmlSanitizeMethod.Strict:
                    return SanitizeRestrict(input);

                case HtmlSanitizeMethod.PlainText:
                    return SanitizePlainText(input);

                default:
                    throw new ArgumentOutOfRangeException(nameof(htmlSanitizeMethod));
            }
        }

        private static string SanitizeDefault(string input)
        {
            return Sanitizer.GetSafeHtmlFragment(input);
        }

        private static string SanitizeBasic(string input)
        {
            var sanitizer = new HtmlSanitizer();

            return sanitizer.Sanitize(input);
        }

        private static string SanitizeRestrict(string input)
        {
            var sanitizer = new HtmlSanitizer();

            sanitizer.AllowedSchemes.Remove("http");
            sanitizer.AllowedSchemes.Remove("https");

            return sanitizer.Sanitize(input);
        }

        private static string SanitizePlainText(string input)
        {
            var sanitizedInput = SanitizeDefault(input);

            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(sanitizedInput);

            var text = htmlDoc.DocumentNode.InnerText;

            var plainText = Regex
                .Replace(text, @"<(.|n)*?>", string.Empty)
                .Replace("&nbsp", "");

            return plainText;
        }
    }
}
