﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Type helper
    /// </summary>
    public static class TypeHelper
    {
        private static readonly Type[] NumericTypes =
        {
            typeof (byte), typeof (decimal), typeof (double), typeof (float), typeof (int), typeof (long),
            typeof (sbyte), typeof (short), typeof (uint), typeof (ulong), typeof (ushort)
        };

        private static readonly Type[] IntegralTypes =
        {
            typeof (byte), typeof (int), typeof (long),
            typeof (sbyte), typeof (short), typeof (uint), typeof (ulong), typeof (ushort)
        };

        private static readonly IDictionary<string, Type> _foundTypes = new ConcurrentDictionary<string, Type>();
        private static IList<Type> _lastLoadedTypes = null;
        private static long _lastLoadedTypesAssemblyCount = -1;
        private static object _lastLoadedTypesLockingObject = new object();

        /// <summary>
        /// Returns all types from currently loaded assemblies
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> GetLoadedTypes()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            if (assemblies.Length == _lastLoadedTypesAssemblyCount)
            {
                return _lastLoadedTypes;
            }

            // this is a workaround, enumerating types of some assemblies causes errors in VS2015
            var types = assemblies
                .Where(a => !a.IsDynamic)
                .SelectMany(a => FunctionalHelper.Try(() => a.GetTypes(), () => Enumerable.Empty<Type>()))
                .ToList();

            lock (_lastLoadedTypesLockingObject)
            {
                _lastLoadedTypesAssemblyCount = assemblies.Length;
                _lastLoadedTypes = types;
            }

            return types;
        }

        /// <summary>
        /// Find type definition looking in multiple assemblies
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns>Type infomation</returns>
        public static Type FindTypeInLoadedAssemblies(string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                throw new ArgumentNullException(nameof(typeName));
            }

            Type foundType = null;

            if (_foundTypes.TryGetValue(typeName, out foundType))
            {
                return foundType;
            }

            var type = GetLoadedTypes().SingleOrDefault(t => t.FullName == typeName);

            if (type != null)
            {
                _foundTypes.Add(typeName, type);
            }

            return type;
        }

        /// <summary>
        /// Return list of all subclasses of specific type within same assembly
        /// </summary>
        /// <param name="type">Type of class to find subclasses for</param>
        /// <returns>List of subclass types</returns>
        public static IEnumerable<Type> GetSubclassesOf(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return GetSubclassesOf(type, type.Assembly);
        }

        /// <summary>
        /// Return list of all subclasses of specific type within specified assembly
        /// </summary>
        /// <param name="type">Type of class to find subclasses for</param>
        /// <param name="assembly">Assembly in which the subclasses reside</param>
        /// <returns>List of subclass types</returns>
        public static IEnumerable<Type> GetSubclassesOf(Type type, Assembly assembly)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }

            return assembly.GetTypes().Where(type.IsAssignableFrom);
        }

        /// <summary>
        /// Return list of all subclasses of specific type from all loaded assemblies
        /// </summary>
        /// <param name="type">Type of class to find subclasses for</param>
        /// <param name="includingGivenType">Should the given type be also included in the results.</param>
        /// <returns>List of subclass types</returns>
        public static IEnumerable<Type> GetAllSubClassesOf(Type type, bool includingGivenType)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return GetLoadedTypes().Where(t => (type.IsGenericTypeDefinition
                ? IsSubclassOfRawGeneric(t, type)
                : type.IsAssignableFrom(t)) && (includingGivenType || t != type));
        }

        /// <summary>
        /// Checks if provided "tocheck" type is subclass of generic class
        /// </summary>
        /// <param name="typeToCheck"></param>
        /// <param name="genericType"></param>
        /// <returns>True if type is subclass of generic type</returns>
        public static bool IsSubclassOfRawGeneric(Type typeToCheck, Type genericType)
        {
            if (genericType == null)
            {
                throw new ArgumentNullException(nameof(genericType));
            }

            if (typeToCheck == null)
            {
                throw new ArgumentNullException(nameof(typeToCheck));
            }

            while (typeToCheck != null && typeToCheck != typeof(object))
            {
                var currentType = typeToCheck.IsGenericType ? typeToCheck.GetGenericTypeDefinition() : typeToCheck;
                if (genericType == currentType)
                {
                    return true;
                }

                typeToCheck = typeToCheck.BaseType;
            }

            return false;
        }

        /// <summary>
        /// Returns the types used as parameters for generic type
        /// </summary>
        /// <param name="type">Type to extract parameters from</param>
        /// <param name="genericType">Generic type in which has parameters</param>
        /// <returns>Array of types used as raw generic parameters</returns>
        public static Type[] GetRawGenericArguments([NotNull] Type type, [NotNull] Type genericType)
        {
            if (genericType == null)
            {
                throw new ArgumentNullException(nameof(genericType));
            }

            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            while (type != null && type != typeof(object))
            {
                var currentType = type.IsGenericType ? type.GetGenericTypeDefinition() : type;

                if (genericType == currentType)
                {
                    return type.GetGenericArguments();
                }

                if (type.BaseType == null)
                {
                    break;
                }

                type = type.BaseType;
            }

            return null;
        }


        /// <summary>
        /// Checks if given type is a nullable enum
        /// </summary>
        /// <param name="type">Type to examine</param>
        /// <returns>True if type is a nullable enum</returns>
        public static bool IsNullableEnum([NotNull]Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(Nullable<>))
            {
                return false;
            }

            return type.GetGenericArguments()[0].IsEnum;
        }

        /// <summary>
        /// Checks if given type is an enum marked with FlagsAttribute
        /// </summary>
        /// <param name="type">Type to examine</param>
        /// <returns>True if type is enum marked with FlagsAttribute</returns>
        public static bool IsFlaggedEnum([NotNull]Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.IsEnum && type.GetCustomAttributes<FlagsAttribute>().Any();
        }

        /// <summary>
        /// Extract generic type is passed type is Nullable
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type ExtractIfNullable(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                type = type.GetGenericArguments()[0];
            }

            return type;
        }

        /// <summary>
        /// Get full type name and assembly name without versioning info
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetFullNameWithAssemblyName(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return $"{type.FullName}, {type.Assembly.GetName().Name}";
        }

        /// <summary>
        /// Get list of all non-public instance methods, including the inherited ones.
        /// </summary>
        /// <param name="type">Type to retrieve methods from</param>
        /// <returns>Collection of non-public instance method infos, including inherited ones.</returns>
        public static IEnumerable<MethodInfo> GetNonPublicInstanceMethods(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.GetMethods(BindingFlags.FlattenHierarchy
                                   | BindingFlags.Instance
                                   | BindingFlags.NonPublic);
        }

        /// <summary>
        /// Get list of all public instance methods, including the inherited ones.
        /// </summary>
        /// <param name="type">Type to retrieve methods from</param>
        /// <returns>Collection of public instance method infos, including inherited ones.</returns>
        public static IEnumerable<MethodInfo> GetPublicInstanceMethods(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.GetMethods(BindingFlags.FlattenHierarchy
                                   | BindingFlags.Instance
                                   | BindingFlags.Public);
        }

        /// <summary>
        /// Get list of all public instance methods, including the inherited ones, with specified return type
        /// </summary>
        /// <param name="type">Type to retrieve methods from</param>
        /// <typeparam name="TReturnValue">Returned value type</typeparam>
        /// <returns>Collection of non-public instance method infos, including inherited ones.</returns>
        public static IEnumerable<MethodInfo> GetPublicInstanceMethodsReturning<TReturnValue>(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var returnType = typeof(TReturnValue);

            return type.GetMethods(BindingFlags.FlattenHierarchy
                                   | BindingFlags.Instance
                                   | BindingFlags.Public)
                       .Where(m => returnType.IsAssignableFrom(m.ReturnType));
        }

        private static readonly IDictionary<Type, IEnumerable<PropertyInfo>> TypePublicInstanceProperties = new ConcurrentDictionary<Type, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// Returns a collection of public instance properties in a type, including inherited ones.
        /// </summary>
        /// <param name="type">Type to examine</param>
        /// <returns>Collection of property infos</returns>
        public static IEnumerable<PropertyInfo> GetPublicInstanceProperties(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            IEnumerable<PropertyInfo> result;

            if (TypePublicInstanceProperties.TryGetValue(type, out result))
            {
                return result;
            }

            result = type.GetProperties(BindingFlags.FlattenHierarchy
                                   | BindingFlags.Instance
                                   | BindingFlags.Public);

            TypePublicInstanceProperties[type] = result;

            return result;
        }

        /// <summary>
        /// Returns a collection of public instance properties of expected type in a type, including inherited ones.
        /// </summary>
        /// <param name="type">Type to examine</param>
        /// <returns>Collection of property infos</returns>
        public static IEnumerable<PropertyInfo> GetPublicInstancePropertiesOfType<T>(Type type)
        {
            return GetPublicInstanceProperties(type).Where(x => x.PropertyType == typeof(T));
        }

        /// <summary>
        /// Returns name collection of public instance properties in a type, including inherited ones.
        /// </summary>
        /// <param name="type">Type to examine</param>
        /// <returns>Collection of property names</returns>
        public static string[] GetPublicInstancePropertyNames(Type type)
        {
            return GetPublicInstanceProperties(type).Select(i => i.Name).ToArray();
        }

        /// <summary>
        /// Follows the property path and returns the
        /// </summary>
        /// <param name="type">Type to examine</param>
        /// <returns>Collection of property names</returns>
        public static PropertyInfo GetPropertyByPath(object @object, string propertyPath)
        {
            var type = @object.GetType();
            var pathNodes = propertyPath.Split('.');
            var properties = type.GetProperties();
            var property = properties.SingleOrDefault(p => p.Name == pathNodes[0]);

            if (property == null)
            {
                return null;
            }

            if (pathNodes.Length == 1)
            {
                return property;
            }

            return GetPropertyByPath(property.GetValue(@object, null), string.Join(".", pathNodes.Skip(1)));
        }

        /// <summary>
        /// Get list of public instance properties, that have TAttribute defined against them
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> GetPublicInstancePropertiesWithAttribute<TAttribute>(Type type) where TAttribute : Attribute
        {
            return
                GetPublicInstanceProperties(type)
                    .Where(p => AttributeHelper.GetPropertyAttribute<TAttribute>(p) != null);
        }

        /// <summary>
        /// Gets type by name from an assembly in which given type resides
        /// </summary>
        /// <param name="typeName">Name of the type sought for</param>
        /// <param name="sameAssemblyType">Type to reference assembly by</param>
        /// <param name="throwOnNotFound">Should exception be thrown on type not found</param>
        /// <returns>Type</returns>
        public static Type GetType(string typeName, Type sameAssemblyType, bool throwOnNotFound)
        {
            return sameAssemblyType.Assembly.GetType(typeName, throwOnNotFound);
        }

        /// <summary>
        /// Determines if the type specified is numeric. Considers nullable types numeric, if underlying type is numeric.
        /// </summary>
        /// <param name="type">Type to be examined</param>
        /// <param name="allowNullable">Determines if type can be nullable (of numeric type) to be considered numeric.</param>
        /// <returns>True if type is numeric</returns>
        public static bool IsNumeric(Type type, bool allowNullable = true)
        {
            return NumericTypes.Contains(type)
                   || (allowNullable
                       && IsNullable(type)
                       && type.GenericTypeArguments.Length == 1
                       && NumericTypes.Contains(type.GenericTypeArguments[0]));
        }

        /// <summary>
        ///     Determines if the type specified is integral. Considers nullable types integral, if underlying type is integral.
        /// </summary>
        /// <param name="type">Type to be examined</param>
        /// <param name="allowNullable">Determines if type can be nullable (of numeric type) to be considered numeric.</param>
        /// <returns>True if type is integral numeric</returns>
        public static bool IsIntegral(Type type, bool allowNullable = true)
        {
            return IntegralTypes.Contains(type)
                   || (allowNullable
                       && IsNullable(type)
                       && type.GenericTypeArguments.Length == 1
                       && IntegralTypes.Contains(type.GenericTypeArguments.First()));
        }

        /// <summary>
        /// Determines if the type specified is enumeration (collection or array)
        /// </summary>
        /// <param name="typeToTest">Type to be examined</param>
        /// <returns>True if type is enumeration (IEnumerable&lt;&gt; descendant)</returns>
        public static bool IsEnumeration(Type typeToTest)
        {
            return typeToTest.IsGenericType && typeof(IEnumerable<>).IsAssignableFrom(typeToTest.GetGenericTypeDefinition());
        }

        /// <summary>
        /// Determines if the type specified is enumeration (collection or array) parameterized by one of the types given.
        /// </summary>
        /// <param name="typeToTest">Type to be examined</param>
        /// <param name="typesToMatch">A list of types given collection can be parameterized by</param>
        /// <returns>True if type is enumeration (IEnumerable descendant) containing elements of one of the types given</returns>
        public static bool IsEnumerationOfGivenTypes(Type typeToTest, params Type[] typesToMatch)
        {
            if (typeof(IEnumerable).IsAssignableFrom(typeToTest))
            {
                Type embeddedType = GetEnumerationElementType(typeToTest);

                if (embeddedType != null)
                {
                    return typesToMatch.Contains(embeddedType);
                }
            }

            return false;
        }

        /// <summary>
        /// Returns enumeration (collection or array) inner element type.
        /// </summary>
        /// <param name="type">Type to be examined</param>
        /// <returns>Enumeration inner element type or NULL</returns>
        public static Type GetEnumerationElementType(Type type)
        {
            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                if (type.IsGenericType)
                {
                    return type.GetGenericArguments().First();
                }

                if (type.IsArray)
                {
                    return type.GetElementType();
                }
            }

            return null;
        }

        /// <summary>
        /// Determines if type is nullable. Works only for types retrieved other than via GetType().
        /// </summary>
        /// <remarks>See https://msdn.microsoft.com/en-us/library/ms366789.aspx for discussion.</remarks>
        /// <param name="type"></param>
        /// <returns>True if type is nullable.</returns>
        public static bool IsNullable(Type type)
        {
            return type.IsGenericType
                && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        /// <summary>
        /// Check if the given type is the expected type or its nullable version
        /// </summary>
        /// <typeparam name="T">Expected type</typeparam>
        /// <param name="type">input type</param>
        /// <returns>true if type is typeof(T) or typeof(T?)</returns>
        public static bool IsTypeOrNullableType<T>(Type type) where T : struct
        {
            return type == typeof(T) || type == typeof(T?);
        }

        /// <summary>
        /// Returns full type name suitable for in-code use
        /// </summary>
        /// <param name="type">Type to be examined</param>
        /// <param name="includeNamespace">Should the namespace be included in type name?</param>
        /// <returns>C#-friendly name of the type</returns>
        public static string GetCSharpName([NotNull] Type type, bool includeNamespace = false)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var sb = new StringBuilder();
            sb.Insert(0, GetCSharpTypeName(type, includeNamespace));

            while (type.IsNested && type.DeclaringType != null)
            {
                type = type.DeclaringType;
                sb.Insert(0, GetCSharpTypeName(type, includeNamespace) + ".");

            }

            if (includeNamespace && !string.IsNullOrEmpty(type.Namespace))
            {
                sb.Insert(0, type.Namespace + ".");
            }

            return sb.ToString();
        }

        private static string GetCSharpTypeName(Type type, bool includeNamespace)
        {
            if (!(type.IsGenericType || type.IsGenericTypeDefinition))
            {
                return type.Name;
            }

            var sb = new StringBuilder();

            var beforeTick = type.Name.Split('`')[0];
            sb.Append(beforeTick);

            var genericTypeParameters = type.GetGenericArguments();

            if (genericTypeParameters.Length > 0)
            {
                sb.Append('<');
                sb.Append(string.Join(", ", genericTypeParameters.Select(t => GetCSharpName(t, includeNamespace))));
                sb.Append('>');
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns a collection of namespaces related to given type. Takes into account generic type parameters.
        /// </summary>
        /// <param name="type">Type to be examined</param>
        /// <returns>Names of all namespaces involved in the type</returns>
        public static IEnumerable<string> GetNamespaces([NotNull] Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (!string.IsNullOrEmpty(type.Namespace))
            {
                yield return type.Namespace;
            }

            if (type.IsNested && type.DeclaringType != null)
            {
                foreach (var name in GetNamespaces(type.DeclaringType))
                {
                    yield return name;
                }
            }

            if (type.IsGenericType || type.IsGenericTypeDefinition)
            {
                foreach (var genericArgument in type.GetGenericArguments())
                {
                    foreach (var name in GetNamespaces(genericArgument))
                    {
                        yield return name;
                    }
                }
            }
        }

        /// <summary>
        /// Get type from assemblies referenced by the root assembly
        /// </summary>
        /// <param name="rootAssembly">Assembly from which we should start searching for type</param>
        /// <param name="typeName">Searched type name</param>
        /// <returns></returns>
        public static Type GetType(Assembly rootAssembly, string typeName)
        {
            return rootAssembly.GetReferencedAssemblies()
                .Select(Assembly.Load)
                .Select(assembly => assembly.GetType(typeName, false))
                .FirstOrDefault(result => result != null);
        }

        /// <summary>
        /// Get type from assembly by name
        /// </summary>
        /// <param name="assembly">Assembly in which we search for the type</param>
        /// <param name="typeName">Searched type name (without namespace)</param>
        /// <returns>Type or null if not found</returns>
        public static Type GetTypeByName(Assembly assembly, string typeName)
        {
            return assembly.GetTypes().FirstOrDefault(t => t.FullName == typeName);
        }

        /// <summary>
        /// Get collection element type with IEnumarable interface implementation from type by collection Type
        /// </summary>
        /// <param name="collectionType">Type of collection</param>
        /// <returns></returns>
        public static Type GetCollectionElementType([NotNull] Type collectionType)
        {
            if (collectionType == null)
            {
                throw new NullReferenceException();
            }

            foreach (var interfaceType in collectionType.GetInterfaces())
            {
                if (IsSubclassOfRawGeneric(interfaceType, typeof(IEnumerable<>)))
                {
                    return GetRawGenericArguments(interfaceType, typeof(IEnumerable<>))[0];
                }
            }

            throw new InvalidOperationException("No collection element type found");
        }


        /// <summary>
        /// Finds the IEnumerable implemented by the given type
        /// </summary>
        /// <param name="sequenceType"></param>
        /// <returns></returns>
        public static Type FindIEnumerable(Type sequenceType)
        {
            if (sequenceType == null || sequenceType == typeof(string))
            {
                return null;
            }

            if (sequenceType.IsArray)
            {
                return typeof(IEnumerable<>).MakeGenericType(sequenceType.GetElementType());
            }

            if (sequenceType.IsGenericType)
            {
                foreach (Type arg in sequenceType.GetGenericArguments())
                {
                    Type ienum = typeof(IEnumerable<>).MakeGenericType(arg);
                    if (ienum.IsAssignableFrom(sequenceType))
                    {
                        return ienum;
                    }
                }
            }

            Type[] interfaces = sequenceType.GetInterfaces();

            if (interfaces.Length > 0)
            {
                foreach (Type iface in interfaces)
                {
                    Type ienum = FindIEnumerable(iface);
                    if (ienum != null)
                    {
                        return ienum;
                    }
                }
            }

            if (sequenceType.BaseType != null && sequenceType.BaseType != typeof(object))
            {
                return FindIEnumerable(sequenceType.BaseType);
            }

            return null;
        }

        /// <summary>
        /// Checks if a given method has been overridden in any of the classes in whole inheritance tree line.
        /// </summary>
        /// <param name="methodName">Method name which has to be part of base type, otherwise exception will be thrown. Use nameof() to avoid mistake. Ex. type.IsMethodOverridden(nameof(ToString))</param>
        /// <param name="type">Type to test</param>
        /// <param name="bindingFlags">Use to choose method binding flags. By default Instance | Public | NonPublic is used.</param>
        /// <returns></returns>
        public static bool IsMethodOverridden(
            [NotNull] string methodName,
            [NotNull] Type type,
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
        {
            if (methodName == null)
            {
                throw new ArgumentNullException(nameof(methodName));
            }

            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var method = type.GetMethod(methodName, bindingFlags);

            if (method == null)
            {
                return false;
            }

            if (!method.IsVirtual)
            {
                return false;
            }

            var baseType = type;
            var wasMethodAlreadyDeclaredInTheInheritenceTreeLine = false;

            while (baseType != null)
            {
                if (baseType.GetMethods(bindingFlags | BindingFlags.DeclaredOnly).Any(m => m.Name == method.Name))
                {
                    if (wasMethodAlreadyDeclaredInTheInheritenceTreeLine)
                    {
                        return true;
                    }

                    wasMethodAlreadyDeclaredInTheInheritenceTreeLine = true;
                }

                baseType = baseType.BaseType;
            }

            return false;
        }

        /// <summary>
        /// Return all implementations for given interface
        /// </summary>
        /// <param name="interfaceType">interface type</param>
        public static IEnumerable<Type> GetImplementationsOf(Type interfaceType)
        {
            return GetLoadedTypes().Where(t => !t.IsAbstract && t.GetInterfaces().Contains(interfaceType));
        }

        /// <summary>
        /// Return all implementations for given interface
        /// </summary>
        public static IEnumerable<Type> GetImplementationsOf<TInteface>()
        {
            return GetImplementationsOf(typeof(TInteface));
        }
    }
}
