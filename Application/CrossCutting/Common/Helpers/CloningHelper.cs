﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper methods for cloning, collection cloning`
    /// </summary>
    public static class CloningHelper
    {
        /// <summary>
        /// Clone object chosen serialization method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="cloningMethod"></param>
        /// <returns></returns>
        public static T CloneObject<T>(T source, CloningMethod cloningMethod)
        {
            var serialized = JsonConvert.SerializeObject(source);

            return JsonConvert.DeserializeObject<T>(serialized);
        }

        /// <summary>
        /// Clone object json serialization
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T CloneObject<T>(T source)
        {
            return CloneObject(source, CloningMethod.Json);
        }

        /// <summary>
        /// Clone list 
        /// </summary>
        /// <typeparam name="T">List element type</typeparam>
        /// <param name="enumerableObject">List to be cloned</param>
        /// <param name="cloningExpression">Expression to be executed during cloning, if not provied list be shallow copy</param>
        /// <returns></returns>
        public static List<T> CloneList<T>(List<T> enumerableObject,
            Func<T, T> cloningExpression = null)
        {
            if (enumerableObject == null)
            {
                return null;
            }

            return
                enumerableObject.Select(e => cloningExpression == null ? e : cloningExpression(e))
                    .ToList();
        }
    }
}