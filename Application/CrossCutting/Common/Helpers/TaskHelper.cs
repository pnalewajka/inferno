﻿using System.Threading.Tasks;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class TaskHelper
    {
        /// <summary>
        /// Completed task
        /// </summary>
        public static readonly Task CompletedTask = Task.FromResult(false);
    }
}
