﻿using System;
using System.Data;
using System.Linq;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public class ExceptionHelper
    {
        private static readonly Type[] TransientExceptions = {
            typeof (DBConcurrencyException), typeof(TimeoutException)
        };

        /// <summary>
        /// Get first occurence of exception of specific type traversing through InnerException
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static TException GetFirstOfType<TException>(Exception ex) where TException : Exception
        {
            while (ex != null)
            {
                var result = ex as TException;
                if (result != null)
                {
                    return result;
                }

                ex = ex.InnerException;
            }

            return null;
        }

        public static bool IsTransientException(Exception exception)
        {
            return TransientExceptions.Any(e => e.IsInstanceOfType(exception));
        }
    }
}
