﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// General reflection helper
    /// </summary>
    public static class ReflectionHelper
    {
        /// <summary>
        /// Create instance of specific type using the first default constructor
        /// </summary>
        /// <param name="type">Type to instantiate</param>
        /// <returns>Instance object</returns>
        public static object CreateInstance(Type type)
        {
            return CreateObjectInstance(type, null);
        }

        /// <summary>
        /// Create instance of specific type
        /// </summary>
        /// <param name="type">Type to instantiate</param>
        /// <param name="parameters">Constructor parameters</param>
        /// <returns>Instance object</returns>
        public static object CreateInstance(Type type, params object[] parameters)
        {
            return CreateObjectInstance(type, parameters);
        }

        /// <summary>
        /// Create strongly typed instance of specific type with parameters
        /// </summary>
        /// <typeparam name="T">Expected type of the instantiated object</typeparam>
        /// ///
        /// <param name="type">Type to instantiate</param>
        /// <returns>instance</returns>
        public static T CreateInstance<T>(Type type)
        {
            return (T)CreateObjectInstance(type, null);
        }

        /// <summary>
        /// Create strongly typed instance of specific type with parameters
        /// </summary>
        /// <typeparam name="T">Expected type of the instantiated object</typeparam>
        /// ///
        /// <param name="type">Type to instantiate</param>
        /// <param name="parameters">Constructor parameters</param>
        /// <returns>instance</returns>
        public static T CreateInstance<T>(Type type, params object[] parameters)
        {
            return (T)CreateObjectInstance(type, parameters);
        }

        /// <summary>
        /// Create strongly typed instance of specific type resolving dependencies from current Ioc container when needed
        /// </summary>
        /// <typeparam name="T">Type of the object to be created</typeparam>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns>newly created instance</returns>
        public static T CreateInstanceWithIocDependencies<T>(object parameters = null)
        {
            Type type = typeof(T);

            return (T)CreateInstanceWithIocDependencies(type, parameters);
        }

        /// <summary>
        /// Creates intance of the type based on the IdentifierAttribiute
        /// All IoC dependencies are injected if needed
        /// </summary>
        /// <typeparam name="T">Expected type of the result</typeparam>
        /// <param name="identifier">Class identifier</param>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns></returns>
        public static T CreateInstanceByIdentifier<T>(string identifier, object parameters = null)
        {
            var type = IdentifierHelper.GetTypeByIdentifier(identifier);

            return (T)CreateInstanceWithIocDependencies(type, parameters);
        }

        /// <summary>
        /// Create strongly typed instance of specific type resolving dependencies from current Ioc container when needed
        /// </summary>
        /// <param name="type">Type of object to be created</param>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns>newly created instance</returns>
        public static object CreateInstanceWithIocDependencies(Type type, object parameters = null)
        {
            var constructorInfo = type.GetConstructors().FirstOrDefault();
            constructorInfo = constructorInfo ?? type.GetConstructors(BindingFlags.NonPublic).First();

            var constructorParameters = constructorInfo.GetParameters();
            var resolvedParameters = GetResolvedParameters(constructorParameters, parameters);

            return constructorInfo.Invoke(resolvedParameters.ToArray());
        }

        /// <summary>
        /// Find requested interface's implementation in container and return it's instance
        /// Provide anonymous class with constructor parameters to overwrite
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns></returns>
        public static TInterface ResolveInterface<TInterface>(object parameters = null)
        {
            return (TInterface)ResolveInterface(typeof(TInterface), parameters);
        }

        /// <summary>
        /// Find requested interface's implementation in container and return it's instance
        /// Provide anonymous class with constructor parameters to overwrite
        /// </summary>
        /// <param name="type"></param>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns></returns>
        public static object ResolveInterface(Type type, object parameters = null)
        {
            var container = ContainerConfiguration.Container;

            return container.Resolve(type, parameters ?? new { });
        }

        /// <summary>
        /// Find requested interface's implementation in container and return it's instance if it exists
        /// Return null if implementation is not registered in the container
        /// Provide anonymous class with constructor parameters to overwrite
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns></returns>
        public static TInterface TryResolveInterface<TInterface>(object parameters = null)
        {
            return (TInterface)TryResolveInterface(typeof(TInterface), parameters);
        }

        /// <summary>
        /// Find requested interface's implementation in container and return it's instance if it exists
        /// Return null if implementation is not registered in the container
        /// Provide anonymous class with constructor parameters to overwrite
        /// </summary>
        /// <param name="type"></param>
        /// <param name="parameters">Anonymous class with parameters to be passed in constructor</param>
        /// <returns></returns>
        public static object TryResolveInterface(Type type, object parameters = null)
        {
            var container = ContainerConfiguration.Container;

            return HasComponent(type)
                ? ResolveInterface(type, parameters)
                : null;
        }

        /// <summary>
        /// Check if a component of type T is registered in the container
        /// </summary>
        public static bool HasComponent<T>()
        {
            return HasComponent(typeof(T));
        }

        /// <summary>
        /// Check if a component of certain Type is registered in the container
        /// </summary>
        public static bool HasComponent(Type type)
        {
            return ContainerConfiguration.Container.Kernel.HasComponent(type);
        }

        /// <summary>
        /// Resolve all valid components that match this type.
        /// </summary>
        /// <typeparam name="TInterface">The service type</typeparam>
        public static TInterface[] ResolveAll<TInterface>()
        {
            return ContainerConfiguration.Container.ResolveAll<TInterface>();
        }

        private static IEnumerable<object> GetResolvedParameters(IEnumerable<ParameterInfo> constructorParameters, object parameters)
        {
            var container = ContainerConfiguration.Container;
            IDictionary<string, object> preinitializedParameters = new Dictionary<string, object>();

            if (parameters != null)
            {
                preinitializedParameters = DictionaryHelper.ToDictionary(parameters, p => p, v => v);
            }

            foreach (var constructorParameter in constructorParameters)
            {
                if (preinitializedParameters.ContainsKey(constructorParameter.Name))
                {
                    yield return preinitializedParameters[constructorParameter.Name];
                }
                else
                {
                    yield return container.Resolve(constructorParameter.ParameterType);
                }
            }
        }

        /// <summary>
        /// Find type definition looking in multiple assemblies
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>instance</returns>
        public static T CreateInstance<T>()
        {
            return CreateInstance<T>(typeof(T));
        }

        /// <summary>
        /// Find type definition looking in multiple assemblies
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters">Constructor parameters</param>
        /// <returns>instance</returns>
        public static T CreateInstance<T>(params object[] parameters)
        {
            return CreateInstance<T>(typeof(T), parameters);
        }

        /// <summary>
        /// Find namespace looking in multiple assemblies
        /// </summary>
        /// <param name="nameSpace"></param>
        /// <returns>Type infomation</returns>
        public static bool FindNamespaceInLoadedAssemblies(string nameSpace)
        {
            if (string.IsNullOrEmpty(nameSpace))
            {
                throw new ArgumentNullException(nameof(nameSpace));
            }

            return TypeHelper.GetLoadedTypes().Any(type => type.Namespace == nameSpace);
        }

        /// <summary>
        /// Release object when no longer needed. Container tracks instances of objects 
        /// that implement IDisposable to properly manage component lifecycle. This might lead to
        /// memory leaks when Resolve call is not matched with the corresponding Release call.
        /// </summary>
        /// <param name="instance">Object previously resolved with ReflectionHelper</param>
        public static void Release(object instance)
        {
            ContainerConfiguration.Container.Release(instance);
        }

        private static object CreateObjectInstance(Type type, object[] parameterValues)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var constructors = type.GetConstructors();

            var constructor = parameterValues == null
                ? constructors.FirstOrDefault(c => c.GetParameters().All(p => p.HasDefaultValue))
                : constructors.FirstOrDefault(c => HasMatchingParameters(c, parameterValues, false));

            if (constructor == null && parameterValues != null)
            {
                constructor = constructors.FirstOrDefault(c => HasMatchingParameters(c, parameterValues, true));
            }

            if (constructor == null)
            {
                var expectedParameters = parameterValues == null
                    ? "(all default or none)"
                    : string.Join(", ", parameterValues.Select(p => p?.GetType().Name ?? "<null>"));

                throw new InvalidOperationException($"Could not find a suitable constructor to create an instance of {type.Name}. Expected parameters: {expectedParameters}");
            }

            parameterValues = parameterValues ?? new object[0];
            var constructorParameters = constructor.GetParameters();

            if (parameterValues.Length < constructorParameters.Length)
            {
                parameterValues = parameterValues
                    .Union(constructorParameters
                        .Skip(parameterValues.Length)
                        .Select(p => p.DefaultValue))
                    .ToArray();
            }

            return Activator.CreateInstance(type, parameterValues);
        }

        private static bool HasMatchingParameters(ConstructorInfo constructor, object[] parameterValues, bool allowDefaultTail)
        {
            var constructorParameters = constructor.GetParameters();

            if (constructorParameters.Length > parameterValues.Length && allowDefaultTail)
            {
                parameterValues = parameterValues
                    .Union(Enumerable.Repeat(Type.Missing,
                        constructorParameters.Length - parameterValues.Length))
                    .ToArray();
            }

            if (constructorParameters.Length != parameterValues.Length)
            {
                return false;
            }

            for (var i = 0; i < constructorParameters.Length; i++)
            {
                var parameterValue = parameterValues[i];
                var constructorParameter = constructorParameters[i];

                if (parameterValue == Type.Missing)
                {
                    if (constructorParameter.HasDefaultValue)
                    {
                        continue;
                    }

                    return false;
                }

                var constructorParameterType = constructorParameter.ParameterType;
                var parameterType = parameterValue?.GetType();

                if (parameterType == null)
                {
                    if (TypeHelper.IsNullable(constructorParameterType)
                        || !constructorParameterType.IsValueType)
                    {
                        continue;
                    }

                    return false;
                }

                if (!constructorParameterType.IsAssignableFrom(parameterType))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Sets the value of the property pointed by expression.
        /// </summary>
        /// <param name="target">target class</param>
        /// <param name="memberLamda">property descriptor</param>
        /// <param name="value">value to set</param>
        public static void SetPropertyValue<TTargetObject, TValue>(this TTargetObject target, Expression<Func<TTargetObject, TValue>> memberLamda, TValue value)
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            var property = memberSelectorExpression.Member as PropertyInfo;
            property.SetValue(target, value, null);
        }
    }
}