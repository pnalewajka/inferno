using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper methods related to property processing via reflection
    /// </summary>
    public class PropertyHelper
    {
        /// <summary>
        /// Get property
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propertyNameExpression"></param>
        /// <returns></returns>
        public static PropertyInfo GetProperty<TObject>(Expression<Func<TObject, object>> propertyNameExpression)
        {
            if (propertyNameExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyNameExpression));
            }

            var lambda = propertyNameExpression as LambdaExpression;

            return GetPropertyInfo(lambda);
        }

        /// <summary>
        /// Get property
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propertyNameExpression"></param>
        /// <returns></returns>
        public static PropertyInfo GetProperty(LambdaExpression propertyNameExpression)
        {
            if (propertyNameExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyNameExpression));
            }

            return GetPropertyInfo(propertyNameExpression);
        }

        /// <summary>
        /// Gets the friendly name from given member info
        /// </summary>
        /// <param name="memberInfo">Member info of a given member</param>
        /// <returns>string which can be used i.e. in user interface</returns>
        public static string GetPropertyFriendlyName([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            var displayAttribute = AttributeHelper.GetMemberAttribute(memberInfo, (DisplayAttribute)null);

            if (displayAttribute != null)
            {
                return displayAttribute.GetName();
            }

            var displayNameAttribute = AttributeHelper.GetMemberAttribute(memberInfo, (DisplayNameAttribute)null);

            if (displayNameAttribute != null)
            {
                return displayNameAttribute.DisplayName;
            }

            return NamingConventionHelper.ConvertPascalCaseToLabel(memberInfo.Name);
        }

        /// <summary>
        /// Get name of specific property
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propertyNameExpression"></param>
        /// <returns></returns>
        public static string GetPropertyName<TObject>(Expression<Func<TObject, object>> propertyNameExpression)
        {
            return GetProperty(propertyNameExpression).Name;
        }

        /// <summary>
        /// Get name of specific property
        /// </summary>
        /// <param name="propertyNameExpression"></param>
        /// <returns></returns>
        public static string GetPropertyName(LambdaExpression propertyNameExpression)
        {
            return GetPropertyInfo(propertyNameExpression).Name;
        }

        /// <summary>
        /// Get names of property chain properties
        /// </summary>
        /// <param name="propertyPathExpression"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetPropertyNames<TObject>(Expression<Func<TObject, object>> propertyPathExpression)
        {
            return GetMemberInfos(propertyPathExpression).Select(p => p.Name);
        }

        /// <summary>
        /// Get type of specific property
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propertyNameExpression"></param>
        /// <returns></returns>
        public static Type GetPropertyType<TObject>(Expression<Func<TObject, object>> propertyNameExpression)
        {
            return GetProperty(propertyNameExpression).PropertyType;
        }

        /// <summary>
        /// Tries to find property in defined type by passed name and returs its type
        /// </summary>
        /// <typeparam name="T">Type where property will be searched</typeparam>
        /// <param name="name">Name of property</param>
        /// <returns></returns>
        public static Type GetPropertyTypeByNameOrDefault<T>(string name)
        {
            var propertyInfo = typeof(T).GetProperties()
                                        .SingleOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));

            return propertyInfo?.PropertyType;
        }

        /// <summary>
        /// Get list of properties names
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propertyNameExpressions"></param>
        /// <returns>List with properties names</returns>
        public static IEnumerable<string> GetPropertyNames<TObject>(params Expression<Func<TObject, object>>[] propertyNameExpressions)
        {
            return propertyNameExpressions.Select(GetPropertyName);
        }

        public static LambdaExpression ExtractLambdaExpression(Expression expression)
        {
            switch (expression)
            {
                case UnaryExpression u: return ExtractLambdaExpression(u.Operand);
                case MemberExpression m: return ExtractLambdaExpression(m.Expression);
            }

            return (LambdaExpression)expression;
        }

        /// <summary>
        /// Returns path to property as defined in expression
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="propertyAccessExpression"></param>
        /// <returns>Property path</returns>
        public static string GetPropertyPath<TObject, TValue>(Expression<Func<TObject, TValue>> propertyAccessExpression)
        {
            if (propertyAccessExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyAccessExpression));
            }

            MemberExpression memberExpression;
            var unaryExpression = propertyAccessExpression.Body as UnaryExpression;

            if (unaryExpression != null)
            {
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression)propertyAccessExpression.Body;
            }

            var stack = new Stack<string>();

            while (memberExpression != null)
            {
                stack.Push(memberExpression.Member.Name);
                memberExpression = memberExpression.Expression as MemberExpression;
            }

            return string.Join(".", stack);
        }

        /// <summary>
        /// Returns member info for all elements of the property path
        /// </summary>
        /// <param name="propertyAccessExpression">expression describing property path</param>
        /// <returns></returns>
        public static IEnumerable<MemberInfo> GetPropertyPathMemberInfo<TObject, TResult>(
            Expression<Func<TObject, TResult>> propertyAccessExpression)
        {
            if (propertyAccessExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyAccessExpression));
            }

            return GetPropertyPathMemberInfoFromExpression(propertyAccessExpression);
        }

        /// <summary>
        /// Returns value of a public static property from the specified class.
        /// </summary>
        /// <typeparam name="TClass">Class with the static property</typeparam>
        /// <typeparam name="TProperty">Property type</typeparam>
        /// <param name="propertyName">Name of the static public property to retrieve</param>
        /// <param name="includingNonPublic">Should include non-public properties?</param>
        /// <returns>Value of the specified property</returns>
        public static TProperty GetStaticPropertyValue<TClass, TProperty>(string propertyName, bool includingNonPublic = false)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                throw new ArgumentException(@"Must provide non-empty property name.", nameof(propertyName));
            }

            var type = typeof(TClass);

            return GetStaticPropertyValue<TProperty>(type, propertyName, includingNonPublic);
        }

        /// <summary>
        /// Returns value of a static property from the specified class.
        /// </summary>
        /// <typeparam name="TProperty">Property type</typeparam>
        /// <param name="type">Class with the static property</param>
        /// <param name="propertyName">Name of the static public property to retrieve</param>
        /// <param name="includingNonPublic">Should include non-public properties?</param>
        /// <returns>Value of the specified property</returns>
        public static TProperty GetStaticPropertyValue<TProperty>(Type type, string propertyName, bool includingNonPublic = false)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                throw new ArgumentException(@"Must provide non-empty property name.", nameof(propertyName));
            }

            var bindingFlags = BindingFlags.Public | BindingFlags.Static;
            if (includingNonPublic)
            {
                bindingFlags |= BindingFlags.NonPublic;
            }

            var staticProperty = type.GetProperty(propertyName, bindingFlags);
            return (TProperty)staticProperty.GetValue(null);
        }

        /// <summary>
        /// Returns type of property from specific class
        /// </summary>
        /// <typeparam name="TSourceObject">Type of object which contains property</typeparam>
        /// <param name="propertyName">Name of property</param>
        /// <returns>Property type</returns>
        public static Type GetPropertyType<TSourceObject>(string propertyName)
        {
            return typeof(TSourceObject).GetProperty(propertyName).PropertyType;
        }

        /// <summary>
        /// Returns type of property from specific class
        /// </summary>
        /// <param name="propertyName">Name of property</param>
        /// <param name="sourceType">Type of object which contains property</param>
        /// <returns>Property type</returns>
        public static Type GetPropertyType(string propertyName, Type sourceType)
        {
            return sourceType.GetProperty(propertyName).PropertyType;
        }

        /// <summary>
        /// Checks if provided property is virtual one
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns>null if property is 'mis-configured' - setter and getter are different in terms of virtual and final fields</returns>
        public static bool? IsVirtual(PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
            {
                throw new ArgumentNullException(nameof(propertyInfo));
            }

            bool? found = null;

            foreach (var method in propertyInfo.GetAccessors())
            {
                if (found.HasValue)
                {
                    if (found.Value != method.IsVirtual && !method.IsFinal)
                    {
                        return null;
                    }
                }
                else
                {
                    found = method.IsVirtual && !method.IsFinal;
                }
            }

            return found;
        }

        /// <summary>
        /// Get instance public property by name
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propertyName"></param>
        /// <param name="throwOnError"></param>
        /// <returns></returns>
        public static PropertyInfo GetInstancePublicPropertyByName([NotNull]Type type, [NotNull]string propertyName, bool throwOnError = true)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            var propertyInfo = type.GetProperties(BindingFlags.Instance | BindingFlags.Public).SingleOrDefault(p => p.Name == propertyName);

            if (propertyInfo == null && throwOnError)
            {
                throw new ArgumentOutOfRangeException(nameof(propertyName));
            }

            return propertyInfo;
        }

        /// <summary>
        /// Get list of all public properties of instance
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IList<PropertyInfo> GetInstancePublicProperties([NotNull]Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList();
        }

        /// <summary>
        /// Get a path to a (possibly nested) member
        /// </summary>
        /// <param name="lambda">Member expression to examine</param>
        /// <returns>Enumerable of members on the specified path</returns>
        public static IEnumerable<MemberInfo> GetMemberInfos(LambdaExpression lambda)
        {
            var result = new Stack<MemberInfo>();

            var body = lambda.Body;

            while (true)
            {
                MemberExpression memberExpression = null;
                var unaryExpression = body as UnaryExpression;

                if (unaryExpression != null)
                {
                    while (unaryExpression.Operand is UnaryExpression insideUnnaryExpression)
                    {
                        unaryExpression = insideUnnaryExpression;
                    }

                    memberExpression = unaryExpression.Operand as MemberExpression;
                }
                else if (body is MemberExpression)
                {
                    memberExpression = (MemberExpression)body;
                }

                if (memberExpression == null)
                {
                    break;
                }

                result.Push(memberExpression.Member);
                body = memberExpression.Expression;
            }

            return result;
        }

        /// <summary>
        /// Gets the lambda expression of getting property with certain name
        /// </summary>
        /// <typeparam name="TClass">Type of class which contains the property</typeparam>
        /// <param name="propertyInfo">Property info</param>
        public static Expression<Func<TClass, object>> GetSelectorForProperty<TClass>(PropertyInfo propertyInfo)
        {
            return (Expression<Func<TClass, object>>)GetSelectorForProperty(typeof(TClass), propertyInfo);
        }

        /// <summary>
        /// Gets the lambda expression of getting property with certain name
        /// </summary>
        /// <param name="type">Type of class which contains the property</typeparam>
        /// <param name="propertyInfo">Property</param>
        public static LambdaExpression GetSelectorForProperty(Type type, PropertyInfo propertyInfo)
        {
            var p = Expression.Parameter(type);
            var expr = Expression.Lambda(
                Expression.Convert(
                    Expression.PropertyOrField(
                        Expression.Convert(p, type), propertyInfo.Name), typeof(object)), p);

            return expr;
        }

        /// <summary>
        /// Returns value of a property from the specified class.
        /// </summary>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="sourceObject">Object with the property</param>
        /// <param name="propertyName">Property name</param>
        /// <returns>Value of a property</returns>
        public static TPropType GetPropertyValue<TPropType>(object sourceObject, string propertyName) where TPropType : class
        {
            if (sourceObject == null)
            {
                throw new ArgumentNullException(nameof(sourceObject));
            }

            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            return sourceObject.GetType().GetProperty(propertyName)?.GetValue(sourceObject) as TPropType;
        }

        /// <summary>
        /// Sets the property value of a specified object
        /// </summary>
        /// <param name="sourceObject">Object with the property</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="propertyValue">Property value</param>
        public static void SetValue(object sourceObject, string propertyName, object propertyValue)
        {
            if (sourceObject == null)
            {
                throw new ArgumentNullException(nameof(sourceObject));
            }

            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            PropertyInfo propertyInfo = sourceObject.GetType().GetProperty(propertyName);
            propertyInfo.SetValue(sourceObject, propertyValue);
        }

        /// <summary>
        /// Returns a well-formatted representation of a member path
        /// </summary>
        /// <param name="memberName">Name of the parent member</param>
        /// <param name="subMemberName">Name of sub-member</param>
        /// <returns>Formatted member path</returns>
        public static string CombinePropertyPath(string memberName, string subMemberName)
        {
            return string.IsNullOrEmpty(subMemberName) ? memberName : $"{memberName}.{subMemberName}";
        }

        private static PropertyInfo GetPropertyInfo(LambdaExpression lambda)
        {
            MemberExpression memberExpression;
            var unaryExpression = lambda.Body as UnaryExpression;

            if (unaryExpression != null)
            {
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression)lambda.Body;
            }

            var propertyInfo = (PropertyInfo)memberExpression.Member;

            return propertyInfo;
        }

        private static IEnumerable<MemberInfo> GetPropertyPathMemberInfoFromExpression(Expression expression)
        {
            var memberExpression = expression as MemberExpression;

            if (memberExpression != null)
            {
                return
                    GetPropertyPathMemberInfoFromExpression(memberExpression.Expression)
                        .Concat(new[] { memberExpression.Member });
            }

            var lambdaExpression = expression as LambdaExpression;

            if (lambdaExpression != null)
            {
                return GetPropertyPathMemberInfoFromExpression(lambdaExpression.Body);
            }

            var unaryExpression = expression as UnaryExpression;

            if (unaryExpression != null)
            {
                return GetPropertyPathMemberInfoFromExpression(unaryExpression.Operand);
            }

            var methodCallExpression = expression as MethodCallExpression;

            if (methodCallExpression != null && methodCallExpression.Arguments.Count <= 2)
            {
                return methodCallExpression.Arguments.SelectMany(GetPropertyPathMemberInfoFromExpression);
            }

            var binaryExpression = expression as BinaryExpression;

            if (binaryExpression != null)
            {
                return GetPropertyPathMemberInfoFromExpression(binaryExpression.Left)
                    .Concat(GetPropertyPathMemberInfoFromExpression(binaryExpression.Right));
            }

            if (expression is ParameterExpression || expression is ConstantExpression)
            {
                return Enumerable.Empty<MemberInfo>();
            }

            throw new ArgumentException(@"Unsupported expression type", nameof(expression));
        }
    }
}
