﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class ExpressionParameterHelper
    {
        /// <summary>
        /// Replaces all parameters from 'expression' with other Expression
        /// </summary>
        /// <param name="expression">expression to be modified</param>
        /// <param name="replacements">Parameters to be replaced</param>
        /// <returns></returns>
        public static LambdaExpression Replace(LambdaExpression expression, IDictionary<ParameterExpression, Expression> replacements)
        {
            return (LambdaExpression)(new ParameterReplacerVisitor(replacements).Visit(expression));
        }

        private class ParameterReplacerVisitor : DynamicExpressionVisitor
        {
            private readonly IDictionary<ParameterExpression, Expression> _replacements;

            public ParameterReplacerVisitor(IDictionary<ParameterExpression, Expression> replacements)
            {
                _replacements = replacements;
            }

            protected override Expression VisitLambda<T>(Expression<T> node)
            {
                var parameters =
                    node.Parameters.Where(
                        p => !_replacements.ContainsKey(p) || !(_replacements[p] is ConstantExpression));

                return Expression.Lambda(Visit(node.Body), parameters);
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                if (_replacements.ContainsKey(node))
                {
                    return _replacements[node];
                }

                return base.VisitParameter(node);
            }
        }
    }
}