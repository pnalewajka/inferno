﻿using System;
using System.Net.Mail;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class EmailHelper
    {
        public static bool IsValidEmail(string email)
        {
            if (String.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            try
            {
                var m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
