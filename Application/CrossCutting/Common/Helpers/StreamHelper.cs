﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Stream helper methods
    /// </summary>
    public static class StreamHelper
    {
        private static readonly byte[] ByteOrderMarkUtf8 = Encoding.UTF8.GetPreamble();
        /// <summary>
        /// Get string from stream, decoded using passed encoding 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="encoding">Requested encoding, defaults to UTF8</param>
        /// <returns></returns>
        public static string StreamToString(Stream stream, Encoding encoding = null)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            stream.Position = 0;

            if (!stream.CanRead || !stream.CanSeek)
            {
                throw new ArgumentException();
            }

            var buffer = new byte[stream.Length];

            var startingIndex = 0;
            var bytesToReadCount = Convert.ToInt32(stream.Length);

            stream.Read(buffer, 0, bytesToReadCount);
            stream.Position = 0;

            if (buffer.Length >= ByteOrderMarkUtf8.Length && !ByteOrderMarkUtf8.Where((t, i) => buffer[i] != t).Any())
            {
                startingIndex = ByteOrderMarkUtf8.Length;
                bytesToReadCount -= ByteOrderMarkUtf8.Length;
            }

            return encoding.GetString(buffer, startingIndex, bytesToReadCount);
        }

        /// <summary>
        /// Return stream from passed xDocument
        /// </summary>
        /// <param name="document"></param>
        /// <param name="xmlWriterSettings"></param>
        /// <returns>Data stream, remember to dispose it after use</returns>
        public static MemoryStream XDocumentToStream(XDocument document, XmlWriterSettings xmlWriterSettings = null)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            var memoryStream = new MemoryStream();

            using (var xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings))
            {
                document.WriteTo(xmlWriter);
                xmlWriter.Flush();
            }

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        public static void StreamToFile(MemoryStream memoryStream, string fileName)
        {
            using (FileStream fs = File.OpenWrite(fileName))
            {
                memoryStream.CopyTo(fs);
            }
        }

        /// <summary>
        /// Gets all bytes from the stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static byte[] ReadBytes(Stream stream)
        {
            var memoryStream = stream as MemoryStream;

            if (memoryStream != null)
            {
                return memoryStream.ToArray();
            }

            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, (int)stream.Length);

            return buffer;
        }
    }
}