﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper for working with IdentifierAttribute and types marked with it
    /// </summary>
    public class IdentifierHelper
    {
        private static IReadOnlyDictionary<string, Type> _identifiableTypes;
        private readonly static object _identifiableTypesLock = new object();

        private static readonly Dictionary<Type, string> BasicTypes = new Dictionary<Type, string>
            {
                { typeof(int), "int" },
                { typeof(long), "long" },
                { typeof(decimal), "decimal" },
                { typeof(string), "string" },
                { typeof(bool), "bool" },
                { typeof(DateTime), "DateTime"},
                { typeof(TimeSpan), "TimeSpan" },
                { typeof(Guid), "Guid" },
                { typeof(int[]), "int[]" },
                { typeof(long[]), "long[]" },
                { typeof(decimal[]), "decimal[]" },
                { typeof(string[]), "string[]" },
                { typeof(bool[]), "bool[]" },
                { typeof(DateTime[]), "DateTime[]"},
                { typeof(TimeSpan[]), "TimeSpan[]" },
            };

        /// <summary>
        /// Gets the type by its identifier
        /// </summary>
        /// <param name="identifier">identifier used to mark the type</param>
        /// <param name="throwOnError">should exception be thrown when type not found?</param>
        /// <returns>Type marked with the given identifier</returns>
        public static Type GetTypeByIdentifier(string identifier, bool throwOnError = true)
        {
            Type type;

            if (identifier == null && !throwOnError)
            {
                return null;
            }

            if (_identifiableTypes == null)
            {
                lock (_identifiableTypesLock)
                {
                    if (_identifiableTypes == null)
                    {
                        _identifiableTypes = GetIdentifiableTypes();
                    }
                }
            }

            if (!_identifiableTypes.TryGetValue(identifier, out type) && throwOnError)
            {
                var message = $"Missing type with id {identifier}";
                throw new InvalidDataException(message);
            }

            return type;
        }

        /// <summary>
        /// Returns identifier assigned with IdentifierAttribute to the given type
        /// </summary>
        /// <param name="type">Type marked with the IdentifierAttribute</param>
        /// <param name="throwOnError">should exception be thrown when type not found?</param>
        /// <returns>Value of the identifier</returns>
        public static string GetTypeIdentifier(Type type, bool throwOnError = true)
        {
            if (BasicTypes.ContainsKey(type))
            {
                return BasicTypes[type];
            }

            var identifierAttribute = type.GetCustomAttributes<IdentifierAttribute>(false).SingleOrDefault();

            if (identifierAttribute == null)
            {
                if (throwOnError)
                {
                    var message = $"Missing identifier for type {type.FullName}";
                    throw new InvalidDataException(message);
                }

                return null;
            }

            return identifierAttribute.Value;
        }

        private static Dictionary<string, Type> GetIdentifiableTypes()
        {
            var typesMarkedWithIntanceIdentifier =
                from t in TypeHelper.GetLoadedTypes()
                let attributes = t.GetCustomAttributes(typeof(IdentifierAttribute), false)
                where attributes != null && attributes.Length > 0
                select new { Type = t, Identifier = attributes.Cast<IdentifierAttribute>().First().Value };

            var identifiableTypes = typesMarkedWithIntanceIdentifier.ToDictionary(p => p.Identifier, p => p.Type);

            foreach (var item in BasicTypes)
            {
                identifiableTypes[item.Value] = item.Key;
            }

            return identifiableTypes;
        }
    }
}