﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Support methods for url field/value conversions
    /// </summary>
    public static class UrlFieldsHelper
    {
        private const char PrefixSeparator = '.';

        /// <summary>
        /// Creates object based on the query parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="isRequired"></param>
        /// <param name="urlFieldPrefix"></param>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public static TObject CreateObjectFromQueryParameters<TObject>(
            IDictionary<string, object> parameters,
            bool isRequired,
            string urlFieldPrefix = null)
        {
            var mappings = GetUrlFieldMappingToObjectProperty<TObject>(urlFieldPrefix);
            parameters = parameters
                .Where(p => mappings.ContainsKey(p.Key))
                .ToDictionary(p => mappings[p.Key], p => p.Value);

            if (!isRequired)
            {
                var contextType = typeof(TObject);
                var expectedPropertyNames = TypeHelper.GetPublicInstancePropertyNames(contextType);

                if (!expectedPropertyNames.Any(parameters.ContainsKey))
                {
                    return default(TObject);
                }
            }

            var viewModelFactory = ReflectionHelper.TryResolveInterface<IViewModelFactory<TObject>>();
            var context = viewModelFactory != null ? viewModelFactory.Create() : ReflectionHelper.CreateInstance<TObject>();

            var convertSettings = new ConvertTypeSettings
            {
                EnumNameConversion = EnumNameConversion.ToHyphenated,
                FormatProvider = CultureInfo.InvariantCulture
            };

            DictionaryHelper.PopulateObjectProperties(context, parameters, convertSettings);

            return (TObject)context;
        }

        /// <summary>
        /// Returns query (url) parameters based on the object
        /// </summary>
        /// <param name="context" />
        /// <param name="urlFieldPrefix">Prefix added to the URI parameter name</param>
        /// <returns></returns>
        public static IDictionary<string, object> CreateQueryParametersFromObject(object context, string urlFieldPrefix = null)
        {
            if (context == null)
            {
                return null;
            }

            var mappings = GetObjectPropertyToUrlFieldMapping(context.GetType(), urlFieldPrefix);

            return DictionaryHelper.ToDictionary(context, k => mappings[k], v => v);
        }

        /// <summary>
        /// Get mappings between TObject properties and it's query string presentations:
        ///     UrlFieldNameAttribute.Name if attribute was used
        ///     PascalCaseToHypenated if no attribute was used
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="urlFieldPrefix">Prefix added to the URI parameter name</param>
        /// <returns></returns>
        public static IDictionary<string, string> GetObjectPropertyToUrlFieldMapping(Type objectType, string urlFieldPrefix = null)
        {
            var properties = TypeHelper.GetPublicInstanceProperties(objectType);
            var mappingDictionary = new Dictionary<string, string>();

            foreach (var property in properties)
            {
                var urlFieldNameAttribute = AttributeHelper.GetPropertyAttribute<UrlFieldNameAttribute>(property);
                var urlFieldName = GetUrlFieldName(urlFieldNameAttribute, property, urlFieldPrefix);

                mappingDictionary.Add(property.Name, urlFieldName);
            }

            return mappingDictionary;
        }

        /// <summary>
        /// Get mappings between TObject properties and it's query string presentations:
        ///     UrlFieldNameAttribute.Name if attribute was used
        ///     PascalCaseToHypenated if no attribute was used
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public static IDictionary<string, string> GetObjectPropertyToUrlFieldMapping<TObject>(string urlFieldPrefix = null)
        {
            return GetObjectPropertyToUrlFieldMapping(typeof (TObject), urlFieldPrefix);
        }

        /// <summary>
        /// Get mappings between query string and TObject properties presentations. Key will calculated as follows:
        ///     PascalCaseToHypenated if no attribute was used
        ///     UrlFieldNameAttribute.Name if attribute was used
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="urlFieldPrefix"></param>
        /// <returns></returns>
        public static IDictionary<string, string> GetUrlFieldMappingToObjectProperty(Type objectType, string urlFieldPrefix = null)
        {
            var properties = TypeHelper.GetPublicInstanceProperties(objectType);
            var mappingDictionary = new Dictionary<string, string>();

            foreach (var property in properties)
            {
                var urlFieldNameAttribute = AttributeHelper.GetPropertyAttribute<UrlFieldNameAttribute>(property);
                var urlFieldName = GetUrlFieldName(urlFieldNameAttribute, property, urlFieldPrefix);

                mappingDictionary.Add(urlFieldName, property.Name);
            }

            return mappingDictionary;
        }

        /// <summary>
        /// Get mappings between query string and TObject properties presentations. Key will calculated as follows:
        ///     PascalCaseToHypenated if no attribute was used
        ///     UrlFieldNameAttribute.Name if attribute was used
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public static IDictionary<string, string> GetUrlFieldMappingToObjectProperty<TObject>(string urlFieldPrefix = null)
        {
            return GetUrlFieldMappingToObjectProperty(typeof (TObject), urlFieldPrefix);
        }

        /// <summary>
        /// Get url field name without prefix.
        /// </summary>
        /// <param name="urlFieldName"></param>
        /// <returns></returns>
        public static string GetUnprefixedUrlFieldName(string urlFieldName)
        {
            var prefixSeparatorIndex = urlFieldName.LastIndexOf(PrefixSeparator);

            return prefixSeparatorIndex == -1
                ? urlFieldName
                : urlFieldName.Substring(prefixSeparatorIndex + 1);
        }

        private static string GetUrlFieldName(UrlFieldNameAttribute attribute, PropertyInfo property, string urlFieldPrefix)
        {
            var uriFieldName = attribute != null
                    ? attribute.Name
                    : NamingConventionHelper.ConvertPascalCaseToHyphenated(property.Name);

            if (!urlFieldPrefix.IsNullOrEmpty())
            {
                uriFieldName =
                    $"{NamingConventionHelper.ConvertPascalCaseToHyphenated(urlFieldPrefix)}{PrefixSeparator}{uriFieldName}";
            }

            return uriFieldName;
        }
    }
}
