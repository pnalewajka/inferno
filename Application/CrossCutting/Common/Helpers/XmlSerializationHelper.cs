﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Xml Seriazation helper class
    /// </summary>
    public static class XmlSerializationHelper
    {
        /// <summary>
        /// Serialize given object and type to xml
        /// </summary>
        /// <param name="data">Object to serialize</param>
        /// <param name="type">Type of object</param>
        /// <returns></returns>
        public static string SerializeToXml(object data, Type type)
        {
            using (var memoryStream = new MemoryStream())
            {
                var serializer = new XmlSerializer(type);

                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };

                using (var xmlWriter = XmlWriter.Create(memoryStream, settings))
                {
                    serializer.Serialize(xmlWriter, data);
                }

                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }

        /// <summary>
        /// Serialize given object and type to xml. Generic version.
        /// </summary>
        /// <typeparam name="TClass">Type of the object</typeparam>
        /// <param name="data">Object to serialize</param>
        /// <returns></returns>
        public static string SerializeToXml<TClass>(TClass data) where TClass : class
        {
            return SerializeToXml(data, typeof (TClass));
        }

        /// <summary>
        /// Deserialize object from xml using specific type
        /// </summary>
        /// <param name="xml">Xml with serialized object</param>
        /// <param name="type">Type of object</param>
        /// <returns></returns>
        public static object DeserializeFromXml(string xml, Type type)
        {
            var buffer = Encoding.UTF8.GetBytes(xml);

            using (var memoryStream = new MemoryStream(buffer))
            {
                var readerQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = int.MaxValue
                };

                using (var reader = XmlDictionaryReader.CreateTextReader(memoryStream, readerQuotas))
                {
                    var serializer = new XmlSerializer(type);
                    var result = serializer.Deserialize(reader);

                    return result;
                }
            }
        }

        /// <summary>
        /// Deserialize object from xml using specific type. Generic version.
        /// </summary>
        /// <typeparam name="TClass">Type of object</typeparam>
        /// <param name="xml">Xml with serialized object</param>
        /// <returns></returns>
        public static TClass DeserializeFromXml<TClass>(string xml) where TClass : class
        {
            return (TClass) DeserializeFromXml(xml, typeof (TClass));
        }

        /// <summary>
        /// Serialize object to XElement
        /// </summary>
        /// <typeparam name="TClass">Type of object</typeparam>
        /// <param name="data">Object to serialize</param>
        /// <returns></returns>
        public static XElement SerializeToXElement<TClass>(TClass data)
        {
            return ToXElement(data, typeof(TClass));
        }

        /// <summary>
        /// Deserialize Exception starting from any parent derived from Exception
        /// </summary>
        /// <typeparam name="TException">Type of exception</typeparam>
        /// <param name="xml">Xml with serialized exception</param>
        /// <returns></returns>
        public static TException DeserializeExceptionFromXml<TException>(string xml) where TException : Exception
        {
            var buffer = Encoding.UTF8.GetBytes(xml);
            var type = typeof(TException);

            var attributeOverrides = new XmlAttributeOverrides();
            var attributes = new XmlAttributes { XmlIgnore = true };
            attributeOverrides.Add(typeof(Exception), "Data", attributes);
            attributeOverrides.Add(typeof(Exception), "HResult", attributes);

            using (var memoryStream = new MemoryStream(buffer))
            {
                var readerQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = int.MaxValue
                };

                using (var reader = XmlDictionaryReader.CreateTextReader(memoryStream, readerQuotas))
                {
                    var serializer = new XmlSerializer(type, attributeOverrides, TypeHelper.GetSubclassesOf(typeof(BusinessException)).ToArray(), null, null);
                    var result = serializer.Deserialize(reader);

                    return (TException)result;
                }
            }
        }

        private static XElement Serialize(XmlSerializer xmlSerializer, object data, XmlSerializerNamespaces namespaces)
        {
            var document = new XDocument();

            using (var writer = document.CreateWriter())
            {
                xmlSerializer.Serialize(writer, data, namespaces);
            }

            var element = document.Root;

            if (element == null)
            {
                return null;
            }

            element.Remove();

            return element;
        }

        private static XElement ToXElement(object data, Type type)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var serializer = new XmlSerializer(type);

            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            return Serialize(serializer, data, namespaces);
        }
    }
}