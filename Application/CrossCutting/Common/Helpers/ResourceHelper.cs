﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Set of methods for resource extraction
    /// </summary>
    public class ResourceHelper
    {
        private readonly static IDictionary<Type, ResourceManager> _resourceManagerCache = new ConcurrentDictionary<Type, ResourceManager>();

        /// <summary>
        /// Get stream for resource basing on resource fullResourceName
        /// </summary>
        /// <param name="fullResourceName">Full name of the resource</param>
        /// <param name="assembly">Assembly to extract resource from, if null then calling assembly will be used</param>
        /// <returns>Data stream. Please remember to dispose stream after using</returns>
        public static Stream GetResource(string fullResourceName, Assembly assembly = null)
        {
            if (assembly == null)
            {
                assembly = Assembly.GetCallingAssembly();
            }

            if (!assembly.GetManifestResourceNames().Contains(fullResourceName))
            {
                throw new ResourceNotFoundException("Resource was not found in assembly manifest", fullResourceName, assembly.FullName);
            }

            return assembly.GetManifestResourceStream(fullResourceName);
        }

        /// <summary>
        /// Get string for resource basing on resource fullResourceName
        /// </summary>
        /// <param name="fullResourceName">Full name of the resource</param>
        /// <param name="assembly">Assembly to extract resource from, if null then calling assembly will be used</param>
        /// <returns>Data from resource as string</returns>
        public static string GetResourceAsString(string fullResourceName, Assembly assembly = null)
        {
            if (assembly == null)
            {
                assembly = Assembly.GetCallingAssembly();
            }

            using (var stream = GetResource(fullResourceName, assembly))
            {
                var result = StreamHelper.StreamToString(stream);

                return result;
            }
        }

        /// <summary>
        /// Get string for resource basing on resource short name and type in same namespace
        /// </summary>
        /// <param name="shortResourceName">Last part of resource name</param>
        /// <param name="nearbyType">Type which assembly and namespace should be used to reference local resource</param>
        /// <returns>Data from resource as string</returns>
        public static string GetResourceAsStringByShortName(string shortResourceName, Type nearbyType)
        {
            var assembly = nearbyType.Assembly;
            var typeNamespace = nearbyType.Namespace;
            var resourcePath = $"{typeNamespace}.{shortResourceName}";

            return GetResourceAsString(resourcePath, assembly);
        }

        /// <summary>
        /// Get string for resource basing on resource name and type in same assembly
        /// </summary>
        /// <param name="fullResourceName">Full resource name</param>
        /// <param name="nearbyType">Type which assembly should be used to reference local resource</param>
        /// <returns>Data from resource as string</returns>
        public static string GetResourceAsString(string fullResourceName, Type nearbyType)
        {
            var assembly = nearbyType.Assembly;
            return GetResourceAsString(fullResourceName, assembly);
        }

        /// <summary>
        /// Get string for resource basing on resource manager type, resource key and culture code
        /// </summary>
        /// <param name="resourceManagerType">Resource manager type</param>
        /// <param name="resourceKey">Resource key</param>
        /// <param name="cultureInfo">Culture info</param>
        /// <returns>Data from resource as string</returns>
        public static string GetString(Type resourceManagerType, string resourceKey, CultureInfo cultureInfo = null)
        {
            if (!_resourceManagerCache.ContainsKey(resourceManagerType))
            {
                _resourceManagerCache.Add(resourceManagerType, ResourceManagerHelper.GetResourceManager(resourceManagerType));
            }

            var resourceManager = _resourceManagerCache[resourceManagerType];

            return resourceManager.GetString(resourceKey, cultureInfo ?? CultureInfo.CurrentCulture);
        }
    }
}