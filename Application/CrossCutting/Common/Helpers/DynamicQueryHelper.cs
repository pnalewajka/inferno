using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Helper for constructing dynamic IQuerable expressions for Where methods and OrderBy methods
    /// </summary>
    public static class DynamicQueryHelper
    {
        private class ParameterRebinder : ExpressionVisitor
        {
            private readonly Dictionary<ParameterExpression, ParameterExpression> _map;

            private ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
            {
                _map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
            }

            public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
            {
                return new ParameterRebinder(map).Visit(exp);
            }

            protected override Expression VisitParameter(ParameterExpression p)
            {
                ParameterExpression replacement;

                if (_map.TryGetValue(p, out replacement))
                {
                    p = replacement;
                }

                return base.VisitParameter(p);
            }
        }

        /// <summary>
        /// Builds order by expression
        /// </summary>
        /// <typeparam name="TEntityType">Type of the expression input </typeparam>
        /// <param name="propertyName">Name of the property which should be returned</param>
        /// <returns>Expression to be used in OrderBy mothods referencing given property from TEntityType</returns>
        public static Expression<Func<TEntityType, object>> BuildOrderByExpression<TEntityType>(string propertyName)
        {
            var propertyPath = new[] { propertyName };

            return BuildOrderByExpression<TEntityType>(propertyPath);
        }

        /// <summary>
        /// Builds order by expression
        /// </summary>
        /// <typeparam name="TEntityType">Type of the expression input </typeparam>
        /// <param name="propertyPath">Path to the property which should be returned</param>
        /// <returns>Expression to be used in OrderBy mothods referencing given property from TEntityType</returns>
        public static Expression<Func<TEntityType, object>> BuildOrderByExpression<TEntityType>(string[] propertyPath)
        {
            if (propertyPath == null)
            {
                throw new ArgumentNullException(nameof(propertyPath));
            }

            Type entityType = typeof(TEntityType);
            ParameterExpression arg = Expression.Parameter(entityType, "x");
            Expression expr = arg;

            Type propertyType = entityType;

            foreach (var propertyName in propertyPath)
            {
                PropertyInfo propertyInfo = propertyType.GetProperty(propertyName);
                expr = Expression.Property(expr, propertyInfo);
                propertyType = propertyType.GetProperty(propertyName).PropertyType;
            }

            if (TypeHelper.IsTypeOrNullableType<DateTime>(propertyType)
                || TypeHelper.IsTypeOrNullableType<bool>(propertyType)
                || TypeHelper.IsNumeric(propertyType)
                || TypeHelper.IsNullableEnum(propertyType)
                || propertyType.IsEnum)
            {
                expr = Expression.Convert(expr, typeof(object));
            }

            var delegateType = typeof(Func<,>).MakeGenericType(typeof(TEntityType), typeof(object));

            return (Expression<Func<TEntityType, object>>)Expression.Lambda(delegateType, expr, arg);
        }

        /// <summary>
        /// Builds search condition for a given property name
        /// </summary>
        /// <typeparam name="TEntity">Type of the entity</typeparam>
        /// <param name="searchColumnExpression">expression describing search column or search path</param>
        /// <param name="word">word which is required to be present in the property</param>
        /// <returns>Returns e => Property.Contains(word) like expression</returns>
        public static Expression<Func<TEntity, bool>> BuildSearchCondition<TEntity>(Expression<Func<TEntity, object>> searchColumnExpression, string word)
        {
            if (word == null)
            {
                throw new ArgumentNullException(nameof(word));
            }

            var parameterExpression = searchColumnExpression.Parameters.First();
            var propertyExpression = ExtractNonUnaryExpressionFromLambda(searchColumnExpression);
            var propertyType = ExtractPropertyType(propertyExpression);

            bool expressionIsMethodCall = propertyExpression.NodeType == ExpressionType.Call;

            var resultExpression = expressionIsMethodCall
                ? ApplyAnyForSearchCondition<TEntity>(propertyType, propertyExpression, parameterExpression, word)
                : ApplyComparisonsForSearchCondition<TEntity>(propertyType, propertyExpression, parameterExpression, word);

            return resultExpression;
        }

        private static Expression<Func<TEntity, bool>> ApplyComparisonsForSearchCondition<TEntity>(Type propertyType, Expression propertyExpression, ParameterExpression parameterExpression, string word)
        {
            var constrainedExpression = ConstrainExpressionForSearchCondition(word, propertyType, propertyExpression);

            return (Expression<Func<TEntity, bool>>)Expression.Lambda(constrainedExpression, parameterExpression);
        }

        private static Expression<Func<TEntity, bool>> ApplyAnyForSearchCondition<TEntity>(Type propertyType, Expression propertyExpression, ParameterExpression parameterExpression, string word)
        {
            var sourceExpression = Expression.Parameter(propertyType, "a");
            var constrainedExpression = ConstrainExpressionForSearchCondition(word, propertyType, sourceExpression);

            var anyMethod = typeof(Enumerable).GetMethods(BindingFlags.Static | BindingFlags.Public).First(m => m.Name == nameof(Enumerable.Any) && m.GetParameters().Length == 2);
            var genericAnyMethod = anyMethod.MakeGenericMethod(propertyType);
            var lambda = Expression.Lambda(constrainedExpression, sourceExpression);
            var resultExpression = Expression.Call(null, genericAnyMethod, propertyExpression, lambda);

            return (Expression<Func<TEntity, bool>>)Expression.Lambda(resultExpression, parameterExpression);
        }

        private static Expression ConstrainExpressionForSearchCondition(string word, Type propertyType, Expression expression)
        {
            if (propertyType == typeof(Guid))
            {
                return BuildSearchConditionForGuid(expression, word);
            }

            if (propertyType == typeof(string))
            {
                return BuildSearchConditionForString(expression, word);
            }

            if (TypeHelper.IsNumeric(propertyType))
            {
                return BuildSearchConditionForNumeric(expression, word);
            }

            if (propertyType.IsEnum)
            {
                return BuildSearchConditionForEnum(expression, word, propertyType);
            }

            if (TypeHelper.IsNullable(propertyType))
            {
                return BuildSearchConditionForNullable(expression, word);
            }

            throw new InvalidDataException($"Cannot build search condition for column of type {propertyType.Name}");
        }

        private static Type ExtractPropertyType(Expression propertyExpression)
        {
            if (propertyExpression.NodeType == ExpressionType.Call)
            {
                var methodCallExpression = (MethodCallExpression)propertyExpression;
                var lambdaExpression = (LambdaExpression)methodCallExpression.Arguments.Last();

                return ExtractPropertyType(lambdaExpression.Body);
            }

            if (TypeHelper.IsEnumeration(propertyExpression.Type))
            {
                return propertyExpression.Type.GetGenericArguments().First();
            }

            return propertyExpression.Type;
        }

        public static Expression<Func<TEntity, bool>> BuildEnumFilteringCondition<TEntity>(
            Expression<Func<TEntity, long?>> filteringColumnExpression, long value, bool isFlagged)
        {
            var columnSelector = new BusinessLogic<TEntity, long?>(filteringColumnExpression);
            var constraint = isFlagged
                ? new BusinessLogic<TEntity, bool>(e => (columnSelector.Call(e) & value) == value)
                : new BusinessLogic<TEntity, bool>(e => columnSelector.Call(e) == value);

            return constraint;
        }

        public static Expression<Func<TEntity, bool>> BuildEnumFilteringCondition<TEntity>(
            Expression<Func<TEntity, IEnumerable<long?>>> filteringColumnExpression, long value)
        {
            var columnSelector = new BusinessLogic<TEntity, IEnumerable<long?>>(filteringColumnExpression);
            var constraint = new BusinessLogic<TEntity, bool>(e => columnSelector.Call(e).Contains(value));

            return constraint;
        }

        /// <summary>
        /// Builds enum filtering condition for a given property name
        /// </summary>
        /// <typeparam name="TEntity">Type of the entity</typeparam>
        /// <param name="filteringColumnExpression">Wxpression describing filtering column or search path</param>
        /// <param name="value">Enum value to be used for comparison</param>
        /// <returns>Returns e => Property.Contains(word) like expression</returns>
        public static Expression<Func<TEntity, bool>> BuildEnumFilteringCondition<TEntity>(Expression<Func<TEntity, object>> filteringColumnExpression, Enum value)
        {
            var parameterExpression = filteringColumnExpression.Parameters.First();
            var propertyExpression = ExtractNonUnaryExpressionFromLambda(filteringColumnExpression);
            Type propertyType = ExtractPropertyType(propertyExpression);

            Expression constrainedExpression = null;

            if (propertyType == typeof(string))
            {
                constrainedExpression = BuildSearchConditionForString(propertyExpression, value.GetDescriptionOrValue());
            }
            else if (propertyType == typeof(int) || propertyType == typeof(long))
            {
                constrainedExpression = BuildNumericEqualityForValue<TEntity>(propertyExpression, propertyType, Convert.ToInt64(value));
            }
            else if (propertyType.IsEnum)
            {
                constrainedExpression = BuildEnumEqualityForValue(propertyExpression, propertyType, Convert.ToInt64(value));
            }
            else if (TypeHelper.IsNullableEnum(propertyType))
            {
                constrainedExpression = BuildNullableEnumEqualityForValue(propertyExpression, propertyType, Convert.ToInt64(value));
            }

            if (constrainedExpression == null)
            {
                throw new InvalidDataException($"Cannot build enum filtering condition for column of type {propertyType.Name}");
            }

            return (Expression<Func<TEntity, bool>>)Expression.Lambda(constrainedExpression, parameterExpression);
        }

        private static Expression ExtractNonUnaryExpressionFromLambda<TEntity>(Expression<Func<TEntity, object>> searchColumnExpression)
        {
            var expressionToReturn = searchColumnExpression.Body;

            while (expressionToReturn is UnaryExpression)
            {
                expressionToReturn = (expressionToReturn as UnaryExpression).Operand;
            }

            return expressionToReturn;
        }


        private static Expression BuildSearchConditionForNumeric(Expression expression, string word)
        {
            var toStringMethodInfo = GetToStringMethodInfo();

            return BuildSearchCondition(expression, null, word, toStringMethodInfo);
        }

        private static Expression BuildSearchConditionForGuid(Expression expression, string word)
        {
            var notNull = Expression.NotEqual(expression, Expression.Constant(Guid.Empty));
            var toStringMethodInfo = GetToStringMethodInfo();

            return BuildSearchCondition(expression, notNull, word, toStringMethodInfo);
        }

        private static Expression BuildSearchConditionForString(Expression expression, string word)
        {
            var notNull = Expression.NotEqual(expression, Expression.Constant(null, typeof(string)));
            var toLowerMethod = typeof(string).GetMethod("ToLower", new Type[] { });

            return BuildSearchCondition(expression, notNull, word, toLowerMethod);
        }

        private static Expression BuildSearchCondition(Expression expression, BinaryExpression notNull, string word, MethodInfo method)
        {
            var right = Expression.Constant(word, typeof(string));
            var toStringMethodExpression = Expression.Call(expression, method);
            var contains = CreateContainsMethodExpression(toStringMethodExpression, right);

            return notNull == null ? contains : Expression.AndAlso(notNull, contains);
        }

        private static Expression BuildSearchConditionForEnum(Expression expression, string word, Type propertyType)
        {
            Expression result = null;

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var enumValue in EnumHelper.GetEnumValues(propertyType))
            {
                var description = enumValue.GetDescription();

                if (description.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    result = result == null
                        ? BuildEnumEqualityForValue(expression, propertyType, Convert.ToInt64(enumValue))
                        : Expression.Or(result, BuildEnumEqualityForValue(expression, propertyType, Convert.ToInt64(enumValue)));
                }
            }

            if (result != null)
            {
                return result;
            }

            return Expression.Constant(false);
        }

        private static Expression BuildSearchConditionForNullable(Expression expression, string word)
        {
            var hasValueExpression = Expression.Property(expression, "HasValue");
            var valueExpression = Expression.Property(expression, "Value");
            var nullableValueType = valueExpression.Type;

            return Expression.AndAlso(hasValueExpression, ConstrainExpressionForSearchCondition(word, nullableValueType, valueExpression));
        }

        private static Expression BuildNumericEqualityForValue<TEntity>(Expression expression, Type propertyType, long value)
        {
            Expression result = Expression.Equal(expression, Expression.Constant(value, propertyType));

            return result;
        }

        private static Expression BuildEnumEqualityForValue(Expression expression, Type propertyType, long value)
        {
            var valueAsObject = Enum.ToObject(propertyType, value);
            var valueExpression = Expression.Constant(valueAsObject, propertyType);
            var underlyingType = Enum.GetUnderlyingType(propertyType);

            var expressionNumber = Expression.Convert(expression, underlyingType);
            var valueNumber = Expression.Convert(valueExpression, underlyingType);

            var result = TypeHelper.IsFlaggedEnum(propertyType)
                ? Expression.Equal(Expression.And(expressionNumber, valueNumber), valueNumber)
                : Expression.Equal(expressionNumber, valueNumber);

            return result;
        }

        private static Expression BuildNullableEnumEqualityForValue(Expression expression, Type propertyType, long value)
        {
            var enumType = TypeHelper.ExtractIfNullable(propertyType);

            var valueAsObject = Enum.ToObject(enumType, value);
            Expression result = Expression.Equal(expression, Expression.Constant(valueAsObject, propertyType));

            return result;
        }

        private static MethodInfo GetToStringMethodInfo()
        {
            return typeof(object).GetMethod("ToString", new Type[] { });
        }

        private static Expression CreateContainsMethodExpression(MethodCallExpression methodCallExpression, ConstantExpression right)
        {
            var containsMethodInfo = typeof(string).GetMethod("Contains", new[] { typeof(string) });

            return Expression.Call(methodCallExpression, containsMethodInfo, new[] { right });
        }

        public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            // build parameter map (from parameters of second to parameters of first)
            var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);

            // replace parameters in the second lambda expression with parameters from the first
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);

            // apply composition of lambda expression bodies to parameters from the first expression 
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }

        /// <summary>
        /// Combines two expressions with AND
        /// </summary>
        /// <typeparam name="T">Input parameter for left and right boolean expression</typeparam>
        /// <param name="left">Boolean expression with T as an input</param>
        /// <param name="right">Boolean expression with T as an input</param>
        /// <returns>Expression taking T as input parameter and returing bool which is equivalent of left AND right</returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return left.Compose(right, Expression.AndAlso);
        }

        /// <summary>
        /// Generates a constexp that returns value
        /// </summary>
        /// <param name="inputType">Expression input type</param>
        /// <param name="value">Expression return value</param>
        /// <returns>Expression taking T as input parameter and returing T which is equivalent of value</returns>
        public static LambdaExpression ConstantExpression<T>(Type inputType, T value)
        {
            var lambdaDelegateType = typeof(Func<,>).MakeGenericType(inputType, typeof(T));
            var lambda = Expression.Lambda(lambdaDelegateType, Expression.Constant(value), Expression.Parameter(inputType));

            return lambda;
        }

        /// <summary>
        /// Combines two expressions with AND
        /// </summary>
        /// <param name="inputType">Expression input type</param>
        /// <param name="left">Boolean expression with T as an input</param>
        /// <param name="right">Boolean expression with T as an input</param>
        /// <returns>Expression taking T as input parameter and returing bool which is equivalent of left AND right</returns>
        public static LambdaExpression And(Type inputType, LambdaExpression left, LambdaExpression right)
        {
            return Combine(nameof(And), inputType, left, right);
        }

        /// <summary>
        /// Combines two expressions with OR
        /// </summary>
        /// <param name="inputType">Expression input type</param>
        /// <param name="left">Boolean expression with T as an input</param>
        /// <param name="right">Boolean expression with T as an input</param>
        /// <returns>Expression taking T as input parameter and returing bool which is equivalent of left OR right</returns>
        public static LambdaExpression Or(Type inputType, LambdaExpression left, LambdaExpression right)
        {
            return Combine(nameof(Or), inputType, left, right);
        }

        private static LambdaExpression Combine(string methodName, Type inputType, LambdaExpression left, LambdaExpression right)
        {
            var dynamicQueryHelperAndMethodType =
                typeof(DynamicQueryHelper).GetMethods()
                    .Single(
                        m =>
                        {
                            var parameters = m.GetParameters();

                            return m.Name == methodName
                                   && parameters.Count() == 2
                                   && parameters[0].ParameterType.GetGenericTypeDefinition() == typeof(Expression<>)
                                   && parameters[1].ParameterType.GetGenericTypeDefinition() == typeof(Expression<>);
                        });

            var genericMethod = dynamicQueryHelperAndMethodType.MakeGenericMethod(inputType);

            return (LambdaExpression)genericMethod.Invoke(null, new object[]
            {
                left,
                right
            });
        }

        /// <summary>
        /// Combines two expressions with OR
        /// </summary>
        /// <typeparam name="T">Input parameter for left and right boolean expression</typeparam>
        /// <param name="left">Boolean expression with T as an input</param>
        /// <param name="right">Boolean expression with T as an input</param>
        /// <returns>Expression taking T as input parameter and returing bool which is equivalent of left OR right</returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return left.Compose(right, Expression.OrElse);
        }

        /// <summary>
        /// Combines two labda predicates with or 
        /// </summary>
        public static Func<T, bool> Or<T>(this Func<T, bool> left, Func<T, bool> right)
        {
            return a => left(a) || right(a);
        }

        /// <summary>
        /// Combines two expressions with XOR
        /// </summary>
        /// <typeparam name="T">Input parameter for left and right boolean expression</typeparam>
        /// <param name="left">Boolean expression with T as an input</param>
        /// <param name="right">Boolean expression with T as an input</param>
        /// <returns>Expression taking T as input parameter and returing bool which is equivalent of left XOR right</returns>
        public static Expression<Func<T, bool>> Xor<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return left.Compose(right, Expression.ExclusiveOr);
        }

        /// <summary>
        /// Combines two labda predicates with XOR 
        /// </summary>
        public static Func<T, bool> Xor<T>(this Func<T, bool> left, Func<T, bool> right)
        {
            return a => left(a) ^ right(a);
        }

        /// <summary>
        /// Combines two labda predicates with and 
        /// </summary>
        public static Func<T, bool> And<T>(this Func<T, bool> left, Func<T, bool> right)
        {
            return a => left(a) && right(a);
        }

        /// <summary>
        /// Negate the expression
        /// </summary>
        public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> expression)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.Not(expression.Body), expression.Parameters);
        }

        /// <summary>
        /// Generate expression veryfing if given property is equal a given value for a given TEntity
        /// </summary>
        /// <typeparam name="TEntity">type of the entity</typeparam>
        /// <param name="propertyName">name of the property</param>
        /// <param name="value">value which will be used in equality comparison</param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> GenerateEqualPredicate<TEntity>([NotNull] string propertyName, object value)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            var propertyInfo = typeof(TEntity).GetProperty(propertyName);
            var parameterExpression = Expression.Parameter(typeof(TEntity), "e");
            var left = Expression.Property(parameterExpression, propertyInfo);
            var right = Expression.Constant(value, propertyInfo.PropertyType);
            var equalExpression = Expression.Equal(left, right);

            return Expression.Lambda<Func<TEntity, bool>>(equalExpression, new[] { parameterExpression });
        }

        /// <summary>
        /// Select array if the entity fits the predicate. Return empty array if not.
        /// </summary>
        public static BusinessLogic<TEntity, IQueryable<TOut>> SelectArrayOrEmpty<TEntity, TOut>(
            Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, IQueryable<TOut>>> selector)
        {
            var predicateLogic = new BusinessLogic<TEntity, bool>(predicate);
            var selectorLogic = new BusinessLogic<TEntity, IQueryable<TOut>>(selector);

            return new BusinessLogic<TEntity, IQueryable<TOut>>(
                e => new[] { new TOut[] { } }
                    .Where(i => predicateLogic.Call(e))
                    .Select(i => i.Concat(selectorLogic.Call(e)))
                    .SelectMany(i => i)
                    .AsQueryable());
        }

        /// <summary>
        /// Select array with single element from selector if the entity fits the predicate. Return empty array if not.
        /// </summary>
        public static BusinessLogic<TEntity, IQueryable<TOut>> CreateArrayOrEmpty<TEntity, TOut>(
            Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TOut>> selector)
            where TOut : class
        {
            var selectorLogic = new BusinessLogic<TEntity, TOut>(selector);

            return SelectArrayOrEmpty(predicate, e => new[] { selectorLogic.Call(e) }.AsQueryable());
        }

        /// <summary>
        /// Creates a relevance factor with the predicate and the score
        /// </summary>
        public static BusinessLogic<T, int> CreateRelevanceFactor<T>(
            int score,
            Expression<Func<T, bool>> predicate)
        {
            var businessPredicate = new BusinessLogic<T, bool>(predicate);

            return new BusinessLogic<T, int>(e => businessPredicate.Call(e) ? score : 0);
        }

        /// <summary>
        /// Combines multiple relevance factors and orders by the final score
        /// </summary>
        public static IOrderedQueryable<T> OrderByRelevanceFactors<T>(
            IQueryable<T> query,
            IEnumerable<BusinessLogic<T, int>> relevanceFactors)
        {
            var relevanceScoreSelector = new BusinessLogic<T, int>(e => 0);

            foreach (var factor in relevanceFactors)
            {
                relevanceScoreSelector = new BusinessLogic<T, int>(
                    e => relevanceScoreSelector.Call(e) + factor.Call(e));
            }

            return query.OrderByDescending<T, int>(relevanceScoreSelector);
        }
    }
}