﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    public static class HtmlHelper
    {
        /// <summary>
        /// Reduce length of the given text with html tags to the specified limit and trying
        /// not to cut words and also add '…' at the end without breaking the markup.
        /// </summary>
        /// <param name="htmlText"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string TruncateAtWords([NotNull]string htmlText, int limit)
        {
            if (limit <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit));
            }

            if (htmlText == null)
            {
                return null;
            }

            if (StripHtmlTags(htmlText).Length <= limit)
            {
                return htmlText;
            }

            const char ellipsis = '…';
            const char wordSeparator = ' ';
            const string htmlTagNegativeRegex = "(?![^<]+>)";
            var spacesInText = StringHelper.GetRegexMatches(wordSeparator + htmlTagNegativeRegex, htmlText)
                                .Where(m => m.Index - CalculateHtmlTagsLength(htmlText, m.Index) < limit);
            int lastMatch;

            if (spacesInText.Any())
            {
                lastMatch = spacesInText.Max(m => m.Index);
            }
            else
            {
                var charactersInText = StringHelper.GetRegexMatches("." + htmlTagNegativeRegex, htmlText)
                                    .Where(m => m.Index - CalculateHtmlTagsLength(htmlText, m.Index) < limit);
                lastMatch = charactersInText.Max(m => m.Index);
            }

            htmlText = htmlText.Substring(0, lastMatch) + ellipsis;

            return GetProperlyMarkedHtml(htmlText);
        }

        /// <summary>
        /// Decode html
        /// </summary>
        /// <param name="encodedHtml"></param>
        /// <returns></returns>
        public static string DecodeHtml(string encodedHtml)
        {
            return WebUtility.HtmlDecode(encodedHtml);
        }

        /// <summary>
        /// Remove html tags from string
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripHtmlTags(string htmlString)
        {
            if (htmlString == null)
            {
                return null;
            }

            return HtmlSanitizeHelper.Sanitize(WebUtility.HtmlDecode(htmlString), HtmlSanitizeMethod.PlainText);
        }

        /// <summary>
        /// Decode html and remove html tags from string
        /// </summary>
        /// <param name="encodedHtml"></param>
        /// <returns></returns>
        public static string DecodeAndStripHtmlTags(string encodedHtml)
        {
            return StripHtmlTags(DecodeHtml(encodedHtml));
        }

        private static int CalculateHtmlTagsLength(string text, int index)
        {
            return new Regex("<.*?>").Matches(text.Substring(0, index)).Cast<Match>().Sum(n => n.Length);
        }

        private static string GetProperlyMarkedHtml(string text)
        {
            var document = new HtmlAgilityPack.HtmlDocument
            {
                OptionFixNestedTags = true
            };

            document.LoadHtml(text);

            return document.DocumentNode.OuterHtml;
        }
    }
}
