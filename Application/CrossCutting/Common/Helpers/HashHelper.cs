﻿using System.Security.Cryptography;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.CrossCutting.Common.Helpers
{
    /// <summary>
    /// Hash calculation helpers
    /// </summary>
    public class HashHelper
    {
        /// <summary>
        /// Calculate MD5 hash
        /// </summary>
        /// <param name="content"></param>
        /// <returns>string encoded byte[] result from md5 hash calculation</returns>
        public static string CalculateMd5Hash(string content)
        {
            using (var md5 = MD5.Create())
            {
                var inputBytes = Encoding.ASCII.GetBytes(content);
                var hash = md5.ComputeHash(inputBytes);

                return hash.ToHexString(false);
            }
        }

        /// <summary>
        /// Calculate SHA1 hash
        /// </summary>
        /// <param name="input"></param>
        /// <returns>string encoded byte[] result from sha1 hash calculation</returns>
        public static string CalculateSha1Hash(string input)
        {
            var sha = new SHA1Managed();
            var hash = sha.ComputeHash(Encoding.UTF8.GetBytes(input));

            return hash.ToHexString(false);
        }
    }
}
