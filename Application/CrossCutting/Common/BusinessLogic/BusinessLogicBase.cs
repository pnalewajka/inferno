﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.BusinessLogic
{
    public class BusinessLogicBase
    {
        public LambdaExpression BaseExpression { get; protected set; }

        public BusinessLogicBase(LambdaExpression expression)
        {
            BaseExpression = ReplaceCallWithInline(expression);
        }

        private static LambdaExpression ReplaceCallWithInline(LambdaExpression expression)
        {
            var visitor = new BusinessLogicCallToInlineVisitor();
            var inlinedLambda = visitor.Visit(expression);

            return (LambdaExpression)inlinedLambda;
        }

        protected LambdaExpression GetExpressionWithParameters(LambdaExpression expression, params object[] parameters)
        {
            if (expression.Parameters.Count - 1 != parameters.Count())
            {
                throw new ArgumentException("Incorrect lambda parameter count");
            }

            var replacements = expression.Parameters.Skip(1)
                  .Select(
                      (p, i) =>
                          new
                          {
                              k = p,
                              v = (Expression)Expression.Constant(!p.Type.IsAssignableFrom(parameters[i].GetType())
                                  ? Convert.ChangeType(parameters[i], p.Type)
                                  : parameters[i])
                          })
                  .ToDictionary(i => i.k, i => i.v);

            var newExpression = ExpressionParameterHelper.Replace(expression, replacements);

            return newExpression;
        }

        public class BusinessLogicCallToInlineVisitor : ExpressionVisitor
        {
            protected override Expression VisitNew(NewExpression node)
            {
                if (typeof(BusinessLogicBase).IsAssignableFrom(node.Type))
                {
                    return Expression.Constant(Evaluate(node));
                }

                return base.VisitNew(node);
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                var businessLogic = GetBusinessLogicFromMethodCall(node);

                if (businessLogic is null)
                {
                    return base.VisitMethodCall(node);
                }

                var lambdaToInline = businessLogic.BaseExpression;

                if (lambdaToInline != null)
                {
                    var replacements =
                        lambdaToInline.Parameters.Select((p, i) => new { k = p, v = Visit(node.Arguments[i]) })
                            .ToDictionary(i => i.k, i => i.v);

                    var inlinedLambda = ExpressionParameterHelper.Replace(lambdaToInline, replacements);

                    return inlinedLambda.Body;
                }

                return base.VisitMethodCall(node);
            }

            private static BusinessLogicBase GetBusinessLogicFromMethodCall(MethodCallExpression node)
            {
                if (node.Method.Name == nameof(BusinessLogic<object, object, object>.Parametrize))
                {
                    throw new Exception("Unable to translate .Parametrize() to SQL. Use .Call() instead");
                }

                if (node.Method.Name == nameof(BusinessLogic<object, object, object>.Inline))
                {
                    throw new Exception("Unable to translate .Inline() to SQL. Use .Call() instead");
                }

                if (node.Method.Name != nameof(BusinessLogic<object, object>.Call))
                {
                    return null;
                }

                if (node.Object is MemberExpression member
                    && member.Type.IsSubclassOf(typeof(BusinessLogicBase)))
                {
                    return GetBusinessLogicExpression(member);
                }
                else if (node.Object is NewExpression newExpression
                    && typeof(BusinessLogicBase).IsAssignableFrom(newExpression.Type))
                {
                    return (BusinessLogicBase)Evaluate(newExpression);
                }
                else if (node.Object is ConstantExpression constant
                    && typeof(BusinessLogicBase).IsAssignableFrom(constant.Type))
                {
                    return (BusinessLogicBase)constant.Value;
                }

                return null;
            }

            private static BusinessLogicBase GetBusinessLogicExpression(MemberExpression memberExpression)
            {
                var fieldInfo = memberExpression.Member as FieldInfo;

                if (fieldInfo != null && fieldInfo.IsStatic)
                {
                    var value =
                        fieldInfo.DeclaringType?
                            .GetField(fieldInfo.Name)?
                            .GetValue(null) as BusinessLogicBase;

                    if (value != null)
                    {
                        return value;
                    }
                }

                return (BusinessLogicBase)Evaluate(memberExpression);
            }

            private static object Evaluate(Expression e)
            {
                return Expression.Lambda<Func<object>>(Expression.Convert(e, typeof(object))).Compile()();
            }
        }
    }
}
