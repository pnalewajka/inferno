using System;
using System.Linq.Expressions;

namespace Smt.Atomic.CrossCutting.Common.BusinessLogic
{
    public class BusinessLogic<TEntity, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, TOut>>(
            BusinessLogic<TEntity, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        private readonly Lazy<Func<TEntity, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, TOut>>(BaseExpression.Compile);
        }

        public Expression<Func<TEntity, TOut>> AsExpression() => BaseExpression;

        public Func<TEntity, TOut> GetFunction() => _baseFunc.Value;

        public TOut Call(TEntity e)
        {
            return GetFunction()(e);
        }
    }

    public class BusinessLogic<TEntity, T0, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, TOut>>(
            BusinessLogic<TEntity, T0, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, T0, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        private readonly Lazy<Func<TEntity, T0, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, TOut>>(BaseExpression.Compile);
        }

        public Expression<Func<TEntity, T0, TOut>> AsExpression() => BaseExpression;

        public Func<TEntity, T0, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0)
        {
            return GetFunction()(e, p0);
        }
    }

    public class BusinessLogic<TEntity, T0, T1, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, T1, TOut>>(
            BusinessLogic<TEntity, T0, T1, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, T0, T1, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, T1, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        private readonly Lazy<Func<TEntity, T0, T1, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, T1, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, T1, TOut>>(BaseExpression.Compile);
        }

        public Expression<Func<TEntity, T0, T1, TOut>> AsExpression() => BaseExpression;

        public Func<TEntity, T0, T1, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0, T1 p1)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0, p1));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0, T1 p1)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0, p1);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0, T1 p1)
        {
            return GetFunction()(e, p0, p1);
        }
    }

    public class BusinessLogic<TEntity, T0, T1, T2, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, T1, T2, TOut>>(
            BusinessLogic<TEntity, T0, T1, T2, TOut> logic)
        {
            return logic.BaseExpression;
        }

        public new Expression<Func<TEntity, T0, T1, T2, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, T1, T2, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        private readonly Lazy<Func<TEntity, T0, T1, T2, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, T1, T2, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, T1, T2, TOut>>(BaseExpression.Compile);
        }

        public Func<TEntity, T0, T1, T2, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0, T1 p1, T2 p2)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0, p1, p2));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0, T1 p1, T2 p2)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0, p1, p2);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0, T1 p1, T2 p2)
        {
            return GetFunction()(e, p0, p1, p2);
        }
    }

    public class BusinessLogic<TEntity, T0, T1, T2, T3, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, T1, T2, T3, TOut>>(
            BusinessLogic<TEntity, T0, T1, T2, T3, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, T0, T1, T2, T3, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, T1, T2, T3, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        private readonly Lazy<Func<TEntity, T0, T1, T2, T3, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, T1, T2, T3, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, T1, T2, T3, TOut>>(BaseExpression.Compile);
        }

        public Expression<Func<TEntity, T0, T1, T2, T3, TOut>> AsExpression() => BaseExpression;

        public Func<TEntity, T0, T1, T2, T3, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0, T1 p1, T2 p2, T3 p3)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0, p1, p2, p3));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0, T1 p1, T2 p2, T3 p3)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0, p1, p2, p3);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0, T1 p1, T2 p2, T3 p3)
        {
            return GetFunction()(e, p0, p1, p2, p3);
        }
    }

    public class BusinessLogic<TEntity, T0, T1, T2, T3, T4, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, T1, T2, T3, T4, TOut>>(
            BusinessLogic<TEntity, T0, T1, T2, T3, T4, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, T0, T1, T2, T3, T4, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, T1, T2, T3, T4, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        public Expression<Func<TEntity, T0, T1, T2, T3, T4, TOut>> AsExpression() => BaseExpression;

        private readonly Lazy<Func<TEntity, T0, T1, T2, T3, T4, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, T1, T2, T3, T4, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, T1, T2, T3, T4, TOut>>(BaseExpression.Compile);
        }

        public Func<TEntity, T0, T1, T2, T3, T4, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0, T1 p1, T2 p2, T3 p3, T4 p4)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0, p1, p2, p3, p4));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0, T1 p1, T2 p2, T3 p3, T4 p4)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0, p1, p2, p3, p4);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0, T1 p1, T2 p2, T3 p3, T4 p4)
        {
            return GetFunction()(e, p0, p1, p2, p3, p4);
        }
    }

    public class BusinessLogic<TEntity, T0, T1, T2, T3, T4, T5, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>>(
            BusinessLogic<TEntity, T0, T1, T2, T3, T4, T5, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }

        public Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>> AsExpression() => BaseExpression;

        private readonly Lazy<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, T1, T2, T3, T4, T5, TOut>>(BaseExpression.Compile);
        }

        public Func<TEntity, T0, T1, T2, T3, T4, T5, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0, p1, p2, p3, p4, p5));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0, p1, p2, p3, p4, p5);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
        {
            return GetFunction()(e, p0, p1, p2, p3, p4, p5);
        }
    }

    public class BusinessLogic<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut> : BusinessLogicBase
    {
        public static implicit operator Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>>(
            BusinessLogic<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut> logic)
        {
            return logic.AsExpression();
        }

        public new Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>> BaseExpression
        {
            get { return (Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>>)base.BaseExpression; }
            protected set { base.BaseExpression = value; }
        }


        public Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>> AsExpression() => BaseExpression;


        private readonly Lazy<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>> _baseFunc;

        public BusinessLogic(Expression<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>> expression)
            : base(expression)
        {
            _baseFunc = new Lazy<Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut>>(BaseExpression.Compile);
        }

        public Func<TEntity, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TOut> GetFunction() => _baseFunc.Value;

        public BusinessLogic<TEntity, TOut> Parametrize(T0 p0, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
        {
            return new BusinessLogic<TEntity, TOut>(e => Call(e, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9));
        }

        public Expression<Func<TEntity, TOut>> Inline(T0 p0, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
        {
            var expression = GetExpressionWithParameters(BaseExpression, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);

            return (Expression<Func<TEntity, TOut>>)expression;
        }

        public TOut Call(TEntity e, T0 p0, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
        {
            return GetFunction()(e, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
        }
    }

    // Do you need more? Add them :D
}
