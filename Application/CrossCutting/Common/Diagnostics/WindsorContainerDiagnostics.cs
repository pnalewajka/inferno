﻿using System;
using System.Text;
using Castle.Core.Logging;
using Castle.MicroKernel;
using Castle.Windsor;
using Castle.Windsor.Diagnostics;

namespace Smt.Atomic.CrossCutting.Common.Diagnostics
{
    /// <summary>
    /// Container diagnostics
    /// </summary>
    public static class WindsorContainerDiagnostics
    {
        /// <summary>
        /// Search for lifestyle mismatch
        /// </summary>
        /// <param name="container"></param>
        /// <param name="logger"></param>
        public static void Inspect(IWindsorContainer container, ILogger logger)
        {
            var host = (IDiagnosticsHost)container.Kernel.GetSubSystem(SubSystemConstants.DiagnosticsKey);
            var misconfiguredLifestyleHandlers = host.GetDiagnostic<IPotentialLifestyleMismatchesDiagnostic>().Inspect();

            var inspectionMessage = new StringBuilder();

            foreach (object[] handlers in misconfiguredLifestyleHandlers)
            {
                var errorMessage = $"Invalid lifestyle configuration: {string.Join(", ", handlers)}";
                inspectionMessage.AppendLine(errorMessage);
            }

            var messageString = inspectionMessage.ToString();

            if (!string.IsNullOrWhiteSpace(messageString))
            {
                logger.Fatal(messageString);
                throw new ApplicationException(messageString);
            }
        }
    }
}
