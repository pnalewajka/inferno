﻿using System;
using System.Globalization;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class FormattableExtensions
    {
        public static string ToInvariantString(this IFormattable formattable, string format = null)
        {
            return formattable.ToString(format, CultureInfo.InvariantCulture);
        }
    }
}
