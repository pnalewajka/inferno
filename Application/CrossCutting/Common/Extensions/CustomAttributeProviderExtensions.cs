﻿using System.Reflection;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class CustomAttributeProviderExtensions
    {
        /// <summary>
        /// Gets the attributes. By default does not consider inherited attributes!
        /// </summary>
        /// <param name="member">The member.</param>
        /// <param name="inherit">true if inherited attributes should be considered</param>
        /// <returns>
        /// The member attributes.
        /// </returns>
        public static T[] GetAttributes<T>(this ICustomAttributeProvider member, bool inherit = false) where T : class
        {
            if (typeof (T) != typeof (object))
            {
                return (T[])member.GetCustomAttributes(typeof(T), inherit);
            }

            return (T[])member.GetCustomAttributes(inherit);
        }
    }
}