﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Comparers;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Returns a collection of the original items wrapped along with their index.
        /// </summary>
        /// <typeparam name="T">Type of the enumerated item.</typeparam>
        /// <param name="items">Collection of items</param>
        /// <returns>Collection of wrapped items.</returns>
        public static IEnumerable<EnumeratedItem<T>> WithIndex<T>(this IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            return items.Select((t, i) => new EnumeratedItem<T>(t, i));
        }

        /// <summary>
        /// Returns true, if the collection is null or empty
        /// </summary>
        /// <typeparam name="T">Type of enumerated item</typeparam>
        /// <param name="items">Collection of items</param>
        /// <returns>True if collection is null or empty</returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
        {
            return items == null || !items.Any();
        }

        /// <summary>
        /// Returns HashSet build based on the IEnumerable
        /// </summary>
        /// <typeparam name="T">Type of the enumerated element and the hashset</typeparam>
        /// <param name="items">HashSet of the elemenets</param>
        /// <returns></returns>
        public static HashSet<T> ToHashSet<T>([NotNull] this IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            return new HashSet<T>(items);
        }

        /// <summary>
        /// Adds range of items to an IList collection
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        /// <param name="list">List to be expanded</param>
        /// <param name="items">Items to add</param>
        public static void AddRange<T>([NotNull] this IList<T> list, [NotNull] IEnumerable<T> items)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            foreach (var item in items)
            {
                list.Add(item);
            }
        }

        /// <summary>
        /// Iterates through source watching if cancellation token is not requested
        /// </summary>
        /// <typeparam name="T">Type of the objects we iterate</typeparam>
        /// <param name="source">Enumerable with iterate </param>
        /// <param name="cancellationToken"></param>
        /// <returns>Objects from the source enumeration</returns>
        public static IEnumerable<T> TakeWhileNotCancelled<T>(this IEnumerable<T> source, CancellationToken cancellationToken)
        {
            return source.TakeWhile(item => !cancellationToken.IsCancellationRequested);
        }

        public static IEnumerable<T> TakeIfEnoughTime<T>(this IEnumerable<T> source, TimeSpan remainingTime)
        {
            var safetyFactor = 2;
            var availableTime = remainingTime;

            var startTime = DateTime.Now;
            var processedRecords = 0;
            var averageProcessTime = TimeSpan.Zero;

            foreach (var record in source)
            {
                var expectedProcessingTime = TimeSpan.FromSeconds(averageProcessTime.TotalSeconds * safetyFactor);

                if (availableTime - expectedProcessingTime <= TimeSpan.Zero)
                {
                    yield break;
                }

                yield return record;

                processedRecords++;
                var totalProcessingTime = DateTime.Now - startTime;

                averageProcessTime = TimeSpan.FromSeconds(totalProcessingTime.TotalSeconds / processedRecords);
                availableTime = remainingTime - totalProcessingTime;
            }
        }

        /// <summary>
        /// Returns all contiguous elements from the start of a sequence omitting n elements at the end.
        /// </summary>
        /// <typeparam name="T">Type of the objects we iterate</typeparam>
        /// <param name="source">Enumerable source</param>
        /// <param name="n">Number of elements to omit at the end</param>
        /// <returns>Sequence without n last elements</returns>
        public static IEnumerable<T> TakeAllButLast<T>(this IEnumerable<T> source, int n = 1)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n), "Argument n should be non-negative.");
            }

            var queue = new Queue<T>(n + 1);

            foreach (T element in source)
            {
                queue.Enqueue(element);

                if (queue.Count == n + 1)
                {
                    yield return queue.Dequeue();
                }
            }
        }

        /// <summary>
        /// Filters out any element with duplicate key
        /// </summary>
        /// <typeparam name="T">Element's type</typeparam>
        /// <typeparam name="TKey">Key's type</typeparam>
        /// <param name="source">Source collection</param>
        /// <param name="keySelector">Delegate extracting the key of element</param>
        /// <param name="keyEqualityComparer">Optional comparer for keys</param>
        /// <returns>Resulting collection</returns>
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> source, Func<T, TKey> keySelector, IEqualityComparer<TKey> keyEqualityComparer = null)
        {
            var equalityComparer = new ByKeyEqualityComparer<T, TKey>(keySelector, keyEqualityComparer);

            return source.Distinct(equalityComparer);
        }

        /// <summary>
        /// Creates a collection of type "collectionType" from an IEnumerable
        /// </summary>
        /// <returns>Resulting collection</returns>
        public static object ToCollection(this IEnumerable source, Type collectionType)
        {
            return ToCollection(
                source,
                collectionType,
                TypeHelper.GetEnumerationElementType(collectionType));
        }

        /// <summary>
        /// Creates a collection of type List<elementType> or array of elementType from an IEnumerable
        /// </summary>
        /// <returns>Resulting collection</returns>
        public static object ToCollection(this IEnumerable source, Type collectionType, Type elementType)
        {
            if (!typeof(IEnumerable).IsAssignableFrom(collectionType))
            {
                throw new Exception("Collection type is not assignable to IEnumerable");
            }

            var listType = typeof(List<>).MakeGenericType(elementType);
            var collection = (IList)Activator.CreateInstance(listType);

            foreach (var value in source)
            {
                collection.Add(value);
            }

            if (collectionType.IsArray)
            {
                var toArray = listType.GetMethods()
                    .Where(m => m.Name == nameof(Enumerable.ToArray))
                    .Single();

                return toArray.Invoke(collection, new object[] { });
            }

            return collection;
        }

        public static bool IsSingle<T>(this IEnumerable<T> source)
        {
            return source.Distinct().Take(2).Count() == 1;
        }

        public static bool IsSingleOrEmpty<T>(this IEnumerable<T> source)
        {
            return source.Distinct().Take(2).Count() <= 1;
        }
    }
}