﻿using System;
using System.Reflection;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class MemberInfoExtensions
    {
        public static Type GetUnderlyingType(this MemberInfo memberInfo)
        {
            switch (memberInfo)
            {
                case EventInfo eventInfo:
                    return eventInfo.EventHandlerType;

                case FieldInfo fieldInfo:
                    return fieldInfo.FieldType;

                case MethodInfo methodInfo:
                    return methodInfo.ReturnType;

                case PropertyInfo propertyInfo:
                    return propertyInfo.PropertyType;
            }

            throw new ArgumentException(nameof(memberInfo));
        }
    }
}
