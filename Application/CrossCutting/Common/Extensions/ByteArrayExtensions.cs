﻿using System;
using System.Linq;
using System.Text;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    /// <summary>
    /// Byte array extension methods
    /// </summary>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// Convert byte[] to hex string
        /// </summary>
        /// <param name="array">Array to convert to</param>
        /// <param name="trimZeros">Trim leading zeros, default: true</param>
        /// <returns></returns>
        public static string ToHexString(this byte[] array, bool trimZeros = true)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            var hex = new StringBuilder(array.Length*2);
            var elements = trimZeros ? array.SkipWhile(e => e == 0) : array;

            foreach (var element in elements)
            {
                hex.AppendFormat("{0:x2}", element);
            }

            return hex.ToString();
        }
    }
}
