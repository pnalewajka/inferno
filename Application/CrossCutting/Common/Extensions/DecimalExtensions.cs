﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    /// <summary>
    /// Methods extending decimal class
    /// </summary>
    public static class DecimalExtensions
    {
        /// <summary>
        /// Rounds a decimal value to a specified precision using AwayFromZero midpoint rounding mode
        /// </summary>
        /// <param name="value">A decimal number to round</param>
        /// <param name="decimals">The number of significant decimal places (precision) in the return value.</param>
        /// <returns>The number that is nearest to the <i>value</i> parameter with a precision equal to the <i>decimals</i> parameter</returns>
        public static decimal Round(this decimal value, int decimals = 2)
        {
            return decimal.Round(value, decimals, MidpointRounding.AwayFromZero);
        }
    }
}