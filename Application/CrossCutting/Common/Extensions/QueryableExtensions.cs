﻿using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class QueryableExtensions
    {
        /// <summary>Order by member expression</summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="query">Query to sort</param>
        /// <param name="propertyPath">Property to sort by</param>
        /// <returns>Sorted query</returns>
        public static IOrderedQueryable<T> OrderByProperty<T>(this IQueryable<T> query, string[] propertyPath)
        {
            var orderByProperty = GetOrderedQuery(query, propertyPath, "OrderBy");

            return orderByProperty;
        }

        /// <summary>Order descending by member expression</summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="query">Query to sort</param>
        /// <param name="propertyPath">Property to sort by</param>
        /// <returns>Sorted query</returns>
        public static IOrderedQueryable<T> OrderByPropertyDescending<T>(this IQueryable<T> query, string[] propertyPath)
        {
            var orderByProperty = GetOrderedQuery(query, propertyPath, "OrderByDescending");

            return orderByProperty;
        }

        /// <summary>Follow-up order by member expression</summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="query">Query to sort</param>
        /// <param name="propertyPath">Property to sort by</param>
        /// <returns>Sorted query</returns>
        public static IOrderedQueryable<T> ThenByProperty<T>(this IOrderedQueryable<T> query, string[] propertyPath)
        {
            var orderByProperty = GetOrderedQuery(query, propertyPath, "ThenBy");

            return orderByProperty;
        }

        /// <summary>Follow-up order descending by member expression</summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="query">Query to sort</param>
        /// <param name="propertyPath">Property to sort by</param>
        /// <returns>Sorted query</returns>
        public static IOrderedQueryable<T> ThenByPropertyDescending<T>(this IOrderedQueryable<T> query, string[] propertyPath)
        {
            var orderByProperty = GetOrderedQuery(query, propertyPath, "ThenByDescending");

            return orderByProperty;
        }

        private static IOrderedQueryable<T> GetOrderedQuery<T>(IQueryable<T> query, string[] propertyPath, string methodName)
        {
            var propertyExpression = DynamicQueryHelper.BuildOrderByExpression<T>(propertyPath);
            MemberExpression memberExpression = null;

            if (propertyExpression.Body is MemberExpression)
            {
                memberExpression = propertyExpression.Body as MemberExpression;
            }

            var body = propertyExpression.Body as UnaryExpression;
            if (body != null)
            {
                memberExpression = body.Operand as MemberExpression;
            }

            if (memberExpression == null)
            {
                throw new InvalidExpressionException($"Cannot sort by {string.Join(".", propertyPath)}");
            }

            var sortOrder = Expression.Lambda(memberExpression, propertyExpression.Parameters);

            var callExpression = Expression.Call(typeof (Queryable), methodName,
                                                 new[] {typeof (T), memberExpression.Type},
                                                 query.Expression,
                                                 sortOrder);

            var orderByProperty = (IOrderedQueryable<T>) query.Provider.CreateQuery(callExpression);

            return orderByProperty;
        }
    }
}