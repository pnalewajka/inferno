﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns a value indicating if the specified string matches given regular expression.
        /// </summary>
        /// <param name="s">String to be evaluated</param>
        /// <param name="regex">Regular expression to be matched</param>
        /// <returns>true if there is a match, false otherwise</returns>
        public static bool Matches(this string s, string regex)
        {
            if (s == null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            if (regex == null)
            {
                throw new ArgumentNullException(nameof(regex));
            }

            if (regex == String.Empty)
            {
                return true;
            }

            return Regex.IsMatch(s, regex);
        }

        /// <summary>
        /// Returns regular regex pattern with translated wildcards ? or * to . or .*
        /// </summary>
        /// <param name="input">string with wildcards to be translated</param>
        /// <returns>string with translated wildcards to regular regex syntax</returns>
        public static string WildCardsToRegexSyntax([NotNull] this string input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            return $"^{Regex.Escape(input).Replace("\\?", ".").Replace("\\*", ".*")}$";
        }

        /// <summary>
        /// Trim string at the end removing the sufix if the input starts ends with it
        /// </summary>
        /// <param name="s">string to be trimmed</param>
        /// <param name="sufix">sufix to be removed</param>
        /// <returns></returns>
        public static string TrimEnd(this string s, string sufix)
        {
            if (s == null)
            {
                return null;
            }

            if (!s.EndsWith(sufix))
            {
                return s;
            }

            return s.Substring(0, s.Length - sufix.Length);
        }

        public static string Replace([NotNull] this string s, string oldValue, Func<string> newValue)
        {
            if (s == null)
            {
                throw new ArgumentNullException(nameof(s));
            }

            return s.IndexOf(oldValue, StringComparison.Ordinal) == -1
                       ? s
                       : s.Replace(oldValue, newValue());
        }

        public static string RemoveCharacters(this string input, params char[] characters)
        {
            foreach (var character in characters)
            {
                input = input.Replace(character.ToString(), string.Empty);
            }

            return input;
        }

        /// <summary>
        /// Takes <paramref name="n"/> characters from the beggining of string
        /// </summary>
        /// <param name="input">string to be cut</param>
        /// <param name="n">characters count to be taken from the begining of string</param>
        /// <returns>Cutted string</returns>
        public static string Left([NotNull] this string input, int n)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            if (input.Length < n)
            {
                return input;
            }

            return input.Substring(0, n);
        }

        /// <summary>
        /// Splits comma separated list string
        /// </summary>
        /// <returns>Splited values as T type</returns>
        public static IEnumerable<T> ParseCommaSeparatedList<T>(this string value)
        {
            try
            {
                var valueParts = value.Trim('\"').Split(',').Select(v => v.Trim());

                return valueParts.Where(p => p != String.Empty).Select(p => (T)Convert.ChangeType(p, typeof(T)));
            }
            catch (Exception ex)
            {
                throw new FormatException($"{value} was not in a correct format.", ex);
            }
        }

        /// <summary>
        /// Changes end-lines to match the Unix standard (\n).
        /// </summary>
        /// <param name="s">String to normalize</param>
        /// <returns>String with end-lines normalized to Unix standard (\n)</returns>
        public static string NormalizeEndLinesToUnix(this string s)
        {
            if (!String.IsNullOrEmpty(s))
            {
                return s.Replace("\r\n", "\n").Replace("\r", "\n");
            }

            return s;
        }

        /// <summary>
        /// Parse value to provided type in generic param, or return null (\n).
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <returns>Null or parsed value (\n)</returns>
        public static T? GetValueOrNull<T>(this string valueAsString) where T : struct
        {
            if (string.IsNullOrEmpty(valueAsString))
            {
                return null;
            }

            return (T)Convert.ChangeType(valueAsString, typeof(T));
        }
    }
}
