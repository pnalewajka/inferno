﻿namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    /// <summary>
    /// Wrapper around enumerated item
    /// </summary>
    /// <typeparam name="T">Type of the wrapped item</typeparam>
    public class EnumeratedItem<T>
    {
        /// <summary>
        /// Index in the enumeration
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// Actual enumerated item
        /// </summary>
        public T Item { get; private set; }

        /// <summary>
        /// Is given element first in the enumeration?
        /// </summary>
        public bool IsFirst => Index == 0;

        public EnumeratedItem(T item, int index)
        {
            Index = index;
            Item = item;
        }
    }
}