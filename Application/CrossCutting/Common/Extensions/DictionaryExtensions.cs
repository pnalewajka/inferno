﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class DictionaryExtensions
    {
        public enum DuplicateKeyBehavior
        {
            SkipExisting = 0,
            OverrideValues = 1,
            ThrowException = 2
        }

        public enum MissingKeyBehavior
        {
            SkipMissing = 0,
            ThrowException = 1
        }

        /// <summary>
        /// Adds a range of values to a dictionary from another one. 
        /// Depending on the specified behavior, can skip, override, or throw exception
        /// when working with duplicate keys.
        /// </summary>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
        /// <param name="dictionary">Target dictionary to receive new values</param>
        /// <param name="fromDictionary">Source dictionary with values to add</param>
        /// <param name="behavior">Way to handle duplicate keys</param>
        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
                                                  IDictionary<TKey, TValue> fromDictionary,
            DuplicateKeyBehavior behavior = DuplicateKeyBehavior.OverrideValues)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }

            if (fromDictionary == null)
            {
                throw new ArgumentNullException(nameof(fromDictionary));
            }

            foreach (var kv in fromDictionary)
            {
                var key = kv.Key;

                if (behavior == DuplicateKeyBehavior.ThrowException && dictionary.ContainsKey(key))
                {
                    throw new ArgumentException($"Key {key} already exists in the target dictionary.");
                }

                if (behavior == DuplicateKeyBehavior.OverrideValues || !dictionary.ContainsKey(key))
                {
                    dictionary[key] = kv.Value;
                }
            }
        }

        /// <summary>
        /// Removes elements from dictionary, with keys provided.
        /// Depending on the desired behavior, can skip or throw exception
        /// when working with missing keys.
        /// </summary>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
        /// <param name="dictionary">Target dictionary to remove elements from</param>
        /// <param name="keys">Collection of keys to be removed</param>
        /// <param name="behavior">Way to handle missing keys</param>
        public static void Remove<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
                                                  IEnumerable<TKey> keys,
            MissingKeyBehavior behavior = MissingKeyBehavior.SkipMissing)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }

            if (keys == null)
            {
                throw new ArgumentNullException(nameof(keys));
            }

            foreach (var key in keys)
            {
                if (!dictionary.ContainsKey(key))
                {
                    if (behavior == MissingKeyBehavior.ThrowException)
                    {
                        throw new ArgumentException($"Key {key} does not exists in the target dictionary.");
                    }
                }
                else
                {
                    dictionary.Remove(key);
                }
            }
        }

        /// <summary>
        /// Reverses collection dictionary.
        /// </summary>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
        /// <param name="collectionDictionary">Collection dictionary to reverse</param>
        /// <returns>Reversed collection dictionary</returns>
        public static IDictionary<TValue, List<TKey>> ReverseCollection<TKey, TValue>([NotNull] this IDictionary<TKey, List<TValue>> collectionDictionary)
        {
            if (collectionDictionary == null)
            {
                throw new ArgumentNullException(nameof(collectionDictionary));
            }

            return collectionDictionary
                .SelectMany(
                    m => m.Value.Select(x => new
                        {
                            m.Key,
                            Value = x
                        }))
                .GroupBy(p => p.Value)
                .ToDictionary
                (
                    g => g.Key,
                    g => g.Select(x => x.Key).ToList()
                );
        }

        /// <summary>
        /// Get value from dictionary by key.
        /// If dictionary doesn't contain key return default value.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static TValue GetByKeyOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : default(TValue);
        }

        public static void AddIfNotContainsKey<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
        }
    }
}