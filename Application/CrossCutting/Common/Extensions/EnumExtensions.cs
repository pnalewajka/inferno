﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    /// <summary>
    /// Enum helper methods
    /// </summary>
    public static class EnumExtensions
    {
        private static readonly IDictionary<string, string> GetDescriptionOrValueCache = new ConcurrentDictionary<string, string>();
        private static readonly IDictionary<string, string> GetDescriptionCache = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// Get description from DescriptionAttribute for specific enum value
        /// </summary>
        /// <param name="enumValue">Enum field to be described</param>
        /// <returns>Field's description or empty value</returns>
        public static string GetDescription(this Enum enumValue, CultureInfo culture = null)
        {
            string result;

            var cacheKey = $"{enumValue.GetType().FullName},{culture?.Name ?? System.Threading.Thread.CurrentThread.CurrentUICulture.Name},{enumValue}";

            if (!GetDescriptionCache.TryGetValue(cacheKey, out result))
            {
                if (TypeHelper.IsFlaggedEnum(enumValue.GetType()))
                {
                    return GetFlagDescription(enumValue);
                }
                
                result = AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(enumValue)?.GetDescription(culture)
                    ?? AttributeHelper.GetEnumFieldAttribute<SecurityRoleDescriptionAttribute>(enumValue)?.GetDescription(culture)
                    ?? AttributeHelper.GetEnumFieldAttribute<DescriptionAttribute>(enumValue)?.Description ?? string.Empty;
                
                GetDescriptionCache[cacheKey] = result;
            }

            return result;
        }

        /// <summary>
        /// Get localized description from DescriptionAttribute for specific enum value
        /// </summary>
        /// <param name="enumValue">Enum field to be described</param>
        /// <returns>LocalizedString or null</returns>
        public static LocalizedString GetLocalizedDescription(this Enum enumValue)
        {
            var descriptionLocalizedAttribute = AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(enumValue);

            if (descriptionLocalizedAttribute != null)
            {
                return new LocalizedString(descriptionLocalizedAttribute);
            }

            return null;
        }

        /// <summary>
        /// Get description from DescriptionAttribute for specific enum value
        /// </summary>
        /// <param name="enumValue">Enum field to be described</param>
        /// <returns>Field's description or name when no attribute found</returns>
        public static string GetDescriptionOrValue(this Enum enumValue, CultureInfo culture = null)
        {
            string result;

            var cacheKey =$"{enumValue.GetType().FullName},{culture?.Name ?? System.Threading.Thread.CurrentThread.CurrentUICulture.Name},{enumValue}";

            if (!GetDescriptionOrValueCache.TryGetValue(cacheKey, out result))
            {
                if (TypeHelper.IsFlaggedEnum(enumValue.GetType()))
                {
                    return GetFlagDescription(enumValue, e => e.ToString());
                }

                result = AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(enumValue)?.GetDescription(culture)
                    ?? AttributeHelper.GetEnumFieldAttribute<SecurityRoleDescriptionAttribute>(enumValue)?.GetDescription(culture)
                    ?? AttributeHelper.GetEnumFieldAttribute<DescriptionAttribute>(enumValue)?.Description ?? enumValue.ToString();
                
                GetDescriptionOrValueCache[cacheKey] = result;
            }

            return result;
        }

        private static string GetFlagDescription(Enum enumValue, Func<Enum, string> noDescriptionFallback = null)
        {
            var enumValues = EnumHelper.GetFlags(enumValue);
            var resultParts = new List<string>();

            foreach (Enum value in enumValues)
            {
                var descriptionAttribute = AttributeHelper.GetEnumFieldAttribute<DescriptionAttribute>(value);

                if (descriptionAttribute != null)
                {
                    resultParts.Add(descriptionAttribute.Description);
                }
                else if (noDescriptionFallback != null)
                {
                    resultParts.Add(noDescriptionFallback(value));
                }
            }

            return string.Join(", ", resultParts);
        }
    }
}
