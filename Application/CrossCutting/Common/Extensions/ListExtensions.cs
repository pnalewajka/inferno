﻿using System.Collections;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class ListExtensions
    {
        public static void AddIfNotContains<T>(this IList list, T obj)
        {
            if (!list.Contains(obj))
            {
                list.Add(obj);
            }
        }
    }
}
