﻿using System;
using System.Globalization;
using System.Threading;

namespace Smt.Atomic.CrossCutting.Common.Extensions
{
    public static class DateExtensions
    {
        /// <summary>
        /// Get date of last occurence of specific day of week within month specified as first argument
        /// </summary>
        /// <param name="dateInMonth">Date defining month (day part is irrelevant)</param>
        /// <param name="dayOfWeek">Required day of week</param>
        /// <returns></returns>
        public static DateTime GetLastDayOfWeekInMonth(this DateTime dateInMonth, DayOfWeek dayOfWeek)
        {
            var tmpDate = dateInMonth.AddMonths(1);
            var followingDate = new DateTime(tmpDate.Year, tmpDate.Month, 1).AddDays(-1);

            while (followingDate.DayOfWeek != dayOfWeek)
            {
                followingDate = followingDate.AddDays(-1);
            }

            return followingDate;
        }

        /// <summary>
        /// Get date of first day in month
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetFirstDayInMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Get date of last day in month
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetLastDayInMonth(this DateTime date)
        {
            return GetFirstDayInMonth(date).AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// Get date of first day in year
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetFirstDayOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }

        /// <summary>
        /// Get date of last day in year
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetLastDayOfYear(this DateTime date)
        {
            return GetFirstDayOfYear(date.AddYears(1)).AddDays(-1);
        }

        /// <summary>
        /// Get date for the next day of week
        /// </summary>
        /// <param name="date"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static DateTime GetNextDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
        {
            do
            {
                date = date.AddDays(1);
            }
            while (date.DayOfWeek != dayOfWeek);

            return date;
        }

        /// <summary>
        /// Returns next full hour or the date, should it be a full hour.
        /// Full hour has minutes, seconds and milliseconds equal zero.
        /// </summary>
        /// <param name="date">Reference datetime</param>
        /// <returns>Next full hour</returns>
        public static DateTime NextFullHour(this DateTime date)
        {
            var fullHour = date.Date.AddHours(date.Hour);

            return fullHour == date ? fullHour : fullHour.AddHours(1);
        }

        /// <summary>
        /// Return date time trimmed to full minutes
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime TrimToMinute(this DateTime date)
        {
            return date.Subtract(new TimeSpan(0, 0, 0, date.Second));
        }

        /// <summary>
        /// Return date time just before next minute ( 1 second before)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime JustBeforeNextMinute(this DateTime date)
        {
            const int lastSecondInMinute = 59;

            return date.TrimToMinute().AddSeconds(lastSecondInMinute);
        }

        /// <summary>
        /// Get the previous day if the given day is not the specified day of week, otherwise return given day.
        /// </summary>
        /// <param name="dateInWeek">Date defining week</param>
        /// <param name="dayOfWeek">Required day of week</param>
        /// <returns></returns>
        public static DateTime GetPreviousDayOfWeek(this DateTime dateInWeek, DayOfWeek dayOfWeek)
        {
            while (dateInWeek.DayOfWeek != dayOfWeek)
            {
                dateInWeek = dateInWeek.AddDays(-1);
            }

            return dateInWeek;
        }

        /// <summary>
        /// Get day of the previous week
        /// </summary>
        /// <param name="date"></param>
        /// <param name="dayOfWeek">Required day of week</param>
        /// <returns></returns>
        public static DateTime GetPreviousWeekDay(this DateTime date, DayOfWeek dayOfWeek)
        {
            var currentWeek = date.GetPreviousDayOfWeek(DayOfWeek.Monday);

            return GetPreviousDayOfWeek(currentWeek.AddDays(-1), dayOfWeek);
        }

        /// <summary>
        /// Checking whether the given date is the week last working day (normally it should be Friday)
        /// </summary>
        /// <param name="date">Date to check whether it is the week last working day</param>
        /// <param name="lastWorkingDay">Defined last working day</param>
        /// <returns>Whether the given date is the week last working day</returns>
        public static bool IsLastWorkingDayOfWeek(this DateTime date, DayOfWeek lastWorkingDay = DayOfWeek.Friday)
        {
            return date.DayOfWeek == lastWorkingDay;
        }

        /// <summary>
        /// Checking whether the current date is a weekend (Saturday or Sunday)
        /// </summary>
        /// <param name="date">Date to check whether it is a weekend (Saturday or Sunday)</param>
        /// <returns>Whether the current date is a weekend</returns>
        public static bool IsWeekend(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        /// <summary>
        /// Converts date value to combination of day name and short date string. 
        /// Additionally if culture was specified then the day name will be localized.
        /// </summary>
        /// <param name="date">Date to be converted</param>
        /// <param name="culture">Optional culture info to specify, which culture to use</param>
        /// <returns></returns>
        public static string ToShortDateWithDayNameString(this DateTime date, string culture = null)
        {
            if (string.IsNullOrEmpty(culture))
            {
                return $"{date.DayOfWeek} {date.ToShortDateString()}";
            }

            var cultureInfo = new CultureInfo(culture);
            var dayName = cultureInfo.DateTimeFormat.GetDayName(date.DayOfWeek);

            return $"{dayName} {date.ToShortDateString()}";
        }

        /// <summary>
        /// Get day name for given date
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="cultureInfo">Language culture</param>
        /// <returns></returns>
        public static string GetShortNameOfDay(this DateTime date, CultureInfo cultureInfo = null)
        {
            if (cultureInfo == null)
            {
                cultureInfo = Thread.CurrentThread.CurrentCulture;
            }

            return cultureInfo.DateTimeFormat.AbbreviatedDayNames[(int)date.DayOfWeek];
        }

        /// <summary>
        /// Get date in default format
        /// </summary>
        /// <param name="date"></param>
        /// <returns>String with date in "yyyy-MM-dd" format</returns>
        public static string ToDefaultDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static string ToYearMonthDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM");
        }

        /// <summary>
        /// Get start of day
        /// </summary>
        /// <param name="date"></param>
        /// <returns>Date with zero time</returns>
        public static DateTime StartOfDay(this DateTime date)
        {
            return date.Date;
        }

        /// <summary>
        /// Get datetime before midnight
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateTime one tick before midnight</returns>
        public static DateTime EndOfDay(this DateTime date)
        {
            return date.Date.AddDays(1).AddTicks(-1);
        }

        /// <summary>
        /// Returns Week of the Year
        /// </summary>
        /// <param name="datetime">Date Time</param>
        /// <returns></returns>
        public static int GetWeekOfYear(this DateTime datetime)
        {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(datetime, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
    }
}
