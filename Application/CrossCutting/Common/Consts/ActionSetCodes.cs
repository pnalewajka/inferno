﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public class ActionSetCodes
    {
        [ActionSetDefinition("Actions related to employment termination request.", RequestType.EmploymentTerminationRequest, new[] { RequestStatus.Approved })]
        public const string EmploymentTermination = "EmploymentTermination";

        [ActionSetDefinition("Actions related to employee data change request.", RequestType.EmployeeDataChangeRequest, new[] { RequestStatus.Approved })]
        public const string EmployeeDataChange = "EmployeeDataChange";

        [ActionSetDefinition("Actions related to employment data change request.", RequestType.EmploymentDataChangeRequest, new[] { RequestStatus.Approved })]
        public const string EmploymentDataChange = "EmploymentDataChange";

        [ActionSetDefinition("Actions related to onboarding request.", RequestType.OnboardingRequest, new[] { RequestStatus.Pending, RequestStatus.Approved })]
        public const string Onboarding = "Onboarding";

        [ActionSetDefinition("Actions related to assets request.", RequestType.AssetsRequest, new[] { RequestStatus.Approved })]
        public const string Assets = "Assets";
    }
}
