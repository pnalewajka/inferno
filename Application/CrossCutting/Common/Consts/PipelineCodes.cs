﻿namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public static class PipelineCodes
    {
        public const string Main = "Main";
        public const string Slow = "Slow";
        public const string OncePerYear = "OncePerYear";
        public const string System = "System";
        public const string Notifications = "Notifications";
        public const string EventSource = "EventSource";
        public const string Allocation = "Allocation";
        public const string CustomerPortal = "CustomerPortal";
        public const string EmploymentPeriodRegeneration = "Employment Period Regenerate Job";
        public const string CrmSynchronization = "CRM Synchronization";
        public const string TimeTracking = "Time tracking";
        public const string PeopleManagement = "People management";
        public const string NavisionSynchronization = "Navision synchronization";
        public const string Recruitment = "Recruitment";
    }
}
