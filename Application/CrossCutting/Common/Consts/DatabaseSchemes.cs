﻿namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public static class DatabaseSchemes
    {
        public const string Allocation = "Allocation";
        public const string BusinessTrips = "BusinessTrips";
        public const string Compensation = "Compensation";
        public const string Configuration = "Configuration";
        public const string CustomerPortal = "CustomerPortal";
        public const string MeetMe = "MeetMe";
        public const string Notifications = "Notifications";
        public const string Organization = "Organization";
        public const string PeopleManagement = "PeopleManagement";
        public const string Recruitment = "Recruitment";
        public const string Resumes = "Resumes";
        public const string Scheduling = "Scheduling";
        public const string SkillManagement = "SkillManagement";
        public const string TimeTracking = "TimeTracking";
        public const string Workflows = "Workflows";
        public const string Sales = "Sales";
    }
}
