﻿using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.CrossCutting.Common.Consts
{
    /// <summary>
    /// Storage key values
    /// </summary>
    public static class ParameterKeys
    {
        [SystemParameterDefinition(ValueType = "string", DefaultValue =
           "[] => \r\n" +
           "[machine == \"RDTOOL-ALLO-DEV\"]=>var _paq=_paq||[];_paq.push(['trackPageView']),_paq.push(['enableLinkTracking']),function(){var e='//piwik.intive.com/';_paq.push(['setTrackerUrl',e+'piwik.php']),_paq.push(['setSiteId','3']);_paq.push(['setUserId','$CURRENT-USER-LOGIN$']);var a=document,p=a.createElement('script'),t=a.getElementsByTagName('script')[0];p.type='text/javascript',p.async=!0,p.defer=!0,p.src=e+'piwik.js',t.parentNode.insertBefore(p,t)}();\r\n" +
           "[machine == \"RND-ALLO-APP-1\"]=>var _paq=_paq||[];_paq.push(['trackPageView']),_paq.push(['enableLinkTracking']),function(){var e='//piwik.intive.com/';_paq.push(['setTrackerUrl',e+'piwik.php']),_paq.push(['setSiteId','2']);_paq.push(['setUserId','$CURRENT-USER-LOGIN$']);var a=document,p=a.createElement('script'),t=a.getElementsByTagName('script')[0];p.type='text/javascript',p.async=!0,p.defer=!0,p.src=e+'piwik.js',t.parentNode.insertBefore(p,t)}();")]
        public const string AnalyticsScript = "System.Analytics.Script";

        [SystemParameterDefinition(ValueType = "long", DefaultValue = "0")]
        public const string SystemTimeOffsetInSeconds = "System.Database.TimeOffset";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "30")]
        public const string BusinessEventLeaseTimeInSeconds = "System.EventSourcing.BusinessEventLeaseTimeInSeconds";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "8")]
        public const string AccountsDefaultPasswordMinLength = "Accounts.Users.DefaultPassword.MinLength";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "16")]
        public const string AccountsDefaultPasswordMaxLength = "Accounts.Users.DefaultPassword.MaxLength";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")]
        public const string AccountsDefaultPasswordAllowedCharacters = "Accounts.Users.DefaultPassword.AllowedCharacters";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "48")]
        public const string AccountsLoginTokenMinLength = "Accounts.Users.LoginToken.MinLength";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "64")]
        public const string AccountsLoginTokenMaxLength = "Accounts.Users.LoginToken.MaxLength";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")]
        public const string AccountsLoginTokenAllowedCharacters = "Accounts.Users.LoginToken.AllowedCharacters";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "24")]
        public const string AccountsLoginTokenExpirationInHours = "Accounts.Users.LoginToken.ExpirationInHours";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "false")]
        public const string AccountsNewUserSendPassword = "Accounts.Users.AddUser.SendPassword";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "true")]
        public const string AccountsNewUserSendToken = "Accounts.Users.AddUser.SendToken";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "false")]
        public const string AccountsNewUserStorePasswordInLog = "Accounts.Users.AddUser.StorePasswordInLog";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "devtest@smtsoftware.com")]
        public const string SmtpEmailServiceDefaultFromEmailAddress = "System.Smtp.DefaultFrom.EmailAddress";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "DevTest Account")]
        public const string SmtpEmailServiceDefaultFromFullName = "System.Smtp.DefaultFrom.FullName";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "smtmobile.home.pl")]
        public const string SmtpEmailServiceHostName = "System.Smtp.EmailServer.HostName";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "587")]
        public const string SmtpEmailServicePort = "System.Smtp.EmailServer.Port";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "devtest@smtsoftware.com")]
        public const string SmtpEmailServiceUserName = "System.Smtp.EmailServer.UserName";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "6JPLb1SHF4!^")]
        public const string SmtpEmailServicePassword = "System.Smtp.EmailServer.Password";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "true")]
        public const string SmtpEmailServiceUseSsl = "System.Smtp.EmailServer.UseSsl";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "false")]
        public const string SmtpEmailServiceSetRecipientName = "System.Smtp.EmailServer.SpecifyRecipientName";

        [SystemParameterDefinition(ValueType = "TimeSpan", DefaultValue = "new TimeSpan(1, 0, 0, 0)")]
        public const string TemporaryDocumentTimeToLiveInSeconds = "System.TemporaryDocumentService.TimeToLiveInSeconds";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "204800")]
        public const string TemporaryDocumentMaxUploadChunkSize = "System.DocumentUpload.MaxUploadChunkSize";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "200")]
        public const string UploadPreviewImageMaxDimension = "System.DocumentUpload.MaxPreviewImageDimension";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "400")]
        public const string UploadCropImageMaxDimension = "System.DocumentUpload.MaxCropImageDimension";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "120")]
        public const string SchedulingJobTerminationGracePeriodInSeconds = "System.Scheduling.JobTerminationGracePeriodInSeconds";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "10")]
        public const string SmtpEmailSendingFirstAttemptDelayInSeconds = "Smtp.Email.Sending.FirstAttempt.DelayInSeconds";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "60")]
        public const string SmtpEmailSendingSecondAttemptDelayInSeconds = "Smtp.Email.Sending.SecondAttempt.DelayInSeconds";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "50")]
        public const string SmtpEmailSendingChunkSize = "Smtp.Email.Sending.Chunk.Size";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "800")]
        public const string SmtpEmailSendingChunkDelayInMilliseconds = "Smtp.Email.Sending.Chunk.DelayInMilliseconds";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "7")]
        public const string SmtpEmailDeletionPeriodInDays = "Smtp.Email.Deletion.PeriodInDays";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "0, 0")]
        public const string MapPickerDefaultLocation = "System.MapPicker.DefaultLocation";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "200")]
        public const string MapPickerDefaultEditorHeight = "System.MapPicker.DefaultEditorHeight";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "300")]
        public const string MapPickerDefaultPopupEditorHeight = "System.MapPicker.DefaultPopupEditorHeight";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "100")]
        public const string MapPickerDefaultRadiusInMeters = "System.MapPicker.DefaultRadiusInMeters";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "http://localhost:13060/")]
        public const string ServerAddress = "System.ServerAddress";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}Accounts/Authentication/LogIn?token={1}")]
        public const string TokenLoginUrlFormat = "System.TokenLoginUrlFormat";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "false")]
        public const string SystemSeedRunDataInit = "System.Seed.RunDataInit";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante.alighieri@smtprojects.com")]
        public const string MockEmailAddress = "System.Smtp.Mock.EmailAddress";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "true")]
        public const string SendMockEmailFlag = "System.Smtp.Mock.SendMockEmailFlag";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "UNASSIGNED")]
        public const string OrganizationDefaultOrgUnitCode = "Organization.DefaultOrgUnitCode";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "PL")]
        public const string AllocationDefaultCalendarCode = "Allocation.Calendar.DefaultCode";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"OU=SMT,DC=corp,DC=smtsoftware,DC=com\", \"OU=BLStream,DC=corp,DC=smtsoftware,DC=com\", \"OU=Employees,OU=Users,OU=Company,DC=corp,DC=smtsoftware,DC=com\" }")]
        public const string ActiveDirectoryUserSyncPaths = "Account.User.ActiveDirectory.SyncPaths";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante-employees")]
        public const string ActiveDirectoryEmployeeGroup = "Account.User.ActiveDirectory.EmployeeGroup";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "6DCCEB3B-F13C-404F-A104-A46EA5536DE0,CD34DF85-C09B-4884-9212-8584718562E5,3945C320-3EEB-40F4-98A6-1CA6A2807DDC,C362B4FE-4C62-47D9-8609-577BF4A27265")]
        public const string ActiveDirectoryWritableObjects = "ActiveDirectory.Writer.AllowedWritableObjects";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "80")]
        public const string AllocationLimitHighStatusLimit = "Allocation.Limits.HighStatusLimit";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "60")]
        public const string AllocationLimitLowStatusLimit = "Allocation.Limits.LowStatusLimit";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "6")]
        public const string AllocationEvaluationDefaultEndMonthOffset = "Allocation.Evaluation.DefaultEndMonthOffset";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "1")]
        public const string AllocationEvaluationDefaultRebuildMonthOffset = "Allocation.Evaluation.DefaultRebuildMonthOffset";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = ".3m")]
        public const string StatusReviewReportMinimumAllocationThreshold = "Allocation.Report.StatusReview.MinimunAllocationThreshold";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "8")]
        public const string StatusReviewReportPeriodInWeeks = "Allocation.Report.StatusReview.PeriodInWeeks";

        [SystemParameterDefinition(ValueType = "DateTime", DefaultValue = "new DateTime(2000, 10, 1)")]
        public const string JiraProjectSyncStartDate = "Jira.ProjectSyncStartDate";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante.alighieri")]
        public const string JiraUsername = "Jira.Username";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "DivinaCommedia1321")]
        public const string JiraPassword = "Jira.Password";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "http://localhost:61647/")]
        public const string CustomerPortalServerAddress = "CustomerPortal.ServerAddress";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}#/changeCredentials;token={1}")]
        public const string CustomerPortalTokenLoginUrlFormat = "CustomerPortal.TokenLoginUrlFormat";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}#/candidates;id={1}")]
        public const string CustomerPortalRecommendationUrlFormat = "CustomerPortal.RecommendationUrlFormat";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}/CustomerPortal/ResourceForms/View/{1}")]
        public const string CustomerPortalReviewFormUrlFormat = "CustomerPortal.ReviewFormUrlFormat";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}/candidates/{1}/details/{2}")]
        public const string CustomerPortalCandidateDetailsUrlFormat = "CustomerPortal.CandidateDetailsUrlFormat";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}/CustomerPortal/Challenge/View/{1}?parent-id={2}")]
        public const string CustomerPortalChallengeViewUrlFormat = "CustomerPortal.ChallengeViewUrlFormat";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "false")]
        public const string RunEmployeeAllocationRegenerateJob = "Job.RunEmployeeAllocationRegenerateJob";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "0")]
        public const string StartPointEmployeeAllocationRegenerateJob = "Job.StartPointEmployeeAllocationRegenerateJob";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "800")]
        public const string DelayInMillisecondsEmployeeAllocationRegenerateJob = "Job.DelayInMillisecondsEmployeeAllocationRegenerateJob";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string MaximumAttemptsCountEmployeeAllocationRegenerateJob = "Job.MaximumAttemptsCountEmployeeAllocationRegenerateJob";

        [SystemParameterDefinition(ValueType = "DateTime", DefaultValue = "new DateTime(2016, 1, 1)")]
        public const string AllocationCalculationStartDate = "Allocation.CalculationStartDate";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"atomic@smtsoftware.com\" }")]
        public const string AdminEmailRecipients = "System.Admin.EmailRecipients";

        [SystemParameterDefinition(ValueType = "DateTime", DefaultValue = "new DateTime(2016, 6, 1)")]
        public const string AllocationDataCutOffDate = "Allocation.DataCutOffDate";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string AllocationEmploymentPeriodCanBeClosedInThePastMonthsOnEdit = "Allocation.EmploymentPeriodCanBeClosedInThePastMonthsOnEdit";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "12")]
        public const string AllocationEmploymentPeriodCanBeClosedInThePastMonthsOnAdd = "Allocation.EmploymentPeriodCanBeClosedInThePastMonthsOnAdd";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante.alighieri@intive.com")]
        public const string ExchangeSyncUser = "System.Exchange.SynchronizationUser";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "10")]
        public const string DefaultPageSize = "System.DefaultPageSize";

        [SystemParameterDefinition(ValueType = "string",
            DefaultValue = "[] => \r\n[machine != \"RND-ALLO-APP-1\"] => dante-stage-emails@intive.com\r\n[machine != \"RND-ALLO-APP-1\" && senderName.Contains(\"Customer Portal\")] => ta-customer-portal-list@intive.com",
            ContextType = "EmailMessageParameterContext")]
        public const string MockEmailRecipient = "System.Email.MockEmail";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => It looks like you are having some problems logging in. If you need some support please contact Service Desk (email: support@intive.com or mobile: +48 502 729 800) \r\n [currentCulture == \"pl-PL\"] => Wygląda na to, że masz problemy z zalogowaniem. Jeżeli potrzebujesz pomocy skontaktuj się z zespołem Service Desk (email: support@intive.com or mobile: +48 502 729 800)", ContextType = "CultureDependedParameterContext")]
        public const string LoginAttemptsMessage = "Accounts.Messages.LoginAttempts";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "INTIVE")]
        public const string OrganizationRootUnitCode = "Organization.RootUnitCode";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://outlook.office365.com/owa/?IsDlg=1&path=/options/myaccount/action/photo")]
        public const string EmployeeChangePictureUrl = "Allocation.Employee.ChangePictureUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string NumberOfFutureMonths = "Allocation.Bench.NumberOfFutureMonths";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string AllocationEmploymentPeriodCanBeDeletedInThePastMonths = "Allocation.EmploymentPeriodCanBeDeletedClosedInThePastMonths";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"OU=xDelete\", \"CN=PrivUserGroup\", \"CN=PrivReportingGroup\", \"CN=ReportingGroup\", \"CN=SQLAccessGroup\"}")]
        public const string ActiveDirectoryGroupFilter = "Allocation.ActiveDirectoryGroup.Filter";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://outlook.office365.com/ews/exchange.asmx")]
        public const string ExchangeOutlookUrl = "System.Exchange.Outlook";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "OU=Employees,OU=Users,OU=Company,DC=corp,DC=smtsoftware,DC=com")]
        public const string ActiveDirectoryEmployeePath = "Account.User.ActiveDirectory.EmployeeUsersPath";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "OU=External,OU=Users,OU=Company,DC=corp,DC=smtsoftware,DC=com")]
        public const string ActiveDirectoryExternalPath = "Account.User.ActiveDirectory.ExternalUsersPath";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "intive.com")]
        public const string UserDefaultEmailDomain = "Account.User.DefaultEmailDomain";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "[] => new string[] {\"tatjana.moser\", \"monika.winter\"}\r\n[company == \"SmtSoftware\" || company == \"BlStream\"] => new string[] {\"asy\", \"ewa.furmanek\", \"agnieszka.piatkowska\", \"adw\"}")]
        public const string HRAbsenceAcceptingPersonLogins = "TimeTracking.AbsenceRequest.HRAbsenceAcceptingPersonLogins";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => http://rdtool-allo-dev:9001/DynamicsNAV/OData\r\n[machine == \"RND-ALLO-APP-1\"] => http://fina-navi-app-1.corp.smtsoftware.com:7048/DynamicsNAV/OData")]
        public const string NavisionUrl = "System.NavisionUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "10")]
        public const string BackwardDaysAbsenceLimit = "TimeTracking.AbsenceRequest.BackwardDaysAbsenceLimit";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "false")]
        public const string ShowInactiveUserSyncWarnings = "Account.User.ActiveDirectory.ShowInactiveUserSyncWarnings";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "1")]
        public const string DaysUntilSendingRequestReminders = "Workflows.DaysUntilSendingRequestReminders";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "[] => new string[] { \"lei\", \"rdb\" }\r\n[company == \"SmtSoftware\" || company == \"BlStream\" || company == \"BlStreamInc\" || company == \"SmtSoftwareDcSa\"] => new string[] { \"asy\", \"efu\", \"apt\", \"adw\" }")]
        public const string EmploymentTerminationHRApproverAcronyms = "PeopleManagement.EmploymentTerminationRequest.HRApproverAcronyms";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "{0}CustomerPortal/ScheduleAppointment/List?parent-id={1}")]
        public const string CustomerPortalScheduleAppointmentUrlFormat = "CustomerPortal.ScheduleAppointmentUrlFormat";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "[] => new string[] { }\r\n[acronym == \"lei\" || acronym == \"rdb\"] => new string[] { \"Kupferwerk\" }\r\n[acronym == \"asy\" || acronym == \"efu\" || acronym == \"apt\"] => new string[] { \"BlStream\", \"SmtSoftware\", \"SmtSoftwareUkLtd\", \"BlStreamInc\", \"SmtSoftwareDcSa\", \"MojRachunek\" }")]
        public const string CompaniesImAdministrating = "Organization.CompaniesImAdministating";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "COMMERCIAL")]
        public const string NavisionCodeOfCommercialUtilizationCategory = "UtilizationCategory.NavisionCodeOfCommercialCategory";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://smtstage.crm4.dynamics.com/userdefined/edit.aspx?etc=10045&id=%7b{0}%7d")]
        public const string CrmNeededResourceUrlFormat = "CustomerPortal.CrmNeededResourceUrlFormat";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"6105\", \"6115\", \"6112\", \"6122\"}")]
        public const string ProjectProfitabilityRevenueIncludedAccounts = "Finance.ProjectProfitabilityRevenue.IncludedAccounts";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { }")]
        public const string ProjectProfitabilityRevenueExcludedAccounts = "Finance.ProjectProfitabilityRevenue.ExcludedAccounts";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"7???\", \"TECH020\", \"TECH021\"}")]
        public const string ProjectProfitabilityCostIncludedAccounts = "Finance.ProjectProfitabilityCost.IncludedAccounts";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"7020\", \"79??\"}")]
        public const string ProjectProfitabilityCostExcludedAccounts = "Finance.ProjectProfitabilityCost.ExcludedAccounts";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"TECH025\"}")]
        public const string ProjectProfitabilityServiceCostExcludedAccounts = "Finance.ProjectProfitabilityServiceCost.ExcludedAccounts";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"mit\" }")]
        public const string TimeReportReopenApproverAcronyms = "TimeTracking.TimeReportReopenRequest.ApproverAcronyms";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "http://localhost:8080")]
        public const string CustomerPortalFrontEndUrl = "CustomerPortal.CustomerPortalFrontEndUrl";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => http://sd-navi-app-1.corp.smtsoftware.com:7047/DynamicsNAV/WS/Codeunit/ImportHours\r\n[machine == \"RND-ALLO-APP-1\"] => http://fina-navi-app-1.corp.smtsoftware.com:7047/DynamicsNAV/WS/Codeunit/ImportHours")]
        public const string TimeReportImportHoursUrl = "TimeTracking.ImportHours.Url";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => SD-NAVI-SQL-1\r\n[machine == \"RND-ALLO-APP-1\"] => FINA-NAVI-DB-1")]
        public const string NavisionTransactionsHost = "Navision.Transactions.Host";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => [NAV2016_TEST].[dbo].[Transactions_PL]\r\n[machine == \"RND-ALLO-APP-1\"] => [NAV2016].[dbo].[Transactions_PL]")]
        public const string NavisionTransactionsView = "Navision.Transactions.View";

        [SystemParameterDefinition(ValueType = "bool", DefaultValue = "true")]
        public const string WorkflowsActionSetsEnabled = "Workflows.ActionSets.Enabled";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante-support@intive.com")]
        public const string SystemSupportEmailAddress = "System.Support.EmailAddress";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => kzg\r\n[company == \"SmtSoftware\"] => pah")]
        public const string BusinessTripsFrontDeskAcronym = "BusinessTrips.FrontDeskAcronym";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "CN=corp,CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,DC=corp,DC=smtsoftware,DC=com")]
        public const string ActiveDirectoryDomainInfoPath = "Account.User.ActiveDirectory.DomainInfoPath";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "project = MGMT AND type IN ('Person','Internal Mobility','meet.me') AND resolution = Unresolved AND cf[10611] = '{0}'")]
        public const string JiraLineManagerSyncJobQuery = "Jira.LineManagerSyncJob.Query";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://tracker.intive.com/jira/")]
        public const string JiraReadOnlyUrl = "Jira.ReadOnly.Url";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[] => https://jira-poligon.intive.org/jira/\r\n[machine == \"RND-ALLO-APP-1\"] => https://tracker.intive.com/jira/")]
        public const string JiraReadWriteUrl = "Jira.ReadWrite.Url";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "it@intive.com")]
        public const string ContractTypeNotificationEmail = "ContractType.Notification.EmailAddress";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "-1")]
        public const string DayFromLastInMonthToRemind = "TimeTracking.DayFromLastInMonthToRemind";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string RevokingAbsenceThresholdInDays = "TimeTracking.AbsenceRequest.RevokingAbsenceThresholdInDays";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"agnieszka.swierczek@kbronline.pl\", \"anna.papierz@kbronline.pl\", \"anna.ryta@kbronline.pl\", \"natalia.kmiecicka@kbronline.pl\" }")]
        public const string AbsenceRemovalNotificationList = "Workflows.AbsenceRemovalNotificationList";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "45")]
        public const string BusinessTripsMaximumDaysInPast = "BusinessTrips.MaximumDaysInPast";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string NavisionTransactionMonthsToSyncHourly = "Navision.Transaction.MonthsToSyncHourly";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[]=>I hereby give my consent to have my personal data included in the submitted documents processed by SMT Software Services S.A., BLStream S.A., intive GmbH and by their customers, as deemed necessary for the purposes of the recruitment process, including processing such data in the future for the same purposes. I hereby confirm that my consent is given voluntarily and that I am aware of the rights granted to me under Personal Data Protection Act of 29 August, 1997 (Dz.U. 2016 r. item 922, as amended) \r\n [currentCulture ==  \"pl-PL\"]=>Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w złożonych dokumentach, przez SMT Software Services S.A., BLStream S.A. , intive GmbH, a także przez ich klientów, dla potrzeb niezbędnych do realizacji procesu rekrutacji, w tym także na przetwarzanie ich w przyszłości w tym samym celu. Niniejszą zgodę składam dobrowolnie i oświadczam, że jestem osobą świadomą uprawnień przysługujących mi na podstawie ustawy z dnia 29 sierpnia 1997 r. o Ochronie Danych Osobowych (Dz.U. 2016 poz. 922 ze zm.)", ContextType = "CultureDependedParameterContext")]
        public const string ResumeConsentClauseForDataProcessing = "Resume.ConsentClauseForDataProcessing";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[]=>Intive is: SMT Software Services S.A. + BLStream S.A. + intive GmbH \r\n [currentCulture == \"pl-PL\"]=>Intive to: SMT Software Services S.A. + BLStream S.A. + intive GmbH", ContextType = "CultureDependedParameterContext")]
        public const string ResumeIntiveIs = "Resume.IntiveIs";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string FirstMeetMeWeeksInterval = "MeetMe.FirstMeetMeWeeksInterval";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "12")]
        public const string SecondMeetMeWeeksInterval = "MeetMe.SecondMeetMeWeeksInterval";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "26")]
        public const string NextMeetMeWeeksInterval = "MeetMe.NextMeetMeWeeksInterval";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"@MSTA\", \"SDPL\" }")]
        public const string OrganizationUnitsForSendWelcomeEmail = "Organization.OrganizationUnitsForSendWelcomeEmail";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "14")]
        public const string FollowUpEmailDelayDays = "Allocation.FollowUpEmailDelayDays";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante-recruitment-specialist")]
        public const string RecruitmentSpecialistAdGroup = "Organization.RecruitmentSpecialistAdGroup";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "Special paid leave")]
        public const string SpecialPaidLeaveAbsenceType = "TimeTracking.SpecialPaidLeaveAbsenceType";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "Unpaid leave")]
        public const string UnpaidLeaveAbsenceType = "TimeTracking.UnpaidLeaveAbsenceType";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "Working students - Germany")]
        public const string WorkingStudentsGermanyContractType = "TimeTracking.WorkingStudentsGermanyContractType";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "HRRECR")]
        public const string RecruitmentOrgUnitCode = "Organization.RecruitmentOrgUnitCode";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://tracker.intive.com/jira/projects/PROJECT/issues/{0}")]
        public const string JiraIssueKeyProjectUrlFormat = "Allocation.JiraIssueKeyProjectUrlFormat";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://tracker.intive.com/jira/secure/RapidBoard.jspa?projectKey={0}")]
        public const string JiraKeyProjectKeyUrlFormat = "Allocation.JiraKeyProjectKeyUrlFormat";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "[{\"platform\": \"android\", \"version\": \"0.0.0\", \"download\": \"http://intive.com/\"}, {\"platform\": \"ios\", \"version\": \"0.0.0\", \"download\": \"http://intive.com/\"}]")]
        public const string TimeTrackingMobileVersions = "TimeTracking.MobileVersions";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "30")]
        public const string ResumeDaysToSuggestSkillsFromColleaguesResumees = "Resume.DaysToSuggestSkillsFromColleaguesResumees";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "40")]
        public const string WorkingHoursInWeek = "Allocation.WorkingHoursInWeek";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"Wrocław Kościuszki\", \"Wrocław Prosta\" }")]
        public const string OrganizationCompanyOfficeOnlyLocations = "Organization.Locations.CompanyOfficeOnly";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"Wrocław\" }")]
        public const string OrganizationClientOfficeOnlyLocations = "Organization.Locations.ClientOfficeOnly";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"WorkingStudents\" }")]
        public const string WorkingStudentsContractTypesActiveDirectoryCodes = "TimeTracking.WorkingStudentsContractTypesActiveDirectoryCodes";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "/MeetMe/EmployeeMeeting/List")]
        public const string MeetMeAddFeedbackEmptyContextCancelUrl = "MeetMe.AddFeedbackEmptyContextCancelUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "7")]
        public const string MaxDaysBetweenEmploymentPeriods = "Allocation.MaxDaysBetweenEmploymentPeriods";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"OU=Groups,OU=Company,DC=corp,DC=smtsoftware,DC=com\" }")]
        public const string ActiveDirectoryGroupSyncPaths = "Allocation.ActiveDirectoryGroup.SyncPaths";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "http://sdw-wsrest.ecb.europa.eu/service/data/")]
        public const string EcbApiBaseUrl = "Ecb.Api.BaseUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "5")]
        public const string EcbApiRetryCount = "Ecb.Api.RetryCount";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "EXR/D.{0}.EUR.SP00.A?startPeriod={1}&endPeriod={2}&detail=dataonly")]
        public const string EcbApiCurrencyMeanInDateRangeUrl = "Ecb.Api.CurrencyMeanInDateRangeUrl";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "http://api.nbp.pl/api/exchangerates/")]
        public const string NbpApiBaseUrl = "Nbp.Api.BaseUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "5")]
        public const string NbpApiRetryCount = "Nbp.Api.RetryCount";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "tables/{0}/{1}/{2}/?format=json")]
        public const string NbpApiCurrencyMeanInDateRangeUrl = "Nbp.Api.CurrencyMeanInDateRangeUrl";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "https://openexchangerates.org/api/")]
        public const string OxrApiBaseUrl = "Oxr.Api.BaseUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "5")]
        public const string OxrApiRetryCount = "Oxr.Api.RetryCount";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "historical/{0}.json?app_id={1}&base={2}&symbols={3}")]
        public const string OxrApiCurrencyMeanForDateUrl = "Oxr.Api.CurrencyMeanForDateUrl";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "1")]
        public const string BusinessTripSettlementRequestDaysMargin = "BusinessTrips.SettlementRequest.DaysMargin";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "14")]
        public const string BusinessTripSettlementRequestInvoiceDaysMargin = "BusinessTrips.SettlementRequest.InvoiceDaysMargin";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = "0.3000m")]
        public const string CarGermanyRatePerKilometer = "BusinessTrips.Settlements.CarInGermanyRatePerKilometer";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = "0.5450m")]
        public const string CarUnitedStatesRatePerKilometer = "BusinessTrips.Settlements.CarUnitedStatesRatePerKilometer";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = "0.5214m")]
        public const string CarBelow900ccRatePerKilometer = "BusinessTrips.Settlements.CarBelow900ccRatePerKilometer";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = "0.8358m")]
        public const string CarAbove900ccRatePerKilometer = "BusinessTrips.Settlements.CarAbove900ccRatePerKilometer";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = "0.2302m")]
        public const string MotorcycleRatePerKilometer = "BusinessTrips.Settlements.MotorcycleRatePerKilometer";

        [SystemParameterDefinition(ValueType = "decimal", DefaultValue = "0.1382m")]
        public const string MopedRatePerKilometer = "BusinessTrips.Settlements.MopedRatePerKilometer";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "recruitment-leaders@intive.com")]
        public const string JobOpeningRecruitmentLeadersEmailAddress = "Recruitment.JobOpening.RecruitmentLeadersEmailAddress";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"VacationLeave\", \"TimeOffForOvertime\", \"VacationOnDemand\", \"LeaveOfAbsence\", \"SpecialPaidLeave\", \"ChildCare\" }")]
        public const string PolishCalculatorFullyPaidAbsenceCodes = "Compensation.AbsenceType.FullyPaid.Codes";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"SickLeave\", \"DoctorsChildCare\", \"MaternityLeave\" }")]
        public const string PolishCalculatorPartiallyPaidAbsenceCodes = "Compensation.AbsenceType.PartiallyPaid.Codes";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"ParentalLeave\", \"FreeVacationLeave\" }")]
        public const string PolishCalculatorUnpaidAbsenceCodes = "Compensation.AbsenceType.Unpaid.Codes";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "none")]
        public const string CompensationChangedNotificationReceivers = "Compensation.ChangedNotificationReceivers";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string DaysAfterWhichRemindOfUnsettledBusinessTripsInDays = "BusinessTrips.DaysAfterWhichRemindOfUnsettledBusinessTripsInDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "1")]
        public const string HiringManagerResumeApprovalWorkingDays = "RecruitmentProcessStep.HiringManagerResumeApprovalWorkingDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "5")]
        public const string ClientResumeApprovalWorkingDays = "RecruitmentProcessStep.ClientResumeApprovalWorkingDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string HiringDecisionWorkingDays = "RecruitmentProcessStep.HiringDecisionWorkingDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string TechnicalInterviewResponseDays = "RecruitmentProcessStep.TechnicalInterviewResponseDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string HiringManagerInterviewResponseDays = "RecruitmentProcessStep.HiringManagerInterviewResponseDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string HrInterviewResponseDays = "RecruitmentProcessStep.HrInterviewResponseDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "30")]
        public const string ContractNegotiationCalendarDays = "RecruitmentProcessStep.ContractNegotiationCalendarDays";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "Technical Interviewer")]
        public const string RecruitmentTechnicalInterviewerProfileName = "Recruitment.TechnicalInterviewerProfileName";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "[] => new string[] { \"RecruitmentTestGroup@intive.com\" }\r\n[machine == \"RND-ALLO-APP-1\"] => new string[] { \"RecruitmentJobs@intive.com\" }")]
        public const string InboundEmailGroupNames = "Recruitment.InboundEmailsGroupNames";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "50")]
        public const string InboundEmailPageSize = "Recruitment.InboundEmailPageSize";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "7")]
        public const string DaysRemainingToDataConsentExpiration = "Recruitment.DaysRemainingToDataConsentExpiration";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "10")]
        public const string MaxInboundEmailAttachmentSizeInMB = "Recruitment.MaxInboundEmailAttachmentSizeInMB";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "3")]
        public const string YearsAfterPermanentAnonymizationPossible = "Recruitment.YearsAfterPermanentAnonymizationPossible";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "BlStream")]
        public const string RecruitmentDefaultCompanyActiveDirectoryCode = "Recruitment.DefaultCompany.ActiveDirectoryCode";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "anna.jarco@intive.com")]
        public const string RecruitmentDefaultCoordinatorEmail = "Recruitment.DefaultCoordinator.Email";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "180")]
        public const string OutdatedResumeDaysOffset = "Resumes.OutdatedResumeDaysOffset";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "-1")]
        public const string TimeReportReminderChoreDayFromLastInMonthToRemind = "TimeTracking.ReminderChoreDayFromLastInMonthToRemind";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "-1")]
        public const string ManagerTimeReportReminderChoreDayFromLastInMonthToRemind = "TimeTracking.ManagerReminderChoreDayFromLastInMonthToRemind";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "75")]
        public const string RecruitmentDefaultConsentExpirationDays = "Recruitment.DefaultConsentExpirationDays";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "PL")]
        public const string RecruitmentDefaultCalendarCode = "Recruitment.DefaultCalendarCode";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "[] => new string[0]\r\n[machine == \"RND-ALLO-APP-1\"] => new string[] { \"marcin.switaj@intive.com\" }")]
        public const string CompensationChangedNotificationCcReceivers = "Compensation.ChangedNotificationCcReceivers";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante-recruitment-manager")]
        public const string RecruitmentManagerAdGroup = "Recruitment.RecruitmentManagerAdGroup";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante-line-manager")]
        public const string HiringManagerAdGroup = "Recruitment.HiringManagerAdGroup";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "recruitment-internal@intive.com")]
        public const string JobOpeningRecruitmentInternalEmailAddress = "Recruitment.JobOpening.RecruitmentInternalEmailAddress";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "1")]
        public const string NextMeetingScheduleAfterAbsenceDayDelay = "MeetMe.NextMeetingScheduleAfterAbsenceDayDelay";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "30")]
        public const string EditDoneMeetingDays = "MeetMe.EditDoneMeetingDays";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "-30")]
        public const string AddOrEditNoteForLastOwnMeetingDaysMin = "MeetMe.AddOrEditNoteForLastOwnMeetingDaysMin";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "30")]
        public const string AddOrEditNoteForLastOwnMeetingDaysMax = "MeetMe.AddOrEditNoteForLastOwnMeetingDaysMax";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "00992002")]
        public const string HolidayBonusProjectApn = "Compensation.HolidayBonusProjectApn";

        [SystemParameterDefinition(ValueType = "string[]", ContextType = "ReopenTimeReportNotifyEmailContext", DefaultValue = "[] => new string[] { }\r\n[company == \"Kupferwerk\"] => new string[] { \"jana.kirchner@intive.com\" }")]
        public const string TimeReportReopenedCompanyNotifyPersons = "TimeReport.ReopenedCompanyNotifyPersons";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "Recruitment File Clerk - Research")]
        public const string RecruitmentFileClerkResearchProfileName = "Recruitment.FileClerkResearchProfileName";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "Recruitment File Clerk - Inbound")]
        public const string RecruitmentFileClerkInboundProfileName = "Recruitment.FileClerkInboundProfileName";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "intiveinc@bill.com")]
        public const string CompensationIntiveIncPaymentNotificationEmailAddress = "Compensation.IntiveIncPaymentNotification.EmailAddress";

        [SystemParameterDefinition(ValueType = "string[]", DefaultValue = "new string[] { \"Employment Contract - USA\", \"Employment contract Sales - USA\" }")]
        public const string CompensationIntiveIncPaymentNotificationContractTypes = "Compensation.IntiveIncPaymentNotification.ContractTypes";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "-2")]
        public const string AllocationChartMonthsPastDefault = "Allocation.Chart.Months.Past";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string AllocationChartMonthsFutureDefault = "Allocation.Chart.Months.Future";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "dante.alighieri")]
        public const string TextExtractionUserName = "TextExtraction.UserName";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "DivinaCommedia1321")]
        public const string TextExtractionPassword = "TextExtraction.Password";

        [SystemParameterDefinition(ValueType = "string", DefaultValue = "SMTSOFTWARE")]
        public const string TextExtractionDomain = "TextExtraction.Domain";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "2")]
        public const string BusinessTripSettlementRequestOwnCostDaysMargin = "BusinessTrips.SettlementRequest.OwnCost.DaysMargin";

        [SystemParameterDefinition(ValueType = "int", DefaultValue = "6")]
        public const string WorkflowRequestDefaultDateFilterMonthsOffset = "Workflow.Request.DefaultFilters.MonthsOffset";
    }
}
