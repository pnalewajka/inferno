using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public partial class TemplateCodes
    {
        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when a fixed allocation is added (subject)", "Allocation.Notification.OnFixedAdded.Subject.cshtml")]
        public const string AllocationNotificationOnFixedAddedSubject = "Allocation.Notification.OnFixedAdded.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when a fixed allocation is added", "Allocation.Notification.OnFixedAdded.Body.cshtml")]
        public const string AllocationNotificationOnFixedAddedEmailBody = "Allocation.Notification.OnFixedAdded.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when a planned allocation is added (subject)", "Allocation.Notification.OnPlannedAdded.Subject.cshtml")]
        public const string AllocationNotificationOnPlannedAddedSubject = "Allocation.Notification.OnPlannedAdded.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when a planned allocation is added", "Allocation.Notification.OnPlannedAdded.Body.cshtml")]
        public const string AllocationNotificationOnPlannedAddedEmailBody = "Allocation.Notification.OnPlannedAdded.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when a planned allocation is turned to fixed (subject)", "Allocation.Notification.OnPlannedToFixed.Subject.cshtml")]
        public const string AllocationNotificationOnPlannedToFixedSubject = "Allocation.Notification.OnPlannedToFixed.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when a planned allocation is turned to fixed", "Allocation.Notification.OnPlannedToFixed.Body.cshtml")]
        public const string AllocationNotificationOnPlannedToFixedEmailBody = "Allocation.Notification.OnPlannedToFixed.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when allocation is modified (subject)", "Allocation.Notification.OnChanged.Subject.cshtml")]
        public const string AllocationNotificationOnChangedSubject = "Allocation.Notification.OnChanged.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when allocation is modified", "Allocation.Notification.OnChanged.Body.cshtml")]
        public const string AllocationNotificationOnChangedEmailBody = "Allocation.Notification.OnChanged.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when allocation is deleted (subject)", "Allocation.Notification.OnDeleted.Subject.cshtml")]
        public const string AllocationNotificationOnDeletedSubject = "Allocation.Notification.OnDeleted.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when allocation is deleted", "Allocation.Notification.OnDeleted.Body.cshtml")]
        public const string AllocationNotificationOnDeletedEmailBody = "Allocation.Notification.OnDeleted.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Free or planned employee report for managers (Subject)", "Allocation.Reports.StatusReviewEmail.FreeOrPlanned.Subject.cshtml")]
        public const string AllocationStatusReviewEmailFreeOrPlannedSubject = "Allocation.Reports.StatusReviewEmail.FreeOrPlanned.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Free or planned employee report for managers (Body)", "Allocation.Reports.StatusReviewEmail.FreeOrPlanned.Body.cshtml")]
        public const string AllocationStatusReviewEmailFreeOrPlannedBody = "Allocation.Reports.StatusReviewEmail.FreeOrPlanned.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to customer when resource request was accepted (body)", "CustomerPortal.AppointmentReviewReminder.Body.cshtml")]
        public const string CustomerPortalAppointmentReviewReminderBody = "CustomerPortal.AppointmentReviewReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to customer to remind about appointment review (subject)", "CustomerPortal.AppointmentReviewReminder.Subject.cshtml")]
        public const string CustomerPortalAppointmentReviewReminderSubject = "CustomerPortal.AppointmentReviewReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when SDM sends a recommendation to the client (subject)", "CustomerPortal.Notifications.OnRecommendationSent.Subject.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationSentSubject = "CustomerPortal.Notifications.OnRecommendationSent.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when SDM sends a recommendation to the client (body)", "CustomerPortal.Notifications.OnRecommendationSent.Body.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationSentBody = "CustomerPortal.Notifications.OnRecommendationSent.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Non allocated new hire report for managers (Subject)", "Allocation.Reports.StatusReviewEmail.NonAllocatedNewHires.Subject.cshtml")]
        public const string AllocationStatusReviewEmailNonAllocatedNewHiresSubject = "Allocation.Reports.StatusReviewEmail.NonAllocatedNewHires.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Non allocated new hire report for managers (Body)", "Allocation.Reports.StatusReviewEmail.NonAllocatedNewHires.Body.cshtml")]
        public const string AllocationStatusReviewEmailNonAllocatedNewHiresBody = "Allocation.Reports.StatusReviewEmail.NonAllocatedNewHires.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Under utilized employee report for managers (Subject)", "Allocation.Reports.StatusReviewEmail.UnderUtilized.Subject.cshtml")]
        public const string AllocationStatusReviewEmailUnderUtilizedSubject = "Allocation.Reports.StatusReviewEmail.UnderUtilized.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Under utilized employee report for managers (Body)", "Allocation.Reports.StatusReviewEmail.UnderUtilized.Body.cshtml")]
        public const string AllocationStatusReviewEmailUnderUtilizedBody = "Allocation.Reports.StatusReviewEmail.UnderUtilized.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email with Activation link for the CP client (body)", "CustomerPortal.Notifications.OnCustomerAccountActivationSent.Body.cshtml")]
        public const string CustomerPortalNotificationsOnCustomerAccountActivationSentBody = "CustomerPortal.Notifications.OnCustomerAccountActivationSent.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email with Activation link for the CP client (subject)", "CustomerPortal.Notifications.OnCustomerAccountActivationSent.Subject.cshtml")]
        public const string CustomerPortalNotificationsOnCustomerAccountActivationSentSubject = "CustomerPortal.Notifications.OnCustomerAccountActivationSent.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email with Reactivation link for the CP client (body)", "CustomerPortal.Notifications.OnCustomerAccountActivationResent.Body.cshtml")]
        public const string CustomerPortalNotificationsOnCustomerAccountActivationResentBody = "CustomerPortal.Notifications.OnCustomerAccountActivationResent.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email with Reactivation link for the CP client (subject)", "CustomerPortal.Notifications.OnCustomerAccountActivationResent.Subject.cshtml")]
        public const string CustomerPortalNotificationsOnCustomerAccountActivationResentSubject = "CustomerPortal.Notifications.OnCustomerAccountActivationResent.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when customer accepts recommendation (body)", "CustomerPortal.Notifications.OnRecommendationAccepted.Body.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationAcceptedBody = "CustomerPortal.Notifications.OnRecommendationAccepted.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when customer accepts recommendation (subject)", "CustomerPortal.Notifications.OnRecommendationAccepted.Subject.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationAcceptedSubject = "CustomerPortal.Notifications.OnRecommendationAccepted.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when customer rejects recommendation (body)", "CustomerPortal.Notifications.OnRecommendationRejected.Body.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationRejectedBody = "CustomerPortal.Notifications.OnRecommendationRejected.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when customer rejects recommendation (subject)", "CustomerPortal.Notifications.OnRecommendationRejected.Subject.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationRejectedSubject = "CustomerPortal.Notifications.OnRecommendationRejected.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Template email with question about recommended candidate (body)", "CustomerPortal.Notifications.OnAskAQuestion.Body.cshtml")]
        public const string CustomerPortalNotificationsAskAQuestionBody = "CustomerPortal.Notifications.OnAskAQuestion.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Template email with question about recommended candidate (subject)", "CustomerPortal.Notifications.OnAskAQuestion.Subject.cshtml")]
        public const string CustomerPortalNotificationsAskAQuestionSubject = "CustomerPortal.Notifications.OnAskAQuestion.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when candidate is no longer available (body)", "CustomerPortal.Notifications.OnRecommendationWithdrawn.Body.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationWithdrawnBody = "CustomerPortal.Notifications.OnRecommendationWithdrawn.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when candidate is no longer available (subject)", "CustomerPortal.Notifications.OnRecommendationWithdrawn.Subject.cshtml")]
        public const string CustomerPortalNotificationsOnRecommendationWithdrawnSubject = "CustomerPortal.Notifications.OnRecommendationWithdrawn.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email containts a list of recommendations that are neither accepted nor rejected", "CustomerPortal.Notifications.RecommendationsInPendingStateEmailTemplate.Body.cshtml")]
        public const string CustomerPortalNotificationsRecommendationsInPendingStateEmailTemplateBody = "CustomerPortal.Notifications.RecommendationsInPendingStateEmailTemplate.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email contains all pending recommendations", "CustomerPortal.Notifications.RecommendationsInPendingStateEmailTemplate.Subject.cshtml")]
        public const string CustomerPortalNotificationsRecommendationsInPendingStateEmailTemplateSubject = "CustomerPortal.Notifications.RecommendationsInPendingStateEmailTemplate.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Customer Portal emails footer with legal info", "CustomerPortal.Notifications.Footer.cshtml")]
        public const string CustomerPortalNotificationsFooter = "CustomerPortal.Notifications.Footer";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when customer schedule new interview (body)", "CustomerPortal.InterviewOption.New.Body.cshtml")]
        public const string CustomerPortalNewInterviewOptionsBody = "CustomerPortal.InterviewOption.New.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when customer schedule new interview (subject)", "CustomerPortal.InterviewOption.New.Subject.cshtml")]
        public const string CustomerPortalNewInterviewOptionsSubject = "CustomerPortal.InterviewOption.New.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when customer request new resource (subject)", "CustomerPortal.ResourceRequestSubmitted.Subject.cshtml")]
        public const string CustomerPortalResourceRequestSubmittedSubject = "CustomerPortal.ResourceRequestSubmitted.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when customer request new resource (body)", "CustomerPortal.ResourceRequestSubmitted.Body.cshtml")]
        public const string CustomerPortalResourceRequestSubmittedBody = "CustomerPortal.ResourceRequestSubmitted.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email containts reminder of not finished survey", "Survey.RespondentReminder.Body.cshtml")]
        public const string SurveyRespondentReminderBody = "Survey.RespondentReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email containts reminder of not finished survey", "Survey.RespondentReminder.Subject.cshtml")]
        public const string SurveyRespondentReminderSubject = "Survey.RespondentReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email containts confirmation after submitting survey", "Survey.ConfirmationAfterSubmit.Body.cshtml")]
        public const string SurveyConfirmationAfterSubmitBody = "Survey.ConfirmationAfterSubmit.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email containts confirmation after submitting survey", "Survey.ConfirmationAfterSubmit.Subject.cshtml")]
        public const string SurveyConfirmationAfterSubmitSubject = "Survey.ConfirmationAfterSubmit.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Workflow notify when request is waiting for approval", "Workflows.NotifyApprovers.Subject.cshtml")]
        public const string WorkflowsNotifyApproversMessageSubject = "Workflows.NotifyApprovers.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Workflow notify when request is waiting for approval", "Workflows.NotifyApprovers.Body.cshtml")]
        public const string WorkflowsNotifyApproversMessageBody = "Workflows.NotifyApprovers.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Workflow notify when request was accepted", "Workflows.RequestApproved.Subject.cshtml")]
        public const string WorkflowsRequestApprovedMessageSubject = "Workflows.RequestApproved.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Workflow notify when request was accepted", "Workflows.RequestApproved.Body.cshtml")]
        public const string WorkflowsRequestApprovedMessageBody = "Workflows.RequestApproved.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Workflow notify when request was rejected", "Workflows.RequestRejected.Subject.cshtml")]
        public const string WorkflowsRequestRejectedMessageSubject = "Workflows.RequestRejected.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Workflow notify when request was rejected", "Workflows.RequestRejected.Body.cshtml")]
        public const string WorkflowsRequestRejectedMessageBody = "Workflows.RequestRejected.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Workflow notify when request execution failed", "Workflows.ExecutionException.Subject.cshtml")]
        public const string WorkflowsExecutionExceptionMessageSubject = "Workflows.ExecutionException.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Workflow notify when request execution failed", "Workflows.ExecutionException.Body.cshtml")]
        public const string WorkflowsExecutionExceptionMessageBody = "Workflows.ExecutionException.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Workflow notify when request was executed", "Workflows.RequestExecution.Subject.cshtml")]
        public const string WorkflowsRequestExecutedMessageSubject = "Workflows.RequestExecution.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Workflow notify when request was executed", "Workflows.RequestExecution.Body.cshtml")]
        public const string WorkflowsRequestExecutedMessageBody = "Workflows.RequestExecution.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Reminder of pending requests", "Workflows.PendingRequestsReminder.Subject.cshtml")]
        public const string WorkflowsPendingRequestsReminderMessageSubject = "Workflows.PendingRequestsReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Reminder of pending requests", "Workflows.PendingRequestsReminder.Body.cshtml")]
        public const string WorkflowsPendingRequestsReminderMessageBody = "Workflows.PendingRequestsReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Reminder of not submitted requests", "Workflows.NotSubmittedRequestsReminder.Subject.cshtml")]
        public const string WorkflowsNotSubmittedRequestsReminderMessageSubject = "Workflows.NotSubmittedRequestsReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Reminder of not submitted requests", "Workflows.NotSubmittedRequestsReminder.Body.cshtml")]
        public const string WorkflowsNotSubmittedRequestsReminderMessageBody = "Workflows.NotSubmittedRequestsReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Workflow notify when action is forced", "Workflows.ForcedAction.Subject.cshtml")]
        public const string WorkflowsForcedActionSubject = "Workflows.ForcedAction.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "An action has been forced", "Workflows.ForcedAction.Body.cshtml")]
        public const string WorkflowsForcedActionBody = "Workflows.ForcedAction.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email notification about deleted absence request", "Workflows.RevokedAbsenceRequest.Subject.cshtml")]
        public const string WorkflowsRevokedAbsenceRequestNotificationMessageSubject = "Workflows.RevokedAbsenceRequest.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email notification about deleted absence request", "Workflows.RevokedAbsenceRequest.Body.cshtml")]
        public const string WorkflowsRevokedAbsenceRequestNotificationMessageBody = "Workflows.RevokedAbsenceRequest.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify additional employees about absence", "TimeTracking.AbsenceRequest.Subject.cshtml")]
        public const string TimeTrackingAbsenceRequestNotificationMessageSubject = "TimeTracking.AbsenceRequest.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify additional employees about absence", "TimeTracking.AbsenceRequest.Body.cshtml")]
        public const string TimeTrackingAbsenceRequestNotificationMessageBody = "TimeTracking.AbsenceRequest.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email notification about changes in Contract types", "TimeTracking.ContractType.Subject.cshtml")]
        public const string TimeTrackingContractTypeNotificationMessageSubject = "TimeTracking.ContractType.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email notification about changes in Contract types", "TimeTracking.ContractType.Body.cshtml")]
        public const string TimeTrackingContractTypeNotificationMessageBody = "TimeTracking.ContractType.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email reminder about fill in time report", "TimeTracking.TimeReportReminerJob.Subject.cshtml")]
        public const string TimeTrackingTimeReportReminderJobSubject = "TimeTracking.TimeReportReminerJob.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email reminder about fill in time report", "TimeTracking.TimeReportReminerJob.Body.cshtml")]
        public const string TimeTrackingTimeReportReminderJobBody = "TimeTracking.TimeReportReminerJob.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when operation manager approves interview (subject)", "CustomerPortal.InterviewInvitation.Subject.cshtml")]
        public const string CustomerPortalInterviewInvitationSubject = "CustomerPortal.InterviewInvitation.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when operation manager approves interview (body)", "CustomerPortal.InterviewInvitation.Body.cshtml")]
        public const string CustomerPortalInterviewInvitationBody = "CustomerPortal.InterviewInvitation.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when client puts needed resource on hold (body)", "CustomerPortal.NeededResourcePutOnHold.Body.cshtml")]
        public const string CustomerPortalNeededResourcePutOnHoldBody = "CustomerPortal.NeededResourcePutOnHold.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when client puts needed resource on hold (subject)", "CustomerPortal.NeededResourcePutOnHold.Subject.cshtml")]
        public const string CustomerPortalNeededResourcePutOnHoldSubject = "CustomerPortal.NeededResourcePutOnHold.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when client resumed needed resource (body)", "CustomerPortal.NeededResourceResumed.Body.cshtml")]
        public const string CustomerPortalNeededResourceResumedBody = "CustomerPortal.NeededResourceResumed.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when client resumed needed resource (subject)", "CustomerPortal.NeededResourceResumed.Subject.cshtml")]
        public const string CustomerPortalNeededResourceResumedSubject = "CustomerPortal.NeededResourceResumed.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to customer when resource request was accepted (body)", "CustomerPortal.ResourceRequestAccepted.Body.cshtml")]
        public const string CustomerPortalResourceRequestAcceptedBody = "CustomerPortal.ResourceRequestAccepted.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to customer when resource request was accepted (subject)", "CustomerPortal.ResourceRequestAccepted.Subject.cshtml")]
        public const string CustomerPortalResourceRequestAcceptedSubject = "CustomerPortal.ResourceRequestAccepted.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when ask for resource form details (subject)", "CustomerPortal.AskForResourceFormDetails.Subject.cshtml")]
        public const string CustomerPortalAskForResourceFormDetailsSubject = "CustomerPortal.AskForResourceFormDetails.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when ask for resource form details (body)", "CustomerPortal.AskForResourceFormDetails.Body.cshtml")]
        public const string CustomerPortalAskForResourceFormDetailsBody = "CustomerPortal.AskForResourceFormDetails.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when customer requestes challenge (subject)", "CustomerPortal.ChallengeRequest.Subject.cshtml")]
        public const string CustomerPortalChallengeRequestSubject = "CustomerPortal.ChallengeRequest.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when customer requestes challenge", "CustomerPortal.ChallengeRequest.Body.cshtml")]
        public const string CustomerPortalChallengeRequestBody = "CustomerPortal.ChallengeRequest.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when OM uploads assignment files (subject)", "CustomerPortal.AssignmentFinished.Subject.cshtml")]
        public const string CustomerPortalAssignmentFinishedSubject = "CustomerPortal.AssignmentFinished.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when OM uploads assignment files ", "CustomerPortal.AssignmentFinnished.Body.cshtml")]
        public const string CustomerPortalAssignmentFinishedBody = "CustomerPortal.AssignmentFinnished.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to remind about challenge deadline (subject)", "CustomerPortal.ChallengeDeadline.Subject.cshtml")]
        public const string CustomerPortalChallengeDeadlineSubject = "CustomerPortal.ChallengeDeadline.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to remind about challenge deadline", "CustomerPortal.ChallengeDeadline.Body.cshtml")]
        public const string CustomerPortalChallengeDeadlineBody = "CustomerPortal.ChallengeDeadline.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification about cancellation of employment termination", "PeopleManagement.EmploymentTerminationRequest.Deleted.Body.cshtml")]
        public const string EmploymentTerminationRequestDeletedBody = "PeopleManagement.EmploymentTerminationRequest.Deleted.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification about cancellation of employment termination (subject)", "PeopleManagement.EmploymentTerminationRequest.Deleted.Subject.cshtml")]
        public const string EmploymentTerminationRequestDeletedSubject = "PeopleManagement.EmploymentTerminationRequest.Deleted.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification about cancellation of onboarding (body)", "PeopleManagement.OnboardingRequest.Deleted.Body.cshtml")]
        public const string OnboardingRequestDeletedBody = "PeopleManagement.OnboardingRequest.Deleted.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification about cancellation of onboarding (subject)", "PeopleManagement.OnboardingRequest.Deleted.Subject.cshtml")]
        public const string OnboardingRequestDeletedSubject = "PeopleManagement.OnboardingRequest.Deleted.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification for people responsible for referral program about onboarding", "PeopleManagement.OnboardingRequest.Referral.Body.cshtml")]
        public const string OnboardingRequestReferralSubject = "PeopleManagement.OnboardingRequest.Referral.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification for people responsible for relocation package about onboarding", "PeopleManagement.OnboardingRequest.Relocation.Body.cshtml")]
        public const string OnboardingRequestRelocationBody = "PeopleManagement.OnboardingRequest.Relocation.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification about changes in request which uses SendEmailAction", "Workflows.SendEmailActionUpdate.Body.cshtml")]
        public const string WorkflowsSendEmailActionUpdateBody = "Workflows.SendEmailActionUpdate.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent employee on first day of work.", "Allocation.WelcomeEmailFirstDay.Body.cshtml")]
        public const string WelcomeEmailFirstDayBody = "Allocation.WelcomeEmailFirstDay.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Subject of email sent to employee on first day.", "Allocation.WelcomeEmailFirstDay.Subject.cshtml")]
        public const string WelcomeEmailFirstDaySubject = "Allocation.WelcomeEmailFirstDay.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to employee after two weeks of work.", "Allocation.WelcomeEmailAfterTwoWeeks.Body.cshtml")]
        public const string FollowUpWelcomeEmailBody = "Allocation.WelcomeEmailAfterTwoWeeks.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Subject of email sent to employee after two weeks of work", "Allocation.WelcomeEmailAfterTwoWeeks.Subject.cshtml")]
        public const string FollowUpWelcomeEmailSubject = "Allocation.WelcomeEmailAfterTwoWeeks.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Instructions for employee about employment termination", "PeopleManagement.EmploymentTerminationRequest.Employee.Body.cshtml")]
        public const string PeopleManagementEmploymentTerminationRequestEmployeeBody = "PeopleManagement.EmploymentTerminationRequest.Employee.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Instructions for employee about employment termination (by intive)", "PeopleManagement.EmploymentTerminationRequest.EmployeeByIntive.Body.cshtml")]
        public const string PeopleManagementEmploymentTerminationRequestEmployeeByIntiveBody = "PeopleManagement.EmploymentTerminationRequest.EmployeeByIntive.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification for people responsible for exit interviews about employment termination", "PeopleManagement.EmploymentTerminationRequest.ExitInterview.Body.cshtml")]
        public const string PeopleManagementEmploymentTerminationRequestExitInterviewBody = "PeopleManagement.EmploymentTerminationRequest.ExitInterview.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification for line manager about employment termination", "PeopleManagement.EmploymentTerminationRequest.LineManager.Body.cshtml")]
        public const string PeopleManagementEmploymentTerminationRequestLineManagerBody = "PeopleManagement.EmploymentTerminationRequest.LineManager.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification for other people interested in employment termination", "PeopleManagement.EmploymentTerminationRequest.Other.Body.cshtml")]
        public const string PeopleManagementEmploymentTerminationRequestOtherBody = "PeopleManagement.EmploymentTerminationRequest.Other.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification for people responsible for trainings about employment termination", "PeopleManagement.EmploymentTerminationRequest.Trainings.Body.cshtml")]
        public const string PeopleManagementEmploymentTerminationRequestTrainingsBody = "PeopleManagement.EmploymentTerminationRequest.Trainings.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification for employee when his/her compensation amount changes", "Compensation.CompensationChanged.Notification.Subject.cshtml")]
        public const string CompensationChangedNotificationSubject = "Compensation.CompensationChanged.Notification.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification for employee when his/her compensation amount changes", "Compensation.CompensationChanged.Notification.Body.cshtml")]
        public const string CompensationChangedNotificationBody = "Compensation.CompensationChanged.Notification.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to external company for intive Inc. employees payments (body)", "Compensation.IntiveIncPaymentNotification.Body.cshtml")]
        public const string IntiveIncPaymentNotificationBody = "Compensation.IntiveIncPaymentNotification.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to external company for intive Inc. employees payments (subject)", "Compensation.IntiveIncPaymentNotification.Subject.cshtml")]
        public const string IntiveIncPaymentNotificationSubject = "Compensation.IntiveIncPaymentNotification.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to remind about job opening waiting for approval (body)", "Recruitment.JobOpeningApproval.Body.cshtml")]
        public const string JobOpeningApprovalBody = "Recruitment.JobOpeningApproval.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to remind about job opening waiting for approval (subject)", "Recruitment.JobOpeningApproval.Subject.cshtml")]
        public const string JobOpeningApprovalSubject = "Recruitment.JobOpeningApproval.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about job opening approvement (body)", "Recruitment.JobOpeningApproved.Body.cshtml")]
        public const string JobOpeningApprovedBody = "Recruitment.JobOpeningApproved.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about job opening approvement (subject)", "Recruitment.JobOpeningApproved.Subject.cshtml")]
        public const string JobOpeningApprovedSubject = "Recruitment.JobOpeningApproved.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about job opening rejection (body)", "Recruitment.JobOpeningRejected.Body.cshtml")]
        public const string JobOpeningRejectedBody = "Recruitment.JobOpeningRejected.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about job opening rejection (subject)", "Recruitment.JobOpeningRejected.Subject.cshtml")]
        public const string JobOpeningRejectedSubject = "Recruitment.JobOpeningRejected.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about job opening closing (body)", "Recruitment.JobOpeningClosed.Body.cshtml")]
        public const string JobOpeningClosedBody = "Recruitment.JobOpeningClosed.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about job opening closing (subject)", "Recruitment.JobOpeningClosed.Subject.cshtml")]
        public const string JobOpeningClosedSubject = "Recruitment.JobOpeningClosed.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to an employee assigned to a business trip (body)", "BusinessTrips.BusinessTripAssignment.Body.cshtml")]
        public const string BusinessTripAssignmentChangedBody = "BusinessTrips.BusinessTripAssignment.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to an employee assigned to a business trip (subject)", "BusinessTrips.BusinessTripAssignment.Subject.cshtml")]
        public const string BusinessTripAssignmentChangeSubject = "BusinessTrips.BusinessTripAssignment.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to an employees assigned to a business trip when new communication added (body)", "BusinessTrips.CommunicationMessageAdded.Body.cshtml")]
        public const string BusinessTripCommunicationMessageAddedBody = "BusinessTrips.CommunicationMessageAdded.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to an employees assigned to a business trip when new communication added (subject)", "BusinessTrips.CommunicationMessageAdded.Subject.cshtml")]
        public const string BusinessTripCommunicationMessageAddedSubject = "BusinessTrips.CommunicationMessageAdded.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform that business trip is arranged (body)", "BusinessTrips.BusinessTripArranged.Body.cshtml")]
        public const string BusinessTripsArrangedBody = "BusinessTrips.BusinessTripArranged.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform that business trip is arranged (subject)", "BusinessTrips.BusinessTripArranged.Subject.cshtml")]
        public const string BusinessTripsArrangedSubject = "BusinessTrips.BusinessTripArranged.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to business trip participants reminding to settle business trip (body)", "BusinessTrips.BusinessTripNotSettledReminder.Body.cshtml")]
        public const string BusinessTripNotSettledReminderBody = "BusinessTrips.BusinessTripNotSettledReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to business trip participants reminding to settle business trip (subject)", "BusinessTrips.BusinessTripNotSettledReminder.Subject.cshtml")]
        public const string BusinessTripNotSettledReminderSubject = "BusinessTrips.BusinessTripNotSettledReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about job opening changes from edit (body)", "Recruitment.JobOpeningEditChanges.Body.cshtml")]
        public const string JobOpeningEditChangesBody = "Recruitment.JobOpeningEditChangesBody.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about job opening changes from edit (subject)", "Recruitment.JobOpeningEditChanges.Subject.cshtml")]
        public const string JobOpeningEditChangesSubject = "Recruitment.JobOpeningEditChangesBody.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about incoming candidate data consent expiration date (body)", "GDPR.CandidateDataConsentExpirationReminder.Body.cshtml")]
        public const string CandidateDataConsentExpirationReminderBody = "GDPR.CandidateDataConsentExpirationReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about incoming candidate data consent expiration date (subject)", "GDPR.CandidateDataConsentExpirationReminder.Subject.cshtml")]
        public const string CandidateDataConsentExpirationReminderSubject = "GDPR.CandidateDataConsentExpirationReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about expired candidate data consent (body)", "GDPR.CandidateDataConsentExpiredReminder.Body.cshtml")]
        public const string CandidateDataConsentExpiredReminderBody = "GDPR.CandidateDataConsentExpiredReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about expired candidate data consent (subject)", "GDPR.CandidateDataConsentExpiredReminder.Subject.cshtml")]
        public const string CandidateDataConsentExpiredReminderSubject = "GDPR.CandidateDataConsentExpiredReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about candidate having many data owners (body)", "GDPR.CandidateWithManyDataOwnersReminder.Body.cshtml")]
        public const string CandidateWithManyDataOwnersReminderBody = "GDPR.CandidateWithManyDataOwnersReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about candidate having many data owners (subject)", "GDPR.CandidateWithManyDataOwnersReminder.Subject.cshtml")]
        public const string CandidateWithManyDataOwnersReminderSubject = "GDPR.CandidateWithManyDataOwnersReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about incoming recommendingPerson data consent expiration date (body)", "GDPR.RecommendingPersonDataConsentExpirationReminder.Body.cshtml")]
        public const string RecommendingPersonDataConsentExpirationReminderBody = "GDPR.RecommendingPersonDataConsentExpirationReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about incoming recommendingPerson data consent expiration date (subject)", "GDPR.RecommendingPersonDataConsentExpirationReminder.Subject.cshtml")]
        public const string RecommendingPersonDataConsentExpirationReminderSubject = "GDPR.RecommendingPersonDataConsentExpirationReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about expired recommendingPerson data consent (body)", "GDPR.RecommendingPersonDataConsentExpiredReminder.Body.cshtml")]
        public const string RecommendingPersonDataConsentExpiredReminderBody = "GDPR.RecommendingPersonDataConsentExpiredReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about expired recommendingPerson data consent (subject)", "GDPR.RecommendingPersonDataConsentExpiredReminder.Subject.cshtml")]
        public const string RecommendingPersonDataConsentExpiredReminderSubject = "GDPR.RecommendingPersonDataConsentExpiredReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about recommendingPerson having many data owners (body)", "GDPR.RecommendingPersonWithManyDataOwnersReminder.Body.cshtml")]
        public const string RecommendingPersonWithManyDataOwnersReminderBody = "GDPR.RecommendingPersonWithManyDataOwnersReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about recommendingPerson having many data owners (subject)", "GDPR.RecommendingPersonWithManyDataOwnersReminder.Subject.cshtml")]
        public const string RecommendingPersonWithManyDataOwnersReminderSubject = "GDPR.RecommendingPersonWithManyDataOwnersReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Settlement controlling status changed: Inform participant", "BusinessTrips.SettlementControllingStatusChanged.Subject.cshtml")]
        public const string SettlementControllingStatusChangedSubject = "BusinessTrips.SettlementControllingStatusChanged.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Settlement controlling status changed: Inform participant", "BusinessTrips.SettlementControllingStatusChanged.Body.cshtml")]
        public const string SettlementControllingStatusChangedBody = "BusinessTrips.SettlementControllingStatusChanged.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about candidate changes from edit (body)", "Recruitment.CandidateEditChanges.Body.cshtml")]
        public const string CandidateEditChangesBody = "Recruitment.CandidateEditChanges.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about candidate changes from edit (subject)", "Recruitment.CandidateEditChanges.Subject.cshtml")]
        public const string CandidateEditChangesSubject = "Recruitment.CandidateEditChanges.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about missed candidate next follow-up date (body)", "Recruitment.CandidateNextFollowUpMissedReminder.Body.cshtml")]
        public const string CandidateNextFollowUpMissedReminderBody = "Recruitment.CandidateNextFollowUpMissedReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about missed candidate next follow-up date (subject)", "Recruitment.CandidateNextFollowUpMissedReminder.Subject.cshtml")]
        public const string CandidateNextFollowUpMissedReminderSubject = "Recruitment.CandidateNextFollowUpMissedReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about recruitment process closing (body)", "Recruitment.RecruitmentProcessClosed.Body.cshtml")]
        public const string RecruitmentProcessClosedBody = "Recruitment.RecruitmentProcessClosed.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about recruitment process closing (subject)", "Recruitment.RecruitmentProcessClosed.Subject.cshtml")]
        public const string RecruitmentProcessClosedSubject = "Recruitment.RecruitmentProcessClosed.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about employee Meet.Me date (body)", "MeetMe.OverdueMeetingReminder.Body.cshtml")]
        public const string OverdueMeetingReminderBody = "MeetMe.OverdueMeetingReminder.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about employee Meet.Me date (subject)", "MeetMe.OverdueMeetingReminder.Subject.cshtml")]
        public const string OverdueMeetingReminderSubject = "MeetMe.OverdueMeetingReminder.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about offer made by hiring manager (body)", "Recruitment.NotifyRecruiterAboutOfferMadeByHiringManager.Body.cshtml")]
        public const string NotifyRecruiterAboutOfferMadeByHiringManagerBody = "Recruitment.NotifyRecruiterAboutOfferMadeByHiringManager.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about offer made by hiring manager (subject)", "Recruitment.NotifyRecruiterAboutOfferMadeByHiringManager.Subject.cshtml")]
        public const string NotifyRecruiterAboutOfferMadeByHiringManagerSubject = "Recruitment.NotifyRecruiterAboutOfferMadeByHiringManager.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify decision maker about step update (body)", "Recruitment.NotifyDecisionMakerAboudStepUpdate.Body.cshtml")]
        public const string NotifyDecisionMakerAboutStepUpdateBody = "Recruitment.NotifyDecisionMakerAboudStepUpdate.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify decision maker about step update (subject)", "Recruitment.NotifyDecisionMakerAboudStepUpdate.Subject.cshtml")]
        public const string NotifyDecisionMakerAboutStepUpdateSubject = "Recruitment.NotifyDecisionMakerAboudStepUpdate.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify decision maker about step cancelled (body)", "Recruitment.NotifyDecisionMakerAboutStepCancelled.Body.cshtml")]
        public const string NotifyDecisionMakerAboutStepCancelledBody = "Recruitment.NotifyDecisionMakerAboutStepCancelled.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify decision maker about step cancelled (subject)", "Recruitment.NotifyDecisionMakerAboutStepCancelled.Subject.cshtml")]
        public const string NotifyDecisionMakerAboutStepCancelledSubject = "Recruitment.NotifyDecisionMakerAboutStepCancelled.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify hiring manager about planned interview (body)", "Recruitment.NotifyHiringManagerAboutPlannedInterview.Body.cshtml")]
        public const string NotifyHiringManagerAboutPlannedInterviewBody = "Recruitment.NotifyHiringManagerAboutPlannedInterview.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify hiring manager about planned interview (subject)", "Recruitment.NotifyHiringManagerAboutPlannedInterview.Subject.cshtml")]
        public const string NotifyHiringManagerAboutPlannedInterviewSubject = "Recruitment.NotifyHiringManagerAboutPlannedInterview.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify hiring manager about resume sent for approval (body)", "Recruitment.NotifyHiringManagerAboutResumeSentForApproval.Body.cshtml")]
        public const string NotifyHiringManagerAboutResumeSentForApprovalBody = "Recruitment.NotifyHiringManagerAboutResumeSentForApproval.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify hiring manager about resume sent for approval (subject)", "Recruitment.NotifyHiringManagerAboutResumeSentForApproval.Subject.cshtml")]
        public const string NotifyHiringManagerAboutResumeSentForApprovalSubject = "Recruitment.NotifyHiringManagerAboutResumeSentForApproval.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about client approval (body)", "Recruitment.NotifyRecruiterAboutClientApproval.Body.cshtml")]
        public const string NotifyRecruiterAboutClientApprovalBody = "Recruitment.NotifyRecruiterAboutClientApproval.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about client approval (subject)", "Recruitment.NotifyRecruiterAboutClientApproval.Subject.cshtml")]
        public const string NotifyRecruiterAboutClientApprovalSubject = "Recruitment.NotifyRecruiterAboutClientApproval.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about client rejection (body)", "Recruitment.NotifyRecruiterAboutClientRejection.Body.cshtml")]
        public const string NotifyRecruiterAboutClientRejectionBody = "Recruitment.NotifyRecruiterAboutClientRejection.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about client rejection (subject)", "Recruitment.NotifyRecruiterAboutClientRejection.Subject.cshtml")]
        public const string NotifyRecruiterAboutClientRejectionSubject = "Recruitment.NotifyRecruiterAboutClientRejection.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about hiring manager approval (body)", "Recruitment.NotifyRecruiterAboutHiringManagerApproval.Body.cshtml")]
        public const string NotifyRecruiterAboutHiringManagerApprovalBody = "Recruitment.NotifyRecruiterAboutHiringManagerApproval.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about hiring manager approval (subject)", "Recruitment.NotifyRecruiterAboutHiringManagerApproval.Subject.cshtml")]
        public const string NotifyRecruiterAboutHiringManagerApprovalSubject = "Recruitment.NotifyRecruiterAboutHiringManagerApproval.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about hiring manager rejection (body)", "Recruitment.NotifyRecruiterAboutHiringManagerRejection.Body.cshtml")]
        public const string NotifyRecruiterAboutHiringManagerRejectionBody = "Recruitment.NotifyRecruiterAboutHiringManagerRejection.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about hiring manager rejection (subject)", "Recruitment.NotifyRecruiterAboutHiringManagerRejection.Subject.cshtml")]
        public const string NotifyRecruiterAboutHiringManagerRejectionSubject = "Recruitment.NotifyRecruiterAboutHiringManagerRejection.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about hiring manager sending resume to client for approval (body)", "Recruitment.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppoval.Body.cshtml")]
        public const string NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppovalBody = "Recruitment.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppoval.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about hiring manager sending resume to client for approval (subject)", "Recruitment.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppoval.Subject.cshtml")]
        public const string NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppovalSubject = "Recruitment.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppoval.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about process put on hold (body)", "Recruitment.NotifyAboutProcessPutOnHold.Body.cshtml")]
        public const string NotifyAboutProcessPutOnHoldBody = "Recruitment.NotifyAboutProcessPutOnHold.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about process put on hold (subject)", "Recruitment.NotifyAboutProcessPutOnHold.Subject.cshtml")]
        public const string NotifyAboutProcessPutOnHoldSubject = "Recruitment.NotifyAboutProcessPutOnHold.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about process step put on hold (body)", "Recruitment.NotifyAboutProcessStepPutOnHold.Body.cshtml")]
        public const string NotifyAboutProcessStepPutOnHoldBody = "Recruitment.NotifyAboutProcessStepPutOnHold.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about process step put on hold (subject)", "Recruitment.NotifyAboutProcessStepPutOnHold.Subject.cshtml")]
        public const string NotifyAboutProcessStepPutOnHoldSubject = "Recruitment.NotifyAboutProcessStepPutOnHold.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about technical reviewer approval (body)", "Recruitment.NotifyRecruiterAboutTechnicalReviewerApproval.Body.cshtml")]
        public const string NotifyRecruiterAboutTechnicalReviewerApprovalBody = "Recruitment.NotifyRecruiterAboutTechnicalReviewerApproval.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about technical reviewer approval (subject)", "Recruitment.NotifyRecruiterAboutTechnicalReviewerApproval.Subject.cshtml")]
        public const string NotifyRecruiterAboutTechnicalReviewerApprovalSubject = "Recruitment.NotifyRecruiterAboutTechnicalReviewerApproval.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiter about technical reviewer rejection (body)", "Recruitment.NotifyRecruiterAboutTechnicalReviewerRejection.Body.cshtml")]
        public const string NotifyRecruiterAboutTechnicalReviewerRejectionBody = "Recruitment.NotifyRecruiterAboutTechnicalReviewerRejection.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiter about technical reviewer rejection (subject)", "Recruitment.NotifyRecruiterAboutTechnicalReviewerRejection.Subject.cshtml")]
        public const string NotifyRecruiterAboutTechnicalReviewerRejectionSubject = "Recruitment.NotifyRecruiterAboutTechnicalReviewerRejection.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify technical reviewer about planned interview (body)", "Recruitment.NotifyTechnicalReviewerAboutPlannedInterview.Body.cshtml")]
        public const string NotifyTechnicalReviewerAboutPlannedInterviewBody = "Recruitment.NotifyTechnicalReviewerAboutPlannedInterview.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify technical reviewer about planned interview (subject)", "Recruitment.NotifyTechnicalReviewerAboutPlannedInterview.Subject.cshtml")]
        public const string NotifyTechnicalReviewerAboutPlannedInterviewSubject = "Recruitment.NotifyTechnicalReviewerAboutPlannedInterview.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent to inform about recruitment process changes from edit (body)", "Recruitment.RecruitmentProcessEditChanges.Body.cshtml")]
        public const string RecruitmentProcessEditChangesBody = "Recruitment.RecruitmentProcessEditChangesBody.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent to inform about recruitment process changes from edit (subject)", "Recruitment.RecruitmentProcessEditChanges.Subject.cshtml")]
        public const string RecruitmentProcessEditChangesSubject = "Recruitment.RecruitmentProcessEditChangesBody.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about note added in recruitment process (body)", "Recruitment.NotifyAboutAddedNoteInRecruitmentProcessNotes.Body.cshtml")]
        public const string NotifyAboutAddedNoteInRecruitmentProcessNotesBody = "Recruitment.NotifyAboutAddedNoteInRecruitmentProcessNotes.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about note added in recruitment process (subject)", "Recruitment.NotifyAboutAddedNoteInRecruitmentProcessNotes.Subject.cshtml")]
        public const string NotifyAboutAddedNoteInRecruitmentProcessNotesSubject = "Recruitment.NotifyAboutAddedNoteInRecruitmentProcessNotes.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about note Edited in recruitment process (body)", "Recruitment.NotifyAboutEditedNoteInRecruitmentProcessNotes.Body.cshtml")]
        public const string NotifyAboutEditedNoteInRecruitmentProcessNotesBody = "Recruitment.NotifyAboutEditedNoteInRecruitmentProcessNotes.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about note Edited in recruitment process (subject)", "Recruitment.NotifyAboutEditedNoteInRecruitmentProcessNotes.Subject.cshtml")]
        public const string NotifyAboutEditedNoteInRecruitmentProcessNotesSubject = "Recruitment.NotifyAboutEditedNoteInRecruitmentProcessNotes.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Settlement notify when request was rejected", "Workflows.SettlementRequestRejected.Body.cshtml")]
        public const string SettlementRequestRejectedMessageBody = "Workflows.SettlementRequestRejected.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about recruitment process taken off hold (body)", "Recruitment.NotifyAboutProcessTakenOffHold.Body.cshtml")]
        public const string NotifyAboutProcessTakenOffHoldBody = "Recruitment.NotifyAboutProcessTakenOffHold.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about recruitment process taken off hold (subject)", "Recruitment.NotifyAboutProcessTakenOffHold.Subject.cshtml")]
        public const string NotifyAboutProcessTakenOffHoldSubject = "Recruitment.NotifyAboutProcessTakenOffHold.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about note added in candidate (subject)", "Recruitment.NotifyAboutAddedNoteInCandidate.Subject.cshtml")]
        public const string NotifyAboutAddedNoteInCandidateSubject = "Recruitment.NotifyAboutAddedNoteInCandidate.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about note added in candidate (body)", "Recruitment.NotifyAboutAddedNoteInCandidate.Body.cshtml")]
        public const string NotifyAboutAddedNoteInCandidateBody = "Recruitment.NotifyAboutAddedNoteInCandidate.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about edited note in candidate (subject)", "Recruitment.NotifyAboutEditedNoteInCandidate.Subject.cshtml")]
        public const string NotifyAboutEditedNoteInCandidateSubject = "Recruitment.NotifyAboutEditedNoteInCandidate.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about edited note in candidate (body)", "Recruitment.NotifyAboutEditedNoteInCandidate.Body.cshtml")]
        public const string NotifyAboutEditedNoteInCandidateBody = "Recruitment.NotifyAboutEditedNoteInCandidate.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify hiring manager about hiring decision needed (subject)", "Recruitment.NotifyHiringManagerAboutHiringDecisionNeeded.Subject.cshtml")]
        public const string NotifyHiringManagerAboutHiringDecisionNeededSubject = "Recruitment.NotifyHiringManagerAboutHiringDecisionNeeded.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify hiring manager about hiring decision needed (body)", "Recruitment.NotifyHiringManagerAboutHiringDecisionNeeded.Body.cshtml")]
        public const string NotifyHiringManagerAboutHiringDecisionNeededBody = "Recruitment.NotifyHiringManagerAboutHiringDecisionNeeded.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiters about put on hold job opening (subject)", "Recruitment.JobOpeningPutOnHold.Subject.cshtml")]
        public const string NotifyJobOpeningPutOnHoldSubject = "Recruitment.JobOpeningPutOnHold.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiters about put on hold job opening (body)", "Recruitment.JobOpeningPutOnHold.Body.cshtml")]
        public const string NotifyJobOpeningPutOnHoldBody = "Recruitment.JobOpeningPutOnHold.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiters about take off hold job opening (subject)", "Recruitment.JobOpeningTakeOffHold.Subject.cshtml")]
        public const string NotifyJobOpeningTakeOffHoldSubject = "Recruitment.JobOpeningTakeOffHold.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiters about take off hold job opening (body)", "Recruitment.JobOpeningTakeOffHold.Body.cshtml")]
        public const string NotifyJobOpeningTakeOffHoldBody = "Recruitment.JobOpeningTakeOffHold.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify recruiters about reopen job opening (subject)", "Recruitment.JobOpeningReopen.Subject.cshtml")]
        public const string NotifyJobOpeningReopenSubject = "Recruitment.JobOpeningReopen.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify recruiters about reopen job opening (body)", "Recruitment.JobOpeningReopen.Body.cshtml")]
        public const string NotifyJobOpeningReopenBody = "Recruitment.JobOpeningReopen.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about assignment inbound email (subject)", "Recruitment.InboundEmailAssignment.Subject.cshtml")]
        public const string NotifyAboutInboundEmailAssignmentSubject = "Recruitment.InboundEmailAssignment.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about assignment inbound email (body)", "Recruitment.InboundEmailAssignment.Body.cshtml")]
        public const string NotifyAboutInboundEmailAssignmentBody = "Recruitment.InboundEmailAssignment.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about unassignment inbound email (subject)", "Recruitment.InboundEmailUnassignment.Subject.cshtml")]
        public const string NotifyAboutInboundEmailUnassignmentSubject = "Recruitment.InboundEmailUnassignment.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about unassignment inbound email (body)", "Recruitment.InboundEmailUnassignment.Body.cshtml")]
        public const string NotifyAboutInboundEmailUnassignmentBody = "Recruitment.InboundEmailUnassignment.Body";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about added note in job opening  (body)", "Recruitment.NotifyAboutAddedNoteInJobOpening.Body.cshtml")]
        public const string NotifyAboutAddedNoteInJobOpeningBody = "Recruitment.NotifyAboutAddedNoteInJobOpening.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about added note in job opening (subject)", "Recruitment.NotifyAboutAddedNoteInJobOpening.Subject.cshtml")]
        public const string NotifyAboutAddedNoteInJobOpeningSubject = "Recruitment.NotifyAboutAddedNoteInJobOpening.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about edited note in job opening (body)", "Recruitment.NotifyAboutEditedNoteInJobOpening.Body.cshtml")]
        public const string NotifyAboutEditedNoteInJobOpeningBody = "Recruitment.NotifyAboutEditedNoteInJobOpening.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about edited note in job opening (subject)", "Recruitment.NotifyAboutEditedNoteInJobOpening.Subject.cshtml")]
        public const string NotifyAboutEditedNoteInJobOpeningSubject = "Recruitment.NotifyAboutEditedNoteInJobOpening.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about feedback request in meet me (subject)", "MeetMe.FeedbackRequestNotificationMessage.Body.cshtml")]
        public const string FeedbackRequestNotificationMessageBody = "MeetMe.FeedbackRequestNotificationMessage.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about feedback request in meet me (body)", "MeetMe.FeedbackRequestNotificationMessage.Subject.cshtml")]
        public const string FeedbackRequestNotificationMessageSubject = "MeetMe.FeedbackRequestNotificationMessage.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about added note in job opening (body)", "Recruitment.NotifyAboutAssignedInJobOpening.Body.cshtml")]
        public const string JobOpeningAssignedBody = "Recruitment.NotifyAboutAssignedInJobOpening.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about added note in job opening (subject)", "Recruitment.NotifyAboutAssignedInJobOpening.Subject.cshtml")]
        public const string JobOpeningAssignedSubject = "Recruitment.NotifyAboutAssignedInJobOpening.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about edited note in job opening (body)", "Recruitment.NotifyAboutUnassignedInJobOpening.Body.cshtml")]
        public const string JobOpeningUnassignedBody = "Recruitment.NotifyAboutUnassignedInJobOpening.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about edited note in job opening (subject)", "Recruitment.NotifyAboutUnassignedInJobOpening.Subject.cshtml")]
        public const string JobOpeningUnassignedSubject = "Recruitment.NotifyAboutUnassignedInJobOpening.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about reopening of time report (body)", "TimeTracking.ReopenTimeReport.Body.cshtml")]
        public const string TimeTrackingReopenTimeReportBody = "TimeTracking.ReopenTimeReport.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about reopening of time report (subject)", "TimeTracking.ReopenTimeReport.Subject.cshtml")]
        public const string TimeTrackingReopenTimeReportSubject = "TimeTracking.ReopenTimeReport.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notify about setting meet me (subject)", "MeetMe.MeetingInvitationNotification.Subject.cshtml")]
        public const string MeetMeSendMeetingInvitationSubject = "MeetMe.MeetingInvitationNotification.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notify about setting meet me (body)", "MeetMe.MeetingInvitationNotification.Body.cshtml")]
        public const string MeetMeSendMeetingInvitationBody = "MeetMe.MeetingInvitationNotification.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification for employee when his/her invoice is accepted (subject)", "Compensation.InvoiceAccepted.Subject.cshtml")]
        public const string TimeReportInvoiceAcceptedSubject = "Compensation.InvoiceAccepted.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification for employee when his/her invoice is accepted (body)", "Compensation.InvoiceAccepted.Body.cshtml")]
        public const string TimeReportInvoiceAcceptedBody = "Compensation.InvoiceAccepted.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Notification for employee when his/her invoice is rejected (subject)", "Compensation.InvoiceRejected.Subject.cshtml")]
        public const string TimeReportInvoiceRejectedSubject = "Compensation.InvoiceRejected.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Notification for employee when his/her invoice is rejected (body)", "Compensation.InvoiceRejected.Body.cshtml")]
        public const string TimeReportInvoiceRejectedBody = "Compensation.InvoiceRejected.Body";
    }
}