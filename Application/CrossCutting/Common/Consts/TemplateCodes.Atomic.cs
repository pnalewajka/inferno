using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public partial class TemplateCodes
    {
        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when a user is created; contains user password (body)", "Accounts.Users.PasswordEmail.Body.cshtml")]
        public const string AccountsNewUserPasswordEmailBody = "Accounts.Users.PasswordEmail.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when a user is created; contains user password (subject)", "Accounts.Users.PasswordEmail.Subject.cshtml")]
        public const string AccountsNewUserPasswordEmailSubject = "Accounts.Users.PasswordEmail.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Email sent when a user is issued a login token (body)", "Accounts.Users.LoginTokenEmail.Body.cshtml")]
        public const string AccountsNewUserLoginTokenEmailBody = "Accounts.Users.LoginTokenEmail.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when a user is issued a login token (subject)", "Accounts.Users.LoginTokenEmail.Subject.cshtml")]
        public const string AccountsNewUserLoginTokenEmailSubject = "Accounts.Users.LoginTokenEmail.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Last run attempt of Job failed (Body)", "JobScheduler.LastAttemptOfJobFailed.Body.cshtml")]
        public const string JobSchedulerLastAttemptOfJobFailedBody = "JobScheduler.LastAttemptOfJobFailed.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Last run attempt of Job failed (Subject)", "JobScheduler.LastAttemptOfJobFailed.Subject.cshtml")]
        public const string JobSchedulerLastAttemptOfJobFailedSubject = "JobScheduler.LastAttemptOfJobFailed.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when job failed during processing", "EventSourcing.EventHandlingFailed.Subject.cshtml")]
        public const string EventSourcingEventHandlingFailedSubject = "EventSourcing.EventHandlingFailed.Subject";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when job failed during processing", "EventSourcing.EventHandlingFailed.MoreAttempts.Body.cshtml")]
        public const string EventSourcingEventHandlingFailedMoreAttemptsBody = "EventSourcing.EventHandlingFailed.MoreAttempts.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Email sent when job failed during processing", "EventSourcing.EventHandlingFailed.NoMoreAttempts.Body.cshtml")]
        public const string EventSourcingEventHandlingFailedNoMoreAttemptsBody = "EventSourcing.EventHandlingFailed.NoMoreAttempts.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Bulk edit error message (Subject)", "BulkEdit.ErrorMessage.Subject.cshtml")]
        public const string BulkEditErrorMessageSubject = "BulkEdit.ErrorMessage.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Bulk edit error message (Body)", "BulkEdit.ErrorMessage.Body.cshtml")]
        public const string BulkEditErrorMessageBody = "BulkEdit.ErrorMessage.Body";

        [TemplateDefinition(MessageTemplateContentType.PlainText, "Bulk edit success message (Subject)", "BulkEdit.SuccessMessage.Subject.cshtml")]
        public const string BulkEditSuccessMessageSubject = "BulkEdit.SuccessMessage.Subject";

        [TemplateDefinition(MessageTemplateContentType.Html, "Bulk edit success message (Body)", "BulkEdit.SuccessMessage.Body.cshtml")]
        public const string BulkEditSuccessMessageBody = "BulkEdit.SuccessMessage.Body";
    }
}