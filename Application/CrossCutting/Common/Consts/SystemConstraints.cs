﻿namespace Smt.Atomic.CrossCutting.Common.Consts
{
    /// <summary>
    /// Commonly used values that constrain the system
    /// </summary>
    public static class SystemConstraints
    {
        public const int MinCalendarYear = 1900;
        public const int MaxCalendarYear = 3000;
    }
}