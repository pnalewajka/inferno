﻿namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public static class CurrencyIsoCodes
    {
        public const string PolishZloty = "PLN";

        public const string PoundSterling = "GBP";

        public const string UnitedStatesDollar = "USD";

        public const string Euro = "EUR";
    }
}
