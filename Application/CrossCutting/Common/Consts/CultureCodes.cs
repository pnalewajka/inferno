﻿namespace Smt.Atomic.CrossCutting.Common.Consts
{
    public static class CultureCodes
    {
        public const string English = "en";
        public const string EnglishBritish = "en-GB";
        public const string EnglishAmerican = "en-US";
        public const string Polish = "pl-PL";
    }
}
