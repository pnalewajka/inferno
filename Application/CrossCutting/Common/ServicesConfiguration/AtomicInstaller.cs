﻿using Castle.Windsor;

namespace Smt.Atomic.CrossCutting.Common.ServicesConfiguration
{
    public abstract class AtomicInstaller
    {
        /// <summary>
        /// True if class mappings should be installed by default by convention
        /// Set to false if there is more then one class mapping or for better (manual) control
        /// </summary>
        public bool AutoInstallClassMappings { get; protected set; }
        
        /// <summary>
        /// Configure all services from the given assembly
        /// </summary>
        /// <param name="containerType">Type of the container if configuration should vary depending on the container</param>
        /// <param name="container">Container to be configured</param>
        public abstract void Install(ContainerType containerType, IWindsorContainer container);

        protected AtomicInstaller()
        {
            AutoInstallClassMappings = true;
        }
    }
}
