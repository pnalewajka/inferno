﻿using System;
using System.Collections.Generic;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.CrossCutting.Common.ServicesConfiguration
{
    /// <summary>
    /// Class used to initialize container and store information about container configuration
    /// </summary>
    public abstract class ContainerConfiguration
    {
        protected static WindsorContainer InternalContainer;
        
        /// <summary>
        /// Static accessor for current container
        /// </summary>
        internal static WindsorContainer Container => InternalContainer;

        /// <summary>
        /// Configure container for the given container type
        /// </summary>
        /// <param name="containerType">Type of the container to be configured</param>
        /// <returns>Ready to use container for the given application</returns>
        public virtual WindsorContainer Configure(ContainerType containerType)
        {
            if (InternalContainer != null && containerType != ContainerType.UnitTests)
            {
                throw new InvalidOperationException("Container already configured");
            }

            InternalContainer = new WindsorContainer();
            InternalContainer.AddFacility<LoggingFacility>(f => f.UseNLog().ConfiguredExternally());
            InternalContainer.AddFacility(new TypedFactoryFacility());
            InternalContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(InternalContainer.Kernel));

            foreach (var atomicInstaller in GetInstallers(containerType))
            {
                atomicInstaller.Install(containerType, InternalContainer);

                if (atomicInstaller.AutoInstallClassMappings)
                {
                    InternalContainer.Register(
                        Classes.FromAssemblyContaining(atomicInstaller.GetType())
                            .BasedOn(typeof (IClassMapping<,>)).WithServiceSelf()
                            .WithServiceFirstInterface().LifestyleSingleton());
                }
            }

            return InternalContainer;
        }

        protected abstract IEnumerable<AtomicInstaller> GetInstallers(ContainerType containerType);

        [Obsolete("Direct access to container is considered to be antipattern and should be avoided if possible")]
        public static IKernel GetContainer()
        {
            return InternalContainer.Kernel;
        }

        [Obsolete("This method is supposed to be used only in unit tests")]
        public static void CleanUpConfiguration()
        {
            InternalContainer = null;
        }
    }
}