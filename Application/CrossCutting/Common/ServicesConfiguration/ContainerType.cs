﻿namespace Smt.Atomic.CrossCutting.Common.ServicesConfiguration
{
    /// <summary>
    /// Type of the container (application)
    /// </summary>
    public enum ContainerType
    {
        WebApp,
        WebApi,
        WebServices,
        JobScheduler,
        UnitTests
    }
}