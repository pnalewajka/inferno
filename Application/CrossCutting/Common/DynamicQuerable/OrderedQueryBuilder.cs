﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.CrossCutting.Common.DynamicQuerable
{
    public class OrderedQueryBuilder<T>
    {
        private readonly IQueryable<T> _source;

        public IOrderedQueryable<T> Result { get; private set; }

        public OrderedQueryBuilder(IQueryable<T> source)
        {
            _source = source;
        }

        public void ApplySortingKey<TKey>(Expression<Func<T, TKey>> keySelector, SortingDirection sortingDirection)
        {
            if (Result == null)
            {
                Result = sortingDirection == SortingDirection.Descending
                    ? _source.OrderByDescending(keySelector)
                    : _source.OrderBy(keySelector);
            }
            else
            {
                Result = sortingDirection == SortingDirection.Descending
                    ? Result.ThenByDescending(keySelector)
                    : Result.ThenBy(keySelector);
            }
        }

        public void ApplySortingKey(PropertyPath propertyPath, SortingDirection sortingDirection)
        {
            if (Result == null)
            {
                Result = sortingDirection == SortingDirection.Descending
                    ? _source.OrderByPropertyDescending(propertyPath.Elements)
                    : _source.OrderByProperty(propertyPath.Elements);
            }
            else
            {
                Result = sortingDirection == SortingDirection.Descending
                    ? Result.ThenByPropertyDescending(propertyPath.Elements)
                    : Result.ThenByProperty(propertyPath.Elements);
            }
        }
    }
}
