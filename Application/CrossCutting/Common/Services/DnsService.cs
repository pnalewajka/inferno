﻿using System.Net;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Services
{
    internal class DnsService : IDnsService
    {
        public string GetHostName()
        {
            return Dns.GetHostName();
        }
    }
}
