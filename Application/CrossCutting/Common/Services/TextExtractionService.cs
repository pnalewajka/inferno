﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Services
{
    public partial class TextExtractionService : ITextExtractionService
    {
        public string ExtractPlainText(byte[] content, string extension)
        {
            const string textExtractorPath = "TextExtractor/TextExtractor.exe";

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    Arguments = $"-c \"{extension}\"",
                    FileName = Path.Combine(HttpRuntime.BinDirectory, textExtractorPath)
                }
            };

            ConfigureCredentials(process.StartInfo);

            process.Start();

            process.StandardInput.Write(Convert.ToBase64String(content));
            process.StandardInput.Close();

            var output = process.StandardOutput.ReadToEnd();
            var error = process.StandardError.ReadToEnd();

            process.WaitForExit();

            if (process.ExitCode != 0 && !string.IsNullOrEmpty(error))
            {
                const string invalidStreamException = "IFilterTextReader.Exceptions.IFOldFilterFormat";

                if (error.Contains(invalidStreamException))
                {
                    throw new TextExtractionException(TextExtractionResources.FileCorrupted);
                }

                /*
                 * Text extraction will always fail if you have no proper IFilter installed on your machine:
                 *
                 * .docx, .docm, .pptx, .pptm, .xlsx, .xlsm, .xlsb, .zip, .one, .vdx, .vsd, .vss, .vst, .vdx, .vsx, and .vtx
                 * https://www.microsoft.com/en-us/download/details.aspx?id=17062
                 *
                 * .pdf
                 * https://supportdownloads.adobe.com/detail.jsp?ftpID=5542
                 */

                throw new TextExtractionException(error);
            }

            return output;
        }
    }
}
