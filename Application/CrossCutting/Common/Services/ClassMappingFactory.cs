﻿using System;
using Castle.MicroKernel;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Services
{
    internal class ClassMappingFactory : IClassMappingFactory
    {
        private readonly IKernel _kernel;

        public ClassMappingFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IClassMapping<TSource, TDestination> CreateMapping<TSource, TDestination>() 
            where TDestination : class 
            where TSource : class
        {
            return (IClassMapping<TSource, TDestination>) CreateMapping(typeof(TSource), typeof(TDestination));
        }

        public IClassMapping CreateMapping(Type sourceType, Type destinationType)
        {
            var classMappingType = typeof(IClassMapping<,>).MakeGenericType(sourceType, destinationType);
            return (IClassMapping)_kernel.Resolve(classMappingType);
        }
    }
}