﻿using System.Diagnostics;
using System.Security;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Services
{
    public partial class TextExtractionService : ITextExtractionService
    {
        private readonly ISystemParameterService _systemParameterService;

        public TextExtractionService(ISystemParameterService systemParameterService)
        {
            _systemParameterService = systemParameterService;
        }

        public void ConfigureCredentials(ProcessStartInfo startInfo)
        {
            var password = new SecureString();

            foreach (var character in _systemParameterService.GetParameter<string>(ParameterKeys.TextExtractionPassword))
            {
                password.AppendChar(character);
            }

            startInfo.UserName = _systemParameterService.GetParameter<string>(ParameterKeys.TextExtractionUserName);
            startInfo.Password = password;
            startInfo.Domain = _systemParameterService.GetParameter<string>(ParameterKeys.TextExtractionDomain);
        }
    }
}
