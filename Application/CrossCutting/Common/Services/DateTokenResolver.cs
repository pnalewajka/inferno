﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Services
{
    public class DateTokenResolver : IUrlTokenResolver
    {
        private readonly ITimeService _timeService;
        private const string DateTokenFormat = "{0:yyyy-MM-dd}";

        private const string TodayToken = "%TODAY%";
        private const string FirstDayOfYearToken = "%FDOY%";
        private const string LastDayOfYearToken = "%LDOY%";
        private const string FirstDayOfWeekToken = "%FDOW%";
        private const string CurrentYearToken = "%YEAR%";
        private const string CurrentMonthToken = "%MONTH%";

        public DateTokenResolver(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public IEnumerable<string> GetTokens()
        {
            yield return TodayToken;
            yield return FirstDayOfYearToken;
            yield return LastDayOfYearToken;
            yield return FirstDayOfWeekToken;
            yield return CurrentYearToken;
            yield return CurrentMonthToken;
        }

        public string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            switch (token)
            {
                case TodayToken:
                    return string.Format(DateTokenFormat, _timeService.GetCurrentDate());

                case FirstDayOfYearToken:
                    return string.Format(DateTokenFormat, _timeService.GetCurrentDate().GetFirstDayOfYear());

                case LastDayOfYearToken:
                    return string.Format(DateTokenFormat,
                        _timeService.GetCurrentDate().GetFirstDayOfYear().AddYears(1).AddDays(-1));

                case FirstDayOfWeekToken:
                    return string.Format(DateTokenFormat, _timeService.GetCurrentDate().GetPreviousDayOfWeek(DayOfWeek.Monday));

                case CurrentYearToken:
                    return _timeService.GetCurrentDate().Year.ToString();

                case CurrentMonthToken:
                    return _timeService.GetCurrentDate().Month.ToString();
            }

            throw new ArgumentOutOfRangeException(nameof(token));
        }
    }
}
