﻿namespace Smt.Atomic.CrossCutting.Common.ViewModels
{
    public interface IViewModelFactory<T>
    {
        T Create();
    }
}