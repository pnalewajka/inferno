﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Datetime accessing administration functions
    /// </summary>
    public interface ITimeServiceAdministration
    {
        /// <summary>
        /// Is time being faked?
        /// </summary>
        /// <returns></returns>
        bool IsTimeFake();
        
        /// <summary>
        /// Does current setup allows to modify time
        /// </summary>
        /// <returns></returns>
        bool IsFakeTimeAllowed();
    }
}