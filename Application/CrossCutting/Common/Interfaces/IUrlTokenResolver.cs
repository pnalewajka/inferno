﻿using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface IUrlTokenResolver
    {
        IEnumerable<string> GetTokens();
        string GetTokenValue(string token);
    }
}
