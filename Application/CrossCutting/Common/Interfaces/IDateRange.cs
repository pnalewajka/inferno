﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface IDateRange
    {
        DateTime StartDate { get; }

        DateTime? EndDate { get; }
    }
}
