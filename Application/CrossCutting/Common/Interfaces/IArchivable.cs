﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Represents archivable file used for Zip compression.
    /// </summary>
    public interface IArchivable
    {
        byte[] Content { get; }
        string FileName { get; }
    }
}