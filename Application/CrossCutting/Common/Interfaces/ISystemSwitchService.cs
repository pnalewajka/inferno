﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Retrieves value of a system switch.
    /// System switch is a parameter-like configuration item that significantly affects system behavior.
    /// System switches are stored as simple-value parameters and do not depend on context.
    /// </summary>
    public interface ISystemSwitchService
    {
        /// <summary>
        /// Retrieves time offset for the time service
        /// </summary>
        /// <returns>System time offset in seconds</returns>
        long GetSystemTimeOffset();
    }
}