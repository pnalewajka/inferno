﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Datetime accessing
    /// </summary>
    public interface ITimeService
    {
        /// <summary>
        /// Get Current DateTime
        /// </summary>
        /// <returns></returns>
        DateTime GetCurrentTime();

        /// <summary>
        /// Get Current Date - without time part
        /// </summary>
        /// <returns></returns>
        DateTime GetCurrentDate();

        /// <summary>
        /// Get Current time offset
        /// </summary>
        /// <returns></returns>
        TimeSpan? GetOffset();
    }
}