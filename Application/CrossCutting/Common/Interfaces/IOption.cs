﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface IOption
    {
        bool IsSet { get; set; }
    }
}
