﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface IAuditService
    {
        bool IsAuditEnabled();
        void EnableAudit();
        void DisableAudit(bool dropTables = false);
        void EnableAuditOnTables(long[] ids);
        void DisableAuditOnTables(long[] ids, bool dropTables = false);
    }
}
