﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Time db service for factory method
    /// </summary>
    public interface IDbTimeService : ITimeService
    {
    }
}