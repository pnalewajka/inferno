﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface IWeeklyHours
    {
        decimal MondayHours { get; }

        decimal TuesdayHours { get; }

        decimal WednesdayHours { get; }

        decimal ThursdayHours { get; }

        decimal FridayHours { get; }

        decimal SaturdayHours { get; }

        decimal SundayHours { get; }
    }
}