﻿using System;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface IClassMappingFactory
    {
        IClassMapping<TSource, TDestination> CreateMapping<TSource, TDestination>()
            where TDestination : class
            where TSource : class;

        IClassMapping CreateMapping(Type sourceType, Type destinationType);
    }
}