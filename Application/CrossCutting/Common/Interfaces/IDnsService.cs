﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Access to dns subsystem
    /// </summary>
    public interface IDnsService
    {
        /// <summary>
        /// get host name of local machine
        /// </summary>
        /// <returns></returns>
        string GetHostName();
    }
}
