﻿using System.Threading.Tasks;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Retrieves system parameter values based on parameter key and context.
    /// Parameter values can be stored as switch-like expressions so that the resulting value can be different based e.g. on who or when makes the query
    /// </summary>
    public interface ISystemParameterService
    {
        /// <summary>
        /// Returns the default context that is being used for SystemParameter retrieval
        /// </summary>
        /// <returns>Default Context</returns>
        object GetDefaultContext();

        /// <summary>
        /// Returns the default context for given employee
        /// </summary>
        /// <returns>Default Context</returns>
        object GetDefaultContext(long employeeId);

        /// <summary>
        /// Retrieves parameter value of a given type with a default context
        /// </summary>
        /// <typeparam name="TValue">Expected parameter value type</typeparam>
        /// <param name="parameterKey">Parameter key</param>
        /// <returns>Parameter value</returns>
        TValue GetParameter<TValue>(string parameterKey);

        /// <summary>
        /// Retrieves parameter value of a given type with the specified context
        /// </summary>
        /// <typeparam name="TValue">Expected parameter value type</typeparam>
        /// <param name="parameterKey">Parameter key</param>
        /// <param name="context">Parameter context</param>
        /// <returns>Parameter value</returns>
        TValue GetParameter<TValue>(string parameterKey, object context);

        /// <summary>
        /// Retrieves parameter value of a given type with a default context - asynchronously
        /// </summary>
        /// <typeparam name="TValue">Expected parameter value type</typeparam>
        /// <param name="parameterKey">Parameter key</param>
        /// <returns>Parameter value</returns>
        Task<TValue> GetParameterAsync<TValue>(string parameterKey);

        /// <summary>
        /// Retrieves parameter value of a given type with the specified context - asynchronously
        /// </summary>
        /// <typeparam name="TValue">Expected parameter value type</typeparam>
        /// <param name="parameterKey">Parameter key</param>
        /// <param name="context">Parameter context</param>
        /// <returns>Parameter value</returns>
        Task<TValue> GetParameterAsync<TValue>(string parameterKey, object context);
    }
}