﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Services;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    /// <summary>
    /// Encapsulates http client logic
    /// </summary>
    public interface IHttpClientService
    {
        /// <summary>
        /// Download page, if postData is not null, then method will be set to 'POST', 'GET" will be used otherwise
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="getData"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        HttpResponseData RetrieveHtmlPage(string url, IDictionary<string, string> headers, IDictionary<string, string> getData, IDictionary<string, string> postData);
    }
}
