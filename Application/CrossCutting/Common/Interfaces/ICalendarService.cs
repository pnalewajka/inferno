﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface ICalendarService
    {
        /// <summary>
        /// Returns the number of non-holiday days between two days specified, including both start and end.
        /// Takes into consideration holidays specified in the selected calendar, 
        /// as well as default non-working days for that calendar.
        /// </summary>
        /// <param name="startDate">Start date of the range to examine</param>
        /// <param name="endDate">End date of the range to examine</param>
        /// <param name="calendarCode">Code of the calendar to use</param>
        /// <returns>Number of working days in the range</returns>
        int GetWorkdayCount(DateTime startDate, DateTime endDate, string calendarCode);

        /// <summary>
        /// Returns the number of non-holiday days between two days specified, including both start and end.
        /// Takes into consideration holidays specified in the selected calendar, 
        /// as well as default non-working days for that calendar.
        /// </summary>
        /// <param name="startDate">Start date of the range to examine</param>
        /// <param name="endDate">End date of the range to examine</param>
        /// <param name="calendarId">Id of the calendar to use</param>
        /// <returns>Number of working days in the range</returns>
        int GetWorkdayCount(DateTime startDate, DateTime endDate, long calendarId);

        /// <summary>
        /// Checks if the specified date is a working day.
        /// Takes into consideration holidays specified in the selected calendar, 
        /// as well as default non-working days for that calendar.
        /// </summary>
        /// <param name="date">Date to examine</param>
        /// <param name="calendarCode">Code of the calendar to use</param>
        /// <returns>True if date is a working day</returns>
        bool IsWorkday(DateTime date, string calendarCode);

        /// <summary>
        /// Checks if the specified date is a working day.
        /// Takes into consideration holidays specified in the selected calendar, 
        /// as well as default non-working days for that calendar.
        /// </summary>
        /// <param name="date">Date to examine</param>
        /// <param name="calendarId">Id of the calendar to use</param>
        /// <returns>True if date is a working day</returns>
        bool IsWorkday(DateTime date, long calendarId);

        /// <summary>
        /// Returns a date which is <paramref name="days"/> working days from <paramref name="date"/>
        /// </summary>
        /// <param name="date">Date to which days will be added</param>
        /// <param name="days">Number of days to add to the given date</param>
        /// <param name="calendarCode">Code of the calendar to use</param>
        /// <returns></returns>
        DateTime AddWorkdays(DateTime date, int days, string calendarCode);

        /// <summary>
        /// Returns a date which is <paramref name="days"/> working days from <paramref name="date"/>
        /// </summary>
        /// <param name="date">Date to which days will be added</param>
        /// <param name="days">Number of days to add to the given date</param>
        /// <param name="calendarId">Id of the calendar to use</param>
        /// <returns></returns>
        DateTime AddWorkdays(DateTime date, int days, long calendarId);

        /// <summary>
        /// Returns a calendar id by given corresponding calendar code
        /// </summary>
        /// <param name="calendarCode">Calendar code</param>
        /// <returns>Calendar id</returns>
        long? GetCalendarId(string calendarCode);

        /// <summary>
        /// Returns collection of default days off between two days specified, including both start and end.
        /// </summary>
        /// <param name="startDate">Start date of the range to examine</param>
        /// <param name="endDate">End date of the range to examine</param>
        /// <param name="calendarId">Id of the calendar to use</param>
        /// <returns>Collection of holiday days between two days</returns>
        ICollection<DateTime> GetDefaultDaysOff(DateTime startDate, DateTime endDate, long calendarId);

        /// <summary>
        /// Returns collection of holiday days between two days specified, including both start and end.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="calendarId"></param>
        /// <returns></returns>
        ICollection<DateTime> GetHolidays(DateTime startDate, DateTime endDate, long calendarId);
    }
}