﻿namespace Smt.Atomic.CrossCutting.Common.Interfaces
{
    public interface ITextExtractionService
    {
        string ExtractPlainText(byte[] content, string extension);
    }
}
