﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    /// <summary>
    /// Method for cloning helper
    /// </summary>
    public enum CloningMethod
    {
        /// <summary>
        /// Use jsonconvert
        /// </summary>
        Json
    }
}
