﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    /// <summary>
    /// Configure enum name conversion mode when serializing/deserializing
    /// </summary>
    public enum EnumNameConversion
    {
        /// <summary>
        /// Default behavior
        /// </summary>
        None,

        /// <summary>
        /// Convert enum values from/to hyphenated
        /// </summary>
        ToHyphenated,
    }
}
