﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [Flags]
    public enum DateComparer
    {
        LeftOpen = 1 << 0,
        LeftClosed = 1 << 1,
        RightOpen = 1 << 2,
        RightClosed = 1 << 3,
    }
}
