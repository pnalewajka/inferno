﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum AlertType
    {
        Success,
        Information,
        Warning,
        Error
    }
}