﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [SqlEnum(DatabaseSchemes.Workflows)]
    public enum RequestStatus
    {
        [DescriptionLocalized(nameof(RequestResources.StatusDraft), typeof(RequestResources))]
        Draft,

        [DescriptionLocalized(nameof(RequestResources.StatusPending), typeof(RequestResources))]
        Pending,

        [DescriptionLocalized(nameof(RequestResources.StatusApproved), typeof(RequestResources))]
        Approved,

        [DescriptionLocalized(nameof(RequestResources.StatusRejected), typeof(RequestResources))]
        Rejected,

        [DescriptionLocalized(nameof(RequestResources.StatusCompleted), typeof(RequestResources))]
        Completed,

        [DescriptionLocalized(nameof(RequestResources.StatusError), typeof(RequestResources))]
        Error
    }
}