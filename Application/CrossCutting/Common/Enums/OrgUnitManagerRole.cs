﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum OrgUnitManagerRole
    {
        [DescriptionLocalized("OperationalManager", typeof(OrgUnitManagerRoleResources))]
        OperationalManager = 1,

        [DescriptionLocalized("OperationalDirector", typeof(OrgUnitManagerRoleResources))]
        OperationalDirector = 2,

        [DescriptionLocalized("GeneralManager", typeof(OrgUnitManagerRoleResources))]
        GeneralManager = 3
    }
}
