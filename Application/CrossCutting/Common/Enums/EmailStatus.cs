﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [SqlEnum(DatabaseSchemes.Notifications)]
    public enum EmailStatus
    {
        [DescriptionLocalized("Pending", typeof(EmailStatusResources))]
        Pending = 1,

        [DescriptionLocalized("Sent", typeof(EmailStatusResources))]
        Sent = 2,

        [DescriptionLocalized("DeliveryFailed", typeof(EmailStatusResources))]
        DeliveryFailed = 3
    }
}