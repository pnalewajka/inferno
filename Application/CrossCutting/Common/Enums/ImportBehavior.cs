﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ImportBehavior
    {
        Insert,
        InsertOrUpdate
    }
}
