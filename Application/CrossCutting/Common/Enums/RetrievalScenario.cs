﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    /// <summary>
    /// How are records retrieved in the data service layer?
    /// </summary>
    public enum RetrievalScenario
    {
        /// <summary>
        /// Not applicable in this case
        /// </summary>
        NotApplicable = 0,

        /// <summary>
        /// Load a single record
        /// </summary>
        SingleRecord = 1,

        /// <summary>
        /// Load several records
        /// </summary>
        MultipleRecords = 2,
    }
}
