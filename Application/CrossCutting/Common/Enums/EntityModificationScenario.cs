﻿using System;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [Flags]
    public enum EntityModificationScenario
    {
        AddedOnly = 1 << 0,

        ModifiedOnly = 1 << 1,

        Any = AddedOnly | ModifiedOnly
    }
}
