﻿using Smt.Atomic.CrossCutting.Common.Resources;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [SqlEnum(DatabaseSchemes.CustomerPortal)]
    public enum InterviewType
    {
        [Display(Name = "InterviewTypeF2FLabel", ResourceType =typeof(GeneralResources))]
        F2F = 1,
        [Display(Name = "InterviewTypeSkypeLbel", ResourceType = typeof(GeneralResources))]
        Skype = 2,
        [Display(Name = "InterviewTypePhoneLabel", ResourceType = typeof(GeneralResources))]
        Phone = 3,
        [Display(Name = "InterviewTypeOtherLabel", ResourceType = typeof(GeneralResources))]
        Other = 4,
    }
}
