﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [SqlEnum(DatabaseSchemes.Configuration)]
    public enum CalendarEntryType
    {
        [DescriptionLocalized("BankHoliday", typeof(CalendarEntryTypeResources))]
        BankHoliday,

        [DescriptionLocalized("CompanyHoliday", typeof(CalendarEntryTypeResources))]
        CompanyHoliday
    }
}
