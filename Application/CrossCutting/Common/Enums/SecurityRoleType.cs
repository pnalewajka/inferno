﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    /// <summary>
    /// All security roles between 0 and 9999 are reserved for atomic use
    /// </summary>
    public enum SecurityRoleType
    {
        [SecurityRoleDescription("CanViewUsers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewUsers,

        [SecurityRoleDescription("CanAddUsers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanAddUsers,

        [SecurityRoleDescription("CanEditUsers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanEditUsers,

        [SecurityRoleDescription("CanDeleteUsers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanDeleteUsers,

        [SecurityRoleDescription("CanAssignRoles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanAssignRoles,

        [SecurityRoleDescription("CanAssignProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanAssignProfiles,

        [SecurityRoleDescription("CanRevokeRoles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanRevokeRoles,

        [SecurityRoleDescription("CanRevokeProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanRevokeProfiles,

        [SecurityRoleDescription("CanViewUserRoles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewUserRoles,

        [SecurityRoleDescription("CanViewUserProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewUserProfiles,

        [SecurityRoleDescription("CanViewProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewProfiles,

        [SecurityRoleDescription("CanManageJobDefinitions", typeof(SecurityRoleResources), SecurityRoleModule.Scheduling)]
        CanManageJobDefinitions,

        [SecurityRoleDescription("CanManageJobDefinitionParameters", typeof(SecurityRoleResources), SecurityRoleModule.Scheduling)]
        CanManageJobDefinitionParameters,

        [SecurityRoleDescription("CanManagePipelines", typeof(SecurityRoleResources), SecurityRoleModule.Scheduling)]
        CanManagePipelines,

        [SecurityRoleDescription("CanViewSystemParameters", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanViewSystemParameters,

        [SecurityRoleDescription("CanAddSystemParameters", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanAddSystemParameters,

        [SecurityRoleDescription("CanEditSystemParameters", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanEditSystemParameters,

        [SecurityRoleDescription("CanDeleteSystemParameters", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanDeleteSystemParameters,

        [SecurityRoleDescription("CanUploadDocuments", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanUploadDocuments,

        [SecurityRoleDescription("CanViewSecurityAreas", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewSecurityAreas,

        [SecurityRoleDescription("CanDownloadDocuments", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanDownloadDocuments,

        [SecurityRoleDescription("CanViewMessageTemplates", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanViewMessageTemplates,

        [SecurityRoleDescription("CanAddMessageTemplates", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanAddMessageTemplates,

        [SecurityRoleDescription("CanEditMessageTemplates", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanEditMessageTemplates,

        [SecurityRoleDescription("CanDeleteMessageTemplates", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanDeleteMessageTemplates,

        [SecurityRoleDescription("CanViewCalendars", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanViewCalendars,

        [SecurityRoleDescription("CanAddCalendars", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanAddCalendars,

        [SecurityRoleDescription("CanEditCalendars", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanEditCalendars,

        [SecurityRoleDescription("CanDeleteCalendars", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanDeleteCalendars,

        [SecurityRoleDescription("CanImportUsers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanImportUsers,

        [SecurityRoleDescription("CanViewEmails", typeof(SecurityRoleResources), SecurityRoleModule.Notifications)]
        CanViewEmails,

        [SecurityRoleDescription("CanViewUserDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewUserDataFilters,

        [SecurityRoleDescription("CanAddUserDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanAddUserDataFilters,

        [SecurityRoleDescription("CanEditUserDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanEditUserDataFilters,

        [SecurityRoleDescription("CanDeleteUserDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanDeleteUserDataFilters,

        [SecurityRoleDescription("CanViewProfileDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewProfileDataFilters,

        [SecurityRoleDescription("CanAddProfileDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanAddProfileDataFilters,

        [SecurityRoleDescription("CanEditUserDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanEditProfileDataFilters,

        [SecurityRoleDescription("CanDeleteUserDataFilters", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanDeleteProfileDataFilters,

        [SecurityRoleDescription("CanViewSoftDeletedEntities", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanViewSoftDeletedEntities,

        [SecurityRoleDescription("CanViewOrgUnits", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanViewOrgUnits,

        [SecurityRoleDescription("CanAddOrgUnits", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanAddOrgUnits,

        [SecurityRoleDescription("CanEditOrgUnits", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanEditOrgUnits,

        [SecurityRoleDescription("CanDeleteOrgUnits", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanDeleteOrgUnits,

        [SecurityRoleDescription("CanManageAudit", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanManageAudit,

        [SecurityRoleDescription("CanViewPublishedEventQueueItem", typeof(SecurityRoleResources), SecurityRoleModule.EventSourcing)]
        CanViewPublishedEventQueueItem = 48,

        [SecurityRoleDescription("CanReExecutePublishedEventQueueItem", typeof(SecurityRoleResources), SecurityRoleModule.EventSourcing)]
        CanReExecutePublishedEventQueueItem = 49, //direct set do fix deletion 

        [SecurityRoleDescription("CanExecuteDeveloperTools", typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanExecuteDeveloperTools = 50,

        [SecurityRoleDescription("CanManageUserConsents", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanManageUserConsents = 51,

        [SecurityRoleDescription("CanViewReportDefinition", typeof(SecurityRoleResources), SecurityRoleModule.Reporting)]
        CanViewReportDefinition = 52,

        [SecurityRoleDescription("CanAddReportDefinition", typeof(SecurityRoleResources), SecurityRoleModule.Reporting)]
        CanAddReportDefinition = 53,

        [SecurityRoleDescription("CanEditReportDefinition", typeof(SecurityRoleResources), SecurityRoleModule.Reporting)]
        CanEditReportDefinition = 54,

        [SecurityRoleDescription("CanDeleteReportDefinition", typeof(SecurityRoleResources), SecurityRoleModule.Reporting)]
        CanDeleteReportDefinition = 55,

        [SecurityRoleDescription("CanAddProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanAddProfiles,

        [SecurityRoleDescription("CanEditProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanEditProfiles,

        [SecurityRoleDescription("CanDeleteProfiles", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanDeleteProfiles,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRefreshTemplateDefinitions), typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanRefreshTemplateDefinitions,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanExportEmailTemplates), typeof(SecurityRoleResources), SecurityRoleModule.Configuration)]
        CanExportEmailTemplates,

        // All security roles between 0 and 9999 are reserved for atomic use
        // All security roles below this point are for custom use
        [SecurityRoleDescription("CanChangeOwnPassword", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanChangeOwnPassword = 9999,

        [SecurityRoleDescription("CanViewJobProfiles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanViewJobProfiles = 10000,

        [SecurityRoleDescription("CanAddJobProfiles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanAddJobProfiles = 10001,

        [SecurityRoleDescription("CanEditJobProfiles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanEditJobProfiles = 10002,

        [SecurityRoleDescription("CanDeleteJobProfiles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanDeleteJobProfiles = 10003,
        //Employee

        [SecurityRoleDescription("CanViewEmployee", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmployee = 10004,

        [SecurityRoleDescription("CanAddEmployee", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddEmployee = 10005,

        [SecurityRoleDescription("CanEditEmployee", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditEmployee = 10006,

        [SecurityRoleDescription("CanDeleteEmployee", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteEmployee = 10007,

        [SecurityRoleDescription("CanViewSkills", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanViewSkills = 10008,

        [SecurityRoleDescription("CanAddSkills", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanAddSkills = 10009,

        [SecurityRoleDescription("CanEditSkills", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanEditSkills = 10010,

        [SecurityRoleDescription("CanDeleteSkills", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanDeleteSkills = 10011,

        [SecurityRoleDescription("CanViewSkillTags", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanViewSkillTags = 10012,

        [SecurityRoleDescription("CanAddSkillTags", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanAddSkillTags = 10013,

        [SecurityRoleDescription("CanEditSkillTags", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanEditSkillTags = 10014,

        [SecurityRoleDescription("CanDeleteSkillTags", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanDeleteSkillTags = 10015,
        //EmploymentPeriod

        [SecurityRoleDescription("CanViewEmploymentPeriods", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmploymentPeriods = 10016,

        [SecurityRoleDescription("CanAddEmploymentPeriods", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddEmploymentPeriods = 10017,

        [SecurityRoleDescription("CanEditEmploymentPeriods", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditEmploymentPeriods = 10018,

        [SecurityRoleDescription("CanDeleteEmploymentPeriods", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteEmploymentPeriods = 10019,
        //Project

        [SecurityRoleDescription("CanViewProject", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewProject = 10020,

        [SecurityRoleDescription("CanAddProject", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddProject = 10021,

        [SecurityRoleDescription("CanEditProject", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditProject = 10022,

        [SecurityRoleDescription("CanDeleteProject", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteProject = 10023,

        [SecurityRoleDescription("CanViewStaffingDemand", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewStaffingDemand = 10024,

        [SecurityRoleDescription("CanAddStaffingDemand", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddStaffingDemand = 10025,

        [SecurityRoleDescription("CanEditStaffingDemand", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditStaffingDemand = 10026,

        [SecurityRoleDescription("CanDeleteStaffingDemand", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteStaffingDemand = 10027,
        //AllocationDailyRecord

        [SecurityRoleDescription("CanViewAllocationStatus", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewAllocationStatus = 10032,

        [SecurityRoleDescription("CanAddAllocationStatus", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddAllocationStatus = 10033,

        [SecurityRoleDescription("CanEditAllocationStatus", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditAllocationStatus = 10034,

        [SecurityRoleDescription("CanDeleteAllocationStatus", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteAllocationStatus = 10035,

        [SecurityRoleDescription("CanViewAllocationDailyRecord", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewAllocationDailyRecord = 10036,

        [SecurityRoleDescription("CanAddAllocationDailyRecord", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddAllocationDailyRecord = 10037,

        [SecurityRoleDescription("CanEditAllocationDailyRecord", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditAllocationDailyRecord = 10038,

        [SecurityRoleDescription("CanDeleteAllocationDailyRecord", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteAllocationDailyRecord = 10039,
        //AllocationRequest

        [SecurityRoleDescription("CanViewAllocationRequest", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewAllocationRequest = 10040,

        [SecurityRoleDescription("CanAddAllocationRequest", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddAllocationRequest = 10041,

        [SecurityRoleDescription("CanEditAllocationRequest", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditAllocationRequest = 10042,

        [SecurityRoleDescription("CanResignAllocationRequest", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanResignAllocationRequest = 10043,

        [SecurityRoleDescription("CanViewEmployeeAllocationList", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmployeeAllocationList = 10050,

        [SecurityRoleDescription("CanImportStaffingDemand", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanImportStaffingDemand = 10051,
        //JobMatrix

        [SecurityRoleDescription("CanViewJobMatrix", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanViewJobMatrix = 10100,

        [SecurityRoleDescription("CanAddJobMatrix", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanAddJobMatrix = 10101,

        [SecurityRoleDescription("CanEditJobMatrix", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanEditJobMatrix = 10102,

        [SecurityRoleDescription("CanDeleteJobMatrix", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanDeleteJobMatrix = 10103,
        //Location

        [SecurityRoleDescription("CanViewLocation", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewLocation = 10104,

        [SecurityRoleDescription("CanAddLocation", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddLocation = 10105,

        [SecurityRoleDescription("CanEditLocation", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditLocation = 10106,

        [SecurityRoleDescription("CanDeleteLocation", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteLocation = 10107,

        [SecurityRoleDescription("CanViewJobMatrixLevel", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanViewJobMatrixLevel = 10108,

        [SecurityRoleDescription("CanAddJobMatrixLevel", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanAddJobMatrixLevel = 10109,

        [SecurityRoleDescription("CanEditJobMatrixLevel", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanEditJobMatrixLevel = 10110,

        [SecurityRoleDescription("CanDeleteJobMatrixLevel", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanDeleteJobMatrixLevel = 10111,

        [SecurityRoleDescription("CanEditOwnEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditOwnEmployees = 10112,

        [SecurityRoleDescription("CanEditOwnEmployeeEmploymentPeriods", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditOwnEmployeeEmploymentPeriods = 10113,

        [SecurityRoleDescription("CanEditOwnProjects", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditOwnProjects = 10114,

        [SecurityRoleDescription("CanEditOwnAllocationRequests", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditOwnAllocationRequests = 10115,

        [SecurityRoleDescription("CanBeAssignedAsOrgUnitManager", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanBeAssignedAsOrgUnitManager = 10116,

        [SecurityRoleDescription("CanBeAssignedAsLineManager", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanBeAssignedAsLineManager = 10117,

        [SecurityRoleDescription("CanBeAssignedAsStaffingManager", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanBeAssignedAsStaffingManager = 10118,

        [SecurityRoleDescription("CanViewOwnEmployeeProfile", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewOwnEmployeeProfile = 10119,

        [SecurityRoleDescription("CanLandAtMyEmployeeProfile", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanLandAtMyEmployeeProfile = 10120,

        [SecurityRoleDescription("CanGenerateSeniorityLevelsReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanGenerateSeniorityLevelsReport = 10121,

        [SecurityRoleDescription("CanGenerateUtilizationStatusReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanGenerateUtilizationStatusReport = 10122,

        [SecurityRoleDescription("CanGenerateActiveInactiveEmployeesReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanGenerateActiveInactiveEmployeesReport = 10123,

        [SecurityRoleDescription("CanGenerateNewHiredLeavingEmployeesReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanGenerateNewHiredLeavingEmployeesReport = 10124,

        [SecurityRoleDescription("CanViewIndustry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewIndustry = 10125,

        [SecurityRoleDescription("CanAddIndustry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddIndustry = 10126,

        [SecurityRoleDescription("CanEditIndustry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditIndustry = 10127,

        [SecurityRoleDescription("CanDeleteIndustry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteIndustry = 10128,

        [SecurityRoleDescription("CanViewUsersPicker", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewUsersPicker = 10129,

        [SecurityRoleDescription("CanViewAllocationReports", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanViewAllocationReports = 10130,

        [SecurityRoleDescription("CanViewRegion", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewRegion = 10131,

        [SecurityRoleDescription("CanAddRegion", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddRegion = 10132,

        [SecurityRoleDescription("CanEditRegion", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditRegion = 10133,

        [SecurityRoleDescription("CanDeleteRegion", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteRegion = 10134,

        //NOT USED
        [SecurityRoleDescription("CanViewCrm", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanViewCrm = 10135,

        [SecurityRoleDescription("CanViewBenchEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewBenchEmployees = 10136,

        [SecurityRoleDescription("CanScopeProjectsToAllProjectsByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeProjectsToAllProjectsByDefault = 10137,

        [SecurityRoleDescription("CanScopeAllocationRequestsToAllEmployeesByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeAllocationRequestsToAllEmployeesByDefault = 10138,

        [SecurityRoleDescription("CanScopeEmployeesToAllEmployeesByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeEmployeesToAllEmployeesByDefault = 10139,

        [SecurityRoleDescription("CanScopeEmployeeAllocationsToAllAllocationsByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeEmployeeAllocationsToAllAllocationsByDefault = 10140,

        [SecurityRoleDescription("CanViewTechnologyRadarReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanViewTechnologyRadarReport = 10141,

        [SecurityRoleDescription("CanFilterByMyProjectGuests", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanFilterByMyProjectGuests = 10142,

        [SecurityRoleDescription("CanFilterByMyEmployeesVisiting", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanFilterByMyEmployeesVisiting = 10143,

        [SecurityRoleDescription("CanViewServiceLines", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewServiceLines = 10144,

        [SecurityRoleDescription("CanAddServiceLines", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddServiceLines = 10145,

        [SecurityRoleDescription("CanEditServiceLines", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditServiceLines = 10146,

        [SecurityRoleDescription("CanDeleteServiceLines", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteServiceLines = 10147,

        [SecurityRoleDescription("CanViewSoftwareCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewSoftwareCategories = 10148,

        [SecurityRoleDescription("CanAddSoftwareCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddSoftwareCategories = 10149,

        [SecurityRoleDescription("CanEditSoftwareCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditSoftwareCategories = 10150,

        [SecurityRoleDescription("CanDeleteSoftwareCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteSoftwareCategories = 10151,

        [SecurityRoleDescription("CanViewCustomerSizes", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewCustomerSizes = 10152,

        [SecurityRoleDescription("CanAddCustomerSizes", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddCustomerSizes = 10153,

        [SecurityRoleDescription("CanEditCustomerSizes", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditCustomerSizes = 10154,

        [SecurityRoleDescription("CanDeleteCustomerSizes", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteCustomerSizes = 10155,

        [SecurityRoleDescription("CanViewProjectTags", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewProjectTags = 10156,

        [SecurityRoleDescription("CanAddProjectTags", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanAddProjectTags = 10157,

        [SecurityRoleDescription("CanEditProjectTags", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditProjectTags = 10158,

        [SecurityRoleDescription("CanDeleteProjectTags", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDeleteProjectTags = 10159,

        [SecurityRoleDescription("CanGenerateProjectPresentations", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateProjectPresentations = 10160,

        [SecurityRoleDescription("CanCommentOnBenchEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanCommentOnBenchEmployees = 10161,

        [SecurityRoleDescription("CanBeAssignedAsProjectManager", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanBeAssignedAsProjectManager = 10162,

        [SecurityRoleDescription("CanViewMyTeamAllocations", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewMyTeamAllocations = 10163,

        [SecurityRoleDescription("CanGenerateAllocationPerProjectReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanGenerateAllocationPerProjectReport = 10164,

        [SecurityRoleDescription("CanViewSurveyDefinitions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanViewSurveyDefinitions = 10165,

        [SecurityRoleDescription("CanAddSurveyDefinitions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanAddSurveyDefinitions = 10166,

        [SecurityRoleDescription("CanEditSurveyDefinitions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanEditSurveyDefinitions = 10167,

        [SecurityRoleDescription("CanDeleteSurveyDefinitions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanDeleteSurveyDefinitions = 10168,

        [SecurityRoleDescription("CanViewSurveyQuestions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanViewSurveyQuestions = 10169,

        [SecurityRoleDescription("CanAddSurveyQuestions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanAddSurveyQuestions = 10170,

        [SecurityRoleDescription("CanEditSurveyQuestions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanEditSurveyQuestions = 10171,

        [SecurityRoleDescription("CanDeleteSurveyQuestions", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanDeleteSurveyQuestions = 10172,

        [SecurityRoleDescription("CanViewSurveyConducts", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanViewSurveyConducts = 10173,

        [SecurityRoleDescription("CanCloseSurveyConduct", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanCloseSurveyConduct = 10174,

        [SecurityRoleDescription("CanOpenSurveyConduct", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanOpenSurveyConduct = 10175,

        [SecurityRoleDescription("CanViewSurveys", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanViewSurveys = 10176,

        [SecurityRoleDescription("CanAddRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanAddRequest = 10177,

        [SecurityRoleDescription("CanEditRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanEditRequest = 10178,

        [SecurityRoleDescription("CanDeleteRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanDeleteRequest = 10179,

        [SecurityRoleDescription("CanViewRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanViewRequest = 10180,

        //NOT USED
        [SecurityRoleDescription("CanRequestAllEmployeeKnowledgeChange", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanRequestAllEmployeeKnowledgeChange = 10181,

        //NOT USED
        [SecurityRoleDescription("CanRequestOwnEmployeeKnowledgeChange", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanRequestOwnEmployeeKnowledgeChange = 10182,

        //NOT USED
        [SecurityRoleDescription("CanRequestOwnKnowledgeChange", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanRequestOwnKnowledgeChange = 10183,

        [SecurityRoleDescription("CanScopeAllocationRequestsToOwnOrgUnitsByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeAllocationRequestsToOwnOrgUnitsByDefault = 10184,

        [SecurityRoleDescription("CanScopeEmployeesToOwnOrgUnitsByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeEmployeesToOwnOrgUnitsByDefault = 10185,

        [SecurityRoleDescription("CanScopeEmployeeAllocationsToOwnOrgUnitsByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeEmployeeAllocationsToOwnOrgUnitsByDefault = 10186,

        [SecurityRoleDescription("CanScopeBenchEmployeesToOwnOrgUnitsByDefault", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanScopeBenchEmployeesToOwnOrgUnitsByDefault = 10187,

        [SecurityRoleDescription("CanAddVacationBalance", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddVacationBalance = 10188,

        [SecurityRoleDescription("CanEditVacationBalance", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditVacationBalance = 10189,

        [SecurityRoleDescription("CanDeleteVacationBalance", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteVacationBalance = 10190,

        [SecurityRoleDescription("CanViewVacationBalance", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewVacationBalance = 10191,

        [SecurityRoleDescription("CanMergeProjects", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanMergeProjects = 10192,

        [SecurityRoleDescription("CanViewMyTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewMyTimeReport = 10193,

        [SecurityRoleDescription("CanSaveMyTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanSaveMyTimeReport = 10194,

        [SecurityRoleDescription("CanSubmitRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanSubmitRequest = 10197,

        [SecurityRoleDescription("CanApproveRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanApproveRequest = 10198,

        [SecurityRoleDescription("CanRejectRequest", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanRejectRequest = 10199,

        [SecurityRoleDescription("CanExportAllocationRequests", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportAllocationRequests = 10200,

        [SecurityRoleDescription("CanExportSkillTags", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanExportSkillTags = 10201,

        [SecurityRoleDescription("CanExportSkills", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanExportSkills = 10202,

        [SecurityRoleDescription("CanExportJobProfiles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanExportJobProfiles = 10203,

        [SecurityRoleDescription("CanExportEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportEmployees = 10204,

        [SecurityRoleDescription("CanExportProjects", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportProjects = 10205,

        [SecurityRoleDescription("CanExportProjectTags", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportProjectTags = 10206,

        [SecurityRoleDescription("CanExportProjectStaffingDemands", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportProjectStaffingDemands = 10207,

        [SecurityRoleDescription("CanExportUsers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanExportUsers = 10208,

        [SecurityRoleDescription("CanExportNotAllocatedEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportNotAllocatedEmployees = 10209,

        [SecurityRoleDescription("CanExportJobLogs", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportJobLogs = 10210,

        [SecurityRoleDescription("CanExportVacations", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanExportVacations = 10211,

        [SecurityRoleDescription("CanViewActiveDirectoryGroup", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewActiveDirectoryGroup = 10212,

        [SecurityRoleDescription("CanAddAbsenceType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddAbsenceType = 10213,

        [SecurityRoleDescription("CanEditAbsenceType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditAbsenceType = 10214,

        [SecurityRoleDescription("CanViewAbsenceType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewAbsenceType = 10215,

        [SecurityRoleDescription("CanDeleteAbsenceType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteAbsenceType = 10216,

        [SecurityRoleDescription("CanAddContractType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddContractType = 10217,

        [SecurityRoleDescription("CanEditContractType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditContractType = 10218,

        [SecurityRoleDescription("CanViewContractType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewContractType = 10219,

        [SecurityRoleDescription("CanDeleteContractType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteContractType = 10220,

        [SecurityRoleDescription("CanAddCalendarAbsence", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddCalendarAbsence = 10221,

        [SecurityRoleDescription("CanEditCalendarAbsence", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditCalendarAbsence = 10222,

        [SecurityRoleDescription("CanDeleteCalendarAbsence", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteCalendarAbsence = 10223,

        [SecurityRoleDescription("CanViewCalendarAbsence", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewCalendarAbsence = 10224,

        [SecurityRoleDescription("CanViewImportSource", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewImportSource = 10225,

        [SecurityRoleDescription("CanAddImportSource", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddImportSource = 10226,

        [SecurityRoleDescription("CanEditImportSource", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditImportSource = 10227,

        [SecurityRoleDescription("CanDeleteImportSource", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteImportSource = 10228,

        [SecurityRoleDescription("CanSearchTimeTrackingProjects", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanSearchTimeTrackingProjects = 10229,

        [SecurityRoleDescription("CanViewCompany", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewCompany = 10230,

        [SecurityRoleDescription("CanAddCompany", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddCompany = 10231,

        [SecurityRoleDescription("CanEditCompany", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditCompany = 10232,

        [SecurityRoleDescription("CanDeleteCompany", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteCompany = 10233,

        [SecurityRoleDescription("CanViewOthersTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewOthersTimeReport = 10234,

        [SecurityRoleDescription("CanViewJobTitles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanViewJobTitles = 10235,

        [SecurityRoleDescription("CanAddJobTitles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanAddJobTitles = 10236,

        [SecurityRoleDescription("CanEditJobTitles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanEditJobTitles = 10237,

        [SecurityRoleDescription("CanDeleteJobTitles", typeof(SecurityRoleResources), SecurityRoleModule.SkillManagement)]
        CanDeleteJobTitles = 10238,

        [SecurityRoleDescription("CanViewTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewTimeReport = 10239,

        [SecurityRoleDescription("CanSubmitMyTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanSubmitMyTimeReport = 10240,

        [SecurityRoleDescription("CanImportMyTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanImportMyTimeReport = 10241,

        [SecurityRoleDescription("CanAddPricingEngineer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanAddPricingEngineer = 10242,

        [SecurityRoleDescription("CanEditPricingEngineer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanEditPricingEngineer = 10243,

        [SecurityRoleDescription("CanViewPricingEngineer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanViewPricingEngineer = 10244,

        [SecurityRoleDescription("CanDeletePricingEngineer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanDeletePricingEngineer = 10245,

        [SecurityRoleDescription("CanAddTechnicalInterviewer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanAddTechnicalInterviewer = 10246,

        [SecurityRoleDescription("CanEditTechnicalInterviewer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanEditTechnicalInterviewer = 10247,

        [SecurityRoleDescription("CanViewTechnicalInterviewer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanViewTechnicalInterviewer = 10248,

        [SecurityRoleDescription("CanDeleteTechnicalInterviewer", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanDeleteTechnicalInterviewer = 10249,

        [SecurityRoleDescription("CanViewMyVacationBalance", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewMyVacationBalance = 10250,

        [SecurityRoleDescription("CanGenerateTimeTrackingTasksReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateTimeTrackingTasksReport = 10251,

        [SecurityRoleDescription("CanGenerateTimeTrackingDailyHoursReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateTimeTrackingDailyHoursReport = 10252,

        [SecurityRoleDescription("CanRequestSkillChange", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanRequestSkillChange = 10253,

        [SecurityRoleDescription("CanRequestOvertime", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanRequestOvertime = 10254,

        [SecurityRoleDescription("CanRequestHomeOffice", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanRequestHomeOffice = 10255,

        [SecurityRoleDescription("CanRequestAddingEmployee", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanRequestAddingEmployee = 10256,

        [SecurityRoleDescription("CanRequestAbsence", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanRequestAbsence = 10257,

        [SecurityRoleDescription("CanExportActiveDirectoryGroups", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanExportActiveDirectoryGroups = 10258,

        [SecurityRoleDescription("CanViewTimeTrackingProjectDashboard", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewTimeTrackingProjectDashboard = 10259,

        [SecurityRoleDescription("CanGenerateTimeTrakingProjectSummaryReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateTimeTrakingProjectSummaryReport = 10260,

        [SecurityRoleDescription("CanViewAllActiveDirectoryGroups", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewAllActiveDirectoryGroups,

        [SecurityRoleDescription("CanManageRecommendations", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageRecommendations,

        [SecurityRoleDescription("CanManageWorkflowsOnBehalf", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanManageWorkflowsOnBehalf,

        [SecurityRoleDescription("CanImportEmployee", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanImportEmployee,

        [SecurityRoleDescription("CanViewAllRequests", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanViewAllRequests,

        [SecurityRoleDescription("CanGenerateAbsencesReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateAbsencesReport,

        [SecurityRoleDescription("CanViewTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewTransactions,

        [SecurityRoleDescription("CanExportTimeTrackingReports", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanExportTimeTrackingReports,

        [SecurityRoleDescription("CanExportRequests", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanExportRequests,

        [SecurityRoleDescription("CanExportTimeTrackingDashboard", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanExportTimeTrackingDashboard,

        [SecurityRoleDescription("CanSuperviseTimeReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanSuperviseTimeReport,

        [SecurityRoleDescription("CanViewProjectTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewProjectTransactions,

        [SecurityRoleDescription("CanViewEmploymentTerminationReasons", typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanViewEmploymentTerminationReasons,

        [SecurityRoleDescription("CanAddEmploymentTerminationReasons", typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanAddEmploymentTerminationReasons,

        [SecurityRoleDescription("CanEditEmploymentTerminationReasons", typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanEditEmploymentTerminationReasons,

        [SecurityRoleDescription("CanDeleteEmploymentTerminationReasons", typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanDeleteEmploymentTerminationReasons,

        [SecurityRoleDescription("CanBulkEditProjects", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanBulkEditProjects,

        [SecurityRoleDescription("CanBulkEditEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanBulkEditEmployees,

        [SecurityRoleDescription("CanViewAllTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewAllTransactions,

        [SecurityRoleDescription("CanViewMyTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewMyTransactions,

        [SecurityRoleDescription("CanViewAggregatedProjectTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewAggregatedProjectTransactions,

        [SecurityRoleDescription("CanViewAllAggregatedProjectTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewAllAggregatedProjectTransactions,

        [SecurityRoleDescription("CanViewMyAggregatedProjectTransactions", typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewMyAggregatedProjectTransactions,

        [SecurityRoleDescription("CanRequestEmploymentTermination", typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanRequestEmploymentTermination,

        [SecurityRoleDescription("CanViewActionSet", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanViewActionSet,

        [SecurityRoleDescription("CanAddActionSet", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanAddActionSet,

        [SecurityRoleDescription("CanEditActionSet", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanEditActionSet,

        [SecurityRoleDescription("CanDeleteActionSet", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanDeleteActionSet,

        [SecurityRoleDescription("CanViewSubstitution", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanViewSubstitution,

        [SecurityRoleDescription("CanAddSubstitution", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanAddSubstitution,

        [SecurityRoleDescription("CanEditSubstitution", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanEditSubstitution,

        [SecurityRoleDescription("CanDeleteSubstitution", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanDeleteSubstitution,

        [SecurityRoleDescription("CanGenerateResumeReport", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateResumeReport,

        [SecurityRoleDescription("CanGenerateOwnResumeReport", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateOwnResumeReport,

        [SecurityRoleDescription("CanGenerateTimeTrackingSummaryHoursReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateTimeTrackingSummaryHoursReport,

        [SecurityRoleDescription("CanGenerateAbsencesKbrReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateAbsencesKbrReport,

        [SecurityRoleDescription("CanViewRecommendations", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanViewRecommendations = 20000,

        [SecurityRoleDescription("CanViewUserRegistrationForm", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanViewUserRegistrationForm,

        [SecurityRoleDescription("CanViewEmployeeContractType", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmployeeContractType,

        [SecurityRoleDescription("CanEditMyResume", typeof(SecurityRoleResources), SecurityRoleModule.Resume)]
        CanEditMyResume,

        [SecurityRoleDescription("CanTerminateAllEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanTerminateAllEmployees,

        [SecurityRoleDescription("CanTerminateMyEmployees", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanTerminateMyEmployees,

        [SecurityRoleDescription("CanEditReportOnBehalf", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditReportOnBehalf,

        [SecurityRoleDescription("CanViewEmployeeStatus", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmployeeStatus,

        [SecurityRoleDescription("CanDeleteAbsenceRequests", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteAbsenceRequests,

        [SecurityRoleDescription("CanDeleteHomeOfficeRequests", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteHomeOfficeRequests,

        [SecurityRoleDescription("CanViewUtilizationCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewUtilizationCategories,

        [SecurityRoleDescription("CanAddUtilizationCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddUtilizationCategories,

        [SecurityRoleDescription("CanEditUtilizationCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditUtilizationCategories,

        [SecurityRoleDescription("CanDeleteUtilizationCategories", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteUtilizationCategories,

        [SecurityRoleDescription("CanManageNeededResources", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageNeededResources,

        [SecurityRoleDescription("CanManageNeededResourcesInterviews", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageNeededResourcesInterviews,

        [SecurityRoleDescription("CanRequestAbsenceForOthers", typeof(SecurityRoleResources), SecurityRoleModule.Accounts)]
        CanRequestAbsenceForOthers,

        [SecurityRoleDescription("CanGenerateUtilizationReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanGenerateUtilizationReport,

        [SecurityRoleDescription("CanViewUtilizationReport", typeof(SecurityRoleResources), SecurityRoleModule.Reports)]
        CanViewUtilizationReport,

        [SecurityRoleDescription("CanViewAllCalendarAbsence", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewAllCalendarAbsence,

        [SecurityRoleDescription("CanEditActiveDirectoryGroup", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditActiveDirectoryGroup,

        [SecurityRoleDescription("CanReplaceProjectInReports", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanReplaceProjectInReports,

        [SecurityRoleDescription("CanManageSeniorityLevels", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageSeniorityLevels,

        [SecurityRoleDescription("CanManageResourceRecomendations", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageResourceRecomendations,

        //NOT USED
        [SecurityRoleDescription("CanSendResourceRecomendations", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanSendResourceRecomendations,

        [SecurityRoleDescription("CanManageResourceForms", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageResourceForms,

        [SecurityRoleDescription("CanManageReqionOptions", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageReqionOptions,

        //NOT USED
        [SecurityRoleDescription("CanManageOperationManager", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanManageOperationManager,

        [SecurityRoleDescription("CanManageMainTechnology", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageMainTechnology,

        [SecurityRoleDescription("CanViewTimeReportProjectAttribute", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewTimeReportProjectAttribute,

        [SecurityRoleDescription("CanAddTimeReportProjectAttribute", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddTimeReportProjectAttribute,

        [SecurityRoleDescription("CanEditTimeReportProjectAttribute", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditTimeReportProjectAttribute,

        [SecurityRoleDescription("CanDeleteTimeReportProjectAttribute", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteTimeReportProjectAttribute,

        [SecurityRoleDescription("CanViewTimeReportProjectAttributePicker", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewTimeReportProjectAttributePicker,

        [SecurityRoleDescription("CanViewTimeReportProjectAttributeValue", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewTimeReportProjectAttributeValue,

        [SecurityRoleDescription("CanAddTimeReportProjectAttributeValue", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddTimeReportProjectAttributeValue,

        [SecurityRoleDescription("CanEditTimeReportProjectAttributeValue", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditTimeReportProjectAttributeValue,

        [SecurityRoleDescription("CanDeleteTimeReportProjectAttributeValue", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteTimeReportProjectAttributeValue,

        [SecurityRoleDescription("CanViewTimeReportProjectAttributeValuePicker", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewTimeReportProjectAttributeValuePicker,

        [SecurityRoleDescription("CanManageClientData", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageClientData,

        [SecurityRoleDescription("CanManageBusinessUnitOption", typeof(SecurityRoleResources), SecurityRoleModule.CustomerPortal)]
        CanManageBusinessUnitOption,

        [SecurityRoleDescription("CanViewAllSurveyConducts", typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanViewAllSurveyConducts,

        [SecurityRoleDescription("CanRequestEmployeeDataChange", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanRequestEmployeeDataChange,

        [SecurityRoleDescription("CanViewAllEmployeeVacationBalances", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewAllVacationBalances,

        [SecurityRoleDescription("CanViewMyEmployeeVacationBalances", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewMyEmployeeVacationBalances,

        [SecurityRoleDescription("CanSwitchBetweenStageAndProd", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanSwitchBetweenStageAndProd,

        [SecurityRoleDescription("CanAddAbsenceManually", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddAbsenceManually,

        [SecurityRoleDescription("CanReopenTimeTrackingReportRequest", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanReopenTimeTrackingReportRequest,

        [SecurityRoleDescription("CanRequestEmploymentDataChange", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanRequestEmploymentDataChange,

        [SecurityRoleDescription("CanSendTimeReportsToControlling", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanSendTimeReportsToControlling,

        [SecurityRoleDescription("CanChangeApproversInRequests", typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanChangeApproversInRequests,

        //NOT USED
        [SecurityRoleDescription("CanSendChallengeAnswer", typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanSendChallengeAnswer,

        [SecurityRoleDescription("CanGenerateOrgUnitsReport", typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanGenerateOrgUnitsReport,

        [SecurityRoleDescription("CanEditEmployeesResume", typeof(SecurityRoleResources), SecurityRoleModule.Resume)]
        CanEditEmployeesResume,

        [SecurityRoleDescription("CanExportCompany", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanExportCompany,

        [SecurityRoleDescription("CanModifyApprovedEmploymentTerminationRequest", typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanModifyApprovedEmploymentTerminationRequest,

        [SecurityRoleDescription("CanExportContractType", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanExportContractType,

        [SecurityRoleDescription("CanViewCountry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewCountry,

        [SecurityRoleDescription("CanAddCountry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddCountry,

        [SecurityRoleDescription("CanEditCountry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditCountry,

        [SecurityRoleDescription("CanDeleteCountry", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteCountry,

        [SecurityRoleDescription("CanViewCity", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewCity,

        [SecurityRoleDescription("CanAddCity", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanAddCity,

        [SecurityRoleDescription("CanEditCity", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanEditCity,

        [SecurityRoleDescription("CanDeleteCity", typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanDeleteCity,

        [SecurityRoleDescription("CanSeeAbsenceTaskName", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanSeeAbsenceTaskName,

        [SecurityRoleDescription("CanGenerateVacationBalancesReport", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateVacationBalancesReport,

        [SecurityRoleDescription("CanViewOvertimeBank", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewOvertimeBank,

        [SecurityRoleDescription("CanAddOvertimeBank", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddOvertimeBank,

        [SecurityRoleDescription("CanEditOvertimeBank", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditOvertimeBank,

        [SecurityRoleDescription("CanRequestBusinessTrip", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanRequestBusinessTrip,

        [SecurityRoleDescription("CanFilterOvertimeBankByAllEmployees", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanFilterOvertimeBankByAllEmployees,

        [SecurityRoleDescription("CanDeleteOvertimeBank", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteOvertimeBank,

        [SecurityRoleDescription("CanExportOvertimeBank", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanExportOvertimeBank,

        [SecurityRoleDescription("CanViewProjectPicker", typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewProjectPicker,

        [SecurityRoleDescription("CanViewCurrencies", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewCurrencies,

        [SecurityRoleDescription("CanAddCurrencies", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanAddCurrencies,

        [SecurityRoleDescription("CanEditCurrencies", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanEditCurrencies,

        [SecurityRoleDescription("CanDeleteCurrencies", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanDeleteCurrencies,

        [SecurityRoleDescription("CanGenerateVacationBalanceReportForAllEmployees", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateVacationBalanceReportForAllEmployees,

        [SecurityRoleDescription("CanViewBusinessTrips", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTrips,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanForceDeleteAllBusinessTripRequests),
            typeof(SecurityRoleResources)
            , SecurityRoleModule.BusinessTrip)]
        CanForceDeleteAllBusinessTripRequests,

        [SecurityRoleDescription("CanAddAdvancedPaymentRequest", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanRequestAdvancedPaymentRequest,

        [SecurityRoleDescription("CanViewBusinessTripParticipants", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTripParticipants,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanManageBusinessTripFrontDesk),
            typeof(SecurityRoleResources)
            , SecurityRoleModule.BusinessTrip)]
        CanManageBusinessTripFrontDesk,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanManageBusinessTripArrangements),
            typeof(SecurityRoleResources)
            , SecurityRoleModule.BusinessTrip)]
        CanManageBusinessTripArrangements,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanViewBusinessTripItinerary),
            typeof(SecurityRoleResources)
            , SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTripItinerary,

        [SecurityRoleDescription("CanAddBusinessTripParticipants", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanAddBusinessTripParticipants,

        [SecurityRoleDescription("CanEditBusinessTripParticipants", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanEditBusinessTripParticipants,

        [SecurityRoleDescription("CanDeleteBusinessTripParticipants", typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanDeleteBusinessTripParticipants,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanViewBusinessTripAdvancedPayments),
            typeof(SecurityRoleResources)
            , SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTripAdvancedPayments,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanGenerateEmployeesExceedingWorkingLimitReport),
            typeof(SecurityRoleResources),
            SecurityRoleModule.TimeTracking)]
        CanGenerateEmployeesExceedingWorkingLimitReport,

        [SecurityRoleDescription("CanViewMyTimeReportBeta", typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewMyTimeReportBeta,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddMeetingDetails), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanAddMeetingDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditMeetingDetails), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanEditMeetingDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteMeetingDetails), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanDeleteMeetingDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMeetingDetails), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewMeetingDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewEmployeeMeetings), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewEmployeeMeetings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddMeetingNotes), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanAddMeetingNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteMeetingNotes), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanDeleteMeetingNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditMeetingNotes), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanEditMeetingNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMeetingNotes), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewMeetingNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddFeedback), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanAddFeedback,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteFeedback), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanDeleteFeedback,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditFeedback), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanEditFeedback,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewFeedbacks), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewFeedbacks,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAbsenceSchedule), typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanViewAbsenceSchedule,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewBusinessTripCommunication), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTripCommunication,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyBusinessTrips), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewMyBusinessTrips,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllBusinessTrips), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewAllBusinessTrips,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestOnboarding), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanRequestOnboarding,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAssetTypes), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanViewAssetTypes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddAssetTypes), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanAddAssetTypes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditAssetTypes), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanEditAssetTypes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteAssetTypes), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanDeleteAssetTypes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateWorkingStudentsReport), typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanGenerateWorkingStudentsReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestAssets), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanRequestAssets,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateOnboardingOverviewReport), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanGenerateOnboardingOverviewReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMeetingsOverview), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewMeetingsOverview,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateOnboardingOverviewReport), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanGenerateMyEmployeesOnboardingOverviewReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateOnboardingOverviewReport), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanGenerateAllEmployeesOnboardingOverviewReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestFeedback), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanRequestFeedback,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteFeedbackRequest), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanDeleteFeedbackRequest,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRefreshActionSetDefinitions), typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanRefreshActionSetDefinitions,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateEmploymentChangeReport), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateEmploymentChangeReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateAllocatedAndReportedHoursReport), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateAllocatedAndReportedHoursReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanExportTechnicalInterviewers), typeof(SecurityRoleResources), SecurityRoleModule.Organization)]
        CanExportTechnicalInterviewers,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanLandAtAllocationPage), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanLandAtAllocationPage,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentDataConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewRecruitmentDataConsent,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentDataConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanAddRecruitmentDataConsent,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentDataConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanEditRecruitmentDataConsent,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentDataConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanDeleteRecruitmentDataConsent,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyMeetingsOverview), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewMyMeetingsOverview,

        //NOT USED
        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentApplicant), typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanViewRecruitmentApplicant,

        //NOT USED
        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentApplicant), typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanAddRecruitmentApplicant,

        //NOT USED
        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentApplicant), typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanEditRecruitmentApplicant,

        //NOT USED
        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentApplicant), typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanDeleteRecruitmentApplicant,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewWhistleBlowingList), typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanViewWhistleBlowingList,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentJobOpening), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentJobOpening,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentJobOpening), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentJobOpening,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentJobOpening), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentJobOpening,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentJobOpening), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentJobOpening,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddWhistleBlowMessage), typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanAddWhistleBlowMessage,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentCandidate), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentCandidate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentCandidate), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentCandidate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentCandidate), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentCandidate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentCandidate), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentCandidate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanSetAsReadWhistleBlowMessage), typeof(SecurityRoleResources), SecurityRoleModule.Survey)]
        CanSetAsReadWhistleBlowMessage,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentContactRecord), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentContactRecord,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentContactRecord), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentContactRecord,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentContactRecord), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentContactRecord,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentContactRecord), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentContactRecord,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateBenchAllocationReport), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateBenchAllocationReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewLanguage), typeof(SecurityRoleResources), SecurityRoleModule.Dictionaries)]
        CanViewLanguage,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewBusinessTripSettlements), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTripSettlements,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanManageOwnBusinessTripSettlements), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanManageOwnBusinessTripSettlements,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanManageOthersBusinessTripSettlements), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanManageOthersBusinessTripSettlements,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanExportMeetings), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanExportMeetings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAssignFrontDeskResponsibility), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanAssignFrontDeskResponsibility,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcess), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcess,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentProcess), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentProcess,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentProcess), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentProcess,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentProcess), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentProcess,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentDataOrigin), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentDataOrigin,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentDataOrigin), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentDataOrigin,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentDataOrigin), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentDataOrigin,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentDataOrigin), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentDataOrigin,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAssignLineManagerInOnboardingRequest), typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanAssignLineManagerInOnboardingRequest,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewSalesCustomers), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanViewSalesCustomers,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditSalesCustomers), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanEditSalesCustomers,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteSalesCustomers), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanDeleteSalesCustomers,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddSalesCustomers), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanAddSalesCustomers,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllJobPostings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditCompensationParameters), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditCompensationParameters,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanManageBusinessTripParticipantPersonalData), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanManageBusinessTripParticipantPersonalData,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecommendingPersons), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecommendingPersons,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecommendingPersons), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecommendingPersons,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecommendingPersons), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecommendingPersons,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecommendingPersons), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecommendingPersons,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDataOwners), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewDataOwners,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddDataOwners), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanAddDataOwners,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditDataOwners), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanEditDataOwners,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteDataOwners), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanDeleteDataOwners,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllCandidates), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllCandidates,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllRecruitmentProcesses), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllRecruitmentProcesses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyJobPostings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewMyJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyCandidates), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewMyCandidates,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyRecruitmentProcesses), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewMyRecruitmentProcesses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDataActivities), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewDataActivities,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddDataActivities), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanAddDataActivities,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditDataActivities), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanEditDataActivities,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteDataActivities), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanDeleteDataActivities,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewBusinessTripAcceptanceConditions), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewBusinessTripAcceptanceConditions,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanForceDeleteAllBusinessTripSettlementRequests),
            typeof(SecurityRoleResources),
            SecurityRoleModule.BusinessTrip)]
        CanForceDeleteAllBusinessTripSettlementRequests,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddDailyAllowance), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanAddDailyAllowance,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditDailyAllowance), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanEditDailyAllowance,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDailyAllowance), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewDailyAllowance,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteDailyAllowance), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanDeleteDailyAllowance,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanImportDailyAllowance), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanImportDailyAllowance,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanExportDailyAllowance), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanExportDailyAllowance,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewJobApplications), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewJobApplications,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddJobApplications), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanAddJobApplications,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditJobApplications), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanEditJobApplications,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteJobApplications), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanDeleteJobApplications,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentTechnicalReviews), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentTechnicalReviews,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentTechnicalReviews), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentTechnicalReviews,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentTechnicalReviews), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentTechnicalReviews,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentTechnicalReviews), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentTechnicalReviews,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateInvoiceCalculationsReport), typeof(SecurityRoleResources), SecurityRoleModule.Reporting)]
        CanGenerateInvoiceCalculationsReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanApproveJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanApproveJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateBusinessTripSettlementSummaryReport), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanGenerateBusinessTripSettlementSummaryReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanCloseJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanCloseJobOpenings,

        [SecurityRoleDescription(
            nameof(SecurityRoleResources.CanManageAllBusinessTripArrangements),
            typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanManageAllBusinessTripArrangements,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateBusinessTripSettlementReport), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanGenerateBusinessTripSettlementReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestBusinessTripOnBehalf), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanRequestBusinessTripOnBehalf,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllBusinessTripRequests), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewAllBusinessTripRequests,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllBusinessTripSettlementRequests), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewAllBusinessTripSettlementRequests,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllAdvancedPaymentRequests), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanViewAllAdvancedPaymentRequests,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyInboundEmails), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewMyInboundEmails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllInboundEmails), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllInboundEmails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentCandidateFile), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentCandidateFile,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentCandidateFile), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentCandidateFile,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentCandidateFile), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentCandidateFile,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentCandidateFile), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentCandidateFile,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAssignRecruitmentInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAssignRecruitmentInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewEmployeePersonalData), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmployeePersonalData = 20249,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditEmployeePersonalData), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanEditEmployeePersonalData = 20250,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanCloseRecruitmentInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanCloseRecruitmentInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddBonuses), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanAddBonuses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditBonuses), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanEditBonuses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewBonuses), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanViewBonuses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyEmployeesBonuses), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanViewMyEmployeesBonuses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllEmployeesBonuses), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanViewAllEmployeesBonuses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteBonuses), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanDeleteBonuses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAnonymizeCandidates), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAnonymizeCandidates,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanChangeSettlementRequestControllingStatus), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanChangeSettlementRequestControllingStatus,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentRecommendingPersonContactRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentRecommendingPersonContactRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentRecommendingPersonContactRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentRecommendingPersonContactRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentRecommendingPersonContactRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentRecommendingPersonContactRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteRecruitmentRecommendingPersonContactRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanDeleteRecruitmentRecommendingPersonContactRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAbsenceRequest), typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanViewAbsenceRequest,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateMyBusinessTripSettlementReport), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanGenerateMyBusinessTripSettlementReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateAllBusinessTripSettlementReport), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanGenerateAllBusinessTripSettlementReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllTechnicalReviews), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllTechnicalReviews,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyTechnicalReviews), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewMyTechnicalReviews,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanWorkWithRecruitmentData), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanWorkWithRecruitmentData,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyEmployeesMeetings), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewMyEmployeeMeetings = 20270,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllEmployeeMeetings), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanViewAllEmployeeMeetings = 20271,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAnonymizeRecommendingPersons), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAnonymizeRecommendingPersons,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateAccountingReport), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanGenerateAccountingReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanCloseRecruitmentProcesses), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanCloseRecruitmentProcesses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyRecruitmentTeamRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyRecruitmentTeamRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewPublicRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewPublicRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllRecruitmentProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllRecruitmentProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyRecruitmentProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewMyRecruitmentProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanReopenProcessSteps), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanReopenProcessSteps,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewStepDetailsWhenDone), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewStepDetailsWhenDone,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewBasicCandidateData), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewBasicCandidateData,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewManagerialCandidateData), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewManagerialCandidateData,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllCandidateData), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllCandidateData,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanPutRecruitmentProcessOnHold), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanPutRecruitmentProcessOnHold,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanTakeRecruitmentProcessOffHold), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanTakeRecruitmentProcessOffHold,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestTaxDeductibleCost), typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanRequestTaxDeductibleCost,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanCreateEmptySettlementRequest), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanCreateEmptySettlementRequest,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanBeRecruitmentDataWatcher), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanBeRecruitmentDataWatcher,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllJobOpeningsInOrgUnit), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllJobOpeningsInOrgUnit,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllJobOpeningsBySameLineManager), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllJobOpeningsBySameLineManager,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDraftJobOpeningsFromOtherAuthors), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewDraftJobOpeningsFromOtherAuthors,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewInboundEmailsSendForResearch), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewInboundEmailsSendForResearch,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewHighlyConfidentialJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewHighlyConfidentialJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewHighlyConfidentialCandidates), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewHighlyConfidentialCandidates,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewEmployeeSalaryData), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewEmployeeSalaryData,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanChangeRecruitmentProcessStepDecisionMaker), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanChangeRecruitmentProcessStepDecisionMaker,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcessOfferedSalary), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcessOfferedSalary,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcessFinalSalary), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcessFinalSalary,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateTechnicalVerificationReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateTechnicalVerificationReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyRecruitmentTeamRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyRecruitmentTeamRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyRecruitmentTeamRecruitmentRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyRecruitmentTeamRecruitmentRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyRecruitmentTeamRecruitmentRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyRecruitmentTeamRecruitmentRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyHiringManagerRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyHiringManagerRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyHiringManagerRecruitmentRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyHiringManagerRecruitmentRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyHiringManagerRecruitmentRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyHiringManagerRecruitmentRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyHiringManagerRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyHiringManagerRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyTechnicalReviewerRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyTechnicalReviewerRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyTechnicalReviewerRecruitmentRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyTechnicalReviewerRecruitmentRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyTechnicalReviewerRecruitmentRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyTechnicalReviewerRecruitmentRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewOnlyTechnicalReviewerRecruitmentJobOpeningNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewOnlyTechnicalReviewerRecruitmentJobOpeningNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewPublicRecruitmentRecruitmentProcessNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewPublicRecruitmentRecruitmentProcessNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewPublicRecruitmentRecommendingPersonNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewPublicRecruitmentRecommendingPersonNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewPublicRecruitmentCandidateNotes), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewPublicRecruitmentCandidateNotes,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllRecruitmentProcessStepsFromMyRecruitmentProcesses), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAllRecruitmentProcessStepsFromMyRecruitmentProcesses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentProcessCoordinators), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentProcessCoordinators,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditJobOpeningCoordinators), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditJobOpeningCoordinators,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditCandidateCoordinators), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditCandidateCoordinators,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecommendingPersonCoordinators), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecommendingPersonCoordinators,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentProcessOfferDetails), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentProcessOfferDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditRecruitmentProcessFinalDetails), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanEditRecruitmentProcessFinalDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewGlobalRate), typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanViewGlobalRate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddGlobalRate), typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanAddGlobalRate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditGlobalRate), typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanEditGlobalRate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteGlobalRate), typeof(SecurityRoleResources), SecurityRoleModule.Finance)]
        CanDeleteGlobalRate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcessStepsTooEarlyForHiringManager), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcessStepsTooEarlyForHiringManager,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanExportJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanExportJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewJobOpeningsOtherThanActiveOrClosed), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewJobOpeningsOtherThanActiveOrClosed,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecruitmentStepViaDropdown), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecruitmentStepViaDropdown,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddAnyRecruitmentStep), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddAnyRecruitmentStep,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAdvancedRecruitmentMenuItems), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewAdvancedRecruitmentMenuItems,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateDeductibleCostReport), typeof(SecurityRoleResources), SecurityRoleModule.Reporting)]
        CanGenerateDeductibleCostReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddRecommendingPersonConsents), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanAddRecommendingPersonConsents,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanShareEmployeesResume), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanShareEmployeesResume,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentProcessesByMyRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentProcessesByMyRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDownloadSharedEmployeeResume), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanDownloadSharedEmployeeResume,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentProcessesByAnyOwner), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentProcessesByAnyOwner,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentProcessStepsByMyRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentProcessStepsByMyRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentProcessStepsByMyProcess), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentProcessStepsByMyProcess,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentProcessStepsByAnyOwner), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentProcessStepsByAnyOwner,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateWeeklyAllocationDataReport), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanGenerateWeeklyAllocationDataReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentProcessStepsByType), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentProcessStepsByType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewEmployeesResume), typeof(SecurityRoleResources), SecurityRoleModule.Resume)]
        CanViewEmployeesResume,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestBonus), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestBonus,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateTechnicalReviewSummaryReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateTechnicalReviewSummaryReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDashboards), typeof(SecurityRoleResources), SecurityRoleModule.Default)]
        CanViewDashboards,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanPutJobOpeningOnHold), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanPutJobOpeningOnHold,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanTakeJobOpeningOffHold), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanTakeJobOpeningOffHold,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateBonusRequestReport), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanGenerateBonusRequestReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanSendInvoiceNotification), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanSendInvoiceNotification,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteBusinessTripArrangements), typeof(SecurityRoleResources), SecurityRoleModule.BusinessTrip)]
        CanDeleteBusinessTripArrangements,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanCloneJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanCloneJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanReOpenJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanReOpenJobOpenings,
        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestExpense), typeof(SecurityRoleResources), SecurityRoleModule.TimeTracking)]
        CanRequestExpense,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentProcessProgressReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentProcessProgressReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentProcessProgressReportForAllEmployees), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentProcessProgressReportForAllEmployees,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentProcessProgressReportForMyEmployees), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentProcessProgressReportForMyEmployees,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateLeavingsReport), typeof(SecurityRoleResources), SecurityRoleModule.PeopleManagement)]
        CanGenerateLeavingsReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentRejectedCandidateReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentRejectedCandidateReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentReportsForAllEmployees), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentReportsForAllEmployees,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentReportsForMyEmployees), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentReportsForMyEmployees,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentProcessFunnelReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentProcessFunnelReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentHiredCandidateReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentHiredCandidateReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewMyEmployeeContractType), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewMyEmployeeContractType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllEmployeeContractType), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewAllEmployeeContractType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentProcessDurationPerRecruiterReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentProcessDurationPerRecruiterReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewTechnicalVerificationDetails), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewTechnicalVerificationDetails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanUnassignRecruitmentInboundEmail), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanUnassignRecruitmentInboundEmail,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentProcessDurationPerDecisionMakerReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentProcessDurationPerDecisionMakerReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentJobOpeningCurrentStepReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentJobOpeningCurrentStepReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentReportForMyJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentReportForMyJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentReportForAllJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentReportForAllJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentReportForMyHiringManagerJobOpenings), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentReportForMyHiringManagerJobOpenings,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewKpiDefinitions), typeof(SecurityRoleResources), SecurityRoleModule.KPI)]
        CanViewKpiDefinitions,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddKpiDefinitions), typeof(SecurityRoleResources), SecurityRoleModule.KPI)]
        CanAddKpiDefinitions,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditKpiDefinitions), typeof(SecurityRoleResources), SecurityRoleModule.KPI)]
        CanEditKpiDefinitions,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteKpiDefinitions), typeof(SecurityRoleResources), SecurityRoleModule.KPI)]
        CanDeleteKpiDefinitions,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanPerformKpiReports), typeof(SecurityRoleResources), SecurityRoleModule.KPI)]
        CanPerformKpiReports,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentJobOpeningStepFunnelReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentJobOpeningStepFunnelReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanCloneRecruitmentProcess), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanCloneRecruitmentProcess,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterCandidateContactRecordByMyRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterCandidateContactRecordByMyRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterCandidateContactRecordByAnyOwner), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterCandidateContactRecordByAnyOwner,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterJobApplicationRecordByMyRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterJobApplicationRecordByMyRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterJobApplicationRecordByAnyOwner), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterJobApplicationRecordByAnyOwner,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterInboundEmailByMyRecords), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterInboundEmailByMyRecords,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterInboundEmailByAnyOwner), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterInboundEmailByAnyOwner,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateRecruitmentJobOpeningStatusReport), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanGenerateRecruitmentJobOpeningStatusReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFilterRecruitmentInboundEmailNonAssigned), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFilterRecruitmentInboundEmailNonAssigned,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddOrEditNoteForLastOwnMeeting), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanAddOrEditNoteForLastOwnMeeting,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanReopenRecruitmentProcesses), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanReopenRecruitmentProcesses,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewSalesInquiry), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanViewSalesInquiry,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddSalesInquiry), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanAddSalesInquiry,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditSalesInquiry), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanEditSalesInquiry,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteSalesInquiry), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanDeleteSalesInquiry,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanFavorCandidate), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanFavorCandidate,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanSendInboundEmailForResearch), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanSendInboundEmailForResearch,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewSalesOfferType), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanViewSalesOfferType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddSalesOfferType), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanAddSalesOfferType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditSalesOfferType), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanEditSalesOfferType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteSalesOfferType), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanDeleteSalesOfferType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanGenerateIntiveIncPaymentNotificationReport), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanGenerateIntiveIncPaymentNotificationReport,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanMergeDataOwners), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanMergeDataOwners,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanSendMeetingInvitation), typeof(SecurityRoleResources), SecurityRoleModule.MeetMe)]
        CanSendMeetingInvitation,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanExportBonus), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanExportBonus,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestOncallBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestOncallBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestShortTermBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestShortTermBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestReallocationBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestReallocationBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestRecommendationBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestRecommendationBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestBenefitsBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestBenefitsBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestRegularBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestRegularBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestHolidayBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestHolidayBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanRequestBonusType), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanRequestBonusType,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanChangeSalesInquiryStatus), typeof(SecurityRoleResources), SecurityRoleModule.Sales)]
        CanChangeSalesInquiryStatus,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewJobOpeningHistory), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewJobOpeningHistory,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewRecruitmentProcessHistory), typeof(SecurityRoleResources), SecurityRoleModule.Recruitment)]
        CanViewRecruitmentProcessHistory,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewContractorInvoices), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanViewContractorInvoices,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanChangeInvoiceStatus), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanChangeInvoiceStatus,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteApprovedBonusRequest), typeof(SecurityRoleResources), SecurityRoleModule.Workflows)]
        CanDeleteApprovedBonusRequest,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllocationRequestChangeDemand), typeof(SecurityRoleResources), SecurityRoleModule.Allocations)]
        CanViewAllocationRequestChangeDemand,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanSetColorCategoryToInboundEmails), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanSetColorCategoryToInboundEmails,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanUploadInvoice), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanUploadInvoice,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanUploadInvoiceOnBehalf), typeof(SecurityRoleResources), SecurityRoleModule.Compensation)]
        CanUploadInvoiceOnBehalf,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDataProcessingAgreement), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewDataProcessingAgreement,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllDataProcessingAgreement), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewAllDataProcessingAgreement,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanAddDataProcessingAgreement), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanAddDataProcessingAgreement,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanEditDataProcessingAgreement), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanEditDataProcessingAgreement,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanDeleteDataProcessingAgreement), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanDeleteDataProcessingAgreement,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewDataProcessingAgreementConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewDataProcessingAgreementConsent,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanViewAllDataProcessingAgreementConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanViewAllDataProcessingAgreementConsent,

        [SecurityRoleDescription(nameof(SecurityRoleResources.CanApproveDataProcessingAgreementConsent), typeof(SecurityRoleResources), SecurityRoleModule.GDPR)]
        CanApproveDataProcessingAgreementConsent,
    }
}
