﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ApprovalGroupMode
    {
        [DescriptionLocalized("And", typeof(GeneralResources))]
        And = 0,

        [DescriptionLocalized("Or", typeof(GeneralResources))]
        Or = 1
    }
}
