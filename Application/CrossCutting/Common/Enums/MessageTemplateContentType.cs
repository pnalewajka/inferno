﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum MessageTemplateContentType
    {
        [DescriptionLocalized("PlainText", typeof(MessageTemplateResources))]
        PlainText,

        [DescriptionLocalized("Html", typeof(MessageTemplateResources))]
        Html
    }
}