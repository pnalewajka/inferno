﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    /// <summary>
    /// Authorization provider type
    /// </summary>
    public enum AuthorizationProviderType
    {
        Atomic = 1,
        ActiveDirectory = 2,
        GooglePlus = 3,
        Facebook = 4, 
        Twitter = 5,
    }
}