﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum SecurityRoleModule
    {
        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Default), typeof(SecurityRoleModuleResources))]
        Default,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Accounts), typeof(SecurityRoleModuleResources))]
        Accounts,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Scheduling), typeof(SecurityRoleModuleResources))]
        Scheduling,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Configuration), typeof(SecurityRoleModuleResources))]
        Configuration,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Notifications), typeof(SecurityRoleModuleResources))]
        Notifications,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Organization), typeof(SecurityRoleModuleResources))]
        Organization,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.EventSourcing), typeof(SecurityRoleModuleResources))]
        EventSourcing,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Reporting), typeof(SecurityRoleModuleResources))]
        Reporting,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Allocations), typeof(SecurityRoleModuleResources))]
        Allocations,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.SkillManagement), typeof(SecurityRoleModuleResources))]
        SkillManagement,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Dictionaries), typeof(SecurityRoleModuleResources))]
        Dictionaries,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Reports), typeof(SecurityRoleModuleResources))]
        Reports,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Survey), typeof(SecurityRoleModuleResources))]
        Survey,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Workflows), typeof(SecurityRoleModuleResources))]
        Workflows,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.TimeTracking), typeof(SecurityRoleModuleResources))]
        TimeTracking,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.CustomerPortal), typeof(SecurityRoleModuleResources))]
        CustomerPortal,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Finance), typeof(SecurityRoleModuleResources))]
        Finance,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.PeopleManagement), typeof(SecurityRoleModuleResources))]
        PeopleManagement,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Resume), typeof(SecurityRoleModuleResources))]
        Resume,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.BusinessTrip), typeof(SecurityRoleModuleResources))]
        BusinessTrip,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.MeetMe), typeof(SecurityRoleModuleResources))]
        MeetMe,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.GDPR), typeof(SecurityRoleModuleResources))]
        GDPR,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Recruitment), typeof(SecurityRoleModuleResources))]
        Recruitment,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Sales), typeof(SecurityRoleModuleResources))]
        Sales,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.Compensation), typeof(SecurityRoleModuleResources))]
        Compensation,

        [DescriptionLocalized(nameof(SecurityRoleModuleResources.KPI), typeof(SecurityRoleModuleResources))]
        KPI
    }
}
