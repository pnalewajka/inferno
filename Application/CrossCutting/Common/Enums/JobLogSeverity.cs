﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum JobLogSeverity
    {
        [DescriptionLocalized("Unknown", typeof(JobLogSeverityResources))]
        Unknown = 0,

        [DescriptionLocalized("Fatal", typeof(JobLogSeverityResources))]
        Fatal = 1,

        [DescriptionLocalized("Error", typeof(JobLogSeverityResources))]
        Error = 2,

        [DescriptionLocalized("Warning", typeof(JobLogSeverityResources))]
        Warning = 3,
        
        Information = 4,
        
        Debug = 5,
        
        Trace = 6,
    }
}