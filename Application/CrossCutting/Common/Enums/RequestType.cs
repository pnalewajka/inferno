﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [SqlEnum(DatabaseSchemes.Workflows)]
    public enum RequestType
    {
        [DescriptionLocalized(nameof(RequestTypeResources.SkillChangeRequest), typeof(Resources.RequestTypeResources))]
        SkillChangeRequest = 0,

        [DescriptionLocalized(nameof(RequestTypeResources.AddEmployeeRequest), typeof(Resources.RequestTypeResources))]
        AddEmployeeRequest = 1,

        [DescriptionLocalized(nameof(RequestTypeResources.AbsenceRequest), typeof(Resources.RequestTypeResources))]
        AbsenceRequest = 2,

        [DescriptionLocalized(nameof(RequestTypeResources.AddHomeOfficeRequest), typeof(Resources.RequestTypeResources))]
        AddHomeOfficeRequest = 3,

        [DescriptionLocalized(nameof(RequestTypeResources.OvertimeRequest), typeof(Resources.RequestTypeResources))]
        OvertimeRequest = 4,

        [DescriptionLocalized(nameof(RequestTypeResources.ProjectTimeReportApprovalRequest), typeof(Resources.RequestTypeResources))]
        ProjectTimeReportApprovalRequest = 5,

        [DescriptionLocalized(nameof(RequestTypeResources.EmploymentTerminationRequest), typeof(Resources.RequestTypeResources))]
        EmploymentTerminationRequest = 6,

        [DescriptionLocalized(nameof(RequestTypeResources.EmployeeDataChangeRequest), typeof(Resources.RequestTypeResources))]
        EmployeeDataChangeRequest = 7,

        [DescriptionLocalized(nameof(RequestTypeResources.ReopenTimeTrackingReportRequest), typeof(Resources.RequestTypeResources))]
        ReopenTimeTrackingReportRequest = 8,

        [DescriptionLocalized(nameof(RequestTypeResources.EmploymentDataChangeRequest), typeof(Resources.RequestTypeResources))]
        EmploymentDataChangeRequest = 9,

        [DescriptionLocalized(nameof(RequestTypeResources.BusinessTripRequest), typeof(Resources.RequestTypeResources))]
        BusinessTripRequest = 10,

        [DescriptionLocalized(nameof(RequestTypeResources.AdvancedPaymentRequest), typeof(Resources.RequestTypeResources))]
        AdvancedPaymentRequest = 11,

        [DescriptionLocalized(nameof(RequestTypeResources.OnboardingRequest), typeof(Resources.RequestTypeResources))]
        OnboardingRequest = 12,

        [DescriptionLocalized(nameof(RequestTypeResources.AssetsRequest), typeof(Resources.RequestTypeResources))]
        AssetsRequest = 13,

        [DescriptionLocalized(nameof(RequestTypeResources.FeedbackRequest), typeof(Resources.RequestTypeResources))]
        FeedbackRequest = 14,

        [DescriptionLocalized(nameof(RequestTypeResources.BusinessTripSettlementRequest), typeof(RequestTypeResources))]
        BusinessTripSettlementRequest = 15,

        [DescriptionLocalized(nameof(RequestTypeResources.TaxDeductibleCostRequest), typeof(RequestTypeResources))]
        TaxDeductibleCostRequest = 16,

        [DescriptionLocalized(nameof(RequestTypeResources.BonusRequest), typeof(RequestTypeResources))]
        BonusRequest = 17,

        [DescriptionLocalized(nameof(RequestTypeResources.ExpenseRequest), typeof(RequestTypeResources))]
        ExpenseRequest = 18,
    }

    [SqlEnum(DatabaseSchemes.Workflows)]
    public enum RequestTypeGroup
    {
        [DescriptionLocalized(nameof(RequestTypeResources.Manager), typeof(Resources.RequestTypeResources))]
        Manager = 1,

        [DescriptionLocalized(nameof(RequestTypeResources.TimeTracking), typeof(Resources.RequestTypeResources))]
        TimeTracking = 2,

        [DescriptionLocalized(nameof(RequestTypeResources.Travel), typeof(Resources.RequestTypeResources))]
        Travel = 3,

        [DescriptionLocalized(nameof(RequestTypeResources.Other), typeof(Resources.RequestTypeResources))]
        Other = 4
    }
}