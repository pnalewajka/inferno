﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ReferenceType
    {
        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeCandidate), typeof(ReferenceTypeResources))]
        Candidate = 1,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeEmployee), typeof(ReferenceTypeResources))]
        Employee = 2,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeRecommendingPerson), typeof(ReferenceTypeResources))]
        RecommendingPerson = 3,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeUser), typeof(ReferenceTypeResources))]
        User = 4,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeCandidateContact), typeof(ReferenceTypeResources))]
        CandidateContact = 5,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeRecommendingPersonContact), typeof(ReferenceTypeResources))]
        RecommendingPersonContact = 6,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeCandidateNote), typeof(ReferenceTypeResources))]
        CandidateNote = 7,

        [DescriptionLocalized(nameof(ReferenceTypeResources.ReferenceTypeRecommendingPersonNote), typeof(ReferenceTypeResources))]
        RecommendingPersonNote = 8,

        [DescriptionLocalized(nameof(ReferenceTypeResources.DataOwner), typeof(ReferenceTypeResources))]
        DataOwner = 10,

        [DescriptionLocalized(nameof(ReferenceTypeResources.DataConsent), typeof(ReferenceTypeResources))]
        DataConsent = 11,

        [DescriptionLocalized(nameof(ReferenceTypeResources.DataActivity), typeof(ReferenceTypeResources))]
        DataActivity = 12,

        [DescriptionLocalized(nameof(ReferenceTypeResources.JobApplication), typeof(ReferenceTypeResources))]
        JobApplication = 13,

        [DescriptionLocalized(nameof(ReferenceTypeResources.CandidateFile), typeof(ReferenceTypeResources))]
        CandidateFile = 14,

        [DescriptionLocalized(nameof(ReferenceTypeResources.TechnicalReview), typeof(ReferenceTypeResources))]
        TechnicalReview = 15,

        [DescriptionLocalized(nameof(ReferenceTypeResources.RecruitmentProcess), typeof(ReferenceTypeResources))]
        RecruitmentProcess = 16,

        [DescriptionLocalized(nameof(ReferenceTypeResources.RecruitmentProcessStep), typeof(ReferenceTypeResources))]
        RecruitmentProcessStep = 17,
    }
}