﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ActionUrlType
    {
        ListProjects,
        ViewProject,
        EditProject,
        ListAllocations,
    }
}
