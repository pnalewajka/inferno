﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum BusinessTripPickerMode
    {
        All = 0,

        OnlyFinished = 1,

        NotAccounted = 2
    }
}
