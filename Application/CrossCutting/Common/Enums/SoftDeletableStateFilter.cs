﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum SoftDeletableStateFilter
    {
        [DescriptionLocalized("Existing", typeof(SoftDeletableStateFilterResources))]
        Existing,

        [DescriptionLocalized("Deleted", typeof(SoftDeletableStateFilterResources))]
        Deleted,

        [DescriptionLocalized("All", typeof(SoftDeletableStateFilterResources))]
        All
    }
}
