using System;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [Serializable]
    [DataContract]
    [SqlEnum(DatabaseSchemes.CustomerPortal)]
    public enum RecommendationStatus
    {
        [EnumMember]
        [DescriptionLocalized("StatusNoneDescription", typeof(Resources.RecommendationResources))]
        None = 0,

        [DescriptionLocalized("StatusAddedDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Added = 1,

        [DescriptionLocalized("StatusSentDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Sent = 2,

        [DescriptionLocalized("StatusInvitedDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Invited = 3,

        [DescriptionLocalized("StatusRejectedDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Rejected = 4,

        [DescriptionLocalized("StatusWithdrawnDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Withdrawn = 5,

        [DescriptionLocalized("StatusArchivedDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Archived = 6,

        [DescriptionLocalized("StatusApprovedDescription", typeof(Resources.RecommendationResources))]
        [EnumMember]
        Approved = 11,
    }
}
