﻿using System;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    [Serializable]
    [DataContract]
    [SqlEnum(DatabaseSchemes.Scheduling)]
    public enum JobStatus
    {
        [EnumMember]
        Starting = 1,

        [EnumMember]
        Finished = 2,

        [EnumMember]
        Failed = 3,

        [EnumMember]
        Aborted = 4,
    }
}
