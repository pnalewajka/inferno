﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum JobOpeningHistoryEntryType
    {
        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeCreate), typeof(JobOpeningHistoryEntryTypeResources))]
        Created = 1,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeUpdate), typeof(JobOpeningHistoryEntryTypeResources))]
        Updated = 2,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeRemove), typeof(JobOpeningHistoryEntryTypeResources))]
        Removed = 3,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeWatch), typeof(JobOpeningHistoryEntryTypeResources))]
        Watched = 4,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeUnwatch), typeof(JobOpeningHistoryEntryTypeResources))]
        Unwatched = 5,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeClose), typeof(JobOpeningHistoryEntryTypeResources))]
        Closed = 5,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeReopen), typeof(JobOpeningHistoryEntryTypeResources))]
        Reopened = 6,        

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypePutOnHold), typeof(JobOpeningHistoryEntryTypeResources))]
        PutOnHold = 7,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeTakeOffHold), typeof(JobOpeningHistoryEntryTypeResources))]
        TookOffHold = 8,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeAddNote), typeof(JobOpeningHistoryEntryTypeResources))]
        AddedNote = 9,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeClone), typeof(JobOpeningHistoryEntryTypeResources))]
        Cloned = 10,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeReject), typeof(JobOpeningHistoryEntryTypeResources))]
        Rejected = 11,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeSendForApproval), typeof(JobOpeningHistoryEntryTypeResources))]
        SentForApproval = 12,

        [DescriptionLocalized(nameof(JobOpeningHistoryEntryTypeResources.ActivityTypeApprove), typeof(JobOpeningHistoryEntryTypeResources))]
        Approved = 13,
    }
}