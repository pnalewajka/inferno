﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum CalendarAppointmentType
    {
        Meeting = 0,
        Absence = 1,
        HomeOffice = 2
    }
}