﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ConsentDecisionType
    {
        Revoked = 1,
        Granted = 2
    }
}