﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum TemporaryDocumentState
    {
        /// <summary>
        /// File upload was started (new file status)
        /// </summary>
        Started = 1,

        /// <summary>
        /// File upload failed for some reason
        /// </summary>
        Failed = 2,

        /// <summary>
        /// File was successfully uploaded
        /// </summary>
        Completed = 3
    }
}
