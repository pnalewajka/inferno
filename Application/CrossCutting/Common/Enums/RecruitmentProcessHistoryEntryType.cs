﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum RecruitmentProcessHistoryEntryType
    {
        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeCreate), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Created = 1,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeUpdate), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Updated = 2,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeRemove), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Removed = 3,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeWatch), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Watched = 4,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeUnwatch), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Unwatched = 5,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeClose), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Closed = 6,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeReopen), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Reopened = 7,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypePutOnHold), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        PutOnHold = 8,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeTakeOffHold), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        TookOffHold = 9,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeAddNote), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        AddedNote = 10,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeClone), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        Cloned = 11,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeCreateStep), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        CreatedStep = 12,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeUpdateStep), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        UpdatedStep = 13,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeRemoveStep), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        RemovedStep = 14,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeReopenStep), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        ReopenedStep = 15,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeCandidateReject), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        CandidateRejected = 16,

        [DescriptionLocalized(nameof(RecruitmentProcessHistoryEntryTypeResources.ActivityTypeCandidateApprove), typeof(RecruitmentProcessHistoryEntryTypeResources))]
        CandidateApproved = 17,
    }
}