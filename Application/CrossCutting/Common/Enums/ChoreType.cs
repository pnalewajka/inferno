﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ChoreType
    {
        [DescriptionLocalized(nameof(ChoreResources.DefunctJob), typeof(ChoreResources))]
        DefunctJob,

        [DescriptionLocalized(nameof(ChoreResources.PendingRequests), typeof(ChoreResources))]
        PendingRequests,

        [DescriptionLocalized(nameof(ChoreResources.PendingSurveys), typeof(ChoreResources))]
        PendingSurveys,

        [DescriptionLocalized(nameof(ChoreResources.EmailDeliveryFailed), typeof(ChoreResources))]
        EmailDeliveryFailed,

        [DescriptionLocalized(nameof(ChoreResources.FailedBusinessEvents), typeof(ChoreResources))]
        FailedBusinessEvents,

        [DescriptionLocalized(nameof(ChoreResources.UnsettledBusinessTrip), typeof(ChoreResources))]
        UnsettledBusinessTrip,

        [DescriptionLocalized(nameof(ChoreResources.OutdatedResume), typeof(ChoreResources))]
        OutdatedResume,

        [DescriptionLocalized(nameof(ChoreResources.OwnTimeReportReminder), typeof(ChoreResources))]
        OwnTimeReportReminder,

        [DescriptionLocalized(nameof(ChoreResources.OverdueMeetings), typeof(ChoreResources))]
        OverdueMeetings,

        [DescriptionLocalized(nameof(ChoreResources.MyEmployeesTimeReportReminder), typeof(ChoreResources))]
        MyEmployeesTimeReportReminder,

        [DescriptionLocalized(nameof(ChoreResources.InboundEmails), typeof(ChoreResources))]
        InboundEmails,

        [DescriptionLocalized(nameof(ChoreResources.PendingDataProcessingAgreementConsent), typeof(ChoreResources))]
        PendingDataProcessingAgreementConsent,

        [DescriptionLocalized(nameof(ChoreResources.UserAvatarMissing), typeof(ChoreResources))]
        UserAvatarMissing,
    }
}
