﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;

namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum ActivityType
    {
        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeCreatedRecord), typeof(ActivityTypeResources))]
        CreatedRecord = 0,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeUpdatedRecord), typeof(ActivityTypeResources))]
        UpdatedRecord = 1,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeListedRecord), typeof(ActivityTypeResources))]
        ListedRecord = 2,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeViewedRecord), typeof(ActivityTypeResources))]
        ViewedRecord = 3,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeCreatedDocument), typeof(ActivityTypeResources))]
        CreatedDocument = 4,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeViewedDocument), typeof(ActivityTypeResources))]
        ViewedDocument = 5,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeUpdatedDocument), typeof(ActivityTypeResources))]
        UpdatedDocument = 6,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeRemovedDocument), typeof(ActivityTypeResources))]
        RemovedDocument = 7,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeRemovedRecord), typeof(ActivityTypeResources))]
        RemovedRecord = 8,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeDataConsentExpired), typeof(ActivityTypeResources))]
        DataConsentExpired = 9,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeAnonymizedRecord), typeof(ActivityTypeResources))]
        AnonymizeRecord = 10,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeSharedDocumentWithCustomer), typeof(ActivityTypeResources))]
        SharedDocumentWithCustomer = 11,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeWatch), typeof(ActivityTypeResources))]
        Watch = 12,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeUnwatch), typeof(ActivityTypeResources))]
        Unwatch = 13,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeResumeGenerated), typeof(ActivityTypeResources))]
        ResumeGenerated = 14,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeResumeShareTokenGenerated), typeof(ActivityTypeResources))]
        ResumeShareTokenGenerated = 15,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityUnjustifiedDownloadAttempt), typeof(ActivityTypeResources))]
        UnjustifiedDownloadAttempt = 16,

        [DescriptionLocalized(nameof(ActivityTypeResources.ActivityTypeMergeDataOwner), typeof(ActivityTypeResources))]
        MergeDataOwner = 17,
    }
}