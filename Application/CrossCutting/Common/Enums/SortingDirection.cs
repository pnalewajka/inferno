namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum SortingDirection
    {
        Unspecified = 0,
        Ascending,
        Descending
    }
}