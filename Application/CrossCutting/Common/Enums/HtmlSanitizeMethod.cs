﻿namespace Smt.Atomic.CrossCutting.Common.Enums
{
    public enum HtmlSanitizeMethod
    {
        /// <summary>
        /// AntiXSS 4.3.0, default
        /// </summary>
        Default = 0,

        /// <summary>
        /// HtmlSanitizer with default config
        /// </summary>
        Basic = 1,

        /// <summary>
        /// HtmlSanitizer with custom config
        /// </summary>
        Strict = 2,

        /// <summary>
        /// Remove all tags
        /// </summary>
        PlainText = 3,
    }
}
