﻿using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class UnzippedFile : IArchivable
    {
        public byte[] Content { get; set; }

        public string FileName { get; set; }

        public string MimeType { get; set; }
    }
}
