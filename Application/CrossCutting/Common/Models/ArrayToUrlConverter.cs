﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    /// <summary>
    /// Converter used for serializing/deserializing arrays as string
    /// Used for filters view models to enforce long[] properties to be stored in the form acceptable in url
    /// </summary>
    public class ArrayToUrlConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.IsArray;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var newValue = serializer.Deserialize(reader);

            var elementType = objectType.GetElementType();
            var values = ((string)newValue).Split(',');
            var array = Array.CreateInstance(elementType, values.Length);

            for (int i = 0; i < values.Length; i++)
            {
                array.SetValue(Convert.ChangeType(values[i], elementType), i);
            }

            return array;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var enumerable = (IEnumerable)value;

            var result = string.Join(",", enumerable.OfType<object>().Select(v => v.ToString()));
            serializer.Serialize(writer, result);
        }
    }
}
