﻿using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class Optional<TValue> : IOption
    {
        public Optional()
        {
        }

        public Optional(TValue value)
        {
            IsSet = true;
            Value = value;
        }

        public Optional(bool isSet, TValue value)
        {
            IsSet = isSet;
            Value = value;
        }

        public bool IsSet { get; set; }

        public TValue Value { get; set; }

        public static implicit operator Optional<TValue>(TValue value)
        {
            return new Optional<TValue>(value);
        }
    }
}
