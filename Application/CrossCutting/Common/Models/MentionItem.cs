﻿namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class MentionItem
    {
        public long Id { get; set; }

        public string Label { get; set; }

        public string SourceName { get; set; }
    }
}
