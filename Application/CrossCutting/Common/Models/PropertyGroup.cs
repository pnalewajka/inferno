﻿using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class PropertyGroup
    {
        public IList<string> PropertyNames { get; set; }

        public PropertyGroup()
        {
            PropertyNames = new List<string>();
        }
    }
}
