﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class PropertyPath
    {
        private static readonly Regex MethodPattern = new Regex(@"(?<Name>[A-Za-z_][A-Za-z0-9_]+){1}?(?<Parameters>\([A-Za-z0-9_ ,]*\)){1}?", RegexOptions.Compiled);
        private static readonly Regex IndexerPattern = new Regex(@"(?<Name>[A-Za-z_][A-Za-z0-9_]+){1}?(?<Index>\[[0-9]+\]){1}?", RegexOptions.Compiled);

        public string[] Elements { get; private set; }

        public PropertyPath(IEnumerable<string> propertyPathElements)
        {
            if (propertyPathElements == null || propertyPathElements.Count() < 1)
            {
                throw new ArgumentNullException(nameof(propertyPathElements));
            }

            Elements = propertyPathElements.ToArray();
        }

        public PropertyPath([NotNull]string propertyPath)
        {
            if (string.IsNullOrEmpty(propertyPath))
            {
                throw new ArgumentNullException(nameof(propertyPath));
            }

            Elements = propertyPath.Split('.');
        }

        public TResult ResolveValueOrDefault<TResult>([NotNull] object sourceObject) where TResult : class
        {
            if (sourceObject == null)
            {
                throw new ArgumentNullException(nameof(sourceObject));
            }

            foreach (var propertyName in Elements)
            {
                if (MethodPattern.IsMatch(propertyName))
                {
                    sourceObject = ResolveMethodValueOrDefault(sourceObject, propertyName);
                }
                else if (IndexerPattern.IsMatch(propertyName))
                {
                    sourceObject = ResolveIndexerValueOrDefault(sourceObject, propertyName);
                }
                else
                {
                    sourceObject = ResolvePropertyValueOrDefault(sourceObject, propertyName);
                }
            }

            return (TResult)sourceObject;
        }

        public Type ResolveType([NotNull] object sourceObject)
        {
            if (sourceObject == null)
            {
                throw new ArgumentNullException(nameof(sourceObject));
            }

            return ResolveType(sourceObject.GetType());
        }

        public Type ResolveType([NotNull] Type sourceType)
        {
            if (sourceType == null)
            {
                throw new ArgumentNullException(nameof(sourceType));
            }

            foreach (var propertyName in Elements)
            {
                if (MethodPattern.IsMatch(propertyName))
                {
                    sourceType = ResolveMethodPropertyType(sourceType, propertyName);
                }
                else if (IndexerPattern.IsMatch(propertyName))
                {
                    sourceType = ResolveIndexerPropertyType(sourceType, propertyName);
                }
                else
                {
                    sourceType = ResolveObjectPropertyType(sourceType, propertyName);
                }
            }

            return sourceType;
        }

        private Type ResolveObjectPropertyType([NotNull] Type sourceType, [NotNull] string propertyName)
        {
            var propertyType = PropertyHelper.GetPropertyType(propertyName, sourceType);

            if (propertyType == null)
            {
                throw new PropertyNotFoundException($"PropertyName '{propertyName}' from propertyPath '{string.Join(propertyName, '.')}' not found.");
            }

            return propertyType;
        }

        private Type ResolveMethodPropertyType([NotNull] Type sourceType, [NotNull] string propertyName)
        {
            var methodName = MethodPattern.Match(propertyName).Groups["Name"].Value;
            var method = sourceType.GetMethod(methodName);

            if (method == null)
            {
                throw new Exception($"Method '{methodName}' from propertyPath '{string.Join(propertyName, '.')}' not found.");
            }

            return method.ReturnType;
        }

        private Type ResolveIndexerPropertyType([NotNull]Type sourceType, [NotNull] string propertyName)
        {
            var collectionMatch = IndexerPattern.Match(propertyName).Groups;
            var collectionName = collectionMatch["Name"].Value;
            var collectionType = PropertyHelper.GetPropertyType(collectionName, sourceType);

            if (collectionType == null)
            {
                throw new Exception($"Collection '{collectionName}' from propertyPath '{string.Join(propertyName, '.')}' not found.");
            }

            return collectionType.GetGenericArguments().First();
        }

        private object ResolvePropertyValueOrDefault(object sourceObject, [NotNull] string propertyName)
        {
            if (sourceObject == null)
            {
                return null;
            }

            var property = sourceObject.GetType().GetProperty(propertyName);

            if (property == null)
            {
                throw new PropertyNotFoundException($"PropertyName '{propertyName}' from propertyPath '{string.Join(propertyName, '.')}' not found.");
            }

            return PropertyHelper.GetPropertyValue<object>(sourceObject, propertyName);
        }

        private object ResolveIndexerValueOrDefault(object sourceObject, [NotNull] string propertyName)
        {
            if (sourceObject == null)
            {
                return null;
            }

            var collectionMatch = IndexerPattern.Match(propertyName).Groups;
            var collectionName = collectionMatch["Name"].Value;
            var collection = PropertyHelper.GetPropertyValue<IList>(sourceObject, collectionName);

            if (collection == null)
            {
                throw new Exception($"Collection '{collectionName}' from propertyPath '{string.Join(propertyName, '.')}' not found.");
            }

            var index = collectionMatch["Index"].Value.Trim(new char[] { '[', ']' });
            var indexValue = int.Parse(index);

            if (collection.Count <= indexValue)
            {
                throw new Exception($"Index '{indexValue}' of collection '{collectionName}' from propertyPath '{string.Join(propertyName, '.')}' is out of range.");
            }

            return collection[indexValue];
        }

        private object ResolveMethodValueOrDefault(object sourceObject, [NotNull] string propertyName)
        {
            if (sourceObject == null)
            {
                return null;
            }

            var methodName = MethodPattern.Match(propertyName).Groups["Name"].Value;
            var method = (sourceObject).GetType().GetMethod(methodName);

            if (method == null)
            {
                throw new Exception($"Method '{methodName}' from propertyPath '{string.Join(propertyName, '.')}' not found.");
            }

            return method.Invoke(sourceObject, null);
        }
    }
}
