﻿using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public abstract class LocalizedStringBase
    {
        /// <summary>
        /// Default language key for target culture 
        /// </summary>
        public const string DefaultLanguageKey = "";

        /// <summary>
        /// Returns representation in specified culture
        /// </summary>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public string ToString(CultureInfo cultureInfo)
        {
            return LocalizedStringHelper.GetLocalizedString(this, cultureInfo);
        }

        /// <summary>
        /// Returns representation in current UI culture
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToString(CultureInfo.CurrentUICulture);
        }
    }
}
