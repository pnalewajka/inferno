﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using System.Web.Mvc;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class ArchivableFileContentResult
        : FileContentResult, IArchivable
    {
        public ArchivableFileContentResult(byte[] fileContents, string contentType)
            : base(fileContents, contentType)
        {
        }

        public byte[] Content => FileContents;

        public string FileName => FileDownloadName;
    }
}
