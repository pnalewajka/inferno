﻿using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Resources;
using System;
using System.Globalization;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class LocalizedString : LocalizedStringBase
    {
        [TargetCulture(DefaultLanguageKey)]
        [DescriptionLocalized("EnglishLanguageCode", typeof(LocalizedStringResource))]
        public string English { get; set; }

        [TargetCulture("pl")]
        [DescriptionLocalized("PolishLanguageCode", typeof(LocalizedStringResource))]
        public string Polish { get; set; }

        public LocalizedString()
        {
            English = string.Empty;
            Polish = string.Empty;
        }

        public LocalizedString([NotNull] DescriptionLocalizedAttribute attr)
        {
            var enCulture = CultureInfo.CreateSpecificCulture("en-US");
            var plCulture = CultureInfo.CreateSpecificCulture("pl-PL");

            English = attr.GetDescription(enCulture);
            Polish = attr.GetDescription(plCulture);
        }

        public LocalizedString([NotNull] Type resourceType, [NotNull] string resourceKey, params object[] args)
        {
            var resourceManager = ResourceManagerHelper.GetResourceManager(resourceType);
            var enCulture = CultureInfo.CreateSpecificCulture("en-US");
            var plCulture = CultureInfo.CreateSpecificCulture("pl-PL");
            
            using (CultureInfoHelper.SetCurrentCulture(enCulture))
            using (CultureInfoHelper.SetCurrentUICulture(enCulture))
            {
                var resource = resourceManager.GetString(resourceKey, enCulture);
                English = string.Format(resource, args);
            }

            using (CultureInfoHelper.SetCurrentCulture(plCulture))
            using (CultureInfoHelper.SetCurrentUICulture(plCulture))
            {
                var resource = resourceManager.GetString(resourceKey, plCulture);
                Polish = string.Format(resource, args);
            }
        }

        public LocalizedString(Func<string> factory)
        {
            var enCulture = CultureInfo.CreateSpecificCulture("en-US");
            var plCulture = CultureInfo.CreateSpecificCulture("pl-PL");
            
            using (CultureInfoHelper.SetCurrentCulture(enCulture))
            using (CultureInfoHelper.SetCurrentUICulture(enCulture))
            {
                English = factory();
            }

            using (CultureInfoHelper.SetCurrentCulture(plCulture))
            using (CultureInfoHelper.SetCurrentUICulture(plCulture))
            {
                Polish = factory();
            }
        }
    }
}
