﻿using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.CrossCutting.Common.Models
{
    public class CurrencyAmount
    {
        private readonly int _formattingPrecision;

        private CurrencyAmount(int precision)
        {
            _formattingPrecision = precision;
        }

        public decimal Amount { get; set; }

        public string CurrencyIsoCode { get; set; }

        public override string ToString()
        {
            var mask = "0.".PadRight(2 + _formattingPrecision, '0');

            return $"{Amount.ToInvariantString(mask)} {CurrencyIsoCode}";
        }

        public static CurrencyAmount CreateRateAmount(decimal value, string currencyIsoCode) => new CurrencyAmount(4)
        {
            Amount = value,
            CurrencyIsoCode = currencyIsoCode
        };

        public static CurrencyAmount CreateValueAmount(decimal value, string currencyIsoCode) => new CurrencyAmount(2)
        {
            Amount = value,
            CurrencyIsoCode = currencyIsoCode
        };

        public static CurrencyAmount Parse(string value, int precision = 2)
        {
            var splittedValue = value.Split(new char[] { ' ' });

            return new CurrencyAmount(precision)
            {
                Amount = decimal.Parse(splittedValue[0], NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture),
                CurrencyIsoCode = splittedValue[1],
            };
        }
    }
}
