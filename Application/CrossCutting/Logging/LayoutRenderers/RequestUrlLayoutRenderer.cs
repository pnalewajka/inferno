﻿using System.Text;
using NLog;
using NLog.LayoutRenderers;
using Smt.Atomic.CrossCutting.Security.Interfaces;

namespace Smt.Atomic.CrossCutting.Logging.LayoutRenderers
{
    [LayoutRenderer("request-url")]
    public class RequestUrlLayoutRenderer : HttpContextLayoutRenderer
    {
        protected override void Append(
            IHttpCurrentContextDataService httpCurrentContext,
            StringBuilder builder,
            LogEventInfo logEvent)
        {
            builder.Append(httpCurrentContext.Request.Url.AbsoluteUri);
        }
    }
}