﻿using System.Text;
using NLog;
using NLog.LayoutRenderers;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Logging.LayoutRenderers
{
    [LayoutRenderer("session-id")]
    public class SessionIdLayoutRenderer : HttpContextLayoutRenderer
    {
        protected override void Append(
            IHttpCurrentContextDataService httpCurrentContext,
            StringBuilder builder,
            LogEventInfo logEvent)
        {
            var sessionId = AuthenticationHelper.GetSessionIdFromAuthorizationCookie(httpCurrentContext.Request);
            builder.Append(sessionId ?? NullText);
        }
    }
}