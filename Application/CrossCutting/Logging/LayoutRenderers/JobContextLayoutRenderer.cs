﻿using System;
using System.Text;
using System.Threading;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Logging.LayoutRenderers
{
    [LayoutRenderer("jobContext")]
    public class JobContextLayoutRenderer : LayoutRenderer
    {
        private readonly Lazy<IJobExecutionContextService> _jobExecutionContextService =
            new Lazy<IJobExecutionContextService>(
                () => ReflectionHelper.TryResolveInterface<IJobExecutionContextService>());

        [DefaultParameter]
        public string Item { get; set; }

        public long? JobDefinitionId() => JobExecutionContext()?.JobDefinitionId;

        public string SchedulerName() => JobExecutionContext()?.SchedulerName;

        public string Host() => JobExecutionContext()?.Host;

        public string Pipeline() => JobExecutionContext()?.Pipeline;

        public Guid? ExecutionId() => JobExecutionContext()?.ExecutionId;

        protected JobExecutionContext JobExecutionContext() => _jobExecutionContextService.Value.GetContext(Thread.CurrentThread);

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            if (Item == null)
            {
                throw new ArgumentException(@"You must specify item. Example: ${jobContext:item=JobDefinitionId}");
            }

            switch (Item)
            {
                case "JobDefinitionId":
                {
                    builder.Append(JobDefinitionId());
                    break;
                }

                case "SchedulerName":
                {
                    builder.Append(SchedulerName());
                    break;
                }

                case "Host":
                {
                    builder.Append(Host());
                    break;
                }

                case "Pipeline":
                {
                    builder.Append(Pipeline());
                    break;
                }

                case "ExecutionId":
                {
                    builder.Append(ExecutionId());
                    break;
                }

                default:
                {
                    throw new ArgumentException($"Argument `{Item}` not found");
                }
            }
        }
    }
}