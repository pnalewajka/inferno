﻿using System;
using System.Text;
using NLog;
using NLog.LayoutRenderers;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Interfaces;

namespace Smt.Atomic.CrossCutting.Logging.LayoutRenderers
{
    public abstract class HttpContextLayoutRenderer : LayoutRenderer
    {
        protected const string NullText = "[null]";

        private IHttpCurrentContextDataService _httpCurrentContextDataService;

        private IHttpCurrentContextDataService HttpCurrentContext
        {
            get
            {
                if (_httpCurrentContextDataService == null)
                {
                    _httpCurrentContextDataService = ReflectionHelper.TryResolveInterface<IHttpCurrentContextDataService>();
                }

                return _httpCurrentContextDataService;
            }
        }

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var httpCurrentContext = HttpCurrentContext;

            if (httpCurrentContext == null)
            {
                builder.Append("[no-http-context]");

                return;
            }

            try
            {
                Append(httpCurrentContext, builder, logEvent);
            }
            catch (Exception ex)
            {
                builder.AppendFormat("[error:{0}]", ex.Message);
                throw;
            }
        }

        protected abstract void Append(
            IHttpCurrentContextDataService httpCurrentContext,
            StringBuilder builder,
            LogEventInfo logEvent);
    }
}
