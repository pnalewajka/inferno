﻿using System.Text;
using NLog;
using NLog.LayoutRenderers;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Logging.LayoutRenderers
{
    [LayoutRenderer("activity-id")]
    public class ActivityIdLayoutRenderer : HttpContextLayoutRenderer
    {
        protected override void Append(
            IHttpCurrentContextDataService httpCurrentContext,
            StringBuilder builder,
            LogEventInfo logEvent)
        {
            var activityId = ActivityTrackingHelper.GetActivityIdFromActivityTrackingCookie(httpCurrentContext.Request);
            builder.Append(activityId ?? NullText);
        }
    }
}
