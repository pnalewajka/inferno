﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.NtlmAuthentication;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Security.Services;

namespace Smt.Atomic.CrossCutting.Security
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                    container.Register(Component.For<IPrincipalProvider>().ImplementedBy<ThreadPrincipalProvider>().LifestylePerWebRequest());
                    container.Register(Component.For<IHandShakeStateCacheService>().ImplementedBy<HandShakeStateCacheService>().LifestyleSingleton());
                    container.Register(Component.For<INtlmAuthenticationService>().ImplementedBy<NtlmAuthenticationService>().LifestyleTransient());
                    container.Register(Classes.FromThisAssembly().BasedOn<IAuthorizationProvider>().WithServiceBase().LifestyleTransient());
                    container.Register(Component.For<IHttpCurrentContextDataService, HttpCurrentContextDataService>().ImplementedBy<HttpCurrentContextDataService>());
                    break;

                case ContainerType.WebServices:
                    container.Register(Component.For<IPrincipalProvider>()
                        .ImplementedBy<ThreadPrincipalProvider>()
                        .LifestylePerWcfOperation());
                    break;

                case ContainerType.JobScheduler:
                    container.Register(Component.For<IPrincipalProvider>().ImplementedBy<ThreadPrincipalProvider>().LifestylePerThread());
                    break;
            }
        }
    }
}