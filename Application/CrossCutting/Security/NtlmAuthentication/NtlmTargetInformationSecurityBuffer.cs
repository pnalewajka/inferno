using System;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public class NtlmTargetInformationSecurityBuffer
    {
        public short Type { get; private set; }

        public short Length { get; private set; }

        public string Content { get; private set; }


        public NtlmTargetInformationSecurityBuffer(byte[] body, int offset)
        {
            Type = BitConverter.ToInt16(body, offset);
            Length = BitConverter.ToInt16(body, offset + 2);

            if (Length > 0)
            {
                var stringData = ByteArrayHelper.Splice(body, offset + 4, Length);
                Content = Encoding.Unicode.GetString(stringData);
            }

        }
    }
}