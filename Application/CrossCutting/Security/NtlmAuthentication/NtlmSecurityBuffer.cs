﻿using System;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Security buffer (for data extration from NTLM message)
    /// </summary>
    public class NtlmSecurityBuffer
    {
        /// <summary>
        /// Content length
        /// </summary>
        public short Length { get; private set; }

        /// <summary>
        /// Allocated space
        /// </summary>
        public short AllocatedSpace { get; private set; }

        /// <summary>
        /// Message bytes offset
        /// </summary>
        public int Offset { get; private set; }

        /// <summary>
        /// Extracted buffer
        /// </summary>
        public byte[] Buffer { get; private set; }

        public NtlmSecurityBuffer(byte[] body, int offset)
        {
            Length = BitConverter.ToInt16(body, offset);
            AllocatedSpace = BitConverter.ToInt16(body, offset + 2);
            Offset = BitConverter.ToInt32(body, offset + 4);

            if (Length > 0 && Offset > 0)
            {
                Buffer = ByteArrayHelper.Splice(body, Offset, Length);
            }
        }

        public string GetUnicodeString()
        {
            return Length > 0 ? Encoding.Unicode.GetString(Buffer) : null;
        }
    }
}