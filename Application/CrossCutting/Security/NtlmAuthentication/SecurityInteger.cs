using System.Runtime.InteropServices;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Class extracted from https://github.com/pysco68/Pysco68.Owin.Authentication.Ntlm
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SecurityInteger
    {
        public uint LowPart;
        public int HighPart;
    }
}