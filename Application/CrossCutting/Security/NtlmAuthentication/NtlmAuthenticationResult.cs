﻿using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public class NtlmAuthenticationResult
    {
        public NtlmAuthenticationResultAction Action { get; set; }
        public int StatusCode { get; set; }
        public Dictionary<string,string> Headers { get; set; }
        public string UserName { get; set; }
        public string DomainName { get; set; }
        public bool IsAuthenticated { get; set; }

        public NtlmAuthenticationResult()
        {
            Headers = new Dictionary<string, string>();
        }
    }
}