using System;
using System.Runtime.InteropServices;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Class extracted from https://github.com/pysco68/Pysco68.Owin.Authentication.Ntlm
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SecurityHandle
    {
        public IntPtr LowPart;
        public IntPtr HighPart;

        /// <summary>
        /// Resets all internal pointers to default value
        /// </summary>
        public void Reset()
        {
            LowPart = HighPart = IntPtr.Zero;
        }
    }
}