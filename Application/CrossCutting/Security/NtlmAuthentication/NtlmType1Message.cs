﻿using System;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public class NtlmType1Message : NtlmTypeMessage
    {
        public NtlmSecurityBuffer SuppliedDomain { get; private set; }
        public NtlmSecurityBuffer SuppliedWorkstation { get; private set; }
        public NtlmOsVersion OsVersion { get; private set; }

        public NtlmType1Message(byte[] body) : base(body)
        {
            if (MessageType != NtlmMessageType.Type1)
            {
                throw new Exception($"Incorrect message type: {MessageType}, should be {NtlmMessageType.Type1}");
            }

            Options = (NtlmMessageOptions)BitConverter.ToInt32(body, 12);
            SuppliedDomain = new NtlmSecurityBuffer(body, 16);
            SuppliedWorkstation = new NtlmSecurityBuffer(body, 24);
            OsVersion = new NtlmOsVersion(body, 32);
        }
    }
}
