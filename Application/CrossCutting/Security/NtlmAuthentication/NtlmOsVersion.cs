﻿using System;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Ntlm OS version from NTLM message
    /// </summary>
    public class NtlmOsVersion
    {
        public byte MajorVersionNumber { get; private set; }
        public byte MinorVersionNumber { get; private set; }
        public short BuildNumber { get; private set; }

        public NtlmOsVersion(byte[] body, int offset)
        {
            MajorVersionNumber = body[offset];
            MinorVersionNumber = body[offset + 1];
            BuildNumber = BitConverter.ToInt16(body, offset + 2);
        }

    }
}