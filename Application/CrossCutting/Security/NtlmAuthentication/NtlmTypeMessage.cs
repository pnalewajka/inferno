using System;
using System.Linq;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Ntlm challenge documentation
    /// http://davenport.sourceforge.net/ntlm.html
    /// </summary>
    public class NtlmTypeMessage
    {
        private const string NtlmSignature = "NTLMSSP";
        private const string AuthenticationMethod = "NTLM ";

        public string Signature { get; private set; }
        public NtlmMessageType MessageType { get; private set; }
        public NtlmMessageOptions Options { get; protected set; }

        public byte[] Token { get; protected set; }

        protected NtlmTypeMessage(byte[] body)
        {
            if (body == null || body.Length < 16)
            {
                //malformed ntlm, don't do anything with it
                return;
            }

            Signature = Encoding.ASCII.GetString(body, 0, 7);
            MessageType = (NtlmMessageType)BitConverter.ToInt32(body, 8);

            Token = body;

            if (!IsNtlmMessage)
            {
                throw new Exception("Malformed message body");
            }
        }

        public bool IsNtlmMessage => Signature == NtlmSignature;

        public static NtlmTypeMessage ParseNtlmHeader([NotNull] string authorizationHeader)
        {
            if (string.IsNullOrWhiteSpace(authorizationHeader))
            {
                throw new ArgumentException("authorizationHeader");
            }

            if (!authorizationHeader.StartsWith(authorizationHeader))
            {
                throw new Exception("Not a NTLM authentication response");
            }

            var messageBody = Convert.FromBase64String(authorizationHeader.Substring(AuthenticationMethod.Length));

            return new NtlmTypeMessage(messageBody);
        }

        public static NtlmTypeMessage ParseMessage([NotNull] string authorizationHeader)
        {
            var messageBody = Convert.FromBase64String(authorizationHeader.Substring(AuthenticationMethod.Length));
            var header = ParseNtlmHeader(authorizationHeader);

            if (!header.IsNtlmMessage)
            {
                return null;
            }

            switch (header.MessageType)
            {
                case NtlmMessageType.Type1:
                    return new NtlmType1Message(messageBody);

                case NtlmMessageType.Type2:
                    return new NtlmType2Message(messageBody);

                case NtlmMessageType.Type3:
                    return new NtlmType3Message(messageBody);
            }

            return null;
        }

        protected byte[] GetTokenPart(byte[] token, int start, int length)
        {
            return token.Skip(start).Take(length).ToArray();
        }
    }
}