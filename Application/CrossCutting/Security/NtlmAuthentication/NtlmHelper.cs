﻿using System;
using System.Security.Principal;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public class HandShakeState : IDisposable
    {
        private SecurityHandle _credentials;
        private SecurityHandle _context;

        public bool TryAcquireServerChallenge(ref byte[] message)
        {
            var clientToken = new SecurityBufferDesciption(message);
            var serverToken = new SecurityBufferDesciption(NtlmSettings.MaximumTokenSize);

            try
            {
                var lifetime = new SecurityInteger();

                var result = InterOp.AcquireCredentialsHandle(
                    null,
                    "NTLM",
                    NtlmSettings.SecurityCredentialsInbound,
                    IntPtr.Zero,
                    IntPtr.Zero,
                    0,
                    IntPtr.Zero,
                    ref _credentials,
                    ref lifetime);

                if (result != NtlmSettings.SuccessfulResult)
                {
                    // Credentials acquire operation failed.
                    return false;
                }

                uint contextAttributes;

                result = InterOp.AcceptSecurityContext(
                    ref _credentials,                       // [in] handle to the credentials
                    IntPtr.Zero,                                // [in/out] handle of partially formed context.  Always NULL the first time through
                    ref clientToken,                            // [in] pointer to the input buffers
                    NtlmSettings.StandardContextAttributes,           // [in] required context attributes
                    NtlmSettings.SecurityNativeDataRepresentation,    // [in] data representation on the target
                    out _context,                           // [in/out] receives the new context handle    
                    out serverToken,                            // [in/out] pointer to the output buffers
                    out contextAttributes,                      // [out] receives the context attributes        
                    out lifetime);                              // [out] receives the life span of the security context

                if (result != NtlmSettings.IntermediateResult)
                {
                    // Client challenge issue operation failed.
                    return false;
                }
            }
            finally
            {
                message = serverToken.GetBytes();
                clientToken.Dispose();
                serverToken.Dispose();
            }

            return true;
        }

        public IdentityResult IsClientResponseValid(byte[] message)
        {
            var clientToken = new SecurityBufferDesciption(message);
            var serverToken = new SecurityBufferDesciption(NtlmSettings.MaximumTokenSize);
            var securityContextHandle = IntPtr.Zero;

            try
            {
                uint contextAttributes;
                var lifetime = new SecurityInteger();

                var result = InterOp.AcceptSecurityContext(
                    ref _credentials,                       // [in] handle to the credentials
                    ref _context,                           // [in/out] handle of partially formed context.  Always NULL the first time through
                    ref clientToken,                            // [in] pointer to the input buffers
                    NtlmSettings.StandardContextAttributes,           // [in] required context attributes
                    NtlmSettings.SecurityNativeDataRepresentation,    // [in] data representation on the target
                    out _context,                           // [in/out] receives the new context handle    
                    out serverToken,                            // [in/out] pointer to the output buffers
                    out contextAttributes,                      // [out] receives the context attributes        
                    out lifetime);                              // [out] receives the life span of the security context

                if (result != NtlmSettings.SuccessfulResult)
                {
                    return new IdentityResult(false);
                }

                if (InterOp.QuerySecurityContextToken(ref _context, ref securityContextHandle) != 0)
                {
                    return new IdentityResult(false);
                }

                using (var identity = new WindowsIdentity(securityContextHandle))
                {
                    //WARNING!!: IE NTLM causes translation to fail, due to domain configuration

                    //var groups = identity
                    //    .Groups
                    //    .Where(g => g.IsValidTargetType(typeof (NTAccount)))
                    //    .Select(g => (NTAccount) g.Translate(typeof (NTAccount)))
                    //    .Select(n => n.Value).ToList();

                    return new IdentityResult(true, identity.IsAuthenticated, identity.Name, null);
                }
            }
            finally
            {
                clientToken.Dispose();
                serverToken.Dispose();

                InterOp.CloseHandle(securityContextHandle);
            }

        }

        ~HandShakeState()
        {
            DisposeBuffers();
        }

        private void DisposeBuffers()
        {
            if (_credentials.LowPart != IntPtr.Zero || _credentials.HighPart != IntPtr.Zero)
            {
                InterOp.FreeCredentialsHandle(ref _credentials);
            }
        }

        public void Dispose()
        {
            DisposeBuffers();

            _credentials.Reset();
            _context.Reset();
        }
    }
}
