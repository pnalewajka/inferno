using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Result of NTLM authentication
    /// </summary>
    public class IdentityResult
    {
        /// <summary>
        /// Is processing result valid?
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// Is user authenticated?
        /// </summary>
        public bool IsAuthenticated { get; private set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// Name of domain against which user was authenticate
        /// </summary>
        public string DomainName { get; private set; }

        /// <summary>
        /// Resolved group names from WindowsIdentity
        /// </summary>
        public List<string> Groups { get; private set; }

        public IdentityResult(bool isValid)
        {
            IsValid = isValid;
        }

        public IdentityResult(bool isValid, bool isAuthenticated, string name, List<string> groups) : this(isValid)
        {
            IsAuthenticated = isAuthenticated;
            Groups = groups;

            var nameParts = name.Split('\\');
            DomainName = nameParts[0];
            UserName = nameParts[1];
        }
    }
}