﻿namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Ntlm message type (step within NTLM handshake)
    /// Description available at http://davenport.sourceforge.net/ntlm.html
    /// </summary>
    public enum NtlmMessageType
    {
        /// <summary>
        /// Client issued message, type 1, options exchange
        /// </summary>
        Type1 = 1,

        /// <summary>
        /// Server issued message, type 2, random bytes for HMAC encoding
        /// </summary>
        Type2 = 2,

        /// <summary>
        /// Client issued message, type 3, final authetication results
        /// </summary>
        Type3 = 3
    }
}