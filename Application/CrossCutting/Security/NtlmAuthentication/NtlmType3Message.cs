﻿using System;
using System.Text;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public class NtlmType3Message : NtlmTypeMessage
    {
        public byte[] LmResponse { get; private set; }
        public byte[] NtlmResponse { get; private set; }
        public string TargetName { get; private set; }
        public string UserName { get; private set; }
        public string WorkstationName { get; private set; }
        public byte[] SessionKey { get; private set; }

        public NtlmType3Message(byte[] body)
            : base(body)
        {
            if (MessageType != NtlmMessageType.Type3)
            {
                throw new Exception($"Incorrect message type: {MessageType}, should be {NtlmMessageType.Type3}");
            }

            LmResponse = new NtlmSecurityBuffer(body, 12).Buffer;
            NtlmResponse = new NtlmSecurityBuffer(body, 20).Buffer;
            TargetName = new NtlmSecurityBuffer(body, 28).GetUnicodeString();
            UserName = new NtlmSecurityBuffer(body, 36).GetUnicodeString();
            WorkstationName = new NtlmSecurityBuffer(body, 44).GetUnicodeString();
            SessionKey = new NtlmSecurityBuffer(body, 52).Buffer;
            Options = (NtlmMessageOptions)BitConverter.ToInt32(body, 60);
        }
    }
}