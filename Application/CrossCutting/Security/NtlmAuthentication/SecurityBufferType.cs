﻿namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Class extracted from https://github.com/pysco68/Pysco68.Owin.Authentication.Ntlm
    /// </summary>
    public enum SecurityBufferType
    {
        SecbufferVersion = 0,
        SecbufferEmpty = 0,
        SecbufferData = 1,
        SecbufferToken = 2
    }
}
