﻿using System;
using System.Runtime.InteropServices;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Class extracted from https://github.com/pysco68/Pysco68.Owin.Authentication.Ntlm
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SecurityBufferDesciption : IDisposable
    {
        public int ulVersion;
        public int cBuffers;
        public IntPtr pBuffers; //Point to SecBuffer

        public SecurityBufferDesciption(int bufferSize)
        {
            ulVersion = (int) SecurityBufferType.SecbufferVersion;
            cBuffers = 1;
            var thisSecBuffer = new SecurityBuffer(bufferSize);
            pBuffers = Marshal.AllocHGlobal(Marshal.SizeOf(thisSecBuffer));
            Marshal.StructureToPtr(thisSecBuffer, pBuffers, false);
        }

        public SecurityBufferDesciption(byte[] secBufferBytes)
        {
            ulVersion = (int) SecurityBufferType.SecbufferVersion;
            cBuffers = 1;
            var thisSecBuffer = new SecurityBuffer(secBufferBytes);
            pBuffers = Marshal.AllocHGlobal(Marshal.SizeOf(thisSecBuffer));
            Marshal.StructureToPtr(thisSecBuffer, pBuffers, false);
        }

        public SecurityBufferDesciption(BufferWrapper[] secBufferBytesArray)
        {
            if (secBufferBytesArray == null || secBufferBytesArray.Length == 0)
            {
                throw new ArgumentException("secBufferBytesArray cannot be null or 0 length");
            }

            ulVersion = (int) SecurityBufferType.SecbufferVersion;
            cBuffers = secBufferBytesArray.Length;

            //Allocate memory for SecBuffer Array....
            pBuffers = Marshal.AllocHGlobal(Marshal.SizeOf(typeof (Buffer))*cBuffers);

            for (var index = 0; index < secBufferBytesArray.Length; index++)
            {
                //Super hack: Now allocate memory for the individual SecBuffers
                //and just copy the bit values to the SecBuffer array!!!
                var thisSecBuffer = new SecurityBuffer(secBufferBytesArray[index].Buffer,
                    secBufferBytesArray[index].BufferType);

                //We will write out bits in the following order:
                //int cbBuffer;
                //int BufferType;
                //pvBuffer;
                //Note that we won't be releasing the memory allocated by ThisSecBuffer until we
                //are disposed...
                var currentOffset = index*Marshal.SizeOf(typeof (Buffer));
                Marshal.WriteInt32(pBuffers, currentOffset, thisSecBuffer.cbBuffer);
                Marshal.WriteInt32(pBuffers, currentOffset + Marshal.SizeOf(thisSecBuffer.cbBuffer),
                    thisSecBuffer.cbBufferType);
                Marshal.WriteIntPtr(pBuffers,
                    currentOffset + Marshal.SizeOf(thisSecBuffer.cbBuffer) + Marshal.SizeOf(thisSecBuffer.cbBufferType),
                    thisSecBuffer.pvBuffer);
            }
        }

        public void Dispose()
        {
            if (pBuffers != IntPtr.Zero)
            {
                if (cBuffers == 1)
                {
                    var thisSecBuffer =
                        (SecurityBuffer) Marshal.PtrToStructure(pBuffers, typeof (SecurityBuffer));
                    thisSecBuffer.Dispose();
                }
                else
                {
                    for (var index = 0; index < cBuffers; index++)
                    {
                        //The bits were written out the following order:
                        //int cbBuffer;
                        //int BufferType;
                        //pvBuffer;
                        //What we need to do here is to grab a hold of the pvBuffer allocate by the individual
                        //SecBuffer and release it...
                        var currentOffset = index*Marshal.SizeOf(typeof (Buffer));
                        var secBufferpvBuffer = Marshal.ReadIntPtr(pBuffers,
                            currentOffset + Marshal.SizeOf(typeof (int)) + Marshal.SizeOf(typeof (int)));
                        Marshal.FreeHGlobal(secBufferpvBuffer);
                    }
                }

                Marshal.FreeHGlobal(pBuffers);
                pBuffers = IntPtr.Zero;
            }
        }

        public byte[] GetBytes()
        {
            byte[] buffer = null;

            if (pBuffers == IntPtr.Zero)
            {
                throw new InvalidOperationException("Object has already been disposed!!!");
            }

            if (cBuffers == 1)
            {
                var thisSecBuffer =
                    (SecurityBuffer) Marshal.PtrToStructure(pBuffers, typeof (SecurityBuffer));

                if (thisSecBuffer.cbBuffer > 0)
                {
                    buffer = new byte[thisSecBuffer.cbBuffer];
                    Marshal.Copy(thisSecBuffer.pvBuffer, buffer, 0, thisSecBuffer.cbBuffer);
                }
            }
            else
            {
                var bytesToAllocate = 0;

                for (var index = 0; index < cBuffers; index++)
                {
                    //The bits were written out the following order:
                    //int cbBuffer;
                    //int BufferType;
                    //pvBuffer;
                    //What we need to do here calculate the total number of bytes we need to copy...
                    var currentOffset = index*Marshal.SizeOf(typeof (Buffer));
                    bytesToAllocate += Marshal.ReadInt32(pBuffers, currentOffset);
                }

                buffer = new byte[bytesToAllocate];

                for (int index = 0, bufferIndex = 0; index < cBuffers; index++)
                {
                    //The bits were written out the following order:
                    //int cbBuffer;
                    //int BufferType;
                    //pvBuffer;
                    //Now iterate over the individual buffers and put them together into a
                    //byte array...
                    var currentOffset = index*Marshal.SizeOf(typeof (Buffer));
                    var bytesToCopy = Marshal.ReadInt32(pBuffers, currentOffset);
                    var secBufferpvBuffer = Marshal.ReadIntPtr(pBuffers,
                        currentOffset + Marshal.SizeOf(typeof (int)) + Marshal.SizeOf(typeof (int)));
                    Marshal.Copy(secBufferpvBuffer, buffer, bufferIndex, bytesToCopy);
                    bufferIndex += bytesToCopy;
                }
            }

            return (buffer);
        }
    }
}
