using System;
using System.Runtime.InteropServices;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    /// <summary>
    /// Class extracted from https://github.com/pysco68/Pysco68.Owin.Authentication.Ntlm
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SecurityBuffer : IDisposable
    {
        public int cbBuffer;
        public int cbBufferType;
        public IntPtr pvBuffer;

        public SecurityBuffer(int bufferSize)
        {
            cbBuffer = bufferSize;
            cbBufferType = (int)SecurityBufferType.SecbufferToken;
            pvBuffer = Marshal.AllocHGlobal(bufferSize);
        }

        public SecurityBuffer(byte[] secBufferBytes)
        {
            cbBuffer = secBufferBytes.Length;
            cbBufferType = (int)SecurityBufferType.SecbufferToken;
            pvBuffer = Marshal.AllocHGlobal(cbBuffer);
            Marshal.Copy(secBufferBytes, 0, pvBuffer, cbBuffer);
        }

        public SecurityBuffer(byte[] secBufferBytes, SecurityBufferType bufferType)
        {
            cbBuffer = secBufferBytes.Length;
            cbBufferType = (int)bufferType;
            pvBuffer = Marshal.AllocHGlobal(cbBuffer);
            Marshal.Copy(secBufferBytes, 0, pvBuffer, cbBuffer);
        }

        public void Dispose()
        {
            if (pvBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(pvBuffer);
                pvBuffer = IntPtr.Zero;
            }
        }
    }
}