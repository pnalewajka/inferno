﻿using System;
using Smt.Atomic.CrossCutting.Security.Exceptions;
using Smt.Atomic.CrossCutting.Security.Interfaces;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    internal class NtlmAuthenticationService : INtlmAuthenticationService
    {
        private readonly IHandShakeStateCacheService _handShakeStateCacheService;

        public NtlmAuthenticationService(IHandShakeStateCacheService handShakeStateCacheService)
        {
            _handShakeStateCacheService = handShakeStateCacheService;
        }

        private void SetInitialAuthenticationResult(NtlmAuthenticationResult result)
        {
            result.Action = NtlmAuthenticationResultAction.ChallengeAuth;
            result.StatusCode = 401;
            result.Headers.Add("WWW-Authenticate", "NTLM");
        }

        public NtlmAuthenticationResult Authenticate(Guid guid, string authenticationHeader)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(guid));
            }

            var result = new NtlmAuthenticationResult();
            var handshakeState = _handShakeStateCacheService.GetHandShakeState(guid);

            if (string.IsNullOrEmpty(authenticationHeader))
            {
                SetInitialAuthenticationResult(result);

                return result;
            }

            if (handshakeState == null)
            {
                _handShakeStateCacheService.ReleaseHandShakeState(guid);
                SetInitialAuthenticationResult(result);

                return result;
            }

            var messageHeader = NtlmTypeMessage.ParseNtlmHeader(authenticationHeader);

            if (messageHeader.IsNtlmMessage && messageHeader.MessageType == NtlmMessageType.Type1)
            {
                var message = NtlmTypeMessage.ParseMessage(authenticationHeader) as NtlmType1Message;

                if (message == null)
                {
                    throw new UnexpectedNtlmMessageException(authenticationHeader, NtlmMessageType.Type1);
                }

                var token = message.Token;

                if (handshakeState.TryAcquireServerChallenge(ref token))
                {
                    // send the type 2 message
                    var authorization = Convert.ToBase64String(token);
                    result.Headers.Add("WWW-Authenticate", string.Concat("NTLM ", authorization));
                    result.StatusCode = 401;
                    result.Action = NtlmAuthenticationResultAction.ChallengeAuth;

                    return result;
                }
            }

            if (messageHeader.IsNtlmMessage && messageHeader.MessageType == NtlmMessageType.Type3)
            {
                var message = NtlmTypeMessage.ParseMessage(authenticationHeader) as NtlmType3Message;

                if (message == null)
                {
                    throw new UnexpectedNtlmMessageException(authenticationHeader, NtlmMessageType.Type3);
                }

                try
                {
                    var identityResult = handshakeState.IsClientResponseValid(message.Token);

                    if (identityResult.IsValid && identityResult.IsAuthenticated)
                    {
                        result.Action = NtlmAuthenticationResultAction.ChallengeFinished;
                        result.IsAuthenticated = true;
                        result.UserName = identityResult.UserName;
                        result.DomainName = identityResult.DomainName;

                        return result;
                    }
                    else
                    {
                        result.Action = NtlmAuthenticationResultAction.ChallengeFailed;
                        result.Headers.Add("WWW-Authenticate", "NTLM");
                        result.StatusCode = 401;
                    }
                }
                finally
                {
                    _handShakeStateCacheService.ReleaseHandShakeState(guid);
                }
            }

            return result;
        }
    }
}