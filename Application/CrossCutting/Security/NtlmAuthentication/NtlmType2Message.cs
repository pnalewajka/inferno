﻿using System;
using System.Text;

namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public class NtlmType2Message : NtlmTypeMessage
    {
        public string TargetName { get; private set; }
        public NtlmOsVersion OsVersion { get; private set; }
        public byte[] Challenge { get; private set; }
        public byte[] Context { get; private set; }

        public NtlmType2Message(byte[] body)
            : base(body)
        {
            if (MessageType != NtlmMessageType.Type2)
            {
                throw new Exception($"Incorrect message type: {MessageType}, should be {NtlmMessageType.Type2}");
            }

            TargetName = new NtlmSecurityBuffer(body, 12).GetUnicodeString();
            Options = (NtlmMessageOptions)BitConverter.ToInt32(body, 20);
            Challenge = GetTokenPart(body, 24, 8);
            Context = GetTokenPart(body, 32, 8);

            OsVersion = new NtlmOsVersion(body, 48);
        }
    }
}