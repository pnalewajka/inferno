﻿namespace Smt.Atomic.CrossCutting.Security.NtlmAuthentication
{
    public enum NtlmAuthenticationResultAction
    {
        /// <summary>
        /// Challenge failed
        /// </summary>
        ChallengeFailed = 0,
        /// <summary>
        /// Proceed with redirect
        /// </summary>
        Redirect,
        /// <summary>
        /// Begin challenge
        /// </summary>
        ChallengeAuth,
        /// <summary>
        /// Challenge finished
        /// </summary>
        ChallengeFinished
    }
}