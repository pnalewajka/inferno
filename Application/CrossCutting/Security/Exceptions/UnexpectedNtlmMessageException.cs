﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.NtlmAuthentication;

namespace Smt.Atomic.CrossCutting.Security.Exceptions
{
    /// <summary>
    /// Received Ntlm message wasn't actually what was expected
    /// </summary>
    [Serializable]
    public class UnexpectedNtlmMessageException : Exception
    {
        private const string MessageFormat = "Unexpected NTLM message: {0}, expected message type {1}";
        public NtlmMessageType MessageType { get; protected set; }

        public UnexpectedNtlmMessageException()
        {
        }

        public UnexpectedNtlmMessageException(string messageBody, NtlmMessageType messageType)
            : base(string.Format(MessageFormat, messageBody, messageType))
        {
            MessageType = messageType;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected UnexpectedNtlmMessageException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            MessageType = (NtlmMessageType)info.GetInt32(PropertyHelper.GetPropertyName<UnexpectedNtlmMessageException>(e => e.MessageType));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<UnexpectedNtlmMessageException>(e => e.MessageType), MessageType);
            base.GetObjectData(info, context);
        }

    }
}
