﻿using System;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.CrossCutting.Security.Models
{
    public class ParsedLogin
    {
        public string Login { get; set; }
        public string Domain { get; set; }

        public bool IsEmail => EmailHelper.IsValidEmail(Login);

        public static ParsedLogin Parse(string username)
        {
            // no idea what to do with "smtsoftware\" username - so no go :)
            if (string.IsNullOrWhiteSpace(username) || username.EndsWith("\\"))
            {
                return new ParsedLogin();
            }

            // only \ is no go too
            username = username.TrimStart('\\');
            if (string.IsNullOrWhiteSpace(username))
            {
                return new ParsedLogin();
            }

            if (username.Contains(@"\"))
            {
                // and must be in between as end and begining was not allowed
                var items = username.Split(new[] { @"\" }, StringSplitOptions.RemoveEmptyEntries);
                var domain = items[0].ToLower();
                var ldapName = items[1].ToLower();

                return new ParsedLogin
                {
                    Login = ldapName,
                    Domain = domain,
                };
            }

            return new ParsedLogin
            {
                Login = username,
            };
        }

        private ParsedLogin()
        {
        }
    }
}
