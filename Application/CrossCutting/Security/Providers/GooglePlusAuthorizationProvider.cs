﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Flurl;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Results;
using Smt.Atomic.CrossCutting.Security.Providers.GoogleAuthorizationClasses;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    /// <summary>
    /// Detailed Google OAuth protocol:
    /// https://developers.google.com/identity/protocols/OAuth2WebServer
    /// </summary>
    [Identifier("AuthorizationProvider.GooglePlus")]
    public class GooglePlusAuthorizationProvider : AuthorizationProviderBase
    {
        private const string CodeParameter = "code";
        private const string ErrorParameter = "error";

        private readonly GooglePlusSettings _settings;

        public override bool IsEnabled => _settings.IsEnabled;

        public override AuthorizationProviderType ProviderType => AuthorizationProviderType.GooglePlus;

        public GooglePlusAuthorizationProvider
            (
            ISettingsProvider settingsProvider,
            ILogger logger,
            IHttpClientService httpClientService
            ) : base(httpClientService, logger)
        {
            Name = "GooglePlus";
            Identifier = typeof (GooglePlusAuthorizationProvider).GetAttributes<IdentifierAttribute>().Single().Value;
            IconClass = "fa fa-google-plus-square";
            _settings = settingsProvider.AuthorizationProviderSettings.GooglePlusSettings;
        }

        public override UserAuthorizationValidationResult Validate(HttpRequestData requestData)
        {
            var failedUserValidationData = new UserAuthorizationValidationResult
            {
                IsAuthenticated = false
            };

            if (requestData.Params.ContainsKey(ErrorParameter))
            {
                Logger.WarnFormat("Google request failure: {0}", requestData.Params[ErrorParameter]);
                return failedUserValidationData;
            }

            if (!requestData.Params.ContainsKey(CodeParameter))
            {
                return failedUserValidationData;
            }

            var code = requestData.Params[CodeParameter].ToString();

            var tokensResult = GetExchangeTokens(code);

            if (tokensResult == null || !tokensResult.Result)
            {
                return failedUserValidationData;
            }

            var userData = GetUserData(tokensResult.State.AccessToken, tokensResult.State.TokenType);

            if (!userData.Result)
            {
                return failedUserValidationData;
            }

            return new UserAuthorizationValidationResult
            {
                ProviderType = AuthorizationProviderType.GooglePlus,
                Email = userData.State.Email,
                FirstName = userData.State.GivenName,
                LastName = userData.State.FamilyName,
                IsAuthenticated = true
            };
        }

        private BoolResultWithState<GoogleApiUserInfoResult> GetUserData(string accessToken, string tokenType)
        {
            var failedResult = new BoolResultWithState<GoogleApiUserInfoResult>(false, null);

            var userInfoResult = GetDataFromJson<GoogleApiUserInfoResult>(_settings.UserInfoEndpoint,
                DictionaryHelper.ToDictionary(new
                {
                    Authorization = $"{tokenType} {accessToken}"
                }, p => p, v => v.ToString()), null, null);

            if (!userInfoResult.Result || string.IsNullOrEmpty(userInfoResult.State.Email))
            {
                Logger.WarnFormat(
                    "Google API response body unexpected, should be json with exchange token, received {0}",
                    userInfoResult.State != null ? userInfoResult.State.JsonBody : "<empty>");

                return failedResult;
            }

            return userInfoResult;
        }

        private BoolResultWithState<GoogleApiTokenExchangeResult> GetExchangeTokens(string authorizationCode)
        {
            const string tokenTypeBearer = "Bearer";

            var tokensResult = GetDataFromJson<GoogleApiTokenExchangeResult>(_settings.TokenExchangeEndpoint, null, null,
                DictionaryHelper.ToDictionary(new
                {
                    code = authorizationCode,
                    client_id = _settings.ClientId,
                    client_secret = _settings.ClientSecret,
                    redirect_uri = _settings.RedirectUri,
                    grant_type = "authorization_code"
                }, p => p, v => v.ToString()));

            if (!tokensResult.Result || tokensResult.State.TokenType != tokenTypeBearer)
            {
                Logger.ErrorFormat(
                    "Google API response body unexpected, should be json with exchange token, received {0}",
                    tokensResult.State != null ? tokensResult.State.JsonBody : "<empty>");

                return tokensResult;
            }

            return tokensResult;
        }

        public override HttpResponseRedirectData GetSignOnRedirection()
        {
            var redirectData = new HttpResponseRedirectData();
            var url = new Url(_settings.AuthorizationEndpoint);

            url.SetQueryParams(
                new
                {
                    redirect_uri = _settings.RedirectUri,
                    response_type = "code",
                    client_id = _settings.ClientId,
                    scope = string.Join(" ", _settings.Scopes),
                    state = string.Empty,
                }
                );

            redirectData.RedirectUri = new Uri(url);

            return redirectData;
        }
    }
}