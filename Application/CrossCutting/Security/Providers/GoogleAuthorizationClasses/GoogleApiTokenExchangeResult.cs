using Newtonsoft.Json;

namespace Smt.Atomic.CrossCutting.Security.Providers.GoogleAuthorizationClasses
{
    internal class GoogleApiTokenExchangeResult : ApiJsonBody
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        /// <summary>
        /// Should be 'Bearer'
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }
}