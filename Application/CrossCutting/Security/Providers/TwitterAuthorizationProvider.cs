using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using TweetSharp;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    /// <summary>
    /// https://github.com/danielcrenna/tweetsharp
    /// </summary>
    [Identifier("AuthorizationProvider.Twitter")]
    public class TwitterAuthorizationProvider : AuthorizationProviderBase
    {
        private readonly TwitterSettings _settings;

        public TwitterAuthorizationProvider(ISettingsProvider settingsProvider, IHttpClientService httpClientService, ILogger logger) : base(httpClientService, logger)
        {
            _settings = settingsProvider.AuthorizationProviderSettings.TwitterSettings;
            Name = "Twitter";
            Identifier = typeof(TwitterAuthorizationProvider).GetAttributes<IdentifierAttribute>().Single().Value;
            IconClass = "fa fa-twitter-square";
        }

        public override UserAuthorizationValidationResult Validate(HttpRequestData requestData)
        {
            var verifierCode = requestData.Params["oauth_verifier"].ToString();
            var oAuthToken = requestData.Params["oauth_token"].ToString();

            var requestToken = new OAuthRequestToken { Token = oAuthToken };

            var service = new TwitterService(_settings.ConsumerKey, _settings.ConsumerSecret);
            var accessToken = service.GetAccessToken(requestToken, verifierCode);

            service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
            var user = service.VerifyCredentials(new VerifyCredentialsOptions {SkipStatus = true});

            if (user == null)
            {
                return new UserAuthorizationValidationResult
                {
                    IsAuthenticated = false,
                    ProviderType = AuthorizationProviderType.Twitter
                };
            }

            return new UserAuthorizationValidationResult
            {
                ExternalId = user.Id.ToInvariantString(),
                IsAuthenticated = user.Id > 0,
                ProviderType = AuthorizationProviderType.Twitter
            };
        }

        public override bool IsEnabled => _settings.IsEnabled;

        public override AuthorizationProviderType ProviderType => AuthorizationProviderType.Twitter;

        public override HttpResponseRedirectData GetSignOnRedirection()
        {
            var twitterService = new TwitterService(_settings.ConsumerKey, _settings.ConsumerSecret);
            var requestToken = twitterService.GetRequestToken(_settings.CallbackUrl);
            var redirectUri = twitterService.GetAuthorizationUri(requestToken);

            return new HttpResponseRedirectData
            {
                RedirectUri = redirectUri
            };
        }
    }
}