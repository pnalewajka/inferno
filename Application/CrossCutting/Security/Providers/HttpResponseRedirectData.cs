﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    /// <summary>
    /// Redirect response data
    /// </summary>
    public class HttpResponseRedirectData
    {
        /// <summary>
        /// Address to redirect to
        /// </summary>
        public Uri RedirectUri { get; set; }

        /// <summary>
        /// Additional headers
        /// </summary>
        public Dictionary<string, string> Headers { get; set; }
    }
}