﻿using System;
using System.Collections.Generic;
using System.Net;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Results;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Security.Interfaces;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    public abstract class AuthorizationProviderBase : IAuthorizationProvider
    {
        protected readonly IHttpClientService HttpClientService;
        protected readonly ILogger Logger;
        public abstract bool IsEnabled { get; }
        public string Name { get; protected set; }
        public string Identifier { get; protected set; }
        public string IconClass { get; protected set; }
        public abstract AuthorizationProviderType ProviderType { get; }

        private readonly List<string> _jsonKnownContentTypes = new List<string>
        {
            "application/json",
            "application/x-javascript",
            "text/javascript",
            "text/x-javascript",
            "text/x-json"
        };


        protected AuthorizationProviderBase(IHttpClientService httpClientService, ILogger logger)
        {
            HttpClientService = httpClientService;
            Logger = logger;
        }

        public abstract UserAuthorizationValidationResult Validate(HttpRequestData requestData);
        
        public abstract HttpResponseRedirectData GetSignOnRedirection();

        protected BoolResultWithState<TResult> GetDataFromJson<TResult>(string url, IDictionary<string, string> headers,
            IDictionary<string, string> getParameters, IDictionary<string, string> postParameters)
            where TResult : ApiJsonBody
        {
            var failedResult = new BoolResultWithState<TResult>(false, null);
            HttpResponseData responseData;

            try
            {
                responseData = HttpClientService.RetrieveHtmlPage(url, headers, getParameters, postParameters);
            }
            catch (Exception e)
            {
                Logger.WarnFormat(e, "{0} API request failure", ProviderType);

                return failedResult;
            }

            if (responseData.StatusCode != HttpStatusCode.OK || !_jsonKnownContentTypes.Contains(responseData.ContentType))
            {
                Logger.WarnFormat("{0} API request failure {1}, response body {2}", ProviderType,
                    responseData.StatusCode, responseData.Body);

                return failedResult;
            }

            var deserializedData =
                JsonHelper.Deserialize(responseData.Body, typeof (TResult)) as
                    TResult;

            if (deserializedData == null)
            {
                Logger.ErrorFormat(
                    "{0} API response body unexpected, should be json for {1} , received {2}",
                    ProviderType,
                    typeof (TResult).Name,
                    responseData.Body);

                return failedResult;
            }

            deserializedData.JsonBody = responseData.Body;

            return new BoolResultWithState<TResult>(true, deserializedData);
        }
    }
}