using System;
using System.Collections.Specialized;
using System.Linq;
using Castle.Core.Logging;
using Flurl;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Results;
using Smt.Atomic.CrossCutting.Security.Providers.FacebookAuthorizationClasses;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    /// <summary>
    /// Detailed OAuth protocol:
    /// https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow
    /// </summary>
    [Identifier("AuthorizationProvider.Facebook")]
    public class FacebookAuthorizationProvider : AuthorizationProviderBase
    {
        private const string CodeParameter = "code";
        private const string ErrorParameter = "error";
        private readonly FacebookSettings _settings;

        public override bool IsEnabled => _settings.IsEnabled;

        public override AuthorizationProviderType ProviderType => AuthorizationProviderType.Facebook;

        public FacebookAuthorizationProvider(ISettingsProvider settingsProvider, IHttpClientService httpClientService,
            ILogger logger) : base(httpClientService, logger)
        {
            _settings = settingsProvider.AuthorizationProviderSettings.FacebookSettings;
            Name = "Facebook";
            Identifier = typeof (FacebookAuthorizationProvider).GetAttributes<IdentifierAttribute>().Single().Value;
            IconClass = "fa fa-facebook-square";
        }

        public override UserAuthorizationValidationResult Validate(HttpRequestData requestData)
        {
            var failedUserValidationData = new UserAuthorizationValidationResult
            {
                IsAuthenticated = false
            };

            if (requestData.Params.ContainsKey(ErrorParameter))
            {
                Logger.WarnFormat("Google request failure: {0}", requestData.Params[ErrorParameter]);
                return failedUserValidationData;
            }

            if (!requestData.Params.ContainsKey(CodeParameter))
            {
                return failedUserValidationData;
            }

            var code = requestData.Params[CodeParameter].ToString();

            var tokensResult = GetExchangeTokens(code);

            if (tokensResult == null)
            {
                return failedUserValidationData;
            }

            var appAccessToken = GetAppAccessToken();
            var debugInfo = GetTokenDebugInfo(appAccessToken.State.AccessToken, tokensResult.State.AccessToken);

            var userData = GetUserData(tokensResult.State.AccessToken, tokensResult.State.TokenType);

            if (!userData.Result)
            {
                return failedUserValidationData;
            }

            return new UserAuthorizationValidationResult
            {
                ProviderType = AuthorizationProviderType.Facebook,
                Email = userData.State.Email,
                FirstName = userData.State.FirstName,
                LastName = userData.State.LastName,
                IsAuthenticated = true
            };
        }

        private BoolResultWithState<FacebookApiTokenExchangeResult> GetExchangeTokens(string authorizationCode)
        {
            var tokensResult = GetDataFromJson<FacebookApiTokenExchangeResult>(_settings.TokenExchangeEndpoint, null, null,
                DictionaryHelper.ToDictionary(new
                {
                    code = authorizationCode,
                    client_id = _settings.ClientId,
                    client_secret = _settings.ClientSecret,
                    redirect_uri = _settings.RedirectUri,
                }, p => p, v => v.ToString()));

            if (!tokensResult.Result || string.IsNullOrEmpty(tokensResult.State.AccessToken))
            {
                Logger.ErrorFormat(
                    "Facebook API response body unexpected, should be json with exchange token, received {0}",
                    tokensResult.State.JsonBody);

                return tokensResult;
            }

            return tokensResult;
        }

        private BoolResultWithState<FacebookApiTokenExchangeResult> GetAppAccessToken()
        {
            var tokensResult = GetDataFromJson<FacebookApiTokenExchangeResult>(_settings.TokenExchangeEndpoint, null, null,
                DictionaryHelper.ToDictionary(new
                {
                    client_id = _settings.ClientId,
                    client_secret = _settings.ClientSecret,
                    grant_type = "client_credentials"
                }, p => p, v => v.ToString()));

            if (!tokensResult.Result || string.IsNullOrEmpty(tokensResult.State.AccessToken))
            {
                Logger.ErrorFormat(
                    "Facebook API response body unexpected, should be json with exchange token, received {0}",
                    tokensResult.State.JsonBody);

                return tokensResult;
            }

            return tokensResult;
        }

        private BoolResultWithState<FacebookApiTokenDebugResult> GetTokenDebugInfo(string appAccessToken, string queryToken)
        {
            var tokensResult = GetDataFromJson<FacebookApiTokenDebugResult>(_settings.DebugTokenEndpoint, null, 
                DictionaryHelper.ToDictionary(new
                {
                    access_token = appAccessToken,
                    input_token = queryToken
                }, p => p, v => v.ToString()), null);

            return tokensResult;
        }



        public override HttpResponseRedirectData GetSignOnRedirection()
        {
            var redirectData = new HttpResponseRedirectData();
            var url = new Url(_settings.AuthorizationEndpoint);

            url.SetQueryParams(
                    new
                    {
                        redirect_uri = _settings.RedirectUri,
                        response_type = "code",
                        client_id = _settings.ClientId,
                        scope = string.Join(",", _settings.Scopes),
                        state = string.Empty,
                    }
                );

            redirectData.RedirectUri = new Uri(url);

            return redirectData;
        }

        private BoolResultWithState<FacebookApiUserInfoResult> GetUserData(string accessToken, string tokenType)
        {
            var failedResult = new BoolResultWithState<FacebookApiUserInfoResult>(false, null);

            var userInfoResult = GetDataFromJson<FacebookApiUserInfoResult>(
                _settings.UserInfoEndpoint,
                null,
                DictionaryHelper.ToDictionary(new
                {
                    access_token = accessToken,
                    fields = "id,name,email,first_name,last_name"
                }, p => p, v => v.ToString()),
                null);

            if (!userInfoResult.Result || string.IsNullOrEmpty(userInfoResult.State.Email))
            {
                Logger.WarnFormat(
                    "Facebook API response body unexpected, should be json with exchange token, received {0}",
                    userInfoResult.State != null ? userInfoResult.State.JsonBody : "<empty>");

                return failedResult;
            }

            return userInfoResult;
        }
    }
}