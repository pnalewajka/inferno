using Newtonsoft.Json;

namespace Smt.Atomic.CrossCutting.Security.Providers.FacebookAuthorizationClasses
{
    internal class FacebookApiTokenExchangeResult : ApiJsonBody
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }
}