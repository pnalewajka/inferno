using Newtonsoft.Json;

namespace Smt.Atomic.CrossCutting.Security.Providers.FacebookAuthorizationClasses
{
    internal class FacebookApiUserInfoResult : ApiJsonBody
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("verified")]
        public string Verified { get; set; }
    }
}