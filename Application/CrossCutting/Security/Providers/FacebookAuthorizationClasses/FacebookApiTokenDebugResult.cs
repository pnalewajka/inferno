using Newtonsoft.Json;

namespace Smt.Atomic.CrossCutting.Security.Providers.FacebookAuthorizationClasses
{
    internal class FacebookApiTokenDebugResult : ApiJsonBody
    {
        [JsonProperty("is_valid")]
        public string IsValid { get; set; }
    }
}