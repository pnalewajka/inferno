﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    /// <summary>
    /// User validation result (via authorization provider)
    /// </summary>
    [Serializable]
    public class UserAuthorizationValidationResult
    {
        /// <summary>
        /// User was authenticated
        /// </summary>
        public bool IsAuthenticated { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        ///</summary>
        public string Email { get; set; }

        /// <summary>
        /// Auth provider type (will be persisted in DB)
        /// </summary>
        public AuthorizationProviderType ProviderType { get; set; }

        /// <summary>
        /// External id, currently only used by Twitter
        /// </summary>
        public string ExternalId { get; set; }
    }
}
