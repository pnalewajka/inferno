﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Settings.Models;

namespace Smt.Atomic.CrossCutting.Security.Providers.TwitterAuthorizationClasses
{
    /// <summary>
    /// Required parameters description: 
    /// https://dev.twitter.com/oauth/overview/authorizing-requests
    /// </summary>
    public class TwitterHeaderParametersBase
    {
        [TwitterParameterName("oauth_consumer_key")]
        public string ConsumerKey { get; set; }

        [TwitterParameterName("oauth_nonce")]
        public string Nonce { get; set; }

        [TwitterParameterName("oauth_signature")]
        public string Signature { get; set; }

        [TwitterParameterName("oauth_signature_method")]
        public string SignatureMethod { get; set; }

        [TwitterParameterName("oauth_timestamp")]
        public string Timestamp { get; set; }

        [TwitterParameterName("oauth_token")]
        public string Token { get; set; }

        [TwitterParameterName("oauth_version")]
        public string Version { get; set; }

        public TwitterHeaderParametersBase(TwitterSettings settings)
        {
            const string signatureMethod = "HMAC-SHA1";

            ConsumerKey = settings.ConsumerKey;
            Nonce = Guid.NewGuid().ToString();
            SignatureMethod = signatureMethod;
            Timestamp = DateHelper.ToUnixEpochTime(DateTime.UtcNow).ToInvariantString();
        }

        public string GetAuthorizationHeaderValue()
        {
            return string.Empty;
        }

        public string GetSignatureBase(string httpMethod, string baseUrl)
        {
            var currentObjectType = GetType();
            
            var properties =
                TypeHelper.GetPublicInstancePropertiesWithAttribute<TwitterParameterNameAttribute>(currentObjectType);

            var parametersList = new Dictionary<string, string>();

            foreach (var property in properties)
            {
                var propertyValueObject = property.GetValue(this);
                var propertyValue = propertyValueObject == null ? string.Empty : propertyValueObject.ToString();
                var propertyKey =
                    AttributeHelper.GetPropertyAttribute<TwitterParameterNameAttribute>(property).ParameterName;

                parametersList.Add(Uri.EscapeDataString(propertyKey), Uri.EscapeDataString(propertyValue));
            }

            var parametersStringBuilder = new StringBuilder();

            foreach (var keyValuePair in parametersList.OrderBy(k => k.Key).ToList())
            {
                if (parametersStringBuilder.Length > 0)
                {
                    parametersStringBuilder.Append("&");
                }

                parametersStringBuilder.AppendFormat("{0}={1}", keyValuePair.Key, keyValuePair.Value);
            }

            return parametersStringBuilder.ToString();
        }
    }

    /// <summary>
    /// https://dev.twitter.com/oauth/reference/post/oauth/request_token
    /// </summary>
    public class TwitterRequestTokenHeaderParameters : TwitterHeaderParametersBase
    {
        [TwitterParameterName("oauth_callback")]
        public string CallBack { get; private set; }

        public TwitterRequestTokenHeaderParameters(TwitterSettings settings) : base(settings)
        {
            CallBack = settings.CallbackUrl;
        }
    }
}
