﻿using System;

namespace Smt.Atomic.CrossCutting.Security.Providers.TwitterAuthorizationClasses
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TwitterParameterNameAttribute : Attribute
    {
        public string ParameterName { get; private set; }

        public TwitterParameterNameAttribute(string parameterName)
        {
            ParameterName = parameterName;
        }
    }
}
