﻿using System.Collections.Generic;

namespace Smt.Atomic.CrossCutting.Security.Providers
{
    /// <summary>
    /// Http Request data, will be mapped from HttpRequest
    /// </summary>
    public class HttpRequestData
    {
        public IDictionary<string, object> Headers { get; set; }
        public IDictionary<string, object> Params { get; set; }
        public int StatusCode { get; set; }
    }
}
