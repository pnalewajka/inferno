﻿using System;
using Smt.Atomic.CrossCutting.Security.NtlmAuthentication;

namespace Smt.Atomic.CrossCutting.Security.Interfaces
{
    public interface INtlmAuthenticationService
    {
        /// <summary>
        /// Authenticate using NTLM scheme
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="authenticationHeader"></param>
        /// <returns></returns>
        NtlmAuthenticationResult Authenticate(Guid guid, string authenticationHeader);
    }
}
