﻿using System.Web;

namespace Smt.Atomic.CrossCutting.Security.Interfaces
{
    public interface IHttpCurrentContextDataService
    {
        string RequestId { get; }

        string UserName { get; }

        HttpRequest Request { get; }

        HttpResponse Response { get; }
    }
}
