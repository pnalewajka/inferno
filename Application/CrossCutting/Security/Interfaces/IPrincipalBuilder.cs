﻿using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.CrossCutting.Security.Interfaces
{
    public interface IPrincipalBuilder
    {
        /// <summary>
        /// Builds a proper principal based on the login
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        IAtomicPrincipal BuildPrincipal(string login);
    }
}