﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Providers;

namespace Smt.Atomic.CrossCutting.Security.Interfaces
{
    public interface IAuthorizationProvider
    {
        /// <summary>
        /// Validate user
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        UserAuthorizationValidationResult Validate(HttpRequestData requestData);

        /// <summary>
        /// Is provider enabled
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Name 
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Class identifier (from IdentifierAttribute)
        /// </summary>
        string Identifier { get; }

        /// <summary>
        /// Icon class for usage in html
        /// </summary>
        string IconClass { get; }

        /// <summary>
        /// Auth provider type
        /// </summary>
        AuthorizationProviderType ProviderType { get; }

        /// <summary>
        /// Redirection data (for signin purposes)
        /// </summary>
        HttpResponseRedirectData GetSignOnRedirection();
    }
}
