﻿using System;
using Smt.Atomic.CrossCutting.Security.NtlmAuthentication;

namespace Smt.Atomic.CrossCutting.Security.Interfaces
{
    /// <summary>
    /// Ntlm handshake state cache service, used for platform security handle and context handle processing between all stages of NTLM handshake
    /// </summary>
    public interface IHandShakeStateCacheService
    {
        /// <summary>
        /// Acquire handshakestate for processing
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        HandShakeState GetHandShakeState(Guid id);

        /// <summary>
        /// Release allocated handshake
        /// </summary>
        /// <param name="id"></param>
        void ReleaseHandShakeState(Guid id);
    }
}
