﻿namespace Smt.Atomic.CrossCutting.Security.Enums
{
    public enum PasswordStatus
    {
        NotApplicable = 0,

        Valid,
        MustSet,
        MustChange
    }
}
