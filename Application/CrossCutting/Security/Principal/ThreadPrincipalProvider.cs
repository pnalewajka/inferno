﻿using System.Threading;

namespace Smt.Atomic.CrossCutting.Security.Principal
{
    public class ThreadPrincipalProvider : IPrincipalProvider
    {
        public IAtomicPrincipal Current => (IAtomicPrincipal)Thread.CurrentPrincipal;

        public bool IsSet => Thread.CurrentPrincipal is IAtomicPrincipal;
    }
}
