﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Enums;
using Smt.Atomic.CrossCutting.Security.Principal.Claims;
using Smt.Atomic.CrossCutting.Security.Resources;

namespace Smt.Atomic.CrossCutting.Security.Principal
{
    public class AtomicPrincipal : ClaimsPrincipal, IAtomicPrincipal
    {
        private readonly ClaimsReader _claimsReader = new ClaimsReader();
        private readonly ClaimsWriter _claimsWriter = new ClaimsWriter();

        public static IAtomicPrincipal Anonymous => new AtomicPrincipal();

        public string Login => Identity.Name;

        public long? Id
        {
            get
            {
                var claim = _claimsReader.GetClaimValues<long>(ClaimsIdentity, AtomicClaimTypes.Id).SingleOrDefault();

                if (claim == null)
                {
                    // for unauthorized
                    return null;
                }

                return claim.Value;
            }
        }

        public long? ImpersonatedById
        {
            get
            {
                var claim = _claimsReader.GetClaimValues<long>(ClaimsIdentity, AtomicClaimTypes.ImpersonatedById).SingleOrDefault();

                if (claim == null)
                {
                    // for unauthorized
                    return null;
                }

                return claim.Value;
            }
        }

        public string FirstName => Identity.IsAuthenticated ? _claimsReader.GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.FirstName).SingleOrDefault() : PrincipalResources.AnonymousUserDefaultFirstName;

        public string LastName => Identity.IsAuthenticated ? _claimsReader.GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.LastName).SingleOrDefault() : PrincipalResources.AnonymousUserDefaultLastName;

        public string Email => _claimsReader.GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.Email).SingleOrDefault();

        public string JobTitle => _claimsReader.GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.JobTitle).SingleOrDefault();

        public string CompanyName => _claimsReader.GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.CompanyName).SingleOrDefault();

        public PasswordStatus PasswordStatus => _claimsReader.GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.PasswordStatus).Select(EnumHelper.GetEnumValue<PasswordStatus>).SingleOrDefault();

        public string[] Roles => _claimsReader
            .GetObjectClaimValues<string>(ClaimsIdentity, ClaimsIdentity.RoleClaimType)
            .ToArray();

        public string[] Profiles => _claimsReader
            .GetObjectClaimValues<string>(ClaimsIdentity, AtomicClaimTypes.Profile)
            .ToArray();

        public bool IsAuthenticated => Identity.IsAuthenticated;

        private ClaimsIdentity ClaimsIdentity => (ClaimsIdentity)Identity;

        public AtomicPrincipal(string userLogin) : base(new GenericIdentity(userLogin))
        {
        }

        private AtomicPrincipal() : base(new GenericIdentity(string.Empty))
        {
            _claimsWriter.AddClaimValue(ClaimsIdentity, AtomicClaimTypes.FirstName, PrincipalResources.AnonymousUserDefaultFirstName);
            _claimsWriter.AddClaimValue(ClaimsIdentity, AtomicClaimTypes.LastName, PrincipalResources.AnonymousUserDefaultLastName);
        }

        public bool IsInRole(SecurityRoleType role)
        {
            return IsInRole(role.ToString());
        }

        public long EmployeeId
        {
            get
            {
                var claim = _claimsReader.GetClaimValues<long>(ClaimsIdentity, AtomicClaimTypes.EmployeeId).SingleOrDefault();

                if (claim == null)
                {
                    // for unauthorized
                    return -1;
                }

                return claim.Value;
            }
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}".Trim()
                   + (string.IsNullOrEmpty(Login) ? string.Empty : $" ({Login})");
        }
    }
}