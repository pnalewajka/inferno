﻿namespace Smt.Atomic.CrossCutting.Security.Principal
{
    /// <summary>
    /// Principal provider
    /// </summary>
    public interface IPrincipalProvider
    {
        /// <summary>
        /// Get information about currently logged user
        /// </summary>
        IAtomicPrincipal Current { get; }

        /// <summary>
        /// Allows to determine if the principal is properly set.
        /// Used on rare occasions when handling an exception that happens very early in the call,
        /// before the right principal could be set.
        /// </summary>
        bool IsSet { get; }
    }
}
