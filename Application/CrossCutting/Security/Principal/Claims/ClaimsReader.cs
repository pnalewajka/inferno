﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
namespace Smt.Atomic.CrossCutting.Security.Principal.Claims
{
    /// <summary>
    /// Reads claims
    /// </summary>
    public class ClaimsReader
    {
        /// <summary>
        /// Get value type claim value
        /// </summary>
        /// <typeparam name="T">type to convert claim string representation to</typeparam>
        /// <param name="identity">identity that contains claim</param>
        /// <param name="type">claim type</param>
        /// <returns>Value of claim type</returns>
        public IEnumerable<T?> GetClaimValues<T>(ClaimsIdentity identity, string type) where T : struct
        {
            return GetBaseClaimValues<T>(identity, type).Select(c => c as T?);
        }

        /// <summary>
        /// Get object type claim value
        /// </summary>
        /// <typeparam name="T">type to convert claim string representation to</typeparam>
        /// <param name="identity">identity that contains claim</param>
        /// <param name="type">claim type</param>
        /// <returns>Value of claim type</returns>
        public IEnumerable<T> GetObjectClaimValues<T>(ClaimsIdentity identity, string type) where T : class
        {
            return GetBaseClaimValues<T>(identity, type).Select(c => c as T);
        }

        private IEnumerable<object> GetBaseClaimValues<T>(ClaimsIdentity identity, string type)
        {
            foreach (var claim in identity.Claims.Where(f => f.Type == type))
            {
                Type t = typeof(T);

                if (claim == null)
                {
                    yield return null;
                }

                if (string.IsNullOrWhiteSpace(claim.Value) && t != typeof(string))
                {
                    yield return null;
                }

                t = Nullable.GetUnderlyingType(t) ?? t;

                yield return Convert.ChangeType(claim.Value, t);
            }
        }
    }
}
