﻿
namespace Smt.Atomic.CrossCutting.Security.Principal.Claims
{
    public static class AtomicClaimTypes
    {
        public const string DisplayName = "http://atomic.smtsoftware.com/Security/Principal/Claims/DisplayName";
        public const string Id = "http://atomic.smtsoftware.com/Security/Principal/Claims/Id";
        public const string ImpersonatedById = "http://atomic.smtsoftware.com/Security/Principal/Claims/ImpersonatedById";
        public const string FirstName = "http://atomic.smtsoftware.com/Security/Principal/Claims/FirstName";
        public const string LastName = "http://atomic.smtsoftware.com/Security/Principal/Claims/LastName";
        public const string Email = "http://atomic.smtsoftware.com/Security/Principal/Claims/Email";
        public const string JobTitle = "http://atomic.smtsoftware.com/Security/Principal/Claims/JobTitle";
        public const string CompanyName = "http://atomic.smtsoftware.com/Security/Principal/Claims/CompanyName";
        public const string PasswordStatus = "http://atomic.smtsoftware.com/Security/Principal/Claims/PasswordStatus";
        public const string Profile = "http://atomic.smtsoftware.com/Security/Principal/Claims/Profile";
        public const string DomainName = "http://atomic.smtsoftware.com/Security/Principal/Claims/DomainName";
        public const string WindowsGroupSid = "http://atomic.smtsoftware.com/Security/Principal/Claims/WindowsGroupSid";
        public const string AuthFailReason = "http://atomic.smtsoftware.com/Security/Principal/Claims/AuthFailReason";
        public const string EmployeeId = "http://atomic.smtsoftware.com/Security/Principal/Claims/EmployeeId";

        public const string IdentityProvider =
            "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";
    }
}
