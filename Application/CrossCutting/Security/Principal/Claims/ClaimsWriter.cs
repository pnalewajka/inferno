﻿using System.Linq;
using System.Security.Claims;

namespace Smt.Atomic.CrossCutting.Security.Principal.Claims
{
    /// <summary>
    /// Writes claims to identity
    /// </summary>
    public class ClaimsWriter
    {
        /// <summary>
        /// Adds new claim value to identity
        /// </summary>
        /// <param name="identity">Identity to add claim value to</param>
        /// <param name="claimType">Claim type</param>
        /// <param name="value">Value to add</param>
        /// <returns>True if success</returns>
        public bool AddClaimValue(ClaimsIdentity identity, string claimType, string value)
        {
            return SetClaimValue(identity, claimType, value, removeExisting: false);
        }

        /// <summary>
        /// Adds new claim value to identity if it does not exists (based on type). Overrides if claim type already exists in identity
        /// </summary>
        /// <param name="identity">Identity to add claim value to</param>
        /// <param name="claimType">Claim type</param>
        /// <param name="value">Value to add</param>
        /// <returns>True if success</returns>
        public bool OverrideClaimValue(ClaimsIdentity identity, string claimType, string value)
        {
            return SetClaimValue(identity, claimType, value, removeExisting: true);
        }

        private bool SetClaimValue(ClaimsIdentity identity, string claimType, string value, bool removeExisting)
        {
            if (removeExisting)
            {
                foreach (var machingClaim in identity.Claims.Where(f => f.Type == claimType))
                {
                    if (!identity.TryRemoveClaim(machingClaim))
                    {
                        return false;
                    }
                }
            }

            identity.AddClaim(new Claim(claimType, value));

            return true;
        }
    }
}
