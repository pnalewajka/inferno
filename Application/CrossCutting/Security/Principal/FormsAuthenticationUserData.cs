﻿using System;

namespace Smt.Atomic.CrossCutting.Security.Principal
{
    /// <summary>
    /// Class used to store additional data in authentication cookie (as user data)
    /// Content is serialized using JsonSerializer
    /// </summary>
    public class FormsAuthenticationUserData
    {
        public string SessionId { get; set; }

        public long UserId { get; set; }

        public string Login { get; set; }

        public FormsAuthenticationUserData()
        {
        }

        public FormsAuthenticationUserData(string sessionId, long userId, string login)
        {
            SessionId = sessionId;
            UserId = userId;
            Login = login;
        }
    }
}