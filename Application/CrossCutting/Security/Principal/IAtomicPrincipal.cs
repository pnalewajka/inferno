﻿using System.Security.Principal;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Enums;

namespace Smt.Atomic.CrossCutting.Security.Principal
{
    public interface IAtomicPrincipal : IPrincipal
    {
        long? Id { get; }

        string FirstName { get; }

        string LastName { get; }

        string Login { get; }

        string Email { get; }

        string JobTitle { get; }

        string CompanyName { get; }

        long? ImpersonatedById { get; }

        bool IsAuthenticated { get; }

        PasswordStatus PasswordStatus { get; }

        string[] Roles { get; }

        string[] Profiles { get; }

        bool IsInRole(SecurityRoleType role);

        long EmployeeId { get; }

    }
}