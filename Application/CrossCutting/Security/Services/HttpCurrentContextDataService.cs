﻿using System;
using System.Web;
using Smt.Atomic.CrossCutting.Security.Interfaces;

namespace Smt.Atomic.CrossCutting.Security.Services
{
    public class HttpCurrentContextDataService : IHttpCurrentContextDataService
    {
        private const string RequestIdKey = "RequestId";

        public string RequestId => GetRequestId();

        public string UserName => GetUserName();

        public HttpRequest Request => HttpContext.Current.Request;

        public HttpResponse Response => HttpContext.Current.Response;

        public void SetNewRequestId()
        {
            var context = HttpContext.Current;

            if (context != null)
            {
                context.Items[RequestIdKey] = Guid.NewGuid().ToString("N");
            }
        }

        private string GetRequestId()
        {
            var context = HttpContext.Current;

            return context == null
                ? null
                : (string) context.Items[RequestIdKey];
        }

        private string GetUserName()
        {
            var context = HttpContext.Current;

            return context == null || context.User == null || context.User.Identity == null 
                ? null 
                : context.User.Identity.Name;
        }
    }
}
