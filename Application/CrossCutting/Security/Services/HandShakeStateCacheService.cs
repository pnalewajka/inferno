﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.NtlmAuthentication;

namespace Smt.Atomic.CrossCutting.Security.Services
{
    internal class HandShakeStateCacheService : IHandShakeStateCacheService
    {
        private readonly Dictionary<Guid, WeakReference<HandShakeState>> _handShakeReferences;

        public HandShakeStateCacheService()
        {
            _handShakeReferences = new Dictionary<Guid, WeakReference<HandShakeState>>();
        }

        public HandShakeState GetHandShakeState(Guid id)
        {
            if (!_handShakeReferences.ContainsKey(id))
            {
                var handShakeState = new HandShakeState();
                _handShakeReferences.Add(id, new WeakReference<HandShakeState>(handShakeState));
            }

            HandShakeState existingHandShakeState;

            if (_handShakeReferences[id].TryGetTarget(out existingHandShakeState))
            {
                return existingHandShakeState;
            }

            return null;
        }

        public void ReleaseHandShakeState(Guid id)
        {
            if (_handShakeReferences.ContainsKey(id))
            {
                var handShakeReference = _handShakeReferences[id];
                _handShakeReferences.Remove(id);

                HandShakeState handShakeState;

                if (handShakeReference.TryGetTarget(out handShakeState))
                {
                    handShakeState.Dispose();
                }
            }
        }
    }
}
