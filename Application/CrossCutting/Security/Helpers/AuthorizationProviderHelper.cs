﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Interfaces;

namespace Smt.Atomic.CrossCutting.Security.Helpers
{
    public static class AuthorizationProviderHelper
    {
        /// <summary>
        /// Instantiate authorization provider based on identifier
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public static IAuthorizationProvider GetProviderByIdentifier(string identifier)
        {
            var providerType = IdentifierHelper.GetTypeByIdentifier(identifier);

            return providerType == null ? null : ReflectionHelper.CreateInstanceByIdentifier<IAuthorizationProvider>(identifier);
        }
    }
}
