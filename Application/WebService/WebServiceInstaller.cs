﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.WebServices.Accounts.Contracts.Service;
using Smt.Atomic.WebServices.Accounts.Services;

namespace Smt.Atomic.WebService
{
    public class WebServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<IAuthenticationService>()
                .ImplementedBy<AuthenticationService>()
                .LifestyleTransient()
                .Named("AuthenticationService"));

            container.Register(Component.For<IAccountsService>()
                .ImplementedBy<AccountsService>()
                .LifestyleTransient()
                .Named("AccountsService"));
        }
    }
}