﻿using System;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.WebServices.Common.Factories;

namespace Smt.Atomic.WebService
{
    public class InitializeAll
    {
        public static void AppInitialize()
        {
            var containerConfiguration = new WebServicesContainerConfiguration();
            var container = containerConfiguration.Configure(ContainerType.WebServices);
            container.AddFacility<WcfFacility>(f =>
            {
                f.CloseTimeout = TimeSpan.Zero;
            });

            container.Register(
                Component.For<IServiceHostBuilder<DefaultServiceModel>>()
                    .ImplementedBy<AtomicServiceHostBuilder>()
                    .Named("AtomicServiceHostBuilder")
                    .IsDefault());
        }
    }
}

