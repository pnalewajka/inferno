﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Attributes
{
    public class RequestControllerAttribute : IdentifierAttribute
    {
        public RequestControllerAttribute(RequestType type)
            : base(RequestControllerHelper.BuildIdentifier(type))
        {
        }
    }
}