﻿using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Attributes.CellContent
{
    public class IconizedIsActiveAttribute : IconizedAttribute
    {
        protected override string GetIconCssClass(object value)
        {
            var contentType = (bool)value;

            switch (contentType)
            {
                case true:
                    return "fa fa-check";

                case false:
                    return "fa fa-times";

                default:
                    return null;
            }
        }
    }
}