﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Attributes.CellContent
{
    public class IconizedTimeReportStatusAttribute: IconizedAttribute
    {
        protected override string GetCssClass(object value)
        {
            var status = (TimeReportStatus)value;

            switch (status)
            {
                case TimeReportStatus.NotStarted:
                    return "fa fa-hourglass-o";
                case TimeReportStatus.Draft:
                    return "fa fa-hourglass-half";
                case TimeReportStatus.Submitted:
                    return "fa fa-hourglass-end";
                case TimeReportStatus.Accepted:
                    return "fa fa-thumbs-o-up";
                case TimeReportStatus.Rejected:
                    return "fa fa-thumbs-o-down";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}