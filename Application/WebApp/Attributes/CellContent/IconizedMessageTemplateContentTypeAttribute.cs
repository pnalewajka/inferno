﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Attributes.CellContent
{
    public class IconizedMessageTemplateContentTypeAttribute : IconizedAttribute
    {
        protected override string GetIconCssClass(object value)
        {
            var contentType = (MessageTemplateContentType)value;

            switch (contentType)
            {
                case MessageTemplateContentType.Html:
                    return "fa fa-file-code-o";

                case MessageTemplateContentType.PlainText:
                    return "fa fa-file-o";

                default:
                    return null;
            }
        }
    }
}