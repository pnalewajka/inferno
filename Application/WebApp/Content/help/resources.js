﻿var helpResources = {
    pages: [
        {
            url: "/Workflows/Request/Add?request-type=business-trip-request",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/Business+Trip+Request" },
                { path: "#approverGroupContainer", link: "https://wiki.intive.com/confluence/display/DAN/Requests+Approvers" } 
            ]
        },
        {
            url: "/Workflows/Request/Add?request-type=business-trip-settlement-request",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/Business+Trip+Settlement+Request" },
                { path: "#approverGroupContainer", link: "https://wiki.intive.com/confluence/display/DAN/Requests+Approvers" }
            ]
        },
        {
            url: "/Workflows/Request/Add?request-type=advanced-payment-request",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/Advanced+payment+request" },
                { path: "#approverGroupContainer", link: "https://wiki.intive.com/confluence/display/DAN/Requests+Approvers" }
            ]
        },
        {
            url: "/BusinessTrips/BusinessTrip/List",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/Business+Trips" },
            ]
        },
        {
            url: "/Workflows/Request/Add?request-type=onboarding-request",
            resources: [
                { path: "#RequestObjectViewModel\\.job-title-id!parent!parent", link: "https://wiki.intive.com/confluence/display/DAN/tips%3A+Job+Title" },
                { path: "#RequestObjectViewModel\\.login!parent", link: "https://wiki.intive.com/confluence/display/DAN/tips%3A+Login" },
                { path: "#RequestObjectViewModel\\.org-unit-id!parent!parent", link: "https://wiki.intive.com/confluence/display/DAN/tips%3A+Org+Unit" },
                { path: "#RequestObjectViewModel\\.recruiter-employee-id!parent!parent", link: "https://wiki.intive.com/confluence/display/DAN/tips%3A+Recruiter" },
                { path: "#watchers-user-ids!parent!parent", link: "https://wiki.intive.com/confluence/display/DAN/tips%3A+Watchers" }
            ]
        },
        {
            url: "/TimeTracking/TimeTrackingDashboard/List",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/Dashboard" }
            ]
        },
        {
            url: "/Allocation/Project/Edit",
            resources: [
                { path: "#apn!parent", link: "https://wiki.intive.com/confluence/display/DAN/Accounting+Projects+Number+Scheme" }
            ]
        },
        {
            url: "/Workflows/Request/List",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/Requests" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=absence-request']", link: "https://wiki.intive.com/confluence/display/DAN/Absence+Request" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=add-home-office-request']", link: "https://wiki.intive.com/confluence/display/DAN/Home+Office+Request" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=overtime-request']", link: "https://wiki.intive.com/confluence/display/DAN/Overtime+Request" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=employee-data-change-request']", link: "https://wiki.intive.com/confluence/display/DAN/Employee+data+change+request" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=employment-data-change-request']", link: "https://wiki.intive.com/confluence/display/DAN/Employment+data+change+request" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=employment-termination-request']", link: "https://wiki.intive.com/confluence/display/DAN/Employment+termination+request" },
                { path: "a[data-url^='/Workflows/Request/Add?context=~context&request-type=onboarding-request']", link: "https://wiki.intive.com/confluence/display/DAN/Onboarding+request" }
            ]
        },
        {
            url: "/TimeTracking/MyTimeReport/MyTimeReport",
            resources: [
                { path: "", link: "https://wiki.intive.com/confluence/display/DAN/My+Report" },
                { path: "a[href^='/Workflows/Request/Add?request-type=reopen-time-tracking-report-request']", link: "https://wiki.intive.com/confluence/display/DAN/Unlocking+the+Report" },
                { path: "a[href^='/TimeTracking/MyTimeReport/ImportFromSource?inputSourceId=1']", link: "https://wiki.intive.com/confluence/display/DAN/Importing+Data+from+JIRA" }
            ]
        }
    ]
};