﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Helpers
{
    public static class CardIndexControllerAuthorizationHelper
    {
        public static SecurityRoleType[] GetRequiredSecurityRolesForController<TController>()
           where TController : ICardIndexController
        {
            var parentControllerType = typeof(TController);

            return GetRequiredSecurityRolesForController(parentControllerType);
        }


        public static SecurityRoleType[] GetRequiredSecurityRolesForController(Type controllerType)
        {
            if (controllerType.BaseType == typeof(SubCardIndexController<,,,>) || controllerType.BaseType == typeof(ReadOnlySubCardIndexController<,,,>))
            {
                var parentControllerType = controllerType.GenericTypeArguments.Single(s => s is ICardIndexController);

                var parentSecurityRoles = GetRequiredSecurityRolesForController(parentControllerType);

                return GetSecurityRolesForController(controllerType).Union(parentSecurityRoles).ToArray();
            }

            return GetSecurityRolesForController(controllerType);
        }

        private static SecurityRoleType[] GetSecurityRolesForController(Type controllerType)
        {
            var atomicRequiredRoleAttribute = AttributeHelper.GetClassAttributes<AtomicAuthorizeAttribute>(controllerType);
            var apiRequiredRoleAttribute = AttributeHelper.GetClassAttributes<ApiAuthorizeAttribute>(controllerType);

            if (!atomicRequiredRoleAttribute.Any() && !apiRequiredRoleAttribute.Any())
            {
                return new SecurityRoleType[] { };
            }

            return atomicRequiredRoleAttribute
                    .SelectMany(a => a.Roles)
                    .Union(apiRequiredRoleAttribute.SelectMany(a => a.Roles))
                    .ToArray();
        }
    }
}
