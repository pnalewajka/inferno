﻿using System;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;

namespace Smt.Atomic.WebApp.Helpers
{
    public static class RequestControllerHelper
    {
        public const string RequestTypeUrlParameterName = "request-type";

        public static string BuildIdentifier(RequestType type)
        {
            return $"Workflows.RequestController.{type.ToString()}";
        }

        public static Type GetControllerByType(RequestType? type)
        {
            if (!type.HasValue)
            {
                return typeof(RequestGenericController);
            }

            var identifier = BuildIdentifier(type.Value);

            return IdentifierHelper.GetTypeByIdentifier(identifier, false)
                ?? typeof(RequestGenericController);
        }

        public static RequestType ExtractRequestTypeFromUrlParameter(HttpContextBase context)
        {
            var requestParam = context.Request.QueryString[RequestTypeUrlParameterName];
            var pascalCaseEnumAsString = NamingConventionHelper.ConvertHyphenatedToPascalCase(requestParam);

            return EnumHelper.GetEnumValue<RequestType>(pascalCaseEnumAsString);
        }
    }
}