﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

namespace Smt.Atomic.WebApp.Helpers
{
    public static class CellContentHelper
    {
        public static DisplayUrl GetSkypeLink(string skypeName, string displayLabel, string cssClass = null)
        {
            if (string.IsNullOrWhiteSpace(skypeName))
            {
                return null;
            }

            return new DisplayUrl
            {
                Label = displayLabel,
                LinkCssClass = "no-click",
                CssClass = cssClass,
                IconCssClass = IconHelper.GetCssClass(FontAwesome.Skype),
                Action = new UriAction($"skype:{HttpUtility.UrlEncode(skypeName)}?chat", skipUriValidation: true)
            };
        }

        public static DisplayUrl GetEmailLink(string emailAddress, string displayLabel, string cssClass = null, string subject = null, string body = null)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return null;
            }

            var parameters = new List<string>();

            if (!string.IsNullOrWhiteSpace(subject))
            {
                parameters.Add($"subject={HttpUtility.UrlEncode(subject)}");
            }

            if (!string.IsNullOrWhiteSpace(body))
            {
                parameters.Add($"body={HttpUtility.UrlEncode(body)}");
            }

            var questionMark = !parameters.Any() ? null : "?";

            return new DisplayUrl
            {
                Label = displayLabel,
                LinkCssClass = "no-click",
                CssClass = cssClass,
                IconCssClass = IconHelper.GetCssClass(FontAwesome.EnvelopeO),
                Action = new UriAction($"mailto:{HttpUtility.UrlEncode(emailAddress)}{questionMark}{string.Join("&", parameters)}", skipUriValidation: true)
            };
        }

        public static DisplayUrl GetEmailOrPageLink(string address, string displayLabel, string linkCssClass = null, string pageCssClass = null, string subject = null, string body = null)
        {
            if (address.Contains("@"))
            {
                return GetEmailLink(address, displayLabel, linkCssClass, subject, body);
            }

            return GetPageLink(address, displayLabel, pageCssClass);
        }

        public static DisplayUrl GetPageLink(string pageUrl, string linkText, string pageCssClass = null, bool skipIcon = false)
        {
            if (string.IsNullOrWhiteSpace(pageUrl))
            {
                return null;
            }

            linkText = linkText ?? pageUrl;

            return new DisplayUrl
            {
                Label = linkText,
                LinkCssClass = "no-click",
                CssClass = pageCssClass,
                IconCssClass = skipIcon ? null : IconHelper.GetCssClass(FontAwesome.ExternalLink),
                Action = new UriAction($"{pageUrl}"),
                Target = "_blank"
            };
        }
    }
}