﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Breadcrumbs
{
    public abstract class BreadcrumbItem
    {
        private UrlHelper _urlHelper;

        protected UrlHelper UrlHelper => _urlHelper ?? (_urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext));

        protected BreadcrumbContext Context { get; set; }

        public List<BreadcrumbItemViewModel> PreviousItems { get; set; }

        public string Url { get; set; }

        [LocalizationRequired]
        public string DisplayName { get; set; }

        protected BreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
        {
            PreviousItems = new List<BreadcrumbItemViewModel>();
            Context = breadcrumbContextProvider.Context;
        }

        protected void SetItems(IList<BreadcrumbItemViewModel> items)
        {
            if (!items.Any())
            {
                return;
            }

            PreviousItems.AddRange(items.Take(items.Count - 1));

            var last = items.Last();
            SetDisplayName(last.DisplayName);
            Url = last.Url;
        }

        protected virtual void SetDisplayName(string displayName)
        {
            DisplayName = displayName;
        }

        public BreadcrumbItemViewModel ToViewModel()
        {
            return new BreadcrumbItemViewModel(Url, DisplayName);
        }
    }
}