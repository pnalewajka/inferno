﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Models;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Breadcrumbs
{
    public class BreadcrumbService : IBreadcrumbContextProvider, IBreadcrumbService
    {
        private const string BreadcrumbItemSuffix = "BreadcrumbItem";

        private readonly ILogger _logger;

        private static IReadOnlyDictionary<string, Type> _breadcrumbItemTypes;
        private readonly static object _breadcrumbItemTypesLock = new object();

        private readonly UrlHelper _urlHelper;

        protected static IReadOnlyDictionary<string, Type> BreadcrumbItemTypes
        {
            get
            {
                if (_breadcrumbItemTypes == null)
                {
                    lock (_breadcrumbItemTypesLock)
                    {
                        if (_breadcrumbItemTypes == null)
                        {
                            var webAppAssembly = typeof(MvcApplication).Assembly;
                            var types = webAppAssembly.GetTypes();
                            _breadcrumbItemTypes = types.Where(t => t.Name.EndsWith(BreadcrumbItemSuffix)).ToDictionary(t => t.Name, t => t);
                        }
                    }
                }

                return _breadcrumbItemTypes;
            }
        }

        public BreadcrumbContext Context { get; set; }

        public BreadcrumbService(ILogger logger)
        {
            _logger = logger;
            _urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
        }

        public IList<BreadcrumbItemViewModel> BuildBreadcrumbBar(string breadcrumbPath)
        {
            if (string.IsNullOrWhiteSpace(breadcrumbPath))
            {
                return new List<BreadcrumbItemViewModel>();
            }

            var types = breadcrumbPath.Split('/').Select(GetBreadcrumbItemType);
        
            return BuildBreadcrumbBar(types.ToArray());
        }

        public IList<BreadcrumbItemViewModel> BuildBreadcrumbBar([NotNull] params Type[] breadcrumbPath)
        {
            if (breadcrumbPath == null)
            {
                throw new ArgumentNullException(nameof(breadcrumbPath));
            }

            var items = breadcrumbPath.Select(t => ReflectionHelper.CreateInstanceWithIocDependencies(t)).Cast<BreadcrumbItem>();
            
            var result = new List<BreadcrumbItemViewModel>();

            foreach (var breadcrumbItem in items)
            {
                if (!breadcrumbItem.PreviousItems.IsNullOrEmpty())
                {
                    result.AddRange(breadcrumbItem.PreviousItems);    
                }

                result.Add(breadcrumbItem.ToViewModel());
            }

            return result;
        }

        public IEnumerable<BreadcrumbItemViewModel> BuildDefaultBreadcrumbBar()
        {
            var actionDescriptor = Context.ActionDescriptor;

            var actionName = actionDescriptor.ActionName;
            var controllerName = actionDescriptor.ControllerDescriptor.ControllerName;

            var references = Context.SiteMap.GetPath(controllerName, actionName);
            
            return references.Select(CreateBreadcrumbItemViewModel);
        }

        private BreadcrumbItemViewModel CreateBreadcrumbItemViewModel(NamedAction namedAction)
        {
            var url = namedAction.GetUrl(_urlHelper);
            var displayName = namedAction.GetPlainText();

            return new BreadcrumbItemViewModel(url, displayName);
        }

        private Type GetBreadcrumbItemType(string breadcrumbItemName)
        {
            var breadcrumbItemTypeName = breadcrumbItemName + BreadcrumbItemSuffix;

            Type result;

            if (BreadcrumbItemTypes.TryGetValue(breadcrumbItemTypeName, out result))
            {
                return result;                
            }

            _logger.Warn(() => $"Specific breadcrumb item not found for '{breadcrumbItemName}' element.");

            return typeof(DefaultBreadcrumbItem);
        }
    }
}