using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class CardIndexBreadcrumbItem : BreadcrumbItem
    {
        public CardIndexBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider, IBreadcrumbService breadcrumbService) : base(breadcrumbContextProvider)
        {
            var controllerTypes =  GetControllerTypes();
            var controllerName = RoutingHelper.GetControllerName(controllerTypes.First());

            var references = Context.SiteMap.GetPath(controllerName, "List").ToList();

            var items = references
                .Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText()))
                .ToList();

            var breadcrumbPath = controllerTypes
                .Skip(1)
                .Select(subcardIndexType => RoutingHelper.GetControllerName(subcardIndexType))
                .Aggregate(string.Empty, (current, breadcrumbItemTypeName) => current + breadcrumbItemTypeName);

            items.AddRange(breadcrumbService.BuildBreadcrumbBar(breadcrumbPath));

            SetItems(items);
        }

        protected virtual List<Type> GetControllerTypes()
        {
            var result = new List<Type>();

            var controllerType = GetTargetControllerType();
            result.Add(controllerType);

            while (TypeHelper.IsSubclassOfRawGeneric(controllerType, typeof (SubCardIndexController<,,,>)))
            {
                controllerType = TypeHelper.GetRawGenericArguments(controllerType, typeof (SubCardIndexController<,,,>))[2];
                result.Insert(0, controllerType);
            }

            return result;
        }

        protected virtual Type GetTargetControllerType()
        {
            return Context.Controller.GetType();
        }
    }
}