using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class StartBreadcrumbItem : BreadcrumbItem
    {
        public StartBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            var rootPage = Context.SiteMap.GetRootPageOrDefault();
            
            Url = rootPage?.GetUrl(UrlHelper);
            DisplayName = rootPage != null ? rootPage.GetPlainText() : AtomicResources.NoStartPageBreadcrumbItemName;
        }
    }
}