﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.Reporting.Models;
using Smt.Atomic.WebApp.Areas.Reporting.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class ReportDesignBreadcrumbItem : ModelBasedBreadcrumbItem
    {
        public ReportDesignBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
        }

        protected override string TitleText => ReportDefinitionResources.ReportDesignBreadcrumbTitle;

        protected override object GetViewModelRecord(IBreadcrumbContextProvider breadcrumbContextProvider)
        {
            var reportDesignViewModel = breadcrumbContextProvider.Context.ViewModel as ModelBasedDialogViewModel<ReportDesignViewModel>;

            return reportDesignViewModel?.Model;
        }
    }
}