﻿using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class GenericReportBreadcrumbItem : BreadcrumbItem
    {
        public GenericReportBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
                 var atomicController = breadcrumbContextProvider?.Context?.Controller as AtomicController;
                 DisplayName = atomicController != null ? atomicController.Layout.PageTitle : "";
         }
    }
}