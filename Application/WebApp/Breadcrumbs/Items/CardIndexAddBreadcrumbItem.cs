using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class CardIndexAddBreadcrumbItem : BreadcrumbItem
    {
        public CardIndexAddBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = HtmlMarkupHelper.RemoveAccelerator(CardIndexResources.CardIndexAddRecordButtonText);
        }
    }
}