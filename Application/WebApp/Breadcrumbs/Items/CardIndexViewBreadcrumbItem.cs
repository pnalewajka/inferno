using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class CardIndexViewBreadcrumbItem : CardIndexViewOrEditBreadcrumbItem
    {
        public CardIndexViewBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
        }

        protected override string TitleText => CardIndexResources.CardIndexViewRecordButtonText;
    }
}