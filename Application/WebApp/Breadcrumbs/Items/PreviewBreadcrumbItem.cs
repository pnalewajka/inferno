﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Survey.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class PreviewBreadcrumbItem : BreadcrumbItem
    {
        public PreviewBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = SurveyDefinitionResources.SurveyDefinitionPreview;
        }
    }
}