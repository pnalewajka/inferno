using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class CardIndexEditBreadcrumbItem : CardIndexViewOrEditBreadcrumbItem
    {
        public CardIndexEditBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
        }

        protected override string TitleText => CardIndexResources.CardIndexEditRecordButtonText;
    }
}