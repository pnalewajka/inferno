﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public abstract class ModelBasedBreadcrumbItem : BreadcrumbItem
    {
        public ModelBasedBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = GetDisplayName(breadcrumbContextProvider);
        }

        protected virtual string TitleText => string.Empty;

        protected abstract object GetViewModelRecord(IBreadcrumbContextProvider breadcrumbContextProvider);

        private string GetDisplayName(IBreadcrumbContextProvider breadcrumbContextProvider)
        {
            var title = HtmlMarkupHelper.RemoveAccelerator(TitleText);

            var viewModelRecord = GetViewModelRecord(breadcrumbContextProvider);

            if (viewModelRecord != null)
            {
                var displayName = TypeHelper.IsMethodOverridden(nameof(ToString), viewModelRecord.GetType())
                    ? string.Format(CardIndexResources.CardIndexDetailedPageTitleFormat, title, viewModelRecord.ToString())
                    : title;

                return displayName;
            }

            return title;
        }
    }
}