﻿using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class MyProfileBreadcrumbItem : BreadcrumbItem
    {
        public MyProfileBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) 
            : base(breadcrumbContextProvider)
        {
            DisplayName = HtmlMarkupHelper.RemoveAccelerator(EmployeeResources.MyProfile);
        }
    }
}