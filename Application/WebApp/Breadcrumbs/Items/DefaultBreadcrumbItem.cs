﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class DefaultBreadcrumbItem : BreadcrumbItem
    {
        public DefaultBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = AtomicResources.DefaultBreadcrumbItemName;
            Url = null;
        }
    }
}