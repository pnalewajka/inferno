﻿using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class CardIndexViewOrEditBreadcrumbItem : ModelBasedBreadcrumbItem
    {
        public CardIndexViewOrEditBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
        }

        protected override object GetViewModelRecord(IBreadcrumbContextProvider breadcrumbContextProvider)
        {
            var cardIndexRecordViewModel = breadcrumbContextProvider.Context.ViewModel as ICardIndexRecordViewModel;

            return cardIndexRecordViewModel?.ViewModelRecord;
        }
    }
}