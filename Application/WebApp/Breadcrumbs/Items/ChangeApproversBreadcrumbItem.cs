﻿using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class ChangeApproversBreadcrumbItem : BreadcrumbItem
    {
        public ChangeApproversBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            var atomicController = breadcrumbContextProvider?.Context?.Controller as AtomicController;
            DisplayName = atomicController != null ? atomicController.Layout.PageTitle : "";
        }
    }
}