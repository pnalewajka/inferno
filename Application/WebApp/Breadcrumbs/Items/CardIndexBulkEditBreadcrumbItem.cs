using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.WebApp.Breadcrumbs.Items
{
    public class CardIndexBulkEditBreadcrumbItem : BreadcrumbItem
    {
        public CardIndexBulkEditBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = HtmlMarkupHelper.RemoveAccelerator(CardIndexResources.CardIndexBulkEditButtonText);
        }
    }
}