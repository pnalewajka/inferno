using System;
using Castle.Windsor;

namespace Smt.Atomic.WebApp.Validation
{
    internal class ValidationServiceProvider : IServiceProvider
    {
        private readonly IWindsorContainer _windsorContainer;

        public ValidationServiceProvider(IWindsorContainer windsorContainer)
        {
            _windsorContainer = windsorContainer;
        }

        public object GetService(Type serviceType)
        {
            return _windsorContainer.Resolve(serviceType);
        }
    }
}