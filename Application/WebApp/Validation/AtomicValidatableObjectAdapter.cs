using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Castle.Windsor;

namespace Smt.Atomic.WebApp.Validation
{
    public class AtomicValidatableObjectAdapter : ModelValidator
    {
        private readonly ValidationServiceProvider _validationServiceProvider;

        public AtomicValidatableObjectAdapter(ModelMetadata metadata, ControllerContext context, WindsorContainer windsorContainer)
            : base(metadata, context)
        {
            _validationServiceProvider = new ValidationServiceProvider(windsorContainer);
        }

        public override IEnumerable<ModelValidationResult> Validate(object container)
        {
            object model = Metadata.Model;
            
            if (model == null)
            {
                return Enumerable.Empty<ModelValidationResult>();
            }

            var validatable = model as IValidatableObject;
            
            if (validatable == null)
            {
                throw new InvalidOperationException("The model is not of validatable type");
            }

            var validationContext = new ValidationContext(validatable, _validationServiceProvider, null);
            var validationResults = validatable.Validate(validationContext);
            
            return ConvertResults(validationResults);
        }

        private IEnumerable<ModelValidationResult> ConvertResults(IEnumerable<ValidationResult> results)
        {
            foreach (ValidationResult result in results)
            {
                if (result != ValidationResult.Success)
                {
                    if (result.MemberNames == null || !result.MemberNames.Any())
                    {
                        yield return new ModelValidationResult { Message = result.ErrorMessage };
                    }
                    else
                    {
                        foreach (string memberName in result.MemberNames)
                        {
                            yield return new ModelValidationResult { Message = result.ErrorMessage, MemberName = memberName };
                        }
                    }
                }
            }
        }
    }
}