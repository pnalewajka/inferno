using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Castle.Windsor;

namespace Smt.Atomic.WebApp.Validation
{
    public class AtomicModelValidator : DataAnnotationsModelValidator
    {
        private readonly ValidationServiceProvider _validationServiceProvider;

        public AtomicModelValidator(
            ModelMetadata metadata,
            ControllerContext context,
            ValidationAttribute attribute,
            WindsorContainer windsorContainer)
            : base(metadata, context, attribute)
        {
            _validationServiceProvider = new ValidationServiceProvider(windsorContainer);
        }

        public override IEnumerable<ModelValidationResult> Validate(object container)
        {
            ValidationContext context = CreateValidationContext(container);
            ValidationResult result = Attribute.GetValidationResult(Metadata.Model, context);

            if (result != ValidationResult.Success)
            {
                if (result != null)
                {
                    yield return new ModelValidationResult
                    {
                        Message = result.ErrorMessage
                    };
                }
            }
        }

        protected virtual ValidationContext CreateValidationContext(object container)
        {
            var context = new ValidationContext(container ?? Metadata.Model, _validationServiceProvider, null)
            {
                DisplayName = Metadata.GetDisplayName()
            };

            return context;
        }
    }
}