using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Core.Logging;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Services;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Validation;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using StackExchange.Profiling;
using StackExchange.Profiling.EntityFramework6;
using StackExchange.Profiling.Mvc;

#if !DEBUG
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
#endif

namespace Smt.Atomic.WebApp
{
    public class MvcApplication : HttpApplication
    {
        private static RequestAuthenticationHandler _requestAuthenticationHandler;
        private static WindsorContainer _container;

        protected void Application_Start() 
        {
            //Microsoft.SqlServer.Types require this to work correctly with ASP.NET applications. See readme.htm for details
            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));

            var containerConfiguration = new WebAppContainerConfiguration();
            _container = containerConfiguration.Configure(ContainerType.WebApp);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var logger = _container.Resolve<ILogger>();
            var settingsProvider = _container.Resolve<ISettingsProvider>();
            GlobalFilters.Filters.Add(new GeneralErrorHandler(logger, settingsProvider));

            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
            _requestAuthenticationHandler = new RequestAuthenticationHandler(_container);

            ConfigureModelBinders();

            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(_container.Kernel));

#if !DEBUG
            Layout.CompileTemplates();
#endif
            
            //change default validation factories so that service locator is available for validators
            DataAnnotationsModelValidatorProvider.RegisterDefaultAdapterFactory((metadata, context, attribute) => new AtomicModelValidator(metadata, context, attribute, _container));
            DataAnnotationsModelValidatorProvider.RegisterDefaultValidatableObjectAdapterFactory((metadata, context) => new AtomicValidatableObjectAdapter(metadata, context, _container));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(
            typeof(RequiredAttribute), typeof(CustomRequiredAttributeAdapter));

            //configure Miniprofiler
            var currentEnginesList = ViewEngines.Engines.ToList();

            MiniProfilerEF6.Initialize();
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.AddRange(currentEnginesList.Select(x => new ProfilingViewEngine(x)));
            MiniProfiler.Settings.Results_Authorize = AtomicCodeProfilerHelper.IsUserAllowedToSeeMiniProfilerUi;
        }

        protected void Application_Error()
        {
            const string controllerKey = "controller";
            const string actionKey = "action";

            var exception = Server.GetLastError();
            if (exception == null)
            {
                return;
            }

            var httpException = exception as HttpException;
            var statusCode = httpException?.GetHttpCode() ?? 500;

            Response.Clear();
            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;

            var routeData = new RouteData();

            routeData.Values[controllerKey] = RoutingHelper.GetControllerName<ErrorController>();
            routeData.Values[ErrorController.ExceptionKey] = exception;

            switch (statusCode)
            {
                case (int)HttpStatusCode.NotFound:
                    routeData.Values[actionKey] = MethodHelper.GetMethodName<ErrorController>(c => c.PageNotFound());
                    break;

                default:
                    routeData.Values[actionKey] = MethodHelper.GetMethodName<ErrorController>(c => c.OtherError());
                    break;
            }

            Response.StatusCode = statusCode;

            var errorController = ReflectionHelper.CreateInstanceWithIocDependencies<ErrorController>() as IController;
            var requestContext = new RequestContext(new HttpContextWrapper(Context), routeData);
            errorController.Execute(requestContext);

            Response.End();
        }

        protected void Application_BeginRequest()
        {
            if (AtomicCodeProfilerHelper.IsProfilerEnabled)
            {
                MiniProfiler.Start();
            }

            var currentContext = _container.Resolve<HttpCurrentContextDataService>();
            currentContext.SetNewRequestId();

            if (ActivityTrackingHelper.GetActivityIdFromActivityTrackingCookie(currentContext.Request) == null)
            {
                var cookie = ActivityTrackingHelper.CreateActivityTrackingCookie();
                currentContext.Request.Cookies.Add(cookie);
                currentContext.Response.Cookies.Add(cookie);
            }
        }

        protected void Application_EndRequest()
        {
            if (AtomicCodeProfilerHelper.IsProfilerEnabled)
            {
                MiniProfiler.Stop();
            }
        }

        private void Application_PostAuthenticateRequest(object sender, EventArgs eventArgs)
        {
            //this handler subscription cannot be moved into RequestAuthenticationHandler as it is causing
            //error due to Global.asax event handler quirks: 
            _requestAuthenticationHandler.HandlePostAuthenticateRequest(this);
        }

        private static void ConfigureModelBinders()
        {
            ModelBinders.Binders.DefaultBinder = new AtomicModelBinder();
            DefaultModelBinder.ResourceClassKey = nameof(ModelValidationMessages);
            RegisterBinder<CommaSeparatedListModelBinder>();
            RegisterBinder<CommaSeparatedArrayModelBinder>();
            RegisterBinder<SemanticTypeModelBinder>();
            RegisterBinder<GeographicLocationViewModelModelBinder>();
        }

        private static void RegisterBinder<TBinder>() where TBinder : IAtomicModelBinder, new()
        {
            new TBinder().Register(ModelBinders.Binders);
        }
    }
}