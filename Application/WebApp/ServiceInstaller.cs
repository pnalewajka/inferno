﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Common.ViewModels;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Breadcrumbs;
using Smt.Atomic.WebApp.Services;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<BaseRequestControllerDependencies>().LifestylePerWebRequest());

            if (containerType != ContainerType.JobScheduler)
            {
                container.Register
                    (
                        Classes.FromThisAssembly().BasedOn<IController>().LifestylePerWebRequest(),
                        Component.For<IWebApplicationSettings>()
                            .ImplementedBy<WebApplicationSettings>()
                            .LifestyleTransient(),
                        Component.For<IAlertService>().ImplementedBy<AlertService>().LifestyleTransient(),
                        Component.For<IBaseControllerDependencies>()
                            .ImplementedBy<BaseControllerDependencies>()
                            .LifestyleTransient(),
                        Component.For<IBreadcrumbService, IBreadcrumbContextProvider>()
                            .ImplementedBy<BreadcrumbService>()
                            .LifestylePerWebRequest(),
                        Component.For<IReportRetrievalService>()
                            .ImplementedBy<ReportRetrievalService>()
                            .LifestyleTransient(),
                        Classes.FromThisAssembly()
                            .BasedOn(typeof(IViewModelFactory<>))
                            .WithService.AllInterfaces()
                            .LifestylePerWebRequest(),
                        Classes.FromThisAssembly()
                            .BasedOn<IUrlTokenResolver>()
                            .WithServiceFromInterface()
                            .LifestyleTransient(),
                        Component.For<IClassMapping<ChoreDto, ChoreViewModel>>().ImplementedBy<ProxyChoreDtoToChoreViewModelMapping>().IsDefault().LifestyleTransient(),
                        Classes.FromThisAssembly().BasedOn<ChoreDtoToChoreViewModelMapping>().WithServiceBase().LifestyleTransient()
                    );
            }
        }
    }
}