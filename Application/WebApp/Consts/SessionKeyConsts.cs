﻿namespace Smt.Atomic.WebApp.Consts
{
    internal class SessionKeyConsts
    {
        /// <summary>
        /// Key for user validation data storage
        /// </summary>
        public const string UserValidationDataSessionKey = "UserValidationDataSessionKey";
    }
}