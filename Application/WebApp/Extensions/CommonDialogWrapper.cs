﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;

namespace Smt.Atomic.WebApp.Extensions
{
    public class CommonDialogWrapper : IDisposable
    {
        private readonly DialogViewModel _dialogModel;
        private readonly TextWriter _writer;
        private bool _hasMovedOnToFooter;

        public CommonDialogWrapper(DialogViewModel dialogModel, ViewContext viewContext)
        {
            _dialogModel = dialogModel;
            _writer = viewContext.Writer;
        }

        public void Dispose()
        {
            WriteClosingSection();
        }

        public void WriteOpeningSection()
        {
            var classNames = new List<string>();

            if (_dialogModel.HasIcon)
            {
                classNames.Add("has-icon");
            }

            if (_dialogModel.ClassName != null)
            {
                classNames.Add(_dialogModel.ClassName);
            }

            var classNameInsert = classNames.Any() ? $" {string.Join(" ", classNames)}" : string.Empty;
            var isErrorInsert = _dialogModel.IsErrorMessage ? " data-is-error=\"1\"" : string.Empty;

            _writer.WriteLine("<div id=\"" + _dialogModel.DialogId + "\" class=\"modal common-dialog"
                              + classNameInsert + "\"" + isErrorInsert + ">");

            _writer.WriteLine("<div class=\"modal-dialog\">");
            _writer.WriteLine("<div class=\"modal-content\">");
            _writer.WriteLine("<div class=\"modal-header\">");
            _writer.WriteLine(
                "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>");
            _writer.WriteLine("<h4 class=\"modal-title\">" + (_dialogModel.Title ?? string.Empty) + "</h4>");

            _writer.WriteLine("</div>");
            _writer.WriteLine("<div class=\"modal-body\">");

            if (_dialogModel.HasIcon)
            {
                _writer.WriteLine("<div class=\"dialog-icon\">");
                _writer.WriteLine("<i class=\"" + IconHelper.GetCssClass(_dialogModel.IconName) +
                                  " aria-hidden=\"true\"></i>");
                _writer.WriteLine("</div>");
            }

            _writer.WriteLine("<div class=\"html-message\">");
        }

        public void MoveOnToFooter()
        {
            if (!_hasMovedOnToFooter)
            {
                _writer.WriteLine("</div>");

                if (_dialogModel.Buttons.Any())
                {
                    _writer.WriteLine("</div>");
                    _writer.WriteLine("<div class=\"modal-footer\">");
                }
            }

            _hasMovedOnToFooter = true;
        }

        private void WriteClosingSection()
        {
            MoveOnToFooter();

            var dialogButtons = _dialogModel.Buttons
                                            .Where(b => !string.IsNullOrWhiteSpace(b.Text))
                                            .ToList();

            _writer.WriteLine("<div class=\"pull-right\">");

            for (var i = 0; i < dialogButtons.Count; i++)
            {
                var button = dialogButtons[i];

                var className = i == 0 ? "btn-primary" : "btn-default";
                var attributeInsert = string.Empty;

                if (button.IsDisabled)
                {
                    className += " disabled";
                    attributeInsert += " disabled";
                }

                if (button.IsDismissButton)
                {
                    attributeInsert += " data-dismiss=\"modal\"";
                }

                if (!string.IsNullOrWhiteSpace(button.Hotkey))
                {
                    attributeInsert += $" data-hotkey=\"{button.Hotkey}\"";
                }

                var buttonHtml = new StringBuilder();

                buttonHtml.Append("<button type=\"button\"");
                buttonHtml.Append(" class=\"btn " + className + "\"");
                buttonHtml.Append(attributeInsert);
                buttonHtml.Append(" data-button=\"" + button.Tag + "\">");
                buttonHtml.Append(button.Text + "</button>");

                _writer.WriteLine(buttonHtml.ToString());
            }

            _writer.WriteLine("</div>");
            _writer.WriteLine("</div>");
            _writer.WriteLine("</div>");
            _writer.WriteLine("</div>");
            _writer.WriteLine("</div>");
        }
    }
}