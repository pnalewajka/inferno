﻿using System;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Models.Layout;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using System.Text.RegularExpressions;

namespace Smt.Atomic.WebApp.Extensions
{
    /// <summary>
    /// Extension methods which facilitate generating HTML code on the basis of HtmlHelper class
    /// </summary>
    public static class HtmlHelperExtensions
    {
        public static CommonDialogWrapper BeginCommonDialog(this HtmlHelper htmlHelper, DialogViewModel dialogModel)
        {
            var modalWrapper = new CommonDialogWrapper(dialogModel, htmlHelper.ViewContext);

            modalWrapper.WriteOpeningSection();

            return modalWrapper;
        }

        public static MvcHtmlString GlobalDataScript(this HtmlHelper htmlHelper)
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            var layoutModel = htmlHelper.ViewData.GetLayoutModel();

            var footerResources = layoutModel.GetGlobalDataScript(urlHelper);

            return new MvcHtmlString(footerResources);
        }

        public static MvcHtmlString FooterWebResources(this HtmlHelper htmlHelper)
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            var layoutModel = GetLayoutModel(htmlHelper);

            var footerResources = layoutModel.GetFooterWebResources(urlHelper);

            return new MvcHtmlString(footerResources);
        }

        public static string ValidationSummaryAsAlerts(this HtmlHelper htmlHelper, bool excludePropertyErrors)
        {
            var viewData = htmlHelper.ViewData;
            var modelState = viewData.ModelState;

            var errors = modelState
                .Where(p => !excludePropertyErrors || p.Key == string.Empty)
                .SelectMany(p => p.Value.Errors)
                .Select(e => new AlertViewModel
                    {
                        IsDismissable = true,
                        Type = AlertType.Error,
                        Message = e.ErrorMessage
                    });

            var layoutModel = GetLayoutModel(htmlHelper);

            layoutModel.Alerts.AddRange(errors);

            return string.Empty;
        }

        /// <summary>
        /// Convert new line characters \r\n and \n to &lt;br&gt; html tag
        /// </summary>
        /// <param name="htmlHelper">HtmlHelper class instance</param>
        /// <param name="input">string to convert</param>
        /// <returns>string with converted new line characters</returns>
        public static MvcHtmlString ConvertNewLinesToBrTag(this HtmlHelper htmlHelper, string input)
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            return new MvcHtmlString(Regex.Replace(htmlHelper.Encode(input), @"\r\n?|\n", "<br>"));
        }

        private static UrlHelper GetUrlHelper(HtmlHelper htmlHelper)
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            return urlHelper;
        }

        private static LayoutViewModel GetLayoutModel(HtmlHelper htmlHelper)
        {
            return htmlHelper.ViewData.GetLayoutModel();
        }

        /// <summary>
        /// Remove Html tags and replace Html special entities with coresponding signs
        /// </summary>
        /// <param name="input">string to convert</param>
        /// <returns>converted string without Html tags and Html specific entities</returns>
        public static string StripHtml(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            return
                Regex.Replace(input, "<.*?>", String.Empty)
                .Replace("&nbsp;", " ")
                .Replace("&quot;", "\"")
                .Replace("&amp;", "&")
                .Replace("&lt;", "<")
                .Replace("&gt;", ">");
        }
    }
}