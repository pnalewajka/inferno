﻿using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Models.Layout;

namespace Smt.Atomic.WebApp.Extensions
{
    public static class ViewDataExtensions
    {
        private const string LayoutViewModelKey = "LayoutViewModel";

        public static LayoutViewModel GetLayoutModel([NotNull]this ViewDataDictionary viewData, bool allowNullModel = false)
        {
            if (viewData == null)
            {
                throw new ArgumentNullException(nameof(viewData));
            }

            var layoutModel = viewData[LayoutViewModelKey] as LayoutViewModel;

            if (layoutModel == null && !allowNullModel)
            {
                throw new NullReferenceException("Layout model is null. Page controller should inherit from AtomicController or provide a valid LayoutViewModel object.");
            }

            return layoutModel;
        }

        public static void SetLayoutModel([NotNull]this ViewDataDictionary viewData, LayoutViewModel layoutModel)
        {
            if (viewData == null)
            {
                throw new ArgumentNullException(nameof(viewData));
            }

            viewData[LayoutViewModelKey] = layoutModel;
        }

        public static LayoutViewModel AssureLayoutModel([NotNull]this ViewDataDictionary viewData)
        {
            if (viewData == null)
            {
                throw new ArgumentNullException(nameof(viewData));
            }

            var layout = viewData.GetLayoutModel(true);

            if (layout == null)
            {
                viewData[LayoutViewModelKey] = layout = GetDefaultLayoutModel();
            }

            return layout;
        }

        private static LayoutViewModel GetDefaultLayoutModel()
        {
            var atomicController = ReflectionHelper.CreateInstanceWithIocDependencies<AtomicController>();
            return atomicController.Layout;
        }
    }
}