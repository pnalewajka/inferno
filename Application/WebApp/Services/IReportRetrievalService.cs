﻿using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.Dto;

namespace Smt.Atomic.WebApp.Services
{
    public interface IReportRetrievalService
    {
        IList<ReportDefinitionDto> GetRows(string parameters);
    }
}
