﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models.Alerts;

namespace Smt.Atomic.WebApp.Services
{
    internal class AlertService : IAlertService
    {
        private const string AlertSessionKey = "UIAlertList";

        private HttpSessionState Session => HttpContext.Current.Session;

        public void AddAlert(AlertType alertType, string message, bool isDismisable = true)
        {
            var alerts = Session[AlertSessionKey] as IList<AlertViewModel> ?? new List<AlertViewModel>();

            var alert = new AlertViewModel
            {
                Message = message,
                IsDismissable = isDismisable,
                Type = alertType
            };
            alerts.Add(alert);
            Session[AlertSessionKey] = alerts;
        }

        public void AddSuccess(string message, bool isDismisable = true)
        {
            AddAlert(AlertType.Success, message, isDismisable);
        }

        public void AddInformation(string message, bool isDismisable = true)
        {
            AddAlert(AlertType.Information, message, isDismisable);
        }

        public void AddWarning(string message, bool isDismisable = true)
        {
            AddAlert(AlertType.Warning, message, isDismisable);
        }

        public void AddError(string message, bool isDismisable = true)
        {
            AddAlert(AlertType.Error, message, isDismisable);
        }

        public List<AlertViewModel> GetAlerts()
        {
            var alerts = (Session == null ? null : Session[AlertSessionKey]) as List<AlertViewModel>;
            
            return alerts ?? new List<AlertViewModel>();
        }

        public void ClearAlerts()
        {
            Session[AlertSessionKey] = null;            
        }
    }
}