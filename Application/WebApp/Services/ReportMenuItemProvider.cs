using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.SiteMap.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.WebApp.Services
{
    [Identifier("MenuItemProviders.Reports")]
    public class ReportMenuItemProvider : IMenuItemProviderService
    {
        private readonly IReportRetrievalService _reportRetrivalService;

        public ReportMenuItemProvider(IReportRetrievalService reportRetrievalService)
        {
            _reportRetrivalService = reportRetrievalService;
        }

        public IList<MenuItem> GetItems(string parameters)
        {
            var reportRecords = _reportRetrivalService.GetRows(parameters);

            return reportRecords
                .Select(r => new LocalizedMenuItem(r.Name.ToString())
                {
                    AreaName = "Reporting",
                    ControllerName = "ReportExecution",
                    ActionName = "ConfigureReport",
                    ActionParameters = $"reportCode={r.Code}",
                    SiteMapRoles = $"{r.RequiredRole}",
                    AllowedForProfiles = string.Join(",", r.AllowedForProfileNames),
                    GroupName = r.Group
                })
                .Cast<MenuItem>()
                .OrderBy(i => i.GroupName)
                .ThenBy(i => i.Text)
                .ToList();
        }
    }
}
