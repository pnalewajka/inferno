﻿using System.Collections.Generic;
using Castle.Core;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;

namespace Smt.Atomic.WebApp.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class ReportRetrievalService : IReportRetrievalService
    {
        private const int MaxReportCount = 100;
        private readonly IReportDefinitionCardIndexDataService _reportCardIndexDataService;

        public ReportRetrievalService(IReportDefinitionCardIndexDataService reportCardIndexDataService)
        {
            _reportCardIndexDataService = reportCardIndexDataService;
        }

        [Cached(CacheArea = CacheAreas.ReportDefinitions)]
        public IList<ReportDefinitionDto> GetRows(string parameters)
        {
            var searchCriteria = new SearchCriteria();

            searchCriteria.Codes.Add(SearchAreaCodes.Area);

            var criteria = new QueryCriteria
            {
                PageSize = MaxReportCount,
                SearchCriteria = searchCriteria,
                SearchPhrase = parameters
            };

            return _reportCardIndexDataService.GetRecords(criteria).Rows;
        }
    }
}