﻿using System;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Services
{
    internal class WebApplicationSettings : IWebApplicationSettings
    {
        public string LogoUrl => "~/Content/images/dante-logo.png";

        public string ProjectName => RenderersResources.DefaultProjectName;

        public string DefaultPageTitle => RenderersResources.DefaultPageTitle;

        public string DefaultPageHeaderViewName => "~/Views/Shared/_Header.cshtml";

        public string DefaultPageHeader => RenderersResources.DefaultPageHeader;

        public string SiteMapPath => "Smt.Atomic.WebApp.sitemap.xml";

        public Type SiteMapResources => typeof(MenuResources);

        public string MasterName => "~/Views/Shared/_Layout.cshtml";

        public string DefaultAnonymousLandingPageUrl => "~/Website/Home/Index";

        public string DefaultAuthorizedLandingPageUrl => "~/Accounts/Authentication/Index";
    }
}