using System;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Interfaces;

namespace Smt.Atomic.WebApp.Services
{
    internal class BaseControllerDependencies : IBaseControllerDependencies
    {
        public IAlertService AlertService { get; private set; }
        public ISystemParameterService SystemParameterService { get; set; }
        public IExportService ExportService { get; set; }
        public ISettingsProvider SettingsProvider { get; set; }
        public IBreadcrumbService BreadcrumbService { get; set; }
        public ISiteMapService SiteMapService { get; private set; }
        public IPrincipalProvider PrincipalProvider { get; private set; }
        public IClassMappingFactory ClassMappingFactory { get; private set; }
        public IWebApplicationSettings WebApplicationSettings { get; private set; }
        public IImportService ImportService { get; set; }
        public IAnonymizationService AnonymizationService { get; private set; }
        public IDtoPropertyNameCardIndexDataService FieldNameService { get; private set; }
        public IUserChoreProvider UserChoresProvider { get; private set; }

        public BaseControllerDependencies(
            ISiteMapService siteMapService, 
            IPrincipalProvider principalProvider,
            IAlertService alertService,
            ISystemParameterService systemParameterService,
            IExportService exportService,
            ISettingsProvider settingsProvider,
            IBreadcrumbService breadcrumbService,
            IClassMappingFactory classMappingFactory, 
            IWebApplicationSettings webApplicationSettings,
            IImportService importService,
            IAnonymizationService anonymizationService,
            IDtoPropertyNameCardIndexDataService fieldNameService,
            IUserChoreProvider userChoresProvider)
        {
            WebApplicationSettings = webApplicationSettings;
            ImportService = importService;
            AnonymizationService = anonymizationService;
            SystemParameterService = systemParameterService;
            SiteMapService = siteMapService;
            PrincipalProvider = principalProvider;
            AlertService = alertService;
            ExportService = exportService;
            SettingsProvider = settingsProvider;
            BreadcrumbService = breadcrumbService;
            ClassMappingFactory = classMappingFactory;
            FieldNameService = fieldNameService;
            UserChoresProvider = userChoresProvider;
        }
    }
}