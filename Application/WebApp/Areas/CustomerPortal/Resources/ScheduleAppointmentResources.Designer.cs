﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ScheduleAppointmentResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ScheduleAppointmentResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.CustomerPortal.Resources.ScheduleAppointmentResources", typeof(ScheduleAppointmentResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        internal static string AppointmentStatus {
            get {
                return ResourceManager.GetString("AppointmentStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approve.
        /// </summary>
        internal static string ApproveAppointment {
            get {
                return ResourceManager.GetString("ApproveAppointment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date.
        /// </summary>
        internal static string DateName {
            get {
                return ResourceManager.GetString("DateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End time.
        /// </summary>
        internal static string EndDateName {
            get {
                return ResourceManager.GetString("EndDateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Schedule appointments.
        /// </summary>
        internal static string PageTitle {
            get {
                return ResourceManager.GetString("PageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        internal static string RecommendationName {
            get {
                return ResourceManager.GetString("RecommendationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Re-send confirmation.
        /// </summary>
        internal static string ResendEmailConfirmationn {
            get {
                return ResourceManager.GetString("ResendEmailConfirmationn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        internal static string ScheduleAppointmentStatusViewModelStatusLabel {
            get {
                return ResourceManager.GetString("ScheduleAppointmentStatusViewModelStatusLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start time.
        /// </summary>
        internal static string StartDateName {
            get {
                return ResourceManager.GetString("StartDateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        internal static string StatusFilter {
            get {
                return ResourceManager.GetString("StatusFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        internal static string StatusFilterGroup {
            get {
                return ResourceManager.GetString("StatusFilterGroup", resourceCulture);
            }
        }
    }
}
