﻿module ResourceForms {
    import StandardButtons = Dialogs.StandardButtons;

    export function showApproveDialog(row: Grid.IRow, modelIdentifier: string, title: string, postUrl: string): void {
        Dialogs.showModel({
            modelId: modelIdentifier,
            title: title,
            eventHandler: eventArgs => {
                if (eventArgs.isOk) {
                    eventArgs.model.ResourceFormId = row.id;
                    $.post(`${postUrl}?id=${row.id}`, eventArgs.model).then(() => {
                        window.location.reload(true);
                    });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function askForDetails(row: Grid.IRow, modelIdentifier: string, title: string, postUrl: string): void {
        Dialogs.showModel({
            modelId: modelIdentifier,
            title: title,
            eventHandler: eventArgs => {
                if (eventArgs.isOk) {
                    eventArgs.model.ResourceFormId = row.id;
                    $.post(`${postUrl}?id=${row.id}`, eventArgs.model).then(() => {
                        window.location.reload(true);
                    });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function openCrm(row: any, addressFormat: string) {
        var address = Strings.format(addressFormat, row.neededResourceCrmCode);
        window.open(address, "_blank");
    }
}