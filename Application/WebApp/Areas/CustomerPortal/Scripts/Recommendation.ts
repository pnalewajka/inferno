﻿namespace Recommendations {
    import StandardButtons = Dialogs.StandardButtons;

    export function openSendToCustomerDialog(row: Grid.IRow, modelId: string, title: string, saveUrl: string): void {
        Dialogs.showModel({
            modelId: modelId, title: title, eventHandler: eventArgs => {
                CommonDialogs.pleaseWait.show();
                $.post(saveUrl + window.location.search, $.extend({}, eventArgs.model, { Id: row.id })).then(() => {
                    window.location.reload(true);
                });
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });

    }
}
