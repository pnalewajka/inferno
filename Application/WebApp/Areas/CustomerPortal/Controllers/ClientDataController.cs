﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;
using System;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageClientData)]
    public class ClientDataController : CardIndexController<ClientDataViewModel, ClientDataDto>
    {
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IPrincipalProvider _principalProvider;

        public ClientDataController(
            IClientDataCardIndexDataService cardIndexDataService, 
            IBusinessEventPublisher businessEventPublisher, 
            IBaseControllerDependencies dependencies, 
            IPrincipalProvider principalProvider)
            : base(cardIndexDataService, dependencies)
        {
            _businessEventPublisher = businessEventPublisher;
            _principalProvider = principalProvider;

            CardIndex.Settings.Title = ClientDataResource.PageTitle;

            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.DefaultNewRecord =
                () => new ClientDataViewModel
                {
                    ManagerUserId = _principalProvider.Current.Id ?? 0
                };

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ClientDataResource.RefreshClientData,
                OnClickAction = Url.UriActionGet(nameof(RefreshRecomendations), KnownParameter.SelectedIds),
                Icon = FontAwesome.Refresh,
                RequiredRole = SecurityRoleType.CanManageNeededResources,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                }
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = ClientDataResource.FilterPerson,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<ClientUserFilterViewModel>(
                            FilterCodes.ManagerUser,
                            NeededResourceResources.FilterManager)
                    }
                }
            };
        }


        public ActionResult RefreshRecomendations(long[] ids)
        {
            foreach (var clientDataId in ids)
            {
                _businessEventPublisher.PublishBusinessEvent(new TriggerCrmClientSynchronizationEvent { ClientDataId = clientDataId });
            }

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        public override ActionResult Delete([ModelBinder(typeof(CommaSeparatedListModelBinder))] IList<long> ids)
        {
            throw new NotImplementedException();
        }
    }
}
