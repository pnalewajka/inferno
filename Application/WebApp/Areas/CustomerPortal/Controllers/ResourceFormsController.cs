﻿using System.Web.Http;
using System.Web.Mvc;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageResourceForms)]
    public class ResourceFormsController : CardIndexController<ResourceFormViewModel, ResourceFormDto>
    {
        private readonly IResourceFormDataService _cardIndexDataService;

        public ResourceFormsController(IResourceFormDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            ISystemParameterService systemParameterService
            )
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.Title = ResourceFormResources.PageTitle;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ResourceFormResources.Approve,
                OnClickAction = new JavaScriptCallAction($"ResourceForms.showApproveDialog(row, '{ApproveResourceRequestViewModel.ModelIdentifier}', '{ResourceFormResources.ApproveDialogTitle}', '{Url.Action("ApproveResourceRequestDialog")}')"),
                Icon = FontAwesome.Check,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.state == { (int)ResourceFormState.Submitted }"
                }
            });

            var crmUrlFormat = systemParameterService.GetParameter<string>(ParameterKeys.CrmNeededResourceUrlFormat);

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ResourceFormResources.OpenInCrm,
                OnClickAction = new JavaScriptCallAction($"ResourceForms.openCrm(row, '{crmUrlFormat}')"),
                Icon = FontAwesome.FolderOpen,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "!!row.neededResourceCrmCode"
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ResourceFormResources.AskForDetails,
                OnClickAction = new JavaScriptCallAction($"ResourceForms.askForDetails(row, '{AskForDetailsViewModel.ModelIdentifier}', '{ResourceFormResources.AskForDetails}', '{Url.Action("AskForDetails")}')"),
                Icon = FontAwesome.Question,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.state == { (int)ResourceFormState.Submitted }"
                }
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = RecommendationResources.StatusFilterGroup,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.ResourceFormState,
                            DisplayName = ResourceFormResources.PendingRequestsFilter
                        },
                        new Filter
                        {
                            Code = FilterCodes.ResourceFormInCRM,
                            DisplayName = ResourceFormResources.InCrmFilter
                        }
                    },
                },
            };
        }

        [System.Web.Mvc.HttpPost]
        public void AskForDetails(long id, [FromBody]AskForDetailsViewModel model)
        {
            _cardIndexDataService.AskForDetails(id, model.Question);
        }

        public override ActionResult List(GridParameters parameters)
        {
            Layout.Scripts.Add("~/Areas/CustomerPortal/Scripts/ResourceForms.js");
            return base.List(parameters);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult ApproveResourceRequestDialog([FromBody]ApproveResourceRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                _cardIndexDataService.ApproveResourceForm(model.ResourceFormId, model);
            }

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }
    }
}