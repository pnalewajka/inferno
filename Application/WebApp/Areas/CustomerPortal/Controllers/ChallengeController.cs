﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using Smt.Atomic.WebApp.Controllers;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize]
    public class ChallengeController : SubCardIndexController<ChallengeViewModel, ChallengeDto, NeededResourcesController>
    {
        private readonly IChallengeCardIndexDataService _cardIndexDataService;

        public ChallengeController(
            IChallengeCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.Title = ChallengeResources.PageTitle;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = ChallengeResources.AnswersFilterGroup,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.HasAnswers,
                            DisplayName = ChallengeResources.HasAnswersFilter
                        },
                        new Filter
                        {
                            Code = FilterCodes.NoAnswers,
                            DisplayName = ChallengeResources.NoAnswersFilter,
                            
                        }
                    },
                },
            };
        }
    }
}