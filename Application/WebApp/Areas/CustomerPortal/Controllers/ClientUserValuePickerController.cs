﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageClientData)]
    [Identifier("ValuePickers.RecommendationClientUser")]
    public class ClientUserValuePickerController : ReadOnlyCardIndexController<UserViewModel, UserDto, ParentIdContext>
    {
        public ClientUserValuePickerController(
            IClientUserCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}