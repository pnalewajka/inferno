﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using Smt.Atomic.WebApp.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageNeededResourcesInterviews)]
    public class ScheduleAppointmentController : SubCardIndexController<ScheduleAppointmentViewModel, ScheduleAppointmentDto, NeededResourcesController>
    {
        private readonly IRecomendationScheduleAppointmentsService _recomendationScheduleAppointmentsService;

        public ScheduleAppointmentController(IScheduleAppointmentCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies, IRecomendationScheduleAppointmentsService recomendationScheduleAppointmentsService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _recomendationScheduleAppointmentsService = recomendationScheduleAppointmentsService;
            CardIndex.Settings.Title = ScheduleAppointmentResources.PageTitle;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ScheduleAppointmentResources.ApproveAppointment,
                OnClickAction = Url.UriActionGet("Approve", "ScheduleAppointment", KnownParameter.ParentId),
                Icon = FontAwesome.Check,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.appointmentStatus == {(int)AppointmentStatusType.Pending}"
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ScheduleAppointmentResources.ResendEmailConfirmationn,
                OnClickAction = Url.UriActionGet("ReSendNotification", "ScheduleAppointment", KnownParameter.ParentId),
                Icon = FontAwesome.MailForward,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.appointmentStatus == {(int)AppointmentStatusType.Accepted}"
                }
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = ScheduleAppointmentResources.StatusFilterGroup,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<ScheduleAppointmentStatusViewModel>
                        ( FilterCodes.AppointmentStatus,
                            ScheduleAppointmentResources.StatusFilter),
                    },
                },
            };
        }

        public ActionResult Approve([ModelBinder(typeof(ParentIdModelBinder))]long appointmentId)
        {
            var recommendationId = _recomendationScheduleAppointmentsService.ApproveAppointment(appointmentId);
            return Redirect(Url.Action("List", new RouteValueDictionary { { GridParameterModelBinder.ContextParentIdParameterUrlName, recommendationId } }));
        }

        public ActionResult ReSendNotification([ModelBinder(typeof(ParentIdModelBinder))] long appointmentId)
        {
            var recommendationId = _recomendationScheduleAppointmentsService.ReSendNotification(appointmentId);
            return Redirect(Url.Action("List", new RouteValueDictionary { { GridParameterModelBinder.ContextParentIdParameterUrlName, recommendationId } }));
        }
    }

    public class ParentIdModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return long.Parse(controllerContext.RequestContext.HttpContext.Request[GridParameterModelBinder.ContextParentIdParameterUrlName]);
        }
    }
}
