﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using Smt.Atomic.WebApp.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageNeededResources)]
    public class NeededResourcesController : CardIndexController<NeededResourceViewModel, NeededResourceDto>
    {
        private readonly INeededResourceCardIndexDataService _cardIndexDataService;

        public NeededResourcesController(INeededResourceCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.OnClickAction = Url.UriActionGet(
                nameof(ResourceRecomendationsController.List),
                RoutingHelper.GetControllerName<ResourceRecomendationsController>(),
                KnownParameter.ParentId);

            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = NeededResourceResources.PageTitle;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = NeededResourceResources.SeeInterviews,
                OnClickAction = Url.UriActionGet(nameof(List), "ScheduleAppointment", KnownParameter.ParentId),
                Icon = FontAwesome.Calendar,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = NeededResourceResources.SeeChallenges,
                OnClickAction = Url.UriActionGet(nameof(List), "Challenge", KnownParameter.ParentId, "filter", FilterCodes.NoAnswers),
                Icon = FontAwesome.Calendar,
                RequiredRole = SecurityRoleType.CanManageNeededResourcesInterviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = NeededResourceResources.ResumeRecommendations,
                OnClickAction = Url.UriActionPost(nameof(ResumeRecommendations), KnownParameter.SelectedId),
                Icon = FontAwesome.Play,
                RequiredRole = SecurityRoleType.CanManageNeededResources,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.isOnHold == true"
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = NeededResourceResources.RefreshClientData,
                OnClickAction = Url.UriActionPost(nameof(RefreshRecomendations), KnownParameter.SelectedIds),
                Icon = FontAwesome.Refresh,
                RequiredRole = SecurityRoleType.CanManageNeededResources,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                }
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = NeededResourceResources.ClientFilterGroup,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<ClientUserFilterViewModel>(
                            FilterCodes.ClientId,
                            NeededResourceResources.ClientFilter)
                    }
                },
                new FilterGroup
                {
                    DisplayName = NeededResourceResources.StatusFilterGroup,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.ActiveRequestsFilter,
                            DisplayName = NeededResourceResources.ActiveFilter,
                        },
                        new Filter
                        {
                            Code = FilterCodes.OnHoldRequestsFilter,
                            DisplayName = NeededResourceResources.OnHoldFilter,
                        }
                    }
                },
                new FilterGroup
                {
                    DisplayName = NeededResourceResources.NeededActionFilterGroup,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.HasPendingAppointments,
                            DisplayName = NeededResourceResources.PendingAppointmentsFilter,
                        },
                        new Filter
                        {
                            Code = FilterCodes.HasChallenges,
                            DisplayName = NeededResourceResources.HasChallengesFilter,
                        }
                    }
                }
            };
        }

        [HttpPost]
        public ActionResult ResumeRecommendations(long id)
        {
            _cardIndexDataService.Resume(id, null);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        public ActionResult RefreshRecomendations(long[] ids)
        {
            _cardIndexDataService.TriggerNeededResourcesRefresh(ids);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }
    }
}