using System;
using System.Web.Mvc;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageRecommendations)]
    public class RecommendationController : AtomicController
    {
        private readonly IRecommendationCardIndexDataService _recommendationService;

        public RecommendationController(IRecommendationCardIndexDataService recommendationService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(baseControllerDependencies)
        {
            _recommendationService = recommendationService;
        }

        public ActionResult Index(Guid resourceId)
        {
            var id = _recommendationService.GetRecommendationByCrmId(resourceId);

            if (id.HasValue)
            {
                return RedirectToAction("Edit", "ResourceRecomendations", new {id});
            }

            AddAlert(AlertType.Warning, $"Racommendation with Id={resourceId} was not synchronized");

            return RedirectToAction("List", "NeededResources");
        }
    }
}