﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using Smt.Atomic.WebApp.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageResourceRecomendations)]
    public class ResourceRecomendationsController :
        SubCardIndexController<RecommendationViewModel, RecommendationDto, NeededResourcesController>
    {
        private readonly IBaseControllerDependencies _baseControllerDependencies;
        private readonly IRecommendationCardIndexDataService _cardIndexDataService;

        public ResourceRecomendationsController(IRecommendationCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _baseControllerDependencies = baseControllerDependencies;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.Title = RecommendationResources.PageTitle;

            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanManageResourceRecomendations;

            CardIndex.Settings.EditButton.Text = RecommendationResources.SendToCustomerButton;
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "!!row.canSendRecommendation"
            };

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = RecommendationResources.RefreshRecommendationFiles,
                Icon = FontAwesome.Refresh,
                OnClickAction = Url.UriActionPost(nameof(RefreshDocuments), KnownParameter.SelectedIds),
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                }
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = RecommendationResources.StatusFilterGroup,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<RecommendationStatusViewModel>(
                            FilterCodes.RecommendationStatusFilter,
                            RecommendationResources.StatusFilter),
                        new Filter
                            {
                                Code = FilterCodes.RecommendationSeenByCustomer,
                                DisplayName = RecommendationResources.SeenByCustomerFilter
                            }
                    },
                },
            };
        }


        public ActionResult RefreshDocuments(long[] ids)
        {
            var response = _cardIndexDataService.RefreshRecommendations(ids);
            AddAlerts(response.Alerts);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }
    }
}