﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageReqionOptions)]
    [Identifier("ValuePickers.RegionOption")]
    public class RegionOptionValuePickerController : ReadOnlyCardIndexController<RegionOptionsViewModel, RegionOptionsDto>
    {
        public RegionOptionValuePickerController(
            IRegionOptionsCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}