﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageMainTechnology)]
    [Identifier("ValuePickers.MainTechnologyOption")]
    public class MainTechnologyOptionValuePickerController : ReadOnlyCardIndexController<MainTechnologyOptionsViewModel, MainTechnologyOptionsDto>
    {
        public MainTechnologyOptionValuePickerController(
            IMainTechnologyOptionsCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}