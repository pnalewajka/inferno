﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class BusinesUnitOptionsViewModel
    {
        public long Id { get; set; }

        [MaxLength(1000)]
        [DisplayNameLocalized(nameof(NeededResourceResources.BusinesUnitOptionsDisplayValueLabel), typeof(NeededResourceResources))]
        public string DisplayValue { get; set; }

        [Visibility(VisibilityScope.None)]
        public int CrmId { get; set; }

        public override string ToString()
        {
            return DisplayValue;
        }
    }
}
