﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class NeededResourceViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.ResourceNameHeader), typeof(NeededResourceResources))]
        public string Name { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid CrmId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.RecommendationsCount), typeof(NeededResourceResources))]
        [NonSortable]
        public int RecommendationsCount { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.RejectedRecommendationsCount), typeof(NeededResourceResources))]
        [NonSortable]
        public int RejectedRecommendationsCount { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.InvitedRecommendationsCount), typeof(NeededResourceResources))]
        [NonSortable]
        public int InvitedRecommendationsCount { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.ApprovedRecommendationsCount), typeof(NeededResourceResources))]
        [NonSortable]
        public int ApprovedRecommendationsCount { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.IsOnHold), typeof(NeededResourceResources))]
        public bool IsOnHold { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.HasChallenge), typeof(NeededResourceResources))]
        public bool HasChallenge { get; set; }

        [DisplayNameLocalized(nameof(NeededResourceResources.HasAppointments), typeof(NeededResourceResources))]
        public bool HasAppointments { get; set; }
    }
}