﻿
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ChallengeDtoToChallengeViewModelMapping : ClassMapping<ChallengeDto, ChallengeViewModel>
    {
        public ChallengeDtoToChallengeViewModelMapping()
        {
            Mapping = d => new ChallengeViewModel
            {
                Id = d.Id,
                Name = d.Name,
                RecommendationId = d.RecommendationId,
                Deadline = d.Deadline,
                AnswerComment = d.AnswerComment,
                ChallengeComment = d.ChallengeComment,
                ChallengeDocuments = d.ChallengeDocuments == null
                ? new System.Collections.Generic.List<Presentation.Common.Models.DocumentViewModel>()
                : d.ChallengeDocuments
    .Select(
        p => new Presentation.Common.Models.DocumentViewModel
        {
            ContentType = p.ContentType,
            DocumentName = p.DocumentName,
            DocumentId = p.DocumentId,
        })
    .ToList(),
                AnswerDocuments = d.AnswerDocuments == null
                ? new System.Collections.Generic.List<Presentation.Common.Models.DocumentViewModel>()
                : d.AnswerDocuments
    .Select(
        p => new Presentation.Common.Models.DocumentViewModel
        {
            ContentType = p.ContentType,
            DocumentName = p.DocumentName,
            DocumentId = p.DocumentId,
        })
    .ToList(),
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
