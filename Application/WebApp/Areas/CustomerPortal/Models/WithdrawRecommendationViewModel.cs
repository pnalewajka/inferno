﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.CustomerPortal.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class WithdrawRecommendationViewModel
    {
        [Visibility(VisibilityScope.None)]
        public Guid ResourceId { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(RecommendationResources.CandidateName), typeof(RecommendationResources))]
        public string CandidateName { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(RecommendationResources.NeededResourceName), typeof(RecommendationResources))]
        public string NeededResourceName { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(RecommendationResources.ClientName), typeof(RecommendationResources))]
        public string ClientName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Multiline(5)]
        [DataType(DataType.MultilineText)]
        [DisplayNameLocalized(nameof(RecommendationResources.CommentToCustomer), typeof(RecommendationResources))]
        public string CommentToCustomer { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(RecommendationResources.NeededResourceName), typeof(RecommendationResources))]
        public string NeededResourceShortName => NeededResourceNameHelper.PrepareNeededResourceNiceName(NeededResourceName);
    }
}