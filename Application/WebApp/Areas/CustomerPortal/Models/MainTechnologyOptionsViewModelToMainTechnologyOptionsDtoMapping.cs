﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class MainTechnologyOptionsViewModelToMainTechnologyOptionsDtoMapping : ClassMapping<MainTechnologyOptionsViewModel, MainTechnologyOptionsDto>
    {
        public MainTechnologyOptionsViewModelToMainTechnologyOptionsDtoMapping()
        {
            Mapping = v => new MainTechnologyOptionsDto
            {
                Id = v.Id,
                CrmId = v.CrmId,
                DisplayValue = v.DisplayValue
            };
        }
    }
}
