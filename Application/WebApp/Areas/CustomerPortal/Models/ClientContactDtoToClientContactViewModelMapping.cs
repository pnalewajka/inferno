﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ClientContactDtoToClientContactViewModelMapping : ClassMapping<ClientContactDto, ClientContactViewModel>
    {
        public ClientContactDtoToClientContactViewModelMapping()
        {
            Mapping = d => new ClientContactViewModel
            {
                Id = d.Id,
                CreatedOn = d.CreatedOn,
                CrmId = d.CrmId,
                Name = d.Name,
                CompanyName = d.CompanyName,
                Email = d.Email,
                CompenyGuidId = d.CompenyGuidId
            };
        }
    }
}
