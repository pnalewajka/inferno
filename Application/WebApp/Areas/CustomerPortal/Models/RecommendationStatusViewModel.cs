﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier("Filters.RecommendationStatus")]
    public class RecommendationStatusViewModel
    {
        [DisplayNameLocalized(nameof(RecommendationResources.RecommendationStatusViewModelStatusLabel), typeof(RecommendationResources))]
        public RecommendationStatus Status { get; set; }

        public override string ToString()
        {
            return Status.GetDescription();
        }
    }
}
