﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.Business.CustomerPortal.DocumentMappings;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class RecommendationViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid ResourceId { get; set; }

        [DisplayNameLocalized(nameof(RecommendationResources.Name), typeof(RecommendationResources))]
        [Required]
        public string Name { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RecommendationResources.HasDocumentsLabel), typeof(RecommendationResources))]
        public bool HasDocuments => Documents != null && Documents.Any();

        [DisplayNameLocalized(nameof(RecommendationResources.Rate), typeof(RecommendationResources))]
        [Required]
        public string Rate { get; set; }

        [DisplayNameLocalized(nameof(RecommendationResources.Availability), typeof(RecommendationResources))]
        [Required]
        public string Availability { get; set; }

        [Multiline(3)]
        [DataType(DataType.MultilineText)]
        [DisplayNameLocalized(nameof(RecommendationResources.Note), typeof(RecommendationResources))]
        [Visibility(VisibilityScope.Form)]
        public string Note { get; set; }

        [DisplayNameLocalized(nameof(RecommendationResources.Years), typeof(RecommendationResources))]
        [Range(0, 100, ErrorMessageResourceName = "InvalidScopeOfYears",
            ErrorMessageResourceType = typeof(RecommendationResources))]
        [Required]
        public int? Years { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? UserId { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendationResources.DocumentsLabel), typeof(RecommendationResources))]
        [DocumentUpload(typeof(RecommendationDocumentMapping))]
        public List<Presentation.Common.Models.DocumentViewModel> Documents { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? NeededResourceId { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime CreatedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public string NeededResourceName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RecommendationResources.SeenByCustomerLabel), typeof(RecommendationResources))]
        public bool SeenByCustomer { get; set; }

        [Visibility(VisibilityScope.None)]

        public bool IsOutside { get; set; }

        [Visibility(VisibilityScope.None)]

        public DateTime? ChallengeDeadline { get; set; }

        [Visibility(VisibilityScope.None)]
        public int ActiveDaysCount { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime? NextAppointmentDate { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool InterviewCompleted { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RecommendationResources.StatusLabel), typeof(RecommendationResources))]
        public RecommendationStatus Status { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime? RejectedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanSendRecommendation =>
            Status == RecommendationStatus.None;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Documents == null || !Documents.Any())
            {
                yield return  new ValidationResult(RecommendationResources.DocumentsRequiredMessage);
            }

            if (Documents != null && Documents.Any(d => d.ContentType != "application/pdf"))
            {
                yield return  new ValidationResult(RecommendationResources.OnlyPdfAllowedMessage);
            }
        }
    }
}
