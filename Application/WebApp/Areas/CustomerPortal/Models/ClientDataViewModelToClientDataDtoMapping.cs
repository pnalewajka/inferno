﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ClientDataViewModelToClientDataDtoMapping : ClassMapping<ClientDataViewModel, ClientDataDto>
    {
        public ClientDataViewModelToClientDataDtoMapping()
        {
            Mapping = v => new ClientDataDto
            {
                Id = v.Id,
                ClientUserId = v.ClientUserId,
                ManagerUserId = v.ManagerUserId,
                ClientContactId = v.ClientContactId,
                Timestamp = v.Timestamp,
                ContactPersonPhone = v.ContactPersonPhone
            };
        }
    }
}
