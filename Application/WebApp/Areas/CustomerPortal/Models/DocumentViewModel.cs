using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class DocumentViewModel
    {
        [Order(0)]
        [StringLength(255)]
        public string Name { get; set; }

        [Order(1)]
        public string Url { get; set; }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid UniqueId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public bool IsSelected { get; set; }
    }
}
