﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier("CustomerPortal.ClientContactViewModel")]
    public class ClientContactViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime CreatedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid CrmId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid CompenyGuidId { get; set; }

        public string CompanyName { get; set; }

        public override string ToString()
        {
            return $"{Name} ({CompanyName})";
        }
    }
}
