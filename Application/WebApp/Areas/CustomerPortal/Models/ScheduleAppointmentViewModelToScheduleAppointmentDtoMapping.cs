﻿using System;
using System.Globalization;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ScheduleAppointmentViewModelToScheduleAppointmentDtoMapping : ClassMapping<ScheduleAppointmentViewModel, ScheduleAppointmentDto>
    {
        public ScheduleAppointmentViewModelToScheduleAppointmentDtoMapping()
        {
            Mapping = v => new ScheduleAppointmentDto
            {
                Id = v.Id,
                RecomendationId = v.RecomendationId,
                RecomendationName = v.RecomendationName,
                NeededResourceName = v.NeededResourceName,
                AppointmentDate = v.AppointmentDate.Date.Add(TimeSpan.Parse(v.DisplayStartDate, CultureInfo.InvariantCulture)),
                AppointmentEndDate = v.AppointmentEndDate.Date.Add(TimeSpan.Parse(v.DisplayEndDate, CultureInfo.InvariantCulture)),
                AppointmentStatus = v.AppointmentStatus,
                RecommendationId = v.RecommendationId,
                Timestamp = v.Timestamp
            };
        }
    }
}
