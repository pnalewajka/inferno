﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ResourceFormDtoToResourceFormViewModelMapping : ClassMapping<ResourceFormDto, ResourceFormViewModel>
    {
        public ResourceFormDtoToResourceFormViewModelMapping()
        {
            Mapping = d => new ResourceFormViewModel
            {
                Id = d.Id,
                ResourceName = d.ResourceName,
                ResourceCount = d.ResourceCount,
                PlannedStart = d.PlannedStart,
                ProjectDuration = d.ProjectDuration,
                Location = d.Location,
                ProjectDescription = d.ProjectDescription,
                PositionRequirements = d.PositionRequirements,
                State = d.State,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                Question = d.Question,
                NeededResourceCrmCode = d.NeededResourceCrmCode,
                AdditionalComment = d.AdditionalComment,
                RecruitmentPhases = d.RecruitmentPhases,
                RoleDescription = d.RoleDescription,
                Languages = string.Join(" ", (d.LanguageOptions ?? new List<LanguageOptionDto>()).Select(l => $"{l.LanguageName}, {l.LevelName}")),
                Files = d.Files.Select(f => new Presentation.Common.Models.DocumentViewModel
                {
                    DocumentId = f.DocumentId,
                    ContentType = f.ContentType,
                    DocumentName = f.DocumentName
                }).ToList(),
                RequestType = d.RequestType
            };
        }
    }
}
