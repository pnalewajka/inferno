﻿using System.Globalization;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ScheduleAppointmentDtoToScheduleAppointmentViewModelMapping : ClassMapping<ScheduleAppointmentDto, ScheduleAppointmentViewModel>
    {
        public ScheduleAppointmentDtoToScheduleAppointmentViewModelMapping()
        {
            Mapping = d => new ScheduleAppointmentViewModel
            {
                Id = d.Id,
                RecomendationId = d.RecomendationId,
                RecomendationName = d.RecomendationName,
                NeededResourceName = d.NeededResourceName,
                AppointmentDate = d.AppointmentDate,
                AppointmentEndDate = d.AppointmentEndDate,
                AppointmentStatus = d.AppointmentStatus,
                DisplayStartDate = d.AppointmentDate.ToString(ScheduleAppointmentViewModel.TimeSpanFormat, CultureInfo.DefaultThreadCurrentCulture),
                DisplayEndDate = d.AppointmentEndDate.ToString(ScheduleAppointmentViewModel.TimeSpanFormat, CultureInfo.DefaultThreadCurrentCulture),
                DisplayDate = d.AppointmentDate.ToShortDateString(),
                RecommendationId = d.RecommendationId,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
