﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier(ModelIdentifier)]
    public class ApproveResourceRequestViewModel : IResourceFormApproveData
    {
        public const string ModelIdentifier = "CustomerProtal.ApproveResourceRequestViewModel";

        public int Id { get; set; }

        [Required]
        public string Rate { get; set; }

        [ValuePicker(Type = typeof(SeniorityLevelPickerController))]
        public long[] SeniorityLevels { get; set; }

        [Required]
        [ValuePicker(Type =typeof(MainTechnologyOptionValuePickerController))]
        public long MainTechnology { get; set; }

        [Required]
        [ValuePicker(Type = typeof(RegionOptionValuePickerController))]
        public long Region { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public long ResourceFormId { get; set; }
    }
}