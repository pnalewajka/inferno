﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class MainTechnologyOptionsDtoToMainTechnologyOptionsViewModelMapping : ClassMapping<MainTechnologyOptionsDto, MainTechnologyOptionsViewModel>
    {
        public MainTechnologyOptionsDtoToMainTechnologyOptionsViewModelMapping()
        {
            Mapping = d => new MainTechnologyOptionsViewModel
            {
                Id = d.Id,
                CrmId = d.CrmId,
                DisplayValue = d.DisplayValue
            };
        }
    }
}
