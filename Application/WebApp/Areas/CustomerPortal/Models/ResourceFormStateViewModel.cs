﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier("Filters.ResourceFormState")]
    public class ResourceFormStateViewModel
    {
        [DisplayNameLocalized(nameof(ResourceFormResources.ResourceFormStateViewModelStateLabel), typeof(ResourceFormResources))]
        public ResourceFormState State { get; set; }

        public override string ToString()
        {
            return State.GetDescription();
        }
    }
}