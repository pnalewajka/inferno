﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ClientDataDtoToClientDataViewModelMapping : ClassMapping<ClientDataDto, ClientDataViewModel>
    {
        public ClientDataDtoToClientDataViewModelMapping()
        {
            Mapping = d => new ClientDataViewModel
            {
                Id = d.Id,
                ClientUserId = d.ClientUserId,
                ManagerUserId = d.ManagerUserId,
                ClientContactId = d.ClientContactId,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                ContactPersonPhone = d.ContactPersonPhone
            };
        }
    }
}
