﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Business.CustomerPortal.Dto;
using DocumentDto = Smt.Atomic.Business.Common.Dto.DocumentDto;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class RecommendationViewModelToRecommendationDtoMapping : ClassMapping<RecommendationViewModel, RecommendationDto>
    {
        public RecommendationViewModelToRecommendationDtoMapping()
        {
            Mapping = v => new RecommendationDto
            {
                Id = v.Id,
                ResourceId = v.ResourceId,
                Name = v.Name,
                Rate = v.Rate,
                Availability = v.Availability,
                Note = v.Note,
                //Notes = v.Notes,
                //Documents = v.Documents,
                Documents = v.Documents == null ? new List<DocumentDto>() : v.Documents.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId,
                    TemporaryDocumentId = p.TemporaryDocumentId
                }).ToList(),
                Years = v.Years,
                UserId = v.UserId,
                Timestamp = v.Timestamp,
                NeededResourceId = v.NeededResourceId,
                CreatedOn = v.CreatedOn,
                NeededResourceName = v.NeededResourceName,
                SeenByCustomer = v.SeenByCustomer,
                IsOutside = v.IsOutside,
                ChallengeDeadline = v.ChallengeDeadline,
                ActiveDaysCount = v.ActiveDaysCount,
                NextAppointmentDate = v.NextAppointmentDate,
                InterviewCompleted = v.InterviewCompleted,
                Status = v.Status,
                RejectedOn = v.RejectedOn
            };
        }
    }
}
