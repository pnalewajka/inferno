﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class BusinesUnitOptionsViewModelToBusinesUnitOptionsDtoMapping : ClassMapping<BusinesUnitOptionsViewModel, BusinesUnitOptionsDto>
    {
        public BusinesUnitOptionsViewModelToBusinesUnitOptionsDtoMapping()
        {
            Mapping = v => new BusinesUnitOptionsDto
            {
                Id = v.Id,
                CrmId = v.CrmId,
                DisplayValue = v.DisplayValue
            };
        }
    }
}
