using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class DocumentViewModelToDocumentDtoMapping : ClassMapping<DocumentViewModel, DocumentDto>
    {
        public DocumentViewModelToDocumentDtoMapping()
        {
            Mapping = v => new DocumentDto
            {
                Name = v.Name,
                Url = v.Url,
                Id = v.Id,
                Timestamp = v.Timestamp,
                UniqueId = v.UniqueId
            };
        }
    }
}
