﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier("Filter.NeededResourceClient")]
    public class NeededResourceClientFilterVieweModel
    {
        [ValuePicker(Type = typeof(ClientUserValuePickerController))]
        [DisplayNameLocalized(nameof(ClientDataResource.ClientAccountLabel), typeof(ClientDataResource))]
        public long CrmAccountId { get; set; }

        public override string ToString()
        {
            return ClientDataResource.CrmAccountFilter;
        }
    }
}