﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ChallengeViewModelToChallengeDtoMapping : ClassMapping<ChallengeViewModel, ChallengeDto>
    {
        public ChallengeViewModelToChallengeDtoMapping()
        {
            Mapping = v => new ChallengeDto
            {
                Id = v.Id,
                RecommendationId = v.RecommendationId,
                Deadline = v.Deadline,
                AnswerComment = v.AnswerComment,
                ChallengeDocuments = v.ChallengeDocuments == null ? new List<Business.Common.Dto.DocumentDto>().ToArray() : v.ChallengeDocuments.Select(p => new Business.Common.Dto.DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId,
                    TemporaryDocumentId = p.TemporaryDocumentId
                }).ToArray(),
                AnswerDocuments = v.AnswerDocuments == null ? new List<Business.Common.Dto.DocumentDto>().ToArray() : v.AnswerDocuments.Select(p => new Business.Common.Dto.DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId,
                    TemporaryDocumentId = p.TemporaryDocumentId
                }).ToArray(),
                Timestamp = v.Timestamp
            };
        }
    }
}
