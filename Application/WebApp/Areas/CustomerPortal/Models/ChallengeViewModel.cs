﻿using Smt.Atomic.Business.CustomerPortal.DocumentMappings;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ChallengeViewModel
    {
        public ChallengeViewModel()
        {
            ChallengeDocuments = new List<Presentation.Common.Models.DocumentViewModel>();
            AnswerDocuments = new List<Presentation.Common.Models.DocumentViewModel>();
        }

        public long Id { get; set; }

        [Order(1)]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ChallengeResources.ChallengeNameLabel), typeof(ChallengeResources))]
        public string Name { get; set; }

        [Visibility(VisibilityScope.None)]
        public long RecommendationId { get; set; }

        [Order(6)]
        [Multiline(3)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ChallengeResources.ChallengeAnswerCommentLabel), typeof(ChallengeResources))]
        public string AnswerComment { get; set; }

        [Order(4)]
        [Multiline(3)]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ChallengeResources.ChallengeChallengeCommentLabel), typeof(ChallengeResources))]
        public string ChallengeComment { get; set; }

        [Order(3)]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ChallengeResources.ChallengeDeadlineLabel), typeof(ChallengeResources))]
        public DateTime Deadline { get; set; }

        [Order(2)]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DocumentUpload(typeof(ChallengeDocumentMapping))]
        [DisplayNameLocalized(nameof(ChallengeResources.ChallengeChallengeDocumentsLabel), typeof(ChallengeResources))]
        public List<Presentation.Common.Models.DocumentViewModel> ChallengeDocuments { get; set; }

        [Order(5)]
        [Visibility(VisibilityScope.Form)]
        [DocumentUpload(typeof(AnswerDocumentMapping))]
        [DisplayNameLocalized(nameof(ChallengeResources.ChallengeAnswerDocumentsLabel), typeof(ChallengeResources))]
        public List<Presentation.Common.Models.DocumentViewModel> AnswerDocuments { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(ChallengeResources.HasAnswersLabel), typeof(ChallengeResources))]
        public bool HasAnswers =>
    AnswerDocuments != null &&
    AnswerDocuments.Any();
    }
}
