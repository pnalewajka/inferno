using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class DocumentDtoToDocumentViewModelMapping : ClassMapping<DocumentDto, DocumentViewModel>
    {
        public DocumentDtoToDocumentViewModelMapping()
        {
            Mapping = d => new DocumentViewModel
            {
                Name = d.Name,
                Url = d.Url,
                Id = d.Id,
                UniqueId = d.UniqueId,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
