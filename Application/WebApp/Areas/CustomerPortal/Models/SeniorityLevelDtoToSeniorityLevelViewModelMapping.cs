﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class SeniorityLevelDtoToSeniorityLevelViewModelMapping : ClassMapping<SeniorityLevelDto, SeniorityLevelViewModel>
    {
        public SeniorityLevelDtoToSeniorityLevelViewModelMapping()
        {
            Mapping = d => new SeniorityLevelViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description
            };
        }
    }
}
