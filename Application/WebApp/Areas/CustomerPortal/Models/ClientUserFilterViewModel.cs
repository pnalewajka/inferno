﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier("Filter.NeededResourceOperationManager")]
    public class ClientUserFilterViewModel
    {
        [ValuePicker(Type = typeof(ClientUserValuePickerController))]
        [DisplayNameLocalized(nameof(ClientDataResource.ClientUserLabel), typeof(ClientDataResource))]
        public long UserId { get; set; }

        public override string ToString()
        {
            return ClientDataResource.ManagerUserFilter;
        }
    }
}