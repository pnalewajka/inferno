﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Controllers;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ClientDataViewModel
    {
        public long Id { get; set; }

        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(ClientDataResource.ClientUserLabel), typeof(ClientDataResource))]
        public long ClientUserId { get; set; }

        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(ClientDataResource.ManagerUserLabel), typeof(ClientDataResource))]
        public long ManagerUserId { get; set; }

        [ValuePicker(Type = typeof(ClientContactPickerController))]
        [DisplayNameLocalized(nameof(ClientDataResource.ClientContactLabel), typeof(ClientDataResource))]
        public long ClientContactId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(ClientDataResource.ContactPersonPhoneLabel), typeof(ClientDataResource))]
        [Visibility(VisibilityScope.Form)]
        public string ContactPersonPhone { get; set; }
    }
}
