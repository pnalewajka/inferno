﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class BusinesUnitOptionsDtoToBusinesUnitOptionsViewModelMapping : ClassMapping<BusinesUnitOptionsDto, BusinesUnitOptionsViewModel>
    {
        public BusinesUnitOptionsDtoToBusinesUnitOptionsViewModelMapping()
        {
            Mapping = d => new BusinesUnitOptionsViewModel
            {
                Id = d.Id,
                CrmId = d.CrmId,
                DisplayValue = d.DisplayValue
            };
        }
    }
}
