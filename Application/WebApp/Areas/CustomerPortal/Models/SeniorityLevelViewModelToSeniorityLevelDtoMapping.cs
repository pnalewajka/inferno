﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class SeniorityLevelViewModelToSeniorityLevelDtoMapping : ClassMapping<SeniorityLevelViewModel, SeniorityLevelDto>
    {
        public SeniorityLevelViewModelToSeniorityLevelDtoMapping()
        {
            Mapping = v => new SeniorityLevelDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description
            };
        }
    }
}
