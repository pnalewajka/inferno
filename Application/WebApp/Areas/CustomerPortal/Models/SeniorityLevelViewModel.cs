﻿using System;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class SeniorityLevelViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}
