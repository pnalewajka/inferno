﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Smt.Atomic.Business.CustomerPortal.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ResourceFormViewModel
    {

        public long Id { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.State), typeof(ResourceFormResources))]
        [ReadOnly(true)]
        public ResourceFormState State { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.RequestTypeLabel), typeof(ResourceFormResources))]
        [ReadOnly(true)]
        public ResourceRequestType RequestType { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.ResourceName), typeof(ResourceFormResources))]
        public string ResourceName { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.ResourceCount), typeof(ResourceFormResources))]
        public int ResourceCount { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.PlannedStart), typeof(ResourceFormResources))]
        public string PlannedStart { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.ProjectDuration), typeof(ResourceFormResources))]
        public string ProjectDuration { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.Location), typeof(ResourceFormResources))]
        public string Location { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.ProjectDescription), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Form)]
        public string ProjectDescription { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.RoleDescription), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Form)]
        public string RoleDescription { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.PositionRequirements), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Form)]
        public string PositionRequirements { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.Languages), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        public string Languages { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.AdditionalComment), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Form)]
        public string AdditionalComment { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.RecruitmentPhases), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Form)]
        public string RecruitmentPhases { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ResourceFormResources.Question), typeof(ResourceFormResources))]
        public string Question { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(ResourceFormResources.WaitingForClarification), typeof(ResourceFormResources))]
        public bool WaitingForClarification => State == ResourceFormState.Draft && string.IsNullOrEmpty(Question);

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid? NeededResourceCrmCode { get; set; }

        [DisplayNameLocalized(nameof(ResourceFormResources.ResourceFormViewModelInCrmLabel), typeof(ResourceFormResources))]
        [Visibility(VisibilityScope.Grid)]
        public bool InCrm => NeededResourceCrmCode.HasValue;

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ResourceFormResources.RequirementFiles), typeof(ResourceFormResources))]
        [DocumentUpload(typeof(ResourceFormFileMapping))]
        public List<Presentation.Common.Models.DocumentViewModel> Files { get; set; }
    }
}
