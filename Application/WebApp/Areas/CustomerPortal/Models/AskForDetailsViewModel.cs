﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier(ModelIdentifier)]
    public class AskForDetailsViewModel
    {
        public const string ModelIdentifier = "CustomerProtal.AskForDetailsViewModel";


        [Required]
        [MaxLength(2000)]
        [DisplayNameLocalized(nameof(ResourceFormResources.QuestionLabel), typeof(ResourceFormResources))]
        [Multiline(10)]
        public string Question { get; set; }
    }
}