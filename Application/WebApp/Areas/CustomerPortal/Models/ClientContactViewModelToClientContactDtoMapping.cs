﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ClientContactViewModelToClientContactDtoMapping : ClassMapping<ClientContactViewModel, ClientContactDto>
    {
        public ClientContactViewModelToClientContactDtoMapping()
        {
            Mapping = v => new ClientContactDto
            {
                Id = v.Id,
                CreatedOn = v.CreatedOn,
                CrmId = v.CrmId,
                Name = v.Name
            };
        }
    }
}
