﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class RegionOptionsDtoToRegionOptionsViewModelMapping : ClassMapping<RegionOptionsDto, RegionOptionsViewModel>
    {
        public RegionOptionsDtoToRegionOptionsViewModelMapping()
        {
            Mapping = d => new RegionOptionsViewModel
            {
                Id = d.Id,
                CrmId = d.CrmId,
                DisplayValue = d.DisplayValue
            };
        }
    }
}
