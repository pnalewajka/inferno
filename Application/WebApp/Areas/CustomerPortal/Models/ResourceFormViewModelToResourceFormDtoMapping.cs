﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ResourceFormViewModelToResourceFormDtoMapping : ClassMapping<ResourceFormViewModel, ResourceFormDto>
    {
        public ResourceFormViewModelToResourceFormDtoMapping()
        {
            Mapping = v => new ResourceFormDto
            {
                Id = v.Id,
                ResourceName = v.ResourceName,
                ResourceCount = v.ResourceCount,
                PlannedStart = v.PlannedStart,
                ProjectDuration = v.ProjectDuration,
                State = v.State,
                Location = v.Location,
                ProjectDescription = v.ProjectDescription,
                PositionRequirements = v.PositionRequirements,
                Timestamp = v.Timestamp,
                NeededResourceCrmCode = v.NeededResourceCrmCode,
                Files = v.Files == null ? null : v.Files.Select(f => new Business.Common.Dto.DocumentDto
                {
                    DocumentId = f.DocumentId,
                    ContentType = f.ContentType,
                    TemporaryDocumentId = f.TemporaryDocumentId,
                    DocumentName = f.DocumentName
                }).ToArray(),
                RequestType = v.RequestType
            };
        }
    }
}
