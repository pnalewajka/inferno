﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class RecommendationDtoToRecommendationViewModelMapping : ClassMapping<RecommendationDto, RecommendationViewModel>
    {
        public RecommendationDtoToRecommendationViewModelMapping()
        {
            Mapping = d => new RecommendationViewModel
            {
                Id = d.Id,
                ResourceId = d.ResourceId,
                Name = d.Name,
                Rate = d.Rate,
                Availability = d.Availability,
                Note = d.Note,
                Documents = d.Documents.Select(doc => new Presentation.Common.Models.DocumentViewModel
                {
                    DocumentId = doc.DocumentId,
                    ContentType = doc.ContentType,
                    DocumentName = doc.DocumentName
                }).ToList(),
                Years = d.Years,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                UserId = d.UserId,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                NeededResourceId = d.NeededResourceId,
                CreatedOn = d.CreatedOn,
                NeededResourceName = d.NeededResourceName,
                SeenByCustomer = d.SeenByCustomer,
                IsOutside = d.IsOutside,
                ChallengeDeadline = d.ChallengeDeadline,
                ActiveDaysCount = d.ActiveDaysCount,
                NextAppointmentDate = d.NextAppointmentDate,
                InterviewCompleted = d.InterviewCompleted,
                Status = d.Status,
                RejectedOn = d.RejectedOn
            };
        }
    }
}
