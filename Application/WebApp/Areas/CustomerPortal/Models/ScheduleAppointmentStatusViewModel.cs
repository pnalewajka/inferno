﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier("Filters.ScheduleAppointmentStatus")]
    public class ScheduleAppointmentStatusViewModel
    {
        [DisplayNameLocalized(nameof(ScheduleAppointmentResources.ScheduleAppointmentStatusViewModelStatusLabel), typeof(ScheduleAppointmentResources))]
        public AppointmentStatusType Status { get; set; }

        public override string ToString()
        {
            return Status.GetDescriptionOrValue();
        }
    }
}