﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class NeededResourceDtoToNeededResourceViewModelMapping : ClassMapping<NeededResourceDto, NeededResourceViewModel>
    {
        public NeededResourceDtoToNeededResourceViewModelMapping()
        {
            Mapping = d => new NeededResourceViewModel
            {
                Id = d.Id,
                Name = d.Name,
                CrmId = d.CrmId,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                InvitedRecommendationsCount = d.InvitedRecommendationsCount,
                ApprovedRecommendationsCount = d.ApprovedRecommendationsCount,
                RecommendationsCount = d.RecommendationsCount,
                RejectedRecommendationsCount = d.RejectedRecommendationsCount,
                IsOnHold = d.IsOnHold,
                HasChallenge = d.HasChallenges,
                HasAppointments = d.HasAppointments
            };
        }
    }
}
