﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class NeededResourceViewModelToNeededResourceDtoMapping : ClassMapping<NeededResourceViewModel, NeededResourceDto>
    {
        public NeededResourceViewModelToNeededResourceDtoMapping()
        {
            Mapping = v => new NeededResourceDto
            {
                Id = v.Id,
                Name = v.Name,
                CrmId = v.CrmId,
                Timestamp = v.Timestamp
            };
        }
    }
}
