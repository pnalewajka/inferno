﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class RegionOptionsViewModelToRegionOptionsDtoMapping : ClassMapping<RegionOptionsViewModel, RegionOptionsDto>
    {
        public RegionOptionsViewModelToRegionOptionsDtoMapping()
        {
            Mapping = v => new RegionOptionsDto
            {
                Id = v.Id,
                CrmId = v.CrmId,
                DisplayValue = v.DisplayValue
            };
        }
    }
}
