using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    [Identifier(ModelIdentifier)]
    public class SendRecommendationViewModel : ISendRecommendationModel
    {
        public long Id { get; set; }

        public const string ModelIdentifier = "CustomerPortal.SendRecommendationViewModel";

        [DisplayNameLocalized(nameof(RecommendationResources.Rate), typeof(RecommendationResources))]
        [Required]
        public string Rate { get; set; }

        [DisplayNameLocalized(nameof(RecommendationResources.Availability), typeof(RecommendationResources))]
        [Required]
        public string Availability { get; set; }

        [Multiline(3)]
        [DataType(DataType.MultilineText)]
        [DisplayNameLocalized(nameof(RecommendationResources.Note), typeof(RecommendationResources))]
        [Visibility(VisibilityScope.Form)]
        public string Note { get; set; }

        [DisplayNameLocalized(nameof(RecommendationResources.Years), typeof(RecommendationResources))]
        [Range(0, 100, ErrorMessageResourceName = "InvalidScopeOfYears",
            ErrorMessageResourceType = typeof(RecommendationResources))]
        [Required]
        public int? Years { get; set; }
    }
}