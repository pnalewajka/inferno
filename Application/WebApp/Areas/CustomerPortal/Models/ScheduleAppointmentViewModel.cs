﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.CustomerPortal.Resources;

namespace Smt.Atomic.WebApp.Areas.CustomerPortal.Models
{
    public class ScheduleAppointmentViewModel : IValidatableObject
    {
        internal const string TimeSpanFormat = "HH:mm";

        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long RecomendationId { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(ScheduleAppointmentResources.RecommendationName), typeof(ScheduleAppointmentResources))]
        public string RecomendationName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string NeededResourceName { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(ScheduleAppointmentResources.DateName), typeof(ScheduleAppointmentResources))]
        public string DisplayDate { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ScheduleAppointmentResources.StartDateName), typeof(ScheduleAppointmentResources))]
        public string DisplayStartDate { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ScheduleAppointmentResources.EndDateName), typeof(ScheduleAppointmentResources))]
        public string DisplayEndDate { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime AppointmentDate { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime AppointmentEndDate { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(ScheduleAppointmentResources.AppointmentStatus), typeof(ScheduleAppointmentResources))]
        public AppointmentStatusType AppointmentStatus { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public long RecommendationId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            TimeSpan startTimeSpan, endTimeSpan;

            if (!TimeSpan.TryParse(DisplayStartDate, CultureInfo.InvariantCulture, out startTimeSpan))
            {
                yield return new ValidationResult("Can't parse start date");
            }

            if (!TimeSpan.TryParse(DisplayEndDate, CultureInfo.InvariantCulture, out endTimeSpan))
            {
                yield return new ValidationResult("Can't parse end date");
            }

            if (endTimeSpan < startTimeSpan)
            {
                yield return new ValidationResult("Invalid period");

            }
        }
    }
}