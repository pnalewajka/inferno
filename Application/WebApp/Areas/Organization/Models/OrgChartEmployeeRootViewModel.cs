﻿using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartEmployeeRootViewModel
    {
        [JsonProperty(PropertyName = "longId")]
        public long LongId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "managerName")]
        public string ManagerName { get; set; }

        [JsonProperty(PropertyName = "managerTitle")]
        public string ManagerTitle { get; set; }

        [JsonProperty(PropertyName = "imageAddress")]
        public string ImageAddress { get; set; }

        [JsonProperty(PropertyName = "locationName")]
        public string LocationName { get; set; }

    }
}