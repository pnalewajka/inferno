﻿using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartEmployeeItemViewModel : OrgChartLevelItemBaseViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "root")]
        public OrgChartEmployeeRootViewModel Root { get; set; }
    }
}