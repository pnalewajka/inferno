﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class PricingEngineerViewModelToPricingEngineerDtoMapping : ClassMapping<PricingEngineerViewModel, PricingEngineerDto>
    {
        public PricingEngineerViewModelToPricingEngineerDtoMapping()
        {
            Mapping = v => new PricingEngineerDto
            {
                Id = v.Id,
                Timestamp = v.Timestamp,
                EmployeeId = v.EmployeeId,
                SkillTagIds = v.SkillTags != null && v.SkillTags.Length > 0 ? v.SkillTags : new long[] { },
            };
        }
    }
}
