﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class OrgUnitsReportParametersViewModelToOrgUnitsReportParametersDtoMapping : ClassMapping<OrgUnitsReportParametersViewModel, OrgUnitsReportParametersDto>
    {
        public OrgUnitsReportParametersViewModelToOrgUnitsReportParametersDtoMapping()
        {
            Mapping = d => new OrgUnitsReportParametersDto
            {
                OrgUnitIds = d.OrgUnitIds
            };
        }
    }
}
