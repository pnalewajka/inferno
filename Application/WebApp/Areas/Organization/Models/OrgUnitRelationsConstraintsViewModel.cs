﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitRelationsConstraintsViewModel
    {
        public IDictionary<string, IEnumerable<string>> Employees { get; set; }

        public IDictionary<string, IEnumerable<string>> Projects { get; set; }

        public IDictionary<string, IEnumerable<string>> SurveyConducts { get; set; }
    }
}