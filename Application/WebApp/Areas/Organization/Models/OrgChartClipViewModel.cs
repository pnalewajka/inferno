﻿using Newtonsoft.Json;
using Smt.Atomic.Business.Organization.Dto;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartClipViewModel
    {
        [JsonProperty(PropertyName = "focusedNodeId")]
        public string FocusedNodeId { get; set; }

        [JsonProperty(PropertyName = "clipType")]
        public ClipType ClipType { get; set; }
    }
}