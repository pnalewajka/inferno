﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartEmployeeViewModel
    {
        [JsonProperty(PropertyName = "children")]
        public IList<OrgChartEmployeeItemViewModel> Children { get; set; }

        public OrgChartEmployeeViewModel()
        {
            Children = new List<OrgChartEmployeeItemViewModel>();
        }
    }
}