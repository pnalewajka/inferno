﻿
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class TechnicalInterviewerViewModelToTechnicalInterviewerDtoMapping : ClassMapping<TechnicalInterviewerViewModel, TechnicalInterviewerDto>
    {
        public TechnicalInterviewerViewModelToTechnicalInterviewerDtoMapping()
        {
            Mapping = v => new TechnicalInterviewerDto
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                Timestamp = v.Timestamp,
                SkillTagIds = v.SkillTagIds != null && v.SkillTagIds.Length > 0 ? v.SkillTagIds : new long[] { },
                CandidateMaxLevel = v.CandidateMaxLevel,
                CanInterviewForeignCandidates = v.CanCanInterviewForeignCandidates,
                Comments = v.Comments,
                SkypeName = v.SkypeName,
                AvailabilityDetails = v.AvailabilityDetails
            };
        }
    }
}
