﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartEmployeeDtoToOrgChartEmployeeViewModelMapping : ClassMapping<OrgChartEmployeeDto, OrgChartEmployeeViewModel>
    {
        public OrgChartEmployeeDtoToOrgChartEmployeeViewModelMapping(
            IClassMapping<OrgChartEmployeeItemDto, OrgChartEmployeeItemViewModel> employeeItemMapping)
        {
            Mapping = d => new OrgChartEmployeeViewModel
            {
                Children = d.Children == null
                    ? new List<OrgChartEmployeeItemViewModel>()
                    : d.Children.Select(employeeItemMapping.CreateFromSource).ToList()
            };
        }
    }
}