﻿
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class TechnicalInterviewerDtoToTechnicalInterviewerViewModelMapping : ClassMapping<TechnicalInterviewerDto, TechnicalInterviewerViewModel>
    {
        public TechnicalInterviewerDtoToTechnicalInterviewerViewModelMapping()
        {
            Mapping = d => new TechnicalInterviewerViewModel
            {
                Id = d.Id,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                SkillTagIds = d.SkillTagIds,
                EmployeeId = d.EmployeeId,
                LocationName = d.Location,
                CompanyName = d.CompanyName,
                FirstName = d.FirstName,
                LastName = d.LastName,
                Acronym = d.Acronym,
                CandidateMaxLevel = d.CandidateMaxLevel,
                CanCanInterviewForeignCandidates = d.CanInterviewForeignCandidates,
                Comments = d.Comments,
                AvailabilityDetails = d.AvailabilityDetails,
                SkypeName = d.SkypeName
            };
        }
    }
}
