﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitDtoToOrgUnitViewModelMapping : ClassMapping<OrgUnitDto, OrgUnitViewModel>
    {
        public OrgUnitDtoToOrgUnitViewModelMapping()
        {
            Mapping = d => new OrgUnitViewModel
            {
                Id = d.Id,
                Code = d.Code,
                Path = d.Path,
                Name = d.Name,
                FlatName = d.FlatName,
                OrgUnitFeatures = d.OrgUnitFeatures,
                NameSortOrder = d.NameSortOrder,
                DescendantCount = d.DescendantCount,
                Description = d.Description,
                ParentId = d.ParentId,
                DefaultProjectContributionMode = d.DefaultProjectContributionMode,
                TimeReportingMode = d.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = d.AbsenceOnlyDefaultProjectId,
                Timestamp = d.Timestamp,
                Depth = d.Depth,
                CalendarId = d.CalendarId,
                EmployeeId = d.EmployeeId,
                RegionId = d.RegionId,
                CustomOrder = d.CustomOrder,
                OrgUnitManagerRole = d.OrgUnitManagerRole,
            };
        }
    }
}
