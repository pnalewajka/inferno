﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class PricingEngineerViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Required]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long EmployeeId { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectResources.KeyTechnologiesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(SkillTagPickerController))]
        public long[] SkillTags { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringLength(255)]
        public string LocationName { get; set; }

        public string GetFullName()
        {
            return $"{LastName} {FirstName} {LocationName}".Trim();
        }

        public override string ToString()
        {
            return GetFullName();
        }
    }
}