﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class OrgUnitsReportParametersDtoToOrgUnitsReportParametersViewModelMapping : ClassMapping<OrgUnitsReportParametersDto, OrgUnitsReportParametersViewModel>
    {
        public OrgUnitsReportParametersDtoToOrgUnitsReportParametersViewModelMapping()
        {
            Mapping = d => new OrgUnitsReportParametersViewModel
            {
                OrgUnitIds = d.OrgUnitIds == null ? null : d.OrgUnitIds.ToArray()
            };
        }
    }
}
