﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitChartDtoToOrgUnitChartViewModelMapping : ClassMapping<OrgUnitChartDto, OrgUnitChartViewModel>
    {
        public OrgUnitChartDtoToOrgUnitChartViewModelMapping()
        {
            Mapping = d => new OrgUnitChartViewModel
            {
                Id = d.Id,
                Code = d.Code,
                Name = d.Name,
                ImageAddress = d.ImageAddress,
                LineManagerId = d.LineManagerId,
                ManagerName = d.ManagerName,
                ManagerTitle = d.ManagerTitle
            };
        }
    }
}