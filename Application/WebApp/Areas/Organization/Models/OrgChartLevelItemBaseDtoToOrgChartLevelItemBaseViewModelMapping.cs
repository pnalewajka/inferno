﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartLevelItemBaseDtoToOrgChartLevelItemBaseViewModelMapping : ClassMapping<OrgChartLevelItemBaseDto, OrgChartLevelItemBaseViewModel>
    {
        private readonly IClassMapping<OrgUnitChartDto, OrgUnitChartViewModel> _orgUnitChartMapping;
        private readonly IClassMapping<OrgChartEmployeeRootDto, OrgChartEmployeeRootViewModel> _orgChartEmployeeRootMapping;

        public OrgChartLevelItemBaseDtoToOrgChartLevelItemBaseViewModelMapping(
            IClassMapping<OrgUnitChartDto, OrgUnitChartViewModel> orgUnitChartMapping,
            IClassMapping<OrgChartEmployeeRootDto, OrgChartEmployeeRootViewModel> orgChartEmployeeRootMapping)
        {
            _orgUnitChartMapping = orgUnitChartMapping;
            _orgChartEmployeeRootMapping = orgChartEmployeeRootMapping;

            Mapping = d => CreateElement(d);
        }

        private OrgChartLevelItemBaseViewModel CreateElement(OrgChartLevelItemBaseDto dto)
        {
            var orgChartLevelDto = dto as OrgChartLevelDto;

            if (orgChartLevelDto != null)
            {
                return new OrgChartLevelViewModel
                {
                    Root = orgChartLevelDto.Root == null
                        ? null
                        : _orgUnitChartMapping.CreateFromSource(orgChartLevelDto.Root),
                    Relationship = orgChartLevelDto.Relationship,
                    Children = orgChartLevelDto.Children == null
                        ? new List<OrgChartLevelItemBaseViewModel>()
                        : orgChartLevelDto.Children.Select(CreateElement).ToList()
                };
            }

            var orgChartEmployeeLevelDto = dto as OrgChartEmployeeItemDto;

            if (orgChartEmployeeLevelDto != null)
            {
                return new OrgChartEmployeeItemViewModel
                {
                    Id = orgChartEmployeeLevelDto.Id,
                    Root = orgChartEmployeeLevelDto.Root == null
                        ? null
                        : _orgChartEmployeeRootMapping.CreateFromSource(orgChartEmployeeLevelDto.Root),
                    Relationship = orgChartEmployeeLevelDto.Relationship,
                    Children = orgChartEmployeeLevelDto.Children == null
                        ? new List<OrgChartLevelItemBaseViewModel>()
                        : orgChartEmployeeLevelDto.Children.Select(CreateElement).ToList()
                };
            }

            return null;
        }
    }
}