﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class PricingEngineerDtoToPricingEngineerViewModelMapping : ClassMapping<PricingEngineerDto, PricingEngineerViewModel>
    {
        public PricingEngineerDtoToPricingEngineerViewModelMapping()
        {
            Mapping =
                d =>
                    new PricingEngineerViewModel
                    {
                        Id = d.Id,
                        ImpersonatedById = d.ImpersonatedById,
                        ModifiedById = d.ModifiedById,
                        ModifiedOn = d.ModifiedOn,
                        Timestamp = d.Timestamp,
                        SkillTags = d.SkillTagIds,
                        EmployeeId = d.EmployeeId,
                        LocationName = d.Location,
                        FirstName = d.FirstName,
                        LastName = d.LastName
                    };
        }
    }
}
