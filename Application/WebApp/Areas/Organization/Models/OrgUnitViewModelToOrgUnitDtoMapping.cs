﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitViewModelToOrgUnitDtoMapping : ClassMapping<OrgUnitViewModel, OrgUnitDto>
    {
        public OrgUnitViewModelToOrgUnitDtoMapping()
        {
            Mapping = v => new OrgUnitDto
            {
                Id = v.Id,
                Code = v.Code,
                Path = v.Path,
                Name = v.Name,
                FlatName = v.FlatName,
                OrgUnitFeatures = v.OrgUnitFeatures,
                NameSortOrder = v.NameSortOrder,
                DescendantCount = v.DescendantCount,
                Description = v.Description,
                ParentId = v.ParentId,
                DefaultProjectContributionMode = v.DefaultProjectContributionMode,
                TimeReportingMode = v.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = v.TimeReportingMode == TimeReportingMode.AbsenceOnly ? v.AbsenceOnlyDefaultProjectId : null,
                Timestamp = v.Timestamp,
                Depth = v.Depth,
                CalendarId = v.CalendarId,
                EmployeeId = v.EmployeeId,
                RegionId = v.RegionId,
                CustomOrder = v.CustomOrder,
                OrgUnitManagerRole = v.OrgUnitManagerRole,
            };
        }
    }
}
