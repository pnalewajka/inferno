﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartDtoToOrgChartViewModelMapping : ClassMapping<OrgChartDto, OrgChartViewModel>
    {
        public OrgChartDtoToOrgChartViewModelMapping(
            IClassMapping<OrgChartClipDto, OrgChartClipViewModel> orgChartClipMapping,
            IClassMapping<OrgChartLevelDto, OrgChartLevelViewModel> orgChartLevelMapping)
        {
            Mapping = d => new OrgChartViewModel
            {
                Clip = d.Clip == null
                    ? null
                    : orgChartClipMapping.CreateFromSource(d.Clip),
                TopLevel = d.TopLevel == null
                    ? null
                    : orgChartLevelMapping.CreateFromSource(d.TopLevel)
            };
        }
    }
}