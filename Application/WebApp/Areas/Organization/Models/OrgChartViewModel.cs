﻿using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartViewModel
    {
        [JsonProperty(PropertyName = "topLevel")]
        public OrgChartLevelViewModel TopLevel { get; set; }

        [JsonProperty(PropertyName = "clip")]
        public OrgChartClipViewModel Clip { get; set; }
    }
}