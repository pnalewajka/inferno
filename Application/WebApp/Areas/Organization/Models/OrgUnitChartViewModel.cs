﻿using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitChartViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "lineManagerId")]
        public long? LineManagerId { get; set; }

        [JsonProperty(PropertyName = "managerName")]
        public string ManagerName { get; set; }

        [JsonProperty(PropertyName = "managerTitle")]
        public string ManagerTitle { get; set; }

        [JsonProperty(PropertyName = "imageAddress")]
        public string ImageAddress { get; set; }

        [JsonIgnore]
        public string Code { get; set; }
    }
}