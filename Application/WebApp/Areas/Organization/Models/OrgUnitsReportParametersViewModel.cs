﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Resources;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    [Identifier("ViewModel.OrgUnitsReportParameters")]
    public class OrgUnitsReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(OrgUnitResources.OrgUnitIdsLabel), typeof(OrgUnitResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long[] OrgUnitIds { get; set; }
    }
}