﻿using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartLevelViewModel : OrgChartLevelItemBaseViewModel
    {
        public OrgChartLevelViewModel()
        {
            Root = new OrgUnitChartViewModel();    
        }

        [JsonProperty(PropertyName = "root")]
        public OrgUnitChartViewModel Root { get; set; }
    }
}