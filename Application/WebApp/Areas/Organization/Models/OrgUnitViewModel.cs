using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Configuration.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Resources;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    [TabDescription(0, "basic", "BasicInformationTabName", typeof(OrgUnitResources))]
    [TabDescription(1, "management", "ManagementInformationTabName", typeof(OrgUnitResources))]
    [TabDescription(2, "timetracking", "TimeTrackingTabName", typeof(OrgUnitResources))]
    public class OrgUnitViewModel : ITreeNodeItemViewModel, IValidatableObject
    {
        public long Id { get; set; }

        [Tab("basic")]
        [Order(1)]
        [NonSortable]
        [StringLength(255)]
        [Required]
        [DisplayNameLocalized(nameof(OrgUnitResources.NameLabel), typeof(OrgUnitResources))]
        public string Name { get; set; }

        [Tab("basic")]
        [Order(2)]
        [NonSortable]
        [StringLength(32)]
        [Required]
        [DisplayNameLocalized(nameof(OrgUnitResources.CodeLabel), typeof(OrgUnitResources))]
        public string Code { get; set; }

        [Tab("basic")]
        [Order(3)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(OrgUnitResources.ParentLabel), typeof(OrgUnitResources))]
        public long? ParentId { get; set; }

        [Tab("basic")]
        [Order(4)]
        [Required]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(64)]
        [DisplayNameLocalized(nameof(OrgUnitResources.FlatNameLabel), typeof(OrgUnitResources))]
        public string FlatName { get; set; }

        [Tab("basic")]
        [Order(5)]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(OrgUnitResources.DescriptionLabel), typeof(OrgUnitResources))]
        [StringLength(1024)]
        public string Description { get; set; }

        [Tab("management")]
        [Order(6)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsOrgUnitManager) + "'")]
        [DisplayNameLocalized(nameof(OrgUnitResources.EmployeeLabel), typeof(OrgUnitResources))]
        public long? EmployeeId { get; set; }

        [Tab("management")]
        [Order(7)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(OrgUnitResources.OrgUnitManagerRoleLabel), typeof(OrgUnitResources))]
        public OrgUnitManagerRole? OrgUnitManagerRole { get; set; }

        [Tab("management")]
        [Order(8)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CalendarPickerController))]
        [DisplayNameLocalized(nameof(OrgUnitResources.CalendarLabel), typeof(OrgUnitResources))]
        public long? CalendarId { get; set; }

        [Tab("management")]
        [Order(9)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(RegionController))]
        [DisplayNameLocalized(nameof(OrgUnitResources.RegionLabel), typeof(OrgUnitResources))]
        public long? RegionId { get; set; }

        [Tab("management")]
        [Order(10)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(OrgUnitResources.DefaultProjectContributionModeLabel), typeof(OrgUnitResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<InheritableBool>))]
        public InheritableBool DefaultProjectContributionMode { get; set; }

        [Tab("management")]
        [Order(11)]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(OrgUnitResources.OrgUnitFeaturesLabel), typeof(OrgUnitResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<OrgUnitFeatures>))]
        public OrgUnitFeatures OrgUnitFeatures { get; set; }

        [Tab("timetracking")]
        [Order(12)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(OrgUnitResources.TimeReportingModeLabel), typeof(OrgUnitResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "InheritedValue", ResourceType = typeof(OrgUnitResources), ItemProvider = typeof(EnumBasedListItemProvider<TimeReportingMode>))]
        public TimeReportingMode? TimeReportingMode { get; set; }

        [Tab("timetracking")]
        [Order(13)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(SimpleProjectPickerController))]
        [DisplayNameLocalized(nameof(OrgUnitResources.AbsenceOnlyDefaultProjectIdLabel), typeof(OrgUnitResources))]
        public long? AbsenceOnlyDefaultProjectId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long NameSortOrder { get; set; }

        [Visibility(VisibilityScope.None)]
        public long DescendantCount { get; set; }

        public byte[] Timestamp { get; set; }
        
        [Visibility(VisibilityScope.None)]
        [StringLength(900)]
        public string Path { get; set; }

        [Visibility(VisibilityScope.None)]
        public int? Depth { get; set; }
        
        public override string ToString()
        {
            return FlatName;
        }

        [Tab("basic")]
        [Visibility(VisibilityScope.Form)]
        [Range(0, 99998)]
        [DisplayNameLocalized(nameof(OrgUnitResources.CustomOrderLabel), typeof(OrgUnitResources))]
        public long? CustomOrder { get; set; }

        public string GetRowCssClass()
        {
            if (Depth > 0)
            {
                return $"tree-item-indent-{Depth}";
            }

            return "tree-item-indent-0";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ParentId.HasValue)
            {
                if (!TimeReportingMode.HasValue)
                {
                    yield return new ValidationResult(OrgUnitResources.InheritedError,
                        new List<string> {nameof(TimeReportingMode)});
                }

                if (DefaultProjectContributionMode == InheritableBool.Inherited)
                {
                    yield return new ValidationResult(OrgUnitResources.InheritedError,
                        new List<string> { nameof(DefaultProjectContributionMode) });
                }
            }

            if (TimeReportingMode == CrossCutting.Business.Enums.TimeReportingMode.AbsenceOnly && !AbsenceOnlyDefaultProjectId.HasValue)
            {
                yield return new ValidationResult(OrgUnitResources.EmptyAbsenceOnlyDefaultProjectError,
                    new List<string> { nameof(AbsenceOnlyDefaultProjectId) });
            }
        }
    }
}