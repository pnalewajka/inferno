﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitImportViewModelToOrgUnitImportDtoMapping : ClassMapping<OrgUnitImportViewModel, OrgUnitImportDto>
    {
        public OrgUnitImportViewModelToOrgUnitImportDtoMapping()
        {
            Mapping = v => new OrgUnitImportDto
            {
                ParentCode = v.ParentCode,
                Id = v.Id,
                Code = v.Code,
                Name = v.Name,
                Description = v.Description,
            };
        }
    }
}
