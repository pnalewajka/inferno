﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartEmployeeItemDtoToOrgChartEmployeeItemViewModelMapping : ClassMapping<OrgChartEmployeeItemDto, OrgChartEmployeeItemViewModel>
    {
        public OrgChartEmployeeItemDtoToOrgChartEmployeeItemViewModelMapping(
            IClassMapping<OrgChartLevelItemBaseDto, OrgChartLevelItemBaseViewModel> orgChartLevelItemBaseMapping)
        {
            Mapping = d => orgChartLevelItemBaseMapping.CreateFromSource(d) as OrgChartEmployeeItemViewModel;
        }
    }
}