﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitImportDtoToOrgUnitImportViewModelMapping : ClassMapping<OrgUnitImportDto, OrgUnitImportViewModel>
    {
        public OrgUnitImportDtoToOrgUnitImportViewModelMapping()
        {
            Mapping = d => new OrgUnitImportViewModel
            {
                ParentCode = d.ParentCode,
                Id = d.Id,
                Code = d.Code,
                Name = d.Name,
                Description = d.Description,
            };
        }
    }
}
