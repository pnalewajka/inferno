﻿using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitDtoToOrgUnitChartViewModelMapping : ClassMapping<OrgUnitDto, OrgUnitChartViewModel>
    {
        public OrgUnitDtoToOrgUnitChartViewModelMapping()
        {
            Mapping = dto => new OrgUnitChartViewModel
            {
                Id = dto.Id,
                Name = RemoveManagerNameFromOrgUnitName(dto.FlatName),
                Code = dto.Code
            };
        }

        private static string RemoveManagerNameFromOrgUnitName(string name)
        {
            return Regex.Replace(name, "[(].*[)]", "").Split(':').FirstOrDefault() ?? name;
        }
    }
}