﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartEmployeeRootDtoToOrgChartEmployeeRootViewModelMappingcs : ClassMapping<OrgChartEmployeeRootDto, OrgChartEmployeeRootViewModel>
    {
        public OrgChartEmployeeRootDtoToOrgChartEmployeeRootViewModelMappingcs()
        {
            Mapping = d => new OrgChartEmployeeRootViewModel
            {
                Id = d.Id,
                LongId = d.LongId,
                ImageAddress = d.ImageAddress,
                LocationName = d.LocationName,
                ManagerName = d.ManagerName,
                ManagerTitle = d.ManagerTitle
            };
        }
    }
}