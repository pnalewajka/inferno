﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public abstract class OrgChartLevelItemBaseViewModel
    {
        protected OrgChartLevelItemBaseViewModel()
        {
            Children = new List<OrgChartLevelItemBaseViewModel>();
        }

        [JsonProperty(PropertyName = "relationship")]
        public string Relationship { get; set; }

        [JsonProperty(PropertyName = "children")]
        public IList<OrgChartLevelItemBaseViewModel> Children { get; set; }
    }
}