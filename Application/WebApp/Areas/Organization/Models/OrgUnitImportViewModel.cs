﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Organization.Resources;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgUnitImportViewModel : ITreeNodeImportItemViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(OrgUnitResources.ParentLabel), typeof(OrgUnitResources))]
        [Order(4)]
        public string ParentCode { get; set; }

        [DisplayNameLocalized(nameof(OrgUnitResources.CodeLabel), typeof(OrgUnitResources))]
        [Order(2)]
        [StringLength(32)]
        [Required]
        public string Code { get; set; }

        [DisplayNameLocalized(nameof(OrgUnitResources.NameLabel), typeof(OrgUnitResources))]
        [Order(1)]
        [NonSortable]
        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(OrgUnitResources.DescriptionLabel), typeof(OrgUnitResources))]
        [Order(3)]
        [NonSortable]
        [StringLength(1024)]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(OrgUnitResources.IsDepartmentLabel), typeof(OrgUnitResources))]
        [Order(5)]
        [NonSortable]
        public bool IsDepartment { get; set; }
    }
}