﻿using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class OrgChartClipDtoToOrgChartClipViewModelMapping : ClassMapping<OrgChartClipDto, OrgChartClipViewModel>
    {
        public OrgChartClipDtoToOrgChartClipViewModelMapping()
        {
            Mapping = d => new OrgChartClipViewModel
            {
                ClipType = d.ClipType,
                FocusedNodeId = d.FocusedNodeId
            };
        }
    }
}