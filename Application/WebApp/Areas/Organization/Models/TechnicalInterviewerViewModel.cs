﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.Organization.Models
{
    public class TechnicalInterviewerViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public string LastName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string Acronym { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Required]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long EmployeeId { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.KeyTechnologiesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(SkillTagPickerController))]
        public long[] SkillTagIds { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public string LocationName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeCompany), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public string CompanyName { get; set; }

        [MinValue(1)]
        [MaxValue(5)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.CandidateMaxLevelLabel), nameof(EmployeeResources.CandidateMaxLevelShortLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public int CandidateMaxLevel { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.CanInterviewForeignCandidatesLabel), nameof(EmployeeResources.CanInterviewForeignCandidatesShortLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public bool CanCanInterviewForeignCandidates { get; set; }

        [StringLength(50)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkypeName), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public string SkypeName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkypeName), typeof(EmployeeResources))]
        public DisplayUrl GetSkypeName(UrlHelper helper) => CellContentHelper.GetSkypeLink(SkypeName, SkypeName);

        [StringLength(128)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeAvailabilityDetails), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public string AvailabilityDetails { get; set; }

        [Multiline(2)]
        [StringLength(128)]
        [DisplayNameLocalized(nameof(EmployeeResources.CommentsLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public string Comments { get; set; }

        public string GetFullName()
        {
            return $"{LastName} {FirstName} {LocationName}".Trim();
        }

        public override string ToString()
        {
            return GetFullName();
        }
    }
}
