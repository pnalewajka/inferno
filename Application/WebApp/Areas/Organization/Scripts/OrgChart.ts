﻿module OrgChart {
    class Unit {
        name: string;
        managerName: string;
        managerTitle: string;
        imageAddress: string;
        locationName: string;
        id: string;
        longId: number;
        lineManagerId?: number;
    }

    class OrganizationLevel {
        root: Unit;
        name: string;
        title: string;
        children: [OrganizationLevel];
    }

    enum ClipType {
        Unspecified = 0,
        DescendantsOnly = 1,
        AncestorsOnly = 2
    }

    export enum View {
        Normal,
        PictureOnly,
        Condensed,

        Length
    }

    export enum Direction {
        TopToBottom,
        BottomToTop,

        Length
    }

    class Clip {
        focusedNodeId: string;
        clipType: ClipType;
    }

    class PageModel {
        topLevel: OrganizationLevel;
        clip: Clip;
    }

    declare var pageModel: PageModel;
    var initialDeferred: JQueryDeferred<Object>;
    var initialPromise: JQueryPromise<Object>;

    var actualView: View = View.Normal;
    var actualDirection: Direction = Direction.BottomToTop;
    var actualZoom: number = 100;

    var observer: MutationObserver;
    var centerScrollTimeout: number | undefined = undefined;

    var initiallyFocusedNode: JQuery | undefined = undefined;

    function observeNode(node: Node) {
        const mutationObserverConfig: MutationObserverInit = {
            childList: false,
            attributes: true,
            characterData: false,
            subtree: false,
            attributeOldValue: true,
            characterDataOldValue: false,
            attributeFilter: ["class"]
        };

        observer.observe(node, mutationObserverConfig);
    }

    function selectOrgChart(): JQuery {
        return $("#orgChart");
    }

    function getOrgChartData(): OrganizationLevel {
        function mapOneLevel(level: OrganizationLevel) {
            level.name = level.root.managerName || level.root.name;
            level.title = (level.root.managerName && level.root.name) || "";

            for (let child of level.children) {
                mapOneLevel(child);
            }
        }

        mapOneLevel(pageModel.topLevel);

        return pageModel.topLevel;
    }

    function addElementsToTile($node: JQuery, level: OrganizationLevel) {
        $node.children("div").remove();

        let $table = $("<table></table>", {
            "class": "nodeTable"
        });

        let $firstRow = $("<tr></tr>");
        $table.append($firstRow);

        if (level.root.imageAddress) {
            $node.addClass("nodeWithImageContent");

            let $imageCell = $("<td></td>", {
                "rowspan": 3
            });

            $firstRow.append($imageCell);

            $imageCell.addClass("imageColumn");

            let $imageNode = $("<img></img>", {
                "class": "image"
            }).attr("src", level.root.imageAddress);

            let $imageDiv = $("<div></div>", {
                "class": "imageContainer"
            });

            $imageDiv.append($imageNode);
            $imageCell.append($imageDiv);
        }

        let $nameCell = $("<td></td>");
        let $titleDiv = $("<div></div>", {
            "class": "nodeName"
        });

        $titleDiv.append(level.root.managerName);

        const recordId = level.root.longId
            ? level.root.longId
            : level.root.lineManagerId;

        if (recordId !== undefined) {
            $titleDiv
                .addClass(Hub.hubClass)
                .click(() => { Hub.open(recordId, "EmployeeValuePickers.Employee"); })
        }

        $nameCell.append($titleDiv);
        $firstRow.append($nameCell);

        let $orgRow;

        if (level.root.name) {
            $orgRow = $("<tr></tr>");

            let $orgCell = $("<td></td>");

            let $orgNode = $("<div></div>", {
                "class": "nodeOrg"
            }).text(level.root.name);

            $orgCell.append($orgNode);
            $orgRow.append($orgCell);
        }

        let $managerTitleRow;

        if (level.root.managerTitle) {

            $managerTitleRow = $("<tr></tr>");

            let $managerTitleCell = $("<td></td>");
            let $locationInfo = level.root.locationName ? ", " + level.root.locationName : "";

            let $managerTitleNode = $("<div></div>", {
                "class": "nodeManagerTitle"
            }).text(level.root.managerTitle + $locationInfo);

            $managerTitleCell.append($managerTitleNode);
            $managerTitleRow.append($managerTitleCell);
        }

        let $emptyRow = $("<tr></tr>");
        let $emptyCell = $("<td></td>");
        $emptyCell.html("&nbsp;");
        $emptyRow.append($emptyCell);

        if ($orgRow) {

            $table.append($orgRow);

            if ($managerTitleRow) {
                $table.append($managerTitleRow);
            }
        } else {
            if ($managerTitleRow) {
                $table.append($managerTitleRow);
            }
        }

        let tooltip = (level.root.managerName || "") + "\n" +
            (level.root.name || "") + "\n" +
            (level.root.managerTitle ? level.root.managerTitle + (level.root.locationName ? ", " + level.root.locationName : "") : "");

        $node.prepend($table);
        $node.attr("title", tooltip);

        Utils.pushBackBrowserTask(() => {
            $node.parent().addClass("td-encapsulating-node");
            observeNode($node.get(0));
        });
    }

    function showOnlyAncestors($node: JQuery, level: OrganizationLevel) {
        let chart = selectOrgChart();

        function hideRecursive($singleNode: JQuery) {
            chart.orgchart("hideSiblings", $singleNode);
            let $parents = chart.orgchart("getRelatedNodes", $singleNode, "parent");

            if ($parents.length) {
                hideRecursive($($parents[0]));
            }
        }

        initialPromise = initialPromise.then(() => {
            chart.orgchart("hideChildren", $node);
            hideRecursive($node);

            return {};
        });
    }

    function showOnlyDescendants($node: JQuery, level: OrganizationLevel) {
        let chart = selectOrgChart();

        function showRecursive($singleNode: JQuery) {
            chart.orgchart("showChildren", $singleNode);
            let $grandchildren = chart.orgchart("getRelatedNodes", $singleNode, "children");
            for (let index = 1; index < $grandchildren.length; ++index) {
                showRecursive($($grandchildren[index]));
            }
        }

        initialPromise = initialPromise.then(() => {
            chart.orgchart("hideParent", $node);
            showRecursive($node);

            return {};
        });
    }

    function clipIfNeeded($node: JQuery, level: OrganizationLevel) {
        if (pageModel.clip && level.root && pageModel.clip.focusedNodeId == level.root.id) {
            initiallyFocusedNode = $node;

            if (pageModel.clip.clipType === ClipType.AncestorsOnly) {
                showOnlyAncestors($node, level);
            } else if (pageModel.clip.clipType === ClipType.DescendantsOnly) {
                showOnlyDescendants($node, level);
            }
        }
    }

    function getLocatedViewString(view: View) {
        switch (view) {
            case View.Normal:
                return Globals.resources.Normal;

            case View.PictureOnly:
                return Globals.resources.PictureOnly;

            case View.Condensed:
                return Globals.resources.Condensed;
        }

        return "";
    }

    function getLocatedDirectionString(direction: Direction) {
        switch (direction) {
            case Direction.TopToBottom:
                return Globals.resources.TopToBottom;

            case Direction.BottomToTop:
                return Globals.resources.BottomToTop;
        }

        return "";
    }

    function getClassByView(view: View) {
        switch (view) {
            case View.Normal:
                return "normal";

            case View.PictureOnly:
                return "pictureOnly";

            case View.Condensed:
                return "condensed";
        }

        return "";
    }

    function getClassByDirection(direction: Direction) {
        switch (direction) {
            case Direction.TopToBottom:
                return "t2b";

            case Direction.BottomToTop:
                return "b2t";
        }

        return "";
    }

    function setDropdownTitles() {
        $("#viewDropdownTitle").text(Globals.resources.View);
        $("#directionDropdownTitle").text(Globals.resources.Direction);
        $("#zoomDropdownTitle").text(Globals.resources.Zoom);
    }

    function initializeViewDropdown() {
        let viewDropdown = $("#viewDropdownItems");

        for (let i = 0; i < View.Length; i++) {
            let aTag = $("<a></a>");

            aTag.attr("role", "menuitem");
            aTag.attr("tabindex", "-1");
            aTag.attr("href", "#");
            aTag.attr("onclick", `OrgChart.setView(OrgChart.View.${View[i]})`);
            aTag.addClass("filter-menu-item");
            aTag.attr("data-view-config", View[i]);

            let spanTag = $("<span></span>");

            spanTag.text(getLocatedViewString(i));
            aTag.append(spanTag);

            let liTag = $("<li></li>");

            liTag.attr("role", "presentation");
            liTag.append(aTag);

            viewDropdown.append(liTag);
        }

        $("#viewDropdown").removeClass("uninitializedDropdown");
    }

    function initializeDirectionDropdown() {
        let directionDropdown = $("#directionDropdownItems");

        for (let i = 0; i < Direction.Length; i++) {
            let aTag = $("<a></a>");

            aTag.attr("role", "menuitem");
            aTag.attr("tabindex", "-1");
            aTag.attr("href", "#");
            aTag.attr("onclick", `OrgChart.setDirection(OrgChart.Direction.${Direction[i]})`);
            aTag.addClass("filter-menu-item");
            aTag.attr("data-direction-config", Direction[i]);

            let spanTag = $("<span></span>");

            spanTag.text(getLocatedDirectionString(i));
            aTag.append(spanTag);

            let liTag = $("<li></li>");

            liTag.attr("role", "presentation");
            liTag.append(aTag);

            directionDropdown.append(liTag);
        }

        $("#directionDropdown").removeClass("uninitializedDropdown");
    }

    function initializeZoomDropdown() {
        let viewDropdown = $("#zoomDropdownItems");

        for (let i = 150; i >= 50; i -= 25) {
            let aTag = $("<a></a>");

            aTag.attr("role", "menuitem");
            aTag.attr("tabindex", "-1");
            aTag.attr("href", "#");
            aTag.attr("onclick", `OrgChart.setZoom(${i})`);
            aTag.addClass("filter-menu-item");
            aTag.attr("data-zoom-config", i);

            let spanTag = $("<span></span>");

            spanTag.text(i + "%");
            aTag.append(spanTag);

            let liTag = $("<li></li>");

            liTag.attr("role", "presentation");
            liTag.append(aTag);

            viewDropdown.append(liTag);
        }

        let zoomDropdown = $("#zoomDropdown");

        zoomDropdown.attr("title", Globals.resources.ZoomTooltip);
        zoomDropdown.removeClass("uninitializedDropdown");
        zoomDropdown.on("mousewheel DOMMouseScroll", (event: JQueryMouseEventObject) => {
            let change = (<WheelEvent>event.originalEvent).wheelDelta > 0 || (<WheelEvent>event.originalEvent).detail < 0 ? 1 : -1;

            if (change < 0) {
                actualZoom -= 25;

                if (actualZoom < 50) {
                    actualZoom = 50;
                }
            } else {
                actualZoom += 25;

                if (actualZoom > 150) {
                    actualZoom = 150;
                }
            }

            setZoom(actualZoom);

            event.preventDefault();
        });
    }

    function initializeDropdowns() {
        setDropdownTitles();
        initializeViewDropdown();
        initializeDirectionDropdown();
        initializeZoomDropdown();
    }

    export function setView(view: View) {
        $(".orgchart").removeClass(getClassByView(actualView));
        $(".orgchart").addClass(getClassByView(view));

        actualView = view;

        $("#viewDropdownItems").find(".radio").removeClass("radio");
        $("#viewDropdownItems").find(`[data-view-config='${View[view]}']`).addClass("radio");
    }

    export function setDirection(direction: Direction) {
        $(".orgchart").removeClass(getClassByDirection(actualDirection));
        $(".orgchart").addClass(getClassByDirection(direction));

        actualDirection = direction;

        $("#directionDropdownItems").find(".radio").removeClass("radio");
        $("#directionDropdownItems").find(`[data-direction-config='${Direction[direction]}']`).addClass("radio");
    }

    export function setZoom(zoom: number) {
        let translation = zoom >= 100 ? (1 - 100 / zoom) * 100 : (zoom - 100) / 2;

        $("#orgChart")
            .css("transform", `translate(${translation}%, ${translation}%) scale(${(zoom / 100)})`)
            .css("height", zoom + "%");

        actualZoom = zoom;

        if (zoom < 100) {
            $(".orgchart").addClass("zoomedOut");
        } else {
            $(".orgchart").removeClass("zoomedOut");
        }

        $("#zoomDropdownItems").find(".radio").removeClass("radio");
        $("#zoomDropdownItems").find(`[data-zoom-config='${zoom}']`).addClass("radio");
    }

    function createNode($node: JQuery, level: OrganizationLevel) {
        addElementsToTile($node, level);
        clipIfNeeded($node, level);
    }

    function getNodeUrl($nodeData: any) {
        return "/Organization/OrgUnitChart/Children?id=" + $nodeData.root.id;
    }

    function centerScrollAnimated(container: JQuery, element: JQuery) {
        let containerOffset = container.offset();
        let scrollLeft = container.scrollLeft();
        let scrollTop = container.scrollTop();

        let elementOffset = element.offset();
        let zoom = actualZoom / 100;

        $("#orgChartContainer").animate({
            scrollTop: scrollTop + elementOffset.top - containerOffset.top + element.height() * zoom / 2 - container.height() / 2,
            scrollLeft: scrollLeft + elementOffset.left - containerOffset.left + element.width() * zoom / 2 - container.width() / 2
        }, 500);
    }

    function centerScroll(container: JQuery, element: JQuery) {
        let containerOffset = container.offset();
        let scrollLeft = container.scrollLeft();
        let scrollTop = container.scrollTop();

        let elementOffset = element.offset();
        let zoom = actualZoom / 100;

        let orgChartContainer = $("#orgChartContainer");

        orgChartContainer.scrollTop(scrollTop + elementOffset.top - containerOffset.top + element.height() * zoom / 2 - container.height() / 2);
        orgChartContainer.scrollLeft(scrollLeft + elementOffset.left - containerOffset.left + element.width() * zoom / 2 - container.width() / 2);
    }

    function initializeOrgChart() {
        observer = new MutationObserver((mutations: MutationRecord[], observer: MutationObserver) => {
            for (let mutation of mutations) {
                let element = <HTMLElement>mutation.target;
                let oldClasses = mutation.oldValue!.split(" ");

                // JQuery.OrgChart library hack to detect opened children
                if (!element.classList.contains("slide")
                    && $.inArray("slide", oldClasses) >= 0) {
                    if (centerScrollTimeout != undefined) {
                        clearTimeout(centerScrollTimeout);
                    }

                    centerScrollTimeout = window.setTimeout(() => {
                        centerScrollTimeout = undefined;

                        let container = $("#orgChartContainer");
                        let node = $(element).closest(".nodes").parent();

                        centerScrollAnimated(container, node);
                    }, 1);
                }
            }
        });

        let config = {
            "data": getOrgChartData(),
            "nodeTitle": "name",
            "nodeContent": "title",
            "exportButton": true,
            "parentNodeSymbol": "fa-folder-open-o",
            "createNode": createNode,
            "ajaxURL": {
                "children": getNodeUrl
            }
        }

        if (!pageModel.clip) {
            (config as any)["depth"] = 3;
        }

        let orgChart = selectOrgChart();

        orgChart.orgchart(config);

        setView(actualView);
        setDirection(actualDirection);
        setZoom(actualZoom);

        if (initiallyFocusedNode != undefined) {
            let container = $("#orgChartContainer");

            centerScroll(container, initiallyFocusedNode);
        }
    }

    function showOrgChart() {
        initialPromise = initialPromise.then(() => {
            let eventToHandle = "transitionend";
            let selector = "*";

            selectOrgChart().on(eventToHandle, selector, () => {
                selectOrgChart().removeClass("initiallyInvisible");
                selectOrgChart().off(eventToHandle, selector);
            });

            return {};
        });
    }

    function resolveInitialPromise() {
        setTimeout(() => {
            initialDeferred.resolve();
        });
    }

    $(() => {
        initialDeferred = $.Deferred();
        initialPromise = initialDeferred.promise();

        initializeDropdowns();
        initializeOrgChart();

        showOrgChart();
        $(".oc-export-btn")
            .appendTo("#orgChartToolbar")
            .removeClass("oc-export-btn")
            .addClass("btn btn-default");
        resolveInitialPromise();
    });
}