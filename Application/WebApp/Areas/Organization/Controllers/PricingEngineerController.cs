﻿using System.Collections.Generic;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using Smt.Atomic.WebApp.Areas.Organization.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;

namespace Smt.Atomic.WebApp.Areas.Organization.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewPricingEngineer)]
    public class PricingEngineerController : CardIndexController<PricingEngineerViewModel, PricingEngineerDto>
    {
        public PricingEngineerController(IPricingEngineerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = PricingEngineerResources.PricingEngineers;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddPricingEngineer;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditPricingEngineer;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewPricingEngineer;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeletePricingEngineer;

            SetupFilterGroups();
        }

        private void SetupFilterGroups()
        {
            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<ProjectKeyTechnologyFilterViewModel>(OrganizationFilterCodes.KeyTechnologyFilter, PricingEngineerResources.PricingEngineersKeyTechnologyFilterName)
                    }
                },
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<LocationFilterViewModel>(OrganizationFilterCodes.LocationFilter, PricingEngineerResources.PricingEngineersLocationFilterName)
                    }
                },
            };
        }
    }
}