﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using Smt.Atomic.WebApp.Areas.Organization.Resources;
using Smt.Atomic.WebApp.Models;

namespace Smt.Atomic.WebApp.Areas.Organization.Controllers
{
    [Identifier("ValuePickers.OrgUnit")]
    public class OrgUnitPickerController
        : OrgUnitController
        , IHubController
    {
        private readonly IEmployeeService _employeeService;
        private readonly IClassMapping<EmployeeDto, EmployeeViewModel> _employeeDtoToViewModelMapping;

        private const string MinimalViewId = "minimal";

        public OrgUnitPickerController(
            IOrgUnitCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IEmployeeService employeeService,
            IClassMapping<EmployeeDto, EmployeeViewModel> employeeDtoToViewModelMapping)
            : base(cardIndexDataService, baseControllerDependencies, null)
        {
            _employeeService = employeeService;
            _employeeDtoToViewModelMapping = employeeDtoToViewModelMapping;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        public PartialViewResult Hub(long id)
        {
            var orgUnit = CardIndex.GetRecordById(id);
            EmployeeDto employeeDto = null;

            if (orgUnit.EmployeeId.HasValue)
            {
                employeeDto = _employeeService.GetEmployeesById(new[] { orgUnit.EmployeeId.Value })[0];
            }

            var model = new OrgUnitHubViewModel
            {
                EmployeeId = orgUnit.EmployeeId,
                Employee = employeeDto != null ? _employeeDtoToViewModelMapping.CreateFromSource(employeeDto) : null,
                Name = orgUnit.Name,
                FlatName = orgUnit.FlatName,
            };

            var dialogModel = new ModelBasedDialogViewModel<OrgUnitHubViewModel>(model, StandardButtons.None)
            {
                ClassName = "orgunit-hub",
                Title = orgUnit.FlatName,
            };

            return PartialView(
                "~/Areas/Organization/Views/Hubs/OrgUnitHub.cshtml",
                dialogModel);
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<OrgUnitViewModel>(OrgUnitResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<OrgUnitViewModel, object>>>
            {
                c => c.Name,
                c => c.Code,
                c => c.EmployeeId
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}