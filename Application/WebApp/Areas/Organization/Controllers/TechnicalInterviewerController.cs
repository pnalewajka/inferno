﻿using System.Collections.Generic;
using Smt.Atomic.Business.Organization.Consts;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using Smt.Atomic.WebApp.Areas.Organization.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Areas.Organization.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTechnicalInterviewer)]
    public class TechnicalInterviewerController : CardIndexController<TechnicalInterviewerViewModel, TechnicalInterviewerDto>
    {
        private const int ExportPageSize = 1000;
        private const string UsersParameterName = "users";
        private const string SelectedAcronymToken = "~selected.acronym";

        public TechnicalInterviewerController(ITechnicalInterviewerCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = MenuResources.TechnicalInterviewers;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddTechnicalInterviewer;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditTechnicalInterviewer;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewTechnicalInterviewer;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteTechnicalInterviewer;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportTechnicalInterviewers);

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Icon = FontAwesome.CalendarO,
                Text = TechnicalInterviewerResources.CheckAvailability,
                OnClickAction = Url.UriActionGet<AbsenceScheduleController>(c => c.ScheduleList(null, null, null, null), KnownParameter.None, UsersParameterName, SelectedAcronymToken),
                RequiredRole = SecurityRoleType.CanViewAbsenceSchedule,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                }
            });

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<TechnicalInterviewerViewModel>(m => m.AvailabilityDetails).Visibility = ControlVisibility.Remove;
                form.GetControlByName<TechnicalInterviewerViewModel>(m => m.SkypeName).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<TechnicalInterviewerViewModel>(m => m.EmployeeId).IsReadOnly = true;
            };

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.EmployeeName, TechnicalInterviewerResources.EmployeeNameSearchAreaLabel),
                new SearchArea(SearchAreaCodes.SkypeName, TechnicalInterviewerResources.SkypeNameSearchAreaLabel),
                new SearchArea(SearchAreaCodes.SkillTag, TechnicalInterviewerResources.SkillTagSearchAreaLabel)
            };

            SetupFilterGroups();
        }

        private void SetupFilterGroups()
        {
            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<SkillTagFilterViewModel>(OrganizationFilterCodes.SkillTagFilter, TechnicalInterviewerResources.TechnicalInterviewerKeyTechnologyFilterName)
                    }
                },
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<LocationFilterViewModel>(OrganizationFilterCodes.LocationFilter, TechnicalInterviewerResources.TechnicalInterviewerLocationFilterName)
                    }
                },
            };
        }

        public override System.Web.Mvc.FilePathResult ExportToExcel(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;

            return base.ExportToExcel(parameters);
        }

        public override System.Web.Mvc.FilePathResult ExportToCsv(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;

            return base.ExportToCsv(parameters);
        }
    }
}
