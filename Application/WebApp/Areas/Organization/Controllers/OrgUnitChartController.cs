﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using Smt.Atomic.WebApp.Areas.Organization.Resources;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Areas.Organization.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewOrgUnits)]
    public class OrgUnitChartController : AtomicController
    {
        private readonly IOrgChartService _orgChartService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IClassMapping<OrgChartDto, OrgChartViewModel> _orgChartMapping;
        private readonly IClassMapping<OrgChartEmployeeDto, OrgChartEmployeeViewModel> _orgChartEmployeeMapping;
        private readonly IEmployeeService _employeeService;

        public OrgUnitChartController(
            IBaseControllerDependencies baseDependencies,
            IOrgChartService orgChartService,
            IOrgUnitService orgUnitService,
            IClassMapping<OrgUnitDto, OrgUnitChartViewModel> orgUnitMapping,
            IClassMapping<OrgChartDto, OrgChartViewModel> orgChartMapping,
            IClassMapping<OrgChartEmployeeDto, OrgChartEmployeeViewModel> orgChartEmployeeMapping,
            IEmployeeService employeeService,
            ISkillService skillService,
            ISystemParameterService parameterService) : base(baseDependencies)
        {
            _orgChartService = orgChartService;
            _orgUnitService = orgUnitService;
            _orgChartMapping = orgChartMapping;
            _orgChartEmployeeMapping = orgChartEmployeeMapping;
            _employeeService = employeeService;
        }

        public ActionResult OrgChart(long? id, ClipType? clipType, bool isEmployeeId = false)
        {
            Layout.Scripts.Add("~/Areas/Organization/Scripts/orgChart.js");

            Layout.Css.Add("~/Content/jquery.orgchart.css");
            Layout.Css.Add("~/Areas/Organization/Content/OrgChart.css");

            Layout.Resources.AddFrom<OrgChartResources>();

            Layout.PageTitle = MenuResources.OrgChart;
            Layout.PageHeader = MenuResources.OrgChart;

            var orgChart = GetOrganizationTree(id, clipType, isEmployeeId);

            return View(orgChart);
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Client, VaryByParam = "id")]
        public ActionResult Children(string id)
        {
            long lineManagerId;
            long? orgUnitId = null;

            if (id.StartsWith("e"))
            {
                lineManagerId = long.Parse(id.Substring(1));
            }
            else
            {
                orgUnitId = long.Parse(id);
                lineManagerId = _orgUnitService.GetOrgUnitManagerId(orgUnitId.Value).Value;
            }

            var result = _orgChartEmployeeMapping.CreateFromSource(_orgChartService.GetEmployeeChildren(lineManagerId, orgUnitId));

            return new JsonNetResult(result);
        }

        private OrgChartViewModel GetOrganizationTree(long? id, ClipType? clipType, bool isEmployeeId)
        {
            var orgTree = _orgChartMapping.CreateFromSource(_orgChartService.GetOrganizationTree());

            OrgChartEmployeeViewModel lastLevel = null;

            if (ModelState.IsValid && id.HasValue && clipType.HasValue && clipType != ClipType.Unspecified)
            {
                orgTree.Clip = new OrgChartClipViewModel
                {
                    ClipType = clipType.Value,
                    FocusedNodeId = id.Value.ToString()
                };

                if (isEmployeeId)
                {
                    var orgUnits = _orgUnitService.GetAllOrgUnits().ToList();
                    var employeeDescendants = GetEmployeeDescendants(id, orgUnits, orgTree);
                    lastLevel = employeeDescendants.Descendants;
                    id = employeeDescendants.Id;
                }
            }

            var employeesWithChildren = _employeeService.GetLineManagersWithNotTerminatedChildrenIds();

            SetRelationshipAndChildren(orgTree.TopLevel, null, id, lastLevel, employeesWithChildren);

            return orgTree;
        }

        private EmployeeDescendants GetEmployeeDescendants(long? id, IList<OrgUnitDto> orgUnits, OrgChartViewModel orgTree)
        {
            OrgChartEmployeeViewModel lastLevel;
            var employee = _employeeService.GetEmployeeOrDefaultById(id.Value);
            var selectedEmployee = employee;
            var selectedEmployeeChildren = lastLevel = _orgChartEmployeeMapping.CreateFromSource(_orgChartService.GetEmployeeChildren(employee.Id, employee.OrgUnitId, true));

            EmployeeDto lastEmployee;

            while (employee.LineManagerId != null)
            {
                var level = _orgChartEmployeeMapping.CreateFromSource(_orgChartService.GetEmployeeChildren(employee.LineManagerId.Value, employee.OrgUnitId, true));

                if (lastLevel != null)
                {
                    var employeeId = "e" + employee.Id;
                    var toExtend = level.Children.Single(x => x.Id == employeeId);
                    toExtend.Children = lastLevel.Children.OfType<OrgChartLevelItemBaseViewModel>().ToList();
                }

                lastLevel = level;
                lastEmployee = employee;

                var orgUnit = orgUnits.Where(x => x.EmployeeId == lastEmployee.LineManagerId)
                    .OrderBy(x => x.DescendantCount)
                    .FirstOrDefault();

                if (orgUnit != null)
                {
                    if (orgTree.Clip.ClipType == ClipType.DescendantsOnly && !selectedEmployeeChildren.Children.Any())
                    {
                        if (orgUnit.EmployeeId == selectedEmployee.LineManagerId)
                        {
                            orgTree.Clip.FocusedNodeId = orgUnit.Id.ToInvariantString();
                        }
                        else
                        {
                            orgTree.Clip.FocusedNodeId = "e" + selectedEmployee.LineManagerId.Value;
                        }
                    }
                    else
                    {
                        orgTree.Clip.FocusedNodeId = "e" + id;
                    }

                    return new EmployeeDescendants()
                    {
                        Descendants = lastLevel,
                        Id = orgUnit.Id
                    };
                }

                employee = _employeeService.GetEmployeeOrDefaultById(employee.LineManagerId.Value);
            }

            return null;
        }

        private void SetRelationshipAndChildren(OrgChartLevelItemBaseViewModel node, OrgChartLevelItemBaseViewModel parent, long? id, OrgChartEmployeeViewModel treeWithEmployee, IEnumerable<long> employeesWithChildren)
        {
            var hasParent = parent != null;
            var hasSiblings = hasParent && parent.Children.Count > 1;
            var hasChildren = node.Children != null && node.Children.Count > 0;

            var orgNode = node as OrgChartLevelViewModel;

            if (orgNode != null)
            {
                if (orgNode.Root.Id == id)
                {
                    if (treeWithEmployee == null && !hasChildren)
                    {
                        var employee = _orgChartEmployeeMapping.CreateFromSource(_orgChartService.GetEmployeeChildren(orgNode.Root.LineManagerId.Value, null));
                        node.Children = employee.Children.OfType<OrgChartLevelItemBaseViewModel>().ToList();
                    }
                    else if (treeWithEmployee != null)
                    {
                        if (hasChildren)
                        {
                            var orgNodeChildren = node.Children
                                .OfType<OrgChartLevelViewModel>()
                                .OfType<OrgChartLevelItemBaseViewModel>();
                            node.Children = orgNodeChildren
                                .Union(treeWithEmployee.Children)
                                .OfType<OrgChartLevelItemBaseViewModel>().ToList();
                        }
                        else
                        {
                            node.Children = treeWithEmployee.Children.OfType<OrgChartLevelItemBaseViewModel>().ToList();
                        }
                    }

                    hasChildren = node.Children.Any();
                }
                else if (orgNode.Root.LineManagerId.HasValue && !hasChildren)
                {
                    if (employeesWithChildren != null)
                    {
                        hasChildren = employeesWithChildren.Contains(orgNode.Root.LineManagerId.Value);

                        if (hasChildren)
                        {
                            var employee = _orgChartEmployeeMapping.CreateFromSource(_orgChartService.GetEmployeeChildren(orgNode.Root.LineManagerId.Value, null));
                            node.Children = employee.Children.OfType<OrgChartLevelItemBaseViewModel>().ToList();
                        }
                    }
                    else
                    {
                        hasChildren = _employeeService
                            .GetEmployeesByLineManagerId(orgNode.Root.LineManagerId.Value)
                            .Any(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated);
                    }
                }
            }

            var employeeNode = node as OrgChartEmployeeItemViewModel;

            if (employeeNode != null && !hasChildren)
            {
                if (employeesWithChildren != null)
                {
                    hasChildren = employeesWithChildren.Contains(employeeNode.Root.LongId);
                }
                else
                {
                    hasChildren = _employeeService
                        .GetEmployeesByLineManagerId(employeeNode.Root.LongId)
                        .Any(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated);
                }
            }

            node.Relationship = $"{(hasParent ? 1 : 0)}{(hasSiblings ? 1 : 0)}{(hasChildren ? 1 : 0)}";

            if (node.Children != null)
            {
                foreach (var child in node.Children)
                {
                    SetRelationshipAndChildren(child, node, id, treeWithEmployee, employeesWithChildren);
                }
            }
        }

        private class EmployeeDescendants
        {
            public OrgChartEmployeeViewModel Descendants { get; set; }

            public long Id { get; set; }
        }
    }
}