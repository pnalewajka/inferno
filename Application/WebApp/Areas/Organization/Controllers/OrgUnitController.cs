﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.TreeView;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using Smt.Atomic.WebApp.Areas.Organization.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;

namespace Smt.Atomic.WebApp.Areas.Organization.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewOrgUnits)]
    [PerformanceTest(nameof(OrgUnitController.List))]
    public class OrgUnitController : CardIndexController<OrgUnitViewModel, OrgUnitDto>
    {
        private readonly IOrgUnitCardIndexDataService _cardIndexDataService;

        public OrgUnitController(IOrgUnitCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IOrgUnitImporterCardIndexDataService importerCardIndexDataService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = OrgUnitResources.OrgUnitControllerTitle;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddOrgUnits;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewOrgUnits;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditOrgUnits;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteOrgUnits;
            CardIndex.Settings.TreeMode = TreeDisplayMode.Collapsible;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(OrganizationFilterCodes.Name, OrgUnitResources.NameLabel),
                new SearchArea(OrganizationFilterCodes.Code, OrgUnitResources.CodeLabel),
                new SearchArea(OrganizationFilterCodes.FlatName, OrgUnitResources.FlatNameLabel),
                new SearchArea(OrganizationFilterCodes.Description, OrgUnitResources.DescriptionLabel),
                new SearchArea(OrganizationFilterCodes.Employee, OrgUnitResources.EmployeeLabel),
            };

            CardIndex.Settings.DeleteButton.Confirmation.Message = OrgUnitResources.DeleteOrgUnitConfirmationMessage;

            CardIndex.Settings.PageSize = 100;

            if (importerCardIndexDataService != null)
            {
                var indexImportSettings = CardIndex.Settings.AddImport<OrgUnitImportViewModel, OrgUnitImportDto>(importerCardIndexDataService, OrgUnitResources.ImportWindowTitle);
                indexImportSettings.RequiredRole = SecurityRoleType.CanAddOrgUnits;
            }
        }

        public override ActionResult Delete([ModelBinder(typeof(CommaSeparatedListModelBinder))] IList<long> ids)
        {
            var result = _cardIndexDataService.ValidateRelationConstraints(ids);
            if (!result.IsOrgUnitBound)
            {
                return base.Delete(ids);
            }

            var model = new OrgUnitRelationsConstraintsViewModel
            {
                Employees = result.Employees,
                Projects = result.Projects,
                SurveyConducts = result.SurveyConducts
            };

            CardIndex.Settings.Header = OrgUnitResources.OrgUnitBoundedErrorHeader;

            return View("OrgUnitDeleteError", model);
        }
    }
}