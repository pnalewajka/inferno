﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Organization.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class TechnicalInterviewerResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TechnicalInterviewerResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Organization.Resources.TechnicalInterviewerResources", typeof(TechnicalInterviewerResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check Availability.
        /// </summary>
        internal static string CheckAvailability {
            get {
                return ResourceManager.GetString("CheckAvailability", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employee Name.
        /// </summary>
        internal static string EmployeeNameSearchAreaLabel {
            get {
                return ResourceManager.GetString("EmployeeNameSearchAreaLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skill Tags.
        /// </summary>
        internal static string SkillTagSearchAreaLabel {
            get {
                return ResourceManager.GetString("SkillTagSearchAreaLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skype Name.
        /// </summary>
        internal static string SkypeNameSearchAreaLabel {
            get {
                return ResourceManager.GetString("SkypeNameSearchAreaLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Technologies....
        /// </summary>
        internal static string TechnicalInterviewerKeyTechnologyFilterName {
            get {
                return ResourceManager.GetString("TechnicalInterviewerKeyTechnologyFilterName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location....
        /// </summary>
        internal static string TechnicalInterviewerLocationFilterName {
            get {
                return ResourceManager.GetString("TechnicalInterviewerLocationFilterName", resourceCulture);
            }
        }
    }
}
