﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AssetsRequestResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AssetsRequestResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.PeopleManagement.Resources.AssetsRequestResources", typeof(AssetsRequestResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment for Backpack.
        /// </summary>
        public static string AssetTypeBackpackCommentLabel {
            get {
                return ResourceManager.GetString("AssetTypeBackpackCommentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Backpack.
        /// </summary>
        public static string AssetTypeBackpackLabel {
            get {
                return ResourceManager.GetString("AssetTypeBackpackLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment for Hardware Set.
        /// </summary>
        public static string AssetTypeHardwareSetCommentLabel {
            get {
                return ResourceManager.GetString("AssetTypeHardwareSetCommentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hardware Set.
        /// </summary>
        public static string AssetTypeHardwareSetLabel {
            get {
                return ResourceManager.GetString("AssetTypeHardwareSetLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment for Headset.
        /// </summary>
        public static string AssetTypeHeadsetCommentLabel {
            get {
                return ResourceManager.GetString("AssetTypeHeadsetCommentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Headset.
        /// </summary>
        public static string AssetTypeHeadsetLabel {
            get {
                return ResourceManager.GetString("AssetTypeHeadsetLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment for Optional Hardware.
        /// </summary>
        public static string AssetTypeOptionalHardwareCommentLabel {
            get {
                return ResourceManager.GetString("AssetTypeOptionalHardwareCommentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Optional Hardware.
        /// </summary>
        public static string AssetTypeOptionalHardwareLabel {
            get {
                return ResourceManager.GetString("AssetTypeOptionalHardwareLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment for Other.
        /// </summary>
        public static string AssetTypeOtherCommentLabel {
            get {
                return ResourceManager.GetString("AssetTypeOtherCommentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        public static string AssetTypeOtherLabel {
            get {
                return ResourceManager.GetString("AssetTypeOtherLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment for Software.
        /// </summary>
        public static string AssetTypeSoftwareCommentLabel {
            get {
                return ResourceManager.GetString("AssetTypeSoftwareCommentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Software.
        /// </summary>
        public static string AssetTypeSoftwareLabel {
            get {
                return ResourceManager.GetString("AssetTypeSoftwareLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comments for IT.
        /// </summary>
        public static string CommentsForITLabel {
            get {
                return ResourceManager.GetString("CommentsForITLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employee.
        /// </summary>
        public static string EmployeeLabel {
            get {
                return ResourceManager.GetString("EmployeeLabel", resourceCulture);
            }
        }
    }
}
