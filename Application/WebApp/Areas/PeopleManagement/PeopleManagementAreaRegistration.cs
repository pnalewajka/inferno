﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement
{
    public class PeopleManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName => "PeopleManagement";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "OnboardingRequest_before_request_refactor",
                "PeopleManagement/OnboardingRequest/{*path}",
                new { controller = "OnboardingRedirectRequest", action = "RedirectToNewController" }
            );

            context.MapRoute(
                "PeopleManagement_default",
                "PeopleManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional });
        }
    }
}