﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Models;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers
{
    [Identifier("ValuePickers.AssetType")]
    public class AssetTypePickerController : AssetTypeController
    {
        private const string MinimalViewId = "minimal";

        public AssetTypePickerController(
            IAssetTypeCardIndexService cardIndexDataService,
            IBaseControllerDependencies dependencies,
            IUnitOfWorkService<IPeopleManagementDbScope> peopleManagementUnitOfWorkService)
            : base(cardIndexDataService, dependencies, peopleManagementUnitOfWorkService)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfiguration();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfiguration()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<AssetTypeViewModel>(AssetTypeResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<AssetTypeViewModel, object>>>
            {
                c => c.Name,
                x => x.Category,
                x => x.Description
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}