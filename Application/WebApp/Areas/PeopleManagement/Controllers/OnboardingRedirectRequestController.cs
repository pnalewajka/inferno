﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers
{
    public class OnboardingRedirectRequestController : Controller
    {
        public ActionResult RedirectToNewController()
        {
            // redirect to new area
            var newUrl = Request.RawUrl.Replace(
                RoutingHelper.GetAreaName<OnboardingRedirectRequestController>(),
                RoutingHelper.GetAreaName<OnboardingRequestController>());

            return RedirectPermanent(newUrl);
        }
    }
}
