﻿using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers
{
    [Identifier("ValuePickers.EmploymentTerminationReason")]
    public class EmploymentTerminationReasonPickerController : EmploymentTerminationReasonController
    { 
        public EmploymentTerminationReasonPickerController(IEmploymentTerminationReasonCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
        }
    }
}