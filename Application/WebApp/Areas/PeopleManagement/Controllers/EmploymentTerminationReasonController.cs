﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Models;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmploymentTerminationReasons)]
    public class EmploymentTerminationReasonController : CardIndexController<EmploymentTerminationReasonViewModel, EmploymentTerminationReasonDto>
    {
        public EmploymentTerminationReasonController(IEmploymentTerminationReasonCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = EmploymentTerminationRequestResources.ReasonTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddEmploymentTerminationReasons;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditEmploymentTerminationReasons;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteEmploymentTerminationReasons;
        }
    }
}