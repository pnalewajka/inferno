﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Models;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewAssetTypes)]
    public class AssetTypeController : CardIndexController<AssetTypeViewModel, AssetTypeDto, FilteredAssetTypePickerContext>
    {
        private readonly IUnitOfWorkService<IPeopleManagementDbScope> _peopleManagementUnitOfWorkService;

        public AssetTypeController(
            IAssetTypeCardIndexService cardIndexDataService,
            IBaseControllerDependencies dependencies,
            IUnitOfWorkService<IPeopleManagementDbScope> peopleManagementUnitOfWorkService)
            : base(cardIndexDataService, dependencies)
        {
            _peopleManagementUnitOfWorkService = peopleManagementUnitOfWorkService;

            CardIndex.Settings.Title = AssetTypeResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddAssetTypes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditAssetTypes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteAssetTypes;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = AssetTypeResources.Status,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.AssetTypeActive,
                            DisplayName = AssetTypeResources.Active,
                        },
                        new Filter
                        {
                            Code = FilterCodes.AssetTypeInactive,
                            DisplayName = AssetTypeResources.Inactive,
                        },
                    },
                },
                new FilterGroup
                {
                    DisplayName = AssetTypeResources.Category,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<AssetTypeCategory>()
                }
            };
        }

        public JsonNetResult IsCommentRequiredForIds(long[] assetTypeIds)
        {
            string[] hints = null;
            var isCommentRequired = false;

            if (assetTypeIds != null)
            {
                using (var unitOfWork = _peopleManagementUnitOfWorkService.Create())
                {
                    var records = unitOfWork.Repositories.AssetTypes
                        .Where(d => assetTypeIds.Contains(d.Id) && d.DetailsRequired == true)
                        .Select(d => new 
                        {
                            NameEnglish = d.NameEn,
                            NamePolish = d.NamePl,
                            HintEnglish = d.DetailsHintEn,
                            HintPolish = d.DetailsHintPl
                        })
                        .ToList();

                    if (records.Any())
                    {
                        isCommentRequired = true;
                        hints = records
                            .Select(d => new
                            {
                                Name = new LocalizedString() { English = d.NameEnglish, Polish = d.NamePolish },
                                Hint = new LocalizedString() { English = d.HintEnglish, Polish = d.HintPolish }
                            })
                            .Select(d => $"{d.Name}: {d.Hint}")
                            .ToArray();
                    }
                }
            }

            var result = new
            {
                IsCommentRequired = isCommentRequired,
                Hints = hints
            };

            return new JsonNetResult(result) { SerializerSettings = JsonHelper.DefaultAjaxSettings }; 
        }
    }
}