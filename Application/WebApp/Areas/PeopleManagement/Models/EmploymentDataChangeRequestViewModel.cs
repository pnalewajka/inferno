﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentDataChangeRequestViewModel
    {
        [Order(0)]
        [Required]
        [UpdatesApprovers]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.EmployeeLabel), typeof(EmploymentDataChangeRequestResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=employees-to-change'")]
        public long EmployeeId { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.EffectiveOnLabel), typeof(EmploymentDataChangeRequestResources))]
        public DateTime EffectiveOn { get; set; }

        [Order(2)]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.SalaryLabel), typeof(EmploymentDataChangeRequestResources))]
        public Optional<string> Salary { get; set; }

        [Order(3)]
        [OptionalRequired]
        [Render(Size = Size.Large)]
        [ValuePicker(Type = typeof(ContractTypePickerController), OnResolveUrlJavaScript = "url += '&should-filter-inactive=true'", DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.ContractTypeLabel), typeof(EmploymentDataChangeRequestResources))]
        public Optional<long?> ContractType { get; set; }

        [Order(4)]
        [OptionalRequired]
        [Render(Size = Size.Large)]
        [Hint("ContractDurationHint", typeof(EmploymentDataChangeRequestResources))]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<ContractDurationType>), EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit)]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.ContractDurationLabel), typeof(EmploymentDataChangeRequestResources))]
        public Optional<ContractDurationType?> ContractDuration { get; set; }

        [Order(5)]
        [OptionalRequired]
        [Render(Size = Size.Large)]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.CompanyLabel), typeof(EmploymentDataChangeRequestResources))]
        public Optional<long?> Company { get; set; }

        [Order(6)]
        [OptionalRequired]
        [Render(Size = Size.Large)]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.JobTitleLabel), typeof(EmploymentDataChangeRequestResources))]
        public Optional<long?> JobTitle { get; set; }

        [Order(7)]
        [AllowHtml]
        [HtmlSanitize]
        [Multiline(5)]
        [StringLength(2048)]
        [DataType(DataType.MultilineText)]
        [Tooltip(nameof(CommentsForPayroll))]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.CommentsLabel), typeof(EmploymentDataChangeRequestResources))]
        public string CommentsForPayroll { get; set; }

        [Order(8)]
        [Multiline(5)]
        [StringLength(2048)]
        [Tooltip(nameof(Justification))]
        [DataType(DataType.MultilineText)]
        [DisplayNameLocalized(nameof(EmploymentDataChangeRequestResources.JustificationLabel), typeof(EmploymentDataChangeRequestResources))]
        public string Justification { get; set; }
    }
}