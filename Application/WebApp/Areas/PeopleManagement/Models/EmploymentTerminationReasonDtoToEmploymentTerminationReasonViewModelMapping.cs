﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationReasonDtoToEmploymentTerminationReasonViewModelMapping : ClassMapping<EmploymentTerminationReasonDto, EmploymentTerminationReasonViewModel>
    {
        public EmploymentTerminationReasonDtoToEmploymentTerminationReasonViewModelMapping()
        {
            Mapping = d => new EmploymentTerminationReasonViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                IsVoluntarilyTurnover = d.IsVoluntarilyTurnover,
                IsOtherDepartureReasons = d.IsOtherDepartureReasons,
                Timestamp = d.Timestamp
            };
        }
    }
}