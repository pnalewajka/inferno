﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AddHomeOfficeRequestViewModelToAddHomeOfficeRequestDtoMapping :
        ClassMapping<AddHomeOfficeRequestViewModel, AddHomeOfficeRequestDto>
    {
        public AddHomeOfficeRequestViewModelToAddHomeOfficeRequestDtoMapping()
        {
            Mapping = v => new AddHomeOfficeRequestDto
            {
                From = v.From,
                To = v.To
            };
        }
    }
}