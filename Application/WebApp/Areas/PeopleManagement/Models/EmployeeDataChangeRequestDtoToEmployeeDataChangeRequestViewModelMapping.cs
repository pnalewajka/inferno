﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmployeeDataChangeRequestDtoToEmployeeDataChangeRequestViewModelMapping : ClassMapping<EmployeeDataChangeRequestDto, EmployeeDataChangeRequestViewModel>
    {
        public EmployeeDataChangeRequestDtoToEmployeeDataChangeRequestViewModelMapping()
        {
            Mapping = d => new EmployeeDataChangeRequestViewModel
            {
                EmployeeIds = d.EmployeeIds,
                EffectiveOn = d.EffectiveOn,
                JobProfile = d.JobProfile,
                OrgUnit = d.OrgUnit,
                OrgUnitNameTooltip = d.OrgUnitNameTooltip,
                Location = d.Location,
                PlaceOfWork = d.PlaceOfWork,
                LineManager = d.LineManager
            };
        }
    }
}