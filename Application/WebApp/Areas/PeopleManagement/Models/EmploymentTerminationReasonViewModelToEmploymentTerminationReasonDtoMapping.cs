﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationReasonViewModelToEmploymentTerminationReasonDtoMapping : ClassMapping<EmploymentTerminationReasonViewModel, EmploymentTerminationReasonDto>
    {
        public EmploymentTerminationReasonViewModelToEmploymentTerminationReasonDtoMapping()
        {
            Mapping = v => new EmploymentTerminationReasonDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                IsVoluntarilyTurnover = v.IsVoluntarilyTurnover,
                IsOtherDepartureReasons = v.IsOtherDepartureReasons,
                Timestamp = v.Timestamp
            };
        }
    }
}