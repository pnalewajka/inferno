﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Extensions;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetTypeDtoToAssetTypeViewModelMapping : ClassMapping<AssetTypeDto, AssetTypeViewModel>
    {
        public AssetTypeDtoToAssetTypeViewModelMapping()
        {
            Mapping = d => new AssetTypeViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Category = d.Category,
                DetailsRequired = d.DetailsRequired,
                DetailsHint = d.DetailsHint,
                IsActive = d.IsActive,
                Description = HtmlHelperExtensions.StripHtml(d.Description),
                DescriptionRichText = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}