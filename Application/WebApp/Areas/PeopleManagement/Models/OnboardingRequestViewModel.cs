﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Workflows.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingRequestViewModel
    {
        private const string OnboardingRequestControllerPath = "/Workflows/OnboardingRequest/";
        private const string LoginRegExp = @"^[a-z0-9\.\-_]*$";

        public OnboardingRequestViewModel()
        {
            WelcomeAnnounceEmailPhotos = new List<DocumentViewModel>();
            PayrollDocuments = new List<DocumentViewModel>();
            ForeignEmployeeDocuments = new List<DocumentViewModel>();
        }

        public long Id { get; set; }

        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url += '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.LineManagerLabel), typeof(OnboardingRequestResources))]
        public long? LineManagerEmployeeId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.RecruiterLabel), typeof(OnboardingRequestResources))]
        public long RecruiterEmployeeId { get; set; }

        [Required]
        [StringLength(255)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.FirstNameLabel), typeof(OnboardingRequestResources))]
        [OnValueChange(ServerHandler = OnboardingRequestControllerPath + nameof(OnboardingRequestController.UpdateSuggestedLogin))]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.LastNameLabel), typeof(OnboardingRequestResources))]
        [OnValueChange(ServerHandler = OnboardingRequestControllerPath + nameof(OnboardingRequestController.UpdateSuggestedLogin))]
        public string LastName { get; set; }

        [Required]
        [StringLength(20)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.LoginLabel), typeof(OnboardingRequestResources))]
        [RegularExpression(LoginRegExp, ErrorMessageResourceName = nameof(OnboardingRequestResources.LoginInvalidCharactersMessage), ErrorMessageResourceType = typeof(OnboardingRequestResources))]
        public string Login { get; set; }

        [StringFormat("{0:dd/MMM/yy}")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.SignedOnLabel), typeof(OnboardingRequestResources))]
        public DateTime? SignedOn { get; set; }

        [Required]
        [StringFormat("{0:dd/MMM/yy}")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.StartDateLabel), typeof(OnboardingRequestResources))]
        public DateTime StartDate { get; set; }

        [Required]
        [StringFormat("{0:dd/MMM/yy}")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.OnboardingDateLabel), typeof(OnboardingRequestResources))]
        public DateTime? OnboardingDate { get; set; }

        [Required]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.LocationLabel), typeof(OnboardingRequestResources))]
        public long LocationId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.OnboardingLocationLabel), typeof(OnboardingRequestResources))]
        public long? OnboardingLocationId { get; set; }

        [Required]
        [Render(Size = Size.Medium)]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.PlaceOfWorkLabel), typeof(OnboardingRequestResources))]
        public PlaceOfWork? PlaceOfWork { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.CompanyLabel), typeof(OnboardingRequestResources))]
        public long CompanyId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.JobTitleLabel), typeof(OnboardingRequestResources))]
        public long JobTitleId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.JobProfileLabel), typeof(OnboardingRequestResources))]
        public long JobProfileId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(ContractTypePickerController), OnResolveUrlJavaScript = "url += '&should-filter-inactive=true'", DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.ProbationPeriodContractTypeLabel), typeof(OnboardingRequestResources))]
        public long ProbationPeriodContractTypeId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(ContractTypePickerController), OnResolveUrlJavaScript = "url += '&should-filter-inactive=true'", DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.ContractTypeLabel), typeof(OnboardingRequestResources))]
        public long ContractTypeId { get; set; }

        [Required]
        [StringLength(128)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.ProbationPeriodCompensationLabel), typeof(OnboardingRequestResources))]
        public string ProbationPeriodCompensation { get; set; }

        [Required]
        [StringLength(128)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.CompensationLabel), typeof(OnboardingRequestResources))]
        public string Compensation { get; set; }

        [AllowHtml]
        [Multiline(5)]
        [StringLength(2048)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.CommentsForPayrollLabel), typeof(OnboardingRequestResources))]
        public string CommentsForPayroll { get; set; }

        [Required]
        [UpdatesApprovers]
        [ValuePicker(Type = typeof(OrgUnitPickerController), OnResolveUrlJavaScript = "url=url + '&filter=" + OrganizationFilterCodes.HideDefault + "'")]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.OrgUnitLabel), typeof(OnboardingRequestResources))]
        public long OrgUnitId { get; set; }

        [Multiline(5)]
        [StringLength(2048)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.CommentsForHiringManagerLabel), typeof(OnboardingRequestResources))]
        public string CommentsForHiringManager { get; set; }

        [DisplayNameLocalized(nameof(OnboardingRequestResources.IsForeignEmployeeLabel), typeof(OnboardingRequestResources))]
        public bool IsForeignEmployee { get; set; }

        [DisplayNameLocalized(nameof(OnboardingRequestResources.IsComingFromReferralProgramLabel), typeof(OnboardingRequestResources))]
        [OnValueChange(ServerHandler = OnboardingRequestControllerPath + nameof(OnboardingRequestController.UpdateDetailsVisibility))]
        public bool IsComingFromReferralProgram { get; set; }

        [Multiline(5)]
        [StringLength(2048)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.ReferralProgramDetailsLabel), typeof(OnboardingRequestResources))]
        public string ReferralProgramDetails { get; set; }

        [DisplayNameLocalized(nameof(OnboardingRequestResources.IsRelocationPackageNeededLabel), typeof(OnboardingRequestResources))]
        [OnValueChange(ServerHandler = OnboardingRequestControllerPath + nameof(OnboardingRequestController.UpdateDetailsVisibility))]
        public bool IsRelocationPackageNeeded { get; set; }

        [Multiline(5)]
        [StringLength(2048)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.RelocationPackageDetailsLabel), typeof(OnboardingRequestResources))]
        public string RelocationPackageDetails { get; set; }

        [Multiline(5)]
        [StringLength(2048)]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.WelcomeAnnounceEmailInformationLabel), typeof(OnboardingRequestResources))]
        public string WelcomeAnnounceEmailInformation { get; set; }

        [DocumentUpload(typeof(EmployeePhotoDocumentMapping))]
        [Hint(nameof(RequestResources.AvailableFormats), typeof(RequestResources))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.WelcomeAnnounceEmailPhotosLabel), typeof(OnboardingRequestResources))]
        public List<DocumentViewModel> WelcomeAnnounceEmailPhotos { get; set; }

        [DocumentUpload(typeof(PayrollDocumentMapping))]
        [Hint(nameof(RequestResources.AvailableFormats), typeof(RequestResources))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.PayrollDocumentsLabel), typeof(OnboardingRequestResources))]
        public List<DocumentViewModel> PayrollDocuments { get; set; }

        [DocumentUpload(typeof(ForeignEmployeeDocumentMapping))]
        [Hint(nameof(RequestResources.AvailableFormats), typeof(RequestResources))]
        [DisplayNameLocalized(nameof(OnboardingRequestResources.ForeignEmployeeDocumentsLabel), typeof(OnboardingRequestResources))]
        public List<DocumentViewModel> ForeignEmployeeDocuments { get; set; }
    }
}
