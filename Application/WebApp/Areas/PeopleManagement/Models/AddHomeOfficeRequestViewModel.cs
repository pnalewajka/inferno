﻿using System;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AddHomeOfficeRequestViewModel
    {
        [DisplayNameLocalized(nameof(AddHomeOfficeRequestResources.HomeOfficeFromLabel), typeof(AddHomeOfficeRequestResources))]
        [UpdatesApprovers]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(AddHomeOfficeRequestResources.HomeOfficeToLabel), typeof(AddHomeOfficeRequestResources))]
        [UpdatesApprovers]
        public DateTime To { get; set; }

        public override string ToString()
        {
            return string.Format(AddHomeOfficeRequestResources.HomeOfficeDescription, From, To);
        }
    }
}
