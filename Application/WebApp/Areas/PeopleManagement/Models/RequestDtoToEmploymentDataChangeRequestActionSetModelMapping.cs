﻿using System;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class RequestDtoToEmploymentDataChangeRequestActionSetModelMapping : ClassMapping<RequestDto, EmploymentDataChangeRequestActionSetModel>
    {
        public RequestDtoToEmploymentDataChangeRequestActionSetModelMapping(
            ICompanyService companyService,
            IEmployeeService employeeService,
            IJobTitleService jobTitleService,
            IContractTypeService contractTypeService)
        {
            Mapping = r => GetActionSetModel(r, companyService, employeeService, jobTitleService, contractTypeService);
        }

        private static EmploymentDataChangeRequestActionSetModel GetActionSetModel(
            RequestDto request,
            ICompanyService companyService,
            IEmployeeService employeeService,
            IJobTitleService jobTitleService,
            IContractTypeService contractTypeService)
        {
            var employmentDataChangeRequest = (EmploymentDataChangeRequestDto)request.RequestObject;
            var employee = employeeService.GetEmployeeOrDefaultById(employmentDataChangeRequest.EmployeeId);

            if (!employee.LineManagerId.HasValue)
            {
                throw new InvalidOperationException($"Given employee (ID={employee.Id}) has no line manager assigned");
            }

            var contractType = contractTypeService.GetCurrentContractTypeOrDefaultByEmployeeId(employmentDataChangeRequest.EmployeeId);

            using (CultureInfoHelper.SetCurrentUICulture(CultureInfo.InvariantCulture))
            {
                return new EmploymentDataChangeRequestActionSetModel
                {
                    EffectiveOn = employmentDataChangeRequest.EffectiveOn,
                    Employee = employee,
                    LineManager = employeeService.GetEmployeeOrDefaultById(employee.LineManagerId.Value),
                    Salary = employmentDataChangeRequest.Salary,
                    OldContractType = contractType.Name,
                    NewContractType = employmentDataChangeRequest.ContractType.IsSet
                        ? contractTypeService.GetContractTypeOrDefaultById(employmentDataChangeRequest.ContractType.Value.Value).Name
                        : new Optional<LocalizedString>(),
                    ContractDuration = employmentDataChangeRequest.ContractDuration.IsSet
                        ? employmentDataChangeRequest.ContractDuration.Value.GetDescription()
                        : new Optional<string>(),
                    Company = employmentDataChangeRequest.Company.IsSet
                        ? companyService.GetCompanyOrDefaultById(employmentDataChangeRequest.Company.Value.Value).Name
                        : new Optional<string>(),
                    JobTitle = employmentDataChangeRequest.JobTitle.IsSet
                        ? jobTitleService.GetJobTitleOrDefaultById(employmentDataChangeRequest.JobTitle.Value.Value).Name.English
                        : new Optional<string>(),
                    CommentsForPayroll = employmentDataChangeRequest.CommentsForPayroll,
                    Approvers = request.ApprovalGroups
                        .SelectMany(g => g.Approvals)
                        .Where(a => a.Status == ApprovalStatus.Approved)
                        .Select(a => a.ForcedByUserId.HasValue
                            ? $"{a.ForcedByUserFullName} (on behalf of {a.ApproverFullName})"
                            : a.ApproverFullName)
                        .ToList()
                };
            }
        }
    }
}