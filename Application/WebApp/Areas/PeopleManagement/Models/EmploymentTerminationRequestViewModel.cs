﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationRequestViewModel : IValidatableObject
    {
        [Order(0)]
        [Required]
        [UpdatesApprovers]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.EmployeeLabel), typeof(EmploymentTerminationRequestResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=employees-to-terminate'")]
        public long EmployeeId { get; set; }

        [Order(1)]
        [Required]
        [StringFormat("{0:dd/MMM/yy}")]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.TerminationDateLabel), typeof(EmploymentTerminationRequestResources))]
        public DateTime TerminationDate { get; set; }

        [Order(2)]
        [Required]
        [StringFormat("{0:dd/MMM/yy}")]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.EndDateLabel), typeof(EmploymentTerminationRequestResources))]
        public DateTime EndDate { get; set; }

        [Order(3)]
        [Required]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.ReasonLabel), typeof(EmploymentTerminationRequestResources))]
        [ValuePicker(Type = typeof(EmploymentTerminationReasonPickerController))]
        public long ReasonId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (TerminationDate > EndDate)
            {
                yield return new ValidationResult(EmploymentTerminationRequestResources.TerminationGreaterThanEndError, new[] { nameof(TerminationDate) });
            }
        }
    }
}