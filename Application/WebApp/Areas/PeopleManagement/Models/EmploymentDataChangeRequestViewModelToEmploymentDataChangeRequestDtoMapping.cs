﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentDataChangeRequestViewModelToEmploymentDataChangeRequestDtoMapping : ClassMapping<EmploymentDataChangeRequestViewModel, EmploymentDataChangeRequestDto>
    {
        public EmploymentDataChangeRequestViewModelToEmploymentDataChangeRequestDtoMapping()
        {
            Mapping = v => new EmploymentDataChangeRequestDto
            {
                EmployeeId = v.EmployeeId,
                EffectiveOn = v.EffectiveOn,
                Salary = v.Salary,
                ContractType = v.ContractType,
                ContractDuration = v.ContractDuration,
                Company = v.Company,
                JobTitle = v.JobTitle,
                CommentsForPayroll = v.CommentsForPayroll,
                Justification = v.Justification
            };
        }
    }
}