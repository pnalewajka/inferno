﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class RequestDtoToEmployeeDataChangeRequestActionSetModelMapping : ClassMapping<RequestDto, EmployeeDataChangeRequestActionSetModel>
    {
        public RequestDtoToEmployeeDataChangeRequestActionSetModelMapping(
            ITimeService timeService,
            IEmployeeService employeeService,
            ILocationService locationService,
            IContractTypeService contractTypeService)
        {
            Mapping = r => GetActionSetModel(r, timeService, employeeService, locationService, contractTypeService);
        }

        private static EmployeeDataChangeRequestActionSetModel GetActionSetModel(
            RequestDto request,
            ITimeService timeService,
            IEmployeeService employeeService,
            ILocationService locationService,
            IContractTypeService contractTypeService)
        {
            var employeeDataChangeRequest = (EmployeeDataChangeRequestDto)request.RequestObject;

            return new EmployeeDataChangeRequestActionSetModel
            {
                EffectiveOn = employeeDataChangeRequest.EffectiveOn.HasValue
                    ? employeeDataChangeRequest.EffectiveOn.Value
                    : timeService.GetCurrentDate(),
                Location = employeeDataChangeRequest.Location.IsSet
                    ? locationService.GetLocationOrDefaultById(employeeDataChangeRequest.Location.Value.Value).Name
                    : new Optional<string>(),
                Entries = employeeDataChangeRequest.EmployeeIds.Select(id =>
                {
                    var employee = employeeService.GetEmployeeOrDefaultById(id);

                    if (!employee.LineManagerId.HasValue)
                    {
                        throw new InvalidOperationException($"Given employee (ID={employee.Id}) has no line manager assigned");
                    }

                    return new EmployeeDataChangeRequestActionSetModel.EmployeeDescriptor
                    {
                        Employee = employee,
                        LineManager = employeeService.GetEmployeeOrDefaultById(employee.LineManagerId.Value),
                        ContractTypeName = contractTypeService.GetCurrentContractTypeOrDefaultByEmployeeId(employee.Id).Name,
                    };
                }).ToList(),
                Approvers = request.ApprovalGroups
                    .SelectMany(g => g.Approvals)
                    .Where(a => a.Status == ApprovalStatus.Approved)
                    .Select(a => a.ForcedByUserId.HasValue 
                        ? $"{a.ForcedByUserFullName} (on behalf of {a.ApproverFullName})"
                        : a.ApproverFullName)
                    .ToList()
            };
        }
    }
}