﻿using Smt.Atomic.Business.PeopleManagement.ReportParameters;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingOverviewReportParametersDtoToOnboardingOverviewReportParametersViewModelMapping
        : ClassMapping<OnboardingOverviewReportParametersDto, OnboardingOverviewReportParametersViewModel>
    {
        public OnboardingOverviewReportParametersDtoToOnboardingOverviewReportParametersViewModelMapping()
        {
            Mapping = d => new OnboardingOverviewReportParametersViewModel
            {
                From = d.From,
                To = d.To,
                Statuses = d.Statuses,
                Scope = d.Scope,
            };
        }
    }
}