﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetTypeViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [LocalizedStringLength(255)]
        [LocalizedStringRequired]
        [Order(1)]
        [DisplayNameLocalized(nameof(AssetTypeResources.Name), typeof(AssetTypeResources))]
        public LocalizedString Name { get; set; }

        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(AssetTypeResources.Category), typeof(AssetTypeResources))]
        [Render(GridCssClass = "column-width-small")]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<AssetTypeCategory>))]
        public AssetTypeCategory? Category { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(AssetTypeResources.DetailsRequired), typeof(AssetTypeResources))]
        [Render(GridCssClass = "column-width-normal")]
        public bool DetailsRequired { get; set; }

        [LocalizedStringLength(255)]
        [Order(4)]
        [DisplayNameLocalized(nameof(AssetTypeResources.DetailsHint), typeof(AssetTypeResources))]
        public LocalizedString DetailsHint { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(AssetTypeResources.IsActive), typeof(AssetTypeResources))]
        [Render(GridCssClass = "column-width-normal")]
        public bool IsActive { get; set; }

        [AllowHtml]
        [RichText]
        [Visibility(VisibilityScope.Form)]
        [Order(5)]
        [DisplayNameLocalized(nameof(AssetTypeResources.Description), typeof(AssetTypeResources))]
        public string DescriptionRichText { get; set; }

        [Multiline(6)]
        [Visibility(VisibilityScope.Grid)]
        [Order(5)]
        [DisplayNameLocalized(nameof(AssetTypeResources.Description), typeof(AssetTypeResources))]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (DetailsRequired && (string.IsNullOrEmpty(DetailsHint.English) || string.IsNullOrEmpty(DetailsHint.Polish)))
            {
                issues.Add(new ValidationResult(AssetTypeResources.DetailsHintRequired,
                    new List<string> { nameof(DetailsHint) }));
            }

            return issues;
        }
    }
}