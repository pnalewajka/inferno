﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationRequestViewModelToEmploymentTerminationRequestDtoMapping : ClassMapping<EmploymentTerminationRequestViewModel, EmploymentTerminationRequestDto>
    {
        public EmploymentTerminationRequestViewModelToEmploymentTerminationRequestDtoMapping()
        {
            Mapping = v => new EmploymentTerminationRequestDto
            {
                EmployeeId = v.EmployeeId,
                TerminationDate = v.TerminationDate,
                EndDate = v.EndDate,
                ReasonId = v.ReasonId
            };
        }
    }
}