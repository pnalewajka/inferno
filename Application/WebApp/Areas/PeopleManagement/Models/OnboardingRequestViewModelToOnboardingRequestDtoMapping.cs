﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingRequestViewModelToOnboardingRequestDtoMapping : ClassMapping<OnboardingRequestViewModel, OnboardingRequestDto>
    {
        public OnboardingRequestViewModelToOnboardingRequestDtoMapping()
        {
            Mapping = v => new OnboardingRequestDto
            {
                Id = v.Id,
                LineManagerEmployeeId = v.LineManagerEmployeeId,
                RecruiterEmployeeId = v.RecruiterEmployeeId,
                FirstName = HumanNameHelper.SanitizeName(v.FirstName),
                LastName = HumanNameHelper.SanitizeName(v.LastName),
                Login = v.Login,
                SignedOn = v.SignedOn,
                StartDate = v.StartDate,
                LocationId = v.LocationId,
                PlaceOfWork = v.PlaceOfWork,
                CompanyId = v.CompanyId,
                JobTitleId = v.JobTitleId,
                JobProfileId = v.JobProfileId,
                ContractTypeId = v.ContractTypeId,
                ProbationPeriodContractTypeId = v.ProbationPeriodContractTypeId,
                Compensation = v.Compensation,
                ProbationPeriodCompensation = v.ProbationPeriodCompensation,
                CommentsForPayroll = v.CommentsForPayroll,
                OrgUnitId = v.OrgUnitId,
                CommentsForHiringManager = v.CommentsForHiringManager,
                IsForeignEmployee = v.IsForeignEmployee,
                IsComingFromReferralProgram = v.IsComingFromReferralProgram,
                ReferralProgramDetails = v.ReferralProgramDetails,
                IsRelocationPackageNeeded = v.IsRelocationPackageNeeded,
                RelocationPackageDetails = v.RelocationPackageDetails,
                OnboardingDate = v.OnboardingDate,
                OnboardingLocationId = v.OnboardingLocationId,
                WelcomeAnnounceEmailInformation = v.WelcomeAnnounceEmailInformation,
                WelcomeAnnounceEmailPhotos = v.WelcomeAnnounceEmailPhotos.Select(d => new DocumentDto
                {
                    ContentType = d.ContentType,
                    DocumentName = d.DocumentName,
                    DocumentId = d.DocumentId,
                    TemporaryDocumentId = d.TemporaryDocumentId,
                }).ToArray(),
                PayrollDocuments = v.PayrollDocuments.Select(d => new DocumentDto
                {
                    ContentType = d.ContentType,
                    DocumentName = d.DocumentName,
                    DocumentId = d.DocumentId,
                    TemporaryDocumentId = d.TemporaryDocumentId,
                }).ToArray(),
                ForeignEmployeeDocuments = v.ForeignEmployeeDocuments.Select(d => new DocumentDto
                {
                    ContentType = d.ContentType,
                    DocumentName = d.DocumentName,
                    DocumentId = d.DocumentId,
                    TemporaryDocumentId = d.TemporaryDocumentId,
                }).ToArray()
            };
        }
    }
}
