﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AddEmployeeRequestDtoToAddEmployeeRequestViewModelMapping : ClassMapping<AddEmployeeRequestDto, AddEmployeeRequestViewModel>
    {
        public AddEmployeeRequestDtoToAddEmployeeRequestViewModelMapping()
        {
            Mapping = d => new AddEmployeeRequestViewModel
            {
                FirstName = d.FirstName,
                LastName = d.LastName,
                Email = d.Email,
                Login = d.Login,
                CrmId = d.CrmId,
                JobTitleId = d.JobTitleId,
                JobProfileId = d.JobProfileId,
                OrgUnitId = d.OrgUnitId,
                LocationId = d.LocationId,
                PlaceOfWork = d.PlaceOfWork,
                CompanyId = d.CompanyId,
                LineManagerId = d.LineManagerId,
                ContractFrom = d.ContractFrom,
                ContractTo = d.ContractTo,
                ContractSignDate = d.ContractSignDate,
                MondayHours = d.MondayHours,
                TuesdayHours = d.TuesdayHours,
                WednesdayHours = d.WednesdayHours,
                ThursdayHours = d.ThursdayHours,
                FridayHours = d.FridayHours,
                SaturdayHours = d.SaturdayHours,
                SundayHours = d.SundayHours,
                ContractTypeId = d.ContractTypeId,
                ContractDuration = d.ContractDuration,
                VacationHourToDayRatio = d.VacationHourToDayRatio,
                TimeReporting = d.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = d.AbsenceOnlyDefaultProjectId,
                CalendarId = d.CalendarId
            };
        }
    }
}