﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Configuration.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    [TabDescription(0, "basic", "UserAccountTabName", typeof(AddEmployeeRequestResources))]
    [TabDescription(1, "managementdata", "OrganizationTabName", typeof(AddEmployeeRequestResources))]
    [TabDescription(2, "employment", "EmploymentTabName", typeof(AddEmployeeRequestResources))]
    [TabDescription(3, "worktime", "WorkingHoursTabName", typeof(AddEmployeeRequestResources))]
    [TabDescription(4, "timetracking", "TimeTrackingTabName", typeof(AddEmployeeRequestResources))]
    public class AddEmployeeRequestViewModel : IValidatableObject
    {
        private const string LoginValidationRegExp = @"^[a-z0-9\.\-_]*$";

        [Tab("basic")]
        [Order(0)]
        [Required]
        [StringLength(255)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.FirstNameLabel), typeof(AddEmployeeRequestResources))]
        public string FirstName { get; set; }

        [Tab("basic")]
        [Order(1)]
        [Required]
        [StringLength(255)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.LastNameLabel), typeof(AddEmployeeRequestResources))]
        public string LastName { get; set; }

        [Tab("basic")]
        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.CompanyLabel), typeof(AddEmployeeRequestResources))]
        [ValuePicker(Type = typeof(CompanyPickerController), OnSelectionChangedJavaScript = "AddEmployeeRequest.onCompanyPickerSelectionChanged")]
        public long CompanyId { get; set; }

        [Tab("basic")]
        [Order(3)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.JobTitleLabel), typeof(AddEmployeeRequestResources))]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        public long JobTitleId { get; set; }

        [Tab("basic")]
        [Order(4)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.JobProfileLabel), typeof(AddEmployeeRequestResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long JobProfileId { get; set; }

        [Tab("basic")]
        [Order(5)]
        [Required]
        [StringLength(20)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.LoginLabel), typeof(AddEmployeeRequestResources))]
        [RegularExpression(LoginValidationRegExp, ErrorMessageResourceName = nameof(AddEmployeeRequestResources.LoginInvalidCharactersMessage), ErrorMessageResourceType = typeof(AddEmployeeRequestResources))]
        public string Login { get; set; }

        [Tab("basic")]
        [Order(6)]
        [Required]
        [StringLength(255)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.EmailLabel), typeof(AddEmployeeRequestResources))]
        [EmailAddress]
        public string Email { get; set; }

        [Tab("basic")]
        [Order(7)]
        [StringLength(64)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.CrmIdLabel), typeof(AddEmployeeRequestResources))]
        public string CrmId { get; set; }

        [Tab("managementdata")]
        [Order(8)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.OrgUnitLabel), typeof(AddEmployeeRequestResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController), OnSelectionChangedJavaScript = "AddEmployeeRequest.onOrgUnitPickerSelectionChanged")]
        public long OrgUnitId { get; set; }

        [Tab("managementdata")]
        [Order(9)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.LocationLabel), typeof(AddEmployeeRequestResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long LocationId { get; set; }

        [Tab("managementdata")]
        [Order(10)]
        [Required]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.PlaceOfWorkLabel), typeof(AddEmployeeRequestResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        public PlaceOfWork PlaceOfWork { get; set; }

        [Tab("managementdata")]
        [Order(11)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.LineManagerLabel), typeof(AddEmployeeRequestResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url += '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        public long LineManagerId { get; set; }

        [Tab("employment")]
        [Order(13)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.ContractFromLabel), typeof(AddEmployeeRequestResources))]
        public DateTime ContractFrom { get; set; }

        [Tab("employment")]
        [Order(14)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.ContractToLabel), typeof(AddEmployeeRequestResources))]
        public DateTime? ContractTo { get; set; }

        [Tab("employment")]
        [Order(15)]
        [Required]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.ContractSignDateLabel), typeof(AddEmployeeRequestResources))]
        public DateTime ContractSignDate { get; set; }

        [Tab("employment")]
        [Order(16)]
        [Required]
        [ValuePicker(Type = typeof(ContractTypePickerController), OnResolveUrlJavaScript = "url += '&should-filter-inactive=true'", DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.ContractTypeLabel), typeof(AddEmployeeRequestResources))]
        public long ContractTypeId { get; set; }

        [Tab("employment")]
        [Order(16)]
        [Required]
        [Render(Size = Size.Medium)]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<ContractDurationType>), EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractDuration), typeof(EmployeeResources))]
        public ContractDurationType? ContractDuration { get; set; }

        [Tab("employment")]
        [Order(17)]
        [ValuePicker(Type = typeof(CalendarPickerController), DialogWidth = "900px")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.EmploymentPeriodCalendar), typeof(AddEmployeeRequestResources))]
        public long? CalendarId { get; set; }

        [Tab("worktime")]
        [Order(17)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.MondayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal MondayHours { get; set; }

        [Tab("worktime")]
        [Order(18)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.TuesdayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal TuesdayHours { get; set; }

        [Tab("worktime")]
        [Order(19)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.WednesdayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal WednesdayHours { get; set; }

        [Tab("worktime")]
        [Order(20)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.ThursdayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal ThursdayHours { get; set; }

        [Tab("worktime")]
        [Order(21)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.FridayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal FridayHours { get; set; }

        [Tab("worktime")]
        [Order(22)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.SaturdayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal SaturdayHours { get; set; }

        [Tab("worktime")]
        [Order(23)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.SundayHoursLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal SundayHours { get; set; }

        [Tab("worktime")]
        [Order(24)]
        [Required]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.VacationHourToDayRatioLabel), typeof(AddEmployeeRequestResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal VacationHourToDayRatio { get; set; }

        [Tab("timetracking")]
        [Order(25)]
        [Required]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.TimeReportingLabel), typeof(AddEmployeeRequestResources))]
        public TimeReportingMode TimeReporting { get; set; }

        [Tab("timetracking")]
        [Order(26)]
        [ValuePicker(Type = typeof(SimpleProjectPickerController))]
        [DisplayNameLocalized(nameof(AddEmployeeRequestResources.AbsenceOnlyDefaultProjectLabel), typeof(AddEmployeeRequestResources))]
        public long? AbsenceOnlyDefaultProjectId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (ContractSignDate > ContractFrom)
            {
                issues.Add(new ValidationResult(EmployeeResources.SignedOnDateGreaterThanStartDate,
                    new List<string> { nameof(ContractSignDate) }));
            }

            if (ContractTo.HasValue && ContractFrom > ContractTo.Value)
            {
                issues.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate,
                    new List<string> { nameof(ContractFrom) }));
            }

            return issues;
        }
    }
}