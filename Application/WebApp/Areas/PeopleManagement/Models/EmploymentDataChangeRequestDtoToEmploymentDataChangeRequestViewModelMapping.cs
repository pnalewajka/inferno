﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentDataChangeRequestDtoToEmploymentDataChangeRequestViewModelMapping : ClassMapping<EmploymentDataChangeRequestDto, EmploymentDataChangeRequestViewModel>
    {
        public EmploymentDataChangeRequestDtoToEmploymentDataChangeRequestViewModelMapping()
        {
            Mapping = d => new EmploymentDataChangeRequestViewModel
            {
                EmployeeId = d.EmployeeId,
                EffectiveOn = d.EffectiveOn,
                Salary = d.Salary,
                ContractType = d.ContractType,
                ContractDuration = d.ContractDuration,
                Company = d.Company,
                JobTitle = d.JobTitle,
                CommentsForPayroll = d.CommentsForPayroll,
                Justification = d.Justification
            };
        }
    }
}