﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationRequestActionSetModel
    { 
        public EmployeeDto Employee { get; set; }

        public EmployeeDto LineManager { get; set; }

        public DateTime EndDate { get; set; }

        public LocalizedString ContractTypeName { get; set; }

        public LocalizedString TerminationReasonText { get; set; }
    }
}