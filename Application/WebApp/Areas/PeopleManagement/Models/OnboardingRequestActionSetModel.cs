﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingRequestActionSetModel
    {
        public Uri RequestViewLink { get; set; }

        public Uri RequestEditLink { get; set; }

        public Uri AssetsRequestEditLink { get; set; }

        public Uri WelcomeAnnounceEmailPhotosLink { get; set; }

        public Uri PayrollDocumentsLink { get; set; }

        public Uri ForeignEmployeeDocumentsLink { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public string EmployeeFullName { get; set; }

        public string EmployeeLogin { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime OnboardingDate { get; set; }

        public string Location { get; set; }

        public string OnboardingLocation { get; set; }

        public string PlaceOfWork { get; set; }

        public string Company { get; set; }

        public LocalizedString JobTitle { get; set; }

        public LocalizedString ProbationPeriodContractType { get; set; }

        public LocalizedString ContractType { get; set; }

        public string ProbationPeriodCompensation { get; set; }

        public string Compensation { get; set; }

        public string CommentsForPayroll { get; set; }

        public bool IsForeignEmployee { get; set; }

        public bool IsComingFromReferralProgram { get; set; }

        public string ReferralProgramDetails { get; set; }

        public bool IsRelocationPackageNeeded { get; set; }

        public string RelocationPackageDetails { get; set; }

        public string WelcomeAnnounceEmailInformation { get; set; }

        public EmployeeDto Requester { get; set; }

        public EmployeeDto Recruiter { get; set; }

        public EmployeeDto TeamLeaderOfRecruiter { get; set; }

        public EmployeeDto HeadOfRecruitment { get; set; }

        public EmployeeDto LineManager { get; set; }

        public EmployeeDto HiringManager { get; set; }
    }
}
