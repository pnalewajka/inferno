﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.PeopleManagement.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    [Identifier("ViewModels.OnboardingOverviewReportParameters")]
    public class OnboardingOverviewReportParametersViewModel
        : IValidatableObject
    {
        [DisplayNameLocalized(nameof(GeneralResources.From), typeof(GeneralResources))]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(GeneralResources.To), typeof(GeneralResources))]
        public DateTime? To { get; set; }

        [DisplayNameLocalized(nameof(OnboardingRequestResources.RequestStatus), typeof(OnboardingRequestResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<RequestStatus>))]
        public RequestStatus[] Statuses { get; set; }

        [DisplayNameLocalized(nameof(OnboardingRequestResources.Scope), typeof(OnboardingRequestResources))]
        public OnboardingOverviewReportScope Scope { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (From > To)
            {
                yield return new ValidationResult(GeneralResources.ToDateShouldBeGreaterThanFromDate);
            }
        }
    }
}