﻿using System;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingDocumentsViewModel
    {
        public string ActionName { get; set; }

        public DocumentDto[] Documents { get; set; }

        public Guid Secret { get; set; }
    }
}
