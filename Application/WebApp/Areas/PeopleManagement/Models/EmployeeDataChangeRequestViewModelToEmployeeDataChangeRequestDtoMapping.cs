﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmployeeDataChangeRequestViewModelToEmployeeDataChangeRequestDtoMapping : ClassMapping<EmployeeDataChangeRequestViewModel, EmployeeDataChangeRequestDto>
    {
        public EmployeeDataChangeRequestViewModelToEmployeeDataChangeRequestDtoMapping()
        {
            Mapping = v => new EmployeeDataChangeRequestDto
            {
                EmployeeIds = v.EmployeeIds,
                EffectiveOn = v.EffectiveOn,
                JobProfile = v.JobProfile,
                OrgUnit = v.OrgUnit,
                Location = v.Location,
                PlaceOfWork = v.PlaceOfWork,
                LineManager = v.LineManager
            };
        }
    }
}