﻿using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationReasonViewModel
    {
        public long Id { get; set; }

        [Order(1)]
        [LocalizedStringLength(255)]
        [LocalizedStringRequired]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.NameLabel), typeof(EmploymentTerminationRequestResources))]
        public LocalizedString Name { get; set; }

        [Order(2)]
        [LocalizedStringLength(512)]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.DescriptionLabel), typeof(EmploymentTerminationRequestResources))]
        public LocalizedString Description { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.IsAttributeLabel), typeof(EmploymentTerminationRequestResources))]
        public bool IsVoluntarilyTurnover { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(EmploymentTerminationRequestResources.IsTurnoverLabel), typeof(EmploymentTerminationRequestResources))]
        public bool IsOtherDepartureReasons { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}