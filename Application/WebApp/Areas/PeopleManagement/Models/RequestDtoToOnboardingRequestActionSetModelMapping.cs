﻿using System;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class RequestDtoToOnboardingRequestActionSetModelMapping : ClassMapping<RequestDto, OnboardingRequestActionSetModel>
    {
        public RequestDtoToOnboardingRequestActionSetModelMapping(
            ICompanyService companyService,
            IEmployeeService employeeService,
            IJobTitleService jobTitleService,
            ILocationService locationService,
            IContractTypeService contractTypeService,
            ISystemParameterService systemParameterService,
            IOnboardingRequestService onboardingRequestService,
            IUnitOfWorkService<IPeopleManagementDbScope> unitOfWorkService)

        {
            Mapping = r => GetActionSetModel(r, companyService, employeeService, jobTitleService, locationService, contractTypeService,
                systemParameterService, onboardingRequestService, unitOfWorkService);
        }

        private static OnboardingRequestActionSetModel GetActionSetModel(
            RequestDto request,
            ICompanyService companyService,
            IEmployeeService employeeService,
            IJobTitleService jobTitleService,
            ILocationService locationService,
            IContractTypeService contractTypeService,
            ISystemParameterService systemParameterService,
            IOnboardingRequestService onboardingRequestService,
            IUnitOfWorkService<IPeopleManagementDbScope> unitOfWorkService)
        {
            var onboardingRequest = (OnboardingRequestDto)request.RequestObject;
            var employeesWithAccess = onboardingRequestService.GetAccessDescription(request);

            var recruiterEmployeeId = employeesWithAccess.GetEmployeeIdByUserType(OnboardingRequestUserType.Recruiter);
            var recruiter = employeeService.GetEmployeeOrDefaultById(recruiterEmployeeId ?? default(long));
            var teamLeaderOfRecruiterEmployeeId = employeesWithAccess.GetEmployeeIdByUserType(OnboardingRequestUserType.TeamLeaderOfRecruiter);

            if (!teamLeaderOfRecruiterEmployeeId.HasValue)
            {
                throw new InvalidOperationException($"Recruiter's org unit (ID={recruiter.OrgUnitId}) has no manager assigned");
            }

            var headOfRecruitmentEmployeeId = employeesWithAccess.GetEmployeeIdByUserType(OnboardingRequestUserType.HeadOfRecruitment);

            if (!headOfRecruitmentEmployeeId.HasValue)
            {
                var recruitmentOrgUnitCode = systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentOrgUnitCode);

                throw new InvalidOperationException($"Org unit Recruitment (Code={recruitmentOrgUnitCode}) has no manager assigned");
            }

            var baseUri = new Uri(systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress));

            var location = locationService.GetLocationOrDefaultById(onboardingRequest.LocationId);
            var onboardingLocation = locationService.GetLocationOrDefaultById(onboardingRequest.OnboardingLocationId.Value);

            var requesterEmployeeId = employeesWithAccess.GetEmployeeIdByUserType(OnboardingRequestUserType.Requester);
            var lineManagerEmployeeId = employeesWithAccess.GetEmployeeIdByUserType(OnboardingRequestUserType.LineManager);
            var hiringManagerEmployeeId = employeesWithAccess.GetEmployeeIdByUserType(OnboardingRequestUserType.HiringManager);

            using (var unitOfWork = unitOfWorkService.Create())
            {
                var assetsRequestId = unitOfWork.Repositories.AssetRequests
                    .Where(r => r.OnboardingRequestId == request.Id)
                    .Select(r => (long?)r.Id)
                    .SingleOrDefault();

                return new OnboardingRequestActionSetModel
                {
                    RequestViewLink = new Uri(baseUri, $"Workflows/Request/View/{request.Id}"),
                    RequestEditLink = new Uri(baseUri, $"Workflows/Request/Edit/{request.Id}"),
                    AssetsRequestEditLink = assetsRequestId.HasValue
                        ? new Uri(baseUri, $"Workflows/Request/Edit/{assetsRequestId.Value}")
                        : null,
                    WelcomeAnnounceEmailPhotosLink = new Uri(baseUri, $"Workflows/OnboardingRequest/WelcomeAnnounceEmailPhotos/{request.Id}?secret={onboardingRequest.WelcomeAnnounceEmailPhotosSecret}"),
                    PayrollDocumentsLink = new Uri(baseUri, $"Workflows/OnboardingRequest/PayrollDocuments/{request.Id}?secret={onboardingRequest.PayrollDocumentsSecret}"),
                    ForeignEmployeeDocumentsLink = new Uri(baseUri, $"Workflows/OnboardingRequest/ForeignEmployeeDocuments/{request.Id}?secret={onboardingRequest.ForeignEmployeeDocumentsSecret}"),
                    RequestStatus = request.Status,
                    EmployeeFullName = $"{onboardingRequest.FirstName} {onboardingRequest.LastName}",
                    EmployeeLogin = onboardingRequest.Login,
                    StartDate = onboardingRequest.StartDate,
                    OnboardingDate = onboardingRequest.OnboardingDate.Value,
                    Location = location.Name,
                    OnboardingLocation = onboardingLocation.Name,
                    PlaceOfWork = onboardingRequest.PlaceOfWork.GetDescription(CultureInfo.InvariantCulture),
                    Company = companyService.GetCompanyOrDefaultById(onboardingRequest.CompanyId)?.Name,
                    JobTitle = jobTitleService.GetJobTitleOrDefaultById(onboardingRequest.JobTitleId)?.Name,
                    ProbationPeriodContractType = contractTypeService.GetContractTypeOrDefaultById(onboardingRequest.ProbationPeriodContractTypeId)?.Name,
                    ContractType = contractTypeService.GetContractTypeOrDefaultById(onboardingRequest.ContractTypeId)?.Name,
                    ProbationPeriodCompensation = onboardingRequest.ProbationPeriodCompensation,
                    Compensation = onboardingRequest.Compensation,
                    CommentsForPayroll = onboardingRequest.CommentsForPayroll,
                    IsForeignEmployee = onboardingRequest.IsForeignEmployee,
                    IsComingFromReferralProgram = onboardingRequest.IsComingFromReferralProgram,
                    ReferralProgramDetails = onboardingRequest.ReferralProgramDetails,
                    IsRelocationPackageNeeded = onboardingRequest.IsRelocationPackageNeeded,
                    RelocationPackageDetails = onboardingRequest.RelocationPackageDetails,
                    WelcomeAnnounceEmailInformation = onboardingRequest.WelcomeAnnounceEmailInformation,
                    Requester = employeeService.GetEmployeeOrDefaultById(requesterEmployeeId ?? default(long)),
                    Recruiter = recruiter,
                    TeamLeaderOfRecruiter = employeeService.GetEmployeeOrDefaultById(teamLeaderOfRecruiterEmployeeId ?? default(long)),
                    HeadOfRecruitment = employeeService.GetEmployeeOrDefaultById(headOfRecruitmentEmployeeId ?? default(long)),
                    LineManager = employeeService.GetEmployeeOrDefaultById(lineManagerEmployeeId ?? default(long)),
                    HiringManager = employeeService.GetEmployeeOrDefaultById(hiringManagerEmployeeId ?? default(long))
                };
            }
        }
    }
}
