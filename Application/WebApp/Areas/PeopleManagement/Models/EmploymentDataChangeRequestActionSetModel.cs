﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentDataChangeRequestActionSetModel
    { 
        public DateTime EffectiveOn { get; set; }

        public EmployeeDto Employee { get; set; }

        public EmployeeDto LineManager { get; set; }

        public Optional<string> Salary { get; set; }

        public LocalizedString OldContractType { get; set; }

        public Optional<LocalizedString> NewContractType { get; set; }

        public Optional<string> ContractDuration { get; set; }

        public Optional<string> Company { get; set; }

        public Optional<string> JobTitle { get; set; }

        public string CommentsForPayroll { get; set; }

        public IEnumerable<string> Approvers { get; set; }
    }
}
