﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class RequestDtoToAssetsRequestActionSetModelMapping : ClassMapping<RequestDto, AssetsRequestActionSetModel>
    {
        public RequestDtoToAssetsRequestActionSetModelMapping(
            IEmployeeService employeeService,
            IUnitOfWorkService<IPeopleManagementDbScope> peopleManagementUnitOfWorkService)
        {
            Mapping = r => GetActionModel(r, employeeService, peopleManagementUnitOfWorkService);
        }

        private static AssetsRequestActionSetModel GetActionModel(
            RequestDto request,
            IEmployeeService employeeService,
            IUnitOfWorkService<IPeopleManagementDbScope> peopleManagementUnitOfWorkService)
        {
            var assetsRequest = (AssetsRequestDto)request.RequestObject;
            var employee = employeeService.GetEmployeeOrDefaultById(assetsRequest.EmployeeId);

            if (!employee.LineManagerId.HasValue)
            {
                throw new InvalidOperationException($"Given employee (ID={employee.Id}) has no line manager assigned");
            }

            if (!assetsRequest.OnboardingRequestId.HasValue)
            {
                throw new InvalidOperationException($"Given assets request (ID={assetsRequest.Id}) has no OnboardingRequest assigned");
            }

            using (var unityOfWork = peopleManagementUnitOfWorkService.Create())
            {
                var onboardingRequest = unityOfWork.Repositories.OnboardingRequests
                    .Where(o => o.Id == assetsRequest.OnboardingRequestId)
                    .Select(o => new { o.RecruiterEmployeeId, o.OnboardingDate, o.StartDate, o.Login })
                    .Single();

                return new AssetsRequestActionSetModel()
                {
                    Employee = employee,
                    LineManager = employeeService.GetEmployeeOrDefaultById(employee.LineManagerId.Value),
                    OnboardingDate = onboardingRequest.OnboardingDate.Value,
                    StartDate = onboardingRequest.StartDate,
                    Recruiter = employeeService.GetEmployeeOrDefaultById(onboardingRequest.RecruiterEmployeeId),
                    Login = onboardingRequest.Login,
                    HardwareSet = GetAggregatedAssetTypeNames(assetsRequest, peopleManagementUnitOfWorkService, AssetTypeCategory.HardwareSet),
                    HardwareSetComment = assetsRequest.AssetTypeHardwareSetComment,
                    Headset = GetAggregatedAssetTypeNames(assetsRequest, peopleManagementUnitOfWorkService, AssetTypeCategory.Headset),
                    HeadsetComment = assetsRequest.AssetTypeHeadsetComment,
                    Backpack = GetAggregatedAssetTypeNames(assetsRequest, peopleManagementUnitOfWorkService, AssetTypeCategory.Backpack),
                    BackpackComment = assetsRequest.AssetTypeBackpackComment,
                    OptionalHardware = GetAggregatedAssetTypeNames(assetsRequest, peopleManagementUnitOfWorkService, AssetTypeCategory.OptionalHardware),
                    OptionalHardwareComment = assetsRequest.AssetTypeOptionalHardwareComment,
                    Software = GetAggregatedAssetTypeNames(assetsRequest, peopleManagementUnitOfWorkService, AssetTypeCategory.Software),
                    SoftwareComment = assetsRequest.AssetTypeSoftwareComment,
                    Other = GetAggregatedAssetTypeNames(assetsRequest, peopleManagementUnitOfWorkService, AssetTypeCategory.Other),
                    OtherComment = assetsRequest.AssetTypeOtherComment,
                    CommentForIT = assetsRequest.CommentForIT
                };
            }
        }

        private static string GetAggregatedAssetTypeNames(
            AssetsRequestDto assetsRequest,
            IUnitOfWorkService<IPeopleManagementDbScope> peopleManagementUnitOfWorkServicet,
            AssetTypeCategory assetTypeCategory)
        {
            var ids = GetAssetTypeIds(assetsRequest, assetTypeCategory);

            if (ids != null && ids.Any())
            {
                using (var unityOfWork = peopleManagementUnitOfWorkServicet.Create())
                {
                    var assetNames = unityOfWork.Repositories.AssetTypes
                        .GetByIds(ids)
                        .Select(a => a.NameEn)
                        .ToList();

                    return string.Join(", ", assetNames);
                }
            }

            return string.Empty;
        }

        private static long[] GetAssetTypeIds(AssetsRequestDto assetsRequest, AssetTypeCategory assetTypeCategory)
        {
            switch (assetTypeCategory)
            {
                case AssetTypeCategory.HardwareSet:
                    return assetsRequest.AssetTypeHardwareSetId.HasValue
                        ? new[] { assetsRequest.AssetTypeHardwareSetId.Value }
                        : new long[0];

                case AssetTypeCategory.Headset:
                    return assetsRequest.AssetTypeHeadsetId.HasValue
                        ? new[] { assetsRequest.AssetTypeHeadsetId.Value }
                        : new long[0];

                case AssetTypeCategory.Backpack:
                    return assetsRequest.AssetTypeBackpackId.HasValue
                        ? new[] { assetsRequest.AssetTypeBackpackId.Value }
                        : new long[0];

                case AssetTypeCategory.OptionalHardware:
                    return assetsRequest.AssetTypeOptionalHardwareIds;

                case AssetTypeCategory.Software:
                    return assetsRequest.AssetTypeSoftwareId.HasValue
                        ? new[] { assetsRequest.AssetTypeSoftwareId.Value }
                        : new long[0];

                case AssetTypeCategory.Other:
                    return assetsRequest.AssetTypeOtherIds;

                default:
                    return null;
            }
        }
    }
}