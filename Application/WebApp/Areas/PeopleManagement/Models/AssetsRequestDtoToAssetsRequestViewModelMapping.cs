﻿using System.Collections.Generic;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetsRequestDtoToAssetsRequestViewModelMapping : ClassMapping<AssetsRequestDto, AssetsRequestViewModel>
    {
        public AssetsRequestDtoToAssetsRequestViewModelMapping()
        {
            Mapping = d => new AssetsRequestViewModel
            {
                Id = d.Id,
                OnboardingRequestId = d.OnboardingRequestId,
                EmployeeId = d.EmployeeId,
                AssetTypeHardwareSetId = d.AssetTypeHardwareSetId,
                AssetTypeHardwareSetTooltip = d.AssetTypeHardwareSetName == null ? "" : d.AssetTypeHardwareSetName.ToString(),
                AssetTypeHardwareSetComment = d.AssetTypeHardwareSetComment,
                AssetTypeHeadsetId = d.AssetTypeHeadsetId,
                AssetTypeHeadsetTooltip = d.AssetTypeHeadsetName == null ? "" : d.AssetTypeHeadsetName.ToString(),
                AssetTypeHeadsetComment = d.AssetTypeHeadsetComment,
                AssetTypeBackpackId = d.AssetTypeBackpackId,
                AssetTypeBackpackTooltip = d.AssetTypeBackpackName == null ? "" : d.AssetTypeBackpackName.ToString(),
                AssetTypeBackpackComment = d.AssetTypeBackpackComment,
                AssetTypeOptionalHardwareIds = d.AssetTypeOptionalHardwareIds,
                AssetTypeOptionalHardwareTooltip = d.AssetTypeOptionalHardwareNames == null ? "" : string.Join(", ", (IEnumerable<LocalizedString>)d.AssetTypeOptionalHardwareNames),
                AssetTypeOptionalHardwareComment = d.AssetTypeOptionalHardwareComment,
                AssetTypeSoftwareId = d.AssetTypeSoftwareId,
                AssetTypeSoftwareTooltip = d.AssetTypeSoftwareName == null ? "" : d.AssetTypeSoftwareName.ToString(),
                AssetTypeSoftwareComment = d.AssetTypeSoftwareComment,
                AssetTypeOtherIds = d.AssetTypeOtherIds,
                AssetTypeOtherTooltip = d.AssetTypeOtherNames == null ? "" : string.Join(", ", (IEnumerable<LocalizedString>)d.AssetTypeOtherNames),
                AssetTypeOtherComment = d.AssetTypeOtherComment,
                CommentForIT = d.CommentForIT
            };
        }
    }
}
