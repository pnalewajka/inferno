﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetsRequestViewModelToAssetsRequestDtoMapping : ClassMapping<AssetsRequestViewModel, AssetsRequestDto>
    {
        public AssetsRequestViewModelToAssetsRequestDtoMapping()
        {
            Mapping = v => new AssetsRequestDto
            {
                Id = v.Id,
                OnboardingRequestId = v.OnboardingRequestId,
                EmployeeId = v.EmployeeId,
                AssetTypeHardwareSetId = v.AssetTypeHardwareSetId,
                AssetTypeHardwareSetComment = v.AssetTypeHardwareSetComment,
                AssetTypeHeadsetId = v.AssetTypeHeadsetId,
                AssetTypeHeadsetComment = v.AssetTypeHeadsetComment,
                AssetTypeBackpackId = v.AssetTypeBackpackId,
                AssetTypeBackpackComment = v.AssetTypeBackpackComment,
                AssetTypeOptionalHardwareIds = v.AssetTypeOptionalHardwareIds,
                AssetTypeOptionalHardwareComment = v.AssetTypeOptionalHardwareComment,
                AssetTypeSoftwareId = v.AssetTypeSoftwareId,
                AssetTypeSoftwareComment = v.AssetTypeSoftwareComment,
                AssetTypeOtherIds = v.AssetTypeOtherIds,
                AssetTypeOtherComment = v.AssetTypeOtherComment,
                CommentForIT = v.CommentForIT
            };
        }
    }
}
