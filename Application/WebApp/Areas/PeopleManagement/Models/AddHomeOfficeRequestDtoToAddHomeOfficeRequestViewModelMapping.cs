﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AddHomeOfficeRequestDtoToAddHomeOfficeRequestViewModelMapping :
        ClassMapping<AddHomeOfficeRequestDto, AddHomeOfficeRequestViewModel>
    {
        public AddHomeOfficeRequestDtoToAddHomeOfficeRequestViewModelMapping()
        {
            Mapping = d => new AddHomeOfficeRequestViewModel
            {
                From = d.From,
                To = d.To
            };
        }
    }
}