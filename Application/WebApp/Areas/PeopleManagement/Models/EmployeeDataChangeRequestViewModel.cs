﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmployeeDataChangeRequestViewModel
    {
        [Order(0)]
        [Required]
        [CannotBeEmpty]
        [UpdatesApprovers]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.EmployeesLabel), typeof(EmployeeDataChangeRequestResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=employees-to-change'")]
        public long[] EmployeeIds { get; set; }

        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.EffectiveOnLabel), typeof(EmployeeDataChangeRequestResources))]
        public DateTime? EffectiveOn { get; set; }

        [Order(2)]
        [UpdatesApprovers]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.LineManagerLabel), typeof(EmployeeDataChangeRequestResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url += '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        public Optional<long?> LineManager { get; set; }

        [Order(3)]
        [OptionalRequired]
        [UpdatesApprovers]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.OrgUnitLabel), typeof(EmployeeDataChangeRequestResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [Tooltip(nameof(OrgUnitNameTooltip))]
        public Optional<long?> OrgUnit { get; set; }

        [Visibility(VisibilityScope.None)]
        public string OrgUnitNameTooltip { get; set; }

        [Order(4)]
        [OptionalRequired]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.JobProfileLabel), typeof(EmployeeDataChangeRequestResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public Optional<long?> JobProfile { get; set; }

        [Order(5)]
        [OptionalRequired]
        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.LocationLabel), typeof(EmployeeDataChangeRequestResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public Optional<long?> Location { get; set; }

        [Order(6)]
        [OptionalRequired]
        [DisplayNameLocalized(nameof(EmployeeDataChangeRequestResources.PlaceOfWorkLabel), typeof(EmployeeDataChangeRequestResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        public Optional<PlaceOfWork?> PlaceOfWork { get; set; }
    }
}