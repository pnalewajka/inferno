﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmployeeDataChangeRequestActionSetModel
    { 
        public DateTime EffectiveOn { get; set; }

        public Optional<string> Location { get; set; }

        public IEnumerable<EmployeeDescriptor> Entries { get; set; }

        public IEnumerable<string> Approvers { get; set; }

        public class EmployeeDescriptor
        {
            public EmployeeDto Employee { get; set; }

            public EmployeeDto LineManager { get; set; }

            public LocalizedString ContractTypeName { get; set; }
        }
    }
}