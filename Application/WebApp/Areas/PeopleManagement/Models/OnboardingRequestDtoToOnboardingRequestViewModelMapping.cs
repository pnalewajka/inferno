﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingRequestDtoToOnboardingRequestViewModelMapping : ClassMapping<OnboardingRequestDto, OnboardingRequestViewModel>
    {
        public OnboardingRequestDtoToOnboardingRequestViewModelMapping()
        {
            Mapping = d => new OnboardingRequestViewModel
            {
                Id = d.Id,
                LineManagerEmployeeId = d.LineManagerEmployeeId,
                RecruiterEmployeeId = d.RecruiterEmployeeId,
                FirstName = d.FirstName,
                LastName = d.LastName,
                Login = d.Login,
                SignedOn = d.SignedOn,
                StartDate = d.StartDate,
                LocationId = d.LocationId,
                PlaceOfWork = d.PlaceOfWork,
                CompanyId = d.CompanyId,
                JobTitleId = d.JobTitleId,
                JobProfileId = d.JobProfileId,
                ContractTypeId = d.ContractTypeId,
                ProbationPeriodContractTypeId = d.ProbationPeriodContractTypeId,
                Compensation = d.Compensation,
                ProbationPeriodCompensation = d.ProbationPeriodCompensation,
                CommentsForPayroll = d.CommentsForPayroll,
                OrgUnitId = d.OrgUnitId,
                CommentsForHiringManager = d.CommentsForHiringManager,
                IsForeignEmployee = d.IsForeignEmployee,
                IsComingFromReferralProgram = d.IsComingFromReferralProgram,
                ReferralProgramDetails = d.ReferralProgramDetails,
                IsRelocationPackageNeeded = d.IsRelocationPackageNeeded,
                RelocationPackageDetails = d.RelocationPackageDetails,
                OnboardingDate = d.OnboardingDate,
                OnboardingLocationId = d.OnboardingLocationId,
                WelcomeAnnounceEmailInformation = d.WelcomeAnnounceEmailInformation,
                WelcomeAnnounceEmailPhotos = d.WelcomeAnnounceEmailPhotos.Select(f => new DocumentViewModel
                {
                    ContentType = f.ContentType,
                    DocumentName = f.DocumentName,
                    DocumentId = f.DocumentId
                }).ToList(),
                PayrollDocuments = d.PayrollDocuments.Select(f => new DocumentViewModel
                {
                    ContentType = f.ContentType,
                    DocumentName = f.DocumentName,
                    DocumentId = f.DocumentId
                }).ToList(),
                ForeignEmployeeDocuments = d.ForeignEmployeeDocuments.Select(f => new DocumentViewModel
                {
                    ContentType = f.ContentType,
                    DocumentName = f.DocumentName,
                    DocumentId = f.DocumentId
                }).ToList()
            };
        }
    }
}
