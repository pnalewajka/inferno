﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AddEmployeeRequestViewModelToAddEmployeeRequestDtoMapping : ClassMapping<AddEmployeeRequestViewModel, AddEmployeeRequestDto>
    {
        public AddEmployeeRequestViewModelToAddEmployeeRequestDtoMapping()
        {
            Mapping = v => new AddEmployeeRequestDto
            {
                FirstName = v.FirstName,
                LastName = v.LastName,
                Email = v.Email,
                Login = v.Login,
                CrmId = v.CrmId,
                JobTitleId = v.JobTitleId,
                JobProfileId = v.JobProfileId,
                OrgUnitId = v.OrgUnitId,
                LocationId = v.LocationId,
                PlaceOfWork = v.PlaceOfWork,
                CompanyId = v.CompanyId,
                LineManagerId = v.LineManagerId,
                ContractFrom = v.ContractFrom,
                ContractTo = v.ContractTo,
                ContractSignDate = v.ContractSignDate,
                MondayHours = v.MondayHours,
                TuesdayHours = v.TuesdayHours,
                WednesdayHours = v.WednesdayHours,
                ThursdayHours = v.ThursdayHours,
                FridayHours = v.FridayHours,
                SaturdayHours = v.SaturdayHours,
                SundayHours = v.SundayHours,
                ContractTypeId = v.ContractTypeId,
                ContractDuration = v.ContractDuration,
                VacationHourToDayRatio = v.VacationHourToDayRatio,
                TimeReportingMode = v.TimeReporting,
                AbsenceOnlyDefaultProjectId = v.AbsenceOnlyDefaultProjectId,
                CalendarId = v.CalendarId
            };
        }
    }
}