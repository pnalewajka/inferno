﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class EmploymentTerminationRequestDtoToEmploymentTerminationRequestViewModelMapping : ClassMapping<EmploymentTerminationRequestDto, EmploymentTerminationRequestViewModel>
    {
        public EmploymentTerminationRequestDtoToEmploymentTerminationRequestViewModelMapping()
        {
            Mapping = d => new EmploymentTerminationRequestViewModel
            {
                EmployeeId = d.EmployeeId,
                TerminationDate = d.TerminationDate,
                EndDate = d.EndDate,
                ReasonId = d.ReasonId
            };
        }
    }
}