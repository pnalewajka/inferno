﻿using Smt.Atomic.Business.PeopleManagement.ReportParameters;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class OnboardingOverviewReportParametersViewModelToOnboardingOverviewReportParametersDtoMapping
        : ClassMapping<OnboardingOverviewReportParametersViewModel, OnboardingOverviewReportParametersDto>
    {
        public OnboardingOverviewReportParametersViewModelToOnboardingOverviewReportParametersDtoMapping()
        {
            Mapping = v => new OnboardingOverviewReportParametersDto
            {
                From = v.From,
                To = v.To,
                Statuses = v.Statuses,
                Scope = v.Scope,
            };
        }
    }
}