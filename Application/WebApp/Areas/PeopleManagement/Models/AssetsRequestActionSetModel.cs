﻿using System;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetsRequestActionSetModel
    {
        public EmployeeDto Employee { get; set; }

        public EmployeeDto LineManager { get; set; }

        public EmployeeDto Recruiter { get; set; }

        public DateTime OnboardingDate { get; set; }

        public DateTime StartDate { get; set; }

        public string Login { get; set; }

        public string HardwareSet { get; set; }

        public string HardwareSetComment { get; set; }

        public string Headset { get; set; }

        public string HeadsetComment { get; set; }

        public string Backpack { get; set; }

        public string BackpackComment { get; set; }

        public string OptionalHardware { get; set; }

        public string OptionalHardwareComment { get; set; }

        public string Software { get; set; }

        public string SoftwareComment { get; set; }

        public string Other { get; set; }

        public string OtherComment { get; set; }

        public string CommentForIT { get; set; }
    }
}
