﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetsRequestViewModel
    {
        public long Id { get; set; }

        [HiddenInput]
        public long? OnboardingRequestId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(AssetsRequestResources.EmployeeLabel), typeof(AssetsRequestResources))]
        [UpdatesApprovers]
        public long EmployeeId { get; set; }

        [Tooltip(nameof(AssetTypeHardwareSetTooltip))]
        [ValuePicker(Type = typeof(AssetTypePickerController),
           OnResolveUrlJavaScript = "url = url +'&category=hardware-set&should-be-active=true'",
           OnSelectionChangedJavaScript = "AssetsRequest.onAssetTypeHardwareSetPickerSelectionChanged")]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeHardwareSetLabel), typeof(AssetsRequestResources))]
        public long? AssetTypeHardwareSetId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string AssetTypeHardwareSetTooltip { get; set; }

        [Hint("Hint")]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeHardwareSetCommentLabel), typeof(AssetsRequestResources))]
        public string AssetTypeHardwareSetComment { get; set; }

        [Tooltip(nameof(AssetTypeHeadsetTooltip))]
        [ValuePicker(Type = typeof(AssetTypePickerController),
            OnResolveUrlJavaScript = "url = url +'&category=headset&should-be-active=true'",
            OnSelectionChangedJavaScript = "AssetsRequest.onAssetTypeHeadsetPickerSelectionChanged")]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeHeadsetLabel), typeof(AssetsRequestResources))]
        public long? AssetTypeHeadsetId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string AssetTypeHeadsetTooltip { get; set; }

        [Hint("Hint")]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeHeadsetCommentLabel), typeof(AssetsRequestResources))]
        public string AssetTypeHeadsetComment { get; set; }

        [Tooltip(nameof(AssetTypeBackpackTooltip))]
        [ValuePicker(Type = typeof(AssetTypePickerController),
            OnResolveUrlJavaScript = "url = url +'&category=backpack&should-be-active=true'",
            OnSelectionChangedJavaScript = "AssetsRequest.onAssetTypeBackpackPickerSelectionChanged")]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeBackpackLabel), typeof(AssetsRequestResources))]
        public long? AssetTypeBackpackId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string AssetTypeBackpackTooltip { get; set; }

        [Hint("Hint")]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeBackpackCommentLabel), typeof(AssetsRequestResources))]
        public string AssetTypeBackpackComment { get; set; }

        [Tooltip(nameof(AssetTypeOptionalHardwareTooltip))]
        [ValuePicker(Type = typeof(AssetTypePickerController),
            OnResolveUrlJavaScript = "url = url +'&category=optional-hardware&should-be-active=true'",
            OnSelectionChangedJavaScript = "AssetsRequest.onAssetTypeOptionalHardwarePickerSelectionChanged")]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeOptionalHardwareLabel), typeof(AssetsRequestResources))]
        public long[] AssetTypeOptionalHardwareIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public string AssetTypeOptionalHardwareTooltip { get; set; }

        [Hint("Hint")]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeOptionalHardwareCommentLabel), typeof(AssetsRequestResources))]
        public string AssetTypeOptionalHardwareComment { get; set; }

        [Tooltip(nameof(AssetTypeSoftwareTooltip))]
        [ValuePicker(Type = typeof(AssetTypePickerController),
            OnResolveUrlJavaScript = "url = url +'&category=software&should-be-active=true'",
            OnSelectionChangedJavaScript = "AssetsRequest.onAssetTypeSoftwarePickerSelectionChanged")]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeSoftwareLabel), typeof(AssetsRequestResources))]
        public long? AssetTypeSoftwareId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string AssetTypeSoftwareTooltip { get; set; }

        [Hint("Hint")]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeSoftwareCommentLabel), typeof(AssetsRequestResources))]
        public string AssetTypeSoftwareComment { get; set; }

        [Tooltip(nameof(AssetTypeOtherTooltip))]
        [ValuePicker(Type = typeof(AssetTypePickerController),
            OnResolveUrlJavaScript = "url = url +'&category=other&should-be-active=true'",
            OnSelectionChangedJavaScript = "AssetsRequest.onAssetTypeOtherPickerSelectionChanged")]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeOtherLabel), typeof(AssetsRequestResources))]
        public long[] AssetTypeOtherIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public string AssetTypeOtherTooltip { get; set; }

        [Hint("Hint")]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.AssetTypeOtherCommentLabel), typeof(AssetsRequestResources))]
        public string AssetTypeOtherComment { get; set; }

        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(AssetsRequestResources.CommentsForITLabel), typeof(AssetsRequestResources))]
        public string CommentForIT { get; set; }
    }
}