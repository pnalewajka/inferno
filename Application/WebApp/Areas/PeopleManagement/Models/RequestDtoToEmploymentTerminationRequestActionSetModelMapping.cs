﻿using System;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class RequestDtoToEmploymentTerminationRequestActionSetModelMapping : ClassMapping<RequestDto, EmploymentTerminationRequestActionSetModel>
    {
        public RequestDtoToEmploymentTerminationRequestActionSetModelMapping(
            IEmployeeService employeeService,
            IContractTypeService contractTypeService,
            IEmploymentTerminationReasonService employmentTerminationReasonService)
        {
            Mapping = r => GetActionSetModel(r, employeeService, contractTypeService, employmentTerminationReasonService);
        }

        private static EmploymentTerminationRequestActionSetModel GetActionSetModel(
            RequestDto request,
            IEmployeeService employeeService,
            IContractTypeService contractTypeService,
            IEmploymentTerminationReasonService employmentTerminationReasonService)
        {
            var employmentTerminationRequest = (EmploymentTerminationRequestDto)request.RequestObject;
            var employee = employeeService.GetEmployeeOrDefaultById(employmentTerminationRequest.EmployeeId);

            if (!employee.LineManagerId.HasValue)
            {
                throw new InvalidOperationException($"Given employee (ID={employee.Id}) has no line manager assigned");
            }

            return new EmploymentTerminationRequestActionSetModel
            {
                Employee = employee,
                LineManager = employeeService.GetEmployeeOrDefaultById(employee.LineManagerId.Value),
                EndDate = employmentTerminationRequest.EndDate,
                ContractTypeName = contractTypeService.GetLatestContractTypeOrDefaultByEmployeeId(employmentTerminationRequest.EmployeeId).Name,
                TerminationReasonText = employmentTerminationReasonService.GetEmploymentTerminationReasonOrDefaultById(employmentTerminationRequest.ReasonId).Name
            };
        }
    }
}