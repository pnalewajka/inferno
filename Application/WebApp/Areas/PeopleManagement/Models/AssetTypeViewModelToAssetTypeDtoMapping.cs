﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.PeopleManagement.Models
{
    public class AssetTypeViewModelToAssetTypeDtoMapping : ClassMapping<AssetTypeViewModel, AssetTypeDto>
    {
        public AssetTypeViewModelToAssetTypeDtoMapping()
        {
            Mapping = d => new AssetTypeDto
            {
                Id = d.Id,
                Name = d.Name,
                Category = d.Category.Value,
                DetailsRequired = d.DetailsRequired,
                DetailsHint = d.DetailsHint,
                IsActive = d.IsActive,
                Description = d.DescriptionRichText,
                Timestamp = d.Timestamp
            };
        }
    }
}