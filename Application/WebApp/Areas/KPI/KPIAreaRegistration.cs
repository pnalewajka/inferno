﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.KPI
{
    public class KPIAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "KPI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "KPI_default",
                "KPI/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
