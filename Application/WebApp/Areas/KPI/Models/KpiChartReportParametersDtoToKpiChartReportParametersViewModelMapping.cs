﻿using System.Linq;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiChartReportParametersDtoToKpiChartReportParametersViewModelMapping : ClassMapping<KpiChartReportParametersDto, KpiChartReportParametersViewModel>
    {
        public KpiChartReportParametersDtoToKpiChartReportParametersViewModelMapping()
        {
            Mapping = v => new KpiChartReportParametersViewModel
            {
                ReportType = v.ReportType,
                KpiDefinitionId = v.KpiDefinitionId,
                KpiDefinitionWithAggregationFunctionIds = v.KpiDefinitions.Select(k => k.CombinedId).ToArray(),
                ReportStartDate = v.ReportStartDate,
                ReportEndDate = v.ReportEndDate,
                Entity = v.Entity,
                ProjectIds = v.ProjectIds,
                ProjectManagerId = v.ProjectManagerId,
                ProjectOrgUnitIds = v.ProjectOrgUnitIds,
                ProjectDate = v.ProjectDate,
                EmployeeIds = v.EmployeeIds,
                EmployeeStatus = v.EmployeeStatus,
                EmployeeOrgUnitIds = v.EmployeeOrgUnitIds,
                EmployeeLineManagerId = v.EmployeeLineManagerId,
                EmployeeLocationIds = v.EmployeeLocationIds,
                EmployeeContractTypeIds = v.EmployeeContractTypeIds,
                OrgUnitIds = v.OrgUnitIds,
                OrgUnitManagerIds = v.OrgUnitManagerIds,
            };
        }
    }
}