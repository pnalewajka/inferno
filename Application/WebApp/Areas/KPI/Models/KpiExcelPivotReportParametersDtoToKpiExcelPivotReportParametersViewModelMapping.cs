﻿using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiExcelPivotReportParametersDtoToKpiExcelPivotReportParametersViewModelMapping : ClassMapping<KpiExcelPivotReportParametersDto, KpiExcelPivotReportParametersViewModel>
    {
        public KpiExcelPivotReportParametersDtoToKpiExcelPivotReportParametersViewModelMapping()
        {
            Mapping = v => new KpiExcelPivotReportParametersViewModel
            {
                KpiDefinitionIds = v.KpiDefinitionIds,
                ReportStartDate = v.ReportStartDate,
                ReportEndDate = v.ReportEndDate,
                Entity = v.Entity,
                ProjectIds = v.ProjectIds,
                ProjectManagerId = v.ProjectManagerId,
                ProjectOrgUnitIds = v.ProjectOrgUnitIds,
                ProjectDate = v.ProjectDate,
                EmployeeIds = v.EmployeeIds,
                EmployeeStatus = v.EmployeeStatus,
                EmployeeOrgUnitIds = v.EmployeeOrgUnitIds,
                EmployeeLineManagerId = v.EmployeeLineManagerId,
                EmployeeLocationIds = v.EmployeeLocationIds,
                EmployeeContractTypeIds = v.EmployeeContractTypeIds,
                OrgUnitIds = v.OrgUnitIds,
                OrgUnitManagerIds = v.OrgUnitManagerIds,
            };
        }
    }
}