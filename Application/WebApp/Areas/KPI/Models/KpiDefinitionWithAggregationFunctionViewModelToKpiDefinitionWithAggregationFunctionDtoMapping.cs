﻿using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiDefinitionWithAggregationFunctionViewModelToKpiDefinitionWithAggregationFunctionDtoMapping : ClassMapping<KpiDefinitionWithAggregationFunctionViewModel, KpiDefinitionWithAggregationFunctionDto>
    {
        public KpiDefinitionWithAggregationFunctionViewModelToKpiDefinitionWithAggregationFunctionDtoMapping()
        {
            Mapping = v => new KpiDefinitionWithAggregationFunctionDto
            {
                Id = v.Id,
                NameAndFunction = v.NameAndFunction,
            };
        }
    }
}
