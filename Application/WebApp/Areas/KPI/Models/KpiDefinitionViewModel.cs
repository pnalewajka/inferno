﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.KPI.Resources;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    [Identifier("Models.KpiDefinitionViewModel")]
    public class KpiDefinitionViewModel
    {
        public long Id { get; set; }

        [StringLength(35)]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.NameLabel), typeof(KpiDefinitionResources))]
        [Required]
        public string Name { get; set; }

        [AllowHtml]
        [StringLength(1000)]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.DescriptionLabel), typeof(KpiDefinitionResources))]
        [RichText(RichTextEditorPresets.Standard)]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.EntityLabel), typeof(KpiDefinitionResources))]
        [Required]
        public KpiEntity Entity { get; set; }

        [Required]
        [StringLength(4000)]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.SqlFormulaLabel), typeof(KpiDefinitionResources))]
        [CodeEditor(SyntaxType.Sql)]
        public string SqlFormula { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.RolesLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(RolePickerController))]
        public long[] RoleIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
