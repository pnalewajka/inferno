﻿using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiDefinitionDtoToKpiDefinitionViewModelMapping : ClassMapping<KpiDefinitionDto, KpiDefinitionViewModel>
    {
        public KpiDefinitionDtoToKpiDefinitionViewModelMapping()
        {
            Mapping = d => new KpiDefinitionViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Entity = d.Entity,
                RoleIds = d.RoleIds,
                SqlFormula = d.SqlFormula,
                Timestamp = d.Timestamp
            };
        }
    }
}
