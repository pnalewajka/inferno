﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.KPI.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    [TabDescription(0, "reportParameters", nameof(KpiDefinitionResources.ReportParametersTabLabel), typeof(KpiDefinitionResources))]
    [TabDescription(1, "entityFilters", nameof(KpiDefinitionResources.EntityFilteringTabLabel), typeof(KpiDefinitionResources))]
    public class KpiReportEntitiesBaseViewModel
    {
        private const string ProjectClass = "project-filter";
        private const string EmployeeClass = "employee-filter";
        private const string OrgUnitClass = "orgunit-filter";

        [DisplayNameLocalized(nameof(KpiDefinitionResources.ProjectsLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [Tab("entityFilters")]
        [Render(ControlCssClass = ProjectClass)]
        public long[] ProjectIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.ProjectManagerLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Tab("entityFilters")]
        [Render(ControlCssClass = ProjectClass)]
        public long? ProjectManagerId { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.OrgUnitsLabel), typeof(KpiDefinitionResources))]
        [Render(ControlCssClass = ProjectClass)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [Tab("entityFilters")]
        public long[] ProjectOrgUnitIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.ProjectDateLabel), typeof(KpiDefinitionResources))]
        [Render(ControlCssClass = ProjectClass)]
        [DatePicker]
        [DataType(DataType.Date)]
        [Tab("entityFilters")]
        public DateTime? ProjectDate { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.EmployeesLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Tab("entityFilters")]
        [Render(ControlCssClass = EmployeeClass)]
        public long[] EmployeeIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.EmployeeStatusLabel), typeof(KpiDefinitionResources))]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<EmployeeStatus>), EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent)]
        [Render(ControlCssClass = EmployeeClass)]
        [Tab("entityFilters")]
        public EmployeeStatus? EmployeeStatus { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.OrgUnitsLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [Render(ControlCssClass = EmployeeClass)]
        [Tab("entityFilters")]
        public long[] EmployeeOrgUnitIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.EmployeeLineManagerLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Render(ControlCssClass = EmployeeClass)]
        [Tab("entityFilters")]
        public long? EmployeeLineManagerId { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.EmployeeLocationsLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [Render(ControlCssClass = EmployeeClass)]
        [Tab("entityFilters")]
        public long[] EmployeeLocationIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.EmployeeContractTypesLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [Render(ControlCssClass = EmployeeClass)]
        [Tab("entityFilters")]
        public long[] EmployeeContractTypeIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.OrgUnitsLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [Render(ControlCssClass = OrgUnitClass)]
        [Tab("entityFilters")]
        public long[] OrgUnitIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.OrgUnitManagersLabel), typeof(KpiDefinitionResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Render(ControlCssClass = OrgUnitClass)]
        [Tab("entityFilters")]
        public long[] OrgUnitManagerIds { get; set; }
    }
}