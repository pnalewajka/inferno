﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.KPI.Controllers;
using Smt.Atomic.WebApp.Areas.KPI.Resources;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    [Identifier("Models.KpiReportParameters")]
    [Script("~/Areas/KPI/Scripts/KpiReportParameters.js")]
    public class KpiReportParametersViewModel : KpiReportEntitiesBaseViewModel
    {
        [Required]
        [Tab("reportParameters")]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.EntityLabel), typeof(KpiDefinitionResources))]
        [OnValueChange(ExecuteOnLoad = true, ClientHandler = "KpiReportParameters.onEntityChange()")]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<KpiEntity>), EmptyItemBehavior = EmptyItemBehavior.NotAvailable)]
        public KpiEntity Entity { get; set; }

        [CannotBeEmpty]
        [Tab("reportParameters")]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.KpiDefinitionsLabel), typeof(KpiDefinitionResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(KpiDefinitionPickerController),
            OnResolveUrlJavaScript = "url += '&entity=' + $('select[name=\"" + nameof(Entity) + "\"]').val()")]
        public long[] KpiDefinitionIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.KpiDefinitionsLabel), typeof(KpiDefinitionResources))]
        [Visibility(VisibilityScope.None)]
        [Tab("reportParameters")]
        [Hub(nameof(KpiDefinitionIds))]
        public string[] KpiDefinitionNames { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.ReportDateLabel), typeof(KpiDefinitionResources))]
        [DatePicker]
        [Tab("reportParameters")]
        [DataType(DataType.Date)]
        public DateTime ReportDate { get; set; }
    }
}