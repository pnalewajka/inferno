﻿using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiDefinitionViewModelToKpiDefinitionDtoMapping : ClassMapping<KpiDefinitionViewModel, KpiDefinitionDto>
    {
        public KpiDefinitionViewModelToKpiDefinitionDtoMapping()
        {
            Mapping = v => new KpiDefinitionDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                Entity = v.Entity,
                RoleIds = v.RoleIds,
                SqlFormula = v.SqlFormula,
                Timestamp = v.Timestamp
            };
        }
    }
}
