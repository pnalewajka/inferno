﻿using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiDefinitionWithAggregationFunctionDtoToKpiDefinitionWithAggregationFunctionViewModelMapping : ClassMapping<KpiDefinitionWithAggregationFunctionDto, KpiDefinitionWithAggregationFunctionViewModel>
    {
        public KpiDefinitionWithAggregationFunctionDtoToKpiDefinitionWithAggregationFunctionViewModelMapping()
        {
            Mapping = d => new KpiDefinitionWithAggregationFunctionViewModel
            {
                Id = d.Id,
                NameAndFunction = d.NameAndFunction,
            };
        }
    }
}
