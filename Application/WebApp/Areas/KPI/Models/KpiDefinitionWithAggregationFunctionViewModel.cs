﻿namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    public class KpiDefinitionWithAggregationFunctionViewModel
    {
        public long Id { get; set; }

        public string NameAndFunction { get; set; }

        public override string ToString()
        {
            return NameAndFunction;
        }
    }
}