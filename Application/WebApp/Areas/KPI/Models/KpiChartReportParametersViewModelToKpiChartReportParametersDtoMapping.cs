﻿using System.Linq;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.KPI.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class KpiChartReportParametersViewModelToKpiChartReportParametersDtoMapping : ClassMapping<KpiChartReportParametersViewModel, KpiChartReportParametersDto>
    {
        public KpiChartReportParametersViewModelToKpiChartReportParametersDtoMapping()
        {
            Mapping = v => new KpiChartReportParametersDto
            {
                ReportType = v.ReportType,
                KpiDefinitionId = v.KpiDefinitionId,
                KpiDefinitions = v.KpiDefinitionWithAggregationFunctionIds.Select(k =>
                    KpiDefinitionWithAggregationFunction.FromCombinedId(k)).ToArray(),
                ReportStartDate = v.ReportStartDate,
                ReportEndDate = v.ReportEndDate,
                Entity = v.Entity,
                ProjectIds = v.ProjectIds,
                ProjectManagerId = v.ProjectManagerId,
                ProjectOrgUnitIds = v.ProjectOrgUnitIds,
                ProjectDate = v.ProjectDate,
                EmployeeIds = v.EmployeeIds,
                EmployeeStatus = v.EmployeeStatus,
                EmployeeOrgUnitIds = v.EmployeeOrgUnitIds,
                EmployeeLineManagerId = v.EmployeeLineManagerId,
                EmployeeLocationIds = v.EmployeeLocationIds,
                EmployeeContractTypeIds = v.EmployeeContractTypeIds,
                OrgUnitIds = v.OrgUnitIds,
                OrgUnitManagerIds = v.OrgUnitManagerIds,
            };
        }
    }
}