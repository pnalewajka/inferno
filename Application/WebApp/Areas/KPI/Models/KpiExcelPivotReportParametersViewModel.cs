﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.KPI.Controllers;
using Smt.Atomic.WebApp.Areas.KPI.Resources;

namespace Smt.Atomic.WebApp.Areas.KPI.Models
{
    [Identifier("Models.KpiExcelPivotReportParameters")]
    [Script("~/Areas/KPI/Scripts/KpiReportParameters.js")]
    public class KpiExcelPivotReportParametersViewModel : KpiReportEntitiesBaseViewModel, IValidatableObject
    {
        [Required]
        [Tab("reportParameters")]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.EntityLabel), typeof(KpiDefinitionResources))]
        [OnValueChange(ExecuteOnLoad = true, ClientHandler = "KpiReportParameters.onEntityChange()")]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<KpiEntity>), EmptyItemBehavior = EmptyItemBehavior.NotAvailable)]
        public KpiEntity Entity { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.KpiDefinitionsLabel), typeof(KpiDefinitionResources))]
        [Visibility(VisibilityScope.Form)]
        [Tab("reportParameters")]
        [ValuePicker(Type = typeof(KpiDefinitionPickerController),
            OnResolveUrlJavaScript = "url += '&entity=' + $('select[name=\"" + nameof(Entity) + "\"]').val()")]
        public long[] KpiDefinitionIds { get; set; }

        [DisplayNameLocalized(nameof(KpiDefinitionResources.KpiDefinitionsLabel), typeof(KpiDefinitionResources))]
        [Visibility(VisibilityScope.None)]
        [Hub(nameof(KpiDefinitionIds))]
        [Tab("reportParameters")]
        public string[] KpiDefinitionnNames { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.ReportStartDateLabel), typeof(KpiDefinitionResources))]
        [DatePicker]
        [DataType(DataType.Date)]
        [Tab("reportParameters")]
        public DateTime ReportStartDate { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(KpiDefinitionResources.ReportEndDateLabel), typeof(KpiDefinitionResources))]
        [DatePicker]
        [Tab("reportParameters")]
        [DataType(DataType.Date)]
        public DateTime ReportEndDate { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ReportStartDate > ReportEndDate)
            {
                yield return new PropertyValidationResult(KpiDefinitionResources.StartDateExceedsEndDate, () => ReportStartDate);
            }
        }
    }
}