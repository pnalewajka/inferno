﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.KPI.Models;
using Smt.Atomic.WebApp.Areas.KPI.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.KPI.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewKpiDefinitions)]
    public class KpiDefinitionController : CardIndexController<KpiDefinitionViewModel, KpiDefinitionDto>
    {
        public KpiDefinitionController(
            IKpiDefinitionCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = KpiDefinitionResources.Title;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddKpiDefinitions;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewKpiDefinitions;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditKpiDefinitions;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteKpiDefinitions;

            var gridViewConfiguration = new GridViewConfiguration<KpiDefinitionViewModel>("List", "list")
            {
                IsDefault = true
            };

            gridViewConfiguration.IncludeColumns(new List<Expression<Func<KpiDefinitionViewModel, object>>>
            {
                definition => definition.Name,
                definition => definition.Description,
                definition => definition.Entity
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = "project",
                            DisplayName = "Project",
                        },
                        new Filter
                        {
                            Code = "employee",
                            DisplayName = "Employee",
                        },
                        new Filter
                        {
                            Code = "orgunit",
                            DisplayName = "OrgUnit",
                        }
                    }
                }
            };

            CardIndex.Settings.GridViewConfigurations = new List<IGridViewConfiguration> { gridViewConfiguration };
        }
    }
}