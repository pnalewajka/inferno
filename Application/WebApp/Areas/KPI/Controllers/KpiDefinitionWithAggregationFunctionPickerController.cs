﻿using System.Web.Mvc;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.Business.KPI.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.KPI.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.KPI.Controllers
{
    [Identifier("ValuePickers.KpiDefinitionWithAggregationFunction")]
    [AtomicAuthorize(SecurityRoleType.CanViewKpiDefinitions)]
    public class KpiDefinitionWithAggregationFunctionPickerController : CardIndexController<KpiDefinitionWithAggregationFunctionViewModel, KpiDefinitionWithAggregationFunctionDto, KpiDefinitionContext>, IHubController
    {
        private readonly IKpiDefinitionCardIndexDataService _kpiDefinitionCardIndex;
        private readonly IClassMappingFactory _classMappingFactory;

        public KpiDefinitionWithAggregationFunctionPickerController(IKpiDefinitionWithAggregationFunctionCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies, IKpiDefinitionCardIndexDataService kpiDefinitionCardIndexDataService) : base(cardIndexDataService, baseControllerDependencies)
        {
            _kpiDefinitionCardIndex = kpiDefinitionCardIndexDataService;
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;
        }

        public PartialViewResult Hub(long id)
        {
            var kpiDefinitionId = KpiDefinitionWithAggregationFunction.FromCombinedId(CardIndex.GetRecordById(id).Id).KpiDefinitionId;
            var model = _classMappingFactory.CreateMapping<KpiDefinitionDto, KpiDefinitionViewModel>().CreateFromSource(_kpiDefinitionCardIndex.GetRecordById(kpiDefinitionId));

            var dialogModel = new ModelBasedDialogViewModel<KpiDefinitionViewModel>(model, StandardButtons.None)
            {
                Title = $"{model.Name} ({model.Entity.ToString()})",
            };

            return PartialView(
                "~/Areas/KPI/Views/KpiDefinitionHub.cshtml",
                dialogModel);
        }
    }
}