﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.KPI.Models;
using Smt.Atomic.WebApp.Areas.KPI.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.KPI.Controllers
{
    [Identifier("ValuePickers.KpiDefinition")]
    [AtomicAuthorize]
    public class KpiDefinitionPickerController : ReadOnlyCardIndexController<KpiDefinitionViewModel, KpiDefinitionDto, KpiDefinitionContext>, IHubController
    {
        public KpiDefinitionPickerController(IKpiDefinitionCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
        }

        public PartialViewResult Hub(long id)
        {
            var model = CardIndex.GetRecordById(id);

            var dialogModel = new ModelBasedDialogViewModel<KpiDefinitionViewModel>(model, StandardButtons.None)
            {
                Title = $"{model.Name} ({model.Entity.ToString()})",
            };

            return PartialView(
                "~/Areas/KPI/Views/KpiDefinitionHub.cshtml",
                dialogModel);
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            CardIndex.Settings.PageSize = 15;
            base.PrepareForValuePicker(allowMultipleRowSelection);
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configuration = new GridViewConfiguration<KpiDefinitionViewModel>(KpiDefinitionResources.Title, "KpiDefinitionPicker")
            {
                IsDefault = true
            };

            configuration.IncludeColumns(new List<Expression<Func<KpiDefinitionViewModel, object>>>
            {
                definition => definition.Name,
                definition => definition.Description,
                definition => definition.Entity
            });

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}