﻿namespace KpiReportParameters {
    const oneKpiReportType = "1";
    const requiredClass = "required";
    let shouldClearKpiDefinitions = false;

    function addRequiredClassForKpiDefinitions() {
        $('label[for="kpi-definition-with-aggregation-function-ids"]').addClass(requiredClass);
        $('label[for="kpi-definition-id"]').addClass(requiredClass);
        $('label[for="kpi-definition-ids"]').addClass(requiredClass);
    }

    export function onReportTypeChange() {
        if ($("select[id=\"report-type\"]").val() === oneKpiReportType) {
            $("[data-field-name=\"KpiDefinitionId\"]").parent().show();
            $("[data-field-name=\"KpiDefinitionWithAggregationFunctionIds\"]").parent().hide();
        } else {
            $("[data-field-name=\"KpiDefinitionId\"]").parent().hide();
            $("[data-field-name=\"KpiDefinitionWithAggregationFunctionIds\"]").parent().show();
        }
    }

    function clearKpiDefinitions() {
        $("[data-field-name=\"KpiDefinitionId\"]").find(".value-picker-remove-button").click();
        $("[data-field-name=\"KpiDefinitionIds\"]").find(".value-picker-remove-button").click();
        $("[data-field-name=\"KpiDefinitionWithAggregationFunctionIds\"]").find(".value-picker-remove-button").click();
    }

    export function onEntityChange() {
        if (shouldClearKpiDefinitions) {
            clearKpiDefinitions();
        } else {
            shouldClearKpiDefinitions = true;
            addRequiredClassForKpiDefinitions();
        }

        $(".project-filter").hide();
        $(".employee-filter").hide();
        $(".orgunit-filter").hide();

        switch ($("select[id=\"entity\"]").val()) {
            case "0": $(".project-filter").show(); break;
            case "1": $(".employee-filter").show(); break;
            case "2": $(".orgunit-filter").show(); break;
        }
    }
}
