﻿using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Reporting.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Reporting.Controllers
{
    [Identifier("ValuePickers.ReportRelatedClass")]
    [AtomicAuthorize(SecurityRoleType.CanViewReportDefinition)]
    public class ReportRelatedClassPickerController : ReadOnlyCardIndexController<ReportRelatedClassViewModel, ReportRelatedClassDto, ReportRelatedClassContext>
    {
        public ReportRelatedClassPickerController(
            IReportRelatedClassCardIndexDataService dataSourceTypeCardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(dataSourceTypeCardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = true;
        }
    }
}