﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Reporting.Models;
using Smt.Atomic.WebApp.Areas.Reporting.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Reporting.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewReportDefinition)]
    [PerformanceTest(nameof(ReportDefinitionController.List))]
    public class ReportDefinitionController : CardIndexController<ReportDefinitionViewModel, ReportDefinitionDto>
    {
        private readonly IReportRegistrationService _reportRegistrationService;
        private readonly IReportDesignService _reportDesignService;
        private readonly IClassMapping<ReportDesignDto, ReportDesignViewModel> _reportDesignDtoToReportDesignViewModelMapping;
        private readonly IClassMapping<ReportDesignViewModel, ReportDesignDto> _reportDesignViewModelToReportDesignDtolMapping;

        public ReportDefinitionController(
            IReportRegistrationService reportRegistrationService,
            IReportDesignService reportDesignService,
            IClassMapping<ReportDesignDto, ReportDesignViewModel> reportDesignDtoToReportDesignViewModelMapping,
            IClassMapping<ReportDesignViewModel, ReportDesignDto> reportDesignViewModelToReportDesignDtolMapping,
            IReportDefinitionCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            _reportRegistrationService = reportRegistrationService;
            _reportDesignService = reportDesignService;
            _reportDesignDtoToReportDesignViewModelMapping = reportDesignDtoToReportDesignViewModelMapping;
            _reportDesignViewModelToReportDesignDtolMapping = reportDesignViewModelToReportDesignDtolMapping;

            CardIndex.Settings.Title = ReportDefinitionResources.ReportDefinitionControllerTitle;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddReportDefinition;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditReportDefinition;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteReportDefinition;

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Code,  ReportDefinitionResources.ReportDefinitionCodeLabel),
                new SearchArea(SearchAreaCodes.Name,  ReportDefinitionResources.ReportDefinitionNameLabel),
                new SearchArea(SearchAreaCodes.Description, ReportDefinitionResources.ReportDefinitionDescriptionLabel),
                new SearchArea(SearchAreaCodes.Area, ReportDefinitionResources.ReportDefinitionAreaLabel),
            };

            Layout.Scripts.Add("~/Areas/Reporting/Scripts/ReportingDialog.js");

            AddRegisterDefaultReportButton();
            AddDesignButton();
            AddPerformReportButton();
        }

        private void AddPerformReportButton()
        {
            var performReportAction = Url.Action(nameof(ReportExecutionController.ConfigureReportDialog), RoutingHelper.GetControllerName<ReportExecutionController>());
            var button = new ToolbarButton
            {
                Text = ReportDefinitionResources.PerformReportButtonText,
                RequiredRole = SecurityRoleType.CanViewReportDefinition,
                OnClickAction = new JavaScriptCallAction($"ReportingDialog.onReportClicked('{performReportAction}');"),
                Icon = FontAwesome.Play,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void AddRegisterDefaultReportButton()
        {
            var button = new ToolbarButton
            {
                Text = ReportDefinitionResources.RegisterDefaultReportsButtonText,
                RequiredRole = SecurityRoleType.CanAddReportDefinition,
                OnClickAction = Url.UriActionGet(nameof(RegisterNewReports)),
                Icon = FontAwesome.Cogs,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void AddDesignButton()
        {
            var button = new ToolbarButton
            {
                Text = ReportDefinitionResources.DesignReportTemplatesButtonText,
                Icon = FontAwesome.Pencil,
                RequiredRole = SecurityRoleType.CanEditReportDefinition,
                OnClickAction = Url.UriActionGet(nameof(Design), KnownParameter.SelectedId),
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.dataSourceTypeIdentifierCode === 'DataSource.SqlBasedDataSource'"
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        [AtomicAuthorize(SecurityRoleType.CanAddReportDefinition)]
        public ActionResult RegisterNewReports()
        {
            _reportRegistrationService.RegisterNewReports();

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanEditReportDefinition)]
        [BreadcrumbBar("CardIndex/ReportDesign")]
        public ActionResult Design(long id)
        {
            Layout.Scripts.Add("~/Areas/Reporting/Scripts/ReportDesign.js");

            var model = _reportDesignDtoToReportDesignViewModelMapping.CreateFromSource(_reportDesignService.GetReportDesignById(id));
            var dialogModel = new EditDialogViewModel<ReportDesignViewModel>(model);

            return View(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanEditReportDefinition)]
        public ActionResult Design(ReportDesignViewModel model)
        {
            if (ModelState.IsValid)
            {
                _reportDesignService.UpdateReportDesign(_reportDesignViewModelToReportDesignDtolMapping.CreateFromSource(model));
                AddAlert(AlertType.Success, ReportDefinitionResources.ReportTemplatesSavedSuccessAlert);
            }

            return Design(model.Id);
        }
    }
}