﻿using System.Web.Mvc;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Areas.Reporting.Models;

namespace Smt.Atomic.WebApp.Areas.Reporting.Controllers
{
    [AtomicAuthorize]
    public class PivotTableController : AtomicController
    {
        public PivotTableController(
            IBaseControllerDependencies baseDependencies)
            : base(baseDependencies)
        {
            Layout.Scripts.Add("~/Scripts/xlnt/xlnt.js");
            Layout.Scripts.Add("~/Scripts/pivot/pivot.js");
            Layout.Scripts.Add("~/Scripts/pivot/pivot.subtotal.js");
            Layout.Scripts.Add("~/Areas/Reporting/Scripts/PivotTableReport.js");
            Layout.Scripts.Add("~/Areas/Reporting/Scripts/PivotReportMenu.js");

            Layout.Css.Add("~/Content/pivot/pivot.css");
            Layout.Css.Add("~/Content/pivot/pivot.subtotal.css");

            Layout.Resources.AddFrom<CardIndexResources>(nameof(CardIndexResources.RecordCountFormat));
            Layout.EnumResources.Add<RequestStatus>();
            Layout.EnumResources.Add<CostItemGroupType>();
        }

        [BreadcrumbBar("Start/GenericReport")]
        public ActionResult Index(PivotTableViewModel viewModel)
        {
            Layout.PageTitle = Layout.PageHeader = viewModel.ReportName;

            viewModel.ReportParametersDialogUrl = Url.Action(nameof(ReportExecutionController.ConfigureReportDialog),
                RoutingHelper.GetControllerName<ReportExecutionController>());

            return View("PivotTable", viewModel);
        }
    }
}