﻿using System.Web.Mvc;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Extensions;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Areas.Reporting.Resources;

namespace Smt.Atomic.WebApp.Areas.Reporting.Controllers
{
    [AtomicAuthorize]
    public class ReportExecutionController : AtomicController
    {
        private readonly IReportingService _reportingService;

        public ReportExecutionController(
            IReportingService reportingService,
            IBaseControllerDependencies baseDependencies)
            : base(baseDependencies)
        {
            _reportingService = reportingService;

            Layout.Resources.AddFrom<CardIndexResources>(nameof(CardIndexResources.RecordCountFormat));
        }

        [HttpPost]
        public ActionResult ConfigureReportDialog(string reportCode)
        {
            var viewModel = _reportingService.GetReportViewModel(reportCode);
            ViewModelHelper.TryUpdateModel(this, viewModel, clearValidationErrors: true);

            ViewBag.ReportCode = reportCode;
            var dialogModel = new ModelBasedDialogViewModel<object>(viewModel)
            {
                Title = ReportingResources.ReportParametersDialogTitle
            };

            return PartialView("PerformReportDialog", dialogModel);
        }

        [BreadcrumbBar("Start/GenericReport")]
        public ActionResult ConfigureReport(string reportCode)
        {
            const string generateParameterName = "Generate";

            if (Request.ContainsQueryParameter(generateParameterName))
            {
                return PerformReport(reportCode, true); // Force generation from GET request
            }

            var viewModel = _reportingService.GetReportViewModel(reportCode);
            var reportName = _reportingService.GetReportName(reportCode);

            ViewModelHelper.TryUpdateModel(this, viewModel, clearValidationErrors: true);

            ViewBag.ReportCode = reportCode;
            Layout.PageTitle = Layout.PageHeader = reportName.ToString();

            return View("PerformReport", viewModel);
        }

        public ActionResult PerformReport(string reportCode, bool forceReport = false)
        {
            // In case someone navigates using normal GET (without forcing), redirect to parameters
            if (!forceReport && Request.GetActionType() == ActionType.Get)
            {
                return new RedirectResult(Url.ActionWithParams(nameof(ConfigureReport), "reportCode={0}", (object)reportCode));
            }

            var viewModel = _reportingService.GetReportViewModel(reportCode);
            ViewModelHelper.TryUpdateModel(this, viewModel);

            if (!ModelState.IsValid)
            {
                ViewBag.ReportCode = reportCode;

                return View(viewModel);
            }

            using (var reportContent = _reportingService.PerformReport(reportCode, viewModel))
            {
                return reportContent.Render();
            }
        }

        [HttpPost]
        public ActionResult PerformJsonReport(string reportCode)
        {
            var viewModel = _reportingService.GetReportViewModel(reportCode);
            ViewModelHelper.TryUpdateModel(this, viewModel);

            if (!ModelState.IsValid)
            {
                ViewBag.ReportCode = reportCode;

                return new JsonNetResult(new
                {
                    ok = false,
                });
            }

            return new JsonNetResult(new
            {
                ok = true,
                data = _reportingService.GetReportData(reportCode, viewModel),
            });
        }

        [HttpPost]
        public ActionResult PerformReportDialog(string reportCode)
        {
            var viewModel = _reportingService.GetReportViewModel(reportCode);
            ViewModelHelper.TryUpdateModel(this, viewModel);

            if (!ModelState.IsValid)
            {
                ViewBag.ReportCode = reportCode;
                var dialogModel = new ModelBasedDialogViewModel<object>(viewModel)
                {
                    Title = ReportingResources.ReportParametersDialogTitle
                };

                return PartialView(dialogModel);
            }

            var url = Url.Action("PerformReport", new { reportCode });

            return DialogHelper.Redirect(this, url, true);
        }
    }
}