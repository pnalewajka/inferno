﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Areas.Reporting.Models;

namespace Smt.Atomic.WebApp.Areas.Reporting.Controllers
{
    [AtomicAuthorize]
    public class ChartController : AtomicController
    {
        public ChartController(
            IBaseControllerDependencies baseDependencies)
            : base(baseDependencies)
        {
            Layout.Scripts.Add("~/Scripts/Chart.bundle.js");
            Layout.Scripts.Add("~/Areas/Reporting/Scripts/PivotReportMenu.js");
            Layout.Scripts.Add("~/Areas/Reporting/Scripts/ChartColors.js");
            Layout.Scripts.Add("~/Areas/Reporting/Scripts/ChartReport.js");
            Layout.Css.Add("~/Areas/Reporting/Content/ChartReport.css");

            Layout.Resources.AddFrom<CardIndexResources>(nameof(CardIndexResources.RecordCountFormat));
        }

        [BreadcrumbBar("Start/GenericReport")]
        public ActionResult Index(ChartViewModel viewModel)
        {
            Layout.PageTitle = Layout.PageHeader = viewModel.ReportName;

            viewModel.ReportParametersDialogUrl = Url.Action(nameof(ReportExecutionController.ConfigureReportDialog),
                RoutingHelper.GetControllerName<ReportExecutionController>());

            return View("Chart", viewModel);
        }
    }
}