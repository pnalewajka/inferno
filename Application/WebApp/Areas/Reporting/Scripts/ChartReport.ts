﻿module ChartReport {

    export interface IPivotChartConfig {
        type: string;
        data: any;
    }

    declare var Chart: any;
    export function changeConfiguration(config: IPivotChartConfig): void {
        $("#chart-canvas").remove();
        $("#chart-container").append('<canvas id="chart-canvas"></canvas>');
        const canvas = <HTMLCanvasElement>document.getElementById("chart-canvas");
        const ctx = canvas.getContext("2d");

        const graph = new Chart(ctx, config);
    }

    export function showParametersModal(dialogUrl: string, reportCode: string, parameters: any): void {
        const url = UrlHelper.updateUrlParameter(dialogUrl, "reportCode", reportCode);
        Dialogs.showDialog({
            actionUrl: url,
            formData: parameters,
            eventHandler: () => { }
        });
    }

    export function populateConfigurations(configurations: Reporting.IPivotMenuItem<IPivotChartConfig>[]): void {
        const $configMenu = $('#predefined-config-menu');
        $configMenu.append(PivotReportMenu.buildMenu(configurations, $configMenu, changeConfigurationSpecific));

        if (configurations.length > 0) {
            $configMenu.append($('<li class="sudo divider"></li>'));
        }
    }

    function changeConfigurationSpecific(config: Reporting.IPivotMenuItem<IPivotChartConfig>): void {
        changeConfiguration(config.config);
    }

    export const bootstrapColors = Reporting.Chart.getBootstrapColors();

    export function getColor(colorIndex: number): string {
        return bootstrapColors[colorIndex % bootstrapColors.length];        
    }
}
