﻿namespace Reporting.Chart {
    const MattRed = "#d9534f";
    const Red = "#ff4444";
    const DarkRed = "#CC0000";
    const MattOrange = "#f0ad4e";
    const Orange = "#ffbb33";
    const DarkOrange = "#FF8800";
    const MattGreen = "#5CB85C";
    const Green = "#00C851";
    const DarkGreen = "#007E33";
    const LightBlue = "#5BC0DE";
    const MattBlue = "#428BCA";
    const Blue = "#33b5e5";
    const DarkBlue = "#0099CC";

    export function getBootstrapColors() {
        return [
            MattRed,
            MattOrange,
            MattGreen,
            MattBlue,
            Red,
            LightBlue,
            Orange,
            Green,
            Blue,
            DarkRed,
            MattOrange,
            Orange,
            DarkOrange,
            DarkGreen,
            DarkBlue
        ];
    }
}
