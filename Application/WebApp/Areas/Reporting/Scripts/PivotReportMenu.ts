﻿module PivotReportMenu {
    export function buildMenu<TConfig>(configurations: ReadonlyArray<Reporting.IPivotMenuItem<TConfig>>, $configMenu: JQuery, changeConfig: (config: Reporting.IPivotMenuItem<TConfig>) => void): JQuery {
        let menuItems: IMenuItem<TConfig>[] = [];
        let $tmpConfigMenu = $(`<ul/>`);
        for (let config of configurations) {
            if (config.name != undefined) {
                menuItems = [...menuItems, ...buildMenuItem<TConfig>(config.name.split("/"), config, menuItems, 0)];
            }
        }

        for (let i = 0; i < menuItems.length; i++) {
            $tmpConfigMenu.append(buildMenuBranch(menuItems[i], $configMenu, changeConfig));
        }

        var firstChild = getFirstChild(menuItems[0])

        applyCurrentConfigurationSpecific(firstChild, $tmpConfigMenu);

        return $tmpConfigMenu.children();
    }

    function getFirstChild<TConfig>(menuItem: IMenuItem<TConfig>): IMenuItem<TConfig> {
        return menuItem.submenu.length > 0 ? getFirstChild(menuItem.submenu[0]) : menuItem;
    }

    export function applyCurrentConfigurationSpecific<TConfig>(menuItem: IMenuItem<TConfig>, $configMenu: JQuery): void {
        let breadcrumb = $('.selectedConfiguration');
        breadcrumb.length === 0 ?
            $('.breadcrumb').append($(`<li class="active selectedConfiguration">${menuItem.config.name}</li>`)) : breadcrumb[0].innerText = menuItem.config.name;
        
        var id = GenerateIdForMenuItem(menuItem);

        $configMenu.find('.radio').removeClass('radio');
        $configMenu.find(`#${id}`).children('a').addClass('radio');
    }

    function GenerateIdForMenuItem<TConfig>(menuItem: IMenuItem<TConfig>): string {
        return menuItem.config.name.replace(/[^a-zA-Z]/g, "");
    }

    function buildMenuBranch<TConfig>(menuItem: IMenuItem<TConfig>, $configMenu: JQuery, changeConfig: (config: Reporting.IPivotMenuItem<TConfig>) => void): JQuery {
        if (menuItem.submenu.length > 0) {

            const $superLi = $(`<li class="dropdown-submenu"><a href="#">${menuItem.name}</a><ul class="dropdown-menu"></ul></li></ul>`);

            for (let submenu of menuItem.submenu) {
                $superLi.children('ul').append(buildMenuBranch(submenu, $configMenu, changeConfig));
            }

            return $superLi;
        }

        var id = GenerateIdForMenuItem(menuItem);

        let $li = $(`<li id="${id}"><a href="#"><span>${menuItem.name}</span></a></li>`);

        $li.on('click', function () {
            if (menuItem.config != undefined) {
                changeConfig(menuItem.config);
                const $configMenu = $('#predefined-config-menu');
                applyCurrentConfigurationSpecific(menuItem, $configMenu);
            }
        }).find("a").click($event => $event.preventDefault());

        return $li;
    }


    function buildMenuItem<TConfig>(names: string[], config: Reporting.IPivotMenuItem<TConfig>, configGroupList: IMenuItem<TConfig>[], level: number): IMenuItem<TConfig>[] {
        const name = names[level];
        let result: IMenuItem<TConfig>[] = [];

        let [item] = configGroupList.filter((e) => e.name === name);

        if (!item) {
            item = getBaseConfigGroup(name, config);
            result = [...result, item];
        }

        if (names.length > level + 1) {
            item.submenu = [...item.submenu, ...buildMenuItem(names, config, item.submenu, level + 1)];
        }

        return result;
    };

    function getBaseConfigGroup<TConfig>(name: string, config: Reporting.IPivotMenuItem<TConfig>): IMenuItem<TConfig> {
        return {
            name: name,
            submenu: [],
            config: config
        };
    }

    interface IMenuItem<TConfig> {
        name: string;
        submenu: IMenuItem<TConfig>[];
        config: Reporting.IPivotMenuItem<TConfig>;
    }
}
