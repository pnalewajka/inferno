﻿module PivotTableReport {

    declare var pivotTable: Reporting.PivotTable;

    export function init(pivot: Reporting.PivotTable, reportName: string): void {
        const customConfigs = pivot.getConfigDefinitions();
        const $configMenu = $('#predefined-config-menu');

        $configMenu.append(PivotReportMenu.buildMenu(customConfigs, $configMenu, changeConfigurationSpecific));

        createExportToExcelButton(pivot, reportName, $configMenu);

        if (customConfigs.length > 0) {
            $configMenu.append($('<li class="sudo divider"></li>'));
        }
    }

    function createExportToExcelButton(pivot: Reporting.PivotTable, reportName: string, $configMenu: JQuery): void {
        var button = $("#download-excel");

        load_xlnt({ locateFile: url => "/Scripts/xlnt/" + url }).then(xlnt => {
            button.prop("disabled", false);
            button.click(() => pivot.downloadExcel(xlnt, `${reportName}.xlsx`));
        });
    }

    function changeConfigurationSpecific(config: Reporting.IPivotTablePredefinedConfig): void {
        pivotTable.applyConfigByCode(config.code);
    }
}
