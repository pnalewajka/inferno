﻿module ReportingDialog {
    var dialogWidth: string = "1000px";

    export interface IReportRow extends Grid.IRow {
        code: string
    };

    export function onReportClicked(dialogUrl: string, reportCode?: string) {
        if (reportCode === undefined) {
            const component = Grid.getComponent($("table.table")[0]);
            const selectedReports = component.getSelectedRowData<IReportRow>();
            reportCode = selectedReports[0].code;
        }

        let url = UrlHelper.updateUrlParameter(dialogUrl, "reportCode", reportCode);

        Dialogs.showDialog({
            actionUrl: url,
            onBeforeInit: (dialog) => {
                Dialogs.setWidth(dialog, dialogWidth);
            }
        });
    }
}