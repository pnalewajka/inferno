﻿namespace ReportDesign {
    export function saveDesignAndPerformReport(saveDesignUrl: string, performReportUrl: string, reportCode: string): void {
        $.ajax({
            type: "POST",
            url: saveDesignUrl,
            data: $("#main-form").serialize(),
            success: (data) =>
                ReportingDialog.onReportClicked(performReportUrl, reportCode)
        });
    }
}
