﻿using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportRelatedClassViewModelToReportRelatedClassDtoMapping : ClassMapping<ReportRelatedClassViewModel, ReportRelatedClassDto>
    {
        public ReportRelatedClassViewModelToReportRelatedClassDtoMapping()
        {
            Mapping = v => new ReportRelatedClassDto
            {
                Id = v.Id,
                Identifier = v.Code
            };
        }
    }
}
