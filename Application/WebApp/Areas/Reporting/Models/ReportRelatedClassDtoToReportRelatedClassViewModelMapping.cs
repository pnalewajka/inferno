﻿using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportRelatedClassDtoToReportRelatedClassViewModelMapping : ClassMapping<ReportRelatedClassDto, ReportRelatedClassViewModel>
    {
        public ReportRelatedClassDtoToReportRelatedClassViewModelMapping()
        {
            Mapping = d => new ReportRelatedClassViewModel
            {
                Id = d.Id,
                Code = d.Identifier
            };
        }
    }
}
