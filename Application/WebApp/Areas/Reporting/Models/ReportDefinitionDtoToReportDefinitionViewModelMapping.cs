﻿using System.Linq;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportDefinitionDtoToReportDefinitionViewModelMapping : ClassMapping<ReportDefinitionDto, ReportDefinitionViewModel>
    {
        public ReportDefinitionDtoToReportDefinitionViewModelMapping(
            IReportRelatedClassCardIndexDataService dataSourceTypeCardIndexDataService)
        {
            Mapping = d => new ReportDefinitionViewModel
            {
                Id = d.Id,
                Code = d.Code,
                Name = d.Name,
                Description = d.Description,
                ReportingEngineIdentifierId = string.IsNullOrWhiteSpace(d.ReportingEngineIdentifier) ? 0 : dataSourceTypeCardIndexDataService.GetId(d.ReportingEngineIdentifier),
                ReportingEngineIdentifierCode = d.ReportingEngineIdentifier,
                OutputFileNameTemplate = d.OutputFileNameTemplate,
                DataSourceTypeIdentifierCode = d.DataSourceTypeIdentifier,
                DataSourceTypeIdentifierId = string.IsNullOrWhiteSpace(d.DataSourceTypeIdentifier) ? 0 : dataSourceTypeCardIndexDataService.GetId(d.DataSourceTypeIdentifier),
                ReportTemplates = d.ReportTemplates.Select(rt=>new Presentation.Common.Models.DocumentViewModel
                {
                    ContentType = rt.ContentType,
                    DocumentName = rt.DocumentName,
                    DocumentId = rt.DocumentId
                }).ToList(),
                Area = d.Area,
                Group = d.Group,
                AllowedForProfiles = d.AllowedForProfiles,
                Timestamp = d.Timestamp
            };
        }
    }
}
