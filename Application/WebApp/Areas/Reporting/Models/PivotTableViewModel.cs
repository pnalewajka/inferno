﻿using System;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class PivotTableViewModel
    {
        public string ReportName { get; set; }
        public string ReportCode { get; set; }
        public string ReportParametersDialogUrl { get; set; }
        public string ParametersJson { get; set; }
        public string ConfigurationJavascript { get; set; }
        public string DataJson { get; set; }
    }
}