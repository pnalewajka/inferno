﻿using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportDesignDtoToReportDesignViewModelMapping : ClassMapping<ReportDesignDto, ReportDesignViewModel>
    {
        public ReportDesignDtoToReportDesignViewModelMapping()
        {
            Mapping = d => new ReportDesignViewModel
            {
                Id = d.Id,
                Code = d.Code,
                Name = d.Name,
                ManifestContent = d.ManifestContent,
                SqlContent = d.SqlContent,
                ConfigContent = d.ConfigContent,
            };
        }
    }
}