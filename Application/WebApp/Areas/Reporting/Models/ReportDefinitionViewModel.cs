﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Reporting.DocumentMapping;
using Smt.Atomic.Business.Reporting.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Reporting.Controllers;
using Smt.Atomic.WebApp.Areas.Reporting.Resources;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportDefinitionViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionCodeLabel), typeof(ReportDefinitionResources))]
        public string Code { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionNameLabel), typeof(ReportDefinitionResources))]
        public LocalizedString Name { get; set; }

        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionDescriptionLabel), typeof(ReportDefinitionResources))]
        public LocalizedString Description { get; set; }

        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionAreaLabel), typeof(ReportDefinitionResources))]
        [Visibility(VisibilityScope.Form)]
        public string Area { get; set; }

        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionGroupLabel), typeof(ReportDefinitionResources))]
        [Visibility(VisibilityScope.Form)]
        public string Group { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionOutputFileNameTemplateLabel), typeof(ReportDefinitionResources))]
        public string OutputFileNameTemplate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Required]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportingEngineIdentifierLabel), typeof(ReportDefinitionResources))]
        [ValuePicker(Type = typeof(ReportRelatedClassPickerController), OnResolveUrlJavaScript = "url=url + '&class-type=" + nameof(ReportRelatedClassType.ReportingEngine) + "'")]
        public long ReportingEngineIdentifierId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportingEngineIdentifierLabel), typeof(ReportDefinitionResources))]
        public string ReportingEngineIdentifierCode { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionDataSourceTypeIdentifierLabel), typeof(ReportDefinitionResources))]
        [ValuePicker(Type = typeof(ReportRelatedClassPickerController), OnResolveUrlJavaScript = "url=url + '&class-type=" + nameof(ReportRelatedClassType.DataSource) + "'")]
        [Visibility(VisibilityScope.Form)]
        public long DataSourceTypeIdentifierId { get; set; }

        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionDataSourceTypeIdentifierLabel), typeof(ReportDefinitionResources))]
        [Visibility(VisibilityScope.Grid)]
        public string DataSourceTypeIdentifierCode { get; set; }

        [DocumentUpload(typeof(ReportTemplateDocumentMapping))]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionReportTemplatesLabel), typeof(ReportDefinitionResources))]
        [Visibility(VisibilityScope.Form)]
        public List<DocumentViewModel> ReportTemplates { get; set; }

        [ValuePicker(Type = typeof(ProfilePickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [Visibility(VisibilityScope.Form)]
        public long[] AllowedForProfiles { get; set; }

        public byte[] Timestamp { get; set; }

        public ReportDefinitionViewModel()
        {
            ReportTemplates = new List<DocumentViewModel>();
        }

        public override string ToString()
        {
            return $"[{Code}] {Name}";
        }
    }
}
