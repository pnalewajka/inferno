﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Reporting.Resources;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    [TabDescription(0, "manifest", "ManifestTabName", typeof(ReportDefinitionResources))]
    [TabDescription(1, "sql", "SqlTabName", typeof(ReportDefinitionResources))]
    [TabDescription(2, "config", "ConfigTabName", typeof(ReportDefinitionResources))]
    [Identifier("Models.ReportDesignViewModel")]
    public class ReportDesignViewModel
    {
        public long Id { get; set; }
        
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionCodeLabel), typeof(ReportDefinitionResources))]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.None)]
        public string Code { get; set; }

        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionNameLabel), typeof(ReportDefinitionResources))]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.None)]
        public LocalizedString Name { get; set; }

        [Tab("manifest")]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ManifestContent), typeof(ReportDefinitionResources))]
        [CodeEditor(SyntaxType.Xml)]
        [Multiline(40)]
        [AllowHtml]
        [Required]
        public string ManifestContent { get; set; }

        [Tab("sql")]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.SqlContent), typeof(ReportDefinitionResources))]
        [CodeEditor(SyntaxType.Sql)]
        [Multiline(40)]
        [AllowHtml]
        [Required]
        public string SqlContent { get; set; }

        [Tab("config")]
        [DisplayNameLocalized(nameof(ReportDefinitionResources.ConfigContent), typeof(ReportDefinitionResources))]
        [CodeEditor(SyntaxType.JavaScript)]
        [Multiline(40)]
        [AllowHtml]
        [Required]
        public string ConfigContent { get; set; }

        public override string ToString()
        {
            return $"[{Code}] {Name}";
        }
    }
}