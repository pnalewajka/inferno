﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Reporting.Resources;
namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportRelatedClassViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(ReportDefinitionResources.ReportDefinitionConfigurationCodeLabel), typeof(ReportDefinitionResources))]
        public string Code { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}
