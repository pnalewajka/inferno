﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportDefinitionViewModelToReportDefinitionDtoMapping : ClassMapping<ReportDefinitionViewModel, ReportDefinitionDto>
    {
        public ReportDefinitionViewModelToReportDefinitionDtoMapping(
            IReportRelatedClassCardIndexDataService dataSourceTypeCardIndexDataService)
        {
            Mapping = v => new ReportDefinitionDto
            {
                Id = v.Id,
                Code = v.Code,
                Name = v.Name,
                Description = v.Description,
                ReportingEngineIdentifier = dataSourceTypeCardIndexDataService.GetIdentifierOrDefault(v.ReportingEngineIdentifierId),
                DataSourceTypeIdentifier = dataSourceTypeCardIndexDataService.GetIdentifierOrDefault(v.DataSourceTypeIdentifierId),
                OutputFileNameTemplate = v.OutputFileNameTemplate,
                ReportTemplates = v.ReportTemplates.Select(rt => new DocumentDto
                {
                    ContentType = rt.ContentType,
                    DocumentId = rt.DocumentId,
                    DocumentName = rt.DocumentName,
                    TemporaryDocumentId = rt.TemporaryDocumentId
                }).ToArray(),
                Area = v.Area,
                Group = v.Group,
                AllowedForProfiles = v.AllowedForProfiles,
                Timestamp = v.Timestamp
            };
        }
    }
}
