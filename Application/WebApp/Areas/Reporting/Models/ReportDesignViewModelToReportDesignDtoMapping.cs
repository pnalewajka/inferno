﻿using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Reporting.Models
{
    public class ReportDesignViewModelToReportDesignDtoMapping : ClassMapping<ReportDesignViewModel, ReportDesignDto>
    {
        public ReportDesignViewModelToReportDesignDtoMapping()
        {
            Mapping = v => new ReportDesignDto
            {
                Id = v.Id,
                Code = v.Code,
                Name = v.Name,
                ManifestContent = v.ManifestContent,
                SqlContent = v.SqlContent,
                ConfigContent = v.ConfigContent,
            };
        }
    }
}