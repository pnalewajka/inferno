﻿using System.IO;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.Reporting.Renderers
{
    [Identifier("ReportRenderer.ReportExcelFileRenderer")]
    public class ReportExcelFileRenderer : ReportFileRenderer
    {
        private const string ExcelExtension = ".xlsx";

        public ReportExcelFileRenderer(IRazorTemplateService razorTemplateService) : base(razorTemplateService)
        {
        }

        protected override string GetContentType(IReportGenerationContext context)
        {
            return MimeHelper.OfficeDocumentExcel;
        }

        protected override string GetFileName(IReportGenerationContext context)
        {
            return Path.ChangeExtension(base.GetFileName(context), ExcelExtension);
        }
    }
}