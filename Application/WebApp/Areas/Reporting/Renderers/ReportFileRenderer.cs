﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.Reporting.Renderers
{
    [Identifier("ReportRenderer.FileContent")]
    public class ReportFileRenderer : IReportRenderer
    {
        private readonly IRazorTemplateService _razorTemplateService;

        public ReportFileRenderer(
            IRazorTemplateService razorTemplateService)
        {
            _razorTemplateService = razorTemplateService;
        }

        protected virtual string GetFileName(IReportGenerationContext context)
        {
            var template = context.Definition.ReportTemplates.FirstOrDefault()?.DocumentName ?? context.Definition.OutputFileNameTemplate;

            return template;
        }

        protected virtual string GetContentType(IReportGenerationContext context)
        {
            var template = context.Definition.ReportTemplates.First();

            return template.ContentType;
        }

        public ActionResult Render(IReportGenerationContext context)
        {
            var fileName = GetFileName(context);

            if (!string.IsNullOrWhiteSpace(context.Definition.OutputFileNameTemplate))
            {
                var code = context.ReportData.Value.GetType().FullName;
                fileName = _razorTemplateService.TemplateRunAndCompile(code, context.Definition.OutputFileNameTemplate, context.ReportData.Value);
            }

            return new ArchivableFileContentResult(StreamHelper.ReadBytes(context.Content), GetContentType(context))
            {
                FileDownloadName = fileName
            };
        }
    }
}
