﻿using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Models.Dynamic;
using Smt.Atomic.WebApp.Areas.Reporting.Controllers;
using Smt.Atomic.WebApp.Areas.Reporting.Models;

namespace Smt.Atomic.WebApp.Areas.Reporting.Renderers
{
    [Identifier("ReportRenderer.Chart")]
    public class ChartRenderer : IReportRenderer
    {
        private readonly IReportingService _reportingService;
        private readonly IClassMappingFactory _classMappingFactory;

        public ChartRenderer(IReportingService reportingService, IClassMappingFactory classMappingFactory)
        {
            _reportingService = reportingService;
            _classMappingFactory = classMappingFactory;
        }

        public ActionResult Render(IReportGenerationContext context)
        {
            var configurationJavascript = new StreamReader(context.Content).ReadToEnd();
            var reportData = JsonHelper.Serialize(context.ReportData.Value, new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                Converters = new JsonConverter[]
                {
                    new StringEnumConverter(),
                },
            });

            var viewModelType = _reportingService.GetReportViewModelType(context.Definition.Code);
            var dtoType = context.Parameters.GetType();
            var mapping = _classMappingFactory.CreateMapping(dtoType, viewModelType);
            var parametersViewModel = viewModelType != typeof(DynamicViewModel)
                ? mapping.CreateFromSource(context.Parameters)
                : null;

            return new TransferActionResult(
                typeof(ChartController),
                nameof(ChartController.Index),
                new Dictionary<string, object>()
                {
                    {
                        "viewModel",
                        new ChartViewModel
                        {
                            ReportName = context.Definition.Name.English,
                            ReportCode = context.Definition.Code,
                            ParametersJson = JsonHelper.Serialize(parametersViewModel ?? context.Parameters),
                            ConfigurationJavascript = configurationJavascript,
                            DataJson = reportData,
                        }
                    }
                }
            );
        }
    }
}