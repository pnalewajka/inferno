﻿using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Notifications.Models
{
    public class EmailDtoToEmailViewModelMapping : ClassMapping<EmailDto, EmailViewModel>
    {
        public EmailDtoToEmailViewModelMapping()
        {
            Mapping = d => new EmailViewModel
            {
                Id = d.Id,
                To = d.To,
                From = d.From,
                Subject = d.Subject,
                Status = d.Status,
                SentOn = d.SentOn,
                Body = GetContentBody(d.Content.IsHtml, d.Content.Body),
                Exception = d.Exception,
                CreatedBy = d.CreatedBy,
                CreatedOn = d.CreatedOn
            };
        }

        private static string GetContentBody(bool isHtml, string body)
        {
            if (isHtml)
            {
                var lowerBody = body.ToLower();
                var start = lowerBody.IndexOf("<body>") + 6;
                var end = lowerBody.IndexOf("</body>");

                if (start != -1 && end != -1)
                {
                    body = body.Substring(start, end - start);
                }
            }

            return body;
        }
    }
}
