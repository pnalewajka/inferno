﻿using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Notifications.Models
{
    public class EmailViewModelToEmailDtoMapping : ClassMapping<EmailViewModel, EmailDto>
    {
        public EmailViewModelToEmailDtoMapping()
        {
            Mapping = d => new EmailDto
            {
                Id = d.Id,
                To = d.To,
                From = d.From,
                Subject = d.Subject,
                Status = d.Status,
                SentOn = d.SentOn,
                Exception = d.Exception,
                CreatedBy = d.CreatedBy,
                CreatedOn = d.CreatedOn
            };
        }
    }
}
