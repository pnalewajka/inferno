﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Notifications.Resources;

namespace Smt.Atomic.WebApp.Areas.Notifications.Models
{
    public class EmailViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(EmailResource.CreatedOnLabel), typeof(EmailResource))]
        public DateTime CreatedOn { get; set; }

        [StringLength(384)]
        [StringFormat(ShouldEncodeHtml = true)]
        [DisplayNameLocalized(nameof(EmailResource.ToLabel), typeof(EmailResource))]
        public string To { get; set; }

        [StringLength(128)]
        [StringFormat(ShouldEncodeHtml = true)]
        [DisplayNameLocalized(nameof(EmailResource.FromLabel), typeof(EmailResource))]
        public string From { get; set; }

        [StringLength(256)]
        [DisplayNameLocalized(nameof(EmailResource.SubjectLabel), typeof(EmailResource))]
        public string Subject { get; set; }

        [DisplayNameLocalized(nameof(EmailResource.StatusLabel), typeof(EmailResource))]
        public EmailStatus Status { get; set; }

        [DisplayNameLocalized(nameof(EmailResource.SentOnLabel), typeof(EmailResource))]
        public DateTime? SentOn { get; set; }

        [DisplayNameLocalized(nameof(EmailResource.ExceptionLabel), typeof(EmailResource))]
        [Visibility(VisibilityScope.Form)]
        public string Exception { get; set; }

        [DisplayNameLocalized(nameof(EmailResource.CreatedByLabel), typeof(EmailResource))]
        public string CreatedBy { get; set; }

        [DisplayNameLocalized(nameof(EmailResource.BodyLabel), typeof(EmailResource))]
        [Visibility(VisibilityScope.None)]
        public string Body { get; set; }
    }
}