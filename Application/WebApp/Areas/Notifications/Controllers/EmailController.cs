﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Notifications.Models;
using Smt.Atomic.WebApp.Areas.Notifications.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Notifications.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmails)]
    [PerformanceTest(nameof(EmailController.List))]
    public class EmailController : ReadOnlyCardIndexController<EmailViewModel, EmailDto, ParentIdContext>
    {
        private readonly IEmailCardIndexService _emailCardIndexService;

        public EmailController(IEmailCardIndexService emailCardIndexService, IBaseControllerDependencies baseControllerDependencies)
            : base(emailCardIndexService, baseControllerDependencies)
        {
            _emailCardIndexService = emailCardIndexService;

            CardIndex.Settings.Title = EmailResource.EmailControllerTitle;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.ShouldHideFilters = false;
            CardIndex.Settings.ViewViewName = "View";
            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = EmailResource.StatusFilterGroupName,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<EmailStatus>()
                }
            };

            var manageProfilesButton = new ToolbarButton
            {
                Text = EmailResource.ResendEmailButtonText,
                OnClickAction = Url.UriActionPost(nameof(RetrySending), KnownParameter.SelectedId),
                Icon = FontAwesome.Refresh,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.exception!=null"
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(manageProfilesButton);
        }

        [HttpPost]
        public ActionResult RetrySending(long id)
        {
            var result = _emailCardIndexService.RetrySending(id);

            AddAlerts(result);

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }
    }
}