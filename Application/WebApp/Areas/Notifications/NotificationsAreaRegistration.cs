﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Notifications
{
    public class NotificationsAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Notifications";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Notifications_default",
                "Notifications/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}