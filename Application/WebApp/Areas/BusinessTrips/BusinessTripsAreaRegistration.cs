﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips
{
    public class BusinessTripsAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "BusinessTrips";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BusinessTrips_default",
                "BusinessTrips/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}