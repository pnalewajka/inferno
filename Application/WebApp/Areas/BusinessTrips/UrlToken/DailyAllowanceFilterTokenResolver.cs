﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.UrlToken
{
    public class DailyAllowanceFilterTokenResolver : IUrlTokenResolver
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;

        private const string DailyAllowanceFilterToken = "%DAILY-ALLOWANCE-FILTER%";

        public DailyAllowanceFilterTokenResolver(IPrincipalProvider principalProvider, ITimeService timeService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
        }

        public IEnumerable<string> GetTokens()
        {
            yield return DailyAllowanceFilterToken;
        }

        public string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeId = _principalProvider.Current.EmployeeId;

                if (employeeId == -1)
                {
                    return string.Empty;
                }

                var countryId = unitOfWork.Repositories.Employees.Where(e => e.Id == employeeId).Select(e => e.Location.City.CountryId).Single();
                var date = $"{_timeService.GetCurrentDate():yyyy-MM-dd}";

                return
                    $"employee-country.country-id={countryId}&filter=employee-country*valid-for-date&valid-for-date.valid-for={date}";
            }

        }
    }
}