﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Breadcrumbs
{
    public class BusinessTripItineraryBreadcrumbItem : BusinessTripBaseCustomViewBreadcrumbItem
    {
        public BusinessTripItineraryBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = BusinessTripResources.ItinereryTitle;
        }

        protected override string LastItemDisplayName => BusinessTripResources.ItinereryTitle;
    }
}