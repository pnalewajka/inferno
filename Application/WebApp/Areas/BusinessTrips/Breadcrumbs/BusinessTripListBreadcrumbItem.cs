﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Breadcrumbs
{
    public class BusinessTripListBreadcrumbItem : CardIndexBreadcrumbItem
    {
        public BusinessTripListBreadcrumbItem(
            IBreadcrumbContextProvider breadcrumbContextProvider,
            IBreadcrumbService breadcrumbService)
            : base(breadcrumbContextProvider, breadcrumbService)
        {
        }

        protected override void SetDisplayName(string displayName)
        {
            var contextDate = ((BusinessTripController)Context.Controller).GetContextDate();

            if (contextDate.HasValue)
            {
                var newDisplayName = string.Format(
                    BusinessTripResources.BusinessTripBreacrumbItemSuffix,
                    displayName,
                    DateHelper.BeginningOfMonth(contextDate.Value.Year, (byte)contextDate.Value.Month));

                base.SetDisplayName(newDisplayName);
            }
            else
            {
                base.SetDisplayName(displayName);
            }
        }
    }
}