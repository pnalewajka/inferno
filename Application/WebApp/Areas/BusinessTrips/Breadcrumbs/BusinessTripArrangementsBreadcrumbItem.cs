﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Breadcrumbs
{
    public class BusinessTripArrangementsBreadcrumbItem : BusinessTripBaseCustomViewBreadcrumbItem
    {
        public BusinessTripArrangementsBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
        }

        protected override string LastItemDisplayName => BusinessTripResources.ArrangementsTitle;
    }
}
