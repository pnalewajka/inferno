﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Breadcrumbs
{
    public class BusinessTripAdvancedPaymentBreadcrumbItem : BreadcrumbItem
    {
        public BusinessTripAdvancedPaymentBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = BusinessTripResources.AdvancedPaymentsTitle;
        }
    }
}