﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Breadcrumbs
{
    public class BusinessTripSettlementsBreadcrumbItem : BreadcrumbItem
    {
        public BusinessTripSettlementsBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = BusinessTripResources.SettlementsTitle;
        }
    }
}
