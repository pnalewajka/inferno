﻿using System.Linq;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Breadcrumbs
{
    public abstract class BusinessTripBaseCustomViewBreadcrumbItem : BreadcrumbItem
    {
        public BusinessTripBaseCustomViewBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            var controllerName = RoutingHelper.GetControllerName<BusinessTripController>();
            var references = Context.SiteMap.GetPath(controllerName, nameof(BusinessTripController.List)).ToList();

            var items = references
                .Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText()))
                .Concat(new[] { new BreadcrumbItemViewModel(null, LastItemDisplayName) })
                .ToList();

            SetItems(items);
        }

        protected abstract string LastItemDisplayName { get; }
    }
}
