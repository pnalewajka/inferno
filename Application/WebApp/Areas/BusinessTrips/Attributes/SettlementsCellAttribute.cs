﻿using System.Linq;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Attributes
{
    public class SettlementsCellAttribute : CellContentAttribute
    {
        public class SettlementRenderingModel : CustomControlRenderingModel, ICellContent
        {
            public override string ToString()
            {
                var settlements = (BusinessTripSettlementViewModel[])ControlModel;

                return string.Join(", ", settlements.Select(g => g.ToString()));
            }
        }
    
        private string customViewPath;

        public SettlementsCellAttribute(string viewPath = null)
        {
            customViewPath = viewPath;
        }

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            return new SettlementRenderingModel { CssClass = "settlement", ControlModel = cellContentConverterContext.PropertyValue };
        }

        public override string ViewPath => customViewPath ?? "Settlements.Column";

        protected override string TemplateCode => string.Empty;
    }
}