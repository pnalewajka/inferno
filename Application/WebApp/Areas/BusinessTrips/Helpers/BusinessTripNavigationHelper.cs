﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Helpers
{
    public static class BusinessTripNavigationHelper
    {
        public class BusinessTripTab
        {
            public string Name { get; set; }

            public Type Controller { get; set; }

            public string ActionName { get; set; }

            public SecurityRoleType RequiredRole { get; set; }

            public bool IsDefault { get; set; }

            public string GetUrl(UrlHelper urlHelper, string query)
            {
                return urlHelper.UriActionFormatGet(ActionName, RoutingHelper.GetControllerName(Controller)).Url + query;
            }

            public bool IsActive(ControllerBase controllerBase)
            {
                return controllerBase.GetType() == Controller;
            }            
        }

        public static IEnumerable<BusinessTripTab> GetTabs()
        {
            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.ItinereryTitle,
                Controller = typeof(BusinessTripItineraryController),
                ActionName = nameof(BusinessTripItineraryController.Index),
                RequiredRole = SecurityRoleType.CanViewBusinessTripItinerary,
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.ParticipantsTitle,
                Controller = typeof(BusinessTripParticipantController),
                ActionName = nameof(BusinessTripParticipantController.List),
                RequiredRole = SecurityRoleType.CanViewBusinessTripParticipants
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.ArrangementsTitle,
                Controller = typeof(BusinessTripArrangementsController),
                ActionName = nameof(BusinessTripArrangementsController.Index),
                RequiredRole = SecurityRoleType.CanManageBusinessTripArrangements
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.FrontDeskTitle,
                Controller = typeof(BusinessTripArrangementTrackingController),
                ActionName = nameof(BusinessTripArrangementTrackingController.Table),
                RequiredRole = SecurityRoleType.CanManageBusinessTripFrontDesk
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.CommunicationTitle,
                Controller = typeof(BusinessTripCommunicationController),
                ActionName = nameof(BusinessTripCommunicationController.List),
                RequiredRole = SecurityRoleType.CanViewBusinessTripCommunication
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.AdvancedPaymentsTitle,
                Controller = typeof(BusinessTripAdvancedPaymentController),
                ActionName = nameof(BusinessTripAdvancedPaymentController.List),
                RequiredRole = SecurityRoleType.CanViewBusinessTripAdvancedPayments
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.SettlementsTitle,
                Controller = typeof(BusinessTripSettlementsController),
                ActionName = nameof(BusinessTripSettlementsController.List),
                RequiredRole = SecurityRoleType.CanViewBusinessTripSettlements
            };

            yield return new BusinessTripTab
            {
                Name = BusinessTripResources.AcceptanceConditions,
                Controller = typeof(BusinessTripAcceptanceConditionsController),
                ActionName = nameof(BusinessTripSettlementsController.Edit),
                RequiredRole = SecurityRoleType.CanViewBusinessTripAcceptanceConditions
            };
        }
    }
}
