﻿using System;
using Smt.Atomic.Business.BusinessTrips.Interfaces;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class ArrangementDocumentSaveContract : IArrangementDocumentSaveContract
    {
        public long? DocumentId { get; set; }
        public Guid? TemporaryDocumentId { get; set; }
        public string ContentType { get; set; }
        public string DocumentName { get; set; }
    }
}