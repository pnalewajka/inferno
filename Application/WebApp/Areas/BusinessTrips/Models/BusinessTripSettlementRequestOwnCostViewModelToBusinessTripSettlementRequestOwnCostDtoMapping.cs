﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestOwnCostViewModelToBusinessTripSettlementRequestOwnCostDtoMapping : ClassMapping<BusinessTripSettlementRequestOwnCostViewModel, BusinessTripSettlementRequestOwnCostDto>
    {
        public BusinessTripSettlementRequestOwnCostViewModelToBusinessTripSettlementRequestOwnCostDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementRequestOwnCostDto
            {
                Description = v.Description,
                TransactionDate = v.TransactionDate,
                Amount = v.Amount,
                CurrencyId = v.CurrencyId,
                HasInvoiceIssued = v.HasInvoiceIssued,
            };
        }
    }
}
