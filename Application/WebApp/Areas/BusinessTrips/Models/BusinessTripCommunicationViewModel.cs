﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripCommunicationViewModel : IContextModel
    {
        public IContextSettings ContextSettings { get; set; }

        public object ContextViewModel { get; set; }

        public bool IsContextViewCollapsed { get; set; }

        public long CurrentEmployeeId { get; set; }

        public ICollection<BusinessTripMessageViewModel> Messages { get; set; }
    }
}
