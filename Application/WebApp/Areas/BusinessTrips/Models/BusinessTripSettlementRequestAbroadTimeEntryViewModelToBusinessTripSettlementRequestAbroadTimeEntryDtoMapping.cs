﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestAbroadTimeEntryViewModelToBusinessTripSettlementRequestAbroadTimeEntryDtoMapping : ClassMapping<BusinessTripSettlementRequestAbroadTimeEntryViewModel, BusinessTripSettlementRequestAbroadTimeEntryDto>
    {
        public BusinessTripSettlementRequestAbroadTimeEntryViewModelToBusinessTripSettlementRequestAbroadTimeEntryDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementRequestAbroadTimeEntryDto
            {
                DepartureDateTime = v.DepartureDateTime,
                DepartureCountryId = v.DepartureCountryId,
                DepartureCityId = v.DepartureCityId,
                DestinationType = v.DestinationType,
            };
        }
    }
}
