﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementSummaryDataSourceParametersViewModelToBusinessTripSettlementSummaryDataSourceParametersDtoMapping : ClassMapping<BusinessTripSettlementSummaryDataSourceParametersViewModel, BusinessTripSettlementSummaryDataSourceParametersDto>
    {
        public BusinessTripSettlementSummaryDataSourceParametersViewModelToBusinessTripSettlementSummaryDataSourceParametersDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementSummaryDataSourceParametersDto
            {
                Year = v.Year,
                Month = v.Month
            };
        }
    }
}
