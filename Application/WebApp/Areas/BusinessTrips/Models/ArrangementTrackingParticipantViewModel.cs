﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class SaveArrangementTrackingParticipantViewModel
    {
        public long ParticipantId { get; set; }

        public IDictionary<string, int> Statuses { get; set; }
    }
}