﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestViewModel
    {
        private const double MaxBusinessTripHours = 744d;
        private const string ControllerPath = "/Workflows/BusinessTripSettlementRequest/";

        public long Id { get; set; }

        [Required]
        [ValuePicker(Type = typeof(BusinessTripPickerController), OnResolveUrlJavaScript = "url += '&mode=only-finished'", DialogWidth = "1200px")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.BusinessTrip), typeof(BusinessTripSettlementRequestResources))]
        public long BusinessTripId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(BusinessTripParticipantPickerController),
            OnSelectionChangedJavaScript = "BusinessTripSettlementRequest.onParticipantChanged",
            OnResolveUrlJavaScript = "url += '&picker-filter=ManageSettlements&parent-id=' + $('[name=\"RequestObjectViewModel." + nameof(BusinessTripId) + "\"]').val()")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Participant), typeof(BusinessTripSettlementRequestResources))]
        public long BusinessTripParticipantId { get; set; }

        [ReadOnly(true)]
        [HiddenInput]
        public bool IsEmpty { get; set; }

        [HiddenInput]
        public long? DefaultCurrencyId { get; set; }

        [HiddenInput]
        public string DefaultCurrencyName { get; set; }

        [HiddenInput]
        public long? DefaultCardHolderEmployeeId { get; set; }

        [HiddenInput]
        public string DefaultCardHolderEmployeeName { get; set; }

        [HiddenInput]
        public DateTime? SettlementDate { get; set; }

        [TimePicker(30)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Departure), typeof(BusinessTripSettlementRequestResources))]
        [OnValueChange(
            ServerHandler = ControllerPath + nameof(BusinessTripSettlementRequestController.UpdateMealsTable),
            ServerResultClientHandler = "Settlements.updateMeals")]
        public DateTime? DepartureDateTime { get; set; }

        [Required]
        [Repeater(null, typeof(BusinessTripSettlementRequestResources), CanAddRecord = true, CanReorder = true, CanDeleteRecord = true, AddButtonText = nameof(BusinessTripSettlementRequestResources.AddTimeEntry), HideInReadOnlyWhenEmpty = true)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.TimeEntries), typeof(BusinessTripSettlementRequestResources))]
        [OnValueChange(
            ServerHandler = ControllerPath + nameof(BusinessTripSettlementRequestController.UpdateMealsTable),
            ServerResultClientHandler = "Settlements.updateMeals")]
        public List<BusinessTripSettlementRequestAbroadTimeEntryViewModel> AbroadTimeEntries { get; set; }

        [TimePicker(30)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Arrival), typeof(BusinessTripSettlementRequestResources))]
        [OnValueChange(
            ServerHandler = ControllerPath + nameof(BusinessTripSettlementRequestController.UpdateMealsTable),
            ServerResultClientHandler = "Settlements.updateMeals")]
        public DateTime? ArrivalDateTime { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.HoursWorkedForClient), typeof(BusinessTripSettlementRequestResources))]
        [Range(0d, MaxBusinessTripHours, ErrorMessageResourceType = typeof(BusinessTripSettlementRequestResources),ErrorMessageResourceName = nameof(BusinessTripSettlementRequestResources.HourLimitMustBeGreaterThanZero_ErrorMessage))]
        public decimal HoursWorkedForClient { get; set; }
        
        [Required]
        [CustomControl(TemplatePath = "Areas.BusinessTrips.Views.Settlements.Meals")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Meals), typeof(BusinessTripSettlementRequestResources))]
        public List<BusinessTripSettlementRequestMealViewModel> Meals { get; set; }

        [Required]
        [Repeater(CanAddRecord = true, CanReorder = true, CanDeleteRecord = true, HideInReadOnlyWhenEmpty = true)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.OwnCosts), typeof(BusinessTripSettlementRequestResources))]
        public List<BusinessTripSettlementRequestOwnCostViewModel> OwnCosts { get; set; }

        [Required]
        [Repeater(CanAddRecord = true, CanReorder = true, CanDeleteRecord = true, HideInReadOnlyWhenEmpty = true)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.CompanyCosts), typeof(BusinessTripSettlementRequestResources))]
        public List<BusinessTripSettlementRequestCompanyCostViewModel> CompanyCosts { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.AdditionalAdvancePayment), typeof(BusinessTripSettlementRequestResources))]
        [Repeater(CanAddRecord = true, CanReorder = true, CanDeleteRecord = true, HideInReadOnlyWhenEmpty = true)]
        public List<BusinessTripSettlementRequestAdvancePaymentViewModel> AdditionalAdvancePayments { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.PrivateVehicleMileage), typeof(BusinessTripSettlementRequestResources))]
        [Repeater(CanAddRecord = true, CanDeleteRecord = true, MinCount = 0, MaxCount = 1, HideInReadOnlyWhenEmpty = true)]
        public BusinessTripSettlementRequestVehicleMileageViewModel PrivateVehicleMileage { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.PrivateVehicleMileage), typeof(BusinessTripSettlementRequestResources))]
        [Repeater(CanAddRecord = true, CanDeleteRecord = true, MinCount = 0, MaxCount = 1, HideInReadOnlyWhenEmpty = true)]
        public BusinessTripSettlementRequestSimplifiedVehicleMileageViewModel SimplifiedPrivateVehicleMileage { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        [ReadOnly(true)] // Avoid ModelBinder
        public string CalculationResult { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        public BusinessTripSettlementControllingStatus ControllingStatus { get; set; }

        public BusinessTripSettlementRequestViewModel()
        {
            AbroadTimeEntries = new List<BusinessTripSettlementRequestAbroadTimeEntryViewModel>();
            Meals = new List<BusinessTripSettlementRequestMealViewModel>();
            OwnCosts = new List<BusinessTripSettlementRequestOwnCostViewModel>();
            CompanyCosts = new List<BusinessTripSettlementRequestCompanyCostViewModel>();
            AdditionalAdvancePayments = new List<BusinessTripSettlementRequestAdvancePaymentViewModel>();
        }
    }
}
