﻿using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripTabViewModel<TViewModel> : IContextModel
    {
        public BusinessTripTabViewModel(TViewModel model)
        {
            ViewModel = model;
        }

        public IContextSettings ContextSettings { get; set; }

        public object ContextViewModel { get; set; }

        public bool IsContextViewCollapsed { get; set; }

        public TViewModel ViewModel { get; protected set; }
    }
}