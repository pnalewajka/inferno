﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripAdvancedPaymentRequestViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripResources.Requestor), typeof(BusinessTripResources))]
        public string Requestor { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripResources.Participant), typeof(BusinessTripResources))]
        public string ParticipantFullName { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripResources.Status), typeof(BusinessTripResources))]
        public RequestStatus Status { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripResources.AmountLabel), typeof(BusinessTripResources))]
        public decimal Amount { get; set; }

        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.Currency), typeof(BusinessTripResources))]
        public long CurrencyId { get; set; }
    }
}
