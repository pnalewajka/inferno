﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripParticipantDtoToBusinessTripParticipantViewModelMapping : ClassMapping<BusinessTripParticipantDto, BusinessTripParticipantViewModel>
    {
        private readonly IEmployeeService _employeeService;

        public BusinessTripParticipantDtoToBusinessTripParticipantViewModelMapping(IEmployeeService employeeService)
        {
            _employeeService = employeeService;

            Mapping = d => new BusinessTripParticipantViewModel
            {
                Id = d.Id,
                BusinessTripId = d.BusinessTripId,
                EmployeeId = d.EmployeeId,
                DepartureCityId = d.DepartureCityId,
                StartedOn = d.StartedOn,
                EndedOn = d.EndedOn,
                TransportationPreferences = d.TransportationPreferences,
                AccommodationPreferences = d.AccommodationPreferences,
                DepartureCityTaxiVouchers = d.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = d.DestinationCityTaxiVouchers,
                LuggageDeclarations = d.LuggageDeclarations.Select(GetLuggageViewModel).ToList(),
                ItineraryDetails = d.ItineraryDetails,
                CompanyId = d.CompanyId,
                ContractTypeId = d.ContractTypeId,
                DateOfBirth = d.DateOfBirth,
                PersonalNumber = d.PersonalNumber,
                PersonalNumberType = d.PersonalNumberType,
                IdCardNumber = d.IdCardNumber,
                PhoneNumber = d.PhoneNumber,
                HomeAddress = d.HomeAddress,
                EmployeeName = d.EmployeeName,
                Timestamp = d.Timestamp
            };
        }

        private DeclaredLuggageViewModel GetLuggageViewModel(DeclaredLuggageDto declaredLuggageDto)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(declaredLuggageDto.EmployeeId);

            return new DeclaredLuggageViewModel
            {
                Id = declaredLuggageDto.Id,
                EmployeeId = declaredLuggageDto.EmployeeId,
                EmployeeFullName = employee?.FullName,
                HandLuggageItems = declaredLuggageDto.HandLuggageItems,
                RegisteredLuggageItems = declaredLuggageDto.RegisteredLuggageItems
            };
        }
    }
}
