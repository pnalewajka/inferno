﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripAdvancedPaymentRequestViewModelToBusinessTripAdvancedPaymentRequestDtoMapping : ClassMapping<BusinessTripAdvancedPaymentRequestViewModel, BusinessTripAdvancedPaymentRequestDto>
    {
        public BusinessTripAdvancedPaymentRequestViewModelToBusinessTripAdvancedPaymentRequestDtoMapping()
        {
            Mapping = v => new BusinessTripAdvancedPaymentRequestDto
            {
                Id = v.Id,
                Amount = v.Amount,
                CurrencyId = v.CurrencyId,
            };
        }
    }
}
