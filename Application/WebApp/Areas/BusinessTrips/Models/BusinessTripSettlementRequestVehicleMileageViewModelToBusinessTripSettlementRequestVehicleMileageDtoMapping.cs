﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestVehicleMileageViewModelToBusinessTripSettlementRequestVehicleMileageDtoMapping : ClassMapping<BusinessTripSettlementRequestVehicleMileageViewModel, BusinessTripSettlementRequestVehicleMileageDto>
    {
        public BusinessTripSettlementRequestVehicleMileageViewModelToBusinessTripSettlementRequestVehicleMileageDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementRequestVehicleMileageDto
            {
                VehicleType = v.VehicleType,
                IsCarEngineCapacityOver900cc = v.VehicleType == VehicleType.Car && v.IsCarEngineCapacityOver900cc,
                RegistrationNumber = v.RegistrationNumber,
                OutboundFromCityId = v.OutboundFromCityId,
                OutboundToCityId = v.OutboundToCityId,
                OutboundKilometers = v.PreferredDistanceUnit == MileageUnit.Kilometers ? v.OutboundKilometers : UnitHelper.ConvertMilesToKilometers(v.OutboundKilometers),
                InboundFromCityId = v.InboundFromCityId,
                InboundToCityId = v.InboundToCityId,
                InboundKilometers = v.PreferredDistanceUnit == MileageUnit.Kilometers ? v.InboundKilometers : UnitHelper.ConvertMilesToKilometers(v.InboundKilometers),
            };
        }
    }
}
