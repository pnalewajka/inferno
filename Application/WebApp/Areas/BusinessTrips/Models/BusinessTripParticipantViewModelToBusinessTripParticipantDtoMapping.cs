﻿using System.Linq;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripParticipantViewModelToBusinessTripParticipantDtoMapping : ClassMapping<BusinessTripParticipantViewModel, BusinessTripParticipantDto>
    {
        public BusinessTripParticipantViewModelToBusinessTripParticipantDtoMapping()
        {
            Mapping = v => new BusinessTripParticipantDto
            {
                Id = v.Id,
                BusinessTripId = v.BusinessTripId,
                EmployeeId = v.EmployeeId,
                StartedOn = v.StartedOn,
                EndedOn = v.EndedOn,
                DepartureCityId = v.DepartureCityId,
                TransportationPreferences = v.TransportationPreferences,
                AccommodationPreferences = v.AccommodationPreferences,
                DepartureCityTaxiVouchers = v.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = v.DestinationCityTaxiVouchers,
                LuggageDeclarations = v.LuggageDeclarations.Select(d => new DeclaredLuggageDto
                {
                    Id = d.Id,
                    EmployeeId = d.EmployeeId,
                    HandLuggageItems = d.HandLuggageItems,
                    RegisteredLuggageItems = d.RegisteredLuggageItems
                }).ToArray(),
                ItineraryDetails = v.ItineraryDetails,
                DateOfBirth = v.DateOfBirth,
                PersonalNumber = v.PersonalNumber,
                PersonalNumberType = v.PersonalNumberType,
                IdCardNumber = v.IdCardNumber,
                PhoneNumber = v.PhoneNumber,
                HomeAddress = v.HomeAddress,
                Timestamp = v.Timestamp
            };
        }
    }
}
