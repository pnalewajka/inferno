﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripViewModel
    {
        public long Id { get; set; }

        [Render(GridCssClass = "bt-identifier")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.Identifier), typeof(BusinessTripResources))]
        public string Identifier { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.ProjectsLabel), typeof(BusinessTripResources))]
        [Hub(nameof(ProjectIds))]
        public string[] ProjectNames { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(SimpleProjectPickerController))]
        [ReadOnly(true)]
        public long[] ProjectIds { get; set; }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.StartedOnLabel), typeof(BusinessTripResources))]
        public DateTime StartedOn { get; set; }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.EndedOnLabel), typeof(BusinessTripResources))]
        public DateTime EndedOn { get; set; }

        [Visibility(VisibilityScope.Grid)]
        public BusinessTripStatus Status { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [Tooltip(nameof(DepartureCityNames))]
        [DisplayNameLocalized(nameof(BusinessTripResources.DepartureCityLabel), typeof(BusinessTripResources))]
        public string DepartureCityNames { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [Tooltip(nameof(DestinationCityName))]
        [DisplayNameLocalized(nameof(BusinessTripResources.DestinationCityLabel), typeof(BusinessTripResources))]
        public string DestinationCityName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(BusinessTripResources.DateLabel), typeof(BusinessTripResources))]
        public string Date => $"{StartedOn:d} - {EndedOn:d}";

        [DisplayNameLocalized(nameof(BusinessTripResources.TravelExplanationLabel), typeof(BusinessTripResources))]
        [StringLength(2000)]
        [Ellipsis(MaxStringLength = 40)]
        [AllowHtml]
        [HtmlSanitize]
        [Visibility(VisibilityScope.Grid)]
        public string TravelExplanation { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [ReadOnly(true)]
        public long[] ParticipantEmployeeIds { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.ParticipantFullNamesLabel), typeof(BusinessTripResources))]
        [Hub(nameof(ParticipantEmployeeIds))]
        public string[] ParticipantFullNames { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tooltip(nameof(AcceptingPersonFullNames))]
        [DisplayNameLocalized(nameof(BusinessTripResources.AcceptingPersonFullNamesLabel), typeof(BusinessTripResources))]
        public string AcceptingPersonFullNames { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.FrontDeskAssigneeLabel), typeof(BusinessTripResources))]
        public long? FrontDeskAssigneeEmployeeId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ItineraryDetails { get; set; }

        [Render(Size = Size.Large)]
        [Visibility(VisibilityScope.Form)]
        [Tooltip(nameof(ItineraryDetailsText))]
        [DisplayNameLocalized(nameof(BusinessTripResources.ItineraryDetailsLabel), typeof(BusinessTripResources))]
        public string ItineraryDetailsText => string.Join(" ",
            StringHelper.BreakString(
                CrossCutting.Common.Helpers.HtmlHelper.StripHtmlTags(ItineraryDetails)).Where(l => !string.IsNullOrWhiteSpace(l)));

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.CompanyNames), typeof(BusinessTripResources))]
        public string CompanyNames { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.ReinvoicingToCustomer), typeof(BusinessTripResources))]
        public bool ReinvoicingToCustomer { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [SettlementsCell]
        [DisplayNameLocalized(nameof(BusinessTripResources.SettlementsTitle), typeof(BusinessTripResources))]
        public BusinessTripSettlementViewModel[] Settlements { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public string DefaultDoubleClickRowUrl { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.AttachmentsLabel), typeof(BusinessTripResources))]
        [DocumentUpload(typeof(ArrangementDocumentMapping))]
        [DocumentList]
        [NonSortable]
        public DocumentViewModel[] Attachments { get; set; }

        [Visibility(VisibilityScope.None)]
        public BusinessTripParticipantViewModel[] Participants { get; set; }

        public override string ToString()
        {
            var lengthInDays = (EndedOn - StartedOn).Days + 1;

            return string.Format(lengthInDays == 1
                ? BusinessTripResources.BusinessTripViewModelOneDayTripFormat
                : BusinessTripResources.BusinessTripViewModelManyDaysTripFormat,
                DestinationCityName,
                StartedOn.ToShortDateString(),
                lengthInDays);
        }
    }
}
