﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [Identifier("Fitlers.DailyAllowanceValidForFilter")]
    [DescriptionLocalized(nameof(DailyAllowanceResources.ValidFor), typeof(DailyAllowanceResources))]
    public class DailyAllowanceValidForFilterViewModel
    {
        [Required]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.FilterValidForDate), typeof(DailyAllowanceResources))]
        public DateTime ValidFor { get; set; }

        public override string ToString()
        {
            return ValidFor.ToShortDateString();
        }
    }
}