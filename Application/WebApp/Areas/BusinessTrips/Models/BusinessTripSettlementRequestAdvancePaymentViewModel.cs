﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestAdvancePaymentViewModel
    {
        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.Currency), typeof(BusinessTripResources))]
        public long CurrencyId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripResources.AmountLabel), typeof(BusinessTripResources))]
        public decimal Amount { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripResources.WithdrawnOnLabel), typeof(BusinessTripResources))]
        public DateTime WithdrawnOn { get; set; }
    }
}
