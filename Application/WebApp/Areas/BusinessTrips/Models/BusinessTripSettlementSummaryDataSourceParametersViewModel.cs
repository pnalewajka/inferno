﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [Identifier("Models.BusinessTripSettlementSummaryReportParameters")]
    public class BusinessTripSettlementSummaryDataSourceParametersViewModel
    {
        [DisplayNameLocalized(nameof(ReportsResources.YearLabel), typeof(ReportsResources))]
        public int Year { get; set; }

        [MinValue(1), MaxValue(12)]
        [DisplayNameLocalized(nameof(ReportsResources.MonthLabel), typeof(ReportsResources))]
        public int Month { get; set; }
    }
}
