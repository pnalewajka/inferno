﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripAcceptanceConditionsDtoToBusinessTripAcceptanceConditionsViewModelMapping : ClassMapping<BusinessTripAcceptanceConditionsDto, BusinessTripAcceptanceConditionsViewModel>
    {
        public BusinessTripAcceptanceConditionsDtoToBusinessTripAcceptanceConditionsViewModelMapping()
        {
            Mapping = d => new BusinessTripAcceptanceConditionsViewModel
            {
                Id = d.Id,
                ReinvoicingToCustomer = d.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = d.ReinvoiceCurrencyId,
                CostApprovalPolicy = d.CostApprovalPolicy,
                MaxCostsCurrencyId = d.MaxCostsCurrencyId,
                MaxHotelCosts = d.MaxHotelCosts,
                MaxTotalFlightsCosts = d.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = d.MaxOtherTravelCosts
            };
        }
    }
}
