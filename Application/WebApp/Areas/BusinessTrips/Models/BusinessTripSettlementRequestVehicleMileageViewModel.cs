﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [FieldSetDescription(1, "vehicle", nameof(BusinessTripSettlementRequestResources.VehicleInformation), typeof(BusinessTripSettlementRequestResources))]
    [FieldSetDescription(2, "outbound", nameof(BusinessTripSettlementRequestResources.Outbound), typeof(BusinessTripSettlementRequestResources))]
    [FieldSetDescription(3, "inbound", nameof(BusinessTripSettlementRequestResources.Inbound), typeof(BusinessTripSettlementRequestResources))]
    [FieldSetDescription(4, "total", nameof(BusinessTripSettlementRequestResources.Total), typeof(BusinessTripSettlementRequestResources))]
    public class BusinessTripSettlementRequestVehicleMileageViewModel
    {
        public BusinessTripSettlementRequestVehicleMileageViewModel()
        {
            IsCarEngineCapacityOver900cc = true;
            PreferredDistanceUnit = MileageUnit.Kilometers;
        }

        [Required]
        [FieldSet("vehicle")]
        [OnValueChange(ClientHandler = "BusinessTripSettlementRequest.onVehicleTypeChanged")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.VehicleType), typeof(BusinessTripSettlementRequestResources))]
        public VehicleType VehicleType { get; set; }

        [FieldSet("vehicle")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.IsCarEngineCapacityOver900cc), typeof(BusinessTripSettlementRequestResources))]
        public bool IsCarEngineCapacityOver900cc { get; set; }

        [Required]
        [FieldSet("vehicle")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.RegistrationNumber), typeof(BusinessTripSettlementRequestResources))]
        public string RegistrationNumber { get; set; }

        [Required]
        [FieldSet("vehicle")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.MileageUnit), typeof(BusinessTripSettlementRequestResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<MileageUnit>))]
        public MileageUnit? PreferredDistanceUnit { get; set; }

        [Required]
        [FieldSet("outbound")]
        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.FromCity), typeof(BusinessTripSettlementRequestResources))]
        public long OutboundFromCityId { get; set; }

        [Required]
        [FieldSet("outbound")]
        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.ToCity), typeof(BusinessTripSettlementRequestResources))]
        public long OutboundToCityId { get; set; }

        [Required]
        [FieldSet("outbound")]
        [OnValueChange(ClientHandler = "BusinessTripSettlementRequest.onPrivateVehicleMileageKilometersChange")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Mileage), typeof(BusinessTripSettlementRequestResources))]
        [Range(0, 100000)]
        public decimal OutboundKilometers { get; set; }

        [Required]
        [FieldSet("inbound")]
        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.FromCity), typeof(BusinessTripSettlementRequestResources))]
        public long InboundFromCityId { get; set; }

        [Required]
        [FieldSet("inbound")]
        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.ToCity), typeof(BusinessTripSettlementRequestResources))]
        public long InboundToCityId { get; set; }

        [Required]
        [FieldSet("inbound")]
        [OnValueChange(ClientHandler = "BusinessTripSettlementRequest.onPrivateVehicleMileageKilometersChange")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Mileage), typeof(BusinessTripSettlementRequestResources))]
        [Range(0, 100000)]
        public decimal InboundKilometers { get; set; }

        [FieldSet("total")]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Mileage), typeof(BusinessTripSettlementRequestResources))]
        public decimal TotalKilometers => OutboundKilometers + InboundKilometers;
    }
}
