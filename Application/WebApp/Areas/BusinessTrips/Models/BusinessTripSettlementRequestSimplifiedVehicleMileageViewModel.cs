﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestSimplifiedVehicleMileageViewModel
    {
        [Required]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.MileageUnit), typeof(BusinessTripSettlementRequestResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<MileageUnit>))]
        public MileageUnit PreferredDistanceUnit { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Mileage), typeof(BusinessTripSettlementRequestResources))]
        [Range(0, 100000)]
        public decimal Kilometers { get; set; }
    }
}
