﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [Identifier("Filters.FrontDeskAsigneeFilter")]
    [DescriptionLocalized(nameof(BusinessTripResources.FrontDeskAsigneeFilterDialogTitle), typeof(BusinessTripResources))]
    public class BusinessTripFrontDeskAssigneeFilterViewModel
    {
        [DisplayNameLocalized(nameof(BusinessTripResources.FrontDeskAssigneeLabel), typeof(BusinessTripResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanAssignFrontDeskResponsibility) + "'")]
        [Render(Size = Size.Large)]
        public long EmployeeId { get; set; }

        public override string ToString()
        {
            return BusinessTripResources.FrontDeskAssigneeLabel;
        }
    }
}