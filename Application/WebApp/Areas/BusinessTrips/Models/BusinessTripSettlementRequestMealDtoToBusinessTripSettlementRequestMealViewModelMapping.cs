﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestMealDtoToBusinessTripSettlementRequestMealViewModelMapping : ClassMapping<BusinessTripSettlementRequestMealDto, BusinessTripSettlementRequestMealViewModel>
    {
        public BusinessTripSettlementRequestMealDtoToBusinessTripSettlementRequestMealViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestMealViewModel
            {
                CountryId = d.CountryId,
                CountryName = d.CountryName,
                Day = d.Day,
                BreakfastCount = d.BreakfastCount,
                LunchCount = d.LunchCount,
                DinnerCount = d.DinnerCount,
            };
        }
    }
}
