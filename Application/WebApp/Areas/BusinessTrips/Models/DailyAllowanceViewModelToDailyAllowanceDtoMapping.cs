﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class DailyAllowanceViewModelToDailyAllowanceDtoMapping : ClassMapping<DailyAllowanceViewModel, DailyAllowanceDto>
    {
        public DailyAllowanceViewModelToDailyAllowanceDtoMapping()
        {
            Mapping = v => new DailyAllowanceDto
            {
                Id = v.Id,
                ValidFrom = v.ValidFrom,
                ValidTo = v.ValidTo,
                EmploymentCountryId = v.EmploymentCountryId,
                TravelCityId = v.TravelCityId,
                TravelCountryId = v.TravelCountryId,
                CurrencyId = v.CurrencyId,
                Allowance = v.Allowance,
                DepartureAndArrivalDayAllowance = v.DepartureAndArrivalDayAllowance,
                AccommodationLumpSum = v.AccommodationLumpSum,
                Timestamp = v.Timestamp
            };
        }
    }
}
