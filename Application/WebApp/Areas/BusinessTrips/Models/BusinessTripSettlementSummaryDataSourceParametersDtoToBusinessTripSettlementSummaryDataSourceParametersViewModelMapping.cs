﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementSummaryDataSourceParametersDtoToBusinessTripSettlementSummaryDataSourceParametersViewModelMapping : ClassMapping<BusinessTripSettlementSummaryDataSourceParametersDto, BusinessTripSettlementSummaryDataSourceParametersViewModel>
    {
        public BusinessTripSettlementSummaryDataSourceParametersDtoToBusinessTripSettlementSummaryDataSourceParametersViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementSummaryDataSourceParametersViewModel
            {
                Year = d.Year,
                Month = d.Month
            };
        }
    }
}
