﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [TabDescription(0, "employee", nameof(BusinessTripResources.ParticipantTabName), typeof(BusinessTripResources))]
    [TabDescription(1, "itinerary", nameof(BusinessTripResources.ItineraryTabName), typeof(BusinessTripResources))]
    public class BusinessTripParticipantViewModel : BusinessTripDisplayParticipantViewModel
    {
        private const string BusinessTripParticipantControllerPath = "/BusinessTrips/BusinessTripParticipant/";

        [Tab("employee")]
        [OnValueChange(ServerHandler = BusinessTripParticipantControllerPath + nameof(BusinessTripParticipantController.UpdateEmployeePersonalInformation))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=not-terminated-employees'", OnSelectionChangedJavaScript = "Luggage.onParticipantChanged($('[data-field-name=\"EmployeeId\"]'));")]
        [DisplayNameLocalized(nameof(BusinessTripResources.EmployeeLabel), typeof(BusinessTripResources))]
        public override long EmployeeId { get; set; }

        [Tab("employee")]
        [NonSortable]
        [ReadOnly(true)]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.CompanyLabel), typeof(BusinessTripResources))]
        public long? CompanyId { get; set; }

        [Tab("employee")]
        [NonSortable]
        [ReadOnly(true)]
        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.ContractTypeLabel), typeof(BusinessTripResources))]
        public long? ContractTypeId { get; set; }
        
        [Tab("employee")]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(BusinessTripResources.DateOfBirthLabel), typeof(BusinessTripResources))]
        [RoleRequired(SecurityRoleType.CanManageBusinessTripParticipantPersonalData)]
        public DateTime? DateOfBirth { get; set; }

        [Tab("employee")]
        [StringLength(32)]
        [DisplayNameLocalized(nameof(BusinessTripResources.PersonalNumberLabel), typeof(BusinessTripResources))]
        [RoleRequired(SecurityRoleType.CanManageBusinessTripParticipantPersonalData)]
        public string PersonalNumber { get; set; }

        [Tab("employee")]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<PersonalNumberType>))]
        [DisplayNameLocalized(nameof(BusinessTripResources.PersonalNumberTypeLabel), typeof(BusinessTripResources))]
        [RoleRequired(SecurityRoleType.CanManageBusinessTripParticipantPersonalData)]
        [Visibility(VisibilityScope.Form)]
        public PersonalNumberType? PersonalNumberType { get; set; }

        [Tab("employee")]
        [StringLength(32)]
        [DisplayNameLocalized(nameof(BusinessTripResources.IdCardNumberLabel), typeof(BusinessTripResources))]
        [RoleRequired(SecurityRoleType.CanManageBusinessTripParticipantPersonalData)]
        public string IdCardNumber { get; set; }

        [Tab("employee")]
        [StringLength(32)]
        [DisplayNameLocalized(nameof(BusinessTripResources.PhoneNumberLabel), typeof(BusinessTripResources))]
        [RoleRequired(SecurityRoleType.CanManageBusinessTripParticipantPersonalData)]
        public string PhoneNumber { get; set; }

        [Tab("employee")]
        [Multiline(3)]
        [StringLength(128)]
        [DisplayNameLocalized(nameof(BusinessTripResources.HomeAddressLabel), typeof(BusinessTripResources))]
        [RoleRequired(SecurityRoleType.CanManageBusinessTripParticipantPersonalData)]
        public string HomeAddress { get; set; }

        [Tab("itinerary")]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(BusinessTripResources.DepartureCityLabel), typeof(BusinessTripResources))]
        [ValuePicker(Type = typeof(CityPickerController))]
        [OnValueChange(ServerHandler = BusinessTripParticipantControllerPath + nameof(BusinessTripParticipantController.UpdateLuggageAndTaxiVoucherAvailability))]
        public long DepartureCityId { get; set; }

        [Tab("itinerary")]
        [DisplayNameLocalized(nameof(BusinessTripResources.StartedOnLabel), typeof(BusinessTripResources))]
        [Visibility(VisibilityScope.Form)]
        public DateTime StartedOn { get; set; }

        [Tab("itinerary")]
        [DisplayNameLocalized(nameof(BusinessTripResources.EndedOnLabel), typeof(BusinessTripResources))]
        [Visibility(VisibilityScope.Form)]
        public DateTime EndedOn { get; set; }

        [Tab("itinerary")]
        [OnValueChange(ServerHandler = BusinessTripParticipantControllerPath + nameof(BusinessTripParticipantController.UpdateLuggageAndTaxiVoucherAvailability))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<MeansOfTransport>))]
        [DisplayNameLocalized(nameof(BusinessTripResources.TransportationPreferencesLabel), typeof(BusinessTripResources))]
        [Visibility(VisibilityScope.Form)]
        public MeansOfTransport TransportationPreferences { get; set; }

        [Tab("itinerary")]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<AccommodationType>))]
        [DisplayNameLocalized(nameof(BusinessTripResources.AccommodationPreferencesLabel), typeof(BusinessTripResources))]
        [Visibility(VisibilityScope.Form)]
        public AccommodationType AccommodationPreferences { get; set; }

        [Tab("itinerary")]
        [MinValue(0)]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(BusinessTripResources.DepartureCityTaxiVouchersLabel), typeof(BusinessTripResources))]
        public int DepartureCityTaxiVouchers { get; set; }

        [Tab("itinerary")]
        [MinValue(0)]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(BusinessTripResources.DestinationCityTaxiVouchersLabel), typeof(BusinessTripResources))]
        public int DestinationCityTaxiVouchers { get; set; }

        [Tab("itinerary")]
        [CustomControl(TemplatePath = "Areas.BusinessTrips.Views.Shared.Luggage")]
        [Visibility(VisibilityScope.Form)]
        public List<DeclaredLuggageViewModel> LuggageDeclarations { get; set; }

        [Tab("itinerary")]
        [AllowHtml]
        [MaxLength(4096)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(BusinessTripResources.ItineraryDetailsLabel), typeof(BusinessTripResources))]
        [RichText(RichTextEditorPresets.Standard)]
        public string ItineraryDetails { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long BusinessTripId { get; set; }

        [Visibility(VisibilityScope.None)]
        public override string EmployeeName { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return EmployeeName;
        }
    }
}
