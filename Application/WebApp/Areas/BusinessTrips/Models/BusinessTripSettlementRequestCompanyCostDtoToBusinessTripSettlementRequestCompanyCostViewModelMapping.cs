﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestCompanyCostDtoToBusinessTripSettlementRequestCompanyCostViewModelMapping : ClassMapping<BusinessTripSettlementRequestCompanyCostDto, BusinessTripSettlementRequestCompanyCostViewModel>
    {
        public BusinessTripSettlementRequestCompanyCostDtoToBusinessTripSettlementRequestCompanyCostViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestCompanyCostViewModel
            {
                Description = d.Description,
                TransactionDate = d.TransactionDate,
                CurrencyId = d.CurrencyId,
                Amount = d.Amount,
                PaymentMethod = d.PaymentMethod,
                CardHolderEmployeeId = d.CardHolderEmployeeId,
                HasInvoiceIssued = d.HasInvoiceIssued,
            };
        }
    }
}
