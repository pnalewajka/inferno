﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripArrangementTrackingViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long BusinessTripId { get; set; }

        [AllowHtml]
        [MaxLength(4096)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.ArrangementTrackingRemarks), typeof(BusinessTripRequestResources))]
        [RichText(mentionJavaScriptResolver: "Dante.Mentions.EmployeeMentionValuePicker()")]
        public string Remarks { get; set; }

        [Visibility(VisibilityScope.None)]
        public ArrangementTrackingParticipantDto[] Participants { get; set; }
    }
}