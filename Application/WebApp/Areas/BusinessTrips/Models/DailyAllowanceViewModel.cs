﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class DailyAllowanceViewModel : IValidatableObject
    { 
        public long Id { get; set; }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.ValidFrom), typeof(DailyAllowanceResources))]
        public DateTime? ValidFrom { get; set; }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.ValidTo), typeof(DailyAllowanceResources))]
        public DateTime? ValidTo { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CountryPickerController))]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.EmploymentCountry), typeof(DailyAllowanceResources))]
        public long EmploymentCountryId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmploymentCountryName { get; set; }

        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.TravelCity), typeof(DailyAllowanceResources))]
        public long? TravelCityId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string TravelCityName { get; set; }

        [ValuePicker(Type = typeof(CountryPickerController))]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.TravelCountry), typeof(DailyAllowanceResources))]
        public long? TravelCountryId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string TravelCountryName { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.Currency), typeof(DailyAllowanceResources))]
        public long CurrencyId { get; set; }

        [Required]
        [MinValue(1)]
        [Render(Size = Size.Small)]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.Allowance), typeof(DailyAllowanceResources))]
        public decimal Allowance { get; set; }

        [Render(Size = Size.Small)]
        [Hint(nameof(DailyAllowanceResources.DepartureAndArrivalDayAllowanceHint), typeof(DailyAllowanceResources))]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.DepartureAndArrivalDayAllowance), typeof(DailyAllowanceResources))]
        public decimal? DepartureAndArrivalDayAllowance { get; set; }

        [Required]
        [MinValue(1)]
        [Render(Size = Size.Small)]
        [DisplayNameLocalized(nameof(DailyAllowanceResources.AccommodationLumpSum), typeof(DailyAllowanceResources))]
        public decimal AccommodationLumpSum { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // Exactly one of the fields must have a value
            if (!(TravelCityId.HasValue ^ TravelCountryId.HasValue))
            {
                yield return new ValidationResult(DailyAllowanceResources.CityOrCountryRequiredErrorMessage,
                    new[] { nameof(TravelCityId), nameof(TravelCountryId) });
            }

            if (ValidFrom == null && ValidTo == null)
            {
                yield return new ValidationResult(DailyAllowanceResources.NoDateEnteredErrorMessage,
                    new[] {ValidFrom == null ? nameof(ValidFrom) : nameof(ValidTo)});
            }
            if (ValidFrom > ValidTo)
            {
                yield return new ValidationResult(DailyAllowanceResources.ValidFromBiggerThanValidToErrorMessage,
                    new[] { ValidFrom == null ? nameof(ValidFrom) : nameof(ValidTo) });
            }
            
            yield return ValidationResult.Success;
        }

        public override string ToString()
        {
            if (ValidTo.HasValue)
            {
                return string.Format(DailyAllowanceResources.EntityDescriptionWithBothDates,
                    EmploymentCountryName, TravelCountryName ?? TravelCityName, ValidFrom, ValidTo);
            }

            return string.Format(DailyAllowanceResources.EntityDescriptionWithOneDate,
                EmploymentCountryName, TravelCountryName ?? TravelCityName, ValidFrom);
        }
    }
}
