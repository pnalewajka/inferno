﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestCompanyCostViewModelToBusinessTripSettlementRequestCompanyCostDtoMapping : ClassMapping<BusinessTripSettlementRequestCompanyCostViewModel, BusinessTripSettlementRequestCompanyCostDto>
    {
        public BusinessTripSettlementRequestCompanyCostViewModelToBusinessTripSettlementRequestCompanyCostDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementRequestCompanyCostDto
            {
                Description = v.Description,
                TransactionDate = v.TransactionDate,
                CurrencyId = v.CurrencyId,
                Amount = v.Amount,
                PaymentMethod = v.PaymentMethod,
                CardHolderEmployeeId = v.CardHolderEmployeeId,
                HasInvoiceIssued = v.HasInvoiceIssued,
            };
        }
    }
}
