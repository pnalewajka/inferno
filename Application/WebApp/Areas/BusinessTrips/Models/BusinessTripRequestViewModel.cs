﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripRequestViewModel : IValidatableObject
    {
        private const string BusinessTripRequestControllerPath = "/Workflows/BusinessTripRequest/";

        public BusinessTripRequestViewModel()
        {
            LuggageDeclarations = new List<DeclaredLuggageViewModel>();
        }

        public long Id { get; set; }

        [UpdatesApprovers]
        [Required]
        [MinLength(1, ErrorMessageResourceName = nameof(BusinessTripRequestResources.ProjectRequiredErrorMessage), ErrorMessageResourceType = typeof(BusinessTripRequestResources))]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.ProjectsLabel), typeof(BusinessTripRequestResources))]
        [ValuePicker(Type = typeof(SimpleProjectPickerController), OnResolveUrlJavaScript = "url += '&project-status=fixed&only-active-projects=true&only-projects-with-key=true'")]
        public long[] ProjectIds { get; set; }

        [Required]
        [UpdatesApprovers(ServerResultClientHandler = "BusinessTripRequest.updateApproversDeparturesAndVouchers")]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.ParticipantsLabel), typeof(BusinessTripRequestResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=not-terminated-employees'", OnSelectionChangedJavaScript = "Luggage.onParticipantsChanged($('[data-field-name=\"RequestObjectViewModel.ParticipantEmployeeIds\"]'));")]
        public long[] ParticipantEmployeeIds { get; set; }

        [HiddenInput]
        [OnValueChange(ServerHandler = BusinessTripRequestControllerPath + nameof(BusinessTripRequestController.UpdateDepartureCities))]
        public bool ForceDepartureCitiesRecalculation { get; set; } = false;

        [Required]
        [DatePicker(EmptyIfDefault = true)]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.StartedOnLabel), typeof(BusinessTripRequestResources))]
        [OnValueChange(ServerHandler = BusinessTripRequestControllerPath + nameof(BusinessTripRequestController.UpdateEndedOn))]
        public DateTime StartedOn { get; set; }

        [Required]
        [DatePicker(EmptyIfDefault = true)]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.EndedOnLabel), typeof(BusinessTripRequestResources))]
        public DateTime EndedOn { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.DepartureCityLabel), typeof(BusinessTripRequestResources))]
        [ValuePicker(Type = typeof(CityPickerController), OnResolveUrlJavaScript = "url += '&view=" + CityPickerController.BusinessTripsViewCode + "'", DialogWidth = "800px")]
        [OnValueChange(ServerHandler = BusinessTripRequestControllerPath + nameof(BusinessTripRequestController.UpdateTaxiVoucherAvailability))]
        public long[] DepartureCityIds { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.DestinationCityLabel), typeof(BusinessTripRequestResources))]
        [ValuePicker(Type = typeof(CityPickerController), OnResolveUrlJavaScript = "url += '&view=" + CityPickerController.BusinessTripsViewCode + "'", DialogWidth = "800px")]
        [OnValueChange(ServerHandler = BusinessTripRequestControllerPath + nameof(BusinessTripRequestController.UpdateTaxiVoucherAvailability))]
        public long DestinationCityId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.TravelExplanationLabel), typeof(BusinessTripRequestResources))]
        [StringLength(2000)]
        [Multiline]
        [AllowHtml]
        [HtmlSanitize]
        public string TravelExplanation { get; set; }

        [Hint(nameof(BusinessTripRequestResources.AreTravelArrangementsNeededHint), typeof(BusinessTripRequestResources))]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.AreTravelArrangementsNeededLabel), typeof(BusinessTripRequestResources))]
        [OnValueChange(ClientHandler = "BusinessTripRequest.toggleArrangements")]
        public bool AreTravelArrangementsNeeded { get; set; }

        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<MeansOfTransport>))]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.TransportationPreferencesLabel), typeof(BusinessTripRequestResources))]
        [OnValueChange(ServerHandler = BusinessTripRequestControllerPath + nameof(BusinessTripRequestController.UpdateLuggageAndTaxiVoucherAvailability))]
        public MeansOfTransport TransportationPreferences { get; set; }

        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<AccommodationType>))]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.AccommodationPreferencesLabel), typeof(BusinessTripRequestResources))]
        public AccommodationType AccommodationPreferences { get; set; }

        [MinValue(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.DepartureCityTaxiVouchersLabel), typeof(BusinessTripRequestResources))]
        public int DepartureCityTaxiVouchers { get; set; }

        [MinValue(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.DestinationCityTaxiVouchersLabel), typeof(BusinessTripRequestResources))]
        public int DestinationCityTaxiVouchers { get; set; }

        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.BusinessTrips.Views.Shared.Luggage")]
        public List<DeclaredLuggageViewModel> LuggageDeclarations { get; set; }

        [AllowHtml]
        [MaxLength(4096)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(BusinessTripRequestResources.ItineraryDetailsLabel), typeof(BusinessTripRequestResources))]
        [Prompt(nameof(BusinessTripRequestResources.ItineraryDetailsPlaceholder), typeof(BusinessTripRequestResources))]
        [RichText(RichTextEditorPresets.Standard)]
        public string ItineraryDetails { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ParticipantEmployeeIds.Any())
            {
                yield return new ValidationResult(BusinessTripRequestResources.ParticipantsEmptyError, new[] { nameof(ParticipantEmployeeIds) });
            }

            if (StartedOn > EndedOn)
            {
                yield return new ValidationResult(BusinessTripRequestResources.StartedOnGreaterThanEndedOnError, new[] { nameof(StartedOn) });
            }

            if (TransportationPreferences != default(MeansOfTransport) && string.IsNullOrWhiteSpace(ItineraryDetails))
            {
                var errorMessage = GetMissingItineraryDetailsMessage();

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    yield return new ValidationResult(errorMessage, new[] { nameof(ItineraryDetails) });
                }
            }

            if ((TransportationPreferences.HasFlag(MeansOfTransport.Other) || AccommodationPreferences.HasFlag(AccommodationType.Other))
                && string.IsNullOrEmpty(ItineraryDetails))
            {
                yield return new ValidationResult(BusinessTripRequestResources.ItineraryDetailsEmptyError, new[] { nameof(ItineraryDetails) });
            }
        }

        private string GetMissingItineraryDetailsMessage()
        {
            var atLeastTwoFlagSelected = (TransportationPreferences & (TransportationPreferences - 1)) != 0;
            if (atLeastTwoFlagSelected)
            {
                return BusinessTripRequestResources.ManyTransportsScheduleRequiredError;
            }

            if (TransportationPreferences.HasFlag(MeansOfTransport.Train))
            {
                return BusinessTripRequestResources.TrainScheduleRequiredError;
            }

            if (TransportationPreferences.HasFlag(MeansOfTransport.Plane))
            {
                return BusinessTripRequestResources.PlaneScheduleRequiredError;
            }

            if (TransportationPreferences.HasFlag(MeansOfTransport.Ship))
            {
                return BusinessTripRequestResources.ShipScheduleRequiredError;
            }

            return string.Empty;
        }
    }
}
