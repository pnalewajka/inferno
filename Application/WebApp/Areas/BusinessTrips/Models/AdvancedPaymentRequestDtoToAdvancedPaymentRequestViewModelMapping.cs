﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class AdvancedPaymentRequestDtoToAdvancedPaymentRequestViewModelMapping : ClassMapping<AdvancedPaymentRequestDto, AdvancedPaymentRequestViewModel>
    {
        public AdvancedPaymentRequestDtoToAdvancedPaymentRequestViewModelMapping()
        {
            Mapping = d => new AdvancedPaymentRequestViewModel
            {
                Id = d.Id,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                CurrencyDisplayName = d.CurrencyDisplayName,
                BusinessTripId = d.BusinessTripId,
                ParticipantId = d.ParticipantId,
            };
        }
    }
}
