﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestAbroadTimeEntryViewModel
    {
        [Required]
        [TimePicker(30)]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.BorderCrossingDate), typeof(BusinessTripSettlementRequestResources))]
        public DateTime DepartureDateTime { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CountryPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Country), typeof(BusinessTripSettlementRequestResources))]
        public long DepartureCountryId { get; set; }

        [ValuePicker(Type = typeof(CityPickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $(control).parents('.repeater-item').first().find('[name$=\"" + nameof(DepartureCountryId) + "\"]').val()")]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.City), typeof(BusinessTripSettlementRequestResources))]
        public long? DepartureCityId { get; set; }

        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(EnumBasedListItemProvider<BusinessTripDestinationType>))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.DestinationType), typeof(BusinessTripSettlementRequestResources))]
        public BusinessTripDestinationType? DestinationType { get; set; }
    }
}
