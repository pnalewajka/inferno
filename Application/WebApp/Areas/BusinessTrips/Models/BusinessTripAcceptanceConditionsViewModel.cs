﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [FieldSetDescription(1, "standard", nameof(ProjectResources.StandardFieldset), typeof(ProjectResources))]
    [FieldSetDescription(2, "reinvoice", nameof(ProjectResources.ReinvoiceFieldset), typeof(ProjectResources))]
    public class BusinessTripAcceptanceConditionsViewModel
    {
        public long Id { get; set; }

        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.ReinvoiceCurrency), typeof(ProjectResources))]
        [FieldSet("reinvoice")]
        public long? ReinvoiceCurrencyId { get; set; }

        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.MaxCostsCurrency), typeof(ProjectResources))]
        [FieldSet("standard")]
        public long? MaxCostsCurrencyId { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.CostApprovalPolicy), typeof(ProjectResources))]
        [FieldSet("standard")]
        [OnValueChange(ClientHandler = "AcceptanceConditionsHandler.updateAcceptanceFunctions")]
        public CostApprovalPolicy CostApprovalPolicy { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.MaxHotelCosts), typeof(ProjectResources))]
        [FieldSet("standard")]
        public decimal? MaxHotelCosts { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.MaxTotalFlightsCosts), typeof(ProjectResources))]
        [FieldSet("standard")]
        public decimal? MaxTotalFlightsCosts { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.MaxOtherTravelCosts), typeof(ProjectResources))]
        [FieldSet("standard")]
        public decimal? MaxOtherTravelCosts { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.ReinvoicingToCustomer), typeof(ProjectResources))]
        [FieldSet("reinvoice")]
        [OnValueChange(ClientHandler = "AcceptanceConditionsHandler.updateAcceptanceFunctions")]
        public bool ReinvoicingToCustomer { get; set; }
    }
}
