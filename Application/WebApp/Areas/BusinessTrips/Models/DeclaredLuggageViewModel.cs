﻿namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class DeclaredLuggageViewModel
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFullName { get; set; }

        public int HandLuggageItems { get; set; }

        public int RegisteredLuggageItems { get; set; }
    }
}
