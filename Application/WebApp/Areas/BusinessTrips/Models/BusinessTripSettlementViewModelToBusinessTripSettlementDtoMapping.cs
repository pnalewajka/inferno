﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementViewModelToBusinessTripSettlementDtoMapping : ClassMapping<BusinessTripSettlementViewModel, BusinessTripSettlementDto>
    {
        public BusinessTripSettlementViewModelToBusinessTripSettlementDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementDto
            {
            };
        }
    }
}
