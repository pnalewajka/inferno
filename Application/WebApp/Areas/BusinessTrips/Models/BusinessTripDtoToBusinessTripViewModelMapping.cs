﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Helpers;
using static Smt.Atomic.WebApp.Areas.BusinessTrips.Helpers.BusinessTripNavigationHelper;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripDtoToBusinessTripViewModelMapping : ClassMapping<BusinessTripDto, BusinessTripViewModel>
    {
        private readonly IPrincipalProvider _principalProvider;

        public BusinessTripDtoToBusinessTripViewModelMapping(
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider)
        {
            var settlementMapping = classMappingFactory.CreateMapping<BusinessTripSettlementDto, BusinessTripSettlementViewModel>();
            var participantMapping = classMappingFactory.CreateMapping<BusinessTripParticipantDto, BusinessTripParticipantViewModel>();

            _principalProvider = principalProvider;

            Mapping = d => new BusinessTripViewModel
            {
                Id = d.Id,
                Identifier = d.Identifier,
                ProjectNames = d.ProjectNames,
                ProjectIds = d.ProjectIds,
                StartedOn = d.StartedOn,
                EndedOn = d.EndedOn,
                Status = d.Status,
                TravelExplanation = d.TravelExplanation,
                DepartureCityNames = string.Join(", ", d.DepartureCityNames),
                DestinationCityName = d.DestinationCityName,
                ItineraryDetails = d.ItineraryDetails,
                FrontDeskAssigneeEmployeeId = d.FrontDeskAssigneeEmployeeId,
                AcceptingPersonFullNames = string.Join(", ", d.AcceptingPersonFullNames),
                ParticipantFullNames = d.ParticipantFullNames,
                CompanyNames = string.Join(", ", d.CompanyNames),
                Attachments = d.Attachments
                    .Select(a => new DocumentViewModel
                    {
                        ContentType = a.ContentType,
                        DocumentId = a.DocumentId,
                        DocumentName = a.DocumentName,
                        TemporaryDocumentId = a.TemporaryDocumentId
                    }).ToArray(),
                Settlements = d.Settlements.Select(settlementMapping.CreateFromSource).ToArray(),
                ParticipantEmployeeIds = d.Participants.Select(p => p.EmployeeId).ToArray(),
                Participants = d.Participants.Select(participantMapping.CreateFromSource).ToArray(),
                DefaultDoubleClickRowUrl = GetDefaultTabUrl(d.Status, d.ParticipantIds.Contains(_principalProvider.Current.Id.Value), d.Id),
                ReinvoicingToCustomer = d.ReinvoicingToCustomer,
                Timestamp = d.Timestamp
            };
        }

        private string GetDefaultTabUrl(BusinessTripStatus status, bool isParticipant, long id)
        {
            var tabs = BusinessTripNavigationHelper.GetTabs();
            var actionName = string.Empty;
            var controller = default(Type);

            switch (status)
            {
                case BusinessTripStatus.ArrangementInProgress:
                    actionName = nameof(BusinessTripArrangementTrackingController.Table);
                    controller = typeof(BusinessTripArrangementTrackingController);
                    break;

                case BusinessTripStatus.Arranged:
                case BusinessTripStatus.Ongoing:
                    actionName = isParticipant
                        ? nameof(BusinessTripItineraryController.Index)
                        : nameof(BusinessTripArrangementsController.Index);

                    controller = isParticipant
                        ? typeof(BusinessTripItineraryController)
                        : typeof(BusinessTripArrangementsController);

                    break;

                case BusinessTripStatus.Finished:
                case BusinessTripStatus.Accounted:
                    actionName = nameof(BusinessTripSettlementsController.List);
                    controller = typeof(BusinessTripSettlementsController);
                    break;

                default:
                    actionName = nameof(BusinessTripItineraryController.Index);
                    controller = typeof(BusinessTripItineraryController);
                    break;
            }

            if (!IsActionAccesible(tabs, controller, actionName))
            {
                foreach (var tab in tabs)
                {
                    if (_principalProvider.Current.IsInRole(tab.RequiredRole))
                    {
                        actionName = tab.ActionName;
                        controller = tab.Controller;
                        break;
                    }
                }
            }

            if (actionName != string.Empty && controller != null)
            {
                return new UrlHelper(HttpContext.Current.Request.RequestContext)
                    .UriActionGet(actionName, RoutingHelper.GetControllerName(controller), KnownParameter.ParentId).Url;
            }

            return null;
        }

        private bool IsActionAccesible(IEnumerable<BusinessTripTab> tabs, Type controllerType, string actionName)
        {
            var requiredRole = tabs.SingleOrDefault(t => t.ActionName == actionName && t.Controller == controllerType)?.RequiredRole;

            if (!requiredRole.HasValue)
            {
                return true;
            }

            return _principalProvider.Current.IsInRole(requiredRole.Value);
        }
    }
}
