﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripAdvancedPaymentRequestDtoToBusinessTripAdvancedPaymentRequestViewModelMapping : ClassMapping<BusinessTripAdvancedPaymentRequestDto, BusinessTripAdvancedPaymentRequestViewModel>
    {
        public BusinessTripAdvancedPaymentRequestDtoToBusinessTripAdvancedPaymentRequestViewModelMapping()
        {
            Mapping = d => new BusinessTripAdvancedPaymentRequestViewModel
            {
                Id = d.Id,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                Status = d.Status,
                Requestor = $"{d.RequestorFirstName} {d.RequestorLastName}",
                ParticipantFullName = d.ParticipantFullName,
            };
        }
    }
}
