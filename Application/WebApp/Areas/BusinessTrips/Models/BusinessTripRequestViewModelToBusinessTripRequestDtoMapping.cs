﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripRequestViewModelToBusinessTripRequestDtoMapping : ClassMapping<BusinessTripRequestViewModel, BusinessTripRequestDto>
    {
        public BusinessTripRequestViewModelToBusinessTripRequestDtoMapping()
        {
            Mapping = v => new BusinessTripRequestDto
            {
                Id = v.Id,
                ProjectIds = v.ProjectIds,
                ParticipantEmployeeIds = v.ParticipantEmployeeIds,
                StartedOn = v.StartedOn,
                EndedOn = v.EndedOn,
                DepartureCityIds = v.DepartureCityIds,
                DestinationCityId = v.DestinationCityId,
                AreTravelArrangementsNeeded = v.AreTravelArrangementsNeeded,
                TransportationPreferences = v.TransportationPreferences,
                AccommodationPreferences = v.AccommodationPreferences,
                DepartureCityTaxiVouchers = v.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = v.DestinationCityTaxiVouchers,
                TravelExplanation = v.TravelExplanation,
                LuggageDeclarations = v.LuggageDeclarations.Select(d => new DeclaredLuggageDto
                {
                    Id = d.Id,
                    EmployeeId = d.EmployeeId,
                    HandLuggageItems = d.HandLuggageItems,
                    RegisteredLuggageItems = d.RegisteredLuggageItems
                }).ToArray(),
                ItineraryDetails = v.ItineraryDetails
            };
        }
    }
}
