﻿using System.ComponentModel;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementViewModel
    {
        public long? Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long ParticipantId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long EmployeeId { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementsResources.EmployeeFirstNameLabel), typeof(BusinessTripSettlementsResources))]
        public string EmployeeFirstName { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementsResources.EmployeeLastNameLabel), typeof(BusinessTripSettlementsResources))]
        public string EmployeeLastName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        [Tooltip(nameof(EmployeeOrgUnitCode))]
        public string EmployeeOrgUnitName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmployeeOrgUnitCode { get; set; }

        [ApproversCell("~/Areas/Workflows/Views/Shared/ApproverGroups.Column.cshtml")]
        [DisplayNameLocalized(nameof(RequestResource.Approvers), typeof(RequestResource))]
        [NonSortable]
        public ApprovalGroupViewModel[] ApprovalGroups { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripSettlementsResources.RequestStatusLabel), typeof(BusinessTripSettlementsResources))]
        [NonSortable]
        public RequestStatus? RequestStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsOnControllingPhase => RequestStatus.HasValue
            && (RequestStatus == CrossCutting.Common.Enums.RequestStatus.Approved
                || RequestStatus == CrossCutting.Common.Enums.RequestStatus.Completed);

        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        [ReadOnly(true)]
        public BusinessTripSettlementControllingStatus? ControllingStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsRequestAvailable { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeSubmitted { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool? IsEmptyRequest { get; set; }

        public override string ToString()
        {
            return $"{EmployeeFirstName} {EmployeeLastName}";
        }
    }
}