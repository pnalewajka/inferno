﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [Identifier("Models.BusinessTripSettlementReportParameters")]
    public class BusinessTripSettlementDataSourceParametersViewModel
    {
        [ValuePicker(Type = typeof(BusinessTripPickerController))]
        [DisplayNameLocalized(nameof(ReportsResources.BusinessTripLabel), typeof(ReportsResources))]
        public long BusinessTripId { get; set; }

        [ValuePicker(
            Type = typeof(BusinessTripParticipantPickerController),
            OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name=\"" + nameof(BusinessTripId) + "\"]').val()"
                + " + '&picker-filter=SettlementReport'")]
        [DisplayNameLocalized(nameof(ReportsResources.BusinessTripParticipantLabel), typeof(ReportsResources))]
        public long BusinessTripParticipantId { get; set; }
    }
}
