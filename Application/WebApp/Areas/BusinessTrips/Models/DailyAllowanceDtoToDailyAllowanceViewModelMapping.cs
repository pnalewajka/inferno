﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class DailyAllowanceDtoToDailyAllowanceViewModelMapping : ClassMapping<DailyAllowanceDto, DailyAllowanceViewModel>
    {
        public DailyAllowanceDtoToDailyAllowanceViewModelMapping()
        {
            Mapping = d => new DailyAllowanceViewModel
            {
                Id = d.Id,
                ValidFrom = d.ValidFrom,
                ValidTo = d.ValidTo,
                EmploymentCountryId = d.EmploymentCountryId,
                EmploymentCountryName = d.EmploymentCountryName,
                TravelCityId = d.TravelCityId,
                TravelCityName = d.TravelCityName,
                TravelCountryId = d.TravelCountryId,
                TravelCountryName = d.TravelCountryName,
                CurrencyId = d.CurrencyId,
                Allowance = d.Allowance,
                DepartureAndArrivalDayAllowance = d.DepartureAndArrivalDayAllowance,
                AccommodationLumpSum = d.AccommodationLumpSum,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
