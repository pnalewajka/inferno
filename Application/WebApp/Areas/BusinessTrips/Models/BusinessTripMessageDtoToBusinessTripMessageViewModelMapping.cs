﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripMessageDtoToBusinessTripMessageViewModelMapping : ClassMapping<BusinessTripMessageDto, BusinessTripMessageViewModel>
    {
        public BusinessTripMessageDtoToBusinessTripMessageViewModelMapping()
        {
            Mapping = d => new BusinessTripMessageViewModel
            {
                Id = d.Id,
                AuthorEmployeeId = d.AuthorEmployeeId,
                AddedOn = d.AddedOn,
                Content = d.Content,
                BusinessTripId = d.BusinessTripId,
                AuthorEmail = d.AuthorEmail,
                AuthorFullName = d.AuthorFullName
            };
        }
    }
}
