﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestMealViewModelToBusinessTripSettlementRequestMealDtoMapping : ClassMapping<BusinessTripSettlementRequestMealViewModel, BusinessTripSettlementRequestMealDto>
    {
        public BusinessTripSettlementRequestMealViewModelToBusinessTripSettlementRequestMealDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementRequestMealDto
            {
                CountryId = v.CountryId,
                Day = v.Day,
                BreakfastCount = v.BreakfastCount,
                LunchCount = v.LunchCount,
                DinnerCount = v.DinnerCount,
            };
        }
    }
}
