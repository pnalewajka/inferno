﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestAdvancePaymentDtoToBusinessTripSettlementRequestAdvancePaymentViewModelMapping : ClassMapping<BusinessTripSettlementRequestAdvancePaymentDto, BusinessTripSettlementRequestAdvancePaymentViewModel>
    {
        public BusinessTripSettlementRequestAdvancePaymentDtoToBusinessTripSettlementRequestAdvancePaymentViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestAdvancePaymentViewModel
            {
                CurrencyId = d.CurrencyId,
                Amount = d.Amount,
                WithdrawnOn = d.WithdrawnOn,
            };
        }
    }
}
