namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripDisplayParticipantViewModel
    {
        public long Id { get; set; }

        public virtual long EmployeeId { get; set; }

        public virtual string EmployeeName { get; set; }
    }
}