﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestOwnCostViewModel
    {
        [Required]
        [HtmlSanitize]
        [AllowHtml]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Description), typeof(BusinessTripSettlementRequestResources))]
        public string Description { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.TransactionDate), typeof(BusinessTripSettlementRequestResources))]
        public DateTime TransactionDate { get; set; }

        [Required]
        [Hint(nameof(BusinessTripResources.GrossOrNetAmountHint), typeof(BusinessTripResources))]
        [DisplayNameLocalized(nameof(BusinessTripResources.AmountLabel), typeof(BusinessTripResources))]
        [MinValue(0)]
        public decimal Amount { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.Currency), typeof(BusinessTripResources))]
        public long CurrencyId { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripResources.HasInvoiceIssuedLabel), typeof(BusinessTripResources))]
        public bool HasInvoiceIssued { get; set; }
    }
}
