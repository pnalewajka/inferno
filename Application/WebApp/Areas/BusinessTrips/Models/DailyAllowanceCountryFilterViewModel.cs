﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [Identifier("Fitlers.DailyAllowanceCountryFilter")]
    [DescriptionLocalized(nameof(DailyAllowanceResources.EmploymentCountry), typeof(DailyAllowanceResources))]
    public class DailyAllowanceCountryFilterViewModel
    {
        [DisplayNameLocalized(nameof(DailyAllowanceResources.FilterEmploymentCountry), typeof(DailyAllowanceResources))]
        [ValuePicker(Type = typeof(CountryPickerController))]
        [Required]
        public long CountryId { get; set; }

        public override string ToString()
        {
            return DailyAllowanceResources.EmploymentCountry;
        }
    }
}