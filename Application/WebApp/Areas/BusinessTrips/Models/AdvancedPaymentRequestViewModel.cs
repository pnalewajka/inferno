﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class AdvancedPaymentRequestViewModel
    {
        public long Id { get; set; }

        [Required]
        [ValuePicker(Type = typeof(BusinessTripPickerController), OnResolveUrlJavaScript = "url += '&mode=not-accounted'", DialogWidth = "1200px")]
        [DisplayNameLocalized(nameof(BusinessTripResources.BusinessTripLabel), typeof(BusinessTripResources))]
        public long BusinessTripId { get; set; }

        [Required]
        [ValuePicker(Type = typeof(BusinessTripParticipantPickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name=\"RequestObjectViewModel." + nameof(BusinessTripId) + "\"]').val()")]
        [DisplayNameLocalized(nameof(BusinessTripResources.Participant), typeof(BusinessTripResources))]
        [UpdatesApprovers]
        public long ParticipantId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripResources.AmountLabel), typeof(BusinessTripResources))]
        public decimal Amount { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController), 
            OnResolveUrlJavaScript = "url += '&participant-id=' + $('[name=\"RequestObjectViewModel." + nameof(ParticipantId) + "\"]').val()",
            OnSelectionChangedJavaScript = "AdvancedPaymentRequest.onCurrencyPickerSelectionChanged")]
        [DisplayNameLocalized(nameof(BusinessTripResources.Currency), typeof(BusinessTripResources))]
        public long CurrencyId { get; set; }

        [HiddenInput]
        public string CurrencyDisplayName { get; set; }
    }
}
