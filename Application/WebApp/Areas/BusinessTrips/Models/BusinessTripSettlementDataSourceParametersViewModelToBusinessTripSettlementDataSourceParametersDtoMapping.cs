﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementDataSourceParametersViewModelToBusinessTripSettlementDataSourceParametersDtoMapping : ClassMapping<BusinessTripSettlementDataSourceParametersViewModel, BusinessTripSettlementDataSourceParametersDto>
    {
        public BusinessTripSettlementDataSourceParametersViewModelToBusinessTripSettlementDataSourceParametersDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementDataSourceParametersDto
            {
                BusinessTripId = v.BusinessTripId,
                BusinessTripParticipantId = v.BusinessTripParticipantId
            };
        }
    }
}
