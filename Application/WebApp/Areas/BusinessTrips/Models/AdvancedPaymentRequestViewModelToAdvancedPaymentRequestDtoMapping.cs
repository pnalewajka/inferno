﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class AdvancedPaymentRequestViewModelToAdvancedPaymentRequestDtoMapping : ClassMapping<AdvancedPaymentRequestViewModel, AdvancedPaymentRequestDto>
    {
        public AdvancedPaymentRequestViewModelToAdvancedPaymentRequestDtoMapping()
        {
            Mapping = v => new AdvancedPaymentRequestDto
            {
                Id = v.Id,
                Amount = v.Amount,
                CurrencyId = v.CurrencyId,
                CurrencyDisplayName = v.CurrencyDisplayName,
                BusinessTripId = v.BusinessTripId,
                ParticipantId = v.ParticipantId,
            };
        }
    }
}
