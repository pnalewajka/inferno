﻿using System.Linq;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementDtoToBusinessTripSettlementViewModelMapping : ClassMapping<BusinessTripSettlementDto, BusinessTripSettlementViewModel>
    {
        private readonly IBusinessTripSettlementService _businessTripSettlementService;
        private readonly IClassMapping<ApprovalGroupDto, ApprovalGroupViewModel> _approvalClassMapping;

        public BusinessTripSettlementDtoToBusinessTripSettlementViewModelMapping(
            IBusinessTripSettlementService businessTripSettlementService,
            IClassMappingFactory classMappingFactory)
        {
            _businessTripSettlementService = businessTripSettlementService;
            _approvalClassMapping = classMappingFactory.CreateMapping<ApprovalGroupDto, ApprovalGroupViewModel>();

            Mapping = d => CreateSettlementViewModel(d);
        }

        private BusinessTripSettlementViewModel CreateSettlementViewModel(BusinessTripSettlementDto d)
        {
            var settlementRequestDto = (BusinessTripSettlementRequestDto)d.Request?.RequestObject;

            return new BusinessTripSettlementViewModel
            {
                Id = settlementRequestDto?.Id,
                ParticipantId = d.Participant.Id,
                EmployeeId = d.Participant.EmployeeId,
                EmployeeFirstName = d.Participant.EmployeeFirstName,
                EmployeeLastName = d.Participant.EmployeeLastName,
                RequestStatus = d.Request?.Status,
                ApprovalGroups = d.Request != null
                    ? d.Request.ApprovalGroups.Select(_approvalClassMapping.CreateFromSource).ToArray()
                    : new ApprovalGroupViewModel[0],
                IsRequestAvailable = settlementRequestDto != null ? _businessTripSettlementService.IsRequestAvailableAfterFiltering(settlementRequestDto.Id) : false,
                ControllingStatus = settlementRequestDto?.ControllingStatus,
                IsEmptyRequest = settlementRequestDto?.IsEmpty,
                EmployeeOrgUnitName = d.Participant.EmployeeOrgUnitName,
                EmployeeOrgUnitCode = d.Participant.EmployeeOrgUnitCode
            };
        }
    }
}
