namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class ArrangementDocumentViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}