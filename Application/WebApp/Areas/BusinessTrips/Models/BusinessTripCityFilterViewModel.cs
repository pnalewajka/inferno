﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    [Identifier("Filters.BusinessTripCity")]
    [DescriptionLocalized(nameof(BusinessTripResources.CityDialogLabel), typeof(BusinessTripResources))]
    public class BusinessTripCityFilterViewModel
    {
        [DisplayNameLocalized(nameof(BusinessTripResources.CityLabel), typeof(BusinessTripResources))]
        [ValuePicker(Type = typeof(CityPickerController), OnResolveUrlJavaScript = "url += '&view=" + CityPickerController.BusinessTripsViewCode + "'", DialogWidth = "800px")]
        [Render(Size = Size.Large)]
        public long CityId { get; set; }

        public override string ToString()
        {
            return BusinessTripResources.CityLabel;
        }
    }
}