﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripAcceptanceConditionsViewModelToBusinessTripAcceptanceConditionsDtoMapping : ClassMapping<BusinessTripAcceptanceConditionsViewModel, BusinessTripAcceptanceConditionsDto>
    {
        public BusinessTripAcceptanceConditionsViewModelToBusinessTripAcceptanceConditionsDtoMapping()
        {
            Mapping = v => new BusinessTripAcceptanceConditionsDto
            {
                Id = v.Id,
                ReinvoicingToCustomer = v.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = v.ReinvoiceCurrencyId,
                CostApprovalPolicy = v.CostApprovalPolicy,
                MaxCostsCurrencyId = v.MaxCostsCurrencyId,
                MaxHotelCosts = v.MaxHotelCosts,
                MaxTotalFlightsCosts = v.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = v.MaxOtherTravelCosts
            };
        }
    }
}
