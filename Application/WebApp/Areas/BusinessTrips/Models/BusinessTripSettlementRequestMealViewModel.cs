﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestMealViewModel
        : IValidatableObject
    {
        public long? CountryId { get; set; }

        public string CountryName { get; set; }

        public DateTime? Day { get; set; }

        [Required]
        public long BreakfastCount { get; set; }

        [Required]
        public long LunchCount { get; set; }

        [Required]
        public long DinnerCount { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // one of the fields must have a value (Day or Country)
            if (!(Day.HasValue ^ CountryId.HasValue))
            {
                yield return new ValidationResult("[dev] something is broken in meals table");
            }
        }
    }
}
