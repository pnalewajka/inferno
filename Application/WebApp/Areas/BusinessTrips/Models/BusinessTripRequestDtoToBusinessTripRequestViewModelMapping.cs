﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripRequestDtoToBusinessTripRequestViewModelMapping : ClassMapping<BusinessTripRequestDto, BusinessTripRequestViewModel>
    {
        private readonly IEmployeeService _employeeService;

        public BusinessTripRequestDtoToBusinessTripRequestViewModelMapping(IEmployeeService employeeService)
        {
            _employeeService = employeeService;

            Mapping = d => new BusinessTripRequestViewModel
            {
                Id = d.Id,
                ProjectIds = d.ProjectIds,
                ParticipantEmployeeIds = d.ParticipantEmployeeIds,
                StartedOn = d.StartedOn,
                EndedOn = d.EndedOn,
                DepartureCityIds = d.DepartureCityIds,
                DestinationCityId = d.DestinationCityId,
                AreTravelArrangementsNeeded = d.AreTravelArrangementsNeeded,
                TransportationPreferences = d.TransportationPreferences,
                AccommodationPreferences = d.AccommodationPreferences,
                DepartureCityTaxiVouchers = d.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = d.DestinationCityTaxiVouchers,
                TravelExplanation = d.TravelExplanation,
                LuggageDeclarations = d.LuggageDeclarations.Select(GetLuggageViewModel).ToList(),
                ItineraryDetails = d.ItineraryDetails
            };
        }

        private DeclaredLuggageViewModel GetLuggageViewModel(DeclaredLuggageDto declaredLuggageDto)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(declaredLuggageDto.EmployeeId);

            return new DeclaredLuggageViewModel
            {
                Id = declaredLuggageDto.Id,
                EmployeeId = declaredLuggageDto.EmployeeId,
                EmployeeFullName = employee?.FullName,
                HandLuggageItems = declaredLuggageDto.HandLuggageItems,
                RegisteredLuggageItems = declaredLuggageDto.RegisteredLuggageItems
            };
        }
    }
}
