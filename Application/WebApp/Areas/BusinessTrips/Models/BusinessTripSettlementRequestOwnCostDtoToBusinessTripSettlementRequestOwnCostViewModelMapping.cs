﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestOwnCostDtoToBusinessTripSettlementRequestOwnCostViewModelMapping : ClassMapping<BusinessTripSettlementRequestOwnCostDto, BusinessTripSettlementRequestOwnCostViewModel>
    {
        public BusinessTripSettlementRequestOwnCostDtoToBusinessTripSettlementRequestOwnCostViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestOwnCostViewModel
            {
                Description = d.Description,
                TransactionDate = d.TransactionDate,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                HasInvoiceIssued = d.HasInvoiceIssued,
            };
        }
    }
}
