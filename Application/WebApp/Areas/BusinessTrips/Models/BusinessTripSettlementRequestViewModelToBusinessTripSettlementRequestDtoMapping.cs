﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestViewModelToBusinessTripSettlementRequestDtoMapping : ClassMapping<BusinessTripSettlementRequestViewModel, BusinessTripSettlementRequestDto>
    {
        public BusinessTripSettlementRequestViewModelToBusinessTripSettlementRequestDtoMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = v => new BusinessTripSettlementRequestDto
            {
                Id = v.Id,
                BusinessTripId = v.BusinessTripId,
                BusinessTripParticipantId = v.BusinessTripParticipantId,
                IsEmpty = v.IsEmpty,
                SettlementDate = v.SettlementDate,
                DepartureDateTime = v.DepartureDateTime,
                ArrivalDateTime = v.ArrivalDateTime,
                HoursWorkedForClient = v.HoursWorkedForClient,
                SimplifiedPrivateVehicleMileage = v.SimplifiedPrivateVehicleMileage != null
                    ? (v.SimplifiedPrivateVehicleMileage.PreferredDistanceUnit == MileageUnit.Kilometers
                        ? v.SimplifiedPrivateVehicleMileage.Kilometers
                        : UnitHelper.ConvertMilesToKilometers(v.SimplifiedPrivateVehicleMileage.Kilometers))
                    : (decimal?)null,
                PreferredDistanceUnit = v.SimplifiedPrivateVehicleMileage != null
                    ? v.SimplifiedPrivateVehicleMileage.PreferredDistanceUnit
                    : (v.PrivateVehicleMileage != null ? v.PrivateVehicleMileage.PreferredDistanceUnit : null),
                AbroadTimeEntries = v.AbroadTimeEntries.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAbroadTimeEntryViewModel,
                    BusinessTripSettlementRequestAbroadTimeEntryDto>().CreateFromSource).ToArray(),
                Meals = v.Meals.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestMealViewModel,
                    BusinessTripSettlementRequestMealDto>().CreateFromSource).ToArray(),
                OwnCosts = v.OwnCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestOwnCostViewModel,
                    BusinessTripSettlementRequestOwnCostDto>().CreateFromSource).ToArray(),
                CompanyCosts = v.CompanyCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestCompanyCostViewModel,
                    BusinessTripSettlementRequestCompanyCostDto>().CreateFromSource).ToArray(),
                AdditionalAdvancePayments = v.AdditionalAdvancePayments.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAdvancePaymentViewModel,
                    BusinessTripSettlementRequestAdvancePaymentDto>().CreateFromSource).ToArray(),
                PrivateVehicleMileage = v.PrivateVehicleMileage == null
                    ? null
                    : mappingFactory.CreateMapping<
                        BusinessTripSettlementRequestVehicleMileageViewModel,
                        BusinessTripSettlementRequestVehicleMileageDto>().CreateFromSource(v.PrivateVehicleMileage),
                ControllingStatus = v.ControllingStatus,
            };
        }
    }
}