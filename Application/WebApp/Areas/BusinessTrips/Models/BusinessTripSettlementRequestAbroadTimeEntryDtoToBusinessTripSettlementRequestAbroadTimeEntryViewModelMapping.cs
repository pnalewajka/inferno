﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestAbroadTimeEntryDtoToBusinessTripSettlementRequestAbroadTimeEntryViewModelMapping : ClassMapping<BusinessTripSettlementRequestAbroadTimeEntryDto, BusinessTripSettlementRequestAbroadTimeEntryViewModel>
    {
        public BusinessTripSettlementRequestAbroadTimeEntryDtoToBusinessTripSettlementRequestAbroadTimeEntryViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestAbroadTimeEntryViewModel
            {
                DepartureDateTime = d.DepartureDateTime,
                DepartureCountryId = d.DepartureCountryId,
                DepartureCityId = d.DepartureCityId,
                DestinationType = d.DestinationType,
            };
        }
    }
}
