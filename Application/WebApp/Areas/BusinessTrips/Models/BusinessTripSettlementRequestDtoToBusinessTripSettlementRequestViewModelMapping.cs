﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestDtoToBusinessTripSettlementRequestViewModelMapping : ClassMapping<BusinessTripSettlementRequestDto, BusinessTripSettlementRequestViewModel>
    {
        public BusinessTripSettlementRequestDtoToBusinessTripSettlementRequestViewModelMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = d => new BusinessTripSettlementRequestViewModel
            {
                Id = d.Id,
                BusinessTripId = d.BusinessTripId,
                BusinessTripParticipantId = d.BusinessTripParticipantId,
                IsEmpty = d.IsEmpty,
                DefaultCurrencyId = d.DefaultCurrencyId,
                DefaultCurrencyName = d.DefaultCurrencyName,
                DefaultCardHolderEmployeeId = d.DefaultCardHolderEmployeeId,
                DefaultCardHolderEmployeeName = d.DefaultCardHolderEmployeeName,
                SettlementDate = d.SettlementDate,
                DepartureDateTime = d.DepartureDateTime,
                ArrivalDateTime = d.ArrivalDateTime,
                HoursWorkedForClient = d.HoursWorkedForClient,
                SimplifiedPrivateVehicleMileage = d.SimplifiedPrivateVehicleMileage == null ? null :
                    new BusinessTripSettlementRequestSimplifiedVehicleMileageViewModel
                    {
                        PreferredDistanceUnit = d.PreferredDistanceUnit.Value,
                        Kilometers = d.PreferredDistanceUnit.Value == MileageUnit.Kilometers 
                            ? d.SimplifiedPrivateVehicleMileage.Value 
                            : UnitHelper.ConvertKilometersToMiles(d.SimplifiedPrivateVehicleMileage.Value)
                    },
                CalculationResult = d.CalculationResult,
                AbroadTimeEntries = d.AbroadTimeEntries.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAbroadTimeEntryDto,
                    BusinessTripSettlementRequestAbroadTimeEntryViewModel>().CreateFromSource).ToList(),
                Meals = d.Meals.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestMealDto,
                    BusinessTripSettlementRequestMealViewModel>().CreateFromSource).ToList(),
                OwnCosts = d.OwnCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestOwnCostDto,
                    BusinessTripSettlementRequestOwnCostViewModel>().CreateFromSource).ToList(),
                CompanyCosts = d.CompanyCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestCompanyCostDto,
                    BusinessTripSettlementRequestCompanyCostViewModel>().CreateFromSource).ToList(),
                AdditionalAdvancePayments = d.AdditionalAdvancePayments.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAdvancePaymentDto,
                    BusinessTripSettlementRequestAdvancePaymentViewModel>().CreateFromSource).ToList(),
                PrivateVehicleMileage = CreateMileage(d, mappingFactory),
                ControllingStatus = d.ControllingStatus,
            };
        }

        private BusinessTripSettlementRequestVehicleMileageViewModel CreateMileage(BusinessTripSettlementRequestDto request, IClassMappingFactory mappingFactory)
        {
            if (request.PrivateVehicleMileage == null)
            {
                return null;
            }

            var mapping = mappingFactory.CreateMapping<BusinessTripSettlementRequestVehicleMileageDto, BusinessTripSettlementRequestVehicleMileageViewModel>();
            var mileage = mapping.CreateFromSource(request.PrivateVehicleMileage);

            mileage.PreferredDistanceUnit = request.PreferredDistanceUnit;
            mileage.InboundKilometers = mileage.PreferredDistanceUnit == MileageUnit.Kilometers
                ? mileage.InboundKilometers
                : UnitHelper.ConvertKilometersToMiles(mileage.InboundKilometers);
            mileage.OutboundKilometers = mileage.PreferredDistanceUnit == MileageUnit.Kilometers
                ? mileage.OutboundKilometers
                : UnitHelper.ConvertKilometersToMiles(mileage.OutboundKilometers);

            return mileage;
        }
    }
}