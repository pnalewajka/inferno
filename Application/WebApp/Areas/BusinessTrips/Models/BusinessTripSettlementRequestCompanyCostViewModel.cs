﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestCompanyCostViewModel : IValidatableObject
    {
        [Required]
        [HtmlSanitize]
        [AllowHtml]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.Description), typeof(BusinessTripSettlementRequestResources))]
        public string Description { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.TransactionDate), typeof(BusinessTripSettlementRequestResources))]
        public DateTime TransactionDate { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(BusinessTripResources.Currency), typeof(BusinessTripResources))]
        public long CurrencyId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripResources.AmountLabel), typeof(BusinessTripResources))]
        [MinValue(0)]
        public decimal Amount { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.PaymentMethod), typeof(BusinessTripSettlementRequestResources))]
        public BusinessTripSettlementPaymentMethod PaymentMethod { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(BusinessTripSettlementRequestResources.CardHolder), typeof(BusinessTripSettlementRequestResources))]
        public long? CardHolderEmployeeId { get; set; }

        [DisplayNameLocalized(nameof(BusinessTripResources.HasInvoiceIssuedLabel), typeof(BusinessTripResources))]
        public bool HasInvoiceIssued { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (PaymentMethod == BusinessTripSettlementPaymentMethod.CreditCard && CardHolderEmployeeId == null)
            {
                yield return
                    new ValidationResult(BusinessTripSettlementRequestResources.CardHolderEmployeeRequired,
                        new[] {nameof(CardHolderEmployeeId) });
            }
        }
    }
}
