﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestVehicleMileageDtoToBusinessTripSettlementRequestVehicleMileageViewModelMapping : ClassMapping<BusinessTripSettlementRequestVehicleMileageDto, BusinessTripSettlementRequestVehicleMileageViewModel>
    {
        public BusinessTripSettlementRequestVehicleMileageDtoToBusinessTripSettlementRequestVehicleMileageViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestVehicleMileageViewModel
            {
                VehicleType = d.VehicleType,
                IsCarEngineCapacityOver900cc = d.IsCarEngineCapacityOver900cc,
                RegistrationNumber = d.RegistrationNumber,
                OutboundFromCityId = d.OutboundFromCityId,
                OutboundToCityId = d.OutboundToCityId,
                OutboundKilometers = d.OutboundKilometers,
                InboundFromCityId = d.InboundFromCityId,
                InboundToCityId = d.InboundToCityId,
                InboundKilometers = d.InboundKilometers,
            };
        }
    }
}