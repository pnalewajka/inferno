﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementRequestAdvancePaymentViewModelToBusinessTripSettlementRequestAdvancePaymentDtoMapping : ClassMapping<BusinessTripSettlementRequestAdvancePaymentViewModel, BusinessTripSettlementRequestAdvancePaymentDto>
    {
        public BusinessTripSettlementRequestAdvancePaymentViewModelToBusinessTripSettlementRequestAdvancePaymentDtoMapping()
        {
            Mapping = v => new BusinessTripSettlementRequestAdvancePaymentDto
            {
                CurrencyId = v.CurrencyId,
                Amount = v.Amount,
                WithdrawnOn = v.WithdrawnOn,
            };
        }
    }
}
