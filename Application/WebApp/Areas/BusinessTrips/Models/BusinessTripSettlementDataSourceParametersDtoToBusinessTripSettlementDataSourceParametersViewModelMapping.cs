﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripSettlementDataSourceParametersDtoToBusinessTripSettlementDataSourceParametersViewModelMapping : ClassMapping<BusinessTripSettlementDataSourceParametersDto, BusinessTripSettlementDataSourceParametersViewModel>
    {
        public BusinessTripSettlementDataSourceParametersDtoToBusinessTripSettlementDataSourceParametersViewModelMapping()
        {
            Mapping = d => new BusinessTripSettlementDataSourceParametersViewModel
            {
                BusinessTripId = d.BusinessTripId,
                BusinessTripParticipantId = d.BusinessTripParticipantId
            };
        }
    }
}
