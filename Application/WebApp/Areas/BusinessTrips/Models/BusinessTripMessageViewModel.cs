﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripMessageViewModel
    {
        public long Id { get; set; }

        public long AuthorEmployeeId { get; set; }

        public DateTime AddedOn { get; set; }

        [RichText]
        [AllowHtml]
        public string Content { get; set; }

        public long BusinessTripId { get; set; }

        public string AuthorEmail { get; set; }

        public string AuthorFullName { get; set; }
    }
}
