﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripArrangementViewModel
    {
        public long Id { get; set; }

        public ArrangementType ArrangementType { get; set; }

        public string Name { get; set; }

        public DateTime IssuedOn { get; set; }

        public IList<BusinessTripDisplayParticipantViewModel> Participants { get; set; }

        public string Details { get; set; }

        public CurrencyViewModel Currency { get; set; }

        public decimal? Value { get; set; }

        public IList<ArrangementDocumentViewModel> ArrangementDocuments { get; set; }

        public byte[] Timestamp { get; set; }
    }
}