﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Models
{
    public class BusinessTripArrangementSaveContract : IBusinessTripArrangementSaveContract
    {
        public long? Id { get; set; }

        public ArrangementType ArrangementType { get; set; }

        public string Name { get; set; }

        public DateTime IssuedOn { get; set; }

        public IList<long> ParticipantIds { get; set; }

        public string Details { get; set; }

        public long? CurrencyId { get; set; }

        public decimal? Value { get; set; }

        public List<ArrangementDocumentSaveContract> ArrangementDocumentsContracts { get; set; }
        
        public byte[] Timestamp { get; set; }

        IList<IArrangementDocumentSaveContract> IBusinessTripArrangementSaveContract.ArrangementDocuments 
            => (ArrangementDocumentsContracts ?? new List<ArrangementDocumentSaveContract>()).Cast<IArrangementDocumentSaveContract>().ToList();
    }
}