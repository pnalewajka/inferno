﻿namespace Settlements
{
    export const UpdateMealsEventDispatcher = new Atomic.Events.EventDispatcher<IMeal[]>();

    interface IRequestObjectModel {
        Meals: IMeal[];
    }

    // From/To server
    interface IMeal
    {
        CountryId: string | null;
        CountryName: string | null;
        Day: string | null;
        BreakfastCount: string;
        LunchCount: string;
        DinnerCount: string;
    }

    type CountProperty = "BreakfastCount" | "LunchCount" | "DinnerCount";

    interface IMealsTableProps
    {
        meals: IMeal[];
    }

    interface IMealsTableState
    {
        meals: IMeal[];
    }

    export class ReadOnlyMealsTable extends React.Component<IMealsTableProps, IMealsTableState>
    {
        protected updateMealsEventHandler = (e: IMeal[]) => this.updateMeals(e);

        public constructor(props: IMealsTableProps)
        {
            super(props);

            this.state = {
                meals: [...props.meals]
            };
        }

        public componentDidMount(): void
        {
            UpdateMealsEventDispatcher.subscribe(this.updateMealsEventHandler);
        }

        public componentWillUnmount(): void
        {
            UpdateMealsEventDispatcher.unsubscribe(this.updateMealsEventHandler);
        }

        protected updateMeals(newMeals: IMeal[]): void
        {
            // readonly, do nothing
        }

        protected switchMode<TOut>(perCountrySelector: () => TOut | null, dailySelector: () => TOut | null): TOut
        {
            return (this.state.meals.some(m => m.CountryId != null)
                ? perCountrySelector()
                : dailySelector())!;
        }

        protected getRows(): JSX.Element[]
        {
            return this.state.meals.map((m, i) =>
                <tr key={i} >
                    <td><span>{this.switchMode(() => m.CountryName, () => m.Day)}</span></td>
                    <td><span>{this.switchMode(() => m.BreakfastCount, () => (m.BreakfastCount ? "Yes" : ""))}</span></td>
                    <td><span>{this.switchMode(() => m.LunchCount, () => (m.LunchCount ? "Yes" : ""))}</span></td>
                    <td><span>{this.switchMode(() => m.DinnerCount, () => (m.DinnerCount ? "Yes" : ""))}</span></td>
                </tr>);
        }

        public render(): JSX.Element
        {
            return (
                <table className="table table-bordered table-condensed table-hover small luggage-table">
                    <thead>
                        <tr>
                            <td><span>{this.switchMode(() => Globals.resources.Country as string, () => Globals.resources.Day)}</span></td>
                            <td><span>{Globals.resources.Breakfast}</span></td>
                            <td><span>{Globals.resources.Lunch}</span></td>
                            <td><span>{Globals.resources.Dinner}</span></td>
                        </tr>
                    </thead>
                    <tbody>
                        { this.getRows() }
                    </tbody>
                </table>
            );
        }
    }

    export class MealsTable extends ReadOnlyMealsTable
    {
        private getInputName(index: number, property: keyof IMeal): string
        {
            return `RequestObjectViewModel.Meals[${index}].${property}`;
        }

        protected getRows(): JSX.Element[]
        {
            return this.state.meals.map((m, i) =>
                <tr key={i}>
                    <td>
                        <span>{this.switchMode(
                            () => m.CountryName,
                            () => DateHelper.toDateString(DateHelper.parseBackendDate(m.Day!)))}</span>
                        <input
                            type="hidden"
                            name={this.getInputName(i, this.switchMode<keyof IMeal>(() => "CountryId", () => "Day"))}
                            value={this.switchMode(() => m.CountryId, () => m.Day)} />
                    </td>
                    <td>{this.getCountInput(i, "BreakfastCount")}</td>
                    <td>{this.getCountInput(i, "LunchCount")}</td>
                    <td>{this.getCountInput(i, "DinnerCount")}</td>
                </tr>);
        }

        private getCountInput(index: number, property: CountProperty)
        {
            const meal = this.state.meals[index];

            if (meal.CountryId != null)
            {
                return <input
                    key={index + property}
                    name={this.getInputName(index, property)}
                    value={meal[property]}
                    onChange={e => { meal[property] = e.target.value; this.forceUpdate(); }}
                    onBlur={() => this.sanitizeValue(meal, property)} />;
            }

            return (
                <span>
                    <input
                        type={"checkbox"}
                        key={index + property + "checkbox"}
                        checked={(parseInt(meal[property], 10) || 0) > 0}
                        onChange={e => { meal[property] = (e.target.checked ? "1" : "0"); this.forceUpdate(); }} />

                    <input
                        type={"hidden"}
                        key={index + property}
                        name={this.getInputName(index, property)}
                        value={meal[property]} />
                </span>);
        }

        private sanitizeValue(meal: IMeal, property: CountProperty): void
        {
            const value = parseInt(meal[property], 10) || 0;
            meal[property] = Math.max(0, value).toString(10);

            this.forceUpdate();
        }

        protected updateMeals(newMeals: IMeal[]): void
        {
            this.setState({ meals: newMeals });
        }
    }

    export function InitReadOnlyMealsTable(meals: IMeal[])
    {
        ReactDOM.render(
            <Settlements.ReadOnlyMealsTable meals={meals} />,
            document.getElementById("RequestObject.meals"));
    }

    export function InitMealsTable(meals: IMeal[])
    {
        ReactDOM.render(
            <Settlements.MealsTable meals={meals} />,
            document.getElementById("RequestObject.meals"));
    }

    export function updateMeals(meals: IMeal[])
    {
        Settlements.UpdateMealsEventDispatcher.dispatch(meals);
    }

    export function handleUpdateApprovers(serverResultData: { ApprovalGroups: WorkflowRequests.ApprovalGroup[], RequestObjectViewModel: IRequestObjectModel }): void {

        WorkflowRequests.updateApprovers(serverResultData);

        const settlementRequest = serverResultData.RequestObjectViewModel;

        if (settlementRequest == null) {
            return;
        }

        updateMeals(settlementRequest.Meals);
    }
}
