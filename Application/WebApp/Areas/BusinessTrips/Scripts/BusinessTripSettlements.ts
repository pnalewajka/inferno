﻿namespace BusinessTrip.Settlements {

    interface ISettlement {
        id: number | null;
        participantId: number;
        employeeFirstName: string;
        employeeLastName: string;
    }

    interface ICreateRequestRedirectParams {
        participantId: number;
        baseOnRequestId?: number;
    }

    type ISettlementRow = Grid.IRow & ISettlement;

    const returnPoint = 'return-point=businessTrips.businessTripSettlements.list';

    const Translations: {
        CopyRequestDataTitleFormat: string;
        CopyRequestDataContentFormat: string;
        ConfirmCopyDataText: string;
        DeclineCopyDataText: string;
        CreateEmptySettlementConfirmationMessage: string;
        ToolbarButtonConfirmationDialogTitle: string;
    } = Globals.resources;

    function redirectToCreatedRequest(addUrl: string, params: ICreateRequestRedirectParams): void {
        const paramsArray: string[] = [
            returnPoint,
            'request-type=business-trip-settlement-request',
            `participant-id=${params.participantId}`,
        ];

        if (params.baseOnRequestId !== undefined) {
            paramsArray.push(`base-on=${params.baseOnRequestId}`);
        }

        Forms.redirect(`${addUrl}?${paramsArray.join("&")}`);
    }

    export function CreateSettlementRequest(grid: ISettlement, addUrl: string): void {
        const data = Grid.getComponent($(".grid-table table.table")[0]).getRowData<ISettlementRow>();
        const [createdSettlement] = data.filter(s => s.id !== null);

        if (createdSettlement === undefined) {
            redirectToCreatedRequest(addUrl, { participantId: grid.participantId });
        } else {
            const message = Strings.format(Translations.CopyRequestDataTitleFormat, `${createdSettlement.employeeFirstName} ${createdSettlement.employeeLastName}`);
            CommonDialogs.confirm(message, message, (data) => {
                if (data.isOk) {
                    if (createdSettlement.id !== null) {
                        redirectToCreatedRequest(addUrl, { participantId: grid.participantId, baseOnRequestId: createdSettlement.id });
                    }
                    return;
                }

                redirectToCreatedRequest(addUrl, { participantId: grid.participantId });
            }, "", Translations.ConfirmCopyDataText, Translations.DeclineCopyDataText);
        }
    }

    export function ViewSettlementRequest(grid: ISettlement, viewUrl: string): void {
        Forms.redirect(`${viewUrl}/${grid.id}?${returnPoint}`);
    }

    export function CreateEmptySettlementRequest(grid: ISettlement, createUrl: string, parentId: number): void {
        CommonDialogs.confirm(Translations.CreateEmptySettlementConfirmationMessage,
            Translations.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    CommonDialogs.pleaseWait.show();
                    Forms.submit(`${createUrl}?participantId=${grid.participantId}&settlementRequestId=${parentId}`);
                }
            });
    }

    export function SubmitSettlementRequest(grid: ISettlement, submitUrl: string): void {
        CommonDialogs.pleaseWait.show();
        $.post(`${submitUrl}/${grid.id}`).then(() => window.location.reload());
    }
}