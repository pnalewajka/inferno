﻿$(() => {
    // Method first() is crucial because there are 7 checkboxes like this and we don't want to call server 7 times
    $("input[name='TransportationPreferences'][type='checkbox']").first().trigger('change');
});
