﻿namespace BussinessTrip.Arrangements {

    export const Translations: {
        AddFileLabel: string,
        AddSimpleLabel: string,
        ArrangementTypeHeader: string,
        CurrencyHeader: string,
        DateLabel: string,
        DeleteLabel: string,
        DetailsHeader: string,
        DocumentsHeader: string,
        NameHeader: string,
        ParticipantsHeader: string,
        SelectCurrencyText: string,
        SelectParticipantText: string,
        ValueHeader: string,
        DeleteConfirmTitle: string,
        DeleteConfirmBody: string,
        DeleteConfirmRow: string,
        SettlementRequestsAlreadyExistsTitle: string,
        SettlementRequestsAlreadyExistsContent: string,
        AcceptRecalculation: string,
        SaveArrangementsText: string,
        SaveInProgressArrangementsText: string,
    } = Globals.resources;

    class ArrangementsGrid extends Atomic.Ui.Grid<IArrangementViewModel> { }

    export function Initialize(data: IArrangementComponentProps): void {
        ReactDOM.render(<ArrangementsComponent {...data} />, document.getElementById("business-arrangements"));
    }

    export enum ArrangementType {
        Unknown = 0,
        Flight = 1,
        Train = 2,
        InvoiceFlight = 3,
        InvoiceTrain = 4,
        Taxi = 5,
        InvoiceTaxi = 6,
        Hotel = 7,
        InvoiceHotel = 8,
        Other = 9,
        InvoiceOther = 10,
    }

    export interface ISaveDocumentContract {
        DocumentId?: number;
        TemporaryDocumentId?: string;
        DocumentName: string;
    }

    export interface IArrangementSaveContract {
        Id?: number;
        ArrangementType: ArrangementType;
        Name: string;
        IssuedOn: string;
        ParticipantIds: number[];
        Details: string;
        CurrencyId: number | null;
        Value: string | null;
        ArrangementDocumentsContracts: ISaveDocumentContract[];
        Timestamp?: string;
    }

    export interface IArrangementViewModel {
        id?: number;
        arrangementType: ArrangementType;
        name: string;
        participants: IParticipant[];
        issuedOn?: Date;
        details: string;
        currency: ICurrency | null;
        value: number | null;
        arrangementDocuments: IArrangementDocument[];
        timestamp?: string;
    }

    export interface IParticipant {
        id: number;
        employeeId: number;
        employeeName: string;
    }

    export interface ICurrency {
        id: number;
        name: string;
        isoCode: string;
        "symbol": string;
    }

    export interface IArrangementDocument {
        id?: number;
        temporaryId?: string;
        name: string;
        $uploadProgress?: number | undefined;
    }

    export interface IArrangementComponentProps {
        hasAnySettlementRequest: boolean;
        arrangements: IArrangementViewModel[];
        participants: IParticipant[];
        businessTripId: number;
        canDeleteArrangements: boolean;
    }

    export interface IArrangementComponentState {
        arrangements: IArrangementViewModel[];
        businessTripId: number | null;
        settlementRecalculationApproved: boolean;
        hasAnySettlementRequest: boolean;
        isSaving: boolean;
        canDeleteArrangements: boolean;
    }

    class ArrangementsComponent extends Dante.Components.StateComponent<IArrangementComponentProps, IArrangementComponentState> {

        constructor() {
            super();

            this.state = {
                arrangements: [],
                businessTripId: null,
                settlementRecalculationApproved: false,
                hasAnySettlementRequest: false,
                isSaving: false,
                canDeleteArrangements: false
            };

            this.saveDebouncer = new Atomic.Debouncer(this.saveArrangementData.bind(this), 0);
            this.saveDebouncer.subscribeWindowClose();
        }

        private saveDebouncer: Atomic.Debouncer;

        private get editDisabled(): boolean {
            return this.state.hasAnySettlementRequest && !this.state.settlementRecalculationApproved;
        }

        private get editEnabled(): boolean {
            return !this.editDisabled;
        }

        private allowRowEdit(row: IArrangementViewModel): boolean {
            return !(this.editDisabled || this.state.isSaving || row.id === undefined);
        }

        private allowRowDelete(): boolean {
            return this.state.canDeleteArrangements;
        }

        private columns: Array<Atomic.Ui.IGridColumnDefinition<IArrangementViewModel>> = [
            {
                code: "arrangementType",
                label: Translations.ArrangementTypeHeader,
                resolveComponent: (cellData, model) => <Dante.Ui.EditableEnumDropdown
                    disabled={!this.allowRowEdit(model)}
                    className="arrangement-type ellipsis"
                    onChange={(v: ArrangementType) => { this.updateArrangementType(model, v); }}
                    value={model.arrangementType}
                    enum={ArrangementType}
                    enumResources={Globals.enumResources.ArrangementType} />
            },
            { code: "name", label: Translations.NameHeader, resolveComponent: (cellData, model) => <Dante.Ui.EditableInputComponent disabled={this.editDisabled || model.id === undefined} className="arrangement-name ellipsis" value={model.name} onChange={(value) => { this.updateArrangementName(model, value); }} /> },
            { code: "details", label: Translations.DetailsHeader, resolveComponent: (cellData, model) => <Dante.Ui.EditableInputComponent disabled={this.editDisabled || model.id === undefined} className="ellipsis" value={model.details} onChange={(value) => { this.updateArrangementDetails(model, value); }} /> },
            {
                code: "participants",
                label: Translations.ParticipantsHeader,
                resolveComponent: (cellData, model) => (
                    <Dante.Ui.EditableMultipleValuePicker
                        disabled={!this.allowRowEdit(model)}
                        className="arrangement-participant ellipsis"
                        resolveUrl={(url: string) => `${url}&parent-id=${this.state.businessTripId}`}
                        valuePickerId="ValuePickers.BusinessTripParticipant"
                        values={model.participants == null ? null : model.participants.map(p => ({ key: p.id, name: p.employeeName }))}
                        defaultText={Translations.SelectParticipantText}
                        onChange={(a: IParticipant[]) => { this.updateArrangementParticipants(model, a); }} />
                )
            },
            {
                code: "date",
                label: Translations.DateLabel,
                resolveComponent: (cellData, model) => (
                    <Dante.Ui.EditableDatePicker
                        disabled={!this.allowRowEdit(model)}
                        className="arrangement-issued-on"
                        value={model.issuedOn}
                        autofocus={true}
                        onChange={(d) => { if (d !== null) { this.updateArrangementDate(model, d); } }} />
                )
            },
            {
                code: "currency",
                label: Translations.CurrencyHeader,
                resolveComponent: (cellData, model) => (
                    <Dante.Ui.EditableValuePicker
                        disabled={!this.allowRowEdit(model)}
                        valuePickerId="ValuePickers.Currencies"
                        value={model.currency == null ? null : ({ name: `${model.currency.name} ${model.currency.symbol}`, key: model.currency.id })}
                        defaultText={Translations.SelectCurrencyText}
                        onChange={(a: ICurrency) => { this.updateArrangementCurrency(model, a); }} />
                )
            },
            { code: "value", label: Translations.ValueHeader, resolveComponent: (cellData, model) => <Dante.Ui.EditableInputNumberComponent disabled={!this.allowRowEdit(model)} value={model.value} onChange={(value) => { this.updateArrangementValue(model, value); }} /> },
            { code: "documents", label: " ", resolveComponent: (cellData, model) => this.displayArrangementDocuments(model) },
            {
                code: "select", label: "", resolveComponent: (cellData, model) => {
                    if (model.id === undefined || !this.allowRowDelete()) {
                        return null;
                    }

                    return (
                        <button className="btn btn-link btn-sm" disabled={this.editDisabled} onClick={() => this.deleteArrangement(model)}>
                            <i className="fa fa-trash-o"></i>
                        </button>
                    );
                }
            },
        ];

        private displayArrangementDocuments(model: IArrangementViewModel): JSX.Element | string {
            if (!model.arrangementDocuments) {
                return "";
            }

            return (
                <div className="row-line arrangement-docs">
                    {model.arrangementDocuments.map((d, i) => (
                        d.$uploadProgress === undefined
                            ? this.displayDownloadDocument(d)
                            : this.displayUploadIndicator(d.$uploadProgress)
                    ))}
                </div>
            );
        }

        private displayUploadIndicator(value: number): JSX.Element {
            return (
                <i
                    className="fa fa-refresh fa-spin"
                    title={`${value}%`}
                >
                </i>);
        }

        private displayDownloadDocument(document: IArrangementDocument): JSX.Element {
            if (!!document.id) {
                return (
                    <a href={`/DocumentDownload/Download/Documents.ArrangementDocumentMapping/${document.id}`} key={document.id}>
                        <i title={document.name} className="fa fa-files-o"></i>
                    </a>);
            }

            return <i title={document.name} className="fa fa-files-o"></i>;
        }

        private pageTitle: string | undefined = undefined;
        private showDirtyIndicator(): void {
            if (this.pageTitle === undefined) {
                this.pageTitle = document.title;
            }

            document.title = `* ${document.title}`;
        }

        private hideDirtyIndicator(): void {
            if (this.pageTitle !== undefined) {
                document.title = this.pageTitle;
                this.pageTitle = undefined;
            }
        }

        private saveData(): void {
            const someFileStillSending = this.state.arrangements.some(a => a.arrangementDocuments.some(d => d.$uploadProgress !== undefined));

            if (someFileStillSending) {
                return;
            }

            this.showDirtyIndicator();
            this.saveDebouncer.execute();
        }

        private updateArrangementItem(model: IArrangementViewModel, updateAction: (item: IArrangementViewModel) => IArrangementViewModel): void {
            this.updateState({ arrangements: [...this.state.arrangements.map(a => (a !== model ? a : updateAction(a)))] });
            this.saveData();
        }

        private updateArrangementType(model: IArrangementViewModel, arrangementType: ArrangementType): void {
            this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { arrangementType }));
        }

        private updateArrangementName(model: IArrangementViewModel, newName: string): void {
            if (!!newName) {
                this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { name: newName }));
            }
        }

        private updateArrangementDetails(model: IArrangementViewModel, newDetails: string): void {
            this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { details: newDetails }));
        }

        private updateArrangementCurrency(model: IArrangementViewModel, currency: ICurrency): void {
            this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { currency }));
        }

        private updateArrangementValue(model: IArrangementViewModel, value: number | null): void {
            this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { value }));
        }

        private updateArrangementParticipants(model: IArrangementViewModel, participants: IParticipant[]): void {
            this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { participants: [...participants] }));
        }

        private updateArrangementDate(model: IArrangementViewModel, issuedOn: Date): void {
            this.updateArrangementItem(model, (a) => Dante.Utils.Assign(a, { issuedOn }));
        }

        private simpleAddArrangement() {
            const newArrangement: IArrangementViewModel = {
                arrangementType: ArrangementType.Unknown,
                name: "Arrangement",
                participants: [],
                issuedOn: new Date(),
                details: "",
                currency: null,
                value: null,
                arrangementDocuments: []
            };

            this.updateState({ arrangements: [...this.state.arrangements, newArrangement] });
            this.saveData();
        }

        private deleteArrangement(arrangement: IArrangementViewModel): void {
            const body = Dante.Utils.ElementToHtml(
                <div>
                    <div>{Translations.DeleteConfirmBody}</div>
                    <div>{arrangement.name}</div>
                </div>
            );

            CommonDialogs.confirm(body, Translations.DeleteConfirmTitle, (e) => {
                if (e.isOk) {
                    const url = Globals.resolveUrl(`~/BusinessTrips/BusinessTripArrangements/RemoveArrangements?parent-id=${this.state.businessTripId}&arrangementId=${arrangement.id}&recalculate-approved=${this.state.settlementRecalculationApproved}`);
                    this.updateState({ isSaving: true });

                    $.post(url, (result) => {
                        this.handleResponse(result);
                    });
                }
            });
        }

        private addFile(name: string, uniqueId: string, file: File): void {
            const newFileArrangementModel: IArrangementViewModel = {
                arrangementType: ArrangementType.Unknown,
                name,
                participants: [],
                issuedOn: new Date(),
                details: "",
                currency: null,
                value: null,
                arrangementDocuments: [{ temporaryId: uniqueId, name, $uploadProgress: 1 }]
            };

            this.updateState({ arrangements: [...this.state.arrangements, newFileArrangementModel] }, () => {
                PdfHelper.readPdf(file)
                    .then(lines => {
                        this.predictAndUpdateArrangementFields(uniqueId, name, lines.join(" "));
                    }).catch(() => {
                        this.predictAndUpdateArrangementFields(uniqueId, name);
                    });
            });
        }

        private predictAndUpdateArrangementFields(uniqueId: string, fileName: string, fileContent?: string): void {
            const newPartial = this.predictArrangementFields(fileName, fileContent || "");

            this.updateState({
                arrangements: [...this.state.arrangements.map(a => {
                    if (a.arrangementDocuments.some(d => d.temporaryId === uniqueId)) {
                        return Dante.Utils.Assign(a, newPartial);
                    }

                    return a;
                })]
            });
        }

        private predictArrangementFields(fileName: string, fileContent: string): Partial<IArrangementViewModel> {
            const newPartial: Partial<IArrangementViewModel> = {};
            const searchContent = `${fileName} ${fileContent}`.toLowerCase();

            const participateType = this.predictArrangementType(searchContent);
            if (participateType !== undefined) {
                newPartial.arrangementType = participateType;
            }

            newPartial.participants = this.predictParticipants(searchContent);

            return newPartial;
        }

        private predictParticipants(content: string): IParticipant[] {
            return [...this.props.participants.filter(p => {
                return p.employeeName.toLowerCase().split(" ").every(word => content.indexOf(word) !== -1);
            })];
        }

        private predictArrangementType(searchContent: string): ArrangementType | undefined {
            if (["lot", "flyer", "lufthansa"].some(key => searchContent.indexOf(key) !== -1)) {
                return this.predictIsInvoice(searchContent) ? ArrangementType.InvoiceFlight : ArrangementType.Flight;
            }

            if (["intercity", "pkp", "train", "bilet kolejowy"].some(key => searchContent.indexOf(key) !== -1)) {
                return this.predictIsInvoice(searchContent) ? ArrangementType.InvoiceTrain : ArrangementType.Train;
            }

            if (["hotel", "wynajem pokoi"].some(key => searchContent.indexOf(key) !== -1)) {
                return this.predictIsInvoice(searchContent) ? ArrangementType.InvoiceHotel : ArrangementType.Hotel;
            }

            if (["taxi"].some(key => searchContent.indexOf(key) !== -1)) {
                return this.predictIsInvoice(searchContent) ? ArrangementType.InvoiceTaxi : ArrangementType.Taxi;
            }

            if (this.predictIsInvoice(searchContent)) {
                return ArrangementType.InvoiceOther;
            }

            return undefined;
        }

        private predictIsInvoice(searchContent: string): boolean {
            return ["faktura", "invoice"].some(key => searchContent.indexOf(key) !== -1);
        }

        private uploadProps: Atomic.Ui.IFileProps = {
            saveUrl: "/DocumentUpload/UploadDocument",
            dropMessage: Translations.AddFileLabel,
            onFileAdded: (n, i, f) => {
                this.addFile(n, i, f);
            },
            onFileProgress: (fileId, progress) => {
                this.setState(Dante.Utils.Assign(this.state, {
                    arrangements: [...this.state.arrangements.map(a => {
                        if (a.arrangementDocuments.some(f => f.temporaryId === fileId)) {
                            return Dante.Utils.Assign(a, {
                                arrangementDocuments: [...a.arrangementDocuments.map(d => (d.temporaryId === fileId ? Dante.Utils.Assign(d, { $uploadProgress: progress }) : d))]
                            });
                        }

                        return a;
                    })]
                }));
            },
            onFileUploaded: (fileId) => {
                this.setState(Dante.Utils.Assign(this.state, {
                    arrangements: [...this.state.arrangements.map(a => {
                        if (a.arrangementDocuments.some(f => f.temporaryId === fileId)) {
                            a.arrangementDocuments = [...a.arrangementDocuments.map(d => {
                                if (d.temporaryId === fileId) {
                                    const cloneDocument = Dante.Utils.Clone(d);
                                    delete cloneDocument.$uploadProgress;

                                    return cloneDocument;
                                } else {
                                    return d;
                                }
                            })];
                        }

                        return a;
                    })]
                }));

                this.saveData();
            }
        };

        private decimalSendValue(value: number | null): string {
            return (value || "")
                .toString()
                .replace(/\./g, Globals.formatting.decimalSeparator)
                .replace(/\,/g, Globals.formatting.decimalSeparator);
        }

        private saveArrangementData(): void {
            if (this.editDisabled) {
                return;
            }

            const arrangements: IArrangementSaveContract[] = this.state.arrangements.map(a => ({
                Id: a.id,
                ArrangementType: a.arrangementType,
                Name: a.name,
                IssuedOn: (a.issuedOn as Date).toISOString(),
                ParticipantIds: a.participants.map(p => p.id),
                Details: a.details,
                CurrencyId: !!a.currency ? a.currency.id as number : null,
                Value: this.decimalSendValue(a.value),
                ArrangementDocumentsContracts: a.arrangementDocuments
                    .filter(d => d.$uploadProgress === undefined)
                    .map(d => ({ DocumentId: d.id, TemporaryDocumentId: d.temporaryId, DocumentName: d.name })),
                Timestamp: a.timestamp,
            }));

            const url = Globals.resolveUrl(`~/BusinessTrips/BusinessTripArrangements/SaveArrangements?parent-id=${this.state.businessTripId}&recalculate-approved=${this.state.settlementRecalculationApproved}`);
            const data = { arrangements };
            this.updateState({ isSaving: true });
            $.post(url, data, (result) => {
                this.handleResponse(result);
                this.updateState({ isSaving: false });
            });
        }

        private handleResponse(result: any): void {
            const dataResult = Dante.Utils.parseJson<{
                arrangements: Array<IArrangementViewModel>,
                hasAnySettlementRequest?: boolean,
                successful: boolean
            }>(result);

            const newState: Partial<IArrangementComponentState> = {
                hasAnySettlementRequest: dataResult.hasAnySettlementRequest
            };

            if (dataResult.successful) {
                newState.arrangements = dataResult.arrangements.map(d => {
                    d.issuedOn = d.issuedOn !== undefined ? new Date(d.issuedOn) : undefined;

                    return d;
                });

                newState.isSaving = false;
            }

            this.updateState(newState, () => {
                if (this.editDisabled) {
                    CommonDialogs.confirm(Translations.SettlementRequestsAlreadyExistsContent, Translations.SettlementRequestsAlreadyExistsTitle, (eventArgs) => {
                        if (eventArgs.isOk) {
                            this.updateState({ settlementRecalculationApproved: true }, () => this.saveArrangementData());
                        }
                    });
                }
            });

            this.hideDirtyIndicator();
        }

        protected receiveProps(nextProps: IArrangementComponentProps): void {
            const newState: Partial<IArrangementComponentState> = {
                arrangements: this.props.arrangements,
                hasAnySettlementRequest: this.props.hasAnySettlementRequest,
                businessTripId: this.props.businessTripId,
                canDeleteArrangements: this.props.canDeleteArrangements,
            };

            this.updateState(newState);
        }

        private renderSettlementWarning(show: boolean): JSX.Element | void {
            if (show) {
                return (
                    <div className="alert alert-warning" style={{ marginTop: 10 }}>
                        <strong>{Translations.SettlementRequestsAlreadyExistsTitle}</strong>&nbsp;
                        <span>{Translations.SettlementRequestsAlreadyExistsContent}</span>
                        <button
                            type="button"
                            className="close"
                            onClick={() => {
                                this.updateState({ settlementRecalculationApproved: true });
                            }}>
                            <span aria-hidden="true">{Translations.AcceptRecalculation}</span>
                        </button>
                    </div>
                );
            }

            return;
        }

        public render() {
            const gridProps: Atomic.Ui.IGridProps<IArrangementViewModel> = {
                columns: this.editEnabled ? this.columns : this.columns.filter(c => c.code !== "select"),
                rows: [...this.state.arrangements]
            };

            return (
                <div className="section">
                    {this.renderSettlementWarning(!this.editEnabled)}
                    {
                        !this.editEnabled
                            ? null
                            : (
                                <div className="section-header">
                                    <div className="btn-group">
                                        <button className="btn btn-default" onClick={() => { this.simpleAddArrangement(); }}>
                                            <i className="fa fa-plus"></i>
                                            <span>{Translations.AddSimpleLabel}</span>
                                        </button>
                                        <Atomic.Ui.FileButton {...this.uploadProps} />
                                    </div>
                                    <button className="btn btn-primary" disabled={this.state.isSaving} onClick={() => { this.saveArrangementData() }}>
                                        <i className={Dante.Utils.ClassNames({
                                            "fa-save": !this.state.isSaving, "fa-spinner": this.state.isSaving
                                        }, "fa")}></i>
                                        <span>{this.state.isSaving ? Translations.SaveInProgressArrangementsText : Translations.SaveArrangementsText}</span>
                                    </button>
                                </div>
                            )
                    }

                    <div className={Dante.Utils.ClassNames({ disabled: this.editDisabled }, "section-grid")}>
                        <Atomic.Ui.FileDropZone dropMessage={Translations.AddFileLabel}>
                            <ArrangementsGrid {...gridProps} />
                        </Atomic.Ui.FileDropZone>
                    </div>
                </div>
            );
        }
    }
}
