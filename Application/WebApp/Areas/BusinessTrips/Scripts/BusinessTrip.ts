﻿module BusinessTrip {
    export function assignFrontDeskResponsibility(grid: Grid.GridComponent, role: string) {
        const urlResolveCallback = (url: string) => url + `&role=${role}`;

        ValuePicker.selectSingle("EmployeeValuePickers.Manager", (record: Grid.IRow) => {
            CommonDialogs.confirm(Globals.resources.AssignFrontDeskResponsibilityConfirmationMessage + Utils.getHtmlList([record]),
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                    if (eventArgs.isOk) {
                        const businessTripIds = grid.getSelectedRowData().map(r => r.id.toString()).join(",");
                        const url = `~/BusinessTrips/BusinessTrip/AssignFrontDeskResponsibility?businessTripIds=${businessTripIds}&employeeId=${record.id}`;

                        Forms.submit(Globals.resolveUrl(url), "POST");
                    }
                });
        }, urlResolveCallback);
    }

    interface IBusinessTripGridRow extends Grid.IRow {
        defaultDoubleClickRowUrl : string
    }

    export function onRowDoubleClicked(grid: Grid.GridComponent, row: Element, rowData: IBusinessTripGridRow, isSelected: boolean): void {
        if (rowData.defaultDoubleClickRowUrl !== undefined && rowData.defaultDoubleClickRowUrl != null) {
            Forms.redirect(rowData.defaultDoubleClickRowUrl.replace("~this.id", rowData.id));
        }
    }
}
