﻿module Luggage {
    export function onParticipantChanged(participantControl: JQuery): void {
        const id = participantControl.children('input[name="EmployeeId"]').val();

        if (!isAlreadyAdded(id)) {
            const tableRows = $(".luggage-table tbody tr");
            const fullName = participantControl.find('input#employee-id').val();

            tableRows.remove();
            addNewRow(id, fullName, 0);
        }
        recalculateIndexValues();
    }

    export function onParticipantsChanged(participantControl:JQuery): void {
        let tableRows = $(".luggage-table tbody tr");

        tableRows.each((index, element: HTMLElement) => {
            var id = element.getAttribute("data-id") as string;

            if (!isStillSelected(participantControl, id)) {
                $(element).remove();
            }
        });

        let tableRowCount = tableRows.length;

        participantControl.find('.value-picker-item').each((index, element: HTMLElement) => {
            var id = element.getAttribute("data-id") as string;
            
            if (!isAlreadyAdded(id)) {
                var fullName = element.innerText;
                addNewRow(id, fullName, tableRowCount);
                tableRowCount++;
            }
        });
        recalculateIndexValues();
    }

    function isAlreadyAdded(id: string): boolean {
        return $('.luggage-table tr[data-id=\"' + id + '\"]').length !== 0;
    }

    function isStillSelected(participantControl: JQuery, id: string): boolean {
        return participantControl.find('.value-picker-item[data-id="' + id + '"]').length !== 0;
    }

    function addNewRow(id: string, fullName: string, index: number): void {
        const luggageTable = $('.luggage-table');
        const propertyName = luggageTable.data('model');

        var newRow = $(
        '<tr data-id=\"' + id + '\" data-index=\"' + index + '\" >' +
            '<td>' +
            fullName +
            '<input type="hidden" name="' + propertyName + '[' + index + '].EmployeeId" value="' + id + '" />' +
            '<input type="hidden" name="' + propertyName + '[' + index + '].EmployeeFullName" value="' + fullName +'" />'+
            '</td>' + 
            '<td><input type="number" name="' + propertyName + '[' + index + '].HandLuggageItems" value="0" data-val="true" min="0" max="5"/></td>' + 
            '<td><input type="number" name="' + propertyName + '[' + index + '].RegisteredLuggageItems" value="0" data-val="true" min="0" max="5"/></td>' + 
        '</tr>'
        );

        luggageTable.children('tbody').append(newRow);
    }

    function recalculateIndexValues():void {
        const luggageRows = $(".luggage-table tbody tr");
        let index = 0;
        luggageRows.each((elementIndex, element: HTMLElement) => {
            const existingIndex = element.getAttribute('data-index');
            $(element).find("input").each((inputIndex, inputElement: HTMLInputElement) => {
                inputElement.name = inputElement.name.replace(`[${existingIndex}]`, `[${index}]`);
            });
            element.setAttribute('data-index', index.toString());
            index++;
        });
    };
}