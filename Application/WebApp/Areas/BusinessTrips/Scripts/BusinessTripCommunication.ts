﻿module BusinessTripCommunication {
    export function onCommentFormSubmit(): boolean {
        var content = CKEDITOR.instances['content'].getData();
        var submit = document.getElementById("submitComment") as HTMLInputElement;
        var validation = document.getElementById("validation") as HTMLParagraphElement;

        if (content.length === 0) {
            validation.hidden = false;

            return false;
        }

        validation.hidden = true;
        submit.disabled = true;

        $(window).off("beforeunload");

        return true;
    }

    export function onMentionClick(event: JQueryEventObject): void {
        const $element = $(event.target);
        const rowItem = {
            Id: +$element.attr("data-id"),
            Label: $element.attr("data-label"),
            SourceName: $element.attr("data-source")
        };

        CKEDITOR.instances['content'].insertHtml(`@${JSON.stringify(rowItem)}&zwj;`);
    }
}

$(() => {
    const openAddComment = () => {
        $('.communication-container').addClass('open');
    };

    const closeAddComment = () => {
        $('.communication-container').removeClass('open');
    };

    const initAddComment = () => {
        if ($(".message-container .message-wrapper").length > 0 && $(window).innerWidth() >= 768) {
            $('.communication-container').addClass('communication-container-flexed');
        } else {
            $('.communication-container').removeClass('communication-container-flexed');
        }
    };

    initAddComment();

    $(".btn-show-comment").click(openAddComment);

    $(".btn-close-comment").click(closeAddComment);

    $(".mention-item").click((event) => {
        openAddComment();
        BusinessTripCommunication.onMentionClick(event);
    });

    $(window).resize(initAddComment);

    $(window).on("beforeunload", (event): void | boolean => {
        const content = CKEDITOR.instances["content"].getData();

        if (content.length > 0) {
            openAddComment();

            return false;
        }
    });
});