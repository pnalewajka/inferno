﻿declare const CKEDITOR: any;

namespace BussinessTrip.ArrangementTracking {
    enum TrackedArrangement {
        Train = 0,
        Ship = 1,
        Plane = 2,
        Hotel = 3,
        Insurance = 4,
        Taxi = 5,
    }

    enum ArrangementTrackingStatus {
        ToDo = 0,
        RequestedExternal = 1,
        InProgress = 2,
        Done = 3,
    }

    interface ITrackingStatusPickerProps {
        onChange: (value: ArrangementTrackingStatus) => void;
        currentValue: ArrangementTrackingStatus;
        arrangement: TrackedArrangement;
    }

    interface IAllTrackingStatusPickerProps {
        onChange: (value: ArrangementTrackingStatus) => void;
        arrangement: TrackedArrangement;
        getStats: (arrangement: TrackedArrangement) => IArrangementTrackingStats;
    }

    interface IArrangementTrackingProps {
        businessTripId: number,
        participants: IParticipant[],
    }

    interface IArrangementTrackingState {
        participants: IParticipant[],
        trackedArrangement: TrackedArrangement[],
    }

    interface IArrangementTrackingStats {
        total: number,
        toDo: number,
        inProgress: number,
        done: number,
    }

    interface IGridExtraProps {
        updateAllArrangements(arrangement: TrackedArrangement, status: ArrangementTrackingStatus): void;
    }

    interface ITrackingComponent extends Dante.Ui.IEnumDropdownProps {
    }

    class ArrangementTrackingGrid extends Atomic.Ui.Grid<IParticipant> {

        private getStats(arrangement: TrackedArrangement, relevantParticipants: IParticipant[]): ArrangementTrackingStatus {
            const total = relevantParticipants.filter(p => p.Statuses[arrangement] !== undefined).length;
            const toDo = relevantParticipants.filter(p => p.Statuses[arrangement] === ArrangementTrackingStatus.ToDo).length;
            const done = relevantParticipants.filter(p => p.Statuses[arrangement] === ArrangementTrackingStatus.Done).length;
            const inProgress = total - toDo - done;

            const generalStatus = done === total
                ? ArrangementTrackingStatus.Done
                : toDo === total
                    ? ArrangementTrackingStatus.ToDo
                    : ArrangementTrackingStatus.InProgress;

            return generalStatus;
        }

        private allParticipantsRow(columnDefinitions: Atomic.Ui.IGridColumnDefinition<IParticipant>[], rows: IParticipant[]): JSX.Element {
            var trackedDefinitions = columnDefinitions as Array<ArrangementTrackingColumnDefinition>;
            var arrangementColumns = trackedDefinitions.filter(d => d.trackedArrangement !== undefined);

            return (
                <tr key={-1}>
                    <td colSpan={columnDefinitions.length - arrangementColumns.length}>
                        <div className="row-line">{Globals.resources.AllParticipants}</div>
                    </td>
                    {arrangementColumns.map((a: ArrangementTrackingColumnDefinition, index: number) => {
                        if (a.trackedArrangement === undefined) {
                            return null;
                        }

                        const status = this.getStats(a.trackedArrangement, rows);

                        return (
                            <td key={a.code} className="grid-column grid-column-arrangement-status">
                                <CustomTrackingStatusPicker
                                    onChange={(v: ArrangementTrackingStatus) => {
                                        if (a.trackedArrangement !== undefined) {
                                            (this.props.extraProps as IGridExtraProps).updateAllArrangements(a.trackedArrangement, v);
                                        }
                                    }}
                                    value={status}
                                    enum={ArrangementTrackingStatus}
                                    enumResources={Globals.enumResources.ArrangementTrackingStatus}
                                />
                            </td>
                        );
                    })}
                </tr>
            );
        }

        protected renderRows(columnDefinitions: Atomic.Ui.IGridColumnDefinition<IParticipant>[], rows: IParticipant[]): JSX.Element[] {
            const originalRows = rows.map((row, index) => this.renderRow(columnDefinitions, row, index));

            if (rows.length === 1) {
                return originalRows;
            }

            return [this.allParticipantsRow(columnDefinitions, rows), ...originalRows];
        }
    }

    class ArrangementTracking extends React.Component<IArrangementTrackingProps, IArrangementTrackingState>
    {
        private saveParticipants = new Atomic.Debouncer(() => {
            $.post("SaveArrangementStatuses" + window.location.search, {
                "participants": this.state.participants.filter(p => p.ParticipantId > 0),
            });
        }, 1000);

        public constructor(props: IArrangementTrackingProps) {
            super(props);
            this.state = {
                participants: [...this.props.participants],
                trackedArrangement: Dante.Utils.Distinct(Dante.Utils.SelectMany(this.props.participants, p => Dante.Utils.Keys(p.Statuses))).map(Number)
            };
        }

        public componentWillMount(): void {
            this.saveParticipants.subscribeWindowClose();
        }

        public componentWillUnmount(): void {
            this.saveParticipants.unsubscribeWindowClose();
        }

        private createColumns(): Atomic.Ui.IGridColumnDefinition<IParticipant>[] {
            const columns: ArrangementTrackingColumnDefinition[] = [
                {
                    code: "Name",
                    label: Globals.resources.Participant,
                    resolveComponent: (context, row) => <div className={"row-line"}>{row.ParticipantName}</div>,
                },
            ];
            this.state.trackedArrangement.forEach(a => {
                this.addTrackingColumn(columns, a);
            });

            return columns;
        }

        private addTrackingColumn(
            columns: ArrangementTrackingColumnDefinition[],
            arrangement: TrackedArrangement): void {
            if (this.state.participants.some(s => s.Statuses[arrangement] !== undefined)) {
                columns.push({
                    code: TrackedArrangement[arrangement],
                    columnCssClass: "grid-column-arrangement-status",
                    label: getArrangementDisplayName(arrangement),
                    resolveComponent: this.arrangementTrackingCellFactory.bind(this),
                    trackedArrangement: arrangement,
                });
            }
        }

        private arrangementTrackingCellFactory(
            context: Atomic.Ui.ICellContext<IParticipant>,
            row: IParticipant): JSX.Element | null {
            const columnDefinition = context.columnDefinition as ArrangementTrackingColumnDefinition;
            const arrangement = columnDefinition.trackedArrangement;

            if (arrangement === undefined) {
                throw new Error("Column definition has no arrangement set");
            }

            const cellStatus = row.Statuses[arrangement];

            return cellStatus !== undefined
                ? <CustomTrackingStatusPicker
                    onChange={(v: ArrangementTrackingStatus) => {
                        this.onCellChange(row.ParticipantId, arrangement, v)
                    }}
                    value={cellStatus}
                    enum={ArrangementTrackingStatus}
                    enumResources={Globals.enumResources.ArrangementTrackingStatus} />
                : null;
        }

        private onCellChange(participantId: number, arrangement: TrackedArrangement, value: ArrangementTrackingStatus): void {
            const newState = Dante.Utils.Clone(this.state);

            for (const participant of newState.participants) {
                if (participant.ParticipantId === participantId) {
                    participant.Statuses[arrangement] = value;
                }
            }

            this.setState(
                newState,
                () => this.saveParticipants.execute());
        }

        private gridExtraProps: IGridExtraProps = {
            updateAllArrangements: (arrangement: TrackedArrangement, status: ArrangementTrackingStatus) => {

                const newState = Dante.Utils.Clone(this.state);

                for (const participant of newState.participants) {
                    if (participant.Statuses[arrangement] !== undefined) {
                        participant.Statuses[arrangement] = status;
                    }
                }

                this.setState(newState, () => this.saveParticipants.execute());
            }
        };

        public render(): JSX.Element {
            return <ArrangementTrackingGrid extraProps={this.gridExtraProps} rows={this.state.participants} columns={this.createColumns()} />;
        }
    }

    class CustomTrackingStatusPicker extends Dante.Ui.EditableEnumDropdown {
        protected getIcon(status?: ArrangementTrackingStatus): JSX.Element | undefined {
            if (status === ArrangementTrackingStatus.Done) {
                return <i className={"fa fa-check"}></i>;
            }

            return;
        }

        protected getTrackingStatus(): ArrangementTrackingStatus | undefined {
            return this.props.value as ArrangementTrackingStatus;
        }

        protected renderDisplayText(): string | JSX.Element {
            return (
                <span>
                    {this.getIcon(this.getTrackingStatus())}
                    {super.renderDisplayText()}
                </span>
            );
        }
    }

    function getArrangementDisplayName(arrangement: TrackedArrangement): string {
        return Globals.enumResources.TrackedArrangement[arrangement];
    }

    function getStatusDisplayName(status?: ArrangementTrackingStatus): string {
        return status !== undefined
            ? Globals.enumResources.ArrangementTrackingStatus[status]
            : "";
    }

    interface IParticipant {
        ParticipantId: number;
        ParticipantName: string;
        Statuses: { [key: string]: number; };
    }

    interface ArrangementTrackingColumnDefinition extends Atomic.Ui.IGridColumnDefinition<IParticipant> {
        trackedArrangement?: TrackedArrangement;
    }

    export function Initialize(businessTripId: number, participants: IParticipant[]): void {
        initializeRemarks(businessTripId, participants);
        initializeGrid(businessTripId, participants);
    }

    function initializeRemarks(businessTripId: number, participants: IParticipant[]): void {
        Forms.onCkEditorInit = () => {
            const editor = CKEDITOR.instances["remarks"];

            const saveRemarks = new Atomic.Debouncer(() => {
                $.post("SaveRemarks" + window.location.search, {
                    "remarks": editor.getData(),
                });
            }, 1000);

            saveRemarks.subscribeWindowClose();

            editor.on("change", () => saveRemarks.execute());
        };
    }

    function initializeGrid(businessTripId: number, participants: IParticipant[]): void {
        ReactDOM.render(
            <ArrangementTracking
                businessTripId={businessTripId}
                participants={participants} />,
            document.getElementById("arrangement-tracking-grid"));
    }
}
