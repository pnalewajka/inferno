﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    public abstract class BusinessTripTabCardIndexController<TViewModel, TDto>
        : SubCardIndexController<TViewModel, TDto, BusinessTripController, ParentIdContext>
        where TDto : class, new()
        where TViewModel : class, new()
    {
        public BusinessTripTabCardIndexController(
            ICardIndexDataService<TDto> cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            Layout.Resources.AddFrom<BusinessTripResources>();
            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/BusinessTrip.js");

            Layout.Css.Add("~/Areas/BusinessTrips/Content/Columns.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/SettlementsColumn.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/BusinessTrips.css");
        }
    }

    public abstract class BusinessTripTabReadOnlyCardIndexController<TViewModel, TDto>
        : ReadOnlySubCardIndexController<TViewModel, TDto, BusinessTripController, ParentIdContext>
        where TDto : class, new()
        where TViewModel : class, new()
    {
        public BusinessTripTabReadOnlyCardIndexController(
            ICardIndexDataService<TDto> cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            Layout.Resources.AddFrom<BusinessTripResources>();
            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/BusinessTrip.js");

            Layout.Css.Add("~/Areas/BusinessTrips/Content/Columns.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/SettlementsColumn.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/BusinessTrips.css");
        }

        protected ActionResult TabView(string tabName, TViewModel model)
        {
            var parentController = ReflectionHelper.CreateInstanceWithIocDependencies<BusinessTripController>();

            var viewModel = new BusinessTripTabViewModel<TViewModel>(model)
            {
                ContextSettings =
                    new CustomContextSettings("~/Areas/BusinessTrips/Views/Shared/BusinessTripContext.cshtml"),
                ContextViewModel = parentController.GetViewModelFromContext(Context)
            };

            return View(tabName, viewModel);
        }

        protected ActionResult TabView(TViewModel model)
        {
            return TabView(null, model);
        }
    }
}