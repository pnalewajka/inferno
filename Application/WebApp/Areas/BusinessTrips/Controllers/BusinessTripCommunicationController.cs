﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripCommunication)]
    public class BusinessTripCommunicationController
        : BusinessTripTabController<BusinessTripMessageViewModel>
    {
        private readonly IClassMapping<BusinessTripMessageDto, BusinessTripMessageViewModel> _businessTripMessageDtoToBusinessTripMessageViewModelClassMapping;

        public BusinessTripCommunicationController(
            IPrincipalProvider principalProvider,
            IBusinessTripService businessTripService,
            IBaseControllerDependencies dependencies,
            IClassMapping<BusinessTripMessageDto, BusinessTripMessageViewModel> businessTripMessageDtoToBusinessTripMessageViewModelClassMapping)
            : base(dependencies, businessTripService, principalProvider)
        {
            _businessTripMessageDtoToBusinessTripMessageViewModelClassMapping = businessTripMessageDtoToBusinessTripMessageViewModelClassMapping;

            Layout.PageTitle = BusinessTripResources.CommunicationTitle;
            
            Layout.Scripts.Add("~/Scripts/Atomic/SubCardIndex.js"); // Needed for context toggling
            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/BusinessTripCommunication.js");
        }

        [HttpPost]
        public ActionResult AddMessage(BusinessTripMessageViewModel viewModel)
        {
            if (!BusinessTripService.IsBusinessTripAccessible(Context.ParentId))
            {
                throw new BusinessException(BusinessTripResources.AccessError);
            }

            BusinessTripService.AddBusinessTripMessage(Context.ParentId, PrincipalProvider.Current.EmployeeId, viewModel.Content);

            return RedirectToAction(nameof(List), new RouteValueDictionary {
                { NamingConventionHelper.ConvertPascalCaseToHyphenated(nameof(Context.ParentId)), Context.ParentId }
            });
        }

        [HttpGet]
        [BreadcrumbBar("BusinessTripCommunication")]
        public ActionResult List()
        {
            if (!BusinessTripService.IsBusinessTripAccessible(Context.ParentId))
            {
                throw new BusinessException(BusinessTripResources.AccessError);
            }

            var messages = BusinessTripService.GetBusinessTripMessagesOrDefaultById(Context.ParentId);
            var parentController = ReflectionHelper.CreateInstanceWithIocDependencies<BusinessTripController>();

            var viewModel = new BusinessTripCommunicationViewModel
            {
                ContextSettings = new CustomContextSettings("~/Areas/BusinessTrips/Views/Shared/BusinessTripContext.cshtml"),
                ContextViewModel = parentController.GetViewModelFromContext(Context),
                CurrentEmployeeId = PrincipalProvider.Current.EmployeeId,
                Messages = messages.Select(_businessTripMessageDtoToBusinessTripMessageViewModelClassMapping.CreateFromSource).ToList()
            };

            return View("~/Areas/BusinessTrips/Views/Communication/Communication.cshtml", viewModel);
        }
    }
}
