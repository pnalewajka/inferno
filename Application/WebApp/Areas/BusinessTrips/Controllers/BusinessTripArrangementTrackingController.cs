﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripFrontDesk)]
    public class BusinessTripArrangementTrackingController
        : BusinessTripTabController<BusinessTripArrangementTrackingViewModel>
    {
        public BusinessTripArrangementTrackingController(
            IBaseControllerDependencies baseDependencies,
            IBusinessTripService businessTripService,
            IPrincipalProvider principalProvider)
            : base(baseDependencies, businessTripService, principalProvider)
        {
            Layout.EnumResources.Add<ArrangementTrackingStatus>();
            Layout.EnumResources.Add<TrackedArrangement>();

            Layout.Resources.AddFrom<BusinessTripResources>(nameof(BusinessTripResources.Participant));
            Layout.Resources.AddFrom<BusinessTripResources>(nameof(BusinessTripResources.ChangeAll));
            Layout.Resources.AddFrom<BusinessTripResources>(nameof(BusinessTripResources.AllParticipants));
        }

        [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripFrontDesk)]
        [BreadcrumbBar("BusinessTripArrangementTracking")]
        public ActionResult Table()
        {
            IsBusinessTripAccessible();

            Layout.PageTitle = BusinessTripResources.FrontDeskTitle;

            var viewModel = new BusinessTripArrangementTrackingViewModel
            {
                BusinessTripId = Context.ParentId,
                Remarks = BusinessTripService.GetArrangementTrackingRemarks(Context.ParentId),
                Participants = BusinessTripService.GetArrangementTrackingEntries(Context.ParentId),
            };

            return TabView(viewModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripFrontDesk)]
        public void SaveRemarks(BusinessTripArrangementTrackingViewModel viewModel)
        {
            IsBusinessTripAccessible();

            BusinessTripService.SaveArrangementTrackingRemarks(Context.ParentId, viewModel.Remarks);
        }

        [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripFrontDesk)]
        public void SaveArrangementStatuses(SaveArrangementTrackingParticipantViewModel[] participants)
        {
            IsBusinessTripAccessible();

            var participantDtos = participants.Select(p => new ArrangementTrackingParticipantDto
            {
                ParticipantId = p.ParticipantId,
                Statuses = p.Statuses.ToDictionary(
                    k => EnumHelper.GetEnumValue<TrackedArrangement>(k.Key),
                    k => (ArrangementTrackingStatus)k.Value),
            }).ToArray();

            BusinessTripService.SaveArrangementTrackingStatuses(Context.ParentId, participantDtos);
            BusinessTripService.SendArrangedNotificationEmailIfNeeded(Context.ParentId);
        }
    }
}