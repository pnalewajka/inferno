﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Helpers;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.ReportDataSources;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Helpers;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models;
using Smt.Atomic.WebApp.Resources;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTrips)]
    [PerformanceTest(nameof(BusinessTripController.List))]
    public class BusinessTripController
        : ReadOnlyCardIndexController<BusinessTripViewModel, BusinessTripDto, BusinessTripContext>
    {
        private readonly ITimeService _timeService;
        private readonly IReportingService _reportingService;
        private readonly IBusinessTripService _businessTripService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;

        public BusinessTripController(
            IBusinessTripService businessTripService,
            IReportingService reportingService,
            IBusinessTripParticipantsService businessTripParticipantsService,
            IBusinessTripCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies,
            ITimeService timeService)
            : base(cardIndexDataService, dependencies)
        {
            _timeService = timeService;
            _reportingService = reportingService;
            _businessTripService = businessTripService;
            _businessTripParticipantsService = businessTripParticipantsService;

            Layout.Resources.AddFrom<BusinessTripResources>();
            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/BusinessTrip.js");

            Layout.Css.Add("~/Areas/BusinessTrips/Content/Columns.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/SettlementsColumn.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/BusinessTrips.css");

            CardIndex.Settings.Title = BusinessTripResources.BusinessTripsTitle;

            CardIndex.Settings.AllowMultipleRowSelection = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanGenerateBusinessTripSettlementReport);

            CardIndex.Settings.OnRowDoubleClickedJavaScript = "BusinessTrip.onRowDoubleClicked";

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Apn, ProjectResources.ApnLabel),
                new SearchArea(SearchAreaCodes.Participants, BusinessTripResources.ParticipantsTitle),
                new SearchArea(SearchAreaCodes.Projects, BusinessTripResources.ProjectsLabel),
                new SearchArea(SearchAreaCodes.DepartureCity, BusinessTripResources.Departure),
                new SearchArea(SearchAreaCodes.DestinationCity, BusinessTripResources.Destination),
            };

            var detailsButton = new ToolbarButton
            {
                Text = BusinessTripResources.DetailsLabel,
                OnClickAction = new DropdownAction
                {
                    Items = BusinessTripNavigationHelper.GetTabs()
                        .Select(t => new CommandButton
                        {
                            Text = t.Name,
                            RequiredRole = t.RequiredRole,
                            Availability = new ToolbarButtonAvailability { Mode = AvailabilityMode.SingleRowSelected },
                            OnClickAction = Url.UriActionGet(t.ActionName, RoutingHelper.GetControllerName(t.Controller), KnownParameter.ParentId),
                            IsDefault = t.IsDefault
                        })
                        .ToList<ICommandButton>()
                }
            };

            var assignButton = new ToolbarButton
            {
                Text = BusinessTripResources.AssignFrontDeskResponsibilityLabel,
                Availability = new ToolbarButtonAvailability { Mode = AvailabilityMode.AtLeastOneRowSelected },
                RequiredRole = SecurityRoleType.CanAssignFrontDeskResponsibility,
                OnClickAction = new JavaScriptCallAction(
                $"BusinessTrip.assignFrontDeskResponsibility(grid, '{nameof(SecurityRoleType.CanManageBusinessTripFrontDesk)}');")
            };

            var reportButton = new ToolbarButton
            {
                Text = BusinessTripResources.GenerateSettlementReports,
                Availability = new ToolbarButtonAvailability { Mode = AvailabilityMode.AtLeastOneRowSelected },
                RequiredRole = SecurityRoleType.CanGenerateBusinessTripSettlementReport,
                OnClickAction = Url.UriActionGet(nameof(GenerateSettlementReports), RoutingHelper.GetControllerName(this), KnownParameter.SelectedIds | KnownParameter.Context),
            };

            CardIndex.Settings.ToolbarButtons.Clear();
            CardIndex.Settings.ToolbarButtons.Add(detailsButton);
            CardIndex.Settings.ToolbarButtons.Add(assignButton);
            CardIndex.Settings.ToolbarButtons.Add(reportButton);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    ButtonText = BusinessTripResources.Status,
                    DisplayName = BusinessTripResources.Status,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<BusinessTripStatus>()
                },
                new FilterGroup
                {
                    DisplayName = BusinessTripResources.FrontDeskAssigneeFilter,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<BusinessTripFrontDeskAssigneeFilterViewModel>(Business.BusinessTrips.Consts.FilterCodes.FrontDeskAsigneeFilter, BusinessTripResources.FrontDeskAssigneeFilterName)
                    }
                },
                new FilterGroup
                {
                    DisplayName = BusinessTripResources.Scope,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.AllTrips,
                            DisplayName = BusinessTripResources.AllTrips,
                        },
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.MyTrips,
                            DisplayName = BusinessTripResources.MyTrips,
                        },
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.ApprovedByMe,
                            DisplayName = BusinessTripResources.ApprovedByMe,
                        },
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.WatchedByMe,
                            DisplayName = BusinessTripResources.WatchedByMe,
                        },
                    }
                },
                new FilterGroup
                {
                    DisplayName = BusinessTripResources.DateFilterGroup,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<DateRangeFilterViewModel>(Business.BusinessTrips.Consts.FilterCodes.DateRangeFilter, BusinessTripResources.DateRangeFilterName),
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.AllMode,
                            DisplayName = BusinessTripResources.AllDateMode
                        },
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.MonthlyMode,
                            DisplayName = BusinessTripResources.MonthlyMode
                        }
                    }
                },
                new FilterGroup
                {
                    DisplayName = BusinessTripResources.BusinessTripCompany,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<CompanyFilterViewModel>(Business.Allocation.Consts.FilterCodes.CompanyFilter, BusinessTripResources.BusinessTripCompanyFilterName)
                    }
                },
                new FilterGroup
                {
                    DisplayName = BusinessTripResources.BusinessTripPlaceFilterGroupName,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<BusinessTripCityFilterViewModel>(Business.BusinessTrips.Consts.FilterCodes.DestinationPlaceFilter, BusinessTripResources.BusinessTripDestinationPlaceFilterName),
                        new ParametricFilter<BusinessTripCityFilterViewModel>(Business.BusinessTrips.Consts.FilterCodes.DeparturePlaceFilter, BusinessTripResources.BusinessTripDeparturePlaceFilterName)
                    }
                },
                new FilterGroup
                {
                    DisplayName = BusinessTripResources.BusinessTripFrontDeskArrangement,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.FrontDeskArrangedFilter,
                            DisplayName = BusinessTripResources.BusinessTripFrontDeskArrangedFilterName
                        },
                        new Filter
                        {
                            Code = Business.BusinessTrips.Consts.FilterCodes.FrontDeskInArrangementFilter,
                            DisplayName = BusinessTripResources.BusinessTripFrontDeskInArrangementFilterName
                        }
                    }
                },
                new FilterGroup
                {
                    ButtonText = BusinessTripResources.Status,
                    DisplayName = BusinessTripResources.SettlementControllingStatus,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<BusinessTripSettlementControllingStatus>()
                },
            };

            CardIndex.Settings.GridViewConfigurations = GetGridDefaultConfiguration();
        }

        private IList<IGridViewConfiguration> GetGridDefaultConfiguration()
        {
            var isFrontDesk = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanManageBusinessTripFrontDesk);

            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<BusinessTripViewModel>(BusinessTripResources.SimpleViewName, "Simple") { IsDefault = !isFrontDesk };
            configuration.IncludeColumns(new List<Expression<Func<BusinessTripViewModel, object>>>
            {
                c => c.Identifier,
                x => x.ProjectNames,
                x => x.StartedOn,
                x => x.EndedOn,
                x => x.Status,
                x => x.DestinationCityName,
                x => x.FrontDeskAssigneeEmployeeId,
                x => x.Attachments,
                x => x.Settlements,
            });
            configurations.Add(configuration);

            configuration = new GridViewConfiguration<BusinessTripViewModel>(BusinessTripResources.FullViewName, "Full") { IsDefault = isFrontDesk };
            configuration.IncludeColumns(new List<Expression<Func<BusinessTripViewModel, object>>>
            {
                c => c.Identifier,
                x => x.ProjectNames,
                x => x.StartedOn,
                x => x.EndedOn,
                x => x.Status,
                x => x.DepartureCityNames,
                x => x.DestinationCityName,
                x => x.TravelExplanation,
                x => x.ParticipantFullNames,
                x => x.FrontDeskAssigneeEmployeeId,
                x => x.CompanyNames,
                x => x.Settlements,
            });
            configurations.Add(configuration);

            configuration = new GridViewConfiguration<BusinessTripViewModel>(BusinessTripResources.InvoicingViewName, "Invoicing") { IsDefault = false };
            configuration.IncludeColumns(new List<Expression<Func<BusinessTripViewModel, object>>>
            {
                c => c.Identifier,
                x => x.ProjectNames,
                x => x.StartedOn,
                x => x.EndedOn,
                x => x.Status,
                x => x.DepartureCityNames,
                x => x.DestinationCityName,
                x => x.TravelExplanation,
                x => x.FrontDeskAssigneeEmployeeId,
                x => x.CompanyNames,
                x => x.Settlements,
                x => x.ReinvoicingToCustomer,
            });
            configurations.Add(configuration);

            return configurations;
        }

        [BreadcrumbBar("BusinessTripList")]
        public override ActionResult List(GridParameters parameters)
        {
            var currentDate = _timeService.GetCurrentDate();
            var contextDate = GetContextDate();

            if (contextDate.HasValue)
            {
                CardIndex.Settings.ToolbarButtons.InsertRange(0, GetTimeCommandButtons(currentDate, contextDate.Value));
            }

            var context = (BusinessTripContext)parameters.Context;
            context.Month = contextDate != null ? (byte)contextDate.Value.Month : (byte)0;
            context.Year = contextDate != null ? contextDate.Value.Year : 0;

            return base.List(parameters);
        }

        private IEnumerable<CommandButton> GetTimeCommandButtons(DateTime currentDate, DateTime contextDate)
        {
            yield return new ToolbarButton
            {
                Text = LayoutResources.PreviousMonthShort,
                Icon = FontAwesome.CaretLeft,
                OnClickAction = GetUriActionForDate(contextDate.AddMonths(-1)),
                Group = "nextPrev"
            };

            yield return new ToolbarButton
            {
                Text = LayoutResources.CurrentMonth,
                Icon = FontAwesome.MapMarker,
                OnClickAction = GetUriActionForDate(currentDate),
                Group = "nextPrev"
            };

            yield return new ToolbarButton
            {
                Text = LayoutResources.NextMonthShort,
                Icon = FontAwesome.CaretRight,
                OnClickAction = GetUriActionForDate(contextDate.AddMonths(1)),
                Group = "nextPrev"
            };
        }

        private UriAction GetUriActionForDate(DateTime? date)
        {
            IDictionary<string, string> parameters =
                UrlFieldsHelper.GetObjectPropertyToUrlFieldMapping(typeof(BusinessTripContext));

            var query = new NameValueCollection(Url.RequestContext.HttpContext.Request.QueryString);

            if (date.HasValue)
            {
                query[parameters[nameof(BusinessTripContext.Year)]] = date.Value.Year.ToString();
                query[parameters[nameof(BusinessTripContext.Month)]] = date.Value.Month.ToString();
            }
            else
            {
                query.Remove(parameters[nameof(BusinessTripContext.Year)]);
                query.Remove(parameters[nameof(BusinessTripContext.Month)]);
            }

            return Url.UriActionGet(nameof(List), KnownParameter.None,
                query.ToDictionary().SelectMany(p => new[] { p.Key, p.Value.ToString() }).ToArray());
        }

        public DateTime? GetContextDate()
        {
            var filterParameterValue = Request.QueryString["filter"] ?? string.Empty;
            var filters = filterParameterValue.Split(',');

            if (filters.Contains(Business.BusinessTrips.Consts.FilterCodes.AllMode))
            {
                return null;
            }

            var contextDate =
                (Context.Month == 0 || Context.Year == 0)
                    ? (DateTime?)null
                    : DateHelper.BeginningOfMonth(Context.Year, Context.Month);

            if (contextDate == null && filters.Contains(Business.BusinessTrips.Consts.FilterCodes.MonthlyMode))
            {
                return _timeService.GetCurrentDate();
            }

            return contextDate;
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAssignFrontDeskResponsibility)]
        public ActionResult AssignFrontDeskResponsibility(long[] businessTripIds, long employeeId)
        {
            _businessTripService.AssignFrontDeskResponsibility(businessTripIds, employeeId);

            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanGenerateBusinessTripSettlementReport)]
        public ActionResult GenerateSettlementReports(long[] ids)
        {
            var parameters = new List<BusinessTripSettlementDataSourceParametersViewModel>();

            foreach (var businessTripId in ids)
            {
                parameters.AddRange(
                    _businessTripService.GetBusinessTripParticipantIdsOrDefaultById(businessTripId, p => p.SettlementRequests.Any())
                        .Select(id => new BusinessTripSettlementDataSourceParametersViewModel
                        {
                            BusinessTripId = businessTripId,
                            BusinessTripParticipantId = id
                        })
                    );
            }

            var currentPrincipal = GetCurrentPrincipal();

            if (BusinessTripSettlementDataSource.ShouldDataBeLimitedToOwnSettlements(currentPrincipal))
            {
                var participantIds = _businessTripParticipantsService.GetParticipantIdsByEmployeeId(currentPrincipal.EmployeeId);

                parameters = parameters.Where(p => participantIds.Contains(p.BusinessTripParticipantId)).ToList();
            }

            if (!parameters.Any())
            {
                AddAlert(AlertType.Information, BusinessTripResources.EmptyReportData);

                return Redirect(CardIndexHelper.GetReturnOrListUrl(this, Context));
            }

            var reports = parameters
                .Select(p => new
                {
                    Parameters = p,
                    ReportCode = _businessTripParticipantsService.GetSettlementReportCodeByParticipantId(p.BusinessTripParticipantId)
                })
                .Select(p => _reportingService.PerformReport(p.ReportCode, p.Parameters).Render())
                .Cast<ArchivableFileContentResult>()
                .ToList();

            var report = reports.Count == 1
                ? reports.Single()
                : new ArchivableFileContentResult(ZipArchiveHelper.GetZipArchiveBytes(reports), MimeHelper.ZipFile)
                {
                    FileDownloadName = ZipArchiveHelper.GetZipFileName(BusinessTripSettlementReportHelper.ZipFileName)
                };

            return new FileContentResult(report.Content, report.ContentType)
            {
                FileDownloadName = report.FileName
            };
        }
    }
}
