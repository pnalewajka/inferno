using System.Web.Routing;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripAdvancedPayments)]
    public class BusinessTripAdvancedPaymentController 
        : BusinessTripTabReadOnlyCardIndexController<BusinessTripAdvancedPaymentRequestViewModel, BusinessTripAdvancedPaymentRequestDto>
    {
        public BusinessTripAdvancedPaymentController(
            IBusinessTripAdvancedPaymentCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            var requestAdvancePaymentUrl = Url.Action(nameof(RequestController.Add), RoutingHelper.GetControllerName<RequestController>(),
                new RouteValueDictionary
                {
                    { RoutingHelper.AreaParameterName, RoutingHelper.GetAreaName<RequestController>() },
                    { RequestControllerHelper.RequestTypeUrlParameterName, NamingConventionHelper.ConvertPascalCaseToHyphenated(RequestType.AdvancedPaymentRequest.ToString()) },
                    { NamingConventionHelper.ConvertPascalCaseToHyphenated(nameof(AdvancedPaymentRequestViewModel.BusinessTripId)), Context.ParentId.ToString() }
                });

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            var requestAdvancePaymentButton = new ToolbarButton
            {
                Text = BusinessTripResources.RequestAdvancePaymentButton,
                RequiredRole = SecurityRoleType.CanRequestAdvancedPaymentRequest,
                OnClickAction = new UriAction(requestAdvancePaymentUrl),
            };
            CardIndex.Settings.ToolbarButtons.Add(requestAdvancePaymentButton);

            CardIndex.Settings.ContextSettings = new CustomContextSettings("~/Areas/BusinessTrips/Views/Shared/BusinessTripContext.cshtml");
            CardIndex.Settings.EmptyListMessage = BusinessTripResources.EmptyAdvancedPaymentsMessage;
            CardIndex.Settings.Title = BusinessTripResources.AdvancedPaymentsTitle;

            SetDoubleClickAction();
        }

        private void SetDoubleClickAction()
        {
            var doubleClickUrl = Url.Action(
                nameof(RequestController.View),
                RoutingHelper.GetControllerName<RequestController>(),
                new { Area = RoutingHelper.GetAreaName<RequestController>() });

            CardIndex.Settings.OnRowDoubleClickedJavaScript = $"Forms.redirect('{doubleClickUrl}/' + rowData.id)";
        }

        protected override void AddGoToParentButton(object context)
        {
        }
    }
}
