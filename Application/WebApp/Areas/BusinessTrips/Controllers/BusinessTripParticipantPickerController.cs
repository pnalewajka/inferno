using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [Identifier("ValuePickers.BusinessTripParticipant")]
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripParticipants)]
    public class BusinessTripParticipantPickerController :
        ReadOnlyCardIndexController<BusinessTripParticipantViewModel, BusinessTripParticipantDto, ParticipantPickerContext>
    {
        public BusinessTripParticipantPickerController(IBusinessTripParticipantCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<BusinessTripParticipantViewModel>(BusinessTripResources.MinimalViewText, "minimal") { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<BusinessTripParticipantViewModel, object>>>
            {
                p => p.EmployeeId,
                p => p.CompanyId,
                p => p.DepartureCityId,
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}