﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripArrangements)]
    public class BusinessTripArrangementsController : BusinessTripTabController<object>
    {
        private readonly IBusinessTripArrangementsService _businessTripArrangementsService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;
        private readonly IBusinessTripSettlementService _businessTripSettlementService;

        private readonly IClassMappingFactory _classMappingFactory;

        public BusinessTripArrangementsController(
            IBaseControllerDependencies baseDependencies,
            IBusinessTripService businessTripService,
            IBusinessTripArrangementsService businessTripArrangementsService,
            IBusinessTripParticipantsService businessTripParticipantsService,
            IBusinessTripSettlementService businessTripSettlementService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider)
            : base(baseDependencies, businessTripService, principalProvider)
        {
            _businessTripArrangementsService = businessTripArrangementsService;
            _businessTripParticipantsService = businessTripParticipantsService;
            _businessTripSettlementService = businessTripSettlementService;

            _classMappingFactory = classMappingFactory;

            Layout.EnumResources.Add<ArrangementType>();
            Layout.Resources.AddFrom<ArrangementsResources>();
        }

        private JsonResult HandleRecalculationRequired(Action action)
        {
            if (!Context.RecalculateApproved && HasAnySettlementRequest())
            {
                var stopResults = JsonHelper.Serialize(
                    new
                    {
                        HasAnySettlementRequest = true,
                        Successful = false,
                        Arrangements = GetArrangements(),
                    },
                    JsonHelper.DefaultAjaxSettings);

                return Json(stopResults);
            }

            action();

            var results = JsonHelper.Serialize(
                new
                {
                    Arrangements = GetArrangements(),
                    Successful = true,

                },
                JsonHelper.DefaultAjaxSettings);

            return Json(results);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanDeleteBusinessTripArrangements)]
        public JsonResult RemoveArrangements(long arrangementId)
        {
            return HandleRecalculationRequired(() =>
            {
                _businessTripArrangementsService.RemoveArrangement(arrangementId);
            });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripArrangements)]
        public JsonResult SaveArrangements(List<BusinessTripArrangementSaveContract> arrangements)
        {
            return HandleRecalculationRequired(() =>
            {
                _businessTripArrangementsService.SaveBusinessTripArrangements(Context.ParentId, arrangements ?? new List<BusinessTripArrangementSaveContract>());
            });
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripArrangements)]
        public List<BusinessTripArrangementViewModel> GetArrangements()
        {
            IsBusinessTripAccessible();

            var currenceyMapping = _classMappingFactory.CreateMapping<CurrencyDto, CurrencyViewModel>();

            return _businessTripArrangementsService.GetBusinessTripArrangements(Context.ParentId)
                .Select(a => new BusinessTripArrangementViewModel
                {
                    Id = a.Id,
                    ArrangementType = a.ArrangementType,
                    Name = a.Name,
                    Details = a.Details,
                    IssuedOn = a.IssuedOn,
                    Value = a.Value,
                    Participants = a.Participants?.Select(p => new BusinessTripDisplayParticipantViewModel
                    {
                        Id = p.Id,
                        EmployeeId = p.EmployeeId,
                        EmployeeName = p.EmployeeName
                    }).ToList(),
                    Currency = a.Currency != null ? currenceyMapping.CreateFromSource(a.Currency) : null,
                    ArrangementDocuments = a.ArrangementDocuments?.Select(d => new ArrangementDocumentViewModel
                    {
                        Id = d.Id,
                        Name = d.Name
                    }).ToList(),
                    Timestamp = a.Timestamp
                }).ToList();
        }

        public bool HasAnySettlementRequest()
        {
            return _businessTripSettlementService.HasAnySettlementRequest(Context.ParentId);
        }

        public List<BusinessTripParticipantViewModel> GetParticipants()
        {
            var participants = _businessTripParticipantsService.GetBusinessTripParticipants(Context.ParentId);
            var participantMapping = _classMappingFactory.CreateMapping<BusinessTripParticipantDto, BusinessTripParticipantViewModel>();

            return participants.Select(participantMapping.CreateFromSource).ToList();
        }

        [AtomicAuthorize(SecurityRoleType.CanManageBusinessTripArrangements)]
        [BreadcrumbBar("BusinessTripArrangements")]
        public ActionResult Index()
        {
            var arrangements = GetArrangements();
            var participants = GetParticipants();
            var hasAnySettlementRequest = HasAnySettlementRequest();
            var canDeleteArrangements = PrincipalProvider.Current.IsInRole(SecurityRoleType.CanDeleteBusinessTripArrangements);

            Layout.PageTitle = ArrangementsResources.PageTitle;

            return TabView(new
            {
                HasAnySettlementRequest = hasAnySettlementRequest,
                Arrangements = arrangements,
                Participants = participants,
                BusinessTripId = Context.ParentId,
                CanDeleteArrangements = canDeleteArrangements
            });
        }
    }
}
