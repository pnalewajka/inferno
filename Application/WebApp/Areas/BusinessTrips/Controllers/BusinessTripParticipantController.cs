﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripParticipants)]
    public class BusinessTripParticipantController
        : BusinessTripTabCardIndexController<BusinessTripParticipantViewModel, BusinessTripParticipantDto>
    {
        private readonly ICityService _cityService;
        private readonly ITimeService _timeService;
        private readonly IEmployeeService _employeeService;
        private readonly IBusinessTripService _businessTripService;
        private readonly IEmploymentPeriodService _employmentPeriodService;

        public BusinessTripParticipantController(
            ICityService cityService,
            ITimeService timeService,
            IEmployeeService employeeService,
            IBusinessTripService businessTripService,
            IEmploymentPeriodService employmentPeriodService,
            IBusinessTripParticipantCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            _cityService = cityService;
            _timeService = timeService;
            _employeeService = employeeService;
            _businessTripService = businessTripService;
            _employmentPeriodService = employmentPeriodService;

            CardIndex.Settings.Title = BusinessTripResources.ParticipantsTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddBusinessTripParticipants;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditBusinessTripParticipants;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteBusinessTripParticipants;
            CardIndex.Settings.ContextSettings = new CustomContextSettings("~/Areas/BusinessTrips/Views/Shared/BusinessTripContext.cshtml");

            CardIndex.Settings.DefaultNewRecord = () =>
            {
                var record = cardIndexDataService.GetDefaultNewRecord(Context.ParentId);
                var mapping = dependencies.ClassMappingFactory
                    .CreateMapping<BusinessTripParticipantDto, BusinessTripParticipantViewModel>();

                record.BusinessTripId = Context.ParentId;

                return mapping.CreateFromSource(record);
            };

            CardIndex.Settings.AddFormCustomization = SetControlsVisibility;
            CardIndex.Settings.EditFormCustomization = form =>
            {
                SetControlsVisibility(form);

                form.GetControlByName<BusinessTripParticipantViewModel>(v => v.EmployeeId).IsReadOnly = true;
            };
            
            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/BusinessTripParticipant.js");
        }

        [HttpPost]
        public JsonNetResult UpdateEmployeePersonalInformation(BusinessTripParticipantViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<BusinessTripParticipantViewModel>();
            var employee = _employeeService.GetEmployeeOrDefaultById(viewModel.EmployeeId);

            if (employee != null)
            {
                var cultureInfo = CultureInfo.CurrentCulture;
                var employmentPeriod = _employmentPeriodService.GetEmploymentPeriod(employee.Id, _timeService.GetCurrentTime());
                var city = employee.LocationId.HasValue ? _cityService.GetCityOrDefaultByLocationId(employee.LocationId.Value) : null;

                if (city == null)
                {
                    throw new BusinessException(BusinessTripResources.ParticipantWithoutLocationError);
                }

                response.Operations.Add(SetOperation(
                    v => v.CompanyId,
                    employmentPeriod?.CompanyId == null ? null : new ValuePickerResponseViewModel
                    {
                        ElementId = employmentPeriod.CompanyId.Value,
                        ElementDescription = employmentPeriod.CompanyName
                    }
                ));

                response.Operations.Add(SetOperation(
                    v => v.ContractTypeId,
                    employmentPeriod?.ContractTypeId == null ? null : new ValuePickerResponseViewModel
                    {
                        ElementId = employmentPeriod.ContractTypeId,
                        ElementDescription = employmentPeriod.ContractTypeName.ToString()
                    }
                ));

                response.Operations.Add(SetOperation(
                    v => v.DepartureCityId,
                    new ValuePickerResponseViewModel
                    {
                        ElementId = city.Id,
                        ElementDescription = $"{city.Name}, {city.CountryName}",
                    }));

                response.Operations.Add(EditableOperation(v => v.DateOfBirth, !employee.DateOfBirth.HasValue));
                response.Operations.Add(EditableOperation(v => v.PersonalNumber, string.IsNullOrEmpty(employee.PersonalNumber)));
                response.Operations.Add(EditableOperation(v => v.PersonalNumberType, !employee.PersonalNumberType.HasValue));
                response.Operations.Add(SetOperation(v => v.DateOfBirth, employee.DateOfBirth?.ToString("d", cultureInfo)));
                response.Operations.Add(SetOperation(v => v.PersonalNumber, employee.PersonalNumber));
                response.Operations.Add(SetOperation(v => v.PersonalNumberType, employee.PersonalNumberType.ToString()));
                response.Operations.Add(SetOperation(v => v.IdCardNumber, employee.IdCardNumber));
                response.Operations.Add(SetOperation(v => v.PhoneNumber, employee.PhoneNumber));
                response.Operations.Add(SetOperation(v => v.HomeAddress, employee.HomeAddress));
            }

            return response.ToJsonNetResult();
        }

        [HttpPost]
        public JsonNetResult UpdateLuggageAndTaxiVoucherAvailability(BusinessTripParticipantViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<BusinessTripParticipantViewModel>();
            var businessTrip = _businessTripService.GetBusinessTripOrDefaultById(viewModel.BusinessTripId);
            var departureCity = _cityService.GetCityOrDefaultById(viewModel.DepartureCityId);
            var destinationCity = _cityService.GetCityOrDefaultById(businessTrip.DestinationCityId);

            response.Operations.Add(new ClientSideVisibilityOperation<BusinessTripParticipantViewModel>(
                v => v.LuggageDeclarations, viewModel.TransportationPreferences.HasFlag(MeansOfTransport.Plane)));

            response.Operations.Add(new ClientSideVisibilityOperation<BusinessTripParticipantViewModel>(
                v => v.DepartureCityTaxiVouchers,
                (departureCity?.IsVoucherServiceAvailable ?? false) && viewModel.TransportationPreferences.HasFlag(MeansOfTransport.Taxi)));

            response.Operations.Add(new ClientSideVisibilityOperation<BusinessTripParticipantViewModel>(
                v => v.DestinationCityTaxiVouchers,
                (destinationCity?.IsVoucherServiceAvailable ?? false) && viewModel.TransportationPreferences.HasFlag(MeansOfTransport.Taxi)));

            return response.ToJsonNetResult();
        }

        protected override void AddGoToParentButton(object context)
        {
        }

        private void SetControlsVisibility(FormRenderingModel form)
        {
            var businessTrip = _businessTripService.GetBusinessTripOrDefaultById(Context.ParentId);
            var viewModel = form.GetViewModel<BusinessTripParticipantViewModel>();

            var departureVisibility =
                IsVoucherServiceAvaliable(viewModel.DepartureCityId)
                    ? ControlVisibility.Show : ControlVisibility.Hide;
            var destinationVisibility =
                IsVoucherServiceAvaliable(businessTrip.DestinationCityId)
                    ? ControlVisibility.Show : ControlVisibility.Hide;

            form.GetControlByName<BusinessTripParticipantViewModel>(v => v.DepartureCityTaxiVouchers).Visibility = departureVisibility;
            form.GetControlByName<BusinessTripParticipantViewModel>(v => v.DestinationCityTaxiVouchers).Visibility = destinationVisibility;

            var employee = _employeeService.GetEmployeeOrDefaultById(viewModel.EmployeeId);

            if (employee != null)
            {
                form.GetControlByName<BusinessTripParticipantViewModel>(
                    v => v.DateOfBirth).IsReadOnly = employee.DateOfBirth.HasValue;
                form.GetControlByName<BusinessTripParticipantViewModel>(
                    v => v.PersonalNumber).IsReadOnly = !string.IsNullOrEmpty(employee.PersonalNumber);
                form.GetControlByName<BusinessTripParticipantViewModel>(
                    v => v.PersonalNumberType).IsReadOnly = employee.PersonalNumberType.HasValue;
            }
        }

        private bool IsVoucherServiceAvaliable(long destinationCityId)
        {
            return _cityService.GetCityOrDefaultById(destinationCityId)?.IsVoucherServiceAvailable ?? false;
        }

        private static ClientSideValuePickerOperation<BusinessTripParticipantViewModel> SetOperation(
            Expression<Func<BusinessTripParticipantViewModel, object>> resolver, ValuePickerResponseViewModel model)
                =>  new ClientSideValuePickerOperation<BusinessTripParticipantViewModel>(resolver, model);

        private static ClientSideValueChangeOperation<BusinessTripParticipantViewModel> SetOperation(
            Expression<Func<BusinessTripParticipantViewModel, object>> resolver, string value)
                => new ClientSideValueChangeOperation<BusinessTripParticipantViewModel>(resolver, value);

        private static ClientSideEditableOperation<BusinessTripParticipantViewModel> EditableOperation(
            Expression<Func<BusinessTripParticipantViewModel, object>> resolver, bool isEditable)
                => new ClientSideEditableOperation<BusinessTripParticipantViewModel>(resolver, isEditable);
    }
}
