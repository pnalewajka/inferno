﻿using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripAcceptanceConditions)]
    public class BusinessTripAcceptanceConditionsController
        : BusinessTripTabController<BusinessTripAcceptanceConditionsViewModel>
    {
        private readonly IBusinessTripAcceptanceConditionsService _businessTripAcceptanceConditionsService;
        private IBaseControllerDependencies _baseControllerDependencies;


        public BusinessTripAcceptanceConditionsController(
            IBaseControllerDependencies baseDependencies,
            IBusinessTripService businessTripService,
            IPrincipalProvider principalProvider,
            IBusinessTripAcceptanceConditionsService businessTripAcceptanceConditionsService)
            : base(baseDependencies, businessTripService, principalProvider)
        {
            _businessTripAcceptanceConditionsService = businessTripAcceptanceConditionsService;
            _baseControllerDependencies = baseDependencies;

            Layout.Css.Add("~/Areas/BusinessTrips/Content/AcceptanceConditions.css");
            Layout.Scripts.Add("~/Areas/Allocation/Scripts/AcceptanceConditionsHandler.js");
        }

        [HttpGet]
        public ActionResult Edit()
        {
            Layout.PageTitle = BusinessTripResources.AcceptanceConditions;

            var acceptanceConditions = _businessTripAcceptanceConditionsService.GetAcceptanceConditionsForBusinessTrip(Context.ParentId);
            var mapping = _baseControllerDependencies.ClassMappingFactory.CreateMapping<BusinessTripAcceptanceConditionsDto, BusinessTripAcceptanceConditionsViewModel>();

            return TabView("~/Areas/BusinessTrips/Views/AcceptanceConditions/Index.cshtml", mapping.CreateFromSource(acceptanceConditions));
        }

        [HttpPost]
        public ActionResult Edit(BusinessTripAcceptanceConditionsViewModel model)
        {
            var mapping = _baseControllerDependencies.ClassMappingFactory.CreateMapping<BusinessTripAcceptanceConditionsViewModel, BusinessTripAcceptanceConditionsDto>();

            var result = _businessTripAcceptanceConditionsService.ChangeAcceptanceConditionsForBusinessTrip(Context.ParentId, mapping.CreateFromSource(model));

            AddAlerts(result.Alerts);

            if (result.IsSuccessful)
            {
                return RedirectToAction("Edit", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, Context.ParentId } });
            }
            else
            {
                return TabView("~/Areas/BusinessTrips/Views/AcceptanceConditions/Index.cshtml", model);
            }
        }
    }
}
