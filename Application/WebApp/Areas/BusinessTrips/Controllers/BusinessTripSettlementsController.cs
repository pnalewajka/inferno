﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Helpers;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.ReportDataSources;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripSettlements)]
    public class BusinessTripSettlementsController
        : BusinessTripTabCardIndexController<BusinessTripSettlementViewModel, BusinessTripSettlementDto>
    {
        private readonly IBaseControllerDependencies _baseControllerDependencies;
        private readonly IRequestWorkflowService _requestWorkflowService;
        private readonly IBusinessTripSettlementService _settlementService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;
        private readonly IReportingService _reportingService;

        public BusinessTripSettlementsController(
            IBusinessTripService businessTripService,
            IBusinessTripSettlementsCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IRequestWorkflowService requestWorkflowService,
            IBusinessTripSettlementService settlementService,
            IBusinessTripParticipantsService businessTripParticipantsService,
            IReportingService reportingService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _baseControllerDependencies = baseControllerDependencies;
            _requestWorkflowService = requestWorkflowService;
            _settlementService = settlementService;
            _reportingService = reportingService;
            _businessTripParticipantsService = businessTripParticipantsService;

            CardIndex.Settings.Title = BusinessTripResources.SettlementsTitle;

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.ContextSettings = new CustomContextSettings("~/Areas/BusinessTrips/Views/Shared/BusinessTripContext.cshtml");

            SetupRowButtons();

            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/BusinessTripSettlements.js");

            Layout.Resources.AddFrom<BusinessTripSettlementsResources>();
        }

        protected override void AddGoToParentButton(object context)
        {
        }

        private void SetupRowButtons()
        {
            var area = RoutingHelper.GetAreaName<RequestController>();
            var controller = RoutingHelper.GetControllerName<RequestController>();
            var currentController = RoutingHelper.GetControllerName(this);

            var currentUser = _baseControllerDependencies.PrincipalProvider.Current;

            var roles = _baseControllerDependencies.PrincipalProvider.Current.Roles.ToList();

            var canManageOwnBusinessTripSettlements = currentUser.IsInRole(SecurityRoleType.CanManageOwnBusinessTripSettlements);
            var canManageOthersBusinessTripSettlements = currentUser.IsInRole(SecurityRoleType.CanManageOthersBusinessTripSettlements);
            var canChangeControllingStatus = currentUser.IsInRole(SecurityRoleType.CanChangeSettlementRequestControllingStatus);
            var canGenerateSettlementReport = currentUser.IsInRole(SecurityRoleType.CanGenerateBusinessTripSettlementReport);
            var canAddRequest = currentUser.IsInRole(SecurityRoleType.CanAddRequest);
            var canViewRequest = currentUser.IsInRole(SecurityRoleType.CanViewRequest);
            var canEditRequest = currentUser.IsInRole(SecurityRoleType.CanEditRequest);
            var canCreateEmptySettlementRequest = currentUser.IsInRole(SecurityRoleType.CanCreateEmptySettlementRequest);

            var employeeId = _baseControllerDependencies.PrincipalProvider.Current.EmployeeId;

            if (canManageOwnBusinessTripSettlements || canManageOthersBusinessTripSettlements)
            {
                if (canAddRequest)
                {
                    CardIndex.Settings.RowButtons.Add(new RowButton
                    {
                        Text = BusinessTripSettlementsResources.CreateSettlementRequestButton,
                        OnClickAction = new JavaScriptCallAction($"BusinessTrip.Settlements.CreateSettlementRequest(this.rowData[0], '{Url.Action(nameof(RequestController.Add), controller, new { area })}')"),
                        Icon = FontAwesome.Plus,
                        Visibility = CommandButtonVisibility.Grid,
                        Availability = new RowButtonAvailability<BusinessTripSettlementViewModel>
                        {
                            Predicate = m => m.Id == null
                                && ((m.EmployeeId == employeeId && canManageOwnBusinessTripSettlements) || (m.EmployeeId != employeeId && canManageOthersBusinessTripSettlements))
                        }
                    });
                }

                CardIndex.Settings.RowButtons.Add(new RowButton
                {
                    Text = RequestResource.Submit,
                    OnClickAction = new JavaScriptCallAction($"BusinessTrip.Settlements.SubmitSettlementRequest(this.rowData[0], '{Url.Action(nameof(RequestController.Submit), controller, new { area })}')"),
                    Icon = FontAwesome.MailForward,
                    Visibility = CommandButtonVisibility.Grid,
                    RequiredRole = SecurityRoleType.CanSubmitRequest,
                    Availability = new RowButtonAvailability<BusinessTripSettlementViewModel>
                    {
                        Predicate = m => m.CanBeSubmitted
                    }
                });

                CardIndex.Settings.RowButtons.Add(new RowButton
                {
                    Text = BusinessTripSettlementsResources.ViewSettlementRequestButton,
                    OnClickAction = new JavaScriptCallAction($"BusinessTrip.Settlements.ViewSettlementRequest(this.rowData[0], '{Url.Action(nameof(RequestController.View), controller, new { area })}')"),
                    Icon = FontAwesome.FileTextO,
                    Visibility = CommandButtonVisibility.Grid,
                    Availability = new RowButtonAvailability<BusinessTripSettlementViewModel>
                    {
                        Predicate = m => m.Id != null && canViewRequest && m.IsRequestAvailable
                    }
                });

                if (canEditRequest)
                {
                    CardIndex.Settings.RowButtons.Add(new RowButton
                    {
                        Text = BusinessTripSettlementsResources.EditSettlementRequestButton,
                        OnClickAction = new JavaScriptCallAction($"BusinessTrip.Settlements.ViewSettlementRequest(this.rowData[0], '{Url.Action(nameof(RequestController.Edit), controller, new { area })}')"),
                        Icon = FontAwesome.Edit,
                        Visibility = CommandButtonVisibility.Grid,
                        Availability = new RowButtonAvailability<BusinessTripSettlementViewModel>
                        {
                            Predicate = m => m.Id != null
                                && (m.RequestStatus == RequestStatus.Draft || m.RequestStatus == RequestStatus.Rejected)
                                && ((m.EmployeeId == employeeId && canManageOwnBusinessTripSettlements) || (m.EmployeeId != employeeId && canManageOthersBusinessTripSettlements))
                                && m.IsRequestAvailable
                        }
                    });
                }

                if (canCreateEmptySettlementRequest)
                {
                    CardIndex.Settings.RowButtons.Add(new RowButton
                    {
                        Text = BusinessTripSettlementsResources.CreateEmptySettlementRequestButton,
                        OnClickAction = new JavaScriptCallAction($"BusinessTrip.Settlements.CreateEmptySettlementRequest(this.rowData[0], '{Url.Action(nameof(CreateEmptySettlementRequest))}','{Context.ParentId}')"),
                        Icon = FontAwesome.Plus,
                        RequiredRole = SecurityRoleType.CanCreateEmptySettlementRequest,
                        Visibility = CommandButtonVisibility.Grid,
                        Availability = new RowButtonAvailability<BusinessTripSettlementViewModel>
                        {
                            Predicate = m => m.Id == null
                        }
                    });
                }

                if (canChangeControllingStatus)
                {
                    CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
                    {
                        Text = BusinessTripSettlementsResources.ChangeControllingStatus,
                        OnClickAction = GetChangeControllingStatusDropdownAction(),
                        Icon = FontAwesome.Edit,
                        RequiredRole = SecurityRoleType.CanChangeSettlementRequestControllingStatus,
                        Visibility = CommandButtonVisibility.Grid,
                        Availability = new ToolbarButtonAvailability
                        {
                            Mode = AvailabilityMode.AtLeastOneRowSelected,
                            Predicate = "row.id != null && row.isOnControllingPhase",
                        },
                    });
                }

                if (canGenerateSettlementReport)
                {
                    CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
                    {
                        Text = BusinessTripResources.GenerateSettlementReports,
                        Availability = new ToolbarButtonAvailability
                        {
                            Mode = AvailabilityMode.AtLeastOneRowSelected,
                            Predicate = "row.id != null",
                        },
                        RequiredRole = SecurityRoleType.CanGenerateBusinessTripSettlementReport,
                        OnClickAction = Url.UriActionGet(nameof(GenerateSettlementReports), RoutingHelper.GetControllerName(this), KnownParameter.SelectedIds | KnownParameter.Context),
                    });
                }
            }
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanCreateEmptySettlementRequest)]
        public ActionResult CreateEmptySettlementRequest(long participantId, long settlementRequestId)
        {
            _settlementService.CreateEmptyRequest(participantId);

            return RedirectToAction(nameof(List), new RouteValueDictionary() { { RoutingHelper.ParentIdParameterName, settlementRequestId } });
        }

        private DropdownAction GetChangeControllingStatusDropdownAction()
        {
            var controllingStatuses = EnumHelper.GetEnumValues<BusinessTripSettlementControllingStatus>();
            var dropdownItems = new List<ICommandButton>();

            foreach (var controllingStatus in controllingStatuses)
            {
                var typeValueName = (long)controllingStatus;

                var addItemButton = new CommandButton
                {
                    RequiredRole = SecurityRoleType.CanChangeSettlementRequestControllingStatus,
                    Text = controllingStatus.GetDescriptionOrValue(),
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.AtLeastOneRowSelected,
                        Predicate = "row.id != null && row.isOnControllingPhase",
                    },
                    OnClickAction = Url.UriActionPost(
                        nameof(ChangeControllingStatus), KnownParameter.SelectedIds,
                        "status", typeValueName.ToString())
                };

                dropdownItems.Add(addItemButton);
            }

            return new DropdownAction
            {
                Items = dropdownItems,
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanChangeSettlementRequestControllingStatus)]
        public ActionResult ChangeControllingStatus(long[] ids, BusinessTripSettlementControllingStatus status)
        {
            _settlementService.UpdateControllingStatus(ids, status);

            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanGenerateBusinessTripSettlementReport)]
        public ActionResult GenerateSettlementReports(long[] ids)
        {
            var parameters = new List<BusinessTripSettlementDataSourceParametersViewModel>();

            foreach (var settlementRequestId in ids)
            {
                var request = _settlementService.GetSettlementRequestByIdOrDefault(settlementRequestId);

                parameters.Add(new BusinessTripSettlementDataSourceParametersViewModel
                {
                    BusinessTripId = request.BusinessTripId,
                    BusinessTripParticipantId = request.BusinessTripParticipantId
                });
            }

            var currentPrincipal = GetCurrentPrincipal();

            if (BusinessTripSettlementDataSource.ShouldDataBeLimitedToOwnSettlements(currentPrincipal))
            {
                var participantIds = _businessTripParticipantsService.GetParticipantIdsByEmployeeId(currentPrincipal.EmployeeId);

                parameters = parameters.Where(p => participantIds.Contains(p.BusinessTripParticipantId)).ToList();
            }

            if (!parameters.Any())
            {
                AddAlert(AlertType.Information, BusinessTripResources.EmptyReportData);

                return Redirect(CardIndexHelper.GetReturnOrListUrl(this, Context));
            }

            var reports = parameters
                .Select(p => _reportingService.PerformReport(BusinessTripSettlementReportHelper.ReportDataSource, p).Render())
                .Cast<ArchivableFileContentResult>()
                .ToList();

            var report = reports.Count == 1
                ? reports.Single()
                : new ArchivableFileContentResult(ZipArchiveHelper.GetZipArchiveBytes(reports), MimeHelper.ZipFile)
                {
                    FileDownloadName = ZipArchiveHelper.GetZipFileName(BusinessTripSettlementReportHelper.ZipFileName)
                };

            return new FileContentResult(report.Content, report.ContentType)
            {
                FileDownloadName = report.FileName
            };
        }
    }
}
