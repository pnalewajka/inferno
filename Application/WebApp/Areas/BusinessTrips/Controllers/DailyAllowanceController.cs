﻿using System.Collections.Generic;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDailyAllowance)]
    public class DailyAllowanceController : CardIndexController<DailyAllowanceViewModel, DailyAllowanceDto>
    {
        public DailyAllowanceController(
            IDailyAllowanceCardIndexDataService dailyAllowanceCardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(dailyAllowanceCardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = DailyAllowanceResources.DailyAllowanceTitle;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDailyAllowance;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDailyAllowance;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewDailyAllowance;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDailyAllowance;

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportDailyAllowance);
            CardIndex.Settings.AddImport<DailyAllowanceViewModel, DailyAllowanceDto>(dailyAllowanceCardIndexDataService,
                DailyAllowanceResources.ImportButtonName, SecurityRoleType.CanImportDailyAllowance);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup()
                {
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<DailyAllowanceCountryFilterViewModel,CountryDto>(FilterCodes.DailyAllowanceCountry, DailyAllowanceResources.EmploymentCountry),
                        new ParametricFilter<DailyAllowanceValidForFilterViewModel>(FilterCodes.DailyAllowanceValidForDate, DailyAllowanceResources.ValidFor),
                    }
                }
            };
        }
    }
}