﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [Identifier("ValuePickers.BusinessTrips")]
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTrips)]
    public class BusinessTripPickerController : CardIndexController<BusinessTripViewModel, BusinessTripDto, BusinessTripContext>
    {
        private const string DefaultViewId = "default";

        public BusinessTripPickerController(IBusinessTripCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = BusinessTripResources.BusinessTripsTitle;
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<BusinessTripViewModel>(BusinessTripResources.DefaultViewText, DefaultViewId) { IsDefault = true };

            configuration.ExcludeAllColumns();

            configuration.IncludeColumns(new List<Expression<Func<BusinessTripViewModel, object>>>
            {
                c => c.ProjectNames,
                c => c.Identifier,
                c => c.StartedOn,
                c => c.EndedOn,
                c => c.Status,
                c => c.DepartureCityNames,
                c => c.DestinationCityName,
                c => c.TravelExplanation,
                c => c.ParticipantFullNames
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}