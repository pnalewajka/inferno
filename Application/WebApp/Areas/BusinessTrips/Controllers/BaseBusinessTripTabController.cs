using System;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    public abstract class BusinessTripTabController<TViewModel> : AtomicController
    {
        protected readonly IBusinessTripService BusinessTripService;
        protected readonly IPrincipalProvider PrincipalProvider;
        private Lazy<ArrangementContext> _context;

        protected BusinessTripTabController(IBaseControllerDependencies baseDependencies,
            IBusinessTripService businessTripService, IPrincipalProvider principalProvider) : base(baseDependencies)
        {
            BusinessTripService = businessTripService;
            PrincipalProvider = principalProvider;

            Layout.Css.Add("~/Areas/BusinessTrips/Content/BusinessTrips.css");
        }

        protected ArrangementContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new Lazy<ArrangementContext>(() =>
                        UrlFieldsHelper.CreateObjectFromQueryParameters<ArrangementContext>(
                            HttpContext.Request.QueryString.ToDictionary(), true));
                }

                return _context.Value;
            }
        }

        protected void IsBusinessTripAccessible()
        {
            if (!BusinessTripService.IsBusinessTripAccessible(Context.ParentId))
            {
                throw new BusinessException(BusinessTripResources.AccessError);
            }
        }

        protected ActionResult TabView(string tabName, TViewModel model)
        {
            var parentController = ReflectionHelper.CreateInstanceWithIocDependencies<BusinessTripController>();

            var viewModel = new BusinessTripTabViewModel<TViewModel>(model)
            {
                ContextSettings =
                    new CustomContextSettings("~/Areas/BusinessTrips/Views/Shared/BusinessTripContext.cshtml"),
                ContextViewModel = parentController.GetViewModelFromContext(Context)
            };

            return View(tabName, viewModel);
        }

        protected ActionResult TabView(TViewModel model)
        {
            return TabView(null, model);
        }
    }
}