﻿using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.BusinessTrips.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBusinessTrips)]
    public class BusinessTripItineraryController
        : BusinessTripTabReadOnlyCardIndexController<BusinessTripViewModel, BusinessTripDto>
    {
        public BusinessTripItineraryController(
            IBusinessTripCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
        }

        [HttpGet]
        [BreadcrumbBar("BusinessTripItinerary")]
        [AtomicAuthorize(SecurityRoleType.CanViewBusinessTripItinerary)]
        public ActionResult Index()
        {
            var result = (ViewResult)View(Context.ParentId);
            var businessTripViewModel = (result.Model as CardIndexRecordViewModel<BusinessTripViewModel>).ViewModelRecord;

            Layout.PageTitle = BusinessTripResources.ItinereryTitle;

            return TabView("~/Areas/BusinessTrips/Views/Itinerary/Itinerary.cshtml", businessTripViewModel);
        }
    }
}
