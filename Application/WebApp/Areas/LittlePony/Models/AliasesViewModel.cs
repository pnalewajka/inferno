﻿using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    public class AliasesViewModel
    {
        public string OrdinaryProperty { get; set; }

        [Alias("DifferentName")]
        public string AliasedProperty { get; set; }
    }
}