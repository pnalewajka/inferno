﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Identifier("Models.PersonModel")]
    public class PersonModel : IValidatableObject
    {
        [StringLength(32)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(32)]
        public string LastName { get; set; }

        public string GetFullName()
        {
            return (string.Format(DialogsDemoResources.PersonModelGetFullNameFormat, FirstName, LastName)).Trim();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FirstName == "Ziggy" && LastName == "Stardust")
            {
                yield return new ValidationResult(DialogsDemoResources.PersonModelValidateZiggyStardustNotAPersonValidationErrorMessage);
            }

            if (LastName == "Bono")
            {
                yield return new PropertyValidationResult(DialogsDemoResources.PersonModelValidateDoNotUseBonoStageName, () => LastName);
            }
        }
    }
}