﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Identifier("RenderersDemoRolePickerFormatter.TestFormatClass")]
    public class RenderersDemoRolePickerFormatter : IListItemFormatter<SecurityRoleViewModel>
    {
        public string Format(SecurityRoleViewModel viewModel)
        {
            return $"{viewModel.Description} ({viewModel.Id})";
        }
    }
}