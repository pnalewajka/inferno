﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Accounts.Services;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.LittlePony.Controllers;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Layout(typeof(InlineLayout))]
    [FieldSetDescription(1, "personal", "PersonalDataLegend", typeof(RenderersDemoResources))]
    [FieldSetDescription(2, "details", "DetailsLegend", typeof(RenderersDemoResources))]
    [FieldSetDescription(3, "entities", "RelatedDataLegend", typeof(RenderersDemoResources))]
    public class InlineLayoutSampleModel : IValidatableObject
    {
        [MinValue(0)]
        [MaxValue(15)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FavoriteIntegerLabel), typeof(RenderersDemoResources))]
        public int FavoriteInteger { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.FavoriteRealLabel), typeof(RenderersDemoResources))]
        [Render(ControlCssClass = "great-stuff")]
        public double FavoriteReal { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FirstNameLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Render(Size = Size.Small, ControlCssClass = "form-group-lg")]
        [FieldSet("personal")]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.LastNameLabel), typeof(RenderersDemoResources))]
        [Order(0)]
        [StringLength(15, ErrorMessageResourceName = "LastNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(RenderersDemoResources))]
        [FieldSet("personal")]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.PublicNumberLabel), typeof(RenderersDemoResources))]
        [Prompt("PinPlaceholder", typeof(RenderersDemoResources))]
        [Required]
        [StringLength(34)]
        [FieldSet("details")]
        public string PublicIdentificationNumber { get; set; }

        public bool IsPublicIdentificationNumberReadOnly => false;

        [Multiline(4)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.DescriptionLabel), typeof(RenderersDemoResources))]
        [FieldSet("details")]
        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [Range(typeof(DateTime), "1/1/2012", "1/1/2019")]
        [DisplayNameLocalized(nameof(RenderersDemoResources.BirthDateLabel), typeof(RenderersDemoResources))]
        [FieldSet("personal")]
        public DateTime BirthDate { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.IsMarriedLabel), typeof(RenderersDemoResources))]
        public bool IsMarried { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.MotherLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), DialogWidth = "900px")]
        [FieldSet("entities")]
        public long Mother { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.FatherLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url +'&parent-id=3'")]
        [FieldSet("entities")]
        public long? Father { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.ChildrenLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnSelectionChangedJavaScript = "alert('Children='+$(control).find('#children').val());")]
        [FieldSet("entities")]
        [CannotBeEmpty]
        public long[] Children { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.ChildrenAdvancedLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserDemoPickerController), OnSelectionChangedJavaScript = "alert('ChildrenAdvanced='+$(control).find('#children-advanced').val());")]
        [FieldSet("entities")]
        public long[] ChildrenAdvanced { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.SecurityRoleLabel), typeof(RenderersDemoResources))]
        [FieldSet("entities")]
        public SecurityRoleType SecurityRole { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.SecurityRole2Label), typeof(RenderersDemoResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SecurityRoleType>))]
        [FieldSet("entities")]
        public SecurityRoleType SecurityRole2 { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.PersonalAddress), typeof(RenderersDemoResources))]
        public SampleAddressModel PersonalAddress { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.WorkAddress), typeof(RenderersDemoResources))]
        public SampleAddressModel WorkAddress { get; set; }

        [ColorPicker(ColorPickerFormat.Hex)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.ColorLabel), typeof(RenderersDemoResources))]
        public string Color { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.RolePickerLabel), typeof(RenderersDemoResources))]
        [TypeAhead(Type = typeof(RolePickerController), FormFormatterType = typeof(RenderersDemoRolePickerFormatter))]
        public long? Role { get; set; }

        [CannotBeEmpty]
        [DisplayNameLocalized(nameof(RenderersDemoResources.RolePickerLabel), typeof(RenderersDemoResources))]
        [TypeAhead(Type = typeof(RolePickerController), MinimumInputLength = 1)]
        public long[] RoleList { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.LocalizedString), typeof(RenderersDemoResources))]
        [Required]
        public LocalizedString LocalizedString { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.OptionalUserWithChanges), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public Optional<long?> OptionalUserWithChanges { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.OptionalUserWithoutChanges), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public Optional<long?> OptionalUserWithoutChanges { get; set; }

        public static InlineLayoutSampleModel CreateSample()
        {
            return new InlineLayoutSampleModel
            {
                FirstName = "Joe",
                LastName = "Doe",
                PublicIdentificationNumber = "132",
                Mother = 1,
                Children = new long[] {1, 2},
                Description = "Quite\r\na\r\ngentleman",
                BirthDate = DateTime.Now,
                SecurityRole = EnumHelper.GetOutOfRangeValue<SecurityRoleType>(),
                FavoriteInteger = 11,
                FavoriteReal = Math.PI,
                WorkAddress = new SampleAddressModel(),
                PersonalAddress = new SampleAddressModel(),
                Color = "#CCCCCC",
                LocalizedString = new LocalizedString
                {
                    Polish = "To jest tekst napisany w języku polskim",
                    English = "This is text written in English"
                },
                OptionalUserWithChanges = new Optional<long?>
                {
                    IsSet = true,
                    Value = 1L
                },
                OptionalUserWithoutChanges = new Optional<long?>
                {
                    IsSet = false,
                    Value = null
                }
            };
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (!string.IsNullOrEmpty(LastName) && !LastName.EndsWith("a"))
            {
                issues.Add(new ValidationResult("First name must ends with 'a'", PropertyHelper.GetPropertyNames<SimpleLayoutSampleModel>(m=>m.LastName)));
            }

            return issues;
        }
    }
}