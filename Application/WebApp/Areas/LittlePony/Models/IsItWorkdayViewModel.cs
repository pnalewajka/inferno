﻿using System;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    public class IsItWorkdayViewModel
    {
        public DateTime Day { get; set; }

        public bool IsWorkday { get; set; }
    }
}