﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Layout(typeof(SimpleLayout))]
    [TabDescription(0, GeneralTab, nameof(RenderersDemoResources.GeneralTabName), typeof(RenderersDemoResources))]
    [TabDescription(1, AdditionalTab, nameof(RenderersDemoResources.AdditionalTabName), typeof(RenderersDemoResources))]
    [TabDescription(2, ThirdTab, nameof(ThirdTabName))]
    public class TabbedSampleModel
    {
        private const string GeneralTab = "general";
        private const string AdditionalTab = "additional";
        private const string ThirdTab = "third";

        public long Id { get; set; }

        [Tab(AdditionalTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FavoriteIntegerLabel), typeof(RenderersDemoResources))]
        public long FavoriteInteger { get; set; }

        [Tab(AdditionalTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FavoriteRealLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public decimal FavoriteReal { get; set; }

        [Order(1)]
        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FirstNameLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Render(Size = Size.Small, ControlCssClass = "form-group-lg")]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.LastNameLabel), typeof(RenderersDemoResources))]
        [Tab(GeneralTab)]
        [Order(0)]
        [StringLength(15, ErrorMessageResourceName = "LastNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(RenderersDemoResources))]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.PublicNumberLabel), typeof(RenderersDemoResources))]
        [Prompt("PinPlaceholder", typeof(RenderersDemoResources))]
        [Required]
        [StringLength(34)]
        [Tab(AdditionalTab)]
        public string PublicIdentificationNumber { get; set; }

        [Tab(AdditionalTab)]
        public bool IsPublicIdentificationNumberReadOnly => false;

        [Multiline(4)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.DescriptionLabel), typeof(RenderersDemoResources))]
        [Tab(ThirdTab)]
        public string Description { get; set; }

        [Range(typeof(DateTime), "1/1/2012", "1/1/2019")]
        [Tab(AdditionalTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.BirthDateLabel), typeof(RenderersDemoResources))]
        public DateTime BirthDate { get; set; }

        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.IsMarriedLabel), typeof(RenderersDemoResources))]
        public bool IsMarried { get; set; }

        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.MotherLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), DialogWidth = "900px")]
        public long Mother { get; set; }

        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FatherLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url +'&parent-id=3'")]
        public long? Father { get; set; }

        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.ChildrenLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnSelectionChangedJavaScript = "alert('Children='+$(control).find('#children').val());", DialogWidth = "80%")]
        public long[] Children { get; set; }

        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.SecurityRoleLabel), typeof(RenderersDemoResources))]
        public SecurityRoleType SecurityRole { get; set; }

        [Tab(GeneralTab)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.SecurityRole2Label), typeof(RenderersDemoResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SecurityRoleType>))]
        public SecurityRoleType SecurityRole2 { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ThirdTabName { get { return $"Tab @ {DateTime.Now}"; } }

        public TabbedSampleModel()
        {
            Id = 123;
            FirstName = "Joe";
            LastName = "Doe";
            PublicIdentificationNumber = "132";
            Mother = 1;
            Children = new long[] { 1, 2 };
            Description = "Quite\r\na\r\ngentleman";
            BirthDate = DateTime.Now;
            SecurityRole = EnumHelper.GetOutOfRangeValue<SecurityRoleType>();
            FavoriteInteger = 8;
            FavoriteReal = (decimal)Math.PI;
        }
    }
}