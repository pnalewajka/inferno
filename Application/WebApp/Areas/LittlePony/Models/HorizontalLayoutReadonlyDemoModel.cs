﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Accounts.Services;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Controllers;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Layout(typeof(HorizontalFormLayout))]
    public class HorizontalLayoutReadonlyDemoModel
    {
        [DisplayNameLocalized(nameof(RenderersDemoResources.FavoriteIntegerLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Range(0, 100)]
        public long? FavoriteInteger { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.FavoriteRealLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public decimal FavoriteReal { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.FirstNameLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Render(Size = Size.Small, ControlCssClass = "form-group-lg")]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.LastNameLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Order(0)]
        [StringLength(15, ErrorMessageResourceName = "LastNameMaxLengthErrorMessage", ErrorMessageResourceType = typeof(RenderersDemoResources))]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.PublicNumberLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Prompt("PinPlaceholder", typeof(RenderersDemoResources))]
        [Required]
        [StringLength(34)]
        public string PublicIdentificationNumber { get; set; }

        [Multiline(4)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.DescriptionLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public string Description { get; set; }

        [Range(typeof(DateTime), "1/1/2012", "1/1/2019")]
        [DisplayNameLocalized(nameof(RenderersDemoResources.BirthDateLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public DateTime BirthDate { get; set; }

        [Range(typeof(DateTime), "1/1/2012", "1/1/2019")]
        [DisplayNameLocalized(nameof(RenderersDemoResources.EventDate), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [TimePicker]
        public DateTime EventDate { get; set; }

        [Multiline(4)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.EventDescription), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public string EventDescription { get; set; }

        [ColorPicker]
        [DisplayNameLocalized(nameof(RenderersDemoResources.ColorLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public string Color { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.IsMarriedLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public bool IsMarried { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.IsMarriedLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [RadioGroup(ItemProvider = typeof(YesNoItemProvider))]
        public bool IsMarried2 { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.IsMarriedLabel), typeof(RenderersDemoResources))]
        [RadioGroup(ItemProvider = typeof(YesNoItemProvider), EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent)]
        [ReadOnly(true)]
        public bool? IsMarried3 { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.MotherLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), DialogWidth = "900px", OnResolveUrlJavaScript = "url = UrlHelper.updateUrlParameters(url, 'search=adm&filter=active')")]
        [ReadOnly(true)]
        public long Mother { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.FatherLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url +'&parent-id=3'")]
        [ReadOnly(true)]
        public long? Father { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.ChildrenLabel), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnSelectionChangedJavaScript = "alert('Children='+$(control).find('#children').val());", DialogWidth = "80%")]
        [ReadOnly(true)]
        public long[] Children { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.SecurityRoleLabel), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        public SecurityRoleType SecurityRole { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.SecurityRole2Label), typeof(RenderersDemoResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SecurityRoleType>))]
        [ReadOnly(true)]
        public SecurityRoleType SecurityRole2 { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.RolePickerLabel), typeof(RenderersDemoResources))]
        [TypeAhead(Type = typeof(RolePickerController), FormFormatterType = typeof(RenderersDemoRolePickerFormatter))]
        [ReadOnly(true)]
        public long? Role { get; set; }

        [CannotBeEmpty]
        [DisplayNameLocalized(nameof(RenderersDemoResources.RolePickerLabel), typeof(RenderersDemoResources))]
        [TypeAhead(Type = typeof(RolePickerController), MinimumInputLength = 1)]
        [ReadOnly(true)]
        public long[] RoleList { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.LocalizedString), typeof(RenderersDemoResources))]
        [ReadOnly(true)]
        [Required]
        public LocalizedString LocalizedString { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.OptionalUserWithChanges), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        [ReadOnly(true)]
        public Optional<long?> OptionalUserWithChanges { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.OptionalUserWithoutChanges), typeof(RenderersDemoResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        [ReadOnly(true)]
        public Optional<long?> OptionalUserWithoutChanges { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.ValuePicker), typeof(RenderersDemoResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(IdentifiableTypeListItemProvider<AtomicController>))]
        [Render(Size = Size.Medium)]
        [ReadOnly(true)]
        public long ValuePickerTypeId { get; set; }

        public HorizontalLayoutReadonlyDemoModel()
        {
            FirstName = "Joe";
            LastName = "Doe";
            PublicIdentificationNumber = "132";
            Mother = 1;
            Children = new long[] { 1, 2 };
            Description = "Quite\r\na\r\ngentleman";
            BirthDate = DateTime.Now;
            SecurityRole = EnumHelper.GetOutOfRangeValue<SecurityRoleType>();
            FavoriteInteger = 8;
            FavoriteReal = (decimal)Math.PI;
            LocalizedString = new LocalizedString
            {
                Polish = "To jest tekst napisany w języku polskim",
                English = "This is text written in English"
            };
            OptionalUserWithChanges = new Optional<long?>
            {
                IsSet = true,
                Value = 1L
            };
            OptionalUserWithoutChanges = new Optional<long?>
            {
                IsSet = false,
                Value = null
            };
        }
    }
}