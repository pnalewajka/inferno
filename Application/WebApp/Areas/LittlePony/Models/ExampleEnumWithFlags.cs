﻿using System;
using System.ComponentModel;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Flags]
    public enum ExampleEnumWithFlags
    {
        None = 0,

        [Description("Flag1")]
        Flag1 = 1 << 0,

        [Description("Flag2")]
        Flag2 = 1 << 1,

        Flag3 = Flag1 | Flag2,

        [Description("Flag4")]
        Flag4 = 1 << 2
    }
}
