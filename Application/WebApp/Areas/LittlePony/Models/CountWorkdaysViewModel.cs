﻿using System;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    public class CountWorkdaysViewModel
    {
        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public int DayCount { get; set; }
    }
}