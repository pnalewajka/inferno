﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    public class SampleAddressModel
    {
        [DisplayNameLocalized(nameof(RenderersDemoResources.StreetLabel), typeof(RenderersDemoResources))]
        public string Street { get; set; }

        [StringLength(10, MinimumLength = 3)]
        [DisplayNameLocalized(nameof(RenderersDemoResources.HouseNumberLabel), typeof(RenderersDemoResources))]
        public string HouseNumber { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(RenderersDemoResources.CityLabel), typeof(RenderersDemoResources))]
        public string City { get; set; }
    }
}