﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    [Layout(typeof(HorizontalFormLayout))]
    [FieldSetDescription(1, "personal", "PersonalDataLegend", typeof(RenderersDemoResources))]
    public class HorizontalRepeaterSampleModel
    {
        [DisplayNameLocalized(nameof(RenderersDemoResources.FirstNameLabel), typeof(RenderersDemoResources))]
        [FieldSet("personal")]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.LastNameLabel), typeof(RenderersDemoResources))]
        [FieldSet("personal")]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.AddressList), typeof(RenderersDemoResources))]
        [Repeater("AddressRepeaterList", typeof(RenderersDemoResources),
            CanAddRecord = true,
            AddButtonText = @"AddButtonText",
            CanDeleteRecord = true,
            DeleteButtonText = @"DeleteButtonText",
            CanReorder = true,
            MinCount = 1,
            MaxCount = 3)]
        public List<SampleAddressModel> Addresses { get; set; }

        [Repeater("AddressRepeaterIList", typeof(RenderersDemoResources))]
        public IList<SampleAddressModel> AddressList { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.AddressCollection), typeof(RenderersDemoResources))]
        public ICollection<SampleAddressModel> AddressCollection { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.AddressArray), typeof(RenderersDemoResources))]
        public SampleAddressModel[] AddressArray { get; set; }

        [DisplayNameLocalized(nameof(RenderersDemoResources.EmptyAddresses), typeof(RenderersDemoResources))]
        public List<SampleAddressModel> EmptyAddresses { get; set; }

        [Repeater("NullAddressRepeater", typeof(RenderersDemoResources),
            CanAddRecord = true,
            CanDeleteRecord = true)]
        public List<SampleAddressModel> NullAddresses { get; set; }
    }
}