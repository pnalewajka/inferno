﻿using System;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    public class AddWorkdaysViewModel
    {
        public DateTime Day { get; set; }

        public int Days { get; set; }

        public DateTime Result { get; set; }
    }
}