﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Models
{
    public class PonyUsersModel
    {
        public IEnumerable<PonyUserModel> Users { get; set; }
    }

    public class PonyUserModel
    {
        [StringLength(32)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(32)]
        public string LastName { get; set; }

        public long Id { get; set; }
        
    }
}