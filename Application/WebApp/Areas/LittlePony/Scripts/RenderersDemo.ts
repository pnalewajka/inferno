﻿module RenderersDemo {
    import GridComponent = Grid.GridComponent;
    import Row = Grid.IRow;

    interface IUser extends Row {
        lastName: string
    }

    export function showDialog(msg: string) {
         CommonDialogs.alert({ htmlMessage: msg, title: "Test", eventHandler: undefined, iconName: "", okButtonText: "" });

    }

    export function sayMyName(grid: GridComponent) {
        var msg = grid.getSelectedRowData<IUser>()[0].lastName + ".<br />You’re goddamn right.";
        showDialog(msg);
    }
}