﻿/// <reference path="../../../Scripts/typings/jquery/jquery.d.ts"/>
/// <reference path="../../../Scripts/Atomic/Globals.ts"/>

$(() => {
    var list = $("<ul></ul>");

    for (var key in Globals.resources) {
        list.append($("<li><span class='key'>" + key + "</span> &rarr; <span class='value'>" + Globals.resources[key] + "</span></li>"));
    }

    $("div.content").append(list);
});