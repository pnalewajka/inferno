﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.LittlePony
{
    public class LittlePonyAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "LittlePony";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LittlePony_default2",
                "LittlePony/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}