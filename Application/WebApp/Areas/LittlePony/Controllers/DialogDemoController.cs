﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.LittlePony.Models;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class DialogDemoController : AtomicController
    {
        public DialogDemoController(IBaseControllerDependencies baseDependencies) 
            : base(baseDependencies)
        {
            Layout.Parameters.AddParameter<int>(ParameterKeys.TemporaryDocumentMaxUploadChunkSize);
            Layout.Parameters.AddParameter<int>(ParameterKeys.UploadPreviewImageMaxDimension);
            Layout.Parameters.AddParameter<int>(ParameterKeys.UploadCropImageMaxDimension);
        }

        public ActionResult Index()
        {
            Layout.PageTitle = DialogsDemoResources.IndexPageTitle;
            Layout.PageHeader = DialogsDemoResources.IndexPageHeader;

            return View();
        }

        [HttpPost]
        public ActionResult EditPersonDialog(int id)
        {
            var model = new PersonModel
            {
                FirstName = "Steven",
                LastName = "Seagal"
            };

            var dialogModel = new ModelBasedDialogViewModel<PersonModel>(model)
            {
                Title = string.Format(DialogsDemoResources.DialogsDemoControllerEditPersonDialogTitleFormat, model.GetFullName())
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        public ActionResult ValidatePersonDialog(PersonModel model)
        {
            if (ModelState.IsValid)
            {
                return DialogHelper.Redirect(this, Url.Action("Index", new { it = "worked" }));
            }

            var dialogModel = new ModelBasedDialogViewModel<PersonModel>(model)
            {
                Title = string.Format(DialogsDemoResources.DialogsDemoControllerEditPersonDialogTitleFormat, model.GetFullName())
            };

            return PartialView("EditPersonDialog", dialogModel);
        }
    }
}