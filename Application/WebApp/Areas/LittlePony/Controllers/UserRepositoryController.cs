﻿using System;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Models;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class UserRepositoryController : AtomicController
    {
        private readonly IUserCardIndexService _userCardIndexService;

        public UserRepositoryController(IUserCardIndexService userCardIndexService, IBaseControllerDependencies baseDependencies): base(baseDependencies)
        {
            _userCardIndexService = userCardIndexService;
        }

        public ActionResult Index()
        {
            var userDtos = _userCardIndexService.GetRecords();

            var model = new PonyUsersModel();
            {
                model.Users = userDtos.Rows.Select(u => new PonyUserModel{ Id = u.Id, FirstName = u.FirstName, LastName = u.LastName }).ToList();
            }

            return View(model);
        }

        public ActionResult AddRandomUser()
        {
            var randomUser = new UserDto
            {
                FirstName = "First name _" + Guid.NewGuid(),
                LastName = "Last name _" + Guid.NewGuid()
            };
            _userCardIndexService.AddRecord(randomUser);

            return RedirectToAction("Index");
        }
    }
}