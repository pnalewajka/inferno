﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.BulkEdit.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewMessageTemplates)]
    public class EmailController : Controller
    {
        private readonly IMessageTemplateService _messageTemplateService;

        public EmailController(IMessageTemplateService service)
        {
            _messageTemplateService = service;
        }

        public ActionResult Index()
        {
            var template = _messageTemplateService.ResolveTemplate(
                TemplateCodes.BulkEditErrorMessageBody,
                new BulkEditNotificationMessageDto()
                {
                    Messages = new List<string>(),
                });
            return Content(template.Content ?? "");
        }

        public ActionResult ErrorMessageBody()
        {
            var template = _messageTemplateService.ResolveTemplate(
                TemplateCodes.BulkEditErrorMessageBody,
                new BulkEditNotificationMessageDto
                {
                    Messages = new List<string>(),
                });
            return Content(template.Content ?? "");
        }

        public ActionResult SuccessMessageBody()
        {
            var template = _messageTemplateService.ResolveTemplate(
                TemplateCodes.BulkEditSuccessMessageBody,
                new BulkEditNotificationMessageDto
                {
                    Messages = new List<string>(),
                });
            return Content(template.Content ?? "");
        }

        public ActionResult UserLoginEmailBody()
        {
            var template = _messageTemplateService.ResolveTemplate(
                TemplateCodes.AccountsNewUserLoginTokenEmailBody,
                new UserLoginTokenEmailDto
                {
                    FirstName = string.Empty,
                    LastName = string.Empty,
                    ExpiresOn = DateTime.Now,
                    LoginUrl = string.Empty
                });

            return Content(template.Content ?? "");
        }

        public ActionResult UserPasswordEmailBody()
        {
            var template = _messageTemplateService.ResolveTemplate(
                TemplateCodes.AccountsNewUserPasswordEmailBody,
                new UserPasswordEmailDto
                {
                    FirstName = string.Empty,
                    LastName = string.Empty,
                    Login = string.Empty,
                    Password = string.Empty
                });
            return Content(template.Content ?? "");
        }

    }
}