﻿using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [Identifier("ValuePickers.UsersDemo")]
    public class UserDemoPickerController : UserPickerController
    {
        public UserDemoPickerController(
            IUserCardIndexService userCardIndexService,
            IUserService userService,
            IBaseControllerDependencies baseControllerDependencies,
            IEmployeeService employeeService)
            : base(userCardIndexService, userService, baseControllerDependencies, employeeService)
        {
            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = RenderersDemoResources.AlertTest,
                Visibility = CommandButtonVisibility.ValuePicker,
                OnClickAction = new JavaScriptCallAction(
                    $"RenderersDemo.showDialog('{RenderersDemoResources.AlertText}')")
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = RenderersDemoResources.SayMyName,
                Visibility = CommandButtonVisibility.ValuePicker,
                Availability = new ToolbarButtonAvailability {Mode = AvailabilityMode.SingleRowSelected},
                OnClickAction = new JavaScriptCallAction("RenderersDemo.sayMyName(grid)")
            });
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            Layout.PageTitle = allowMultipleRowSelection
                                   ? RenderersDemoResources.UserDemoValuePickerMultipleTitle
                                   : RenderersDemoResources.UserDemoValuePickerSingleTitle;
        }
    }
}