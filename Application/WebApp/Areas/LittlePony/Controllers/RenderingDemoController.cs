﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Models;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class RenderingDemoController : AtomicController
    {
        private readonly IAlertService _alertService;

        public RenderingDemoController(
            IBaseControllerDependencies baseDependencies,
            IAlertService alertService) 
            : base(baseDependencies)
        {
            _alertService = alertService;
        }

        public ActionResult TabSample()
        {
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.TabsUsageTitle;

            var model = new TabbedSampleModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult TabSample(TabbedSampleModel model)
        {
            _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);

            return TabSample();
        }

        public ActionResult HorizontalLayoutSample()
        {
            Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.HorizontalLayoutDemoTitle;

            var model = new HorizontalLayoutSampleModel();
            
            return View(model);
        }

        [HttpPost]
        public ActionResult HorizontalLayoutSample(HorizontalLayoutSampleModel model)
        {
            _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);

            return HorizontalLayoutSample();
        }

        public ActionResult HorizontalLayoutReadonlyDemo()
        {
            Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.HorizontalLayoutReadonlyDemoTitle;

            var model = new HorizontalLayoutReadonlyDemoModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult HorizontalLayoutReadonlyDemo(HorizontalLayoutReadonlyDemoModel model)
        {
            _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);

            return HorizontalLayoutReadonlyDemo();
        }

        public ActionResult InlineLayoutSample()
        {
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.InlineLayoutDemoTitle;

            var model = InlineLayoutSampleModel.CreateSample();

            return View(model);
        }

        [HttpPost]
        public ActionResult InlineLayoutSample(InlineLayoutSampleModel model)
        {
            _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);

            return InlineLayoutSample();
        }

        public ActionResult InlineLayoutSampleSelectedFields()
        {
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.InlineLayoutDemoTitle;

            var model = InlineLayoutSampleModel.CreateSample();

            return View(model);
        }

        [HttpPost]
        public ActionResult InlineLayoutSampleSelectedFields(InlineLayoutSampleModel model)
        {
            _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);

            return InlineLayoutSampleSelectedFields();
        }

        public ActionResult SimpleLayoutSample()
        {
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.SimpleLayoutDemoTitle;

            Layout.Scripts.Add("~/Areas/LittlePony/Scripts/RenderersDemo.js");

            var model = SimpleLayoutSampleModel.CreateSample();

            return View(model);
        }

        [HttpPost]
        public ActionResult SimpleLayoutSample(SimpleLayoutSampleModel model)
        {
            if (ModelState.IsValid)
            {
                _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);
            }

            return View(model);
        }

        public ActionResult SimpleLayoutReadonlyDemo()
        {
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.SimpleLayoutReadonlyDemoTitle;

            Layout.Scripts.Add("~/Areas/LittlePony/Scripts/RenderersDemo.js");

            var model = SimpleLayoutReadonlyDemoModel.CreateSample();

            return View(model);
        }

        [HttpPost]
        public ActionResult SimpleLayoutReadonlyDemo(SimpleLayoutReadonlyDemoModel model)
        {
            if (ModelState.IsValid)
            {
                _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);
            }

            return View(model);
        }

        public ActionResult SummaryLayoutSample()
        {
            Layout.PageTitle = Layout.PageHeader = RenderersDemoResources.SummaryLayoutDemoTitle;

            var model = new SummaryLayoutSampleModel();
            
            return View(model);
        }

        [HttpPost]
        public ActionResult SummaryLayoutSample(SummaryLayoutSampleModel model)
        {
            _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);

            return SummaryLayoutSample();
        }

        public ActionResult RepeaterSample()
        {
            Layout.PageTitle = RenderersDemoResources.RepeaterPageTitle;
            Layout.PageHeader = RenderersDemoResources.RepeaterPageHeader;

            var repeaterSampleModel = new RepeaterSampleModel
            {
                Addresses = new List<SampleAddressModel> { new SampleAddressModel(), new SampleAddressModel(), new SampleAddressModel() },
                AddressList = new List<SampleAddressModel> { new SampleAddressModel() },
                AddressCollection = new List<SampleAddressModel> { new SampleAddressModel() },
                AddressArray = new[] { new SampleAddressModel(), new SampleAddressModel() },
                EmptyAddresses = new List<SampleAddressModel>(),
                NullAddresses = null
            };

            return View(repeaterSampleModel);
        }

        public ActionResult HorizontalRepeaterSample()
        {
            Layout.PageTitle = RenderersDemoResources.RepeaterPageTitle;
            Layout.PageHeader = RenderersDemoResources.RepeaterPageHeader;

            var repeaterSampleModel = new HorizontalRepeaterSampleModel
            {
                Addresses = new List<SampleAddressModel> { new SampleAddressModel(), new SampleAddressModel(), new SampleAddressModel() },
                AddressList = new List<SampleAddressModel> { new SampleAddressModel() },
                AddressCollection = new List<SampleAddressModel> { new SampleAddressModel() },
                AddressArray = new[] { new SampleAddressModel(), new SampleAddressModel() },
                EmptyAddresses = new List<SampleAddressModel>(),
                NullAddresses = null
            };

            return View(repeaterSampleModel);
        }

        [HttpPost]
        public ActionResult RepeaterSample(RepeaterSampleModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddErrorsToAlertService(_alertService);
            }
            else
            {
                _alertService.AddSuccess(RenderersDemoResources.RecordValidatedMessage);
            }            

            return View(model);
        }
    }
}