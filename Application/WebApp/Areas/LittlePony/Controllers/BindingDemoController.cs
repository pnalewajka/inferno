﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Models;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class BindingDemoController : AtomicController
    {
        public BindingDemoController(IBaseControllerDependencies baseDependencies) : base(baseDependencies)
        {
        }

        [AcceptVerbs(HttpVerbs.Get|HttpVerbs.Post)]
        public ActionResult Index(AliasesViewModel model)
        {
            return View(model ?? new AliasesViewModel());
        }
    }
}