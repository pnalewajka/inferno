﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class LayoutModelDemoController : AtomicController
    {
        public LayoutModelDemoController(IBaseControllerDependencies baseDependencies) 
            : base(baseDependencies)
        {
        }

        public ActionResult Index()
        {
            Layout.PageTitle = LayoutModelDemoResources.IndexPageTitle;
            Layout.PageHeader = LayoutModelDemoResources.IndexPageHeader;

            Layout.Resources.AddFrom<HorseHarnessResources>();

            Layout.Css.Add("~/Areas/LittlePony/Content/LayoutModelDemo.css");
            Layout.Scripts.Add("~/Areas/LittlePony/Scripts/LayoutModelDemo.js");

            return View();
        }
    }
}