﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Models;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class PrintDemoController : AtomicController
    {
        public PrintDemoController(IBaseControllerDependencies baseDependencies) : base(baseDependencies)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Print()
        {
            var printoutModel = new PrintoutViewModel()
            {
                CurrentDate = DateTime.Now.ToShortDateString()
            };

            return GeneratePrintout("~/Areas/LittlePony/Views/PrintDemo/Print.cshtml", printoutModel,
                "~/Areas/LittlePony/Content/PrintDemo.css", PrintDemoResources.PrintoutTitle);
        }
    }
}