﻿using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Models;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class CalendarDemoController : AtomicController
    {
        private const int DefaultCalendarId = 1;
        private readonly ICalendarService _calendarService;

        public CalendarDemoController(IBaseControllerDependencies baseDependencies, ICalendarService calendarService) 
            : base(baseDependencies)
        {
            _calendarService = calendarService;
        }

        public ActionResult IsItWorkday(DateTime day)
        {
            Layout.PageTitle = Layout.PageHeader = CalendarDemoResources.IsWorkdayTitle;

            var model = new IsItWorkdayViewModel
            {
                Day = day,
                IsWorkday = _calendarService.IsWorkday(day, DefaultCalendarId)
            };

            return View(model);
        }

        public ActionResult AddWorkdays(DateTime day, int days)
        {
            Layout.PageTitle = Layout.PageHeader = CalendarDemoResources.AddWordaysTitle;

            var model = new AddWorkdaysViewModel
            {
                Day = day,
                Days = days,
                Result = _calendarService.AddWorkdays(day, days, DefaultCalendarId)
            };

            return View(model);
        }


        public ActionResult CountWorkdays(DateTime from, DateTime to)
        {
            Layout.PageTitle = Layout.PageHeader = CalendarDemoResources.CountWorkdaysTitle;

            var model = new CountWorkdaysViewModel
            {
                From = from,
                To = to,
                DayCount = _calendarService.GetWorkdayCount(from, to, DefaultCalendarId)
            };

            return View(model);
        }
    }
}