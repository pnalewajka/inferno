﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.LittlePony.Resources;

namespace Smt.Atomic.WebApp.Areas.LittlePony.Controllers
{
    [AtomicAuthorize]
    public class AlertDemoController : AtomicController
    {
        public AlertDemoController(IBaseControllerDependencies baseDependencies) : base(baseDependencies)
        {
            var alertService = baseDependencies.AlertService;
            
            alertService.AddError(AlertDemoResources.DangerMessage);
            alertService.AddWarning(AlertDemoResources.WarningMessage);
            alertService.AddSuccess(AlertDemoResources.SuccessMessage);
            alertService.AddInformation(AlertDemoResources.InfoMessage);

            Layout.PageTitle = Layout.PageHeader = AlertDemoResources.PageTitle;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}