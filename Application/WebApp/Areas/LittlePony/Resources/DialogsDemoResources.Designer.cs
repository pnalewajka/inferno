﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.LittlePony.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DialogsDemoResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DialogsDemoResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.LittlePony.Resources.DialogsDemoResources", typeof(DialogsDemoResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit person: {0}.
        /// </summary>
        public static string DialogsDemoControllerEditPersonDialogTitleFormat {
            get {
                return ResourceManager.GetString("DialogsDemoControllerEditPersonDialogTitleFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Basic alert.
        /// </summary>
        public static string IndexAlert1ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert1ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is a basic alert..
        /// </summary>
        public static string IndexAlert1HtmlMessage {
            get {
                return ResourceManager.GetString("IndexAlert1HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alert with title.
        /// </summary>
        public static string IndexAlert2ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert2ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Another rather simple alert - with &lt;u&gt;title&lt;/u&gt;..
        /// </summary>
        public static string IndexAlert2HtmlMessage {
            get {
                return ResourceManager.GetString("IndexAlert2HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Basic notification.
        /// </summary>
        public static string IndexAlert2Title {
            get {
                return ResourceManager.GetString("IndexAlert2Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alert with button.
        /// </summary>
        public static string IndexAlert3ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert3ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sample alert with a button text set..
        /// </summary>
        public static string IndexAlert3HtmlMessage {
            get {
                return ResourceManager.GetString("IndexAlert3HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go on.
        /// </summary>
        public static string IndexAlert3OkButtonText {
            get {
                return ResourceManager.GetString("IndexAlert3OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Button-change alert.
        /// </summary>
        public static string IndexAlert3Title {
            get {
                return ResourceManager.GetString("IndexAlert3Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alert with icon.
        /// </summary>
        public static string IndexAlert4ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert4ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some other alert with an &lt;i&gt;icon&lt;/i&gt;..
        /// </summary>
        public static string IndexAlert4HtmlMessage {
            get {
                return ResourceManager.GetString("IndexAlert4HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Icon-aware alert.
        /// </summary>
        public static string IndexAlert4Title {
            get {
                return ResourceManager.GetString("IndexAlert4Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Event-handling #1.
        /// </summary>
        public static string IndexAlert5ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert5ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to When you press &lt;b&gt;OK&lt;/b&gt;, there will be another, old-school alert..
        /// </summary>
        public static string IndexAlert5HtmlMessage {
            get {
                return ResourceManager.GetString("IndexAlert5HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to That was an OK button alright....
        /// </summary>
        public static string IndexAlert5OnOkAlertMessage {
            get {
                return ResourceManager.GetString("IndexAlert5OnOkAlertMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Event-handling alert.
        /// </summary>
        public static string IndexAlert5Title {
            get {
                return ResourceManager.GetString("IndexAlert5Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Event-handling #2.
        /// </summary>
        public static string IndexAlert6ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert6ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to When you press &lt;b&gt;OK&lt;/b&gt;, there will be another, old-school alert..
        /// </summary>
        public static string IndexAlert6HtmlMessage {
            get {
                return ResourceManager.GetString("IndexAlert6HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to That was an OK button alright....
        /// </summary>
        public static string IndexAlert6OnOkAlertMessage {
            get {
                return ResourceManager.GetString("IndexAlert6OnOkAlertMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to That was NOT an OK button, my friend....
        /// </summary>
        public static string IndexAlert6OnOtherAlertMessage {
            get {
                return ResourceManager.GetString("IndexAlert6OnOtherAlertMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Event-handling alert (sneaky edition).
        /// </summary>
        public static string IndexAlert6Title {
            get {
                return ResourceManager.GetString("IndexAlert6Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chained alerts.
        /// </summary>
        public static string IndexAlert7ButtonText {
            get {
                return ResourceManager.GetString("IndexAlert7ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is an alert!&lt;br/&gt;There&lt;br/&gt;is&lt;br/&gt;so much&lt;br/&gt;to say!.
        /// </summary>
        public static string IndexAlert7HtmlMessage1 {
            get {
                return ResourceManager.GetString("IndexAlert7HtmlMessage1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This is a friendly message..
        /// </summary>
        public static string IndexAlert7HtmlMessage2 {
            get {
                return ResourceManager.GetString("IndexAlert7HtmlMessage2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go on.
        /// </summary>
        public static string IndexAlert7OkButtonText {
            get {
                return ResourceManager.GetString("IndexAlert7OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK, we\&apos;re done here!.
        /// </summary>
        public static string IndexAlert7OnOkAlertMessage {
            get {
                return ResourceManager.GetString("IndexAlert7OnOkAlertMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warning.
        /// </summary>
        public static string IndexAlert7Title1 {
            get {
                return ResourceManager.GetString("IndexAlert7Title1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information.
        /// </summary>
        public static string IndexAlert7Title2 {
            get {
                return ResourceManager.GetString("IndexAlert7Title2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alert.
        /// </summary>
        public static string IndexAlertSectionTitle {
            get {
                return ResourceManager.GetString("IndexAlertSectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Basic confirm.
        /// </summary>
        public static string IndexConfirm1ButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm1ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you really want to continue?.
        /// </summary>
        public static string IndexConfirm1HtmlMessage {
            get {
                return ResourceManager.GetString("IndexConfirm1HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm with title.
        /// </summary>
        public static string IndexConfirm2ButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm2ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you really want to continue - with &lt;u&gt;title&lt;/u&gt;?.
        /// </summary>
        public static string IndexConfirm2HtmlMessage {
            get {
                return ResourceManager.GetString("IndexConfirm2HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm.
        /// </summary>
        public static string IndexConfirm2Title {
            get {
                return ResourceManager.GetString("IndexConfirm2Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm with OK button.
        /// </summary>
        public static string IndexConfirm3ButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm3ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you really want to continue - with &lt;i&gt;OK button&lt;/i&gt;?.
        /// </summary>
        public static string IndexConfirm3HtmlMessage {
            get {
                return ResourceManager.GetString("IndexConfirm3HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        public static string IndexConfirm3OkButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm3OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm.
        /// </summary>
        public static string IndexConfirm3Title {
            get {
                return ResourceManager.GetString("IndexConfirm3Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm with both buttons.
        /// </summary>
        public static string IndexConfirm4ButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm4ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nope.
        /// </summary>
        public static string IndexConfirm4CancelButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm4CancelButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you really want to continue - with &lt;i&gt;both buttons&lt;/i&gt;?.
        /// </summary>
        public static string IndexConfirm4HtmlMessage {
            get {
                return ResourceManager.GetString("IndexConfirm4HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeah.
        /// </summary>
        public static string IndexConfirm4OkButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm4OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm.
        /// </summary>
        public static string IndexConfirm4Title {
            get {
                return ResourceManager.GetString("IndexConfirm4Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm your action.
        /// </summary>
        public static string IndexConfirm5ButtonText {
            get {
                return ResourceManager.GetString("IndexConfirm5ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you really want to continue - &lt;i&gt;with event handlers&lt;/i&gt;?.
        /// </summary>
        public static string IndexConfirm5HtmlMessage {
            get {
                return ResourceManager.GetString("IndexConfirm5HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You said Cancel..
        /// </summary>
        public static string IndexConfirm5OnCancelAlertText {
            get {
                return ResourceManager.GetString("IndexConfirm5OnCancelAlertText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You said OK..
        /// </summary>
        public static string IndexConfirm5OnOkAlertText {
            get {
                return ResourceManager.GetString("IndexConfirm5OnOkAlertText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I guess you do not want to continue....
        /// </summary>
        public static string IndexConfirm5OnOtherAlertText {
            get {
                return ResourceManager.GetString("IndexConfirm5OnOtherAlertText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please confirm.
        /// </summary>
        public static string IndexConfirm5Title {
            get {
                return ResourceManager.GetString("IndexConfirm5Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm.
        /// </summary>
        public static string IndexConfirmSectionTitle {
            get {
                return ResourceManager.GetString("IndexConfirmSectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sample &lt;u&gt;Common&lt;/u&gt; Dialogs.
        /// </summary>
        public static string IndexPageHeader {
            get {
                return ResourceManager.GetString("IndexPageHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sample common dialogs.
        /// </summary>
        public static string IndexPageTitle {
            get {
                return ResourceManager.GetString("IndexPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Basic prompt.
        /// </summary>
        public static string IndexPrompt1ButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt1ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please input value.
        /// </summary>
        public static string IndexPrompt1HtmlMessage {
            get {
                return ResourceManager.GetString("IndexPrompt1HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt with title.
        /// </summary>
        public static string IndexPrompt2ButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt2ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please input value - with &lt;u&gt;title&lt;/u&gt;.
        /// </summary>
        public static string IndexPrompt2HtmlMessage {
            get {
                return ResourceManager.GetString("IndexPrompt2HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt - with &lt;u&gt;title&lt;/u&gt;.
        /// </summary>
        public static string IndexPrompt2Title {
            get {
                return ResourceManager.GetString("IndexPrompt2Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt with input label.
        /// </summary>
        public static string IndexPrompt3ButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt3ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please input value - with &lt;u&gt;input label&lt;/u&gt;.
        /// </summary>
        public static string IndexPrompt3HtmlMessage {
            get {
                return ResourceManager.GetString("IndexPrompt3HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Custom value.
        /// </summary>
        public static string IndexPrompt3InputLabel {
            get {
                return ResourceManager.GetString("IndexPrompt3InputLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt - with &lt;u&gt;input label&lt;/u&gt;.
        /// </summary>
        public static string IndexPrompt3Title {
            get {
                return ResourceManager.GetString("IndexPrompt3Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt with OK button.
        /// </summary>
        public static string IndexPrompt4ButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt4ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please input value - with &lt;i&gt;OK button&lt;/i&gt;.
        /// </summary>
        public static string IndexPrompt4HtmlMessage {
            get {
                return ResourceManager.GetString("IndexPrompt4HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string IndexPrompt4OkButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt4OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt - with &lt;i&gt;OK button&lt;/i&gt;?.
        /// </summary>
        public static string IndexPrompt4Title {
            get {
                return ResourceManager.GetString("IndexPrompt4Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt with both buttons.
        /// </summary>
        public static string IndexPrompt5ButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt5ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exit.
        /// </summary>
        public static string IndexPrompt5CancelButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt5CancelButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please input value - with &lt;i&gt;both buttons&lt;/i&gt;.
        /// </summary>
        public static string IndexPrompt5HtmlMessage {
            get {
                return ResourceManager.GetString("IndexPrompt5HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string IndexPrompt5OkButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt5OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt - with &lt;i&gt;both buttons&lt;/i&gt;.
        /// </summary>
        public static string IndexPrompt5Title {
            get {
                return ResourceManager.GetString("IndexPrompt5Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Event-handling.
        /// </summary>
        public static string IndexPrompt6ButtonText {
            get {
                return ResourceManager.GetString("IndexPrompt6ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please input value - with &lt;i&gt;event handler&lt;/i&gt;.
        /// </summary>
        public static string IndexPrompt6HtmlMessage {
            get {
                return ResourceManager.GetString("IndexPrompt6HtmlMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You said Cancel.
        /// </summary>
        public static string IndexPrompt6OnCancelAlertText {
            get {
                return ResourceManager.GetString("IndexPrompt6OnCancelAlertText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You said OK and entered: .
        /// </summary>
        public static string IndexPrompt6OnOkAlertText {
            get {
                return ResourceManager.GetString("IndexPrompt6OnOkAlertText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prompt.
        /// </summary>
        public static string IndexPromptSectionTitle {
            get {
                return ResourceManager.GetString("IndexPromptSectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit person&apos;s data.
        /// </summary>
        public static string IndexShowDialogButtonText {
            get {
                return ResourceManager.GetString("IndexShowDialogButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show custom dialog.
        /// </summary>
        public static string IndexShowDialogSectionTitle {
            get {
                return ResourceManager.GetString("IndexShowDialogSectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Were are done here, &lt;i&gt;Mr. {0}&lt;/i&gt;.
        /// </summary>
        public static string IndexShowModel1AlertHtmlMessageFormat {
            get {
                return ResourceManager.GetString("IndexShowModel1AlertHtmlMessageFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        public static string IndexShowModel1AlertOkButtonText {
            get {
                return ResourceManager.GetString("IndexShowModel1AlertOkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show person data.
        /// </summary>
        public static string IndexShowModel1ButtonText {
            get {
                return ResourceManager.GetString("IndexShowModel1ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quite a personal model.
        /// </summary>
        public static string IndexShowModel1Title {
            get {
                return ResourceManager.GetString("IndexShowModel1Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Were are done here, &lt;i&gt;Mr. {0}&lt;/i&gt;.
        /// </summary>
        public static string IndexShowModel2AlertHtmlMessageFormat {
            get {
                return ResourceManager.GetString("IndexShowModel2AlertHtmlMessageFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        public static string IndexShowModel2AlertOkButtonText {
            get {
                return ResourceManager.GetString("IndexShowModel2AlertOkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show missing model.
        /// </summary>
        public static string IndexShowModel2ButtonText {
            get {
                return ResourceManager.GetString("IndexShowModel2ButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rather a missing model.
        /// </summary>
        public static string IndexShowModel2Title {
            get {
                return ResourceManager.GetString("IndexShowModel2Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show model.
        /// </summary>
        public static string IndexShowModelSectionTitle {
            get {
                return ResourceManager.GetString("IndexShowModelSectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} {1}.
        /// </summary>
        public static string PersonModelGetFullNameFormat {
            get {
                return ResourceManager.GetString("PersonModelGetFullNameFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to That&apos;s just a stage name. Use Paul David Hewson instead..
        /// </summary>
        public static string PersonModelValidateDoNotUseBonoStageName {
            get {
                return ResourceManager.GetString("PersonModelValidateDoNotUseBonoStageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to That&apos;s not a real person..
        /// </summary>
        public static string PersonModelValidateZiggyStardustNotAPersonValidationErrorMessage {
            get {
                return ResourceManager.GetString("PersonModelValidateZiggyStardustNotAPersonValidationErrorMessage", resourceCulture);
            }
        }
    }
}
