﻿using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Consents.Resources;

namespace Smt.Atomic.WebApp.Areas.Consents.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageUserConsents)]
    public class UserConsentsController : BaseConsentController
    {
        private readonly IBaseControllerDependencies _baseDependencies;
        private readonly IUserCardIndexService _userCardIndexService;

        public UserConsentsController(IBaseControllerDependencies baseDependencies,
            IUserCardIndexService userCardIndexService,
            IConsentDataService consentCardIndexDataService)
            : base(baseDependencies, consentCardIndexDataService)
        {
            _baseDependencies = baseDependencies;
            _userCardIndexService = userCardIndexService;
        }

        protected override string GetToggleActionUrl()
        {
            return Url.Action
                (
                    nameof(UserConsentsController.ToggleConsent),
                    RoutingHelper.GetControllerName<UserConsentsController>(),
                    new RouteValueDictionary
                    {
                        { RoutingHelper.AreaParameterName, RoutingHelper.GetAreaName<UserConsentsController>() },
                        { RoutingHelper.ParentIdParameterName, GetUserId() }
                    }
                );
        }

        public override ActionResult Index()
        {
            var userId = GetUserId();

            var userDto = _userCardIndexService.GetRecordById(userId);
            var userClassMapping = _baseDependencies.ClassMappingFactory.CreateMapping<UserDto, UserViewModel>();
            var userViewModel = userClassMapping.CreateFromSource(userDto);

            Layout.PageTitle = Layout.PageHeader = string.Format(ConsentsResources.UserConsentsFormat, userViewModel);

            return base.Index();
        }

        protected override long GetUserId()
        {
            long userId;

            if (long.TryParse(Request.QueryString[RoutingHelper.ParentIdParameterName] ?? string.Empty, out userId))
            {
                return userId;
            }

            throw new BusinessException("Unauthorized change of user consents");
        }
    }
}