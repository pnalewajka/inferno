﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Consents.Models;
using Smt.Atomic.WebApp.Areas.Consents.Resources;

namespace Smt.Atomic.WebApp.Areas.Consents.Controllers
{
    public abstract class BaseConsentController : AtomicController
    {
        private readonly IBaseControllerDependencies _baseDependencies;
        private readonly IConsentDataService _consentCardIndexDataService;

        protected BaseConsentController(IBaseControllerDependencies baseDependencies,
            IConsentDataService consentCardIndexDataService)
            : base(baseDependencies)
        {
            _baseDependencies = baseDependencies;
            _consentCardIndexDataService = consentCardIndexDataService;

            Layout.Resources.AddFrom<ConsentsResources>("ConsentsSaved");
        }

        protected abstract long GetUserId();

        protected abstract string GetToggleActionUrl();

        public virtual ActionResult Index()
        {
            var userId = GetUserId();

            Layout.Scripts.Add("~/Areas/Consents/Scripts/Consents.js");
            Layout.Css.Add("~/Areas/Consents/Content/MyConsents.css");

            var classMapper =
                _baseDependencies.ClassMappingFactory.CreateMapping<ConsentGroupDto, ConsentGroupViewModel>();

            var records = _consentCardIndexDataService
                .GetUserConsentGroups(userId)
                .Select(classMapper.CreateFromSource)
                .ToList();

            var viewModel = new ConsentDetailsViewModel
            {
                ConsentGroups = records,
                ToggleActionUrl = GetToggleActionUrl()
            };

            return View("_ConsentList", viewModel);
        }

        [HttpPost]
        public virtual void ToggleConsent(ConsentDecisionViewModel model)
        {
            var userId = GetUserId();

            var classMapping =
                _baseDependencies.ClassMappingFactory.CreateMapping<ConsentDecisionViewModel, ConsentDecisionDto>();

            _consentCardIndexDataService.ToggleConsent(classMapping.CreateFromSource(model), userId);
        }
    }
}