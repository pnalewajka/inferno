﻿using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Consents.Resources;

namespace Smt.Atomic.WebApp.Areas.Consents.Controllers
{
    [AtomicAuthorize]
    public class MyConsentsController : BaseConsentController
    {
        private readonly IBaseControllerDependencies _baseDependencies;

        public MyConsentsController(IBaseControllerDependencies baseDependencies,
            IConsentDataService consentCardIndexDataService)
            : base(baseDependencies, consentCardIndexDataService)
        {
            _baseDependencies = baseDependencies;

            Layout.PageHeader = ConsentsResources.PageTitle;
            Layout.PageTitle = ConsentsResources.PageTitle;
        }

        protected override long GetUserId()
        {
            if (!_baseDependencies.PrincipalProvider.Current.Id.HasValue)
            {
                throw new BusinessException("Unauthorized change of user consents");
            }

            return _baseDependencies.PrincipalProvider.Current.Id.Value;
        }

        protected override string GetToggleActionUrl()
        {
            return Url.Action("ToggleConsent", "MyConsents", new { area = "Consents" });
        }
    }
}