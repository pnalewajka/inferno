﻿$(() => {
    $("#consent-list .consent").click(function (this: JQuery, e: MouseEvent) {
        e.stopPropagation();

        if ($(".consent").hasClass("in-progress")) {
            return;
        }

        addDisabledClasses();

        const $buttonGroup = $(this);
        const isRevoked: boolean = $buttonGroup.hasClass("consent-revoked");

        if (isRevoked) {
            $buttonGroup.removeClass("consent-revoked").addClass("consent-granted");
        } else {
            $buttonGroup.removeClass("consent-granted").addClass("consent-revoked");
        }

        const $container = $("#consent-list");

        $.post($container.data("toggle-url"), {
            EnumIdentifier: $buttonGroup.data("enum-name"),
            EnumValue: $buttonGroup.data("enum-value")
        }, () => {
            var currentdate = new Date();
            var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth() + 1) + "/"
                + currentdate.getFullYear() + " @ "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

            AlertMessages.addSuccess(`[${datetime}] ${Globals.resources.ConsentsSaved}`);

            var currentAlert = $(".alert-container > div").last();
            currentAlert.delay(1000).fadeOut("slow", () => currentAlert.remove());
            removeDisabledClasses();
        }).fail(() => {
            removeDisabledClasses();
        });
    });

    $("#consent-list legend .fa").click(function(this: JQuery) {
        $(this).toggleClass("fa-chevron-up").toggleClass("fa-chevron-down").parents("fieldset:first").toggleClass("collapsed");
    });

    function addDisabledClasses() {
        $(".consent").addClass("in-progress");
        $(".fa").addClass("disabled-checkbox");
    }

    function removeDisabledClasses() {
        $(".consent").removeClass("in-progress");
        $(".fa").removeClass("disabled-checkbox");
    }
});