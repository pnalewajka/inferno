﻿using System.Linq;
using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentGroupViewModelToConsentGroupDtoMapping : ClassMapping<ConsentGroupViewModel, ConsentGroupDto>
    {
        public ConsentGroupViewModelToConsentGroupDtoMapping(IClassMappingFactory mappingFactory)
        {
            var consentMapping = mappingFactory.CreateMapping<ConsentViewModel, ConsentDto>();

            Mapping = v => new ConsentGroupDto
            {
                EnumIdentifier = v.EnumIdentifier,
                Description = v.Description,
                Consents = v.Consents.Select(consentMapping.CreateFromSource)
            };
        }
    }
}
