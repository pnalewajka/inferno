﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentDetailsViewModel
    {
        public IReadOnlyCollection<ConsentGroupViewModel> ConsentGroups { get; set; }

        public string ToggleActionUrl { get; set; }
    }
}