﻿namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentViewModel
    {
        public string ConsentValue { get; set; }

        public string Description { get; set; }

        public bool IsRevoked { get; set; }
    }
}
