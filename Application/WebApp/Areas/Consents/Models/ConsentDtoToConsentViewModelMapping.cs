﻿using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentDtoToConsentViewModelMapping : ClassMapping<ConsentDto, ConsentViewModel>
    {
        public ConsentDtoToConsentViewModelMapping()
        {
            Mapping = d => new ConsentViewModel
            {
                ConsentValue = d.ConsentValue,
                Description = d.Description,
                IsRevoked = d.IsRevoked
            };
        }
    }
}
