﻿using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentDecisionDtoToConsentDecisionViewModelMapping : ClassMapping<ConsentDecisionDto, ConsentDecisionViewModel>
    {
        public ConsentDecisionDtoToConsentDecisionViewModelMapping()
        {
            Mapping = d => new ConsentDecisionViewModel
            {
                Id = d.Id,
                UserId = d.UserId,
                EnumIdentifier = d.EnumIdentifier,
                EnumValue = d.EnumValue,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
