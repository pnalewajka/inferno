﻿using System.Linq;
using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentGroupDtoToConsentGroupViewModelMapping : ClassMapping<ConsentGroupDto, ConsentGroupViewModel>
    {
        public ConsentGroupDtoToConsentGroupViewModelMapping(IClassMappingFactory mappingFactory)
        {
            var consentMapping = mappingFactory.CreateMapping<ConsentDto, ConsentViewModel>();

            Mapping = d => new ConsentGroupViewModel
            {
                EnumIdentifier = d.EnumIdentifier,
                Description = d.Description,
                Consents = d.Consents.Select(consentMapping.CreateFromSource)
            };
        }
    }
}
