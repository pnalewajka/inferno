﻿using System;
namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentDecisionViewModel
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public string EnumIdentifier { get; set; }

        public string EnumValue { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
