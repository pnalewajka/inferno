﻿using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentDecisionViewModelToConsentDecisionDtoMapping : ClassMapping<ConsentDecisionViewModel, ConsentDecisionDto>
    {
        public ConsentDecisionViewModelToConsentDecisionDtoMapping()
        {
            Mapping = v => new ConsentDecisionDto
            {
                Id = v.Id,
                UserId = v.UserId,
                EnumIdentifier = v.EnumIdentifier,
                EnumValue = v.EnumValue,
                Timestamp = v.Timestamp
            };
        }
    }
}
