﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentGroupViewModel
    {
        public string EnumIdentifier { get; set; }

        public string Description { get; set; }

        public IEnumerable<ConsentViewModel> Consents { get; set; }
    }
}
