﻿using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Consents.Models
{
    public class ConsentViewModelToConsentDtoMapping : ClassMapping<ConsentViewModel, ConsentDto>
    {
        public ConsentViewModelToConsentDtoMapping()
        {
            Mapping = v => new ConsentDto
            {
                ConsentValue = v.ConsentValue,
                Description = v.Description,
                IsRevoked = v.IsRevoked
            };
        }
    }
}
