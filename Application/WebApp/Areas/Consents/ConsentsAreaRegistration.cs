﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Consents
{
    public class ConsentsAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Consents";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Consents_default",
                "Consents/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
                );
        }
    }
}