﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Scheduling.Models;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageJobDefinitionParameters)]
    public class JobDefinitionParameterController : SubCardIndexController<JobDefinitionParameterViewModel, JobDefinitionParameterDto, JobDefinitionController>
    {
        public JobDefinitionParameterController(IJobDefinitionParameterCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = SchedulingResources.JobDefinitionParameterControllerPageTitle;
        }
    }
}