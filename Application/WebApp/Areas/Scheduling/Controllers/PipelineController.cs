﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Scheduling.Models;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManagePipelines)]
    public class PipelineController : CardIndexController<PipelineViewModel, PipelineDto>
    {
        public PipelineController(IPipelineCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = SchedulingResources.PipelineControllerPageTitle;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
        }
    }
}