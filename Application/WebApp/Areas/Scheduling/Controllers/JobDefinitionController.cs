﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Enums;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Scheduling.Models;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageJobDefinitions)]
    [PerformanceTest(nameof(JobDefinitionController.List))]
    public class JobDefinitionController : CardIndexController<JobDefinitionViewModel, JobDefinitionDto>
    {
        private const string DefaultViewId = "default";
        private const string JobLogOrderByKey = "order";
        private const string JobLogOrderByValue = "modified-on-desc";
        private readonly IJobDefinitionCardIndexService _cardIndexDataService;
        private readonly IBaseControllerDependencies _baseControllerDependencies;

        public JobDefinitionController(IJobDefinitionCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _baseControllerDependencies = baseControllerDependencies;

            CardIndex.Settings.Title = SchedulingResources.JobDefinitionControllerPageTitle;

            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
            CardIndex.Settings.PageSize = 100;

            CardIndex.Settings.EditButton.IsDefault = false;
            CardIndex.Settings.ViewButton.IsDefault = false;

            var manageParametersButton = new ToolbarButton
            {
                Text = SchedulingResources.ManageParametersButtonText,
                OnClickAction = Url.UriActionGet<JobDefinitionParameterController>(c => c.List(null), KnownParameter.ParentId),
                Group = "Management",
                Icon = FontAwesome.Cogs,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(manageParametersButton);

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = SchedulingResources.ShowJobLogsText,
                OnClickAction = Url.UriActionGet<JobLogController>(c => c.List(null), KnownParameter.ParentId, JobLogOrderByKey, JobLogOrderByValue),
                Group = "Management",
                Icon = FontAwesome.ThList,
                IsDefault = true,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = SchedulingResources.ResurrectText,
                Icon = FontAwesome.Gavel,
                OnClickAction = Url.UriActionGet(nameof(Resurrect), KnownParameter.SelectedId),
                Confirmation = new Confirmation
                {
                    IsRequired = true,
                    Message = SchedulingResources.ResurrectJobConfirmationText
                },
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.status == {(int)JobStatusType.Defunct}"
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = SchedulingResources.ForceRunText,
                Icon = FontAwesome.Play,
                OnClickAction = Url.UriActionGet(nameof(ForceRun), KnownParameter.SelectedId),
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.status != {(int)JobStatusType.Defunct} && row.status != {(int)JobStatusType.Running}"
                }
            });

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<JobDefinitionViewModel>(m => m.StatusDescription).IsReadOnly = true;
            };
        }

        [AllowAnonymous]
        public ActionResult HealthCheck()
        {
            var jobDtos = _cardIndexDataService.GetRecords();
            var mapping = _baseControllerDependencies.ClassMappingFactory.CreateMapping<JobDefinitionDto, JobDefinitionViewModel>();
            var jobs = jobDtos.Rows.Select(d => mapping.CreateFromSource(d)).ToList();

            var isAllOk = jobs.All(j => j.Status == JobStatusType.Running || j.Status == JobStatusType.NoData || j.Status == JobStatusType.Healthy);

            Layout.MasterName = string.Empty;

            Response.StatusCode = (int)(isAllOk ? HttpStatusCode.OK : HttpStatusCode.InternalServerError);
            Response.TrySkipIisCustomErrors = true;

            return View(jobs);
        }

        public ActionResult Resurrect(long id)
        {
            var result = _cardIndexDataService.Resurrect(id);
            AddAlerts(result.Alerts);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        public ActionResult ForceRun(long id)
        {
            var result = _cardIndexDataService.ForceRun(id);
            AddAlerts(result.Alerts);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var job = (JobDefinitionViewModel)base.GetViewModelFromContext(childContext);

            var panel = new PanelViewModel(job.Code);

            if (!string.IsNullOrWhiteSpace(job.Description))
            {
                panel.AddProperty(SchedulingResources.JobDefinitionDescriptionLabel, job.Description);
            }

            var settings = string.IsNullOrWhiteSpace(job.ScheduleSettings)
                ? RenderersResources.EmptyHtml
                : job.ScheduleSettings;

            panel.AddProperty(SchedulingResources.JobDefinitionScheduleSettingsLabel, settings);
            panel.AddProperty(SchedulingResources.StatusLabel, job.StatusDescription);

            return panel;
        }

        private static IList<IGridViewConfiguration> GetGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<JobDefinitionViewModel>(SchedulingResources.DefaultViewText, DefaultViewId)
            {
                IsDefault = true
            };

            configuration.IncludeAllMethodColumns();
            configuration.IncludeColumns(new List<Expression<Func<JobDefinitionViewModel, object>>>
            {
                c => c.Code,
                c => c.Description,
                c => c.IsEnabled,
                c => c.StatusDescription
            });

            list.Add(configuration);

            return list;
        }
    }
}