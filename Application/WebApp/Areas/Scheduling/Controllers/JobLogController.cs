﻿using System.Collections.Generic;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Scheduling.Models;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageJobDefinitions)]
    public class JobLogController : SubCardIndexController<JobLogViewModel, JobLogDto, JobDefinitionController>
    {
        public JobLogController(IJobLogCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = JobLogResource.JobLogTitle;

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportJobLogs);
            CardIndex.Settings.IsContextViewCollapsed = true;

            Layout.Css.Add("~/Areas/Scheduling/Content/JobLog.css");

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = JobLogResource.FiltersLabel,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<JobLogBetweenDatesFilterViewModel>(JobLogFilterCodes.DateRange, JobLogResource.JobLogBetweenDatesFilterDialogTitle)
                    }
                },
                new FilterGroup
                {
                    DisplayName = JobLogResource.SeverityLabel,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<JobLogSeverity>()
                }
            };
        }
    }
}
