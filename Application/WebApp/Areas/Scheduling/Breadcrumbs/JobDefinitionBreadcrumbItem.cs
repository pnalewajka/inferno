﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Breadcrumbs
{
    public class JobDefinitionBreadcrumbItem : BreadcrumbItem
    {
        public JobDefinitionBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = SchedulingResources.JobDefinitionControllerPageTitle;
        }
    }
}