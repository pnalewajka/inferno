﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobLogViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(JobLogResource.ModifiedOnLabel), typeof(JobLogResource))]
        public DateTime ModifiedOn { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(JobLogResource.SchedulerNameLabel), typeof(JobLogResource))]
        [StringLength(250)]
        public string SchedulerName { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(JobLogResource.HostLabel), typeof(JobLogResource))]
        [StringLength(250)]
        public string Host { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(JobLogResource.PipelineLabel), typeof(JobLogResource))]
        [StringLength(100)]
        public string Pipeline { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(JobLogResource.SeverityLabel), typeof(JobLogResource))]
        public JobLogSeverity JobLogSeverity { get; set; }

        [Order(5)]
        [Render(ControlCssClass = "disable-ellipsis", GridCssClass = "ellipsis message-column")]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobLogResource.MessageLabel), typeof(JobLogResource))]
        [StringLength(2048)]
        public string Message { get; set; }

        [Visibility(VisibilityScope.None)]
        public long JobDefinitionId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
