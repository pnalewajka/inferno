﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Scheduling.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;
using Smt.Atomic.CrossCutting.Common.SemanticTypes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using System.Web.Mvc;
using CronExpressionDescriptor;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobDefinitionViewModel : IValidatableObject
    {
        private static readonly Options ExpressionDescriptorOptions = new Options { ThrowExceptionOnParseError = false };

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Order(0), StringLength(100)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionCodeLabel), typeof(SchedulingResources))]
        public string Code { get; set; }

        [Order(1), StringLength(500)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionDescriptionLabel), typeof(SchedulingResources))]
        public string Description { get; set; }

        [Order(5), StringLength(100)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionClassIdentifierLabel), typeof(SchedulingResources))]
        public string ClassIdentifier { get; set; }

        [Order(2), StringLength(200)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionScheduleSettingsLabel), typeof(SchedulingResources))]
        public string ScheduleSettings { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionScheduleSettingsLabel), typeof(SchedulingResources))]
        public TooltipText GetScheduleSettings(UrlHelper urlHelper)
        {
            return new TooltipText
            {
                Tooltip = ExpressionDescriptor.GetDescription(ScheduleSettings, ExpressionDescriptorOptions),
                Label = ScheduleSettings,
                CssClass = "comment-column"
            };
        }

        [Visibility(VisibilityScope.None)]
        public DateTime? LastRunOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime? LastSuccessOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime? LastFailureOn { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionIsEnabledLabel), typeof(SchedulingResources))]
        public bool IsEnabled { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionShouldRunNowLabel), typeof(SchedulingResources))]
        public bool ShouldRunNow { get; set; }

        [Visibility(VisibilityScope.None)]
        public long PipelineId { get; set; }

        [Visibility(VisibilityScope.None)]
        public int MaxRunAttemptsInBulk { get; set; }

        [Visibility(VisibilityScope.None)]
        public int DelayBetweenBulkRunAttemptsInSeconds { get; set; }

        [Visibility(VisibilityScope.None)]
        public int MaxBulkRunAttempts { get; set; }

        [Visibility(VisibilityScope.Form)]
        public int MaxExecutionPeriodInSeconds { get; set; }
        
        [Visibility(VisibilityScope.None)]
        [NonSortable]
        public JobStatusType Status { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [DisplayNameLocalized(nameof(SchedulingResources.StatusLabel), typeof(SchedulingResources))]
        public string StatusDescription { get; set; }

        public override string ToString()
        {
            return $"{Code} ({StatusDescription})";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!CronExpression.IsValid(ScheduleSettings))
            {
                yield return new PropertyValidationResult(SchedulingResources.JobDefinitionScheduleSettingsInvalidCron, () => ScheduleSettings);
            }
        }
    }
}
