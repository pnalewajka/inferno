﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobLogBetweenDatesFilterDtoToJobLogBetweenDatesFilterViewModelMapping : ClassMapping<JobLogBetweenDatesFilterDto, JobLogBetweenDatesFilterViewModel>
    {
        public JobLogBetweenDatesFilterDtoToJobLogBetweenDatesFilterViewModelMapping()
        {
            Mapping = d => new JobLogBetweenDatesFilterViewModel { From = d.From, To = d.To };
        }
    }
}
