﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobLogDtoToJobLogViewModelMapping : ClassMapping<JobLogDto, JobLogViewModel>
    {
        public JobLogDtoToJobLogViewModelMapping()
        {
            Mapping = d => new JobLogViewModel
            {
                Id = d.Id,
                Message = d.Message,
                JobDefinitionId = d.JobDefinitionId,
                SchedulerName = d.SchedulerName,
                Host = d.Host,
                Pipeline = d.Pipeline,
                JobLogSeverity = d.Severity,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
