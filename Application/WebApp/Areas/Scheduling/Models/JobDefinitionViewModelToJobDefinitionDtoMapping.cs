﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobDefinitionViewModelToJobDefinitionDtoMapping : ClassMapping<JobDefinitionViewModel, JobDefinitionDto>
    {
        public JobDefinitionViewModelToJobDefinitionDtoMapping()
        {
            Mapping = v => new JobDefinitionDto
            {
                Id = v.Id,
                Timestamp = v.Timestamp,
                Code = v.Code,
                Description = v.Description,
                ClassIdentifier = v.ClassIdentifier,
                ScheduleSettings = v.ScheduleSettings,
                LastRunOn = v.LastRunOn,
                LastSuccessOn = v.LastSuccessOn,
                LastFailureOn = v.LastFailureOn,
                IsEnabled = v.IsEnabled,
                ShouldRunNow = v.ShouldRunNow,
                PipelineId = v.PipelineId,
                MaxExecutionPeriodInSeconds = v.MaxExecutionPeriodInSeconds,
                MaxBulkRunAttempts = v.MaxBulkRunAttempts,
                MaxRunAttemptsInBulk = v.MaxRunAttemptsInBulk,
                DelayBetweenBulkRunAttemptsInSeconds = v.DelayBetweenBulkRunAttemptsInSeconds
            };
        }
    }
}
