﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobDefinitionParameterViewModelToJobDefinitionParameterDtoMapping : ClassMapping<JobDefinitionParameterViewModel, JobDefinitionParameterDto>
    {
        public JobDefinitionParameterViewModelToJobDefinitionParameterDtoMapping()
        {
            Mapping = v => new JobDefinitionParameterDto
            {
                Id = v.Id,
                Timestamp = v.Timestamp,
                JobDefinitionId = v.JobDefinitionId,
                ParameterKey = v.ParameterKey,
                ParameterValue = v.ParameterValue
            };
        }
    }
}
