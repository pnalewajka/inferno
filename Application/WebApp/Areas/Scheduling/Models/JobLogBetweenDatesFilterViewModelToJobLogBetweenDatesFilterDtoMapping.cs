﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobLogBetweenDatesFilterViewModelToJobLogBetweenDatesFilterDtoMapping : ClassMapping<JobLogBetweenDatesFilterViewModel, JobLogBetweenDatesFilterDto>
    {
        public JobLogBetweenDatesFilterViewModelToJobLogBetweenDatesFilterDtoMapping()
        {
            Mapping = v => new JobLogBetweenDatesFilterDto
            {
                From = v.From,
                To = v.To
            };
        }
    }
}
