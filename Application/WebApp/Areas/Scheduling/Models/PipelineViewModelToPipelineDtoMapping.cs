﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class PipelineViewModelToPipelineDtoMapping : ClassMapping<PipelineViewModel, PipelineDto>
    {
        public PipelineViewModelToPipelineDtoMapping()
        {
            Mapping = v => new PipelineDto
            {
                Id = v.Id,
                Timestamp = v.Timestamp,
                Name = v.Name
            };
        }
    }
}
