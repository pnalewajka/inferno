﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    [Identifier("Filters.JobLoggedBetweenDates")]
    [DescriptionLocalized("JobLogBetweenDatesFilterDialogTitle", typeof(JobLogResource))]
    public class JobLogBetweenDatesFilterViewModel
    {
        [UrlFieldName("from")]
        [DisplayNameLocalized(nameof(JobLogResource.LoggedOnBetweenDatesFilterFrom), typeof(JobLogResource))]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? From { get; set; }

        [UrlFieldName("to")]
        [DisplayNameLocalized(nameof(JobLogResource.LoggedOnBetweenDatesFilterTo), typeof(JobLogResource))]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? To { get; set; }

        public override string ToString()
        {
            return string.Format(JobLogResource.LoggedOnBetweenDatesFilterSelectedNameFormat, From, To);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (From.HasValue && To.HasValue && From.Value > To.Value)
            {
                yield return new ValidationResult(UserResources.FromDateShouldBeLessThanToDate);
            }
        }
    }
}
