﻿using System;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobDefinitionDtoToJobDefinitionViewModelMapping : ClassMapping<JobDefinitionDto, JobDefinitionViewModel>
    {
        private readonly ITimeService _timeService;

        public JobDefinitionDtoToJobDefinitionViewModelMapping(ITimeService timeService)
        {
            _timeService = timeService;

            Mapping = d => new JobDefinitionViewModel
            {
                Id = d.Id,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                Code = d.Code,
                Description = d.Description,
                ClassIdentifier = d.ClassIdentifier,
                ScheduleSettings = d.ScheduleSettings,
                LastRunOn = d.LastRunOn,
                LastSuccessOn = d.LastSuccessOn,
                LastFailureOn = d.LastFailureOn,
                IsEnabled = d.IsEnabled,
                ShouldRunNow = d.ShouldRunNow,
                PipelineId = d.PipelineId,
                MaxExecutionPeriodInSeconds = d.MaxExecutionPeriodInSeconds,
                Status = d.Status,
                StatusDescription = GetStatusDescription(d),
                MaxBulkRunAttempts = d.MaxBulkRunAttempts,
                MaxRunAttemptsInBulk = d.MaxRunAttemptsInBulk,
                DelayBetweenBulkRunAttemptsInSeconds = d.DelayBetweenBulkRunAttemptsInSeconds
            };
        }

        private string GetStatusDescription(JobDefinitionDto jobJeDefinitionDto)
        {
            switch (jobJeDefinitionDto.Status)
            {
                case JobStatusType.Healthy:
                    return SchedulingResources.StatusHealthyFormat;

                case JobStatusType.Running:
                    var seconds = jobJeDefinitionDto.StartedOn.HasValue ? _timeService.GetCurrentTime().Subtract(jobJeDefinitionDto.StartedOn.Value).TotalSeconds : 0;
                    return string.Format(SchedulingResources.StatusRunningFormat, seconds);

                case JobStatusType.Failed:
                    return string.Format(SchedulingResources.StatusFailedFormat, jobJeDefinitionDto.TimesFailedInRow);

                case JobStatusType.Defunct:
                    return SchedulingResources.StatusDefunctFormat;

                case JobStatusType.NoData:
                    return SchedulingResources.StatusNoDataFormat;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
