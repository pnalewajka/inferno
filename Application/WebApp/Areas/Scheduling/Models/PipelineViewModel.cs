﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class PipelineViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Order(0), StringLength(100)]
        [DisplayNameLocalized(nameof(SchedulingResources.PipelineNameLabel), typeof(SchedulingResources))]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
