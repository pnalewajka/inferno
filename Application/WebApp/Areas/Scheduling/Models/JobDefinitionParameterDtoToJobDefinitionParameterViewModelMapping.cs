﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobDefinitionParameterDtoToJobDefinitionParameterViewModelMapping : ClassMapping<JobDefinitionParameterDto, JobDefinitionParameterViewModel>
    {
        public JobDefinitionParameterDtoToJobDefinitionParameterViewModelMapping()
        {
            Mapping = d => new JobDefinitionParameterViewModel
            {
                Id = d.Id,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                JobDefinitionId = d.JobDefinitionId,
                ParameterKey = d.ParameterKey,
                ParameterValue = d.ParameterValue
            };
        }
    }
}
