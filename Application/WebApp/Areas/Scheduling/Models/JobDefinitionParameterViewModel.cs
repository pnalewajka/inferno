﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Scheduling.Resources;

namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobDefinitionParameterViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public long JobDefinitionId { get; set; }

        [Order(0), Required, StringLength(100)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionParameterKeyLabel), typeof(SchedulingResources))]
        public string ParameterKey { get; set; }

        [Order(0), Required, StringLength(100)]
        [DisplayNameLocalized(nameof(SchedulingResources.JobDefinitionParameterValueLabel), typeof(SchedulingResources))]
        public string ParameterValue { get; set; }
    }
}
