﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class PipelineDtoToPipelineViewModelMapping : ClassMapping<PipelineDto, PipelineViewModel>
    {
        public PipelineDtoToPipelineViewModelMapping()
        {
            Mapping = d => new PipelineViewModel
            {
                Id = d.Id,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                Name = d.Name
            };
        }
    }
}
