﻿using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Scheduling.Models
{
    public class JobLogViewModelToJobLogDtoMapping : ClassMapping<JobLogViewModel, JobLogDto>
    {
        public JobLogViewModelToJobLogDtoMapping()
        {
            Mapping = v => new JobLogDto
            {
                Id = v.Id,
                Message = v.Message,
                JobDefinitionId = v.JobDefinitionId,
                SchedulerName = v.SchedulerName,
                Host = v.Host,
                Pipeline = v.Pipeline,
                Severity = v.JobLogSeverity,
                Timestamp = v.Timestamp
            };
        }
    }
}
