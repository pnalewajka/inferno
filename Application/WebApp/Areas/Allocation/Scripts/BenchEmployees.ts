﻿module BenchEmployees {
    var dateFilterPrefix = "week-start";
    var startDateAttributeName = `${dateFilterPrefix}.week`;

    enum WeekNavigationDirections {
        Previous,
        Current,
        Next
    }

    function getMondayOfCurrentWeek(): Date {
        return DateHelper.createDateAsUTC(DateHelper.getThisWeekMonday(new Date()));
    }

    function getCurrentDate()
    {
        const filters = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), "filter");

        if (filters === null || filters.indexOf(dateFilterPrefix) === -1) {
            return DateHelper.createDateAsUTC(DateHelper.getThisWeekMonday(new Date()));
        }
        else {
            const startDateValue = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), startDateAttributeName);
            return DateHelper.createDateAsUTC(new Date(startDateValue!));
        }
    }

    function changeWeek(direction: WeekNavigationDirections)
    {
        const filters = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), "filter");

        var startDate: Date;
        var newUrl = window.location.href;

        if (filters === null || filters.indexOf(dateFilterPrefix) === -1)
        {
            var newFilters: string;

            if (filters !== null)
            {
                newFilters = `${filters},${dateFilterPrefix}`;
            }
            else
            {
                newFilters = `${dateFilterPrefix}`;
            }

            newUrl = UrlHelper.updateUrlParameter(newUrl, "filter", newFilters);
            startDate = getMondayOfCurrentWeek();
        }
        else
        {
            const startDateValue = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), startDateAttributeName);
            startDate = DateHelper.createDateAsUTC(new Date(startDateValue!));
        }

        switch (direction) {
            case WeekNavigationDirections.Next:
                startDate.setDate(startDate.getDate() + 7);
                break;
            case WeekNavigationDirections.Previous:
                startDate.setDate(startDate.getDate() - 7);
                break;
            case WeekNavigationDirections.Current:
            default:
                startDate = getMondayOfCurrentWeek();
                break;
        }
        
        newUrl = UrlHelper.updateUrlParameter(newUrl, startDateAttributeName, UrlHelper.dateUrlParameter(startDate));

        Forms.redirect(newUrl);
    }

    export function setNextWeek() {
        changeWeek(WeekNavigationDirections.Next);
    }

    export function setPreviousWeek() {
        changeWeek(WeekNavigationDirections.Previous);
    }

    export function setCurrentWeek() {
        changeWeek(WeekNavigationDirections.Current);
    }
    
    export function handleRowDoubleClick(grid: Grid.GridComponent, event: MouseEvent | JQueryEventObject)
    {
        var selectedEmployeeNames = grid.getSelectedRowData<any>().map(r => `${r.firstName} ${r.lastName}`);
        var employeeName = selectedEmployeeNames[0];
        var currentDate = getCurrentDate();

        var dateFrom = new Date();
        dateFrom.setDate(currentDate.getDate() - 14);

        var dateTo = new Date();
        dateTo.setDate(currentDate.getDate() + 49);

        var baseUrl = "/Allocation/EmployeeAllocation/List";
        baseUrl = UrlHelper.updateUrlParameter(baseUrl, "view", "minimal");
        baseUrl = UrlHelper.updateUrlParameter(baseUrl, "period-type", "week");
        baseUrl = UrlHelper.updateUrlParameter(baseUrl, "search", employeeName);
        baseUrl = UrlHelper.updateUrlParameter(baseUrl, "filter", "date-range,all-allocations");
        baseUrl = UrlHelper.updateUrlParameter(baseUrl, "date-range.from", UrlHelper.dateUrlParameter(dateFrom));
        baseUrl = UrlHelper.updateUrlParameter(baseUrl, "date-range.to", UrlHelper.dateUrlParameter(dateTo));

        Forms.open(baseUrl, event.ctrlKey);
    }
}