﻿module ActiveDirectoryGroup {
    export function adGroupIsReadonlyForUser(userId: number, groupRow: any, hasrole: boolean) {
        if (groupRow.ownerId == userId || hasrole) {
            return true;
        }

        return groupRow.managersIds.filter((u: number) => u == userId).length > 0;
    }

    export function trimDescriptionTexts(): void {
        $('.group-description').each((_, element) => {
            const $element = $(element);
            const $p = $element.children('p').first();
            const height = $element.innerHeight();

            if ($p.innerHeight() > height) {
                const text = $p.text().trim();
                const binarySearch = (leftBound: number, rightBound: number) => {
                    const threshold = 3;

                    if (rightBound - leftBound <= threshold) {
                        $p.text(text.substring(0, leftBound) + '…');

                        return;
                    }

                    const middle = Math.floor(leftBound + (rightBound - leftBound) / 2);

                    $p.text(text.substring(0, middle) + '…');

                    if ($p.innerHeight() > height) {
                        binarySearch(leftBound, middle);
                    } else {
                        binarySearch(middle, rightBound);
                    }
                };

                binarySearch(0, text.length);
            }
        });
    }
}

$(() => {
    ActiveDirectoryGroup.trimDescriptionTexts();
});