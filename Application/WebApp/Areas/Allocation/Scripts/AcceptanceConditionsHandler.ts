﻿namespace AcceptanceConditionsHandler {
    export enum CostApprovalPolicy {
        ManualApprovalAll = 0,
        ManualApprovalIfExceedsCompanyPolicy = 1,
        OwnThresholds = 2,
    }

    function getCostApprovalPolicy(): CostApprovalPolicy {
        return parseInt($("[name='CostApprovalPolicy']").val(), 10);
    }

    function isReinvoiceEnabled(): boolean {
        return $("[id='reinvoicing-to-customer']").is(":checked");
    }

    function getEditableOperation(controlId: string, isEditable: boolean): ClientSideOperations.IClientSideOperation {
        const operation = new ClientSideOperations.ClientSideEditableOperation();

        operation.targetElementId = controlId;
        operation.isEditable = isEditable;

        return operation;
    }

    function getVisibleOperation(controlId: string, isVisible: boolean): ClientSideOperations.IClientSideOperation {
        const operation = new ClientSideOperations.ClientSideVisibilityOperation();

        operation.targetElementId = controlId;
        operation.isVisible = isVisible;

        return operation;
    }

    function getReinvoiceOperations(isEditable: boolean): ClientSideOperations.IClientSideOperation[] {
        return [
            getVisibleOperation("reinvoice-currency-id", isEditable),
        ];
    }

    function getMaxValuesOperations(isEditable: boolean): ClientSideOperations.IClientSideOperation[] {
        return [
            getEditableOperation("max-hotel-costs", isEditable),
            getEditableOperation("max-total-flights-costs", isEditable),
            getEditableOperation("max-other-travel-costs", isEditable),
        ];
    }

    export function updateAcceptanceFunctions() {
        const approvalPolicy = getCostApprovalPolicy();
        const reinvoiceEnabled = isReinvoiceEnabled();

        [
            ...getMaxValuesOperations(approvalPolicy === CostApprovalPolicy.OwnThresholds),
            ...getReinvoiceOperations(reinvoiceEnabled)
        ].forEach(o => o.apply());
    }
}

$(() => {
    AcceptanceConditionsHandler.updateAcceptanceFunctions();
});
