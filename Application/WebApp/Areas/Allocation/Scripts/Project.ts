﻿module Project {
    export interface IProjectRow extends Grid.IRow {
        isAvailableForSalesPresentation: boolean;
    }

    export function adjustElementLayout(rows: IProjectRow[], e: Element): void {
        var showWarning = false;
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].isAvailableForSalesPresentation === false) {
                showWarning = true;
                break;
            }
        }

        var element = $(e);
        var linkElement = element.find('a');
        var icons = element.find('i');
        var warningIcon = icons.filter('i.fa-warning');
        
        if (showWarning) {
            icons.hide();

            if (warningIcon.length == 0) {
                linkElement.prepend("<i class='fa fa-warning'></i>");
            }
            else {
                warningIcon.show();
            }

            element.attr('title', Globals.resources.PresentationWithProjectsNotAvailableForSalesMessage);
        }
        else {
            icons.show();
            warningIcon.hide();
            element.removeAttr('title');
        }
    }
}