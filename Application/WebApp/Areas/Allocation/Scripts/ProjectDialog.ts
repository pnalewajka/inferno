﻿module ProjectDialog {
    var dialogWidth: string = "800px";
    var generateReportDialogWidth: string = "1000px";

    export function mergeProjects(dialogUrl: string, component: Grid.GridComponent) {
        const selectedEmployees = component.getSelectedRowData();
        let url = UrlHelper.updateUrlParameter(dialogUrl, "ids", selectedEmployees.map(r => r.id).join(","));

        Dialogs.showDialog({
            actionUrl: url,
            onBeforeInit:
            (dialog) => {
                Dialogs.setWidth(dialog, dialogWidth);
            }
        });
    }

    export function onTimeTrackingDailyHoursGenerateClicked(dialogUrl: string, component: Grid.GridComponent, reportcode: string) {
        const selectedProjects = component.getSelectedRowData();
        let param = "reportCode=" + reportcode + "&projectIds";
        let projectIds = selectedProjects.map(r => r.id).join(",");
        let url = UrlHelper.updateUrlParameter(dialogUrl, param, projectIds);
        Dialogs.showDialog({
            actionUrl: url,
            onBeforeInit:
            (dialog) => {
                Dialogs.setWidth(dialog, generateReportDialogWidth);
            }
        });
    }
}