﻿module AllocationDialog {
    var dialogWidth: string = "550px";

    export function onReportClicked(dialogUrl: string) {
        Dialogs.showDialog({
            actionUrl: dialogUrl,
            eventHandler: eventArgs => {
                if (eventArgs.isOk) {
                    Utils.reloadPage();
                }
            },
            onBeforeInit: (dialog) => {
                Dialogs.setWidth(dialog, dialogWidth);
            }
        });
    }

    export function onCommentClicked(dialogUrl: string) {
        const component = Grid.getComponent($("table.table")[0]);
        const selectedEmployee = component.getSelectedRowData()[0] as any;
        let url = UrlHelper.updateUrlParameter(dialogUrl, "id", selectedEmployee.id);
        url = UrlHelper.updateUrlParameter(url, "date", selectedEmployee.onBenchSince);

        Dialogs.showDialog({
            actionUrl: url,
            eventHandler: eventArgs => {
                if (eventArgs.isOk) {
                    Utils.reloadPage();
                }
            },
            onBeforeInit: (dialog) => {
                Dialogs.setWidth(dialog, dialogWidth);
            }
        });
    }
}