﻿﻿module AllocationRequest {
    export function openCreateProjectDialog(this: any, dialogUrl: string): void {
        const currentTarget: Element = this.event.currentTarget;
        const modal = $(currentTarget).closest(".modal");

        Dialogs.showDialog({
            actionUrl: dialogUrl,
            eventHandler: (eventArgs: Dialogs.EventArgs) => {
                modal.show();
                if (eventArgs.isOk) {
                    CommonDialogs.pleaseWait.hide();
                    ValuePicker.pickItemAndClose(modal, { id: eventArgs.model.Id, toString: eventArgs.model.DisplayName });
                }
            }, onBeforeInit: () => { modal.hide(); }
        });
    }
}