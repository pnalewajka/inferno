﻿namespace Allocation.Screen {
    export interface IFooterAllocationsColorsLegendProps {
        blockTypes: StatusBlockEnum[]
    }

    export let Translations: Interfaces.ITranslations;

    export const allocationStateChange = new Atomic.Events.EventDispatcher();

    export class AllocationGrid extends Dante.Components.StoreComponent<
        Store.IAllocationStoreState,
        Allocation.Interfaces.IGridProps,
        {}
        > {
        private atomicGrid = new Grid.GridComponent($("body")) as dynamic;

        constructor() {
            super(Store.defaultStore());

            Grid.GridComponent.prototype.getSelectedRowData = (() => {
                const { selection } = this.store.getState();
                const { selectedIds } = selection;

                const rows = selectedIds.map(id => ({ id, toString: id.toString() }));

                return rows;
            }) as (<T extends Grid.IRow>() => T[]);
        }

        public componentWillMount(): void {
            super.componentWillMount();
            Translations = { ...Globals.resources, ...this.props.translations };
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            this.atomicGrid.refreshButtonAvailability();
        }

        public render(): JSX.Element {
            return (
                <AllocationScroll dispacher={allocationStateChange}>
                    <InlineDialog eventEmiter={requestDetailsEvent} onClose={() => { Store.defaultStore().dispatch(Store.Actions.cancelEditAllocation()); }}>
                        <RequestInlineForm eventEmiter={requestDetailsEvent} />
                    </InlineDialog>
                    <div key="a" className="show-holidays allocation-group">
                        <DateRangeHeader />
                        <EmployeeAllocationList />
                    </div>
                </AllocationScroll>
            );
        }
    }

    class AllocationScroll extends Dante.DetachedScroll.Component {
        protected globalHeaderHeight(): number {
            return $(".allocation-group-header").height();
        }

        protected globalColumnWidth(): number {
            return $(".employee-info").outerWidth();
        }
        protected dataWidth(): number {
            return $(".allocation-group-row").width();
        }

        protected dataHeight(): number {
            return $(".allocation-group-body").height();
        }

        protected horizontalElementsToScroll(): string {
            return ".employee-info, .column-header, .detached-scroll-content-shadow";
        }

        protected verticalElementsToScroll(): string {
            return ".allocation-group-header";
        }
    }

    export const Footer: React.StatelessComponent<{}> = (): JSX.Element => {
        return (
            <div className="footer-items-wraper">
                <SummaryLegendComponent />
            </div>
        );
    };


    export class SummaryLegendComponent extends Dante.Components.StoreComponent<Store.IAllocationStoreState, {}, {
        showLegend: boolean,
        availableBlockTypes: StatusBlockEnum[]
    }> {
        public constructor() {
            super(Store.defaultStore());
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { selectedView, normalBlocks } = state.blocks;

            const availableBlockTypes = Dante.Utils.Distinct(
                normalBlocks.map(b => b.type)
            );

            this.updateState({
                showLegend: !!(selectedView & Screen.AllocationViewType.SummaryView),
                availableBlockTypes
            });
        }

        public render(): JSX.Element | null {
            if (!this.state.showLegend) {
                return null;
            }

            return <SummaryLegendComponentContent blockTypes={this.state.availableBlockTypes} />;
        }
    }

    export const SummaryLegendComponentContent: React.StatelessComponent<IFooterAllocationsColorsLegendProps> = (props: IFooterAllocationsColorsLegendProps): JSX.Element => {
        const allBlockTypes = [
            { type: StatusBlockEnum.Full, text: Translations.legendNameFormatFull },
            { type: StatusBlockEnum.Empty, text: Translations.legendNameFormatHoliday },
            { type: StatusBlockEnum.Open, text: Translations.legendNameFormatOpen },
            { type: StatusBlockEnum.PartOpen, text: Translations.legendNameFormatPartOpen },
            { type: StatusBlockEnum.Holiday, text: Translations.legendNameFormatHoliday },
            { type: StatusBlockEnum.Vacations, text: Translations.legendNameFormatVacations },
            { type: StatusBlockEnum.OverAllocated, text: Translations.legendNameFormatOverAllocated },
            { type: StatusBlockEnum.PartOverAllocated, text: Translations.legendNameFormatPartOverAllocated },
            { type: StatusBlockEnum.Inactive, text: Translations.legendNameFormatInactive }
        ];

        const typesToDisplay = allBlockTypes.filter(b => props.blockTypes.indexOf(b.type) !== -1);

        return (
            <div className="footer-items footer-items-legend">
                {
                    typesToDisplay.map((b, index) => (
                        <span key={index} className="legend-part-container">
                            <span title={b.text} className={`legend-part legend-part-${StatusBlockEnum[b.type].toLowerCase()}`}></span>
                            <span>{b.text} </span>
                        </span>
                    ))
                }
            </div>
        );
    };
}
