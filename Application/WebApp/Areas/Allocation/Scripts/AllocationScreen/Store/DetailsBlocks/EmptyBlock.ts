﻿namespace Allocation.Store.Block {
    interface IAllocationCompare {
        id: number;
        hours: number;
    }

    export class EmptyBlock
        implements Interfaces.IAllocationDetailsDisplayBlock {
        public employeeId: number;
        public weekNumber: number;
        public dayNumber: number;
        public blockLength: number;
        public allocations: Interfaces.IAllocationGroupProject[];
        public contractedHours: number;
        public type?: Interfaces.DisplayBlockType = Allocation.Screen.StatusBlockEnum.Empty;

        public constructor(employeeId: number, weekNumber: number) {
            this.employeeId = employeeId;
            this.weekNumber = weekNumber;
            this.allocations = [];
            this.dayNumber = 0;
            this.contractedHours = 0;
            this.blockLength = 1;
        }

        public compare(block: Interfaces.IAllocationDetailsDisplayBlock): boolean {
            return block.type !== undefined &&  this.type === block.type;
        }

        public extendLength(newBlock: Interfaces.IAllocationDetailsDisplayBlock): void {
            this.blockLength += newBlock.blockLength;
        }
    }
}
