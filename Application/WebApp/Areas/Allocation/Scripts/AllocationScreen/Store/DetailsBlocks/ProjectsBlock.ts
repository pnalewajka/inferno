﻿namespace Allocation.Store.Block {
    interface IAllocationCompare {
        id: number;
        hours: number;
    }

    export class ProjectsBlock
        implements Interfaces.IAllocationDetailsDisplayBlock {
        public type?: Interfaces.DisplayBlockType;
        public employeeId: number;
        public weekNumber: number;
        public dayNumber: number;
        public blockLength: number;
        public allocations: Interfaces.IAllocationGroupProject[];
        public contractedHours: number;
        public certainty: Screen.AllocationCertainty = Screen.AllocationCertainty.Certain;

        public constructor(employeeId: number, weekNumber: number, day: number, contractedHours: number, allocations: Interfaces.IAllocationGroupProject[]) {
            this.employeeId = employeeId;
            this.weekNumber = weekNumber;
            this.dayNumber = day;
            this.contractedHours = contractedHours;
            this.blockLength = 1;

            this.allocations = this.calculateAllocationsHoursPerPeriod(allocations);
        }

        private calculateAllocationsHoursPerPeriod(allocations: Interfaces.IAllocationGroupProject[] = this.allocations): Interfaces.IAllocationGroupProject[]  {
            return allocations.map(a => ({ ...a, hoursPerPeriod: a.hours * this.blockLength }));
        }

        public compare(block: Interfaces.IAllocationDetailsDisplayBlock): boolean {
            return this.contractedHours === block.contractedHours
                && block.contractedHours > 0
                && this.compareAllocationCollections(this.allocations, block.allocations);
        }

        public extendLength(newBlock: Interfaces.IAllocationDetailsDisplayBlock): void {
            this.blockLength += newBlock.blockLength;
            this.allocations = this.calculateAllocationsHoursPerPeriod();
        }

        private comparableAllocation(allocation: Interfaces.IAllocationGroupProject): IAllocationCompare {
            const comparableAllocation: IAllocationCompare = {
                id: allocation.requestId,
                hours: allocation.hours
            };

            return comparableAllocation;
        }

        private compareAllocationCollections(allocationsA: Interfaces.IAllocationGroupProject[], allocationsB: Interfaces.IAllocationGroupProject[]) {
            return JSON.stringify(allocationsA.map(this.comparableAllocation)) === JSON.stringify(allocationsB.map(this.comparableAllocation));
        }
    }
}
