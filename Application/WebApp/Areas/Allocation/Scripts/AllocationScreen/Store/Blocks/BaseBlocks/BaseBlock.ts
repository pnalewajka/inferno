﻿namespace Allocation.Store.Block {
    export abstract class BaseBlock {
        protected constructor(employeeId: number, weekNumber: number) {
            this.employeeId = employeeId;
            this.weekNumber = weekNumber;
            this.blockLength = 1;
        }

        public employeeId: number;
        public weekNumber: number;
        public blockLength: number;
        public allocatedHours: number;
        public contractedHours: number;
        public type: Screen.StatusBlockEnum;
        public certainty: Screen.AllocationCertainty = Screen.AllocationCertainty.Certain;

        public compare(block: Interfaces.IAllocationNormalDisplayBlock): boolean {
            return this.compareType(block.type)
                && this.compareBlockHours(this, block)
                && this.certainty === block.certainty;
        }

        public extendLength(newItem: Interfaces.IAllocationNormalDisplayBlock): void {
            this.allocatedHours += newItem.allocatedHours;
            this.contractedHours += newItem.contractedHours;
            this.blockLength += newItem.blockLength;
        }

        protected compareType(type: Screen.StatusBlockEnum): boolean {
            return type === this.type;
        }

        protected compareBlockHours(blockA: Interfaces.IAllocationNormalDisplayBlock, blockB: Interfaces.IAllocationNormalDisplayBlock): boolean {
            return true;
        }

        protected displayHoursString(value: number): string {
            return value.toFixed(2).replace(/[.,]00$/, "");
        }

        protected displayHoursShortString(value: number): string {
            return value.toFixed(1).replace(/[.,]0$/, "");
        }

        public get displayText(): string {
            return Screen.StatusBlockEnum[this.type];
        }

        public get displayShortText(): string {
            return this.displayText;
        }

        public get showShortText(): boolean {
            return this.blockLength < 2;
        }
    }
}
