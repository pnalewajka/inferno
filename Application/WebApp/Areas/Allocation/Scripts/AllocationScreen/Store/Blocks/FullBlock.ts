﻿namespace Allocation.Store.Block {
    export class FullBlock
        extends BaseBlock
        implements Interfaces.IAllocationNormalDisplayBlock {

        public constructor(employeeId: number, weekNumber: number, hours: number) {
            super(employeeId, weekNumber);

            this.contractedHours = hours;
            this.allocatedHours = hours;
            this.type = Allocation.Screen.StatusBlockEnum.Full;
        }

        public get displayText(): string {
            return Allocation.Screen.Translations.blockNameFormatFull;
        }
    }
}
