﻿namespace Allocation.Store.Block {
    export class HolidayBlock
        extends BaseBlock
        implements Interfaces.IAllocationNormalDisplayBlock {

        public constructor(employeeId: number, weekNumber: number) {
            super(employeeId, weekNumber);

            this.contractedHours = 0;
            this.allocatedHours = 0;
            this.type = Allocation.Screen.StatusBlockEnum.Holiday;
        }

        public get displayText(): string {
            return Allocation.Screen.Translations.blockNameFormatHoliday;
        }

        public get displayShortText(): string {
            return Allocation.Screen.Translations.blockNameFormatHolidayShort;
        }
    }
}
