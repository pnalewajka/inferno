﻿namespace Allocation.Store.Block {
    export class OpenBlock
        extends BaseBlock
        implements Interfaces.IAllocationNormalDisplayBlock {

        public constructor(employeeId: number, weekNumber: number, contractedHours: number, allocatedHours: number) {
            super(employeeId, weekNumber);
            this.contractedHours = contractedHours;
            this.allocatedHours = allocatedHours;
            this.type = Allocation.Screen.StatusBlockEnum.Open;
        }

        private get displayHours(): number {
            return this.contractedHours - this.allocatedHours;
        }

        public get displayText(): string {
            return Strings.format(Allocation.Screen.Translations.blockNameFormatOpen, this.displayHoursString(this.displayHours));
        }

        public get displayShortText(): string {
            return Strings.format(Allocation.Screen.Translations.blockNameFormatOpenShort, this.displayHoursShortString(this.displayHours));
        }
    }
}
