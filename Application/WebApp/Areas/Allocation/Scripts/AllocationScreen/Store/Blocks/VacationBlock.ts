﻿namespace Allocation.Store.Block {
    export class VacationBlock
        extends BaseBlock
        implements Interfaces.IAllocationNormalDisplayBlock {

        public constructor(employeeId: number, weekNumber: number, dayNumber: number) {
            super(employeeId, weekNumber);

            this.contractedHours = 0;
            this.allocatedHours = 0;
            this.type = Allocation.Screen.StatusBlockEnum.Vacations;
        }

        public get displayText(): string {
            return Allocation.Screen.Translations.blockNameFormatVacation;
        }

        public get displayShortText(): string {
            return Allocation.Screen.Translations.blockNameFormatVacationShort;
        }
    }
}
