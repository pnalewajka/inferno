﻿namespace Allocation.Store.Block {
    export class OverAllocatedBlock
        extends BaseBlock
        implements Interfaces.IAllocationNormalDisplayBlock {

        public constructor(employeeId: number, weekNumber: number, contractedHours: number, allocatedHours: number) {
            super(employeeId, weekNumber);
            this.contractedHours = contractedHours;
            this.allocatedHours = allocatedHours;
            this.calculateType();
        }

        private calculateType(): void {
            this.type = this.isStronglyOverAllocated()
                ? Allocation.Screen.StatusBlockEnum.OverAllocated
                : Allocation.Screen.StatusBlockEnum.PartOverAllocated;
        }

        private get displayHours(): number {
            return this.allocatedHours - this.contractedHours;
        }

        private allocationPercentage(): number {
            if (this.contractedHours > 0) {
                return (this.allocatedHours / this.contractedHours) * 100;
            }

            return 0;
        }

        private isStronglyOverAllocated(): boolean {
            return this.allocationPercentage() >= 120;
        }

        public get displayText(): string {
            return Strings.format(Allocation.Screen.Translations.blockNameFormatOver, this.displayHoursString(this.displayHours));
        }

        public get displayShortText(): string {
            return Strings.format(Allocation.Screen.Translations.blockNameFormatOverShort, this.displayHoursShortString(this.displayHours));
        }

        public extendLength(newItem: Interfaces.IAllocationNormalDisplayBlock): void {
            super.extendLength(newItem);
            this.calculateType();
        }
    }
}
