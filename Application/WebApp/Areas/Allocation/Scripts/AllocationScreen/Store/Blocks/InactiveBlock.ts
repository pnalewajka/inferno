﻿namespace Allocation.Store.Block {
    export class InactiveBlock
        extends BaseBlock
        implements Interfaces.IAllocationNormalDisplayBlock {

        public constructor(employeeId: number, weekNumber: number, dayNumber: number) {
            super(employeeId, weekNumber);

            this.contractedHours = 0;
            this.allocatedHours = 0;
            this.type = Allocation.Screen.StatusBlockEnum.Inactive;
        }

        public get displayText(): string {
            return Allocation.Screen.Translations.blockNameFormatInactive;
        }

        public get displayShortText(): string {
            return Allocation.Screen.Translations.blockNameFormatInactiveShort;
        }
    }
}
