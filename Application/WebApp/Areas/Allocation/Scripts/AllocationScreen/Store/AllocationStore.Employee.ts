﻿namespace Allocation.Store.Employee {
    export interface IState {
        list: Interfaces.IEmployee[];
        permissions: Interfaces.IEmployeePermissions;
    }

    export interface IAction extends Redux.Action {
    }

    export const defaultPermissions: Interfaces.IEmployeePermissions = {
        canEditRequest: false,
        canViewRequest: false,
        canEditEmployee: false,
        canViewEmployee: false,
        canCloneRequest: false,
        canDeleteRequest: false,
        canEditProject: false,
        canViewProject: false,
    };

    export function store(state: IState = { list: [], permissions: defaultPermissions }, action: Actions.StoreAction<Interfaces.IServerResponse>): IState {
        if (action.type == Actions.AllocationActionType.loadData) {
            return loadData(state, action.data);
        }

        return state;
    }

    function loadData(state: IState, data: Interfaces.IServerResponse): IState {
        const list: Interfaces.IEmployee[] = data.employees.map(e => ({
            id: e.id,
            fullName: e.fullName,
            locationName: e.locationName,
            jobMatrixLevels: e.jobMatrixLevels,
            jobProfileNames: e.jobProfileNames,
            lineManagerId: e.lineManagerId,
            lineManagerName: e.lineManagerName,
            orgUnitName: e.orgUnitName,
            processingStatus: e.allocationBlocks.some(a => a.processingStatus === Screen.ProcessingStatus.Processing)
                ? Screen.ProcessingStatus.Processing
                : Screen.ProcessingStatus.Processed 
        }));

        return { list, permissions: data.permissions };
    }
}
