﻿namespace Allocation.Store.DateRange {
    interface IDateRangeBase {
        number: number;
        displayedDays: number;
        displayedDaysWithoutWeekend: number;
    }

    export interface IWeek extends IDateRangeBase {
    }

    export interface IMonth extends IDateRangeBase {
        description: string;
    }

    export interface IState {
        statusFrom: Date;
        statusTo: Date;
        weeks: IWeek[];
        months: IMonth[];
        days: Date[];
    }

    export interface IAction extends Redux.Action {
    }

    type BlockActions =
        Actions.StoreAction<Interfaces.IServerResponse> |
        Actions.StoreAction<void>;

    function getDefaultState(): IState {
        return { statusFrom: new Date(), statusTo: new Date(), weeks: [], months: [], days: [] };
    }

    export function store(state: IState = getDefaultState(), action: BlockActions): IState {

        if (action.type === Actions.AllocationActionType.loadData) {
            return loadData(state, action.data as Interfaces.IServerResponse);
        }

        return state;
    }

    function loadData(state: IState, data: Interfaces.IServerResponse): IState {

        const months: IMonth[] = data.dateRange.months.filter(m => m.count).map((m, i) => {
            return {
                number: new Date(m.date.getDate()).getMonth() + 1,
                description: m.monthDescription,
                displayedDays: m.count,
                displayedDaysWithoutWeekend: m.count - m.weekendCount
            };
        });

        const weeks: IWeek[] = data.dateRange.weeks.filter(w => w.count > 0).map((w, i) => {
            return {
                number: w.week,
                displayedDays: w.count,
                displayedDaysWithoutWeekend: w.count - w.weekendCount
            };
        });

        const days: Date[] = data.dateRange.days;

        return {
            statusFrom: data.statusFrom,
            statusTo: data.statusTo,
            weeks,
            months,
            days,
        };
    }
}
