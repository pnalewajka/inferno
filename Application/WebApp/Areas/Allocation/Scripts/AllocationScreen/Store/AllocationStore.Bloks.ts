﻿namespace Allocation.Store.Block {
    const maxTime = 8640000000000000;

    export interface IBooleanStateSettings {
        includeWeekends: boolean;
        showPlanned: boolean;
        showOvertime: boolean;
        showVacations: boolean;
        showHolidays: boolean;
        showProjectColor: boolean;
    }

    export interface IOverlapingBlock<TBlock> {
        overlapingArray: boolean[];
        block: TBlock;
    }

    export enum OverlayType {
        planned,
        partAbsence
    }

    export interface IState extends IBooleanStateSettings {
        employees: Allocation.Interfaces.IEmployeeRow[];
        weeks: Interfaces.IWeekRange[];
        temporaryBlocks: Interfaces.IAllocationTemporaryBlock[];
        displayBlocks: Interfaces.IAllocationDisplayBlock[];
        normalBlocks: Interfaces.IAllocationNormalDisplayBlock[];
        detailsBlocks: Interfaces.IAllocationDetailsDisplayBlock[];
        overlayBlocks: Interfaces.IAllocationOverlayBlock[];

        statusFrom: Date;
        statusTo: Date;

        extendedView: boolean;
        selectedView: Allocation.Screen.AllocationViewType;
    }

    export interface IAction extends Redux.Action {
    }


    export interface IWeekBlockData {
        employee: Interfaces.IEmployeeRow;
        week: Interfaces.IWeekRange;

        allocations: IOverlapingBlock<Interfaces.IAllocationBlock>[];
        plannedAllocations: IOverlapingBlock<Interfaces.IAllocationBlock>[];
        periods: IOverlapingBlock<Interfaces.IEmploymentPeriodBlocks>[];
        holidays: boolean[];
        absences: boolean[];
        partialAbsences: number[];
    }

    type BlockActions =
        Actions.StoreAction<Interfaces.IServerResponse> |
        Actions.StoreAction<Actions.IToggleViewAction> |
        Actions.StoreAction<keyof IBooleanStateSettings> |
        Actions.StoreAction<void> |
        Actions.StoreAction<Interfaces.IAllocationTemporaryBlock> |
        Actions.StoreAction<number>;

    function getStoreBooleanState(storeKey: string): boolean {
        const filters = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), "filter")
        if (filters !== null) {
            return Dante.Utils.SelectMany(filters.split(","), s => s.split(" ")).indexOf(storeKey) !== -1;
        }

        return false;
    }

    function getDefaultState(): IState {
        let selectedView: Screen.AllocationViewType = getStoreBooleanState("detailed")
            ? Screen.AllocationViewType.DetailedView
            : Screen.AllocationViewType.SummaryView;

        return {
            employees: [],
            temporaryBlocks: [],
            weeks: [],
            displayBlocks: [],
            normalBlocks: [],
            detailsBlocks: [],
            overlayBlocks: [],
            includeWeekends: getStoreBooleanState("weekends"),
            statusFrom: new Date(),
            statusTo: new Date(),
            selectedView,
            showPlanned: getStoreBooleanState("planned"),
            showOvertime: true,
            showVacations: true,
            showHolidays: true,
            showProjectColor: getStoreBooleanState("colors"),
            extendedView: false,
        };
    }

    function setDefaulBooleanVisibility(storeKey: keyof IBooleanStateSettings, value: boolean): void {
        if (localStorage !== undefined) {
            localStorage.setItem(`allocation.${storeKey}`, value ? "true" : "false");
        }
    }

    function getBooleanProperty<T extends IBooleanStateSettings, K extends keyof IBooleanStateSettings>(obj: T, key: K): boolean {
        return !!obj[key];
    }

    export function store(state: IState = getDefaultState(), action: BlockActions): IState {
        const newState: IState = {
            ...{}, ...state
        };

        if (action.type === Actions.AllocationActionType.toggleBooleanSetting) {
            const propertyName: keyof IBooleanStateSettings = action.data as keyof IBooleanStateSettings;
            const newValue = !getBooleanProperty(newState, propertyName);

            setDefaulBooleanVisibility(propertyName, newValue);
            const updatedState = {
                ...newState,
                ... {
                    [propertyName]: !getBooleanProperty(newState, propertyName)
                }
            };

            if (propertyName === "showProjectColor") {
                return updatedState;
            } else {
                return recalculateData(updatedState);
            }
        }

        if (action.type === Actions.AllocationActionType.editAllocation) {
            return editAllocation(newState, action.data as Interfaces.IAllocationTemporaryBlock);
        }

        if (action.type === Actions.AllocationActionType.cancelEditAllocation) {
            return cancelEditAllocation(newState);
        }

        if (action.type === Actions.AllocationActionType.loadData) {
            return loadData(newState, action.data as Interfaces.IServerResponse);
        }

        return newState;
    }

    function cancelEditAllocation(state: IState): IState {
        return recalculateData({
            ...state,
            ...{
                temporaryBlocks: []
            }
        });
    }


    function editAllocation(state: IState, blockPartial: Interfaces.IAllocationTemporaryBlock): IState {
        return recalculateData({
            ...state,
            ...{
                temporaryBlocks:
                [
                    ...state.temporaryBlocks.filter(b => b.employeeId !== blockPartial.employeeId && b.requestId !== blockPartial.requestId),
                    blockPartial
                ]
            }
        });
    }

    function recalculateData(state: IState): IState {
        const resultBlocks = state.employees.reduce((result, employee) => {
            result.displayBlocks = [...result.displayBlocks, ...getEmployeeBlocks(employee, state.includeWeekends, state.statusFrom, state.statusTo)];

            return result;
        }, {
                displayBlocks: [] as Interfaces.IAllocationDisplayBlock[],
                normalBlocks: [] as Interfaces.IAllocationNormalDisplayBlock[],
                detailsBlocks: [] as Interfaces.IAllocationDetailsDisplayBlock[],
                overlayBlocks: [] as Interfaces.IAllocationOverlayBlock[],
            });

        enumerateEmployeeBlocks(state, (data) => {
            resultBlocks.normalBlocks = [...resultBlocks.normalBlocks, ...getEmployeeNormalBlocks(data, state)];
            resultBlocks.detailsBlocks = [...resultBlocks.detailsBlocks, ...getEmployeeDetailsBlocks(data, state)];
            const overlay = getEmployeeOverlayBlocks(data, state);

            if (overlay !== undefined) {
                resultBlocks.overlayBlocks = [
                    ...resultBlocks.overlayBlocks,
                    overlay
                ];
            }

            if (data.partialAbsences.some(p => p !== 0)) {
                resultBlocks.overlayBlocks = [
                    ...resultBlocks.overlayBlocks,
                    {
                        employeeId: data.employee.id,
                        weekNumber: data.week.week,
                        overlapingArray: data.partialAbsences.map(r => r !== 0),
                        type: Allocation.Store.Block.OverlayType.partAbsence
                    }
                ]
            }
        });

        return {
            ...state,
            ...{
                displayBlocks: resultBlocks.displayBlocks,
                normalBlocks: resultBlocks.normalBlocks,
                detailsBlocks: resultBlocks.detailsBlocks,
                overlayBlocks: resultBlocks.overlayBlocks,
            }
        };
    }

    function loadData(state: IState, data: Interfaces.IServerResponse): IState {
        const extendedView = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), "view") !== "minimal";

        return recalculateData({
            ...state,
            ...{
                employees: data.employees,
                weeks: data.dateRange.weeks,
                statusFrom: data.statusFrom,
                statusTo: data.statusTo,
                extendedView,
            }
        });
    }

    function enumerateEmployeeBlocks(state: IState, weekAction: (data: IWeekBlockData) => void): void {
        const {
            employees,
            weeks,
            temporaryBlocks
        } = state;

        employees.forEach(employee => {
            weeks.forEach(week => {
                const thisEmployeeAllocationGroup = employee
                    .allocationBlocks
                    .map(allocation => {
                        const [tempBlock] = temporaryBlocks.filter(b => b.employeeId === employee.id && b.requestId === allocation.requestId);

                        if (tempBlock === undefined) {
                            return allocation;
                        }

                        const startDate = tempBlock === undefined || tempBlock.startDate === undefined
                            ? allocation.startDate
                            : tempBlock.startDate;

                        const endDate = tempBlock === undefined || tempBlock.endDate === undefined
                            ? allocation.endDate
                            : tempBlock.endDate;

                        const allocationDayHours = tempBlock === undefined || tempBlock.hours === undefined || tempBlock.hours.length === 0
                            ? allocation.allocationDayHours
                            : tempBlock.hours;

                        const certainty = tempBlock === undefined || tempBlock.certainty === undefined
                            ? allocation.certainty
                            : tempBlock.certainty;

                        return {
                            ...allocation,
                            ... { startDate, endDate, allocationDayHours, certainty }
                        };
                    })
                    .filter(allocation => isOverlaping(allocation.startDate, allocation.endDate, week.weekStart, week.weekEnd))
                    .reduce((all, allocation) => {
                        const allocationOverlapingArray = overlapingArray(week.weekStart, week.weekEnd, allocation.startDate, allocation.endDate);
                        const thisWeekAllocation: IOverlapingBlock<Interfaces.IAllocationBlock> = {
                            overlapingArray: allocationOverlapingArray,
                            block: allocation
                        };

                        if ((state.showPlanned ? true : allocation.certainty === Screen.AllocationCertainty.Certain)) {
                            all.thisWeekAllocations = [...all.thisWeekAllocations, thisWeekAllocation];
                        }

                        if (allocation.certainty !== Screen.AllocationCertainty.Certain) {
                            all.thisWeekPlannedAllocations = [...all.thisWeekPlannedAllocations, thisWeekAllocation];
                        }

                        return all;
                    }, {
                        thisWeekAllocations: [] as IOverlapingBlock<Interfaces.IAllocationBlock>[],
                        thisWeekPlannedAllocations: [] as IOverlapingBlock<Interfaces.IAllocationBlock>[],
                    });

                const thisWeek = thisEmployeeAllocationGroup.thisWeekAllocations;
                const plannedOverlays = thisEmployeeAllocationGroup.thisWeekPlannedAllocations;
                const thisPeriods: IOverlapingBlock<Interfaces.IEmploymentPeriodBlocks>[] = employee.employmentPeriodBlocks
                    .filter(e => isOverlaping(e.startDate, e.endDate, week.weekStart, week.weekEnd))
                    .map(period => ({
                        overlapingArray: overlapingArray(week.weekStart, week.weekEnd, period.startDate, period.endDate),
                        block: period
                    }));

                const thisHolidays = !state.showHolidays
                    ? []
                    : fillOverlapingArray(week.weekStart, week.weekEnd, employee.holidays);

                const employeeAbsences = !state.showVacations
                    ? []
                    : employee.absences
                        .filter(a => isOverlaping(a.startDate, a.endDate, week.weekStart, week.weekEnd));

                const thisAbsences: Interfaces.IAbsencePeriodBlock[] = employeeAbsences
                    .map(a => overlapingArray(week.weekStart, week.weekEnd, a.startDate, a.endDate).map(r => r ? a.hours : 0))
                    .reduce((absenceArray, absence) => {

                        return absence.map((a, i) => {
                            const [period] = thisPeriods.filter(p => p.overlapingArray[i]);
                            const contractedHours = period === undefined ? 0 : period.block.contractedHours[i];

                            const absenceObject: Interfaces.IAbsencePeriodBlock = {
                                isAbsence: a > 0,
                                isPartialAbsence: a < contractedHours,
                                hours: a,
                            };

                            return absenceObject;
                        });
                    }, [] as Interfaces.IAbsencePeriodBlock[]);

                weekAction({
                    employee,
                    week,
                    allocations: thisEmployeeAllocationGroup.thisWeekAllocations,
                    plannedAllocations: thisEmployeeAllocationGroup.thisWeekPlannedAllocations,
                    periods: thisPeriods,
                    holidays: thisHolidays,
                    absences: thisAbsences.map(a => a.isAbsence && !a.isPartialAbsence),
                    partialAbsences: thisAbsences.filter(a => a.isPartialAbsence).map(a => a.hours),
                });
            });
        });
    }

    function isPartialAbsence(absence: Interfaces.IAbsenceBlock, contractedHours: number): boolean {
        return isSameDay(absence.startDate, absence.endDate) && absence.hours < contractedHours;
    }

    function getEmployeeOverlayBlocks(data: IWeekBlockData, state: Block.IBooleanStateSettings): Interfaces.IAllocationOverlayBlock | undefined {
        if (state.showPlanned || data.plannedAllocations.length === 0) {
            return undefined;
        }

        const daysInWeek = state.includeWeekends ? 7 : 5;

        const overlapingArray = Dante.Utils.Range(daysInWeek).map(day => data.plannedAllocations.some(p => p.overlapingArray[day]));

        return {
            employeeId: data.employee.id,
            weekNumber: data.week.week,
            overlapingArray,
            type: Allocation.Store.Block.OverlayType.planned,
        } as Interfaces.IAllocationOverlayBlock;
    }

    function getEmployeeDetailsBlocks(data: IWeekBlockData, state: Block.IBooleanStateSettings): Interfaces.IAllocationDetailsDisplayBlock[] {
        const {
            week,
            periods,
            absences,
            holidays,
            allocations
        } = data;

        const employeeId = data.employee.id;

        const daysInWeek = state.includeWeekends ? 7 : 5;

        const blocks = Dante.Utils.Range(daysInWeek).reduce((previosuValue: Interfaces.IAllocationDetailsDisplayBlock[], currentValue: number) => {
            const isAbsence = !!absences[currentValue];
            const isHoliday = !!holidays[currentValue];
            const [thisDayContractedHours] = periods
                .filter(p => p.overlapingArray[currentValue]);

            let newItem: Interfaces.IAllocationDetailsDisplayBlock = new EmptyBlock(employeeId, week.week);
            if (thisDayContractedHours === undefined || thisDayContractedHours.block.contractedHours[currentValue] === 0) {
                newItem.type = Allocation.Screen.StatusBlockEnum.Inactive;
            } else if (isHoliday) {
                newItem.type = Allocation.Screen.StatusBlockEnum.Holiday;
            } else if (isAbsence) {
                newItem.type = Allocation.Screen.StatusBlockEnum.Vacations;
            } else {
                const thisDay: Interfaces.IAllocationGroupProject[] = allocations
                    .filter(t => t.overlapingArray[currentValue] && t.block.allocationDayHours[currentValue] > 0)
                    .map(a => {
                        const hours = a.block.allocationDayHours[currentValue];

                        const projectGroup: Interfaces.IAllocationGroupProject = {
                            requestId: a.block.requestId,
                            projectId: a.block.projectId,
                            projectName: a.block.projectName,
                            projectShortName: a.block.projectShortName,
                            projectAcronym: a.block.projectAcronym || "",
                            projectColor: a.block.projectColor,
                            projectColorIsDark: a.block.projectColorIsDark,
                            certainty: a.block.certainty,
                            hours,
                            hoursPerPeriod: hours
                        };

                        return projectGroup;
                    });

                const [thisDayContractedHours] = periods
                    .filter(p => p.overlapingArray[currentValue])
                    .map(p => p.block.contractedHours[currentValue]);

                newItem = new ProjectsBlock(employeeId, week.week, currentValue, thisDayContractedHours, thisDay);
            }

            const [lastItem] = previosuValue.slice(-1);

            if (lastItem === undefined || !lastItem.compare(newItem)) {
                previosuValue.push(newItem);
            } else {
                lastItem.extendLength(newItem);
            }

            return previosuValue;
        }, []);

        return blocks;
    }

    function getEmployeeNormalBlocks(data: IWeekBlockData, state: Block.IBooleanStateSettings): Interfaces.IAllocationNormalDisplayBlock[] {
        const {
            week,
            periods,
            absences,
            holidays,
            allocations
        } = data;

        const employeeId = data.employee.id;

        const daysInWeek = state.includeWeekends ? 7 : 5;

        if (periods.length === 0) {
            const inactiveWeek = new InactiveBlock(employeeId, week.week, 0);
            inactiveWeek.blockLength = daysInWeek;

            return [inactiveWeek];
        }

        const blocks = Dante.Utils.Range(daysInWeek).reduce((previousValue: Interfaces.IAllocationNormalDisplayBlock[], currentValue: number) => {
            let newItem: Interfaces.IAllocationNormalDisplayBlock = new InactiveBlock(employeeId, week.week, currentValue);
            const [lastItem] = previousValue.slice(-1);
            const isAbsence = !!absences[currentValue];
            const isHoliday = !!holidays[currentValue];

            if (isHoliday) {
                newItem = new HolidayBlock(employeeId, week.week);
            } else if (isAbsence) {
                newItem = new VacationBlock(employeeId, week.week, currentValue);
            } else {
                const includedAllocations = allocations.filter(w => w.overlapingArray[currentValue] && w.block.allocationDayHours[currentValue] > 0);
                const certainty = includedAllocations.reduce((c, a) => c === Screen.AllocationCertainty.Certain ? a.block.certainty : c, Screen.AllocationCertainty.Certain);
                const allocatedHours = includedAllocations.map(w => w.block.allocationDayHours[currentValue]).reduce((a, b) => a + b, 0);
                const contractedHours = periods.filter(w => w.overlapingArray[currentValue]).map(w => w.block.contractedHours[currentValue]).reduce((a, b) => a + b, 0);

                if (contractedHours === 0) {
                    newItem = new InactiveBlock(employeeId, week.week, currentValue);
                } else if (allocatedHours === contractedHours || (!state.showOvertime && allocatedHours > contractedHours)) {
                    newItem = new FullBlock(employeeId, week.week, contractedHours);
                } else if (allocatedHours === 0) {
                    newItem = new OpenBlock(employeeId, week.week, contractedHours, allocatedHours);
                } else if (allocatedHours < contractedHours) {
                    newItem = new PartOpenBlock(employeeId, week.week, contractedHours, allocatedHours);
                } else if (allocatedHours > contractedHours) {
                    newItem = new OverAllocatedBlock(employeeId, week.week, contractedHours, allocatedHours);
                }

                newItem.certainty = certainty;
            }

            if (lastItem === undefined) {
                previousValue.push(newItem);
            } else if (newItem.compare(lastItem)) {
                lastItem.extendLength(newItem);
            } else {
                previousValue.push(newItem);
            }

            return previousValue;
        }, [] as Interfaces.IAllocationNormalDisplayBlock[]);

        return blocks;
    }

    function getEmployeeBlocks(employee: Interfaces.IEmployeeRow, includeWeekends: boolean, statusFrom: Date, statusTo: Date): Interfaces.IAllocationDisplayBlock[] {
        const {
            allocationBlocks: allocations,
            absences,
            id: employeeId } = employee;

        const allocationBlocks: Interfaces.IAllocationDisplayBlock[] = allocations.map(originalBlock => ({
            startPosition: startPosition(statusFrom, originalBlock.startDate, includeWeekends),
            blockLength: blockLength(originalBlock.startDate, originalBlock.endDate, includeWeekends, statusFrom, statusTo),
            employeeId,
            originalBlock,
            startDate: originalBlock.startDate,
            endDate: originalBlock.endDate,
            absenceBlock: null,
        }));

        const absenceAllocations: Interfaces.IAllocationDisplayBlock[] = absences.map(a => {
            const block: Interfaces.IAllocationDisplayBlock = {
                startPosition: startPosition(statusFrom, a.startDate, includeWeekends),
                blockLength: blockLength(a.startDate, a.endDate, includeWeekends, statusFrom, statusTo),
                employeeId,
                originalBlock: null,
                absenceBlock: a,
            };

            return block;
        });

        const allocationGroups = getBlockGroups(allocationBlocks);
        const allocationSize = allocationGroups.length === 0 ? 0 : Math.max(...allocationGroups.map(g => g.blockRowNumber === null ? 0 : g.blockRowNumber as number)) + 1;
        const absenceGroups = getBlockGroups(absenceAllocations)
            .map(g => {
                g.blockRowNumber = (g.blockRowNumber || 0) + allocationSize;

                return g;
            });

        return [...allocationGroups, ...absenceGroups];
    }

    function daysBetween(fromDate: Date, toDate: Date, includeWeekends: boolean): number {
        const oneDay = 24 * 60 * 60 * 1000;
        const fromDateDays = fromDate.getTime();
        const toDateDays = toDate.getTime();

        const diffDays = Math.round((toDateDays - fromDateDays) / (oneDay));

        if (includeWeekends) {
            return diffDays;
        }

        if (diffDays > 0) {
            let weekendDays = 0;
            for (let i = 0; i < diffDays; i++) {
                const currentDate = new Date(fromDate);
                currentDate.setDate(fromDate.getDate() + i);

                const currentDay = currentDate.getDay();
                if ((currentDay === 6) || (currentDay === 0)) {
                    weekendDays++;
                }
            }

            return diffDays - weekendDays;
        }

        return 0;
    }

    function startPosition(periodStartDate: Date, startDate: Date, includeWeekends: boolean): number {

        if (startDate.getTime() < periodStartDate.getTime()) {
            return 0;
        }

        return daysBetween(periodStartDate, startDate, includeWeekends);
    }

    function blockLength(startDate: Date, endDate: Date | null, includeWeekends: boolean, statusFrom: Date, statusTo: Date): number {
        const periodStartDate = statusFrom;
        const periodEndDate = statusTo;
        const beginingOfBlockDate = startDate.getTime() < periodStartDate.getTime() ? periodStartDate : startDate;
        const endOfBlockDate = !!endDate ? endDate.getTime() > periodEndDate.getTime() ? periodEndDate : endDate : periodEndDate;

        return daysBetween(beginingOfBlockDate, endOfBlockDate, includeWeekends) + extendBlockLength(beginingOfBlockDate, endOfBlockDate, includeWeekends);
    }

    function extendBlockLength(fromDate: Date, toDate: Date, includeWeekends: boolean): number {
        if (!includeWeekends) {

            if (toDate.getDay() === 0 || toDate.getDay() === 6) {
                return 0;
            }
        }

        return 1;
    }

    function isRangeOverlaping(fromA: number, toA: number, fromB: number, toB: number): boolean {
        return (toA >= fromB) && (toB > fromA);
    }

    function isSameDay(d1: Date, d2: Date): boolean {
        return d1.getFullYear() === d2.getFullYear() &&
            d1.getMonth() === d2.getMonth() &&
            d1.getDate() === d2.getDate();
    }

    function isOverlaping(fromA: Date, toA: Date | null, fromB: Date, toB: Date | null): boolean {
        return isRangeOverlaping(fromA.getTime(), toA === null ? maxTime : toA.getTime(), fromB.getTime(), toB === null ? maxTime : toB.getTime());
    }

    function enumerateDays(from: Date, to: Date): number[] {
        const millisecondsToDay = (1000 * 60 * 60 * 24);

        const fromTime = from.getTime() / millisecondsToDay;
        const toTime = to.getTime() / millisecondsToDay;

        return Dante.Utils.Range(toTime - fromTime);
    }

    function fillOverlapingArray(from: Date, to: Date, days: Date[]): boolean[] {
        const millisecondsToDay = (1000 * 60 * 60 * 24);

        const fromTime = from.getTime() / millisecondsToDay;
        const toTime = to.getTime() / millisecondsToDay;

        return enumerateDays(from, to).map(d => days.some(day => day.getTime() / millisecondsToDay === fromTime + d));
    }

    function overlapingArray(fromA: Date, toA: Date | null, fromB: Date, toB: Date | null): boolean[] {
        const millisecondsToDay = (1000 * 60 * 60 * 24);

        const fromATime = fromA.getTime() / millisecondsToDay;
        const toATime = (toA === null ? maxTime : toA.getTime()) / millisecondsToDay;
        const fromBTime = fromB.getTime() / millisecondsToDay;
        const toBTime = (toB === null ? maxTime : toB.getTime()) / millisecondsToDay;

        return Dante.Utils.Range(toATime - fromATime).map(d => {
            const current = fromATime + d;

            return current >= fromBTime && current <= toBTime;
        });
    }

    function getBlockGroups(blocks: Interfaces.IAllocationDisplayBlock[]): Interfaces.IAllocationDisplayBlock[] {
        return blocks.map(block => {
            const overlaping = blocks
                .filter(b => isRangeOverlaping(block.startPosition, block.startPosition + block.blockLength, b.startPosition, b.startPosition + b.blockLength))
                .sort((a, b) => a.startPosition - b.startPosition);

            block.blockRowNumber = overlaping.indexOf(block);

            return block;
        });
    }
}
