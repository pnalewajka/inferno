﻿namespace Allocation.Store.Select {
    export interface IState {
        selectedIds: number[];
        orderBy: string;
        orderDirection: boolean;
        customView: { [key: number]: Screen.AllocationViewType | null };
    }

    export interface IAction extends Redux.Action {
        employeeId: number;
    }

    const defaultState: IState = {
        selectedIds: [],
        orderBy: "full-name",
        orderDirection: true,
        customView: {},
    };


    type SelectionActions =
        Actions.StoreAction<Actions.ToggleSelectionAction>
        | Actions.StoreAction<Interfaces.IServerResponse>
        | Actions.StoreAction<Actions.IEmployeeViewToggle>;

    export function store(state: IState = defaultState, action: SelectionActions): IState {
        if (action.type === Store.Actions.AllocationActionType.toggleSelect) {
            return handleToggleAction(state, action.data as Actions.ToggleSelectionAction);
        } else if (action.type === Store.Actions.AllocationActionType.loadData) {
            const { orderBy, orderDirection } = action.data as Interfaces.IServerResponse;

            return Dante.Utils.Assign(state, { orderBy, orderDirection });
        } else if (action.type === Store.Actions.AllocationActionType.toggleEmployeeViewVisibility) {
            return handleToggleEmployeeViewType(state, action.data as Actions.IEmployeeViewToggle);
        } else if (action.type === Store.Actions.AllocationActionType.toggleViewVisibility) {
            return {
                ...state, ...{ customView: {} }
            };
        }

        return state;
    }

    function handleToggleEmployeeViewType(state: IState, data: Actions.IEmployeeViewToggle): IState {
        return {
            ...state,
            ...{
                customView: {
                    ...state.customView,
                    [data.employeeId]: data.viewType
                }
            }
        };
    }

    function handleViewChangeAction(state: IState): IState {
        return state;
    }

    function handleToggleAction(state: IState, data: Actions.ToggleSelectionAction) {
        if (state.selectedIds.indexOf(data.employeeId) === -1) {
            return Dante.Utils.Assign(state, { selectedIds: [...state.selectedIds, data.employeeId] });
        } else {
            return Dante.Utils.Assign(state, { selectedIds: [...state.selectedIds.filter(e => e !== data.employeeId)] });
        }
    }
}