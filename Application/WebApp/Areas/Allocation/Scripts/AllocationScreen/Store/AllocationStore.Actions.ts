﻿namespace Allocation.Store.Actions {
    export enum AllocationActionType {
        loadData,
        toggleSelect,
        toggleBooleanSetting,
        toggleViewVisibility,
        toggleEmployeeViewVisibility,
        editAllocation,
        cancelEditAllocation,
    }

    export interface StoreAction<T> extends Redux.Action {
        data: T;
    }

    export interface IToggleViewAction extends Redux.Action {
        view: Allocation.Screen.AllocationViewType;
    }

    export interface ToggleSelectionAction extends Redux.Action {
        employeeId: number;
    }

    export interface LoadDataAction extends Redux.Action {
        employeeId: number;
    }

    export interface IEmployeeViewToggle extends Redux.Action {
        employeeId: number;
        viewType: Screen.AllocationViewType | null;
    }

    export function toggleSelection(employeeId: number): StoreAction<ToggleSelectionAction> {
        const data = { employeeId } as ToggleSelectionAction;

        return { type: AllocationActionType.toggleSelect, data };
    }

    export function loadData(data: Interfaces.IServerResponse): StoreAction<Interfaces.IServerResponse> {
        return { type: AllocationActionType.loadData, data };
    }

    export function toggleBooleanSetting(property: keyof Block.IBooleanStateSettings): StoreAction<keyof Block.IBooleanStateSettings> {
        return { type: AllocationActionType.toggleBooleanSetting, data: property };
    }

    export function toggleViewVisibility(view: Allocation.Screen.AllocationViewType): StoreAction<IToggleViewAction> {
        return {
            type: AllocationActionType.toggleViewVisibility,
            data: { view } as IToggleViewAction
        };
    }

    export function toggleEmployeeVisibilityType(employeeId: number, viewType: Screen.AllocationViewType | null): StoreAction<IEmployeeViewToggle> {
        return {
            type: AllocationActionType.toggleEmployeeViewVisibility,
            data: {
                employeeId, viewType
            } as IEmployeeViewToggle
        };
    }

    export function editAllocation(data: Interfaces.IAllocationTemporaryBlock): StoreAction<Interfaces.IAllocationTemporaryBlock> {
        return {
            type: AllocationActionType.editAllocation,
            data
        };
    }

    export function cancelEditAllocation(): StoreAction<undefined> {
        return {
            type: AllocationActionType.cancelEditAllocation,
            data: undefined
        };
    }
}
