﻿namespace Allocation.Store {
    export interface IAllocationStoreState {
        selection: Select.IState;
        blocks: Block.IState;
        employee: Employee.IState;
        dateRange: DateRange.IState;
    }

    export const defaultStore = Dante.Utils.Lazy(() => {
        const storeState: Redux.ReducersMapObject = {
            selection: Select.store as Redux.Reducer<Select.IState>,
            blocks: Block.store as Redux.Reducer<Block.IState>,
            employee: Employee.store as Redux.Reducer<Employee.IState>,
            dateRange: DateRange.store as Redux.Reducer<DateRange.IState>,
        };

        return Redux.createStore<IAllocationStoreState>(Redux.combineReducers<IAllocationStoreState>(storeState));
    });
}
