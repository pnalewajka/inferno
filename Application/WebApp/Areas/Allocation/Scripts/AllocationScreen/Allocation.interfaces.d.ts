﻿declare namespace Allocation.Interfaces {
    export interface ITranslations {
        dayDictionary: string[],
        weekFormat: string;
        shortWeekFormat: string;
        showWeekends: string;
        plannedStatus: string;
        certainStatus: string;
        jiraIssueKeyProjectUrlFormat: string;
        jiraKeyProjectKeyUrlFormat: string;
        employeeLabel: string;
        projectNameLabel: string;
        clientNameLabel: string;
        jiraIssueKeyLabel: string;
        jiraKeyLabel: string;
        allocationPercentageLabel: string;
        hoursLabel: string;
        allocationTypeLabel: string;
        deleteButtonLabel: string;
        cloneButtonLabel: string;
        deleteConfirmationTitle: string;
        deleteConfirmationMessage: string;
        pleaseWaitProcessing: string;
        absenceTaskDescription: string;
        employeeLocationViewName: string;
        employeeJobProfileViewName: string;
        employeeJobMatrixViewName: string;
        employeeLineManagerViewName: string;
        employeeOrgUnitViewName: string;
        employeeViewName: string;
        sortByLabel: string;
        none: string;
        advanceEditButton: string;
        simpleEditButton: string;
        employeeAbsenceDescription: string;
        employeeOneDayAbsenceDescription: string;
        viewOptionAllocation: string;
        viewOptionFulfilment: string;
        viewOptionDetailed: string;
        blockNameFormatOpen: string;
        blockNameFormatOpenShort: string;
        blockNameFormatOver: string;
        blockNameFormatOverShort: string;
        blockNameFormatFull: string;
        blockNameFormatInactive: string;
        blockNameFormatInactiveShort: string;
        blockNameFormatHoliday: string;
        blockNameFormatHolidayShort: string;
        blockNameFormatVacation: string;
        blockNameFormatVacationShort: string;
        blockNameFormatPerWeek: string;
        blockNameFormatPerDays: string;
        legendNameFormatFull: string;
        legendNameFormatEmpty: string;
        legendNameFormatOpen: string;
        legendNameFormatPartOpen: string;
        legendNameFormatHoliday: string;
        legendNameFormatVacations: string;
        legendNameFormatOverAllocated: string;
        legendNameFormatPartOverAllocated: string;
        legendNameFormatInactive: string;        
        showPlannedLabel: string;
        showOvertimeLabel: string;
        showVacationsLabel: string;
        showHolidaysLabel: string;
        showProjectColorsLabel: string;
        customizeViewHeader: string;
        additionalInfoHeader: string;
        displayModeLabel: string;
        multipleProjectsFormat: string;
        overtimeFormat: string;
        todayButtonLabel: string;
        fridayButtonLabel: string;
        eomButtonLabel: string;
        addButtonLabel: string;
        extendButtonLabel: string;
        StartDateLabel: string;
        StartDateLabelShort: string;
        EndDateLabelShort: string;
        EndDateLabel: string;
        canEditAllocationWarningMessageFormat: string;
        splitButtonLabel: string;
        saveButtonText: string;
        cancelButtonText: string;
        clearButtonText: string;
    }

    export interface IDateRangeGroup {
        count: number;
        weekendCount: number
    }

    export interface IMonthRange extends IDateRangeGroup {
        date: Date;
        monthDescription: string;
    }

    export interface IWeekRange extends IDateRangeGroup {
        week: number,
        weekStart: Date;
        weekEnd: Date;
    }

    export interface IDateRange {
        months: IMonthRange[];
        weeks: IWeekRange[];
        days: Date[];
    }

    export interface IEmployee {
        id: number;

        fullName: string;
        locationName: string;
        jobMatrixLevels: string[];
        jobProfileNames: string[];
        lineManagerId: number | null;
        lineManagerName: string;
        orgUnitName: string;
        processingStatus: Screen.ProcessingStatus;

    }

    export interface IEmployeePermissions {
        canEditRequest: boolean;
        canViewRequest: boolean;
        canEditEmployee: boolean;
        canViewEmployee: boolean;
        canCloneRequest: boolean;
        canDeleteRequest: boolean;
        canEditProject: boolean;
        canViewProject: boolean;
    }

    export interface IEmployeeRow extends IEmployee {
        allocationBlocks: IAllocationBlock[];
        employmentPeriodBlocks: IEmploymentPeriodBlocks[];
        absences: IAbsenceBlock[];
        holidays: Date[];
    }

    export interface IEmployeeComponentProps {
        employeeId: number;
    }

    export interface IEmployeeRowProps extends IEmployeeComponentProps {
    }

    export interface IEmployeeRowState {
        isSelected: boolean;
        selectedView: Allocation.Screen.AllocationViewType;
        customView: Allocation.Screen.AllocationViewType | null;
        employee: IEmployee;
    }

    export interface IServerResponse {
        permissions: Interfaces.IEmployeePermissions;
        employees: Allocation.Interfaces.IEmployeeRow[];
        dateRange: Allocation.Interfaces.IDateRange;
        translations: ITranslations;
        statusFrom: Date;
        statusTo: Date;
        orderBy: string;
        orderDirection: boolean;
    }

    export interface IGridProps extends IServerResponse {

    }

    export interface IAllocationBaseBlock {
        employeeId: number;
        startPosition: number;
        blockLength: number;

        blockRowNumber?: number;
    }

    export interface IAllocationAbsenceBlock extends IAllocationBaseBlock {
    }

    export interface IAllocationNormalDisplayBlock {
        employeeId: number;
        weekNumber: number;
        blockLength: number;
        allocatedHours: number;
        contractedHours: number;

        certainty: Screen.AllocationCertainty;
        type: Allocation.Screen.StatusBlockEnum;

        compare(block: IAllocationNormalDisplayBlock): boolean;
        extendLength(newBlock: IAllocationNormalDisplayBlock): void;
        readonly displayText: string;
        readonly displayShortText: string;
        readonly showShortText: boolean;
    }

    export interface IAllocationGroupProject {
        requestId: number;
        projectId: number;
        projectName: string;
        projectShortName: string;
        projectAcronym: string;
        projectColor: string;
        projectColorIsDark: boolean;
        certainty: Screen.AllocationCertainty;
        hours: number;
        hoursPerPeriod: number;
    }

    export type DisplayBlockType = Allocation.Screen.StatusBlockEnum.Holiday
        | Allocation.Screen.StatusBlockEnum.Vacations
        | Allocation.Screen.StatusBlockEnum.Inactive
        | Allocation.Screen.StatusBlockEnum.Empty;

    export interface IAllocationDetailsDisplayBlock {
        employeeId: number;
        weekNumber: number;
        blockLength: number;

        allocations: IAllocationGroupProject[];
        contractedHours: number;
        type?: DisplayBlockType;

        compare(block: IAllocationDetailsDisplayBlock): boolean;
        extendLength(newBlock: IAllocationDetailsDisplayBlock): void;
    }
  
    export interface IAllocationOverlayBlock {
        employeeId: number;
        weekNumber: number;
        overlapingArray: boolean[];
        type: Allocation.Store.Block.OverlayType;
    }

    export interface IAllocationDisplayBlock extends IAllocationBaseBlock {
        originalBlock: IAllocationBlock | null;
        absenceBlock: IAbsenceBlock | null;
    }

    export interface IAbsenceBlock {
        startDate: Date;
        endDate: Date;
        requestId: number;
        absenceName: string;
        status: number;
        statusName: string;
        hours: number;
    }

    export interface IAbsencePeriodBlock {
        isAbsence: boolean;
        isPartialAbsence: boolean;
        hours: number;
    }

    export interface IEmploymentPeriodBlocks {
        startDate: Date;
        endDate: Date;
        contractedHours: number[];
    }

    export interface IAllocationBlock {
        requestId: number;
        startDate: Date;
        endDate: Date | null;
        allocationName: string
        allocationPercentage: number;
        certainty: Screen.AllocationCertainty;

        projectId: number;
        projectName: string;
        projectShortName: string;
        projectAcronym: string;
        projectColor: string;
        projectColorIsDark: boolean;
        projectPictureId?: number;
        clientShortName: string;
        jiraIssueKey: string;
        jiraKey: string;
        employeeId: number;
        employeeName: string;
        employeeAcronym: string;
        allocationDayHours: number[];
        processingStatus: Screen.ProcessingStatus;
        contentType: Screen.AllocationRequestContentType;
    }

    export interface IAllocationEditContract {
        startDate: Date;
        endDate: Date | null;
        hours: number[];
        certainty: Screen.AllocationCertainty;
    }

    export interface IAllocationTemporaryBlock extends Partial<IAllocationEditContract> {
        employeeId: number;
        requestId?: number;
    }
}
