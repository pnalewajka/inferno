﻿namespace Allocation.Screen {
    export class BooleanToggle extends Components.SettingBaseToggle<{ selected: (state: Store.IAllocationStoreState) => boolean, onChangeAction: () => Redux.Action, label: string }> {
        protected switchAction(): Redux.Action {
            return this.props.onChangeAction();
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const isViewSelected = this.props.selected(state);

            this.updateState({ checked: isViewSelected });
        }
    }
}
