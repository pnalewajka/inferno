namespace Allocation.Screen {
    export interface IAllocationState {
        employeeIds: number[];
    }
    
    export class EmployeeAllocationList extends Dante.Components.StoreComponent<Store.IAllocationStoreState, {}, IAllocationState> {
        public constructor() {
            super(Store.defaultStore());
            this.state = {
                employeeIds: []
            };
        }

        private mouseEvent = new Atomic.Events.EventDispatcher<number>();

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { employee } = state;
            this.updateState({
                employeeIds: [...employee.list.map(e => e.id)],
            });
        }

        private handleMouseMove = (event: React.MouseEvent<HTMLDivElement>) => {
            const { clientX } = event;

            this.mouseEvent.dispatch(clientX);
        }

        public render(): JSX.Element {
            return (
                <div className="allocation-group-body" onMouseMove={e => this.handleMouseMove(e)}>
                    {this.state.employeeIds.map(e => (
                        <EmployeeRow key={e} employeeId={e} />
                    ))}
                    <EmployeeBlockTracker mouseEvent={this.mouseEvent} scrollEvent={Dante.DetachedScroll.ScrollEvent} />
                </div>
            );
        }
    }
}
