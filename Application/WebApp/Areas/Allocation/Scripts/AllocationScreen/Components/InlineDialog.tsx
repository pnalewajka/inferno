﻿namespace Allocation.Screen {
    interface IInlineDialogState extends IInlineDialogEvent {
    }

    export interface IInlineDialogEvent {
        isOpen: boolean;
        positionX: number;
        positionY: number;
    }

    export class InlineDialog extends Dante.Components.StateComponent<{ eventEmiter: Atomic.Events.EventDispatcher<IInlineDialogEvent>, onClose?: () => void }, IInlineDialogState> {
        public constructor() {
            super({
                isOpen: false,
                positionX: 0,
                positionY: 0
            });
        }

        public componentWillMount(): void {
            this.props.eventEmiter.subscribe(event => {
                this.updateState({ ...event });
            });
        }

        protected propsChanged(): boolean {
            return false;
        }

        public render(): JSX.Element {
            return (
                <div ref={item => {
                    if (item !== null) {
                        $(item).draggable({
                            stop: () => {
                                $(item).css({ width: "auto", height: "auto" });
                            }
                        });
                    }
                }} className={Dante.Utils.ClassNames({ "inline-dialog-open": this.state.isOpen }, "inline-dialog")} style={{ top: this.state.positionY, left: this.state.positionX }
                }>
                    <div className={"inline-dialog-content"}>
                        <div className={"inline-dialog-content-header"}>
                            <i className="fa fa-times-circle-o" onClick={() => {                                
                                this.closeDialog();
                                if (this.props.onClose !== undefined) {
                                    this.props.onClose();
                                }
                            }}></i>
                        </div>
                        <div className={"inline-dialog-content-form"}>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            );
        }

        private closeDialog(): void {
            this.props.eventEmiter.dispatch({
                ...this.state,
                ...{ isOpen: false }
            });
        }
    }
}
