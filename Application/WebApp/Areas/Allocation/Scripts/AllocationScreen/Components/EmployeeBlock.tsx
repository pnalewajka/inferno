﻿namespace Allocation.Screen {
    interface IEmployeeState {
        employee?: Allocation.Interfaces.IEmployee;
        extendedView: boolean;
    }

    export class EmployeeBlock extends Dante.Components.StoreComponent<Store.IAllocationStoreState, Allocation.Interfaces.IEmployeeComponentProps, IEmployeeState> {
        public constructor() {
            super(Store.defaultStore());
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { extendedView } = state.blocks;
            const [employee] = state.employee.list.filter(e => e.id === this.props.employeeId);

            this.updateState({ employee, extendedView });
        }

        private getEmployeeData(): JSX.Element | null {
            const { employee } = this.state;

            if (employee !== undefined) {
                const [jobProfileName] = employee.jobProfileNames;
                const [jobMatrixLevel] = employee.jobMatrixLevels;
                const lineManager = employee.lineManagerName;
                const orgUnit = employee.orgUnitName;
                const location = employee.locationName;

                const elements: Array<JSX.Element | undefined> = [
                    !!jobProfileName ? <span key={"jpn"}>{jobProfileName}</span> : undefined,
                    !!jobMatrixLevel ? <span key={"jml"}>{jobMatrixLevel}</span> : undefined,
                    this.state.extendedView && !!employee.lineManagerId ? <span key={"lm"} onClick={(e) => { e.stopPropagation(); this.openHub(employee.lineManagerId); }} className={Hub.hubClass}>{lineManager}</span> : undefined,
                    this.state.extendedView && !!orgUnit ? <span key={"ou"}>{orgUnit}</span> : undefined,
                    !!location ? <span key={"l"}>{location}</span> : undefined,
                ].reduce((res, el, index) => {
                    if (el === undefined) {
                        return res;
                    }

                    if (res.length === 0) {
                        return [el];
                    }

                    return [...res, <span className="separator" key={`sep_${index}`}>,</span>, el];
                }, [] as JSX.Element[]);

                return (
                    <span>
                        {elements}
                    </span>
                );
            }

            return null;
        }

        private openHub(recordId: number | null): void {
            if (recordId !== null) {
                Hub.open(recordId, "EmployeeValuePickers.Employee");
            }
        }

        public render(): JSX.Element | null {
            if (this.state.employee === undefined) {
                return null;
            }

            return (
                <div className="employee-info-block">
                    {
                        this.state.employee.processingStatus === Screen.ProcessingStatus.Processing
                            ? <i title={Translations.pleaseWaitProcessing} className="fa fa-spinner fa-spin fa-3x fa-fw processing-indicator"></i>
                            : null
                    }
                    <ToggleEmployeeView employeeId={this.state.employee.id} />
                    <span className={Dante.Utils.CombineClassNames(["employee-name", Hub.hubClass])} onClick={(e) => { e.stopPropagation(); this.openHub(this.props.employeeId); }}>{this.state.employee.fullName}</span>
                    <span className="employee-data">{this.getEmployeeData()}</span>
                </div>
            );
        }
    }
}
