﻿namespace Allocation.Screen {
    export enum StatusBlockEnum {
        Full,
        Empty,
        Open,
        PartOpen,
        Holiday,
        Vacations,
        OverAllocated,
        PartOverAllocated,
        Inactive
    }

    export interface IStatusAllocationBlockProps {
        type: StatusBlockEnum;
        text?: string;
        tooltipText?: string;
        blockSize?: number;
        isPlanned?: boolean;
    }

    export function StatusAllocationBlock(props: IStatusAllocationBlockProps): JSX.Element | null {
        const { blockSize } = props;

        if (blockSize === undefined || blockSize < 1) {
            return null;
        }

        return (
            <div
                style={{ width: blockSize * cellWidth }}
                className={Dante.Utils.CombineClassNames(["block-part", `block-part-${StatusBlockEnum[props.type].toLowerCase()}`])}>
                <span>
                    {props.text || ""}
                </span>
                {!!props.isPlanned ? <span title={Translations.plannedStatus} className="marker marker-planned"></span> : null}
            </div>
        );
    }
}
