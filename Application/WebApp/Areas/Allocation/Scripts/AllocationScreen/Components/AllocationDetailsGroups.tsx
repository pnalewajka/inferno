namespace Allocation.Screen {
    interface IAllocationDetailsGroupsState {
        weeks: Store.DateRange.IWeek[];
        blocks: Interfaces.IAllocationDetailsDisplayBlock[];
        includeWeekends: boolean;
    }

    export class AllocationDetailsGroups extends Dante.Components.StoreComponent<Store.IAllocationStoreState, Allocation.Interfaces.IEmployeeRowProps, IAllocationDetailsGroupsState> {
        public constructor() {
            super(Store.defaultStore());
            this.state = { weeks: [], blocks: [], includeWeekends: false };
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const blocks = [...state.blocks.detailsBlocks.filter(b => b.employeeId === this.props.employeeId)];
            const weeks = [...state.dateRange.weeks];

            this.updateState({
                weeks,
                blocks,
                includeWeekends: state.blocks.includeWeekends,
            });
        }

        private blockTranslationMapping: { [key: number]: { shortText: string, longText: string } } = {
            [Allocation.Screen.StatusBlockEnum.Holiday]: { shortText: Translations.blockNameFormatHolidayShort, longText: Translations.blockNameFormatHoliday },
            [Allocation.Screen.StatusBlockEnum.Vacations]: { shortText: Translations.blockNameFormatVacationShort, longText: Translations.blockNameFormatVacation },
            [Allocation.Screen.StatusBlockEnum.Inactive]: { shortText: Translations.blockNameFormatInactiveShort, longText: Translations.blockNameFormatInactive }
        };

        private resolveEmptyBlockText(type: Interfaces.DisplayBlockType, blockLength: number): string {
            const translations = this.blockTranslationMapping[type];

            if (translations !== undefined) {
                return blockLength > 1 ? translations.longText : translations.shortText;
            }

            return "";
        }

        public render(): JSX.Element {
            return (
                <div className="allocation-group-normal-blocks-row">
                    {
                        this.state.weeks.map((w, i) => {
                            const weekBlocks = this.state.blocks.filter(b => b.weekNumber === w.number);

                            return (
                                <div className={Dante.Utils.ClassNames({ "no-weekends": !this.state.includeWeekends }, "week-block")} key={i}>
                                    {
                                        weekBlocks.map((b, block_index) => (
                                            b.type === undefined
                                                ? (
                                                    <ProjectsAllocationBlock
                                                        key={block_index}
                                                        blockHeight={60}
                                                        contractedHours={b.contractedHours}
                                                        blockWidth={b.blockLength}
                                                        allocations={b.allocations}
                                                    />
                                                )
                                                : (
                                                    <div className="block-part block-group" key={block_index}>
                                                        <div className={"block-part-inactive"}>
                                                            <StatusAllocationBlock
                                                                key={block_index}
                                                                type={b.type}
                                                                text={this.resolveEmptyBlockText(b.type, b.blockLength)}
                                                                blockSize={b.blockLength} />
                                                        </div>
                                                    </div>
                                                )
                                        ))
                                    }
                                </div>
                            );
                        })
                    }
                </div>
            );
        }
    }
}
