﻿namespace Allocation.Screen {
    export interface IProjectsAllocationBlockProps {
        blockWidth: number;
        blockHeight: number;
        contractedHours: number;
        allocations: Interfaces.IAllocationGroupProject[];
    }

    function calculateBlockHeight(totalHeight: number, totalHours: number, actualhours: number): number {
        const scale = (actualhours / totalHours);

        return totalHeight * scale;
    }

    function calculateFontSize(size: number): number {
        const fontSize: number = size * 0.9;

        if (fontSize > 10) {
            return 10;
        }

        if (fontSize < 4) {
            return 5;
        }

        return fontSize;
    }

    export class ProjectsAllocationBlock extends Dante.Components.StoreComponent<Store.IAllocationStoreState, IProjectsAllocationBlockProps, { showProjectColor: boolean, splitOvertimes: boolean, sumHours: number, openHours: number }> {
        public constructor() {
            super(Store.defaultStore());
        }

        private multiProjectDialog: Dante.Ui.DialogComponent | null = null;

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const sumHours = this.props.allocations.reduce((a, b) => a + b.hoursPerPeriod, 0);
            const openHours = (this.props.contractedHours * this.props.blockWidth) - sumHours;

            this.setState({
                showProjectColor: state.blocks.showProjectColor,
                splitOvertimes: state.blocks.showOvertime,
                sumHours,
                openHours: openHours > 0 ? openHours : 0
            });
        }

        protected registerTooltip(container: JSX.Element | null): void {
            if (container != null) {
                $(container).tooltip({
                    container: "body",
                    html: true,
                    delay: 400,
                    title: () => {
                        const content = this.renderTooltip();

                        if (content !== null && (this.multiProjectDialog === null || !this.multiProjectDialog.state.isOpen)) {
                            return Dante.Utils.ElementToHtml(content);
                        }

                        return null;
                    }
                });
            }
        }

        protected renderTooltip(): JSX.Element | null {
            if (this.props.allocations.length === 0) {
                return null;
            }

            return (
                <div>
                    {
                        this.props.allocations.map((a, i) => (
                            <div className="tooltip-row" key={i}>
                                <span className="tooltip-row-description elipsis">{a.projectName}</span>
                                <span className="tooltip-row-value">{RenderProjectsCollection.displayHoursPerPeriod(a.hoursPerPeriod, this.props.blockWidth)}</span>
                            </div>
                        ))
                    }
                    {
                        this.state.openHours > 0
                            ? (
                                <div className="tooltip-row tooltip-row-summary">
                                    <span className="tooltip-row-description">&nbsp;</span>
                                    <span className="tooltip-row-value">{Strings.format(Translations.blockNameFormatOpen, this.state.openHours.toFixed(2))}</span>
                                </div>
                            )
                            : null
                    }
                </div>
            );
        }

        private multipleProjectsAllocations(allocations: Interfaces.IAllocationGroupProject[], blockWidth: number, blockHeight: number, contractedHours: number, openHours: number): JSX.Element {
            const dialogRowSize = 20;
            const sumHours = allocations.reduce((sum, a) => sum + a.hours, 0);
            const overtimeHours = sumHours - contractedHours;
            const projectsLabel = Strings.format(Translations.multipleProjectsFormat, allocations.length);

            const displayBlockSize = (sumHours / contractedHours) * blockHeight;
            const openHoursDisplayBlockSize = (openHours / contractedHours) * blockHeight;

            const fillHours = contractedHours - allocations.reduce((a, b) => a + b.hours, 0);

            return (
                <div className="projects-collection">
                    {allocations.some(b => b.certainty === Screen.AllocationCertainty.Planned) ? <span title={Translations.plannedStatus} className="marker marker-planned"></span> : null}
                    <div className="multiple-projects" style={{ height: displayBlockSize > blockHeight ? blockHeight : displayBlockSize }}>
                        <span>{projectsLabel}</span>
                        {overtimeHours > 0 ? <span className="part-overallocated">{Strings.format(Translations.overtimeFormat, overtimeHours.toFixed(2))}</span> : null}
                        <div className="small-project-icons">
                            <i className="fa fa-arrow-circle-o-down" onClick={($event) => {
                                $event.stopPropagation();
                                if (this.multiProjectDialog != null) {
                                    this.multiProjectDialog.show();
                                }
                            }}>

                            </i>
                            <IconsRenderProjectCollection allocations={allocations} contractedHours={contractedHours} />
                        </div>
                    </div>
                    <div className="project-allocation-block project-allocation-block-empty" style={{ height: openHoursDisplayBlockSize, fontSize: openHoursDisplayBlockSize }}></div>
                    {RenderProjectsCollection.renderEmptyBlock(fillHours, blockHeight, contractedHours)}
                    <Dante.Ui.DialogComponent ref={(d) => { this.multiProjectDialog = d; }} title={`${projectsLabel} ${overtimeHours > 0 ? Strings.format(Translations.overtimeFormat, overtimeHours.toFixed(2)) : ""}`}>
                        <div>
                            <RenderProjectsCollection allocations={allocations} contractedHours={contractedHours} />
                        </div>
                    </Dante.Ui.DialogComponent>
                </div>
            );
        }

        public render(): JSX.Element | null {
            const { blockWidth, blockHeight, allocations, contractedHours } = this.props;
            const { showProjectColor } = this.state;

            if (blockWidth < 1) {
                return null;
            }

            const splitResult = allocations.sort((a, b) => a.hours < b.hours ? -1 : 1).reduce((result, allocation, index) => {
                if (!this.state.splitOvertimes) {
                    result.regularProjects.push(allocation);

                    return result;
                }

                const regularHours: number = result.regularProjects.reduce((a, b) => a + b.hours, 0);

                if (regularHours + allocation.hours > this.props.contractedHours) {
                    if (regularHours < this.props.contractedHours) {
                        result.regularProjects.push({ ...allocation, ...{ hours: this.props.contractedHours - regularHours } });
                        result.overtimeProjects.push({ ...allocation, ...{ hours: regularHours + allocation.hours - this.props.contractedHours } });
                    } else {
                        result.overtimeProjects.push(allocation);
                    }
                } else {
                    result.regularProjects.push(allocation);
                }

                result.availableBlockSize -= allocation.hours;

                if (result.availableBlockSize < 0) {
                    result.availableBlockSize = 0;
                }

                return result;
            }, {
                    regularProjects: [] as Interfaces.IAllocationGroupProject[],
                    overtimeProjects: [] as Interfaces.IAllocationGroupProject[],
                    availableBlockSize: this.props.contractedHours,
                });

            return (
                <div ref={this.registerTooltip.bind(this)} className="block-part-container">
                    <div
                        style={{
                            width: blockWidth * cellWidth,
                            minHeight: blockHeight,
                        }}
                        className={Dante.Utils.CombineClassNames(["block-part", "block-group"])}>
                        {
                            splitResult.regularProjects.length > 4
                                ? (
                                    <div className={"block-part-regular"}>
                                        {
                                            this.multipleProjectsAllocations(splitResult.regularProjects, blockWidth, blockHeight, contractedHours, splitResult.availableBlockSize)
                                        }
                                    </div>
                                )
                                : (
                                    <div className={"block-part-regular"}>
                                        <BlockRenderProjectCollection
                                            allocations={splitResult.regularProjects}
                                            contractedHours={8}
                                            blockWidth={blockWidth}
                                            blockHeight={60}
                                            isOvertime={false}
                                            fillHours={splitResult.availableBlockSize} />
                                    </div>
                                )
                        }
                    </div>
                    {
                        splitResult.overtimeProjects.length > 0
                            ? (
                                <div className="block-part">
                                    <div className={"block-part-overtime"}>
                                        <BlockRenderProjectCollection
                                            allocations={splitResult.overtimeProjects}
                                            contractedHours={contractedHours}
                                            blockWidth={blockWidth}
                                            blockHeight={blockHeight}
                                            isOvertime={true} />
                                    </div>
                                </div>
                            )
                            : null
                    }
                </div>
            );
        }
    }
}
