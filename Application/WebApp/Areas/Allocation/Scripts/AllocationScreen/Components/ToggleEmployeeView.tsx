﻿namespace Allocation.Screen {
    export class ToggleEmployeeView extends Dante.Components.StoreComponent<Store.IAllocationStoreState, { employeeId: number }, { type: AllocationViewType | null, globalType: AllocationViewType }> {
        public constructor() {
            super(Store.defaultStore());
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { selectedView } = state.blocks;
            this.updateState({ globalType: selectedView });
        }

        private get typeToSet(): AllocationViewType {
            return !!(this.state.globalType & AllocationViewType.DetailedView) ? AllocationViewType.SummaryView : AllocationViewType.DetailedView;
        }

        private dispachViewChange(tempViewType: AllocationViewType | null): void {
            super.dispatchToStore(Store.Actions.toggleEmployeeVisibilityType(this.props.employeeId, tempViewType));
        }

        private toggleView($event: React.MouseEvent<HTMLElement>): void {
            $event.stopPropagation();

            this.updateState({
                type: this.typeToSet === this.state.type ? null : this.typeToSet
            }, () => {
                this.dispachViewChange(this.state.type);
            });
        }

        public render(): JSX.Element | null {
            if (!!(this.state.globalType & AllocationViewType.DetailedView) && !!(this.state.globalType & AllocationViewType.SummaryView)) {
                return null;
            }

            return (
                <div className={"employee-toggle"}>
                    <i
                        className={"fa fa-sliders"}
                        onClick={this.toggleView.bind(this)}>
                    </i>
                </div>
            );
        }
    }
}
