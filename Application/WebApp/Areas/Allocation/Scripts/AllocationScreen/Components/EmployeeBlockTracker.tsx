﻿namespace Allocation.Screen {
    export interface IEmployeeBlockTrackerProps extends Dante.Ui.IDisableable {
        mouseEvent: Atomic.Events.EventDispatcher<number>;
        scrollEvent: Atomic.Events.EventDispatcher<Dante.DetachedScroll.ScrollPoint>;
    }

    export function normalizeHorizontalPosition(position: number): number {
        const dayCellWidth: number = 30;

        return +(position / dayCellWidth).toFixed(0) * dayCellWidth - 2;
    }

    export class EmployeeBlockTracker extends React.Component<IEmployeeBlockTrackerProps, {}> {
        private indicatorElement: HTMLDivElement | null = null;
        private borderSizeCorrection: number = 2;
        private mousePosition: number = 0;
        private scrollPosition: number = 0;

        public componentWillMount(): void {
            this.props.mouseEvent.subscribe((horizontalPosition) => {
                this.mousePosition = normalizeHorizontalPosition(horizontalPosition);
                this.recalculatePosition();
            });

            this.props.scrollEvent.subscribe(point => {
                this.scrollPosition = normalizeHorizontalPosition(point.horizontal);
                this.recalculatePosition();
            });
        }

        private recalculatePosition(): void {
            if (this.indicatorElement !== null) {
                this.indicatorElement.style.transform = `translateX(${this.borderSizeCorrection + normalizeHorizontalPosition(this.mousePosition + this.scrollPosition)}px)`;
            }
        }

        public render(): JSX.Element | null {
            if (!!this.props.disabled) {
                return null;
            }

            return (
                <div ref={element => this.indicatorElement = element} className="employee-vertical-indicator"></div>
            );
        }
    }
}
