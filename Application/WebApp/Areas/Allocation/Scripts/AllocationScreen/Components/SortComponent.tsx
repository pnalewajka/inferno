namespace Allocation.Screen {
    interface ISortOption {
        label: string;
        sortKey: string;
    }

    export class SortComponent extends Dante.Components.StoreComponent<Store.IAllocationStoreState, {}, { orderBy: string, orderDirection: boolean }> {
        constructor() {
            super(Store.defaultStore());
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { orderBy, orderDirection } = state.selection;
            this.updateState({ orderBy, orderDirection });
        }

        private sortOptions: ISortOption[] = [
            { label: Translations.employeeViewName, sortKey: "full-name" },
            { label: Translations.employeeLocationViewName, sortKey: "location-name" },
            { label: Translations.employeeOrgUnitViewName, sortKey: "org-unit-name" },
            { label: Translations.employeeJobProfileViewName, sortKey: "job-profile-ids" },
            { label: Translations.employeeJobMatrixViewName, sortKey: "job-matrix-ids" },
            { label: Translations.employeeLineManagerViewName, sortKey: "line-manager-id" },
        ];

        private navigateToStageValues(): void {
            const orderValue = `${this.state.orderBy}${this.state.orderDirection ? "" : "-desc"}`;
            window.location.search = UrlHelper.updateUrlParameter(window.location.search, "order", orderValue);
        }

        private switchSortOrder(orderBy: string): void {
            if (this.state.orderBy === orderBy) {
                this.updateState({ orderDirection: !this.state.orderDirection }, () => this.navigateToStageValues());
            } else {
                this.updateState({ orderBy }, () => this.navigateToStageValues());
            }
        }

        public render(): JSX.Element {
            const [selectedOption] = this.sortOptions.filter(s => s.sortKey === this.state.orderBy);

            const selectedOptionLabel = !selectedOption ? Translations.none : selectedOption.label;

            return (
                <div className="sort-component">
                    <span className="sort-component-value" data-toggle="dropdown">
                        <span>{Translations.sortByLabel}: {selectedOptionLabel}</span>
                        <i className={Dante.Utils.ClassNames({ "fa-sort-asc": this.state.orderDirection, "fa-sort-desc": !this.state.orderDirection }, "fa sort-indicator")}></i>
                    </span>
                    <div className="dropdown-menu">
                        {
                            this.sortOptions.map(o => (
                                <li key={o.sortKey} className="dropdown-item" onClick={() => { this.switchSortOrder(o.sortKey); }}>
                                    <span className="sort-component-switch">
                                        {o.label}
                                        {this.state.orderBy === o.sortKey ? <i className="fa fa-check" aria-hidden="true"></i> : null}
                                    </span>
                                </li>
                            ))
                        }
                    </div>
                </div>
            );
        }
    }
}
