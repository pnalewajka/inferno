﻿namespace Allocation.Screen {
    export class ViewToggle extends Components.SettingBaseToggle<{ viewType: AllocationViewType, label: string }> {
        protected switchAction(): Redux.Action {
            return Store.Actions.toggleViewVisibility(this.props.viewType);
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const isViewSelected = !!(state.blocks.selectedView & this.props.viewType);

            this.updateState({ checked: isViewSelected, disabled: state.blocks.selectedView === this.props.viewType });
        }
    }
}
