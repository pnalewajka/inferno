﻿namespace Allocation.Screen.Components {
    interface ISettingsToggleState {
        checked: boolean;
        disabled?: boolean;
    }

    export interface ISettingToggleProps {
        label: string;
    }

    export abstract class SettingBaseToggle<P extends ISettingToggleProps = ISettingToggleProps, S extends ISettingsToggleState = ISettingsToggleState> extends Dante.Components.StoreComponent<Store.IAllocationStoreState, P, S> {
        constructor() {
            super(Store.defaultStore());
        }

        protected abstract switchAction(): Redux.Action;

        public render(): JSX.Element {
            return (
                <div className="toggle">
                    <span>{this.props.label}</span>
                    <Atomic.Ui.ToggleButton
                        disabled={this.state.disabled}
                        onChanged={(v) => {
                            this.dispatchToStore(this.switchAction());
                            allocationStateChange.dispatch({});
                        }} checked={this.state.checked} />
                </div>
            );
        }
    }
}
