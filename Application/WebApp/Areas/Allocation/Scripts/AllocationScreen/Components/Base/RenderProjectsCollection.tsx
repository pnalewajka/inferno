﻿namespace Allocation.Screen {
    const whiteColor: string = "#fff";
    const darkColor: string = "#333";

    export interface IRenderProjectsCollectionProps {
        allocations: Interfaces.IAllocationGroupProject[];
        blockHeight?: number;
        contractedHours: number;
        fillHours?: number;
        forceStyle?: React.CSSProperties;
        blockWidth?: number;
    }

    export interface IBlockRenderProjectsCollectionProps extends IRenderProjectsCollectionProps {
        isOvertime: boolean;
    }

    export interface IRenderProjectsCollectionState {
        showProjectColor: boolean;
    }

    export abstract class RenderProjectsCollection<P extends IRenderProjectsCollectionProps = IRenderProjectsCollectionProps>
        extends Dante.Components.StoreComponent<Store.IAllocationStoreState, P, IRenderProjectsCollectionState> {
        public constructor() {
            super(Store.defaultStore());
        }

        public render(): JSX.Element {
            const emptyBlockSize = this.props.fillHours === undefined ? 0 : this.calculateBlockHeight(+this.props.fillHours);

            return (
                <div className={Dante.Utils.CombineClassNames(["projects-collection", this.containerClassName()])
                }>
                    {
                        this.props.allocations
                            .sort((a, b) => a.certainty === Screen.AllocationCertainty.Certain ? 0 : 1)
                            .map((b, i) => {
                                const size = this.calculateBlockHeight(b.hours);
                                const style: React.CSSProperties = {
                                    ...{
                                        height: size,
                                        maxHeight: size,
                                        minHeight: size,
                                        lineHeight: `${size}px`,
                                        fontSize: this.calculateFontSize(size)
                                    },
                                    ...(
                                        this.showProjectColors()
                                            ? {
                                                color: b.projectColorIsDark && this.state.showProjectColor ? whiteColor : darkColor,
                                                backgroundColor: this.state.showProjectColor ? b.projectColor : undefined,
                                            }
                                            : {}
                                    )
                                };

                                return (
                                    <div
                                        key={i}
                                        style={style}
                                        onClick={(e: React.MouseEvent<HTMLElement>) => {
                                            requestDetailsEvent.dispatch({ requestId: b.requestId, isOpen: true, positionX: e.clientX - 200, positionY: e.clientY + 30 });
                                        }}
                                        className={Dante.Utils.ClassNames({
                                            "project-allocation-block-planned": b.certainty === Screen.AllocationCertainty.Planned && this.showProjectColors(),
                                            "project-allocation-block-certain": b.certainty === Screen.AllocationCertainty.Certain && this.showProjectColors(),
                                        }, "project-allocation-block")}
                                    >
                                        {
                                            this.renderProjectData(b)
                                        }
                                    </div>
                                );
                            })
                    }
                    {
                        this.props.fillHours > 0
                            ? <div
                                style={{
                                    height: emptyBlockSize,
                                    maxHeight: emptyBlockSize,
                                    minHeight: emptyBlockSize,
                                }}
                                className="project-allocation-block project-allocation-block-empty">
                                &nbsp;
                            </div>
                            : null
                    }
                </div>
            );
        }

        public static calculateBlockHeight(actualhours: number, blockHeight: number | undefined, contractedHours: number): number | undefined {
            if (blockHeight === undefined) {
                return undefined;
            }

            const scale = (actualhours / contractedHours);

            return blockHeight * scale;
        }

        public static renderEmptyBlock(fillHours: number, blockHeight: number, contractedHours: number): JSX.Element | null {
            if (fillHours <= 0) {
                return null;
            }

            const emptyBlockSize = fillHours === undefined ? 0 : this.calculateBlockHeight(+fillHours, blockHeight, contractedHours);

            return (
                <div
                    title={Strings.format(Translations.blockNameFormatOpen, RenderProjectsCollection.displayHours(fillHours))}
                    style={{
                        height: emptyBlockSize,
                        maxHeight: emptyBlockSize,
                        minHeight: emptyBlockSize,
                    }}
                    className="project-allocation-block project-allocation-block-empty">
                    &nbsp;
                </div>
            );
        }

        public static displayHours(value?: number): string {
            if (value === undefined) {
                return "";
            }

            return value.toFixed(2);
        }

        public static displayHoursPerPeriod(value: number, periodLength: number = 1): string {
            const formattedValue = RenderProjectsCollection.displayHours(value);

            return periodLength === 5 || periodLength === 7
                ? Strings.format(Translations.blockNameFormatPerWeek, formattedValue)
                : Strings.format(Translations.blockNameFormatPerDays, formattedValue, periodLength);
        }

        protected showProjectColors(): boolean {
            return true;
        }

        protected containerClassName(): string {
            return "";
        }

        protected propsChanged(nextProps: IRenderProjectsCollectionProps): boolean {
            return this.props.allocations.length !== nextProps.allocations.length;
        }

        protected stateChanged(nextState: IRenderProjectsCollectionState): boolean {
            return this.state.showProjectColor !== nextState.showProjectColor;
        }

        protected renderProjectData(block: Interfaces.IAllocationGroupProject): JSX.Element | undefined {
            return (
                <span>
                    {block.certainty === Screen.AllocationCertainty.Planned && this.state.showProjectColor ? <span title={Translations.plannedStatus} className="marker marker-planned"></span> : null}
                    <span className="project-block project-block-code">
                        {block.projectAcronym}
                    </span>
                    <span className="project-block project-block-name">
                        {block.projectShortName}
                    </span>
                    <span className="project-block project-block-hours">
                        {`(${RenderProjectsCollection.displayHours(block.hours)}h)`}
                    </span>
                </span>
            );
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { blocks: { showProjectColor } } = state;

            this.updateState({ showProjectColor });
        }

        protected calculateBlockHeight(actualhours: number): number | undefined {
            const { blockHeight, contractedHours } = this.props as IRenderProjectsCollectionProps;

            return RenderProjectsCollection.calculateBlockHeight(actualhours, blockHeight, contractedHours);
        }

        protected calculateFontSize(size?: number): number | undefined {
            if (size === undefined) {
                return undefined;
            }

            const fontSize: number = size * 0.9;

            if (fontSize > 10) {
                return 10;
            }

            if (fontSize < 4) {
                return 5;
            }

            return fontSize;
        }
    }

    export class IconsRenderProjectCollection extends RenderProjectsCollection {
        public containerClassName(): string {
            const baseClass = super.containerClassName();

            return `projects-collection-icons`;
        }

        protected renderProjectData(block: Interfaces.IAllocationGroupProject): JSX.Element | undefined {
            return undefined;
        }
    }

    export class BlockRenderProjectCollection extends RenderProjectsCollection<IBlockRenderProjectsCollectionProps> {
        public containerClassName(): string {
            const baseClass = super.containerClassName();
            if (this.props.isOvertime) {
                return `projects-collection-overtime`;
            }

            return baseClass;
        }

        protected showProjectColors(): boolean {
            return !this.props.isOvertime;
        }

        protected renderProjectData(block: Interfaces.IAllocationGroupProject): JSX.Element | undefined {
            return (
                <span>
                    {block.certainty === Screen.AllocationCertainty.Planned && this.state.showProjectColor ? <span title={Translations.plannedStatus} className="marker marker-planned"></span> : null}
                    <span className="project-block project-block-code">
                        {block.projectAcronym}
                    </span>
                </span>
            );
        }
    }
}
