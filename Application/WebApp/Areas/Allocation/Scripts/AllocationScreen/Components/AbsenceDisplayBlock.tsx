namespace Allocation.Screen {
    export const AbsenceDisplayBlock: React.StatelessComponent<Interfaces.IAbsenceBlock> = (block: Interfaces.IAbsenceBlock): JSX.Element => {
        const fromDate = block.startDate.toLocaleDateString();
        const toDate = block.endDate.toLocaleDateString();

        const absenceDescription =
            fromDate === toDate
                ? Strings.format(Translations.employeeOneDayAbsenceDescription, block.absenceName, fromDate, block.statusName)
                : Strings.format(Translations.employeeAbsenceDescription, block.absenceName, fromDate, toDate, block.statusName);

        return (
            <span className="block-name" title={absenceDescription} >
                <i className="fa fa-suitcase"></i>
            </span >
        );
    };
}
