﻿namespace Allocation.Screen {
    export interface IWeekBlockProps {

    }

    interface IWeekBlockState {
        showWeekends: boolean;
    }

    export class WeekBlock extends Dante.Components.StoreComponent<Store.IAllocationStoreState, IWeekBlockProps, IWeekBlockState> {
        protected updateFromStore(state: Store.IAllocationStoreState): void {
            this.updateState({ showWeekends: state.blocks.includeWeekends });
        }
    }
}
