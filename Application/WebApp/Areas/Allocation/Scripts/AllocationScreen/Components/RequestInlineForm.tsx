﻿namespace Allocation.Screen {
    export interface IRequestDialogEvent extends IInlineDialogEvent {
        requestId?: number;
    }

    export const requestDetailsEvent = new Atomic.Events.EventDispatcher<IRequestDialogEvent>();

    interface IRequestData {
        requestId: number;
        requestName: string;
    }

    interface IRequestFormData extends Interfaces.IAllocationEditContract, IRequestData {
        employeeId: number;
        employeeName: string;
    }

    interface IRequestInlineFormState {
        isOpen: boolean;
        availableRequests?: IRequestData[];
        request?: IRequestFormData;
        originalRequest?: IRequestFormData;
        event?: IRequestDialogEvent;
    }

    export class RequestInlineForm extends Dante.Components.StateComponent<{ eventEmiter: Atomic.Events.EventDispatcher<IRequestDialogEvent> }, IRequestInlineFormState> {
        public constructor() {
            super({
                isOpen: false,
            });
        }

        public componentWillMount(): void {
            this.props.eventEmiter.subscribe(event => {
                if (event.requestId === undefined) {
                    return;
                }

                const employees = Store.defaultStore().getState().blocks.employees;
                const [employee] = employees.filter(e => e.allocationBlocks.some(b => b.requestId === event.requestId));
                const [block] = employee.allocationBlocks.filter(b => b.requestId === event.requestId);
                const request: IRequestFormData = {
                    requestId: event.requestId || 0,
                    employeeId: employee.id,
                    employeeName: employee.fullName,
                    startDate: block.startDate,
                    endDate: block.endDate,
                    hours: block.allocationDayHours,
                    requestName: block.projectShortName,
                    certainty: block.certainty,
                };

                const availableRequests: IRequestData[] = employee.allocationBlocks.map(b => ({ requestId: b.requestId, requestName: b.projectShortName }));

                this.updateState({
                    isOpen: event.isOpen && event.requestId !== undefined,
                    event,
                    availableRequests,
                    request: { ...request },
                    originalRequest: { ...request }
                });
            });
        }

        private get hasRequestChanged(): boolean {
            return JSON.stringify(this.state.request) !== JSON.stringify(this.state.originalRequest);
        }

        private updateRequest(partialRequest: Partial<IRequestFormData>, storeUpdate?: (request: IRequestFormData) => void): void {
            if (this.state.request === undefined) {
                return;
            }

            this.updateState({ request: { ...this.state.request, ...partialRequest } }, () => {
                if (this.state.request !== undefined) {
                    if (storeUpdate === undefined) {
                        Store.defaultStore().dispatch(Store.Actions.editAllocation({
                            ...(this.state.request as Interfaces.IAllocationEditContract),
                            ...{ employeeId: this.state.request.employeeId }
                        }));
                    } else {
                        storeUpdate(this.state.request);
                    }
                }
            });
        }

        private updateRequestEndDate(endDate: Date | null): void {
            this.updateRequest({ endDate });
        }

        private updateRequestStartDate(startDate: Date): void {
            this.updateRequest({ startDate });
        }

        private updateRequestHours(value: number, index: number): void {
            if (this.state.request === undefined) {
                return;
            }

            this.updateRequest({
                hours: [...this.state.request.hours.map((h, i) => i === index ? value : h)]
            });
        }

        private get canEditStartDate(): boolean {
            if (this.state.request === undefined) {
                return false;
            }

            return Dante.Utils.daysBetween(new Date(), this.state.request.startDate) < 30;
        }

        private get canEditEndDate(): boolean {
            if (this.state.request === undefined) {
                return false;
            }

            if (this.state.request.endDate === null) {
                return true;
            }

            return Dante.Utils.daysBetween(new Date(), this.state.request.endDate) < 30;
        }

        private get canEditHours(): boolean {
            return this.canEditStartDate;
        }

        private saveChange(): void {
            if (this.state.request === undefined) {
                return;
            }

            this.props.eventEmiter.dispatch({
                isOpen: false,
                positionX: 0,
                positionY: 0
            });

            $.post(`/Allocation/EmployeeAllocation/EditRequest/${this.state.request.requestId}`, {
                StartDate: this.state.request.startDate.toISOString(),
                EndDate: this.state.request.endDate === null ? null : this.state.request.endDate.toISOString(),
                Hours: this.state.request.hours,
                Certainty: this.state.request.certainty,
            });
        }

        private cancelChange(): void {
            if (this.state.originalRequest === undefined) {
                return;
            }

            this.updateRequest({ ...this.state.originalRequest }, (r) => {
                Store.defaultStore().dispatch(Store.Actions.cancelEditAllocation());
            });
        }

        public render(): JSX.Element | null {
            const changeOptions: Dante.Ui.IOptionPickerOption[] =
                this.state.availableRequests === undefined
                    ? []
                    : this.state.availableRequests.map(r => ({
                        display: r.requestName,
                        onClick: () => {
                            if (this.state.event === undefined || !this.state.event.isOpen) {
                                return;
                            }

                            this.props.eventEmiter.dispatch({
                                ...this.state.event,
                                ... { requestId: r.requestId }
                            });
                        },
                    }))
                ;

            if (this.state.isOpen && this.state.request !== undefined && this.state.availableRequests !== undefined && this.state.availableRequests.length > 0) {
                return (
                    <div className="request-inline-form">
                        <div className="request-inline-form-row request-inline-form-row-employee" title={this.state.request.employeeName}>
                            <span>{this.state.request.employeeName}</span>
                        </div>
                        <div className="request-inline-form-row request-inline-form-row-project" title={this.state.request.requestName}>
                            <span>{this.state.request.requestName}</span>
                            {
                                this.state.availableRequests.length > 1
                                    ? <Dante.Ui.OptionPicker options={changeOptions} text={""} disabled={this.hasRequestChanged} />
                                    : null
                            }
                        </div>
                        <div className="request-inline-form-row">
                            <label>{Translations.StartDateLabelShort}</label>
                            <div className="input-group">
                                <Dante.Ui.DatePicker
                                    className="form-control"
                                    value={this.state.request.startDate}
                                    disabled={!this.canEditStartDate}
                                    autofocus={false}
                                    onChange={(d) => { if (!!d) { this.updateRequestStartDate(d); } }} />
                                <div className="input-group-addon">
                                    <i className="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div className="request-inline-form-row">
                            <label>{Translations.EndDateLabelShort}</label>
                            <div className="input-group">
                                <Dante.Ui.DatePicker
                                    className="form-control"
                                    value={this.state.request.endDate}
                                    disabled={!this.canEditEndDate}
                                    autofocus={false}
                                    onChange={(d) => {
                                        this.updateRequestEndDate(d);
                                    }} />
                                <div className="input-group-addon">
                                    <i className="fa fa-calendar"></i>
                                </div>
                            </div>
                            <button disabled={!this.canEditEndDate} onClick={() => { this.updateRequestEndDate(new Date()); }} className="btn btn-link date-action">{Translations.todayButtonLabel}</button>
                            <button disabled={!this.canEditEndDate} onClick={() => { this.updateRequestEndDate(Dante.Utils.getEndOfWeek()); }} className="btn btn-link date-action">{Translations.fridayButtonLabel}</button>
                            <button disabled={!this.canEditEndDate} onClick={() => { this.updateRequestEndDate(Dante.Utils.getEndOfMonth()); }} className="btn btn-link date-action">{Translations.eomButtonLabel}</button>
                            {
                                this.state.request.endDate !== null
                                    ? <button disabled={!this.canEditEndDate} onClick={() => { this.updateRequestEndDate(null); }} className="btn btn-link date-action">{Translations.clearButtonText}</button>
                                    : null
                            }
                        </div>
                        <div className="request-inline-form-row">
                            <label>{Translations.hoursLabel}</label>
                            <table>
                                <tbody>
                                    <tr>
                                        {
                                            this.state.request.hours.map((h, i) => <th key={i}><label>{Translations.dayDictionary[(i + 1) % 7]}</label></th>)
                                        }
                                    </tr>
                                    <tr>
                                        {
                                            this.state.request.hours.map((h, i) => (
                                                <td key={i}>
                                                    <HoursInput
                                                        disabled={!this.canEditHours}
                                                        type="text"
                                                        value={h}
                                                        className="form-control"
                                                        onValueChanged={numberValue => {
                                                            this.updateRequestHours(numberValue, i);
                                                        }} />
                                                </td>
                                            ))
                                        }
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="request-inline-form-row request-inline-form-row-toggle">
                            <label>{Translations.certainStatus}</label>
                            <div className="input-group">
                                <Atomic.Ui.ToggleButton
                                    disabled={!this.canEditHours}
                                    onChanged={() => {
                                        if (this.state.request !== undefined) {
                                            this.updateRequest({
                                                certainty: this.state.request.certainty === Screen.AllocationCertainty.Certain ? Screen.AllocationCertainty.Planned : Screen.AllocationCertainty.Certain
                                            });
                                        }
                                    }}
                                    checked={this.state.request.certainty === Screen.AllocationCertainty.Certain} />
                            </div>
                        </div>
                        {
                            !this.canEditStartDate
                                ? (
                                    <div className="request-inline-form-row">
                                        <div className="alert alert-danger">{Strings.format(Translations.canEditAllocationWarningMessageFormat, 30)}</div>
                                    </div>
                                )
                                : null
                        }
                        <div className="request-inline-form-row request-inline-form-row-actions">
                            {
                                this.hasRequestChanged
                                    ? <div className="btn-group btn-group-sm pull-right">
                                        <button className="btn btn-default" onClick={() => this.saveChange()}>{Translations.saveButtonText}</button>
                                        <button className="btn btn-default" onClick={() => this.cancelChange()}>{Translations.cancelButtonText}</button>
                                    </div>
                                    : <div className="btn-group btn-group-sm pull-right">
                                        <button disabled={true} className="btn btn-default">{Translations.addButtonLabel}</button>
                                        <button disabled={true} className="btn btn-default">{Translations.splitButtonLabel}</button>
                                        <button disabled={true} className="btn btn-default">{Translations.cloneButtonLabel}</button>
                                        <button disabled={true} className="btn btn-default">{Translations.extendButtonLabel}</button>
                                        <button disabled={true} className="btn btn-default">{Translations.deleteButtonLabel}</button>
                                    </div>
                            }
                        </div>
                    </div>
                );
            }

            return null;
        }
    }
}
