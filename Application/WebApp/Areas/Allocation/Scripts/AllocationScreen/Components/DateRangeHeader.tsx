namespace Allocation.Screen {
    interface IDateRangeHeaderState {
        includeWeekends: boolean;
        weeks: Store.DateRange.IWeek[];
        months: Store.DateRange.IMonth[];
        days: Date[];
    }

    export class DateRangeHeader extends Dante.Components.StoreComponent<Store.IAllocationStoreState, {}, IDateRangeHeaderState> {
        constructor() {
            super(Store.defaultStore());
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            this.updateState({
                includeWeekends: state.blocks.includeWeekends,
                weeks: state.dateRange.weeks,
                months: state.dateRange.months,
                days: state.dateRange.days,
            });
        }

        private daysInGroup(group: Interfaces.IDateRangeGroup): number {
            const groupSize = this.state.includeWeekends ? group.count : group.count - group.weekendCount;

            return groupSize;
        }

        private dateRangeGroupStyle(groupSize: number): React.CSSProperties {
            return { width: groupSize * cellWidth, minWidth: groupSize * cellWidth, maxWidth: groupSize * cellWidth };
        }

        public render(): JSX.Element {
            const monthFullDescriptionLevel = 4;
            const weekFullDescriptionLevel = 2;

            return (
                <div className="allocation-group-header">
                    <div className="allocation-group-header-months">
                        <div className="dailies">
                            <div className="column-header">&nbsp;</div>
                            {
                                this.state.months
                                    .map((m, i) => {
                                        return (
                                            <div key={i} className="month" style={this.dateRangeGroupStyle(this.state.includeWeekends ? m.displayedDays : m.displayedDaysWithoutWeekend)}>
                                                {m.description}
                                            </div>
                                        );
                                    })
                            }
                        </div>
                    </div>
                    <div className="allocation-group-header-weeks">
                        <div className="dailies">
                            <div className="column-header">&nbsp;</div>
                            {
                                this.state.weeks
                                    .map((w, i) => {
                                        return (
                                            <div key={i} className={Dante.Utils.ClassNames({ "no-weekends": !this.state.includeWeekends }, "week")}>
                                                {Strings.format(w.displayedDays > weekFullDescriptionLevel ? Translations.weekFormat : Translations.shortWeekFormat, w.number)}
                                            </div>
                                        );
                                    })
                            }
                        </div>
                    </div>
                    <div className="allocation-group-header-days">
                        <div className="dailies">
                            <div className="column-header">
                                <SortComponent />
                            </div>
                            {
                                this.state.days
                                    .filter(d => {
                                        if (this.state.includeWeekends) {
                                            return true;
                                        }

                                        const day = d.getDay();

                                        return (day !== 6) && (day !== 0);
                                    })
                                    .map((d, i) => {
                                        const day = d.getDay();
                                        const isWeekend = (day === 6) || (day === 0);

                                        return (
                                            <div key={i} className={Dante.Utils.ClassNames({ weekend: isWeekend }, "day")}>
                                                <span className="daynumber">{d.getDate()}</span>
                                                <span className="dayname">{Translations.dayDictionary[day]}</span>
                                            </div>
                                        );
                                    })
                            }
                        </div>
                    </div>
                </div>
            );
        }
    }
}
