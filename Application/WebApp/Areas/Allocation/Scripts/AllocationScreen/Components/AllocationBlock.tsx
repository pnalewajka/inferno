namespace Allocation.Screen {
    export class AllocationBlock extends React.Component<Allocation.Interfaces.IAllocationDisplayBlock> {
        private detailsBox?: AllocationDetailsTooltip;

        private blockClick(event: React.MouseEvent<HTMLDivElement>): void {
            if (this.detailsBox !== undefined) {
                this.detailsBox.show(event.currentTarget);
            }
        }

        public render(): JSX.Element | null {
            const { startPosition, blockLength, originalBlock, absenceBlock, blockRowNumber } = this.props;
            const blockStyles: React.CSSProperties = {
                left: startPosition * cellWidth,
                width: blockLength * cellWidth,
                top: (blockRowNumber || 0) * rowHeight
            };

            if (originalBlock !== null) {

                const classNames = Dante.Utils.ClassNames({ certain: originalBlock.certainty === 1 }, "block");

                const percentage = Dante.Utils.DisplayNumber(originalBlock.allocationPercentage);

                return (
                    <div
                        onClick={this.blockClick.bind(this)}
                        className={classNames}
                        style={blockStyles}
                    >
                        <span className="block-name">{originalBlock.allocationName}</span>
                        <AllocationDetailsTooltip
                            block={this.props.originalBlock as Allocation.Interfaces.IAllocationBlock}
                            ref={(tt: AllocationDetailsTooltip | null) => { if (tt !== null) { this.detailsBox = tt; } }}
                        />
                    </div>
                );
            }

            if (absenceBlock !== null) {
                return (
                    <div style={blockStyles} className="block absence">
                        <AbsenceDisplayBlock {...absenceBlock} />
                    </div>
                );
            }

            return null;
        }
    }
}
