﻿namespace Allocation.Screen {
    export class EmployeeRow extends Dante.Components.StoreComponent<Store.IAllocationStoreState, Allocation.Interfaces.IEmployeeRowProps, Allocation.Interfaces.IEmployeeRowState> {
        constructor() {
            super(Store.defaultStore());
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { selectedIds, customView } = state.selection;
            const { list } = state.employee;
            const { selectedView } = state.blocks;
            const [employee] = list.filter(e => e.id === this.props.employeeId);

            this.updateState({
                isSelected: selectedIds.some(i => i === this.props.employeeId),
                employee,
                selectedView,
                customView: customView[this.props.employeeId] || null
            });
        }

        private adjustColumn(column: HTMLDivElement | null): void {
            if (column == null) {
                return;
            }

            const { clientWidth } = column;
            if (clientWidth > maxColumnWidth || !maxColumnWidth) {
                maxColumnWidth = clientWidth;
            }

            debounceWidth();
        }

        private shouldRenderView(type: Allocation.Screen.AllocationViewType): boolean {
            if (!!this.state.customView) {
                return !!(this.state.customView & type);
            }

            return !!(this.state.selectedView & type);
        }

        public render(): JSX.Element {
            return (
                <div className="allocation-group">
                    <div className="allocation-group-row" onClick={() => { this.dispatchToStore(Store.Actions.toggleSelection(this.props.employeeId)); }}>
                        <div className={Dante.Utils.ClassNames({ selected: this.state.isSelected }, "employee-info pointer-all")} ref={(item) => this.adjustColumn(item)}>
                            <EmployeeBlock employeeId={this.props.employeeId} />
                        </div>
                        <div className="allocation-group-container">
                            {this.shouldRenderView(Allocation.Screen.AllocationViewType.AllocationView) ? <AllocationGroups employeeId={this.props.employeeId} /> : null}
                            {this.shouldRenderView(Allocation.Screen.AllocationViewType.SummaryView) ? <AllocationNormalGroups employeeId={this.props.employeeId} /> : null}
                            {this.shouldRenderView(Allocation.Screen.AllocationViewType.DetailedView) ? <AllocationDetailsGroups employeeId={this.props.employeeId} /> : null}
                            <OverlayGroups employeeId={this.props.employeeId} />
                        </div>
                    </div>
                </div>
            );
        }
    }
}
