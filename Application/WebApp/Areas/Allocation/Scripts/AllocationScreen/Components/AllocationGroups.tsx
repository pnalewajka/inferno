namespace Allocation.Screen {
    export class AllocationGroups extends Dante.Components.StoreComponent<Store.IAllocationStoreState, Allocation.Interfaces.IEmployeeRowProps, {
        displayBlocks: Interfaces.IAllocationDisplayBlock[],
        blockSize: number,
    }> {
        public constructor() {
            super(Store.defaultStore());
            this.state = { displayBlocks: [], blockSize: 1 };
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { displayBlocks } = state.blocks;
            const employeeDisplayBlocks = displayBlocks.filter(b => b.employeeId === this.props.employeeId);
            const selectedView = state.blocks.selectedView;

            const blockSize = Math.max(...employeeDisplayBlocks.map(d => d.blockRowNumber || 0)) + 1;

            this.updateState({ displayBlocks: employeeDisplayBlocks, blockSize });
        }

        public render(): JSX.Element | null {
            const groupSize = this.state.displayBlocks.length > 0 ? Math.max(...this.state.displayBlocks.map(b => b.blockRowNumber || 1)) + 1 : 1;

            return (
                <div className="allocation-group-blocks-row" style={{ height: groupSize * rowHeight }}>
                    {
                        this.state.displayBlocks.map((block, groupIndex) => (
                            <AllocationBlock key={`${groupIndex}`} {...block} />
                        ))
                    }
                </div>
            );
        }
    }
}
