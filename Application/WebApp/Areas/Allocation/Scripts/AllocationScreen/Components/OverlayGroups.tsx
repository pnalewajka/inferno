﻿namespace Allocation.Screen {
    interface IOverlayGroupsState {
        weeks: Store.DateRange.IWeek[];
        overlays: Interfaces.IAllocationOverlayBlock[];
        includeWeekends: boolean;
    }

    export class OverlayGroups extends Dante.Components.StoreComponent<Store.IAllocationStoreState, Allocation.Interfaces.IEmployeeRowProps, IOverlayGroupsState> {
        public constructor() {
            super(Store.defaultStore());
            this.state = { weeks: [], overlays: [], includeWeekends: true };
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const weeks = [...state.dateRange.weeks];
            const overlays = [...state.blocks.overlayBlocks.filter(b => b.employeeId === this.props.employeeId)];
            this.updateState({
                weeks,
                overlays,
                includeWeekends: state.blocks.includeWeekends,
            });
        }

        private overlayTitle(weekBlocks: Interfaces.IAllocationOverlayBlock): string {
            return weekBlocks.type === Store.Block.OverlayType.planned
                ? Translations.plannedStatus
                : Translations.blockNameFormatVacation;
        }

        public render(): JSX.Element | null {
            return (
                <div className="allocation-group-normal-blocks-row">
                    {
                        this.state.weeks.map((w, i) => {
                            const [weekBlocks] = this.state.overlays.filter(b => b.weekNumber === w.number);

                            return (
                                <div className={Dante.Utils.ClassNames({ "no-weekends": !this.state.includeWeekends }, "week-block")} key={w.number}>
                                    {
                                        weekBlocks === undefined
                                            ? null
                                            : weekBlocks.overlapingArray.map((b, ix) => (
                                                <div
                                                    title={this.overlayTitle(weekBlocks)}
                                                    className={Dante.Utils.ClassNames({
                                                        "overlay-block-planned": weekBlocks.type === Store.Block.OverlayType.planned && b,
                                                        "overlay-block-absence": weekBlocks.type === Store.Block.OverlayType.partAbsence && b,
                                                    }, "overlay-block")}
                                                    key={ix}>
                                                </div>
                                                ))
                                    }
                                </div>
                            );
                        })
                    }
                </div>
            );
        }
    }
}
