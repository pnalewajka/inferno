namespace Allocation.Screen {
    interface IAllocationNormalGroupsState {
        weeks: Store.DateRange.IWeek[];
        blocks: Interfaces.IAllocationNormalDisplayBlock[];
        includeWeekends: boolean;
    }

    export class AllocationNormalGroups extends Dante.Components.StoreComponent<Store.IAllocationStoreState, Allocation.Interfaces.IEmployeeRowProps, IAllocationNormalGroupsState> {
        public constructor() {
            super(Store.defaultStore());
            this.state = { weeks: [], blocks: [], includeWeekends: false };
        }

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const blocks = [...state.blocks.normalBlocks.filter(b => b.employeeId === this.props.employeeId)];
            const weeks = [...state.dateRange.weeks];

            this.updateState({ weeks, blocks, includeWeekends: state.blocks.includeWeekends });
        }

        public render(): JSX.Element {
            return (
                <div className="allocation-group-normal-blocks-row">
                    {
                        this.state.weeks.map((w, i) => {
                            const weekBlocks = this.state.blocks.filter(b => b.weekNumber === w.number);

                            return (
                                <div className={Dante.Utils.ClassNames({ "no-weekends": !this.state.includeWeekends }, "week-block")} key={i}>
                                    {
                                        weekBlocks.map((b, block_index) => (
                                            <StatusAllocationBlock
                                                key={block_index}
                                                type={b.type}
                                                tooltipText={b.showShortText ? b.displayText : undefined}
                                                text={b.showShortText ? b.displayShortText : b.displayText}
                                                blockSize={b.blockLength}
                                                isPlanned={b.certainty === Screen.AllocationCertainty.Planned}
                                            />
                                        ))
                                    }
                                </div>
                            );
                        })
                    }
                </div>
            );
        }
    }
}
