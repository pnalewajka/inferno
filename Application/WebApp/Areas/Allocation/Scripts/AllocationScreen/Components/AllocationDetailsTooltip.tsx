namespace Allocation.Screen {
    export class AllocationDetailsTooltip extends Dante.Components.StoreComponent<Store.IAllocationStoreState, { block: Allocation.Interfaces.IAllocationBlock }, { isOpen: boolean, permissions: Interfaces.IEmployeePermissions }> {
        public constructor() {
            super(Store.defaultStore());

            this.state = { isOpen: false, permissions: Store.Employee.defaultPermissions };
        }

        private rowTopOffset?: number;
        private rowleftOffset?: number;

        protected updateFromStore(state: Store.IAllocationStoreState): void {
            const { permissions } = state.employee;

            this.updateState({ permissions });
        }

        public show(blockElement: HTMLDivElement): void {
            if (blockElement.parentElement !== null) {
                this.rowTopOffset = blockElement.parentElement.offsetTop;
            }

            this.updateState({ isOpen: true });
        }

        public hide(): void {
            this.updateState({ isOpen: false });
        }

        private preventDefault(e: React.MouseEvent<HTMLElement>): boolean {
            e.preventDefault();
            e.stopPropagation();

            return false;
        }

        private rendered(item: HTMLDivElement): void {
            let overAnchor: boolean = false;

            const $item = $(item)
                .blur(() => { if (!overAnchor) { this.hide(); } })
                .focus();

            $("a[href][target]", $item)
                .click((e) => { overAnchor = false; this.hide(); e.stopPropagation(); })
                .mouseenter(() => { overAnchor = true; })
                .mouseleave(() => { overAnchor = false; });

            $("button", $item)
                .click((e) => {
                    setTimeout(() => {
                        overAnchor = false;
                    }, 1);
                })
                .mouseenter(() => { overAnchor = true; })
                .mouseleave(() => { overAnchor = false; });

            if (this.rowTopOffset !== undefined && $item.outerHeight() > this.rowTopOffset) {
                $item.addClass("on-bottom");
                allocationStateChange.dispatch({});
            } else {
                $item.addClass("on-top");
            }
        }

        private openBlockEdit(requestId: number): void {
            Allocation.Screen.openCloneRequestDialog("/Allocation/AllocationRequest/GetAddDialog", this.props.block.requestId);
        }

        private openBlockDelete(requestId: number): void {
            CommonDialogs.confirm(`${Translations.deleteConfirmationMessage} ${this.props.block.employeeName}`, Translations.deleteConfirmationTitle, (result) => {
                if (result.isOk) {
                    this.hide();
                    CommonDialogs.pleaseWait.show();
                    $.post(`/Allocation/AllocationRequest/Delete/?ids=${requestId}`).then(() => {
                        window.location.reload();
                    }).always(() => {
                        CommonDialogs.pleaseWait.hide();
                    });
                }
            });
        }

        private PremissionAnchor(data: { canAccess: boolean, url: string, style: React.CSSProperties } & JSX.ElementChildrenAttribute): JSX.Element {
            if (data.canAccess) {
                return <a style={data.style} target="_blank" href={data.url}>{data.children}</a>;
            }

            return <span style={data.style}>{data.children}</span>;
        }

        private openEditDialog(contentType: Screen.AllocationRequestContentType): void {
            Dialogs.showDialog({
                actionUrl: `/Allocation/AllocationRequest/GetEditDialog/${this.props.block.requestId}?content-type=${contentType}`,
                eventHandler: (result) => {
                    if (result.isOk) {
                        window.location.reload();
                    }
                }
            });
        }

        public render(): JSX.Element | null {
            if (this.state.isOpen) {
                const anchorStyle: React.CSSProperties = { color: this.props.block.projectColor };
                const jiraIssueKeyUrl = Strings.format(Translations.jiraIssueKeyProjectUrlFormat, this.props.block.jiraIssueKey);
                const jiraKeyUrl = Strings.format(Translations.jiraKeyProjectKeyUrlFormat, this.props.block.jiraKey);

                const blockFields: Array<{
                    notUsed?: boolean,
                    label: string,
                    render: () => JSX.Element | string
                }> = [
                        {
                            label: Translations.employeeLabel,
                            render: () => (
                                <this.PremissionAnchor
                                    canAccess={this.state.permissions.canEditEmployee || this.state.permissions.canViewEmployee}
                                    style={anchorStyle}
                                    url={`/Allocation/Employee/${this.state.permissions.canEditEmployee ? "Edit" : "View"}/${this.props.block.employeeId}`}>
                                    {`${this.props.block.employeeName} (${this.props.block.employeeAcronym})`}
                                </this.PremissionAnchor>
                            )
                        },
                        {
                            label: Translations.projectNameLabel,
                            render: () => (
                                <this.PremissionAnchor
                                    canAccess={this.state.permissions.canEditProject || this.state.permissions.canViewProject}
                                    style={anchorStyle}
                                    url={`/Allocation/Project/${this.state.permissions.canEditProject ? "Edit" : "View"}/${this.props.block.projectId}`}>
                                    {this.props.block.projectShortName}
                                </this.PremissionAnchor>
                            )
                        },
                        { label: Translations.clientNameLabel, render: () => this.props.block.clientShortName },
                        { label: Translations.jiraIssueKeyLabel, render: () => <a style={anchorStyle} target="_blank" href={jiraIssueKeyUrl}>{this.props.block.jiraIssueKey}</a> },
                        { notUsed: !this.props.block.jiraKey, label: Translations.jiraKeyLabel, render: () => <a style={anchorStyle} target="_blank" href={jiraKeyUrl}>{this.props.block.jiraKey}</a> },
                        { label: Translations.allocationPercentageLabel, render: () => `${this.props.block.allocationPercentage}%` },

                        { label: Translations.hoursLabel, render: () => this.props.block.allocationDayHours.map((h, i) => `${Translations.dayDictionary[i]}: ${h}`).join(", ") },
                        { label: Translations.allocationTypeLabel, render: () => Globals.enumResources.AllocationCertainty[this.props.block.certainty] },
                    ];

                return (
                    <div className="block-tooltip" onClick={this.preventDefault} onDoubleClick={this.preventDefault}>
                        <div className="block-tooltip-body" tabIndex={1} ref={(item) => { if (item !== null) { this.rendered(item); } }}>
                            <div className="icons">
                                <i className="fa fa-close" onClick={() => this.hide()}></i>
                            </div>
                            {blockFields.filter(f => !f.notUsed).map(f => (
                                <div className="form-group" key={f.label}>
                                    <label>{f.label}</label>
                                    <span className="form-text">{f.render()}</span>
                                </div>
                            ))}
                            <div className="btn-group">
                                {
                                    this.state.permissions.canCloneRequest
                                        ? <button className="btn btn-default btn-xs" onClick={(e) => { e.stopPropagation(); e.preventDefault(); this.openBlockEdit(this.props.block.requestId); }}>{Translations.cloneButtonLabel}</button>
                                        : null
                                }
                                {
                                    this.state.permissions.canDeleteRequest
                                        ? <button className="btn btn-default btn-xs" onClick={(e) => { e.stopPropagation(); e.preventDefault(); this.openBlockDelete(this.props.block.requestId); }}>{Translations.deleteButtonLabel}</button>
                                        : null
                                }

                                <button className="btn btn-default btn-xs" onClick={(e) => { e.stopPropagation(); e.preventDefault(); this.openEditDialog(Screen.AllocationRequestContentType.Simple); }}>{Translations.simpleEditButton}</button>
                                <button className="btn btn-default btn-xs" onClick={(e) => { e.stopPropagation(); e.preventDefault(); this.openEditDialog(Screen.AllocationRequestContentType.Advanced); }}>{Translations.advanceEditButton}</button>
                            </div>
                            <div className="project-picture">
                                <div className="project-picture-body" style={{ backgroundImage: `url('/Allocation/ProjectPicture/Download?projectId=${this.props.block.projectId}&userPictureSize=Normal')` }}></div>
                            </div>
                        </div>
                    </div>
                );
            }

            return null;
        }
    }
}
