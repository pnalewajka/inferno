﻿namespace Allocation.Screen {
    interface IHoursInputState {
        value: string | string[] | number;
    }

    export class HoursInput extends Dante.Components.StateComponent<React.InputHTMLAttributes<HTMLInputElement> & { onValueChanged: (value: number) => void }, IHoursInputState> {
        public render(): JSX.Element {

            const inputProps =
                {
                    ...this.props,
                    ... {
                        value: this.state.value,
                        onChange: this.updateHours.bind(this),
                        onBlur: () => {
                            this.updateState({ value: this.props.value });
                        }
                    }
                };

            delete inputProps.onValueChanged;

            return <input {...inputProps} />;
        }

        protected receiveProps(newProps: React.InputHTMLAttributes<HTMLInputElement>): void {
            this.updateState({ value: newProps.value });
        }

        protected propsChanged(nextProps: React.InputHTMLAttributes<HTMLInputElement>): boolean {
            return nextProps.value !== this.props.value || nextProps.disabled !== this.props.disabled;
        }

        protected stateChanged(nextState: IHoursInputState): boolean {
            return nextState.value !== this.state.value;
        }

        private updateHours(event: React.ChangeEvent<HTMLInputElement>): void {
            const stringValue = event.target.value;
            const value = +stringValue;

            if (isNaN(value) || value < 0 || value > 24) {
                this.updateState({ value: stringValue });
            } else {
                this.props.onValueChanged(value);
            }
        }
    }
}
