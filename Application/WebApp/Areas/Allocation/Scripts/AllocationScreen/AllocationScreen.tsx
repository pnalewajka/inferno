﻿namespace Allocation.Screen {
    export function Init(data: dynamic) {
        Store.defaultStore().dispatch(Store.Actions.loadData(data));
        ReactDOM.render(<Allocation.Screen.AllocationGrid {...data} />, document.getElementById("allocation-table"));
        ReactDOM.render(<Footer />, document.getElementById("footer-items"));
    }

    export function openAddRequestDialog(url: string, contentType: string): void {
        const store = Store.defaultStore();
        const { selection } = store.getState();

        Dialogs.showDialog({
            actionUrl: `${url}?ids=${selection.selectedIds.join(',')}&content-type=${contentType}`,
            eventHandler: (result) => {
                if (result.isOk) {
                    window.location.reload();
                }
            }
        });
    }

    export function openCloneRequestDialog(url: string, requestId: number) {
        Dialogs.showDialog({
            actionUrl: `${url}?clone-from-id=${requestId}`,
            eventHandler: (result) => {
                if (result.isOk) {
                    window.location.reload();
                }
            }
        });
    }

    export enum AllocationViewType {
        SummaryView = 1 << 0,
        DetailedView = 1 << 1,
        AllocationView = 1 << 2,
    }

    export enum AllocationCertainty {
        Planned = 0,
        Certain = 1,
    }

    export enum ProcessingStatus {
        Processing = 1,
        Processed = 2,
    }

    export enum AllocationRequestContentType {
        Simple = 1,
        Advanced = 2
    }
}