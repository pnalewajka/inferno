namespace Allocation.Screen {
  export const cellWidth: number = 30;
  export const rowHeight: number = 25;
  export let maxColumnWidth: number = 100;
  export const defaultInfoPadding: number = 5;

  export const debounceWidth = Dante.Utils.Debounce(() => {
    const newWidth = maxColumnWidth;
    Dante.Utils.StaticCss(".allocation-group .employee-info, .column-header", {
      minWidth: newWidth,
      maxWidth: newWidth,
      width: newWidth
    });
  });
}
