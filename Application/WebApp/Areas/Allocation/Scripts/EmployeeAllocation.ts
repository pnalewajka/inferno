﻿Grid.GridComponent.prototype.getSelectedRowData = (() => {
    const $selectedRows = $(`tr.${EmployeeAllocation.selectRowClass}[id]`);
    const results: { id: string }[] = [];

    for (let i = 0; i < $selectedRows.length; i++) {
        results.push({ id: $($selectedRows[i]).attr("id") });
    }

    return results;
}) as <T extends Grid.IRow>() => T[];

module EmployeeAllocation {
    export const selectRowClass = "allocation-selected";

    var dateFilterPrefix = "date-range";
    var periodTypeUrlName = "period-type";

    export function changeAllocationPeriodType(type: string) {
        const newUrl = UrlHelper.updateUrlParameter(window.location.href, periodTypeUrlName, type);
        window.location.href = newUrl;
    }

    export function changeFilterDate(monthsToAdd: number, defaultFrom: Date, defaultTo: Date) {
        debugger;
        const fromKey = `${dateFilterPrefix}.from`;
        const toKey = `${dateFilterPrefix}.to`;

        const filters = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), "filter");
        let toDate: Date;
        let fromDate: Date;
        let newUrl = window.location.href;
        if (filters === null || filters.indexOf(dateFilterPrefix) === -1 || monthsToAdd === null) {
            let newFilters: string;
            if (filters !== null) {
                newFilters = `${filters},${dateFilterPrefix}`;
            }
            else {
                newFilters = `${dateFilterPrefix}`;
            }
            newUrl = UrlHelper.updateUrlParameter(newUrl, "filter", newFilters);

            fromDate = defaultFrom;
            toDate = defaultTo;
        } else {
            const fromUrl = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), fromKey);
            const toUrl = UrlHelper.getQueryParameter(UrlHelper.getUrlParameters(window.location.href), toKey);
            toDate = new Date(toUrl!);
            fromDate = new Date(fromUrl!);
        }

        fromDate.setMonth(fromDate.getMonth() + monthsToAdd);
        toDate.setMonth(toDate.getMonth() + monthsToAdd);

        newUrl = UrlHelper.updateUrlParameter(newUrl, toKey, UrlHelper.dateUrlParameter(toDate));
        newUrl = UrlHelper.updateUrlParameter(newUrl, fromKey, UrlHelper.dateUrlParameter(fromDate));

        Forms.redirect(newUrl);
    }
}

$(() => {

    const $table = $(".grid-allocation table");
    const $fixedColumns = $(".fix-horizontal", $table);
    const $fixedHeaders = $(".fix-vertical", $table);

    $(".grid-allocation")
        .scroll(function(this: JQuery, e: any) {
            const positon = $("table", this).position();
            $fixedColumns.css("left", positon.left * -1);
            $fixedHeaders.css("top", positon.top * -1);
        });

    const noProjectMessage = $(".grid-allocation [data-no-project-message]").data("no-project-message");

    $(".allocation-project-row").hover(function(this: JQuery) {
        const $item = $(this);
        if ($item.is(".proc")) {
            return;
        }

        if ($(".message", $item).length > 0) {
            $item.append(`<span class="message-hover">${noProjectMessage}</span>`);
        }

        if ($item.is("a[href][data-period]")) {
            const $parent = $item.parents("tr[data-url-add][data-url-edit]");
            const period = $item.data("period");
            if ($item.is("[data-project]")) {
                const editUrltemplate = $parent.data("url-edit");
                const projectId = $item.data("project");
                $item.attr("href", editUrltemplate.replace("~period", period).replace("~projectId", projectId));
            } else {
                const addUrltemplate = $parent.data("url-add");
                $item.attr("href", addUrltemplate.replace("~period", period));
            }
        }

        $item.append("<div class='ripple'></div>");
        $item.addClass("proc");
    }, () => {});
    

    $(".grid-table").css("visibility", "visible");

    const selectRow = ($row: JQuery, force = false) => {
        if (force && $row.is(`.${EmployeeAllocation.selectRowClass}`)) {
            $row.addClass(EmployeeAllocation.selectRowClass);
        } else {
            $row.toggleClass(EmployeeAllocation.selectRowClass);
        }
        var grid = <Grid.GridComponent>Controls.getInstance(Grid.GridComponent, $row[0]);
        grid.onRowClicked($row);
    };

    let clickCounter: number = 0;
    let timer: number;
    $(".weekly-projects a[href]").click(function(this: JQuery, eventData: MouseEvent) {
        const $anchor = $(this);
        const $rowParent = $anchor.parents("tr");

        eventData.preventDefault();
        eventData.stopPropagation();

        selectRow($rowParent, true);

        clickCounter++;
        if (clickCounter > 1) {
            clearTimeout(timer);
            selectRow($rowParent, true);
            window.location.assign($anchor.attr("href"));
        } else {
            timer = window.setTimeout(() => {
                clickCounter = 0;
            }, 200);
        }
    });

    $("tbody tr td", $table).click(function(this: JQuery, eventArgs: MouseEvent) {
        eventArgs.stopPropagation();
        selectRow($(this).parent());
    });
}); 