﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Allocation.Breadcrumbs
{
    public class EmploymentPeriodBreadcrumbItem : BreadcrumbItem
    {
        public EmploymentPeriodBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = Resources.EmployeeResources.HrdataTabName;
        }
    }
}