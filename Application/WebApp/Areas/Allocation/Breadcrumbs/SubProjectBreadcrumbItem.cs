﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Allocation.Breadcrumbs
{
    public class SubProjectBreadcrumbItem : BreadcrumbItem
    {
        public SubProjectBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = ProjectResources.SubProjects;
        }
    }
}