﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Allocation.Breadcrumbs
{
    public class StaffingDemandBreadcrumbItem : BreadcrumbItem
    {
        public StaffingDemandBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) 
            : base(breadcrumbContextProvider)
        {
            DisplayName = ProjectResources.StaffingDemandTitle;
        }
    }
}