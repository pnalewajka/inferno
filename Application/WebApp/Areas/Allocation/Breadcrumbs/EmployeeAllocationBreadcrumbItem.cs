﻿using System;
using System.Linq;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.Allocation.Breadcrumbs
{
    public class EmployeeAllocationBreadcrumbItem : CardIndexBreadcrumbItem
    {
        public EmployeeAllocationBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider, IBreadcrumbService breadcrumbService) : base(breadcrumbContextProvider, breadcrumbService)
        {
            var atomicController = (AtomicController)breadcrumbContextProvider.Context.Controller;

            if (atomicController is EmployeeAllocationController)
            {
                DisplayName = atomicController.Layout.PageTitle;
            }
            else if (atomicController is MyTeamAllocationController)
            {
                var controllerName = RoutingHelper.GetControllerName<EmployeeAllocationController>();
                var references = Context.SiteMap.GetPath(controllerName, nameof(EmployeeAllocationController.List)).ToList();

                var items = references
                    .Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText()))
                    .Take(references.Count - 1)
                    .Concat(new[] { new BreadcrumbItemViewModel(null, atomicController.Layout.PageTitle) })
                    .ToList();

                SetItems(items);
            }
            else
            {
                throw new ArgumentException($"Unknown controller type: {atomicController.GetType()}", nameof(breadcrumbContextProvider.Context.Controller));
            }
        }
    }
}
