﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.WebApp.Areas.Allocation.Helpers
{
    public static class EmployeeContractHelper
    {
        public static DateTime? GetEmploymentStartDate(EmploymentPeriodDto current, EmploymentPeriodDto[] all)
        {
            if (current == null)
            {
                return null;
            }

            var employmentsInOrder = all.OrderBy(x => x.StartDate).ToList();
            int index = employmentsInOrder.FindIndex(x => x.StartDate.Date == current.StartDate.Date);

            DateTime result = current.StartDate.Date;

            for (int i = index - 1; i >= 0; i--)
            {
                DateTime toFind = result.AddDays(-1);

                if (employmentsInOrder[i].EndDate?.Date == toFind)
                {
                    result = employmentsInOrder[i].StartDate.Date;
                }
                else
                {
                    break;
                }
            }

            return result;
        }

        public static DateTime? GetEmploymentEndDate(EmploymentPeriodDto current, EmploymentPeriodDto[] all)
        {
            if (current?.EndDate == null)
            {
                return null;
            }

            var employmentsInOrder = all.OrderBy(x => x.StartDate).ToList();
            int index = employmentsInOrder.FindIndex(x => x.StartDate.Date == current.StartDate.Date);

            DateTime result = current.EndDate.Value.Date;

            for (int i = index + 1; i < employmentsInOrder.Count; i++)
            {
                DateTime toFind = result.AddDays(1);

                if (employmentsInOrder[i].StartDate.Date == toFind)
                {
                    if (employmentsInOrder[i].EndDate.HasValue)
                    {
                        result = employmentsInOrder[i].EndDate.Value.Date;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    break;
                }
            }

            return result;
        }
    }
}
