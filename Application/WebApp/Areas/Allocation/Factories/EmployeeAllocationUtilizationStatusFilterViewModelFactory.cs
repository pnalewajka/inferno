﻿using System;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeAllocationUtilizationStatusFilterViewModelFactory : IViewModelFactory<EmployeeAllocationUtilizationStatusFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public EmployeeAllocationUtilizationStatusFilterViewModelFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public EmployeeAllocationUtilizationStatusFilterViewModel Create()
        {
            return new EmployeeAllocationUtilizationStatusFilterViewModel
            {
                Week = _timeService.GetCurrentDate().GetPreviousDayOfWeek(DayOfWeek.Monday),
                AllocationStatuses = new long[] {}
            };
        }
    }
}