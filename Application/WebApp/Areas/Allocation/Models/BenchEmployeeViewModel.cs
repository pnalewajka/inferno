﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Formatters;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class BenchEmployeeViewModel
    {
        private const decimal FulltimeEquivalentHours = 40;

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long Id { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Hub(nameof(Id))]
        public TooltipText GetName(UrlHelper helper)
        {
            const decimal grayingOutThreshold = 60M;

            var name = $"{FirstName} {LastName}";
            var shouldBeGrayedOut = StaffingStatus.Any(s => s.AllocationUsage >= grayingOutThreshold && s.StartDate > CurrentDate &&
                (s.AllocationText == BenchEmployeeResources.ConfirmedFromText
                    || s.AllocationText == BenchEmployeeResources.ConfirmedFromTextWithEnd));

            return new TooltipText
            {
                CssClass = shouldBeGrayedOut ? "disabled-icon" : null,
                Label = name,
                Tooltip = shouldBeGrayedOut ? string.Format(EmployeeResources.GrayedOutEmployee, grayingOutThreshold) : string.Empty
            };
        }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        [StringLength(50)]
        [Visibility(VisibilityScope.None)]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        [StringLength(50)]
        [Visibility(VisibilityScope.None)]
        public string LastName { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? LocationId { get; set; }

        [Order(9)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        public long? LineManagerId { get; set; }

        [Order(8)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeManager), nameof(EmployeeResources.EmployeeManagerShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsOrgUnitManager) + "'")]
        public long? ManagerId { get; set; }

        [Order(7)]
        [Ellipsis(MaxStringLength = 20)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        public long[] SkillIds { get; set; }

        [Order(5)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long[] JobProfileIds { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLabel), nameof(EmployeeResources.EmployeeJobMatrixShortLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController), GridFormatterType = typeof(JobMatrixLevelPickerGridFormatter))]
        public long[] JobMatrixIds { get; set; }

        [Order(11)]
        [DisplayNameLocalized(nameof(EmployeeResources.WeeksOnBench), nameof(EmployeeResources.WeeksOnBenchShort), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public int WeeksOnBench { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime? OnBenchSince { get; set; }

        [Order(1)]
        [NonSortable]
        [DisplayNameLocalized(nameof(EmployeeResources.StaffingStatus), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public HtmlString AllocationDescription => new HtmlString(string.Join("<br/>", StaffingStatus));

        [Visibility(VisibilityScope.None)]
        public IList<StaffingStatusDto> StaffingStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal PercentageAllocation { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal HoursAllocated { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal AvailableHours { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime CurrentDate { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(EmployeeResources.CurrentUtilization), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "percentage-column")]
        public string CurrentUtilization => $"{decimal.Round(PercentageAllocation)}%";

        [Order(3)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.FulltimeEquivalent), typeof(EmployeeResources))]
        public decimal FTE => Math.Max(0, (AvailableHours - HoursAllocated) / FulltimeEquivalentHours);

        [Order(11)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), typeof(EmployeeResources))]
        public string OrgUnitFlatName { get; set; }

        [Order(12)]
        [Ellipsis(MaxStringLength = 150, CssClass = "comment-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.StaffingComment), typeof(EmployeeResources))]
        [NonSortable]
        public string StaffingComment { get; set; }

        [Order(3)]
        [ProgressBar(BarCssClass = "progress-bar-success")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized("ResumePercentage", typeof(EmployeeResources))]
        public decimal FillPercentage { get; set; }

        [Order(4)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized("ResumeModificationDate", typeof(EmployeeResources))]
        public DateTime? ResumeModifiedOn { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(VacationBalanceResources.VacationBalanceTitle), typeof(VacationBalanceResources))]
        [NonSortable]
        public decimal VacationBalance { get; set; }
    }
}