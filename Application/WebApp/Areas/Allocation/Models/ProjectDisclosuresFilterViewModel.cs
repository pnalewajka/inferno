﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.ProjectDisclosuresFilter")]
    [DescriptionLocalized("ProjectDisclosuresFilterDialogTitle", typeof(ProjectResources))]
    [DialogLayout(LabelWidth = LabelWidth.Large)]
    public class ProjectDisclosuresFilterViewModel 
    {
        public ProjectDisclosuresFilterViewModel()
        {
            WeCanTalkAboutClientFilterOption = BoolFilterOptionsEnum.Yes;
            WeCanTalkAboutProjectFilterOption = BoolFilterOptionsEnum.Yes;
            WeCanUseClientLogoFilterOption = BoolFilterOptionsEnum.Yes;
        }

        [DisplayNameLocalized(nameof(ProjectResources.WeCanTalkAboutClient), typeof(ProjectResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<BoolFilterOptionsEnum>))]
        public BoolFilterOptionsEnum WeCanTalkAboutClientFilterOption { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.WeCanTalkAboutProject), typeof(ProjectResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<BoolFilterOptionsEnum>))]
        public BoolFilterOptionsEnum WeCanTalkAboutProjectFilterOption { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.WeCanUseClientLogo), typeof(ProjectResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<BoolFilterOptionsEnum>))]
        public BoolFilterOptionsEnum WeCanUseClientLogoFilterOption { get; set; }

        public override string ToString()
        {
            return ProjectResources.ProjectDisclosuresFilterDialogTitle;
        }
    }
}