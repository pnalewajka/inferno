﻿using System;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EditAllocationRequestModel : IEditAllocationContract
    {
        public EditAllocationRequestModel()
        {
            Hours = new decimal[7];
        }

        public decimal MondayHours => Hours[0];

        public decimal TuesdayHours => Hours[1];

        public decimal WednesdayHours => Hours[2];

        public decimal ThursdayHours => Hours[3];

        public decimal FridayHours => Hours[4];

        public decimal SaturdayHours => Hours[5];

        public decimal SundayHours => Hours[6];

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal[] Hours { get; set; }

        public AllocationCertainty Certainty { get; set; }
    }
}