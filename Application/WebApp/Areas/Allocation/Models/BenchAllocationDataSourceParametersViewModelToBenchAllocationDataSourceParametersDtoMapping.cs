﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class BenchAllocationDataSourceParametersViewModelToBenchAllocationDataSourceParametersDtoMapping : ClassMapping<BenchAllocationDataSourceParametersViewModel, BenchAllocationDataSourceParametersDto>
    {
        public BenchAllocationDataSourceParametersViewModelToBenchAllocationDataSourceParametersDtoMapping()
        {
            Mapping = v => new BenchAllocationDataSourceParametersDto
            {
                ContractTypeIds = v.ContractTypeIds,
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                JobProfileIds = v.JobProfileIds,
                OrgUnitId = v.OrgUnitId,
                LocationIds = v.LocationIds,
                PlaceOfWork = v.PlaceOfWork,
                SkipWorkingStudents = v.SkipWorkingStudents,
                StaffingStatuses = v.StaffingStatuses,
                EmployeeStatuses = v.EmployeeStatuses,
            };
        }
    }
}