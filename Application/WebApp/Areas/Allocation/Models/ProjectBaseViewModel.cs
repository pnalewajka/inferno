﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public abstract class ProjectBaseViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.ProjectShortNameLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [StringLength(128)]
        public virtual string ProjectShortName { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.NameLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [StringLength(255)]
        public virtual string ClientProjectName { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.ClientShortNameLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(128)]
        public virtual string ClientShortName { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.ProjectShortNameLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        public virtual string ProjectName { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.DescriptionLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(4096)]
        [Render(GridCssClass = "up-to-40-percent", Size = Size.Large)]
        public virtual string Description { get; set; }

        [StringLength(9)]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectAcronymLabel), typeof(ProjectResources))]
        [Required]
        public virtual string Acronym { get; set; }
    }
}