﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectReportParametersViewModelToProjectReportParametersDtoMapping : ClassMapping<ProjectReportParametersViewModel, ProjectReportParametersDto>
    {
        public ProjectReportParametersViewModelToProjectReportParametersDtoMapping()
        {
            Mapping = v => new ProjectReportParametersDto
            {
                ProjectIds = v.ProjectIds
            };
        }
    }
}
