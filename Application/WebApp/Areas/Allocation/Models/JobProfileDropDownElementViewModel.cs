﻿using Smt.Atomic.WebApp.Areas.Allocation.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class JobProfileDropDownElementViewModel : IChartDropDownElement
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(Name) ? Description : Name;
        }
    }
}