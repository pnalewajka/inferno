﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectInvoicingRateViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.ProjectInvoicingRateCode), typeof(ProjectResources))]
        [Required]
        public string Code { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.ProjectInvoicingRateValue), typeof(ProjectResources))]
        [Required]
        public decimal Value { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.ProjectInvoicingRateDescription), typeof(ProjectResources))]
        public string Description { get; set; }
    }
}
