﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using Controllers;
    using CrossCutting.Common.Attributes;
    using Presentation.Renderers.Bootstrap.Attributes;
    using Resources;
    using Smt.Atomic.Business.Allocation.Consts;

    [Identifier("AllocationRequestFilter.employeesAndProjects")]
    [DescriptionLocalized("EmployeesAndProjectsFilter", typeof(AllocationRequestResources))]
    public class AllocationRequestProjectsAndEmployeesFilterViewModel
    {
        [ValuePicker(Type = typeof (ProjectPickerController), OnResolveUrlJavaScript = "url += '&filter=" + FilterCodes.ActiveProjects + "'")]
        [DisplayNameLocalized(nameof(AllocationRequestResources.ProjectsLabel), typeof(AllocationRequestResources))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] ProjectsIds { get; set; }

        [ValuePicker(Type = typeof (EmployeePickerController))]
        [DisplayNameLocalized(nameof(AllocationRequestResources.EmployeesLabel), typeof(AllocationRequestResources))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] EmployeesIds { get; set; }

        public override string ToString()
        {
            return AllocationRequestResources.EmployeesAndProjectsFilter;
        }
    }
}