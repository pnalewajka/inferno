﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.WebApp.Areas.Allocation.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ChartDropDownViewModel
    {
        public ChartDropDownViewModel(IChartDropDownElement dropDownItem, IEnumerable<ChartEntryViewModel> chartEntries, 
            decimal averageNotAllocatedPercentForPeriod, DateTime averagePeriodFrom, DateTime averagePeriodTo )
        {
            Items = chartEntries.ToList();
            DropDownItemViewModel = dropDownItem;
            AverageNotAllocatedPercentForPeriod = averageNotAllocatedPercentForPeriod;
            AveragePeriodFrom = averagePeriodFrom;
            AveragePeriodTo = averagePeriodTo;
        }

        public DateTime CurrentWeek { get; set; }

        public IChartDropDownElement DropDownItemViewModel { get; set; }

        public IReadOnlyCollection<ChartEntryViewModel> Items { get; set; }

        public decimal AverageNotAllocatedPercentForPeriod { get; set; }

        public DateTime AveragePeriodFrom { get; set; }

        public DateTime AveragePeriodTo { get; set; }
    }
}