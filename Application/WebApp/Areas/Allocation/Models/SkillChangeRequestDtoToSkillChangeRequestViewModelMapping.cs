﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SkillChangeRequestDtoToSkillChangeRequestViewModelMapping : ClassMapping<SkillChangeRequestDto, SkillChangeRequestViewModel>
    {
        public SkillChangeRequestDtoToSkillChangeRequestViewModelMapping()
        {
            Mapping = d => new SkillChangeRequestViewModel
            {
                EmployeeId = d.EmployeeId,
                SkillIds = d.SkillIds
            };
        }
    }
}
