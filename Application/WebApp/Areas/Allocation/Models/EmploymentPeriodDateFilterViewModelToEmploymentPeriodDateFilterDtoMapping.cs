﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmploymentPeriodDateFilterViewModelToEmploymentPeriodDateFilterDtoMapping : ClassMapping<EmploymentPeriodDateFilterViewModel, EmploymentPeriodDateFilterDto>
    {
        public EmploymentPeriodDateFilterViewModelToEmploymentPeriodDateFilterDtoMapping()
        {
            Mapping = v => new EmploymentPeriodDateFilterDto
            {
                From = v.From.Value,
                To = v.To.Value
            };
        }
    }
}
