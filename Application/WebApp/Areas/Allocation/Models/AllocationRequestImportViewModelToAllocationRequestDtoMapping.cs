﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.ComponentModel;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationRequestImportViewModelToAllocationRequestDtoMapping : ClassMapping<AllocationRequestImportViewModel, AllocationRequestDto>
    {
        private readonly IAllocationRequestImportLookupService _lookapService;

        public AllocationRequestImportViewModelToAllocationRequestDtoMapping(IAllocationRequestImportLookupService lookapService)
        {
            _lookapService = lookapService;
            Mapping = m => GetDto(m);
        }

        private AllocationRequestDto GetDto(AllocationRequestImportViewModel viewModel)
        {
            var dto = new AllocationRequestDto();

            if (viewModel.ContentType == 0)
            {
                viewModel.ContentType = AllocationRequestViewModelToAllocationRequestDtoMapping.DetermineContentType(viewModel);
            }

            SetCommonFields(dto, viewModel);

            switch (viewModel.ContentType)
            {
                case AllocationRequestContentType.Simple:
                    AllocationRequestViewModelToAllocationRequestDtoMapping.SetHoursPerDay(dto, viewModel);
                    break;

                case AllocationRequestContentType.Advanced:
                    AllocationRequestViewModelToAllocationRequestDtoMapping.SetHoursFromDays(dto, viewModel);
                    break;

                default:
                    throw new InvalidEnumArgumentException(@"ContentType");
            }

            return dto;
        }
        
        private void SetCommonFields(AllocationRequestDto dto, AllocationRequestImportViewModel model)
        {
            dto.Id = model.Id;
            dto.EmployeeId = _lookapService.GetEmployeeIdByEmail(model.EmployeeEmail);
            dto.ProjectId = _lookapService.GetProjectIdByCode(model.ProjectCode);
            dto.StartDate = model.StartDate;
            dto.EndDate = model.EndDate;
            dto.ContentType = model.ContentType;
            dto.AllocationCertainty = model.AllocationCertainty;
            dto.Timestamp = model.Timestamp;
            dto.EmployeeFirstName = model.EmployeeFirstName;
            dto.EmployeeLastName = model.EmployeeLastName;
            dto.ProjectName = model.ProjectCode;
        }
    }
}