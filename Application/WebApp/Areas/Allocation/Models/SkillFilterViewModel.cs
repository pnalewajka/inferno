﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.SkillFilter")]
    [DescriptionLocalized("EmployeeSkillsFilterDialogTitle", typeof(EmployeeResources))]
    public class SkillFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] SkillIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeeSkillsLabel;
        }
    }
}