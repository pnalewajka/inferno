﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectInvoicingRateViewModelToProjectInvoicingRateDtoMapping : ClassMapping<ProjectInvoicingRateViewModel, ProjectInvoicingRateDto>
    {
        public ProjectInvoicingRateViewModelToProjectInvoicingRateDtoMapping()
        {
            Mapping = v => new ProjectInvoicingRateDto
            {
                Code = v.Code,
                Value = v.Value,
                Description = v.Description
            };
        }
    }
}
