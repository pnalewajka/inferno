﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationRequestViewModelBinder : AtomicModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var contentTypePropertyName = PropertyHelper.GetPropertyName<AllocationRequestViewModel>(t => t.ContentType);
            var contentTypeParameterName = NamingConventionHelper.ConvertPascalCaseToHyphenated(contentTypePropertyName);
            var value = bindingContext.ValueProvider.GetValue(contentTypeParameterName);

            if (value != null)
            {
                var contentType = (AllocationRequestContentType)value.ConvertTo(typeof(AllocationRequestContentType));
                var viewModelType = AllocationRequestDtoToAllocationRequestViewModelMapping.GetViewModelType(contentType);

                bindingContext.ModelMetadata = ModelMetadataProviders
                    .Current
                    .GetMetadataForType(null, viewModelType);
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}