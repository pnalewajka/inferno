using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Models.SimpleProjectViewModel")]
    public class SimpleProjectViewModel : ProjectBaseViewModel
    {
        [Required]
        public override string ClientShortName { get; set; }

        [Required]
        public override string ProjectShortName { get; set; }

        [Required]
        public override string Acronym { get; set; }

        [Required]
        public override string Description { get; set; }
    }
}