﻿using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ManagerViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        public string FirstName { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        public string LastName { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(EmployeeResources.TitleLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobTitlePickerController))]
        public long? JobTitleId { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.TitleLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        public LocalizedString JobTitleName { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}".Trim();
        }
    }
}
