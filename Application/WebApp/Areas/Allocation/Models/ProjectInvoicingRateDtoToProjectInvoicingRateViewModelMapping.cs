﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectInvoicingRateDtoToProjectInvoicingRateViewModelMapping : ClassMapping<ProjectInvoicingRateDto, ProjectInvoicingRateViewModel>
    {
        public ProjectInvoicingRateDtoToProjectInvoicingRateViewModelMapping()
        {
            Mapping = d => new ProjectInvoicingRateViewModel
            {
                Code = d.Code,
                Value = d.Value,
                Description = d.Description
            };
        }
    }
}
