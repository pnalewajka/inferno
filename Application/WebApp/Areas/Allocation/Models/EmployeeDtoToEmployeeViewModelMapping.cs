﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeDtoToEmployeeViewModelMapping : ClassMapping<EmployeeDto, EmployeeViewModel>
    {
        public EmployeeDtoToEmployeeViewModelMapping()
        {
            Mapping = d => new EmployeeViewModel
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                JobTitleId = d.JobTitleId,
                Email = d.Email,
                Login = d.Login,
                DateOfBirth = d.DateOfBirth,
                PersonalNumber = d.PersonalNumber,
                PersonalNumberType = d.PersonalNumberType,
                IdCardNumber = d.IdCardNumber,
                PhoneNumber = d.PhoneNumber,
                SkypeName = d.SkypeName,
                HomeAddress = d.HomeAddress,
                AvailabilityDetails = d.AvailabilityDetails,
                CurrentEmployeeStatus = d.CurrentEmployeeStatus,
                Timestamp = d.Timestamp,
                JobProfileIds = d.JobProfileIds.ToArray(),
                SkillIds = d.SkillIds.ToArray(),
                JobMatrixId = d.JobMatrixIds != null && d.JobMatrixIds.Any()
                            ? d.JobMatrixIds.First() 
                            : (long?) null,
                LocationId = d.LocationId,
                PlaceOfWork = d.PlaceOfWork,
                CompanyId = d.CompanyId,
                UserId = d.UserId,
                OrgUnitId = d.OrgUnitId,
                LineManagerId = d.LineManagerId,
                IsProjectContributor = d.IsProjectContributor,
                IsCoreAsset = d.IsCoreAsset,
                MinimumOvertimeBankBalance = d.MinimumOvertimeBankBalance,
                JobProfileNames = d.JobProfileNames,
                SkillNames = d.SkillNames,
                JobMatrixName = d.JobMatrixName,
                LocationName = d.LocationName,
                JobTitleName = d.JobTitleName,
                OrgUnitName = d.OrgUnitName,
                CompanyName = d.CompanyName,
                LineManagerFullName = d.LineManagerFullName,
                Acronym = d.Acronym,
                ResumeModificationDate = d.ResumeModificationDate,
                LastResumeId = d.LastResumeId,
                FillPercentage = d.FillPercentage.FirstOrDefault(),
                EmployeeFeatures = d.EmployeeFeatures,
                IsOwnEmployee = d.IsOwnEmployee
            };
        }
    }
}
