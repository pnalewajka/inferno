﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ActiveDirectoryGroupViewModelToActiveDirectoryGroupDtoMapping : ClassMapping<ActiveDirectoryGroupViewModel, ActiveDirectoryGroupDto>
    {
        public ActiveDirectoryGroupViewModelToActiveDirectoryGroupDtoMapping()
        {
            Mapping = v => new ActiveDirectoryGroupDto
            {
                Id = v.Id,
                Name = v.Name,
                GroupType = v.GroupType,
                Email = v.Email,
                Description = v.Description,
                Info = v.Info,
                Aliases = v.Aliases,
                UsersIds = v.UserIds,
                OwnerId = v.OwnerId,
                ManagersIds = v.ManagersIds,
                SubgroupIds = v.SubgroupIds,
                ActiveDirectoryId = v.ActiveDirectoryId,
                Timestamp = v.Timestamp,
            };
        }
    }
}
