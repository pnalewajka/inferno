﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("EmployeeAllocationFilter.LanguageWithLevelFilter")]
    [DescriptionLocalized("LanguageTitle", typeof(EmployeeResources))]
    public class LanguageWithLevelFilterViewModel
    {
        public LanguageWithLevelFilterViewModel()
        {
            LanguageIds = new long[0];
        }

        [DisplayNameLocalized(nameof(EmployeeResources.LanguageTitle), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(LanguageWithLevelPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] LanguageIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.LanguageTitle;
        }
    }
}
