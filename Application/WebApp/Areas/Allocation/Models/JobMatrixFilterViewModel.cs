﻿using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.JobMatrixFilter")]
    [DescriptionLocalized("EmployeeJobMatrixFilterDialogTitle", typeof(EmployeeResources))]
    public class JobMatrixFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixShortLabel), typeof(EmployeeResources))]
        [CheckboxGroup(ItemProvider = typeof (EnumBasedListItemProvider<JobMatrixLevels>))]
        [JsonConverter(typeof (ArrayToUrlConverter))]
        [Render(Size = Size.Large)]
        public long[] JobMatrixLevels { get; set; }

        public JobMatrixFilterViewModel()
        {
            JobMatrixLevels = new long[] { };
        }

        public override string ToString()
        {
            return EmployeeResources.EmployeeJobMatrixLabel;
        }
    }
}