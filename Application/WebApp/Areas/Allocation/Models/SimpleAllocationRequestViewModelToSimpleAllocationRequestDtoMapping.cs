﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SimpleAllocationRequestViewModelToSimpleAllocationRequestDtoMapping : ClassMapping<SimpleAllocationRequestViewModel, AllocationRequestDto>
    {
        public SimpleAllocationRequestViewModelToSimpleAllocationRequestDtoMapping()
        {
            Mapping = v => new AllocationRequestDto
            {
                Id = v.Id,
                ContentType = v.ContentType,
                EndDate = v.EndDate,
                StartDate = v.StartDate,
                EmployeeId = v.EmployeeId,
                ProjectId = v.ProjectId,
                Timestamp = v.Timestamp,
                FridayHours = v.HoursPerDay,
                MondayHours = v.HoursPerDay,
                ThursdayHours = v.HoursPerDay,
                TuesdayHours = v.HoursPerDay,
                WednesdayHours = v.HoursPerDay,
                EmployeeFirstName = v.EmployeeFirstName,
                EmployeeLastName = v.EmployeeLastName,
                ProjectName = v.ProjectCode
            };
        }
    }
}