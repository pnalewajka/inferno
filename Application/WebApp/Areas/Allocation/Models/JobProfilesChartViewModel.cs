﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.WebApp.Areas.Allocation.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class JobProfilesChartViewModel : IChartViewModel
    {
        private const int WeeksBeforeTodayCalculatedToNotAllocatedAverage = 3;

        public long? SelectedItemId { get; set; }

        public long? SelectedLocationId { get; set; }

        public long? SelectedLevel { get; set; }

        public DateTime CurrentWeek { get; set; }

        public IReadOnlyCollection<IChartDropDownElement> DropdownItems { get; set; }

        public IReadOnlyCollection<IChartDropDownElement> LocationDropDownItems { get; set; }

        public IReadOnlyCollection<IChartDropDownElement> LevelDropDownItems { get; set; }

        public IReadOnlyCollection<ChartDropDownViewModel> Charts { get; set; }

        public JobProfilesChartViewModel(IReadOnlyCollection<EmployeeAllocationPeriodDto> elements,
            IEnumerable<JobProfileDto> jobProfiles, IEnumerable<LocationDto> locations, JobMatrixLevels[] levels,
            DateTime currentWeek, bool collapse = true)
        {
            var jobProfilesList = new List<JobProfileDropDownElementViewModel>();

            foreach (var jobProfileDto in jobProfiles)
            {
                var dropDownElementViewModel = new JobProfileDropDownElementViewModel
                {
                    Id = jobProfileDto.Id,
                    Description = jobProfileDto.Description,
                    Name = jobProfileDto.Name,
                    Path = $"job-profile-{jobProfileDto.Id}"
                };

                jobProfilesList.Add(dropDownElementViewModel);
            }

            DropdownItems = jobProfilesList;

            LocationDropDownItems = locations.Select(l => new JobProfileDropDownElementViewModel
            {
                Id = l.Id,
                Name = l.Name,
                Path = $"job-profile-location-{l.Id}"
            }).ToList();

            LevelDropDownItems = levels.Select(l => new JobProfileDropDownElementViewModel
            {
                Id = (long) l,
                Description = l.GetDescription(),
                Name = l.GetDescription(),
                Path = $"job-profile-level-{(long) l}"
            }).ToList();

            CurrentWeek = currentWeek;
            var averagePeriodFrom = currentWeek.AddDays(WeeksBeforeTodayCalculatedToNotAllocatedAverage*-7);
            var averagePeriodTo = currentWeek;

            Charts = new List<ChartDropDownViewModel>
            {
                new ChartDropDownViewModel(jobProfilesList.FirstOrDefault(j => j.Id == SelectedItemId),
                    elements.Select(
                        v => new ChartEntryViewModel
                        {
                            Date = v.Week,
                            PlannedFTE = v.PlannedFTE,
                            AvailableFTE = v.AvailableFTE,
                            AllocatedFTE = v.AllocatedFTE,
                            NotAllocatedFTE = v.NotAllocatedFTE,
                            NotAllocatedPercentage = v.NotAllocatedPercentage
                        }),
                    elements.Where(v => v.Week >= averagePeriodFrom && v.Week <= averagePeriodTo)
                        .Average(v => v.NotAllocatedPercentage),
                    averagePeriodFrom,
                    averagePeriodTo
                    )
                {CurrentWeek = currentWeek}
            };
        }
    }
}