﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeAllocationViewModel
    {
        public class EmployeeAbsenceViewModel
        {
            public DateTime From { get; set; }

            public DateTime To { get; set; }

            public RequestStatus RequestStatus { get; set; }
        }

        public class ProjectAllocationViewModel
        {
            public decimal PercentageAllocation => AvailableHours > decimal.Zero
                ? (100 * (HoursAllocated / AvailableHours)) : 0;

            public decimal RoundedPercentageAllocation => decimal.Round(PercentageAllocation);

            public decimal HoursAllocated { get; set; }

            public decimal AvailableHours { get; set; }

            public string ProjectColor { get; set; }

            public string ProjectName { get; set; }

            public long ProjectId { get; set; }

            public bool HasOverAllocation => RoundedPercentageAllocation >= 101;

            public bool IsVacation { get; set; }

            public AllocationCertainty? Certainty { get; set; }

            /// <summary>
            /// Gets the info about the current utilization in a percentage form (week view) or 
            /// allocated hours/available hours form
            /// </summary>
            /// <param name="isPeriodTypeADay">Whether we are in a day view</param>
            /// <returns>The info about the current utilization in a percentage form (week view) or x/y form</returns>
            public string GetUtilizationInfo(bool isPeriodTypeADay)
            {
                return isPeriodTypeADay
                    ? $"{HoursAllocated:0.##}/{AvailableHours:0.##}"
                    : string.Format(EmployeeAllocation.ProjectPercentageAllocationFormat, RoundedPercentageAllocation);
            }
        }

        public class AbsenceBlockViewModel
        {
            public string AbsenceName { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            public long? RequestId { get; set; }

            public RequestStatus Status { get; set; }

            public string StatusName { get; set; }

            public decimal Hours { get; set; }
        }

        public class AllocationBlockViewModel
        {
            public long RequestId { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime? EndDate { get; set; }

            public string AllocationName { get; set; }

            public decimal[] AllocationDayHours { get; set; }

            public long ProjectId { get; set; }

            public string ProjectName { get; set; }

            public string ProjectShortName { get; set; }

            public string ProjectColor { get; set; }

            public string ProjectAcronym { get; set; }

            public bool ProjectColorIsDark { get; set; }

            public long? ProjectPictureId { get; set; }

            public string ClientShortName { get; set; }

            public string JiraIssueKey { get; set; }

            public string JiraKey { get; set; }

            public long EmployeeId { get; set; }

            public string EmployeeName { get; set; }

            public string EmployeeAcronym { get; set; }

            public AllocationCertainty Certainty { get; set; }

            public DateTime PeriodStartDate { get; set; }

            public DateTime PeriodEndDate { get; set; }

            public ProcessingStatus ProcessingStatus { get; set; }

            public AllocationRequestContentType ContentType { get; set; }
        }

        public class EmploymentPeriodBlockViewModel
        {
            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            public decimal[] ContractedHours { get; set; }
        }

        public class EmployeeAllocationSingleRecordViewModel
        {
            public string StatusDescription { get; set; }

            public AllocationStatus Status { get; set; }

            public decimal PercentageAllocation { get { return ProjectAllocations.Sum(p => p.PercentageAllocation); } }

            public decimal RoundedPercentageAllocation => decimal.Round(PercentageAllocation);

            public DateTime AllocationPeriod { get; set; }

            public decimal AvailableHours { get; set; }

            public ProjectAllocationViewModel[] ProjectAllocations { get; set; }

            public EmployeeAbsenceViewModel[] EmployeeAbsences { get; set; }

            public bool HasOverAllocation => RoundedPercentageAllocation >= 101;

            public bool IsEmploymentPeriodCurrentDay { get; set; }

            public bool IsEmploymentPeriodCurrentWeek { get; set; }

            public bool IsEmploymentPeriodLastWorkingWeekOfTheMonth { get; set; }

            public string GetEmployeeAbsencesTooltipText()
            {
                if (EmployeeAbsences.IsNullOrEmpty())
                {
                    return string.Empty;
                }

                return string.Join(Environment.NewLine, (EmployeeAbsences.Select(a => a.From == a.To
                    ? string.Format(EmployeeAllocation.EmployeeOneDayAbsenceDescription, AllocationRequestResources.AbsenceTaskDescription, a.From.ToShortDateString(), a.RequestStatus.GetDescription())
                    : string.Format(EmployeeAllocation.EmployeeAbsenceDescription, AllocationRequestResources.AbsenceTaskDescription, a.From.ToShortDateString(), a.To.ToShortDateString(), a.RequestStatus.GetDescription()))));
            }

            /// <summary>
            /// Gets formatted allocation period date depending on whether we want to display day or week view
            /// and also depending on browser culture
            /// </summary>
            /// <param name="isPeriodTypeADay">Whether the current view displays grid in day view</param>
            /// <param name="culture">Web browser culture like 'en-GB'</param>
            /// <returns>Formatted allocation period date</returns>
            public string GetAllocationPeriodShortDateString(bool isPeriodTypeADay, string culture)
            {
                return isPeriodTypeADay
                    ? AllocationPeriod.ToShortDateWithDayNameString(culture)
                    : AllocationPeriod.ToShortDateString();
            }

            /// <summary>
            /// Gets the info about the current utilization in a percentage form (week view) or 
            /// allocated hours/available hours form
            /// </summary>
            /// <param name="isPeriodTypeADay">Whether we are in a day view</param>
            /// <returns>The info about the current utilization in a percentage form (week view) or x/y form</returns>      
            public string GetUtilizationInfo(bool isPeriodTypeADay)
            {
                return isPeriodTypeADay
                    ? $"{ProjectAllocations.Sum(p => p.HoursAllocated):0.##}/{AvailableHours:0.##}"
                    : string.Format(EmployeeAllocation.ProjectPercentageAllocationFormat, RoundedPercentageAllocation);
            }
        }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public string Email { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? UserId { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long EmployeeId { get; set; }

        [Visibility(VisibilityScope.None)]
        public EmployeeAllocationSingleRecordViewModel[] Allocations { get; set; }

        [Visibility(VisibilityScope.None)]
        public AllocationBlockViewModel[] AllocationBlocks { get; set; }

        [Visibility(VisibilityScope.None)]
        public EmploymentPeriodBlockViewModel[] EmploymentPeriodBlocks { get; set; }

        [Visibility(VisibilityScope.None)]
        public AbsenceBlockViewModel[] Absences { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime[] Holidays { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasOverAllocation => Allocations != null && Allocations.Any(a => a.HasOverAllocation);

        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringLength(255)]
        [Render(GridCssClass = "show-legend")]
        [Hub(nameof(EmployeeId))]
        public string FullName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.None)]
        [StringLength(255)]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.None)]
        [StringLength(255)]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? LocationId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        public string LocationName { get; set; }


        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long? JobProfileId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string[] JobProfileNames { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [Ellipsis(MaxStringLength = 30)]
        public long[] SkillIds { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController))]
        public long? JobMatrixId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string[] JobMatrixLevels { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        public long? LineManagerId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string LineManagerName { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnitShort), typeof(EmployeeResources))]
        public long OrgUnitId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string OrgUnitName { get; set; }
    }
}
