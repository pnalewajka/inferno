﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class MyProfileViewModel
    {
        public long Id { get; set; }

        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        public string FirstName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.TitleLabel), typeof(EmployeeResources))]
        public LocalizedString Title { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeStatusLabel), nameof(EmployeeResources.EmployeeStatusShortLabel), typeof(EmployeeResources))]
        public string CurrentEmployeeStatus { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeEmailLabel), typeof(EmployeeResources))]
        public string Email { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        public string LocationName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        public string SkillNames { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        public string JobProfileNames { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeCompany), typeof(EmployeeResources))]
        public string CompanyName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeUser), typeof(EmployeeResources))]
        public string UserFullName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        public string LineManagerFullName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), nameof(EmployeeResources.EmployeeOrgUnitShort), typeof(EmployeeResources))]
        public string OrgUnitName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeIsProjectContributorLabel), typeof(EmployeeResources))]
        public bool IsProjectContributor { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeIsCoreAssetLabel), typeof(EmployeeResources))]
        public bool IsCoreAsset { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePictureLabel), typeof(EmployeeResources))]
        public string PictureUrl { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeEmailGroupsLabel), typeof(EmployeeResources))]
        public IDictionary<long, string> AdEmailGroupNames { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSecurityGroupsLabel), typeof(EmployeeResources))]
        public IDictionary<long, string> AdSecurityGroupNames { get; set; }

        public bool CanViewActiveDirectoryGroups { get; set; }

        public decimal AvailableVacations { get; set; }

        public decimal TotalVacations { get; set; }

        public decimal AvailableOvertime { get; set; }

        public MyProfileSurveyViewModel[] Surveys { get; set; }

        public long PendingRequestCount { get; set; }

        public string ChangePictureUrl { get; set; }

        public long? OwnedOrgUnitId { get; set; }
        
        public bool IsInOrgChart { get; set; }

        public string FullName
        {
            get { return $"{FirstName} {LastName}".Trim(); }
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}