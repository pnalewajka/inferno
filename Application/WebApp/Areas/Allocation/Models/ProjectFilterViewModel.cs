﻿namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using Newtonsoft.Json;
    using Presentation.Renderers.Bootstrap.Attributes;
    using Resources;
    using Smt.Atomic.CrossCutting.Common.Attributes;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [DescriptionLocalized("ProjectFilter", typeof(ProjectResources))]
    [Identifier("ProjectFilter.DateRangeFilter")]
    public class ProjectFilterViewModel : IValidatableObject
    {

        [DisplayNameLocalized(nameof(ProjectResources.From), typeof(ProjectResources))]
        [DataType(DataType.Date)]
        [Required]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(ProjectResources.To), typeof(ProjectResources))]
        [DataType(DataType.Date)]
        [Required]
        public DateTime To { get; set; }

        public override string ToString()
        {
            return string.Format(ProjectResources.FromTo, From.ToShortDateString(), To.ToShortDateString());
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (From > To)
            {
                issues.Add(new ValidationResult(ProjectResources.RangeFilterFromGreaterThanTo));
            }

            return issues;
        }
    }
}