﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("ViewModel.ProjectPresentationParameters")]
    public class ProjectReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.ProjectsLabel), typeof(ProjectResources))]
        [Required]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public long[] ProjectIds { get; set; }
    }
}
