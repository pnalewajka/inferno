﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.OrgUnitFilter")]
    [DescriptionLocalized("EmployeeOrgUnitFilterDialogTitle", typeof(EmployeeResources))]
    public class OrgUnitFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] OrgUnitIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeeOrgUnit;
        }
    }
}