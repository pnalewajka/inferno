﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectClientViewModelToProjectClientDtoMapping : ClassMapping<ProjectClientViewModel, ProjectClientDto>
    {
        public ProjectClientViewModelToProjectClientDtoMapping()
        {
            Mapping = v => new ProjectClientDto
            {
                ProjectId = v.Id,
                ClientName = v.ClientName
            };
        }
    }
}
