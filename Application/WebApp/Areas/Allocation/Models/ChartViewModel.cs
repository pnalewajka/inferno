﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ChartViewModel
    {
        public ChartViewModel(ChartOrgUnitViewModel orgUnit, IEnumerable<ChartEntryViewModel> chartEntries)
        {
            Elements = chartEntries.ToList();
            OrgUnitViewModel = orgUnit;
        }

        public DateTime CurrentWeek { get; set; }

        public ChartOrgUnitViewModel OrgUnitViewModel { get; set; }

        public IReadOnlyCollection<ChartEntryViewModel> Elements { get; set; } 
    }
}