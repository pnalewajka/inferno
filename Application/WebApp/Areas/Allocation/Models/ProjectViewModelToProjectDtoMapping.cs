﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectViewModelToProjectDtoMapping : ClassMapping<ProjectViewModel, ProjectDto>
    {
        public ProjectViewModelToProjectDtoMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = v => new ProjectDto
            {
                Id = v.Id,
                SetupId = v.SetupId,
                IsMainProject = v.IsMainProject,
                JiraIssueKey = v.JiraIssueKey,
                JiraKey = v.JiraKey,
                PetsCode = v.PetsCode,
                Apn = v.Apn,
                KupferwerkProjectId = v.KupferwerkProjectId,
                ProjectType = v.ProjectType,
                ProjectFeatures = v.ProjectFeatures,
                ClientShortName = v.ClientShortName,
                ProjectShortName = v.ProjectShortName,
                Description = v.Description,
                OrgUnitId = v.OrgUnitId,
                OrgUnitName = v.OrgUnitName,
                ProjectManagerId = v.ProjectManagerId,
                SupervisorManagerId = v.SupervisorManagerId,
                SalesAccountManagerId = v.SalesAccountManagerId,
                ProjectManagerFullName = v.ProjectManagerFullName,
                SupervisorFullName = v.SupervisorFullName,
                StartDate = v.StartDate,
                EndDate = v.EndDate,
                Region = v.Region,
                ProjectStatus = v.ProjectStatus,
                CalendarId = v.CalendarId,
                Color = v.Color,
                GenericName = v.GenericName,
                Timestamp = v.Timestamp,
                IndustryIds = v.IndustryIds != null && v.IndustryIds.Length > 0 ? v.IndustryIds : new long[] { },
                IsAvailableForSalesPresentation = v.IsAvailableForSalesPresentation,
                TechnologyIds = v.TechnologyIds != null && v.TechnologyIds.Length > 0 ? v.TechnologyIds : new long[] { },
                SoftwareCategoryIds = v.SoftwareCategoryIds != null && v.SoftwareCategoryIds.Length > 0 ? v.SoftwareCategoryIds : new long[] { },
                TagIds = v.TagIds != null && v.TagIds.Length > 0 ? v.TagIds : new long[] { },
                Picture = v.Picture == null
                    ? null
                    : new DocumentDto
                    {
                        ContentType = v.Picture.ContentType,
                        DocumentName = v.Picture.DocumentName,
                        DocumentId = v.Picture.DocumentId,
                        TemporaryDocumentId = v.Picture.TemporaryDocumentId,
                    },
                ServiceLineIds = v.ServiceLineIds != null && v.ServiceLineIds.Length > 0 ? v.ServiceLineIds : new long[] { },
                CurrentPhase = v.CurrentPhase,
                Disclosures = v.Disclosures,
                HasClientReferences = v.HasClientReferences,
                CustomerSizeId = v.CustomerSizeId,
                BusinessCase = v.BusinessCase,
                ReferenceMaterialAttachments = v.ReferenceMaterialAttachments == null ? new List<DocumentDto>() : v.ReferenceMaterialAttachments.Select(
                    p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    }).ToList(),
                ProjectInformationAttachments = v.ProjectInformationAttachments == null ? new List<DocumentDto>() : v.ProjectInformationAttachments.Select(
                    p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    }).ToList(),
                OtherAttachments = v.OtherAttachments == null ? new List<DocumentDto>() : v.OtherAttachments.Select(
                    p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    }).ToList(),
                TimeTrackingImportSourceId = v.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = v.AllowedOvertimeTypes,
                TimeReportAcceptingPersonIds = v.TimeReportAcceptingPersonIds,
                IsApprovedByRequesterMainManager = v.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = v.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = v.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = v.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = v.UtilizationCategoryId,
                AttributesIds = v.Attributes.ToArray(),
                ReportingStartsOn = v.ReportingStartsOn,
                ReinvoicingToCustomer = v.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = v.ReinvoiceCurrencyId,
                CostApprovalPolicy = v.CostApprovalPolicy,
                MaxCostsCurrencyId = v.MaxCostsCurrencyId,
                MaxHotelCosts = v.MaxHotelCosts,
                MaxTotalFlightsCosts = v.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = v.MaxOtherTravelCosts,
                InvoicingRates = v.InvoicingRates == null ? new ProjectInvoicingRateDto[0] : v.InvoicingRates.Select(
                    mappingFactory.CreateMapping<ProjectInvoicingRateViewModel, ProjectInvoicingRateDto>().CreateFromSource).ToArray(),
                InvoicingAlgorithm = v.InvoicingAlgorithm,
                Acronym = v.Acronym,
                AllowedBonusTypes = v.AllowedBonusTypes,
            };
        }
    }
}
