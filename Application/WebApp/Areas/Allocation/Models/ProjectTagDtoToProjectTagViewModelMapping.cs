﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectTagDtoToProjectTagViewModelMapping : ClassMapping<ProjectTagDto, ProjectTagViewModel>
    {
        public ProjectTagDtoToProjectTagViewModelMapping()
        {
            Mapping = d => new ProjectTagViewModel
            {
                Id = d.Id, Name = d.Name,
                Description = d.Description,
                TagType = d.TagType,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
