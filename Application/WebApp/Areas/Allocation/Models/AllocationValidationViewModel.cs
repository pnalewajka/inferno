﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationValidationViewModel
    {
        public AllocationRequestViewModel ViewModel { get; set; }

        public IReadOnlyCollection<OverAllocatedWarningViewModel> Warnings { get; set; }

        public Action<FormRenderingModel> FormCustomization { get; set; }
    }
}