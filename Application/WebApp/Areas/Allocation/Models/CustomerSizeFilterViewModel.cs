﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.CustomerSizeFilter")]
    [DescriptionLocalized("CustomerSizeFilterDialogTitle", typeof(ProjectResources))]
    public class CustomerSizeFilterViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.CustomerSizeLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(CustomerSizeController))]
        public long CustomerSizeId { get; set; }

        public override string ToString()
        {
            return ProjectResources.CustomerSizeLabel;
        }
    }
}