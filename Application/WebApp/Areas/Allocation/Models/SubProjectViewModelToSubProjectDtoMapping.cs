﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SubProjectViewModelToSubProjectDtoMapping
        : ClassMapping<SubProjectViewModel, SubProjectDto>
    {
        public SubProjectViewModelToSubProjectDtoMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = v => new SubProjectDto
            {
                Id = v.Id,
                SetupId = v.SetupId,
                IsMainProject = v.IsMainProject,
                JiraIssueKey = v.JiraIssueKey,
                JiraKey = v.JiraKey,
                PetsCode = v.PetsCode,
                Apn = v.Apn,
                KupferwerkProjectId = v.KupferwerkProjectId,
                ProjectShortName = v.ProjectShortName,
                Description = v.Description,
                StartDate = v.StartDate,
                EndDate = v.EndDate,
                TimeTrackingImportSourceId = v.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = v.AllowedOvertimeTypes,
                TimeReportAcceptingPersonIds = v.TimeReportAcceptingPersonIds,
                IsApprovedByRequesterMainManager = v.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = v.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = v.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = v.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = v.UtilizationCategoryId,
                AttributesIds = v.Attributes.ToArray(),
                ReportingStartsOn = v.ReportingStartsOn,
                Acronym = v.Acronym,
                AllowedBonusTypes = v.AllowedBonusTypes,
            };
        }
    }
}
