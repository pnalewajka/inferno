﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmploymentPeriodDateFilterDtoToEmploymentPeriodDateFilterViewModelMapping : ClassMapping<EmploymentPeriodDateFilterDto, EmploymentPeriodDateFilterViewModel>
    {
        public EmploymentPeriodDateFilterDtoToEmploymentPeriodDateFilterViewModelMapping()
        {
            Mapping = d => new EmploymentPeriodDateFilterViewModel
            {
                From = d.From,
                To = d.To
            };
        }
    }
}
