﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmploymentPeriodDtoToEmploymentPeriodViewModelMapping : ClassMapping<EmploymentPeriodDto, EmploymentPeriodViewModel>
    {
        public EmploymentPeriodDtoToEmploymentPeriodViewModelMapping()
        {
            Mapping = d => new EmploymentPeriodViewModel
            {
                Id = d.Id,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                SignedOn = d.SignedOn,
                Employee = d.EmployeeId,
                EmploymentType = d.EmploymentType,
                CompanyId = d.CompanyId,
                JobTitleId = d.JobTitleId,
                IsActive = d.IsActive,
                InactivityReason = d.InactivityReason,
                NoticePeriod = d.NoticePeriod,
                MondayHours = d.MondayHours,
                TuesdayHours = d.TuesdayHours,
                WednesdayHours = d.WednesdayHours,
                ThursdayHours = d.ThursdayHours,
                FridayHours = d.FridayHours,
                SaturdayHours = d.SaturdayHours,
                SundayHours = d.SundayHours,
                ContractTypeId = d.ContractTypeId,
                ContractDuration = d.ContractDuration,
                CalendarId = d.CalendarId,
                VacationHourToDayRatio = d.VacationHourToDayRatio,
                TimeReportingMode = d.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = d.AbsenceOnlyDefaultProjectId,
                IsClockCardRequired = d.IsClockCardRequired,
                CompensationCurrencyId = d.CompensationCurrencyId,
                ContractorCompanyName = d.ContractorCompanyName,
                ContractorCompanyTaxIdentificationNumber = d.ContractorCompanyTaxIdentificationNumber,
                ContractorCompanyStreetAddress = d.ContractorCompanyStreetAddress,
                ContractorCompanyCity = d.ContractorCompanyCity,
                ContractorCompanyZipCode = d.ContractorCompanyZipCode,
                HourlyRate = d.HourlyRate,
                HourlyRateDomesticOutOfOfficeBonus = d.HourlyRateDomesticOutOfOfficeBonus,
                HourlyRateForeignOutOfOfficeBonus = d.HourlyRateForeignOutOfOfficeBonus,
                MonthlyRate = d.MonthlyRate,
                SalaryGross = d.SalaryGross,
                AuthorRemunerationRatio = d.AuthorRemunerationRatio,
                Timestamp = d.Timestamp,
                TerminatedOn = d.TerminatedOn,
                ChangeExistingAllocations = d.ChangeExistingAllocations,
                ClosePreviousPeriod = d.ClosePreviousPeriod,
                TerminationReasonId = d.TerminationReasonId,
                LocationId = d.LocationId
            };
        }
    }
}
