﻿using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SkillChangeRequestViewModel
    {
        [DisplayNameLocalized(nameof(SkillChangeRequestResources.SkillChangeRequestEmployeeIdLabel), typeof(SkillChangeRequestResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [UpdatesApprovers]
        public long EmployeeId { get; set; }

        [DisplayNameLocalized(nameof(SkillChangeRequestResources.SkillChangeRequestSkillIdsLabel), typeof(SkillChangeRequestResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        public long[] SkillIds { get; set; }
    }
}
