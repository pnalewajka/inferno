﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmploymentPeriodViewModelToEmploymentPeriodDtoMapping : ClassMapping<EmploymentPeriodViewModel, EmploymentPeriodDto>
    {
        public EmploymentPeriodViewModelToEmploymentPeriodDtoMapping()
        {
            Mapping = v => new EmploymentPeriodDto
            {
                Id = v.Id,
                StartDate = v.StartDate ?? new DateTime(),
                EndDate = v.EndDate,
                SignedOn = v.SignedOn ?? new DateTime(),
                EmployeeId = v.Employee,
                CompanyId = v.CompanyId,
                JobTitleId = v.JobTitleId,
                IsActive = v.IsActive,
                InactivityReason = v.InactivityReason,
                NoticePeriod = v.NoticePeriod,
                MondayHours = v.MondayHours,
                TuesdayHours = v.TuesdayHours,
                WednesdayHours = v.WednesdayHours,
                ThursdayHours = v.ThursdayHours,
                FridayHours = v.FridayHours,
                SaturdayHours = v.SaturdayHours,
                SundayHours = v.SundayHours,
                ContractTypeId = v.ContractTypeId,
                ContractDuration = v.ContractDuration,
                CalendarId = v.CalendarId,
                VacationHourToDayRatio = v.VacationHourToDayRatio,
                TimeReportingMode = v.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = v.TimeReportingMode == TimeReportingMode.AbsenceOnly ? v.AbsenceOnlyDefaultProjectId : null,
                IsClockCardRequired = v.IsClockCardRequired,
                CompensationCurrencyId = v.CompensationCurrencyId,
                ContractorCompanyName = v.ContractorCompanyName,
                ContractorCompanyTaxIdentificationNumber = v.ContractorCompanyTaxIdentificationNumber,
                ContractorCompanyStreetAddress = v.ContractorCompanyStreetAddress,
                ContractorCompanyCity = v.ContractorCompanyCity,
                ContractorCompanyZipCode = v.ContractorCompanyZipCode,
                HourlyRate = v.HourlyRate,
                HourlyRateDomesticOutOfOfficeBonus = v.HourlyRateDomesticOutOfOfficeBonus,
                HourlyRateForeignOutOfOfficeBonus = v.HourlyRateForeignOutOfOfficeBonus,
                MonthlyRate = v.MonthlyRate,
                SalaryGross = v.SalaryGross,
                AuthorRemunerationRatio = v.AuthorRemunerationRatio,
                Timestamp = v.Timestamp,
                TerminatedOn = v.TerminatedOn,
                ChangeExistingAllocations = v.ChangeExistingAllocations,
                ClosePreviousPeriod = v.ClosePreviousPeriod,
                TerminationReasonId = v.TerminationReasonId,
                LocationId = v.LocationId
            };
        }
    }
}
