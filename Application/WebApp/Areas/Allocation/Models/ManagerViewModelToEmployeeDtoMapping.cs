﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ManagerViewModelToEmployeeDtoMapping : ClassMapping<ManagerViewModel, EmployeeDto>
    {
        public ManagerViewModelToEmployeeDtoMapping()
        {
            Mapping = v => new EmployeeDto
            {
                Id = v.Id,
                FirstName = v.FirstName,
                LastName = v.LastName,
                JobTitleId = v.JobTitleId,
                JobTitleName = v.JobTitleName
            };
        }
    }
}
