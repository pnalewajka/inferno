﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeDtoToManagerViewModelMapping : ClassMapping<EmployeeDto, ManagerViewModel>
    {
        public EmployeeDtoToManagerViewModelMapping()
        {
            Mapping = d => new ManagerViewModel
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                JobTitleId = d.JobTitleId,
                JobTitleName = d.JobTitleName
            };
        }
    }
}
