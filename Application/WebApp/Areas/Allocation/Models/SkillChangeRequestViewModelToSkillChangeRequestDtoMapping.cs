﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SkillChangeRequestViewModelToSkillChangeRequestDtoMapping : ClassMapping<SkillChangeRequestViewModel, SkillChangeRequestDto>
    {
        public SkillChangeRequestViewModelToSkillChangeRequestDtoMapping()
        {
            Mapping = v => new SkillChangeRequestDto
            {
                EmployeeId = v.EmployeeId,
                SkillIds = v.SkillIds
            };
        }
    }
}
