﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.DocumentMappings;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Configuration.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [TabDescription(0, "basic", nameof(ProjectResources.BasicInformationTabName), typeof(ProjectResources))]
    [TabDescription(1, "management", nameof(ProjectResources.ManagementTabName), typeof(ProjectResources))]
    [TabDescription(2, "scheduling", nameof(ProjectResources.SchedulingTabName), typeof(ProjectResources))]
    [TabDescription(3, "presentation", nameof(ProjectResources.PresentationDetailsTabName), typeof(ProjectResources))]
    [TabDescription(4, "salesInformation", nameof(ProjectResources.SalesInformation), typeof(ProjectResources))]
    [TabDescription(5, "timetracking", nameof(ProjectResources.TimeTracking), typeof(ProjectResources))]
    [TabDescription(6, "attachments", nameof(ProjectResources.AttachmentTabName), typeof(ProjectResources))]
    [TabDescription(7, "acceptanceConditions", nameof(ProjectResources.AcceptanceConditions), typeof(ProjectResources))]
    [TabDescription(8, "invoicing", nameof(ProjectResources.Invoicing), typeof(ProjectResources))]
    [FieldSetDescription(1, "import", nameof(ProjectResources.ImportTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(2, "approver", nameof(ProjectResources.ApproverTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(3, "overtime", nameof(ProjectResources.OvertimeTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(4, "config", nameof(ProjectResources.ConfigTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(5, "standard", nameof(ProjectResources.StandardFieldset), typeof(ProjectResources))]
    [FieldSetDescription(6, "reinvoice", nameof(ProjectResources.ReinvoiceFieldset), typeof(ProjectResources))]
    [Identifier("Models.ProjectViewModel")]
    public class ProjectViewModel : SubProjectViewModel, IValidatableObject
    {
        [Visibility(VisibilityScope.None)]
        public override string ClientProjectName { get; set; }

        [Tab("basic")]
        [StringLength(128)]
        [Order(1)]
        [Required]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ReadOnly(false)]
        public override string ClientShortName { get; set; }

        [Tab("basic")]
        [StringLength(128)]
        [Order(2)]
        [Required]
        [Visibility(VisibilityScope.FormAndGrid)]
        public override string ProjectShortName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.None)]
        public override string ProjectName { get; set; }

        [Tab("basic")]
        [Order(42)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(ProjectResources.RegionLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(60)]
        public string Region { get; set; }

        [Tab("basic")]
        [Order(7)]
        [ReadOnly(true)]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<ProjectType>))]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectType), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public ProjectType? ProjectType { get; set; }

        [Tab("basic")]
        [Order(8)]
        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectFeatures), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<ProjectFeatures>))]
        public ProjectFeatures ProjectFeatures { get; set; }

        [Tab("basic")]
        [Order(9)]
        [Multiline(4)]
        [Ellipsis(MaxStringLength = 180)]
        [Required]
        public override string Description { get; set; }

        [Tab("management")]
        [Order(10)]
        [DisplayNameLocalized(nameof(ProjectResources.SalesAccountManagerLabel), nameof(ProjectResources.SalesAccountManagerLabelShort), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(ManagerPickerController))]
        public long? SalesAccountManagerId { get; set; }

        [Tab("management")]
        [Order(11)]
        [DisplayNameLocalized(nameof(ProjectResources.SupervisorManagerLabel), nameof(ProjectResources.SupervisorManagerLabelShort), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(ManagerPickerController))]
        public long? SupervisorManagerId { get; set; }

        [Tab("management")]
        [Order(12)]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectManagerLabel), nameof(ProjectResources.ProjectManagerLabelShort), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsProjectManager) + "'")]
        public long? ProjectManagerId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ProjectManagerFullName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string SupervisorFullName { get; set; }

        [Tab("management")]
        [Order(13)]
        [Required]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(ProjectResources.OrgUnitLabel), nameof(ProjectResources.OrgUnitLabelShort), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long OrgUnitId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string OrgUnitName { get; set; }

        [Tab("salesInformation")]
        [Order(24)]
        [DisplayNameLocalized(nameof(DictionaryResources.IndustryTitle), typeof(DictionaryResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(IndustryController))]
        public long[] IndustryIds { get; set; }

        [Tab("scheduling")]
        [Order(15)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectStatusLabel), nameof(ProjectResources.ProjectStatusLabelShort), typeof(ProjectResources))]
        public ProjectStatus ProjectStatus { get; set; }

        [Tab("scheduling")]
        [Order(16)]
        [AllowBulkEdit]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CalendarPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.CalendarLabel), typeof(ProjectResources))]
        public long? CalendarId { get; set; }

        [Tab("presentation")]
        [Order(17)]
        [StringLength(255)]
        [ColorPicker]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ProjectResources.ColorLabel), typeof(ProjectResources))]
        public string Color { get; set; }

        [Tab("presentation")]
        [Order(18)]
        [StringLength(255)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ProjectResources.GenericNameLabel), typeof(ProjectResources))]
        public string GenericName { get; set; }

        [Tab("salesInformation")]
        [Order(30)]
        [DisplayNameLocalized(nameof(ProjectResources.IsAvailableForSalesPresentationLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public bool IsAvailableForSalesPresentation { get; set; }

        [Tab("salesInformation")]
        [Order(25)]
        [DisplayNameLocalized(nameof(ProjectResources.KeyTechnologiesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(SkillTagPickerController))]
        [ReadOnly(true)]
        public long[] KeyTechnologyIds { get; set; }

        [Tab("salesInformation")]
        [Order(26)]
        [DisplayNameLocalized(nameof(ProjectResources.TechnologiesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(SkillPickerController))]
        public long[] TechnologyIds { get; set; }

        [Tab("salesInformation")]
        [Order(21)]
        [DisplayNameLocalized(nameof(ProjectResources.ServiceLinesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(ServiceLineController))]
        public long[] ServiceLineIds { get; set; }

        [Tab("salesInformation")]
        [Order(22)]
        [DisplayNameLocalized(nameof(ProjectResources.SoftwareCategoriesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(SoftwareCategoryPickerController))]
        public long[] SoftwareCategoryIds { get; set; }

        [Tab("salesInformation")]
        [Order(23)]
        [DisplayNameLocalized(nameof(ProjectResources.TagsLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [TypeAhead(Type = typeof(ProjectTagPickerController))]
        public long[] TagIds { get; set; }

        [Tab("salesInformation")]
        [Order(19)]
        [DisplayNameLocalized(nameof(ProjectResources.PictureLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [CropImageUpload(typeof(ProjectPictureMapping), 500, 500, 500, 500, true)]
        public DocumentViewModel Picture { get; set; }

        [Tab("salesInformation")]
        [Order(20)]
        [DisplayNameLocalized(nameof(ProjectResources.CurrentPhaseLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<ProjectPhase>))]
        public ProjectPhase CurrentPhase { get; set; }

        [Tab("salesInformation")]
        [Order(28)]
        [DisplayNameLocalized(nameof(ProjectResources.DisclosuresLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<ProjectDisclosures>))]
        public ProjectDisclosures Disclosures { get; set; }

        [Tab("salesInformation")]
        [Order(29)]
        [DisplayNameLocalized(nameof(ProjectResources.HasClientReferencesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public bool HasClientReferences { get; set; }

        [Tab("salesInformation")]
        [Order(26)]
        [DisplayNameLocalized(nameof(ProjectResources.CustomerSizeLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CustomerSizeController))]
        public long? CustomerSizeId { get; set; }

        [Tab("salesInformation")]
        [Order(27)]
        [DisplayNameLocalized(nameof(ProjectResources.BusinessCaseLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [Multiline(4)]
        public string BusinessCase { get; set; }

        [Order(28)]
        [DisplayNameLocalized(nameof(ProjectResources.JiraLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Grid)]
        public bool IsJiraProject { get; set; }

        [Tab("attachments")]
        [Order(30)]
        [DisplayNameLocalized(nameof(ProjectResources.ReferenceMaterialAttachmentLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [DocumentUpload(typeof(ProjectAttachmentMapping))]
        public List<DocumentViewModel> ReferenceMaterialAttachments { get; set; }

        [Tab("attachments")]
        [Order(31)]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectInformationAttachmentLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [DocumentUpload(typeof(ProjectAttachmentMapping))]
        public List<DocumentViewModel> ProjectInformationAttachments { get; set; }

        [Tab("attachments")]
        [Order(32)]
        [DisplayNameLocalized(nameof(ProjectResources.OtherAttachmentLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [DocumentUpload(typeof(ProjectAttachmentMapping))]
        public List<DocumentViewModel> OtherAttachments { get; set; }

        [Tab("acceptanceConditions")]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.ReinvoiceCurrency), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("reinvoice")]
        public long? ReinvoiceCurrencyId { get; set; }

        [Tab("acceptanceConditions")]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.MaxCostsCurrency), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("standard")]
        public long? MaxCostsCurrencyId { get; set; }

        [Tab("acceptanceConditions")]
        [DisplayNameLocalized(nameof(ProjectResources.CostApprovalPolicy), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("standard")]
        [OnValueChange(ClientHandler = "AcceptanceConditionsHandler.updateAcceptanceFunctions")]
        public CostApprovalPolicy CostApprovalPolicy { get; set; }

        [Tab("acceptanceConditions")]
        [DisplayNameLocalized(nameof(ProjectResources.MaxHotelCosts), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("standard")]
        public decimal? MaxHotelCosts { get; set; }

        [Tab("acceptanceConditions")]
        [DisplayNameLocalized(nameof(ProjectResources.MaxTotalFlightsCosts), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("standard")]
        public decimal? MaxTotalFlightsCosts { get; set; }

        [Tab("acceptanceConditions")]
        [DisplayNameLocalized(nameof(ProjectResources.MaxOtherTravelCosts), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("standard")]
        public decimal? MaxOtherTravelCosts { get; set; }

        [Tab("acceptanceConditions")]
        [DisplayNameLocalized(nameof(ProjectResources.ReinvoicingToCustomer), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("reinvoice")]
        [OnValueChange(ClientHandler = "AcceptanceConditionsHandler.updateAcceptanceFunctions")]
        public bool ReinvoicingToCustomer { get; set; }

        [Tab("invoicing")]
        [DisplayNameLocalized(nameof(ProjectResources.InvoicingRates), typeof(ProjectResources))]
        [Repeater(CanAddRecord = true, CanDeleteRecord = true, CanReorder = true)]
        [Visibility(VisibilityScope.Form)]
        public List<ProjectInvoicingRateViewModel> InvoicingRates { get; set; }

        [Tab("invoicing")]
        [AllowHtml]
        [DisplayNameLocalized(nameof(ProjectResources.InvoicingAlgorithm), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [CodeEditor(SyntaxType.TypeScript)]
        public string InvoicingAlgorithm { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return ClientProjectName;
        }

        public ProjectViewModel()
        {
            ReferenceMaterialAttachments = new List<DocumentViewModel>();
            ProjectInformationAttachments = new List<DocumentViewModel>();
            OtherAttachments = new List<DocumentViewModel>();
            InvoicingRates = new List<ProjectInvoicingRateViewModel>();
            IndustryIds = new long[0];
            TechnologyIds = new long[0];
            KeyTechnologyIds = new long[0];
            SoftwareCategoryIds = new long[0];
            TagIds = new long[0];
            ServiceLineIds = new long[0];
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = base.Validate(validationContext).ToList();

            if (GenericName == ProjectShortName)
            {
                validationResults.Add(new ValidationResult(ProjectResources.GenericNameEqualToName));
            }

            return validationResults;
        }
    }
}
