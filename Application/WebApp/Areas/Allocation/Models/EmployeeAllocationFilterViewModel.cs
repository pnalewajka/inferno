﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Presentation.Common.Converters;
    using Presentation.Renderers.Bootstrap.Attributes;
    using Resources;
    using System.ComponentModel.DataAnnotations;
    using Smt.Atomic.CrossCutting.Common.Attributes;
    using System;
    
    [DescriptionLocalized("EmployeeAllocationFilter", typeof(AllocationResources))]
    [Identifier("EmployeeAllocationFilter.DateRangeFilter")]
    public class EmployeeAllocationFilterViewModel : IValidatableObject
    {


        [DisplayNameLocalized(nameof(AllocationResources.From), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(AllocationResources.To), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime To { get; set; }

        public override string ToString()
        {
            return string.Format(AllocationResources.FromTo, From.ToShortDateString(), To.ToShortDateString());
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (From > To)
            {
                issues.Add(new ValidationResult(AllocationResources.RangeFilterFromGreaterThanTo));
            }

            return issues;
        }
    }
}