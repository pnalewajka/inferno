﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Configuration.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [TabDescription(0, "basic", nameof(EmployeeResources.EmploymentTabName), typeof(EmployeeResources))]
    [TabDescription(1, "worktime", nameof(EmployeeResources.WorkingHoursTabName), typeof(EmployeeResources))]
    [TabDescription(2, "timetracking", nameof(EmployeeResources.TimeTrackingTabName), typeof(EmployeeResources))]
    [TabDescription(3, "compensation", nameof(EmployeeResources.CompensationTabName), typeof(EmployeeResources))]
    public class EmploymentPeriodViewModel : IValidatableObject
    {
        private const string EmploymentPeriodControllerPath = "/Allocation/EmploymentPeriod/";

        [Tab("basic")]
        [Required]
        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentStartDate), typeof(EmployeeResources))]
        public DateTime? StartDate { get; set; }

        [Tab("basic")]
        [StringFormat("{0:d}")]
        [NonSortable]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentEndDate), typeof(EmployeeResources))]
        public DateTime? EndDate { get; set; }

        [Tab("basic")]
        [Required]
        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentSignedOnDate), nameof(EmployeeResources.EmploymentSignedOnDateShort), typeof(EmployeeResources))]
        public DateTime? SignedOn { get; set; }

        [Tab("basic")]
        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentTerminatedOnDate), nameof(EmployeeResources.EmploymentTerminatedOnDateShort), typeof(EmployeeResources))]
        public DateTime? TerminatedOn { get; set; }

        [Tab("basic")]
        [DisplayNameLocalized(nameof(EmployeeResources.ChangeExistingAllocations), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public bool ChangeExistingAllocations { get; set; }

        [Tab("basic")]
        [DisplayNameLocalized(nameof(EmployeeResources.AutomaticallyClosePreviousPeriod), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public bool ClosePreviousPeriod { get; set; }

        [Tab("basic")]
        [Required]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long Employee { get; set; }

        [Tab("basic")]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? LocationId { get; set; }

        [Tab("basic")]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeCompany), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        public long? CompanyId { get; set; }


        [Tab("basic")]
        [DisplayNameLocalized(nameof(EmployeeResources.TitleLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        public long? JobTitleId { get; set; }

        [Tab("basic")]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodIsActive), typeof(EmployeeResources))]
        public bool IsActive { get; set; }

        [Tab("basic")]
        [Required]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodInactivityReason), typeof(EmployeeResources))]
        public EmployeeInactivityReason InactivityReason { get; set; }

        [Tab("basic")]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodTerminationReason), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmploymentTerminationReasonPickerController))]
        public long? TerminationReasonId { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeStatus), typeof(EmployeeResources))]
        public string StatusWithReason
        {
            get
            {
                return IsActive
                    ? EmployeeResources.EmploymentPeriodIsActiveStatus
                    : $"{EmployeeResources.EmploymentPeriodIsInactiveStatus} ({InactivityReason.GetDescription()})";
            }
        }

        [Tab("basic")]
        [Required]
        [NonSortable]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodNoticePeriod), nameof(EmployeeResources.EmploymentPeriodNoticePeriodShort), typeof(EmployeeResources))]
        public NoticePeriod? NoticePeriod { get; set; }

        [Tab("basic")]
        [Required]
        [Render(Size = Size.Medium)]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<ContractDurationType>), EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractDuration), typeof(EmployeeResources))]
        public ContractDurationType? ContractDuration { get; set; }

        [Tab("basic")]
        [NonSortable]
        [ValuePicker(Type = typeof(CalendarPickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodCalendar), typeof(EmployeeResources))]
        public long? CalendarId { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentMondayHours), nameof(EmployeeResources.EmploymentMondayHoursShort), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal MondayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentTuesdayHours), nameof(EmployeeResources.EmploymentTuesdayHoursShort), typeof(EmployeeResources))]
        [Precision(0)]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal TuesdayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentWednesdayHours), nameof(EmployeeResources.EmploymentWednesdayHoursShort), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal WednesdayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentThursdayHours), nameof(EmployeeResources.EmploymentThursdayHoursShort), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal ThursdayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentFridayHours), nameof(EmployeeResources.EmploymentFridayHoursShort), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal FridayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentSaturdayHours), nameof(EmployeeResources.EmploymentSaturdayHoursShort), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal SaturdayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentSundayHours), nameof(EmployeeResources.EmploymentSundayHoursShort), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal SundayHours { get; set; }

        [Tab("worktime")]
        [Required]
        [NonSortable]
        [Precision(0)]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodVacationHourToDayRatio), typeof(EmployeeResources))]
        [Range(0, 24, ErrorMessageResourceName = "WorkingHoursRangeValidationMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public decimal VacationHourToDayRatio { get; set; }

        [Tab("timetracking")]
        [Required]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodTimeReporting), typeof(EmployeeResources))]
        public TimeReportingMode TimeReportingMode { get; set; }

        [Tab("timetracking")]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(SimpleProjectPickerController))]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodAbsenceOnlyDefaultProject), typeof(EmployeeResources))]
        public long? AbsenceOnlyDefaultProjectId { get; set; }

        [Tab("timetracking")]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodIsClockCardRequired), typeof(EmployeeResources))]
        public bool IsClockCardRequired { get; set; }

        [Tab("compensation")]
        [Required]
        [NonSortable]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [OnValueChange(ServerHandler = EmploymentPeriodControllerPath + nameof(EmploymentPeriodController.UpdateCompensationParametersVisibility), ExecuteOnLoad = true)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractType), typeof(EmployeeResources))]
        public long ContractTypeId { get; set; }

        [Tab("compensation")]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodCompensationCurrency), typeof(EmployeeResources))]
        public long? CompensationCurrencyId { get; set; }

        [Tab("compensation")]
        [StringLength(256)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.HourlyRateBasedPL,
            TimeTrackingCompensationCalculators.HourlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedPL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimeAndSickLeavePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractorCompanyName), typeof(EmployeeResources))]
        public string ContractorCompanyName { get; set; }

        [Tab("compensation")]
        [StringLength(64)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.HourlyRateBasedPL,
            TimeTrackingCompensationCalculators.HourlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedPL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimeAndSickLeavePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractorCompanyTaxIdentificationNumber), typeof(EmployeeResources))]
        public string ContractorCompanyTaxIdentificationNumber { get; set; }

        [Tab("compensation")]
        [StringLength(256)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
         [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.HourlyRateBasedPL,
            TimeTrackingCompensationCalculators.HourlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedPL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimeAndSickLeavePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractorCompanyStreetAddress), typeof(EmployeeResources))]
        public string ContractorCompanyStreetAddress { get; set; }

        [Tab("compensation")]
        [StringLength(128)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.HourlyRateBasedPL,
            TimeTrackingCompensationCalculators.HourlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedPL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimeAndSickLeavePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractorCompanyCity), typeof(EmployeeResources))]
        public string ContractorCompanyCity { get; set; }

        [Tab("compensation")]
        [StringLength(16)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.HourlyRateBasedPL,
            TimeTrackingCompensationCalculators.HourlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedPL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimeAndSickLeavePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractorCompanyZipCode), typeof(EmployeeResources))]
        public string ContractorCompanyZipCode { get; set; }

        [Tab("compensation")]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.HourlyRateBasedPL,
            TimeTrackingCompensationCalculators.HourlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.HourlyRateBasedDE)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodHourlyRate), typeof(EmployeeResources))]
        public decimal? HourlyRate { get; set; }

        [Tab("compensation")]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(BusinessTripCompensationCalculators.HourlyRateBasedPL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodHourlyRateDomesticOutOfOfficeBonus), typeof(EmployeeResources))]
        public decimal? HourlyRateDomesticOutOfOfficeBonus { get; set; }

        [Tab("compensation")]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(BusinessTripCompensationCalculators.HourlyRateBasedPL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodHourlyRateForeignOutOfOfficeBonus), typeof(EmployeeResources))]
        public decimal? HourlyRateForeignOutOfOfficeBonus { get; set; }

        [Tab("compensation")]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.MonthlyRateBasedPL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateBasedWithOvertimeAndSickLeavePL,
            TimeTrackingCompensationCalculators.MonthlyRateEmpty)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodMonthlyRate), typeof(EmployeeResources))]
        public decimal? MonthlyRate { get; set; }

        [Tab("compensation")]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(
            TimeTrackingCompensationCalculators.EmploymentContractPL,
            TimeTrackingCompensationCalculators.EmploymentContractWithCdePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodSalaryGross), typeof(EmployeeResources))]
        public decimal? SalaryGross { get; set; }

        [Tab("compensation")]
        [Range(0, 100)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanEditCompensationParameters)]
        [CompensationCalculatorRequirement(TimeTrackingCompensationCalculators.EmploymentContractWithCdePL)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodAuthorRemunerationRatio), typeof(EmployeeResources))]
        public decimal? AuthorRemunerationRatio { get; set; }

        [Visibility(VisibilityScope.None)]
        public EmploymentType? EmploymentType { get; set; }

        public long Id { get; set; }

        public byte[] Timestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (StartDate > EndDate)
            {
                issues.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate,
                   new List<string> { nameof(StartDate) }));
            }

            if (SignedOn > StartDate)
            {
                issues.Add(new ValidationResult(EmployeeResources.SignedOnDateGreaterThanStartDate,
                           PropertyHelper.GetPropertyNames<EmploymentPeriodViewModel>(m => m.SignedOn)));
            }

            if (TimeReportingMode == TimeReportingMode.AbsenceOnly && AbsenceOnlyDefaultProjectId == null)
            {
                issues.Add(new ValidationResult(EmployeeResources.AbsenceOnlyDefaultProjectIsRequired,
                           PropertyHelper.GetPropertyNames<EmploymentPeriodViewModel>(m => m.AbsenceOnlyDefaultProjectId)));
            }

            if (TerminatedOn != null && TerminationReasonId == null)
            {
                issues.Add(new ValidationResult(string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, EmployeeResources.EmploymentPeriodTerminationReason),
                           PropertyHelper.GetPropertyNames<EmploymentPeriodViewModel>(m => m.TerminationReasonId)));
            }

            return issues;
        }

        public override string ToString()
        {
            var employmentTypeDescription = EmploymentType?.GetDescription() ?? EmployeeResources.NoDataText;

            if (EndDate.HasValue)
            {
                return string.Format(EmployeeResources.EmploymentRowText, StartDate.Value.ToShortDateString(),
                    EndDate.Value.ToShortDateString(), employmentTypeDescription, InactivityReason.GetDescription());
            }

            return string.Format(EmployeeResources.EmploymentRowTextNoEndDate, StartDate.Value.ToShortDateString(),
                employmentTypeDescription, InactivityReason.GetDescription());
        }
    }
}
