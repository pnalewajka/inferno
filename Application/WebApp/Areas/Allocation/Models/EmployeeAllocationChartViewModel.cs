﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeAllocationChartViewModel
    {
        private const int WeeksBeforeTodayCalculatedToNotAllocatedAverage = 3;

        public DateTime CurrentWeek { get; set; }

        public IReadOnlyCollection<ChartDropDownViewModel> Charts { get; set; }
        
        public AllocationChartSearchViewModel SearchViewModel { get; set; }

        public EmployeeAllocationChartViewModel(
            AllocationChartSearchViewModel searchViewModel,
            IDictionary<string, IReadOnlyCollection<EmployeeAllocationPeriodDto>> elements, 
            DateTime currentWeek)
        {
            var orgUnitList = new List<ChartOrgUnitViewModel>();
            SearchViewModel = searchViewModel;

            var averagePeriodFrom = currentWeek.AddDays(WeeksBeforeTodayCalculatedToNotAllocatedAverage * -7);
            var averagePeriodTo = currentWeek;

            Charts =
                elements.Select(
                    e => { 
                        var averagePeriodItems = e.Value
                                .Where(v => v.Week >= averagePeriodFrom && v.Week <= averagePeriodTo)
                                .ToList();

                        return new ChartDropDownViewModel(new ChartOrgUnitViewModel(),
                        e.Value.Select(v => new ChartEntryViewModel
                        {
                            Date = v.Week,
                            PlannedFTE = v.PlannedFTE,
                            AvailableFTE = v.AvailableFTE,
                            AllocatedFTE = v.AllocatedFTE,
                            NotAllocatedFTE = v.NotAllocatedFTE,
                            NotAllocatedPercentage = v.NotAllocatedPercentage
                        }),
                        averagePeriodItems.Any() ? averagePeriodItems.Average(v => v.NotAllocatedPercentage) : 0,
                        averagePeriodFrom,
                        averagePeriodTo
                        )
                        { CurrentWeek = currentWeek };
                    })
                    .ToList();
        }
    }
}