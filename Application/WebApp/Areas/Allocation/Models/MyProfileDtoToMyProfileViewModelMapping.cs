﻿using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class MyProfileDtoToMyProfileViewModelMapping : ClassMapping<MyProfileDto, MyProfileViewModel>
    {
        public MyProfileDtoToMyProfileViewModelMapping()
        {
            Mapping = d => new MyProfileViewModel
            {
                Id = d.Id,
                Timestamp = d.Timestamp,
                FirstName = d.FirstName,
                LastName = d.LastName,
                Email = d.Email,
                Title = d.JobTitle,
                LocationName = d.LocationName,
                CurrentEmployeeStatus = d.CurrentEmployeeStatus.GetDescription(CultureInfo.CurrentUICulture),
                IsProjectContributor = d.IsProjectContributor,
                IsCoreAsset = d.IsCoreAsset,
                JobProfileNames = d.JobProfileNames,
                SkillNames = d.SkillNames,
                CompanyName = d.Company != null ? d.Company.Name : string.Empty,
                UserFullName = d.User != null ? d.User.GetFullName() : string.Empty,
                LineManagerFullName = d.LineManager != null ? d.LineManager.FullName : string.Empty,
                OrgUnitName = d.OrgUnitName,
                AdEmailGroupNames = d.ActiveDirectoryEmailGroupNames,
                AdSecurityGroupNames = d.ActiveDirectorySecurityGroupNames,
                CanViewActiveDirectoryGroups = d.CanViewActiveDirectoryGroups,
                IsInOrgChart = d.IsInOrgChart,
                AvailableVacations = d.AvailableVacations,
                TotalVacations = d.TotalVacations,
                AvailableOvertime = d.AvailableOvertime,
                Surveys = d.Surveys != null ? d.Surveys.Select(s => new MyProfileSurveyViewModel
                    {
                        Id = s.Id,
                        Name = s.Name,
                    }).ToArray() : new MyProfileSurveyViewModel[0],
                PendingRequestCount = d.PendingRequestCount,
            };
        }
    }
}