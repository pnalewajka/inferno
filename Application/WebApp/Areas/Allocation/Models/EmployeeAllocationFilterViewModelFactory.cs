﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeAllocationFilterViewModelFactory : IViewModelFactory<EmployeeAllocationFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public EmployeeAllocationFilterViewModelFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public EmployeeAllocationFilterViewModel Create()
        {
            return new EmployeeAllocationFilterViewModel
            {
                From = _timeService.GetCurrentDate(),
                To = _timeService.GetCurrentDate().AddMonths(2)
            };
        }
    }
}