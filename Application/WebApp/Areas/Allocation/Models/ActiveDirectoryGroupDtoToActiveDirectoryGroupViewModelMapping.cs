﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ActiveDirectoryGroupDtoToActiveDirectoryGroupViewModelMapping : ClassMapping<ActiveDirectoryGroupDto, ActiveDirectoryGroupViewModel>
    {
        private IPrincipalProvider _principalProvider;

        public ActiveDirectoryGroupDtoToActiveDirectoryGroupViewModelMapping(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;

            Mapping = d => new ActiveDirectoryGroupViewModel
            {
                Id = d.Id,
                ActiveDirectoryId = d.ActiveDirectoryId,
                Name = d.Name,
                GroupType = d.GroupType,
                Email = d.Email,
                Description = d.Description,
                Info = d.Info,
                Aliases = d.Aliases,
                UserIds = d.UsersIds,
                OwnerId = d.OwnerId,
                ManagersIds = d.ManagersIds,
                SubgroupIds = d.SubgroupIds,
                CurrentUserId = _principalProvider.Current.Id.Value,
                Timestamp = d.Timestamp,
            };
        }
    }
}
