﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier(ViewModelIdentifier)]
    public class AllocationChartSearchViewModel: IValidatableObject
    {
        internal const string ViewModelIdentifier = "Allocation.AllocationChartSearchViewModel";

        public AllocationChartSearchViewModel()
        {
            OrgUnitIds = new long[0];
            CountryIds = new long[0];
            LocationIds = new long[0];
        }

        [DisplayNameLocalized(nameof(EmployeeAllocationChartsResources.SelectChartLabel), typeof(EmployeeAllocationChartsResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long[] OrgUnitIds { get; set; }

        [ValuePicker(Type = typeof(CountryPickerController))]
        [DisplayNameLocalized(nameof(EmployeeAllocationChartsResources.CountriesLabel), typeof(EmployeeAllocationChartsResources))]
        public long[] CountryIds { get; set; }

        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(EmployeeAllocationChartsResources.LocationsLabel), typeof(EmployeeAllocationChartsResources))]
        public long[] LocationIds { get; set; }

        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeAllocationChartsResources.DateFromLabel), typeof(EmployeeAllocationChartsResources))]
        public DateTime? DateFrom { get; set; }

        [Render(Size = Size.Large)]
        [DisplayNameLocalized(nameof(EmployeeAllocationChartsResources.DateToLabel), typeof(EmployeeAllocationChartsResources))]
        public DateTime? DateTo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DateFrom.HasValue && DateTo.HasValue && DateTo <= DateFrom)
            {
                yield return new ValidationResult(EmployeeAllocationChartsResources.InvalidDateRangeError);
            }
        }
    }
}