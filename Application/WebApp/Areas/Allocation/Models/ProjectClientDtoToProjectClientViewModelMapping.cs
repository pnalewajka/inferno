﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectClientDtoToProjectClientViewModelMapping : ClassMapping<ProjectClientDto, ProjectClientViewModel>
    {
        public ProjectClientDtoToProjectClientViewModelMapping()
        {
            Mapping = d => new ProjectClientViewModel
            {
                Id = d.ProjectId,
                ClientName = d.ClientName
            };
        }
    }
}
