﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.LocationFilter")]
    [DescriptionLocalized("EmployeeLocationFilterDialogTitle", typeof(EmployeeResources))]
    public class LocationFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [Required]
        [MinLength(1, ErrorMessageResourceName = "LocationRequiredErrorMessage", ErrorMessageResourceType = typeof(EmployeeResources))]
        public long[] LocationIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeeLocationLabel;
        }
    }
}