﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using CrossCutting.Common.Extensions;
    using static ActiveDirectoryGroupVisualizationDto;
    using static ActiveDirectoryGroupVisualizationViewModel;

    public class ActiveDirectoryGroupVisualizationDtoToActiveDirectoryGroupVisualizationViewModelMapping : ClassMapping<ActiveDirectoryGroupVisualizationDto, ActiveDirectoryGroupVisualizationViewModel[]>
    {
        public ActiveDirectoryGroupVisualizationDtoToActiveDirectoryGroupVisualizationViewModelMapping()
        {
            Mapping = d => ConvertToVisualizationViewModel(d, 0);
        }

        private static ActiveDirectoryGroupVisualizationViewModel[] ConvertToVisualizationViewModel(
            ActiveDirectoryGroupVisualizationDto dto, int nestingLevel)
        {
            var managerIds = dto.Managers.Select(m => m.Id).ToHashSet();
            var memberIds = dto.Members.Select(m => m.Id).ToHashSet();

            var result = new List<ActiveDirectoryGroupVisualizationViewModel>
            {
                new ActiveDirectoryGroupVisualizationViewModel
                {
                    Id = dto.Id,
                    Name = dto.Name,
                    Email = dto.Email,
                    GroupType = dto.GroupType,
                    NestingLevel = nestingLevel,
                    Users = (dto.Owner != null ? new[] { dto.Owner } : Enumerable.Empty<ActiveDirectoryGroupUserDto>())
                        .Concat(dto.Managers)
                        .Concat(dto.Members)
                        .DistinctBy(u => u.Id)
                        .Select(u => ConvertUserViewModel(u, dto.Owner?.Id, managerIds, memberIds))
                        .ToList()
                }
            };

            foreach (var subgroup in dto.Subgroups)
            {
                result.AddRange(ConvertToVisualizationViewModel(subgroup, nestingLevel + 1));
            }

            return result.ToArray();
        }

        private static ActiveDirectoryGroupUserViewModel ConvertUserViewModel(
            ActiveDirectoryGroupUserDto dto, long? ownerId, IEnumerable<long> managerIds, IEnumerable<long> memberIds)
        {
            return new ActiveDirectoryGroupUserViewModel
            {
                Id = dto.Id,
                EmployeeId = dto.EmployeeId,
                FullName = dto.FullName,
                IsOwner = ownerId == dto.Id,
                IsManager = managerIds.Contains(dto.Id),
                IsMember = memberIds.Contains(dto.Id) 
            };
        }
    }
}
