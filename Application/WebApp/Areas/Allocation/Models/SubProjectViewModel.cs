﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [TabDescription(0, "basic", nameof(ProjectResources.BasicInformationTabName), typeof(ProjectResources))]
    [TabDescription(1, "management", nameof(ProjectResources.ManagementTabName), typeof(ProjectResources))]
    [TabDescription(2, "scheduling", nameof(ProjectResources.SchedulingTabName), typeof(ProjectResources))]
    [TabDescription(5, "timetracking", nameof(ProjectResources.TimeTracking), typeof(ProjectResources))]
    [FieldSetDescription(1, "import", nameof(ProjectResources.ImportTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(2, "approver", nameof(ProjectResources.ApproverTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(3, "overtime", nameof(ProjectResources.OvertimeTimetracking), typeof(ProjectResources))]
    [FieldSetDescription(4, "config", nameof(ProjectResources.ConfigTimetracking), typeof(ProjectResources))]
    [Identifier("Models.SubProjectViewModel")]
    public class SubProjectViewModel : ProjectBaseViewModel, IValidatableObject
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        [ReadOnly(true)]
        public long SetupId { get; set; }

        [Visibility(VisibilityScope.None)]
        [ReadOnly(true)]
        public bool IsMainProject { get; set; }

        [Visibility(VisibilityScope.None)]
        [ReadOnly(true)]
        public override string ClientShortName { get; set; }

        [Visibility(VisibilityScope.None)]
        [ReadOnly(true)]
        public override string ProjectName { get; set; }

        [Visibility(VisibilityScope.None)]
        [ReadOnly(true)]
        public override string ClientProjectName { get; set; }

        [Tab("basic")]
        [StringLength(128)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Order(2)]
        [Required]
        public override string ProjectShortName { get; set; }

        [Tab("basic")]
        [Order(3)]
        [StringLength(256)]
        [DisplayNameLocalized(nameof(ProjectResources.PetsCodeLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public string PetsCode { get; set; }

        [Tab("basic")]
        [Order(4)]
        [StringLength(256)]
        [DisplayNameLocalized(nameof(ProjectResources.JiraKeyLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public string JiraKey { get; set; }

        [Tab("basic")]
        [Order(5)]
        [DisplayNameLocalized(nameof(ProjectResources.JiraIssueKeyLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(256)]
        public string JiraIssueKey { get; set; }

        [Tab("basic")]
        [Order(6)]
        [DisplayNameLocalized(nameof(ProjectResources.ApnLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(256)]
        public string Apn { get; set; }

        [Tab("basic")]
        [Order(7)]
        [DisplayNameLocalized(nameof(ProjectResources.KupferwerkProjectIdLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(256)]
        public string KupferwerkProjectId { get; set; }

        [Tab("basic")]
        [Order(9)]
        [Multiline(4)]
        [Ellipsis(MaxStringLength = 180)]
        public override string Description { get; set; }

        [Tab("basic")]
        [Order(43)]
        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectAllowedBonusTypesLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<BonusTypeSet>))]
        public BonusTypeSet AllowedBonusTypes { get; set; }

        [Tab("scheduling")]
        [Order(13)]
        [Visibility(VisibilityScope.Form)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(ProjectResources.StartDateLabel), typeof(ProjectResources))]
        public DateTime? StartDate { get; set; }

        [Tab("scheduling")]
        [Order(14)]
        [Visibility(VisibilityScope.Form)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(ProjectResources.EndDateLabel), typeof(ProjectResources))]
        public DateTime? EndDate { get; set; }

        [Tab("timetracking")]
        [Order(33)]
        [FieldSet("import")]
        [DisplayNameLocalized(nameof(ProjectResources.TimeTrackingImportSource), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(TimeTrackingImportSourceController))]
        [Visibility(VisibilityScope.Form)]
        public long? TimeTrackingImportSourceId { get; set; }

        [Tab("timetracking")]
        [Order(34)]
        [FieldSet("import")]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(ProjectResources.JiraIssueToTimeReportRowMapper), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(JiraIssueToTimeReportRowMapperPickerController))]
        public long JiraIssueToTimeReportRowMapperId { get; set; }

        [Tab("timetracking")]
        [Order(35)]
        [FieldSet("approver")]
        [DisplayNameLocalized(nameof(ProjectResources.IsApprovedByRequesterMainManager), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public bool IsApprovedByRequesterMainManager { get; set; }

        [Tab("timetracking")]
        [Order(36)]
        [FieldSet("approver")]
        [DisplayNameLocalized(nameof(ProjectResources.IsApprovedByMainManagerForProjectManager), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public bool IsApprovedByMainManagerForProjectManager { get; set; }

        [Tab("timetracking")]
        [Order(37)]
        [FieldSet("approver")]
        [DisplayNameLocalized(nameof(ProjectResources.TimeReportAcceptingPerson), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Visibility(VisibilityScope.Form)]
        public long[] TimeReportAcceptingPersonIds { get; set; }

        [Tab("timetracking")]
        [Order(38)]
        [FieldSet("overtime")]
        [DisplayNameLocalized(nameof(ProjectResources.AllowedOvertimeTypes), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<OverTimeTypes>))]
        public OverTimeTypes AllowedOvertimeTypes { get; set; }

        [Tab("timetracking")]
        [Order(39)]
        [FieldSet("config")]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(ProjectResources.TimeTrackingProjectType), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public TimeTrackingProjectType TimeTrackingType { get; set; }

        [Tab("timetracking")]
        [Order(40)]
        [FieldSet("config")]
        [DisplayNameLocalized(nameof(ProjectResources.UtilizationCategoryLabel), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        [ValuePicker(Type = typeof(UtilizationCategoryPickerController))]
        public long? UtilizationCategoryId { get; set; }

        [Tab("timetracking")]
        [Order(41)]
        [FieldSet("config")]
        [DisplayNameLocalized(nameof(ProjectResources.Attributes), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(TimeReportProjectAttributePickerController))]
        public List<long> Attributes { get; set; }

        [Tab("timetracking")]
        [Order(42)]
        [FieldSet("config")]
        [DisplayNameLocalized(nameof(ProjectResources.ReportingStartsOn), typeof(ProjectResources))]
        [Visibility(VisibilityScope.Form)]
        public DateTime? ReportingStartsOn { get; set; }

        [Tab("basic")]
        [Order(3)]
        [Required]
        public override string Acronym { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsOwnProject { get; set; }

        public override string ToString()
        {
            return ClientProjectName;
        }

        public SubProjectViewModel()
        {
            TimeReportAcceptingPersonIds = new long[0];
            Attributes = new List<long>();
        }

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            if (StartDate > EndDate)
            {
                validationResults.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate,
                   new List<string> { nameof(StartDate) }));
            }

            return validationResults;
        }
    }
}
