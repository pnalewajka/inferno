﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class StaffingDemandDtoToStaffingDemandViewModelMapping : ClassMapping<StaffingDemandDto, StaffingDemandViewModel>
    {
        public StaffingDemandDtoToStaffingDemandViewModelMapping()
        {
            Mapping = d => new StaffingDemandViewModel
            {
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                Description = d.Description,
                ProjectId = d.ProjectId,
                EmployeesNumber = d.EmployeesNumber,
                JobMatrixLevelId = d.JobMatrixLevelId,
                SkillsIds = d.SkillsIds,
                JobProfileId = d.JobProfileId,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}
