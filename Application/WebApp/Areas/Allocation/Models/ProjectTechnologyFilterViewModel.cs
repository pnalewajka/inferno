﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.ProjectTechnologyFilter")]
    [DescriptionLocalized("ProjectTechnologiesFilterDialogTitle", typeof(ProjectResources))]
    public class ProjectTechnologyFilterViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.TechnologiesLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] TechnologyIds { get; set; }

        public override string ToString()
        {
            return ProjectResources.TechnologiesLabel;
        }
    }
}