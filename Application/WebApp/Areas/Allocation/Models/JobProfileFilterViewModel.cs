﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.JobProfileFilter")]
    [DescriptionLocalized("EmployeeJobProfileFilterDialogTitle", typeof(EmployeeResources))]
    public class JobProfileFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] JobProfileIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeeJobProfileLabel;
        }
    }
}