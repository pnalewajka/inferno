﻿namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using Newtonsoft.Json;

    using Smt.Atomic.Business.Common.Services;
    using Smt.Atomic.CrossCutting.Business.Enums;
    using Smt.Atomic.CrossCutting.Common.Attributes;
    using Smt.Atomic.CrossCutting.Common.Models;
    using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
    using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
    using Smt.Atomic.WebApp.Areas.Allocation.Resources;
    using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

    [Identifier("EmployeeAllocationFilter.SkillsFilter")]
    [DescriptionLocalized("SkillsFilter", typeof(AllocationResources))]
    public class SkillsFilterViewModel
    {
        public SkillsFilterViewModel()
        {
            Skill1Name = new long[] { };
            Skill2Name = new long[] { };
            Skill3Name = new long[] { };
            Skill4Name = new long[] { };
            Skill5Name = new long[] { };
        }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] Skill1Name { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeMinSkillLevel), typeof(EmployeeResources))]
        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SkillExpertiseLevel>))]
        public long Skill1MinimumLevel { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] Skill2Name { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeMinSkillLevel), typeof(EmployeeResources))]
        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SkillExpertiseLevel>))]
        public long Skill2MinimumLevel { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] Skill3Name { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeMinSkillLevel), typeof(EmployeeResources))]
        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SkillExpertiseLevel>))]
        public long Skill3MinimumLevel { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] Skill4Name { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeMinSkillLevel), typeof(EmployeeResources))]
        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SkillExpertiseLevel>))]
        public long Skill4MinimumLevel { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] Skill5Name { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeMinSkillLevel), typeof(EmployeeResources))]
        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<SkillExpertiseLevel>))]
        public long Skill5MinimumLevel { get; set; }

        public override string ToString()
        {
            return AllocationResources.SkillsFilter;
        }
    }
}