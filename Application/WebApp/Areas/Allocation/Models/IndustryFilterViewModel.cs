﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("EmployeeAllocationFilter.IndustryFilter")]
    [DescriptionLocalized("IndustryTitle", typeof(DictionaryResources))]
    public class IndustryFilterViewModel
    {
        public IndustryFilterViewModel()
        {
            IndustryIds = new long[] { };
        }

        [DisplayNameLocalized(nameof(DictionaryResources.IndustryTitle), typeof(DictionaryResources))]
        [ValuePicker(Type = typeof(IndustryPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] IndustryIds { get; set; }

        public override string ToString()
        {
            return DictionaryResources.IndustryTitle;
        }
    }
}