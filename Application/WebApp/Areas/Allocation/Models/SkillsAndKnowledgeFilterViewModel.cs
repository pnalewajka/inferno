﻿using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("EmployeeAllocationFilter.SkillsAndKnowledgeFilter")]
    [DescriptionLocalized("SkillsAndKnowledgeFilter", typeof(AllocationResources))]
    public class SkillsAndKnowledgeFilterViewModel
    {
        public SkillsAndKnowledgeFilterViewModel()
        {
            JobProfileIds = new long[] {};  
            JobMatrixLevels = new long[] {};  
            SkillIds = new long[] {};
        }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] JobProfileIds { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixShortLabel), typeof(EmployeeResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<JobMatrixLevels>))]
        [JsonConverter(typeof (ArrayToUrlConverter))]
        [Render(Size = Size.Large)]
        public long[] JobMatrixLevels { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] SkillIds { get; set; }

        public override string ToString()
        {
            return AllocationResources.SkillsAndKnowledgeFilter;
        }
    }
}