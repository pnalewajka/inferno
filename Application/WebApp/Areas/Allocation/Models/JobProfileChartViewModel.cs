using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Model.JobProfileChartViewModel")]
    public class JobProfileChartViewModel
    {
        public JobProfileChartViewModel(IEnumerable<JobProfileChartDetailsDto> modelDto)
        {
            JobProfileDetails = modelDto.Select(m => new JobProfileChartDetailsViewModel
            {
                LocationId = m.LocationId,
                JobProfileId = m.JobProfileId,
                JobMatrixLevel = m.JobMatrixLevel,
                LocationName = m.LocationName,
                JobProfileName = m.JobProfileName,
                JobMatrixLevelCode = m.JobMatrixLevelCode,
                JobMatrixLevelName = m.JobMatrixLevelName,
                AllocatedFte = m.AllocatedFte,
                AvailableFte = m.AvailableFte,
                PlannedFte = m.PlannedFte,
                FreeFte = m.FreeFte
            }).ToList();
        }

        public DateTime Period { get; set; }

        public long[] OrgUnitIds { get; set; }

        public IList<JobProfileChartDetailsViewModel> JobProfileDetails { get; set; }
    }
}