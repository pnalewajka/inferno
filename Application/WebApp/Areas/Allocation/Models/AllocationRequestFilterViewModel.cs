﻿namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using Newtonsoft.Json;
    using Presentation.Renderers.Bootstrap.Attributes;
    using Resources;
    using Smt.Atomic.CrossCutting.Common.Attributes;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [DescriptionLocalized("AllocationRequestFilter", typeof(AllocationResources))]
    [Identifier("AllocationRequestFilter.DateRangeFilter")]
    public class AllocationRequestFilterViewModel : IValidatableObject
    {
        public AllocationRequestFilterViewModel()
        {
            From = DateTime.Now.Date;
        }

        [DisplayNameLocalized(nameof(AllocationResources.From), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(AllocationResources.To), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        public DateTime? To { get; set; }

        public override string ToString()
        {
            return string.Format(AllocationResources.FromTo, From.ToShortDateString(), To?.ToShortDateString());
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (From > To)
            {
                issues.Add(new ValidationResult(AllocationResources.RangeFilterFromGreaterThanTo));
            }

            return issues;
        }
    }
}