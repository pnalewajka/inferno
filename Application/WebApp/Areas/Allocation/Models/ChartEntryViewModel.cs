﻿using System;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ChartEntryViewModel
    {
        public DateTime Date { get; set; }

        public decimal AllocatedFTE { get; set; }

        public decimal PlannedFTE { get; set; }

        public decimal AvailableFTE { get; set; }

        public decimal NotAllocatedFTE { get; set; }

        public decimal NotAllocatedPercentage { get; set; }
    }
}