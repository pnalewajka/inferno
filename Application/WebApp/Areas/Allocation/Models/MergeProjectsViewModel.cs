﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RestSharp.Validation;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Models.MergeProjectsViewModel")]
    public class MergeProjectsViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.SourceProjectsLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(InfernoProjectPickerController), OnResolveUrlJavaScript = "url=url + '&filter=inferno-projects'")]
        [Render(Size = Size.Large)]
        public IList<long> SourceProjectIds { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ProjectResources.TargetProjectLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(JiraProjectPickerController), OnResolveUrlJavaScript = "url=url + '&filter=jira-projects'")]
        [Render(Size = Size.Large)]
        public long TargetProjectId { get; set; }
    }
}