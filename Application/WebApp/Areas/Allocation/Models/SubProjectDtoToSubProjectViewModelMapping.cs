﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SubProjectDtoToSubProjectViewModelMapping
        : ClassMapping<SubProjectDto, SubProjectViewModel>
    {
        public SubProjectDtoToSubProjectViewModelMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = d => new SubProjectViewModel
            {
                Id = d.Id,
                SetupId = d.SetupId,
                IsMainProject = d.IsMainProject,
                JiraIssueKey = d.JiraIssueKey,
                JiraKey = d.JiraKey,
                PetsCode = d.PetsCode,
                Apn = d.Apn,
                KupferwerkProjectId = d.KupferwerkProjectId,
                Description = d.Description,
                ProjectShortName = d.ProjectShortName,
                ClientProjectName = d.ClientProjectName,
                ProjectName = d.ProjectName,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                TimeTrackingImportSourceId = d.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = d.AllowedOvertimeTypes,
                TimeReportAcceptingPersonIds = d.TimeReportAcceptingPersonIds == null ? null : d.TimeReportAcceptingPersonIds.Any() ? d.TimeReportAcceptingPersonIds.ToArray() : null,
                IsApprovedByRequesterMainManager = d.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = d.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = d.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = d.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = d.UtilizationCategoryId,
                Attributes = d.AttributesIds.ToList(),
                ReportingStartsOn = d.ReportingStartsOn,
                Acronym = d.Acronym,
                IsOwnProject = d.IsOwnProject,
                AllowedBonusTypes = d.AllowedBonusTypes,
            };
        }
    }
}
