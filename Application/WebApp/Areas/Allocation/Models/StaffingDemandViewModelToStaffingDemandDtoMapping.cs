﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class StaffingDemandViewModelToStaffingDemandDtoMapping : ClassMapping<StaffingDemandViewModel, StaffingDemandDto>
    {
        public StaffingDemandViewModelToStaffingDemandDtoMapping()
        {
            Mapping = v => new StaffingDemandDto
            {
                StartDate = v.StartDate,
                EndDate = v.EndDate,
                Description = v.Description,
                ProjectId = v.ProjectId,
                EmployeesNumber = v.EmployeesNumber,
                JobMatrixLevelId = v.JobMatrixLevelId,
                SkillsIds = v.SkillsIds != null && v.SkillsIds.Length > 0 ? v.SkillsIds : new long[] { },
                JobProfileId = v.JobProfileId,
                Id = v.Id,
                Timestamp = v.Timestamp
            };
        }
    }
}
