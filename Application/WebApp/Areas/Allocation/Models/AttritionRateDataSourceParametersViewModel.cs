﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Models.AttritionReportParametres")]
    public class AttritionRateDataSourceParametersViewModel
    {
        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ApprovedFromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ApprovedToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [Order(3)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.ContractTypesText), typeof(ReportResources))]
        public long[] ContractTypeIds { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(ReportResources.SkipWorkingStudents), typeof(ReportResources))]
        public bool SkipWorkingStudents { get; set; }

        [Order(5)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long? OrgUnitId { get; set; }

        [Order(6)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeLocationLabel), typeof(ReportResources))]
        public long? LocationId { get; set; }

        [Order(7)]
        [DisplayNameLocalized(nameof(ReportResources.EmployeePlaceOfWork), typeof(ReportResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        public PlaceOfWork? PlaceOfWork { get; set; }
    }
}