﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AttritionRateDataSourceParametersViewModelToAttritionRateDataSourceParametersDtoMapping : ClassMapping<AttritionRateDataSourceParametersViewModel, AttritionRateDataSourceParametersDto>
    {
        public AttritionRateDataSourceParametersViewModelToAttritionRateDataSourceParametersDtoMapping()
        {
            Mapping = v => new AttritionRateDataSourceParametersDto
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,  
                ContractTypeIds = v.ContractTypeIds,
                OrgUnitId = v.OrgUnitId,
                SkipWorkingStudents = v.SkipWorkingStudents,
                LocationId = v.LocationId,
                PlaceOfWork = v.PlaceOfWork
            };
        }
    }
}