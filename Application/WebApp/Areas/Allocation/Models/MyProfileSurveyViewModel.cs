﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class MyProfileSurveyViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}