﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectTagViewModelToProjectTagDtoMapping : ClassMapping<ProjectTagViewModel, ProjectTagDto>
    {
        public ProjectTagViewModelToProjectTagDtoMapping()
        {
            Mapping = v => new ProjectTagDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                TagType = v.TagType,
                Timestamp = v.Timestamp
            };
        }
    }
}
