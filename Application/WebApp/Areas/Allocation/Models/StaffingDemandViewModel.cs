﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class StaffingDemandViewModel : IValidatableObject
    {
        [Order(0)]
        [Required]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ProjectResources.StartDateLabel), nameof(ProjectResources.StartDateLabelShort), typeof(ProjectResources))]
        public DateTime StartDate { get; set; }

        [Order(1)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ProjectResources.EndDateLabel), nameof(ProjectResources.EndDateLabelShort), typeof(ProjectResources))]
        public DateTime? EndDate { get; set; }

        [Order(2)]
        [Required]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ProjectResources.DescriptionLabel), typeof(ProjectResources))]
        public string Description { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.ProjectLabel), typeof(ProjectResources))]
        public long ProjectId { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(ProjectResources.EmployeesNumberLabel), nameof(ProjectResources.EmployeesNumberLabelShort), typeof(ProjectResources))]
        public decimal EmployeesNumber { get; set; }

        [Order(5)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.JobMatrixLabel), nameof(ProjectResources.JobMatrixLabelShort), typeof(ProjectResources))]
        public long? JobMatrixLevelId { get; set; }

        [Order(6)]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(SkillPickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.SkillLabel), typeof(ProjectResources))]
        public long[] SkillsIds { get; set; }

        [Order(7)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [DisplayNameLocalized(nameof(ProjectResources.JobProfileLabel), typeof(ProjectResources))]
        public long? JobProfileId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return StartDate.ToString();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (StartDate > EndDate)
            {
                issues.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate,
                   new List<string> { nameof(StartDate) }));
            }

            return issues;
        }
    }
}
