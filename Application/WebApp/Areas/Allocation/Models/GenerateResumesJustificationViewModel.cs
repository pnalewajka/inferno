﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Resume.Resources;
using Smt.Atomic.WebApp.Areas.Sales.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Models.GenerateResumesJustification")]
    public class GenerateResumesJustificationViewModel
    {
        [HiddenInput]
        public long[] EmployeeIds { get; set; }

        [DisplayNameLocalized(nameof(ResumeResources.GenerateResumesCustomerToShareResumeWith), typeof(ResumeResources))]
        [ValuePicker(Type = typeof(CustomerPickerController))]
        [Required]
        public virtual long CustomerId { get; set; }

        [HtmlSanitize]
        [AllowHtml]
        [DisplayNameLocalized(nameof(ResumeResources.GenerateResumesComment), typeof(ResumeResources))]
        [Multiline(5)]
        public string Comment { get; set; }

        public GenerateResumesJustificationViewModel(long[] employeeIds)
        {
            EmployeeIds = employeeIds;
        }

        public GenerateResumesJustificationViewModel()
        {
        }
    }
}