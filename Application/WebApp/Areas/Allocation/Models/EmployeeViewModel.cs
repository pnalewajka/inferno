﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Formatters;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [TabDescription(0, "basic", "UserAccountTabName", typeof(EmployeeResources))]
    [TabDescription(1, "personal", "PersonalInformationTabName", typeof(EmployeeResources))]
    [TabDescription(2, "managementdata", "OrganizationTabName", typeof(EmployeeResources))]
    [TabDescription(3, "knowledgetrack", "KnowledgeTrackTabName", typeof(EmployeeResources))]
    [Identifier("Models.EmployeeViewModel")]
    public class EmployeeViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Tab("basic")]
        [Order(0)]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Tab("basic")]
        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(255)]
        public string LastName { get; set; }

        [Order(2)]
        [ProgressBar(BarCssClass = "progress-bar-success")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.ResumePercentage), typeof(EmployeeResources))]
        public decimal FillPercentage { get; set; }

        [Order(3)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.ResumeModificationDate), typeof(EmployeeResources))]
        public DateTime? ResumeModificationDate { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? LastResumeId { get; set; }

        [Tab("basic")]
        [Order(2)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(EmployeeResources.TitleLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(JobTitlePickerController))]
        public long? JobTitleId { get; set; }

        [ReadOnly(true)]
        [Tab("basic")]
        [Order(3)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeStatusLabel), nameof(EmployeeResources.EmployeeStatusShortLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [RoleRequired(SecurityRoleType.CanViewEmployeeStatus)]
        public EmployeeStatus CurrentEmployeeStatus { get; set; }
        
        [Tab("basic")]
        [Order(4)]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeEmailLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Tab("basic")]
        [Order(5)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeUser), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? UserId { get; set; }

        [Tab("personal")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeDateOfBirth), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewEmployeePersonalData, SecurityRoleType.CanEditEmployeePersonalData)]
        public DateTime? DateOfBirth { get; set; }

        [Tab("personal")]
        [StringLength(32)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePersonalNumber), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewEmployeePersonalData, SecurityRoleType.CanEditEmployeePersonalData)]
        public string PersonalNumber { get; set; }

        [Tab("personal")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePersonalNumberType), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewEmployeePersonalData, SecurityRoleType.CanEditEmployeePersonalData)]
        public PersonalNumberType? PersonalNumberType { get; set; }

        [Tab("personal")]
        [StringLength(32)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeIdCardNumber), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewEmployeePersonalData, SecurityRoleType.CanEditEmployeePersonalData)]
        public string IdCardNumber { get; set; }

        [Tab("personal")]
        [StringLength(32)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePhoneNumber), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public string PhoneNumber { get; set; }

        [Tab("personal")]
        [StringLength(50)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkypeName), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public string SkypeName { get; set; }

        [Tab("personal")]
        [Multiline(3)]
        [StringLength(128)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeHomeAddress), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewEmployeePersonalData, SecurityRoleType.CanEditEmployeePersonalData)]
        public string HomeAddress { get; set; }

        [Tab("personal")]
        [Multiline(2)]
        [StringLength(128)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeAvailabilityDetails), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public string AvailabilityDetails { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLabel), nameof(EmployeeResources.EmployeeJobMatrixShortLabel), typeof(EmployeeResources))]
        [Tab("knowledgetrack")]
        [Order(14)]
        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController), GridFormatterType = typeof(JobMatrixLevelPickerGridFormatter))]
        public long? JobMatrixId { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [Tab("knowledgetrack")]
        [Order(16)]
        [Visibility(VisibilityScope.Form)]
        [Ellipsis(MaxStringLength = 30)]
        [ValuePicker(Type = typeof(SkillPickerController))]
        public long[] SkillIds { get; set; }

        [Tab("knowledgetrack")]
        [Order(15)]
        [AllowBulkEdit]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long[] JobProfileIds { get; set; }      

        [Tab("managementdata")]
        [Order(6)]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeCompany), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        public long? CompanyId { get; set; }

        [Tab("managementdata")]
        [Order(7)]
        [Required]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? LocationId { get; set; }

        [Tab("managementdata")]
        [Order(8)]
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePlaceOfWork), nameof(EmployeeResources.EmployeePlaceOfWorkShort), typeof(EmployeeResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        public PlaceOfWork? PlaceOfWork { get; set; }

        [Tab("managementdata")]
        [Order(10)]
        [AllowBulkEdit]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        public long? LineManagerId { get; set; }

        [Tab("managementdata")]
        [Order(9)]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), nameof(EmployeeResources.EmployeeOrgUnitShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [Required]
        [Visibility(VisibilityScope.Form)]
        public long OrgUnitId { get; set; }

        [Tab("managementdata")]
        [Order(12)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeIsProjectContributorLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public bool IsProjectContributor { get; set; }

        [Tab("managementdata")]
        [Order(13)]
        [AllowBulkEdit]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeIsCoreAssetLabel), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public bool IsCoreAsset { get; set; }

        [Tab("managementdata")]
        [Order(14)]
        [DisplayNameLocalized(nameof(EmployeeResources.MinimumOvertimeBankBalance), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Form)]
        public decimal MinimumOvertimeBankBalance { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(8)]
        [DisplayNameLocalized(nameof(EmployeeResources.JobProfileNamesLabel), typeof(EmployeeResources))]
        public ICollection<string> JobProfileNames { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(9)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [Ellipsis(MaxStringLength = 30)]
        public ICollection<string> SkillNames { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Hub(nameof(OrgUnitId))]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), nameof(EmployeeResources.EmployeeOrgUnitShort), typeof(EmployeeResources))]
        [Order(4)]
        public string OrgUnitName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(3)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        public string LocationName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(2)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeCompany), typeof(EmployeeResources))]
        public string CompanyName { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.TitleLabel), typeof(EmployeeResources))]
        [Order(10)]
        [Visibility(VisibilityScope.Grid)]
        public LocalizedString JobTitleName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(7)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLabel), nameof(EmployeeResources.EmployeeJobMatrixShortLabel), typeof(EmployeeResources))]
        public string JobMatrixName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(5)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [Hub(nameof(LineManagerId))]
        public string LineManagerFullName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Order(10)]
        [DisplayNameLocalized(nameof(EmployeeResources.LoginLabel), typeof(EmployeeResources))]
        public string Login { get; set; }

        [StringLength(15)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.AcronymLabel), typeof(EmployeeResources))]
        public string Acronym { get; set; }

        [Tab("managementdata")]
        [Order(14)]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFeaturesLabel), typeof(EmployeeResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<EmployeeFeatures>))]
        public EmployeeFeatures EmployeeFeatures { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsOwnEmployee { get; set; }

        public string GetFullName()
        {
            return $"{FirstName} {LastName}".Trim();
        }

        public override string ToString()
        {
            return GetFullName();
        }
    }
}
