﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("BenchEmployeeFilter.WeekRangeFilter")]
    [DescriptionLocalized("BenchEmployeeWeekFilter", typeof(AllocationResources))]
    public class BenchEmployeeFilterViewModel : IValidatableObject
    {
        [DisplayNameLocalized(nameof(AllocationResources.Week), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        [Required]
        public DateTime Week { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            Week = Week.GetPreviousDayOfWeek(DayOfWeek.Monday);

            return new List<ValidationResult>();
        }

        public override string ToString()
        {
            return string.Format(AllocationResources.FromTo, Week.ToShortDateString(), Week.GetNextDayOfWeek(DayOfWeek.Sunday).ToShortDateString());
        }
    }
}