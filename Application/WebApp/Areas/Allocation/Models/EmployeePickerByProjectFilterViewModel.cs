﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Allocation.EmployeePickerByProjectFilterViewModel")]
    [DescriptionLocalized("EmployeePickerByProjectFilterDialogTitle", typeof(EmployeeResources))]
    public class EmployeePickerByProjectFilterViewModel
    {
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(EmployeeResources.Project), typeof(EmployeeResources))]
        public long ProjectId { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePickerDateLabel), typeof(EmployeeResources))]
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeePickerDateLabel;
        }
    }
}