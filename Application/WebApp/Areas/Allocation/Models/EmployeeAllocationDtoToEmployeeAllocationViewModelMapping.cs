﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;
using static Smt.Atomic.WebApp.Areas.Allocation.Models.EmployeeAllocationViewModel;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeAllocationDtoToEmployeeAllocationViewModelMapping : ClassMapping<EmployeeAllocationDto, EmployeeAllocationViewModel>
    {
        public EmployeeAllocationDtoToEmployeeAllocationViewModelMapping(ITimeService timeService, IClassMappingFactory classMappingFactory)
        {
            var currentDate = timeService.GetCurrentDate().Date;
            var beginningOfCurrentWeek = timeService.GetCurrentDate().Date.GetPreviousDayOfWeek(DayOfWeek.Monday);

            var statusDescriptionDictionary = Enum.GetValues(typeof(AllocationStatus))
                .Cast<AllocationStatus>()
                .ToDictionary(e => e, e => e.GetDescription());

            var allocationBlockMapping = classMappingFactory.CreateMapping<AllocationBlockDto, AllocationBlockViewModel>();

            Mapping = d => new EmployeeAllocationViewModel
            {
                EmployeeId = d.EmployeeId,
                FirstName = d.FirstName,
                LastName = d.LastName,
                FullName = d.FullName,
                Email = d.Email,
                LocationId = d.LocationId,
                LocationName = d.LocationName,
                Id = d.Id,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                JobProfileId = d.JobProfileIds.Length > 0 ? d.JobProfileIds[0] : (long?)null,
                JobProfileNames = d.JobProfileNames,
                SkillIds = d.SkillIds.Length > 0 ? d.SkillIds : null,
                JobMatrixId = d.JobMatrixIds.Length > 0 ? d.JobMatrixIds[0] : (long?)null,
                JobMatrixLevels = d.JobMatrixLevels,
                UserId = d.UserId,
                LineManagerId = d.LineManagerId,
                LineManagerName = d.LineManagerName,
                OrgUnitId = d.OrgUnitId,
                OrgUnitName = d.OrgUnitName,
                Holidays = d.Holidays,
                Absences = d.Absences.Select(a => new AbsenceBlockViewModel
                {
                    StartDate = a.StartDate,
                    EndDate = a.EndDate,
                    RequestId = a.RequestId,
                    AbsenceName = a.AbsenceName == null ? AllocationRequestResources.AbsenceTaskDescription : a.AbsenceName.ToString(),
                    Status = a.Status,
                    StatusName = a.StatusName,
                    Hours = a.Hours,
                }).ToArray(),
                EmploymentPeriodBlocks = d.EmploymentPeriodBlocks.Select(e => new EmploymentPeriodBlockViewModel
                {
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    ContractedHours = e.ContractedHours
                }).ToArray(),
                AllocationBlocks = d.AllocationBlocks.Select(allocationBlockMapping.CreateFromSource).ToArray(),
                Allocations = d.Allocations.Select(x => new EmployeeAllocationSingleRecordViewModel
                {
                    AllocationPeriod = x.AllocationPeriod,
                    StatusDescription = statusDescriptionDictionary.ContainsKey(x.Status) ? statusDescriptionDictionary[x.Status] : string.Empty,
                    Status = x.Status,
                    AvailableHours = x.AvailableHours,
                    ProjectAllocations =
                        x.ProjectAllocations.GroupBy(pa => new { pa.ProjectId, pa.ProjectColor, pa.ProjectName, pa.Certainty })
                            .Select(pag => new ProjectAllocationViewModel
                            {
                                AvailableHours = x.AvailableHours,
                                HoursAllocated = pag.Sum(pa => pa.HoursAllocated),
                                ProjectColor = pag.Key.ProjectColor,
                                ProjectName = pag.Key.ProjectName,
                                ProjectId = pag.Key.ProjectId,
                                IsVacation = pag.All(p => p.IsVacation),
                                Certainty = pag.Key.Certainty
                            }).ToArray(),
                    IsEmploymentPeriodCurrentDay = x.AllocationPeriod.Date == currentDate,
                    IsEmploymentPeriodCurrentWeek = x.AllocationPeriod.Date == beginningOfCurrentWeek,
                    IsEmploymentPeriodLastWorkingWeekOfTheMonth = CheckIfEmploymentPeriodIsLastWorkingWeekOfTheMonth(x.AllocationPeriod),
                    EmployeeAbsences = x.EmployeeAbsences.Select(a => new EmployeeAllocationViewModel.EmployeeAbsenceViewModel
                    {
                        From = a.From,
                        To = a.To,
                        RequestStatus = a.RequestStatus
                    }).ToArray()
                }
                ).OrderBy(x => x.AllocationPeriod).ToArray()
            };
        }

        private bool CheckIfEmploymentPeriodIsLastWorkingWeekOfTheMonth(DateTime allocationPeriod)
        {
            var beginningOfTheLastWeekOfMonth =
                allocationPeriod.GetLastDayInMonth().GetPreviousDayOfWeek(DayOfWeek.Monday);

            return allocationPeriod.Date == beginningOfTheLastWeekOfMonth;
        }
    }
}
