﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [TabDescription(0, "basic", "BasicAllocationTabName", typeof(ReportResources))]
    [TabDescription(1, "advanced", "AdvancedAllocationTab", typeof(ReportResources))]
    [Identifier("Models.BenchAllocationReportParametres")]
    public class BenchAllocationDataSourceParametersViewModel
    {
        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [Tab("advanced")]
        [Order(1)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.ContractTypesText), typeof(ReportResources))]
        public long[] ContractTypeIds { get; set; }

        [Tab("advanced")]
        [Order(2)]
        [DisplayNameLocalized(nameof(ReportResources.SkipWorkingStudents), typeof(ReportResources))]
        public bool SkipWorkingStudents { get; set; }

        [Tab("basic")]
        [Order(3)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long? OrgUnitId { get; set; }

        [Tab("basic")]
        [Order(4)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeLocationLabel), typeof(ReportResources))]
        public long[] LocationIds { get; set; }

        [Tab("basic")]
        [Order(5)]
        [DisplayNameLocalized(nameof(ReportResources.EmployeePlaceOfWork), typeof(ReportResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        public PlaceOfWork? PlaceOfWork { get; set; }

        [Tab("advanced")]
        [Order(3)]
        [ValuePicker(Type = typeof(JobProfilePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.JobProfilesLabel), typeof(ReportResources))]
        public long[] JobProfileIds { get; set; }

        [Tab("advanced")]
        [Order(4)]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<StaffingStatus>))]
        [DisplayNameLocalized(nameof(ReportResources.StaffingStatusLabel), typeof(ReportResources))]
        public StaffingStatus[] StaffingStatuses { get; set; }

        [Tab("advanced")]
        [Order(5)]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<EmployeeStatus>))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeStatusLabel), typeof(ReportResources))]
        public EmployeeStatus[] EmployeeStatuses { get; set; }
    }
}