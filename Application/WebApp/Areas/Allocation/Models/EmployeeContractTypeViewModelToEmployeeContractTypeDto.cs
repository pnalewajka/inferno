﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeContractTypeViewModelToEmployeeContractTypeDto : ClassMapping<EmployeeContractTypeViewModel, EmployeeContractTypeDto>
    {
    }
}