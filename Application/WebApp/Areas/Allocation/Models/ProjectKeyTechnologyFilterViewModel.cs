﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.ProjectKeyTechnologyFilter")]
    [DescriptionLocalized("ProjectKeyTechnologiesFilterDialogTitle", typeof(ProjectResources))]
    public class ProjectKeyTechnologyFilterViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.KeyTechnologiesLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(KeyTechnologyTagPickerController), OnResolveUrlJavaScript = "url=url + '&filter=key-technology'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] KeyTechnologyIds { get; set; }

        public override string ToString()
        {
            return ProjectResources.KeyTechnologiesLabel;
        }
    }
}