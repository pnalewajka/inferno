﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Models.CommentViewModel")]
    public class CommentViewModel
    {
        [AllowHtml]
        [Multiline(4)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(EmployeeResources.BenchEmployeeComment), typeof(EmployeeResources))]
        public string Description { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public EmployeeCommentType EmployeeCommentType { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long EmployeeId { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public DateTime? RelevantOn { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long? EmployeeCommentId { get; set; }
    }
}