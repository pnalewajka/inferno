﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ActiveDirectoryGroupVisualizationViewModel
    {
        public class ActiveDirectoryGroupUserViewModel
        {
            public long Id { get; set; }

            public long EmployeeId { get; set; }

            public string FullName { get; set; }

            public bool IsOwner { get; set; }

            public bool IsManager { get; set; }

            public bool IsMember { get; set; }
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public ActiveDirectoryGroupType GroupType { get; set; }

        public string GroupTypeCss => GroupType == ActiveDirectoryGroupType.Security ? "fa fa-shield"
            : GroupType == ActiveDirectoryGroupType.Mailing ? "fa fa-envelope-o"
            : string.Empty;

        public int NestingLevel { get; set; }

        public ICollection<ActiveDirectoryGroupUserViewModel> Users { get; set; }

        public ICollection<ActiveDirectoryGroupVisualizationViewModel> Subgroups { get; set; }
    }
}
