﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationRequestDtoToAllocationRequestImportViewModelMapping : ClassMapping<AllocationRequestDto, AllocationRequestImportViewModel>
    {
        private readonly IAllocationRequestImportLookupService _lookupService;

        public AllocationRequestDtoToAllocationRequestImportViewModelMapping(
            IAllocationRequestImportLookupService lookupService)
        {
            _lookupService = lookupService;
            Mapping = d => GetViewModel(d);
        }

        private AllocationRequestImportViewModel GetViewModel(AllocationRequestDto dto)
        {
            var viewModel = new AllocationRequestImportViewModel
            {
                Id = dto.Id,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                ContentType = dto.ContentType,
                AllocationCertainty = dto.AllocationCertainty,
                HoursPerDay = dto.MondayHours,
                MondayHours = dto.MondayHours,
                TuesdayHours = dto.TuesdayHours,
                WednesdayHours = dto.WednesdayHours,
                ThursdayHours = dto.ThursdayHours,
                FridayHours = dto.FridayHours,
                SaturdayHours = dto.SaturdayHours,
                SundayHours = dto.SundayHours,
                Timestamp = dto.Timestamp,
                EmployeeFirstName = dto.EmployeeFirstName,
                EmployeeLastName = dto.EmployeeLastName,
                ProjectCode = _lookupService.GetProjectCodeById(dto.ProjectId),
                EmployeeEmail = _lookupService.GetEmployeeEmailById(dto.EmployeeId)
            };

            return viewModel;
        }
    }
}