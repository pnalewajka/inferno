﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class SimpleAllocationRequestDtoToSimpleAllocationRequestViewModelMapping : ClassMapping<AllocationRequestDto, SimpleAllocationRequestViewModel>
    {
        public SimpleAllocationRequestDtoToSimpleAllocationRequestViewModelMapping()
        {
            Mapping = v => new SimpleAllocationRequestViewModel
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                ProjectId = v.ProjectId,
                StartDate = v.StartDate,
                EndDate = v.EndDate,
                ContentType = v.ContentType,
                AllocationCertainty = v.AllocationCertainty,
                HoursPerDay = v.MondayHours,
                Timestamp = v.Timestamp,
                EmployeeFirstName = v.EmployeeFirstName,
                EmployeeLastName = v.EmployeeLastName,
                ProjectCode = v.ProjectName,
            };
        }
    }
}