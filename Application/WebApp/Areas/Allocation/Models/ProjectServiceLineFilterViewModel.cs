﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.ProjectServiceLineFilter")]
    [DescriptionLocalized("ProjectServiceLinesFilterDialogTitle", typeof(ProjectResources))]
    public class ProjectServiceLineFilterViewModel
    {
        [DisplayNameLocalized(nameof(ProjectResources.ServiceLinesLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(ServiceLineController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] ServiceLineIds { get; set; }

        public override string ToString()
        {
            return ProjectResources.ServiceLinesLabel;
        }
    }
}