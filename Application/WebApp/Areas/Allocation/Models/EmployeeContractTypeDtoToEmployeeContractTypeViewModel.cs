﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.WebApp.Areas.Allocation.Helpers;
using System.Linq;
using System.Globalization;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeContractTypeDtoToEmployeeContractTypeViewModel : ClassMapping<EmployeeContractTypeDto, EmployeeContractTypeViewModel>
    {
        public EmployeeContractTypeDtoToEmployeeContractTypeViewModel()
        {
            Mapping = e => new EmployeeContractTypeViewModel
            {
                Id = e.EmployeeId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Acronym = e.Acronym,
                CompanyId = e.CompanyId,
                LocationId = e.LocationId,
                LineManagerId = e.LineManagerId,
                EndDate = e.CurrentEmployeeEmploymentPeriod != null
                    ? e.CurrentEmployeeEmploymentPeriod.EndDate
                    : e.LastEmployeeEmploymentPeriod != null ? e.LastEmployeeEmploymentPeriod.EndDate : null,
                StartDate = e.CurrentEmployeeEmploymentPeriod != null
                    ? e.CurrentEmployeeEmploymentPeriod.StartDate
                    : (e.LastEmployeeEmploymentPeriod != null ? (DateTime?)e.LastEmployeeEmploymentPeriod.StartDate : null),
                ContractTypeId = e.ContractTypeId != null
                    ? e.ContractTypeId
                    : e.LastEmployeeEmploymentPeriod != null ? e.LastEmployeeEmploymentPeriod.ContractTypeId : (long?)null,
                CurrentEmployeeStatus = e.CurrentEmployeeStatus,
                ContractDuration = e.ContractDuration,
                TimeReportingMode = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.TimeReportingMode : (TimeReportingMode?)null,
                MondayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.MondayHours : 0,
                TuesdayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.TuesdayHours : 0,
                WednesdayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.WednesdayHours : 0,
                ThursdayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.ThursdayHours : 0,
                FridayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.FridayHours : 0,
                SaturdayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.SaturdayHours : 0,
                SundayHours = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.SundayHours : 0,
                VacationHourToDayRatio = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.VacationHourToDayRatio : 0,
                EmployeeHourBalance = e.EmployeeHourBalance,
                EmploymentStartDate = EmployeeContractHelper.GetEmploymentStartDate(e.CurrentEmployeeEmploymentPeriod ?? e.LastEmployeeEmploymentPeriod, e.EmployeeEmploymentPeriods),
                EmploymentEndDate = EmployeeContractHelper.GetEmploymentEndDate(e.CurrentEmployeeEmploymentPeriod ?? e.LastEmployeeEmploymentPeriod, e.EmployeeEmploymentPeriods),
                CurrentStatus = e.LeavingReasonText == null
                    ? e.CurrentEmployeeStatus.GetDescription(CultureInfo.CurrentUICulture)
                    : $"{e.CurrentEmployeeStatus.GetDescription(CultureInfo.CurrentUICulture)} ({e.LeavingReasonText.ToString()})",
                HourlyRate = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.HourlyRate.GetValueOrDefault() : default(decimal),
                HourlyRateDomesticOutOfOfficeBonus = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.HourlyRateDomesticOutOfOfficeBonus.GetValueOrDefault() : default(decimal),
                HourlyRateForeignOutOfOfficeBonus = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.HourlyRateForeignOutOfOfficeBonus.GetValueOrDefault() : default(decimal),
                MonthlyRate = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.MonthlyRate.GetValueOrDefault() : default(decimal),
                SalaryGross = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.SalaryGross.GetValueOrDefault() : default(decimal),
                AuthorRemunerationRatio = e.CurrentEmployeeEmploymentPeriod != null ? e.CurrentEmployeeEmploymentPeriod.AuthorRemunerationRatio.GetValueOrDefault() : default(decimal),
                JobMatrixLevel = e.JobMatrixLevel
            };
        }
    }
}