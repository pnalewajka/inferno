﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ActiveDirectoryGroupViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid ActiveDirectoryId { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupNameLabel), typeof(ActiveDirectoryGroupResources))]
        [Order(0)]
        [ReadOnly(true)]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupGroupTypeLabel), typeof(ActiveDirectoryGroupResources))]
        [Order(1)]
        [ReadOnly(true)]
        public ActiveDirectoryGroupType GroupType { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.None)]
        public string GroupTypeCss => GroupType == ActiveDirectoryGroupType.Security ? "fa fa-shield"
            : GroupType == ActiveDirectoryGroupType.Mailing ? "fa fa-envelope-o"
                : string.Empty;

        [NonSortable]
        [Visibility(VisibilityScope.None)]
        public long CurrentUserId { get; set; }

        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupDescriptionLabel), typeof(ActiveDirectoryGroupResources))]
        [Order(3)]
        [StringLength(1023)]
        public string Description { get; set; }

        [StringLength(1023)]
        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupInfoLabel), typeof(ActiveDirectoryGroupResources))]
        [Order(4)]
        public string Info { get; set; }

        [Order(4)]
        [NonSortable]
        [ReadOnly(true)]
        [Ellipsis(MaxStringLength = 40)]
        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupAliasesLabel), typeof(ActiveDirectoryGroupResources))]
        public string Aliases { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupEmailLabel), typeof(ActiveDirectoryGroupResources))]
        [Order(2)]
        [ReadOnly(true)]
        public string Email { get; set; }

        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupUserIdsLabel), typeof(ActiveDirectoryGroupResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        [NonSortable]
        [Ellipsis(CssClass = "group-member", MaxStringLength = 400, IsHtml = true)]
        [Order(7)]
        public long[] UserIds { get; set; }

        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupManagersIdsLabel), typeof(ActiveDirectoryGroupResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        [NonSortable]
        [Order(6)]
        public long[] ManagersIds { get; set; }

        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupOwnerIdLabel), typeof(ActiveDirectoryGroupResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        [NonSortable]
        [Order(5)]
        public long? OwnerId { get; set; }

        [Order(4)]
        [NonSortable]
        [ReadOnly(true)]
        [ValuePicker(Type = typeof(ActiveDirectoryGroupPickerController))]
        [DisplayNameLocalized(nameof(ActiveDirectoryGroupResources.ActiveDirectoryGroupSubgroupIdsLabel), typeof(ActiveDirectoryGroupResources))]
        public long[] SubgroupIds { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
