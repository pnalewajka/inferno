﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeAllocationViewModelToEmployeeAllocationDtoMapping : ClassMapping<EmployeeAllocationViewModel, EmployeeAllocationDto>
    {
        public EmployeeAllocationViewModelToEmployeeAllocationDtoMapping()
        {
            Mapping = v => new EmployeeAllocationDto
            {
                FirstName = v.FirstName,
                LastName = v.LastName,
                Email = v.Email,
                LocationId = v.LocationId,
                Id = v.Id,
                Timestamp = v.Timestamp,
                JobProfileIds = v.JobProfileId != null ? new long[] { v.JobProfileId.Value } : new long[] { },
                SkillIds = v.SkillIds.Length > 0 ? v.SkillIds : new long[] { },
                JobMatrixIds = v.JobMatrixId != null ? new long[] { v.JobMatrixId.Value } : new long[] { },
                UserId = v.UserId,
                LineManagerId = v.LineManagerId,
                OrgUnitId = v.OrgUnitId
            };
        }
    }
}
