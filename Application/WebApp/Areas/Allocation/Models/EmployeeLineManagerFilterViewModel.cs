﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.EmployeeLineManagerFilter")]
    [DescriptionLocalized("EmployeeLineManagerFilterDialogTitle", typeof(EmployeeResources))]
    public class EmployeeLineManagerFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] LineManagerIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeeLineManager;
        }
    }
}
