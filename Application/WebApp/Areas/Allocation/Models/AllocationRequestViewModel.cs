﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Allocation.AllocationRequestViewModel")]
    [ModelBinder(typeof(AllocationRequestViewModelBinder))]
    [TabDescription(0, AllocationTab, nameof(AllocationRequestResources.AllocationTabName), typeof(AllocationRequestResources))]
    [TabDescription(1, HoursTab, nameof(AllocationRequestResources.HoursTabName), typeof(AllocationRequestResources))]
    [TabDescription(2, ChangesTab, nameof(AllocationRequestResources.ChangesTabName), typeof(AllocationRequestResources))]
    public class AllocationRequestViewModel : IValidatableObject
    {
        private const string AllocationTab = "allocation";
        private const string HoursTab = "hours";
        private const string ChangesTab = "changes";

        public AllocationRequestViewModel()
        {
            EmployeeIds = new long[0];
        }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long Id { get; set; }

        [Tab(AllocationTab)]
        [ImportIgnore]
        [DisplayNameLocalized(nameof(AllocationRequestResources.EmployeeLabel), typeof(AllocationRequestResources))]
        [Required]
        [Order(0)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long EmployeeId { get; set; }

        [Tab(AllocationTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.EmployeesLabel), typeof(AllocationRequestResources))]
        [Required]
        [Order(0)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&" + AllocationRequestController.ContentTypeParameterName + "=' + $('[name = " + nameof(ContentType) + "]').val()")]
        [Visibility(VisibilityScope.Form)]
        public long[] EmployeeIds { get; set; }

        [Tab(AllocationTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.ProjectLabel), typeof(AllocationRequestResources))]
        [Required]
        [Order(1)]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public long ProjectId { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public AllocationRequestContentType ContentType { get; set; }

        [Tab(AllocationTab)]
        [Required]
        [Order(2)]
        [Render(Size = Size.Small)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.AllocationCertaintyLabel), typeof(AllocationRequestResources))]
        public AllocationCertainty AllocationCertainty { get; set; }

        [Tab(AllocationTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.StartDateLabel), nameof(AllocationRequestResources.StartDateLabelShort), typeof(AllocationRequestResources))]
        [Required]
        [Order(3)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime StartDate { get; set; }

        [Tab(AllocationTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.EndDateLabel), nameof(AllocationRequestResources.EndDateLabelShort), typeof(AllocationRequestResources))]
        [Order(4)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? EndDate { get; set; }

        [Tab(AllocationTab)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.HoursPerDayLabel), typeof(AllocationRequestResources))]
        [Required]
        [Order(9)]
        public virtual decimal HoursPerDay { get; set; }

        [Tab(AllocationTab)]
        [Render(Size = Size.Small)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.ProcessingStatus), typeof(AllocationRequestResources))]
        [Order(10)]
        [Visibility(VisibilityScope.Grid)]
        public ProcessingStatus ProcessingStatus { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.MondayHoursLabel), nameof(AllocationRequestResources.MondayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(11)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal MondayHours { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.TuesdayHoursLabel), nameof(AllocationRequestResources.TuesdayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(12)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal TuesdayHours { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.WednesdayHoursLabel), nameof(AllocationRequestResources.WednesdayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(13)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal WednesdayHours { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.ThursdayHoursLabel), nameof(AllocationRequestResources.ThursdayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(14)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal ThursdayHours { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.FridayHoursLabel), nameof(AllocationRequestResources.FridayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(15)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal FridayHours { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.SaturdayHoursLabel), nameof(AllocationRequestResources.SaturdayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(16)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal SaturdayHours { get; set; }

        [Tab(HoursTab)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.SundayHoursLabel), nameof(AllocationRequestResources.SundayHoursLabelShort), typeof(AllocationRequestResources))]
        [Order(17)]
        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        public virtual decimal SundayHours { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmployeeFirstName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmployeeLastName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ProjectCode { get; set; }

        [Tab(ChangesTab)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.ChangeDemand), nameof(AllocationRequestResources.ChangeDemandShort), typeof(AllocationRequestResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<AllocationChangeDemand>))]
        [RoleRequired(SecurityRoleType.CanViewAllocationRequestChangeDemand)]
        [Tooltip(nameof(Comment))]
        public AllocationChangeDemand ChangeDemand { get; set; }

        [Tab(ChangesTab)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AllocationRequestResources.Comment), typeof(AllocationRequestResources))]
        [StringLength(200)]
        [Multiline(2)]
        [RoleRequired(SecurityRoleType.CanViewAllocationRequestChangeDemand)]
        public string Comment { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (EmployeeId == 0 && (EmployeeIds == null || !EmployeeIds.Any()))
            {
                yield return new PropertyValidationResult(AllocationRequestResources.EmployeesRequiredMessage, () => EmployeeIds);
            }

            if (StartDate > EndDate)
            {
                yield return new ValidationResult(CommonResources.StartDateGreaterThanEndDate,
                    new List<string> { nameof(StartDate) });
            }
        }
    }
}
