﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    using Controllers;
    using CrossCutting.Common.Attributes;
    using Presentation.Renderers.Bootstrap.Attributes;
    using Resources;

    [Identifier("AllocationRequestFilter.ProjectTags")]
    [DescriptionLocalized("ProjectTagsFilter", typeof(AllocationRequestResources))]
    public class AllocationRequestProjectTagsFilterViewModel
    {
        [ValuePicker(Type = typeof (ProjectTagPickerController))]
        [DisplayNameLocalized(nameof(AllocationRequestResources.ProjectTagsLabel), typeof(AllocationRequestResources))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] ProjectTagIds { get; set; }

        public override string ToString()
        {
            return AllocationRequestResources.ProjectTagsFilter;
        }
    }
}