﻿using Smt.Atomic.WebApp.Areas.Allocation.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ChartOrgUnitViewModel : IChartDropDownElement
    {
        public long Id { get; set; }

        public string Path { get; set; }

        public string Name { get; set; }

        public long? ManagerId { get; set; }

        public string ManagerFirstName { get; set; }

        public string ManagerLastName { get; set; }

        public override string ToString()
        {
            if (!ManagerId.HasValue || Name.Contains($"{ManagerFirstName} {ManagerLastName}"))
            {
                return Name;
            }

            return $"{Name} ({ManagerFirstName} {ManagerLastName})";
        }
    }
}