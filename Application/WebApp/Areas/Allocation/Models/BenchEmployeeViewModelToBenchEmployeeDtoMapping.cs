﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class BenchEmployeeViewModelToBenchEmployeeDtoMapping : ClassMapping<BenchEmployeeViewModel, BenchEmployeeDto>
    {
        public BenchEmployeeViewModelToBenchEmployeeDtoMapping()
        {
            Mapping =
                v =>
                    new BenchEmployeeDto
                    {
                        Id = v.Id,
                        FirstName = v.FirstName,
                        LastName = v.LastName,
                        LocationId = v.LocationId,
                        LineManagerId = v.LineManagerId,
                        ManagerId = v.ManagerId,
                        WeeksOnBench = v.WeeksOnBench,
                        StaffingStatus = v.StaffingStatus,
                        StaffingComment = v.StaffingComment
                    };
        }
    }
}
