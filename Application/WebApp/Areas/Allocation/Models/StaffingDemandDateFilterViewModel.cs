﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [DescriptionLocalized("EmployeeAllocationFilter", typeof(AllocationResources))]
    [Identifier("StaffingDemandDateFilter.DateRangeFilter")]
    public class StaffingDemandDateFilterViewModel : IValidatableObject
    {

        [DisplayNameLocalized(nameof(AllocationResources.From), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(AllocationResources.To), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        public DateTime To { get; set; }

        public override string ToString()
        {
            return string.Format(AllocationResources.FromTo, From.ToShortDateString(), To.ToShortDateString());
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (From > To)
            {
                issues.Add(new ValidationResult(AllocationResources.RangeFilterFromGreaterThanTo));
            }

            return issues;
        }
    }
}