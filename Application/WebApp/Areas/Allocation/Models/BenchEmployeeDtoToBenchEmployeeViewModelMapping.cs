﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class BenchEmployeeDtoToBenchEmployeeViewModelMapping : ClassMapping<BenchEmployeeDto, BenchEmployeeViewModel>
    {
        public BenchEmployeeDtoToBenchEmployeeViewModelMapping()
        {
            Mapping =
                d =>
                    new BenchEmployeeViewModel
                    {
                        Id = d.Id,
                        FirstName = d.FirstName,
                        LastName = d.LastName,
                        LocationId = d.LocationId,
                        LineManagerId = d.LineManagerId,
                        ManagerId = d.ManagerId,
                        WeeksOnBench = d.WeeksOnBench,
                        OnBenchSince = d.OnBenchSince,
                        StaffingStatus = d.StaffingStatus,
                        JobProfileIds = d.JobProfileIds,
                        SkillIds = d.SkillIds,
                        JobMatrixIds = d.JobMatrixIds,
                        StaffingComment = d.StaffingComment,
                        AvailableHours = d.AvailableHours,
                        CurrentDate = d.CurrentDate,
                        HoursAllocated = d.HoursAllocated,
                        PercentageAllocation = d.AvailableHours > decimal.Zero ? (100 * (d.HoursAllocated / d.AvailableHours)) : 0,
                        FillPercentage = d.FillPercentage,
                        ResumeModifiedOn = d.LastResumeModifiedOn,
                        OrgUnitFlatName = d.OrgUnitFlatName,
                        VacationBalance = d.VacationBalance
                    };
        }
    }
}
