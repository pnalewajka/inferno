﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectDtoToProjectViewModelMapping : ClassMapping<ProjectDto, ProjectViewModel>
    {
        public ProjectDtoToProjectViewModelMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = d => new ProjectViewModel
            {
                Id = d.Id,
                SetupId = d.SetupId,
                IsMainProject = d.IsMainProject,
                ClientProjectName = d.ClientProjectName,
                JiraIssueKey = d.JiraIssueKey,
                JiraKey = d.JiraKey,
                PetsCode = d.PetsCode,
                Apn = d.Apn,
                Region = d.Region,
                KupferwerkProjectId = d.KupferwerkProjectId,
                ProjectType = d.ProjectType,
                ProjectFeatures = d.ProjectFeatures,
                Description = d.Description,
                ClientShortName = d.ClientShortName,
                ProjectShortName = d.ProjectShortName,
                ProjectName = d.ProjectName,
                OrgUnitId = d.OrgUnitId,
                OrgUnitName = d.OrgUnitName,
                ProjectManagerId = d.ProjectManagerId,
                SupervisorManagerId = d.SupervisorManagerId,
                SalesAccountManagerId = d.SalesAccountManagerId,
                ProjectManagerFullName = d.ProjectManagerFullName,
                SupervisorFullName = d.SupervisorFullName,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                ProjectStatus = d.ProjectStatus,
                CalendarId = d.CalendarId,
                Color = d.Color,
                GenericName = d.GenericName,
                Timestamp = d.Timestamp,
                IndustryIds = d.IndustryIds == null ? null : d.IndustryIds.Any() ? d.IndustryIds.ToArray() : null,
                IsAvailableForSalesPresentation = d.IsAvailableForSalesPresentation,
                TechnologyIds = d.TechnologyIds == null ? null : d.TechnologyIds.Any() ? d.TechnologyIds.ToArray() : null,
                KeyTechnologyIds = d.KeyTechnologyIds == null ? null : d.KeyTechnologyIds.Any() ? d.KeyTechnologyIds.ToArray() : null,
                SoftwareCategoryIds = d.SoftwareCategoryIds == null ? null : d.SoftwareCategoryIds.Any() ? d.SoftwareCategoryIds.ToArray() : null,
                TagIds = d.TagIds == null ? null : d.TagIds.Any() ? d.TagIds.ToArray() : null,
                Picture = d.Picture == null
                    ? null
                    : new DocumentViewModel
                    {
                        ContentType = d.Picture.ContentType,
                        DocumentName = d.Picture.DocumentName,
                        DocumentId = d.Picture.DocumentId,
                    },
                ServiceLineIds = d.ServiceLineIds == null ? null : d.ServiceLineIds.Any() ? d.ServiceLineIds.ToArray() : null,
                CurrentPhase = d.CurrentPhase,
                Disclosures = d.Disclosures,
                HasClientReferences = d.HasClientReferences,
                CustomerSizeId = d.CustomerSizeId,
                BusinessCase = d.BusinessCase,
                IsJiraProject = d.IsJiraProject,
                ReferenceMaterialAttachments = d.ReferenceMaterialAttachments == null ? null : d.ReferenceMaterialAttachments
                        .Select(p => new DocumentViewModel
                        {
                            ContentType = p.ContentType,
                            DocumentName = p.DocumentName,
                            DocumentId = p.DocumentId
                        }).ToList(),
                ProjectInformationAttachments = d.ProjectInformationAttachments == null ? null : d.ProjectInformationAttachments
                        .Select(p => new DocumentViewModel
                        {
                            ContentType = p.ContentType,
                            DocumentName = p.DocumentName,
                            DocumentId = p.DocumentId
                        }).ToList(),
                OtherAttachments = d.OtherAttachments == null ? null : d.OtherAttachments
                        .Select(p => new DocumentViewModel
                        {
                            ContentType = p.ContentType,
                            DocumentName = p.DocumentName,
                            DocumentId = p.DocumentId
                        }).ToList(),
                TimeTrackingImportSourceId = d.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = d.AllowedOvertimeTypes,
                TimeReportAcceptingPersonIds = d.TimeReportAcceptingPersonIds == null ? null : d.TimeReportAcceptingPersonIds.Any() ? d.TimeReportAcceptingPersonIds.ToArray() : null,
                IsApprovedByRequesterMainManager = d.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = d.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = d.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = d.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = d.UtilizationCategoryId,
                Attributes = d.AttributesIds.ToList(),
                ReportingStartsOn = d.ReportingStartsOn,
                ReinvoicingToCustomer = d.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = d.ReinvoiceCurrencyId,
                CostApprovalPolicy = d.CostApprovalPolicy,
                MaxCostsCurrencyId = d.MaxCostsCurrencyId,
                MaxHotelCosts = d.MaxHotelCosts,
                MaxTotalFlightsCosts = d.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = d.MaxOtherTravelCosts,
                IsOwnProject = d.IsOwnProject,
                InvoicingRates = d.InvoicingRates == null ? null : d.InvoicingRates.Select(mappingFactory.CreateMapping<
                    ProjectInvoicingRateDto,
                    ProjectInvoicingRateViewModel>().CreateFromSource).ToList(),
                InvoicingAlgorithm = d.InvoicingAlgorithm,
                Acronym = d.Acronym,
                AllowedBonusTypes = d.AllowedBonusTypes,
            };
        }
    }
}
