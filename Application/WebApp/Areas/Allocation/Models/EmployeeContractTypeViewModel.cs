﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeContractTypeViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(EmployeeResources.AcronymLabel), nameof(EmployeeResources.AcronymLabelShort), typeof(EmployeeResources))]
        [Render(GridCssClass = "column-width-small")]
        public string Acronym { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeFirstNameLabel), typeof(EmployeeResources))]
        [Render(GridCssClass = "column-width-big")]
        public string FirstName { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLastNameLabel), typeof(EmployeeResources))]
        [Render(GridCssClass = "column-width-medium")]
        public string LastName { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeCompany), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [Render(GridCssClass = "column-width-medium")]
        public long? CompanyId { get; set; }

        [Order(5)]
        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractType), nameof(EmployeeResources.EmploymentPeriodContractTypeShort), typeof(EmployeeResources))]
        [NonSortable]
        public long? ContractTypeId { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLevelLabel), typeof(EmployeeResources))]
        [Render(GridCssClass = "column-width-big")]
        public string JobMatrixLevel { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [Render(GridCssClass = "column-width-big")]
        public long? LocationId { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        [Render(GridCssClass = "column-width-medium")]
        public long? LineManagerId { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodContractDuration), typeof(EmployeeResources))]
        public ContractDurationType? ContractDuration { get; set; }

        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentStartDateFullName), nameof(EmployeeResources.EmploymentStartDateShortName), typeof(EmployeeResources))]
        public DateTime? StartDate { get; set; }

        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentEndDateFullName), nameof(EmployeeResources.EmploymentEndDateShortName), typeof(EmployeeResources))]
        public DateTime? EndDate { get; set; }

        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentStartDateLabel), nameof(EmployeeResources.EmploymentStartDateLabelShort), typeof(EmployeeResources))]
        public DateTime? EmploymentStartDate { get; set; }

        [NonSortable]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentEndDateLabel), nameof(EmployeeResources.EmploymentEndDateLabelShort), typeof(EmployeeResources))]
        public DateTime? EmploymentEndDate { get; set; }

        [Visibility(VisibilityScope.None)]
        public EmployeeStatus CurrentEmployeeStatus { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeStatusLabel), nameof(EmployeeResources.EmployeeStatusShortLabel), typeof(EmployeeResources))]
        [Render(GridCssClass = "column-width-normal")]
        public string CurrentStatus { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentPeriodTimeReporting), nameof(EmployeeResources.EmploymentPeriodTimeReportingShort), typeof(EmployeeResources))]
        public TimeReportingMode? TimeReportingMode { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentMondayHours), nameof(EmployeeResources.EmploymentMondayHoursShort), typeof(EmployeeResources))]
        public decimal MondayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentTuesdayHours), nameof(EmployeeResources.EmploymentTuesdayHoursShort), typeof(EmployeeResources))]
        public decimal TuesdayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentWednesdayHours), nameof(EmployeeResources.EmploymentWednesdayHoursShort), typeof(EmployeeResources))]
        public decimal WednesdayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentThursdayHours), nameof(EmployeeResources.EmploymentThursdayHoursShort), typeof(EmployeeResources))]
        public decimal ThursdayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentFridayHours), nameof(EmployeeResources.EmploymentFridayHoursShort), typeof(EmployeeResources))]
        public decimal FridayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentSaturdayHours), nameof(EmployeeResources.EmploymentSaturdayHoursShort), typeof(EmployeeResources))]
        public decimal SaturdayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.EmploymentSundayHours), nameof(EmployeeResources.EmploymentSundayHoursShort), typeof(EmployeeResources))]
        public decimal SundayHours { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [DisplayNameLocalized(nameof(EmployeeResources.VacationRatio), nameof(EmployeeResources.VacationRatioShort), typeof(EmployeeResources))]
        public decimal VacationHourToDayRatio { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(EmployeeResources.VacationBalance), nameof(EmployeeResources.VacationBalanceShort), typeof(EmployeeResources))]
        public decimal EmployeeHourBalance { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.HourlyRateShort), typeof(EmployeeResources))]
        [RoleRequired(SecurityRoleType.CanViewEmployeeSalaryData)]
        public decimal HourlyRate { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.HourlyRateDomesticOutOfOfficeBonusShort), typeof(EmployeeResources))]
        [RoleRequired(SecurityRoleType.CanViewEmployeeSalaryData)]
        public decimal HourlyRateDomesticOutOfOfficeBonus { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.HourlyRateForeignOutOfOfficeBonusShort), typeof(EmployeeResources))]
        [RoleRequired(SecurityRoleType.CanViewEmployeeSalaryData)]
        public decimal HourlyRateForeignOutOfOfficeBonus { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.MonthlyRateShort), typeof(EmployeeResources))]
        [RoleRequired(SecurityRoleType.CanViewEmployeeSalaryData)]
        public decimal MonthlyRate { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.SalaryGrossShort), typeof(EmployeeResources))]
        [RoleRequired(SecurityRoleType.CanViewEmployeeSalaryData)]
        public decimal SalaryGross { get; set; }

        [NonSortable]
        [Render(GridCssClass = "numeric-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(EmployeeResources.AuthorRemunerationRatioShort), typeof(EmployeeResources))]
        [RoleRequired(SecurityRoleType.CanViewEmployeeSalaryData)]
        public decimal AuthorRemunerationRatio { get; set; }
    }
}