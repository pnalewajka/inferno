﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using Smt.Atomic.Business.Allocation.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [DescriptionLocalized("EmployeeAllocationUtilizationStatusFilter", typeof(AllocationResources))]
    [Identifier("EmployeeAllocationFilter.UtilizationStatusFilter")]
    public class EmployeeAllocationUtilizationStatusFilterViewModel : IValueAdjustingObject
    {
        [DisplayNameLocalized(nameof(AllocationResources.Week), typeof(AllocationResources))]
        [DataType(DataType.Date)]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime Week { get; set; }
        
        [DisplayNameLocalized(nameof(AllocationResources.AllocationStatus), typeof(AllocationResources))]
        [CheckboxGroup(ItemProvider = typeof(AllocationStatusListItemProvider))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] AllocationStatuses { get; set; }

        [ImportIgnore]
        [CustomControl(TemplatePath = "Areas.Allocation.Views.EmployeeAllocation.UtilizationStatusLegend")]
        public bool UtilizationStatusLegend { get; protected set; }

        public override string ToString()
        {
            var allocationStatuses = AllocationStatuses.Select(a => ((AllocationStatus) a).GetDescription()).ToArray();
            return string.Format(AllocationResources.AllocationUtilizationStatusFilterDescription,
                string.Join(", ", allocationStatuses), Week.GetPreviousDayOfWeek(DayOfWeek.Monday).ToShortDateString(),
                Week.GetNextDayOfWeek(DayOfWeek.Sunday).ToShortDateString());
        }

        public void OnModelUpdated()
        {
            Week = Week.GetPreviousDayOfWeek(DayOfWeek.Monday);
        }
    }
}