﻿using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Allocation.AdvancedAllocationRequestViewModel")]
    public class AdvancedAllocationRequestViewModel : AllocationRequestViewModel
    {
        [HiddenInput]
        public override decimal HoursPerDay { get; set; }

        public AdvancedAllocationRequestViewModel()
        {
            ContentType = AllocationRequestContentType.Advanced;
            StartDate = DateTime.Now;
        }

        public string GetFullName()
        {
            return string.Format(AllocationRequestResources.DeletingRecordsPopup, EmployeeFirstName, EmployeeLastName, ProjectCode).Trim();
        }

        public override string ToString()
        {
            return GetFullName();
        }
    }
}