﻿using System.ComponentModel;
using System.IO;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationRequestViewModelToAllocationRequestDtoMapping :
        ClassMapping<AllocationRequestViewModel, AllocationRequestDto>
    {
        public AllocationRequestViewModelToAllocationRequestDtoMapping()
        {
            Mapping = m => GetDto(m);
        }

        private static AllocationRequestDto GetDto(AllocationRequestViewModel viewModel)
        {
            var dto = new AllocationRequestDto();

            if (viewModel.ContentType == 0)
            {
                viewModel.ContentType = DetermineContentType(viewModel);
            }

            SetCommonFields(dto, viewModel);

            switch (viewModel.ContentType)
            {
                case AllocationRequestContentType.Simple:
                    SetHoursPerDay(dto, viewModel);
                    break;
                case AllocationRequestContentType.Advanced:
                    SetHoursFromDays(dto, viewModel);
                    break;
                default:
                    throw new InvalidEnumArgumentException(@"ContentType");
            }

            dto.EmployeeIds = viewModel.EmployeeIds ?? new long[0];

            return dto;
        }

        public static AllocationRequestContentType DetermineContentType(AllocationRequestViewModel viewModel)
        {
            var seemsSimple = viewModel.HoursPerDay > 0;
            var seemsAdvanced = viewModel.MondayHours > 0 || viewModel.TuesdayHours > 0 || viewModel.WednesdayHours > 0 ||
                                viewModel.ThursdayHours > 0 || viewModel.FridayHours > 0 || viewModel.SaturdayHours > 0 ||
                                viewModel.SundayHours > 0;

            if (seemsSimple && !seemsAdvanced)
            {
                return AllocationRequestContentType.Simple;
            }
            if (seemsAdvanced)
            {
                return AllocationRequestContentType.Advanced;
            }

            throw new InvalidDataException("Cannot determine content type based in data");
        }

        private static void SetCommonFields(AllocationRequestDto dto, AllocationRequestViewModel model)
        {
            dto.Id = model.Id;
            dto.EmployeeId = model.EmployeeId;
            dto.ProjectId = model.ProjectId;
            dto.StartDate = model.StartDate;
            dto.EndDate = model.EndDate;
            dto.ContentType = model.ContentType;
            dto.AllocationCertainty = model.AllocationCertainty;
            dto.EmployeeFirstName = model.EmployeeFirstName;
            dto.EmployeeLastName = model.EmployeeLastName;
            dto.ProjectName = model.ProjectCode;
            dto.ChangeDemand = model.ChangeDemand;
            dto.Comment = model.Comment;
            dto.Timestamp = model.Timestamp;
        }

        public static void SetHoursPerDay(AllocationRequestDto dto, AllocationRequestViewModel model)
        {
            dto.MondayHours = model.HoursPerDay;
            dto.TuesdayHours = model.HoursPerDay;
            dto.WednesdayHours = model.HoursPerDay;
            dto.ThursdayHours = model.HoursPerDay;
            dto.FridayHours = model.HoursPerDay;
            dto.SaturdayHours = 0;
            dto.SundayHours = 0;
        }

        public static void SetHoursFromDays(AllocationRequestDto dto, AllocationRequestViewModel model)
        {
            dto.MondayHours = model.MondayHours;
            dto.TuesdayHours = model.TuesdayHours;
            dto.WednesdayHours = model.WednesdayHours;
            dto.ThursdayHours = model.ThursdayHours;
            dto.FridayHours = model.FridayHours;
            dto.SaturdayHours = model.SaturdayHours;
            dto.SundayHours = model.SundayHours;
        }
    }
}