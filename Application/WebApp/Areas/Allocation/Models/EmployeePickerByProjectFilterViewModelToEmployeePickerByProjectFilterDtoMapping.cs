﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeePickerByProjectFilterViewModelToEmployeePickerByProjectFilterDtoMapping : ClassMapping<EmployeePickerByProjectFilterViewModel, EmployeePickerByProjectFilterDto>
    {
        public EmployeePickerByProjectFilterViewModelToEmployeePickerByProjectFilterDtoMapping()
        {
            Mapping = v => new EmployeePickerByProjectFilterDto
            {
                ProjectId = v.ProjectId,
                Date = v.Date
            };
        }
    }
}