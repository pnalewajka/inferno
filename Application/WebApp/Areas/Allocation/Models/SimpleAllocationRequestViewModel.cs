﻿using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Allocation.SimpleAllocationRequestViewModel")]
    public class SimpleAllocationRequestViewModel : AllocationRequestViewModel
    {
        [HiddenInput]
        public override decimal MondayHours { get; set; }
        [HiddenInput]
        public override decimal TuesdayHours { get; set; }
        [HiddenInput]
        public override decimal WednesdayHours { get; set; }
        [HiddenInput]
        public override decimal ThursdayHours { get; set; }
        [HiddenInput]
        public override decimal FridayHours { get; set; }
        [HiddenInput]
        public override decimal SaturdayHours { get; set; }
        [HiddenInput]
        public override decimal SundayHours { get; set; }

        public SimpleAllocationRequestViewModel()
        {
            ContentType = AllocationRequestContentType.Simple;
            StartDate = DateTime.Now;     
        }

        public string GetFullName()
        {
            return string.Format(AllocationRequestResources.DeletingRecordsPopup, EmployeeFirstName, EmployeeLastName, ProjectCode).Trim();
        }

        public override string ToString()
        {
            return GetFullName();
        }
    }
}