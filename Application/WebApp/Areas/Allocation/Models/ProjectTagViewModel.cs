﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Business.Common.Services;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectTagViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(0)]
        [Required]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(ProjectResources.NameLabel), typeof(ProjectResources))]
        public string Name { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(ProjectResources.DescriptionLabel), typeof(ProjectResources))]
        [NonSortable]
        public string Description { get; set; }

        [RequiredEnum(typeof(ProjectTagType))]
        [DisplayNameLocalized(nameof(ProjectResources.TagTypesLabel), typeof(ProjectResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<ProjectTagType>))]
        public ProjectTagType TagType { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
