﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeeViewModelToEmployeeDtoMapping : ClassMapping<EmployeeViewModel, EmployeeDto>
    {
        public EmployeeViewModelToEmployeeDtoMapping()
        {
            Mapping = v => new EmployeeDto
            {
                Id = v.Id,
                FirstName = v.FirstName,
                LastName = v.LastName,
                JobTitleId = v.JobTitleId,
                Email = v.Email,
                DateOfBirth = v.DateOfBirth,
                PersonalNumber = v.PersonalNumber,
                PersonalNumberType = v.PersonalNumberType,
                IdCardNumber = v.IdCardNumber,
                PhoneNumber = v.PhoneNumber,
                SkypeName = v.SkypeName,
                HomeAddress = v.HomeAddress,
                AvailabilityDetails = v.AvailabilityDetails,
                CurrentEmployeeStatus = v.CurrentEmployeeStatus,
                IsProjectContributor = v.IsProjectContributor,
                IsCoreAsset = v.IsCoreAsset,
                MinimumOvertimeBankBalance = v.MinimumOvertimeBankBalance,
                Timestamp = v.Timestamp,
                JobProfileIds = v.JobProfileIds != null && v.JobProfileIds.Length > 0 ? v.JobProfileIds : new long[] { },
                SkillIds = v.SkillIds != null && v.SkillIds.Length > 0 ? v.SkillIds : new long[] { },
                JobMatrixIds = v.JobMatrixId != null ? new[] { v.JobMatrixId.Value  } : new long[] { },
                LocationId = v.LocationId,
                PlaceOfWork = v.PlaceOfWork,
                CompanyId = v.CompanyId,
                UserId = v.UserId,
                OrgUnitId = v.OrgUnitId,
                LineManagerId = v.LineManagerId,
                JobProfileNames = v.JobProfileNames,
                LocationName = v.LocationName,
                JobTitleName = v.JobTitleName,
                OrgUnitName = v.OrgUnitName,
                LineManagerFullName = v.LineManagerFullName,
                Acronym = v.Acronym,
                EmployeeFeatures = v.EmployeeFeatures
            };
        }
    }
}
