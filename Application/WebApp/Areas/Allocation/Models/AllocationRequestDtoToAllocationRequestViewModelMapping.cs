﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationRequestDtoToAllocationRequestViewModelMapping : ClassMapping<AllocationRequestDto, AllocationRequestViewModel>
    {
        private const decimal DefaultHoursPerDay = 8;

        public AllocationRequestDtoToAllocationRequestViewModelMapping()
        {
            Mapping = d => GetViewModel(d);
        }

        public static AllocationRequestViewModel CreateViewModel(AllocationRequestContentType contentType)
        {
            switch (contentType)
            {
                case AllocationRequestContentType.Simple:
                    return new SimpleAllocationRequestViewModel
                    {
                        HoursPerDay = DefaultHoursPerDay
                    };

                case AllocationRequestContentType.Advanced:
                    return new AdvancedAllocationRequestViewModel
                    {
                        MondayHours = DefaultHoursPerDay,
                        TuesdayHours = DefaultHoursPerDay,
                        WednesdayHours = DefaultHoursPerDay,
                        ThursdayHours = DefaultHoursPerDay,
                        FridayHours = DefaultHoursPerDay
                    };

                default:
                    throw new ArgumentOutOfRangeException("contentType");
            }
        }

        public static Type GetViewModelType(AllocationRequestContentType contentType)
        {
            switch (contentType)
            {
                case AllocationRequestContentType.Simple:
                    return typeof(SimpleAllocationRequestViewModel);

                case AllocationRequestContentType.Advanced:
                    return typeof(AdvancedAllocationRequestViewModel);

                default:
                    throw new ArgumentOutOfRangeException("contentType");
            }
        }

        private static AllocationRequestViewModel GetViewModel(AllocationRequestDto dto)
        {
            var viewModel = CreateViewModel(dto.ContentType);
            SetValuesFromDto(dto, viewModel);

            return viewModel;
        }

        private static void SetValuesFromDto(AllocationRequestDto dto, AllocationRequestViewModel model)
        {
            model.Id = dto.Id;
            model.EmployeeId = dto.EmployeeId;
            model.ProjectId = dto.ProjectId;
            model.StartDate = dto.StartDate;
            model.EndDate = dto.EndDate;
            model.ContentType = dto.ContentType;
            model.AllocationCertainty = dto.AllocationCertainty;
            model.HoursPerDay =  dto.MondayHours;
            model.MondayHours = dto.MondayHours;
            model.TuesdayHours = dto.TuesdayHours;
            model.WednesdayHours = dto.WednesdayHours;
            model.ThursdayHours = dto.ThursdayHours;
            model.FridayHours = dto.FridayHours;
            model.SaturdayHours = dto.SaturdayHours;
            model.SundayHours = dto.SundayHours;
            model.EmployeeFirstName = dto.EmployeeFirstName;
            model.EmployeeLastName = dto.EmployeeLastName;
            model.ProjectCode = dto.ProjectName;
            model.ProcessingStatus = dto.ProcessingStatus;
            model.ChangeDemand = dto.ChangeDemand;
            model.Comment = dto.Comment;
            model.Timestamp = dto.Timestamp;
        }
    }
}
