﻿using System;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectClientViewModel
    {
        public long Id { get; set; }

        public string ClientName { get; set; }

        public override string ToString()
        {
            return ClientName;
        }
    }
}
