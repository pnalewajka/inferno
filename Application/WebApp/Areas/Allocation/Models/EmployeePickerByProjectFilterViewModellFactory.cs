﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeePickerByProjectFilterViewModellFactory : IViewModelFactory<EmployeePickerByProjectFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public EmployeePickerByProjectFilterViewModellFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }
        public EmployeePickerByProjectFilterViewModel Create()
        {
            return new EmployeePickerByProjectFilterViewModel
            {
                Date = _timeService.GetCurrentDate()
            };
        }
    }
}