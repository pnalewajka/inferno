﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectFilterViewModelFactory : IViewModelFactory<ProjectFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public ProjectFilterViewModelFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public ProjectFilterViewModel Create()
        {
            return new ProjectFilterViewModel
            {
                From = _timeService.GetCurrentDate(),
                To = _timeService.GetCurrentDate().AddDays(30)
            };
        }
    }
}