using System;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class OverAllocatedWarningViewModel
    {
        public EmployeeViewModel Employee { get; set; }

        public long? ProjectId { get; set; }

        public DateTime? Period { get; set; }
    }
}