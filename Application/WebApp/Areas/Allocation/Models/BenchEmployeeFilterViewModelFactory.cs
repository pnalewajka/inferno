﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class BenchEmployeeFilterViewModelFactory : IViewModelFactory<BenchEmployeeFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public BenchEmployeeFilterViewModelFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public BenchEmployeeFilterViewModel Create()
        {
            return new BenchEmployeeFilterViewModel
            {
                Week = _timeService.GetCurrentDate()
            };
        }
    }
}