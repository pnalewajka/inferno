﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.EmploymentPeriodDateFilter")]
    [DescriptionLocalized("EmploymentPeriodDateRangeFilterTitle", typeof(EmployeeResources))]
    public class EmploymentPeriodDateFilterViewModel : IValidatableObject
    {
        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.DateFromLabel), typeof(EmployeeResources))]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? From { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(EmployeeResources.DateToLabel), typeof(EmployeeResources))]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? To { get; set; }

        public override string ToString()
        {
            var fromValue = From?.ToShortDateString() ?? string.Empty;
            var toValue = To?.ToShortDateString() ?? string.Empty;

            return string.Format(EmployeeResources.EmploymentFromTo, fromValue, toValue);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            if (From > To)
            {
                validationResults.Add(new ValidationResult(AllocationResources.ErrorFromDateIsLaterThanToDate));
            }

            return validationResults;
        }

        public EmploymentPeriodDateFilterViewModel()
        {
            From = DateTime.UtcNow;
            To = null;
        }
    }
}
