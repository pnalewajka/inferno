﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class StaffingDemandDateFilterViewModelFactory : IViewModelFactory<StaffingDemandDateFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public StaffingDemandDateFilterViewModelFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public StaffingDemandDateFilterViewModel Create()
        {
            return new StaffingDemandDateFilterViewModel
            {
                From = _timeService.GetCurrentDate(),
                To = _timeService.GetCurrentDate()
            };
        }
    }
}