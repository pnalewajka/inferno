﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;
using static Smt.Atomic.WebApp.Areas.Allocation.Models.EmployeeAllocationViewModel;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class AllocationBlockDtoToAllocationBlockViewModelMapping: ClassMapping<AllocationBlockDto, AllocationBlockViewModel>
    {
        public AllocationBlockDtoToAllocationBlockViewModelMapping()
        {
            Mapping = x => new AllocationBlockViewModel
            {
                RequestId = x.RequestId,
                AllocationName = x.AllocationName,
                AllocationDayHours = x.AllocationDayHours,
                Certainty = x.Certainty,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                PeriodEndDate = x.PeriodEndDate,
                PeriodStartDate = x.PeriodStartDate,
                ProjectId = x.ProjectId,
                ProjectName = x.ProjectName,
                ProjectAcronym = x.ProjectAcronym,
                ProjectShortName = x.ProjectShortName,
                ProjectColor = x.ProjectColor,
                ProjectColorIsDark = x.ProjectColorIsDark,
                ProjectPictureId = x.ProjectPictureId,
                ClientShortName = x.ClientShortName,
                JiraIssueKey = x.JiraIssueKey,
                JiraKey = x.JiraKey,
                EmployeeId = x.EmployeeId,
                EmployeeAcronym = x.EmployeeAcronym,
                EmployeeName = x.EmployeeName,
                ProcessingStatus = x.ProcessingStatus,
                ContentType = x.ContentType,
            };
        }
    }
}
