﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Allocation.AllocationRequestImportViewModel")]
    [ModelBinder(typeof(AllocationRequestViewModelBinder))]
    public class AllocationRequestImportViewModel : AllocationRequestViewModel
    {
        [Visibility(VisibilityScope.None)]
        [ImportIgnore]
        public new long EmployeeId { get; set; }

        [Visibility(VisibilityScope.None)]
        public new long ProjectId { get; set; }

        [DisplayNameLocalized(nameof(AllocationRequestResources.EmployeeLabel), typeof(AllocationRequestResources))]
        [Required]
        [Order(0)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public string EmployeeEmail { get; set; }

        [DisplayNameLocalized(nameof(AllocationRequestResources.ProjectLabel), typeof(AllocationRequestResources))]
        [Required]
        [Order(1)]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public new string ProjectCode { get; set; }
    }
}
