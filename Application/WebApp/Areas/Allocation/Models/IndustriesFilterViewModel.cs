﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using System;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.IndustriesFilter")]
    public class IndustriesFilterViewModel
    {
        [DisplayNameLocalized(nameof(DictionaryResources.IndustryTitle), typeof(DictionaryResources))]
        [ValuePicker(Type = typeof(IndustryController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] IndustryIds { get; set; }

        public override string ToString()
        {
            return ProjectResources.FilterByIndustryTitle;
        }
    }
}
