﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class ProjectReportParametersDtoToProjectReportParametersViewModelMapping : ClassMapping<ProjectReportParametersDto, ProjectReportParametersViewModel>
    {
        public ProjectReportParametersDtoToProjectReportParametersViewModelMapping()
        {
            Mapping = d => new ProjectReportParametersViewModel
            {
                ProjectIds = d.ProjectIds
            };
        }
    }
}
