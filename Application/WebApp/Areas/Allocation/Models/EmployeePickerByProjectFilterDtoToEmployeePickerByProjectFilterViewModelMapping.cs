﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    public class EmployeePickerByProjectFilterDtoToEmployeePickerByProjectFilterViewModelMapping : ClassMapping<EmployeePickerByProjectFilterDto, EmployeePickerByProjectFilterViewModel>
    {
        public EmployeePickerByProjectFilterDtoToEmployeePickerByProjectFilterViewModelMapping()
        {
            Mapping = d => new EmployeePickerByProjectFilterViewModel
            {
                ProjectId = d.ProjectId,
                Date = d.Date
            };
        }
    }
}