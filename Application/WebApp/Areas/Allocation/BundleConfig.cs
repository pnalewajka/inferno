﻿using System;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Allocation
{
    public class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            var bundle = new ScriptBundle("~/scripts/allocation-react")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder(false)
            };

            bundle.IncludeDirectory("~/Areas/Allocation/Scripts/AllocationScreen", "*.js", true);

            bundles.Add(bundle);
        }
    }
}
