﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.WebApp.Areas.Allocation.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public class ChartAllocationFilterTokenResolver : IUrlTokenResolver
    {
        private readonly IOrgUnitCardIndexDataService _orgUnitService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;

        public ChartAllocationFilterTokenResolver(IPrincipalProvider principalProvider, IOrgUnitCardIndexDataService orgUnitService, ITimeService timeService, ISystemParameterService systemParameterService)
        {
            _orgUnitService = orgUnitService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
        }

        public IEnumerable<string> GetTokens()
        {
            yield return "%CHART-ALLOCATION-SCOPE-FILTER%";
        }

        public string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            var filterParts = new List<string>
            {
                OrgUnitFilter(),
                DateRangeFilter()
            }
            .Where(s => !string.IsNullOrEmpty(s));
            
            return string.Join("&", filterParts);
        }

        private string DateRangeFilter()
        {
            var fromAdjustMonths =_systemParameterService.GetParameter<int>(ParameterKeys.AllocationChartMonthsPastDefault);
            var toAdjustMonths = _systemParameterService.GetParameter<int>(ParameterKeys.AllocationChartMonthsFutureDefault);

            var converterFormat = new DateOnlyDateTimeConverter().DateTimeFormat;
            var currentDate = _timeService.GetCurrentDate();
            var fromDate = DateHelper.BeginningOfMonth(currentDate.AddMonths(fromAdjustMonths));
            var toDate = DateHelper.EndOfMonth(currentDate.AddMonths(toAdjustMonths));

            return $"{nameof(AllocationChartSearchViewModel.DateFrom)}={fromDate.ToString(converterFormat)}&{nameof(AllocationChartSearchViewModel.DateTo)}={toDate.ToString(converterFormat)}";
        }

        private string OrgUnitFilter()
        {
            var chartOrgUnit = _orgUnitService.ChartOrgUnits(OrgUnitFeatures.ShowChart).FirstOrDefault();

            if (chartOrgUnit == null)
            {
                return string.Empty;
            }

            return $"{nameof(AllocationChartSearchViewModel.OrgUnitIds)}={chartOrgUnit.Id}";
        }
    }
}