﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public class EmployeeFilterTokenResolver : FilterTokenResolver
    {
        private const string EmployeeScopeFilter = "%EMPLOYEE-SCOPE-FILTER%";
        private IPrincipalProvider _principalProvider;

        public EmployeeFilterTokenResolver(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService,
            IEmployeeService employeeService, IProjectService projectService)
            : base(principalProvider, orgUnitService, employeeService, projectService)
        {
            _principalProvider = principalProvider;
        }

        public override IEnumerable<string> GetTokens()
        {
            yield return EmployeeScopeFilter;
        }

        public override string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            string filter = ResolveProjectAndEmployeeFilters(SecurityRoleType.CanScopeEmployeesToAllEmployeesByDefault,
                          roleDependentFilter: FilterCodes.AllEmployees,
                          defaultFilter: FilterCodes.AllEmployees,
                          filterWhenUserHasEmployees: FilterCodes.MyEmployees);

            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
            {
                filter = $"{filter},{FilterCodes.ActiveEmployees}";
            }

            return ExtendFilterWithOrgUnitFilter(filter, SecurityRoleType.CanScopeEmployeesToOwnOrgUnitsByDefault);
        }
    }
}