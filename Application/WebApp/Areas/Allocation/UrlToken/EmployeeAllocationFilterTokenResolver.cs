﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public class EmployeeAllocationFilterTokenResolver : FilterTokenResolver
    {
        private const string EmployeeAllocationScopeFilter = "%EMPLOYEE-ALLOCATION-SCOPE-FILTER%";

        public EmployeeAllocationFilterTokenResolver(IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService, IEmployeeService employeeService, IProjectService projectService)
            : base(principalProvider, orgUnitService, employeeService, projectService)
        {
        }

        public override IEnumerable<string> GetTokens()
        {
            yield return EmployeeAllocationScopeFilter;
        }

        public override string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            var filter = ResolveProjectAndEmployeeFilters(SecurityRoleType.CanScopeEmployeeAllocationsToAllAllocationsByDefault,
                roleDependentFilter: FilterCodes.AllAllocations,
                defaultFilter: FilterCodes.AllAllocations,
                filterWhenUserHasEmployees: FilterCodes.MyEmployees);

            return ExtendFilterWithOrgUnitFilter(filter,
                SecurityRoleType.CanScopeEmployeeAllocationsToOwnOrgUnitsByDefault);
        }
    }
}