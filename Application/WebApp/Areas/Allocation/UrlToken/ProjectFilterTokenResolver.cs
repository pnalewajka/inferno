﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public class ProjectFilterTokenResolver : FilterTokenResolver
    {
        private const string ProjectScopeFilter = "%PROJECT-SCOPE-FILTER%";

        public ProjectFilterTokenResolver(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService,
            IEmployeeService employeeService, IProjectService projectService)
            : base(principalProvider, orgUnitService, employeeService, projectService)
        {
        }

        public override IEnumerable<string> GetTokens()
        {
            yield return ProjectScopeFilter;
        }

        public override string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            var parameters = ResolveProjectAndEmployeeFilters(SecurityRoleType.CanScopeProjectsToAllProjectsByDefault,
                roleDependentFilter: FilterCodes.AllProjects,
                defaultFilter: FilterCodes.AllProjects,
                filterWhenUserHasProjects: FilterCodes.MyProjects);

            if (parameters != null)
            {
                return parameters + ",active-projects";
            }

            return "active-projects";
        }
    }
}