﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public class BenchEmployeesFilterTokenResolver : FilterTokenResolver
    {
        private const string BenchEmployeesScopeFilter = "%EMPLOYEE-BENCH-SCOPE-FILTER%";

        public BenchEmployeesFilterTokenResolver(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService,
            IEmployeeService employeeService, IProjectService projectService)
            : base(principalProvider, orgUnitService, employeeService, projectService)
        {
        }

        public override IEnumerable<string> GetTokens()
        {
            yield return BenchEmployeesScopeFilter;
        }

        public override string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            var filter = string.Empty;
            filter = ExtendFilterWithOrgUnitFilter(filter, SecurityRoleType.CanScopeBenchEmployeesToOwnOrgUnitsByDefault);

            return filter;
        }
    }
}