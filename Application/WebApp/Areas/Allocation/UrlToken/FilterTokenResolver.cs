﻿using System.Collections.Generic;
using Castle.Core.Internal;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.Business.Allocation.Consts;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public abstract class FilterTokenResolver : IUrlTokenResolver
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IEmployeeService _employeeService;
        private readonly IProjectService _projectService;

        protected FilterTokenResolver(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService, IEmployeeService employeeService, IProjectService projectService)
        {
            _principalProvider = principalProvider;
            _orgUnitService = orgUnitService;
            _employeeService = employeeService;
            _projectService = projectService;
        }

        public abstract IEnumerable<string> GetTokens();

        public abstract string GetTokenValue(string token);

        public string ResolveProjectAndEmployeeFilters(SecurityRoleType role, string roleDependentFilter,
           string defaultFilter, string filterWhenUserHasEmployees = null, string filterWhenUserHasProjects = null)
        {
            var currentPrincipal = GetCurrentPrincipal();

            if (currentPrincipal.IsInRole(role))
            {
                return roleDependentFilter;
            }

            if (filterWhenUserHasEmployees != null && currentPrincipal.EmployeeId > 0 && HasEmployees())
            {
                return filterWhenUserHasEmployees;
            }

            if (filterWhenUserHasProjects != null && currentPrincipal.EmployeeId > 0 && HasProjects())
            {
                return filterWhenUserHasProjects;
            }

            return defaultFilter;
        }

        private IAtomicPrincipal GetCurrentPrincipal()
        {
            // for the sake of early-in-the-call exception handling
            return _principalProvider.IsSet
                ? _principalProvider.Current
                : AtomicPrincipal.Anonymous;
        }

        public string ExtendFilterWithOrgUnitFilter(string filter, SecurityRoleType requiredRole)
        {
            var orgUnitFilter = GetOrgUnitFilter(requiredRole);

            if (!string.IsNullOrEmpty(orgUnitFilter))
            {
                if (string.IsNullOrEmpty(filter))
                {
                    return orgUnitFilter;
                }

                return $"{filter},{orgUnitFilter}";
            }

            return filter;
        }

        private string GetOrgUnitFilter(SecurityRoleType requiredRole)
        {
            var currentPrincipal = GetCurrentPrincipal();

            if (currentPrincipal.IsInRole(requiredRole))
            {
                var orgUnitIds = _orgUnitService.GetOrgUnitIdsForAllocationByEmployeeId(currentPrincipal.EmployeeId);

                if (orgUnitIds.IsNullOrEmpty())
                {
                    return string.Empty;
                }

                var orgUnitIdsName = NamingConventionHelper.ConvertPascalCaseToHyphenated(nameof(OrgUnitFilterViewModel.OrgUnitIds));
                var filter = $"{FilterCodes.OrgUnitsFilter}&{FilterCodes.OrgUnitsFilter}.{orgUnitIdsName}=";
                var orgUnitIdsJoined = string.Join(",", orgUnitIds);
                return $"{filter}{orgUnitIdsJoined}";
            }

            return string.Empty;
        }

        private bool HasEmployees()
        {
            var currentPrincipal = GetCurrentPrincipal();

            return _employeeService.HasEmployees(currentPrincipal.EmployeeId);
        }

        private bool HasProjects()
        {
            var currentPrincipal = GetCurrentPrincipal();

            return _projectService.HasProjects(currentPrincipal.EmployeeId);
        }
    }
}
