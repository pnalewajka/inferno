﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Allocation.UrlToken
{
    public class AllocationRequestFilterTokenResolver : FilterTokenResolver
    {
        private const string AllocationRequestScopeFilter = "%ALLOCATION-REQUEST-SCOPE-FILTER%";

        public AllocationRequestFilterTokenResolver(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService,
            IEmployeeService employeeService, IProjectService projectService)
            : base(principalProvider, orgUnitService, employeeService, projectService)
        {
        }

        public override IEnumerable<string> GetTokens()
        {
            yield return AllocationRequestScopeFilter;
        }

        public override string GetTokenValue(string token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            var filter = ResolveProjectAndEmployeeFilters(SecurityRoleType.CanScopeAllocationRequestsToAllEmployeesByDefault,
                roleDependentFilter: FilterCodes.AllEmployees,
                defaultFilter: FilterCodes.AllEmployees,
                filterWhenUserHasEmployees: FilterCodes.MyEmployees);

            return ExtendFilterWithOrgUnitFilter(filter, SecurityRoleType.CanScopeAllocationRequestsToOwnOrgUnitsByDefault);
        }
    }
}
