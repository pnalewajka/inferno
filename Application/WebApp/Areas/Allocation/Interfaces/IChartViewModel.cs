﻿using System;
using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.Allocation.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Interfaces
{
    public interface IChartViewModel
    {
        long? SelectedItemId { get; }

        DateTime CurrentWeek { get; }

        IReadOnlyCollection<IChartDropDownElement> DropdownItems { get; }

        IReadOnlyCollection<ChartDropDownViewModel> Charts { get; }
    }
}