﻿namespace Smt.Atomic.WebApp.Areas.Allocation.Interfaces
{
    public interface IChartDropDownElement
    {
        long Id { get; }

        string Name { get; }

        string Path { get; }

        string ToString();
    }
}