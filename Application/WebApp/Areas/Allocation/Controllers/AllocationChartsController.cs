﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Models;
using Smt.Atomic.WebApp.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployeeAllocationList)]
    public class AllocationChartsController : AtomicController
    {
        private readonly IEmployeeAllocationCardIndexDataService _cardIndexDataService;
        private readonly IOrgUnitCardIndexDataService _orgUnitCardIndexDataService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ITimeService _timeService;
        private readonly IEmployeeCardIndexDataService _employeeCardIndexDataService;
        private readonly IClassMapping<OrgUnitDto, OrgUnitViewModel> _orgUnitClassMapping;

        public AllocationChartsController(IBaseControllerDependencies baseDependencies,
            IEmployeeAllocationCardIndexDataService cardIndexDataService,
            IOrgUnitCardIndexDataService orgUnitCardIndexDataService,
            IOrgUnitService orgUnitService,
            ITimeService timeService,
            IEmployeeCardIndexDataService employeeCardIndexDataService,
            IClassMapping<OrgUnitDto, OrgUnitViewModel> orgUnitClassMapping
            ) : base(baseDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _orgUnitCardIndexDataService = orgUnitCardIndexDataService;
            _timeService = timeService;
            _employeeCardIndexDataService = employeeCardIndexDataService;
            _orgUnitClassMapping = orgUnitClassMapping;
            _orgUnitService = orgUnitService;

            Layout.Scripts.Add("~/Scripts/d3/d3.min.js");
            Layout.Scripts.Add("~/Scripts/Dante/Utils.js");
            Layout.Scripts.Add("~/Scripts/Chart/ReportCharts.js");
            Layout.Css.Add("~/Content/ReportCharts.css");
        }

        public ActionResult Index(AllocationChartSearchViewModel model)
        {
            Layout.Scripts.Add("~/Scripts/Chart/ReportDetailsCharts.js");

            Layout.PageHeader = EmployeeAllocationChartsResources.PageTitle;
            Layout.PageTitle = EmployeeAllocationChartsResources.PageTitle;
            Layout.Resources.AddFrom<CardIndexResources>(nameof(CardIndexResources.RecordCountFormat));

            var currentWeek = _timeService.GetCurrentDate().GetPreviousDayOfWeek(DayOfWeek.Monday);

            var allocations = _cardIndexDataService.CalculateWeekAllocations(GetAllocationChartSearchDto(model));

            return View(new EmployeeAllocationChartViewModel(model, allocations, currentWeek));
        }

        public PartialViewResult ProfileDetails(AllocationChartSearchViewModel searchModel, [ModelBinder(typeof(DateTimeModelBinder))]DateTime period)
        {
            var chartDetails = _cardIndexDataService.JobProfileChartDetails(GetAllocationChartSearchDto(searchModel), period).ToList();
            var orgUnitViewModel = string.Join(", ", _orgUnitService.GetOrgUnitNames(searchModel.OrgUnitIds).Select(o => o.Value));

            var model = new ModelBasedDialogViewModel<JobProfileChartViewModel>(new JobProfileChartViewModel(chartDetails) { Period = period, OrgUnitIds = searchModel.OrgUnitIds }, StandardButtons.None)
            {
                Title = string.Format(ProfileDetailsResources.DialogTitleFormat, orgUnitViewModel, period.ToShortDateString())
            };

            return PartialView("ProfileDetails", model);
        }

        private AllocationChartSearchDto GetAllocationChartSearchDto(AllocationChartSearchViewModel model)
        {
            return new AllocationChartSearchDto
            {
                OrgUnitIds = model.OrgUnitIds,
                CountryIds = model.CountryIds,
                LocationIds = model.LocationIds,
                DateFrom = model.DateFrom,
                DateTo = model.DateTo
            };
        }
    }
}
