﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Enums;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Resources;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.Presentation.Renderers.Attributes;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    public abstract class EmployeeAllocationBaseController<TCardIndexDataService> : CardIndexController<EmployeeAllocationViewModel, EmployeeAllocationDto, EmployeeAllocationContext>
        where TCardIndexDataService : IEmployeeAllocationBaseCardIndexDataService
    {
        private const string MinimalViewId = "minimal";
        private const string ExtendedViewId = "extended";
        protected readonly TCardIndexDataService CardIndexDataService;

        protected EmployeeAllocationBaseController(TCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndexDataService = cardIndexDataService;

            CardIndex.Settings.PageSize = 50;

            CardIndex.Settings.DisplayMode = GridDisplayMode.Custom;
            CardIndex.Settings.CustomGridDisplayModeViewName = "_CustomTable";
            CardIndex.Settings.Title = AllocationResources.EmployeeWeeklyAllocationTitle;
            CardIndex.Settings.ListViewName = "~/Areas/Allocation/Views/EmployeeAllocation/List.cshtml";

            Layout.Scripts.Add("~/Areas/Allocation/Scripts/EmployeeAllocation.js");
            Layout.Css.Add("~/Areas/Allocation/Content/EmployeeAllocation.css");

            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();
            CardIndex.Settings.FilterGroups.AddRange(GetEmployeeFilterGroup(AllocationResources.EmployeeFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(GetOrganizationFilterGroup(AllocationResources.OrgFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(GetScopeFilterGroup(AllocationResources.ScopeFilterTitle));

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodesAllocation.FirstName,  EmployeeResources.EmployeeFirstNameLabel),
                new SearchArea(SearchAreaCodesAllocation.LastName,  EmployeeResources.EmployeeLastNameLabel),
                new SearchArea(SearchAreaCodesAllocation.Email, EmployeeResources.Email),
                new SearchArea(SearchAreaCodesAllocation.LocationName, EmployeeResources.EmployeeLocationLabel, false),
                new SearchArea(SearchAreaCodesAllocation.LineMaganerLastName, AddEmployeeRequestResources.LineManagerLabel),
                new SearchArea(SearchAreaCodesAllocation.Skills, AllocationResources.EmployeeSkillsFilterName, false),
                new SearchArea(SearchAreaCodesAllocation.JobProfileName, AddEmployeeRequestResources.JobProfileLabel),
                new SearchArea(SearchAreaCodesAllocation.JobTitle, AddEmployeeRequestResources.JobTitleLabel),
                new SearchArea(SearchAreaCodesAllocation.AllocationStatus, AllocationResources.AllocationStatus, false),
                new SearchArea(SearchAreaCodesAllocation.ProjectName, AllocationResources.ProjectLabel, false),
            };
        }

        [BreadcrumbBar("EmployeeAllocation")]
        public override ActionResult List(GridParameters parameters)
        {
            var context = Context ?? new EmployeeAllocationContext();

            if (!Context.Beta)
            {
                if (context.PeriodType == EmployeeAllocationPeriodType.Day)
                {
                    CardIndex.Settings.Title = AllocationResources.EmployeeDailyAllocationTitle;
                    CardIndex.Settings.Header = AllocationResources.EmployeeDailyAllocationHeader;

                    CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
                    {
                        Icon = FontAwesome.Calendar,
                        Text = AllocationResources.ShowWeeklyAllocations,
                        OnClickAction =
                            new JavaScriptCallAction(
                                $"EmployeeAllocation.changeAllocationPeriodType('{EnumHelper.ToUrlString(EmployeeAllocationPeriodType.Week)}')")
                    });
                }
                else
                {
                    CardIndex.Settings.Title = AllocationResources.EmployeeWeeklyAllocationTitle;
                    CardIndex.Settings.Header = AllocationResources.EmployeeWeeklyAllocationHeader;

                    CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
                    {

                        Icon = FontAwesome.Calendar,
                        Text = AllocationResources.ShowDailyAllocations,
                        OnClickAction =
                            new JavaScriptCallAction(
                                $"EmployeeAllocation.changeAllocationPeriodType('{EnumHelper.ToUrlString(EmployeeAllocationPeriodType.Day)}')")
                    });
                }
            }

            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretLeft, LayoutResources.PreviousMonthShort, -1));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.MapMarker, LayoutResources.CurrentMonth, null));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretRight, LayoutResources.NextMonthShort, 1));

            return base.List(parameters);
        }

        private CommandButton CreateMonthChangeButton(string icon, string text, int? monthsToAdd)
        {
            var fromDate = CardIndexDataService.GetDefaultFromValue().ToString("yyyy-MM-dd");
            var toDate = CardIndexDataService.GetDefaultToValue().ToString("yyyy-MM-dd");

            var months = monthsToAdd?.ToString() ?? "null";

            return new ToolbarButton
            {
                Group = "date-navigation",
                Icon = icon,
                Text = text,
                OnClickAction = new JavaScriptCallAction($"EmployeeAllocation.changeFilterDate({months}, new Date('{fromDate}'), new Date('{toDate}'))")
            };
        }

        private IEnumerable<FilterGroup> GetEmployeeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = AllocationResources.EmployeeFilterTitle,
                DisplayName = AllocationResources.Competences,
                Type = FilterGroupType.AndCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = GetCompetencesFilters().ToList()
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeeStatus,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmployeeStatus>()
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmploymentType,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmploymentType>()
            };
        }

        protected virtual IEnumerable<Filter> GetCompetencesFilters()
        {
            yield return new Filter
            {
                Code = FilterCodes.ProjectContributorFilter,
                DisplayName = AllocationResources.EmployeeProjectContributor
            };
        }

        protected virtual IEnumerable<FilterGroup> GetOrganizationFilterGroup(string groupName)
        {
            return Enumerable.Empty<FilterGroup>();
        }

        private IEnumerable<FilterGroup> GetScopeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.SetRanges,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<EmployeeAllocationFilterViewModel>(FilterCodes.DateRange,
                        AllocationResources.WeekRanges),
                    new ParametricFilter<EmployeeAllocationUtilizationStatusFilterViewModel>(
                        FilterCodes.UtilizationFilter, AllocationResources.UtilizationStatusTitle),
                    new ParametricFilter<AllocationRequestProjectsAndEmployeesFilterViewModel>(
                        FilterCodes.EmployeesAndProjects, AllocationRequestResources.EmployeesAndProjectsFilter),
                    new ParametricFilter<AllocationRequestProjectTagsFilterViewModel>(
                        FilterCodes.ProjectTags, AllocationRequestResources.ProjectTagsFilter),
                },
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.ManagerFilter,
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = GetManagerFilters()
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeeCompany,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<CompanyFilterViewModel>(FilterCodes.CompanyFilter, AllocationResources.EmployeeCompanyFilterName)
                }
            };
        }

        protected virtual List<Filter> GetManagerFilters()
        {
            return new List<Filter>
            {
                new Filter {Code = FilterCodes.MyProjects, DisplayName = AllocationResources.MyProjects},
                new Filter {Code = FilterCodes.MyEmployees, DisplayName = AllocationResources.MyEmployees},
                new Filter { Code = FilterCodes.MyDirectEmployees, DisplayName = EmployeeResources.MyDirectEmployees },
                new Filter {Code = FilterCodes.MyTeamAllocations, DisplayName = AllocationResources.MyTeamAllocations},
                new Filter {Code = FilterCodes.AllAllocations, DisplayName = AllocationResources.AllAllocations},
                new Filter {Code = FilterCodes.MyProjectGuestsFilter, DisplayName = AllocationResources.MyProjectGuests},
                new Filter {Code = FilterCodes.MyEmployeesVisitingFilter, DisplayName = AllocationResources.MyEmployeesVisiting}
            };
        }

        private static IList<IGridViewConfiguration> GetGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var configuration =
                new GridViewConfiguration<EmployeeAllocationViewModel>(AllocationResources.MinimalViewText,
                    MinimalViewId)
                {
                    IsDefault = true
                };

            configuration.IncludeColumns(new List<Expression<Func<EmployeeAllocationViewModel, object>>>
            {
                c => c.FullName
            });

            list.Add(configuration);

            configuration = new GridViewConfiguration<EmployeeAllocationViewModel>(
                AllocationResources.ExtendedViewText, ExtendedViewId);
            configuration.IncludeColumns(new List<Expression<Func<EmployeeAllocationViewModel, object>>>
            {
                c => c.FullName,
                c => c.LocationId,
                c => c.JobProfileId,
                c => c.LocationId,
                c => c.SkillIds,
                c => c.JobMatrixId,
                c => c.LineManagerId,
                c => c.OrgUnitId,
            });

            list.Add(configuration);

            return list;
        }
    }
}