﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("ValuePickers.ProjectTag")]
    public class ProjectTagPickerController : ProjectTagController
    {
        public ProjectTagPickerController(IProjectTagCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}