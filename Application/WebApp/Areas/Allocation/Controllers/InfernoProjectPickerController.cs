﻿using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("EmployeeValuePickers.InfernoProject")]
    public class InfernoProjectPickerController : ProjectPickerController
    {
        public InfernoProjectPickerController(IProjectCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.ToolbarButtons.RemoveAll(t => t.Text == ProjectResources.PickerAddButton);
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                ButtonText = ProjectResources.ProjectFilterButton,
                Visibility = CommandButtonVisibility.ValuePicker,
                Filters =
                    new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.InfernoProjectsFilter,
                            CanBeSwitchedOff = false,
                            DisplayName = ProjectResources.InfernoLabel
                        }
                    }
            });
        }
    }
}