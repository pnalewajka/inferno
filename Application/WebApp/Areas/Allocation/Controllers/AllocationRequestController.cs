﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{

    [AtomicAuthorize(SecurityRoleType.CanViewAllocationRequest)]
    public class AllocationRequestController : CardIndexController<AllocationRequestViewModel, AllocationRequestDto>
    {
        private readonly IAllocationRequestCardIndexDataService _allocationRequestCardIndexDataService;
        private readonly IEmployeeAllocationCardIndexDataService _employeeAllocationCardIndexDataService;
        private readonly IClassMapping<AllocationRequestViewModel, AllocationRequestDto> _allocationRequestClassMapping;
        private readonly IClassMapping<AllocationRequestDto, AllocationRequestViewModel> _allocationRequestFromDtoClassMapping;
        private readonly IClassMapping<EmployeeDto, EmployeeViewModel> _employeeClassMapping;
        public const string ContentTypeParameterName = "content-type";
        public const string ReturnPointParameterName = "return-point";
        public const string EmployeeIdsParameterName = "ids";
        public const string StartDateParameterName = "week";
        public const string CloneFromParameterName = "clone-from-id";

        private const string FullWidthCss = "col-sm-10";

        private string ContentTypeName => Request.QueryString[ContentTypeParameterName] ?? string.Empty;

        private long[] SelectedEmployeeIds =>
            (Request.QueryString[EmployeeIdsParameterName] ?? string.Empty)
            .Split(',')
            .Where(e => !string.IsNullOrEmpty(e))
            .Select(long.Parse).ToArray();

        private DateTime? SelectedStartDate
            =>
                Request.QueryString[StartDateParameterName] == null
                    ? (DateTime?)null
                    : DateTime.Parse(Request.QueryString[StartDateParameterName]);

        private long? SelectedCloneFrom
            =>
                Request.QueryString[CloneFromParameterName] == null
                    ? (long?)null
                    : long.Parse(Request.QueryString[CloneFromParameterName]);

        public AllocationRequestController(IAllocationRequestCardIndexDataService cardIndexDataService,
            IEmployeeAllocationCardIndexDataService employeeAllocationCardIndexDataService,
            IClassMapping<AllocationRequestViewModel, AllocationRequestDto> allocationRequestClassMapping,
            IClassMapping<EmployeeDto, EmployeeViewModel> employeeClassMapping,
            IBaseControllerDependencies baseControllerDependencies,
            IClassMapping<AllocationRequestDto, AllocationRequestViewModel> allocationRequestFromDtoClassMapping)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _allocationRequestCardIndexDataService = cardIndexDataService;
            _employeeAllocationCardIndexDataService = employeeAllocationCardIndexDataService;
            _allocationRequestClassMapping = allocationRequestClassMapping;
            _employeeClassMapping = employeeClassMapping;
            _allocationRequestFromDtoClassMapping = allocationRequestFromDtoClassMapping;
            CardIndex.Settings.PageSize = 50;
            CardIndex.Settings.ShowDeleteButtonOnEdit = true;

            CardIndex.Settings.Title = AllocationRequestResources.Title;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddAllocationRequest;
            SetAddAllocationRequestDropdownItems();
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditAllocationRequest;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanResignAllocationRequest;
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportAllocationRequests);
            CardIndex.Settings.AddImport<AllocationRequestImportViewModel, AllocationRequestDto>(cardIndexDataService,
                EmployeeResources.ImportButtomName);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();
            CardIndex.Settings.FilterGroups.AddRange(EmployeeFilterGroup(AllocationResources.EmployeeFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(OrganizationFilterGroup(AllocationResources.OrgFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(ScopeFilterGroup(AllocationResources.ScopeFilterTitle));

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<AllocationRequestViewModel>(a => a.EmployeeId).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<AllocationRequestViewModel>(a => a.EmployeeIds).Visibility = ControlVisibility.Remove;
            };

            Layout.Scripts.Add("~/Areas/Allocation/Scripts/AllocationRequest.js");
            Layout.Css.Add("~/Areas/Allocation/Content/AllocationRequest.css");
        }

        public static IEnumerable<ICommandButton> AddCommentButtons(UrlHelper urlHelper, KnownParameter parameterContext = KnownParameter.Context, string returnPoint = null)
        {
            var contentTypes = EnumHelper.GetEnumValues<AllocationRequestContentType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var contentType in contentTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(contentType.ToString());
                var customParameters = new List<string>()
                {
                    ContentTypeParameterName, typeValueName
                };

                if (!string.IsNullOrEmpty(returnPoint))
                {
                    customParameters.AddRange(new[] { ReturnPointParameterName, returnPoint });
                }

                var addButton = new CommandButton
                {
                    Group = AllocationRequestResources.AddAllocationRequestOfTypeHeaderText,
                    RequiredRole = SecurityRoleType.CanAddAllocationRequest,
                    Text = contentType.GetDescriptionOrValue(),
                    OnClickAction = urlHelper.UriActionGet("Add", "AllocationRequest", parameterContext, customParameters.ToArray())
                };

                addDropdownItems.Add(addButton);
            }

            var copyButton = new CommandButton
            {
                Group = AllocationRequestResources.AddAllocationRequestOfTypeHeaderText,
                RequiredRole = SecurityRoleType.CanAddAllocationRequest,
                Text = AllocationRequestResources.CloneLabel,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                OnClickAction = urlHelper.UriActionGet("Add", "AllocationRequest", KnownParameter.None, CloneFromParameterName, "~this.id")
            };

            addDropdownItems.Add(copyButton);

            return addDropdownItems;
        }

        public override ActionResult Edit(AllocationRequestViewModel viewModelRecord)
        {
            var modelDto = _allocationRequestClassMapping.CreateFromSource(viewModelRecord);

            var validationPeriodResult = _employeeAllocationCardIndexDataService
                .ValidateEditAllocationRequest(modelDto);

            return OverAllocationViewAction(validationPeriodResult, viewModelRecord, CardIndex.Settings.EditFormCustomization, "ConfirmOverEditAllocation") ?? base.Edit(viewModelRecord);
        }

        public override ActionResult Add(AllocationRequestViewModel viewModelRecord)
        {
            var modelDto = _allocationRequestClassMapping.CreateFromSource(viewModelRecord);

            var validationPeriodResult = _employeeAllocationCardIndexDataService
                .ValidateAddAllocationRequest(modelDto);

            return OverAllocationViewAction(validationPeriodResult, viewModelRecord, CardIndex.Settings.AddFormCustomization, "ConfirmOverAddAllocation") ?? base.Add(viewModelRecord);
        }

        [AtomicAuthorize(SecurityRoleType.CanAddAllocationRequest)]
        [HttpPost]
        public ActionResult ConfirmOverAddAllocation(AllocationRequestViewModel viewModel)
        {
            return base.Add(viewModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditAllocationRequest)]
        [HttpPost]
        public ActionResult ConfirmOverEditAllocation(AllocationRequestViewModel viewModel)
        {
            return base.Edit(viewModel);
        }

        public ActionResult ResolveAllocation(long projectId, long employeeId, [ModelBinder(typeof(DateTimeModelBinder))] DateTime period, string returnPoint)
        {
            var allocationRequestId = _allocationRequestCardIndexDataService.ResolveAllocationRequest(projectId, employeeId, period);

            var parameters = new RouteValueDictionary { { ReturnPointParameterName, returnPoint } };

            if (!allocationRequestId.HasValue)
            {
                parameters.Add(ContentTypeParameterName, "simple");
                return Redirect(Url.Action("Add", parameters));
            }

            parameters.Add("id", allocationRequestId.Value);

            return Redirect(Url.Action("Edit", parameters));
        }

        private ActionResult OverAllocationViewAction(IEnumerable<OverAllocatedWarningDto> allocationWarnings, AllocationRequestViewModel viewModelRecord, Action<FormRenderingModel> formCustomization, string saveAction)
        {
            var validationPeriodResult = allocationWarnings.Select(a => new OverAllocatedWarningViewModel
            {
                Employee = _employeeClassMapping.CreateFromSource(a.Employee),
                Period = a.Period,
                ProjectId = a.ProjectId
            }).ToList();

            if (validationPeriodResult.Any())
            {
                Layout.PageTitle = AllocationRequestResources.Title;
                Layout.PageHeader = AllocationRequestResources.Title;
                Layout.Css.BodyClass = "record-form";
                CardIndex.Settings.Title = AllocationRequestResources.Title;

                ViewBag.SaveAction = saveAction;

                return View("OverAllocatedWarning", new AllocationValidationViewModel
                {
                    ViewModel = viewModelRecord,
                    Warnings = validationPeriodResult,
                    FormCustomization = formCustomization
                });
            }

            return null;
        }

        private void SetAddAllocationRequestDropdownItems()
        {
            CardIndex.Settings.AddButton.OnClickAction = new DropdownAction
            {
                Items = AddCommentButtons(Url).ToList()
            };
        }

        private object GetDefaultNewRecord()
        {
            var cloneFromParameter = SelectedCloneFrom;

            if (cloneFromParameter.HasValue)
            {
                return CloneAllocationRequest(cloneFromParameter.Value);
            }

            var contentType =
                EnumHelper.GetEnumValueOrDefault<AllocationRequestContentType>(ContentTypeName,
                    NamingConventionHelper.ConvertPascalCaseToHyphenated) ??
                AllocationRequestContentType.Simple;

            var model = AllocationRequestDtoToAllocationRequestViewModelMapping.CreateViewModel(contentType);

            model.EmployeeIds = SelectedEmployeeIds;
            var selectedStartData = SelectedStartDate;

            if (selectedStartData.HasValue)
            {
                model.StartDate = selectedStartData.Value;
            }

            return model;
        }

        private AllocationRequestViewModel CloneAllocationRequest(long clonedRequestId)
        {
            var sourceAllocationRequest =
                _allocationRequestFromDtoClassMapping.CreateFromSource(
                    _allocationRequestCardIndexDataService.GetRecordById(clonedRequestId));

            var clonedAllocationRequest =
                AllocationRequestDtoToAllocationRequestViewModelMapping.CreateViewModel(
                    sourceAllocationRequest.ContentType);

            clonedAllocationRequest.EmployeeIds = new[] { sourceAllocationRequest.EmployeeId };
            clonedAllocationRequest.AllocationCertainty = sourceAllocationRequest.AllocationCertainty;
            clonedAllocationRequest.ProcessingStatus = ProcessingStatus.Processing;
            clonedAllocationRequest.StartDate = sourceAllocationRequest.StartDate;
            clonedAllocationRequest.ContentType = sourceAllocationRequest.ContentType;
            clonedAllocationRequest.EndDate = sourceAllocationRequest.EndDate;
            clonedAllocationRequest.HoursPerDay = sourceAllocationRequest.HoursPerDay;
            clonedAllocationRequest.MondayHours = sourceAllocationRequest.MondayHours;
            clonedAllocationRequest.TuesdayHours = sourceAllocationRequest.TuesdayHours;
            clonedAllocationRequest.WednesdayHours = sourceAllocationRequest.WednesdayHours;
            clonedAllocationRequest.ThursdayHours = sourceAllocationRequest.ThursdayHours;
            clonedAllocationRequest.FridayHours = sourceAllocationRequest.FridayHours;
            clonedAllocationRequest.SaturdayHours = sourceAllocationRequest.SaturdayHours;
            clonedAllocationRequest.SundayHours = sourceAllocationRequest.SundayHours;
            clonedAllocationRequest.ProjectId = sourceAllocationRequest.ProjectId;

            return clonedAllocationRequest;
        }

        private IEnumerable<FilterGroup> EmployeeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.Competences,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<SkillsAndKnowledgeFilterViewModel>(
                        FilterCodes.SkillsAndKnowledgeFilter, AllocationResources.SkillsAndKnowledgeFilterName),
                    new ParametricFilter<SkillsFilterViewModel>(
                        FilterCodes.SkillsFilter, AllocationResources.SkillsFilterName)
                }
            };
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeeStatus,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmployeeStatus>()
            };
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmploymentType,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmploymentType>()
            };
        }

        private IEnumerable<FilterGroup> OrganizationFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.OrganizationTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter,
                        AllocationResources.EmployeeOrgUnitFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.OrCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new ParametricFilter<LocationFilterViewModel>(FilterCodes.LocationFilter,
                        AllocationResources.EmployeeLocationFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeesTitle,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<EmployeeLineManagerFilterViewModel>(FilterCodes.LineManagerFilter,
                        AllocationResources.EmployeeLineManagerFilterName)
                }
            };
        }

        private IEnumerable<FilterGroup> ScopeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.SetRanges,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<AllocationRequestFilterViewModel>(FilterCodes.DateRange,
                        AllocationResources.WeekRanges),
                    new ParametricFilter<AllocationRequestProjectsAndEmployeesFilterViewModel>(
                        FilterCodes.EmployeesAndProjects, AllocationRequestResources.EmployeesAndProjectsFilter),
                    new ParametricFilter<AllocationRequestProjectTagsFilterViewModel>(
                        FilterCodes.ProjectTags, AllocationRequestResources.ProjectTagsFilter),
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeeFilterTitle,
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.MyEmployees, DisplayName = AllocationResources.MyEmployees},
                    new Filter {Code = FilterCodes.AllEmployees, DisplayName = AllocationResources.AllEmployees}
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeeCompany,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<CompanyFilterViewModel>(FilterCodes.CompanyFilter, AllocationResources.EmployeeCompanyFilterName)
                }
            };

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewAllocationRequestChangeDemand))
            {
                yield return new FilterGroup
                {
                    ButtonText = groupName,
                    DisplayName = AllocationRequestResources.ChangeDemand,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<AllocationChangeDemand>()
                };
            }
        }

        public override ActionResult GetAddDialog()
        {
            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<AllocationRequestViewModel>(a => a.EmployeeId).Visibility = ControlVisibility.Remove;
                form.GetControlByName<AllocationRequestViewModel>(m => m.AllocationCertainty).ControlWidthClass = FullWidthCss;
                form.GetControlByName<AllocationRequestViewModel>(m => m.ProjectId).ControlWidthClass = FullWidthCss;
            };

            return base.GetAddDialog();
        }

        public override ActionResult GetEditDialog(long id)
        {
            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<AllocationRequestViewModel>(a => a.EmployeeIds).Visibility = ControlVisibility.Remove;
                form.GetControlByName<AllocationRequestViewModel>(m => m.AllocationCertainty).ControlWidthClass = FullWidthCss;
                form.GetControlByName<AllocationRequestViewModel>(m => m.ProjectId).ControlWidthClass = FullWidthCss;
                form.GetControlByName<AllocationRequestViewModel>(m => m.EmployeeId).ControlWidthClass = FullWidthCss;
            };

            var contentType = ContentTypeName;

            if (!string.IsNullOrEmpty(contentType))
            {
                _allocationRequestCardIndexDataService.SetRecordContentType(
                    contentType == ((int)AllocationRequestContentType.Simple).ToString()
                    ? AllocationRequestContentType.Simple
                    : AllocationRequestContentType.Advanced
                );
            }

            return base.GetEditDialog(id);
        }
    }
}