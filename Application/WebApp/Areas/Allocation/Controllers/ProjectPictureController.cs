﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Enums;
using Smt.Atomic.Business.Allocation.Enums;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.MediaProcessing.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProject)]
    public class ProjectPictureController : AtomicController
    {
        private const string DocumentMappingIdentifier = "Pictures.ProjectPictureMapping";
        private readonly IDocumentDownloadService _documentDownloadService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IImageProcessingService _imageProcessingService;
        private readonly ISettingsProvider _settingsProvider;

        public ProjectPictureController(
            IBaseControllerDependencies baseDependencies,
            IDocumentDownloadService documentDownloadService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IImageProcessingService imageProcessingService,
            ISettingsProvider settingsProvider)
            : base(baseDependencies)
        {
            _documentDownloadService = documentDownloadService;
            _unitOfWorkService = unitOfWorkService;
            _imageProcessingService = imageProcessingService;
            _settingsProvider = settingsProvider;
        }

        public ActionResult Download(long projectId, ProjectPictureSize projectPictureSize = ProjectPictureSize.Normal)
        {
            var documentDownloadDto = new DocumentDownloadDto();
            var imagePixelSize = (int)projectPictureSize;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var project = unitOfWork.Repositories.Projects.GetById(projectId);

                byte[] imageData;

                if (project?.ProjectSetup.PictureId != null)
                {
                    documentDownloadDto = _documentDownloadService.GetDocument(DocumentMappingIdentifier, project.ProjectSetup.PictureId.Value);
                    imageData = documentDownloadDto.Content;
                }
                else
                {
                    var path = Server.MapPath(_settingsProvider.DefaultProjectPictureFilePath);

                    imageData = System.IO.File.ReadAllBytes(path);
                    documentDownloadDto.ContentType = _imageProcessingService.GetImageMimeType(imageData);
                }

                documentDownloadDto.Content = _imageProcessingService.ResizeImage(imageData, imagePixelSize);

                return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
                {
                    FileDownloadName = documentDownloadDto.DocumentName
                };
            }
        }
    }
}