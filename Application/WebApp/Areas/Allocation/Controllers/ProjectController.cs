﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Reports.Templates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Finance.Controllers;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.WebApp.Areas.Reporting.Controllers;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProject)]
    [PerformanceTest(nameof(ProjectController.List))]
    public class ProjectController : CardIndexController<ProjectViewModel, ProjectDto>
    {
        private readonly IProjectCardIndexDataService _cardIndexDataService;
        private readonly IClassMapping<ProjectDto, ProjectViewModel> _projectClassMapping;
        private readonly IProjectService _projectService;
        private readonly IPrincipalProvider _principalProvider;

        public ProjectController(IProjectCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IClassMapping<ProjectDto, ProjectViewModel> projectClassMapping,
            IProjectService projectService,
            IPrincipalProvider principalProvider)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _projectClassMapping = projectClassMapping;
            _projectService = projectService;
            _principalProvider = principalProvider;

            ConfigureBulkEdit<IProjectCardIndexDataService>(SecurityRoleType.CanBulkEditProjects);

            Layout.Scripts.Add("~/Areas/Allocation/Scripts/Project.js");
            Layout.Resources.AddFrom<ProjectResources>(nameof(ProjectResources.PresentationWithProjectsNotAvailableForSalesMessage));
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddProject;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteProject;
            CardIndex.Settings.GridViewConfigurations = GetGridSettings().ToList();
            Layout.Css.Add("~/Areas/Allocation/Content/ProjectTile.css");

            Layout.Scripts.Add("~/Areas/Allocation/Scripts/ProjectDialog.js");
            Layout.Scripts.Add("~/Areas/Allocation/Scripts/AcceptanceConditionsHandler.js");

            AddCommandButtons();

            CardIndex.Settings.Title = ProjectResources.ProjectsTitle;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();
            CardIndex.Settings.FilterGroups.AddRange(GetBasicFilterGroup(ProjectResources.BasicFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(GetSalesFilterGroup(ProjectResources.SalesFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(GetDeliveryFilterGroup(ProjectResources.DeliveryFilterTitle));

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportProjects);
            CardIndex.Settings.AddImport<ProjectViewModel, ProjectDto>(cardIndexDataService, ProjectResources.ImportButtomName);

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<ProjectViewModel>(a => a.JiraIssueKey).IsReadOnly = true;
                form.GetControlByName<ProjectViewModel>(a => a.SalesAccountManagerId).IsReadOnly = true;
                form.GetControlByName<ProjectViewModel>(a => a.SupervisorManagerId).IsReadOnly = true;
            };
            CardIndex.Settings.GridViewConfigurations = GetViewConfiguration().ToList();

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.ProjectAcronym, ProjectResources.ProjectAcronym),
                new SearchArea(SearchAreaCodes.ClientShortName, ProjectResources.ClientShortNameLabel),
                new SearchArea(SearchAreaCodes.ProjectShortName, ProjectResources.ProjectShortNameLabel),
                new SearchArea(SearchAreaCodes.Description, ProjectResources.DescriptionLabel),
                new SearchArea(SearchAreaCodes.SalesAccountManager, ProjectResources.SalesAccountManagerLabel),
                new SearchArea(SearchAreaCodes.ProjectManager, ProjectResources.ProjectManagerLabel),
                new SearchArea(SearchAreaCodes.OrgUnit, ProjectResources.OrgUnitLabel),
                new SearchArea(SearchAreaCodes.JiraKey, ProjectResources.JiraKeyLabel),
                new SearchArea(SearchAreaCodes.Apn, ProjectResources.ApnLabel),
                new SearchArea(SearchAreaCodes.KupferwerkProjectId, ProjectResources.KupferwerkProjectIdLabel),
                new SearchArea(SearchAreaCodes.Region, ProjectResources.RegionLabel),
            };

            SetEditButton();
        }

        public override ActionResult Edit(long id)
        {
            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanEditProject))
            {
                return EditAll(id);
            }

            return EditOwn(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditProject)]
        public ActionResult EditAll(long id)
        {
            return base.Edit(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditOwnProjects)]
        public ActionResult EditOwn(long id)
        {
            if (!_projectService.IsEmployeeOwnProject(_principalProvider.Current.EmployeeId, id))
            {
                throw new BusinessException(ProjectResources.IsNotCurrentEmployeeOwnProjectMessage);
            }

            return base.Edit(id);
        }

        private void SetEditButton()
        {
            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanEditProject))
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.Grid;
            }
            else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanEditOwnProjects))
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.Grid;

                CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(ProjectViewModel.IsOwnProject))}"
                };
            }
            else
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            }
        }

        private void AddCommandButtons()
        {
            var subProjectsButton = new ToolbarButton
            {
                Text = ProjectResources.SubProjects,
                OnClickAction = Url.UriActionGet("List", "SubProject", KnownParameter.ParentId),
                Icon = FontAwesome.FolderOpen,
                RequiredRole = SecurityRoleType.CanViewProject,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(subProjectsButton);

            var generatePresentationButton = new ToolbarButton
            {
                Text = ProjectResources.ReportsTitle,
                OnClickAction = new DropdownAction()
                {
                    Items = new List<ICommandButton>()
                    {
                        new CommandButton()
                        {
                            Text = ProjectResources.GenerateDescriptionPresentationTitle,
                            OnClickAction = Url.UriActionGet(
                                nameof(ProjectPresentationGenerationController.Generate), RoutingHelper.GetControllerName<ProjectPresentationGenerationController>(),
                                KnownParameter.SelectedIds, "template", PresentationTemplateNames.Description),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.AtLeastOneRowSelected
                            },
                            LayoutModifierFunctionJavaScript  = "Project.adjustElementLayout",
                            IsDefault = true,
                            RequiredRole = SecurityRoleType.CanGenerateProjectPresentations
                        },
                        new CommandButton()
                        {
                            Text = ProjectResources.GenerateBusinessCasePresentationTitle,
                            OnClickAction = Url.UriActionGet(
                                nameof(ProjectPresentationGenerationController.Generate), RoutingHelper.GetControllerName<ProjectPresentationGenerationController>(),
                                KnownParameter.SelectedIds, "template", PresentationTemplateNames.BusinessCase),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.AtLeastOneRowSelected
                            },
                            LayoutModifierFunctionJavaScript  = "Project.adjustElementLayout",
                            IsDefault = true,
                            RequiredRole = SecurityRoleType.CanGenerateProjectPresentations
                        },
                        new CommandButton()
                        {
                            Text = ProjectResources.GenerateReportedDailyHoursTitle,
                            OnClickAction = new JavaScriptCallAction(
                                $"ProjectDialog.onTimeTrackingDailyHoursGenerateClicked('" +
                                $"{ Url.Action(nameof(ReportExecutionController.ConfigureReportDialog), RoutingHelper.GetControllerName(typeof(ReportExecutionController)), new { Area = "Reporting" }) }', grid, 'TimeTrackingDailyHours');"),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.AtLeastOneRowSelected
                            },
                            IsDefault = true,
                            RequiredRole = SecurityRoleType.CanGenerateTimeTrackingDailyHoursReport
                        },
                        new CommandButton()
                        {
                            Text = ProjectResources.GeneratePivotReportTitle,
                            OnClickAction = new JavaScriptCallAction(
                                $"ProjectDialog.onTimeTrackingDailyHoursGenerateClicked('" +
                                $"{ Url.Action(nameof(ReportExecutionController.ConfigureReportDialog), RoutingHelper.GetControllerName(typeof(ReportExecutionController)), new { Area = "Reporting" }) }', grid, 'TimeTrackingDailyHoursPivot');"),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.AtLeastOneRowSelected
                            },
                            IsDefault = true,
                            RequiredRole = SecurityRoleType.CanGenerateTimeTrackingDailyHoursReport
                        }
                    }
                },
                Visibility = CommandButtonVisibility.Grid,
                Group = "Presentation",
                Icon = FontAwesome.File,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                },
                RequiredRole = SecurityRoleType.CanGenerateProjectPresentations,
            };

            var bulkEditButtonIndex = CardIndex.Settings.ToolbarButtons.FindIndex(b => b.Id == CardIndex.Settings.BulkEditButton.Id);

            CardIndex.Settings.ToolbarButtons.Insert(bulkEditButtonIndex, generatePresentationButton);

            var staffingDemandButton = new ToolbarButton
            {
                Text = ProjectResources.StaffingDemandTitle,
                OnClickAction = Url.UriActionGet(nameof(StaffingDemandController.List), RoutingHelper.GetControllerName<StaffingDemandController>(), KnownParameter.ParentId),
                Group = "Management",
                Icon = FontAwesome.Archive,
                IsDefault = true,
                RequiredRole = SecurityRoleType.CanViewStaffingDemand,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(staffingDemandButton);

            var mergeProjectButton = new ToolbarButton
            {
                Text = ProjectResources.MergeProjectsText,
                RequiredRole = SecurityRoleType.CanMergeProjects,
                Availability =
                    new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.AtLeastOneRowSelected,
                        Predicate = "!row.jiraIssueKey"
                    },
                Group = "Merge",
                Icon = FontAwesome.Magic,
                IsDefault = true,
                OnClickAction = new JavaScriptCallAction(
                    $"ProjectDialog.mergeProjects('{Url.Action(nameof(ProjectController.MergeProjects), RoutingHelper.GetControllerName<ProjectController>())}', grid);")
            };

            CardIndex.Settings.ToolbarButtons.Add(mergeProjectButton);

            var transactionsButton = new ToolbarButton
            {
                Text = TransactionResources.TransactionsTitle,
                OnClickAction = Url.UriActionGet(
                    nameof(ProjectTransactionController.List), RoutingHelper.GetControllerName<ProjectTransactionController>(),
                    KnownParameter.ParentId, "area", RoutingHelper.GetAreaName(typeof(ProjectTransactionController))),
                Icon = FontAwesome.Money,
                IsDefault = true,
                Group = "Transactions",
                RequiredRole = SecurityRoleType.CanViewProjectTransactions,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(transactionsButton);
        }

        private IEnumerable<IGridViewConfiguration> GetViewConfiguration()
        {
            var defaultView = new GridViewConfiguration<ProjectViewModel>(ProjectResources.DefaultView, "default");
            defaultView.IncludeAllColumns();
            defaultView.ExcludeColumn(m => m.PetsCode);
            defaultView.ExcludeColumn(m => m.JiraIssueKey);
            defaultView.ExcludeColumn(m => m.JiraKey);
            defaultView.ExcludeColumn(m => m.Apn);
            defaultView.ExcludeColumn(m => m.KupferwerkProjectId);
            defaultView.IsDefault = true;
            yield return defaultView;

            var extendedConfiguration = new GridViewConfiguration<ProjectViewModel>(ProjectResources.ExtendedView, "extended");
            extendedConfiguration.IncludeAllColumns();
            yield return extendedConfiguration;
        }

        internal static IEnumerable<FilterGroup> GetBasicFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new Filter { Code = FilterCodes.ActiveProjects, DisplayName = ProjectResources.ActiveProjectLabel },
                    new Filter { Code = FilterCodes.InactiveProjects, DisplayName = ProjectResources.InactiveProjectLabel }
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.SetRanges,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<ProjectFilterViewModel>(FilterCodes.DateRange, ProjectResources.DateRange),
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter, ProjectResources.OrgUnitFilters)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.MyProjects, DisplayName = ProjectResources.MyProjects},
                    new Filter {Code = FilterCodes.AllProjects, DisplayName = ProjectResources.AllProjects}
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = ProjectResources.ProjectStatusLabel,
                Type = FilterGroupType.RadioGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<ProjectStatus>()
            };
        }

        protected IEnumerable<FilterGroup> GetSalesFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = ProjectResources.IsAvailableForSalesPresentationLabel,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new Filter
                    {
                        Code = FilterCodes.AvailableForSalesPresentation,
                        DisplayName = ProjectResources.AvailableForPresentation
                    },
                    new Filter
                    {
                        Code = FilterCodes.UnavailableForSalesPresentation,
                        DisplayName = ProjectResources.UnavailableForPresentation
                    },
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = ProjectResources.ReferencesFromClient,
                Type = FilterGroupType.OrCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.WithReferences, DisplayName = ProjectResources.WithReferences},
                    new Filter {Code = FilterCodes.WithoutReferences, DisplayName = ProjectResources.WithoutReferences}
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = ProjectResources.TargetAudience,
                Type = FilterGroupType.AndCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new ParametricFilter<IndustriesFilterViewModel>(FilterCodes.Industries, ProjectResources.FilterByIndustryTitle),
                    new ParametricFilter<SoftwareCategoryFilterViewModel>(FilterCodes.SoftwareCategories, ProjectResources.FilterBySoftwareCategoriesTitle)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<CustomerSizeFilterViewModel>(FilterCodes.CustomerSizeFilter, ProjectResources.CustomerSizeFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<ProjectDisclosuresFilterViewModel>(FilterCodes.ProjectDisclosures, ProjectResources.ProjectDisclosuresLabel)
                }
            };
        }

        protected IEnumerable<FilterGroup> GetDeliveryFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = ProjectResources.ProjectTechnologiesFilterGroupTitle,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<ProjectTechnologyFilterViewModel>(FilterCodes.TechnologyFilter, ProjectResources.ProjectTechnologiesFilterName),
                    new ParametricFilter<ProjectKeyTechnologyFilterViewModel>(FilterCodes.KeyTechnologyFilter, ProjectResources.ProjectKeyTechnologiesFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = ProjectResources.CurrentPhaseLabel,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<ProjectPhase>()
            };
        }

        [AtomicAuthorize(SecurityRoleType.CanAddProject)]
        [HttpPost]
        public ActionResult AddNewProject()
        {
            var dialogModel = new ModelBasedDialogViewModel<SimpleProjectViewModel>(new SimpleProjectViewModel())
            {
                Title = ProjectResources.AddProjectDialogTitle
            };

            return PartialView(dialogModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanAddProject)]
        [HttpPost]
        public ActionResult AddInlineProject(SimpleProjectViewModel model)
        {
            var dialogModel = new ModelBasedDialogViewModel<SimpleProjectViewModel>(model)
            {
                Title = ProjectResources.AddProjectDialogTitle
            };

            if (!ModelState.IsValid)
            {
                return PartialView("AddNewProject", dialogModel);
            }

            var defaultProject = _cardIndexDataService.GetDefaultNewRecord();

            defaultProject.ClientShortName = model.ClientShortName;
            defaultProject.ProjectShortName = model.ProjectShortName;
            defaultProject.Acronym = model.Acronym;
            defaultProject.Description = model.Description;

            var response = _cardIndexDataService.AddRecord(defaultProject);

            CardIndex.AddUniqueKeyViolationAlerts(response, typeof(ProjectViewModel));
            AddAlerts(response);

            if (!response.IsSuccessful)
            {
                return PartialView("AddNewProject", dialogModel);
            }

            var createdProjectDto = _cardIndexDataService.GetRecordById(response.AddedRecordId);
            var createProjectViewModel = _projectClassMapping.CreateFromSource(createdProjectDto);

            return DialogHelper.HandleEvent(dialogModel.DialogId, DialogHelper.OkButtonTag, new { createProjectViewModel.Id, DisplayName = createProjectViewModel.ToString() });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanMergeProjects)]
        public ActionResult MergeProjects(IList<long> ids)
        {
            var model = new MergeProjectsViewModel
            {
                SourceProjectIds = ids
            };

            var dialogModel = new ModelBasedDialogViewModel<MergeProjectsViewModel>(model)
            {
                Title = ProjectResources.ProjectMergeText
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanMergeProjects)]
        public ActionResult MergeProjectsMethod(MergeProjectsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _projectService.MergeProjects(model.SourceProjectIds, model.TargetProjectId);

                return DialogHelper.Reload();
            }

            var dialogModel = new ModelBasedDialogViewModel<MergeProjectsViewModel>(model)
            {
                Title = ProjectResources.ProjectMergeText
            };

            return PartialView("MergeProjects", dialogModel);
        }

        private static IEnumerable<IGridViewConfiguration> GetGridSettings()
        {
            yield return new TilesViewConfiguration<ProjectViewModel>(ProjectResources.ProjectTilesViewName, "tiles")
            {
                IsDefault = false,
                IsHidden = false,
                CustomViewName = "~/Areas/Allocation/Views/Project/_ProjectTile.cshtml",
            };

            var listConfiguration = new GridViewConfiguration<ProjectViewModel>(ProjectResources.ProjectListViewName, "list")
            {
                IsDefault = true,
            };

            listConfiguration.IncludeAllColumns();

            yield return listConfiguration;
        }
    }
}