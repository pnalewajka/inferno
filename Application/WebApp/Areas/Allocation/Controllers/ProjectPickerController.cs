﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("EmployeeValuePickers.Project")]
    [AtomicAuthorize(SecurityRoleType.CanViewProjectPicker)]
    public class ProjectPickerController
        : ReadOnlyCardIndexController<ProjectViewModel, ProjectDto, ProjectPickerContext>
        , IHubController
    {
        private const string MinimalViewId = "minimal";

        public ProjectPickerController(IProjectCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ProjectResources.PickerAddButton,
                Visibility = CommandButtonVisibility.ValuePicker,
                RequiredRole = SecurityRoleType.CanAddProject,
                OnClickAction =
                    new JavaScriptCallAction(
                        $"AllocationRequest.openCreateProjectDialog.bind(this)('{Url.Action("AddNewProject", "Project", new { Area = "Allocation" })}')")
            });

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.ClientShortName, ProjectResources.ClientNameLabel),
                new SearchArea(SearchAreaCodes.ProjectAcronym, ProjectResources.ProjectAcronymLabel),
                new SearchArea(SearchAreaCodes.ProjectShortName, ProjectResources.ProjectNameLabel),
                new SearchArea(SearchAreaCodes.Description, ProjectResources.DescriptionLabel, false),
                new SearchArea(SearchAreaCodes.SalesAccountManager, ProjectResources.SalesAccountManagerLabel, false),
                new SearchArea(SearchAreaCodes.ProjectManager, ProjectResources.ProjectManagerLabel, false),
                new SearchArea(SearchAreaCodes.OrgUnit, ProjectResources.OrgUnitLabel, false),
                new SearchArea(SearchAreaCodes.JiraKey, ProjectResources.JiraIssueKeyLabel, false),
                new SearchArea(SearchAreaCodes.Apn, ProjectResources.ApnLabel, false),
                new SearchArea(SearchAreaCodes.KupferwerkProjectId, ProjectResources.KupferwerkProjectIdLabel, false),
                new SearchArea(SearchAreaCodes.Region, ProjectResources.RegionLabel, false),
            };
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<ProjectViewModel>(ProjectResources.MinimalViewText, MinimalViewId) { IsDefault = true };

            configuration.IncludeColumns(new List<Expression<Func<ProjectViewModel, object>>>
            {
                x => x.ProjectName,
                x => x.ClientShortName,
                x => x.ProjectStatus
            });
            configurations.Add(configuration);

            return configurations;
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();
            CardIndex.Settings.FilterGroups.AddRange(ProjectController.GetBasicFilterGroup(ProjectResources.BasicFilterTitle));

            Layout.PageTitle = allowMultipleRowSelection
                ? ProjectResources.ValuePickerMultipleTitle
                : ProjectResources.ValuePickerSingleTitle;
        }

        public override object GetContextViewModel()
        {
            return base.GetContextViewModel();
        }

        public PartialViewResult Hub(long id)
        {
            var model = CardIndex.GetRecordById(id);

            var dialogModel = new ModelBasedDialogViewModel<ProjectViewModel>(model, StandardButtons.None)
            {
                Title = model.ProjectName,
                ClassName = "project-hub"
            };

            return PartialView(
                "~/Areas/Allocation/Views/ProjectPicker/ProjectHub.cshtml",
                dialogModel);
        }
    }
}