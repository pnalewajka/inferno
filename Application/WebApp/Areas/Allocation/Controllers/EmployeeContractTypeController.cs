﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployeeContractType)]
    public class EmployeeContractTypeController : ReadOnlyCardIndexController<EmployeeContractTypeViewModel, EmployeeContractTypeDto>
    {
        private const int ExportPageSize = 1000;
        private readonly IClassMapping<EmployeeContractTypeDto, EmployeeContractTypeViewModel> _employeeContractTypeDtoToEmployeeContractTypeViewModelClassMapping;
        private readonly IPrincipalProvider _principalProvider;

        public EmployeeContractTypeController(
            IEmployeeContractTypeCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IClassMapping<EmployeeContractTypeDto, EmployeeContractTypeViewModel> employeeContractTypeDtoToEmployeeContractTypeViewModelClassMapping
            ) : base(cardIndexDataService, baseControllerDependencies)
        {
            _employeeContractTypeDtoToEmployeeContractTypeViewModelClassMapping = employeeContractTypeDtoToEmployeeContractTypeViewModelClassMapping;
            _principalProvider = baseControllerDependencies.PrincipalProvider;

            CardIndex.Settings.Title = EmployeeResources.EmployeesContractType;

            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
            CardIndex.Settings.AddExportButtons();

            var employmentPeriodsButton = new ToolbarButton
            {
                Text = EmployeeResources.HrdataTabName,
                OnClickAction = Url.UriActionGet("List", "EmploymentPeriod", KnownParameter.ParentId),
                Group = "Management",
                Icon = FontAwesome.Archive,
                RequiredRole = SecurityRoleType.CanViewEmploymentPeriods,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(employmentPeriodsButton);

            var employeeFilterButtonText = EmployeeResources.EmployeeFilterTitle;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = AllocationResources.Competences,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Presentation.Renderers.CardIndex.Filters.Filter>
                    {
                        new ParametricFilter<SkillsAndKnowledgeFilterViewModel>(
                            FilterCodes.SkillsAndKnowledgeFilter, AllocationResources.SkillsAndKnowledgeFilterName),
                        new ParametricFilter<IndustryFilterViewModel>(
                            FilterCodes.Industries, DictionaryResources.IndustryTitle),
                        new Presentation.Renderers.CardIndex.Filters.Filter {Code = FilterCodes.ProjectContributorFilter, DisplayName = EmployeeResources.EmployeeIsProjectContributorLabel},
                        new ParametricFilter<SkillsFilterViewModel>(FilterCodes.SkillsFilter,AllocationResources.SkillsFilterName),
                        new ParametricFilter<LanguageWithLevelFilterViewModel>(FilterCodes.LanguagesFilter,EmployeeResources.LanguageWithLevelFilterName)
                    }
                },

                GetSimpleFilterGroup(),

                new FilterGroup
                {
                    DisplayName = EmployeeResources.EmployeeCompany,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Presentation.Renderers.CardIndex.Filters.Filter>
                    {
                        new ParametricFilter<CompanyFilterViewModel>(FilterCodes.CompanyFilter, EmployeeResources.EmployeeCompanyFilterName)
                    }
                },

                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                    Filters = new List<Presentation.Renderers.CardIndex.Filters.Filter>
                    {
                        new ParametricFilter<LocationFilterViewModel>(FilterCodes.LocationFilter, EmployeeResources.EmployeeLocationFilterName)
                    }
                },

                new FilterGroup
                {
                    DisplayName = EmployeeResources.EmployeePlaceOfWork,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<PlaceOfWork>()
                },

                new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmploymentType,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<EmploymentType>()
                },

                new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmployeesTitle,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Presentation.Renderers.CardIndex.Filters.Filter>
                    {
                        new ParametricFilter<EmployeeLineManagerFilterViewModel>(FilterCodes.LineManagerFilter, EmployeeResources.EmployeeLineManagerFilterName)
                    }
                }
            };

            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewContractType))
            {
                CardIndex.Settings.FilterGroups.Add(new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmploymentPeriodContractType,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Presentation.Renderers.CardIndex.Filters.Filter>
                    {
                        new ParametricFilter<ContractTypeFilterViewModel>(FilterCodes.ContractTypeFilter,
                            EmployeeResources.EmployeeContractTypeFilterName)
                    }
                });
            }

            // only show the Employee Status filter to people with proper role
            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
            {
                CardIndex.Settings.FilterGroups.Add(new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmployeeStatus,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<EmployeeStatus>()
                });
            }

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                ButtonText = employeeFilterButtonText,
                DisplayName = EmployeeResources.JobProfileLabel,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<JobProfileFilterViewModel>(FilterCodes.JobProfileFilter, EmployeeResources.EmployeeJobProfileFilterName)
                }
            });

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                DisplayName = EmployeeResources.OrganizationTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter, EmployeeResources.EmployeeOrgUnitFilterName)
                }
            });
        }

        [AtomicAuthorize(SecurityRoleType.CanViewEmployee)]
        public override ActionResult List(GridParameters parameters)
        {
            return base.List(parameters);
        }

        public override FilePathResult ExportToExcel(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;
            return base.ExportToExcel(parameters);
        }

        public override FilePathResult ExportToCsv(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;
            return base.ExportToCsv(parameters);
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var listConfiguration = new GridViewConfiguration<EmployeeContractTypeViewModel>(EmployeeResources.EmployeeListViewName, "list")
            {
                IsDefault = true
            };

            listConfiguration.IncludeColumn(x => x.Acronym);
            listConfiguration.IncludeColumn(x => x.FirstName);
            listConfiguration.IncludeColumn(x => x.LastName);
            listConfiguration.IncludeColumn(x => x.ContractTypeId);
            listConfiguration.IncludeColumn(x => x.ContractDuration);
            listConfiguration.IncludeColumn(x => x.StartDate);
            listConfiguration.IncludeColumn(x => x.CurrentStatus);
            listConfiguration.IncludeColumn(x => x.TimeReportingMode);
            listConfiguration.IncludeColumn(x => x.VacationHourToDayRatio);
            list.Add(listConfiguration);

            var extendedListConfiguration = new GridViewConfiguration<EmployeeContractTypeViewModel>(EmployeeResources.EmployeeExtendedListViewName, "extendedlist");
            extendedListConfiguration.IncludeAllColumns();
            extendedListConfiguration.ExcludeColumn(x => x.MondayHours);
            extendedListConfiguration.ExcludeColumn(x => x.TuesdayHours);
            extendedListConfiguration.ExcludeColumn(x => x.WednesdayHours);
            extendedListConfiguration.ExcludeColumn(x => x.ThursdayHours);
            extendedListConfiguration.ExcludeColumn(x => x.FridayHours);
            extendedListConfiguration.ExcludeColumn(x => x.SaturdayHours);
            extendedListConfiguration.ExcludeColumn(x => x.SundayHours);
            extendedListConfiguration.ExcludeColumn(x => x.HourlyRate);
            extendedListConfiguration.ExcludeColumn(x => x.HourlyRateDomesticOutOfOfficeBonus);
            extendedListConfiguration.ExcludeColumn(x => x.HourlyRateForeignOutOfOfficeBonus);
            extendedListConfiguration.ExcludeColumn(x => x.MonthlyRate);
            extendedListConfiguration.ExcludeColumn(x => x.SalaryGross);
            extendedListConfiguration.ExcludeColumn(x => x.AuthorRemunerationRatio);
            extendedListConfiguration.ExcludeColumn(x => x.JobMatrixLevel);
            list.Add(extendedListConfiguration);

            var hoursListConfiguration = new GridViewConfiguration<EmployeeContractTypeViewModel>(EmployeeResources.EmployeeHoursListViewName, "hourslist");
            hoursListConfiguration.IncludeColumn(x => x.Acronym);
            hoursListConfiguration.IncludeColumn(x => x.FirstName);
            hoursListConfiguration.IncludeColumn(x => x.LastName);
            hoursListConfiguration.IncludeColumn(x => x.CurrentStatus);
            hoursListConfiguration.IncludeColumn(x => x.MondayHours);
            hoursListConfiguration.IncludeColumn(x => x.TuesdayHours);
            hoursListConfiguration.IncludeColumn(x => x.WednesdayHours);
            hoursListConfiguration.IncludeColumn(x => x.ThursdayHours);
            hoursListConfiguration.IncludeColumn(x => x.FridayHours);
            hoursListConfiguration.IncludeColumn(x => x.SaturdayHours);
            hoursListConfiguration.IncludeColumn(x => x.SundayHours);
            list.Add(hoursListConfiguration);

            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeSalaryData))
            {
                var salaryViewConfiguration = new GridViewConfiguration<EmployeeContractTypeViewModel>(EmployeeResources.EmployeeContractTypeSalaryViewName, "salaryview");
                salaryViewConfiguration.IncludeColumn(x => x.Acronym);
                salaryViewConfiguration.IncludeColumn(x => x.FirstName);
                salaryViewConfiguration.IncludeColumn(x => x.LastName);
                salaryViewConfiguration.IncludeColumn(x => x.ContractTypeId);
                salaryViewConfiguration.IncludeColumn(x => x.JobMatrixLevel);
                salaryViewConfiguration.IncludeColumn(x => x.HourlyRate);
                salaryViewConfiguration.IncludeColumn(x => x.HourlyRateDomesticOutOfOfficeBonus);
                salaryViewConfiguration.IncludeColumn(x => x.HourlyRateForeignOutOfOfficeBonus);
                salaryViewConfiguration.IncludeColumn(x => x.MonthlyRate);
                salaryViewConfiguration.IncludeColumn(x => x.SalaryGross);
                salaryViewConfiguration.IncludeColumn(x => x.AuthorRemunerationRatio);
                list.Add(salaryViewConfiguration);
            }

            return list;
        }

        protected FilterGroup GetSimpleFilterGroup()
        {
            return new FilterGroup
            {
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter { Code = FilterCodes.MyEmployees, DisplayName = EmployeeResources.MyEmployees},
                    new Filter { Code = FilterCodes.MyDirectEmployees, DisplayName = EmployeeResources.MyDirectEmployees },
                    new Filter { Code = FilterCodes.AllEmployees, DisplayName = EmployeeResources.AllEmployees}
                }
            };
        }
    }
}