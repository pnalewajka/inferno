﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("ValuePickers.ProjectClient")]
    [AtomicAuthorize(SecurityRoleType.CanViewProject)]
    public class ProjectClientPickerController : ReadOnlyCardIndexController<ProjectClientViewModel, ProjectClientDto>
    {
        public ProjectClientPickerController(IProjectClientCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}
