﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployee)]
    [Identifier("EmployeeValuePickers.Manager")]
    public class ManagerPickerController
        : ReadOnlyCardIndexController<ManagerViewModel, EmployeeDto, RequiredRoleContext>
        , IHubController
    {
        private const string MinimalViewId = "minimal";
        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;
        private readonly IClassMapping<EmployeeDto, EmployeeViewModel> _employeeDtoToViewModelMapping;
        private readonly IClassMapping<UserDto, UserViewModel> _userDtoToViewModelMapping;

        public ManagerPickerController(
            IManagerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IUserService userService,
            IEmployeeService employeeService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _userService = userService;
            _employeeService = employeeService;
            _employeeDtoToViewModelMapping = baseControllerDependencies.ClassMappingFactory.CreateMapping<EmployeeDto, EmployeeViewModel>();
            _userDtoToViewModelMapping = baseControllerDependencies.ClassMappingFactory.CreateMapping<UserDto, UserViewModel>();

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<ManagerViewModel>(EmployeeResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<ManagerViewModel, object>>>
            {
                c => c.FirstName,
                x => x.LastName,
                t => t.JobTitleName
            });
            configurations.Add(configuration);

            return configurations;
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            Layout.PageTitle = allowMultipleRowSelection
                ? EmployeeResources.SelectManagers
                : EmployeeResources.SelectManager;
        }

        public PartialViewResult Hub(long id)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(id);
            var user = _userService.GetUserById(employee.UserId.Value);

            var model = new UserEmployeeHubViewModel(GetCurrentPrincipal())
            {
                Employee = _employeeDtoToViewModelMapping.CreateFromSource(employee),
                User = _userDtoToViewModelMapping.CreateFromSource(user),
            };

            var dialogModel = new ModelBasedDialogViewModel<UserEmployeeHubViewModel>(model, StandardButtons.None)
            {
                ClassName = "user-employee-hub",
            };

            return PartialView(
                "~/Views/Shared/_UserEmployeeHub.cshtml",
                dialogModel);
        }
    }
}