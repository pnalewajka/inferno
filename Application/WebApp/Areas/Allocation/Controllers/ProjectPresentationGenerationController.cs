﻿using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reports.Templates;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using System;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanGenerateProjectPresentations)]
    public class ProjectPresentationGenerationController : AtomicController
    {
        private readonly IReportingService _reportingService;

        public ProjectPresentationGenerationController(
            IReportingService reportingService,
            IBaseControllerDependencies baseDependencies)
            : base(baseDependencies)
        {
            _reportingService = reportingService;
        }

        [HttpGet]
        public ActionResult Generate(long[] ids, string template = PresentationTemplateNames.Description)
        {
            var reportViewModel = new ProjectReportParametersViewModel { ProjectIds = ids };
            var report = (FileContentResult)_reportingService.PerformReport(template, reportViewModel).Render();

            return new FileContentResult(report.FileContents, report.ContentType)
            {
                FileDownloadName = $"Project presentation {DateTime.Now:g}.pptx",
            };
        }
    }
}

