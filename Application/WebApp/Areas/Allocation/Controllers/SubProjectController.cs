﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProject)]
    public class SubProjectController : SubCardIndexController<SubProjectViewModel, SubProjectDto, ProjectController>
    {
        private readonly ISubProjectCardIndexDataService _cardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IProjectService _projectService;

        public SubProjectController(
            ISubProjectCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IProjectService projectService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _principalProvider = baseControllerDependencies.PrincipalProvider;
            _projectService = projectService;
            
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddProject;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteProject;

            CardIndex.Settings.Title = ProjectResources.SubProjects;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();

            CardIndex.Settings.EditFormCustomization += form =>
            {
                form.GetControlByName<SubProjectViewModel>(a => a.JiraIssueKey).IsReadOnly = true;
            };

            CardIndex.Settings.GridViewConfigurations = GetViewConfiguration().ToList();

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.ProjectAcronym, ProjectResources.ProjectAcronym),
                new SearchArea(SearchAreaCodes.ClientShortName, ProjectResources.ClientShortNameLabel),
                new SearchArea(SearchAreaCodes.ProjectShortName, ProjectResources.ProjectShortNameLabel),
                new SearchArea(SearchAreaCodes.Description, ProjectResources.DescriptionLabel),
                new SearchArea(SearchAreaCodes.SalesAccountManager, ProjectResources.SalesAccountManagerLabel),
                new SearchArea(SearchAreaCodes.ProjectManager, ProjectResources.ProjectManagerLabel),
                new SearchArea(SearchAreaCodes.OrgUnit, ProjectResources.OrgUnitLabel),
                new SearchArea(SearchAreaCodes.JiraKey, ProjectResources.JiraKeyLabel),
                new SearchArea(SearchAreaCodes.Apn, ProjectResources.ApnLabel),
                new SearchArea(SearchAreaCodes.KupferwerkProjectId, ProjectResources.KupferwerkProjectIdLabel),
                new SearchArea(SearchAreaCodes.Region, ProjectResources.RegionLabel),
            };

            SetEditButton();
        }

        public override ActionResult Edit(long id)
        {
            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanEditProject))
            {
                return EditAll(id);
            }

            return EditOwn(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditProject)]
        public ActionResult EditAll(long id)
        {
            return base.Edit(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditOwnProjects)]
        public ActionResult EditOwn(long id)
        {
            if (!_projectService.IsEmployeeOwnProject(_principalProvider.Current.EmployeeId, id))
            {
                throw new BusinessException(ProjectResources.IsNotCurrentEmployeeOwnProjectMessage);
            }

            return base.Edit(id);
        }

        private void SetEditButton()
        {
            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanEditProject))
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.Grid;
            }
            else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanEditOwnProjects))
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.Grid;

                CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(SubProjectViewModel.IsOwnProject))}"
                };
            }
            else
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            }
        }

        private IEnumerable<IGridViewConfiguration> GetViewConfiguration()
        {
            var defaultView = new GridViewConfiguration<SubProjectViewModel>(ProjectResources.DefaultView, "default");
            defaultView.IncludeAllColumns();
            defaultView.ExcludeColumn(m => m.PetsCode);
            defaultView.ExcludeColumn(m => m.JiraIssueKey);
            defaultView.ExcludeColumn(m => m.JiraKey);
            defaultView.ExcludeColumn(m => m.Apn);
            defaultView.ExcludeColumn(m => m.KupferwerkProjectId);
            defaultView.IsDefault = true;
            
            yield return defaultView;

            var extendedConfiguration = new GridViewConfiguration<SubProjectViewModel>(ProjectResources.ExtendedView, "extended");
            extendedConfiguration.IncludeAllColumns();
            
            yield return extendedConfiguration;
        }

        public override ActionResult Add(SubProjectViewModel viewModelRecord)
        {
            return base.Add(viewModelRecord);
        }
    }
}