﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("EmployeeValuePickers.Employee")]
    [AtomicAuthorize(SecurityRoleType.CanViewEmployee)]
    public class EmployeePickerController
        : CardIndexController<EmployeeViewModel, EmployeeDto, EmployeePickerContext>
        , IHubController
    {
        private const string MinimalViewId = "minimal";
        private readonly IUserService _userService;
        private readonly IClassMapping<UserDto, UserViewModel> _userDtoToViewModelMapping;

        public EmployeePickerController(
            IEmployeePickerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IUserService userService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _userService = userService;
            _userDtoToViewModelMapping = baseControllerDependencies.ClassMappingFactory.CreateMapping<UserDto, UserViewModel>();

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                GetSimpleFilterGroup()
            };

            if (baseControllerDependencies.PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
            {
                CardIndex.Settings.FilterGroups.Add(GetStatusFilterGroup());
            }
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<EmployeeViewModel>(EmployeeResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<EmployeeViewModel, object>>>
            {
                c => c.FirstName,
                x => x.LastName,
                x => x.Acronym
            });
            configurations.Add(configuration);

            return configurations;
        }

        public override ActionResult List(GridParameters parameters)
        {
            if (parameters.Filters == null)
            {
                parameters.Filters = FilterCodes.AllEmployees;
            }

            return base.List(parameters);
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);
       
            Layout.PageTitle = allowMultipleRowSelection
                ? EmployeeResources.SelectPersons
                : EmployeeResources.SelectPerson;
        }

        private FilterGroup GetSimpleFilterGroup()
        {
            return new FilterGroup
            {
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter { Code = FilterCodes.MyEmployees, DisplayName = EmployeeResources.MyEmployees },
                    new Filter { Code = FilterCodes.AllEmployees, DisplayName = EmployeeResources.AllEmployees }
                }
            };
        }

        private FilterGroup GetStatusFilterGroup()
        {
            return new FilterGroup
            {
                Type = FilterGroupType.OrCheckboxGroup,
                DisplayName = EmployeeResources.EmployeeStatus,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmployeeStatus>()
            };
        }

        public PartialViewResult Hub(long id)
        {
            var employee = CardIndex.GetRecordById(id);
            var user = _userService.GetUserById(employee.UserId.Value);

            var model = new UserEmployeeHubViewModel(GetCurrentPrincipal())
            {
                Employee = employee,
                User = _userDtoToViewModelMapping.CreateFromSource(user),
            };

            var dialogModel = new ModelBasedDialogViewModel<UserEmployeeHubViewModel>(model, StandardButtons.None)
            {
                ClassName = "user-employee-hub",
            };

            return PartialView(
                "~/Views/Shared/_UserEmployeeHub.cshtml",
                dialogModel);
        }
    }
}
