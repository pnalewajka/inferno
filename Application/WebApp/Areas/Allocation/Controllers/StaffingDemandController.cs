﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.Business.Allocation.Consts;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewStaffingDemand)]
    public class StaffingDemandController : SubCardIndexController<StaffingDemandViewModel, StaffingDemandDto, ProjectController>
    {
        public StaffingDemandController(IStaffingDemandCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            ITimeService timeService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddStaffingDemand;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditStaffingDemand;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteStaffingDemand;

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportProjectStaffingDemands);

            var indexImportSettings = CardIndex.Settings.AddImport<StaffingDemandViewModel, StaffingDemandDto>(cardIndexDataService, AllocationResources.StaffingDemandImportButtonName);
            indexImportSettings.RequiredRole = SecurityRoleType.CanImportStaffingDemand;

            CardIndex.Settings.Title = ProjectResources.StaffingDemandTitle;
            CardIndex.Settings.DefaultNewRecord = () => new StaffingDemandViewModel()
            {
                ProjectId = this.Context.ParentId,
                StartDate = timeService.GetCurrentDate(),
            };

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<StaffingDemandViewModel>(a => a.ProjectId).IsReadOnly = true;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<StaffingDemandViewModel>(a => a.ProjectId).IsReadOnly = true;
            };

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<StaffingDemandDateFilterViewModel>(FilterCodes.DateRange, AllocationResources.EmployeeAllocationFilter)
                    }
                }
            };
        }
    }
}