﻿using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;
using static Smt.Atomic.WebApp.Areas.Allocation.Models.EmployeeAllocationViewModel;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployeeAllocationList)]
    [PerformanceTest(nameof(EmployeeAllocationController.List))]
    public class EmployeeAllocationController : EmployeeAllocationBaseController<IEmployeeAllocationCardIndexDataService>
    {
        private readonly ISystemParameterService _parameterService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IEmployeeAllocationService _employeeAllocationService;
        private readonly IClassMappingFactory _classMappingFactory;

        public EmployeeAllocationController(IEmployeeAllocationCardIndexDataService cardIndexDataService,
            IEmployeeAllocationService employeeAllocationService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _parameterService = baseControllerDependencies.SystemParameterService;
            _principalProvider = baseControllerDependencies.PrincipalProvider;
            _employeeAllocationService = employeeAllocationService;
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;

            var addActions =
                AllocationRequestController.AddCommentButtons(Url, KnownParameter.SelectedIds,
                    $"{Layout.Scripts.ControllerAreaName}/{Layout.Scripts.ControllerName}/List").ToList();

            if (addActions.Any())
            {
                CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddAllocationRequest;
                CardIndex.Settings.AddButton.OnClickAction = new DropdownAction
                {
                    Items = addActions
                };

                CardIndex.Settings.AddButton.Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                };
            }
            else
            {
                CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            }
        }

        protected override IEnumerable<FilterGroup> GetOrganizationFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.OrganizationTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter,
                        AllocationResources.EmployeeOrgUnitFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.OrCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new ParametricFilter<LocationFilterViewModel>(FilterCodes.LocationFilter,
                        AllocationResources.EmployeeLocationFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeesTitle,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<EmployeeLineManagerFilterViewModel>(FilterCodes.LineManagerFilter,
                        AllocationResources.EmployeeLineManagerFilterName)
                }
            };
        }

        protected override IEnumerable<Filter> GetCompetencesFilters()
        {
            yield return new ParametricFilter<SkillsAndKnowledgeFilterViewModel>(
                FilterCodes.SkillsAndKnowledgeFilter,
                AllocationResources.SkillsAndKnowledgeFilterName);

            yield return new Filter
            {
                Code = FilterCodes.ProjectContributorFilter,
                DisplayName = AllocationResources.EmployeeProjectContributor
            };

            yield return new ParametricFilter<SkillsFilterViewModel>(
                FilterCodes.SkillsFilter,
                AllocationResources.SkillsFilterName);

            yield return new ParametricFilter<LanguageWithLevelFilterViewModel>(FilterCodes.LanguagesFilter,
                EmployeeResources.LanguageWithLevelFilterName);
        }

        [BreadcrumbBar("EmployeeAllocation")]
        public ActionResult Beta(GridParameters parameters)
        {
            Layout.Scripts.Add("~/Areas/Allocation/Scripts/AllocationRequest.js");

            CardIndex.Settings.CustomGridDisplayModeViewName = "_ReactCustomTable";
            CardIndex.Settings.Title = Layout.PageTitle = $"Beta: {AllocationResources.EmployeeWeeklyAllocationTitle}";

            CardIndex.Settings.ListViewName = "~/Areas/Allocation/Views/EmployeeAllocation/List.cshtml";

            Layout.Css.Add("~/Areas/Allocation/Content/AllocationReact.css");
            Context.Beta = true;
            ViewBag.Beta = true;

            ViewBag.Permissions = new
            {
                CanEditRequest = _principalProvider.Current.IsInRole(SecurityRoleType.CanEditAllocationRequest),
                CanViewRequest = _principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllocationRequest),
                CanEditEmployee = _principalProvider.Current.IsInRole(SecurityRoleType.CanEditEmployee),
                CanViewEmployee = _principalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployee),
                CanCloneRequest = _principalProvider.Current.IsInRole(SecurityRoleType.CanAddAllocationRequest),
                CanDeleteRequest = _principalProvider.Current.IsInRole(SecurityRoleType.CanResignAllocationRequest),
                CanEditProject = _principalProvider.Current.IsInRole(SecurityRoleType.CanEditProject),
                CanViewProject = _principalProvider.Current.IsInRole(SecurityRoleType.CanViewProject),
            };

            Layout.Resources.AddFrom<AllocationRequestResources>();
            Layout.Resources.AddFrom<EmployeeAllocation>();
            Layout.EnumResources.Add<AllocationCertainty>();

            ViewBag.JiraIssueKeyProjectUrlFormat = _parameterService.GetParameter<string>(ParameterKeys.JiraIssueKeyProjectUrlFormat);
            ViewBag.JiraKeyProjectKeyUrlFormat = _parameterService.GetParameter<string>(ParameterKeys.JiraKeyProjectKeyUrlFormat);

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                DisplayName = AllocationResources.CustomizeViewHeader,
                ButtonText = AllocationResources.CustomizeViewHeader,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter> {
                    new Filter { Code = FilterCodes.FakeDetailedFilter, DisplayName = AllocationResources.ViewOptionDetailed, },
                    new Filter { Code = FilterCodes.FakePlannedFilter, DisplayName = AllocationResources.ShowPlannedLabel, },
                    new Filter { Code = FilterCodes.FakeColorsFilter, DisplayName = AllocationResources.ShowProjectColorsLabel, },
                    new Filter { Code = FilterCodes.FakeWeekendsFilter, DisplayName = EmployeeAllocation.ShowWeekends, }
                }
            });

            CardIndex.Settings.AddButton.OnClickAction = new DropdownAction
            {
                Items = new List<ICommandButton>
                {
                    new CommandButton
                        {
                            Group = AllocationRequestResources.AddAllocationRequestOfTypeHeaderText,
                            RequiredRole = SecurityRoleType.CanAddAllocationRequest,
                            Text = AllocationRequestContentType.Simple.GetDescription(),
                            OnClickAction = new JavaScriptCallAction($"Allocation.Screen.openAddRequestDialog('{Url.Action("GetAddDialog", "AllocationRequest")}', '{AllocationRequestContentType.Simple.ToString().ToLower()}')")
                        },
                    new CommandButton
                        {
                            Group = AllocationRequestResources.AddAllocationRequestOfTypeHeaderText,
                            RequiredRole = SecurityRoleType.CanAddAllocationRequest,
                            Text = AllocationRequestContentType.Advanced.GetDescription(),
                            OnClickAction = new JavaScriptCallAction($"Allocation.Screen.openAddRequestDialog('{Url.Action("GetAddDialog", "AllocationRequest")}', '{AllocationRequestContentType.Advanced.ToString().ToLower()}')")
                        },
                }
            };

            var listResult = List(parameters);

            // Get current context from/to
            ViewBag.StatusFrom = CardIndexDataService.StatusFrom.Value;
            ViewBag.StatusTo = CardIndexDataService.StatusTo.Value;

            return listResult;
        }

        [HttpPost]
        public JsonNetResult EditRequest(long id, EditAllocationRequestModel model)
        {
            var editResultDto = _employeeAllocationService.Edit(id, model);

            var dtoClassMapping = _classMappingFactory.CreateMapping<AllocationBlockDto, AllocationBlockViewModel>();

            return new JsonNetResult(dtoClassMapping.CreateFromSource(editResultDto)) { SerializerSettings = JsonHelper.DefaultAjaxSettings };
        }

        private static IGridViewConfiguration CreateGridSettings(string label, string code, bool isDefault = false)
        {
            var c = new GridViewConfiguration<EmployeeAllocationViewModel>(label, code) { IsDefault = isDefault };

            return c;
        }
    }
}