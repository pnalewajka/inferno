﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("EmployeeValuePickers.EmployeeByProjectFilter")]
    [AtomicAuthorize(SecurityRoleType.CanViewEmployee)]
    public class EmployeePickerByProjectFilterController
        : CardIndexController<EmployeeViewModel, EmployeeDto, EmployeePickerContext>
    {
        private readonly IUserService _userService;
        private const string MinimalViewId = "minimal";

        public EmployeePickerByProjectFilterController(
            IEmployeePickerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IUserService userService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _userService = userService;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                DisplayName = EmployeeResources.EmployeeControllerAdvancedFilters,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                    {
                        new ParametricFilter<EmployeePickerByProjectFilterViewModel>(FilterCodes.ProjectAndDateFilter, EmployeeResources.EmployeesBelongToProjectFilterName),
                    }
            });
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<EmployeeViewModel>(EmployeeResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<EmployeeViewModel, object>>>
            {
                c => c.FirstName,
                x => x.LastName,
                x => x.Acronym
            });

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}