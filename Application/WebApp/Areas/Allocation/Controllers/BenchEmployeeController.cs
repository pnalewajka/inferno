﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Resume.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBenchEmployees)]
    [PerformanceTest("view=minimal&period-type=week&week-start.week=2018-09-17", nameof(BenchEmployeeController.List))]
    public class BenchEmployeeController : CardIndexController<BenchEmployeeViewModel, BenchEmployeeDto>
    {
        private readonly ITimeService _timeService;
        private readonly IEmployeeCommentService _employeeCommentService;
        private readonly IEmployeeService _employeeService;

        private const int ExportPageSize = 1000;

        public BenchEmployeeController(IBenchEmployeeCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies, ITimeService timeService,
            IEmployeeCommentService employeeCommentService, IEmployeeService employeeService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _timeService = timeService;
            _employeeCommentService = employeeCommentService;
            _employeeService = employeeService;
            Layout.Scripts.Add("~/Areas/Allocation/Scripts/BenchEmployees.js");
            Layout.Scripts.Add("~/Areas/Allocation/Scripts/AllocationDialog.js");
            Layout.Scripts.Add("~/Areas/Resume/Scripts/ResumeApiService.js");

            CardIndex.Settings.Title = AllocationResources.BenchedEmployees;

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();
            CardIndex.Settings.FilterGroups.AddRange(EmployeeFilterGroup(AllocationResources.EmployeeFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(OrganizationFilterGroup(AllocationResources.OrgFilterTitle));
            CardIndex.Settings.FilterGroups.AddRange(ScopeFilterGroup(AllocationResources.ScopeFilterTitle));

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportNotAllocatedEmployees);

            CardIndex.Settings.PageSize = 50;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Group = "date-navigations",
                Icon = FontAwesome.Archive,
                Text = EmployeeResources.AllocationRequests,
                OnClickAction = new JavaScriptCallAction($"BenchEmployees.handleRowDoubleClick"),
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                RequiredRole = SecurityRoleType.CanViewAllocationRequest,
                IsDefault = true
            });

            GetWeekNavigationButtons();

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = EmployeeResources.CommentButtonText,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                RequiredRole = SecurityRoleType.CanCommentOnBenchEmployees,
                OnClickAction = new JavaScriptCallAction($"AllocationDialog.onCommentClicked('{Url.Action("AddEditComment", "BenchEmployee", new { Area = "Allocation" })}');")
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ResumeResources.Generate,
                OnClickAction = new DropdownAction
                {
                    Items = new List<ICommandButton>
                    {
                        new CommandButton()
                        {
                            Text = ResumeResources.GenerateSelectedResumes,
                            OnClickAction = new JavaScriptCallAction("ResumeApi.GenerateResumes(grid);"),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.AtLeastOneRowSelected
                            },
                            IsDefault = true
                        }
                    }
                },
                Visibility = CommandButtonVisibility.Grid,
                Group = "Resume",
                Icon = FontAwesome.Tablet,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                },
                RequiredRole = SecurityRoleType.CanGenerateResumeReport,
            });

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodesBenchEmployee.FirstName,  EmployeeResources.EmployeeFirstNameLabel),
                new SearchArea(SearchAreaCodesBenchEmployee.LastName,  EmployeeResources.EmployeeLastNameLabel),
                new SearchArea(SearchAreaCodesBenchEmployee.EmployeeStatus,  EmployeeResources.EmployeeStatusLabel, false),
                new SearchArea(SearchAreaCodesBenchEmployee.LocationName, EmployeeResources.EmployeeLocationLabel, false),
                new SearchArea(SearchAreaCodesBenchEmployee.LineMaganer, EmployeeResources.EmployeeLineManager),
                new SearchArea(SearchAreaCodesBenchEmployee.OrganizationUnit, EmployeeResources.EmployeeOrgUnit),
                new SearchArea(SearchAreaCodesBenchEmployee.Skills, EmployeeResources.EmployeeSkillsLabel, false),
                new SearchArea(SearchAreaCodesBenchEmployee.JobProfileName, EmployeeResources.JobProfileLabel),
                new SearchArea(SearchAreaCodesBenchEmployee.JobTitle, EmployeeResources.JobTitleLabel),
                new SearchArea(SearchAreaCodesBenchEmployee.StaffingStatus, EmployeeResources.StaffingStatus, false),
            };

            CardIndex.Settings.GridViewConfigurations = GetGridViewConfigurations();
        }

        private IEnumerable<FilterGroup> EmployeeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.Competences,
                Type = FilterGroupType.AndCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new ParametricFilter<SkillsAndKnowledgeFilterViewModel>(
                        FilterCodes.SkillsAndKnowledgeFilter, AllocationResources.SkillsAndKnowledgeFilterName),
                    new Filter
                    {
                        Code = FilterCodes.ProjectContributorFilter,
                        DisplayName = AllocationResources.EmployeeProjectContributor
                    },
                    new ParametricFilter<SkillsFilterViewModel>(
                        FilterCodes.SkillsFilter, AllocationResources.SkillsFilterName),
                    new ParametricFilter<LanguageWithLevelFilterViewModel>(FilterCodes.LanguagesFilter,
                        EmployeeResources.LanguageWithLevelFilterName)
        }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = EmployeeResources.EmployeeStatus,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmployeeStatus>()
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = EmployeeResources.EmploymentType,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<EmploymentType>()
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.StaffingStatus,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new Filter { Code = FilterCodes.AllocationStatusFreeFilter, DisplayName = AllocationResources.StaffingStatusFree },
                    new Filter { Code = FilterCodes.AllocationStatusBusyFilter, DisplayName = AllocationResources.StaffingStatusBusy },
                }
            };
        }

        private IEnumerable<FilterGroup> OrganizationFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = EmployeeResources.OrganizationTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter,
                        EmployeeResources.EmployeeOrgUnitFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                Type = FilterGroupType.OrCheckboxGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new ParametricFilter<LocationFilterViewModel>(FilterCodes.LocationFilter,
                        EmployeeResources.EmployeeLocationFilterName)
                }
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = EmployeeResources.EmployeesTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<EmployeeLineManagerFilterViewModel>(FilterCodes.LineManagerFilter,
                        EmployeeResources.EmployeeLineManagerFilterName)
                }
            };
        }

        private IEnumerable<FilterGroup> ScopeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.WeekFilter,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<BenchEmployeeFilterViewModel>(FilterCodes.WeekFilter, string.Empty) { CanBeSwitchedOff = false },
                },
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.ManagerFilter,
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter { Code = FilterCodes.MyEmployees, DisplayName = EmployeeResources.MyEmployees },
                    new Filter { Code = FilterCodes.MyDirectEmployees, DisplayName = EmployeeResources.MyDirectEmployees },
                    new Filter { Code = FilterCodes.AllEmployees, DisplayName = EmployeeResources.AllEmployees }
                },
            };

            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = AllocationResources.EmployeeCompany,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<CompanyFilterViewModel>(FilterCodes.CompanyFilter, AllocationResources.EmployeeCompanyFilterName)
                }
            };
        }

        public override ActionResult List(GridParameters parameters)
        {
            AddWeekToPageTitle(parameters);

            return base.List(parameters);
        }

        private void GetWeekNavigationButtons()
        {
            CardIndex.Settings.ToolbarButtons.Add(CreateWeekChangeButton(FontAwesome.CaretLeft,
                EmployeeResources.PreviousWeek, "BenchEmployees.setPreviousWeek()"));
            CardIndex.Settings.ToolbarButtons.Add(CreateWeekChangeButton(FontAwesome.MapMarker,
                EmployeeResources.CurrentWeek, "BenchEmployees.setCurrentWeek()"));
            CardIndex.Settings.ToolbarButtons.Add(CreateWeekChangeButton(FontAwesome.CaretRight,
                EmployeeResources.NextWeek, "BenchEmployees.setNextWeek()"));
        }

        private CommandButton CreateWeekChangeButton(string icon, string text, string javaScriptCallActionName)
        {
            return new ToolbarButton
            {
                Group = "date-navigation",
                Icon = icon,
                Text = text,
                OnClickAction = new JavaScriptCallAction(javaScriptCallActionName)
            };
        }

        private void AddWeekToPageTitle(GridParameters parameters)
        {
            var weekFilterValue = parameters.QueryStringFieldValues[$"{FilterCodes.WeekFilter}.week"] as string;

            if (!string.IsNullOrWhiteSpace(weekFilterValue))
            {
                var date = DateTime.Parse(weekFilterValue, CultureInfo.InvariantCulture);
                CardIndex.Settings.Title = CardIndex.Settings.Title + date.ToShortDateString();
            }
            else
            {
                CardIndex.Settings.Title = CardIndex.Settings.Title + GetFirstDayOfCurrentWeekDate();
            }
        }

        private string GetFirstDayOfCurrentWeekDate()
        {
            return _timeService.GetCurrentDate().GetPreviousDayOfWeek(DayOfWeek.Monday).ToShortDateString();
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanCommentOnBenchEmployees)]
        public ActionResult AddEditComment(long id, DateTime? date)
        {
            var employeeComment = _employeeCommentService.GetEmployeeComment(EmployeeCommentType.StaffingComment, id, date);

            var commentViewModel = new CommentViewModel
            {
                EmployeeCommentType = EmployeeCommentType.StaffingComment,
                Description = employeeComment?.Comment,
                EmployeeId = id,
                RelevantOn = date,
                EmployeeCommentId = employeeComment?.Id
            };

            var employeeDto = _employeeService.GetEmployeeOrDefaultById(id);

            var dialogModel = new ModelBasedDialogViewModel<CommentViewModel>(commentViewModel)
            {
                Title = string.Format(EmployeeResources.CommentBenchEmployeeTitle, employeeDto?.FirstName, employeeDto?.LastName)
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanCommentOnBenchEmployees)]
        public ActionResult AddInlineProject(CommentViewModel model)
        {
            var employeeDto = _employeeService.GetEmployeeOrDefaultById(model.EmployeeId);

            var dialogModel = new ModelBasedDialogViewModel<CommentViewModel>(model)
            {
                Title = string.Format(EmployeeResources.CommentBenchEmployeeTitle, employeeDto?.FirstName, employeeDto?.LastName)
            };

            if (!ModelState.IsValid)
            {
                return PartialView("AddEditComment", dialogModel);
            }

            if (model.EmployeeCommentId.HasValue)
            {
                var employeeCommentDto = new EmployeeCommentDto
                {
                    Id = model.EmployeeCommentId.Value,
                    Comment = model.Description,
                };

                _employeeCommentService.EditEmployeeComment(employeeCommentDto);
            }
            else
            {
                var employeeCommentDto = new EmployeeCommentDto
                {
                    Comment = model.Description,
                    EmployeeCommentType = EmployeeCommentType.StaffingComment,
                    EmployeeId = model.EmployeeId,
                    RelevantOn = model.RelevantOn
                };

                _employeeCommentService.AddEmployeeComment(employeeCommentDto);
            }

            return DialogHelper.Reload();
        }

        public override FilePathResult ExportToExcel(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;
            return base.ExportToExcel(parameters);
        }

        public override FilePathResult ExportToCsv(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;
            return base.ExportToCsv(parameters);
        }

        private static IList<IGridViewConfiguration> GetGridViewConfigurations()
        {
            return new List<IGridViewConfiguration>
            {
                GetListGridViewConfiguration(),
                GetResumeFillListGridViewConfiguration(),
                GetVacationBalanceListGridViewConfiguration()
            };
        }

        private static GridViewConfiguration<BenchEmployeeViewModel> GetResumeFillListGridViewConfiguration()
        {
            var resumeFillListConfiguration = new GridViewConfiguration<BenchEmployeeViewModel>(EmployeeResources.ResumePercentageFilling, "resumePercentageFillList");

            resumeFillListConfiguration.IncludeAllColumns(); ;
            resumeFillListConfiguration.ExcludeColumn(x => x.AllocationDescription);
            resumeFillListConfiguration.ExcludeColumn(x => x.WeeksOnBench);
            resumeFillListConfiguration.ExcludeColumn(x => x.ManagerId);
            resumeFillListConfiguration.ExcludeColumn(x => x.VacationBalance);

            return resumeFillListConfiguration;
        }

        private static GridViewConfiguration<BenchEmployeeViewModel> GetListGridViewConfiguration()
        {
            var listConfiguration = new GridViewConfiguration<BenchEmployeeViewModel>(EmployeeResources.EmployeeListViewName, "list")
            {
                IsDefault = true
            };

            listConfiguration.IncludeAllColumns();
            listConfiguration.ExcludeColumn(x => x.FillPercentage);
            listConfiguration.ExcludeColumn(x => x.ResumeModifiedOn);
            listConfiguration.ExcludeColumn(x => x.VacationBalance);

            return listConfiguration;
        }

        private static GridViewConfiguration<BenchEmployeeViewModel> GetVacationBalanceListGridViewConfiguration()
        {
            var vacationBalanceListConfiguration = new GridViewConfiguration<BenchEmployeeViewModel>(VacationBalanceResources.VacationBalanceTitle, "vacationBalanceList");

            vacationBalanceListConfiguration.IncludeAllColumns(); ;
            vacationBalanceListConfiguration.ExcludeColumn(x => x.AllocationDescription);
            vacationBalanceListConfiguration.ExcludeColumn(x => x.WeeksOnBench);
            vacationBalanceListConfiguration.ExcludeColumn(x => x.ManagerId);
            vacationBalanceListConfiguration.ExcludeColumn(x => x.FillPercentage);
            vacationBalanceListConfiguration.ExcludeColumn(x => x.ResumeModifiedOn);

            return vacationBalanceListConfiguration;
        }
    }
}