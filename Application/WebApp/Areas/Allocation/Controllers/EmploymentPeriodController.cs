﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmploymentPeriods)]
    public class EmploymentPeriodController : SubCardIndexController<EmploymentPeriodViewModel, EmploymentPeriodDto, EmployeeController>
    {
        private const decimal DefaultHoursPerDay = 8;

        private readonly IAllocationContractTypeService _contractTypeService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public EmploymentPeriodController(
            IAllocationContractTypeService contractTypeService,
            IEmploymentPeriodCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IDataActivityLogService dataActivityLogService,
            ITimeService timeService) : base(cardIndexDataService, baseControllerDependencies)
        {
            _contractTypeService = contractTypeService;
            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.Title = EmployeeResources.HrdataTabName;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddEmploymentPeriods;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditEmploymentPeriods;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteEmploymentPeriods;

            CardIndex.Settings.DefaultNewRecord = () =>
            {
                using (var unitOfWork = unitOfWorkService.Create())
                {
                    var lastPeriod = unitOfWork.Repositories.EmploymentPeriods
                        .Where(p => p.EmployeeId == Context.ParentId)
                        .OrderByDescending(p => p.StartDate)
                        .FirstOrDefault();

                    return new EmploymentPeriodViewModel()
                    {
                        Employee = Context.ParentId,
                        StartDate = timeService.GetCurrentDate(),
                        EndDate = null,
                        SignedOn = timeService.GetCurrentDate(),
                        CompanyId = lastPeriod?.CompanyId,
                        JobTitleId = lastPeriod?.JobTitleId,
                        MondayHours = lastPeriod?.MondayHours ?? DefaultHoursPerDay,
                        TuesdayHours = lastPeriod?.TuesdayHours ?? DefaultHoursPerDay,
                        WednesdayHours = lastPeriod?.WednesdayHours ?? DefaultHoursPerDay,
                        ThursdayHours = lastPeriod?.ThursdayHours ?? DefaultHoursPerDay,
                        FridayHours = lastPeriod?.FridayHours ?? DefaultHoursPerDay,
                        SaturdayHours = lastPeriod?.SaturdayHours ?? 0,
                        SundayHours = lastPeriod?.SundayHours ?? 0,
                        VacationHourToDayRatio = lastPeriod?.VacationHourToDayRatio ?? DefaultHoursPerDay,
                        IsActive = true,
                        TimeReportingMode = lastPeriod?.TimeReportingMode ?? TimeReportingMode.Detailed,
                        AbsenceOnlyDefaultProjectId = lastPeriod?.AbsenceOnlyDefaultProjectId,
                        AuthorRemunerationRatio = lastPeriod?.AuthorRemunerationRatio,
                        CalendarId = lastPeriod?.CalendarId,
                        ContractDuration = lastPeriod?.ContractDuration,
                        ContractorCompanyName = lastPeriod?.ContractorCompanyName,
                        ContractTypeId = lastPeriod?.ContractTypeId ?? BusinessLogicHelper.NonExistingId,
                        EmploymentType = lastPeriod?.ContractType?.EmploymentType,
                        HourlyRate = lastPeriod?.HourlyRate,
                        HourlyRateDomesticOutOfOfficeBonus = lastPeriod?.HourlyRateDomesticOutOfOfficeBonus,
                        HourlyRateForeignOutOfOfficeBonus = lastPeriod?.HourlyRateForeignOutOfOfficeBonus,
                        IsClockCardRequired = lastPeriod?.IsClockCardRequired ?? false,
                        MonthlyRate = lastPeriod?.MonthlyRate,
                        NoticePeriod = lastPeriod?.NoticePeriod,
                        SalaryGross = lastPeriod?.SalaryGross,
                        ContractorCompanyCity = lastPeriod?.ContractorCompanyCity,
                        ContractorCompanyStreetAddress = lastPeriod?.ContractorCompanyStreetAddress,
                        ContractorCompanyTaxIdentificationNumber = lastPeriod?.ContractorCompanyTaxIdentificationNumber,
                        ContractorCompanyZipCode = lastPeriod?.ContractorCompanyZipCode,
                        CompensationCurrencyId = lastPeriod?.CompensationCurrencyId                        
                    };
                }
            };

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<EmploymentPeriodViewModel>(a => a.Employee).IsReadOnly = true;
                form.GetControlByName<EmploymentPeriodViewModel>(a => a.ChangeExistingAllocations).Visibility = ControlVisibility.Hide;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<EmploymentPeriodViewModel>(a => a.Employee).IsReadOnly = true;
                form.GetControlByName<EmploymentPeriodViewModel>(a => a.ClosePreviousPeriod).Visibility = ControlVisibility.Hide;
            };

            CardIndex.Settings.ViewFormCustomization = form =>
            {
                var viewModel = (EmploymentPeriodViewModel)form.ViewModel;
                var timeTrackingCalculator = _contractTypeService.GetTimeTrackingCompensationCalculatorOrDefaultByContractTypeId(viewModel.ContractTypeId);
                var businessTripCalculator = _contractTypeService.GetBusinessTripCompensationCalculatorOrDefaultByContractTypeId(viewModel.ContractTypeId);

                foreach (var propertyInfo in viewModel.GetType().GetProperties())
                {
                    var requirement = AttributeHelper.GetPropertyAttribute<CompensationCalculatorRequirementAttribute>(propertyInfo);

                    if (requirement != null)
                    {
                        if (!ContainsCalculator(requirement, timeTrackingCalculator, businessTripCalculator))
                        {
                            form.Controls.Single(c => c.Name == propertyInfo.Name).Visibility = ControlVisibility.Hide;
                        }
                    }
                }
            };

            CardIndex.Settings.FilterGroups = new List<FilterGroup>()
            {
                new FilterGroup()
                {
                    DisplayName = EmployeeResources.EmploymentAdvanced,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>()
                    {
                        new ParametricFilter<EmploymentPeriodDateFilterViewModel, EmploymentPeriodDateFilterDto>("EmploymentDateRange", EmployeeResources.EmploymentBetween)
                    }
                }
            };
        }

        [HttpPost]
        public JsonNetResult UpdateCompensationParametersVisibility(EmploymentPeriodViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<EmploymentPeriodViewModel>();

            var timeTrackingCalculator = _contractTypeService.GetTimeTrackingCompensationCalculatorOrDefaultByContractTypeId(viewModel.ContractTypeId);
            var businessTripCalculator = _contractTypeService.GetBusinessTripCompensationCalculatorOrDefaultByContractTypeId(viewModel.ContractTypeId);

            foreach (var propertyInfo in viewModel.GetType().GetProperties())
            {
                var requirement = AttributeHelper.GetPropertyAttribute<CompensationCalculatorRequirementAttribute>(propertyInfo);

                if (requirement != null)
                {
                    var parameter = Expression.Parameter(viewModel.GetType());
                    var property = Expression.Property(parameter, propertyInfo);
                    var conversion = Expression.Convert(property, typeof(object));
                    var lambda = Expression.Lambda<Func<EmploymentPeriodViewModel, object>>(conversion, parameter);

                    response.Operations.Add(new ClientSideVisibilityOperation<EmploymentPeriodViewModel>(
                        lambda, ContainsCalculator(requirement, timeTrackingCalculator, businessTripCalculator)
                    ));
                }
            }

            return response.ToJsonNetResult();
        }

        public override ActionResult List(GridParameters parameters)
        {
            _dataActivityLogService.LogEmployeeDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        private static bool ContainsCalculator(
            CompensationCalculatorRequirementAttribute requirement,
            TimeTrackingCompensationCalculators? timeTrackingCalculator,
            BusinessTripCompensationCalculators? businessTripCalculator)
        {
            return (timeTrackingCalculator.HasValue && requirement.TimeTrackingCalculatorTypes.Contains(timeTrackingCalculator.Value))
                || (businessTripCalculator.HasValue && requirement.BusinessTripCalculatorTypes.Contains(businessTripCalculator.Value));
        }
    }
}
