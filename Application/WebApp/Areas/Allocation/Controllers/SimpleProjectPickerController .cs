﻿using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("ValuePickers.SimpleProject")]
    public class SimpleProjectPickerController : ProjectPickerController
    {
        public SimpleProjectPickerController(
            IProjectCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.ToolbarButtons.Clear();
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            CardIndex.Settings.FilterGroups.Clear();
        }
    }
}