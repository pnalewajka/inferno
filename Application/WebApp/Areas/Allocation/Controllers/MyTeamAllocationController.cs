﻿using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewMyTeamAllocations)]
    public class MyTeamAllocationController : EmployeeAllocationBaseController<IMyTeamAllocationCardIndexDataService>
    {
        public MyTeamAllocationController(IMyTeamAllocationCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
        }

        protected override List<Filter> GetManagerFilters()
        {
            return new List<Filter>
            {
                new Filter {Code = FilterCodes.MyProjects, DisplayName = AllocationResources.MyProjects},
                new Filter {Code = FilterCodes.MyTeamAllocations, DisplayName = AllocationResources.MyTeamAllocations}
            };
        }
    }
}