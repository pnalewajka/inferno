﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [Identifier("ValuePickers.ActiveDirectoryGroup")]
    public class ActiveDirectoryGroupPickerController : ActiveDirectoryGroupController
    {
        public ActiveDirectoryGroupPickerController(
            IActiveDirectoryGroupService activeDirectoryGroupService,
            IActiveDirectoryGroupCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(activeDirectoryGroupService, cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<ActiveDirectoryGroupViewModel>(Presentation.Renderers.Resources.CardIndexResources.CardIndexDefaultGridView, "default") { IsDefault = true };

            configuration.IncludeColumns(new List<Expression<Func<ActiveDirectoryGroupViewModel, object>>>
            {
                x => x.Name,
                x => x.Description
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}