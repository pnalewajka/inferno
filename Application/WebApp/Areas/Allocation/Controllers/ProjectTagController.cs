﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProjectTags)]
    public class ProjectTagController : CardIndexController<ProjectTagViewModel, ProjectTagDto>
    {
        public ProjectTagController(IProjectTagCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddProjectTags;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditProjectTags;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteProjectTags;
            CardIndex.Settings.Title = ProjectResources.ProjectTagsTitle;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportProjectTags);
            CardIndex.Settings.AddImport<ProjectTagViewModel, ProjectTagDto>(cardIndexDataService,
                ProjectResources.ImportButtomName, SecurityRoleType.CanAddProjectTags);
        }
    }
}