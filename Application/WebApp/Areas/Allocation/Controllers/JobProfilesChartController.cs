﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployeeAllocationList)]
    public class JobProfilesChartController : AtomicController
    {
        private readonly ITimeService _timeService;
        private readonly IEmployeeAllocationCardIndexDataService _employeeAllocationCardIndexDataService;
        private readonly IJobProfileService _jobProfileService;
        private readonly ILocationService _locationService;

        public JobProfilesChartController(
            IBaseControllerDependencies baseDependencies, ITimeService timeService,
            IEmployeeAllocationCardIndexDataService employeeAllocationCardIndexDataService,
            IJobProfileService jobProfileService,
            ILocationService locationService)
            : base(baseDependencies)
        {
            _timeService = timeService;
            _employeeAllocationCardIndexDataService = employeeAllocationCardIndexDataService;
            _jobProfileService = jobProfileService;
            _locationService = locationService;

            Layout.Scripts.Add("~/Scripts/d3/d3.min.js");
            Layout.Scripts.Add("~/Scripts/Dante/Utils.js");
            Layout.Scripts.Add("~/Scripts/Chart/ReportCharts.js");
            Layout.Css.Add("~/Content/ReportCharts.css");
        }

        public ActionResult Index(long? id, long? locationId, long? level)
        {
            Layout.PageHeader = JobProfilesChartsResources.SingleChartPageTitle;
            Layout.PageTitle = JobProfilesChartsResources.SingleChartPageTitle;

            var allocations = _employeeAllocationCardIndexDataService.CalculateWeekAllocationsForJobProfile(id, locationId, level);
            var jobProfiles = _jobProfileService.GetAllJobProfiles();
            var locations = _locationService.GetAllLocations();
            var levels = EnumHelper.GetEnumValues<JobMatrixLevels>();
            var currentWeek = _timeService.GetCurrentDate().GetPreviousDayOfWeek(DayOfWeek.Monday);

            var viewModel = new JobProfilesChartViewModel(allocations, jobProfiles, locations, levels, currentWeek)
            {
                SelectedItemId = id,
                SelectedLocationId = locationId,
                SelectedLevel = level
            };
            
            return View(viewModel);
        }
    }
}