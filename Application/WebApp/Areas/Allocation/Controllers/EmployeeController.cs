﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Resources;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Resume.Controllers;
using Smt.Atomic.WebApp.Areas.Resume.Models;
using Smt.Atomic.WebApp.Areas.Resume.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployee)]
    [PerformanceTest("view=minimal&period-type=week", nameof(EmployeeController.List))]
    public class EmployeeController : CardIndexController<EmployeeViewModel, EmployeeDto>
    {
        private const int ExportPageSize = 1000;
        private const int MaximumReportFiles = 15;
        private const string MyProfileCustomViewName = "MyProfile";
        private readonly IMyProfileService _myProfileService;
        private readonly IClassMapping<MyProfileDto, MyProfileViewModel> _myProfileDtoToMyProfileViewModelClassMapping;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IReportingService _reportingService;
        private static IAtomicPrincipal _principal;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IEmployeeService _employeeService;
        private readonly ICustomerCardIndexDataService _customerService;

        public EmployeeController(
            IEmployeeCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IMyProfileService myProfileService,
            IClassMapping<MyProfileDto, MyProfileViewModel> myProfileDtoToMyProfileViewModelClassMapping,
            ISystemParameterService systemParameterService,
            IOrgUnitService orgUnitService,
            IReportingService reportingService,
            IDataActivityLogService dataActivityLogService,
            IEmployeeService employeeService,
            ICustomerCardIndexDataService customerService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _systemParameterService = systemParameterService;
            _orgUnitService = orgUnitService;
            _myProfileService = myProfileService;
            _reportingService = reportingService;
            _myProfileDtoToMyProfileViewModelClassMapping = myProfileDtoToMyProfileViewModelClassMapping;
            _dataActivityLogService = dataActivityLogService;
            _employeeService = employeeService;
            _customerService = customerService;

            _principal = GetCurrentPrincipal();

            ConfigureBulkEdit<IEmployeeCardIndexDataService>(SecurityRoleType.CanBulkEditEmployees);

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddEmployee;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteEmployee;

            CardIndex.Settings.Title = EmployeeResources.EmployeesTitle;

            CardIndex.Settings.DefaultNewRecord = () => new EmployeeViewModel { IsProjectContributor = true };

            Layout.Scripts.Add("~/Areas/Allocation/Scripts/AllocationDialog.js");
            Layout.Scripts.Add("~/Areas/Resume/Scripts/ResumeApiService.js");

            CardIndex.Settings.GridViewConfigurations = GetGridSettings();

            var employmentPeriodsButton = new ToolbarButton
            {
                Text = EmployeeResources.HrdataTabName,
                OnClickAction = Url.UriActionGet("List", "EmploymentPeriod", KnownParameter.ParentId),
                Group = "Management",
                Icon = FontAwesome.HandShakeO,
                RequiredRole = SecurityRoleType.CanViewEmploymentPeriods,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            var resumeShareButton = new ToolbarButton
            {
                Text = ResumeResources.ShareResume,
                OnClickAction = new JavaScriptCallAction($"ResumeApi.OpenResumeShareDialog('{Url.UriActionPost(nameof(EmployeeResumeController.Share), RoutingHelper.GetControllerName<EmployeeResumeController>(), KnownParameter.None, "area", RoutingHelper.GetAreaName<EmployeeResumeController>()).Url}/' + grid.getSelectedRowIds()[0])"),
                Group = "Resume",
                Icon = FontAwesome.Share,
                RequiredRole = SecurityRoleType.CanShareEmployeesResume,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            var resumeEditionButton = new ToolbarButton
            {
                Text = ResumeResources.EditResume,
                OnClickAction = Url.UriActionGet("Index", "EmployeeResume", KnownParameter.SelectedId, "area", "Resume"),
                Group = "Resume",
                Icon = FontAwesome.Edit,
                RequiredRole = SecurityRoleType.CanEditEmployeesResume,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            var employeeGenerationButton = new ToolbarButton
            {
                Text = ResumeResources.Generate,
                OnClickAction = new DropdownAction()
                {
                    Items = new List<ICommandButton>()
                    {
                        new CommandButton()
                        {
                            Text = ResumeResources.GenerateSelectedResumes,
                            OnClickAction = new JavaScriptCallAction("ResumeApi.GenerateResumes(grid);"),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.AtLeastOneRowSelected
                            },
                            IsDefault = true
                        }
                    }
                },
                Visibility = CommandButtonVisibility.Grid,
                Group = "Resume",
                Icon = FontAwesome.Tablet,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                },
                RequiredRole = SecurityRoleType.CanGenerateResumeReport,
            };

            CardIndex.Settings.ToolbarButtons.Add(employmentPeriodsButton);

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportEmployees);

            CardIndex.Settings.ToolbarButtons.Add(resumeEditionButton);

            CardIndex.Settings.ToolbarButtons.Add(resumeShareButton);

            CardIndex.Settings.ToolbarButtons.Add(employeeGenerationButton);

            CardIndex.Settings.AddImport<EmployeeViewModel, EmployeeDto>(cardIndexDataService, EmployeeResources.ImportButtomName, SecurityRoleType.CanImportEmployee);

            CardIndex.Settings.DefaultNewRecord = () => new EmployeeViewModel { CurrentEmployeeStatus = EmployeeStatus.NewHire };

            var employeeFilterButtonText = EmployeeResources.EmployeeFilterTitle;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = AllocationResources.Competences,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<SkillsAndKnowledgeFilterViewModel>(
                            FilterCodes.SkillsAndKnowledgeFilter, AllocationResources.SkillsAndKnowledgeFilterName),
                        new ParametricFilter<IndustryFilterViewModel>(
                            FilterCodes.Industries, DictionaryResources.IndustryTitle),
                        new Filter {Code = FilterCodes.ProjectContributorFilter, DisplayName = EmployeeResources.EmployeeIsProjectContributorLabel},
                        new ParametricFilter<SkillsFilterViewModel>(FilterCodes.SkillsFilter,AllocationResources.SkillsFilterName),
                        new ParametricFilter<LanguageWithLevelFilterViewModel>(FilterCodes.LanguagesFilter,EmployeeResources.LanguageWithLevelFilterName)
                    }
                },

                GetSimpleFilterGroup(),

                new FilterGroup
                {
                    DisplayName = EmployeeResources.EmployeeCompany,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<CompanyFilterViewModel>(FilterCodes.CompanyFilter, EmployeeResources.EmployeeCompanyFilterName)
                    }
                },

                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                    Filters = new List<Filter>
                {
                    new ParametricFilter<LocationFilterViewModel>(FilterCodes.LocationFilter, EmployeeResources.EmployeeLocationFilterName)
                }
                },

                new FilterGroup
                {
                    DisplayName = EmployeeResources.EmployeePlaceOfWork,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<PlaceOfWork>()
                },

                new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmploymentType,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<EmploymentType>()
                },

                new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmployeesTitle,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<EmployeeLineManagerFilterViewModel>(FilterCodes.LineManagerFilter, EmployeeResources.EmployeeLineManagerFilterName)
                    }
                }
            };

            if (baseControllerDependencies.PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewContractType))
            {
                CardIndex.Settings.FilterGroups.Add(new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmploymentPeriodContractType,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<ContractTypeFilterViewModel>(FilterCodes.ContractTypeFilter,
                            EmployeeResources.EmployeeContractTypeFilterName)
                    }
                });
            }

            // only show the Employee Status filter to people with proper role
            if (baseControllerDependencies.PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
            {
                CardIndex.Settings.FilterGroups.Add(new FilterGroup
                {
                    ButtonText = employeeFilterButtonText,
                    DisplayName = EmployeeResources.EmployeeStatus,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<EmployeeStatus>()
                });
            }

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                ButtonText = employeeFilterButtonText,
                DisplayName = EmployeeResources.JobProfileLabel,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<JobProfileFilterViewModel>(FilterCodes.JobProfileFilter, EmployeeResources.EmployeeJobProfileFilterName)
                }
            });

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                DisplayName = EmployeeResources.OrganizationTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter, EmployeeResources.EmployeeOrgUnitFilterName)
                }
            });

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.FirstName,  EmployeeResources.EmployeeFirstNameLabel),
                new SearchArea(SearchAreaCodes.LastName,  EmployeeResources.EmployeeLastNameLabel),
                new SearchArea(SearchAreaCodes.Login, EmployeeResources.LoginLabel),
                new SearchArea(SearchAreaCodes.Acronym, EmployeeResources.AcronymLabel),
                new SearchArea(SearchAreaCodes.OrganizationUnit, EmployeeResources.EmployeeOrgUnit),
                new SearchArea(SearchAreaCodes.JobTitle, AddEmployeeRequestResources.JobTitleLabel),
                new SearchArea(SearchAreaCodes.LineMaganer, AddEmployeeRequestResources.LineManagerLabel),
                new SearchArea(SearchAreaCodes.Skills, AddEmployeeRequestResources.Skills, false)
            };

            SetEditButton();
        }

        protected FilterGroup GetSimpleFilterGroup()
        {
            return new FilterGroup
            {
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.MyEmployees, DisplayName = EmployeeResources.MyEmployees},
                    new Filter { Code = FilterCodes.MyDirectEmployees, DisplayName = EmployeeResources.MyDirectEmployees },
                    new Filter {Code = FilterCodes.AllEmployees, DisplayName = EmployeeResources.AllEmployees}
                }
            };
        }

        [AtomicAuthorize(SecurityRoleType.CanViewEmployee)]
        public override ActionResult List(GridParameters parameters)
        {
            Layout.Css.Add("~/Areas/Allocation/Content/EmployeeTile.css");
            return base.List(parameters);
        }

        [BreadcrumbBar("Start/MyProfile")]
        [AtomicAuthorize(SecurityRoleType.CanViewOwnEmployeeProfile)]
        public ActionResult MyEmployeeProfile()
        {
            if (_principal.EmployeeId <= 0 || !_principal.Id.HasValue)
            {
                return new EmptyResult();
            }

            var myProfileDto = _myProfileService.GetMyProfile(_principal.Id.Value, _principal.EmployeeId);
            var myProfileViewModel = _myProfileDtoToMyProfileViewModelClassMapping.CreateFromSource(myProfileDto);
            myProfileViewModel.PictureUrl = Url.Action("Download", "UserPicture", new { Area = "Accounts", userId = myProfileDto.UserId });
            myProfileViewModel.ChangePictureUrl = _systemParameterService.GetParameter<string>(ParameterKeys.EmployeeChangePictureUrl);
            myProfileViewModel.OwnedOrgUnitId = _orgUnitService.GetOwnedOrgUnit(_principal.EmployeeId);

            Layout.PageHeader = string.Empty;
            Layout.Css.BodyClass = "record-form";
            Layout.Css.Add("~/Areas/Allocation/Content/MyProfile.css");

            return View(MyProfileCustomViewName, myProfileViewModel);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogEmployeeDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogEmployeeDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            if (_principal.IsInRole(SecurityRoleType.CanEditEmployee))
            {
                return EditAll(id);
            }

            return EditOwn(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditEmployee)]
        public ActionResult EditAll(long id)
        {
            return base.Edit(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditOwnEmployees)]
        public ActionResult EditOwn(long id)
        {
            if (!_employeeService.IsEmployeeOfManager(id, _principal.EmployeeId))
            {
                throw new BusinessException(EmployeeResources.IsNotEmployeeOfCurrentEmployeeMessage);
            }

            return base.Edit(id);
        }

        [HttpPost]
        public ActionResult GenerateResumes(long[] employeeIds)
        {
            return GenerateResumes(new GenerateResumesJustificationViewModel(employeeIds));
        }

        [HttpPost]
        public ActionResult PerformGenerateResumes(GenerateResumesJustificationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return GenerateResumes(model);
            }

            var customer = _customerService.GetRecordById(model.CustomerId);

            var tempDataKey = GetTempDataKey(model.EmployeeIds);
            TempData[tempDataKey] = DownloadJustification.CreateDocumentSharedWithCustomerDownloadJustification(customer, model.Comment);

            return DialogHelper.Redirect(this, Url.Action(nameof(PerformGenerateResumes), new { employeeIds = string.Join(",", model.EmployeeIds) }));
        }

        //Responsibility for security check is delegated to the reporting engine.
        //Method requries role CanGenerateResumeReport or CanGenerateOwnResumeReport to generate any data.
        [HttpGet]
        public ActionResult PerformGenerateResumes(long[] employeeIds)
        {
            if (employeeIds.Length > MaximumReportFiles)
            {
                var reportsCountExceededError = string.Format(EmployeeResources.MaximumReportsCountExceededError, MaximumReportFiles);
                AddAlert(AlertType.Error, reportsCountExceededError);

                return Redirect(Request.UrlReferrer.ToString());
            }

            var tempDataKey = GetTempDataKey(employeeIds);
            var downloadJustification = TempData[tempDataKey] as DownloadJustification;

            if (downloadJustification == null)
            {
                AddAlert(AlertType.Error, ResumeResources.GenerateResumesNoJustificationError);

                return RedirectToAction(nameof(List));
            }

            var resumes = employeeIds
                .Select(employeeId => _reportingService
                    .PerformReport(EmployeeResumeHelper.ResumeReportDataSource, new ResumeReportParametersViewModel { EmployeeId = employeeId }).Render())
                    .Cast<ArchivableFileContentResult>()
                    .ToList();

            var reports = resumes.Count == 1
                ? resumes.Single()
                : new ArchivableFileContentResult(ZipArchiveHelper.GetZipArchiveBytes(resumes), MimeHelper.ZipFile)
                {
                    FileDownloadName = ZipArchiveHelper.GetZipFileName(EmployeeResumeHelper.ResumesZipFileName)
                };

            foreach (var employeeId in employeeIds)
            {
                _dataActivityLogService.LogEmployeeDataActivity(employeeId, ActivityType.ResumeGenerated, GetType().Name, downloadJustification.Text);
            }

            return new FileContentResult(reports.Content, reports.ContentType)
            {
                FileDownloadName = reports.FileName
            };
        }

        public override FilePathResult ExportToExcel(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;
            return base.ExportToExcel(parameters);
        }

        public override FilePathResult ExportToCsv(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;
            return base.ExportToCsv(parameters);
        }

        private void SetEditButton()
        {
            if (_principal.IsInRole(SecurityRoleType.CanEditEmployee))
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.Grid;
            }
            else if (_principal.IsInRole(SecurityRoleType.CanEditOwnEmployees))
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.Grid;

                CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(EmployeeViewModel.IsOwnEmployee))}"
                };
            }
            else
            {
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            }
        }

        private static IList<IGridViewConfiguration> GetGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var tilesConfiguration = new TilesViewConfiguration<EmployeeViewModel>(EmployeeResources.EmployeeTilesViewName, "tiles")
            {
                IsDefault = false,
                IsHidden = false,
                CustomViewName = "~/Areas/Allocation/Views/Employee/_EmployeeTile.cshtml",
                InitializeJavaScriptAction = "Tiles.maxSizeForAll"
            };

            var listConfiguration = new GridViewConfiguration<EmployeeViewModel>(EmployeeResources.EmployeeListViewName, "list")
            {
                IsDefault = true
            };
            var resumeFillListConfiguration = new GridViewConfiguration<EmployeeViewModel>(EmployeeResources.ResumePercentageFilling, "resumePercentageFillList");

            var extendedListConfiguration = new GridViewConfiguration<EmployeeViewModel>(EmployeeResources.EmployeeExtendedListViewName, "extendedlist");

            tilesConfiguration.IncludeColumn(u => u.FirstName);
            tilesConfiguration.IncludeColumn(u => u.LastName);
            tilesConfiguration.ExcludeColumn(x => x.FillPercentage);
            tilesConfiguration.ExcludeColumn(x => x.ResumeModificationDate);

            listConfiguration.ExcludeAllColumns();
            listConfiguration.IncludeColumn(u => u.FirstName);
            listConfiguration.IncludeColumn(u => u.LastName);
            listConfiguration.IncludeColumn(u => u.CompanyName);
            listConfiguration.IncludeColumn(u => u.LocationName);
            listConfiguration.IncludeColumn(u => u.OrgUnitName);
            listConfiguration.IncludeColumn(u => u.LineManagerFullName);
            listConfiguration.IncludeColumn(u => u.JobMatrixName);
            listConfiguration.IncludeColumn(u => u.JobProfileNames);
            listConfiguration.IncludeColumn(u => u.SkillNames);
            listConfiguration.IncludeColumn(u => u.JobTitleName);
            listConfiguration.IncludeColumn(u => u.Login);

            extendedListConfiguration.IncludeAllColumns();

            resumeFillListConfiguration.IncludeAllColumns();
            resumeFillListConfiguration.ExcludeColumn(x => x.Email);
            resumeFillListConfiguration.ExcludeColumn(x => x.Acronym);
            resumeFillListConfiguration.ExcludeColumn(x => x.PlaceOfWork);
            resumeFillListConfiguration.ExcludeColumn(x => x.SkillNames);
            resumeFillListConfiguration.ExcludeColumn(x => x.JobTitleName);
            resumeFillListConfiguration.ExcludeColumn(x => x.Login);
            resumeFillListConfiguration.ExcludeColumn(x => x.CurrentEmployeeStatus);

            if (_principal != null)
            {
                if (!_principal.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
                {
                    listConfiguration.ExcludeColumn(x => x.CurrentEmployeeStatus);
                    resumeFillListConfiguration.ExcludeColumn(x => x.CurrentEmployeeStatus);
                    tilesConfiguration.ExcludeColumn(x => x.CurrentEmployeeStatus);
                    extendedListConfiguration.ExcludeColumn(x => x.CurrentEmployeeStatus);
                }
            }

            list.Add(tilesConfiguration);
            list.Add(listConfiguration);
            list.Add(extendedListConfiguration);
            list.Add(resumeFillListConfiguration);

            return list;
        }

        private ActionResult GenerateResumes(GenerateResumesJustificationViewModel model)
        {
            var dialogViewModel = new ModelBasedDialogViewModel<GenerateResumesJustificationViewModel>(model)
            {
                Title = ResumeResources.GenerateResumesDialogTitle,
                LabelWidth = LabelWidth.Large
            };

            return PartialView("GenerateResumesJustification", dialogViewModel);
        }

        private string GetTempDataKey(long[] ids) => $"generate-resumes-justification:{string.Join(",", ids)}";
    }
}