﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Allocation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewActiveDirectoryGroup)]
    public class ActiveDirectoryGroupController : CardIndexController<ActiveDirectoryGroupViewModel, ActiveDirectoryGroupDto>
    {
        private readonly IActiveDirectoryGroupService _activeDirectoryGroupService;
        private readonly IActiveDirectoryGroupCardIndexDataService _cardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMappingFactory _classMappingFactory;

        public ActiveDirectoryGroupController(
            IActiveDirectoryGroupService activeDirectoryGroupService,
            IActiveDirectoryGroupCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseDependencies)
            : base(cardIndexDataService, baseDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _activeDirectoryGroupService = activeDirectoryGroupService;
            _principalProvider = baseDependencies.PrincipalProvider;
            _classMappingFactory = baseDependencies.ClassMappingFactory;

            CardIndex.Settings.Title = ActiveDirectoryGroupResources.ActiveDirectoryGroupTitle;
            CardIndex.Settings.CustomGridDisplayModeViewName = "~/Areas/Allocation/Views/ActiveDirectoryGroup/_ActiveDirectoryGroupTile.cshtml";
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.GridViewConfigurations = GetGridSettings();

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportActiveDirectoryGroups);
            Layout.Css.Add("~/Areas/Allocation/Content/ActiveDirectoryGroup.css");

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Name, ActiveDirectoryGroupResources.ActiveDirectoryGroupNameLabel),
                new SearchArea(SearchAreaCodes.Email, ActiveDirectoryGroupResources.ActiveDirectoryGroupEmailLabel),
                new SearchArea(SearchAreaCodes.Description, ActiveDirectoryGroupResources.ActiveDirectoryGroupDescriptionLabel),
                new SearchArea(SearchAreaCodes.Aliases, ActiveDirectoryGroupResources.ActiveDirectoryGroupAliasesLabel),
                new SearchArea(SearchAreaCodes.ActiveDirectoryGroupOwner, ActiveDirectoryGroupResources.ActiveDirectoryGroupOwnerIdLabel),
                new SearchArea(SearchAreaCodes.Managers, ActiveDirectoryGroupResources.ActiveDirectoryGroupManagersIdsLabel),
                new SearchArea(SearchAreaCodes.Users, ActiveDirectoryGroupResources.ActiveDirectoryGroupUserIdsLabel),
            };

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    ButtonText = ActiveDirectoryGroupResources.FiltersButtonText,
                    DisplayName = ActiveDirectoryGroupResources.GroupsFilterDisplayName,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.AllGroupsFilterText,
                            Code = FilterCodes.AllActiveDirectoryGroups
                        },
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.MyGroupsFilterText,
                            Code = FilterCodes.UsersActiveDirectoryGroups
                        }
                    }
                },
                new FilterGroup
                {
                    Type =  FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    ButtonText = ActiveDirectoryGroupResources.FiltersButtonText,
                    DisplayName = ActiveDirectoryGroupResources.GroupType,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<ActiveDirectoryGroupType>()
                },
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    ButtonText = ActiveDirectoryGroupResources.FiltersButtonText,
                    DisplayName = ActiveDirectoryGroupResources.OwnersFilter,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.OwnersFilterWith,
                            Code = FilterCodes.WithOwnerGroup
                        },
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.OwnersFilterWithout,
                            Code = FilterCodes.WithoutOwnerGroup
                        }
                    }
                },
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    ButtonText = ActiveDirectoryGroupResources.FiltersButtonText,
                    DisplayName = ActiveDirectoryGroupResources.GroupLabel,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.AnnounceLabel,
                            Code = FilterCodes.AnnounceLabel
                        },
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.DanteLabel,
                            Code = FilterCodes.DanteLabel
                        },
                        new Filter
                        {
                            DisplayName = ActiveDirectoryGroupResources.OthersLabel,
                            Code = FilterCodes.OtherLabel
                        }
                    }
                }
            };

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            if (_principalProvider.Current.Id.HasValue)
            {
                var loggedUserId = _principalProvider.Current.Id.Value.ToString();
                var hasRole = _cardIndexDataService.CurrentUserCanEdit.ToString().ToLower();

                CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = @"ActiveDirectoryGroup.adGroupIsReadonlyForUser(" + loggedUserId + ", row, " + hasRole + ");"
                };
            }

            CardIndex.Settings.EditFormCustomization = EditFormCustomization;

            Layout.Scripts.Add("~/Areas/Allocation/Scripts/ActiveDirectoryGroup.js");
        }

        private static IList<IGridViewConfiguration> GetGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var tilesConfiguration = new TilesViewConfiguration<ActiveDirectoryGroupViewModel>(EmployeeResources.EmployeeTilesViewName, "tiles")
            {
                IsDefault = true,
                IsHidden = false,
                CustomViewName = "~/Areas/Allocation/Views/ActiveDirectoryGroup/_ActiveDirectoryGroupTile.cshtml"
            };

            tilesConfiguration.IncludeAllColumns();

            list.Add(tilesConfiguration);

            var listConfiguration = new GridViewConfiguration<ActiveDirectoryGroupViewModel>(EmployeeResources.EmployeeListViewName, "list")
            {
                IsDefault = false
            };

            listConfiguration.IncludeAllColumns();

            list.Add(listConfiguration);

            return list;
        }

        public override ActionResult Add()
        {
            throw new HttpException((int)HttpStatusCode.NotFound, ActiveDirectoryGroupResources.PageNotFound);
        }

        public override ActionResult Add(ActiveDirectoryGroupViewModel viewModelRecord)
        {
            throw new HttpException((int)HttpStatusCode.NotFound, ActiveDirectoryGroupResources.PageNotFound);
        }

        public override ActionResult Delete(IList<long> ids)
        {
            throw new HttpException((int)HttpStatusCode.NotFound, ActiveDirectoryGroupResources.PageNotFound);
        }

        public override ActionResult View(long id)
        {
            var dto = _activeDirectoryGroupService.GetActiveDirectoryGroupVisualizationById(id);
            var mapping = _classMappingFactory.CreateMapping<ActiveDirectoryGroupVisualizationDto, ActiveDirectoryGroupVisualizationViewModel[]>();
            var viewModel = mapping.CreateFromSource(dto);

            return View("~/Areas/Allocation/Views/ActiveDirectoryGroup/ActiveDirectoryGroupVisualization.cshtml", viewModel);
        }

        private void EditFormCustomization(FormRenderingModel model)
        {
            var ownerControlRenderingModel = (ValuePickerRenderingModel)model.GetControlByName<ActiveDirectoryGroupViewModel>(m => m.OwnerId);
            var managersControlRenderingModel = (ValuePickerRenderingModel)model.GetControlByName<ActiveDirectoryGroupViewModel>(m => m.ManagersIds);
            var membersControlRenderingModel = (ValuePickerRenderingModel)model.GetControlByName<ActiveDirectoryGroupViewModel>(m => m.UserIds);
            var infoControl = model.GetControlByName<ActiveDirectoryGroupViewModel>(m => m.Info);
            var descriptionControl = model.GetControlByName<ActiveDirectoryGroupViewModel>(m => m.Description);

            var ownerId = ownerControlRenderingModel.Ids.FirstOrDefault();
            var managersIds = managersControlRenderingModel.Ids;
            var loggedUserId = _principalProvider.Current.Id ?? 0;

            var isGroupOwner = ownerId != default(long) && ownerId == loggedUserId;
            var isManager = managersIds.Contains(loggedUserId);
            var canEdit = _cardIndexDataService.CurrentUserCanEdit;

            ownerControlRenderingModel.IsReadOnly = !isGroupOwner && !canEdit;
            managersControlRenderingModel.IsReadOnly = !isGroupOwner && !canEdit;
            membersControlRenderingModel.IsReadOnly = !isGroupOwner && !isManager && !canEdit;
            infoControl.IsReadOnly = !isGroupOwner && !canEdit;
            descriptionControl.IsReadOnly = !isGroupOwner && !canEdit;
        }
    }
}