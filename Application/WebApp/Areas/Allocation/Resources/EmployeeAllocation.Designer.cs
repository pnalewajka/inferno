﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Allocation.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class EmployeeAllocation {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal EmployeeAllocation() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Allocation.Resources.EmployeeAllocation", typeof(EmployeeAllocation).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add allocation.
        /// </summary>
        public static string AddAllocationText {
            get {
                return ResourceManager.GetString("AddAllocationText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Advance edit.
        /// </summary>
        public static string AdvanceEditButton {
            get {
                return ResourceManager.GetString("AdvanceEditButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allocation percentage:.
        /// </summary>
        public static string AllocationPercentageLabel {
            get {
                return ResourceManager.GetString("AllocationPercentageLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allocation type:.
        /// </summary>
        public static string AllocationTypeLabel {
            get {
                return ResourceManager.GetString("AllocationTypeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Client name:.
        /// </summary>
        public static string ClientNameLabel {
            get {
                return ResourceManager.GetString("ClientNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} from {1} to {2}. Request status: {3}..
        /// </summary>
        public static string EmployeeAbsenceDescription {
            get {
                return ResourceManager.GetString("EmployeeAbsenceDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employee:.
        /// </summary>
        public static string EmployeeLabel {
            get {
                return ResourceManager.GetString("EmployeeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} on {1}. Request status: {2}..
        /// </summary>
        public static string EmployeeOneDayAbsenceDescription {
            get {
                return ResourceManager.GetString("EmployeeOneDayAbsenceDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hours:.
        /// </summary>
        public static string HoursLabel {
            get {
                return ResourceManager.GetString("HoursLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jira Issue:.
        /// </summary>
        public static string JiraIssueKeyLabel {
            get {
                return ResourceManager.GetString("JiraIssueKeyLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jira:.
        /// </summary>
        public static string JiraKeyLabel {
            get {
                return ResourceManager.GetString("JiraKeyLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -.
        /// </summary>
        public static string NoProjects {
            get {
                return ResourceManager.GetString("NoProjects", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Over-Allocation.
        /// </summary>
        public static string OverallocatedMessage {
            get {
                return ResourceManager.GetString("OverallocatedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Planned.
        /// </summary>
        public static string PlannedProject {
            get {
                return ResourceManager.GetString("PlannedProject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} projects.
        /// </summary>
        public static string ProjectCountFormat {
            get {
                return ResourceManager.GetString("ProjectCountFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project name:.
        /// </summary>
        public static string ProjectNameLabel {
            get {
                return ResourceManager.GetString("ProjectNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}%.
        /// </summary>
        public static string ProjectPercentageAllocationFormat {
            get {
                return ResourceManager.GetString("ProjectPercentageAllocationFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show weekends.
        /// </summary>
        public static string ShowWeekends {
            get {
                return ResourceManager.GetString("ShowWeekends", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Simple edit.
        /// </summary>
        public static string SimpleEditButton {
            get {
                return ResourceManager.GetString("SimpleEditButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Week {0}.
        /// </summary>
        public static string WeekNumberFormat {
            get {
                return ResourceManager.GetString("WeekNumberFormat", resourceCulture);
            }
        }
    }
}
