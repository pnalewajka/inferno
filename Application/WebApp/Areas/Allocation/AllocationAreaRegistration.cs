﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Allocation
{
    public class AllocationAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Allocation";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Allocation_default",
                "Allocation/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
