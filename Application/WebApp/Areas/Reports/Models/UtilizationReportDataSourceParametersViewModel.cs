﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    [Identifier("ViewModel.UtilizationReportDataSourceParametersViewModel")]
    public class UtilizationReportDataSourceParametersViewModel : IValidatableObject
    {
        [DisplayNameLocalized(nameof(ReportResources.OrgUnitLabel), typeof(ReportResources))]
        [Required]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long? OrgUnitId { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.UtilizationMonthLabel), typeof(ReportResources))]
        public DateTime UtilizationMonth { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.LocationLabel), typeof(ReportResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? LocationId { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.JobProfileLabel), typeof(ReportResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long? JobProfileId { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.LevelLabel), typeof(ReportResources))]
        public long? LevelId { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.UtilizationCategoryLabel), typeof(ReportResources))]
        [ValuePicker(Type = typeof(UtilizationCategoryPickerController))]
        public List<long> UtilizationCategoryIds { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.IncludeNoCategoryEntriesLabel), typeof(ReportResources))]
        public bool MustIncludeEntriesWithoutCategory { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.HoursLabel), typeof(ReportResources))]
        public HourStatus HourStatus { get; set; } 

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            const long levelMinValue = 1;
            const long levelMaxValue = 5;

            if (LevelId.HasValue)
            {
                if (LevelId < levelMinValue)
                {
                    yield return new ValidationResult(string.Format(ReportResources.MinValueError, levelMinValue), new[] { nameof(LevelId) });
                }

                if (LevelId > levelMaxValue)
                {
                    yield return new ValidationResult(string.Format(ReportResources.MaxValueError, levelMaxValue), new[] { nameof(LevelId) });
                }
            }
        }
    }
}
