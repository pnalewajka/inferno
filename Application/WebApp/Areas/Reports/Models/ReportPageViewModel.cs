﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class ReportPageViewModel
    {
        public bool CanGenerateUtilizationStatusReport { get; private set; }

        public bool CanGenerateActiveInactiveEmployeesReport { get; private set; }

        public bool CanGenerateNewHiredLeavingEmployeesReport { get; private set; }

        public bool CanGenerateSeniorityLevelsReport { get; private set; }

        public bool CanGenerateAllocationPerProjectReport { get; private set; }

        public ReportPageViewModel(IAtomicPrincipal principal)
        {
            CanGenerateUtilizationStatusReport = principal.IsInRole(SecurityRoleType.CanGenerateUtilizationStatusReport);

            CanGenerateActiveInactiveEmployeesReport = principal.IsInRole(SecurityRoleType.CanGenerateActiveInactiveEmployeesReport);

            CanGenerateNewHiredLeavingEmployeesReport = principal.IsInRole(SecurityRoleType.CanGenerateNewHiredLeavingEmployeesReport);

            CanGenerateSeniorityLevelsReport = principal.IsInRole(SecurityRoleType.CanGenerateSeniorityLevelsReport);

            CanGenerateAllocationPerProjectReport = principal.IsInRole(SecurityRoleType.CanGenerateAllocationPerProjectReport);
        }
    }
}