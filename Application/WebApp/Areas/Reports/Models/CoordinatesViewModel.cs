﻿using Newtonsoft.Json;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class CoordinatesViewModel
    {
        [JsonProperty(PropertyName = "r")]
        public int Radius { set; get; }

        [JsonProperty(PropertyName = "t")]
        public int Theta { get; set; }
    }
}