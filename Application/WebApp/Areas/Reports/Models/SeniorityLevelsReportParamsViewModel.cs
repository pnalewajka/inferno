﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    [Identifier("Models.SeniorityLevelsReportParamsViewModel")]
    public class SeniorityLevelsReportParamsViewModel
    {
        [DisplayNameLocalized(nameof(ReportResources.ReportJobMatrixLabel), typeof(ReportResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(JobMatrixListItemProvider),
            EmptyItemDisplayName = "AllJobMatrixes", ResourceType = typeof(ReportResources))]
        [Render(Size = Size.Large)]
        public long? JobMatrix { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.ReportEmployeesFilterLabel), typeof(ReportResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<ReportScopeEnum>))]
        [Render(Size = Size.Large)]
        public ReportScopeEnum Filter { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.AllocationReportTypeFilterType), typeof(ReportResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<AllocationReportType>))]
        [Render(Size = Size.Large)]
        [Required]
        public AllocationReportType ReportType { get; set; }
    }
}