﻿using System.Linq;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class UtilizationReportDataSourceParametersDtoToUtilizationReportDataSourceParametersViewModelMapping : ClassMapping<UtilizationReportDataSourceParametersDto, UtilizationReportDataSourceParametersViewModel>
    {
        public UtilizationReportDataSourceParametersDtoToUtilizationReportDataSourceParametersViewModelMapping()
        {
            Mapping = d => new UtilizationReportDataSourceParametersViewModel
            {
                OrgUnitId = d.OrgUnitId.Value,
                UtilizationMonth = d.UtilizationMonth,
                LocationId = d.LocationId,
                JobProfileId = d.JobProfileId,
                LevelId = d.LevelId,
                UtilizationCategoryIds = d.UtilizationCategoryIds.ToList(),
                MustIncludeEntriesWithoutCategory = d.MustIncludeEntriesWithoutCategory,
                HourStatus = d.HourStatus
            };
        }
    }
}
