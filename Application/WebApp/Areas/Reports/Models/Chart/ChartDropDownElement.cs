﻿namespace Smt.Atomic.WebApp.Areas.Reports.Models.Chart
{
    public class ChartDropDownElement
    {
        public long Id { get; set; }      

        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}