﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.Reports.Models.Chart
{
    public class ChartRowViewModel
    {
        public string Label { get; set; }

        public IEnumerable<ChartCellViewModel> Cells { get; set; }

        public bool IsChartRow { get; set; }

        public long? UtilizationCategoryId { get; set; }

        public bool? Billability { get; set; }

        public bool CanDrillData { get; set; }

        public bool CanExportData { get; set; }
    }
}