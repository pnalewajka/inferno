﻿namespace Smt.Atomic.WebApp.Areas.Reports.Models.Chart
{
    public class ChartOrgUnitDropDownElement : ChartDropDownElement
    {
        public long? ManagerId { get; set; }

        public string ManagerFirstName { get; set; }

        public string ManagerLastName { get; set; }

        public override string ToString()
        {
            if (!ManagerId.HasValue || Name.Contains($"{ManagerFirstName} {ManagerLastName}"))
            {
                return Name;
            }

            return $"{Name} ({ManagerFirstName} {ManagerLastName})";
        }
    }
}