﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.Reports.Models.Chart
{
    public class UtilizationChartViewModel
    {
        public IReadOnlyCollection<ChartDropDownElement> OrgUnitItems { get; set; }

        public IReadOnlyCollection<ChartDropDownElement> LocationItems { get; set; }

        public IReadOnlyCollection<ChartDropDownElement> JobProfileItems { get; set; }

        public IReadOnlyCollection<ChartDropDownElement> LevelItems { get; set; }

        public IReadOnlyCollection<ChartDropDownElement> HourStatusItems { get; set; }

        public IReadOnlyCollection<ChartViewModel> Charts { get; set; }

        public bool CanExportReport { get; set; }

    }

}