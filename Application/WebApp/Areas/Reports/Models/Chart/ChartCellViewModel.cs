﻿using System;

namespace Smt.Atomic.WebApp.Areas.Reports.Models.Chart
{
    public class ChartCellViewModel
    {
        public decimal Value { get; set; }

        public string TextValue { get; set; }

        public DateTime Date { get; set; }
    }
}