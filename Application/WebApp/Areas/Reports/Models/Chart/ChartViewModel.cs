﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Reports.Models.Chart
{
    public class ChartViewModel
    {
        public string Id { get; set; }

        public DateTime CurrentDate { get; set; }

        public IEnumerable<ChartRowViewModel> Rows { get; set; }
    }
}