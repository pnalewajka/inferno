﻿using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class TechnologyRadarItemDtoToTechnologyRadarItemViewModel : ClassMapping<TechnologyRadarItemDto, TechnologyRadarItemViewModel>
    {
        public TechnologyRadarItemDtoToTechnologyRadarItemViewModel()
        {
            Mapping = item => new TechnologyRadarItemViewModel
            {
                TechnologyRadarArea = item.TechnologyRadarArea,
                Description = item.Description,
                Status = item.Status,
                Subject = item.Subject,
                Movement = item.Movement
            };
        }
    }
}