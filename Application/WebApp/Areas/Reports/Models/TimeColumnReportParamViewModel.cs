﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    [Identifier("Models.TimeColumnReportParamViewModel")]
    public class TimeColumnReportParamViewModel : IValidatableObject
    {
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(JobMatrixListItemProvider))]
        [Render(Size = Size.Large)]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime FromDate { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(JobMatrixListItemProvider))]
        [Render(Size = Size.Large)]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime ToDate { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.ReportEmployeesFilterLabel), typeof(ReportResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<ReportScopeEnum>))]
        [Render(Size = Size.Large)]
        public ReportScopeEnum Filter { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.AllocationReportTypeFilterType), typeof(ReportResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<AllocationReportType>))]
        [Render(Size = Size.Large)]
        [Required]
        public AllocationReportType ReportType { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResult = new List<ValidationResult>();

            if (FromDate > ToDate)
            {
                validationResult.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate,
                   new List<string> { nameof(FromDate) }));
            }

            return validationResult;
        }
    }
}