﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class TechnologyRadarLabelsViewModel
    {
        public string Changed => ReportResources.ItemChanged;

        public string NotChanged => ReportResources.ItemNotChanged;

        public string NoDescription => ReportResources.MissingDescription;

        public string CloseButton => ReportResources.Close;

        public string Hold => ReportResources.Hold;

        public string Adopt => ReportResources.Adopt;

        public string Trial => ReportResources.Trial;

        public string Assess => ReportResources.Assess;
    }
}