﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class TechnologyRadarAreaViewModel
    {
        [JsonIgnore]
        public TechnologyRadarArea TechnologyRadarArea { get; set; }

        [JsonProperty(PropertyName = "quadrant")]
        public string AreaDescription => GetAreaFriendlyString();

        [JsonProperty(PropertyName = "left")]
        public int PositionLeft { get; set; }

        [JsonProperty(PropertyName = "top")]
        public int PositionTop { get; set; }

        [JsonProperty(PropertyName = "color")]
        public string Color { get; set; }

        [JsonProperty(PropertyName = "items")]
        public IEnumerable<TechnologyRadarItemViewModel> AreaItems { get; set; }

        private string GetAreaFriendlyString()
        {
            switch (TechnologyRadarArea)
            {
                case TechnologyRadarArea.Platforms:
                    return ReportResources.Platforms;

                case TechnologyRadarArea.PatternsAndPractices:
                    return ReportResources.PatternsAndPracticies;

                case TechnologyRadarArea.LanguagesAndFrameworks:
                    return ReportResources.LanguagesAndFrameworks;

                case TechnologyRadarArea.Tools:
                    return ReportResources.Tools;

                default:
                    return null;
            }
        }
    }
}