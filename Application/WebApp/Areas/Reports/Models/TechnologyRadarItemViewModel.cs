﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Smt.Atomic.Business.Reports.Enums;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class TechnologyRadarItemViewModel
    {
        [JsonProperty(PropertyName = "name")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonIgnore]
        public TechnologyRadarStatus Status { get; set; }

        [JsonIgnore]
        public TechnologyRadarArea TechnologyRadarArea { get; set; }

        [JsonProperty(PropertyName = "pc")]
        public CoordinatesViewModel CoordinatesViewModel { get; set; }

        [JsonProperty(PropertyName = "movement")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TechnologyRadarMovement Movement { get; set; }
    }
}