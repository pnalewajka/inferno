﻿using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class UtilizationReportDataSourceParametersViewModelToUtilizationReportDataSourceParametersDtoMapping : ClassMapping<UtilizationReportDataSourceParametersViewModel, UtilizationReportDataSourceParametersDto>
    {
        public UtilizationReportDataSourceParametersViewModelToUtilizationReportDataSourceParametersDtoMapping()
        {
            Mapping = v => new UtilizationReportDataSourceParametersDto
            {
                OrgUnitId = v.OrgUnitId,
                UtilizationMonth = v.UtilizationMonth,
                LocationId = v.LocationId,
                JobProfileId = v.JobProfileId,
                LevelId = v.LevelId,
                UtilizationCategoryIds = v.UtilizationCategoryIds,
                MustIncludeEntriesWithoutCategory = v.MustIncludeEntriesWithoutCategory,
                HourStatus = v.HourStatus,
            };
        }
    }
}
