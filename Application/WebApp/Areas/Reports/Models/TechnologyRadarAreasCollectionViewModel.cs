﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class TechnologyRadarAreasCollectionViewModel
    {
        public IEnumerable<TechnologyRadarAreaViewModel> Areas { get; set; }
    }
}
