﻿namespace Smt.Atomic.WebApp.Areas.Reports.Models
{
    public class TechnologyRadarReportViewModel
    {
        public TechnologyRadarAreasCollectionViewModel TechnologyRadarAreas { get; set; }

        public TechnologyRadarLabelsViewModel TechnologyRadarLabels { get; set; }
    }
}