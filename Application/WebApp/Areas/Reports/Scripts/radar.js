var radarHeight = 900;
var radarWidth = 1550;
var item = null;
var currentTitle = null;
var allLinks = [];

function init(radarHeight, radarWidth, radarData, labels) {
    var radarArcs = [
      { 'r': 100, 'name': labels.Adopt, 'c': "#CBCCCB" },
      { 'r': 200, 'name': labels.Trial, 'c': "#D7D8D6" },
      { 'r': 300, 'name': labels.Assess, 'c': "#E4E5E4" },
      { 'r': 400, 'name': labels.Hold, 'c': "#BFC0BF" }
    ].reverse();

    var radar = new pv.Panel()
      .width(radarWidth)
      .height(radarHeight)
      .canvas("radar");

    // legend
    radar.add(pv.Dot)
        .top(50)
        .left(50)
        .shape("circle")
        .fillStyle("black")
        .strokeStyle("black")
        .size(16)
        .anchor("right")
        .add(pv.Label)
        .text(labels.NotChanged)
        .textStyle("black");

    radar.add(pv.Dot)
      .top(70)
      .left(50)
      .shape("triangle")
      .fillStyle("black")
      .strokeStyle("black")
      .size(16)
      .angle(45)
      .anchor("right")
      .add(pv.Label)
        .text(labels.Changed)
        .textStyle("black");

    // arcs
    var arcs = radar.add(pv.Dot)
        .data(radarArcs)
        .left(radarWidth / 2)
        .bottom(radarHeight / 2)
        .radius(function(d) { return d.r; })
        .strokeStyle("#ccc");

    // labels on arcs
    arcs.anchor("top")
      .add(pv.Label)
        .textBaseline("top")
        .textMargin(40)
        .text(function (d) { return d.name; })
        .textStyle("grey")
        .font("bold 40px sans-serif");

    //quadrant lines -- vertical
        radar.add(pv.Line)
          .data([(radarHeight / 2 - radarArcs[0].r), radarHeight / 2 + radarArcs[0].r])
          .lineWidth(1)
          .left(radarWidth / 2)
          .bottom(function (d) { return d; })
          .strokeStyle("#ccc");

    //quadrant lines -- horizontal 
        radar.add(pv.Line)
          .data([(radarWidth / 2 - radarArcs[0].r), radarWidth / 2 + radarArcs[0].r])
          .lineWidth(1)
          .bottom(radarHeight / 2)
          .left(function (d) { return d; })
          .strokeStyle("#ccc");

    //Quadrant Ledgends
    var radarQuadrantCtr = 1;
    var quadrantFontSize = 18;
    var headingFontSize = 14;
    var lastQuadrant = "";
    var spacer = 16;
    var fontSize = 10;
    var totalIndex = 1;


    for (var i = 0; i < radarData.length; i++) {

        // quadrant title
        if (lastQuadrant !== radarData[i].quadrant) {
            lastQuadrant = radarData[i].quadrant;
            radar.add(pv.Label)
              .left(radarData[i].left)
              .top(radarData[i].top)
              .text(radarData[i].quadrant)
              .strokeStyle(radarData[i].color)
              .fillStyle(radarData[i].color)
              .font(quadrantFontSize + "px sans-serif");
        }

        // re-order the items by radius, in order to logically group by ring
        var itemsByStage = _.groupBy(radarData[i].items, function (item) { return Math.floor(item.pc.r / 100) });
        var offsetIndex = 0;
        var midIndex = -1;

        var numberOfEmptyGroups = 0;
        for (var stageIndex = 0; stageIndex < radarArcs.length; stageIndex++) {
            if (!itemsByStage[stageIndex]) {
                numberOfEmptyGroups = numberOfEmptyGroups + 1;
                continue;
            }

            if (numberOfEmptyGroups + 1 > stageIndex)
                numberOfEmptyGroups = numberOfEmptyGroups - 1;

            if (stageIndex > 0) {
                if (!itemsByStage[stageIndex - (1 + numberOfEmptyGroups)])
                    offsetIndex = offsetIndex + 1;
                else
                    offsetIndex = offsetIndex + itemsByStage[stageIndex - (1 + numberOfEmptyGroups)].length + 1;
            }
            if ((stageIndex > 1) && (midIndex < 0)) {
                midIndex = offsetIndex;
            }
            numberOfEmptyGroups = 0;

            var left = radarData[i].left;
            var top = radarData[i].top + quadrantFontSize + spacer + (stageIndex * headingFontSize) + (offsetIndex * fontSize);
            if (stageIndex > 1) {
                left = left + 200;
                top = top - (2 * headingFontSize) - (midIndex * fontSize);
            }

            // stage label
            radar.add(pv.Label)
              .left(left + headingFontSize)
              .top(top - headingFontSize / 2)
              .text(radarArcs[radarArcs.length - stageIndex - 1].name)
              .strokeStyle("#ccc")
              .fillStyle("#ccc")
              .font(headingFontSize + "px Courier New");

            // legend label
            radar.add(pv.Label)
                .left(left)
                .top(top)
                .add(pv.Dot)
                .def("i", top)
                .data(itemsByStage[stageIndex])
                .top(function() { return (this.i() + (this.index * fontSize)); })
                .shape(function(d) { return (d.movement === "Changed" ? "triangle" : "circle"); })
                .title(function(d) { return d.name; })
                .cursor("pointer")
                .strokeStyle(setItemColor)
                .fillStyle(setItemColor)
                .event("mouseover", colorLinks)
                .event("mouseout", restoreLinkColors)
                .event("click", setDescriptionInfo)
                .size(fontSize)
                .angle(45)
                .anchor("right")
                .add(pv.Label)
                    .title(function(d) { return d.name; })
                    .event("mouseover", colorLinks)
                    .event("mouseout", restoreLinkColors)
                    .event("click", setDescriptionInfo)
                    .cursor("pointer")
                    .text(function(d) { return radarQuadrantCtr++ + ". " + d.name; })
                    .textStyle(setTextColor);

            // the blip itself
            radar.add(pv.Dot)
              .def("active", false)
              .data(itemsByStage[stageIndex])
              .size(function (d) { return (d.blipSize !== undefined ? d.blipSize : 70); })
              .left(function (d) { return polar_to_raster(d.pc.r, d.pc.t, radarWidth, radarHeight)[0]; })
              .bottom(function (d) { return polar_to_raster(d.pc.r, d.pc.t, radarWidth, radarHeight)[1]; })
              .title(function (d) { return d.name; })
              .cursor("pointer")
              .strokeStyle(setItemColor)
              .fillStyle(setItemColor)
              .event("mouseover", colorLinks)
              .event("mouseout", restoreLinkColors)
              .event("click", setDescriptionInfo)
              .angle(Math.PI)  // 180 degrees in radians
              .shape(function (d) { return (d.movement === "Changed" ? "triangle" : "circle"); })
              .anchor("center")
              .add(pv.Label)
                .events("none")
                .text(function () { return totalIndex++; })
                .textBaseline("middle")
                .textStyle("white")
                .cursor("pointer");
        }
    }

    radar.anchor("radar");
    radar.render();

    allLinks = $("g a");

    $("svg a").tooltip({
        'container': 'body'
    });

    applyViewBox();
};

function setItemColor(object) {
    var quaterNumber = parseInt(object.pc.t / 90);
    if (item == null)
        return window.radarData[quaterNumber].color;
    return object.name === item.title ? window.radarData[quaterNumber].color : "gray";
}

function iterateOverLinks(lambda) {
    for (var i = 0; i < allLinks.length; ++i) {
        var link = $(allLinks[i]);
        var paths = link.children("path");
        if (paths.length) {
            lambda(allLinks[i], paths);
        }

        var circles = link.children("circle");
        if (circles.length) {
            lambda(allLinks[i], circles);
        }
    }
}

function colorLinks() {
    item = this.scene[this.index];
    currentTitle = item.title;
    iterateOverLinks(function (link, elements) {
        elements[0].style.oldStroke = elements[0].style.stroke;
        if (link.getAttribute("data-original-title") === currentTitle) {
            elements[0].style.stroke = "#FF0000";
            elements[0].style.fill = "#FF0000";
        } else {
            elements[0].style.stroke = "gray";
            elements[0].style.fill = "gray";
        }
    });
}

function restoreLinkColors() {
    item = null;
    iterateOverLinks(function (link, elements) {
        elements[0].style.stroke = elements[0].style.oldStroke;
        elements[0].style.fill = elements[0].style.oldStroke;
    });
}

function setTextColor(object) {
    var quaterNumber = parseInt(object.pc.t / 90);
    return (item != null && object.name === item.title ? window.radarData[quaterNumber].color : "gray");
}

function setDescriptionInfo(object) {
    item = null;

    var name = object.name || "";
    var description = object.description || labels.NoDescription;
    var movement = (object.movement || "") === "Changed" ? labels.Changed : labels.NotChanged;

    $("#modalDescription").text(description);
    $("#modalTitle").text(name);
    $("#modalChanged").text(movement);
    $('#itemModal').modal();
}

function applyViewBox() {
    var svg = $("#radar svg")[0];
    svg.setAttribute("viewBox", "0 0 " + radarWidth + " " + radarHeight);
}

$(function() {
    init(radarHeight, radarWidth, radarData, labels);
});