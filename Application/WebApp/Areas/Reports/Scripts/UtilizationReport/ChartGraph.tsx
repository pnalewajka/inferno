module UtilizationReport { 
    export class ChartGraph extends React.Component<IChart, {}> {
        render() {
            return <div id={this.props.Id} className="chart hide-on-mobile" ref={this.renderChart.bind(this)}></div>;
        }

        shouldComponentUpdate(nextProps: IChart, nextState: {}) {
            return false;
        }

        getCellDate(cellDate: Date): Date {
            var date = new Date(cellDate);
            return new Date(date.getFullYear(), date.getMonth(), 1);
        }

        getTextWidth(text: string): number {
            const $test = $("#text-test");
            $test.text(text);
            return Math.max(14, $test.width() + 1);
        }

        verticalAxis(selector: d3.Selection<any>, xScale: any, height: any) {
            selector.append("g")
                .attr("class", "x axis")
                .attr("transform", `translate(0, ${height})`)
                .call(d3.svg.axis().scale(xScale)
                    .orient("bottom").ticks(0));
        }

        horizontalAxis(selector: d3.Selection<any>, yScale: any) {
            selector.append("g")
                .attr("class", "y axis")
                .call(d3.svg.axis().scale(yScale)
                    .orient("left").ticks(5));
        }

        getClassNumberForIndex(index: number): number
        {
            return (index + 1) % 2 + 1;
        }

        renderChart(element: SVGElement) {
            const entryDistance = 90;
            let width = this.props.Rows[0].Cells.length * entryDistance;

            const selector = d3
                .select(`#${this.props.Id}`)
                .append("svg")
                .attr("style", `padding-left:${element.clientWidth - width + entryDistance / 2 - 2.5}px`);

            let valueRange = d3.extent<number>(this.getValueRanges(), d => d);
            let dateRange = d3.extent<Date>(this.getDateRanges(), d => d);

            const xScale = d3.time.scale().range([0, width - entryDistance]).domain(dateRange);
            const yScale = d3.scale.linear().range([element.clientHeight - entryDistance, 0]).domain(valueRange);

            this.verticalAxis(selector, xScale, element.clientHeight - entryDistance);
            this.horizontalAxis(selector, yScale);
            var valueline = d3.svg.line<InfernoChart.IChartEntry>()
                .x(d => xScale(this.getCellDate(d.Date)))
                .y(d => yScale(d.Value))
                .interpolate("linear");

            var lineCounter = 0;

            this.props.Rows
                .filter(r => r.IsChartRow)
                .forEach(r => {
                    ++lineCounter;
                    let circleCounter = 0;
                    const data: InfernoChart.IChartEntry[] = r.Cells.map(c => {
                        return { Date: new Date(c.Date), Value: c.Value };
                    });

                    selector.append("path")
                        .attr("class", `line line-${this.getClassNumberForIndex(lineCounter)}`)
                        .attr("d", valueline(data));

                    r.Cells.forEach(c => {
                        circleCounter++;
                        selector.append("circle")
                            .attr("id", `circle_${circleCounter}`)
                            .attr("data-index", circleCounter)
                            .attr("data-set", lineCounter + 1)
                            .attr("cy", () => yScale(c.Value))
                            .attr("cx", () => xScale(this.getCellDate(c.Date)) + 2.5)
                            .attr("r", () => this.getTextWidth(c.TextValue))
                            .attr("class", `dot dot-${this.getClassNumberForIndex(lineCounter)} ${c.Date === this.props.CurrentDate ? "selected-cell" : ""}`);

                        selector.append("text")
                            .attr("id", `text_${circleCounter}`)
                            .attr("data-index", circleCounter)
                            .attr("data-set", circleCounter + 1)
                            .attr("y", () => yScale(c.Value) + 2.5)
                            .attr("x", () => xScale(this.getCellDate(c.Date)) + 2.5)
                            .attr("text-anchor", "middle")
                            .attr("class", `text text-${this.getClassNumberForIndex(circleCounter)}`)
                            .attr("fill", "#fff")
                            .text(c.Value.toFixed(2));
                    });
                });
        }

        getDateRanges(): Date[] {
            var [row] = this.props.Rows;
            return row.Cells.map(c => this.getCellDate(c.Date));
        }

        getValueRanges(): number[] {
            let valueRanges: number[] = [];

            this.props.Rows.forEach(r => {
                r.Cells.forEach(c => {
                    valueRanges.push(c.Value);
                });
            });

            return valueRanges;
        }
    }
}