module UtilizationReport {
    export class DrillChartRow extends React.Component<IDrildownRowProps, IDrildownRowState> {
        private mounted: boolean = false;
        constructor() {
            super();
            this.state = { Loading: false, Data: null, Employees: null };
        }

        render() {
            if (this.state.Loading) {
                return this.renderLoading();
            }

            if (!!this.state.Data) {
                return this.renderItems();
            }

            return null;
        }

        componentWillMount(): void {
            this.mounted = true;
            this.componentWillReceiveProps(this.props);
        }

        componentWillUnmount(): void {
            this.mounted = false;
        }

        updateState(statePartial: any): void {
            if (this.mounted) {
                this.setState($.extend({}, this.state, statePartial));
            }
        }

        componentWillReceiveProps(nextProps: IDrildownRowProps): void {

            if (!!nextProps.SelectedCell) {
                this.updateState({ Loading: true, Data: null });

                var data = $.extend(
                    {},
                    SearchParameters(),
                    {
                        utilizationCategoryId: nextProps.Row.UtilizationCategoryId,
                        billability: nextProps.Row.Billability,
                        date: nextProps.SelectedCell.Date,
                        displayType: UrlHelper.getQueryParameter(window.location.href, "displayType", "0")
                    });

                $.post(!!nextProps.Row.UtilizationCategoryId ? "/Reports/UtilizationReport/Projects" : "/Reports/UtilizationReport/ProjectWithoutCategory", data).then(response => {
                        this.updateState({ Loading: false, Data: response.Projects });
                    });
            } else {
                this.updateState({ Loading: false, Data: null });
            }
        }

        renderLoading() {
            return <tr><td className="drilled-content" colSpan={this.props.Row.Cells.length + 1}><i className="fa fa-spinner"></i></td></tr>;
        }

        renderItems() {
            var indexOfCell = this.props.Row.Cells.indexOf(this.props.SelectedCell);

            let containerSpan = Math.min(6, this.props.Row.Cells.length);
            let spanBefore = indexOfCell - containerSpan + 1;
            if (spanBefore < 0) {
                spanBefore = 0;
            }

            let spanAfter = this.props.Row.Cells.length - spanBefore - containerSpan;

            return (
                <tr>
                    <td colSpan={spanBefore + 1}></td>
                    <td className="drilled-content" colSpan={containerSpan}>
                        <ul className="list-group">
                            {this.state.Data!.map(d => <ProjectRowComponent key={d.ProjectId} {...{ Project: d, Row: this.props.Row, SelectedCell: this.props.SelectedCell }} />)}
                        </ul>
                    </td>
                    {spanAfter > 0 ? <td colSpan={spanAfter}></td> : null}
                </tr>
            );
        }
    }
}