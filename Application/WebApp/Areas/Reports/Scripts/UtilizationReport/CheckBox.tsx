﻿module UtilizationReport {
    export class CheckBox extends React.Component<{ category?: number }, { isSelected: boolean }> {
        constructor() {
            super();
            this.state = { isSelected: false };
        }

        updateFromStore() {
            var { categories } = getStore().getState();
            this.setState({ isSelected: categories.filter(c => c == this.props.category).length > 0 });
        }

        componentDidMount() {
            getStore().subscribe(this.updateFromStore.bind(this));
            this.updateFromStore();
        }

        defaultStyle: React.CSSProperties = {
            cursor: "pointer",
            paddingRight: 5
        };

        render() {
            return <i
                className={`fa ${this.state.isSelected ? "fa-check-square-o" : "fa-square-o"}`}
                style={this.defaultStyle}
                onClick={() => {
                    if (this.props.category !== undefined) {
                        getStore().dispatch(Actions.ToggleCategoryAction(this.props.category));
                    }
                }}
            >
            </i>
        }
    }
}
