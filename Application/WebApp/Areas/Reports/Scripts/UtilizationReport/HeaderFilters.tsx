﻿module UtilizationReport {
    export enum DisplayType {
        Value = 0,
        Percentage = 1
    }

    export interface ISearchParameters {
        OrgUnitId?: number;
        LocationId?: number;
        JobProfileId?: number;
        LevelId?: number;
        HourStatus?: number;
        Months: number;
    }

    interface IFormComponentProps {
        label: string;
        options: any[];
        name: string;
    }

    function paramMach(name: string) {
        return location.search.match(new RegExp(name + "=(.*?)($|\&)", "i"));
    }

    export function GetSearchParameter(name: string): any {
        var match = paramMach(name);

        return (!!match ? match[1] : null);
    }

    export function SearchParameters(): ISearchParameters {
        return {
            OrgUnitId: GetSearchParameter("orgUnitId"),
            LocationId: GetSearchParameter("locationId"),
            JobProfileId: GetSearchParameter("jobProfileId"),
            LevelId: GetSearchParameter("levelId"),
            HourStatus: GetSearchParameter("hourStatus"),
            Months: GetSearchParameter("months")
        };
    }

    export class HeaderComponent extends React.Component<{
        translations: ITranslations,
        orgUnitItems: IOptionElement[],
        locationItems: IOptionElement[],
        jobProfileItems: IOptionElement[],
        levelItems: IOptionElement[],
        hourStatusItems: IOptionElement[]
    }, {}>{
        private dialogComponent: Dante.Ui.DialogComponent | null = null;

        private displayTypeOptions: any[] = [
            { Id: DisplayType.Value, Name: 'Display hours' },
            { Id: DisplayType.Percentage, Name: 'Display %' },
        ];

        private showFilterDialog(): void {
            if (this.dialogComponent != null) {
                this.dialogComponent.show();
            }
        }

        public render(): JSX.Element {
            const filterParameters: IFormComponentProps[] = [
                { label: this.props.translations.OrgUnitLabel, options: this.props.orgUnitItems, name: "orgUnitId" },
                { label: this.props.translations.LocationLabel, options: [{ Id: null, Name: '' }, ...this.props.locationItems], name: "locationId" },
                { label: this.props.translations.JobProfileLabel, options: [{ Id: null, Name: '' }, ...this.props.jobProfileItems], name: "jobProfileId" },
                { label: this.props.translations.LevelLabel, options: [{ Id: null, Name: '' }, ...this.props.levelItems], name: "levelId" },
                {
                    label: this.props.translations.DisplayTypeLabel, options: [
                        { Id: DisplayType.Value, Name: this.props.translations.HoursDisplayOptionLabel },
                        { Id: DisplayType.Percentage, Name: this.props.translations.PercentageDisplayOptionLabel },
                    ], name: "displayType"
                },
                { label: this.props.translations.HoursLabel, options: this.props.hourStatusItems, name: "hourStatus" },
                { label: this.props.translations.MonthsInPastLabel, options: Dante.Utils.Range(13).filter(r => r > 5).map(r => ({ Id: r, Name: r.toString() })), name: "months" }
            ];

            return (
                <div>
                    <div>
                        <div className="col-lg-12">
                            {filterParameters.map(p => <FilterFieldDisplayComponent key={p.name} showDialog={this.showFilterDialog.bind(this)} {...p} />).filter(r => r != null)}

                            <button className="btn btn-sm btn-link" style={{ marginRight: 10 }} onClick={this.showFilterDialog.bind(this)}>
                                {this.props.translations.ChangeFiltersLabel}
                            </button>
                        </div>
                    </div>
                    <Dante.Ui.DialogComponent
                        ref={dialog => this.dialogComponent = dialog}
                        title={this.props.translations.ChangeFiltersLabel}
                        actions={[Dante.Ui.DialogComponent.SubmitButton(), Dante.Ui.DialogComponent.CancelButton()]}>
                        <form method="GET" style={{ minHeight: 400 }}>
                            <div className="row">
                                {filterParameters.map(p => <FilterFieldEditComponent key={p.name} {...p} />)}
                            </div>
                        </form>
                    </Dante.Ui.DialogComponent>
                </div>
            );
        }
    }

    function FormContainer(props: any): JSX.Element {
        return <div className="form-group">
            <label className="col-sm-2 control-label" style={{ lineHeight: "33.6px" }}>
                <span>
                    {props.label}
                </span>
            </label>
            <div className="col col-lg-10">
                {props.children}
            </div>
        </div>;
    }

    class FilterFieldDisplayComponent extends React.Component<IFormComponentProps & { showDialog?: () => void }> {
        private removeFilterProperty(key: string): void {
            CommonDialogs.pleaseWait.show();
            window.location.href = UrlHelper.updateUrlParameter(window.location.href, key, "")
        }

        public render(): JSX.Element | null {
            let value = GetSearchParameter(this.props.name);
            let selectedId = !!value ? value : this.props.options[0].Id;

            const [selectedOption] = this.props.options.filter(o => o.Id == selectedId);

            if (selectedOption === undefined) {
                return null;
            }

            const { Name } = selectedOption;

            if (Name === "") {
                return null;
            }

            const canRemoveFilter = this.props.options[0].Id == null;

            return (
                <button className="btn btn-sm btn-default" style={{ marginRight: 10 }} onClick={() => { if (this.props.showDialog !== undefined) { this.props.showDialog(); } }}>
                    <b>{this.props.label}</b>
                    <span>: {Name}</span>
                    {
                        canRemoveFilter
                            ? <i style={{ marginLeft: 10 }} className="fa fa-close text-danger" onClick={(e) => { e.preventDefault(); e.stopPropagation(); this.removeFilterProperty(this.props.name); }}>&nbsp;</i>
                            : null
                    }
                </button>
            );
        }
    }

    class FilterFieldInlineEditComponent extends React.Component<IFormComponentProps> {
        public render(): JSX.Element {
            let value = GetSearchParameter(this.props.name);
            let selectedId = !!value ? value : this.props.options[0].Id;

            let attachSelectPicker = (element: HTMLSelectElement) => {
                $(element).selectpicker({})
                    .on('change', function (this: JQuery) {
                        CommonDialogs.pleaseWait.show();
                        let $element = $(this);
                        var selected = $element.find("option:selected").val();
                        $element.parents("form").submit();
                    });
            };

            return (
                <div className="btn-group" style={{ marginRight: 5 }}>
                    {
                        this.props.options.map(o => (
                            <button
                                key={o.Id}
                                disabled={o.Id == selectedId}
                                onClick={() => {
                                    CommonDialogs.pleaseWait.show();
                                    window.location.href = UrlHelper.updateUrlParameter(window.location.href, this.props.name, o.Id)
                                }}
                                className={Dante.Utils.ClassNames({ "btn-default": o.Id == selectedId }, "btn btn-xs")}>
                                {o.Name}
                            </button>
                        ))
                    }
                </div>
            );
        }
    }

    class FilterFieldEditComponent extends React.Component<IFormComponentProps> {
        public render(): JSX.Element {
            let value = GetSearchParameter(this.props.name);
            let selectedId = !!value ? value : this.props.options[0].Id;

            let attachSelectPicker = (element: HTMLSelectElement) => {
                $(element).selectpicker({});
            };

            return (
                <div className="col-lg-12">
                    <FormContainer label={this.props.label}>
                        <select name={this.props.name} className="form-control" defaultValue={selectedId} ref={attachSelectPicker}>
                            {this.props.options.map(o => <option key={o.Id} value={o.Id}>{o.Name}</option>)}
                        </select>
                    </FormContainer>
                </div>
            );
        }
    }
}
