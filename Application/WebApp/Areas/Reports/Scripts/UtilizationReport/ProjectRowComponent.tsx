﻿module UtilizationReport {
    export class ProjectRowComponent extends React.Component<{ Project: IDataProjectRow, Row: IChartRow, SelectedCell: IChartCell }, { Employees: IDataEmployeeRow[] | null }>  {
        constructor() {
            super();
            this.state = { Employees: null };
        }

        public render(): JSX.Element {
            return (
                <li className="list-group-item row">
                    <Chevron isSelected={!!this.state.Employees} onSelect={this.cevronClicked.bind(this)} />
                    <span className="col col-lg-10">{this.props.Project.Name}</span>
                    <b className="col col-lg-2">{ this.props.Project.DisplayValue }</b>
                    {
                        !this.state.Employees ?
                            null :
                            (
                                <ul className="col col-lg-12 row">
                                    {this.state.Employees.map(e => (
                                        <li key={e.EmployeeId} className="row">
                                            <span className="col col-lg-10">{`${e.FirstName} ${e.LastName}`}</span>
                                            <b className="col col-lg-2"> {e.DisplayValue} </b>
                                        </li>
                                    ))}
                                </ul>
                            )
                    }
                </li>
            );
        }

        cevronClicked(projectId: number) {

            if (!!this.state.Employees) {
                this.setState({ Employees: null });
            } else {

                var data = $.extend(
                    {},
                    SearchParameters(),
                    {
                        utilizationCategoryId: this.props.Row.UtilizationCategoryId,
                        billability: this.props.Row.Billability,
                        date: this.props.SelectedCell.Date,
                        projectId: this.props.Project.ProjectId,
                        displayType: UrlHelper.getQueryParameter(window.location.href, "displayType", "0")
                    });

                $.post(!!this.props.Row.UtilizationCategoryId ? "/Reports/UtilizationReport/Employees" : "/Reports/UtilizationReport/EmployeesWithoutCategory", data)
                    .then((response: any) => {
                        this.setState({ Employees: response.Employees });
                    });
            }
        }
    }
}