﻿module UtilizationReport {
    export class ChartCell extends React.Component<{ cell: IChartCell, canDrill: boolean, highlighted: boolean, selected?: boolean, onSelect: (cell: IChartCell | null) => void }, {}> {
        private get displayDrilldown(): boolean {
            return this.props.cell.Value !== 0 && this.props.canDrill;
        }

        render() {
            let extraClasses: string[] = [];
            if (!!this.props.highlighted) {
                extraClasses.push("selected-cell");
            }

            if (!!this.props.selected) {
                extraClasses.push("drilled-cell");
            }

            return (
                <td className={`numeric-column-wide align-right ${extraClasses.join(" ")}`}>
                    {this.displayDrilldown ? <Chevron isSelected={this.props.selected == true} onSelect={this.cevronClicked.bind(this)} /> : null}
                    {this.props.cell.TextValue}
                </td>
            );
        }

        cevronClicked() {
            if (!!this.props.selected) {
                this.props.onSelect(null);
            } else {
                this.props.onSelect(this.props.cell);
            }
        }
    }
}