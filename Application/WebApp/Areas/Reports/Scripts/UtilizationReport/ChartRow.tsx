﻿module UtilizationReport {
    export class ChartRow extends React.Component<{ row: IChartRow, currentDate: Date, canExport: boolean, index: number }, { selectedCell: IChartCell | null }> {

        constructor() {
            super();
            this.state = { selectedCell: null };
        }

        renderCells() {
            let counter = 0;
            return this.props.row.Cells
                .map(c =>
                    <ChartCell
                        cell={c}
                        highlighted={c.Date === this.props.currentDate}
                        selected={this.state.selectedCell === c}
                        canDrill={this.props.row.CanDrillData}
                        onSelect={selectedCell => { this.setState($.extend({}, this.state, { selectedCell })); }}
                        key={counter++}
                    />
                );
        }

        render() {
            return (
                <tbody>
                    <tr className={`row_${this.props.index}  ${this.props.row.IsChartRow ? "chart_row" : ""}`}>
                        <td>
                            {this.props.row.CanExportData && this.props.canExport ? <CheckBox category={this.props.row.UtilizationCategoryId} /> : null}
                            {this.props.row.Label}
                        </td>
                    {this.renderCells()}
                </tr>
                {!!this.state.selectedCell ? <DrillChartRow {...{ Row: this.props.row, SelectedCell: this.state.selectedCell }} /> : null}
                </tbody>
            );
        }
    }
}