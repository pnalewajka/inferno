module UtilizationReport {
    export class ChartTable extends React.Component<IChart, { categoriesSelected: boolean, canExport: boolean }> {

        constructor() {
            super();
            this.state = { categoriesSelected: false, canExport: false };
        }

        componentWillMount() {
            getStore().subscribe(this.refreshState.bind(this));
            this.refreshState();
        }

        refreshState() {
            var storeData = getStore().getState();
            this.setState({ categoriesSelected: storeData.categories.length > 0, canExport: storeData.canExport });
        }

        renderYearHeader(): JSX.Element[] {
            let [firstRow] = this.props.Rows;
            let renderedCells: JSX.Element[] = [];
            let renderedYears: { [key: number]: number } = {};

            firstRow.Cells.forEach(c => {
                renderedYears[new Date(c.Date).getFullYear()] = (renderedYears[new Date(c.Date).getFullYear()] || 0) + 1;
            });

            for (var year in renderedYears) {
                renderedCells.push(<th colSpan={renderedYears[year]} key={year}>{year}</th>);
            }

            return renderedCells;
        }

        renderMonthsHeader(canExport: boolean): JSX.Element[] {
            let [firstRow] = this.props.Rows;
            let counter = 0;

            const iconStyle: React.CSSProperties = {
                float: "right",
                top: "3px",
                position: "relative",
                cursor: "pointer"
            };

            return firstRow.Cells.map(c => (
                <th key={counter++}>
                    <span>{new Date(c.Date).getMonth() + 1}</span>
                    {canExport ? <i onClick={() => { this.generateReport(c.Date); }} className="fa fa-file-excel-o" style={iconStyle}></i> : null}
                </th>
            ));
        }

        generateReport(date: Date) {
            let $form = $(`<form action="/Reporting/ReportExecution/PerformReport?reportCode=UtilizationReport" method="POST"></form>`);
            var searchParameters = SearchParameters();
            var storeData = getStore().getState();
            $('<input type="hidden" name="OrgUnitId" />').val(searchParameters.OrgUnitId!).appendTo($form);
            $('<input type="hidden" name="UtilizationMonth" />').val(date.toString()).appendTo($form);
            $('<input type="hidden" name="LocationId" />').val(searchParameters.LocationId!).appendTo($form);
            $('<input type="hidden" name="JobProfileId" />').val(searchParameters.JobProfileId!).appendTo($form);
            $('<input type="hidden" name="LevelId" />').val(searchParameters.LevelId!).appendTo($form);
            $('<input type="hidden" name="HourStatus" />').val(searchParameters.HourStatus!).appendTo($form);
            $('<input type="hidden" name="UtilizationCategoryIds" />').val(storeData.categories.join(',')).appendTo($form);
            $('<input type="hidden" name="MustIncludeEntriesWithoutCategory" />').val(storeData.categories.some(v => v == null) ? "true" : "false").appendTo($form);

            $form.appendTo($('body')).submit();
        }

        render() {
            let rowCounter = 0;
            let keyCounter = 0;
            return (
                <table className="table table-striped table-bordered table-hover table-chart table-without-last-row">
                    <thead>
                        <tr>
                            <th></th>
                            {this.renderYearHeader()}
                        </tr>
                        <tr>
                            <th></th>
                            {this.renderMonthsHeader(this.state.categoriesSelected && this.state.canExport)}
                        </tr>
                    </thead>
                    {this.props.Rows.map(r => <ChartRow row={r} canExport={this.state.canExport} currentDate={this.props.CurrentDate} key={++keyCounter} index={++rowCounter} />)}
                </table>
            );
        }
    }
}