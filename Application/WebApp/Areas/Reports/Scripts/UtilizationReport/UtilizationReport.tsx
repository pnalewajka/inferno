﻿module UtilizationReport {

    export interface IOptionElement {
        Id: number;
        Name: string;
    }

    export interface IChevronProps {
        isSelected: boolean;
        isLoading?: boolean;
        onSelect?: () => void;
    }

    export interface IChartCell {
        Date: Date;
        TextValue: string;
        Value: number;
    }

    export interface IChartRow {
        IsChartRow: boolean;
        Label: string;
        Cells: IChartCell[];
        UtilizationCategoryId?: number;
        Billability: boolean;
        CanDrillData: boolean;
        CanExportData: boolean;
    }

    export interface IChart {
        CurrentDate: Date;
        Id: string;
        Rows: IChartRow[];
    }

    export interface IDrildownRowProps {
        Row: IChartRow;
        SelectedCell: IChartCell;
    }

    export interface IDrildownRowState {
        Loading: boolean;
        Data: IDataProjectRow[] | null;
        Employees: any[] | null;
    }

    export interface IDataProjectRow {
        ProjectId: number;
        Name: string;
        DisplayValue: string;
    }

    export interface IDataEmployeeRow {
        EmployeeId: number;
        FirstName: string;
        LastName: string;
        DisplayValue: string;
    }

    export interface ITranslations {
        NoData: string,
        OrgUnitLabel: string,
        LocationLabel: string,
        JobProfileLabel: string,
        LevelLabel: string,
        HoursLabel: string,
        ChangeFiltersLabel: string,
        DisplayTypeLabel: string,
        HoursDisplayOptionLabel: string,
        PercentageDisplayOptionLabel: string,
        MonthsInPastLabel: string,
    }

    export function selectedOrgUnit(): number {
        return $(".selectpicker").val();
    }

    export function create(data: {
        Charts: IChart[],
        OrgUnitItems: IOptionElement[],
        LocationItems: IOptionElement[],
        JobProfileItems: IOptionElement[],
        LevelItems: IOptionElement[],
        HourStatusItems: IOptionElement[],
        CanExportReport: boolean,
    }, translate: ITranslations) {
        ReactDOM.render(
            <HeaderComponent
                translations={translate}
                orgUnitItems={data.OrgUnitItems}
                locationItems={data.LocationItems}
                jobProfileItems={data.JobProfileItems}
                levelItems={data.LevelItems}
                hourStatusItems={data.HourStatusItems}
            />,
            document.getElementById("filter-header"))

        if (!data.Charts) {
            ReactDOM.render((
                <div className="chart-container">
                    <div><i>{translate.NoData}</i></div>
                </div>
            ), document.getElementById("chart-table"));
            $("#report-chart-page").css("visibility", "visible");
            return;
        }

        let [chart] = data.Charts;
        var storeData = getStore().dispatch({ type: data.CanExportReport ? 'ENABLE_GENERATE_REPORT' : 'DISABLE_GENERATE_REPORT' });

        ReactDOM.render((
            <div className="chart-container">
                <ChartGraph {...chart} />
                <ChartTable {...chart} />
            </div>
        ), document.getElementById("chart-table"));

        $("#report-chart-page").css("visibility", "visible");
    }

    export const Chevron: React.StatelessComponent<IChevronProps> = (props: IChevronProps) => {
        let classes: string[] = [
            "chevron", "fa"
        ];

        if (props.isLoading) {
            classes.push("fa-spinner");
        } else if (!props.isSelected) {
            classes.push("fa-chevron-circle-down");
        } else {
            classes.push("fa-chevron-circle-up");
        }

        return (
            <i
                className={classes.join(" ")}
                onClick={() => {
                    if (!!props.onSelect) {
                        props.onSelect();
                    }
                }}>
            </i>);
    };
}

