﻿module UtilizationReport {
    var ActionTypes = {
        SelectCategory: 'SELECT_CATEGORY',
        UnSelectCategory: 'UNSELECT_CATEGORY',
        ToggleCategory: 'TOGGLE_CATEGORY',
        EnableExport: 'ENABLE_GENERATE_REPORT',
        DisableExport: 'DISABLE_GENERATE_REPORT'
    }

    export var Actions = {
        SelectCategoryAction: (category: number): ICategoryAction => { return { category, type: ActionTypes.SelectCategory } },
        UnSelectCategoryAction: (category: number): ICategoryAction => { return { category, type: ActionTypes.UnSelectCategory } },
        ToggleCategoryAction: (category: number): ICategoryAction => { return { category, type: ActionTypes.ToggleCategory } },
    }

    export interface ICategoryAction extends Redux.Action{
        category: number;
    }

    export interface IStoreState {
        categories: number[];
        canExport: boolean;
    }
    
    var categoriesReducer = ((state: number[] = [], action: ICategoryAction): number[] => {
        switch (action.type) {
            case ActionTypes.SelectCategory:
                return [...state, action.category];
            case ActionTypes.UnSelectCategory:
                return [...state.filter(c => c != action.category)];
            case ActionTypes.ToggleCategory:
                if (state.filter(c => c == action.category).length > 0) {
                    return [...state.filter(c => c != action.category)];
                } else {
                    return [...state, action.category];
                }
            default:
                return state;
        }
    }) as Redux.Reducer<number[]>;

    var exportReducer: Redux.Reducer<boolean> = (state: boolean = false, action: Redux.Action): boolean => {
        switch (action.type) {
            case ActionTypes.EnableExport:
                return true;
            case ActionTypes.DisableExport:
                return false;
            default:
                return state;
        }
    }

    export const getStore = Dante.Utils.Lazy(() => Redux.createStore<IStoreState>(Redux.combineReducers<IStoreState>({
        categories: categoriesReducer,
        canExport: exportReducer,
    })));
}