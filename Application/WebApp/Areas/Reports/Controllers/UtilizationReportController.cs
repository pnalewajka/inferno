﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Reports.Models.Chart;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Controllers
{
    public class UtilizationReportViewModel : IUtilizationReportSearch
    {
        public long? OrgUnitId { get; set; }

        public long? LocationId { get; set; }

        public long? JobProfileId { get; set; }

        public long? LevelId { get; set; }

        public HourStatus HourStatus { get; set; }

        public bool MustIncludeEntriesWithoutCategory { get; set; }

        public UtilizationDisplayType DisplayType { get; set; }

        public int Months { get; set; }
    }

    [AtomicAuthorize(SecurityRoleType.CanViewUtilizationReport)]
    public class UtilizationReportController : AtomicController
    {
        private readonly IOrgUnitCardIndexDataService _orgUnitCardIndexDataService;
        private readonly ITimeService _timeService;
        private readonly ILocationService _locationService;
        private readonly IJobProfileService _jobProfileService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUtilizationReportService _utilizationReportService;
        private readonly IUnitOfWorkService<IOrganizationScope> _organizationUnitOfWorkService;

        public UtilizationReportController(
            IBaseControllerDependencies baseDependencies,
            IOrgUnitCardIndexDataService orgUnitCardIndexDataService,
            ITimeService timeService,
            ILocationService locationService,
            IJobProfileService jobProfileService,
            IPrincipalProvider principalProvider,
            IUtilizationReportService utilizationReportService,
            IUnitOfWorkService<IOrganizationScope> organizationUnitOfWorkService
            ) : base(baseDependencies)
        {
            _orgUnitCardIndexDataService = orgUnitCardIndexDataService;
            _timeService = timeService;
            _locationService = locationService;
            _jobProfileService = jobProfileService;
            _principalProvider = principalProvider;
            _utilizationReportService = utilizationReportService;
            _organizationUnitOfWorkService = organizationUnitOfWorkService;

            Layout.PageHeader = ReportResources.UtilizationReportPageTitle;
            Layout.PageTitle = ReportResources.UtilizationReportPageTitle;

            Layout.Scripts.Add("~/Scripts/d3/d3.min.js");
            Layout.Css.Add("~/Content/ReportCharts.css");
        }

        public ActionResult Index(UtilizationReportViewModel model)
        {
            if (!model.OrgUnitId.HasValue)
            {
                model.OrgUnitId = _orgUnitCardIndexDataService.TopChartOrgUnit(OrgUnitFeatures.ShowInUtilizationReport)?.Id;

                // Set default OrgUnit id parameter
                var parameters = Request.QueryString.ToDictionary();
                parameters.Add(nameof(model.OrgUnitId), model.OrgUnitId.ToString());

                return Redirect(Url.ActionWithParams(
                    nameof(Index),
                    RoutingHelper.BuildQueryString(parameters)));
            }

            var viewModel = BuildUtilizationChartViewModel(model);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Projects(UtilizationReportViewModel model, long? utilizationCategoryId, bool? billability, DateTime date)
        {
            var projects = _utilizationReportService
                .GetUtilizationProjects(model, utilizationCategoryId, billability, date)
                .ToList();

            return new JsonNetResult(new { Projects = projects });
        }

        [HttpPost]
        public ActionResult ProjectWithoutCategory(UtilizationReportViewModel model, DateTime date)
        {
            var projects = _utilizationReportService
                .GetUtilizationProjectsWithoutUtilization(model, date)
                .ToList();

            return new JsonNetResult(new { Projects = projects });
        }

        [HttpPost]
        public ActionResult Employees(UtilizationReportViewModel model, long? utilizationCategoryId, bool? billability, DateTime date, long projectId)
        {
            var employees = _utilizationReportService
                .GetUtilizationProjectEmployees(model, utilizationCategoryId, billability, date, projectId)
                .ToList();

            return new JsonNetResult(new { Employees = employees });
        }

        [HttpPost]
        public ActionResult EmployeesWithoutCategory(UtilizationReportViewModel model, DateTime date, long projectId)
        {
            var employees = _utilizationReportService
                .GetUtilizationProjectEmployeesWithoutUtilization(model, date, projectId)
                .ToList();

            return new JsonNetResult(new { Employees = employees });
        }

        private UtilizationChartViewModel BuildUtilizationChartViewModel(UtilizationReportViewModel model)
        {
            var chartViewModel = BuildChartViewModelForUtilizationReport(model);

            return new UtilizationChartViewModel
            {
                Charts = chartViewModel != null ? new[] { chartViewModel } : null,
                CanExportReport = _principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateUtilizationReport),
                OrgUnitItems = BuildDropdownItems(),
                LocationItems = BuildLocationDropdownItems(),
                JobProfileItems = BuildJobProfileDropdownItems(),
                LevelItems = BuildLevelsDropdownItems(),
                HourStatusItems = BuildHourStatusDropdownItems()
            };
        }

        private ChartViewModel BuildChartViewModelForUtilizationReport(UtilizationReportViewModel model)
        {
            var currentDate = _timeService.GetCurrentDate().GetFirstDayInMonth();

            var utilizationReportRowModels = _utilizationReportService.GetUtilizationReportRowModels(model).ToList();

            if (utilizationReportRowModels.IsNullOrEmpty())
            {
                return null;
            }

            var rows = utilizationReportRowModels
                       .Select(x => new ChartRowViewModel
                       {
                           Label = x.Label,
                           IsChartRow = x.IsChartRow,
                           UtilizationCategoryId = x.UtilizationCategoryId,
                           Billability = x.Billability,
                           CanDrillData = x.CanDrillData,
                           CanExportData = x.CanExportData,
                           Cells = x.Cells.Select(c => new ChartCellViewModel { Date = c.Date, Value = c.Value, TextValue = c.TextValue })
                       })
                       .ToList();

            return new ChartViewModel { Rows = rows, CurrentDate = currentDate, Id = "utilizationChart" };
        }

        private IReadOnlyCollection<ChartDropDownElement> BuildLocationDropdownItems()
        {
            return _locationService.GetAllLocations().Select(l => new ChartDropDownElement
            {
                Id = l.Id,
                Name = l.Name
            }).ToList();
        }

        private IReadOnlyCollection<ChartDropDownElement> BuildJobProfileDropdownItems()
        {
            return _jobProfileService
                .GetAllJobProfiles()
                .Select(jp =>
                new ChartDropDownElement
                {
                    Id = jp.Id,
                    Name = jp.Name
                }).ToList();
        }

        private IReadOnlyCollection<ChartDropDownElement> BuildLevelsDropdownItems()
        {
            return EnumHelper.GetEnumValues<JobMatrixLevels>()
                .Select(l => new ChartDropDownElement
                {
                    Id = (int)l,
                    Name = l.GetDescription()
                })
                .ToList();
        }

        private IReadOnlyCollection<ChartDropDownElement> BuildHourStatusDropdownItems()
        {
            return EnumHelper.GetEnumValues<HourStatus>()
                .Select(l => new ChartDropDownElement
                {
                    Id = (int)l,
                    Name = l.GetDescription()
                })
                .ToList();
        }

        private IReadOnlyCollection<ChartDropDownElement> BuildDropdownItems()
        {
            using (var unitOfWork = _organizationUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories
                                 .OrgUnits
                                 .OrderBy(ou => ou.NameSortOrder)
                                 .Where(ou => ou.OrgUnitFeatures.HasFlag(OrgUnitFeatures.ShowInUtilizationReport))
                                 .ToList()
                                 .Select(ou => new ChartDropDownElement
                                 {
                                     Id = ou.Id,
                                     Name = OrgUnitName(ou)
                                 }).ToList();
            }
        }

        private string OrgUnitName(OrgUnit ou)
        {
            var managerFirstName = ou.Employee?.FirstName;
            var managerLastName = ou.Employee?.LastName;
            var managerId = ou.EmployeeId;

            if (!managerId.HasValue || ou.Name.Contains($"{managerFirstName} {managerLastName}"))
            {
                return ou.Name;
            }

            return $"{ou.Name} ({managerFirstName} {managerLastName})";
        }
    }
}