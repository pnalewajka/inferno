﻿using System;
using System.IO;
using System.Web.Mvc;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.Reports.Models;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewAllocationReports)]
    public class AllocationReportController : AtomicController
    {
        private const string ExcelFileNameFormat = "{0} {1:yyyy-MM-dd}.xlsx";
        private const string ExcelExtension = ".xlsx";

        private readonly ISeniorityLevelsPerJobProfileReportService _seniorityLevelsPerJobProfileReportService;
        private readonly ISeniorityLevelsPerLocationReportService _seniorityLevelsPerLocationReportService;
        private readonly ISeniorityLevelsPerOrgUnitReportService _seniorityLevelsPerOrgUnitReportService;

        private readonly ITimeColumnPerJobProfileReportService _timeColumnPerJobProfileReportService;
        private readonly ITimeColumnPerLocationReportService _timeColumnPerLocationReportService;
        private readonly ITimeColumnPerOrgUnitReportService _timeColumnPerOrgUnitReportService;

        private readonly IUtilizationStatusPerJobProfileReportService _utilizationStatusPerJobProfileReportService;
        private readonly IUtilizationStatusPerLocationReportService _utilizationStatusPerLocationReportService;
        private readonly IUtilizationStatusPerOrgUnitReportService _utilizationStatusPerOrgUnitReportService;

        private readonly IAllocationsPerProjectReportService _allocationsPerProjectReportService;

        private readonly ITimeService _timeService;
        private readonly IPrincipalProvider _principalProvider;

        public AllocationReportController(IBaseControllerDependencies baseDependencies,
            ISeniorityLevelsPerJobProfileReportService seniorityLevelsPerJobProfileReportService,
            ISeniorityLevelsPerLocationReportService seniorityLevelsPerLocationReportService,
            ISeniorityLevelsPerOrgUnitReportService seniorityLevelsPerOrgUnitReportService,
            ITimeColumnPerJobProfileReportService timeColumnPerJobProfileReportService,
            ITimeColumnPerLocationReportService timeColumnPerLocationReportService,
            ITimeColumnPerOrgUnitReportService timeColumnPerOrgUnitReportService, ITimeService timeService,
            IPrincipalProvider principalProvider,
            IUtilizationStatusPerJobProfileReportService utilizationStatusPerJobProfileReportService,
            IUtilizationStatusPerLocationReportService utilizationStatusPerLocationReportService,
            IUtilizationStatusPerOrgUnitReportService utilizationStatusPerOrgUnitReportService, IAllocationsPerProjectReportService allocationsPerProjectReportService)
            : base(baseDependencies)
        {
            _seniorityLevelsPerJobProfileReportService = seniorityLevelsPerJobProfileReportService;
            _seniorityLevelsPerLocationReportService = seniorityLevelsPerLocationReportService;
            _seniorityLevelsPerOrgUnitReportService = seniorityLevelsPerOrgUnitReportService;
            _timeColumnPerJobProfileReportService = timeColumnPerJobProfileReportService;
            _timeColumnPerLocationReportService = timeColumnPerLocationReportService;
            _timeColumnPerOrgUnitReportService = timeColumnPerOrgUnitReportService;
            _timeService = timeService;
            _principalProvider = principalProvider;
            _utilizationStatusPerJobProfileReportService = utilizationStatusPerJobProfileReportService;
            _utilizationStatusPerLocationReportService = utilizationStatusPerLocationReportService;
            _utilizationStatusPerOrgUnitReportService = utilizationStatusPerOrgUnitReportService;
            _allocationsPerProjectReportService = allocationsPerProjectReportService;
        }

        public ActionResult Index()
        {
            Layout.PageTitle = ReportResources.AllocationReports;
            Layout.PageHeader = ReportResources.AllocationReports;

            var model = new ReportPageViewModel(_principalProvider.Current);

            return View(model);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateSeniorityLevelsReport)]
        public ActionResult SeniorityLevelsReport()
        {
            var model = new SeniorityLevelsReportParamsViewModel
            {
                ReportType = AllocationReportType.PerOrgUnit,
            };
            var dialogModel = new ModelBasedDialogViewModel<SeniorityLevelsReportParamsViewModel>(model)
            {
                Title = ReportResources.ReportEmployeesBySeniorityLevelsText
            };

            return PartialView(dialogModel);
        }
        
        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateUtilizationStatusReport)]
        public ActionResult UtilizationStatusReport()
        {
            var model = new TimeColumnReportParamViewModel
            {
                ReportType = AllocationReportType.PerOrgUnit,
                FromDate = DateTime.Today,
                ToDate = DateTime.Today.AddMonths(2),
            };
            var dialogModel = new ModelBasedDialogViewModel<TimeColumnReportParamViewModel>(model)
            {
                Title = ReportResources.ReportEmployeesByUtilizationStatusText,
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateActiveInactiveEmployeesReport)]
        public ActionResult ActiveInactiveEmployeesReport()
        {
            var model = new TimeColumnReportParamViewModel
            {
                ReportType = AllocationReportType.PerOrgUnit,
                FromDate = DateTime.Today,
                ToDate = DateTime.Today.AddMonths(2),
            };
            var dialogModel = new ModelBasedDialogViewModel<TimeColumnReportParamViewModel>(model)
            {
                Title = ReportResources.ReportActiveInactiveEmployeesText,
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateNewHiredLeavingEmployeesReport)]
        public ActionResult NewHiredLeavingEmployeesReport()
        {
            var model = new TimeColumnReportParamViewModel
            {
                ReportType = AllocationReportType.PerOrgUnit,
                FromDate = DateTime.Today,
                ToDate = DateTime.Today.AddMonths(2),
            };
            var dialogModel = new ModelBasedDialogViewModel<TimeColumnReportParamViewModel>(model)
            {
                Title = ReportResources.ReportEmployeesByNewHiredLeavingEmployeesText,
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateAllocationPerProjectReport)]
        public ActionResult AllocationPerProjectInDaysReport()
        {
            var model = new AllocationPerProjectViewModel
            {
                FromDate = DateTime.Today,
                ToDate = DateTime.Today.AddDays(10),
            };

            var dialogModel = new ModelBasedDialogViewModel<AllocationPerProjectViewModel>(model)
            {
                Title = ReportResources.ReportAllocationsPerProjectsInDaysText,
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateSeniorityLevelsReport)]
        public ActionResult GenerateSeniorityLevelsReport(SeniorityLevelsReportParamsViewModel model)
        {
            if (ModelState.IsValid)
            {
                switch (model.ReportType)
                {
                    case AllocationReportType.PerOrgUnit:
                        return DialogHelper.Redirect(this, Url.Action("GenerateSeniorityLevelsPerOrgUnitReport"), true);

                    case AllocationReportType.PerLocation:
                        return DialogHelper.Redirect(this, Url.Action("GenerateSeniorityLevelsPerLocationReport"), true);

                    case AllocationReportType.PerJobProfile:
                        return DialogHelper.Redirect(this, Url.Action("GenerateSeniorityLevelsPerJobProfileReport"), true);

                    default:
                        throw new InvalidDataException();
                }
            }

            var dialogModel = new ModelBasedDialogViewModel<SeniorityLevelsReportParamsViewModel>(model)
            {
                Title = ReportResources.ReportEmployeesBySeniorityLevelsText
            };

            return PartialView("SeniorityLevelsReport", dialogModel);
        }


        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateUtilizationStatusReport)]
        public ActionResult GenerateUtilizationStatusReport(TimeColumnReportParamViewModel model)
        {
            if (ModelState.IsValid)
            {
                switch (model.ReportType)
                {
                    case AllocationReportType.PerOrgUnit:
                        return DialogHelper.Redirect(this, Url.Action("GenerateUtilizationStatusPerOrgUnitReport"), true);

                    case AllocationReportType.PerLocation:
                        return DialogHelper.Redirect(this, Url.Action("GenerateUtilizationStatusPerLocationReport"), true);

                    case AllocationReportType.PerJobProfile:
                        return DialogHelper.Redirect(this, Url.Action("GenerateUtilizationStatusPerJobProfileReport"), true);

                    default:
                        throw new InvalidDataException();
                }
            }

            var dialogModel = new ModelBasedDialogViewModel<TimeColumnReportParamViewModel>(model)
            {
                Title = ReportResources.ReportEmployeesByUtilizationStatusText
            };

            return PartialView("UtilizationStatusReport", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateActiveInactiveEmployeesReport)]
        public ActionResult GenerateActiveInactiveEmployeesReport(TimeColumnReportParamViewModel model)
        {
            if (ModelState.IsValid)
            {
                switch (model.ReportType)
                {
                    case AllocationReportType.PerOrgUnit:
                        return DialogHelper.Redirect(this, Url.Action("GenerateActiveInactiveEmployeesPerOrgUnitReport"), true);

                    case AllocationReportType.PerLocation:
                        return DialogHelper.Redirect(this, Url.Action("GenerateActiveInactiveEmployeesPerLocationReport"), true);

                    case AllocationReportType.PerJobProfile:
                        return DialogHelper.Redirect(this, Url.Action("GenerateActiveInactiveEmployeesPerJobProfileReport"), true);

                    default:
                        throw new InvalidDataException();
                }
            }

            var dialogModel = new ModelBasedDialogViewModel<TimeColumnReportParamViewModel>(model)
            {
                Title = ReportResources.ReportActiveInactiveEmployeesText
            };

            return PartialView("ActiveInactiveEmployeesReport", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateNewHiredLeavingEmployeesReport)]
        public ActionResult GenerateNewHiredLeavingEmployeesReport(TimeColumnReportParamViewModel model)
        {
            if (ModelState.IsValid)
            {
                switch (model.ReportType)
                {
                    case AllocationReportType.PerOrgUnit:
                        return DialogHelper.Redirect(this, Url.Action("GenerateNewHiredLeavingEmployeesPerOrgUnitReport"), true);

                    case AllocationReportType.PerLocation:
                        return DialogHelper.Redirect(this, Url.Action("GenerateNewHiredLeavingEmployeesPerLocationReport"), true);

                    case AllocationReportType.PerJobProfile:
                        return DialogHelper.Redirect(this, Url.Action("GenerateNewHiredLeavingEmployeesPerJobProfileReport"), true);

                    default:
                        throw new InvalidDataException();
                }
            }

            var dialogModel = new ModelBasedDialogViewModel<TimeColumnReportParamViewModel>(model)
            {
                Title = ReportResources.ReportEmployeesByNewHiredLeavingEmployeesText,
            };

            return PartialView("NewHiredLeavingEmployeesReport", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanGenerateAllocationPerProjectReport)]
        public ActionResult GenerateAllocationPerProjectReport(AllocationPerProjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                return DialogHelper.Redirect(this, Url.Action("GenerateAllocationPerProjectInDaysReport", model));
            }

            var dialogModel = new ModelBasedDialogViewModel<AllocationPerProjectViewModel>(model)
            {
                Title = ReportResources.ReportAllocationsPerProjectsInDaysText
            };

            return PartialView("AllocationPerProjectInDaysReport", dialogModel);
        }

        public FilePathResult GenerateSeniorityLevelsPerOrgUnitReport(SeniorityLevelsReportParamsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var seniorityLevelsReportParameters = new SeniorityLevelsReportParameters(model.Filter, model.JobMatrix);

            using (var memoryStream = _seniorityLevelsPerOrgUnitReportService.GenerateReport(seniorityLevelsReportParameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.EmployeesBySeniorityLevelsReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateSeniorityLevelsPerLocationReport(SeniorityLevelsReportParamsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var seniorityLevelsReportParameters = new SeniorityLevelsReportParameters(model.Filter, model.JobMatrix);

            using (var memoryStream = _seniorityLevelsPerLocationReportService.GenerateReport(seniorityLevelsReportParameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.EmployeesBySeniorityLevelsReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateSeniorityLevelsPerJobProfileReport(SeniorityLevelsReportParamsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var seniorityLevelsReportParameters = new SeniorityLevelsReportParameters(model.Filter, model.JobMatrix);

            using (var memoryStream = _seniorityLevelsPerJobProfileReportService.GenerateReport(seniorityLevelsReportParameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.EmployeesBySeniorityLevelsReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateUtilizationStatusPerOrgUnitReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
            };

            using (var memoryStream = _utilizationStatusPerOrgUnitReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.EmployeesByUtilizationStatusReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateUtilizationStatusPerLocationReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
            };

            using (var memoryStream = _utilizationStatusPerLocationReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.EmployeesByUtilizationStatusReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateUtilizationStatusPerJobProfileReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
            };

            using (var memoryStream = _utilizationStatusPerJobProfileReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.EmployeesByUtilizationStatusReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateActiveInactiveEmployeesPerOrgUnitReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
                TimeColumnReportType = TimeColumnReportType.ActiveInactiveEmployee
            };

            using (var memoryStream = _timeColumnPerOrgUnitReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.ActiveInactiveEmployeesReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateActiveInactiveEmployeesPerLocationReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
                TimeColumnReportType = TimeColumnReportType.ActiveInactiveEmployee
            };

            using (var memoryStream = _timeColumnPerLocationReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.ActiveInactiveEmployeesReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateActiveInactiveEmployeesPerJobProfileReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
                TimeColumnReportType = TimeColumnReportType.ActiveInactiveEmployee
            };

            using (var memoryStream = _timeColumnPerJobProfileReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.ActiveInactiveEmployeesReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateNewHiredLeavingEmployeesPerOrgUnitReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
                TimeColumnReportType = TimeColumnReportType.NewHiredLeavingEmployee
            };

            using (var memoryStream = _timeColumnPerOrgUnitReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.NewHiredLeavingEmployeesReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateNewHiredLeavingEmployeesPerLocationReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
                TimeColumnReportType = TimeColumnReportType.NewHiredLeavingEmployee
            };

            using (var memoryStream = _timeColumnPerLocationReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.NewHiredLeavingEmployeesReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        public FilePathResult GenerateNewHiredLeavingEmployeesPerJobProfileReport(TimeColumnReportParamViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new TimeColumnReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ReportScope = reportParamsViewModel.Filter,
                TimeColumnReportType = TimeColumnReportType.NewHiredLeavingEmployee
            };

            using (var memoryStream = _timeColumnPerJobProfileReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.NewHiredLeavingEmployeesReportFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }

        [AtomicAuthorize(SecurityRoleType.CanGenerateAllocationPerProjectReport)]
        public FilePathResult GenerateAllocationPerProjectInDaysReport(AllocationPerProjectViewModel reportParamsViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidDataException();
            }

            var parameters = new AllocationPerProjectReportParameters
            {
                FromDate = reportParamsViewModel.FromDate,
                ToDate = reportParamsViewModel.ToDate,
                ProjectFilterType = reportParamsViewModel.Filter,
                SkipAbsenceDays = reportParamsViewModel.SkipAbsenceDays
            };

            using (var memoryStream = _allocationsPerProjectReportService.GenerateReport(parameters))
            {
                var fileDownloadName = string.Format(ExcelFileNameFormat, ReportResources.ReportAllocationsPerProjectsInDaysFileName, _timeService.GetCurrentTime());
                string tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

                StreamHelper.StreamToFile(memoryStream, tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName = fileDownloadName
                };

                return result;
            }
        }
    }
}