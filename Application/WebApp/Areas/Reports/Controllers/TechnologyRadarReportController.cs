﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Reports.Helpers;
using Smt.Atomic.WebApp.Areas.Reports.Models;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Reports.Controllers
{
    [Authorize]
    public class TechnologyRadarReportController : AtomicController
    {
        private const int PositionLeft = 45;
        private const int PositionRight = 1180;
        private const int PositionTop = 188;
        private const int PositionBottom = 535;
        private const string PlatformsColor = "#587486";
        private const string PatternsAndPracticesColor = "#8FA227";
        private const string LanguagesAndFrameworksColor = "#DC6F1D";
        private const string ToolingColor = "#B70062";

        private readonly ITechnologyRadarReportService _technologyRadarReportService;

        private readonly IClassMapping<TechnologyRadarItemDto, TechnologyRadarItemViewModel> _radarItemDtoToRadarItemViewModelMapping;

        public TechnologyRadarReportController(IBaseControllerDependencies baseDependencies, ITechnologyRadarReportService technologyRadarReportService) : base(baseDependencies)
        {
            _radarItemDtoToRadarItemViewModelMapping = baseDependencies.ClassMappingFactory.CreateMapping<TechnologyRadarItemDto, TechnologyRadarItemViewModel>();
            _technologyRadarReportService = technologyRadarReportService;
        }

        [AtomicAuthorize(SecurityRoleType.CanViewTechnologyRadarReport)]
        public ActionResult Index()
        {
            Layout.PageTitle = ReportResources.TechnologyRadarReport;
            Layout.PageHeader = ReportResources.TechnologyRadarReport;
            Layout.Scripts.Add("~/Areas/Reports/Scripts/utils.js");
            Layout.Scripts.Add("~/Areas/Reports/Scripts/radar.js");
            Layout.Css.Add("~/Areas/Reports/Content/radar.css");

            var radarAreasDto = _technologyRadarReportService.GetTechnologyRadarAreas().ToList();
            var radarAreas = MapAreas(radarAreasDto);
            TechnologyRadarItemsCoordinatesCalculator.CalculateRadarItemsCoordinates(radarAreas.SelectMany(area => area.AreaItems).ToList());

            var technologyRadarReportViewModel = new TechnologyRadarReportViewModel
            {
                TechnologyRadarAreas = new TechnologyRadarAreasCollectionViewModel
                {
                    Areas = radarAreas
                },
                TechnologyRadarLabels = new TechnologyRadarLabelsViewModel()
            };
            
            return View(technologyRadarReportViewModel);
        }

        private IList<TechnologyRadarAreaViewModel> MapAreas(IList<TechnologyRadarAreaDto> radarAreaDto)
        {
            return new List<TechnologyRadarAreaViewModel>
            {
                new TechnologyRadarAreaViewModel
                {
                    TechnologyRadarArea = TechnologyRadarArea.Platforms,
                    PositionLeft = PositionRight,
                    PositionTop = PositionTop,
                    Color = PlatformsColor,
                    AreaItems = radarAreaDto.First(i => i.TechnologyRadarArea == TechnologyRadarArea.Platforms).AreaItems.Select(TechnologyRadarItemDtoToTechnologyRadarItemViewModel).ToList()
                },
                new TechnologyRadarAreaViewModel
                {
                    TechnologyRadarArea = TechnologyRadarArea.PatternsAndPractices,
                    PositionLeft = PositionLeft,
                    PositionTop = PositionTop,
                    Color = PatternsAndPracticesColor,
                    AreaItems = radarAreaDto.First(i => i.TechnologyRadarArea == TechnologyRadarArea.PatternsAndPractices).AreaItems.Select(TechnologyRadarItemDtoToTechnologyRadarItemViewModel).ToList()
                },
                new TechnologyRadarAreaViewModel
                {
                    TechnologyRadarArea = TechnologyRadarArea.LanguagesAndFrameworks,
                    PositionLeft = PositionLeft,
                    PositionTop = PositionBottom,
                    Color = LanguagesAndFrameworksColor,
                    AreaItems = radarAreaDto.First(i => i.TechnologyRadarArea == TechnologyRadarArea.LanguagesAndFrameworks).AreaItems.Select(TechnologyRadarItemDtoToTechnologyRadarItemViewModel).ToList()
                },
                new TechnologyRadarAreaViewModel
                {
                    TechnologyRadarArea = TechnologyRadarArea.Tools,
                    PositionLeft = PositionRight,
                    PositionTop = PositionBottom,
                    Color = ToolingColor,
                    AreaItems = radarAreaDto.First(i => i.TechnologyRadarArea == TechnologyRadarArea.Tools).AreaItems.Select(TechnologyRadarItemDtoToTechnologyRadarItemViewModel).ToList()
                }
            };
        }

        private TechnologyRadarItemViewModel TechnologyRadarItemDtoToTechnologyRadarItemViewModel(TechnologyRadarItemDto item)
        {
            return _radarItemDtoToRadarItemViewModelMapping.CreateFromSource(item);
        }
    }
}