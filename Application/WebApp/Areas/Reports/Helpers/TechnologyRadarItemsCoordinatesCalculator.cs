﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.WebApp.Areas.Reports.Models;

namespace Smt.Atomic.WebApp.Areas.Reports.Helpers
{
    public static class TechnologyRadarItemsCoordinatesCalculator
    {
        private static readonly IList<int> CirclesOrder = new List<int> { 2, 0, 4, 1, 3 };
        private const int RightAngle = 90;
        private const int GroupRadius = 100;
        private const int DistanceFromStart = 15;
        private const int RowsInGroup = 5;
        private const int RadarItemSize = 17;

        public static void CalculateRadarItemsCoordinates(ICollection<TechnologyRadarItemViewModel> technologyRadarItems)
        {
            foreach (TechnologyRadarArea area in Enum.GetValues(typeof(TechnologyRadarArea)))
            {
                CalculateCoordinatesForArea(technologyRadarItems.Where(r => r.TechnologyRadarArea == area).ToList());
            }
        }

        private static void CalculateCoordinatesForArea(IList<TechnologyRadarItemViewModel> areaTechnologyRadarItems)
        {
            foreach (TechnologyRadarStatus radarStatus in Enum.GetValues(typeof(TechnologyRadarStatus)))
            {
                CalculateCoordinatesForStatus(radarStatus, areaTechnologyRadarItems.Where(r => r.Status == radarStatus).ToList());
            }
        }

        private static void CalculateCoordinatesForStatus(TechnologyRadarStatus technologyRadarStatus, List<TechnologyRadarItemViewModel> statusTechnologyRadarItems)
        {
            var circleSizes = CreateCircleSizes(technologyRadarStatus).ToArray();

            var numberOfCircles = CalculateNumberOfCircles(statusTechnologyRadarItems.Count, circleSizes);

            var usedItems = 0;

            for (var i = 0; i < numberOfCircles; i++)
            {
                var itemsInCircle = GetItemsForCircle(circleSizes[i], statusTechnologyRadarItems, usedItems);
                CalculateCoordinatesOfCircle(itemsInCircle, CirclesOrder[i]);
                usedItems += itemsInCircle.Count;
            }
        }

        private static List<TechnologyRadarItemViewModel> GetItemsForCircle(int circleSize, List<TechnologyRadarItemViewModel> areaTechnologyRadarItems, int usedItems)
        {
            var itemsCount = Math.Min(circleSize, areaTechnologyRadarItems.Count);

            return areaTechnologyRadarItems.Skip(usedItems).Take(itemsCount).ToList();
        }

        private static IEnumerable<int> CreateCircleSizes(TechnologyRadarStatus technologyRadarStatus)
        {
            var radarIndex = (int)technologyRadarStatus;

            foreach (var circleIndex in CirclesOrder)
            {
                if (radarIndex == 0 && circleIndex == 0)
                {
                    yield return 1;
                }
                else
                {
                    yield return radarIndex*RowsInGroup + circleIndex*2 - 1;
                }
            }
        }

        private static int CalculateNumberOfCircles(int statusTechnologyRadarItemsCount, IList<int> circleSizes)
        {
            if (circleSizes == null)
            {
                throw new ArgumentNullException(nameof(circleSizes));
            }

            var circlesCapacity = 0;
            var circleIndex = 0;

            while (statusTechnologyRadarItemsCount > circlesCapacity)
            {
                circlesCapacity += circleSizes[circleIndex++];
            }

            return circleIndex;
        }

        private static void CalculateCoordinatesOfCircle(IReadOnlyList<TechnologyRadarItemViewModel> radarItemsInCircle, int circleIndex)
        {
            for (var i = 0; i < radarItemsInCircle.Count; i++)
            {
                var radarItem = radarItemsInCircle[i];
                var radius = GetItemRadius((int)radarItem.Status, circleIndex);
                var theta = GetItemTheta(radarItemsInCircle.Count, (int)radarItem.TechnologyRadarArea, i);

                radarItem.CoordinatesViewModel = new CoordinatesViewModel
                {
                    Radius = radius,
                    Theta = theta
                };
            }
        }

        private static int GetItemRadius(int itemStatusNumber, int circleIndex)
        {
            return itemStatusNumber * GroupRadius + DistanceFromStart + circleIndex * RadarItemSize;
        }

        private static int GetItemTheta(int radarItemsInCircle, int itemAreaNumber, int itemIndex)
        {
            var angleDifference = RightAngle / radarItemsInCircle;
            var angleFromStart = angleDifference / 2;

            return itemAreaNumber * RightAngle + angleFromStart + itemIndex * angleDifference;
        }
    }
}