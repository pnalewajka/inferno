﻿using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserViewModelToUserDtoMapping : ClassMapping<UserViewModel, UserDto>
    {
        public UserViewModelToUserDtoMapping()
        {
            Mapping = m => new UserDto
            {
                Id = m.Id,
                ModifiedOn = m.ModifiedOn,
                LastLoggedOn = m.LastLoggedOn,
                Login = m.Login,
                FirstName = m.FirstName,
                LastName = m.LastName,
                IsActive = m.IsActive,
                Timestamp = m.Timestamp,
                Email = m.Email,
                    Documents = m.Documents.Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId
                    }).ToList(),
                    Picture = m.Picture == null ? null : new DocumentDto
                    {
                        ContentType = m.Picture.ContentType,
                        DocumentName = m.Picture.DocumentName,
                        DocumentId = m.Picture.DocumentId,
                        TemporaryDocumentId = m.Picture.TemporaryDocumentId
                    },
                Location = m.Location == null
                    ? null
                    : new GeographyPointDto
                    {
                        Latitude = (double)m.Location.Value.Latitude,
                        Longtitude = (double)m.Location.Value.Longitude,
                    },
                LocationRadius = !m.Location.HasValue ? (int?)null : m.Location.Value.Radius
            };
        }
    }
}