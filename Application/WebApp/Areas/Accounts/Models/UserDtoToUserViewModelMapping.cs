﻿using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserDtoToUserViewModelMapping : ClassMapping<UserDto, UserViewModel>
    {
        public UserDtoToUserViewModelMapping()
        {
            Mapping = e => new UserViewModel
            {
                Id = e.Id,
                ModifiedOn = e.ModifiedOn,
                LastLoggedOn = e.LastLoggedOn,
                Login = e.Login,
                FirstName = e.FirstName,
                LastName = e.LastName,
                IsActive = e.IsActive,
                Timestamp = e.Timestamp,
                Email = e.Email,
                Documents =
                    e.Documents.Select(
                        p => new DocumentViewModel
                        {
                            ContentType = p.ContentType,
                            DocumentName = p.DocumentName,
                            DocumentId = p.DocumentId
                        }).ToList(),
                Picture = e.Picture == null
                    ? null
                    : new DocumentViewModel
                    {
                        ContentType = e.Picture.ContentType,
                        DocumentName = e.Picture.DocumentName,
                        DocumentId = e.Picture.DocumentId
                    },
                Location =
                    e.Location == null
                        ? (GeographicLocationViewModel?) null
                        : new GeographicLocationViewModel
                        {
                            Latitude = (decimal)e.Location.Latitude,
                            Longitude = (decimal)e.Location.Longtitude,
                            Radius = e.LocationRadius ?? 0,
                        },

            };
        }
    }
}