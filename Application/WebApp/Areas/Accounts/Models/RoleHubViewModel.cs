﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("Hubs.Role")]
    public class RoleHubViewModel
        : SecurityRoleViewModel
    {
        public class RoleUsage
        {
            public class Comparer : EqualityComparer<RoleUsage>
            {
                public override bool Equals(RoleUsage x, RoleUsage y)
                {
                    return x.Area == y.Area
                        && x.Controller == y.Controller
                        && x.Action == y.Action;
                }

                public override int GetHashCode(RoleUsage obj) => obj.GetHashCode();
            }

            public string Area { get; set; }

            public string Controller { get; set; }

            public string Action { get; set; }
        }

        public ICollection<RoleUsage> Usages { get; set; }

        public RoleHubViewModel(
            SecurityRoleViewModel baseViewModel)
        {
            Id = baseViewModel.Id;
            Name = baseViewModel.Name;
            Description = baseViewModel.Description;
        }
    }
}