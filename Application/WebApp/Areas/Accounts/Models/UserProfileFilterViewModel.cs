﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("Filters.UserProfileFilter")]
    [DescriptionLocalized("SecurityProfileNameLabel", typeof(UserProfileResources))]
    public class UserProfileFilterViewModel
    {
        [ValuePicker(Type = typeof(ProfilePickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [DisplayNameLocalized(nameof(UserProfileResources.SecurityProfileNameLabel), typeof(UserProfileResources))]
        public long ProfileId { get; set; }

        public override string ToString()
        {
            return UserProfileResources.SecurityProfileNameLabel;
        }
    }
}