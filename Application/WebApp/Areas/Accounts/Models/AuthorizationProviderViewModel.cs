﻿namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class AuthorizationProviderViewModel
    {
        public string Name { get; set; }
        public string Identifier { get; set; }
        public string IconClass { get; set; }
    }
}