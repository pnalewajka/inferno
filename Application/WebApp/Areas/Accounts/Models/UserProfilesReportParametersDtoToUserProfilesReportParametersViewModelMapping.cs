﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserProfilesReportParametersDtoToUserProfilesReportParametersViewModelMapping : ClassMapping<UserProfilesReportParametersDto, UserProfilesReportParametersViewModel>
    {
        public UserProfilesReportParametersDtoToUserProfilesReportParametersViewModelMapping()
        {
            Mapping = d => new UserProfilesReportParametersViewModel
            {
                UserIds = d.UserIds,
                ProfileIds = d.ProfileIds
            };
        }
    }
}
