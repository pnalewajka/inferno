﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserRolesReportParametersViewModelToUserRolesReportParametersDtoMapping : ClassMapping<UserRolesReportParametersViewModel, UserRolesReportParametersDto>
    {
        public UserRolesReportParametersViewModelToUserRolesReportParametersDtoMapping()
        {
            Mapping = v => new UserRolesReportParametersDto
            {
                UserIds = v.UserIds,
                RoleIds = v.RoleIds,
            };
        }
    }
}
