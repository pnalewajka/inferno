﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserLastLoggedOnBetweenDatesFilterViewModelToUserLastLoggedOnBetweenDatesFilterMapping : ClassMapping<UserLastLoggedOnBetweenDatesFilterViewModel, UserLastLoggedOnBetweenDatesFilterDto>
    {
        public UserLastLoggedOnBetweenDatesFilterViewModelToUserLastLoggedOnBetweenDatesFilterMapping()
        {
            Mapping = m => new UserLastLoggedOnBetweenDatesFilterDto
            {
                From = m.From,
                To = m.To
            };
        }
    }
}