﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("ViewModel.UserRolesReportParameters")]
    public class UserRolesReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(SecurityPermissionsReportResources.UserRolesReportParametersUserIdsLabel), typeof(SecurityPermissionsReportResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long[] UserIds { get; set; }

        [DisplayNameLocalized(nameof(SecurityPermissionsReportResources.UserRolesReportParametersRoleIdsLabel), typeof(SecurityPermissionsReportResources))]
        [ValuePicker(Type = typeof(RolePickerController), DialogWidth = "1000px")]
        public long[] RoleIds { get; set; }
    }
}