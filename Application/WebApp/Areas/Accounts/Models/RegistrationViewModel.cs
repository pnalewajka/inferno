﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class RegistrationViewModel
    {
        [Required]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserFirstNameLabel), typeof(RegistrationResources))]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserLastNameLabel), typeof(RegistrationResources))]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserEmailLabel), typeof(RegistrationResources))]
        public string Email { get; set; }

        [Required]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserLoginLabel), typeof(RegistrationResources))]
        public string Login { get; set; }
    }
}