﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SecurityRoleViewModel
    {
        [ValuePicker(Type = typeof(RolePickerController), DialogWidth = "1000px")]
        public long Id { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(UserRoleResources.SecurityRoleNameLabel), typeof(UserRoleResources))]
        [Hub(nameof(Id))]
        public string Name { get; set; }

        [Ellipsis(MaxStringLength = 50)]
        [DisplayNameLocalized(nameof(UserRoleResources.SecurityRoleDescriptionLabel), typeof(UserRoleResources))]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(UserRoleResources.SecurityRoleModuleLabel), typeof(UserRoleResources))]
        public string Module { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}