﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SecurityRoleDtoToSecurityRoleViewModelMapping : ClassMapping<SecurityRoleDto, SecurityRoleViewModel>
    {
        public SecurityRoleDtoToSecurityRoleViewModelMapping()
        {
            Mapping = d => new SecurityRoleViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Module = d.Module
            };
        }
    }
}
