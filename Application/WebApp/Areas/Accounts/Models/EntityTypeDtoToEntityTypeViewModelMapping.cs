﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class EntityTypeDtoToEntityTypeViewModelMapping : ClassMapping<EntityTypeDto, EntityTypeViewModel>
    {
        public EntityTypeDtoToEntityTypeViewModelMapping()
        {
            Mapping = e => new EntityTypeViewModel
            {
                Id = e.Id,
                Module = e.Module,
                TypeName = e.Type
            };
        }
    }
}