﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SetPasswordViewModel : IValidatableObject
    {
        [Required]
        [HtmlAttribute("autofocus", "true")]
        [DataType(DataType.Password)]
        [DisplayNameLocalized(nameof(AuthenticationResources.NewPasswordModelNewPasswordLabel), typeof(AuthenticationResources))]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayNameLocalized(nameof(AuthenticationResources.NewPasswordModelReEnteredPasswordLabel), typeof(AuthenticationResources))]
        public string ReEnteredPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (NewPassword != ReEnteredPassword)
            {
                yield return new PropertyValidationResult(AuthenticationResources.PasswordsAreNotEqual, () => ReEnteredPassword);
            }
        }
    }
}