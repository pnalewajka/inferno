﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SecurityProfileDtoToSecurityProfileViewModelMapping : ClassMapping<SecurityProfileDto, SecurityProfileViewModel>
    {
        public SecurityProfileDtoToSecurityProfileViewModelMapping()
        {
            Mapping = d => new SecurityProfileViewModel
            {
                Id = d.Id,
                Name = d.Name,
                ActiveDirectoryGroupName = d.ActiveDirectoryGroupName,
                RoleIds = d.RoleIds
            };
        }
    }
}
