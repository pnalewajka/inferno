﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class ChoreViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(ChoreResources.ChoreTypeColumnName), typeof(ChoreResources))]
        public ChoreType Type { get; set; }

        [DisplayNameLocalized(nameof(ChoreResources.ChoreDueDateColumnName), typeof(ChoreResources))]
        public DateTime? DueDate { get; set; }

        [NonSortable]
        public string Description { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.None)]
        public IList<CommandButton> Actions { get; set; }

        public ChoreViewModel()
        {
            Actions = new List<CommandButton>();
        }
    }
}