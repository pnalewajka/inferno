﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class ChangePasswordViewModel : IValidatableObject
    {
        [Required]
        [HtmlAttribute("autofocus", "true")]
        [DataType(DataType.Password)]
        [DisplayNameLocalized(nameof(AuthenticationResources.ChangePasswordModelOldPasswordLabel), typeof(AuthenticationResources))]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayNameLocalized(nameof(AuthenticationResources.ChangePasswordModelNewPasswordLabel), typeof(AuthenticationResources))]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayNameLocalized(nameof(AuthenticationResources.ChangePasswordModelReEnteredPasswordLabel), typeof(AuthenticationResources))]
        public string ReEnteredPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (NewPassword != ReEnteredPassword)
            {
                yield return new PropertyValidationResult(AuthenticationResources.PasswordsAreNotEqual, () => ReEnteredPassword);
            }

            if (OldPassword == NewPassword)
            {
                yield return new PropertyValidationResult(AuthenticationResources.PasswordIsNotDifferent, () => NewPassword);
            }
        }
    }
}