﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class ResendEmailViewModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserEmailLabel), typeof(RegistrationResources))]
        public string Email { get; set; }
    }
}