﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SecurityRoleViewModelToSecurityRoleDtoMapping : ClassMapping<SecurityRoleViewModel, SecurityRoleDto>
    {
        public SecurityRoleViewModelToSecurityRoleDtoMapping()
        {
            Mapping = v => new SecurityRoleDto
            {
                Id = v.Id,
                Name = v.Name,
                Description =  v.Description
            };
        }
    }
}
