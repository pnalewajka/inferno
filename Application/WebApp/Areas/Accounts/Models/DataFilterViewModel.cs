﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("Models.DataFilterViewModel")]
    public class DataFilterViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? UserId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ProfileId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(DataFilterResources.EntityLabelShort), typeof(DataFilterResources))]
        [ValuePicker(Type = typeof(EntityTypePickerController))]
        [Render(Size = Size.Large)]
        public long EntityTypeId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataFilterResources.EntityLabel), typeof(DataFilterResources))]
        [StringLength(100)]
        public string Entity { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataFilterResources.ModuleLabel), typeof(DataFilterResources))]
        [StringLength(300)]
        public string Module { get; set; }

        [DisplayNameLocalized(nameof(DataFilterResources.ConditionLabel), typeof(DataFilterResources))]
        [StringLength(2048)]
        [Multiline(5)]
        public string Condition { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
