﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class EntityTypeViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(DataFilterResources.TypeNameLabel), typeof(DataFilterResources))]
        public string TypeName { get; set; }

        [DisplayNameLocalized(nameof(DataFilterResources.ModuleLabel), typeof(DataFilterResources))]
        public string Module { get; set; }

        public override string ToString()
        {
            return $"{TypeName} ({Module})";
        }
    }
}