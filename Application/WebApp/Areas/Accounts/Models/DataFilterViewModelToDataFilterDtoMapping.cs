﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class DataFilterViewModelToDataFilterDtoMapping : ClassMapping<DataFilterViewModel, DataFilterDto>
    {
        public DataFilterViewModelToDataFilterDtoMapping(IEntityTypeCardIndexService entityTypeCardIndexService)
        {
            Mapping = v => new DataFilterDto
            {
                Id = v.Id,
                UserId = v.UserId,
                ProfileId = v.ProfileId,
                Entity = entityTypeCardIndexService.GetEntityTypeName(v.EntityTypeId) ?? v.Entity,
                Module = entityTypeCardIndexService.GetEntityModule(v.EntityTypeId) ?? v.Module, 
                Condition = v.Condition,
                Timestamp = v.Timestamp,
            };
        }
    }
}
