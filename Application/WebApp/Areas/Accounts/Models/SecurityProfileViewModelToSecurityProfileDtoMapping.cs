﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SecurityProfileViewModelToSecurityProfileDtoMapping : ClassMapping<SecurityProfileViewModel, SecurityProfileDto>
    {
        public SecurityProfileViewModelToSecurityProfileDtoMapping()
        {
            Mapping = v => new SecurityProfileDto
            {
                Id = v.Id,
                Name = v.Name,
                ActiveDirectoryGroupName = v.ActiveDirectoryGroupName,
                RoleIds = v.RoleIds
            };
        }
    }
}
