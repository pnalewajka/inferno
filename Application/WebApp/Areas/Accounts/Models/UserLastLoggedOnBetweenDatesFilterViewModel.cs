﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("Filters.UserLastLoggedOnBetweenDates")]
    [DescriptionLocalized("UserLastLoggedOnBetweenDatesFilterDialogTitle", typeof(UserResources))]
    public class UserLastLoggedOnBetweenDatesFilterViewModel : IValidatableObject
    {
        [UrlFieldName("from")]
        [DisplayNameLocalized(nameof(UserResources.UserLastLoggedOnBetweenDatesFilterFrom), typeof(UserResources))]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? From { get; set; }

        [UrlFieldName("to")]
        [DisplayNameLocalized(nameof(UserResources.UserLastLoggedOnBetweenDatesFilterTo), typeof(UserResources))]
        [Required]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? To { get; set; }

        public override string ToString()
        {
            return string.Format(UserResources.UserLastLoggedOnBetweenDatesFilterSelectedNameFormat, From, To);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (From.HasValue && To.HasValue && From.Value >= To.Value)
            {
                yield return new ValidationResult(UserResources.FromDateShouldBeLessThanToDate);
            }
        }
    }
}
