﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("Filters.UserRoleFilter")]
    [DescriptionLocalized("SecurityProfileRolesLabel", typeof(UserProfileResources))]
    public class UserRoleFilterViewModel
    {
        [ValuePicker(Type = typeof(RolePickerController), DialogWidth = "1000px", OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [DisplayNameLocalized(nameof(UserProfileResources.SecurityProfileRolesLabel), typeof(UserProfileResources))]
        public long RoleId { get; set; }

        public override string ToString()
        {
            return UserProfileResources.SecurityProfileRolesLabel;
        }
    }
}