﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Accounts.DocumentMappings;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Attributes.CellContent;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("Models.UserViewModel")]
    public class UserViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserLoginLabel), typeof(AuthenticationResources))]
        [StringLength(255)]
        public string Login { get; set; }

        [Order(0)]
        [Required]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserFirstNameLabel), typeof(AuthenticationResources))]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserLastNameLabel), typeof(AuthenticationResources))]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [Order(3)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserEmailLabel), typeof(AuthenticationResources))]
        [StringLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserModifiedOnLabel), typeof(AuthenticationResources))]
        public DateTime ModifiedOn { get; set; }

        [Order(5)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserIsActiveLabel), typeof(AuthenticationResources))]
        [NonSortable]
        [IconizedIsActive]
        public bool IsActive { get; set; }

        public byte[] Timestamp { get; set; }

        [Order(7)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserDocumentsLabel), typeof(AuthenticationResources))]
        [DocumentUpload(typeof(UserDocumentMapping))]
        public List<DocumentViewModel> Documents { get; set; }

        [Order(8)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserPictureLabel), typeof(AuthenticationResources))]
        [Visibility(VisibilityScope.Form)]
        [CropImageUpload(typeof(UserPictureMapping), 10, 100, 10, 100, true)]
        public DocumentViewModel Picture { get; set; }

        [Order(9)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserLastLoggedOnLabel), typeof(AuthenticationResources))]
        public DateTime? LastLoggedOn { get; set; }

        [MapPicker(Height = 300)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AuthenticationResources.UserLocationLabel), typeof(AuthenticationResources))]
        public GeographicLocationViewModel? Location { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeEdited { get { return Login?.ToLowerInvariant() != "administrator"; } }

        [Visibility(VisibilityScope.None)]
        public bool CanBeDeleted { get { return Login?.ToLowerInvariant() != "administrator"; } }

        public UserViewModel()
        {
            Documents = new List<DocumentViewModel>();
        }

        public string GetFullName()
        {
            return $"{FirstName} {LastName}".Trim();
        }

        public override string ToString()
        {
            return GetFullName();
        }
    }
}