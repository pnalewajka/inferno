﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class RegistrationFromAuthorizationProviderViewModel
    {
        [Required]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserFirstNameLabel), typeof(RegistrationResources))]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(255)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserLastNameLabel), typeof(RegistrationResources))]
        public string LastName { get; set; }

        [Required]
        [MaxLength(255)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RegistrationResources.UserEmailLabel), typeof(RegistrationResources))]
        public string Email { get; set; }

        [HiddenInput]
        public string Identifier { get; set; }
    }
}