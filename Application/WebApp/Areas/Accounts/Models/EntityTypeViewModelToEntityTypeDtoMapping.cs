using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class EntityTypeViewModelToEntityTypeDtoMapping : ClassMapping<EntityTypeViewModel, EntityTypeDto>
    {
        public EntityTypeViewModelToEntityTypeDtoMapping()
        {
            Mapping = m => new EntityTypeDto
            {
                Id = m.Id,
                Module = m.Module,
                Type = m.TypeName
            };
        }
    }
}