﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class LoginViewModel
    {
        [Required]
        [HtmlAttribute("autofocus", "true")]
        [DisplayNameLocalized(nameof(AuthenticationResources.LoginModelLoginLabel), typeof(AuthenticationResources))]
        public string Login { get; set; }

        [Required]
        [AllowHtml]
        [DataType(DataType.Password)]
        [DisplayNameLocalized(nameof(AuthenticationResources.LoginModelPasswordLabel), typeof(AuthenticationResources))]
        public string Password { get; set; }

        [DisplayNameLocalized(nameof(AuthenticationResources.LoginModelRememberMeLabel), typeof(AuthenticationResources))]
        public bool RememberMe { get; set; }

        [Visibility(VisibilityScope.None)]
        public List<AuthorizationProviderViewModel> AuthorizationProviders{ get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsPassiveNtlm { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool SkipNtlm { get; set; }

        [HiddenInput]
        public int FailedAttempts { get; set; }
    }
}
