﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserProfilesReportParametersViewModelToUserProfilesReportParametersDtoMapping : ClassMapping<UserProfilesReportParametersViewModel, UserProfilesReportParametersDto>
    {
        public UserProfilesReportParametersViewModelToUserProfilesReportParametersDtoMapping()
        {
            Mapping = v => new UserProfilesReportParametersDto
            {
                UserIds = v.UserIds,
                ProfileIds = v.ProfileIds
            };
        }
    }
}
