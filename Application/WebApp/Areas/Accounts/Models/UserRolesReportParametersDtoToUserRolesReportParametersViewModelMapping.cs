﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class UserRolesReportParametersDtoToUserRolesReportParametersViewModelMapping : ClassMapping<UserRolesReportParametersDto, UserRolesReportParametersViewModel>
    {
        public UserRolesReportParametersDtoToUserRolesReportParametersViewModelMapping()
        {
            Mapping = d => new UserRolesReportParametersViewModel
            {
                UserIds = d.UserIds,
                RoleIds = d.RoleIds,
            };
        }
    }
}
