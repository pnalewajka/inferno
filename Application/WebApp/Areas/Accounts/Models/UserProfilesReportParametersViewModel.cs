﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    [Identifier("ViewModel.UserProfilesReportParameters")]
    public class UserProfilesReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(SecurityPermissionsReportResources.UserProfilesReportParametersUserIdsLabel), typeof(SecurityPermissionsReportResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long[] UserIds { get; set; }

        [DisplayNameLocalized(nameof(SecurityPermissionsReportResources.UserProfilesReportParametersProfileIdsLabel), typeof(SecurityPermissionsReportResources))]
        [ValuePicker(Type = typeof(ProfilePickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        public long[] ProfileIds { get; set; }
    }
}
