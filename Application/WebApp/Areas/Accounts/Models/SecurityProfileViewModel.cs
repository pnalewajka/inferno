﻿using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class SecurityProfileViewModel
    {
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(UserProfileResources.SecurityProfileNameLabel), typeof(UserProfileResources))]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(UserProfileResources.ActiveDirectoryGroupNameLabel), typeof(UserProfileResources))]
        public string ActiveDirectoryGroupName { get; set; }

        [CannotBeEmpty]
        [ValuePicker(Type = typeof(RolePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(UserProfileResources.SecurityProfileRolesLabel), typeof(UserProfileResources))]
        [NonSortable]
        public long[] RoleIds { get; set; }

        public SecurityProfileViewModel()
        {
            RoleIds = new long[]{};
        }

        public override string ToString()
        {
            return Name;
        }
    }
}