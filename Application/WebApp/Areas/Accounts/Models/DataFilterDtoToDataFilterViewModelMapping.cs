﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Models
{
    public class DataFilterDtoToDataFilterViewModelMapping : ClassMapping<DataFilterDto, DataFilterViewModel>
    {
        public DataFilterDtoToDataFilterViewModelMapping(IEntityTypeCardIndexService entityTypeCardIndexService)
        {
            Mapping = d => new DataFilterViewModel
            {
                Id = d.Id,
                UserId = d.UserId,
                ProfileId = d.ProfileId,
                EntityTypeId = entityTypeCardIndexService.GetEntityTypeId(d.Entity),
                Entity = d.Entity,
                Module = d.Module,
                Condition = d.Condition,
                Timestamp = d.Timestamp,
            };
        }
    }
}
