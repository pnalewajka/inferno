﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Accounts.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DataFilterResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DataFilterResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Accounts.Resources.DataFilterResources", typeof(DataFilterResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Condition.
        /// </summary>
        public static string ConditionLabel {
            get {
                return ResourceManager.GetString("ConditionLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Filters.
        /// </summary>
        public static string DataFiltersBreadcrumItem {
            get {
                return ResourceManager.GetString("DataFiltersBreadcrumItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entity Type.
        /// </summary>
        public static string EntityLabel {
            get {
                return ResourceManager.GetString("EntityLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entity.
        /// </summary>
        public static string EntityLabelShort {
            get {
                return ResourceManager.GetString("EntityLabelShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Module.
        /// </summary>
        public static string ModuleLabel {
            get {
                return ResourceManager.GetString("ModuleLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile Data Filters.
        /// </summary>
        public static string ProfileDataFiltersTitle {
            get {
                return ResourceManager.GetString("ProfileDataFiltersTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type Name.
        /// </summary>
        public static string TypeNameLabel {
            get {
                return ResourceManager.GetString("TypeNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Filters.
        /// </summary>
        public static string UserDataFilters {
            get {
                return ResourceManager.GetString("UserDataFilters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Data Filters.
        /// </summary>
        public static string UserDataFiltersTitle {
            get {
                return ResourceManager.GetString("UserDataFiltersTitle", resourceCulture);
            }
        }
    }
}
