﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class MissingAvatarChoreToViewModelMapping
        : ChoreDtoToChoreViewModelMapping
    {
        private readonly string _setAvatarUrl;

        public MissingAvatarChoreToViewModelMapping(ISystemParameterService systemParameterService)
        {
            _setAvatarUrl = systemParameterService.GetParameter<string>(ParameterKeys.EmployeeChangePictureUrl);
        }

        public override ChoreType ChoreType => ChoreType.UserAvatarMissing;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.MissingAvatarChoreDescription),
                Actions = new List<CommandButton>
                {
                    new CommandButton
                    {
                        Text = ChoreResources.View,
                        ButtonType = CommandButtonType.Button,
                        OnClickAction = new UriAction(_setAvatarUrl, ActionType.Get),
                    },
                }
            };
        }
    }
}