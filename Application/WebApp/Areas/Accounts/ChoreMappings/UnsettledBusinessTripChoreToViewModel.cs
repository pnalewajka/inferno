﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class UnsettledBusinessTripChoreToViewModel
        : ChoreDtoToChoreViewModelMapping
    {
        public override ChoreType ChoreType => ChoreType.UnsettledBusinessTrip;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var btId = long.Parse(dto.Parameters);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.UnsettledBusinessTripChoreDescription, btId),
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.View,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        OnClickAction = new UriAction(
                            $"/BusinessTrips/BusinessTripSettlements/List?parent-id={btId}",
                            ActionType.Get),
                    },
                }
            };
        }
    }
}