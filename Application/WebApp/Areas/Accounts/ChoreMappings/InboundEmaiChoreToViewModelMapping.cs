﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class InboundEmaiChoreToViewModelMapping : ChoreDtoToChoreViewModelMapping
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;

        public InboundEmaiChoreToViewModelMapping(
            IPrincipalProvider principalProvider,
            ISystemParameterService systemParameterService)
        {
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
        }

        public override ChoreType ChoreType => ChoreType.InboundEmails;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var count = long.Parse(dto.Parameters);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.InboundEmaisDescription, count),
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.View,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        OnClickAction = new UriAction(
                            $"/Recruitment/InboundEmail/List?filter={GetFiltersByProfilesAndRoles()}",
                            ActionType.Get),
                    },
                }
            };
        }

        private string GetFiltersByProfilesAndRoles()
        {
            var recruitmentFileClerkInboundProfileName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentFileClerkInboundProfileName);
            var recruitmentFileClerkResearchProfileName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentFileClerkResearchProfileName);

            var isClerkInbound = _principalProvider.Current.Profiles.Contains(recruitmentFileClerkInboundProfileName);
            var isClerkResearch = _principalProvider.Current.Profiles.Contains(recruitmentFileClerkResearchProfileName);

            var filters = new List<string> { FilterCodes.StatusDefault };

            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanFilterInboundEmailByAnyOwner))
            {
                filters.Add(FilterCodes.AnyOwner);
            }

            if (isClerkResearch || isClerkInbound)
            {
                if (isClerkInbound)
                {
                    filters.Add(NamingConventionHelper.ConvertPascalCaseToHyphenated(InboundEmailStatus.New.ToString()));
                }

                if (isClerkResearch)
                {
                    filters.Add(NamingConventionHelper.ConvertPascalCaseToHyphenated(InboundEmailStatus.SentForResearch.ToString()));
                }
            }

            return string.Join(",", filters);
        }
    }
}