﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class FailedBusinessEventsChoreToViewModelMapping
        : ChoreDtoToChoreViewModelMapping
    {
        public override ChoreType ChoreType => ChoreType.FailedBusinessEvents;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var count = long.Parse(dto.Parameters);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.FailedBusinessEventsChoreDescription, count),
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.View,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        OnClickAction = new UriAction(
                            $"/EventSourcing/PublishedEventQueueItem/List?filter=failed",
                            ActionType.Get),
                    },
                }
            };
        }
    }
}