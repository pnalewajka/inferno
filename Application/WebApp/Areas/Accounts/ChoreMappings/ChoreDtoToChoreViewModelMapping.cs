﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.WebApp.Areas.Accounts.Models;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public abstract class ChoreDtoToChoreViewModelMapping
        : ClassMapping<ChoreDto, ChoreViewModel>
    {
        public ChoreDtoToChoreViewModelMapping()
        {
            Mapping = d => PopulateCommonFields(CreateViewModel(d), d);
        }

        public abstract ChoreType ChoreType { get; }

        public abstract ChoreViewModel CreateViewModel(ChoreDto dto);

        private ChoreViewModel PopulateCommonFields(ChoreViewModel viewModel, ChoreDto dto)
        {
            viewModel.Id = dto.Id;
            viewModel.Type = dto.Type;
            viewModel.DueDate = dto.DueDate;

            return viewModel;
        }
    }

    public sealed class ProxyChoreDtoToChoreViewModelMapping
        : ClassMapping<ChoreDto, ChoreViewModel>
    {
        private readonly ChoreDtoToChoreViewModelMapping[] _mappings;

        public ProxyChoreDtoToChoreViewModelMapping(
            ChoreDtoToChoreViewModelMapping[] mappings)
        {
            _mappings = mappings;

            Mapping = d => CreateViewModel(d);
        }

        private ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var mapping = _mappings.Single(m => m.ChoreType == dto.Type);

            return mapping.CreateFromSource(dto);
        }
    }
}