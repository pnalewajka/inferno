﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class PendingRequestChoreDtoToChoreViewModelMapping
        : ChoreDtoToChoreViewModelMapping
    {
        public override ChoreType ChoreType => ChoreType.PendingRequests;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var count = long.Parse(dto.Parameters);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.PendingRequestsChoreDescription, count),
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.PendingRequestsChoreView,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        OnClickAction = new UriAction(
                            $"/Workflows/Request/List?filter=decision-awaiting",
                            ActionType.Get),
                    },
                }
            };
        }
    }
}