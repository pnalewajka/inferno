﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class OwnTimeReportReminderChoreToViewModel
        : ChoreDtoToChoreViewModelMapping
    {
        public override ChoreType ChoreType => ChoreType.OwnTimeReportReminder;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var yearMonth = dto.Parameters.Split('/');
            var year = int.Parse(yearMonth[0]);
            var month = int.Parse(yearMonth[1]);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.OwnTimeReportReminderChoreDescription, month, year),
                DueDate = dto.DueDate,
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.View,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        OnClickAction = new UriAction(
                            $"/TimeTracking/MyTimeReport/MyTimeReport?year={year}&month={month}",
                            ActionType.Get),
                    },
                }
            };
        }
    }
}