﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class DefunctJobChoreDtoToChoreViewModelMapping
        : ChoreDtoToChoreViewModelMapping
    {
        public override ChoreType ChoreType => ChoreType.DefunctJob;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var jobDefinitionId = long.Parse(dto.Parameters.Split(',').First());
            var jobDefinitionCode = dto.Parameters.Substring(dto.Parameters.IndexOf(',') + 1);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.DefunctJobChoreDescription, jobDefinitionCode),
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.DefunctJobChoreDetails,
                        OnClickAction = new UriAction(
                            $"/Scheduling/JobDefinition/View/{jobDefinitionId}",
                            ActionType.Get),
                    },
                    new ToolbarButton
                    {
                        Text = ChoreResources.DefunctJobChoreLogs,
                        OnClickAction = new UriAction(
                            $"/Scheduling/JobLog/List?parent-id={jobDefinitionId}&order=modified-on-desc",
                            ActionType.Get),
                    },
                    new ToolbarButton
                    {
                        Text = ChoreResources.DefunctJobChoreResurrect,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        Icon = FontAwesome.Gavel,
                        OnClickAction = new UriAction(
                            $"/Scheduling/JobDefinition/Resurrect/{jobDefinitionId}",
                            ActionType.Get),
                    },
                }
            };
        }
    }
}