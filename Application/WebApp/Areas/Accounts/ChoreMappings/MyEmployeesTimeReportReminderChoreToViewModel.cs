﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.WebApp.Areas.Accounts.ChoreMappings
{
    public class MyEmployeesTimeReportReminderChoreToViewModel
        : ChoreDtoToChoreViewModelMapping
    {
        public override ChoreType ChoreType => ChoreType.MyEmployeesTimeReportReminder;

        public override ChoreViewModel CreateViewModel(ChoreDto dto)
        {
            var countYearMonth = dto.Parameters.Split(',');
            var count = int.Parse(countYearMonth[0]);
            var year = int.Parse(countYearMonth[1]);
            var month = int.Parse(countYearMonth[2]);

            return new ChoreViewModel
            {
                Description = string.Format(ChoreResources.MyEmployeesTimeReportReminderChoreDescription, count, month, year),
                DueDate = dto.DueDate,
                Actions = new List<CommandButton>
                {
                    new ToolbarButton
                    {
                        Text = ChoreResources.View,
                        ButtonStyleType = CommandButtonStyle.Primary,
                        OnClickAction = new UriAction(
                            $"/TimeTracking/TimeReport/List?year={year}&month={month}&view=default&filter=my-direct-employees,draft+not-started",
                            ActionType.Get),
                    },
                }
            };
        }
    }
}