﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Accounts.Breadcrumbs
{
    public class UserProfilesBreadcrumbItem : BreadcrumbItem
    {
        public UserProfilesBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = UserProfileResources.UserProfilesControllerTitle;
        }
    }
}