using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewUserRoles)]
    public class UserRolesController : SubCardIndexController<SecurityRoleViewModel, SecurityRoleDto, UserController, UserItemsContext>
    {
        private readonly IUserPermissionService _userPermissionService;

        public UserRolesController(
            ISecurityRoleCardIndexService cardIndexDataService, 
            IUserPermissionService userPermissionService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _userPermissionService = userPermissionService;
            
            Layout.Scripts.Add("~/Areas/Accounts/Scripts/UserRoles.js");
            CardIndex.Settings.Title = UserRoleResources.UserRolesControllerTitle;

            Layout.Resources.AddFrom<UserRoleResources>("GrantUserRoleConfirmationMessage");
            Layout.Resources.AddFrom<UserRoleResources>("RevokeUserRoleConfirmationMessage");

            CardIndex.Settings.AddButton.OnClickAction = new JavaScriptCallAction("UserRoles.assignSecurityRole(grid);");
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAssignRoles;

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.DeleteButton.Confirmation = null;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanRevokeRoles;
            CardIndex.Settings.DeleteButton.OnClickAction = new JavaScriptCallAction("UserRoles.revokeSecurityRole(grid);");

            var viewUserProfilesButton = new ToolbarButton
            {
                Text = UserResources.ManageUserProfilesButtonText,
                OnClickAction = Url.UriActionGet<UserProfilesController>(c => c.List(null), KnownParameter.Context),
                RequiredRole = SecurityRoleType.CanViewUserProfiles,
                Icon = FontAwesome.Cogs,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(viewUserProfilesButton);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAssignRoles)]
        public ActionResult AssignRolesToUser(long userId, IList<long> roleIds)
        {
            _userPermissionService.GrantSecurityRolesToUser(userId, roleIds);

            return RedirectToAction("List", "UserRoles", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, userId } });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanRevokeRoles)]
        public ActionResult RevokeRolesFromUser(long userId, IList<long> roleIds)
        {
            _userPermissionService.RevokeSecurityRolesFromUser(userId, roleIds);

            return RedirectToAction("List", "UserRoles", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, userId } });
        }
    }
}