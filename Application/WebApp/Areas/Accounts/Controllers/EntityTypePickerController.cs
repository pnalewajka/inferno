﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [Identifier("ValuePickers.Entity")]
    [AtomicAuthorize]
    public class EntityTypePickerController : ReadOnlyCardIndexController<EntityTypeViewModel, EntityTypeDto>
    {
        public EntityTypePickerController(
            IEntityTypeCardIndexService entityTypeCardIndexService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(entityTypeCardIndexService, baseControllerDependencies)
        {
        }
    }
}