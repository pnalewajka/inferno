﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [Identifier("ValuePickers.SecurityProfiles")]
    [AtomicAuthorize(SecurityRoleType.CanViewProfiles)]
    public class ProfilePickerController : ReadOnlyCardIndexController<SecurityProfileViewModel, SecurityProfileDto, UserItemsContext>
    {
        public ProfilePickerController(
            ISecurityProfileCardIndexService securityProfileCardIndexService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(securityProfileCardIndexService, baseControllerDependencies)
        {
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            CardIndex.Settings.DisplayMode = GridDisplayMode.Tiles;
            CardIndex.Settings.CustomGridDisplayModeViewName = "~/Areas/Accounts/Views/SecurityProfile/_SecurityProfileTile.cshtml";

            Layout.PageTitle = allowMultipleRowSelection
                                   ? UserProfileResources.ValuePickerMultipleTitle
                                   : UserProfileResources.ValuePickerSingleTitle;
        }
    }
}