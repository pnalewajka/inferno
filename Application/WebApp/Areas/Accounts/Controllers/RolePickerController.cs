﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [Identifier("ValuePickers.SecurityRoles")]
    [AtomicAuthorize]
    [ValuePickerDefaults(DialogWidth = "700px")]
    public class RolePickerController
        : ReadOnlyCardIndexController<SecurityRoleViewModel, SecurityRoleDto, UserItemsContext>
        , IHubController
    {
        public RolePickerController(
            ISecurityRoleCardIndexService securityRoleCardIndexService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(securityRoleCardIndexService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = false;
        }

        public PartialViewResult Hub(long id)
        {
            var viewModel = CardIndex.GetRecordById(id);
            var hubViewModel = new RoleHubViewModel(viewModel);

            var role = (SecurityRoleType)id;
            hubViewModel.Usages = GetRoleUsages(role);

            var dialogModel = new ModelBasedDialogViewModel<RoleHubViewModel>(hubViewModel, StandardButtons.None)
            {
                Title = hubViewModel.Name,
                ClassName = "role-hub",
            };

            return PartialView(
                "~/Areas/Accounts/Views/Shared/_RoleHub.cshtml",
                dialogModel);
        }

        [Cached(CacheArea = CacheAreas.FeaturePermissions)]
        private ICollection<RoleHubViewModel.RoleUsage> GetRoleUsages(
            SecurityRoleType role)
        {
            var webAppTypes = FunctionalHelper.Try(
                () => typeof(MvcApplication).Assembly.GetTypes(),
                () => Enumerable.Empty<Type>());

            var controllers = webAppTypes
                .Where(t => !t.IsAbstract)
                .Where(t => typeof(AtomicController).IsAssignableFrom(t));

            var securedControllers = controllers
                .Where(c =>
                    (c.GetCustomAttributes<AtomicAuthorizeAttribute>(true).Any(attr => attr.Roles.Contains(role)))
                    || (c.GetCustomAttributes<ApiAuthorizeAttribute>(true).Any(attr => attr.Roles.Contains(role))));

            var controllersWithActions = controllers
                .SelectMany(c => c.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => typeof(ActionResult).IsAssignableFrom(m.ReturnType))
                    .Where(m => (m.GetCustomAttribute<AtomicAuthorizeAttribute>(true)?.Roles.Contains(role) ?? false)
                        || (m.GetCustomAttribute<ApiAuthorizeAttribute>(true)?.Roles.Contains(role) ?? false))
                    .Select(m => new RoleHubViewModel.RoleUsage
                    {
                        Action = m.Name,
                        Controller = RoutingHelper.GetControllerName(c),
                        Area = RoutingHelper.GetAreaName(c),
                    }));

            var roleActions = securedControllers.SelectMany(
                c => c.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => typeof(ActionResult).IsAssignableFrom(m.ReturnType))
                    .Select(m => new RoleHubViewModel.RoleUsage
                    {
                        Action = m.Name,
                        Controller = RoutingHelper.GetControllerName(c),
                        Area = RoutingHelper.GetAreaName(c),
                    }));

            roleActions = roleActions.Concat(controllersWithActions)
                .Distinct(new RoleHubViewModel.RoleUsage.Comparer());

            return roleActions.ToArray();
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            Layout.PageTitle = allowMultipleRowSelection
                ? UserRoleResources.ValuePickerMultipleTitle
                : UserRoleResources.ValuePickerSingleTitle;
        }
    }
}