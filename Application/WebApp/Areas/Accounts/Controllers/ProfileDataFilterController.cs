using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProfileDataFilters)]
    public class ProfileDataFilterController : SubCardIndexController<DataFilterViewModel, DataFilterDto, ProfileController>
    {
        public ProfileDataFilterController(IProfileDataFilterCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = DataFilterResources.ProfileDataFiltersTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddProfileDataFilters;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditProfileDataFilters;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteProfileDataFilters;
        }
    }
}