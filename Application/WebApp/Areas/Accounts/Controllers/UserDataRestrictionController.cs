using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewUserDataFilters)]
    public class UserDataFilterController : SubCardIndexController<DataFilterViewModel, DataFilterDto, UserController>
    {
        public UserDataFilterController(IUserDataFilterCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = DataFilterResources.UserDataFiltersTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddUserDataFilters;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditUserDataFilters;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteUserDataFilters;

            SwitchAddAndEditToDialogs();
        }
    }
}