using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Consts;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Areas.Consents.Controllers;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewUsers)]
    public class UserController : CardIndexController<UserViewModel, UserDto>
    {
        private readonly IUserCardIndexService _userCardIndexService;
        private readonly IUserService _userService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public UserController(
            IUserCardIndexService userCardIndexService,
            IUserService userService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService)
            : base(userCardIndexService, baseControllerDependencies)
        {
            _userCardIndexService = userCardIndexService;
            _userService = userService;
            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.Title = AuthenticationResources.UserControllerTitle;

            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
            Layout.Css.Add("~/Areas/Accounts/Content/User.css");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddUsers;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.ValuePickerAndGrid;

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewUsers;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditUsers;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteUsers;

            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "row.canBeEdited"
            };

            CardIndex.Settings.DeleteButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.AtLeastOneRowSelected,
                Predicate = "row.canBeDeleted"

            };

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportUsers);

            var manageUserDropdownButton = new ToolbarButton
            {
                Text = UserResources.ManageUserDropdownText,
                OnClickAction = new DropdownAction
                {
                    Items = new List<ICommandButton>
                    {
                        new CommandButton
                        {
                            Text = UserResources.IssueLoginTokenButtonText,
                            Confirmation = new Confirmation
                            {
                                IsRequired = true,
                                Message = UserResources.IssueLoginTokenConfirmation, 
                            },
                            OnClickAction = Url.UriActionPost(nameof(IssueLoginToken), KnownParameter.SelectedId),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.SingleRowSelected,
                            }
                        },
                        new CommandButton
                        {
                            Text = UserResources.ManageUserRolesButtonText,
                            OnClickAction = Url.UriActionGet<UserRolesController>(c => List(null), KnownParameter.ParentId),
                            Group = UserResources.ManagementGroup,
                            Icon = FontAwesome.Cog,
                            RequiredRole = SecurityRoleType.CanViewUserRoles,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.SingleRowSelected
                            }
                        },
                        new CommandButton
                        {
                            Text = UserResources.ManageUserProfilesButtonText,
                            OnClickAction = Url.UriActionGet<UserProfilesController>(c => List(null), KnownParameter.ParentId),
                            Group = UserResources.ManagementGroup,
                            Icon = FontAwesome.Cogs,
                            RequiredRole = SecurityRoleType.CanViewUserProfiles,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.SingleRowSelected
                            }
                        },
                        new CommandButton
                        {
                            Text = DataFilterResources.UserDataFilters,
                            OnClickAction = Url.UriActionGet<UserDataFilterController>(c => c.List(null), KnownParameter.ParentId),
                            Group = UserResources.ManagementGroup,
                            Icon = FontAwesome.List,
                            RequiredRole = SecurityRoleType.CanViewUserDataFilters,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.SingleRowSelected
                            }
                        },
                        new CommandButton
                        {
                            Text = UserResources.UserConsentsText,
                            OnClickAction = Url.UriActionGet<UserConsentsController>(c => c.Index(), KnownParameter.ParentId),
                            Group = UserResources.ManagementGroup,
                            Icon = FontAwesome.List,
                            RequiredRole = SecurityRoleType.CanManageUserConsents,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.SingleRowSelected
                            }
                        }
                    }
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(manageUserDropdownButton);
            CardIndex.Settings.DefaultNewRecord = () => new UserViewModel { IsActive = true };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<UserViewModel>(m => m.Login).IsReadOnly = true;
            };

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = UserResources.StatusFilterGroupName,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.UserStatusActive, DisplayName = UserResources.ActiveUsersFilterName },
                        new Filter { Code = FilterCodes.UserStatusInactive, DisplayName = UserResources.InactiveUsersFilterName }
                    }
                },

                new FilterGroup
                {
                    DisplayName = UserResources.UserControllerAdvancedFilters,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<UserLastLoggedOnBetweenDatesFilterViewModel>(FilterCodes.UserLastModifiedBetweenDates, UserResources.UserLastLoggedOnBetweenDatesFilterName),
                        //following and commented out filter serves programming show case purpose only
                        //new ParametricFilter<UserLastLoggedOnBetweenDatesFilterViewModel, UserLastLoggedOnBetweenDatesFilterDto>(FilterCodes.UserLastModifiedBetweenDatesWithDto, UserResources.UserLastLoggedOnBetweenDatesFilterName)
                    }
                },

                new FilterGroup
                {
                    DisplayName = UserResources.UserRoleAndProfileFilter,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<UserRoleFilterViewModel>(FilterCodes.UserRoles, UserResources.UserRoleFilter),
                        new ParametricFilter<UserProfileFilterViewModel>(FilterCodes.UserProfiles, UserResources.UserProfileFilter)
                    }
                },

                CardIndexHelper.BuildSoftDeletableFilterGroup(UserResources.SoftDeletableState)
            };

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Name, UserResources.NameSearchArea),
                new SearchArea(SearchAreaCodes.Login, UserResources.LoginSearchArea),
                new SearchArea(SearchAreaCodes.Email, UserResources.EmailSearchArea)
            };

            var indexImportSettings = CardIndex.Settings.AddImport<UserViewModel, UserDto>(userCardIndexService, UserResources.UserImportButtonName);
            indexImportSettings.RequiredRole = SecurityRoleType.CanImportUsers;

            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;
        }

        [HttpPost]
        public ActionResult IssueLoginToken(long id)
        {
            var userDto = _userCardIndexService.GetRecordById(id);
            var emailDataDto = new UserEmailDataDto
            {
                UserId = userDto.Id,
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Login = userDto.Login,
                Email = userDto.Email,
            };

            _userService.GenerateLoginTokenAndSendEmail(emailDataDto);

            var message = string.Format(UserControllerResources.UserLoginTokenEmailSentMessageFormat, emailDataDto.GetFullName());
            AddAlert(AlertType.Success, message);

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogUserDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogUserDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            return base.Edit(id);
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var user = (UserViewModel)base.GetViewModelFromContext(childContext);
            var panel = new PanelViewModel($"{user.FirstName} {user.LastName}");

            panel.AddProperty(AuthenticationResources.UserLoginLabel, user.Login);
            panel.AddProperty(AuthenticationResources.UserEmailLabel, user.Email);

            panel.StartNewGroup();

            panel.AddProperty(AuthenticationResources.UserIsActiveLabel, user.IsActive ? CommonResources.Yes : CommonResources.No);
            panel.AddProperty(AuthenticationResources.UserLastLoggedOnLabel, $"{user.LastLoggedOn:d}");

            return panel;
        }

        private static IList<IGridViewConfiguration> GetGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var tilesConfiguration = new TilesViewConfiguration<UserViewModel>(UserResources.UserTilesViewName, "tiles")
            {
                IsDefault = true,
                IsHidden = false,
                CustomViewName = "~/Areas/Accounts/Views/User/_UserTile.cshtml",
                InitializeJavaScriptAction = "Tiles.maxSizeForAll",
            };

            tilesConfiguration.IncludeColumn(u => u.FirstName);
            tilesConfiguration.IncludeColumn(u => u.LastName);
            tilesConfiguration.IncludeColumn(u => u.Login);
            tilesConfiguration.IncludeColumn(u => u.Email);
            tilesConfiguration.IncludeColumn(u => u.IsActive);

            list.Add(tilesConfiguration);

            var listConfiguration = new GridViewConfiguration<UserViewModel>(UserResources.UserListViewName, "list")
            {
                IsDefault = false
            };

            listConfiguration.IncludeColumns(new List<Expression<Func<UserViewModel, object>>>
            {
                u => u.FirstName,
                u => u.LastName,
                u => u.Email,
                u => u.Login,
                u => u.IsActive,
            });

            list.Add(listConfiguration);

            return list;
        }

        [PerformanceTest]
        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }
    }
}