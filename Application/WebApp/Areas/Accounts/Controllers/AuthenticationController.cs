﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Results;
using Smt.Atomic.CrossCutting.Security.Enums;
using Smt.Atomic.CrossCutting.Security.Helpers;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.NtlmAuthentication;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Security.Providers;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Filters;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Consts;
using Smt.Atomic.WebApp.Resources;
using WebGrease.Css.Extensions;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize]
    [HandleAntiforgeryError]
    public class AuthenticationController : AtomicController
    {
        private readonly ISecurityService _securityService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IPrincipalBuilder _principalBuilder;
        private readonly IUserService _userService;
        private readonly INtlmAuthenticationService _ntlmAuthenticationService;
        private readonly IDirectoryService _directoryService;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ILoginService _loginService;
        private readonly ISystemParameterService _systemParameterService;

        private const int LoggingSupportHintFailureThreshold = 2;

        public AuthenticationController(
            IBaseControllerDependencies baseDependencies,
            ISecurityService securityService, 
            IPrincipalBuilder principalBuilder,
            IUserService userService,
            INtlmAuthenticationService ntlmAuthenticationService,
            IDirectoryService directoryService,
            ILoginService loginService, ISystemParameterService systemParameterService)
            : base(baseDependencies)
        {
            _securityService = securityService;
            _principalBuilder = principalBuilder;
            _userService = userService;
            _ntlmAuthenticationService = ntlmAuthenticationService;
            _directoryService = directoryService;
            _principalProvider = baseDependencies.PrincipalProvider;
            _settingsProvider = baseDependencies.SettingsProvider;
            _loginService = loginService;
            _systemParameterService = systemParameterService;

            Layout.Scripts.OnDocumentReady.Add("if (!BrowserSurvey.areCookiesEnabled()) {" +
                                               "  AlertMessages.addError('" + AtomicResources.CookiesAreRequiredMessage + "');" +
                                               "}");
        }

        public ActionResult Index()
        {
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerIndexPageTitle;

            return View("~/Areas/Accounts/Views/Authentication/Index.cshtml");
        }

        [AllowAnonymous]
        [PasswordIssueRedirect(false)]
        public ActionResult Login(string token = null, bool skipNtlm = false)
        {
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerLoginPageTitle;

            if (!string.IsNullOrWhiteSpace(token))
            {
                var validationResult = _securityService.ValidateUser(token);

                AddAlerts(validationResult);

                if (validationResult.IsUserAuthenticated)
                {
                    return HandleSuccessfulAuthentication(validationResult, false);
                }
            }

            var loginModel = new LoginViewModel();

            SetAuthorizationProviderInfo(loginModel);
            loginModel.SkipNtlm = skipNtlm;
            loginModel.FailedAttempts++;

            return View("~/Areas/Accounts/Views/Authentication/Login.cshtml", loginModel);
        }

        private void SetAuthorizationProviderInfo(LoginViewModel loginViewModel)
        {
            loginViewModel.AuthorizationProviders =
                ReflectionHelper.ResolveAll<IAuthorizationProvider>()
                    .Where(p => p.IsEnabled)
                    .Select(p => new AuthorizationProviderViewModel { IconClass = p.IconClass, Name = p.Name, Identifier = p.Identifier})
                    .ToList();

            loginViewModel.IsPassiveNtlm =
                _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.IsPassiveModeEnabled;
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PasswordIssueRedirect(false)]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            SetAuthorizationProviderInfo(model);
            
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerLoginPageTitle;
            
            if (ModelState.IsValid)
            {
                var userLogin = _loginService.ResolveLogin(model.Login);

                var validationResult = _securityService.ValidateUser(userLogin, model.Password);

                AddAlerts(validationResult);

                if (validationResult.IsUserAuthenticated)
                {
                    return HandleSuccessfulAuthentication(validationResult, model.RememberMe, returnUrl);
                }

                if (validationResult.IsPasswordExpired)
                {
                    var user = _userService.GetUserByLogin(userLogin);
                    var canResendEmail = await _userService.CanActivationEmailBeResendAsync(user.Email);

                    if (canResendEmail)
                    {
                        await _userService.ResendActivationEmailAsync(user.Email);
                    }
                }

                if (model.FailedAttempts >= LoggingSupportHintFailureThreshold)
                {
                    AddAlert(AlertType.Information,
                        _systemParameterService.GetParameter<string>(
                            ParameterKeys.LoginAttemptsMessage, new CultureDependedParameterContext { CurrentCulture = CultureInfo.CurrentCulture.Name }));
                }

                model.FailedAttempts++;
            }

            model.Password = null;

            return View(model);
        }

        [AtomicAuthorize(SecurityRoleType.CanChangeOwnPassword)]
        [PasswordIssueRedirect(false)]
        public ActionResult ChangePassword()
        {
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerChangePasswordPageTitle;

            if (GetCurrentPrincipal().PasswordStatus == PasswordStatus.MustSet)
            {
                RedirectToAction("SetPassword");
            }
            
            var model = new ChangePasswordViewModel();
            return View(GetChangePasswordViewName(), model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AtomicAuthorize(SecurityRoleType.CanChangeOwnPassword)]
        [PasswordIssueRedirect(false)]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerChangePasswordPageTitle;

            if (ModelState.IsValid)
            {
                var changeResult = _securityService.ChangePassword(GetCurrentPrincipal().Login, model.OldPassword, model.NewPassword);

                AddAlerts(changeResult);

                return changeResult.IsSuccessful 
                    ? RedirectToLandingPage() 
                    : View(GetChangePasswordViewName(), model);
            }

            return View(GetChangePasswordViewName(), model);
        }

        [PasswordIssueRedirect(false)]
        public ActionResult SetPassword()
        {
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerSetPasswordPageTitle;

            if (GetCurrentPrincipal().PasswordStatus != PasswordStatus.MustSet)
            {
                return RedirectToAction("ChangePassword");
            }

            var model = new SetPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PasswordIssueRedirect(false)]
        public ActionResult SetPassword(SetPasswordViewModel model)
        {
            Layout.PageTitle = Layout.PageHeader = AuthenticationResources.AuthenticationControllerSetPasswordPageTitle;

            if (ModelState.IsValid)
            {
                var changeResult = _securityService.SetPassword(GetCurrentPrincipal().Login, model.NewPassword);

                AddAlerts(changeResult);

                return changeResult.IsSuccessful
                    ? RedirectToLandingPage()
                    : View(model);
            }

            return View(model);
        }

        [PasswordIssueRedirect(false)]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToLandingPage(true);
        }

        private string GetChangePasswordViewName()
        {
            return GetCurrentPrincipal().PasswordStatus == PasswordStatus.MustSet
                       ? "MustChangePassword"
                       : "CanChangePassword";
        }

        private ActionResult HandleSuccessfulAuthentication(UserValidationResult validationResult, bool isPersistent, string returnUrl = null)
        {
            var cookie = AuthenticationHelper.CreateAuthenticationCookie(validationResult.UserId, validationResult.Login, isPersistent);
            Response.Cookies.Add(cookie);

            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            RequestAuthenticationHandler.SetPrincipal(validationResult.Login, _principalBuilder, HttpContext.ApplicationInstance);

            return RedirectToLandingPage();
        }

        [HttpGet]
        [AllowAnonymous]
        [PasswordIssueRedirect(false)]
        public ActionResult NtlmAuthenticate(Guid? id, string returnUrl)
        {
            BoolResultWithState<string> redirectViewModel = null;

            if (!id.HasValue)
            {
                return new RedirectToRouteResult(new RouteValueDictionary(new {id = Guid.NewGuid(), returnUrl = returnUrl}));
            }

            var result = _ntlmAuthenticationService.Authenticate(id.Value, Request.Headers["Authorization"]);

            switch (result.Action)
            {
                case NtlmAuthenticationResultAction.ChallengeAuth:
                    Response.StatusCode = result.StatusCode;
                    result.Headers.ForEach(k => Response.AddHeader(k.Key, k.Value));
                    Response.SuppressFormsAuthenticationRedirect = true;
                    Response.End();

                    return new EmptyResult();

                case NtlmAuthenticationResultAction.ChallengeFailed:
                    return SendChallengeResponse();

                case NtlmAuthenticationResultAction.ChallengeFinished:
                    redirectViewModel = ProcessAuthenticatedUser(result, returnUrl);
                    if (!redirectViewModel)
                    {
                        return SendChallengeResponse();
                    }
                    break;
            }

            return View(redirectViewModel);
        }

        private BoolResultWithState<string> ProcessAuthenticatedUser(NtlmAuthenticationResult result, string returnUrl)
        {
            if (!result.IsAuthenticated)
            {
                return new BoolResultWithState<string>(false, string.Empty);
            }

            if (
                !string.Equals(result.DomainName, _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.Domain,
                    StringComparison.InvariantCultureIgnoreCase))
            {
                return new BoolResultWithState<string>(false, string.Empty);
            }

            var userEmail = _directoryService.GetUserEmail(result.UserName);

            if (userEmail == null)
            {
                return new BoolResultWithState<string>(false, string.Empty);
            }

            var validationResult = _securityService.ValidateUserAgainstDirectory(userEmail);

            if (validationResult.IsUserAuthenticated)
            {
                var redirectResult = HandleSuccessfulAuthentication(validationResult, false, returnUrl) as RedirectResult;

                return new BoolResultWithState<string>(true, redirectResult == null ? "/" : redirectResult.Url);
            }

            return new BoolResultWithState<string>(false, string.Empty);
        }

        private ActionResult SendChallengeResponse()
        {
            Response.StatusCode = 401;
            Response.SuppressFormsAuthenticationRedirect = true;
            Response.AddHeader("WWW-Authenticate", "NTLM");
            Response.End();

            return new EmptyResult();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult SignInVia(string identifier)
        {
            var providerInstance = AuthorizationProviderHelper.GetProviderByIdentifier(identifier);

            if (providerInstance == null)
            {
                return RedirectToLandingPage();
            }

            var redirectTo = providerInstance.GetSignOnRedirection();

            return new RedirectResult(redirectTo.RedirectUri.ToString());
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult RedirectedFrom(string identifier)
        {
            var providerInstance = AuthorizationProviderHelper.GetProviderByIdentifier(identifier);

            if (providerInstance == null)
            {
                return RedirectToLandingPage();
            }

            var httpRequestData = new HttpRequestData
            {
                Params = Request.Params.ToDictionary(),
                Headers = Request.Headers.ToDictionary()
            };

            var validationData = providerInstance.Validate(httpRequestData);

            if (!validationData.IsAuthenticated)
            {
                return RedirectToLandingPage();
            }

            var user = _userService.FindUserByProviderDataOrDefault(validationData.Email, validationData.ExternalId, providerInstance.ProviderType);

            if (user != null)
            {
                return HandleSuccessfulAuthentication(new UserValidationResult
                {
                    UserId = user.Id,
                    Login = user.Login,
                    IsUserAuthenticated = true,
                }, false);
            }

            if (string.IsNullOrEmpty(validationData.Email) && !string.IsNullOrEmpty(validationData.ExternalId))
            {
                Session.Add(SessionKeyConsts.UserValidationDataSessionKey, validationData);

                return RedirectToAction("Register", "UserRegistration");
            }

            var userViaEmail = _userService.GetUserOrDefaultByEmail(validationData.Email);

            if (userViaEmail != null)
            {
                _userService.CreateUserCredential(new CredentialDto
                {
                    Email = validationData.Email,
                    ProviderType = providerInstance.ProviderType,
                    UserId = userViaEmail.Id
                });

                return HandleSuccessfulAuthentication(new UserValidationResult
                {
                    UserId = userViaEmail.Id,
                    Login = userViaEmail.Login,
                    IsUserAuthenticated = true,
                }, false);
            }

            var registrationModel = new RegistrationFromAuthorizationProviderViewModel
            {
                Email = validationData.Email,
                FirstName = validationData.FirstName,
                LastName = validationData.LastName,
                Identifier = Guid.NewGuid().ToString()
            };

            Session.Add(registrationModel.Identifier, validationData);

            return View("~/Areas/Accounts/Views/UserRegistration/RegisterFromAuth.cshtml", registrationModel);
        }

    }
}
