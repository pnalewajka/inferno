﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize]
    public class UserChoresController
        : ReadOnlyCardIndexController<ChoreViewModel, ChoreDto>
    {
        public UserChoresController(
            IUserChoresCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            Layout.Css.Add("~/Areas/Accounts/Content/Chore.css");

            CardIndex.Settings.Title = ChoreResources.Title;

            CardIndex.Settings.GridViewConfigurations = GetTileGridSettings();

            CardIndex.Settings.ToolbarButtons.Clear();
            CardIndex.Settings.ShouldDoubleClickToggleSelection = false;
        }

        [BreadcrumbBar("Start/UserChores")]
        public override ActionResult List(GridParameters parameters)
        {
            return base.List(parameters);
        }

        private static IList<IGridViewConfiguration> GetTileGridSettings()
        {
            var list = new List<IGridViewConfiguration>();

            var tilesConfiguration = new TilesViewConfiguration<ChoreViewModel>("default", "tiles")
            {
                IsDefault = true,
                IsHidden = false,
                CustomViewName = "~/Areas/Accounts/Views/UserChore/_UserChoreTile.cshtml"
            };

            tilesConfiguration.IncludeAllColumns();
            list.Add(tilesConfiguration);

            return list;
        }
    }
}
