﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.Providers;
using Smt.Atomic.Presentation.Common;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Filters;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Consts;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewUserRegistrationForm)]
    [HandleAntiforgeryError]
    public class UserRegistrationController : AtomicController
    {
        private readonly IUserService _userService;
        private readonly IClassMapping<UserViewModel, UserDto> _userViewModelToDtoClassMapping;
        private readonly IPrincipalBuilder _principalBuilder;
        private readonly ILogger _logger;

        public UserRegistrationController(
            IBaseControllerDependencies baseDependencies,
            IUserService userService,
            IClassMapping<UserViewModel, UserDto> userViewModelToDtoClassMapping,
            IPrincipalBuilder principalBuilder,
            ILogger logger
            )
            : base(baseDependencies)
        {
            _userService = userService;
            _userViewModelToDtoClassMapping = userViewModelToDtoClassMapping;
            _principalBuilder = principalBuilder;
            _logger = logger;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Register");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegistrationViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegistrationViewModel registrationViewModel)
        {
            var defaultSecurityRoles = new[] { SecurityRoleType.CanChangeOwnPassword };

            if (ModelState.IsValid)
            {
                var userViewModel = new UserViewModel
                {
                    FirstName = registrationViewModel.FirstName,
                    LastName = registrationViewModel.LastName,
                    Email = registrationViewModel.Email,
                    Login = registrationViewModel.Login,
                    IsActive = true
                };

                var userValidationData = Session[SessionKeyConsts.UserValidationDataSessionKey] as UserAuthorizationValidationResult;

                var result =
                    await
                        HandlingHelper.ExecuteWithExceptionHandlingAsync(async () => await _userService.CreateUserAsync(
                            _userViewModelToDtoClassMapping
                                .CreateFromSource(userViewModel),
                            defaultSecurityRoles, userValidationData), (s, a) => new CreateUserResult(s, null, a));

                AddAlerts(result);

                if (result.IsSuccessful)
                {
                    Session.Remove(SessionKeyConsts.UserValidationDataSessionKey);

                    if (userValidationData != null && userValidationData.IsAuthenticated)
                    {
                        return HandleSuccessfulAuthentication(new UserValidationResult
                        {
                            Login = result.UserDto.Login,
                            IsUserAuthenticated = true
                        }, false);
                    }

                    return RedirectToLandingPage(true);
                }
            }

            return View(registrationViewModel);
        }

        [HttpGet]
        public ActionResult ResendEmail()
        {
            return View(new ResendEmailViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResendEmail(ResendEmailViewModel resendEmailViewModel)
        {
            await ValidateResendingEmail(resendEmailViewModel);

            if (ModelState.IsValid)
            {
                var result =
                    await
                        HandlingHelper.ExecuteWithExceptionHandlingAsync(
                            async () => await _userService.ResendActivationEmailAsync(resendEmailViewModel.Email));

                AddAlerts(result.Alerts);

                if (result.IsSuccessful)
                {
                    return RedirectToLandingPage(true);
                }
            }

            return View(resendEmailViewModel);
        }

        private async Task ValidateResendingEmail(ResendEmailViewModel resendEmailViewModel)
        {
            var canResendEmail = await _userService.CanActivationEmailBeResendAsync(resendEmailViewModel.Email);

            if (!canResendEmail)
            {
                ModelState.AddModelError(PropertyHelper.GetPropertyName<ResendEmailViewModel>(r => r.Email),
                    RegistrationResources.ResendingImpossibleMessage);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterFromAuth(RegistrationFromAuthorizationProviderViewModel registrationViewModel)
        {
            var userValidationData = Session[registrationViewModel.Identifier] as UserAuthorizationValidationResult;
            var defaultSecurityRoles = new[] { SecurityRoleType.CanChangeOwnPassword };

            if (userValidationData == null)
            {
                return RedirectToLandingPage();
            }

            registrationViewModel.Email = userValidationData.Email;
            ModelState.Clear();
            TryValidateModel(registrationViewModel);

            if (!ModelState.IsValid)
            {
                return View(registrationViewModel);
            }

            Session.Remove(registrationViewModel.Identifier);

            try
            {
                var result =
                    _userService.TryCreateUserFromAuth(
                        new UserDto
                        {
                            FirstName = registrationViewModel.FirstName,
                            LastName = registrationViewModel.LastName,
                            Email = registrationViewModel.Email,
                            Login = registrationViewModel.Email,
                            IsActive = true,
                        },
                        userValidationData.ProviderType,
                        defaultSecurityRoles);

                if (!result)
                {
                    return RedirectToLandingPage();
                }
            }
            catch (Exception e)
            {
                _logger.ErrorFormat(e, "User registatration from auth provider {0} failed", userValidationData.ProviderType);

                return RedirectToLandingPage();
            }

            return HandleSuccessfulAuthentication(new UserValidationResult
            {
                Login = userValidationData.Email,
                IsUserAuthenticated = true
            }, false);
        }

        private ActionResult HandleSuccessfulAuthentication(UserValidationResult validationResult, bool isPersistent, string returnUrl = null)
        {
            var cookie = AuthenticationHelper.CreateAuthenticationCookie(validationResult.UserId, validationResult.Login, isPersistent);
            Response.Cookies.Add(cookie);

            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            RequestAuthenticationHandler.SetPrincipal(validationResult.Login, _principalBuilder, HttpContext.ApplicationInstance);

            return RedirectToLandingPage();
        }
    }
}