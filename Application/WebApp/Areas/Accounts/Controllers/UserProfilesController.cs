using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewUserProfiles)]
    public class UserProfilesController : SubCardIndexController<SecurityProfileViewModel, SecurityProfileDto, UserController, UserItemsContext>
    {
        private readonly IUserPermissionService _userPermissionService;

        public UserProfilesController(
            ISecurityProfileCardIndexService cardIndexDataService, 
            IUserPermissionService userPermissionService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _userPermissionService = userPermissionService;

            Layout.Scripts.Add("~/Areas/Accounts/Scripts/UserProfiles.js");
            CardIndex.Settings.Title = UserProfileResources.UserProfilesControllerTitle;

            Layout.Resources.AddFrom<UserProfileResources>("GrantUserProfileConfirmationMessage");
            Layout.Resources.AddFrom<UserProfileResources>("RevokeUserProfileConfirmationMessage");

            CardIndex.Settings.AddButton.OnClickAction = new JavaScriptCallAction("UserProfiles.assignSecurityProfile(grid);");
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAssignProfiles;

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.DeleteButton.Confirmation = null;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanRevokeProfiles;
            CardIndex.Settings.DeleteButton.OnClickAction = new JavaScriptCallAction("UserProfiles.revokeSecurityProfile(grid);");

            CardIndex.Settings.DisplayMode = GridDisplayMode.Tiles;
            CardIndex.Settings.CustomGridDisplayModeViewName = "~/Areas/Accounts/Views/SecurityProfile/_SecurityProfileTile.cshtml";

            CardIndex.Settings.RowButtons.Add(new RowButton
            {
                RequiredRole = SecurityRoleType.CanEditProfiles,
                Text = UserProfileResources.EditProfileLabel,
                ButtonStyleType = CommandButtonStyle.Link,
                OnClickAction = Url.UriActionGet<ProfileController>(c => c.Edit(null), KnownParameter.SelectedId)
            });

            var viewUserRolesButton = new ToolbarButton
            {
                Text = UserResources.ManageUserRolesButtonText,
                OnClickAction = Url.UriActionGet<UserRolesController>(c => c.List(null), KnownParameter.Context),
                RequiredRole = SecurityRoleType.CanViewUserRoles,
                Icon = FontAwesome.Cog,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(viewUserRolesButton);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAssignProfiles)]
        public ActionResult AssignProfilesToUser(long userId, IList<long> profileIds)
        {
            _userPermissionService.AssignSecurityProfilesToUser(userId, profileIds);

            return RedirectToAction("List", "UserProfiles", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, userId } });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanRevokeProfiles)]
        public ActionResult RevokeProfilesFromUser(long userId, IList<long> profileIds)
        {
            _userPermissionService.RemoveSecurityProfilesFromUser(userId, profileIds);

            return RedirectToAction("List", "UserProfiles", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, userId } });
        }
    }
}