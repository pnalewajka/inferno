﻿using System.Web.Mvc;
using System.Web.UI;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.MediaProcessing.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize]
    public class UserPictureController : AtomicController
    {
        private const string DocumentMappingIdentifier = "Pictures.UserPictureMapping";
        private readonly IDocumentDownloadService _documentDownloadService;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;
        private readonly IImageProcessingService _imageProcessingService;
        private readonly ISettingsProvider _settingsProvider;

        public UserPictureController(
            IBaseControllerDependencies baseDependencies,
            IDocumentDownloadService documentDownloadService,
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService,
            IImageProcessingService imageProcessingService,
            ISettingsProvider settingsProvider)
            : base(baseDependencies)
        {
            _documentDownloadService = documentDownloadService;
            _unitOfWorkService = unitOfWorkService;
            _imageProcessingService = imageProcessingService;
            _settingsProvider = settingsProvider;
        }

        [OutputCache(Duration = 60 * 60 * 12, Location = OutputCacheLocation.Any, VaryByParam = "*")]
        public ActionResult DownloadByEmployee(long employeeId, UserPictureSize userPictureSize = UserPictureSize.Normal)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetByIdOrDefault(employeeId);

                return DownloadPicture(employee?.User?.PictureId, userPictureSize);
            }
        }

        [OutputCache(Duration = 60 * 60 * 12, Location = OutputCacheLocation.Any, VaryByParam = "*")]
        public ActionResult Download(long userId, UserPictureSize userPictureSize = UserPictureSize.Normal)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetByIdOrDefault(userId);

                return DownloadPicture(user?.PictureId, userPictureSize);
            }
        }

        private ActionResult DownloadPicture(long? pictureId, UserPictureSize userPictureSize)
        {
            var documentDownloadDto = new DocumentDownloadDto();
            var imagePixelSize = (int)userPictureSize;


            byte[] imageData;

            if (pictureId.HasValue)
            {
                documentDownloadDto = _documentDownloadService.GetDocument(DocumentMappingIdentifier, pictureId.Value);
                imageData = documentDownloadDto.Content;
            }
            else
            {
                var path = Server.MapPath(_settingsProvider.DefaultUserPictureFilePath);

                imageData = System.IO.File.ReadAllBytes(path);
                documentDownloadDto.ContentType = _imageProcessingService.GetImageMimeType(imageData);
            }

            documentDownloadDto.Content = _imageProcessingService.ResizeImage(imageData, imagePixelSize);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }
    }
}