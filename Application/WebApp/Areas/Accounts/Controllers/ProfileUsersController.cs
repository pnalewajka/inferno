﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewUsers)]
    [AtomicAuthorize(SecurityRoleType.CanViewUserProfiles)]
    public class ProfileUsersController : SubCardIndexController<UserViewModel, UserDto, ProfileController>
    {
        private readonly IUserPermissionService _userPermissionService;

        public ProfileUsersController(
            IProfileUserCardIndexService cardIndexDataService,
            IUserPermissionService userPermissionService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _userPermissionService = userPermissionService;

            Layout.Scripts.Add("~/Areas/Accounts/Scripts/ProfileUsers.js");
            CardIndex.Settings.Title = UserProfileResources.ProfileUsersControllerTitle;

            Layout.Resources.AddFrom<UserResources>("GrantUserConfirmationMessage");
            Layout.Resources.AddFrom<UserResources>("RevokeUserConfirmationMessage");

            CardIndex.Settings.AddButton.OnClickAction = new JavaScriptCallAction("ProfileUsers.assignUser(grid);");
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddUsers;

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.DeleteButton.Confirmation = null;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteUsers;
            CardIndex.Settings.DeleteButton.OnClickAction = new JavaScriptCallAction("ProfileUsers.revokeUser(grid);");

            CardIndex.Settings.DisplayMode = GridDisplayMode.Tiles;
            CardIndex.Settings.CustomGridDisplayModeViewName = "~/Areas/Accounts/Views/User/_UserTile.cshtml";
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddUsers)]
        public ActionResult AssignUsersToProfile(long profileId, IEnumerable<long> userIds)
        {
            _userPermissionService.AssignUsersToProfile(profileId, userIds);

            return RedirectToAction("List", "ProfileUsers", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, profileId } });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanDeleteUsers)]
        public ActionResult RevokeUsersFromProfile(long profileId, IEnumerable<long> userIds)
        {
            _userPermissionService.RemoveUsersFromProfile(profileId, userIds);

            return RedirectToAction("List", "ProfileUsers", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, profileId } });
        }
    }
}