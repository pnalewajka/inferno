using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.Consts;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Accounts.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProfiles)]
    [PerformanceTest(nameof(ProfileController.List))]
    public class ProfileController : CardIndexController<SecurityProfileViewModel, SecurityProfileDto>
    {
        public ProfileController(ISecurityProfileCardIndexService securityProfileGroupCardIndexService, IBaseControllerDependencies baseControllerDependencies)
            : base(securityProfileGroupCardIndexService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = UserRoleResources.ProfileControllerTitle;
            CardIndex.Settings.ShowDeleteButtonOnEdit = true;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddProfiles;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditProfiles;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteProfiles;

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Name, UserProfileResources.SecurityProfileNameLabel),
                new SearchArea(SearchAreaCodes.Roles, UserProfileResources.SecurityProfileRolesLabel, false),
                new SearchArea(SearchAreaCodes.ActiveDirectoryGroup, UserProfileResources.ActiveDirectoryGroupNameLabel, false),
            };

            var manageDataFilterButton = new ToolbarButton
            {
                Text = DataFilterResources.UserDataFilters,
                OnClickAction = Url.UriActionGet<ProfileDataFilterController>(c => c.List(null), KnownParameter.ParentId),
                Icon = FontAwesome.List,
                RequiredRole = SecurityRoleType.CanViewProfileDataFilters,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.RowButtons.Add(new RowButton
            {
                RequiredRole = SecurityRoleType.CanEditProfiles,
                Text = ProfileResources.EditProfileLabel,
                ButtonStyleType = CommandButtonStyle.Link,
                OnClickAction = Url.UriActionGet(nameof(Edit), KnownParameter.SelectedId)
            });

            CardIndex.Settings.ToolbarButtons.Add(manageDataFilterButton);

            CardIndex.Settings.DisplayMode = GridDisplayMode.Tiles;
            CardIndex.Settings.CustomGridDisplayModeViewName = "~/Areas/Accounts/Views/SecurityProfile/_SecurityProfileTile.cshtml";

            var manageUserDropdownButton = new ToolbarButton
            {
                Text = UserResources.ManageUserDropdownText,
                OnClickAction = new DropdownAction
                {
                    Items = new List<ICommandButton>
                    {
                        new CommandButton
                        {
                            Text = UserResources.ManageUsersButtonText,
                            OnClickAction = Url.UriActionGet<ProfileUsersController>(c => List(null), KnownParameter.ParentId),
                            Group = UserResources.ManagementGroup,
                            Icon = FontAwesome.Users,
                            RequiredRole = SecurityRoleType.CanViewUsers,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.SingleRowSelected
                            }
                        },
                    }
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(manageUserDropdownButton);
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var profile = (SecurityProfileViewModel)base.GetViewModelFromContext(childContext);

            var panelTitle = string.IsNullOrWhiteSpace(profile.ActiveDirectoryGroupName)
                ? profile.Name
                : $"{profile.Name} ({profile.ActiveDirectoryGroupName})";

            var panel = new PanelViewModel(panelTitle);

            return panel;
        }
    }
}