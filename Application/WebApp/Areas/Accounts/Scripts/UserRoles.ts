﻿module UserRoles {
    export function assignSecurityRole(grid: Grid.GridComponent) {
        var context = grid.gridContext;
        var parentId = UrlHelper.getQueryParameter(context, 'parent-id');
        var urlResolveCallback = ValuePicker.getContextResolveUrlCallback({ "parent-id": parentId, "should-exclude": true });

        ValuePicker.selectMultiple("ValuePickers.SecurityRoles", (records: Grid.IRow[]) => {
            if (records.length === 0) {
                return;
            }

            CommonDialogs.confirm(Globals.resources.GrantUserRoleConfirmationMessage + Utils.getHtmlList(records),
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                    if (eventArgs.isOk) {
                        var ids = records.map((v) => v.id.toString()).join(",");
                        var url = Strings.format("../UserRoles/AssignRolesToUser?userId={0}&roleIds={1}", parentId, ids);
                        Forms.submit(url, "POST");
                    }
                });
        }, urlResolveCallback, { selection: new ValuePicker.SelectionManager(ValuePicker.SelectionMode.Multiple), dialogWidth: "1000px" });
    }

    export function revokeSecurityRole(grid: Grid.GridComponent) {
        var context = grid.gridContext;
        var parentId = UrlHelper.getQueryParameter(context, 'parent-id');
        var records = grid.getSelectedRowData<Grid.IRow>();

        CommonDialogs.confirm(Globals.resources.RevokeUserRoleConfirmationMessage + Utils.getHtmlList(records),
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    var ids = records.map((v) => v.id.toString()).join(",");
                    var url = Strings.format("../UserRoles/RevokeRolesFromUser?userId={0}&roleIds={1}", parentId, ids);
                    Forms.submit(url, "POST");
                }
            });
    }
}