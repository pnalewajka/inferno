﻿namespace ProfileUsers {
    export function assignUser(grid: Grid.GridComponent) {
        const context = grid.gridContext;
        const parentId = UrlHelper.getQueryParameter(context, "parent-id");
        const urlResolveCallback = ValuePicker.getContextResolveUrlCallback({ "parent-id": parentId, "should-exclude": true });

        ValuePicker.selectMultiple("ValuePickers.Users", (records: Grid.IRow[]) => {
            if (records.length === 0) {
                return;
            }

            CommonDialogs.confirm(Globals.resources.GrantUserProfileConfirmationMessage + Utils.getHtmlList(records),
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                    if (eventArgs.isOk) {
                        const ids = records.map((v) => v.id.toString()).join(",");
                        const url = "../ProfileUsers/AssignUsersToProfile?profileId=" + parentId + "&userIds=" + ids;
                        Forms.submit(url, "POST");
                    }
                });
        }, urlResolveCallback);
    }

    export function revokeUser(grid: Grid.GridComponent) {
        const context = grid.gridContext;
        const parentId = UrlHelper.getQueryParameter(context, "parent-id");
        const records = grid.getSelectedRowData<Grid.IRow>();

        CommonDialogs.confirm(Globals.resources.RevokeUserProfileConfirmationMessage + Utils.getHtmlList(records),
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    const ids = records.map((v) => v.id.toString()).join(",");
                    const url = "../ProfileUsers/RevokeUsersFromProfile?profileId=" + parentId + "&userIds=" + ids;
                    Forms.submit(url, "POST");
                }
            });
    }
}
