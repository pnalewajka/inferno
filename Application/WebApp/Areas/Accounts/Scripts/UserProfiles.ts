﻿module UserProfiles{
    export function assignSecurityProfile(grid: Grid.GridComponent) {
        var context = grid.gridContext;
        var parentId = UrlHelper.getQueryParameter(context, 'parent-id');
        var urlResolveCallback = ValuePicker.getContextResolveUrlCallback({ "parent-id": parentId, "should-exclude": true });

        ValuePicker.selectMultiple("ValuePickers.SecurityProfiles", (records: Grid.IRow[]) => {
            if (records.length === 0) {
                return;
            }

            CommonDialogs.confirm(Globals.resources.GrantUserProfileConfirmationMessage + Utils.getHtmlList(records),
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    var ids = records.map((v) => v.id.toString()).join(",");
                    var url = "../UserProfiles/AssignProfilesToUser?userId=" + parentId + "&profileIds=" + ids;
                    Forms.submit(url, "POST");
                }
            });
        }, urlResolveCallback);
    }

    export function revokeSecurityProfile(grid: Grid.GridComponent) {
        var context = grid.gridContext;
        var parentId = UrlHelper.getQueryParameter(context, 'parent-id');
        var records = grid.getSelectedRowData<Grid.IRow>();

        CommonDialogs.confirm(Globals.resources.RevokeUserProfileConfirmationMessage + Utils.getHtmlList(records),
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    var ids = records.map((v) => v.id.toString()).join(",");
                    var url = "../UserProfiles/RevokeProfilesFromUser?userId=" + parentId + "&profileIds=" + ids;
                    Forms.submit(url, "POST");
                }
            });
    }
}