﻿using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.UtilizationCategory")]
    public class UtilizationCategoryPickerController : UtilizationCategoryController
    {
        public UtilizationCategoryPickerController(
            IUtilizationCategoryCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridConfigurations();

            cardIndexDataService.MustFilterInactiveUtilizationCategories = true;
        }

        private IList<IGridViewConfiguration> GetGridConfigurations()
        {
            var configuration = GridViewConfiguration<UtilizationCategoryViewModel>.DefaultGridViewConfiguration;            
            configuration.ExcludeColumn(x => x.IsActive);
            configuration.IsDefault = true;

            return new IGridViewConfiguration[] { configuration };
        }
    }
}