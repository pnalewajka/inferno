﻿using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.LanguageWithLevel")]
    [AtomicAuthorize(SecurityRoleType.CanViewLanguage)]
    public class LanguageWithLevelPickerController : LanguageWithLevelController
    {
        public LanguageWithLevelPickerController(ILanguageWithLevelCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);
            CardIndex.Settings.PageSize = (int)LanguageReferenceLevel.C2;
        }
    }
}