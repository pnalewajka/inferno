﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.Currencies")]
    [AtomicAuthorize(SecurityRoleType.CanViewCurrencies)]
    public class CurrencyPickerController : ReadOnlyCardIndexController<CurrencyViewModel, CurrencyDto, ParticipantContext>
    {
        public CurrencyPickerController(
            ICurrencyCardIndexDataService currencyCardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(currencyCardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = CurrencyRequestResources.CurrenciesTitle;
        }
    }
}