﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.SoftwareCategory")]
    public class SoftwareCategoryPickerController : SoftwareCategoryController
    {
        public SoftwareCategoryPickerController(ISoftwareCategoryCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}