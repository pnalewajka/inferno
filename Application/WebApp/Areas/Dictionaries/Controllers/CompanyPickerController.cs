﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.Company")]
    public class CompanyPickerController : CompanyController
    {
        private const string MinimalViewId = "minimal";

        public CompanyPickerController(
            ISystemParameterService systemParameterService,
            ICompanyCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(systemParameterService, cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<CompanyViewModel>(DictionaryResources.MinimalViewText, MinimalViewId) { IsDefault = true };

            configuration.IncludeColumns(new List<Expression<Func<CompanyViewModel, object>>>
            {
                c => c.Name,
                x => x.Description
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}