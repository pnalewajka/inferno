﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCompany)]
    [Identifier("CompanyValuePickers.Company")]
    public class CompanyController : CardIndexController<CompanyViewModel, CompanyDto, CompanyContext>
    {
        public CompanyController(
            ISystemParameterService systemParameterService,
            ICompanyCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            string defaultDomain = systemParameterService.GetParameter<string>(ParameterKeys.UserDefaultEmailDomain);

            CardIndex.Settings.DefaultNewRecord = () => new CompanyViewModel
            {
                EmailDomain = defaultDomain
            };

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddCompany;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCompany;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteCompany;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportCompany);

            CardIndex.Settings.Title = DictionaryResources.CompanyTitle;
        }
    }
}