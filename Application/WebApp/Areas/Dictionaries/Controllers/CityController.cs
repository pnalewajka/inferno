﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCity)]
    public class CityController : CardIndexController<CityViewModel, CityDto, ParentIdContext>
    {
        public CityController(ICityCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddCity;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCity;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteCity;

            CardIndex.Settings.Title = DictionaryResources.CityTitle;
            SwitchAddAndEditToDialogs();
        }
    }
}
