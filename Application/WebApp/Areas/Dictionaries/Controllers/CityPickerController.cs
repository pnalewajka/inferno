﻿using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.City")]
    public class CityPickerController : CityController
    {
        public const string BusinessTripsViewCode = "business-trips";

        public CityPickerController(ICityCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridConfigurations();
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.ValuePicker;
            CardIndex.Settings.AddButton.Text = BusinessTripRequestResources.PickerAddCity;           
        }

        private IList<IGridViewConfiguration> GetGridConfigurations()
        {
            var defaultConfig = GridViewConfiguration<CityViewModel>.DefaultGridViewConfiguration;

            defaultConfig.IsDefault = true;
            defaultConfig.ExcludeAllColumns();
            defaultConfig.IncludeColumn(c => c.Name);
            defaultConfig.IncludeColumn(c => c.CountryName);

            var businessTripsConfig = new GridViewConfiguration<CityViewModel>(DictionaryResources.BusinessTripsViewName, BusinessTripsViewCode)
            {
                IsDefault = false,
                IsHidden = true
            };

            businessTripsConfig.ExcludeAllColumns();
            businessTripsConfig.IncludeColumn(c => c.Name);
            businessTripsConfig.IncludeColumn(c => c.CountryName);
            businessTripsConfig.IncludeColumn(c => c.IsCompanyApartmentAvailable);
            businessTripsConfig.IncludeColumn(c => c.IsVoucherServiceAvailable);

            return new List<IGridViewConfiguration>() { defaultConfig, businessTripsConfig };
        }
    }
}
