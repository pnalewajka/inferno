﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.ServiceLine")]
    [AtomicAuthorize(SecurityRoleType.CanViewServiceLines)]
    public class ServiceLineController : CardIndexController<ServiceLineViewModel, ServiceLineDto>
    {
        public ServiceLineController(IServiceLineCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddServiceLines;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditServiceLines;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteServiceLines;

            CardIndex.Settings.Title = DictionaryResources.ServiceLineTitle;
        }
    }
}