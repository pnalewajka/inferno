﻿using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.Language")]
    [AtomicAuthorize(SecurityRoleType.CanViewLanguage)]
    public class LanguagePickerController : LanguageController
    {
        public LanguagePickerController(ILanguageCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}
