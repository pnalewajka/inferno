﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.Region")]
    [AtomicAuthorize(SecurityRoleType.CanViewRegion)]
    public class RegionController : CardIndexController<RegionViewModel, RegionDto>
    {
        public RegionController(IRegionCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRegion;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRegion;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRegion;

            CardIndex.Settings.Title = DictionaryResources.RegionTitle;
        }
    }
}
