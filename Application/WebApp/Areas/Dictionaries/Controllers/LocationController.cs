﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewLocation)]
    public class LocationController : CardIndexController<LocationViewModel, LocationDto>
    {
        public LocationController(ILocationCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddLocation;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditLocation;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteLocation;

            CardIndex.Settings.Title = DictionaryResources.LocationTitle;
        }
    }
}