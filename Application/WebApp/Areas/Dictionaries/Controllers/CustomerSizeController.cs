﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCustomerSizes)]
    [Identifier("ValuePickers.CustomerSizes")]
    public class CustomerSizeController : CardIndexController<CustomerSizeViewModel, CustomerSizeDto>
    {
        public CustomerSizeController(ICustomerSizeCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddCustomerSizes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCustomerSizes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteCustomerSizes;

            CardIndex.Settings.Title = DictionaryResources.CustomerSizeTitle;
        }
    }
}