﻿using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.Country")]
    public class CountryPickerController : CountryController
    {
        public CountryPickerController(ICountryCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
        }
    }
}
