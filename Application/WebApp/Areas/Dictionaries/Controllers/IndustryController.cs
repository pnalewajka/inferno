﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [Identifier("ValuePickers.Industry")]
    [AtomicAuthorize(SecurityRoleType.CanViewIndustry)]
    public class IndustryController : CardIndexController<IndustryViewModel, IndustryDto>
    {
        public IndustryController(IIndustryCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddIndustry;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditIndustry;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteIndustry;

            CardIndex.Settings.Title = DictionaryResources.IndustryTitle;
        }
    }
}
