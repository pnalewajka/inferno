﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCountry)]
    public class CountryController : CardIndexController<CountryViewModel, CountryDto>
    {
        public CountryController(ICountryCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddCountry;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCountry;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteCountry;

            CardIndex.Settings.Title = DictionaryResources.CountryTitle;
        }
    }
}
