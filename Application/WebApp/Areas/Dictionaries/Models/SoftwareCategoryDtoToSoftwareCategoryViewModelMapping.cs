﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class SoftwareCategoryDtoToSoftwareCategoryViewModelMapping : ClassMapping<SoftwareCategoryDto, SoftwareCategoryViewModel>
    {
        public SoftwareCategoryDtoToSoftwareCategoryViewModelMapping()
        {
            Mapping = d => new SoftwareCategoryViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
