﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class SoftwareCategoryViewModelToSoftwareCategoryDtoMapping : ClassMapping<SoftwareCategoryViewModel, SoftwareCategoryDto>
    {
        public SoftwareCategoryViewModelToSoftwareCategoryDtoMapping()
        {
            Mapping = v => new SoftwareCategoryDto
            {
                Id = v.Id,
                Name = v.Name,
                Timestamp = v.Timestamp
            };
        }
    }
}
