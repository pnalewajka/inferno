﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    [TabDescription(0, "basic", nameof(DictionaryResources.BasicInformationTabName), typeof(DictionaryResources))]
    [TabDescription(1, "recruitment", nameof(DictionaryResources.RecruitmentTabName), typeof(DictionaryResources))]
    [TabDescription(2, "businesstrips", nameof(DictionaryResources.BusinessTripsTabName), typeof(DictionaryResources))]
    [TabDescription(3, "compensation", nameof(DictionaryResources.CompensationTabName), typeof(DictionaryResources))]
    [Identifier("Models.CompanyViewModel")]
    public class CompanyViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [Required]
        [Tab("basic")]
        [StringLength(100)]
        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        public string Name { get; set; }

        [Order(1)]
        [Tab("basic")]
        [DisplayNameLocalized(nameof(DictionaryResources.DescriptionLabel), typeof(DictionaryResources))]
        public string Description { get; set; }

        [Order(2)]
        [Tab("basic")]
        [Required]
        [StringLength(40)]
        [DisplayNameLocalized(nameof(DictionaryResources.EmailDomainLabel), typeof(DictionaryResources))]
        public string EmailDomain { get; set; }

        [Order(3)]
        [Tab("basic")]
        [Required]
        [StringLength(100)]
        [DisplayNameLocalized(nameof(DictionaryResources.ActiveDirectoryCodeLabel), typeof(DictionaryResources))]
        public string ActiveDirectoryCode { get; set; }

        [Order(4)]
        [Tab("basic")]
        [StringLength(100)]
        [DisplayNameLocalized(nameof(DictionaryResources.NavisionCodeLabel), typeof(DictionaryResources))]
        public string NavisionCode { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("basic")]
        [DisplayNameLocalized(nameof(DictionaryResources.CustomOrderLabel), typeof(DictionaryResources))]
        [Range(0, long.MaxValue)]
        public long? CustomOrder { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("recruitment")]
        [DisplayNameLocalized(nameof(DictionaryResources.IsDataAdministratorLabel), typeof(DictionaryResources))]
        public bool IsDataAdministrator { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.AdvancePaymentAcceptingEmployee), typeof(DictionaryResources))]
        public long? AdvancePaymentAcceptingEmployeeId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.TravelExpensesSpecialistsLabel), typeof(DictionaryResources))]
        public long[] TravelExpensesSpecialistsEmployeeIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [DisplayNameLocalized(nameof(DictionaryResources.DefaultFrontDeskAssigneeEmployeeId), typeof(DictionaryResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long? FrontDeskAssigneeEmployeeId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [DisplayNameLocalized(nameof(DictionaryResources.AllowedAdvancedPaymentCurrencies), typeof(DictionaryResources))]
        [Repeater(CanAddRecord = true, CanDeleteRecord = true, CanReorder = true)]
        public List<CompanyAllowedAdvancePaymentCurrencyViewModel> AllowedAdvancedPaymentCurrencies { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.DefaultAdvancedPaymentCurrency), typeof(DictionaryResources))]
        public long DefaultAdvancedPaymentCurrencyId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [DisplayNameLocalized(nameof(DictionaryResources.DefaultAdvancedPaymentDailyAmount), typeof(DictionaryResources))]
        public decimal DefaultAdvancedPaymentDailyAmount { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [DisplayNameLocalized(nameof(DictionaryResources.AllowedAccomodations), typeof(DictionaryResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<AccommodationType>))]
        public AccommodationType AllowedAccomodations { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("businesstrips")]
        [DisplayNameLocalized(nameof(DictionaryResources.AllowedMeansOfTransport), typeof(DictionaryResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<MeansOfTransport>))]
        public MeansOfTransport AllowedMeansOfTransport { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab("compensation")]
        [Multiline]
        [DisplayNameLocalized(nameof(DictionaryResources.InvoiceInformation), typeof(DictionaryResources))]
        public string InvoiceInformation { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}