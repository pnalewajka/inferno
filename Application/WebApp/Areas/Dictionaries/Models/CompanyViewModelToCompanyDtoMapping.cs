﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CompanyViewModelToCompanyDtoMapping : ClassMapping<CompanyViewModel, CompanyDto>
    {
        public CompanyViewModelToCompanyDtoMapping()
        {
            Mapping = v => new CompanyDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                EmailDomain = v.EmailDomain,
                ActiveDirectoryCode = v.ActiveDirectoryCode,
                NavisionCode = v.NavisionCode,
                TravelExpensesSpecialistsEmployeeIds = v.TravelExpensesSpecialistsEmployeeIds,
                Timestamp = v.Timestamp,
                CustomOrder = v.CustomOrder,
                IsDataAdministrator = v.IsDataAdministrator,
                AllowedAdvancedPaymentCurrencies = v.AllowedAdvancedPaymentCurrencies != null ? 
                    v.AllowedAdvancedPaymentCurrencies.Select(c => new CompanyAllowedAdvancePaymentCurrencyDto
                    {
                        CompanyId = v.Id,
                        Currency = new CurrencyDto { Id = c.CurrencyId },
                        MinimumValue = c.MinimumValue,
                    }).ToList()
                    : new List<CompanyAllowedAdvancePaymentCurrencyDto>(),
                DefaultAdvancedPaymentCurrencyId = v.DefaultAdvancedPaymentCurrencyId,
                DefaultAdvancedPaymentDailyAmount = v.DefaultAdvancedPaymentDailyAmount,
                AdvancePaymentAcceptingEmployeeId = v.AdvancePaymentAcceptingEmployeeId,
                InvoiceInformation = v.InvoiceInformation,
                FrontDeskAssigneeEmployeeId = v.FrontDeskAssigneeEmployeeId,
                AllowedAccomodations = v.AllowedAccomodations,
                AllowedMeansOfTransport = v.AllowedMeansOfTransport
            };
        }
    }
}
