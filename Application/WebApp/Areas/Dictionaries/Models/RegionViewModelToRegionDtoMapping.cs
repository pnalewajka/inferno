﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class RegionViewModelToRegionDtoMapping : ClassMapping<RegionViewModel, RegionDto>
    {
        public RegionViewModelToRegionDtoMapping()
        {
            Mapping = v => new RegionDto
            {
                Id = v.Id,
                Name = v.Name,
                Timestamp = v.Timestamp
            };
        }
    }
}
