﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CityDtoToCityViewModelMapping : ClassMapping<CityDto, CityViewModel>
    {
        public CityDtoToCityViewModelMapping()
        {
            Mapping = d => new CityViewModel
            {
                Id = d.Id,
                Name = d.Name,
                CountryId = d.CountryId,
                CountryName = d.CountryName,
                IsCompanyApartmentAvailable = d.IsCompanyApartmentAvailable,
                IsVoucherServiceAvailable = d.IsVoucherServiceAvailable,
                Timestamp = d.Timestamp
            };
        }
    }
}
