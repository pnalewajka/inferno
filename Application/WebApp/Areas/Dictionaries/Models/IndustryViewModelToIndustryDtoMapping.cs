﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class IndustryViewModelToIndustryDtoMapping : ClassMapping<IndustryViewModel, IndustryDto>
    {
        public IndustryViewModelToIndustryDtoMapping()
        {
            Mapping = v => new IndustryDto
            {
                Id = v.Id,
                Name = v.Name,
                Timestamp = v.Timestamp
            };
        }
    }
}
