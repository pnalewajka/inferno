﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class IndustryDtoToIndustryViewModelMapping : ClassMapping<IndustryDto, IndustryViewModel>
    {
        public IndustryDtoToIndustryViewModelMapping()
        {
            Mapping = d => new IndustryViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
