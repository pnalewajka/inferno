﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CurrencyViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayNameLocalized(nameof(CurrencyRequestResources.NameLabel), typeof(CurrencyRequestResources))]
        public string Name { get; set; }

        [Required]
        [StringLength(12)]
        [DisplayNameLocalized(nameof(CurrencyRequestResources.IsoCodeLabel), typeof(CurrencyRequestResources))]
        public string IsoCode { get; set; }

        [Required]
        [StringLength(12)]
        [DisplayNameLocalized(nameof(CurrencyRequestResources.SymbolLabel), typeof(CurrencyRequestResources))]
        public string Symbol { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; internal set; }

        public override string ToString()
        {
            return $"{Symbol} ({IsoCode})";
        }
    }
}
