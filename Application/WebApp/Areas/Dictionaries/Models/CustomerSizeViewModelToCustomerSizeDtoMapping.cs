﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CustomerSizeViewModelToCustomerSizeDtoMapping : ClassMapping<CustomerSizeViewModel, CustomerSizeDto>
    {
        public CustomerSizeViewModelToCustomerSizeDtoMapping()
        {
            Mapping = v => new CustomerSizeDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                Timestamp = v.Timestamp
            };
        }
    }
}
