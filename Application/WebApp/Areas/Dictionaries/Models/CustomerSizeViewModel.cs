﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CustomerSizeViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        [Order(1)]
        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.DescriptionLabel), typeof(DictionaryResources))]
        [Order(2)]
        public string Description { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
