﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LocationViewModelToLocationDtoMapping : ClassMapping<LocationViewModel, LocationDto>
    {
        public LocationViewModelToLocationDtoMapping()
        {
            Mapping = v => new LocationDto
            {
                Id = v.Id,
                Name = v.Name,
                Address = v.Address,
                CityId = v.CityId,
                CountryId = v.CountryId,
                Timestamp = v.Timestamp
            };
        }
    }
}
