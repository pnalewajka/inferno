﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CurrencyViewModelToCurrencyDtoMapping : ClassMapping<CurrencyViewModel, CurrencyDto>
    {
        public CurrencyViewModelToCurrencyDtoMapping()
        {
            Mapping = v => new CurrencyDto
            {
                Id = v.Id,
                Name = v.Name,
                IsoCode = v.IsoCode,
                Symbol = v.Symbol,
                Timestamp = v.Timestamp
            };
        }
    }
}
