﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class RegionDtoToRegionViewModelMapping : ClassMapping<RegionDto, RegionViewModel>
    {
        public RegionDtoToRegionViewModelMapping()
        {
            Mapping = d => new RegionViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
