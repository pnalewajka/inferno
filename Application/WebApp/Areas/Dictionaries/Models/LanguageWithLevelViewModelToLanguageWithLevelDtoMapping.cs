﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LanguageWithLevelViewModelToLanguageWithLevelDtoMapping : ClassMapping<LanguageWithLevelViewModel, LanguageWithLevelDto>
    {
        public LanguageWithLevelViewModelToLanguageWithLevelDtoMapping()
        {
            Mapping = v => new LanguageWithLevelDto
            {
                Id = v.Id,
                NameAndLevel = v.NameAndLevel,
                NameAndLevelEng = v.NameAndLevel.English,
                NameAndLevelPl = v.NameAndLevel.Polish
            };
        }
    }
}
