﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LanguageViewModelToLanguageDtoMapping : ClassMapping<LanguageViewModel, LanguageDto>
    {
        public LanguageViewModelToLanguageDtoMapping()
        {
            Mapping = v => new LanguageDto
            {
                Id = v.Id,
                Name = v.Name
            };
        }
    }
}
