﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CountryViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        public string Name { get; set; }

        [Required]
        [StringLength(2)]
        [DisplayNameLocalized(nameof(DictionaryResources.IsoCode), typeof(DictionaryResources))]
        public string IsoCode { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return $"{Name} ({IsoCode})";
        }
    }
}
