﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    [Identifier("Models.CityPickerViewModel")]
    public class CityViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        public string Name { get; set; }

        [Required]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CountryPickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.CountryLabel), typeof(DictionaryResources))]
        public long CountryId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DictionaryResources.CountryLabel), typeof(DictionaryResources))]
        public string CountryName { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.IsCompanyApartmentAvailableLabel), typeof(DictionaryResources))]
        public bool IsCompanyApartmentAvailable { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.IsVoucherServiceAvailableLabel), typeof(DictionaryResources))]
        public bool IsVoucherServiceAvailable { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return $"{Name}, {CountryName}";
        }
    }
}
