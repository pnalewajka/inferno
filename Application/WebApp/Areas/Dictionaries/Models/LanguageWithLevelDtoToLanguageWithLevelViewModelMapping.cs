﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LanguageWithLevelDtoToLanguageWithLevelViewModelMapping : ClassMapping<LanguageWithLevelDto, LanguageWithLevelViewModel>
    {
        public LanguageWithLevelDtoToLanguageWithLevelViewModelMapping()
        {
            Mapping = d => new LanguageWithLevelViewModel
            {
                Id = d.Id,
                NameAndLevel = d.NameAndLevel
            };
        }
    }
}
