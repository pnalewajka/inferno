﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LocationDtoToLocationViewModelMapping : ClassMapping<LocationDto, LocationViewModel>
    {
        public LocationDtoToLocationViewModelMapping()
        {
            Mapping = d => new LocationViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Address = d.Address,
                CityId = d.CityId,
                CountryId = d.CountryId,
                Timestamp = d.Timestamp
            };
        }
    }
}
