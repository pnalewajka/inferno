﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    [Identifier("Filters.CompanyFilter")]
    [DescriptionLocalized("CompanyFilterDialogTitle", typeof(DictionaryResources))]
    public class CompanyFilterViewModel
    {
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.CompanyLabel), typeof(DictionaryResources))]
        public long[] CompanyIds { get; set; }

        public override string ToString()
        {
            return DictionaryResources.CompanyLabel;
        }
    }
}