﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CityViewModelToCityDtoMapping : ClassMapping<CityViewModel, CityDto>
    {
        public CityViewModelToCityDtoMapping()
        {
            Mapping = v => new CityDto
            {
                Id = v.Id,
                Name = v.Name,
                CountryId = v.CountryId,
                CountryName = v.CountryName,
                IsCompanyApartmentAvailable = v.IsCompanyApartmentAvailable,
                IsVoucherServiceAvailable = v.IsVoucherServiceAvailable,
                Timestamp = v.Timestamp
            };
        }
    }
}
