﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CurrencyDtoToCurrencyViewModelMapping : ClassMapping<CurrencyDto, CurrencyViewModel>
    {
        public CurrencyDtoToCurrencyViewModelMapping()
        {
            Mapping = d => new CurrencyViewModel
            {
                Id = d.Id,
                Name = d.Name,
                IsoCode = d.IsoCode,
                Symbol = d.Symbol,
            };
        }
    }
}
