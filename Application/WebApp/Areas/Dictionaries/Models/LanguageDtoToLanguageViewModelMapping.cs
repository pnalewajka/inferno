﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LanguageDtoToLanguageViewModelMapping : ClassMapping<LanguageDto, LanguageViewModel>
    {
        public LanguageDtoToLanguageViewModelMapping()
        {
            Mapping = e => new LanguageViewModel
            {
                Id = e.Id,
                Name = e.Name
            };
        }
    }
}
