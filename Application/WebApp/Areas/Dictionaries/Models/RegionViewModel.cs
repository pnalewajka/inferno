﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class RegionViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        [Order(1)]
        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
