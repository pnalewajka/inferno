﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class UtilizationCategoryDtoToUtilizationCategoryViewModelMapping : ClassMapping<UtilizationCategoryDto, UtilizationCategoryViewModel>
    {
        public UtilizationCategoryDtoToUtilizationCategoryViewModelMapping()
        {
            Mapping = d => new UtilizationCategoryViewModel
            {
                Id = d.Id,
                NavisionCode = d.NavisionCode,
                NavisionName = d.NavisionName,
                DisplayName = d.DisplayName,
                Order = d.Order,
                IsActive = d.IsActive,
                Timestamp = d.Timestamp
            };
        }
    }
}
