﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    [DescriptionLocalized("SoftwareCategoryFilterDialogTitle", typeof(DictionaryResources))]
    [Identifier("Filters.SoftwareCategoryFilter")]
    public class SoftwareCategoryFilterViewModel
    {
        [DisplayNameLocalized(nameof(DictionaryResources.SoftwareCategoryTitle), typeof(DictionaryResources))]
        [ValuePicker(Type = typeof(SoftwareCategoryPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [Render( Size = Size.Large)]
        public long[] SoftwareCategoryIds { get; set; }

        public override string ToString()
        {
            return DictionaryResources.FilterBySoftwareCategoriesTitle;
        }
    }
}