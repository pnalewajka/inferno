﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LanguageWithLevelViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        [Order(1)]
        [Required]
        [NonSortable]
        public LocalizedString NameAndLevel { get; set; }

        public override string ToString()
        {
            return NameAndLevel.ToString();
        }
    }
}
