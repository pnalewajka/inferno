﻿using System.Linq;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CompanyDtoToCompanyViewModelMapping : ClassMapping<CompanyDto, CompanyViewModel>
    {
        public CompanyDtoToCompanyViewModelMapping()
        {
            Mapping = d => new CompanyViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                EmailDomain = d.EmailDomain,
                ActiveDirectoryCode = d.ActiveDirectoryCode,
                NavisionCode = d.NavisionCode,
                TravelExpensesSpecialistsEmployeeIds = d.TravelExpensesSpecialistsEmployeeIds,
                Timestamp = d.Timestamp,
                CustomOrder = d.CustomOrder,
                IsDataAdministrator = d.IsDataAdministrator,
                AllowedAdvancedPaymentCurrencies = d.AllowedAdvancedPaymentCurrencies
                    .Select(c => new CompanyAllowedAdvancePaymentCurrencyViewModel
                    {
                        CurrencyId = c.Currency.Id,
                        MinimumValue = c.MinimumValue,
                    }).ToList(),
                DefaultAdvancedPaymentCurrencyId = d.DefaultAdvancedPaymentCurrencyId,
                DefaultAdvancedPaymentDailyAmount = d.DefaultAdvancedPaymentDailyAmount,
                AdvancePaymentAcceptingEmployeeId = d.AdvancePaymentAcceptingEmployeeId,
                InvoiceInformation = d.InvoiceInformation,
                FrontDeskAssigneeEmployeeId = d.FrontDeskAssigneeEmployeeId,
                AllowedAccomodations = d.AllowedAccomodations,
                AllowedMeansOfTransport = d.AllowedMeansOfTransport
            };
        }
    }
}
