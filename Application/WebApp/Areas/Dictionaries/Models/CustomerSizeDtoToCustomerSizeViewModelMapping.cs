﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CustomerSizeDtoToCustomerSizeViewModelMapping : ClassMapping<CustomerSizeDto, CustomerSizeViewModel>
    {
        public CustomerSizeDtoToCustomerSizeViewModelMapping()
        {
            Mapping = d => new CustomerSizeViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}
