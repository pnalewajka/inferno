﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class LocationViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        [Order(1)]
        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.AddressLabel), typeof(DictionaryResources))]
        [StringLength(255)]
        public string Address { get; set; }

        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.CityLabel), typeof(DictionaryResources))]
        public long? CityId { get; set; }

        [ValuePicker(Type = typeof(CountryPickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.CountryLabel), typeof(DictionaryResources))]
        [Visibility(VisibilityScope.Form)]
        public long? CountryId { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CityId.HasValue && CountryId.HasValue)
            {
                yield return new ValidationResult(DictionaryResources.CityAndCountryError, new[] { nameof(CityId), nameof(CountryId) });
            }
        }
    }
}
