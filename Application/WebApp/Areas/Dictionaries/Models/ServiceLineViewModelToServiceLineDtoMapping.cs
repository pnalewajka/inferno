﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class ServiceLineViewModelToServiceLineDtoMapping : ClassMapping<ServiceLineViewModel, ServiceLineDto>
    {
        public ServiceLineViewModelToServiceLineDtoMapping()
        {
            Mapping = v => new ServiceLineDto
            {
                Id = v.Id,
                Name = v.Name,
                Timestamp = v.Timestamp
            };
        }
    }
}
