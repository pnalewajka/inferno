﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CountryViewModelToCountryDtoMapping : ClassMapping<CountryViewModel, CountryDto>
    {
        public CountryViewModelToCountryDtoMapping()
        {
            Mapping = v => new CountryDto
            {
                Id = v.Id,
                Name = v.Name,
                IsoCode = v.IsoCode,
                Timestamp = v.Timestamp
            };
        }
    }
}
