﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class ServiceLineDtoToServiceLineViewModelMapping : ClassMapping<ServiceLineDto, ServiceLineViewModel>
    {
        public ServiceLineDtoToServiceLineViewModelMapping()
        {
            Mapping = d => new ServiceLineViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
