﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CompanyAllowedAdvancePaymentCurrencyViewModel
    {
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(DictionaryResources.Currency), typeof(DictionaryResources))]
        public long CurrencyId { get; set; }

        [DisplayNameLocalized(nameof(DictionaryResources.MinimumValue), typeof(DictionaryResources))]
        public decimal MinimumValue { get; set; }
    }
}
