﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class UtilizationCategoryViewModelToUtilizationCategoryDtoMapping : ClassMapping<UtilizationCategoryViewModel, UtilizationCategoryDto>
    {
        public UtilizationCategoryViewModelToUtilizationCategoryDtoMapping()
        {
            Mapping = v => new UtilizationCategoryDto
            {
                Id = v.Id,
                NavisionCode = v.NavisionCode,
                NavisionName = v.NavisionName,
                DisplayName = v.DisplayName,
                Order = v.Order,
                IsActive = v.IsActive,
                Timestamp = v.Timestamp
            };
        }
    }
}
