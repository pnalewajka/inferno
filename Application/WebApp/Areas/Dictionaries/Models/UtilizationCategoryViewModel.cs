﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class UtilizationCategoryViewModel
    {
        public long Id { get; set; }

        [Order(1)]
        [Required]
        [StringLength(64)]
        [DisplayNameLocalized(nameof(DictionaryResources.NavisionCodeLabel), typeof(DictionaryResources))]
        public string NavisionCode { get; set; }

        [Order(2)]
        [Required]
        [StringLength(64)]
        [DisplayNameLocalized(nameof(DictionaryResources.NavisionNameLabel), typeof(DictionaryResources))]
        public string NavisionName { get; set; }

        [Order(3)]
        [Required]
        [StringLength(64)]
        [DisplayNameLocalized(nameof(DictionaryResources.DisplayNameLabel), typeof(DictionaryResources))]
        public string DisplayName { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(DictionaryResources.OrderLabel), typeof(DictionaryResources))]
        public int? Order { get; set; }

        [Order(5)]
        [DisplayNameLocalized(nameof(DictionaryResources.UtilizationCategoryIsActiveLabel), typeof(DictionaryResources))]
        public bool IsActive { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
