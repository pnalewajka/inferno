﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Dictionaries.Models
{
    public class CountryDtoToCountryViewModelMapping : ClassMapping<CountryDto, CountryViewModel>
    {
        public CountryDtoToCountryViewModelMapping()
        {
            Mapping = d => new CountryViewModel
            {
                Id = d.Id,
                Name = d.Name,
                IsoCode = d.IsoCode,
                Timestamp = d.Timestamp
            };
        }
    }
}
