﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Dictionaries
{
    public class DictionariesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Dictionaries";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Dictionaries_default2",
                "Dictionaries/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}