﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Survey.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class SurveyResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SurveyResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Survey.Resources.SurveyResources", typeof(SurveyResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Respondents.
        /// </summary>
        public static string AddRespondentsButtonText {
            get {
                return ResourceManager.GetString("AddRespondentsButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to add respondents?.
        /// </summary>
        public static string AddRespondentsToSurveyConfirmation {
            get {
                return ResourceManager.GetString("AddRespondentsToSurveyConfirmation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        public static string BackButtonText {
            get {
                return ResourceManager.GetString("BackButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string CancelSurveyButtonText {
            get {
                return ResourceManager.GetString("CancelSurveyButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Export.
        /// </summary>
        public static string ExportButtonText {
            get {
                return ResourceManager.GetString("ExportButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fill Survey.
        /// </summary>
        public static string FillSurvey {
            get {
                return ResourceManager.GetString("FillSurvey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Is Completed.
        /// </summary>
        public static string IsCompleted {
            get {
                return ResourceManager.GetString("IsCompleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Edited.
        /// </summary>
        public static string LastEditedAt {
            get {
                return ResourceManager.GetString("LastEditedAt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minimal.
        /// </summary>
        public static string MinimalViewText {
            get {
                return ResourceManager.GetString("MinimalViewText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Answer.
        /// </summary>
        public static string NoAnswerText {
            get {
                return ResourceManager.GetString("NoAnswerText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ok.
        /// </summary>
        public static string OkButtonText {
            get {
                return ResourceManager.GetString("OkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name.
        /// </summary>
        public static string RespondentFirstName {
            get {
                return ResourceManager.GetString("RespondentFirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name.
        /// </summary>
        public static string RespondentLastName {
            get {
                return ResourceManager.GetString("RespondentLastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Respondents were added to survey.
        /// </summary>
        public static string RespondentsWereAddedToSurvey {
            get {
                return ResourceManager.GetString("RespondentsWereAddedToSurvey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No respondents were added to survey.
        /// </summary>
        public static string RespondentsWerentAddedToSurvey {
            get {
                return ResourceManager.GetString("RespondentsWerentAddedToSurvey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string SaveSurveyButtonText {
            get {
                return ResourceManager.GetString("SaveSurveyButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Answers.
        /// </summary>
        public static string ShowAnswers {
            get {
                return ResourceManager.GetString("ShowAnswers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save and Finnish.
        /// </summary>
        public static string SubmitSurveyButtonText {
            get {
                return ResourceManager.GetString("SubmitSurveyButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Answer is Required.
        /// </summary>
        public static string SurveyAnswerIsRequiredValidationErrorMessage {
            get {
                return ResourceManager.GetString("SurveyAnswerIsRequiredValidationErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No further changes possible. Are you sure you want to submit survey?.
        /// </summary>
        public static string SurveyAutoCloseSubmitConfirmation {
            get {
                return ResourceManager.GetString("SurveyAutoCloseSubmitConfirmation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Survey is closed and no further changes possible.
        /// </summary>
        public static string SurveyClosed_ErrorMessage {
            get {
                return ResourceManager.GetString("SurveyClosed_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open Surveys.
        /// </summary>
        public static string SurveyControllerPageTitle {
            get {
                return ResourceManager.GetString("SurveyControllerPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Survey was already finished.
        /// </summary>
        public static string SurveyFinished_ErrorMessage {
            get {
                return ResourceManager.GetString("SurveyFinished_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instruction.
        /// </summary>
        public static string SurveyInstruction {
            get {
                return ResourceManager.GetString("SurveyInstruction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thank You.
        /// </summary>
        public static string SurveyThankYou {
            get {
                return ResourceManager.GetString("SurveyThankYou", resourceCulture);
            }
        }
    }
}
