﻿using System.Linq;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Survey.Controllers;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.Survey.Breadcrumbs
{
    public class SurveyDefinitionsBreadcrumbItem : CardIndexBreadcrumbItem
    {
        public SurveyDefinitionsBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider, IBreadcrumbService breadcrumbService) 
            : base(breadcrumbContextProvider, breadcrumbService)
        {
            var lastBreadcrumbItem = PreviousItems.Last();
            var surveyDefinitionControllerName = RoutingHelper.GetControllerName(typeof(SurveyDefinitionController));
            var surveyConductControllerName = RoutingHelper.GetControllerName(typeof(SurveyConductController));

            Url = lastBreadcrumbItem.Url.Replace(surveyConductControllerName, surveyDefinitionControllerName);
            DisplayName = SurveyDefinitionResources.SurveyDefinitionControllerPageTitle;

            PreviousItems.RemoveAt(PreviousItems.Count - 1);
        }
    }
}