﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Survey.Breadcrumbs
{
    public class WhistleBlowingBreadcrumbItem : BreadcrumbItem
    {
        public WhistleBlowingBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = Resources.WhistleBlowingResources.BreadcrumbLabel;
        }
    }
}