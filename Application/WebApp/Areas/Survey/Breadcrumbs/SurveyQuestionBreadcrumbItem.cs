﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Survey.Breadcrumbs
{
    public class SurveyQuestionBreadcrumbItem:BreadcrumbItem
    {
        public SurveyQuestionBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            :base(breadcrumbContextProvider)
        {
            DisplayName = SurveyQuestionResources.SurveyQuestionControllerPageTitle;
        }
    }
}