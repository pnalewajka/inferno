﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Survey.Breadcrumbs
{
    public class SurveyBreadcrumbItem : BreadcrumbItem
    {
        public SurveyBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = Resources.SurveyResources.SurveyControllerPageTitle;
        }
    }
}