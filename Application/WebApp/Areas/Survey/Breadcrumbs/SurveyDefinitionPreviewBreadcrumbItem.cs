﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Survey.Breadcrumbs
{
    public class SurveyDefinitionPreviewBreadcrumbItem : BreadcrumbItem
    {
        public SurveyDefinitionPreviewBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = Resources.SurveyDefinitionResources.SurveyDefinitionPreview;
        }
    }
}