﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Survey.Breadcrumbs
{
    public class FillSurveyBreadcrumbItem : BreadcrumbItem
    {
        public FillSurveyBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = Resources.SurveyResources.FillSurvey;
        }
    }
}