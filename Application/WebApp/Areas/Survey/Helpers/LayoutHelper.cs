﻿using Smt.Atomic.Presentation.Renderers.Models.Layout;

namespace Smt.Atomic.WebApp.Areas.Survey.Helpers
{
    public static class LayoutHelper
    {
        public static void ApplySurveyStyles(LayoutViewModel layout)
        {
            layout.Css.Add("~/Areas/Survey/Content/Survey.css");
            layout.Scripts.Add("~/Areas/Survey/Scripts/Survey.js");
        }
    }
}