﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public string Name { get; set; }

        [Visibility(VisibilityScope.None)]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool ShouldAutoClose { get; set; }
        
        [Visibility(VisibilityScope.None)]
        public string WelcomeInstruction { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ThankYouMessage { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool ShouldDisplayWelcomeInstruction { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool ShouldDisplayWelcomeInstructionPopup { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool ShouldDisplayThankYouPopup { get; set; }

        [DisplayNameLocalized(nameof(SurveyResources.RespondentFirstName), typeof(SurveyResources))]
        [Visibility(VisibilityScope.Grid)]
        public string RespondentFirstName { get; set; }

        [DisplayNameLocalized(nameof(SurveyResources.RespondentLastName), typeof(SurveyResources))]
        [Visibility(VisibilityScope.Grid)]
        public string RespondentLastName { get; set; }

        [DisplayNameLocalized(nameof(SurveyResources.LastEditedAt), typeof(SurveyResources))]
        [Visibility(VisibilityScope.Grid)]
        public DateTime? LastEditedAt { get; set; }

        [DisplayNameLocalized(nameof(SurveyResources.IsCompleted), typeof(SurveyResources))]
        [Visibility(VisibilityScope.Grid)]
        public bool IsCompleted { get; set; }

        [Visibility(VisibilityScope.None)]
        public long SurveyConductId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long RespondentId { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsPreview { get; set; }

        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public IList<QuestionViewModel> Questions { get; set; }

        [Visibility(VisibilityScope.None)]
        public string VisibilityFunctions { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            foreach (var question in Questions)
            {
                if (question.IsRequired && string.IsNullOrWhiteSpace(question.Answers))
                {
                    issues.Add(new ValidationResult(SurveyResources.SurveyAnswerIsRequiredValidationErrorMessage, new[] { question.FieldName }));
                }
            }

            return issues;
        }

        public static SurveyViewModel Create(SurveyDto survey, bool displayAnswers = false, bool isSurveyPreview = false)
        {
            var viewModel = new SurveyViewModel();
            viewModel.Id = survey.Id;
            viewModel.IsPreview = displayAnswers;
            viewModel.Name = survey.Name;
            viewModel.Description = survey.Description;
            viewModel.ShouldAutoClose = survey.ShouldAutoClose;
            viewModel.WelcomeInstruction = survey.WelcomeInstruction;
            viewModel.ShouldDisplayWelcomeInstruction = !string.IsNullOrWhiteSpace(viewModel.WelcomeInstruction);
            viewModel.ThankYouMessage = survey.ThankYouMessage;
            viewModel.RespondentFirstName = survey.RespondentFirstName;
            viewModel.RespondentLastName = survey.RespondentLastName;
            viewModel.Questions = survey.Questions.Select(a => new QuestionViewModel
            {
                Id = a.Id,
                IsRequired = a.IsRequired,
                QuestionText = a.QuestionText,
                QuestionDescription = a.QuestionDescription,
                FieldName = a.FieldName,
                FieldSize = a.FieldSize,
                Answers = a.Answer,
                Type = a.Type,
                Order = a.Order,
                Items = a.QuestionItems.Items,
                VisibilityCondition = a.VisibilityCondition,
                IsQuestionPreview = isSurveyPreview
            }).ToList();

            return viewModel;
        }
    }
}
