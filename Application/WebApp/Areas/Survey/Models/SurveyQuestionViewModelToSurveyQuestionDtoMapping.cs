﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyQuestionViewModelToSurveyQuestionDtoMapping : ClassMapping<SurveyQuestionViewModel, SurveyQuestionDto>
    {
        public SurveyQuestionViewModelToSurveyQuestionDtoMapping()
        {
            Mapping = viewModel => new SurveyQuestionDto
            {
                Id = viewModel.Id,
                Text = viewModel.Text,
                Type = viewModel.Type,
                IsRequired = viewModel.IsRequired,
                Description = viewModel.Description,
                VisibilityCondition = viewModel.VisibilityCondition,
                Timestamp = viewModel.Timestamp,
                SurveyDefinitionId = viewModel.SurveyDefinitionId,
                FieldName = viewModel.FieldName,
                FieldSize = viewModel.FieldSize,
                Order = viewModel.Order,
                Items = viewModel.Items,
                DefaultAnswer = viewModel.DefaultAnswer ?? viewModel.DefaultAnswers
            };
        }
    }
}
