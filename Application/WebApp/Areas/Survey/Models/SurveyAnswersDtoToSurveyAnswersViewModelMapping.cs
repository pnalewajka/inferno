﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyAnswersDtoToSurveyAnswersViewModelMapping : ClassMapping<SurveyAnswersDto, SurveyAnswersViewModel>
    {
        public SurveyAnswersDtoToSurveyAnswersViewModelMapping()
        {
            Mapping = dto =>
            new SurveyAnswersViewModel
            {
                SurveyId = dto.SurveyId,
                Items = dto.Items.Select(i =>
                new SurveyItemViewModel
                {
                    QuestionId = i.QuestionId,
                    IsRequired = i.IsRequired,
                    FieldName = i.FieldName,
                    Answer = i.Answer,
                    SelectedAnswer = i.SelectedAnswer
                }).ToList()
            };
        }
    }
}
