﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyDefinitionViewModelToSurveyDefinitionDtoMapping : ClassMapping<SurveyDefinitionViewModel, SurveyDefinitionDto>
    {
        public SurveyDefinitionViewModelToSurveyDefinitionDtoMapping()
        {
            Mapping = v => new SurveyDefinitionDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                ShouldAutoClose = v.ShouldAutoClose,
                WelcomeInstruction = v.WelcomeInstruction,
                ThankYouMessage = v.ThankYouMessage,
                Timestamp = v.Timestamp
            };
        }
    }
}
