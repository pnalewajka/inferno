﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class QuestionViewModel
    {
        public long Id { get; set; }

        public string Answers { get; set; }

        public string FieldName { get; set; }

        public SurveyFieldSize FieldSize { get; set; }

        public bool IsRequired { get; set; }

        public bool IsVisible { get; set; }

        public string[] Items { get; set; }

        public long Order { get; set; }

        public string QuestionText { get; set; }

        public string QuestionDescription { get; set; }

        public string VisibilityCondition { get; set; }

        public SurveyQuestionType Type { get; set; }

        public bool IsQuestionPreview { get; set; }
    }
}