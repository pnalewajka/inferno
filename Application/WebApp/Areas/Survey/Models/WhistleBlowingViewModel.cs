﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Accounts.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Resources;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class WhistleBlowingViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.CreatedOnLabel), typeof(WhistleBlowingResources))]
        public DateTime CreatedOn { get; set; }

        [Required]
        [Order(1)]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.TitleLabel), typeof(WhistleBlowingResources))]
        public string Title { get; set; }

        [AllowHtml]
        [Order(2)]
        [Required]
        [StringLength(2048)]
        [RichText(RichTextEditorPresets.Basic)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.MessageLabel), typeof(WhistleBlowingResources))]
        public string Message { get; set; }

        [Order(2)]
        [Ellipsis(MaxStringLength = 200)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.MessageLabel), typeof(WhistleBlowingResources))]
        public string MessageForGrid { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.ModifiedByUserFullNameLabel), typeof(WhistleBlowingResources))]
        public string ModifiedByUserFullName { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.DocumentsLabel), typeof(WhistleBlowingResources))]
        [DocumentUpload(typeof(WhistleBlowingDocumentMapping))]
        public List<DocumentViewModel> Documents { get; set; } = new List<DocumentViewModel>();

        [Order(5)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.ReadLabel), typeof(WhistleBlowingResources))]
        [Visibility(VisibilityScope.Grid)]
        public WhistleBlowingMessageStatus MessageStatus { get; set; }

        [Order(6)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(WhistleBlowingResources.ShowUserNameLabel), typeof(WhistleBlowingResources))]
        public bool ShowUserName { get; set; }
    }
}
