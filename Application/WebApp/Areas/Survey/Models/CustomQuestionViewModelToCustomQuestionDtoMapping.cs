﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class CustomQuestionViewModelToCustomQuestionDtoMapping : ClassMapping<QuestionItemViewModel, QuestionItemDto>
    {
        public CustomQuestionViewModelToCustomQuestionDtoMapping()
        {
            Mapping = viewModel =>
            new QuestionItemDto
            {
                Id = viewModel.Id,
                ItemText = viewModel.ItemText
            };
        }
    }
}
