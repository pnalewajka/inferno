﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyItemDtoToSurveyItemViewModelMapping : ClassMapping<SurveyItemDto, SurveyItemViewModel>
    {
        public SurveyItemDtoToSurveyItemViewModelMapping()
        {
            Mapping = dto =>
            new SurveyItemViewModel
            {
                QuestionId = dto.QuestionId,
                IsRequired = dto.IsRequired,
                FieldName = dto.FieldName,
                SelectedAnswer = dto.SelectedAnswer,
                Answer = dto.Answer
            };
        }
    }
}
