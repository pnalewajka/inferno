﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Resources;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class QuestionItemViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(SurveyQuestionResources.SurveyQuestionAnswer), typeof(SurveyQuestionResources))]
        public string ItemText { get; set; }

        public override string ToString()
        {
            return ItemText;
        }
    }
}
