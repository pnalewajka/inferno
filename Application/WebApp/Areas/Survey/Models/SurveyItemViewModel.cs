﻿using System;
using System.Collections.Generic;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyItemViewModel
    {
        public long QuestionId { get; set; }

        public bool IsRequired { get; set; }

        public bool IsVisible { get; set; }

        public string FieldName { get; set; }

        public IEnumerable<string> SelectedAnswer { get; set; }

        public string Answer { get; set; }
    }
}
