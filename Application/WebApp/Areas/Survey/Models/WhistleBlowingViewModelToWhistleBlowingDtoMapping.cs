﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class WhistleBlowingViewModelToWhistleBlowingDtoMapping : ClassMapping<WhistleBlowingViewModel, WhistleBlowingDto>
    {
        public WhistleBlowingViewModelToWhistleBlowingDtoMapping()
        {
            Mapping = v => new WhistleBlowingDto
            {
                Id = v.Id,
                Title = v.Title,
                Message = v.Message,
                ShowUserName = v.ShowUserName,
                Documents = v.Documents.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId,
                    TemporaryDocumentId = p.TemporaryDocumentId
                }).ToList(),
                MessageStatus = v.MessageStatus,
                CreatedOn = v.CreatedOn,
            };
        }
    }
}
