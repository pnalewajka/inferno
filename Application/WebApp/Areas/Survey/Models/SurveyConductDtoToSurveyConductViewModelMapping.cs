﻿using System.Linq;
using System.Text;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.Survey.Resources;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyConductDtoToSurveyConductViewModelMapping : ClassMapping<SurveyConductDto, SurveyConductViewModel>
    {
        public SurveyConductDtoToSurveyConductViewModelMapping()
        {
            Mapping = dto => new SurveyConductViewModel
            {
                Id = dto.Id,
                SurveyDefinitionId = dto.SurveyDefinitionId,
                SurveyName = dto.SurveyName,
                SurveyDescription = dto.SurveyDescription,
                ShouldAutoClose = dto.ShouldAutoClose,
                OpenedOn = dto.OpenedOn,
                IsClosed = dto.IsClosed,
                Pollster = dto.Pollster,
                OpenedById = dto.OpenedById,
                Timestamp = dto.Timestamp,
                OrgUnitIds = dto.OrgUnitIds,
                EmployeeIds = dto.EmployeeIds,
                ActiveDirectoryGroupIds = dto.ActiveDirectoryGroupIds,
                TargetAudience = BuildTargetAudienceValue(dto)
            };
        }

        private string BuildTargetAudienceValue(SurveyConductDto dto)
        {
            var builder = new StringBuilder();

            if (dto.OrganizationUnitNames.Any())
            {
                builder.Append(
                    $"{SurveyConductResources.TargetOrgUnits}: {string.Join(", ", dto.OrganizationUnitNames)}; ");
            }

            if (dto.EmployeeNames.Any())
            {
                builder.Append(
                    $"{SurveyConductResources.TargetEmployees}: {string.Join(", ", dto.EmployeeNames)}; ");
            }

            if (dto.ActiveDirectoryGroupNames.Any())
            {
                builder.Append(
                    $"{SurveyConductResources.TargetActiveDirectoryGroups}: {string.Join(", ", dto.ActiveDirectoryGroupNames)};");
            }

            return builder.ToString();
        }
    }
}
