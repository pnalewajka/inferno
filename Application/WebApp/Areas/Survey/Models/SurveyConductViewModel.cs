﻿using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Controllers;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    [FieldSetDescription(1, "definition", "SurveyDefinitionLegend", typeof(SurveyConductResources))]
    [FieldSetDescription(2, "target", "TargetAudienceLegend", typeof(SurveyConductResources))]
    public class SurveyConductViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.SurveyDefinition), typeof(SurveyConductResources))]
        [ValuePicker(Type = typeof(SurveyDefinitionPickerController))]
        [Visibility(VisibilityScope.Form)]
        [Required]
        [FieldSet("definition")]
        public long? SurveyDefinitionId { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.SurveyName), typeof(SurveyConductResources))]
        [Visibility(VisibilityScope.Grid)]
        public string SurveyName { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.SurveyDescription), typeof(SurveyConductResources))]
        [Visibility(VisibilityScope.Grid)]
        public string SurveyDescription { get; set; }

        [DisplayNameLocalized(nameof(SurveyDefinitionResources.AutoCloseLabel), typeof(SurveyDefinitionResources))]
        [Visibility(VisibilityScope.Grid)]
        public bool ShouldAutoClose { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.OpenedOn), typeof(SurveyConductResources))]
        [DataType(DataType.Date)]
        [Visibility(VisibilityScope.Grid)]
        [JsonConverter(typeof(DateOnlyDateTimeConverter))]
        public DateTime? OpenedOn { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.IsClosed), typeof(SurveyConductResources))]
        [Visibility(VisibilityScope.Grid)]
        public bool IsClosed { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.Pollster), typeof(SurveyConductResources))]
        [Visibility(VisibilityScope.Grid)]
        public string Pollster { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? OpenedById { get; set; }

        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.TargetOrgUnits), typeof(SurveyConductResources))]
        [ValuePicker(Type = typeof(Organization.Controllers.OrgUnitPickerController))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("target")]
        [NonSortable]
        public long[] OrgUnitIds { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.TargetEmployees), typeof(SurveyConductResources))]
        [ValuePicker(Type = typeof(Allocation.Controllers.EmployeePickerController))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("target")]
        [NonSortable]
        public long[] EmployeeIds { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.TargetActiveDirectoryGroups), typeof(SurveyConductResources))]
        [ValuePicker(Type = typeof(Allocation.Controllers.ActiveDirectoryGroupPickerController))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet("target")]
        [NonSortable]
        public long[] ActiveDirectoryGroupIds { get; set; }

        [DisplayNameLocalized(nameof(SurveyConductResources.TargetAudienceLegend), typeof(SurveyConductResources))]
        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        public string TargetAudience { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if ((OrgUnitIds == null || OrgUnitIds.Length < 1) 
                && (EmployeeIds == null || EmployeeIds.Length < 1) 
                && (ActiveDirectoryGroupIds == null || ActiveDirectoryGroupIds.Length < 1))
            {
                issues.Add(new ValidationResult(SurveyConductResources.TargetAudienceValidationErrorMessage));
            }

            return issues;
        }
    }
}
