﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyDefinitionDtoToSurveyDefinitionViewModelMapping : ClassMapping<SurveyDefinitionDto, SurveyDefinitionViewModel>
    {
        public SurveyDefinitionDtoToSurveyDefinitionViewModelMapping()
        {
            Mapping = d => new SurveyDefinitionViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                ShouldAutoClose = d.ShouldAutoClose,
                WelcomeInstruction = d.WelcomeInstruction,
                ThankYouMessage = d.ThankYouMessage,
                Timestamp = d.Timestamp
            };
        }
    }
}
