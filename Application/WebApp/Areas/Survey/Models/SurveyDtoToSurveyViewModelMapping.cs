﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyDtoToSurveyViewModelMapping : ClassMapping<SurveyDto, SurveyViewModel>
    {
        public SurveyDtoToSurveyViewModelMapping()
        {
            Mapping = dto => new SurveyViewModel
            {
                Id = dto.Id,
                SurveyConductId = dto.SurveyConductId,
                RespondentId = dto.RespondentId,
                RespondentFirstName = dto.RespondentFirstName,
                RespondentLastName = dto.RespondentLastName,
                LastEditedAt = dto.LastEditedAt,
                IsCompleted = dto.IsCompleted,
                Timestamp = dto.Timestamp
            };
        }
    }
}
