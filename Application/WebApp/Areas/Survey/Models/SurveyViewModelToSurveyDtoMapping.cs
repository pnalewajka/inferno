﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyViewModelToSurveyDtoMapping : ClassMapping<SurveyViewModel, SurveyDto>
    {
        public SurveyViewModelToSurveyDtoMapping()
        {
            Mapping = viewModel => new SurveyDto
            {
                Id = viewModel.Id,
                SurveyConductId = viewModel.SurveyConductId,
                RespondentId = viewModel.RespondentId,
                LastEditedAt = viewModel.LastEditedAt,
                IsCompleted = viewModel.IsCompleted,
                Timestamp = viewModel.Timestamp
            };
        }
    }
}
