﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyConductViewModelToSurveyConductDtoMapping : ClassMapping<SurveyConductViewModel, SurveyConductDto>
    {
        public SurveyConductViewModelToSurveyConductDtoMapping()
        {
            Mapping = viewModel =>
            new SurveyConductDto
            {
                Id = viewModel.Id,
                SurveyDefinitionId = viewModel.SurveyDefinitionId,
                OpenedOn = viewModel.OpenedOn,
                OpenedById = viewModel.OpenedById,
                IsClosed = viewModel.IsClosed,
                Timestamp = viewModel.Timestamp,
                OrgUnitIds = viewModel.OrgUnitIds,
                EmployeeIds = viewModel.EmployeeIds,
                ActiveDirectoryGroupIds = viewModel.ActiveDirectoryGroupIds
            };
        }
    }
}
