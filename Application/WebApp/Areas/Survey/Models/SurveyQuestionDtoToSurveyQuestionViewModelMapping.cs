﻿using System;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyQuestionDtoToSurveyQuestionViewModelMapping : ClassMapping<SurveyQuestionDto, SurveyQuestionViewModel>
    {
        public SurveyQuestionDtoToSurveyQuestionViewModelMapping()
        {
            Mapping = d => new SurveyQuestionViewModel
            {
                Id = d.Id,
                Text = d.Text,
                Type = d.Type,
                IsRequired = d.IsRequired,
                Description = d.Description,
                VisibilityCondition = d.VisibilityCondition,
                Timestamp = d.Timestamp,
                SurveyDefinitionId = d.SurveyDefinitionId,
                FieldName = d.FieldName,
                FieldSize = d.FieldSize,
                Order = d.Order,
                Items = d.Items,
                DefaultAnswer = d.DefaultAnswer,
                DefaultAnswers = d.DefaultAnswer
            };
        }
    }
}
