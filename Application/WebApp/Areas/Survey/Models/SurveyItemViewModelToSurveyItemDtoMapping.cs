﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyItemViewModelToSurveyItemDtoMapping : ClassMapping<SurveyItemViewModel, SurveyItemDto>
    {
        public SurveyItemViewModelToSurveyItemDtoMapping()
        {
            Mapping = viewModel =>
            new SurveyItemDto
            {
                QuestionId = viewModel.QuestionId,
                IsRequired = viewModel.IsRequired,
                FieldName = viewModel.FieldName,
                SelectedAnswer = viewModel.SelectedAnswer,
                Answer = viewModel.Answer
            };
        }
    }
}
