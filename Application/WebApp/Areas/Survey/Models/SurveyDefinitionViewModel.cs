﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyDefinitionViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Order(0)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.NameLabel), typeof(SurveyDefinitionResources))]
        [StringLength(255)]
        public string Name { get; set; }

        [Order(0)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.DescriptionLabel), typeof(SurveyDefinitionResources))]
        public string Description { get; set; }

        [Order(0)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.AutoCloseLabel), typeof(SurveyDefinitionResources))]
        public bool ShouldAutoClose { get; set; }

        [Order(0)]
        [Multiline(5)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.WelcomeInstruction), typeof(SurveyDefinitionResources))]
        [StringLength(3000)]
        public string WelcomeInstruction { get; set; }

        [Order(0)]
        [Multiline(5)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.ThankYouMessage), typeof(SurveyDefinitionResources))]
        [StringLength(3000)]
        public string ThankYouMessage { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
