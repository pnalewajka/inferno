﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyQuestionViewModel
    {
        [Visibility(VisibilityScope.None)]
        [NonSortable]
        public long Id { get; set; }

        [Required]
        [StringLength(255)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [Order(1)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.TextLabel), typeof(SurveyDefinitionResources))]
        public string Text { get; set; }

        [Required]
        [StringLength(255)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [Order(2)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.FieldNameLabel), typeof(SurveyDefinitionResources))]
        public string FieldName { get; set; }

        [Required]
        [Order(3)]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.FieldSizeLabel), typeof(SurveyDefinitionResources))]
        public virtual SurveyFieldSize FieldSize { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.TypeLabel), typeof(SurveyDefinitionResources))]
        [Order(0)]
        public virtual SurveyQuestionType Type { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [Order(7)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.IsRequiredLabel), typeof(SurveyDefinitionResources))]
        public bool IsRequired { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Order(8)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.QuestionDescriptionLabel), typeof(SurveyDefinitionResources))]
        [Multiline(3)]
        [StringLength(1000)]
        public string Description { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [Order(9)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.VisibilityConditionLabel), typeof(SurveyDefinitionResources))]
        public string VisibilityCondition { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [NonSortable]
        [Order(4)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.ItemsLabel), typeof(SurveyDefinitionResources))]
        [Multiline(5)]
        public virtual string Items { get; set; }

        [Visibility(VisibilityScope.Form)]
        [NonSortable]
        [Order(5)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.DefaultAnswerLabel), typeof(SurveyDefinitionResources))]
        public virtual string DefaultAnswer { get; set; }

        [Visibility(VisibilityScope.Form)]
        [NonSortable]
        [Order(6)]
        [DisplayNameLocalized(nameof(SurveyDefinitionResources.DefaultAnswersLabel), typeof(SurveyDefinitionResources))]
        [Multiline(5)]
        public virtual string DefaultAnswers { get; set; }

        [Visibility(VisibilityScope.None)]
        [NonSortable]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        [NonSortable]
        public long SurveyDefinitionId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long Order { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
