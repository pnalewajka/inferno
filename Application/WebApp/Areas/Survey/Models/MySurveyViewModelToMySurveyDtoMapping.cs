﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class MySurveyViewModelToMySurveyDtoMapping : ClassMapping<MySurveyViewModel, MySurveyDto>
    {
        public MySurveyViewModelToMySurveyDtoMapping()
        {
            Mapping = v => new MySurveyDto {
                Id = v.Id,
                Timestamp = v.Timestamp
            };
        }
    }
}
