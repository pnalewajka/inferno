﻿using System.Linq;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Extensions;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class WhistleBlowingDtoToWhistleBlowingViewModelMapping : ClassMapping<WhistleBlowingDto, WhistleBlowingViewModel>
    {
        public WhistleBlowingDtoToWhistleBlowingViewModelMapping()
        {
            Mapping = d => new WhistleBlowingViewModel
            {
                Id = d.Id,
                Title = d.Title,
                Message = d.Message,
                MessageForGrid = HtmlHelperExtensions.StripHtml(d.Message),
                ShowUserName = d.ShowUserName,
                ModifiedByUserFullName = d.ModifiedByUserFullName,
                CreatedOn = d.CreatedOn,
                Documents =
                    d.Documents.Select(
                        p => new DocumentViewModel
                        {
                            ContentType = p.ContentType,
                            DocumentName = p.DocumentName,
                            DocumentId = p.DocumentId
                        }).ToList(),
                MessageStatus = d.MessageStatus,
            };
        }
    }
}
