﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class CustomQuestionDtoToCustomQuestionViewModelMapping : ClassMapping<QuestionItemDto, QuestionItemViewModel>
    {
        public CustomQuestionDtoToCustomQuestionViewModelMapping()
        {
            Mapping = dto =>
            new QuestionItemViewModel
            {
                Id = dto.Id,
                ItemText = dto.ItemText
            };
        }
    }
}
