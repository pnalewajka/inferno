﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class MySurveyViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(MySurveysResources.NameLabel), typeof(MySurveysResources))]
        [Visibility(VisibilityScope.Grid)]
        public string Name { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(MySurveysResources.DescriptionLabel), typeof(MySurveysResources))]
        [Visibility(VisibilityScope.Grid)]
        public string Description { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(MySurveysResources.OpenedOnLabel), typeof(MySurveysResources))]
        [Visibility(VisibilityScope.Grid)]
        public DateTime OpenedOn { get; set; }

        [DisplayNameLocalized(nameof(MySurveysResources.IsCompletedLabel), typeof(MySurveysResources))]
        [Order(3)]
        [Visibility(VisibilityScope.Grid)]
        public bool IsCompleted { get; set; }

        [DisplayNameLocalized(nameof(MySurveysResources.IsClosedLabel), typeof(MySurveysResources))]
        [Order(4)]
        [Visibility(VisibilityScope.Grid)]
        public bool IsClosed{ get; set; }

        public byte[] Timestamp { get; set; }
    }
}
