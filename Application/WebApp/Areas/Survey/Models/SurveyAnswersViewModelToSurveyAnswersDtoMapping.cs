﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyAnswersViewModelToSurveyAnswersDtoMapping : ClassMapping<SurveyAnswersViewModel, SurveyAnswersDto>
    {
        public SurveyAnswersViewModelToSurveyAnswersDtoMapping()
        {
            Mapping = viewModel =>
            new SurveyAnswersDto
            {
                SurveyId = viewModel.SurveyId,
                Items = viewModel.Items.Select(i =>
                new SurveyItemDto
                {
                    Answer = i.Answer,
                    IsRequired = i.IsRequired,
                    QuestionId = i.QuestionId,
                    FieldName = i.FieldName,
                    SelectedAnswer = i.SelectedAnswer
                }).ToList()
            };
        }
    }
}
