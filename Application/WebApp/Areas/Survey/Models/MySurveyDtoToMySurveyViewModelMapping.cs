﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class MySurveyDtoToMySurveyViewModelMapping : ClassMapping<MySurveyDto, MySurveyViewModel>
    {
        public MySurveyDtoToMySurveyViewModelMapping()
        {
            Mapping = d => new MySurveyViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                OpenedOn = d.OpenedOn,
                IsCompleted = d.IsCompleted,
                IsClosed = d.IsClosed,
                Timestamp = d.Timestamp
            };
        }
    }
}
