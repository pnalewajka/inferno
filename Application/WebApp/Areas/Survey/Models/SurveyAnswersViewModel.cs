﻿using Antlr.Runtime.Misc;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Survey.Models
{
    public class SurveyAnswersViewModel : IValidatableObject
    {
        public long SurveyId { get; set; }

        public IList<SurveyItemViewModel> Items { get; set; }

        private bool IsNotAnswered(SurveyItemViewModel item)
        {
            var answers = item.SelectedAnswer;
            Func<string, bool> isEmpty = answer => string.IsNullOrWhiteSpace(answer) || answer == "NULL";
            Func<bool> isNullAnswer = () => answers == null || answers.Count() < 1;
            Func<bool> isEmptyAnswer = () => answers.Count() == 1 && isEmpty(answers.Single());

            return isNullAnswer() || isEmptyAnswer();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            foreach (var item in Items)
            {
                if (item.IsRequired && item.IsVisible && IsNotAnswered(item))
                {
                    issues.Add(new ValidationResult(SurveyResources.SurveyAnswerIsRequiredValidationErrorMessage, new[] { item.FieldName }));
                }
            }

            return issues;
        }
    }
}
