﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Survey.Layout
{
    public interface IQuestionVisibilityRenderer
    {
        string Build(bool isPreview);
        void RegisterQuestion(string questionFieldName, string questionSelector, SurveyQuestionType questionType);
        void RegisterQuestionVisibilityCondition(string questionFieldName, string questionSelector, string questionIsVisibleSelector, string condition);
    }
}