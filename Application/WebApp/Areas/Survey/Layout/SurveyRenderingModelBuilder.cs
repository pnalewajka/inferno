﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Survey.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CheckboxGroup;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RadioGroup;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Survey.Layout
{
    public class SurveyRenderingModelBuilder
    {
        public const string QuestionTextLabelCssClassName = "questionText";
        public const int BootstrapDefaultLabelWidth = 12;

        private IQuestionVisibilityRenderer _visibilityRenderer;

        private void ApplyCommonSettings(ControlRenderingModel model, string fieldSelector, string questionIsVisibleSelector, QuestionViewModel question, ModelStateDictionary modelStateDictionary)
        {
            model.Id = $"questionId{question.Id}";
            model.Label = $"{question.Order}. {question.QuestionText}";
            model.CssClass = Presentation.Common.Helpers.HtmlMarkupHelper.AddCssClass(model.CssClass, QuestionTextLabelCssClassName);
            model.Name = fieldSelector;
            model.IsRequired = question.IsRequired;
            model.RequiredMarker = question.IsRequired;
            model.Order = question.Order;
            model.Hint = question.QuestionDescription;
            model.Visibility = string.IsNullOrWhiteSpace(question.VisibilityCondition) ? ControlVisibility.Show : ControlVisibility.Hide;
            model.LabelWidth = BootstrapDefaultLabelWidth;

            if (model.ControlWidth == 0)
            {
                model.ControlWidth = GetControlWidth(question);
            }
            _visibilityRenderer.RegisterQuestion(question.FieldName, fieldSelector, question.Type);

            if (!string.IsNullOrWhiteSpace(question.VisibilityCondition))
            {
                _visibilityRenderer.RegisterQuestionVisibilityCondition(question.FieldName, fieldSelector, questionIsVisibleSelector, question.VisibilityCondition);
            }

            var modelState = modelStateDictionary[question.FieldName];
            model.Errors = modelState != null ? modelState.Errors : new ModelErrorCollection();

            model.RequiredValueErrorMessage = SurveyResources.SurveyAnswerIsRequiredValidationErrorMessage;
        }

        private static int GetControlWidth(QuestionViewModel question)
        {
            switch (question.FieldSize)
            {

                case CrossCutting.Business.Enums.SurveyFieldSize.Small:

                    return 1;

                case CrossCutting.Business.Enums.SurveyFieldSize.Large:

                    return 6;

                default:

                    return 2;
            }
        }

        private IList<ListItem> CreateOptions(QuestionViewModel question, Action<ListItem> markAsSelectedItem)
        {
            var items = new List<ListItem>();

            if (question.Items == null)
                return items;

            int optionId = 0;

            foreach (var option in question.Items)
            {
                var item = new ListItem()
                {
                    Id = optionId++,
                    DisplayName = option
                };

                if (!string.IsNullOrWhiteSpace(question.Answers) && question.Answers.Contains(option))
                {
                    markAsSelectedItem(item);
                }
                items.Add(item);
            }

            return items;
        }

        private IList<ListItem> CreateNoAnswerOption(Action<ListItem> markAsSelectedItem)
        {
            var noAnswerOption = new ListItem { Id = 1, DisplayName = SurveyResources.NoAnswerText };
            markAsSelectedItem(noAnswerOption);

            return new[] { noAnswerOption };
        }

        private ControlRenderingModel CreateControlRenderingModel(string fieldSelector, string questionIsVisibleSelector, QuestionViewModel question, ModelStateDictionary modelStateDictionary, bool isPreview)
        {
            ControlRenderingModel model = null;
            switch (question.Type)
            {
                case SurveyQuestionType.Combo:
                    model = CreateDropdownRenderingModel(question, isPreview);

                    break;
                case SurveyQuestionType.Picker:
                    model = CreateValuePickerRenderingModel(question, isPreview);

                    break;
                case SurveyQuestionType.Checkbox:
                    model = CreateCheckboxGroupRenderingModel(question, isPreview);

                    break;
                case SurveyQuestionType.Radio:
                    model = CreateRadioGroupRenderingModel(question, isPreview);

                    break;
                case SurveyQuestionType.OpenQuestion:
                    model = CreateTextareaRenderingModel(question, isPreview);

                    break;
                default:
                    break;
            }
            ApplyCommonSettings(model, fieldSelector, questionIsVisibleSelector, question, modelStateDictionary);

            return model;
        }

        private bool ShouldDisplayNoAnswer(QuestionViewModel question, bool isPreview)
        {
            return isPreview && string.IsNullOrWhiteSpace(question.Answers);
        }

        private ControlRenderingModel CreateHiddenInputRenderingModel(string fieldName, object value)
        {
            var model = new HiddenInputRenderingModel();
            model.Name = fieldName;
            model.Value = value;

            return model;
        }

        private ControlRenderingModel CreateTextareaRenderingModel(QuestionViewModel question, bool isPreview)
        {
            var model = new TextareaRenderingModel();
            model.Rows = GetTextareaSize(question);
            model.ControlWidth = 6;

            if (ShouldDisplayNoAnswer(question, isPreview))
            {
                model.Value = SurveyResources.NoAnswerText;
            }
            else
            {
                model.Value = question.Answers;
            }

            return model;
        }

        private static byte GetTextareaSize(QuestionViewModel question)
        {
            switch (question.FieldSize)
            {

                case SurveyFieldSize.Small:

                    return 1;
                case SurveyFieldSize.Large:

                    return 5;
                default:

                    return 3;
            }
        }

        private ControlRenderingModel CreateCheckboxGroupRenderingModel(QuestionViewModel question, bool isPreview)
        {
            var model = new CheckboxGroupRenderingModel();

            if (ShouldDisplayNoAnswer(question, isPreview))
            {
                model.Items = CreateNoAnswerOption(markAsSelectedItem: item => model.Select(item));
            }
            else
            {
                model.Items = CreateOptions(question, markAsSelectedItem: item => model.Select(item));
            }

            return model;
        }

        private ControlRenderingModel CreateRadioGroupRenderingModel(QuestionViewModel question, bool isPreview)
        {
            var model = new RadioGroupRenderingModel();

            if (ShouldDisplayNoAnswer(question, isPreview))
            {
                model.Items = CreateNoAnswerOption(markAsSelectedItem: item => model.Value = item.Id);
            }
            else
            {
                model.Items = CreateOptions(question, markAsSelectedItem: item => model.Value = item.Id);
            }

            return model;
        }

        private ControlRenderingModel CreateDropdownRenderingModel(QuestionViewModel question, bool isPreview)
        {
            var model = new DropdownRenderingModel();

            if (ShouldDisplayNoAnswer(question, isPreview))
            {
                model.Items = CreateNoAnswerOption(markAsSelectedItem: item => model.Value = item.Id);
            }
            else
            {
                var items = CreateOptions(question, markAsSelectedItem: item => model.Value = item.Id);
                items.Insert(0, new ListItem { Id = null, DisplayName = "" });
                model.Items = items;
                model.EmptyItemBehavior = Presentation.Renderers.Bootstrap.Enums.EmptyItemBehavior.PresentOnlyOnInit;
            }

            return model;
        }

        private ControlRenderingModel CreateValuePickerRenderingModel(QuestionViewModel question, bool isPreview)
        {
            var model = new ValuePickerRenderingModel();

            model.IsMultiValue = true;
            model.IsClearButtonAvailable = !question.IsRequired;
            model.ValuePickerTypeIdentifier = "ValuePickers.SurveyCustomQuestion";
            model.Size = Size.Large;
            model.OnResolveUrlJavaScript = $"url += '&{GetQueryParameterName<QuestionContext>(q => q.QuestionId)}={question.Id}' + '&{GetQueryParameterName<QuestionContext>(q => q.IsQuestionPreview )}={question.IsQuestionPreview}'";

            if (ShouldDisplayNoAnswer(question, isPreview))
            {
                model.Items = CreateNoAnswerOption(markAsSelectedItem: item => { });
            }
            else
            {
                var selectedItems = new List<ListItem>();
                CreateOptions(question, markAsSelectedItem: item => selectedItems.Add(item));
                model.Items = selectedItems;
            }

            return model;
        }

        private string GetFieldName<TObject>(Expression<Func<TObject, object>> propertyNameExpression)
        {
            var propertyName = PropertyHelper.GetPropertyName(propertyNameExpression);
            return NamingConventionHelper.ConvertPascalCaseToCamelCase(propertyName);
        }

        private string GetQueryParameterName<TObject>(Expression<Func<TObject, object>> propertyNameExpression)
        {
            var propertyName = PropertyHelper.GetPropertyName(propertyNameExpression);
            return NamingConventionHelper.ConvertPascalCaseToHyphenated(propertyName);
        }

        private string GetSurveyAnswersFieldName<TObject>(Expression<Func<TObject, object>> propertyNameExpression, int count)
        {
            return $"{GetFieldName<SurveyAnswersViewModel>(vm => vm.Items)}[{count}].{GetFieldName(propertyNameExpression)}";
        }

        private void BuildQuestion(List<ControlRenderingModel> controls, QuestionViewModel question, ModelStateDictionary modelStateDictionary, int questionNumber, bool isPreview)
        {
            var questionIdFieldName = GetSurveyAnswersFieldName<SurveyItemViewModel>(vm => vm.QuestionId, questionNumber);
            controls.Add(CreateHiddenInputRenderingModel(questionIdFieldName, question.Id));

            var fieldNameFieldName = GetSurveyAnswersFieldName<SurveyItemViewModel>(vm => vm.FieldName, questionNumber);
            controls.Add(CreateHiddenInputRenderingModel(fieldNameFieldName, question.FieldName));

            var isRequiredFieldName = GetSurveyAnswersFieldName<SurveyItemViewModel>(vm => vm.IsRequired, questionNumber);
            controls.Add(CreateHiddenInputRenderingModel(isRequiredFieldName, question.IsRequired));

            var isVisibleFieldName = GetSurveyAnswersFieldName<SurveyItemViewModel>(vm => vm.IsVisible, questionNumber);
            controls.Add(CreateHiddenInputRenderingModel(isVisibleFieldName, true));

            var answersFieldName = GetSurveyAnswersFieldName<SurveyItemViewModel>(vm => vm.SelectedAnswer, questionNumber);
            controls.Add(CreateControlRenderingModel(answersFieldName, isVisibleFieldName, question, modelStateDictionary, isPreview));
        }

        public SurveyRenderingModelBuilder(IQuestionVisibilityRenderer visibilityRenderer)
        {
            _visibilityRenderer = visibilityRenderer;
        }

        public FormRenderingModel BuildRenderingModels(SurveyViewModel surveyViewModel, ModelStateDictionary modelStateDictionary)
        {
            if (surveyViewModel == null)
            {
                throw new ArgumentNullException(nameof(surveyViewModel));
            }

            var questions = surveyViewModel.Questions.OrderBy(q => q.Order).ToArray();
            var controls = new List<ControlRenderingModel>(questions.Length);

            var surveyIdFieldName = GetFieldName<SurveyAnswersViewModel>(vm => vm.SurveyId);
            controls.Add(CreateHiddenInputRenderingModel(surveyIdFieldName, surveyViewModel.Id));

            for (int questionNumber = 0; questionNumber < questions.Length; questionNumber++)
            {
                var question = questions[questionNumber];
                BuildQuestion(controls, question, modelStateDictionary, questionNumber, surveyViewModel.IsPreview);
            }
            surveyViewModel.VisibilityFunctions = _visibilityRenderer.Build(surveyViewModel.IsPreview);

            return new FormRenderingModel(controls, surveyViewModel);
        }
    }
}