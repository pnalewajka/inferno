﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Survey.Layout
{
    public class QuestionFieldMap
    {
        public string QuestionSelector { get; set; }

        public string QuestionFieldName { get; set; }

        public SurveyQuestionType QuestionType { get; set; }
    }
}