﻿namespace Smt.Atomic.WebApp.Areas.Survey.Layout
{
    public class QuestionVisibilityCondition
    {
        public string QuestionFieldName { get; set; }

        public string QuestionSelector { get; set; }

        public string QuestionIsVisibileSelector { get; set; }

        public string Condition { get; set; }
    }
}