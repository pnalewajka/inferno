﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smt.Atomic.WebApp.Areas.Survey.Layout
{
    public class JavascriptQuestionVisibilityRenderer : IQuestionVisibilityRenderer
    {
        private List<QuestionFieldMap> _fieldNameToSelectorMap;
        private List<QuestionVisibilityCondition> _questionVisibilityConditions;

        private static string _refreshQuestionsNumberingFunctionName = "refreshQuestionsNumbering";

        private static string BuildRefreshVisibilityFunction(List<string> checkVisibilityCommands)
        {
            var builder = new StringBuilder("function refreshVisibility() {");
            builder.AppendLine(string.Join(Environment.NewLine, checkVisibilityCommands));
            builder.AppendLine($"{_refreshQuestionsNumberingFunctionName}();");
            builder.AppendLine("};");

            return builder.ToString();
        }

        private static string BuildRefreshQuestionsNumberingFunction()
        {
            string body = 
                @"{
                    $('div.questionText:not(.collapse)>label:first-child')
                    .each(function(index) {
                        $(this).text($(this).text().replace(/^\d+/, index + 1));
                    });
                };";

            return $" function {_refreshQuestionsNumberingFunctionName}(){body}";
        }

        private static string BuildIsQuestionVisibleFunction(string functionName, string questionVisibilityCondition)
        {
            return $"function {functionName} (){{ return ({questionVisibilityCondition}) }}; ";
        }

        private static string BuildRefreshQuestionVisibilityCommand(string functionName, string questionSelector, string questionIsVisibleSelector)
        {
            string selectQuestion = $"$(\"[name='{questionSelector}']\").closest(\"div.form-group.row\")";
            string isQuestionVisible = $"{functionName}()";
            string showQuestion = ".removeClass(\"collapse\");";
            string hideQuestion = ".addClass(\"collapse\");";
            string assignTrueToIsVisibleField = $"$(\"[name='{questionIsVisibleSelector}']\").val(true);";
            string assignFalseToIsVisibleField = $"$(\"[name='{questionIsVisibleSelector}']\").val(false);";            

            var builder = new StringBuilder();
            builder
                .Append("if(").Append(isQuestionVisible).Append("){")
                .Append(selectQuestion).Append(showQuestion)
                .Append(assignTrueToIsVisibleField)
                .Append("} else {")
                .Append(selectQuestion).Append(hideQuestion)
                .Append(assignFalseToIsVisibleField)
                .Append("};");

            return builder.ToString();
        }

        private string BuildQuestionFieldList()
        {
            var fieldsCollection = _fieldNameToSelectorMap.Select(f => '"' + f.QuestionSelector + '"');
            string collection = $"var questionsList = [{string.Join(",", fieldsCollection)}]; ";

            return collection;
        }

        private string ParseVisibilityCondition(string condition, bool isPreview)
        {
            foreach (var fieldMap in _fieldNameToSelectorMap)
            {
                string questionName = string.Format("{{{0}}}", fieldMap.QuestionFieldName);

                if (condition.Contains(questionName))
                {
                    string answerSelectorFormat;

                    if (isPreview)
                    {
                        answerSelectorFormat = GetAnswerSelectorFormat();
                    }
                    else
                    {
                        answerSelectorFormat = GetQuestionSelectorFormat(fieldMap.QuestionType);
                    }

                    condition = condition.Replace(questionName, string.Format(answerSelectorFormat, fieldMap.QuestionSelector));
                }
            }

            return condition;
        }

        private static string GetQuestionSelectorFormat(SurveyQuestionType questionType)
        {
            switch (questionType)
            {
                case SurveyQuestionType.Combo:

                    return "$(\"[name='{0}'] option:selected\").text()";

                case SurveyQuestionType.Checkbox:
                case SurveyQuestionType.Radio:

                    return "$(\"[name='{0}']:checked\").parent().text()";

                case SurveyQuestionType.Picker:
                case SurveyQuestionType.OpenQuestion:

                    return "$(\"[name='{0}']\").val()";

                default:

                    return string.Empty;
            }
        }

        private static string GetAnswerSelectorFormat()
        {
            return "$('#' + $(\"[name='{0}']\").attr('id') + '-display-value').val()";
        }

        public JavascriptQuestionVisibilityRenderer()
        {
            _fieldNameToSelectorMap = new List<QuestionFieldMap>();
            _questionVisibilityConditions = new List<QuestionVisibilityCondition>();
        }

        public void RegisterQuestion(string questionFieldName, string questionSelector, SurveyQuestionType questionType)
        {
            _fieldNameToSelectorMap.Add(
                new QuestionFieldMap
                {
                    QuestionSelector = questionSelector,
                    QuestionFieldName = questionFieldName,
                    QuestionType = questionType
                });
        }

        public void RegisterQuestionVisibilityCondition(string questionFieldName, string questionSelector, string questionIsVisibleSelector, string condition)
        {
            _questionVisibilityConditions.Add(
                new QuestionVisibilityCondition
                {
                    QuestionFieldName = questionFieldName,
                    QuestionSelector = questionSelector,
                    QuestionIsVisibileSelector = questionIsVisibleSelector,
                    Condition = condition
                });
        }

        public string Build(bool isPreview)
        {
            string questionFieldList = BuildQuestionFieldList();

            var visibilityFunctionDeclarations = new List<string>();
            var checkVisibilityCommands = new List<string>();

            foreach (var visibilityCondition in _questionVisibilityConditions)
            {
                string questionVisibilityCondition = ParseVisibilityCondition(visibilityCondition.Condition, isPreview);
                string isQuestionVisibleFunctionName = $"is{visibilityCondition.QuestionFieldName}Visible";
                visibilityFunctionDeclarations.Add(
                    BuildIsQuestionVisibleFunction(isQuestionVisibleFunctionName, questionVisibilityCondition)
                    );
                checkVisibilityCommands.Add(
                    BuildRefreshQuestionVisibilityCommand(isQuestionVisibleFunctionName, visibilityCondition.QuestionSelector, visibilityCondition.QuestionIsVisibileSelector)
                );
            }
            string refreshVisibilityFunction = BuildRefreshVisibilityFunction(checkVisibilityCommands);
            string visibilityFunctions = string.Join(" ", visibilityFunctionDeclarations);
            string refreshQuestionsNumberingFunction = BuildRefreshQuestionsNumberingFunction();

            return questionFieldList + visibilityFunctions + refreshVisibilityFunction + refreshQuestionsNumberingFunction;
        }
    }
}