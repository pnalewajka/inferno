﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Business.Survey.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [Identifier("ValuePickers.SurveyCustomQuestion")]
    [AtomicAuthorize(SecurityRoleType.CanViewSurveyQuestions)]
    public class SurveyValuePickerController : CardIndexController<QuestionItemViewModel, QuestionItemDto, QuestionContext>
    {
        public SurveyValuePickerController(
            ISurveyQuestionItemCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = true;
        }
    }
}