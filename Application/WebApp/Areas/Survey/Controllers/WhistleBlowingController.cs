﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [AllowAnonymous]
    public class WhistleBlowingController : CardIndexController<WhistleBlowingViewModel, WhistleBlowingDto>
    {
        private IWhistleBlowingCardIndexDataService _cardIndexDataService;
        private readonly ISystemParameterService _parameterService;
        private readonly IPrincipalProvider _principalProvider;

        public WhistleBlowingController(
            IWhistleBlowingCardIndexDataService cardIndexDataService,
            IPrincipalProvider principalProvider,
            IBaseControllerDependencies baseControllerDependencies)
           : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _parameterService = baseControllerDependencies.SystemParameterService;
            _principalProvider = principalProvider;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.AddFormCustomization = c => c.GetControlByName<WhistleBlowingViewModel>(v => v.ModifiedByUserFullName).Visibility = ControlVisibility.Hide;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddWhistleBlowMessage;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewWhistleBlowingList;
            CardIndex.Settings.Title = WhistleBlowingResources.FormTitleLabel;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanSetAsReadWhistleBlowMessage,
                Text = WhistleBlowingResources.MarkAsReadLabel,
                OnClickAction = Url.UriActionPost(nameof(SetAsRead), KnownParameter.SelectedIds | KnownParameter.Context),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                }
            });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = WhistleBlowingResources.FilterLabel,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<WhistleBlowingMessageStatus>()
                },
            };
        }

        [AtomicAuthorize(SecurityRoleType.CanSetAsReadWhistleBlowMessage)]
        public ActionResult SetAsRead([ModelBinder(typeof(CommaSeparatedListModelBinder))]IList<long> ids)
        {
            var result = _cardIndexDataService.ToggleSetIsRead(true, ids);

            AddAlerts(result);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl((AtomicController)this, null);

            return new RedirectResult(listActionUrl);
        }

        public override ActionResult List(GridParameters parameters)
        {
            if(_principalProvider.Current.IsInRole(SecurityRoleType.CanViewWhistleBlowingList))
            {
                return base.List(parameters);
            }
            
            return Redirect("/");
        }

        [AtomicAuthorize(SecurityRoleType.CanAddWhistleBlowMessage)]
        [BreadcrumbBar("CardIndex/WhistleBlowing")]
        public override ActionResult Add()
        {           
            return base.Add();
        }

        [AtomicAuthorize(SecurityRoleType.CanAddWhistleBlowMessage)]
        [BreadcrumbBar("CardIndex/WhistleBlowing")]
        public override ActionResult Add(WhistleBlowingViewModel viewModelRecord)
        {
            return base.Add(viewModelRecord);
        }
    }
}