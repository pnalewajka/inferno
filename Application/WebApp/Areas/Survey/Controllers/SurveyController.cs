﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.WebApp.Areas.Survey.Helpers;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSurveys)]
    public class SurveyController : SubCardIndexController<SurveyViewModel, SurveyDto, SurveyConductController>
    {
        private readonly ITimeService _timeService;
        private readonly ISurveyCardIndexDataService _cardIndexDataService;
        private readonly ISurveyExportService _surveyExportService;
        private readonly ISurveyRespondentService _surveyRespondentService;

        public SurveyController(ISurveyCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            ISurveyExportService surveyExportService,
            ITimeService timeService, 
            ISurveyRespondentService surveyRespondentService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            Layout.Scripts.Add("~/Areas/Survey/Scripts/SurveyRespondent.js");

            Layout.Resources.AddFrom<SurveyResources>("AddRespondentsToSurveyConfirmation");

            _timeService = timeService;
            _surveyExportService = surveyExportService;
            _cardIndexDataService = cardIndexDataService;
            _surveyRespondentService = surveyRespondentService;

            CardIndex.Settings.Title = SurveyResources.SurveyControllerPageTitle;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;

            AddShowAnswersButton();
            AddAddRespondentsButton();
            AddExportButton();
        }

        private void AddShowAnswersButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyResources.ShowAnswers,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.CheckCircle,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyController.ShowAnswers), KnownParameter.SelectedId),
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void AddAddRespondentsButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyResources.AddRespondentsButtonText,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.PlusCircle,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always,
                },
                OnClickAction = new JavaScriptCallAction("SurveyRespondent.addRespondentsToSurvey(grid);")
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void AddExportButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyResources.ExportButtonText,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.ArrowCircleDown,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyController.ExportSurvey), KnownParameter.SelectedIds),
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        [HttpGet]
        [DeleteFile]
        [AtomicAuthorize(SecurityRoleType.CanViewSurveyConducts)]
        public FilePathResult ExportSurvey(long[] ids)
        {
            var surveys = _cardIndexDataService.GetSurveys(ids);

            return ExportSurvey(surveys);
        }

        [HttpGet]
        [DeleteFile]
        [AtomicAuthorize(SecurityRoleType.CanViewSurveyConducts)]
        public FilePathResult ExportSurveyConduct(long id)
        {
            var surveys = _cardIndexDataService.GetSurveysForConduct(id);

            return ExportSurvey(surveys);
        }

        private FilePathResult ExportSurvey(IEnumerable<SurveyDto> surveys)
        {
            var fileToExport = _surveyExportService.ExportToExcel(surveys);

            return new FilePathResult(fileToExport, MimeHelper.OfficeDocumentExcel)
            {
                FileDownloadName = string.Format("{0} {1:yyyy-MM-dd}.xlsx", "Surveys", _timeService.GetCurrentTime())
            };
        }

        [HttpGet]
        public ActionResult FillSurvey(long id)
        {
            var surveyId = _cardIndexDataService.GetSurveyForUser(id);

            return RedirectToAction(nameof(SurveyController.FillSurvey), RoutingHelper.GetControllerName<MySurveysController>(), new { id = surveyId });
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanViewSurveyConducts)]
        [BreadcrumbBar("CardIndex/SurveyShowAnswers")]
        public ActionResult ShowAnswers(long id)
        {
            var survey = _cardIndexDataService.GetSurvey(id);
            var viewModel = SurveyViewModel.Create(survey, displayAnswers: true);
            SetPageTitle(viewModel);

            return View(viewModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewSurveyConducts)]
        public ActionResult AddRespondentsToSurveyConduct(long surveyConductId, IList<long> respondentIds)
        {
            bool respondentsAdded = _surveyRespondentService.AddRespondentsToSurveyConduct(surveyConductId, respondentIds);
            if (respondentsAdded)
            {
                AddAlert(AlertType.Success, SurveyResources.RespondentsWereAddedToSurvey);
            }
            else
            {
                AddAlert(AlertType.Information, SurveyResources.RespondentsWerentAddedToSurvey);
            }
            return RedirectToAction("List", "Survey", new RouteValueDictionary { { RoutingHelper.ParentIdParameterName, surveyConductId } });
        }

        private void SetPageTitle(SurveyViewModel viewModel)
        {
            var header = $"{viewModel.Name} - {viewModel.RespondentFirstName} {viewModel.RespondentLastName}";
            Layout.PageTitle = header;
            Layout.PageHeader = header;
        }

        public override ActionResult Add()
        {
            throw new NotImplementedException();
        }

        public override ActionResult Add(SurveyViewModel viewModelRecord)
        {
            throw new NotImplementedException();
        }

        public override ActionResult Edit(long id)
        {
            throw new NotImplementedException();
        }

        public override ActionResult Edit(SurveyViewModel viewModelRecord)
        {
            throw new NotImplementedException();
        }

        public override ActionResult Delete([ModelBinder(typeof(CommaSeparatedListModelBinder))] IList<long> ids)
        {
            throw new NotImplementedException();
        }
    }
}