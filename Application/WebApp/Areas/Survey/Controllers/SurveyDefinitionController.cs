﻿using System.Web.Mvc;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Survey.Helpers;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSurveyDefinitions)]
    public class SurveyDefinitionController : CardIndexController<SurveyDefinitionViewModel, SurveyDefinitionDto>
    {
        private readonly ISurveyDefinitionCardIndexDataService _cardIndexDataService;

        public SurveyDefinitionController(ISurveyDefinitionCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = SurveyDefinitionResources.SurveyDefinitionControllerPageTitle;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSurveyDefinitions;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.Grid;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewSurveyDefinitions;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSurveyDefinitions;
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
            };
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSurveyDefinitions;
            CardIndex.Settings.DeleteButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.AtLeastOneRowSelected,
            };

            AddManageQuestionsButton();
            AddSurveyPreviewButton();
        }

        private void AddSurveyPreviewButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyDefinitionResources.SurveyDefinitionPreview,
                RequiredRole = SecurityRoleType.CanViewSurveyConducts,
                Icon = FontAwesome.FileTextO,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyDefinitionController.Preview), KnownParameter.SelectedId),
            };
            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void AddManageQuestionsButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyDefinitionResources.ManageQuestions,
                RequiredRole = SecurityRoleType.CanEditSurveyDefinitions,
                Icon = FontAwesome.Cog,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyQuestionController.List), RoutingHelper.GetControllerName<SurveyQuestionController>(), KnownParameter.ParentId),
            };
            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        [BreadcrumbBar("CardIndex/SurveyDefinitionPreview")]
        [HttpGet]
        public ActionResult Preview(long? id)
        {
            id = TryGetSurveyDefinitionId(id);

            if (id == null)
            {
                return HttpNotFound();
            }

            var survey = _cardIndexDataService.CreateSurveyPreview(id.Value);
            var viewModel = SurveyViewModel.Create(survey, isSurveyPreview: true);
            viewModel.ShouldDisplayThankYouPopup = !string.IsNullOrWhiteSpace(survey.ThankYouMessage);
            SetPageTitle(viewModel);
            LayoutHelper.ApplySurveyStyles(Layout);

            return View(viewModel);
        }

        private long? TryGetSurveyDefinitionId(long? id)
        {
            if (id.HasValue)
            {
                return id;
            }

            if (Context != null)
            {
                return Context.ParentId;
            }

            return null;
        }

        private void SetPageTitle(SurveyViewModel viewModel)
        {
            Layout.PageTitle = viewModel.Name;
            Layout.PageHeader = viewModel.Name;
        }
    }
}