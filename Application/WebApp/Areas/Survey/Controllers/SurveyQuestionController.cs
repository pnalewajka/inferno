﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSurveyQuestions)]
    public class SurveyQuestionController : SubCardIndexController<SurveyQuestionViewModel, SurveyQuestionDto, SurveyDefinitionController>
    {
        private const string SurveyQuestionTypeParameterName = "question-type";
        private readonly ISurveyQuestionCardIndexDataService _surveyQuestionCardIndexDataService;

        public SurveyQuestionController(
            ISurveyQuestionCardIndexDataService surveyQuestionCardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(surveyQuestionCardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = true;

            _surveyQuestionCardIndexDataService = surveyQuestionCardIndexDataService;

            CardIndex.Settings.Title = SurveyQuestionResources.SurveyQuestionControllerPageTitle;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSurveyQuestions;

            CardIndex.Settings.AddButton.OnClickAction = new DropdownAction()
            {
                Items = CreateAddQuestionButtons().ToList()
            };

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            var upButton = new RowButton()
            {
                RequiredRole = SecurityRoleType.CanEditSurveyDefinitions,
                Text = SurveyDefinitionResources.MoveUpTitle,
                OnClickAction = Url.UriActionPost(nameof(SurveyQuestionController.MoveUp), RoutingHelper.GetControllerName<SurveyQuestionController>(), KnownParameter.SelectedId),
                Icon = FontAwesome.ArrowCircleUp
            };

            var downButton = new RowButton()
            {
                RequiredRole = SecurityRoleType.CanEditSurveyDefinitions,
                Text = SurveyDefinitionResources.MoveDownTitle,
                OnClickAction = Url.UriActionPost(nameof(SurveyQuestionController.MoveDown), RoutingHelper.GetControllerName<SurveyQuestionController>(), KnownParameter.SelectedId),
                Icon = FontAwesome.ArrowCircleDown
            };

            CardIndex.Settings.RowButtons.Add(upButton);
            CardIndex.Settings.RowButtons.Add(downButton);

            CardIndex.Settings.AddFormCustomization = FormCustomization;
            CardIndex.Settings.EditFormCustomization = FormCustomization;

            AddSurveyDefinitionPreviewButton();
        }

        private void FormCustomization(FormRenderingModel model)
        {
            SurveyQuestionType questionType;
            var controlRenderingModel = (DropdownRenderingModel)model.GetControlByName<SurveyQuestionViewModel>(m => m.Type);

            if (controlRenderingModel.Value != null)
            {
                questionType = (SurveyQuestionType) controlRenderingModel.Value;
            }
            else
            {
                questionType = GetQuestionType();
            }

            model.GetControlByName<SurveyQuestionViewModel>(m => m.Items).Visibility =
                questionType == SurveyQuestionType.OpenQuestion ? ControlVisibility.Hide : ControlVisibility.Show;

            model.GetControlByName<SurveyQuestionViewModel>(m => m.Items).IsRequired = questionType != SurveyQuestionType.OpenQuestion;

            AdjustVisibilityOfDefaultAnswerControls(model, questionType);
        }

        private void AdjustVisibilityOfDefaultAnswerControls(FormRenderingModel model, SurveyQuestionType questionType)
        {
            if (DefaultAnswersFieldShouldBeProcess(questionType))
            {
                model.GetControlByName<SurveyQuestionViewModel>(m => m.DefaultAnswer).Visibility = ControlVisibility.Hide;
            }
            else
            {
                model.GetControlByName<SurveyQuestionViewModel>(m => m.DefaultAnswers).Visibility = ControlVisibility.Hide;
            }
        }

        private bool DefaultAnswersFieldShouldBeProcess(SurveyQuestionType questionType)
        {
            return questionType == SurveyQuestionType.Checkbox || questionType == SurveyQuestionType.Picker;
        }

        private void AddSurveyDefinitionPreviewButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyDefinitionResources.SurveyDefinitionPreview,
                RequiredRole = SecurityRoleType.CanViewSurveyDefinitions,
                Icon = FontAwesome.FileTextO,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyDefinitionController.Preview), RoutingHelper.GetControllerName<SurveyDefinitionController>(), KnownParameter.Context),
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private IEnumerable<ICommandButton> CreateAddQuestionButtons()
        {
            var surveyQuestionTypes = Enum.GetValues(typeof(SurveyQuestionType)).Cast<SurveyQuestionType>();
            foreach (var surveyQuestionType in surveyQuestionTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(surveyQuestionType.ToString());
                yield return new CommandButton()
                {
                    RequiredRole = SecurityRoleType.CanAddSurveyQuestions,
                    Text = surveyQuestionType.GetDescription(),
                    OnClickAction = Url.UriActionGet("Add", KnownParameter.Context, SurveyQuestionTypeParameterName, typeValueName),
                };
            }
        }

        private object GetDefaultNewRecord()
        {
            long surveyDefinitionId = Context.ParentId;
            var questionType = GetQuestionType();

            return new SurveyQuestionViewModel
            {
                Type = questionType,
                SurveyDefinitionId = surveyDefinitionId
            };
        }

        private SurveyQuestionType GetQuestionType()
        {
            SurveyQuestionType? questionType = null;

            if (Request.RequestType == WebRequestMethods.Http.Get)
            {
                var questionTypeName = Request.QueryString[SurveyQuestionTypeParameterName];
                questionType = EnumHelper.GetEnumValueOrDefault<SurveyQuestionType>(questionTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);
            }
            else if (Request.RequestType == WebRequestMethods.Http.Post)
            {
                var typeName = PropertyHelper.GetPropertyName<SurveyQuestionViewModel>(m => m.Type);
                questionType = (SurveyQuestionType)int.Parse(Request.Form[typeName]);
            }

            if (!questionType.HasValue)
            {
                throw new ArgumentOutOfRangeException(nameof(questionType));
            }

            return questionType.Value;
        }

        private void ValidateSurveyQuestion(SurveyQuestionType questionType, SurveyQuestionViewModel viewModel)
        {
            if (questionType == SurveyQuestionType.OpenQuestion && viewModel.Items != null)
            {
                throw new InvalidDataException(SurveyQuestionResources.DataManipulationAttempt);
            }
        }

        [HttpPost]
        public override ActionResult Add(SurveyQuestionViewModel viewModelRecord)
        {
            ValidateQuestionDefaultAnswer(GetQuestionType(), viewModelRecord);

            ValidateSurveyQuestion(GetQuestionType(), viewModelRecord);

            return base.Add(viewModelRecord);
        }

        [HttpPost]
        public override ActionResult Edit(SurveyQuestionViewModel viewModelRecord)
        {
            if (DefaultAnswersFieldShouldBeProcess(GetQuestionType()))
            {
                viewModelRecord.DefaultAnswer = null;
            }
            else
            {
                viewModelRecord.DefaultAnswers = null;
            }

            ValidateQuestionDefaultAnswer(GetQuestionType(), viewModelRecord);

            ValidateSurveyQuestion(GetQuestionType(), viewModelRecord);

            return base.Edit(viewModelRecord);
        }

        private void ValidateQuestionDefaultAnswer(SurveyQuestionType questionType, SurveyQuestionViewModel viewModelRecord)
        {
            if (questionType == SurveyQuestionType.Combo || questionType == SurveyQuestionType.Radio)
            {
                if (viewModelRecord.DefaultAnswer != null && !AreDefaultAnswersOnItemsList(viewModelRecord.DefaultAnswer, viewModelRecord.Items))
                {
                    ModelState.AddModelError(
                        PropertyHelper.GetPropertyName<SurveyQuestionViewModel>(m => m.DefaultAnswer),
                        SurveyQuestionResources.DefaultAnswerOfSurveyQuestionIsMissingOnItemsList);
                }
            }
            else if (questionType == SurveyQuestionType.Checkbox || questionType == SurveyQuestionType.Picker)
            {
                if (viewModelRecord.DefaultAnswers != null && !AreDefaultAnswersOnItemsList(viewModelRecord.DefaultAnswers, viewModelRecord.Items))
                {
                    ModelState.AddModelError(
                        PropertyHelper.GetPropertyName<SurveyQuestionViewModel>(m => m.DefaultAnswers),
                        SurveyQuestionResources.DefaultAnswerOfSurveyQuestionIsMissingOnItemsList);
                }
            }
        }

        private bool AreDefaultAnswersOnItemsList(string defaultAnswers, string availableItems)
        {
            return defaultAnswers.Split(new[] { Environment.NewLine }, StringSplitOptions.None)
                  .Except(availableItems.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                  .IsNullOrEmpty();
        }

        [HttpPost]
        public ActionResult MoveUp(long id)
        {
            _surveyQuestionCardIndexDataService.MoveQuestionUpOrder(id);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);
            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        public ActionResult MoveDown(long id)
        {
            _surveyQuestionCardIndexDataService.MoveQuestionDownOrder(id);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);
            return new RedirectResult(listActionUrl);
        }
    }
}