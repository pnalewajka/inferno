﻿using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [Identifier("ValuePickers.SurveyDefinition")]
    public class SurveyDefinitionPickerController : SurveyDefinitionController
    {
        private const string MinimalViewId = "minimal";

        public SurveyDefinitionPickerController(
            ISurveyDefinitionCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<SurveyDefinitionViewModel>(SurveyDefinitionResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<SurveyDefinitionViewModel, object>>>
            {
                c => c.Name,
                x => x.Description
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}