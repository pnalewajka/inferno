﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Business.Survey.Helpers;
using Smt.Atomic.WebApp.Areas.Survey.Helpers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSurveyConducts)]
    public class SurveyConductController : CardIndexController<SurveyConductViewModel, SurveyConductDto>
    {
        private readonly ISurveyConductCardIndexDataService _cardIndexDataService;

        public SurveyConductController(ISurveyConductCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = SurveyConductResources.SurveyConductControllerPageTitle;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewSurveyConducts;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanOpenSurveyConduct;

            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.ViewButton.IsDefault = false;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CreateManageSurveysButton();
            CreateCloseSurveyButton();
            CreateExportButton();
            CreateUrgeRespondentsButton();
            CreateGenerateLinkButton();

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = SurveyConductResources.FilterLabel,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.MySurveys, DisplayName = SurveyConductResources.MySurveyLabel },
                        new Filter { Code = FilterCodes.AllSurveys, DisplayName = SurveyConductResources.AllSurveyLabel }
                    }
                }
            };
        }

        private void CreateCloseSurveyButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyConductResources.CloseSurvey,
                RequiredRole = SecurityRoleType.CanCloseSurveyConduct,
                Icon = FontAwesome.TimesCircle,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.isClosed!=true"
                },
                OnClickAction = Url.UriActionPost(nameof(SurveyConductController.Close), RoutingHelper.GetControllerName<SurveyConductController>(), KnownParameter.SelectedId),
            };
            button.Confirmation.IsRequired = true;
            button.Confirmation.Message = SurveyConductResources.SurveyConductClose_ConfirmationMessage;

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void CreateExportButton()
        {
            var showAnswers = new ToolbarButton()
            {
                Text = SurveyConductResources.ExportButtonText,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.ArrowCircleDown,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyController.ExportSurveyConduct), RoutingHelper.GetControllerName<SurveyController>(), KnownParameter.SelectedId),
            };

            CardIndex.Settings.ToolbarButtons.Add(showAnswers);
        }

        private void CreateManageSurveysButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyConductResources.ManageSurveys,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.Cog,
                IsDefault = true,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                OnClickAction = Url.UriActionGet(nameof(SurveyController.List), RoutingHelper.GetControllerName<SurveyController>(), KnownParameter.ParentId),
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void CreateUrgeRespondentsButton()
        {
            var button = new ToolbarButton()
            {
                Text = SurveyConductResources.UrgeRespondentsButtonText,
                RequiredRole = SecurityRoleType.CanCloseSurveyConduct,
                Icon = FontAwesome.ExclamationCircle,
                IsDefault = false,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.isClosed!=true"
                },
                OnClickAction = Url.UriActionPost(nameof(SurveyConductController.UrgeRespondents), RoutingHelper.GetControllerName<SurveyConductController>(), KnownParameter.SelectedId),
            };
            button.Confirmation.IsRequired = true;
            button.Confirmation.Message = SurveyConductResources.UrgeRespondents_ConfirmationMessage;

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void CreateGenerateLinkButton()
        {
            var fillSurveyUrl = Url.Action(nameof(SurveyController.FillSurvey), RoutingHelper.GetControllerName<SurveyController>());

            var button = new ToolbarButton()
            {
                Text = SurveyConductResources.GenerateLink,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.ExternalLink,
                IsDefault = false,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.isClosed!=true"
                },
                OnClickAction = new JavaScriptCallAction($"SurveyConduct.generateSurveyLink(grid, '{fillSurveyUrl}')"),
            };

            CardIndex.Settings.ToolbarButtons.Add(button);

            Layout.Resources.AddFrom<SurveyConductResources>(nameof(SurveyConductResources.GenerateLink));
            Layout.Scripts.Add("~/Areas/Survey/Scripts/SurveyConduct.js");            
        }

        [HttpPost]
        public ActionResult UrgeRespondents(long id)
        {
            var surveyUrl = Url.Action(nameof(SurveyController.FillSurvey), RoutingHelper.GetControllerName<SurveyController>(), new { Id = id }, Request.Url.Scheme);
            _cardIndexDataService.UrgeRespondents(id, surveyUrl);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        public ActionResult Close(long id)
        {
            _cardIndexDataService.CloseSurveyConduct(id);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpGet]
        public override ActionResult Edit(long id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public override ActionResult Edit(SurveyConductViewModel viewModelRecord)
        {
            throw new NotImplementedException();
        }
    }
}