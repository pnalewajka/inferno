﻿using System.Web.Mvc;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Exceptions;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Survey.Helpers;
using Smt.Atomic.WebApp.Areas.Survey.Models;
using Smt.Atomic.WebApp.Areas.Survey.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Survey.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSurveys)]
    public class MySurveysController : ReadOnlyCardIndexController<MySurveyViewModel, MySurveyDto>
    {
        private readonly ISurveyCardIndexDataService _surveyCardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMappingFactory _classMappingFactory;

        public MySurveysController(
            IMySurveyCardIndexDataService mySurveyCardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            ISurveyCardIndexDataService surveyCardIndexDataService,
            IPrincipalProvider principalProvider)
            : base(mySurveyCardIndexDataService, baseControllerDependencies)
        {
            _surveyCardIndexDataService = surveyCardIndexDataService;
            _principalProvider = principalProvider;
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;

            CardIndex.Settings.Title = MySurveysResources.MySurveysControllerPageTitle;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            CreateCommandButtons();
        }

        private void CreateCommandButtons()
        {
            var fillSurveyButton = new ToolbarButton()
            {
                Text = SurveyResources.FillSurvey,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.CheckCircle,
                IsDefault = true,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.isClosed!=true"
                },
                OnClickAction = Url.UriActionGet(nameof(MySurveysController.FillSurvey), KnownParameter.SelectedId),
            };

            CardIndex.Settings.ToolbarButtons.Add(fillSurveyButton);

            var showAnswersButton = new ToolbarButton()
            {
                Text = SurveyResources.ShowAnswers,
                RequiredRole = SecurityRoleType.CanViewSurveys,
                Icon = FontAwesome.CheckCircle,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.isFinished!=false"
                },
                OnClickAction = Url.UriActionGet(nameof(MySurveysController.ShowAnswers), KnownParameter.SelectedId),
            };

            CardIndex.Settings.ToolbarButtons.Add(showAnswersButton);
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/FillSurvey")]
        public ActionResult FillSurvey(long id)
        {
            var survey = _surveyCardIndexDataService.GetSurvey(id);

            if (survey.RespondentId != _principalProvider.Current.Id)
            {
                return HttpNotFound();
            }

            if (survey.IsSurveyClosed)
            {
                throw new SurveyClosedException();
            }

            var viewModel = SurveyViewModel.Create(survey);
            SetPageTitle(viewModel);
            LayoutHelper.ApplySurveyStyles(Layout);
            viewModel.ShouldDisplayWelcomeInstructionPopup = viewModel.ShouldDisplayWelcomeInstruction;

            return View(viewModel);
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/FillSurvey")]
        public ActionResult FillSurvey(SurveyAnswersViewModel viewModel)
        {
            var answers = MapAnswers(viewModel);
            var survey = _surveyCardIndexDataService.ParseAnswers(answers);

            if (!ModelState.IsValid)
            {
                var surveyViewModel = SurveyViewModel.Create(survey);
                surveyViewModel.ShouldDisplayWelcomeInstruction = false;
                SetPageTitle(surveyViewModel);
                LayoutHelper.ApplySurveyStyles(Layout);

                return View(surveyViewModel);
            }

            _surveyCardIndexDataService.FillSurvey(survey, shouldFinishSurvey: true);
            SendEmailConfirmation(survey);

            if (!string.IsNullOrWhiteSpace(survey.ThankYouMessage))
            {
                var surveyViewModel = SurveyViewModel.Create(survey);
                SetPageTitle(surveyViewModel);
                LayoutHelper.ApplySurveyStyles(Layout);
                surveyViewModel.ShouldDisplayThankYouPopup = true;

                return View(surveyViewModel);
            }

            return RedirectToAction(nameof(MySurveysController.List), RoutingHelper.GetControllerName<MySurveysController>());
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/SurveyShowAnswers")]
        [AtomicAuthorize]
        public ActionResult ShowAnswers(long id)
        {
            var survey = _surveyCardIndexDataService.GetSurvey(id);

            if (survey.RespondentId != _principalProvider.Current.Id)
            {
                return HttpNotFound();
            }

            var viewModel = SurveyViewModel.Create(survey, displayAnswers: true);
            SetPageTitle(viewModel);
            LayoutHelper.ApplySurveyStyles(Layout);

            return View(viewModel);
        }

        private SurveyAnswersDto MapAnswers(SurveyAnswersViewModel viewModel)
        {
            var mapper = _classMappingFactory.CreateMapping<SurveyAnswersViewModel, SurveyAnswersDto>();
            return mapper.CreateFromSource(viewModel);
        }

        private void SetPageTitle(SurveyViewModel viewModel)
        {
            Layout.PageTitle = viewModel.Name;
            Layout.PageHeader = viewModel.Name;
        }

        private void SendEmailConfirmation(SurveyDto survey)
        {
            string surveyAnswersUrl = 
                Url.Action(
                    nameof(MySurveysController.ShowAnswers), 
                    RoutingHelper.GetControllerName<MySurveysController>(), 
                    new { Id = survey.Id }, Request.Url.Scheme);

            string continueSurveyUrl = 
                survey.ShouldAutoClose ? null : 
                Url.Action(
                    nameof(MySurveysController.FillSurvey), 
                    RoutingHelper.GetControllerName<MySurveysController>(), 
                    new { Id = survey.Id }, Request.Url.Scheme);

            _surveyCardIndexDataService.SendServeyConfirmationAfterSubmit(survey.Id, surveyAnswersUrl, continueSurveyUrl);
        }
    }
}