﻿module SurveyVisibility {
    function onQuestionAnswerChanged() {
        (window as any)["refreshVisibility"]();
    };

    export function questionListener(questionSelector: string) {
        $("[name='" + questionSelector + "']").change(function () {
            onQuestionAnswerChanged();
        });
    }
}

$(() => {

    (window as any)["questionsList"].forEach(function (item: string) { SurveyVisibility.questionListener(item); });
    (window as any)["refreshVisibility"]();
});