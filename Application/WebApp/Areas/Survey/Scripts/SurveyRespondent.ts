﻿module SurveyRespondent {
    export function addRespondentsToSurvey(grid: Grid.GridComponent) {
        var context = grid.gridContext;
        var parentId = UrlHelper.getQueryParameter(context, 'parent-id');
        var urlResolveCallback = ValuePicker.getContextResolveUrlCallback({ "parent-id": parentId, "should-exclude": true });

        ValuePicker.selectMultiple("ValuePickers.Users", (records: Grid.IRow[]) => {
            if (records.length === 0) {
                return;
            }

            CommonDialogs.confirm(Globals.resources.AddRespondentsToSurveyConfirmation + Utils.getHtmlList(records),
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                    if (eventArgs.isOk) {
                        var ids = records.map((v) => v.id.toString()).join(",");
                        var url = Strings.format("../Survey/AddRespondentsToSurveyConduct?surveyConductId={0}&respondentIds={1}", parentId, ids);
                        Forms.submit(url, "POST");
                    }
                });
        }, urlResolveCallback);
    }
}