﻿module SurveyConduct {

    export function generateSurveyLink(grid: Grid.GridComponent, url: string): void {
        var selectedItem = grid.getSelectedRowData<Grid.IRow>().map(r => r.id)[0];
        var fillSurveyUrl = `${window.location.origin}${url}/${selectedItem}`;
        CommonDialogs.alert({
            htmlMessage: fillSurveyUrl,
            title: Globals.resources.GenerateLink,
            eventHandler: undefined,
            iconName: "",
            okButtonText: ""
        });
    }
}