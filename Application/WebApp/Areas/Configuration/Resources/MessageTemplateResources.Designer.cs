﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Configuration.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class MessageTemplateResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MessageTemplateResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Configuration.Resources.MessageTemplateResources", typeof(MessageTemplateResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to by content type.
        /// </summary>
        internal static string AddMessageTemplateOfTypeHeaderText {
            get {
                return ResourceManager.GetString("AddMessageTemplateOfTypeHeaderText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The templates that you selected were sent to your email address.
        /// </summary>
        internal static string EmailsWereSent {
            get {
                return ResourceManager.GetString("EmailsWereSent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Export as HTML.
        /// </summary>
        internal static string ExportAsHtmls {
            get {
                return ResourceManager.GetString("ExportAsHtmls", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code.
        /// </summary>
        internal static string MessageTemplateCodeLabel {
            get {
                return ResourceManager.GetString("MessageTemplateCodeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Content.
        /// </summary>
        internal static string MessageTemplateContentLabel {
            get {
                return ResourceManager.GetString("MessageTemplateContentLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Content Type.
        /// </summary>
        internal static string MessageTemplateContentTypeLabel {
            get {
                return ResourceManager.GetString("MessageTemplateContentTypeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Message Templates.
        /// </summary>
        internal static string MessageTemplateControllerTitle {
            get {
                return ResourceManager.GetString("MessageTemplateControllerTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        internal static string MessageTemplateDescriptionLabel {
            get {
                return ResourceManager.GetString("MessageTemplateDescriptionLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modified On.
        /// </summary>
        internal static string MessageTemplateModifiedOnLabel {
            get {
                return ResourceManager.GetString("MessageTemplateModifiedOnLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refresh Definitions.
        /// </summary>
        internal static string RefreshTemplateDefinitions {
            get {
                return ResourceManager.GetString("RefreshTemplateDefinitions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send as Emails.
        /// </summary>
        internal static string SendAsEmails {
            get {
                return ResourceManager.GetString("SendAsEmails", resourceCulture);
            }
        }
    }
}
