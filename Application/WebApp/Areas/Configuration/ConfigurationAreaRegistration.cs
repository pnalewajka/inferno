﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Configuration
{
    public class ConfigurationAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Configuration";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Configuration_default",
                "Configuration/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}