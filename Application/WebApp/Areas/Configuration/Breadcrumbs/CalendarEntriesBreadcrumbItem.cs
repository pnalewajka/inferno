﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Configuration.Breadcrumbs
{
    public class CalendarEntriesBreadcrumbItem : BreadcrumbItem
    {
        public CalendarEntriesBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = CalendarResources.CalendarEntryControllerTitle;
        }
    }
}