using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCalendars)]
    public class CalendarController : CardIndexController<CalendarViewModel, CalendarDto>
    {
        public CalendarController(
            ICalendarCardIndexService calendarGroupCardIndexService, 
            IBaseControllerDependencies baseControllerDependencies, ITimeService timeService)
            : base(calendarGroupCardIndexService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = CalendarResources.CalendarControllerTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddCalendars;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewCalendars;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCalendars;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteCalendars;

            CardIndex.Settings.ViewButton.IsDefault = false;
            CardIndex.Settings.EditButton.IsDefault = false;

            var currentYear = new FilterYearDto {Year = timeService.GetCurrentDate().Year};

            var entriesButton = new ToolbarButton
            {
                Text = CalendarResources.EntriesButtonText,
                OnClickAction = Url.UriActionGet<CalendarEntriesController>(c => c.List(null), KnownParameter.ParentId, GridParameterModelBinder.FilterParameterUrlName, currentYear.GetFilterCode()),
                Group = "Management",
                Icon = FontAwesome.Calendar,
                IsDefault = true,
                RequiredRole = SecurityRoleType.CanViewCalendars,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(entriesButton);
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var calendar = (CalendarViewModel)base.GetViewModelFromContext(childContext);

            var panelTitle = string.IsNullOrWhiteSpace(calendar.Description)
                ? calendar.Code
                : $"{calendar.Description} ({calendar.Code})";

            var panel = new PanelViewModel(panelTitle);

            return panel;
        }
    }
}