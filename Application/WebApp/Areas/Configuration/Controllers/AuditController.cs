﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanManageAudit)]
    public class AuditController : ReadOnlyCardIndexController<AuditTableViewModel, AuditTableDto, ParentIdContext>
    {
        private readonly IAuditService _auditService;
        private bool IsAuditEnabled { get; set; }

        public AuditController(IAuditTableCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies, IAuditService auditService)
           : base(cardIndexDataService, baseControllerDependencies)
        {
            _auditService = auditService;

            IsAuditEnabled = _auditService.IsAuditEnabled();

            CardIndex.Settings.Title = AuditResources.AuditControllerTitle;
            CardIndex.Settings.EmptyListMessage = AuditResources.AuditIsDisabled;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;

            if (IsAuditEnabled)
            {
                var enableAuditOnTableButton = new ToolbarButton
                {
                    Text = AuditResources.EnableAuditOnTablesButtonText,
                    OnClickAction = Url.UriActionPost(nameof(EnableAuditOnTables), KnownParameter.SelectedIds),
                    Icon = FontAwesome.ToggleOn,
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.AtLeastOneRowSelected,
                        Predicate = "row.isAuditEnabled!=true"
                    },
                    Confirmation = new Confirmation
                    {
                        Message = AuditResources.ConfirmMessageEnableAuditOnTable,
                        IsRequired = true
                    },
                    Group = "TableAudit"
                };

                var disableAuditOnTableButton = new ToolbarButton
                {
                    Text = AuditResources.DisableAuditOnTablesButtonText,
                    OnClickAction = Url.UriActionPost(nameof(DisableAuditOnTables), KnownParameter.SelectedIds),
                    Icon = FontAwesome.ToggleOff,
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.AtLeastOneRowSelected,
                        Predicate = "row.isAuditEnabled!=false"
                    },
                    Confirmation = new Confirmation
                    {
                        Message = AuditResources.ConfirmMessageDisableAuditOnTable,
                        IsRequired = true
                    },
                    Group = "TableAudit"
                };

                var disableAuditButton = new ToolbarButton
                {
                    Text = AuditResources.DisableAuditButtonText,
                    OnClickAction = Url.UriActionPost(nameof(DisableAudit)),
                    Icon = FontAwesome.PowerOff,
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.Always
                    },
                    Confirmation = new Confirmation
                    {
                        Message = AuditResources.ConfirmMessageDisableAudit,
                        IsRequired = true
                    },
                    Group = "Audit"
                };

                CardIndex.Settings.ToolbarButtons.Add(disableAuditButton);
                CardIndex.Settings.ToolbarButtons.Add(enableAuditOnTableButton);
                CardIndex.Settings.ToolbarButtons.Add(disableAuditOnTableButton);
            }
            else
            {
                var enableAuditButton = new ToolbarButton
                {
                    Text = AuditResources.EnableAuditButtonText,
                    OnClickAction = Url.UriActionPost(nameof(EnableAudit)),
                    Icon = FontAwesome.PowerOff,
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.Always
                    },
                    Confirmation = new Confirmation
                    {
                        Message = AuditResources.ConfirmMessageEnableAudit,
                        IsRequired = true
                    },
                    Group = "Audit"
                };

                CardIndex.Settings.ToolbarButtons.Add(enableAuditButton);
            }
        }

        [HttpPost]
        public ActionResult EnableAuditOnTables([ModelBinder(typeof(CommaSeparatedListModelBinder))]IList<long> ids)
        {
            _auditService.EnableAuditOnTables(ids.ToArray());
            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpPost]
        public ActionResult DisableAuditOnTables([ModelBinder(typeof(CommaSeparatedListModelBinder))]IList<long> ids)
        {
            _auditService.DisableAuditOnTables(ids.ToArray());

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpPost]
        public ActionResult EnableAudit()
        {
            _auditService.EnableAudit();
            IsAuditEnabled = _auditService.IsAuditEnabled();

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpPost]
        public ActionResult DisableAudit()
        {
            _auditService.DisableAudit();
            IsAuditEnabled = _auditService.IsAuditEnabled();

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }
    }
}