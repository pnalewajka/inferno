﻿using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [Identifier("ValuePickers.Calendar")]
    public class CalendarPickerController : CalendarController
    {
        public CalendarPickerController(ICalendarCardIndexService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies, ITimeService timeService)
            : base(cardIndexDataService, baseControllerDependencies, timeService)
        {
        }
    }
}