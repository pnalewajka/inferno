﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanExecuteDeveloperTools)]
    public class DeveloperToolsController : AtomicController
    {
        public DeveloperToolsController(IBaseControllerDependencies baseControllerDependencies)
            : base(baseControllerDependencies)
        {
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            Layout.PageTitle = DeveloperToolsResources.IndexPageTitle;
            Layout.PageHeader = DeveloperToolsResources.IndexPageHeader;

            var model = new DeveloperToolsConfigViewModel()
            {
                IsMiniProfilerEnable = AtomicCodeProfilerHelper.IsProfilerEnabled
            };

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(DeveloperToolsConfigViewModel model)
        {
            Layout.PageTitle = DeveloperToolsResources.IndexPageTitle;
            Layout.PageHeader = DeveloperToolsResources.IndexPageHeader;

            if (model == null)
            {
                model = new DeveloperToolsConfigViewModel()
                {
                    IsMiniProfilerEnable = AtomicCodeProfilerHelper.IsProfilerEnabled
                };
            }
            else
            {
                if (model.IsMiniProfilerEnable)
                {
                    AtomicCodeProfilerHelper.EnableProfiler();
                }
                else
                {
                    AtomicCodeProfilerHelper.DisableProfiler();
                }
            }

            return View("Index", model);
        }
    }
}