using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Extensions;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.EmailTemplates.Interfaces;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSystemParameters)]
    public class MessageTemplateController : CardIndexController<MessageTemplateViewModel, MessageTemplateDto>
    {
        private const string ContentTypeParameterName = "content-type";
        private readonly IReliableEmailService _reliableMailService;
        private readonly IMessageTemplateCardIndexService _messageTemplateCardIndexService;
        private readonly IAlertService _alertService;
        private readonly ITemplateContentResolvingService _templateContentResolvingService;

        public MessageTemplateController(
            IMessageTemplateCardIndexService messageTemplateCardIndexService,
            IReliableEmailService reliableMailService,
            IAlertService alertService,
            ITemplateContentResolvingService templateContentResolvingService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(messageTemplateCardIndexService, baseControllerDependencies)
        {

            _reliableMailService = reliableMailService;
            _messageTemplateCardIndexService = messageTemplateCardIndexService;
            _alertService = alertService;
            _templateContentResolvingService = templateContentResolvingService;

            CardIndex.Settings.Title = MessageTemplateResources.MessageTemplateControllerTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddMessageTemplates;

            SetAddMessageTemplateDropdownItems();

            var currentPrincipal = GetCurrentPrincipal();

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = MessageTemplateResources.SendAsEmails,
                OnClickAction = Url.UriActionGet("SendAsEmails", KnownParameter.SelectedIds),
                RequiredRole = SecurityRoleType.CanExportEmailTemplates,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                },
                Visibility = currentPrincipal.IsInRole(SecurityRoleType.CanExportEmailTemplates)
                                 ? CommandButtonVisibility.Grid
                                 : CommandButtonVisibility.None
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = MessageTemplateResources.ExportAsHtmls,
                OnClickAction = Url.UriActionGet("ExportAsHtmls", KnownParameter.SelectedIds),
                RequiredRole = SecurityRoleType.CanExportEmailTemplates,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                },
                Visibility = currentPrincipal.IsInRole(SecurityRoleType.CanExportEmailTemplates)
                                 ? CommandButtonVisibility.Grid
                                 : CommandButtonVisibility.None
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = MessageTemplateResources.RefreshTemplateDefinitions,
                OnClickAction = Url.UriActionGet(nameof(RefreshTemplateDefinitions)),
                RequiredRole = SecurityRoleType.CanRefreshTemplateDefinitions,
            });

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewMessageTemplates;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditMessageTemplates;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteMessageTemplates;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;

            CardIndex.AddRowGrouping(t => t.Code, t => t.Code?.Split('.').First());
            CardIndex.AddRowGrouping(t => t.ContentType, t => t.ContentType.GetDescription());
        }

        [AtomicAuthorize(SecurityRoleType.CanExportEmailTemplates)]
        public FileContentResult ExportAsHtmls(List<long> ids)
        {
            return File(_messageTemplateCardIndexService.GetZipWithHtmls(ids), "application/zip", "templates.zip");
        }

        [AtomicAuthorize(SecurityRoleType.CanRefreshTemplateDefinitions)]
        public ActionResult RefreshTemplateDefinitions()
        {
            foreach (var templateContent in _templateContentResolvingService.GetTemplateDefinitions())
            {
                _messageTemplateCardIndexService.CreateOrUpdateTemplate(templateContent.TemplateCode, templateContent.Content, templateContent.Description, templateContent.ContentType);
            }

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanExportEmailTemplates)]
        public ActionResult SendAsEmails(List<long> ids)
        {
            var currentPrincipal = GetCurrentPrincipal();

            var recipientDto = new EmailRecipientDto()
            {
                EmailAddress = currentPrincipal.Email,
                FullName = currentPrincipal.FirstName + " " + currentPrincipal.LastName,
                Type = RecipientType.To
            };

            foreach (var id in ids)
            {
                var record = CardIndex.GetRecordById(id);
                var message = new EmailMessageDto()
                {
                    IsHtml = record.ContentType == MessageTemplateContentType.Html,
                    Subject = record.Code,
                    Body = record.Content,

                    Recipients = new List<EmailRecipientDto>() { recipientDto }
                };

                _reliableMailService.EnqueueMessage(message);
            }

            _alertService.AddSuccess(MessageTemplateResources.EmailsWereSent);

            return List(new GridParameters() { CurrentPageIndex = 0, QueryStringFieldValues = new Dictionary<string, object>() });
        }

        private void SetAddMessageTemplateDropdownItems()
        {
            var contentTypes = EnumHelper.GetEnumValues<MessageTemplateContentType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var contentType in contentTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(contentType.ToString());

                var addButton = new CommandButton
                {
                    Group = MessageTemplateResources.AddMessageTemplateOfTypeHeaderText,
                    RequiredRole = SecurityRoleType.CanAddMessageTemplates,
                    Text = contentType.GetDescriptionOrValue(),
                    OnClickAction = Url.UriActionGet(nameof(Add), KnownParameter.Context, ContentTypeParameterName, typeValueName)
                };

                addDropdownItems.Add(addButton);
            }

            CardIndex.Settings.AddButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        private object GetDefaultNewRecord()
        {
            var contentType = Request.GetQueryParameter<MessageTemplateContentType>(ContentTypeParameterName);
            var model = MessageTemplateDtoToMessageTemplateViewModelMapping.CreateViewModel(contentType);

            return model;
        }
    }
}