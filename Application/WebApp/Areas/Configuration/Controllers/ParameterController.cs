using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Configuration.Consts;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSystemParameters)]
    [PerformanceTest(nameof(ParameterController.List))]
    public class ParameterController : CardIndexController<SystemParameterViewModel, SystemParameterDto>
    {
        public ParameterController(
            ISystemParameterCardIndexService systemParameterGroupCardIndexService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(systemParameterGroupCardIndexService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = ParameterResources.ParameterControllerTitle;

            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewSystemParameters;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSystemParameters;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Name, ParameterResources.SystemParameterKeyLabel),
                new SearchArea(SearchAreaCodes.Value, ParameterResources.SystemParameterValueLabel, false),
                new SearchArea(SearchAreaCodes.ContextType, ParameterResources.SystemParameterContextTypeLabel, false)
            };

            CardIndex.AddRowGrouping(t => t.Key, t => t.Key?.Split('.').First());
        }
    }
}