using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Configuration.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCalendars)]
    public class CalendarEntriesController : SubCardIndexController<CalendarEntryViewModel, CalendarEntryDto, CalendarController>
    {
        private const string NoEntriesFilterCssClass = "subdue";
        private readonly ICalendarEntryCardIndexService _cardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;

        public CalendarEntriesController(ICalendarEntryCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _principalProvider = baseControllerDependencies.PrincipalProvider;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanEditCalendars;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCalendars;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanEditCalendars;

            CardIndex.Settings.Title = CalendarResources.CalendarEntryControllerTitle;
            CardIndex.Settings.PageSize = 30;
        }

        public override ActionResult List(GridParameters parameters)
        {
            InitializeYearFilters(parameters);

            return base.List(parameters);
        }

        [AtomicAuthorize(SecurityRoleType.CanEditCalendars)]
        [HttpPost]
        public ActionResult CopyEntries(int calendarId, int fromYear, int toYear)
        {
            var result = _cardIndexDataService.CopyEntries(calendarId, fromYear, toYear);
            AddAlerts(result);

            var currentYear = new FilterYearDto {Year = toYear};
            var parentId = calendarId.ToInvariantString();
            var filterCode = currentYear.GetFilterCode();

            var uriAction = Url.UriActionGet(nameof(List), KnownParameter.None,
                                             GridParameterModelBinder.ContextParentIdParameterUrlName, parentId,
                                             GridParameterModelBinder.FilterParameterUrlName, filterCode);

            return Redirect(uriAction.Url);
        }

        private void InitializeYearFilters(GridParameters parameters)
        {
            var filterYears = _cardIndexDataService.GetFilterYears().ToList();

            var yearFilters = filterYears
                .Select
                (
                    y => new Filter
                        {
                            Code = y.GetFilterCode(),
                            DisplayName = y.Year.ToInvariantString(),
                            CssClass = y.HasEntries ? null : NoEntriesFilterCssClass
                        }
                ).ToList();

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    ButtonText = CalendarResources.YearButtonText,
                    Filters = yearFilters,
                    Type = FilterGroupType.RadioGroup
                }
            };

            var year = FilterYearDto.TryGetYear(parameters.Filters);

            if (year.HasValue)
            {
                var buttonText = string.Format(CalendarResources.YearButtonTextFormat, year);
                CardIndex.Settings.FilterGroups[0].ButtonText = buttonText;

                var canEditCalendars = _principalProvider.Current.IsInRole(SecurityRoleType.CanEditCalendars);
                var yearModel = new FilterYearsViewModel(year.Value, filterYears, canEditCalendars, Context.ParentId);

                if (!yearModel.CurrentYearHasEntries())
                {
                    CardIndex.Settings.EmptyListViewModel = yearModel;
                    CardIndex.Settings.EmptyListViewName = "~/Areas/Configuration/Views/EmptyCalendar.cshtml";
                }
            }
        }
    }
}