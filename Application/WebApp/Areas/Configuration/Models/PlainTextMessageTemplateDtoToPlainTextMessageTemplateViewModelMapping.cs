﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class PlainTextMessageTemplateDtoToPlainTextMessageTemplateViewModelMapping : ClassMapping<MessageTemplateDto, PlainTextMessageTemplateViewModel>
    {
        public PlainTextMessageTemplateDtoToPlainTextMessageTemplateViewModelMapping()
        {
            Mapping = v => new PlainTextMessageTemplateViewModel
            {
                Code = v.Code,
                Description = v.Description,
                ContentType = v.ContentType,
                Content = v.Content,
                Id = v.Id,
                Timestamp = v.Timestamp
            };
        }
    }
}