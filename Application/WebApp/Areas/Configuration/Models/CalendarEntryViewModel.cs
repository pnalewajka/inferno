﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class CalendarEntryViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(CalendarResources.EntryOnLabel), typeof(CalendarResources))]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "date-column")]
        public DateTime EntryOn { get; set; }

        [StringLength(1024)]
        [DisplayNameLocalized(nameof(CalendarResources.DescriptionLabel), typeof(CalendarResources))]
        [Required]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(CalendarResources.CalendarEntryTypeLabel), typeof(CalendarResources))]
        [Required]
        public CalendarEntryType CalendarEntryType { get; set; }

        [DisplayNameLocalized(nameof(CalendarResources.WorkingHoursLabel), typeof(CalendarResources))]
        [Required]
        [Range(0, 24)]
        public decimal WorkingHours { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return StringHelper.Join(": ", $"{EntryOn:d}", Description);
        }
    }
}
