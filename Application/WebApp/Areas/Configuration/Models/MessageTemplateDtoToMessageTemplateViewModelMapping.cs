﻿using System;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class MessageTemplateDtoToMessageTemplateViewModelMapping : ClassMapping<MessageTemplateDto, MessageTemplateViewModel>
    {
        public MessageTemplateDtoToMessageTemplateViewModelMapping()
        {
            Mapping = d => GetViewModel(d);
        }

        public static Type GetViewModelType(MessageTemplateContentType contentType)
        {
            switch (contentType)
            {
                case MessageTemplateContentType.Html:
                    return typeof(HtmlMessageTemplateViewModel);

                case MessageTemplateContentType.PlainText:
                    return typeof(PlainTextMessageTemplateViewModel);

                default:
                    throw new ArgumentOutOfRangeException(nameof(contentType));
            }
        }

        public static MessageTemplateViewModel CreateViewModel(MessageTemplateContentType contentType)
        {
            switch (contentType)
            {
                case MessageTemplateContentType.Html:
                    return new HtmlMessageTemplateViewModel();

                case MessageTemplateContentType.PlainText:
                    return new PlainTextMessageTemplateViewModel();

                default:
                    throw new ArgumentOutOfRangeException(nameof(contentType));
            }
        }

        private static MessageTemplateViewModel GetViewModel(MessageTemplateDto dto)
        {
            var viewModel = CreateViewModel(dto.ContentType);

            SetValuesFromDto(dto, viewModel);

            return viewModel;
        }

        private static void SetValuesFromDto(MessageTemplateDto dto, MessageTemplateViewModel model)
        {
            model.Code = dto.Code;
            model.Description = dto.Description;
            model.ContentType = dto.ContentType;
            model.Content = dto.Content;
            model.Id = dto.Id;
            model.ModifiedOn = dto.ModifiedOn;
            model.Timestamp = dto.Timestamp;
        }
    }
}
