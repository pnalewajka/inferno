﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class FilterYearsViewModel
    {
        public int CurrentYear { get; private set; }

        public IList<FilterYearDto> FilterYears { get; private set; }

        public bool CanEditCalendars { get; private set; }
        public long CalendarId { get; private set; }

        public FilterYearsViewModel(int currentYear, IList<FilterYearDto> filterYears, bool canEditCalendars, long calendarId)
        {
            FilterYears = filterYears;
            CanEditCalendars = canEditCalendars;
            CalendarId = calendarId;
            CurrentYear = currentYear;
        }

        public bool CurrentYearHasEntries()
        {
            return FilterYears.Any(y => y.Year == CurrentYear && y.HasEntries);
        }

        public IList<FilterYearDto> GetOtherYearsWithEntries()
        {
            return FilterYears.Where(y => y.Year != CurrentYear && y.HasEntries).ToList();
        }

        public bool CanCopyEntries()
        {
            return CanEditCalendars && GetOtherYearsWithEntries().Any();
        }
    }
}