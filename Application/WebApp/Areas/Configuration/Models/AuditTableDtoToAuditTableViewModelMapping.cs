﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class AuditTableDtoToAuditTableViewModelMapping : ClassMapping<AuditTableDto, AuditTableViewModel>
    {
        public AuditTableDtoToAuditTableViewModelMapping()
        {
            Mapping = d => new AuditTableViewModel
            {
                TableName = d.TableName,
                Schema = d.Schema,
                IsAuditEnabled = d.IsAuditEnabled,
                Id = d.Id
            }
            ;
        }
    }
}
