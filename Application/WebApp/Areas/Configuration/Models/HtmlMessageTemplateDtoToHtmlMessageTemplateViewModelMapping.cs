﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class HtmlMessageTemplateDtoToHtmlMessageTemplateViewModelMapping : ClassMapping<MessageTemplateDto, HtmlMessageTemplateViewModel>
    {
        public HtmlMessageTemplateDtoToHtmlMessageTemplateViewModelMapping()
        {
            Mapping = dto => new HtmlMessageTemplateViewModel
            {
                Code = dto.Code,
                Description = dto.Description,
                ContentType = dto.ContentType,
                Content = dto.Content,
                Id = dto.Id,
                ModifiedOn = dto.ModifiedOn,
                Timestamp = dto.Timestamp,
            };
        }
    }
}