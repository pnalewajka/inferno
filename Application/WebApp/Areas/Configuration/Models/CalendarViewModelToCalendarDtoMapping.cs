﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class CalendarViewModelToCalendarDtoMapping : ClassMapping<CalendarViewModel, CalendarDto>
    {
        public CalendarViewModelToCalendarDtoMapping()
        {
            Mapping = v => new CalendarDto
            {
                Code = v.Code,
                Description = v.Description,
                Id = v.Id,
                Timestamp = v.Timestamp
            };
        }
    }
}
