﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class SystemParameterDtoToSystemParameterViewModelMapping : ClassMapping<SystemParameterDto, SystemParameterViewModel>
    {
        public SystemParameterDtoToSystemParameterViewModelMapping()
        {
            Mapping = d => new SystemParameterViewModel
            {
                Id = d.Id,
                Key = d.Key,
                Value = d.Value,
                ValueType = d.ValueType,
                ContextType = d.ContextType,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
