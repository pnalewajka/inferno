﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class HtmlMessageTemplateViewModelToHtmlMessageTemplateDtoMapping : ClassMapping<HtmlMessageTemplateViewModel, MessageTemplateDto>
    {
    }
}