﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class MessageTemplateViewModelBinder : AtomicModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var contentTypePropertyName = PropertyHelper.GetPropertyName<MessageTemplateViewModel>(t => t.ContentType);
            var contentTypeParameterName = NamingConventionHelper.ConvertPascalCaseToHyphenated(contentTypePropertyName);
            var value = bindingContext.ValueProvider.GetValue(contentTypeParameterName);

            if (value != null)
            {
                var pascalCaseValue = NamingConventionHelper.ConvertHyphenatedToPascalCase(value.AttemptedValue);
                var contentType = EnumHelper.GetEnumValue<MessageTemplateContentType>(pascalCaseValue);
                var viewModelType = MessageTemplateDtoToMessageTemplateViewModelMapping.GetViewModelType(contentType);

                bindingContext.ModelMetadata = ModelMetadataProviders
                    .Current
                    .GetMetadataForType(null, viewModelType);
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}