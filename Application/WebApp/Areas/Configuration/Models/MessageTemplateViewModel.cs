﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using Smt.Atomic.WebApp.Attributes.CellContent;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    [ModelBinder(typeof(MessageTemplateViewModelBinder))]
    public class MessageTemplateViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(MessageTemplateResources.MessageTemplateCodeLabel), typeof(MessageTemplateResources))]
        [Required]
        public string Code { get; set; }

        [Order(1)]
        [StringLength(1024)]
        [DisplayNameLocalized(nameof(MessageTemplateResources.MessageTemplateDescriptionLabel), typeof(MessageTemplateResources))]
        public string Description { get; set; }

        [Order(2)]
        [Render(Size = Size.Small)]
        [Required]
        [DisplayNameLocalized(nameof(MessageTemplateResources.MessageTemplateContentTypeLabel), typeof(MessageTemplateResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<MessageTemplateContentType>))]
        [IconizedMessageTemplateContentType]
        public virtual MessageTemplateContentType ContentType { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(MessageTemplateResources.MessageTemplateContentLabel), typeof(MessageTemplateResources))]
        [Visibility(VisibilityScope.Form)]
        [Required]
        [AllowHtml]
        public virtual string Content { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "date-time-column")]
        [DisplayNameLocalized(nameof(MessageTemplateResources.MessageTemplateModifiedOnLabel), typeof(MessageTemplateResources))]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}
