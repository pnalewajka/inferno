﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class AuditTableViewModelToAuditTableDtoMapping : ClassMapping<AuditTableViewModel, AuditTableDto>
    {
        public AuditTableViewModelToAuditTableDtoMapping()
        {
            Mapping = v => new AuditTableDto
            {
                TableName = v.TableName,
                Schema = v.Schema,
                IsAuditEnabled = v.IsAuditEnabled,
                Id = v.Id
            }
            ;
        }
    }
}
