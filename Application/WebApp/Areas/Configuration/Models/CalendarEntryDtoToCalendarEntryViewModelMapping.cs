﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class CalendarEntryDtoToCalendarEntryViewModelMapping : ClassMapping<CalendarEntryDto, CalendarEntryViewModel>
    {
        public CalendarEntryDtoToCalendarEntryViewModelMapping()
        {
            Mapping = d => new CalendarEntryViewModel
            {
                Id = d.Id,
                EntryOn = d.EntryOn,
                Description = d.Description,
                Timestamp = d.Timestamp,
                CalendarEntryType = d.CalendarEntryType,
                WorkingHours = d.WorkingHours
            };
        }
    }
}
