﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class SystemParameterViewModelToSystemParameterDtoMapping : ClassMapping<SystemParameterViewModel, SystemParameterDto>
    {
        public SystemParameterViewModelToSystemParameterDtoMapping()
        {
            Mapping = v => new SystemParameterDto
            {
                Id = v.Id,
                Key = v.Key,
                Value = v.Value,
                ValueType = v.ValueType,
                ContextType = v.ContextType,
                ModifiedOn = v.ModifiedOn,
                Timestamp = v.Timestamp
            };
        }
    }
}
