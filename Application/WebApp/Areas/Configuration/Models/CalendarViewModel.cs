﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class CalendarViewModel
    {
        public long Id { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(CalendarResources.CodeLabel), typeof(CalendarResources))]
        [Render(GridCssClass = "up-to-30-percent")]
        [Required]
        public string Code { get; set; }

        [StringLength(1024)]
        [DisplayNameLocalized(nameof(CalendarResources.DescriptionLabel), typeof(CalendarResources))]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(CalendarResources.ModifiedOnLabel), typeof(CalendarResources))]
        [Render(GridCssClass = "date-time-column")]
        [Visibility(VisibilityScope.Grid)]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return StringHelper.Join(" - ", Code, Description);
        }
    }
}
