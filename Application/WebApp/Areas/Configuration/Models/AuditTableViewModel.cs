﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class AuditTableViewModel
    {
        [ReadOnly(true)]
        [Required]
        [DisplayNameLocalized(nameof(AuditResources.TableNameColumnName), typeof(AuditResources))]
        public string TableName { get; set; }

        [ReadOnly(true)]
        [Required]
        [DisplayNameLocalized(nameof(AuditResources.SchemaColumnName), typeof(AuditResources))]
        public string Schema { get; set; }

        [DisplayNameLocalized(nameof(AuditResources.IsAuditEnabledColumnName), typeof(AuditResources))]
        public bool IsAuditEnabled { get; set; }

        public long Id { get; set; }

        public override string ToString()
        {
            return "[" + Schema + "].[" + TableName + "]";
        }
    }
}
