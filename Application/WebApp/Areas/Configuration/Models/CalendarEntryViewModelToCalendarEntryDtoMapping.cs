﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class CalendarEntryViewModelToCalendarEntryDtoMapping : ClassMapping<CalendarEntryViewModel, CalendarEntryDto>
    {
        public CalendarEntryViewModelToCalendarEntryDtoMapping()
        {
            Mapping = v => new CalendarEntryDto
            {
                Id = v.Id,
                EntryOn = v.EntryOn,
                Description = v.Description,
                Timestamp = v.Timestamp,
                CalendarEntryType = v.CalendarEntryType,
                WorkingHours = v.WorkingHours
            };
        }
    }
}
