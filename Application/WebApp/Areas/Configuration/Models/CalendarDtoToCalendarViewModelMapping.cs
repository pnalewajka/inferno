﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class CalendarDtoToCalendarViewModelMapping : ClassMapping<CalendarDto, CalendarViewModel>
    {
        public CalendarDtoToCalendarViewModelMapping()
        {
            Mapping = d => new CalendarViewModel
            {
                Code = d.Code,
                Description = d.Description,
                Id = d.Id,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
