﻿using System.ComponentModel;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public sealed class HtmlMessageTemplateViewModel : MessageTemplateViewModel
    {
        [ReadOnly(true)]
        public override MessageTemplateContentType ContentType
        {
            get { return base.ContentType; }
            set { base.ContentType = value; }
        }

        [AllowHtml]
        [CodeEditor(SyntaxType.Razor)]
        public override string Content
        {
            get { return base.Content; }
            set { base.Content = value; }
        }

        public HtmlMessageTemplateViewModel()
        {
            ContentType = MessageTemplateContentType.Html;
        }
    }
}