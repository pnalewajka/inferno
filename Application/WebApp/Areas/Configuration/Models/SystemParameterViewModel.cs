﻿using Smt.Atomic.Business.Configuration.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class SystemParameterViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ParameterResources.SystemParameterKeyLabel), typeof(ParameterResources))]
        [Render(GridCssClass = "up-to-30-percent")]
        public string Key { get; set; }

        [DisplayNameLocalized(nameof(ParameterResources.SystemParameterValueLabel), typeof(ParameterResources))]
        [Multiline(4)]
        [Render(GridCssClass = "up-to-30-percent")]
        public string Value { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ParameterResources.SystemParameterValueTypeLabel), typeof(ParameterResources))]
        [Render(GridHeaderCssClass = "single-line")]
        public string ValueType { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ParameterResources.SystemParameterContextTypeLabel), typeof(ParameterResources))]
        [Render(GridHeaderCssClass = "single-line")]
        public string ContextType { get; set; }

        [DisplayNameLocalized(nameof(ParameterResources.SystemParameterModifiedOnLabel), typeof(ParameterResources))]
        [Render(GridCssClass = "date-time-column", GridHeaderCssClass = "single-line")]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var valueType = IdentifierHelper.GetTypeByIdentifier(ValueType, false);
            var hasNonValueIssues = false;

            if (valueType == null)
            {
                yield return new PropertyValidationResult(ParameterResources.InvalidValueTypeMessage, () => ValueType);
                hasNonValueIssues = true;
            }

            var contextType = IdentifierHelper.GetTypeByIdentifier(ContextType, false);

            if (contextType == null && ContextType != null)
            {
                yield return new PropertyValidationResult(ParameterResources.InvalidContextTypeMessage, () => ContextType);
                hasNonValueIssues = true;
            }

            if (hasNonValueIssues)
            {
                yield break;
            }

            var evaluationBuilder = new ParameterEvaluationBuilder(Value, valueType, contextType);

            if (!evaluationBuilder.IsValid)
            {
                yield return new PropertyValidationResult(ParameterResources.InvalidParameterValueMessage, () => Value);
            }
        }
    }
}
