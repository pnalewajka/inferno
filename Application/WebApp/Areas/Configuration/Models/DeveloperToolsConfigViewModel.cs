﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Configuration.Resources;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public class DeveloperToolsConfigViewModel
    {
        [DisplayNameLocalized(nameof(DeveloperToolsResources.MiniProfilerEnable), typeof(DeveloperToolsResources))]
        public bool IsMiniProfilerEnable { get; set; }
    }
}