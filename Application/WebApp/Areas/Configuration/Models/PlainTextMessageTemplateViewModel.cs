﻿using System.ComponentModel;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.WebApp.Areas.Configuration.Models
{
    public sealed class PlainTextMessageTemplateViewModel : MessageTemplateViewModel
    {
        [ReadOnly(true)]
        public override MessageTemplateContentType ContentType
        {
            get { return base.ContentType; }
            set { base.ContentType = value; }
        }

        [Multiline(5)]
        public override string Content
        {
            get { return base.Content; }
            set { base.Content = value; }
        }

        public PlainTextMessageTemplateViewModel()
        {
            ContentType = MessageTemplateContentType.PlainText;
        }
    }
}