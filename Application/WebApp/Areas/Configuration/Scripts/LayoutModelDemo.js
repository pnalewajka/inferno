/// <reference path="../../../Scripts/typings/jquery/jquery.d.ts"/>
/// <reference path="../../../Scripts/Atomic/Globals.ts"/>
$(function () {
    var list = $("<ul></ul>");
    for (var key in Globals.Resources) {
        list.append($("<li><span class='key'>" + key + "</span> &rarr; <span class='value'>" + Globals.Resources[key] + "</span></li>"));
    }
    $("div.layout-body").append(list);
});
//# sourceMappingURL=LayoutModelDemo.js.map