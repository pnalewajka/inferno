﻿var UserProfiles;
(function (UserProfiles) {
    function assignSecurityProfile(grid) {
        var context = grid.gridContext;
        var parentId = context.replace('parent-id=', '');
        ValuePicker.selectMultiple("ValuePickers.SecurityProfiles", function (records) {
            Dialogs.confirm(Globals.Resources.GrantUserProfileConfirmationMessage, Globals.Resources.ToolbarButtonConfirmationDialogTitle, function (eventArgs) {
                if (eventArgs.buttonTag == 'ok') {
                    var ids = records.map(function (v) {
                        return v.id.toString();
                    }).join(",");
                    var url = "../UserProfiles/AssignProfilesToUser?userId=" + parentId + "&profileIds=" + ids;
                    Utils.submit(url, "POST");
                }
            }, null, "", "");
        });
    }
    UserProfiles.assignSecurityProfile = assignSecurityProfile;

    function revokeSecurityProfile(grid) {
        var context = grid.gridContext;
        var parentId = context.replace('parent-id=', '');
        var records = grid.getSelectedRowData();
        var ids = records.map(function (v) {
            return v.id.toString();
        }).join(",");
        var url = "../UserProfiles/RevokeProfilesFromUser?userId=" + parentId + "&profileIds=" + ids;
        Utils.submit(url, "POST");
    }
    UserProfiles.revokeSecurityProfile = revokeSecurityProfile;
})(UserProfiles || (UserProfiles = {}));
