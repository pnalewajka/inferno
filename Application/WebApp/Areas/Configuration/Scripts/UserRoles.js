﻿var UserRoles;
(function (UserRoles) {
    function assignSecurityRole(grid) {
        var context = grid.gridContext;
        var parentId = context.replace('parent-id=', '');
        ValuePicker.selectMultiple("ValuePickers.SecurityRoles", function (records) {
            Dialogs.confirm(Globals.Resources.GrantUserRoleConfirmationMessage, Globals.Resources.ToolbarButtonConfirmationDialogTitle, function (eventArgs) {
                if (eventArgs.buttonTag == 'ok') {
                    var ids = records.map(function (v) {
                        return v.id.toString();
                    }).join(",");
                    var url = Strings.format("../UserRoles/AssignRolesToUser?userId={0}&roleIds={1}", parentId, ids);
                    Utils.submit(url, "POST");
                }
            }, null, "", "");
        });
    }
    UserRoles.assignSecurityRole = assignSecurityRole;

    function revokeSecurityRole(grid) {
        var context = grid.gridContext;
        var parentId = context.replace('parent-id=', '');
        var records = grid.getSelectedRowData();
        var ids = records.map(function (v) {
            return v.id.toString();
        }).join(",");
        var url = Strings.format("../UserRoles/RevokeRolesFromUser?userId={0}&roleIds={1}", parentId, ids);
        Utils.submit(url, "POST");
    }
    UserRoles.revokeSecurityRole = revokeSecurityRole;
})(UserRoles || (UserRoles = {}));
