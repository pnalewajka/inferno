"use strict";
var GDPR;
(function (GDPR) {
    var DataProcessingAgreementConsent;
    (function (DataProcessingAgreementConsent) {
        function DataProcessingAgreementConsentConfirmation(row, createUrl) {
            CommonDialogs.confirm(Globals.resources.DataProcessingAgreementConsentConfirmationMessage, Globals.resources.ToolbarButtonConfirmationDialogTitle, function (eventArgs) {
                if (eventArgs.isOk) {
                    CommonDialogs.pleaseWait.show();
                    Forms.submit(Globals.resolveUrl(createUrl + "\\" + row.id), "POST");
                }
            });
        }
        DataProcessingAgreementConsent.DataProcessingAgreementConsentConfirmation = DataProcessingAgreementConsentConfirmation;
    })(DataProcessingAgreementConsent = GDPR.DataProcessingAgreementConsent || (GDPR.DataProcessingAgreementConsent = {}));
})(GDPR || (GDPR = {}));
//# sourceMappingURL=DataProcessingAgreementConsent.js.map