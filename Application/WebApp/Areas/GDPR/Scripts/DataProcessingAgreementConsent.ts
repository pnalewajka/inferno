﻿namespace GDPR.DataProcessingAgreementConsent {

    export function DataProcessingAgreementConsentConfirmation(row: Grid.IRow, createUrl: string): void {
        CommonDialogs.confirm(Globals.resources.DataProcessingAgreementConsentConfirmationMessage,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    CommonDialogs.pleaseWait.show();
                    Forms.submit(Globals.resolveUrl(`${createUrl}\\${row.id}`), "POST");
                }
            });
    }
}
