﻿using System.Linq;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataProcessingAgreementConsentDtoToDataProcessingAgreementConsentViewModelMapping : ClassMapping<DataProcessingAgreementConsentDto, DataProcessingAgreementConsentViewModel>
    {
        public DataProcessingAgreementConsentDtoToDataProcessingAgreementConsentViewModelMapping()
        {
            Mapping = d => new DataProcessingAgreementConsentViewModel
            {
                Id = d.Id,
                AgreementId = d.AgreementId,
                UserId = d.UserId,
                IsApproved = d.IsApproved,
                ProjectIds = d.ProjectIds,
                Documents = d.Documents
                    .Select(p => new DocumentViewModel
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                    }).ToList(),
                ModifiedOn = d.ModifiedOn
            };
        }
    }
}
