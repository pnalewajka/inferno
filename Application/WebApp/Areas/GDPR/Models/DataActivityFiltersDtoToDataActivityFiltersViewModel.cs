﻿using System;
using System.Linq;
using Smt.Atomic.Business.GDPR.Dto.Filters;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataActivityFiltersDtoToDataActivityFiltersViewModel
        : ClassMapping<DataActivityFiltersDto, DataActivityFiltersViewModel>
    {
        public DataActivityFiltersDtoToDataActivityFiltersViewModel()
        {
            Mapping = da => new DataActivityFiltersViewModel
            {
                ActivityTypes = da.ActivityTypes.Select(a => (long)a).ToArray(),                
                OwnerId = da.OwnerId,
                From = da.From.HasValue ? da.From.Value : (DateTime?)null,
                To = da.To.HasValue ? da.To.Value : (DateTime?)null,
                UserId = da.UserId
            };
        }
    }
}