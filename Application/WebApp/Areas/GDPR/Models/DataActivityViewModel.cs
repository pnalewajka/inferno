﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    [Identifier("Models.DataActivityViewModel")]
    public class DataActivityViewModel
    {
        public long Id { get; set; }

        [Required]
        [ValuePicker(Type = typeof(DataOwnerPickerController), DialogWidth = "900px")]
        [DisplayNameLocalized(nameof(DataActivityResources.DataOwnerLabel), typeof(DataActivityResources))]
        [Render(GridCssClass = "no-break-column", Size = Size.Large)]
        public long DataOwnerId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(DataActivityResources.PerformedOnLabel), typeof(DataActivityResources))]
        [Render(GridCssClass = "no-break-column")]
        public DateTime PerformedOn { get; set; }

        [Required]
        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(DataActivityResources.PerformedByUserLabel), typeof(DataActivityResources))]
        [Render(GridCssClass = "no-break-column", Size = Size.Large)]
        public long PerformedByUserId { get; set; }

        [NonSortable]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(DataActivityResources.ActivityTypeLabel), typeof(DataActivityResources))]
        [Render(GridCssClass = "remaining-space-column", Size = Size.Large)]
        [Visibility(VisibilityScope.Grid)]
        public string ActivityDetails
        {
            get
            {
                var builder = new StringBuilder(
                    $"{EnumExtensions.GetLocalizedDescription(ActivityType).ToString()} ({EnumExtensions.GetLocalizedDescription(ReferenceType).ToString()}.ID: {Id})");

                if (!string.IsNullOrEmpty(S1))
                {
                    builder.Append($" in {S1}");
                }

                if (!string.IsNullOrEmpty(S2))
                {
                    builder.Append($" · {S2}");
                }

                if (!string.IsNullOrEmpty(S3))
                {
                    builder.Append($" · {S3}");
                }

                return builder.ToString();
            }
        }

        [Required]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(DataActivityResources.ActivityTypeLabel), typeof(DataActivityResources))]
        public ActivityType ActivityType { get; set; }

        [Visibility(VisibilityScope.Form)]
        public long? ReferenceId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Required]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<ReferenceType>), EmptyItemBehavior = EmptyItemBehavior.NotAvailable)]
        public ReferenceType? ReferenceType { get; set; }

        [StringLength(500)]
        [Multiline(3)]
        [DisplayNameLocalized(nameof(DataActivityResources.S1Label), typeof(DataActivityResources))]
        [Visibility(VisibilityScope.Form)]
        public string S1 { get; set; }

        [StringLength(500)]
        [Multiline(3)]
        [DisplayNameLocalized(nameof(DataActivityResources.S2Label), typeof(DataActivityResources))]
        [Visibility(VisibilityScope.Form)]
        public string S2 { get; set; }

        [StringLength(2000)]
        [Multiline(5)]
        [DisplayNameLocalized(nameof(DataActivityResources.S3Label), typeof(DataActivityResources))]
        [Visibility(VisibilityScope.Form)]
        public string S3 { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
