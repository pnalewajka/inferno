﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataActivityViewModelToDataActivityDtoMapping : ClassMapping<DataActivityViewModel, DataActivityDto>
    {
        public DataActivityViewModelToDataActivityDtoMapping()
        {
            Mapping = v => new DataActivityDto
            {
                Id = v.Id,
                DataOwnerId = v.DataOwnerId,
                PerformedOn = v.PerformedOn,
                PerformedByUserId = v.PerformedByUserId,
                ActivityType = v.ActivityType,
                ReferenceId = v.ReferenceId,
                ReferenceType = v.ReferenceType,
                S1 = v.S1,
                S2 = v.S2,
                S3 = v.S3,
                Timestamp = v.Timestamp
            };
        }
    }
}
