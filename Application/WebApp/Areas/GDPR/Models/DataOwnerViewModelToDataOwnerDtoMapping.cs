﻿using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataOwnerViewModelToDataOwnerDtoMapping : ClassMapping<DataOwnerViewModel, DataOwnerDto>
    {
        public DataOwnerViewModelToDataOwnerDtoMapping()
        {
            Mapping = v => new DataOwnerDto
            {
                Id = v.Id,
                UserId = v.UserId,
                EmployeeId = v.EmployeeId,
                CandidateId = v.CandidateId,
                RecommendingPersonId = v.RecommendingPersonId,
                IsPotentialDuplicate = v.IsPotentialDuplicate,
                Timestamp = v.Timestamp
            };
        }
    }
}
