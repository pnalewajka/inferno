﻿using System;
using System.Linq;
using Smt.Atomic.Business.GDPR.Dto.Filters;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataActivityFiltersViewModelToDataActivityFiltersDto
        : ClassMapping<DataActivityFiltersViewModel, DataActivityFiltersDto>
    {
        public DataActivityFiltersViewModelToDataActivityFiltersDto()
        {
            Mapping = da => new DataActivityFiltersDto
            {
                ActivityTypes = da.ActivityTypes.Select(a => (ActivityType)a).ToArray(),
                From = da.From.HasValue ? da.From.Value.StartOfDay() : (DateTime?)null,
                To = da.To.HasValue ? da.To.Value.EndOfDay() : (DateTime?)null,
                OwnerId = da.OwnerId,
                UserId = da.UserId
            };
        }
    }
}