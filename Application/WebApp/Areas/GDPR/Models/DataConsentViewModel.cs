﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.GDPR.DocumentMappings;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    [Identifier("Models.DataConsentViewModel")]
    [FieldSetDescription(1, GeneralFieldSet)]
    [FieldSetDescription(2, LastContactFieldSet, nameof(DataConsentResources.DocumentedProof), typeof(DataConsentResources))]
    public class DataConsentViewModel : IValidatableObject
    {
        private const string GeneralFieldSet = "General";
        private const string LastContactFieldSet = "DocumentedProof";

        public long Id { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(DataConsentResources.DataAdministrator), typeof(DataConsentResources))]
        [ValuePicker(Type = typeof(CompanyPickerController), OnResolveUrlJavaScript = "url=url +'&is-data-administrator=true'")]
        [Render(Size = Size.Large)]
        [FieldSet(GeneralFieldSet)]
        public long DataAdministratorId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(DataConsentResources.DataOwner), typeof(DataConsentResources))]
        [ValuePicker(Type = typeof(DataOwnerPickerController), DialogWidth = "900px")]
        [Render(Size = Size.Large)]
        [FieldSet(GeneralFieldSet)]
        public long DataOwnerId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(DataConsentResources.DataConsentType), typeof(DataConsentResources))]
        [FieldSet(GeneralFieldSet)]
        [ReadOnly(true)]
        public DataConsentType DataConsentType { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(DataConsentResources.Scope), typeof(DataConsentResources))]
        [FieldSet(GeneralFieldSet)]
        [StringLength(250)]
        public string Scope { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(DataConsentResources.ExpiresOn), typeof(DataConsentResources))]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [FieldSet(GeneralFieldSet)]
        public DateTime? ExpiresOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(DataConsentResources.DocumentFromDisk), typeof(DataConsentResources))]
        [DocumentUpload(typeof(DataConsentDocumentMapping), DownloadUrlTemplate = "/GDPR/DataConsent/ViewFile/{1}", ShouldUseDownloadDialog = true)]
        [FieldSet(LastContactFieldSet)]
        public List<DocumentViewModel> Documents { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(DataConsentResources.DocumentFormAttachment), typeof(DataConsentResources))]
        [ValuePicker(Type = typeof(InboundEmailDocumentPickerController), OnResolveUrlJavaScript = "url += '&assigned-only=true'")]
        [Render(Size = Size.Large)]
        [FieldSet(LastContactFieldSet)]
        public long? InboundEmailDocumentId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(DataConsentResources.DocumentFormContent), typeof(DataConsentResources))]
        [ValuePicker(Type = typeof(InboundEmailPickerController), OnResolveUrlJavaScript = "url += '&assigned-only=true'")]
        [Render(Size = Size.Large)]
        [FieldSet(LastContactFieldSet)]
        public long? SourceEmailId { get; set; }

        public DataConsentViewModel()
        {
            Documents = new List<DocumentViewModel>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DataConsentBusinessLogic.DoesConsentTypeRequireAttachment.Call(DataConsentType))
            {
                if (Documents.IsNullOrEmpty() && !InboundEmailDocumentId.HasValue && !SourceEmailId.HasValue)
                {
                    yield return new ValidationResult(
                        DataConsentResources.CandidateConsentDocumentsError,
                        new[] { nameof(Documents), nameof(InboundEmailDocumentId), nameof(SourceEmailId) });
                }
            }

            if (DataConsentBusinessLogic.DoesConsentTypeRequireExpirationDate.Call(DataConsentType) && !ExpiresOn.HasValue)
            {
                yield return new ValidationResult(
                    string.Format(DataConsentResources.ValidationEmptyError,
                        DataConsentResources.ExpiresOn,
                        DataConsentResources.DataConsentType,
                        DataConsentType.GetDescriptionOrValue()),
                    new[] { nameof(ExpiresOn) });
            }

            if (DataConsentBusinessLogic.DoesConsentTypeRequireScope.Call(DataConsentType) && string.IsNullOrWhiteSpace(Scope))
            {
                yield return new ValidationResult(
                    string.Format(DataConsentResources.ValidationEmptyError,
                        DataConsentResources.Scope,
                        DataConsentResources.DataConsentType,
                        DataConsentType.GetDescriptionOrValue()),
                    new[] { nameof(Scope) });
            }

            if ((!Documents.IsNullOrEmpty() && InboundEmailDocumentId.HasValue)
                || (!Documents.IsNullOrEmpty() && SourceEmailId.HasValue)
                || (InboundEmailDocumentId.HasValue && SourceEmailId.HasValue))
            {
                yield return new ValidationResult(
                    DataConsentResources.OnlyOneDocumentSourceAllowed,
                    new[] { nameof(Documents), nameof(InboundEmailDocumentId), nameof(SourceEmailId) });
            }
        }

        public override string ToString()
        {
            return $"{DataConsentType.GetDescriptionOrValue()} {ExpiresOn?.ToString("d")}";
        }
    }
}