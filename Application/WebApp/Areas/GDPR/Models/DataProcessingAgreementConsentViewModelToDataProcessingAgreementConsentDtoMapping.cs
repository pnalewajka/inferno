﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataProcessingAgreementConsentViewModelToDataProcessingAgreementConsentDtoMapping : ClassMapping<DataProcessingAgreementConsentViewModel, DataProcessingAgreementConsentDto>
    {
        public DataProcessingAgreementConsentViewModelToDataProcessingAgreementConsentDtoMapping()
        {
            Mapping = v => new DataProcessingAgreementConsentDto
            {
                Id = v.Id,
                AgreementId = v.AgreementId,
                UserId = v.UserId,
                IsApproved = v.IsApproved,
            };
        }
    }
}
