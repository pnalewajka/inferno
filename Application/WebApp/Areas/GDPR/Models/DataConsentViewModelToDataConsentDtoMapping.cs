﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataConsentViewModelToDataConsentDtoMapping : ClassMapping<DataConsentViewModel, DataConsentDto>
    {
        public DataConsentViewModelToDataConsentDtoMapping()
        {
            Mapping = v => new DataConsentDto
            {
                Id = v.Id,
                Type = v.DataConsentType,
                ExpiresOn = v.ExpiresOn,
                DataAdministratorId = v.DataAdministratorId,
                Documents = v.Documents
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    })
                    .Union(
                        v.InboundEmailDocumentId.HasValue
                            ? new List<DocumentDto>
                            {
                                new DocumentDto
                                {
                                    DocumentId = v.InboundEmailDocumentId.Value
                                }
                            }
                            : Enumerable.Empty<DocumentDto>()
                    )
                    .Union(
                        v.SourceEmailId.HasValue
                            ? new List<DocumentDto>
                            {
                                new DocumentDto
                                {
                                    DocumentId = v.SourceEmailId.Value
                                }
                            }
                            : Enumerable.Empty<DocumentDto>()
                    )
                    .ToArray(),
                FileSource = v.InboundEmailDocumentId.HasValue ? FileSource.InboundEmailAttachment
                    : v.SourceEmailId.HasValue ? FileSource.InboundEmailContent
                    : FileSource.Disk,
                Scope = v.Scope,
                DataOwnerId = v.DataOwnerId
            };
        }
    }
}
