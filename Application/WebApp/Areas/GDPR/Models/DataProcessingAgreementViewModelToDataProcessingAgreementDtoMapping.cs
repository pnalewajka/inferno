﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataProcessingAgreementViewModelToDataProcessingAgreementDtoMapping : ClassMapping<DataProcessingAgreementViewModel, DataProcessingAgreementDto>
    {
        public DataProcessingAgreementViewModelToDataProcessingAgreementDtoMapping()
        {
            Mapping = v => new DataProcessingAgreementDto
            {
                Id = v.Id,
                ProjectIds = v.ProjectIds,
                RequiredUserIds = v.RequiredUserIds,
                ValidFrom = v.ValidFrom,
                ValidTo = v.ValidTo,
                Documents = v.Documents.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId,
                    TemporaryDocumentId = p.TemporaryDocumentId,
                }).ToList(),
            };
        }
    }
}
