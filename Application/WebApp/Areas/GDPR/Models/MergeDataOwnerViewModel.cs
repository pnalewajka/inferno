﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    [Identifier("GDPR.MergeDataOwnerViewModel")]
    public class MergeDataOwnerViewModel : IValidatableObject
    {
        [HiddenInput]
        public long DataOwnerId { get; set; }

        [ValuePicker(Type = typeof(DataOwnerPickerController))]
        [DisplayNameLocalized(nameof(DataOwnerResources.MergeDataOwner), typeof(DataOwnerResources))]
        public long MergeDataOwnerId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DataOwnerId == MergeDataOwnerId)
            {
                yield return new ValidationResult(DataOwnerResources.InvalidMergeDataOwners);
            }
        }
    }
}