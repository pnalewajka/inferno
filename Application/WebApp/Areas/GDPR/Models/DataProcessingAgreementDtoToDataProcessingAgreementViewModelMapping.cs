﻿using System.Linq;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataProcessingAgreementDtoToDataProcessingAgreementViewModelMapping : ClassMapping<DataProcessingAgreementDto, DataProcessingAgreementViewModel>
    {
        public DataProcessingAgreementDtoToDataProcessingAgreementViewModelMapping()
        {
            Mapping = d => new DataProcessingAgreementViewModel
            {
                Id = d.Id,
                ProjectIds = d.ProjectIds,
                RequiredUserIds = d.RequiredUserIds,
                ValidFrom = d.ValidFrom,
                ValidTo = d.ValidTo,
                Documents = d.Documents
                    .Select(p => new DocumentViewModel
                     {
                         ContentType = p.ContentType,
                         DocumentName = p.DocumentName,
                         DocumentId = p.DocumentId,
                     }).ToList(),
            };
        }
    }
}
