﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.Business.GDPR.DocumentMappings;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataProcessingAgreementViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [NonSortable]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(DataProcessingAgreementResources.ProjectsLabel), typeof(DataProcessingAgreementResources))]
        public List<long> ProjectIds { get; set; }

        [Required]
        [NonSortable]
        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(DataProcessingAgreementResources.RequiredUsersLabel), typeof(DataProcessingAgreementResources))]
        public List<long> RequiredUserIds { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(DataProcessingAgreementResources.ValidFromLabel), typeof(DataProcessingAgreementResources))]
        public DateTime ValidFrom { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(DataProcessingAgreementResources.ValidToLabel), typeof(DataProcessingAgreementResources))]
        public DateTime ValidTo { get; set; }

        [Required]
        [NonSortable]
        [DocumentList(Truncate = false)]
        [DocumentUpload(typeof(DataProcessingAgreementDocumentMapping))]
        [DisplayNameLocalized(nameof(DataProcessingAgreementResources.DocumentsLabel), typeof(DataProcessingAgreementResources))]
        public List<DocumentViewModel> Documents { get; set; }

        public override string ToString()
        {
            return string.Join(", ", Documents.Select(d => d.ToString()));
        }
    }
}
