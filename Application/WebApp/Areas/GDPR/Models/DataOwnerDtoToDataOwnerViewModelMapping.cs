﻿using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataOwnerDtoToDataOwnerViewModelMapping : ClassMapping<DataOwnerDto, DataOwnerViewModel>
    {
        public DataOwnerDtoToDataOwnerViewModelMapping()
        {
            Mapping = d => new DataOwnerViewModel
            {
                Id = d.Id,
                UserId = d.UserId,
                EmployeeId = d.EmployeeId,
                CandidateId = d.CandidateId,
                RecommendingPersonId = d.RecommendingPersonId,
                IsPotentialDuplicate = d.IsPotentialDuplicate,
                FirstName = d.FirstName,
                LastName = d.LastName,
                EmailAddress = d.EmailAddress,
                PhoneNumber = d.PhoneNumber,
                SocialLink = d.SocialLink,
                HasValidCandidateConsent = d.HasValidCandidateConsent,
                HasValidEmployeeConsent = d.HasValidEmployeeConsent,
                HasValidRecommendingPersonConsent = d.HasValidRecommendingPersonConsent,
                HasValidUserConsent = d.HasValidUserConsent,
                Timestamp = d.Timestamp
            };
        }
    }
}
