﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataOwnerViewModel : IValidatableObject, IGridViewRow
    {
        public long Id { get; set; }

        [Order(1)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(DataOwnerResources.UserLabel), typeof(DataOwnerResources))]
        public long? UserId { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(DataOwnerResources.EmployeeLabel), typeof(DataOwnerResources))]
        public long? EmployeeId { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CandidatePickerController))]
        [DisplayNameLocalized(nameof(DataOwnerResources.CandidateLabel), typeof(DataOwnerResources))]
        public long? CandidateId { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(RecommendingPersonPickerController))]
        [DisplayNameLocalized(nameof(DataOwnerResources.RecommendingPersonLabel), typeof(DataOwnerResources))]
        public long? RecommendingPersonId { get; set; }

        [Order(5)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataOwnerResources.FirstNameLabel), typeof(DataOwnerResources))]
        public string FirstName { get; set; }

        [Order(6)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataOwnerResources.LastNameLabel), typeof(DataOwnerResources))]
        public string LastName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmailAddress { get; set; }

        [Order(7)]
        [SortBy(nameof(EmailAddress))]
        [DisplayNameLocalized(nameof(DataOwnerResources.EmailAddressLabel), typeof(DataOwnerResources))]
        public DisplayUrl GetEmailAddress(UrlHelper helper) => CellContentHelper.GetEmailLink(EmailAddress, EmailAddress);

        [Order(8)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataOwnerResources.PhoneNumberLabel), typeof(DataOwnerResources))]
        public string PhoneNumber { get; set; }

        [Order(9)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataOwnerResources.SocialLink), typeof(DataOwnerResources))]
        public string SocialLink { get; set; }

        [Order(10)]
        [NonSortable]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(DataOwnerResources.ReferencesLabel), typeof(DataOwnerResources))]
        public string References { get { return GetReferenceDescription(); } }

        [Order(11)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(DataOwnerResources.IsPotentialDuplicateLabel), typeof(DataOwnerResources))]
        public bool IsPotentialDuplicate { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool? HasValidCandidateConsent { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool? HasValidRecommendingPersonConsent { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool? HasValidEmployeeConsent { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool? HasValidUserConsent { get; set; }

        public byte[] Timestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CandidateId == null && RecommendingPersonId == null && UserId == null && EmployeeId == null)
            {
                yield return new ValidationResult(DataOwnerResources.AtLeastOneIsRequired);
            }
        }

        public override string ToString()
        {
            var clarification = !string.IsNullOrWhiteSpace(EmailAddress) ? EmailAddress
                : !string.IsNullOrWhiteSpace(PhoneNumber) ? PhoneNumber
                : !string.IsNullOrWhiteSpace(SocialLink) ? SocialLink
                : string.Empty;

            var clarificationFormatted = !string.IsNullOrWhiteSpace(clarification) ? $" ({clarification})" : string.Empty;

            return $"{FirstName} {LastName}{clarificationFormatted}";
        }

        string IGridViewRow.RowCssClass { get { return HasMissingConsent ? CardIndexStyles.RowClassDanger : null; } }

        string IGridViewRow.RowTitle { get { return HasMissingConsent ? GetMissingConsentDescription() : null; } }

        private string GetReferenceDescription()
        {
            var references = new List<string>();

            if (EmployeeId.HasValue)
            {
                references.Add(DataOwnerResources.EmployeeLabel);
            }

            if (CandidateId.HasValue)
            {
                references.Add(DataOwnerResources.CandidateLabel);
            }

            if (RecommendingPersonId.HasValue)
            {
                references.Add(DataOwnerResources.RecommendingPersonLabel);
            }

            if (UserId.HasValue)
            {
                references.Add(DataOwnerResources.UserLabel);
            }

            return string.Join(", ", references);
        }

        private bool HasMissingConsent
        {
            get
            {
                return HasValidCandidateConsent == false
                    || HasValidRecommendingPersonConsent == false
                    || HasValidEmployeeConsent == false
                    || HasValidUserConsent == false;
            }
        }

        private string GetMissingConsentDescription()
        {
            var references = new List<string>();

            if (HasValidEmployeeConsent == false)
            {
                references.Add(DataOwnerResources.EmployeeLabel);
            }

            if (HasValidCandidateConsent == false)
            {
                references.Add(DataOwnerResources.CandidateLabel);
            }

            if (HasValidRecommendingPersonConsent == false)
            {
                references.Add(DataOwnerResources.RecommendingPersonLabel);
            }

            if (HasValidUserConsent == false)
            {
                references.Add(DataOwnerResources.UserLabel);
            }

            if (!references.Any())
            {
                return null;
            }

            return string.Format(DataOwnerResources.HasNoValidConsents, string.Join(", ", references));
        }
    }
}
