﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataActivityDtoToDataActivityViewModelMapping : ClassMapping<DataActivityDto, DataActivityViewModel>
    {
        public DataActivityDtoToDataActivityViewModelMapping()
        {
            Mapping = d => new DataActivityViewModel
            {
                Id = d.Id,
                DataOwnerId = d.DataOwnerId,
                PerformedOn = d.PerformedOn,
                PerformedByUserId = d.PerformedByUserId,
                ActivityType = d.ActivityType,
                ReferenceId = d.ReferenceId,
                ReferenceType = d.ReferenceType,
                S1 = d.S1,
                S2 = d.S2,
                S3 = d.S3,
                Timestamp = d.Timestamp
            };
        }
    }
}
