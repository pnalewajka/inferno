﻿using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class ActivityTypeDtoToActivityTypeViewModelMapping : ClassMapping<ActivityTypeDto, ActivityTypeViewModel>
    {
        public ActivityTypeDtoToActivityTypeViewModelMapping()
        {
            Mapping = da => new ActivityTypeViewModel
            {
                Id = da.Id,
                Description = da.Description
            };
        }
    }
}