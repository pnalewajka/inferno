﻿using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DocumentViewerViewModel : DialogViewModel
    {
        public DocumentViewModel Model { get; private set; }

        public DocumentViewerViewModel(DocumentViewModel model) : base("document-viewer")
        {
            Model = model;
            Title = model.DocumentName;
            Buttons.Add(new DialogButton()
            {
                Text = DocumentViewerResources.DownloadButtonText,
                Tag = "download"
            });
            Buttons.Add(DialogHelper.GetCloseButton());
        }
    }
}