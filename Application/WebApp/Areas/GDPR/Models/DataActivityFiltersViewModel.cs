﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    [Identifier("Filters.DataActivityFilter")]
    [FieldSetDescription(1, GeneralFieldSet)]
    [FieldSetDescription(2, ActivityBetweenFieldSet, nameof(DataActivityResources.ActivityBetweenFieldset), typeof(DataActivityResources))]
    [DescriptionLocalized(nameof(DataActivityResources.DataActivityPropertiesFilter), typeof(DataActivityResources))]
    public class DataActivityFiltersViewModel
    {
        private const string GeneralFieldSet = "General";
        private const string ActivityBetweenFieldSet = "Activity-Between";

        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        [ValuePicker(Type = typeof(DataOwnerPickerController))]
        [DisplayNameLocalized(nameof(DataActivityResources.DataOwnerLabel), typeof(DataActivityResources))]
        public long? OwnerId { get; set; }

        [FieldSet(GeneralFieldSet)]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [ValuePicker(Type = typeof(ActivityTypePickerController))]
        [DisplayNameLocalized(nameof(DataActivityResources.ActivityTypeLabel), typeof(DataActivityResources))]
        public long[] ActivityTypes { get; set; }

        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(DataActivityResources.UserLabel), typeof(DataActivityResources))]
        public long? UserId { get; set; }

        [FieldSet(ActivityBetweenFieldSet)]
        [DisplayNameLocalized(nameof(DataActivityResources.FromLabel), typeof(DataActivityResources))]
        public DateTime? From { get; set; }

        [FieldSet(ActivityBetweenFieldSet)]
        [DisplayNameLocalized(nameof(DataActivityResources.ToLabel), typeof(DataActivityResources))]
        public DateTime? To { get; set; }

        public DataActivityFiltersViewModel()
        {
            ActivityTypes = new long[] { };
        }

        public override string ToString()
        {
            return DataActivityResources.DataActivityPropertiesFilter;
        }
    }
}