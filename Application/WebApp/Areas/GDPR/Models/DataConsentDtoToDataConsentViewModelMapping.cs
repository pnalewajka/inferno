﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataConsentDtoToDataConsentViewModelMapping : ClassMapping<DataConsentDto, DataConsentViewModel>
    {
        public DataConsentDtoToDataConsentViewModelMapping()
        {
            Mapping = d => new DataConsentViewModel
            {
                Id = d.Id,
                DataConsentType = d.Type,
                ExpiresOn = d.ExpiresOn,
                DataAdministratorId = d.DataAdministratorId,
                Documents = (d.Documents ?? new DocumentDto[0])
                    .Select(p => new DocumentViewModel
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    })
                    .ToList(),
                Scope = d.Scope,
                DataOwnerId = d.DataOwnerId
            };
        }
    }
}
