﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class ActivityTypeViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(DataActivityResources.ActivityTypeLabel), typeof(DataActivityResources))]
        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}