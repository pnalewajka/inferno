﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.GDPR.DocumentMappings;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class DataProcessingAgreementConsentViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long AgreementId { get; set; }

        [NonSortable]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(DataProcessingAgreementConsentResources.ProjectsLabel), typeof(DataProcessingAgreementConsentResources))]
        public List<long> ProjectIds { get; set; }

        [NonSortable]
        [DocumentUpload(typeof(DataProcessingAgreementDocumentMapping))]
        [DocumentList(Truncate = false)]
        [DisplayNameLocalized(nameof(DataProcessingAgreementConsentResources.DocumentsLabel), typeof(DataProcessingAgreementConsentResources))]
        public List<DocumentViewModel> Documents { get; set; }

        [NonSortable]
        [ValuePicker(Type = typeof(UserPickerController))]
        [DisplayNameLocalized(nameof(DataProcessingAgreementConsentResources.UserLabel), typeof(DataProcessingAgreementConsentResources))]
        public long UserId { get; set; }

        public DateTime ModifiedOn { get; set; }

        [DisplayNameLocalized(nameof(DataProcessingAgreementConsentResources.IsApprovedLabel), typeof(DataProcessingAgreementConsentResources))]
        public bool IsApproved { get; set; }

        public override string ToString()
        {
            return string.Join(", ", Documents.Select(d => d.ToString()));
        }
    }
}
