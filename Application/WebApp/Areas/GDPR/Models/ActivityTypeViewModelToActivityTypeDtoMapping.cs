﻿using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.GDPR.Models
{
    public class ActivityTypeViewModelToActivityTypeDtoMapping : ClassMapping<ActivityTypeViewModel, ActivityTypeDto>
    {
        public ActivityTypeViewModelToActivityTypeDtoMapping()
        {
            Mapping = da => new ActivityTypeDto
            {
                Id = da.Id,
                Description = da.Description
            };
        }
    }
}