﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.GDPR.Breadcrumbs
{
    public class DataOwnerDataConsentBreadcrumbItem : BreadcrumbItem
    {
        public DataOwnerDataConsentBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = DataConsentResources.Title;
        }
    }
}