﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.GDPR.Breadcrumbs
{
    public class DataOwnerDataActivityBreadcrumbItem : BreadcrumbItem
    {
        public DataOwnerDataActivityBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = DataActivityResources.Title;
        }
    }
}