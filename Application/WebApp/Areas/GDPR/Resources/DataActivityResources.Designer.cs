﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.GDPR.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DataActivityResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DataActivityResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.GDPR.Resources.DataActivityResources", typeof(DataActivityResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity Between.
        /// </summary>
        public static string ActivityBetweenFieldset {
            get {
                return ResourceManager.GetString("ActivityBetweenFieldset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Action.
        /// </summary>
        public static string ActivityTypeLabel {
            get {
                return ResourceManager.GetString("ActivityTypeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Advanced.
        /// </summary>
        public static string AdvancedFilterGroupName {
            get {
                return ResourceManager.GetString("AdvancedFilterGroupName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Activities.
        /// </summary>
        public static string DataActivityPropertiesFilter {
            get {
                return ResourceManager.GetString("DataActivityPropertiesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Owner.
        /// </summary>
        public static string DataOwnerLabel {
            get {
                return ResourceManager.GetString("DataOwnerLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to From.
        /// </summary>
        public static string FromLabel {
            get {
                return ResourceManager.GetString("FromLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Who.
        /// </summary>
        public static string PerformedByUserLabel {
            get {
                return ResourceManager.GetString("PerformedByUserLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to When.
        /// </summary>
        public static string PerformedOnLabel {
            get {
                return ResourceManager.GetString("PerformedOnLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to S1.
        /// </summary>
        public static string S1Label {
            get {
                return ResourceManager.GetString("S1Label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to S2.
        /// </summary>
        public static string S2Label {
            get {
                return ResourceManager.GetString("S2Label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to S3.
        /// </summary>
        public static string S3Label {
            get {
                return ResourceManager.GetString("S3Label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Activities.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To.
        /// </summary>
        public static string ToLabel {
            get {
                return ResourceManager.GetString("ToLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User.
        /// </summary>
        public static string UserLabel {
            get {
                return ResourceManager.GetString("UserLabel", resourceCulture);
            }
        }
    }
}
