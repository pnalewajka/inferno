﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataOwners)]
    public class DataOwnerController : CardIndexController<DataOwnerViewModel, DataOwnerDto>
    {
        private const string DataConsentGroup = "data-consent";
        private const string DataActivityGroup = "data-activity";
        private readonly IDataOwnerService _dataOwnerService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IClassMappingFactory _classMappingFactory;

        public DataOwnerController(IDataOwnerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies,
            IDataOwnerService dataOwnerService, IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _dataOwnerService = dataOwnerService;
            _dataActivityLogService = dataActivityLogService;
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;

            CardIndex.Settings.Title = DataOwnerResources.Title;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDataOwners;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDataOwners;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDataOwners;
            
            CardIndex.Settings.ToolbarButtons.AddRange(GetDataConsentsCommandButtons());
            CardIndex.Settings.ToolbarButtons.AddRange(GetDataActivitiesCommandButtons());
            CardIndex.Settings.ToolbarButtons.Add(GetMergeDataOwnerButton());
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        [HttpPost]
        public ActionResult Merge(MergeDataOwnerViewModel viewModel)
        {
            _dataActivityLogService.LogDataOwnerDataActivity(viewModel.DataOwnerId, ActivityType.MergeDataOwner, viewModel.MergeDataOwnerId.ToString());

            var validationResult = ValidateViewModel(viewModel);

            if (validationResult.Any())
            {
                return MergeDialog(viewModel.DataOwnerId, validationResult);
            }

            var mergeResult = _dataOwnerService.MergeDataOwners(viewModel.DataOwnerId, viewModel.MergeDataOwnerId);

            if (mergeResult.IsSuccessful)
            {
                return DialogHelper.Reload();
            }

            var viewModelAlerts = mergeResult.Alerts
                .Select(a => new AlertViewModel
                {
                    Type = a.Type,
                    Message = a.Message
                }).ToList();

            return MergeDialog(viewModel.DataOwnerId, viewModelAlerts);
        }

        [HttpPost]
        public ActionResult MergeDialog(long id, List<AlertViewModel> alerts = null)
        {
            var viewModel = new MergeDataOwnerViewModel { DataOwnerId = id };

            var dialogModel = new EditDialogViewModel<MergeDataOwnerViewModel>(viewModel)
            {
                Title = DataOwnerResources.MergeDataOwners,
                Alerts = alerts
            };

            return PartialView("DataOwnerMergeDialog", dialogModel);
        }

        private IEnumerable<CommandButton> GetDataConsentsCommandButtons()
        {
            yield return new ToolbarButton
            {
                Text = DataOwnerResources.DataConsentsButtonText,
                OnClickAction = Url.UriActionGet(nameof(DataOwnerDataConsentController.List),
                    RoutingHelper.GetControllerName<DataOwnerDataConsentController>(), KnownParameter.ParentId),
                Icon = FontAwesome.BalanceScale,
                RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataConsentGroup
            };

            var addButton = new ToolbarButton
            {
                Text = DataOwnerResources.DataConsentsAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<DataOwnerDataConsentController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataConsentGroup
            };

            DataConsentController.SetAddDataConsentDropdownItems(addButton, Url, 
                RoutingHelper.GetControllerName<DataOwnerDataConsentController>(), nameof(GetAddDialog), KnownParameter.ParentId);

            yield return addButton;
        }

        private IEnumerable<CommandButton> GetDataActivitiesCommandButtons()
        {
            yield return new ToolbarButton
            {
                Text = DataOwnerResources.DataActivitiesButtonText,
                OnClickAction = Url.UriActionGet(nameof(DataOwnerDataActivityController.List),
                    RoutingHelper.GetControllerName<DataOwnerDataActivityController>(), KnownParameter.ParentId),
                Icon = FontAwesome.History,
                RequiredRole = SecurityRoleType.CanViewDataActivities,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataActivityGroup
            };

            yield return new ToolbarButton
            {
                Text = DataOwnerResources.DataActivitiesAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<DataOwnerDataActivityController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddDataActivities,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataActivityGroup
            };
        }

        private ICommandButton GetMergeDataOwnerButton()
        {
            return new ToolbarButton
            {
                Text = DataOwnerResources.MergeDataOwners,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(MergeDialog), RoutingHelper.GetControllerName(this), KnownParameter.SelectedId)),
                Icon = FontAwesome.Compress,
                RequiredRole = SecurityRoleType.CanMergeDataOwners,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };
        }

        private List<AlertViewModel> ValidateViewModel(object viewModel)
        {
            var validationResult = TryValidateModel(viewModel);

            if (validationResult)
            {
                return new List<AlertViewModel>();
            }

            return ModelState
                   .SelectMany(e => e.Value.Errors)
                   .Select(e => e.ErrorMessage)
                   .Distinct()
                   .Select(message => new AlertViewModel { Message = message, Type = AlertType.Error })
                   .ToList();
        }
    }
}