﻿using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [Identifier("ValuePickers.ActivityType")]
    [AtomicAuthorize(SecurityRoleType.CanViewDataActivities)]
    public class ActivityTypePickerController
        : ReadOnlyCardIndexController<ActivityTypeViewModel, ActivityTypeDto>
    {
        public ActivityTypePickerController(IActivityTypeCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}