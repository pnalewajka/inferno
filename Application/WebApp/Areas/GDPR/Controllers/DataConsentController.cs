﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;
using RecruitmentModels = Smt.Atomic.WebApp.Areas.Recruitment.Models;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
    public class DataConsentController : CardIndexController<DataConsentViewModel, DataConsentDto>
    {
        private readonly IDocumentDownloadService _documentDownloadService;
        private readonly ICustomerCardIndexDataService _customerService;
        private readonly IDataActivityLogService _dataActivityLogService;
        IDataConsentCardIndexDataService _cardIndexDataService;
        internal const string ConsentTypeParameterName = "consent-type";

        public DataConsentController(IDataConsentCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDocumentDownloadService documentDownloadService,
            ICustomerCardIndexDataService customerService,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _documentDownloadService = documentDownloadService;
            _customerService = customerService;
            _dataActivityLogService = dataActivityLogService;
            CardIndex.Settings.Title = DataConsentResources.Title;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentDataConsent;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentDataConsent;

            Layout.Css.Add("~/Areas/Recruitment/Content/DocumentViewer.css");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/DocumentViewer.js");

            SetAddDataConsentDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this), nameof(GetAddDialog));

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;

            ApplyFormConfiguration(CardIndex.Settings);
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
        public ActionResult ViewFile(long id)
        {
            TempData[GetTempDataKey(id)] = RecruitmentModels.DownloadJustification.CreateViewedDocumentDownloadJustification();

            Layout.MasterName = null;

            var viewModel = new DocumentViewModel
            {
                DocumentId = id,
                DocumentName = _cardIndexDataService.GetDocumentNameByDataConsentDocumentId(id)
            };

            var dialogModel = new DocumentViewerViewModel(viewModel);

            return PartialView("DataConsentDocumentViewer", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
        public ActionResult PerformDownload(RecruitmentModels.DownloadJustificationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Download(model);
            }

            var customer = _customerService.GetRecordById(model.CustomerId);

            TempData[GetTempDataKey(model.DocumentId)] = RecruitmentModels.DownloadJustification.CreateDocumentSharedWithCustomerDownloadJustification(customer, model.Comment);

            return DialogHelper.Redirect(this, Url.Action(nameof(PerformDownload), new { Id = model.DocumentId }));
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
        public ActionResult PerformDownload(long id)
        {
            var documentDownloadDto = _documentDownloadService.GetDocument("Documents.DataConsentDocumentMapping", id);
            var dataConsentId = _cardIndexDataService.GetDataConsentIdByDataConsentDocumentId(id);
            var dataOwnerId = _cardIndexDataService.GetDataOwnerIdByDataConsentId(dataConsentId);
            var tempDataKey = GetTempDataKey(id);
            var downloadJustification = TempData[tempDataKey] as RecruitmentModels.DownloadJustification;

            if (Request?.UrlReferrer?.LocalPath == "/Scripts/ViewerJS/")
            {
                TempData.Keep(tempDataKey);
            }

            if (downloadJustification == null)
            {
                AddAlert(AlertType.Error, DataConsentResources.NoDownloadJustificationError);

                return RedirectToAction(nameof(View), new RouteValueDictionary { { "id", dataConsentId } });
            }

            _dataActivityLogService.LogDataOwnerDataActivity(
                dataOwnerId,
                downloadJustification.ActivityType,
                GetType().Name,
                documentDownloadDto.DocumentName,
                downloadJustification.Text);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
        public ActionResult Download(long id)
        {
            return Download(new RecruitmentModels.DownloadJustificationViewModel(id));
        }

        private ActionResult Download(RecruitmentModels.DownloadJustificationViewModel model)
        {
            var documentName = _cardIndexDataService.GetDocumentNameByDataConsentDocumentId(model.DocumentId);
            var dialogViewModel = new ModelBasedDialogViewModel<RecruitmentModels.DownloadJustificationViewModel>(model)
            {
                Title = string.Format(DataConsentResources.DocumentShareDialogTitle, documentName),
                LabelWidth = LabelWidth.Large
            };

            return PartialView(
                    "DataConsentDownloadJustification",
                    dialogViewModel
                    );
        }

        internal static void SetAddDataConsentDropdownItems(
            ICommandButton addButton,
            UrlHelper urlHelper,
            string controllerName,
            string actionName,
            KnownParameter knownParameters = KnownParameter.None)
        {
            var consentTypes = EnumHelper.GetEnumValues<DataConsentType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var consentType in consentTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(consentType.ToString());
                
                var addItemButton = new CommandButton
                {
                    Group = DataConsentResources.ConsentTypeLabel,
                    RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                    Text = consentType.GetDescriptionOrValue(),
                    OnClickAction = JavaScriptCallAction.ShowDialog(urlHelper.UriActionPost(actionName, controllerName, KnownParameter.Context | knownParameters, ConsentTypeParameterName, typeValueName))
                };

                addDropdownItems.Add(addItemButton);
            }

            addButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        internal static void ApplyFormConfiguration(CardIndexSettings cardIndexSettings)
        {
            cardIndexSettings.EditFormCustomization = EditFormCustomization;
        }

        private static void EditFormCustomization(FormRenderingModel form)
        {
            form.GetControlByName<DataConsentViewModel>(c => c.DataAdministratorId).IsReadOnly = true;
            form.GetControlByName<DataConsentViewModel>(c => c.DataOwnerId).IsReadOnly = true;
            form.GetControlByName<DataConsentViewModel>(c => c.DataConsentType).IsReadOnly = true;

            form.GetControlByName<DataConsentViewModel>(c => c.InboundEmailDocumentId).Visibility = ControlVisibility.Hide;
            form.GetControlByName<DataConsentViewModel>(c => c.SourceEmailId).Visibility = ControlVisibility.Hide;
            form.GetControlByName<DataConsentViewModel>(c => c.Documents).Label = DataConsentResources.DocumentsLabel;
        }

        private string GetTempDataKey(long id) => $"document-download-justification:{id}";

        private object GetDefaultNewRecord()
        {
            var consentTypeName = Request.QueryString[ConsentTypeParameterName];
            var consentType = EnumHelper.GetEnumValueOrDefault<DataConsentType>(consentTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (consentType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            var dto = _cardIndexDataService.GetDefaultNewRecord(consentType.Value);

            return dto;
        }
    }
}