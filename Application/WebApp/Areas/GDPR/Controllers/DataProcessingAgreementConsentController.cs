﻿using System.Collections.Generic;
using System.Security;
using System.Web.Mvc;
using Smt.Atomic.Business.GDPR.Consts;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataProcessingAgreementConsent)]
    public class DataProcessingAgreementConsentController
        : ReadOnlyCardIndexController<DataProcessingAgreementConsentViewModel, DataProcessingAgreementConsentDto>
    {
        private readonly IDataProcessingAgreementService _dataProcessingAgreementService;

        public DataProcessingAgreementConsentController(
            IDataProcessingAgreementConsentCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies,
            IDataProcessingAgreementService dataProcessingAgreementService)
            : base(cardIndexDataService, dependencies)
        {
            _dataProcessingAgreementService = dataProcessingAgreementService;

            CardIndex.Settings.Title = DataProcessingAgreementResources.DataProcessingAgreementConsent;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>();

            Layout.Scripts.Add("~/Areas/GDPR/Scripts/DataProcessingAgreementConsent.js");
            Layout.Resources.AddFrom<DataProcessingAgreementResources>(nameof(DataProcessingAgreementResources.DataProcessingAgreementConsentConfirmationMessage));

            CreateGridViewConfigurations();
            CreateRowButtons();
            CreateFilters();
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanApproveDataProcessingAgreementConsent)]
        public ActionResult Approve(long id)
        {
            var result = _dataProcessingAgreementService.ApproveConsent(id);

            if (!result)
            {
                throw new SecurityException();
            }

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return Redirect(listActionUrl);
        }

        private void CreateFilters()
        {
            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = DataProcessingAgreementResources.ContextFilterGroup,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.AllConsents, DisplayName = DataProcessingAgreementResources.AllFilter },
                        new Filter { Code = FilterCodes.OwnConsents, DisplayName = DataProcessingAgreementResources.OwnFilter },
                    }
                },

                new FilterGroup
                {
                    DisplayName = DataProcessingAgreementResources.ApprovalStatusFilterGroup,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.ConsentApproved, DisplayName = DataProcessingAgreementResources.ApprovedFilter },
                        new Filter { Code = FilterCodes.ConsentPending, DisplayName = DataProcessingAgreementResources.PendingFilter },
                    }
                },
            };
        }

        private void CreateRowButtons()
        {
            var currentUserId = GetCurrentPrincipal().Id.Value;

            var approveButton = new RowButton
            {
                Text = DataProcessingAgreementResources.ApproveAction,
                OnClickAction = new JavaScriptCallAction($"GDPR.DataProcessingAgreementConsent.DataProcessingAgreementConsentConfirmation(this.rowData[0], '{Url.Action(nameof(Approve))}')"),
                Icon = FontAwesome.Check,
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanApproveDataProcessingAgreementConsent,
                Availability = new RowButtonAvailability<DataProcessingAgreementConsentViewModel>
                {
                    Predicate = e => !e.IsApproved && e.UserId == currentUserId,
                }
            };

            CardIndex.Settings.RowButtons.Add(approveButton);
        }

        private void CreateGridViewConfigurations()
        {
            CardIndex.Settings.GridViewConfigurations = new List<IGridViewConfiguration>();

            var canViewAllDataProcessingConsents = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewAllDataProcessingAgreementConsent);

            var simpleView = new GridViewConfiguration<DataProcessingAgreementConsentViewModel>(DataProcessingAgreementResources.SimpleView, "simple")
            {
                IsDefault = !canViewAllDataProcessingConsents,
            };

            simpleView.IncludeColumns(
                v => v.ProjectIds,
                v => v.Documents,
                v => v.IsApproved);

            CardIndex.Settings.GridViewConfigurations.Add(simpleView);

            if (canViewAllDataProcessingConsents)
            {
                var legalView = new GridViewConfiguration<DataProcessingAgreementConsentViewModel>(DataProcessingAgreementResources.LegalView, "legal")
                {
                    IsDefault = true,
                };

                legalView.IncludeColumns(
                    v => v.ProjectIds,
                    v => v.UserId,
                    v => v.Documents,
                    v => v.IsApproved);

                CardIndex.Settings.GridViewConfigurations.Add(legalView);
            }
        }
    }
}
