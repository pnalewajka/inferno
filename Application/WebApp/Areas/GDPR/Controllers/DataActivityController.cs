﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.GDPR.Consts;
using Smt.Atomic.Business.GDPR.Dto.Filters;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataActivities)]
    public class DataActivityController : CardIndexController<DataActivityViewModel, DataActivityDto>
    {
        internal const string ReferenceTypePatameterName = "reference-type";
        private readonly IDataActivityCardIndexDataService _cardIndexDataService;

        public DataActivityController(IDataActivityCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = DataActivityResources.Title;

            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDataActivities;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDataActivities;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDataActivities;

            SetAddDataActivityDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this), nameof(GetAddDialog));

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.FilterGroups = GetFilters();

            _cardIndexDataService = cardIndexDataService;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        internal static void SetAddDataActivityDropdownItems(
            ICommandButton addButton,
            UrlHelper urlHelper,
            string controllerName,
            string actionName,
            KnownParameter knownParameters = KnownParameter.None)
        {
            var referenceTypes = EnumHelper.GetEnumValues<ReferenceType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var referenceType in referenceTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(referenceType.ToString());

                var addItemButton = new CommandButton
                {
                    Group = DataConsentResources.ConsentTypeLabel,
                    RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                    Text = referenceType.GetDescriptionOrValue(),
                    OnClickAction = JavaScriptCallAction.ShowDialog(urlHelper.UriActionPost(actionName, controllerName, KnownParameter.Context | knownParameters, ReferenceTypePatameterName, typeValueName))
                };

                addDropdownItems.Add(addItemButton);
            }

            addButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        private object GetDefaultNewRecord()
        {
            var referenceTypeName = Request.QueryString[ReferenceTypePatameterName];
            var referenceType = EnumHelper.GetEnumValueOrDefault<ReferenceType>(referenceTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (referenceType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            var dto = _cardIndexDataService.GetDefaultNewRecord(referenceType.Value);

            return dto;
        }

        private IList<FilterGroup> GetFilters()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = DataActivityResources.AdvancedFilterGroupName,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<DataActivityFiltersViewModel, DataActivityFiltersDto>(
                            FilterCodes.DataActivityFilters,
                            DataActivityResources.AdvancedFilterGroupName)
                    }
                }
            };
        }
    }
}