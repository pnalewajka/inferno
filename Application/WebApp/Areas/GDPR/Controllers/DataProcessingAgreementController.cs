﻿using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataProcessingAgreement)]
    public class DataProcessingAgreementController : CardIndexController<DataProcessingAgreementViewModel, DataProcessingAgreementDto>
    {
        public DataProcessingAgreementController(IDataProcessingAgreementCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = DataProcessingAgreementResources.DataProcessingAgreement;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDataProcessingAgreement;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDataProcessingAgreement;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDataProcessingAgreement;

            CardIndex.Settings.EditFormCustomization += (FormRenderingModel model) =>
            {
                var documentsControl = model.GetControlByName<DataProcessingAgreementViewModel>(v => v.Documents);
                documentsControl.IsReadOnly = true;
            };
        }
    }
}
