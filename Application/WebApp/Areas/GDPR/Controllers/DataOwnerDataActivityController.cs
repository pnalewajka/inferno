﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataActivities)]
    public class DataOwnerDataActivityController : SubCardIndexController<DataActivityViewModel, DataActivityDto, DataOwnerController, ParentIdContext>
    {
        private IDataOwnerDataActivityCardIndexDataService _cardIndexDataService;

        public DataOwnerDataActivityController(
            IDataOwnerDataActivityCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = DataActivityResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDataActivities;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewDataActivities;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDataActivities;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDataActivities;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;

            CardIndex.Settings.AddFormCustomization += EditAddFormCustomization;
            CardIndex.Settings.EditFormCustomization += EditAddFormCustomization;
        }

        private Action<FormRenderingModel> EditAddFormCustomization = form =>
        {
            form.GetControlByName<DataActivityViewModel>(m => m.DataOwnerId).IsReadOnly = true;
            form.GetControlByName<DataActivityViewModel>(m => m.ReferenceId).IsReadOnly = true;
            form.GetControlByName<DataActivityViewModel>(m => m.ReferenceType).IsReadOnly = true;
        };

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.DataOwnerId = Context.ParentId;
            dto.ReferenceId = Context.ParentId;
            dto.ReferenceType = ReferenceType.DataOwner;

            return dto;
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();
            var configuration = new GridViewConfiguration<DataActivityViewModel>(DataActivityResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(c => c.DataOwnerId);
            configurations.Add(configuration);

            return configurations;
        }
    }
}