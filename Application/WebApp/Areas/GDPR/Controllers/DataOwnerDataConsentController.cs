﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
    public class DataOwnerDataConsentController : SubCardIndexController<DataConsentViewModel, DataConsentDto, DataOwnerController, ParentIdContext>
    {
        private IDataOwnerDataConsentCardIndexDataService _cardIndexDataService;

        public DataOwnerDataConsentController(
            IDataOwnerDataConsentCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = DataConsentResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentDataConsent;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentDataConsent;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;

            DataConsentController.ApplyFormConfiguration(CardIndex.Settings);
            DataConsentController.SetAddDataConsentDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this), nameof(GetAddDialog));
        }

        private object GetDefaultNewRecord()
        {
            var consentTypeName = Request.QueryString[DataConsentController.ConsentTypeParameterName];
            var consentType = EnumHelper.GetEnumValueOrDefault<DataConsentType>(consentTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (consentType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            return _cardIndexDataService.GetDefaultNewRecord(Context.ParentId, consentType.Value);
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();
            var configuration = new GridViewConfiguration<DataConsentViewModel>(ContactRecordResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(c => c.DataOwnerId);
            configurations.Add(configuration);

            return configurations;
        }
    }
}