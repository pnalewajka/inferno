﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.GDPR.Controllers
{
    [Identifier("GDPRValuePickers.DataOwner")]
    [AtomicAuthorize(SecurityRoleType.CanViewDataOwners)]
    public class DataOwnerPickerController : ReadOnlyCardIndexController<DataOwnerViewModel, DataOwnerDto>
    {
        private const string MinimalViewId = "minimal";

        public DataOwnerPickerController(IDataOwnerPickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = DataOwnerResources.Title;
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<DataOwnerViewModel>(DataOwnerResources.MinimalViewText, MinimalViewId) { IsDefault = true };

            configuration.IncludeColumns(new List<Expression<Func<DataOwnerViewModel, object>>>
            {
                o => o.FirstName,
                o => o.LastName,
                o => o.PhoneNumber
            });

            configuration.IncludeMethodColumn(nameof(DataOwnerViewModel.GetEmailAddress));

            configuration.IncludeColumns(new List<Expression<Func<DataOwnerViewModel, object>>>
            {
                o => o.SocialLink
            });

            configurations.Add(configuration);

            return configurations;
        }
    }
}