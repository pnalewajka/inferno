﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.GDPR
{
    public class GDPRAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GDPR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GDPR_default",
                "GDPR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
