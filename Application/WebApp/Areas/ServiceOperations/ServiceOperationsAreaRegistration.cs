﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations
{
    public class ServiceOperationsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ServiceOperations";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ServiceOperations_default2",
                "ServiceOperations/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}