﻿using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class PerformRequestApproversChangeViewModel
    {
        public RequestApproversChangeViewModel ViewModel { get; set; }
        public ICollection<ApprovalGroupViewModel> RecordsToChanged { get; set; }
    }
}