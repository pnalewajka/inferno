﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class PerformTimeReportChangeServiceViewModel
    {
        public TimeReportChangeServiceViewModel ViewModel { get; set; }
        public IList<TimeReportRow> RecordsToChanged { get; set; }
    }
}