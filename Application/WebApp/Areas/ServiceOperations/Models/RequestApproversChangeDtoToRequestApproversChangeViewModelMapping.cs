﻿using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class RequestApproversChangeDtoToRequestApproversChangeViewModelMapping :
        ClassMapping<RequestApproversChangeDto, RequestApproversChangeViewModel>
    {
        public RequestApproversChangeDtoToRequestApproversChangeViewModelMapping()
        {
            Mapping = d => new RequestApproversChangeViewModel
            {
                RequestId = d.RequestId
            };
        }
    }
}