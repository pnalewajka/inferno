﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class RequestApproversChangeViewModel
    {
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RequestApproversChangeServiceResources.RequestIdentifierLabel), typeof(RequestApproversChangeServiceResources))]
        public long Id { get; set; }

        [HiddenInput]
        public long RequestId { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.None)]
        [CustomControl(TemplatePath = "Areas.Workflows.Views.Shared.ApproverGroups")]
        public ApprovalGroupViewModel[] ApprovalGroups { get; set; }


        [ReadOnly(true)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(RequestApproversChangeServiceResources.Approvers), typeof(RequestApproversChangeServiceResources))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] ApproversIds { get; set; }

        [HiddenInput]
        public long[] EmployeesIds { get; set; }


        [Required]
        [DisplayNameLocalized(nameof(RequestApproversChangeServiceResources.ApproverChangeLabel), typeof(RequestApproversChangeServiceResources))]
        [ValuePicker(Type = typeof(ApprovalPickerController),
             OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name = " + nameof(Id) + "]').val()")]
        public long ApprovalId { get; set; }


        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(RequestApproversChangeServiceResources.ChangeToLabel), typeof(RequestApproversChangeServiceResources))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long EmployeeId { get; set; }
    }
}