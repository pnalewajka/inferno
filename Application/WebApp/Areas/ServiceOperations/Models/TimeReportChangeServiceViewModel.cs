﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class TimeReportChangeServiceViewModel
    {
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(MonthsListItemProvider))]
        [DisplayNameLocalized(nameof(TimeReportChangeServiceResources.MonthText), typeof(TimeReportChangeServiceResources))]
        public int? Month { get; set; }

        [DisplayNameLocalized(nameof(TimeReportChangeServiceResources.YearText), typeof(TimeReportChangeServiceResources))]
        public int? Year { get; set; }

        [Required]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(TimeReportChangeServiceResources.ProjectSourceText), typeof(TimeReportChangeServiceResources))]
        public long ProjectSourceId { get; set; }

        [Required]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(TimeReportChangeServiceResources.ProjectDestinationText), typeof(TimeReportChangeServiceResources))]
        public long ProjectDestinationId { get; set; }

        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(TimeReportChangeServiceResources.EmployeesText), typeof(TimeReportChangeServiceResources))]
        public long[] EmployeeIds { get; set; }

        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "EmptyStatusText",
            ResourceType = typeof(TimeReportResources), ItemProvider = typeof(EnumBasedListItemProvider<TimeReportStatus>))]
        [DisplayNameLocalized(nameof(TimeReportChangeServiceResources.TRStatusText), typeof(TimeReportChangeServiceResources))]
        public TimeReportStatus? TimeReportStatus { get; set; }
    }
}
