﻿using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class TimeReportChangeServiceViewModelToTimeReportChangeServiceDtoMapping : ClassMapping<TimeReportChangeServiceViewModel, TimeReportChangeServiceDto>
    {
        public TimeReportChangeServiceViewModelToTimeReportChangeServiceDtoMapping()
        {
            Mapping = v => new TimeReportChangeServiceDto
            {
                Month = v.Month,
                Year = v.Year,
                ProjectSourceId = v.ProjectSourceId,
                ProjectDestinationId = v.ProjectDestinationId,
                EmployeeIds = v.EmployeeIds,
                TimeReportStatus = v.TimeReportStatus
            };
        }
    }
}
