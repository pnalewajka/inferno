﻿using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class TimeReportChangeServiceDtoToTimeReportChangeServiceViewModelMapping : ClassMapping<TimeReportChangeServiceDto, TimeReportChangeServiceViewModel>
    {
        public TimeReportChangeServiceDtoToTimeReportChangeServiceViewModelMapping()
        {
            Mapping = d => new TimeReportChangeServiceViewModel
            {
                Month = d.Month,
                Year = d.Year,
                ProjectSourceId = d.ProjectSourceId,
                ProjectDestinationId = d.ProjectDestinationId,
                EmployeeIds = d.EmployeeIds,
                TimeReportStatus = d.TimeReportStatus
            };
        }
    }
}
