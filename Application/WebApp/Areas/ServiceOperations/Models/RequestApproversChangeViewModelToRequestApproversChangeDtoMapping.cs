﻿using System.Linq;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Models
{
    public class RequestApproversChangeViewModelToRequestApproversChangeDtoMapping : ClassMapping<RequestApproversChangeViewModel, RequestApproversChangeDto>
    {
        public RequestApproversChangeViewModelToRequestApproversChangeDtoMapping()
        {
            Mapping = v => new RequestApproversChangeDto
            {
                RequestId = v.RequestId,
                ApprovalId = v.ApprovalId,
                EmployeeId = v.EmployeeId
            };
        }
    }
}
