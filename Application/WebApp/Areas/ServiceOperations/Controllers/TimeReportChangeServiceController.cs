﻿using Smt.Atomic.Presentation.Renderers.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using System.Web.Mvc;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Models;
using Smt.Atomic.Business.ServiceOperations.Interfaces;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanReplaceProjectInReports)]
    public class TimeReportChangeServiceController : AtomicController
    {
        private readonly ITimeReportSupportService _projectSupportService;
        private readonly IClassMapping<TimeReportChangeServiceViewModel, TimeReportChangeServiceDto> _modifyProjectViewModelViewModelToDtoClassMapping;
        private readonly ITimeService _timeService;

        public TimeReportChangeServiceController(ITimeReportSupportService projectSupportService,
            IClassMapping<TimeReportChangeServiceViewModel, TimeReportChangeServiceDto> modifyProjectViewModelViewModelToDtoClassMapping,
            IBaseControllerDependencies baseDependencies,
            ITimeService timeService) : base(baseDependencies)
        {
            _projectSupportService = projectSupportService;
            _modifyProjectViewModelViewModelToDtoClassMapping = modifyProjectViewModelViewModelToDtoClassMapping;
            _timeService = timeService;
        }

        [BreadcrumbBar("Start/ReplaceProject")]
        public ActionResult ChangeTimeReports()
        {
            Layout.PageTitle = Layout.PageHeader = TimeReportChangeServiceResources.ChangeReportHeader;
            var currentDate = _timeService.GetCurrentDate();

            return View("ChangeTimeReports", new TimeReportChangeServiceViewModel() { Month = currentDate.Month, Year = currentDate.Year });
        }

        [HttpPost]
        [BreadcrumbBar("Start/ReplaceProject")]
        public ActionResult ChangeTimeReports(TimeReportChangeServiceViewModel model)
        {
            Layout.PageTitle = Layout.PageHeader = TimeReportChangeServiceResources.ChangeReportHeader;

            if (ModelState.IsValid)
            {
                PerformTimeReportChangeServiceViewModel performVM = new PerformTimeReportChangeServiceViewModel();
                performVM.RecordsToChanged = _projectSupportService.GetTimeReportsToChange(_modifyProjectViewModelViewModelToDtoClassMapping.CreateFromSource(model));
                performVM.ViewModel = model;

                return View("ConfirmChangeTimeReports", performVM);
            }

            return View("ChangeTimeReports", model);
        }

        [HttpPost]
        [BreadcrumbBar("Start/ReplaceProject")]
        public ActionResult ConfirmChangeTimeReports(TimeReportChangeServiceViewModel model)
        {
            BoolResult result = _projectSupportService.ChangeProjectForTimeReport(_modifyProjectViewModelViewModelToDtoClassMapping.CreateFromSource(model));
            AddAlerts(result.Alerts);

            if (result.IsSuccessful)
            {
                var currentDate = _timeService.GetCurrentDate();

                return Redirect(Url.UriActionGet(nameof(TimeReportController.List), RoutingHelper.GetControllerName<TimeReportController>(), KnownParameter.None,
                    "area", RoutingHelper.GetAreaName(typeof(TimeReportController)),
                    "filter", "all-employees",
                    "year", currentDate.Year.ToString(),
                    "month", currentDate.Month.ToString()).Url);
            }

            return View("ChangeTimeReports", model);
        }
    }
}