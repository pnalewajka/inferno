﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.Business.ServiceOperations.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Models;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.Business.Allocation.Interfaces;

namespace Smt.Atomic.WebApp.Areas.ServiceOperations.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanChangeApproversInRequests)]
    public class RequestApproversChangeServiceController : AtomicController
    {
        private readonly IRequestApproversChangeService _approversChangeService;
        private readonly IClassMapping<ApprovalGroupDto, ApprovalGroupViewModel> _aprovalGroupDtoToVievModelMapping;
        private readonly IEmployeeService _employeeService;
        private readonly IClassMapping<RequestApproversChangeViewModel, RequestApproversChangeDto> _modifyRequestViewModelViewModelToDtoClassMapping;

        public RequestApproversChangeServiceController(IRequestApproversChangeService approversChangeService,
            IClassMapping<RequestApproversChangeViewModel, RequestApproversChangeDto> modifyRequestViewModelViewModelToDtoClassMapping,
            IClassMapping<ApprovalGroupDto, ApprovalGroupViewModel> aprovalGroupDtoToVievModelMapping,
            IBaseControllerDependencies baseDependencies,
            IRequestWorkflowService workflowService,
            IEmployeeService employeeService ) : base(baseDependencies)
        {
            _approversChangeService = approversChangeService;
            _modifyRequestViewModelViewModelToDtoClassMapping = modifyRequestViewModelViewModelToDtoClassMapping;
            _aprovalGroupDtoToVievModelMapping = aprovalGroupDtoToVievModelMapping;
            _employeeService = employeeService;
        }

        [BreadcrumbBar("Start/ChangeApprovers")]
        public ActionResult ChangeApprovers(long id)
        {
            Layout.PageTitle = Layout.PageHeader = RequestApproversChangeServiceResources.ChangeRequestApproversHeader;

            var approvalsDto = _approversChangeService.GetApprovalsDtoToChange(id);
            var approvals = approvalsDto.Select(a => _aprovalGroupDtoToVievModelMapping.CreateFromSource(a)).ToList();
            var approversEmployeeIds = approvals.SelectMany(g => g.Approvals.Select(a => a.UserId)).ToArray();
            var employeesIds =
                _employeeService.GetEmployeeIds(
                    approversEmployeeIds);

            var racsvm = new RequestApproversChangeViewModel
            {
                Id = id,
                RequestId = id,
                ApprovalGroups = approvals.ToArray(),
                EmployeesIds = employeesIds.ToArray(),
                ApproversIds = employeesIds.ToArray()
            };

            return View("ChangeApprovers", racsvm);
        }

        [HttpPost]
        [BreadcrumbBar("Start/ChangeApprovers")]
        public ActionResult ChangeApprovers(RequestApproversChangeViewModel model)
        {
            Layout.PageTitle = Layout.PageHeader = RequestApproversChangeServiceResources.ChangeRequestApproversHeader;

            if (ModelState.IsValid)
            {
                var performVM = new PerformRequestApproversChangeViewModel();
                var approvalsDto =  _approversChangeService.GetApprovalsDtoToChange(model.RequestId);
                performVM.RecordsToChanged =  approvalsDto.Select(a => _aprovalGroupDtoToVievModelMapping.CreateFromSource(a)).ToList();

                performVM.ViewModel = model;

                return View("ConfirmChangeApprovers", performVM);
            }

            return View("ChangeApprovers", model);
        }

        [HttpPost]
        [BreadcrumbBar("Start/ChangeApprovers")]
        public ActionResult ConfirmChangeApprovers(RequestApproversChangeViewModel model)
        {
            var result =
                _approversChangeService.ChangeApproverForRequest(
                    _modifyRequestViewModelViewModelToDtoClassMapping.CreateFromSource(model));
            AddAlerts(result.Alerts);

            if (result.IsSuccessful)
            {
                return Redirect(Url.UriActionGet(nameof(RequestController.List),
                    RoutingHelper.GetControllerName<RequestController>(),
                    KnownParameter.None, "area", RoutingHelper.GetAreaName(typeof(RequestController)),
                    "filter", "all-requests").Url);
            }

            return View("ChangeApprovers", model);
        }
    }
}