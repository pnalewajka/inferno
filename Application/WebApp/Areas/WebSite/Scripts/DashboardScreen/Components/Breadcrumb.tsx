﻿namespace Dashboard {
    export class Breadcrumb extends Dante.Components.StateComponent<IDashboardDataProps, { name?: string }> {
        public componentWillMount() {
            hashChangedEvent.subscribe(() => {
                this.handleHashChanged();
            });
            this.handleHashChanged();
        }

        private handleHashChanged(): void {
            const [selected] = this.props.dashboardGroups.filter(e => e.urlParameter === window.location.hash.substr(1));

            if (selected !== undefined && selected.displayName !== undefined) {
                this.updateState({ name: selected.displayName });
            } else {
                this.updateState({ name: ""});
            }
        }

        public render(): JSX.Element {
            return (
                <div className="breadcrumb-container">
                    <li className="active"><a href="/">{Dashboard.Translations.Home}</a></li>
                    <li className="active"><a href="/WebSite/dashboard">{Dashboard.Translations.Dashboard}</a></li>
                    <li className="active">{this.state.name}</li>
                </div>
            );
        }
    }
}
