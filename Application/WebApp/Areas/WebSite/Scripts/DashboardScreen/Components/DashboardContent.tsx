﻿namespace Dashboard {
    export function DashboardContent(props: IDashboardDataState) {
        let menuWraper: HTMLDivElement | null;
        let menuToggleIcon: HTMLElement | null;

        function handleMenuToggle(event: React.MouseEvent<HTMLAnchorElement>) {
            event.preventDefault();
            if (menuWraper !== null) {
                $(menuWraper).toggleClass("active");
            }
            if (menuToggleIcon !== null) {
                $(menuToggleIcon).toggleClass("fa-angle-right").toggleClass("fa-angle-left");
            }
        }

        function getWidgetsToRenderOrNull() {
            const widgetsToRender = Dante.Utils.SelectMany(props.dashboardGroups.filter(g => g.urlParameter === props.selectedurlParameter), g => g.widgets);

            if (widgetsToRender.length === 0) { return (<p>{Dashboard.Translations.NoWidgets}</p>); }

            return (
                widgetsToRender.map((w, i) => {
                    switch (w.type) {
                        case WidgetType.simple:
                            return <SimpleWidgetWrapper key={i} {...(w as ISimpleWidget) } />;
                        case WidgetType.chart:
                            return <ChartWidgetWrapper key={i} {...(w as IChartWidget) } />;
                        case WidgetType.list:
                            return <ListWidgetWrapper key={i} {...(w as IListWidget) } />;
                        case WidgetType.table:
                            return <TableWidgetWrapper key={i} {...(w as ITableWidget) } />;
                        default:
                            return null;
                    }
                })
            );
        }

        return (
            <div id="wrapper" ref={(wraper) => menuWraper = wraper}>
                <SidebarMenu {...props} />

                <div id="page-content-wrapper">
                    <h3 id="home">
                        <a id="menu-toggle" href="#" onClick={handleMenuToggle} className="btn-menu toggle">
                            <i className="fa fa-angle-left" ref={(toggleIcon) => menuToggleIcon = toggleIcon}></i>
                        </a>
                    </h3>

                    <div className="page-content" data-spy="scroll" data-target="#spy">
                        {getWidgetsToRenderOrNull()}
                    </div>
                </div>
            </div>
        );
    }

    function SidebarMenu(props: IDashboardDataState) {

        const menuItems = props.dashboardGroups.map(p => (
            { text: p.displayName, hash: p.urlParameter, isSelected: p.urlParameter === props.selectedurlParameter }
        ));

        const styledMenuItems = menuItems.map((item) =>
            <li className={Dante.Utils.ClassNames({ selected: item.isSelected })} key={item.hash}>
                <a href={`#${item.hash}`} data-scroll>
                    {item.text}
                </a>
            </li>
        );

        return (
            <div id="sidebar-wrapper">
                <nav id="spy">
                    <ul className="nav nav-pills nav-stacked">
                        {styledMenuItems}
                    </ul>
                </nav>
            </div>
        );
    }
}
