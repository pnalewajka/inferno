﻿namespace Dashboard {
    export function ChartWidgetWrapper(props: IChartWidget): JSX.Element {
        return (
            <div className="col-md-4">
                <ChartWidget {...(props as IChartWidget) } />
            </div>
        );
    }

    class ChartWidget extends DashboardBaseWidget<IChartWidget> {
        protected renderContent(): JSX.Element {
            return (
                <div className="dashboard-widget-chart">
                    {this.renderChart()}
                </div>
            );
        }

        private renderChart(): JSX.Element | null {
            switch (this.props.chartType) {
                case ChartType.doughnut:
                case ChartType.pie:
                case ChartType.line:
                case ChartType.bar:
                    return <GetChart {...(this.props as IChartWidget) } />;
                default:
                    return null;
            }
        }
    }

    function registerChart(element: HTMLCanvasElement, data: IChartWidget): Chart | undefined {
        const ctx = element.getContext("2d");

        if (ctx !== null) {
            return createChart(ctx, data);
        }

        return undefined;
    }

    function createChart(ctx: CanvasRenderingContext2D, data: IChartWidget): Chart {
        const datasets = data.datasets.map(v => {
            const response = {
                data: v.data,
                label: v.label,
                hoverBackgroundColor: v.hoverBackgroundColor,
                backgroundColor: v.backgroundColor,
                stack: v.stackName,
                fill: v.isFilled
            };

            return response;
        });

        const options: Chart.ChartOptions = {
            legend: {
                position: "bottom"
            },
            onClick() {
                if (data.url) {
                    window.location.href = data.url;
                }
            },
            pieceLabel: {
                render: data.pieceLabel,
                fontColor: "#ffff",
            }
        };

        if (data.chartType === ChartType.bar) {
            options.scales = {
                xAxes: [{
                    stacked: data.isStacked
                }],
                yAxes: [{
                    stacked: data.isStacked
                }]
            };
        }

        return new Chart(ctx, {
            type: ChartType[data.chartType],
            data: {
                labels: data.labels,
                datasets
            },
            options
        });
    }

    function GetChart(el: IChartWidget): JSX.Element {
        let chartObject: Chart | undefined;

        return (
            <canvas id="myChart" ref={(chart) => {
                if (chart !== null) {
                    chartObject = registerChart(chart, el);
                } else if (chartObject !== undefined) {
                    chartObject.destroy();
                }
            }} ></canvas>
        );
    }

    function getRandomColor(): string {
        const color = [];
        for (let i = 0; i < 1; i++) {
            color.push(Math.floor(Math.random() * 100) + 100);
        }

        return "rgba(100, " + color.map(c => c) + ", 100, 0.4)";
    }

    function getRandomColorHex(): string {
        const hex = "0123456789ABCDEF";
        let color = "#";

        for (let i = 1; i <= 6; i++) {
            color += hex[Math.floor(Math.random() * hex.length)];
        }

        return color;
    }
}
