﻿namespace Dashboard {
    export function ListWidgetWrapper(props: IListWidget): JSX.Element {
        return (
            <div className="col-sm-12 col-md-6 col-lg-4">
                <ListWidget {...(props as IListWidget) } />
            </div>
        );
    }

    abstract class ListWidget extends DashboardBaseWidget<IListWidget> {
        private renderList(item: IListWidgetRow): JSX.Element {
            return (
                <ul className="abc">
                    {item.caption}
                </ul>
            );
        }

        private renderActionButtons(action: IListWidgetRowAction): JSX.Element | null {
            return (
                <a href={action.url}>
                    {action.caption}
                </a>
            );
        }

        protected renderContent(): JSX.Element {
            const list = this.props.rows && this.props.rows.length ? this.props.rows.map((r, i) => {
                return (
                    <li className="widget-list-group-item" key={i}>
                        {r.caption + " "}
                        <div className="btn-group" role="group" aria-label="...">
                            {
                                r.actions && r.actions.length > 0 ? r.actions.map((a, j) => {
                                    const btnStyle = a.buttonClass ? "btn  btn- sm btn-" + ButtonClass[a.buttonClass] : "btn  btn- sm btn-default";

                                    return (
                                        <a href={a.url} className={btnStyle} key={j}> {a.caption} </a>
                                    );
                                }) : ""
                            }
                        </div>
                    </li>
                );
            }) : <p>{this.props.noRowsMessage ? this.props.noRowsMessage : "Pusta lista " + this.props.title} </p>;

            return (
                <div className="dashboard-widget-content row">
                    <div className="col-sm-12">
                        <ul className="widget-list-group">
                            {list}
                        </ul>
                    </div>
                </div>
            );
        }
    }
}
