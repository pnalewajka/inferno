﻿namespace Dashboard {
    export function SimpleWidgetWrapper(props: ISimpleWidget): JSX.Element {
        return (
            <div className={"col-sm-" + (props.values.length * 3).toString()}>
                <SimpleWidget {...(props as ISimpleWidget) } />
            </div>
        );
    }

    abstract class SimpleWidget extends DashboardBaseWidget<ISimpleWidget> {
        protected renderContent(): JSX.Element {
            const singleCellClassName = "col-sm-" + (this.props.values.length * 3).toString();

            const content = this.props.values.map((item, index) =>
                <div key={index} className={"col-md-" + (12 / this.props.values.length).toString()}>
                    <h2>{item.valueCaption}</h2>
                    <p>{item.caption}</p>
                </div>
            );

            return (
                <div className="dashboard-widget-content row">
                    {content}
                </div>
            );
        }
    }
}
