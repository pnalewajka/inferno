﻿namespace Dashboard {
    export abstract class DashboardBaseWidget<P extends IWidget = IWidget, S extends IWidgetState = IWidgetState> extends React.Component<P, S> {
        public render(): JSX.Element | null {
            if (this.isHidden()) {
                return null;
            }

            return (
                <Tooltip text={this.props.tooltipText}>
                    <div className="dashboard-widget">
                        {this.renderHeader()}
                        {this.renderContent()}
                        {this.renderFooter()}
                    </div>
                </Tooltip>
            );
        }

        protected isHidden(): boolean {
            return this.props.isHidden === true;
        }

        protected renderHeader(): JSX.Element {
            const icon = this.props.icon || "glyphicon-asterisk";
            const iconClass = "glyphicon " + icon;

            const color = this.props.headerBackgroud || "#fafafa";
            const headerStyle = {
                backgroundColor: color
            };

            return (
                <div className="dashboard-widget-header" style={headerStyle} >
                    <h5>
                        <span className={iconClass}></span>{" " + this.props.title}
                    </h5>
                </div>
            );
        }

        protected abstract renderContent(): JSX.Element;

        protected renderFooter(): JSX.Element | null {
            const footerHtmlMessage = this.props.footerHtmlMessage !== undefined
                ? this.MarkupFooter(this.props.footerHtmlMessage)
                : null;

            const footerButton = this.props.footerButtonUrl
                ? <a href={this.props.footerButtonUrl} className="btn btn-default">{this.props.footerButtonLabel}</a>
                : null;

            if (footerHtmlMessage === null && footerButton === null) {
                return null;
            }

            return (
                <div className="dashboard-widget-footer">
                    {footerButton}
                    {footerHtmlMessage}
                </div>
            );
        }

        protected createMarkup(html: HTMLElement): dynamic {
            return { __html: html };
        }

        protected MarkupFooter(html: HTMLElement): JSX.Element {
            return <div dangerouslySetInnerHTML={this.createMarkup(html)} />;
        }
    }

    export interface ITooltipProps extends React.HTMLProps<string> {
        text: string | null;
    }

    export class Tooltip extends React.Component<ITooltipProps, {}> {
        private $item: JQuery;

        private atachTooltip(item: HTMLElement | null, text: string | null): void {
            if (item != null && !Strings.isNullOrEmpty(text)) {
                this.$item = $(item).tooltip({ container: $("body"), title: () => text });
            } else if (this.$item != null) {
                this.$item.tooltip("destroy");
            }
        }

        public render() {
            return (
                <div
                    className={`ui-tooltip ${this.props.className}`}
                    key={Strings.isNullOrEmpty(this.props.text) ? "empty_" : `not_empty_${this.props.text}`}
                    ref={item => this.atachTooltip(item as HTMLElement, this.props.text)} >
                    {this.props.children}
                </div>
            );
        }
    }
}
