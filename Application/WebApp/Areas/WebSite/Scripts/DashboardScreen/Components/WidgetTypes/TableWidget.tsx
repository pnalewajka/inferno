﻿namespace Dashboard {
    export function TableWidgetWrapper(props: ITableWidget): JSX.Element {
        return (
            <div className="col-sm-12 col-md-8 col-lg-8">
                <TableWidget {...(props as ITableWidget) } />
            </div>
        );
    }

    abstract class TableWidget extends DashboardBaseWidget<ITableWidget> {
        private renderTableValue(action: ITableWidgetRowAction, index: number): JSX.Element {
            return (<td key={index}> {action.dataHubIdentifier
                ? <span onClick={() => Hub.open(action.dataHubRecordId, action.dataHubIdentifier)} className="hub-trigger">{action.value}</span>
                : action.value}</td>);
        }

        private renderTableHeadValue(caption: string, index: number): JSX.Element {
            return (
                <th key={index} scope="col" className="table-widget-header-cell">{caption}</th>    
            );
        }

        protected renderContent(): JSX.Element {
            const tableHead = (
                <thead>
                    <tr>
                        {this.props.columns.map((c, i) => this.renderTableHeadValue(c, i))}
                    </tr>
                </thead>
            );

            const list = this.props.rows && this.props.rows.length ? this.props.rows.map((r, i) => {
                return (
                    <tr key={i}>
                        {r.actions.map((a, j) => this.renderTableValue(a, j))}
                    </tr>
                );
            }) : <p>{this.props.noRowsMessage ? this.props.noRowsMessage : "Pusta lista " + this.props.title} </p>;

            return (
                <div className="dashboard-widget-content row">
                    <div className="col-sm-12 widget-list-group">
                        <table className="table dashboard-widget-table">
                            {tableHead}
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }
    }
}
