﻿namespace Dashboard {
    export interface IDashboardGroup {
        displayName: string;
        urlParameter: string;
        widgets: IWidget[];
    }

    export interface IWidget {
        type: WidgetType;
        icon?: string;
        headerBackgroud?: string;
        title: string;
        tooltipText: string;
        footerHtmlMessage: HTMLElement;
        footerButtonUrl: string;
        footerButtonLabel: string;
        url?: string;
        isHidden?: boolean;
    }

    export enum WidgetType {
        simple,
        chart,
        list,
        table
    }

    export enum ChartType {
        doughnut,
        pie,
        line,
        bar
    }

    export enum ButtonClass {
        default,
        primary,
        success,
        info,
        warning,
        danger,
        link
    }

    export interface IWidgetState {
    }

    export interface ISimpleWidget extends IWidget {
        values: ISimpleWidgetValues[];
    }

    interface ISimpleWidgetValues {
        caption: string;
        valueCaption: string;
        url?: string;
    }

    export interface ITableWidget extends IWidget {
        noRowsMessage?: string;
        columns: string[];
        rows?: ITableWidgetRow[];
    }

    export interface ITableWidgetRow {
        actions: ITableWidgetRowAction[];
    }

    export interface ITableWidgetRowAction {
        value: string;
        dataHubIdentifier: string;
        dataHubRecordId: number;
        buttonClass?: ButtonClass;
    }

    export interface IListWidget extends IWidget {
        rows?: IListWidgetRow[];
        noRowsMessage?: string;
    }

    export interface IListWidgetRow {
        caption: string;
        actions?: IListWidgetRowAction[];
    }

    export interface IListWidgetRowAction {
        caption: string;
        url: string;
        buttonClass?: ButtonClass;
        method: "GET" | "POST";
    }

    export interface IChartWidget extends IWidget {
        chartType: ChartType;
        labels: string[];
        pieceLabel?: "label" | "value" | "percentage" | "image";
        isStacked: boolean;
        datasets: IChartJsInputDatasets[];
    }

    interface IChartJsInputDatasets {
        label?: string;
        data: number[];
        backgroundColor: string | string[];
        hoverBackgroundColor: string | string[];
        stackName?: string;
        isFilled: boolean;
    }

    export interface IDashboardDataProps {
        defaultDashboardGroupIndex: number;
        dashboardGroups: IDashboardGroup[];
    }

    export interface IDashboardDataState {
        selectedurlParameter: string;
        dashboardGroups: IDashboardGroup[];
        urlParameterChanged: Atomic.Events.EventDispatcher<string>;
    }

    export interface ITranslations {
        Home: string;
        Dashboard: string;
        NoWidgets: string;
    }
}
