﻿namespace Dashboard {
    export const hashChangedEvent = new Atomic.Events.EventDispatcher<string>();
    window.onhashchange = () => {
        hashChangedEvent.dispatch(window.location.hash.substr(1));
    };

    export let Translations: Dashboard.ITranslations;
    export function Initialize(data: IDashboardDataProps, translations: ITranslations) {
        Translations = translations;
        ReactDOM.render(<Dashboard.Breadcrumb {...data} />, document.getElementsByClassName("breadcrumb")[0]);
        ReactDOM.render(<DashboardMasterComponent {...data} />, document.getElementById("dashboard"));
    }

    export class DashboardMasterComponent extends Dante.Components.StateComponent<IDashboardDataProps, IDashboardDataState> {
        public componentDidMount(): void {
            hashChangedEvent.subscribe(() => {
                this.reciveDashboardData();
            });

            this.reciveDashboardData();
        }

        private reciveDashboardData(): void {
            const selectedurlParameter = window.location.hash.substr(1) || this.props.dashboardGroups[0].urlParameter;
            this.updateState({
                selectedurlParameter, dashboardGroups: this.props.dashboardGroups
            });
        }

        public render(): JSX.Element {
            return (
                <Dashboard.DashboardContent {...this.state} />
            );
        }
    }
}
