﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models;

namespace Smt.Atomic.WebApp.Areas.WebSite.Controllers
{
    [Identifier("ValuePickers.PropertyPicker")]
    [AtomicAuthorize]
    public class DtoPropertyPickerController : ReadOnlyCardIndexController<ModelFieldNameViewModel, PropertyNameDto, DtoPropertyCardIndexContext>
    {
        public DtoPropertyPickerController(
            IDtoPropertyNameCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = true;
        }
    }
}