﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.WebSite.Controllers
{
    [AllowAnonymous]
    public class PerformanceController : AtomicController
    {
        public PerformanceController(IBaseControllerDependencies baseDependencies) 
            : base(baseDependencies)
        {
        }

        public ActionResult Index()
        {
            var controllers = TypeHelper.GetLoadedTypes()
               .Where(t => !t.IsAbstract)
               .Where(t => typeof(AtomicController).IsAssignableFrom(t));

            var performanceSubjects = GetMethodDecoratedActions(controllers);
            performanceSubjects.AddRange(GetControllerDecoratedActions(controllers));

            return View("~/Areas/WebSite/Views/Performance/Index.cshtml", performanceSubjects.DistinctBy(p => p.Url));
        }

        /// <summary>
        ///  Gets all methods decorated with performance attribute
        /// </summary>
        /// <param name="controllers">Search scope</param>
        /// <returns>List of performance subjects</returns>
        private List<PerformanceTestViewModel> GetControllerDecoratedActions(IEnumerable<Type> controllers)
        {
            var controllerLevelSubjects = new List<PerformanceTestViewModel>(); 
            var validControllers = controllers.Where(p => p.GetCustomAttributes(typeof(PerformanceTestAttribute), false).Length > 0);

            foreach (var controller in validControllers)
            {
                var canonicalActions = new ReflectedControllerDescriptor(controller).GetCanonicalActions();

                foreach (var attribute in controller.GetCustomAttributes(typeof(PerformanceTestAttribute), false).Cast<PerformanceTestAttribute>())
                {
                    // if no action name provided - use all canonical actions 
                    if (string.IsNullOrEmpty(attribute.Action))
                    {
                        controllerLevelSubjects.AddRange(canonicalActions.Select(action => CreatePerformanceSubject(controller, attribute, action.ActionName)));
                        continue;
                    }

                    // check if provided action name is correct
                    if (!canonicalActions.Any(ca => ca.ActionName == attribute.Action))
                    {
                        throw new Exception($"{controller.Name} PerformanceTestAttribute contains wrongly spefified action name [{attribute.Action}].");
                    }

                    controllerLevelSubjects.Add(CreatePerformanceSubject(controller, attribute, null));
                }
            }

            return controllerLevelSubjects;
        }

        /// <summary>
        ///  Gets all methods decorated with performance attribute
        /// </summary>
        /// <param name="controllers">Search scope</param>
        /// <returns>List of performance subjects</returns>
        private List<PerformanceTestViewModel> GetMethodDecoratedActions(IEnumerable<Type> controllers)
        {
            var actions = controllers
                .SelectMany(p => p.GetMethods().Where(methodInfo => methodInfo.GetCustomAttributes(typeof(PerformanceTestAttribute), false).Length > 0));

            var performanceSubjects = actions.SelectMany(p => p.GetCustomAttributes(typeof(PerformanceTestAttribute), false).Cast<PerformanceTestAttribute>()
                .Select(attribute => CreatePerformanceSubject(p.DeclaringType, attribute, p.Name)))
                .ToList();

            return performanceSubjects;
        }

        /// <summary>
        /// Validates if area name is provided for routing purpose 
        /// </summary>
        /// <param name="parameters">Current set of parameters</param>
        /// <param name="controllerType">Controller type to calculate area name from if needed</param>
        /// <returns></returns>
        private RouteValueDictionary ValidateAndGetActionParameters(string parameters, Type controllerType)
        {
            Dictionary<string, object> paramsDictionary = null;

            if (string.IsNullOrEmpty(parameters))
            {
                paramsDictionary = new Dictionary<string, object>(1);
            }
            else
            {
                var nameValueCollection = HttpUtility.ParseQueryString(parameters);
                paramsDictionary = Enumerable.ToDictionary<string, string, object>(
                    nameValueCollection.AllKeys, 
                    k => k, 
                    k => HttpUtility.ParseQueryString(parameters)[k]);
            }

            if (!paramsDictionary.Keys.Select(c => c.ToLowerInvariant()).Contains("area"))
            {
                paramsDictionary.Add("area", RoutingHelper.GetAreaName(controllerType));
            }

            return new RouteValueDictionary(paramsDictionary);
        }

        /// <summary>
        /// Creates new instance of PerformanceSubject based on provided controller and performance attribute 
        /// </summary>
        /// <param name="controllerType"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private PerformanceTestViewModel CreatePerformanceSubject(Type controllerType, PerformanceTestAttribute attribute, string action)
        {
            var subject = new PerformanceTestViewModel
            {
                Group = RoutingHelper.GetAreaName(controllerType),
                Timeout = attribute.Timeout,
                Url = Url.Action(action ?? attribute.Action, RoutingHelper.GetControllerName(controllerType), ValidateAndGetActionParameters(attribute.Parameters, controllerType), Request.Url.Scheme),
                Name = $"{RoutingHelper.GetControllerName(controllerType)}/{action ?? attribute.Action}",
            };

            return subject;
        }
    }
}
