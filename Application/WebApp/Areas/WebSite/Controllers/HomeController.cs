﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Controllers
{
    [AllowAnonymous]
    public class HomeController : AtomicController
    {
        private readonly IPrincipalProvider _principalProvider;

        public HomeController(IBaseControllerDependencies baseDependencies) 
            : base(baseDependencies)
        {
            _principalProvider = baseDependencies.PrincipalProvider;
        }

        public ActionResult Index()
        {
            return RedirectToLandingPage();
        }

        public ActionResult About()
        {
            return View("~/Areas/WebSite/Views/Home/About.cshtml");
        }

        public ActionResult Contact()
        {
            return View("~/Areas/WebSite/Views/Home/Contact.cshtml");
        }
    }
}
