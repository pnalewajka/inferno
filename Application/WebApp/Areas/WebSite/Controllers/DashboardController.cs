﻿using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.WebSite.Models;
using Smt.Atomic.WebApp.Areas.WebSite.Resources;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.WebSite.Controllers
{
    [AtomicAuthorize]
    public class DashboardController : AtomicController
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IDashboardService _dashboardWidgetService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IClassMapping<DashboardConfigurationDto, DashboardConfigurationViewModel> _dtoToViewModelMapping;

        public DashboardController(IBaseControllerDependencies baseDependencies,
            IDashboardService dashboardWidgetService,
            IClassMappingFactory classMappingFactory)
            : base(baseDependencies)
        {
            _principalProvider = baseDependencies.PrincipalProvider;
            _dashboardWidgetService = dashboardWidgetService;
            _classMappingFactory = classMappingFactory;
            _dtoToViewModelMapping = classMappingFactory
                .CreateMapping<DashboardConfigurationDto, DashboardConfigurationViewModel>();
        }

        [AtomicAuthorize(SecurityRoleType.CanViewDashboards)]
        public ActionResult Index()
        {
            Layout.Scripts.Add("~/scripts/react");
            Layout.Scripts.Add("~/Scripts/Chart.bundle.js");
            Layout.Scripts.Add("~/scripts/dashboard");
            Layout.Css.Add("~/Areas/WebSite/Content/DashboardScreen.css");
            Layout.Resources.AddFrom<DashboardResources>();

            var currentUser = _principalProvider.Current;
            var dto = _dashboardWidgetService.GetDashboardsForEmployee(currentUser.EmployeeId);
            var model = _dtoToViewModelMapping.CreateFromSource(dto);

            return View(model);
        }
    }
}
