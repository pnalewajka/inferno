﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.WebSite.Helpers
{
    public static class WidgetHelper
    {
        public static string GetWidgetButtonLabel(WidgetButtonDto button)
        {
            return button?.Label;
        }

        public static string GetWidgetButtonUrl(WidgetButtonDto button)
        {
            if (button == null)
            {
                return string.Empty;
            }

            const string allocationArea = "Allocation";
            const string allocationRequestController = "AllocationRequest";
            const string projectController = "Project";
            const string listAction = "List";
            const string viewAction = "View";
            const string editAction = "Edit";
            const string urlTemplate = "/{0}/{1}/{2}/{3}{4}";

            switch (button.UrlType)
            {
                case ActionUrlType.ListAllocations:

                    return string.Format(urlTemplate, allocationArea, allocationRequestController, listAction, string.Empty,
                        GetHtmlParametersString(button.Parameters));

                case ActionUrlType.ListProjects:

                    return string.Format(urlTemplate, allocationArea, projectController, listAction, string.Empty,
                        GetHtmlParametersString(button.Parameters));

                case ActionUrlType.ViewProject:

                    return string.Format(urlTemplate, allocationArea, projectController, viewAction, button.EntityId,
                        GetHtmlParametersString(button.Parameters));

                case ActionUrlType.EditProject:

                    return string.Format(urlTemplate, allocationArea, projectController, editAction, button.EntityId,
                        GetHtmlParametersString(button.Parameters));

                default: throw new ArgumentException("Unrecognized ActionUrlType");
            }
        }

        private static string GetHtmlParametersString(IDictionary<string, string> parameters)
        {
            return parameters != null
                ? "?" + string.Join("&", parameters.Select(p => p.Key + "=" + p.Value))
                : null;
        }
    }
}