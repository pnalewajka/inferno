﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ChartWidgetDtoToChartWidgetViewModelMapping : ClassMapping<ChartWidgetDto, ChartWidgetViewModel>
    {
        public ChartWidgetDtoToChartWidgetViewModelMapping()
        {
            Mapping = dto => new ChartWidgetViewModel
            {
                ChartType = dto.ChartType,
                Labels = dto.Labels,
                Datasets = dto.Datasets != null
                        ? dto.Datasets
                            .Select(ChartWidgetValueDtoToChartWidgetValueViewModel).ToList()
                        : new List<ChartWidgetValueViewModel>(),
                Type = dto.Type,
                Icon = dto.Icon,
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                IsHidden = dto.IsHidden,
                Url = dto.Url
            };
        }

        private ChartWidgetValueViewModel ChartWidgetValueDtoToChartWidgetValueViewModel(IChartWidgetValueDto dto)
        {
            return dto != null
                ? new ChartWidgetValueViewModel
                {
                    Label = dto.Label,
                    Data = dto.Data,
                    BackgroundColor = dto.BackgroundColor,
                    HoverBackgroundColor = dto.HoverBackgroundColor
                }
                : default(ChartWidgetValueViewModel);
        }
    }
}