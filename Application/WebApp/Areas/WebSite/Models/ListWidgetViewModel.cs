﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ListWidgetViewModel : WidgetViewModel
    {
        public IReadOnlyCollection<ListWidgetRowViewModel> Rows { get; set; }

        public string EmptyRowsMessage { get; set; }
    }
}