﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class TableWidgetViewModel : WidgetViewModel
    {
        public IReadOnlyCollection<TableWidgetRowViewModel> Rows { get; set; }

        public IReadOnlyCollection<string> Columns { get; set; }
    }
}