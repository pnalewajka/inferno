﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class LineWidgetDtoToLineWidgetViewModelMapping : ClassMapping<LineWidgetDto, LineWidgetViewModel>
    {
        public LineWidgetDtoToLineWidgetViewModelMapping()
        {
            Mapping = dto => new LineWidgetViewModel
            {
                ChartType = dto.ChartType,
                Labels = dto.Labels,
                Datasets = dto.Datasets != null
                        ? dto.Datasets
                            .Select(d => LineWidgetValueDtoToLineWidgetValueViewModel((LineWidgetValueDto)d)).ToList()
                        : new List<LineWidgetValueViewModel>(),
                Type = dto.Type,
                Icon = dto.Icon,
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                IsHidden = dto.IsHidden,
                Url = dto.Url
            };
        }

        private LineWidgetValueViewModel LineWidgetValueDtoToLineWidgetValueViewModel(LineWidgetValueDto dto)
        {
            return dto != null
                 ? new LineWidgetValueViewModel
                 {
                     Label = dto.Label,
                     Data = dto.Data,
                     BackgroundColor = dto.BackgroundColor,
                     HoverBackgroundColor = dto.HoverBackgroundColor,
                     IsFilled = dto.IsFilled
                 }
                 : default(LineWidgetValueViewModel);
        }
    }
}