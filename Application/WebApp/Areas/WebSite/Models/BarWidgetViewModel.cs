﻿namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class BarWidgetViewModel : ChartWidgetViewModel
    {
        public bool IsStacked { get; set; }
    }
}
