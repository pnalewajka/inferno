﻿namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class PieWidgetViewModel : ChartWidgetViewModel
    {
        public string PieceLabel { get; set; }
    }
}
