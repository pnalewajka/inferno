﻿namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class BarWidgetValueViewModel : ChartWidgetValueViewModel
    {
        public string StackName { get; set; }
    }
}
