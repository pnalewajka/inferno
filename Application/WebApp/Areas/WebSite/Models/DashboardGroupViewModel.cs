﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class DashboardGroupViewModel
    {
        public string DisplayName { get; set; }

        public string UrlParameter { get; set; }

        public IReadOnlyCollection<WidgetViewModel> Widgets { get; set; }
    }
}