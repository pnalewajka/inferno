﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class DashboardConfigurationViewModel
    {
        public long DefaultDashboardGroupIndex { get; set; }

        public IReadOnlyCollection<DashboardGroupViewModel> DashboardGroups { get; set; }
    }
}