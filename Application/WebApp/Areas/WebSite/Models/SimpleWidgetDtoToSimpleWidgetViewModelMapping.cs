﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class SimpleWidgetDtoToSimpleWidgetViewModelMapping : ClassMapping<SimpleWidgetDto, SimpleWidgetViewModel>
    {
        public SimpleWidgetDtoToSimpleWidgetViewModelMapping()
        {
            Mapping = dto => new SimpleWidgetViewModel
            {
                Values = dto.Values != null
                        ? dto.Values.Select(
                            v => new SimpleWidgetValueViewModel
                            {
                                Caption = v.Caption,
                                Url = v.Url,
                                ValueCaption = v.ValueCaption
                            }).ToList()
                        : new List<SimpleWidgetValueViewModel>(),
                Type = dto.Type,
                Icon = dto.Icon,
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                IsHidden = dto.IsHidden,
                Url = dto.Url
            };
        }
    }
}