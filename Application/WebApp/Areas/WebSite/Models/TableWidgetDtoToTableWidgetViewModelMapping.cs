﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class TableWidgetDtoToTableWidgetViewModelMapping : ClassMapping<TableWidgetDto, TableWidgetViewModel>
    {
        public TableWidgetDtoToTableWidgetViewModelMapping()
        {
            Mapping = dto => new TableWidgetViewModel
            {
                Columns = dto.Columns,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                Icon = dto.Icon,
                IsHidden = dto.IsHidden,
                Rows = dto.Rows != null
                        ? dto.Rows.Select(r => TableWidgetRowDtoToTableWidgetRowViewModel(r)).ToList()
                        : new List<TableWidgetRowViewModel>(),
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                Type = dto.Type,
                Url = dto.Url,
            };
        }

        private TableWidgetRowViewModel TableWidgetRowDtoToTableWidgetRowViewModel(TableWidgetRowDto dto)
        {
            return dto != null
                ? new TableWidgetRowViewModel
                {
                    Actions = dto.Actions != null
                        ? dto.Actions.Select(a => TableWidgetRowActionDtoToTableWidgetRowActionViewModel(a)).ToList()
                        : new List<TableWidgetRowActionViewModel>(),
                }
                : default(TableWidgetRowViewModel);
        }

        private TableWidgetRowActionViewModel TableWidgetRowActionDtoToTableWidgetRowActionViewModel(TableWidgetRowActionDto dto)
        {
            return dto != null
                ? new TableWidgetRowActionViewModel
                {
                    ButtonClass = dto.ButtonClass,
                    DataHubIdentifier = dto.DataHubIdentifier,
                    DataHubRecordId = dto.DataHubRecordId,
                    Value = dto.Value,
                }
                : default(TableWidgetRowActionViewModel);
        }
    }
}