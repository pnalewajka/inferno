﻿namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class LineWidgetValueViewModel : ChartWidgetValueViewModel
    {
        public bool IsFilled { get; set; }
    }
}
