﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class PieWidgetDtoToPieWidgetViewModelMapping : ClassMapping<PieWidgetDto, PieWidgetViewModel>
    {
        public PieWidgetDtoToPieWidgetViewModelMapping()
        {
            Mapping = dto => new PieWidgetViewModel
            {
                ChartType = dto.ChartType,
                Labels = dto.Labels,
                PieceLabel = dto.PieceLabel.ToString("f").ToLower(),
                Datasets = dto.Datasets != null
                        ? dto.Datasets
                            .Select(d => PieWidgetValueDtoToPieWidgetValueViewModel((PieWidgetValueDto)d)).ToList()
                        : new List<PieWidgetValueViewModel>(),
                Type = dto.Type,
                Icon = dto.Icon,
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                IsHidden = dto.IsHidden,
                Url = dto.Url
            };
        }

        private PieWidgetValueViewModel PieWidgetValueDtoToPieWidgetValueViewModel(PieWidgetValueDto dto)
        {
            return dto != null
                 ? new PieWidgetValueViewModel
                 {
                     Label = dto.Label,
                     Data = dto.Data,
                     BackgroundColor = dto.BackgroundColor,
                     HoverBackgroundColor = dto.HoverBackgroundColor
                 }
                 : default(PieWidgetValueViewModel);
        }
    }
}