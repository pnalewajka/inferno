﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class BarWidgetDtoToBarWidgetViewModelMapping : ClassMapping<BarWidgetDto, BarWidgetViewModel>
    {
        public BarWidgetDtoToBarWidgetViewModelMapping()
        {
            Mapping = dto => new BarWidgetViewModel
            {
                ChartType = dto.ChartType,
                Labels = dto.Labels,
                Datasets = dto.Datasets != null
                        ? dto.Datasets
                            .Select(d => BarWidgetValueDtoToBarWidgetValueViewModel((BarWidgetValueDto)d)).ToList()
                        : new List<BarWidgetValueViewModel>(),
                Type = dto.Type,
                Icon = dto.Icon,
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                IsHidden = dto.IsHidden,
                Url = dto.Url,
                IsStacked = dto.IsStacked
            };
        }

        private BarWidgetValueViewModel BarWidgetValueDtoToBarWidgetValueViewModel(BarWidgetValueDto dto)
        {
            return dto != null
                ? new BarWidgetValueViewModel
                {
                    Label = dto.Label,
                    Data = dto.Data,
                    BackgroundColor = dto.BackgroundColor,
                    HoverBackgroundColor = dto.HoverBackgroundColor,
                    StackName = dto.StackName
                }
                : default(BarWidgetValueViewModel);
        }
    }
}