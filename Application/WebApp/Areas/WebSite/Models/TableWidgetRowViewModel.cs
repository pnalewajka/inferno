﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class TableWidgetRowViewModel
    {
        public IReadOnlyCollection<TableWidgetRowActionViewModel> Actions { get; set; }
    }
}