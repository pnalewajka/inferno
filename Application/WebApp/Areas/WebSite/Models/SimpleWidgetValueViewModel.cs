﻿namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class SimpleWidgetValueViewModel
    {
        public string Caption { get; set; }

        public string ValueCaption { get; set; }

        public string Url { get; set; }
    }
}