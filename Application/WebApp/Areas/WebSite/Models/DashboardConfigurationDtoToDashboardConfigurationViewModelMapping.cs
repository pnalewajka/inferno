﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class DashboardConfigurationDtoToDashboardConfigurationViewModelMapping : ClassMapping<DashboardConfigurationDto, DashboardConfigurationViewModel>
    {
        private readonly IClassMappingFactory _classMappingFactory;

        public DashboardConfigurationDtoToDashboardConfigurationViewModelMapping(IClassMappingFactory classMappingFactory)
        {
            _classMappingFactory = classMappingFactory;

            Mapping = d => new DashboardConfigurationViewModel
            {
                DefaultDashboardGroupIndex = d.DefaultDashboardGroupIndex,
                DashboardGroups = d.DashboardGroups != null
                   ? d.DashboardGroups.Select(
                        p => new DashboardGroupViewModel
                        {
                            DisplayName = p.DisplayName,
                            UrlParameter = p.UrlParameter,
                            Widgets = GetWidgetViewModels(p.Widgets)
                        }).ToList()
                  : new List<DashboardGroupViewModel>()

            };
        }

        private IReadOnlyCollection<WidgetViewModel> GetWidgetViewModels(IReadOnlyCollection<IWidgetDto> widgets)
        {
            var response = new List<WidgetViewModel>();

            if (widgets != null)
            {
                foreach (var widget in widgets)
                {
                    response.Add(GetWidgetViewModel(widget));
                }
            }

            return response;
        }

        private WidgetViewModel GetWidgetViewModel(IWidgetDto widget)
        {
            var response = default(WidgetViewModel);

            if (widget != null)
            {
                switch (widget.Type)
                {
                    case WidgetType.Simple:
                        response = (WidgetViewModel)_classMappingFactory.CreateMapping<SimpleWidgetDto, SimpleWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                    case WidgetType.Chart:
                        response = GetChartWidgetViewModel((IChartWidgetDto)widget);
                        break;
                    case WidgetType.List:
                        response = (WidgetViewModel)_classMappingFactory.CreateMapping<ListWidgetDto, ListWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                    case WidgetType.Table:
                        response =
                            (WidgetViewModel)_classMappingFactory.CreateMapping<TableWidgetDto, TableWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                }
            }

            return response;
        }

        private ChartWidgetViewModel GetChartWidgetViewModel(IChartWidgetDto widget)
        {
            var response = default(ChartWidgetViewModel);

            if (widget != null)
            {
                switch (widget.ChartType)
                {
                    case ChartType.Pie:
                        response = (ChartWidgetViewModel)_classMappingFactory.CreateMapping<ChartWidgetDto, ChartWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                    case ChartType.Line:
                        response = (ChartWidgetViewModel)_classMappingFactory.CreateMapping<LineWidgetDto, LineWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                    case ChartType.Bar:
                        response = (BarWidgetViewModel)_classMappingFactory.CreateMapping<BarWidgetDto, BarWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                    default:
                        response = (ChartWidgetViewModel)_classMappingFactory.CreateMapping<ChartWidgetDto, ChartWidgetViewModel>()
                            .CreateFromSource(widget);
                        break;
                }
            }

            return response;
        }
    }
}
