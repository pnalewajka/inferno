﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class SimpleWidgetViewModel : WidgetViewModel
    {
        public IReadOnlyCollection<SimpleWidgetValueViewModel> Values { get; set; }
    }
}