﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ListWidgetRowActionViewModel
    {
        public string Caption { get; set; }

        public string Url { get; set; }

        public WidgetButtonClass? ButtonClass { get; set; }

        public string Method { get; set; }
    }
}