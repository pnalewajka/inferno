﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ChartWidgetValueViewModel
    {
        public string Label { get; set; }

        public IReadOnlyCollection<string> BackgroundColor { get; set; }

        public IReadOnlyCollection<string> HoverBackgroundColor { get; set; }

        public IReadOnlyCollection<decimal> Data { get; set; }
    }
}