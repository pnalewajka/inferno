﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ListWidgetRowViewModel
    {
        public string Caption { get; set; }

        public IReadOnlyCollection<ListWidgetRowActionViewModel> Actions { get; set; }
    }
}