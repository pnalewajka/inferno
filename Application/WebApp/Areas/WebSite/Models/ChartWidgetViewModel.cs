﻿using Smt.Atomic.Business.WebSite.Enums;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ChartWidgetViewModel : WidgetViewModel
    {
        public ChartType ChartType { get; set; }

        public IReadOnlyCollection<string> Labels { get; set; }

        public IReadOnlyCollection<ChartWidgetValueViewModel> Datasets { get; set; }
    }
}