﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class WidgetViewModel
    {
        public WidgetType Type { get; set; }

        public string Icon { get; set; }

        public string Title { get; set; }

        public string TooltipText { get; set; }

        public string FooterHtmlMessage { get; set; }

        public string FooterButtonUrl { get; set; }

        public string FooterButtonLabel { get; set; }

        public string HeaderBackgroud { get; set; }

        public bool IsHidden { get; set; }

        public string Url { get; set; }
    }
}