﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class TableWidgetRowActionViewModel
    {
        public string Value { get; set; }

        public string DataHubIdentifier { get; set; }

        public long DataHubRecordId { get; set; }

        public WidgetButtonClass? ButtonClass { get; set; }
    }
}