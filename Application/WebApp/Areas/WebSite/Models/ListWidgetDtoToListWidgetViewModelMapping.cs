﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.WebSite.Helpers;

namespace Smt.Atomic.WebApp.Areas.WebSite.Models
{
    public class ListWidgetDtoToListWidgetViewModelMapping : ClassMapping<ListWidgetDto, ListWidgetViewModel>
    {
        public ListWidgetDtoToListWidgetViewModelMapping()
        {
            Mapping = dto => new ListWidgetViewModel
            {
                Rows = dto.Rows != null
                        ? dto.Rows
                            .Select(ListWidgetRowDtoToListWidgetRowViewModel).ToList()
                        : new List<ListWidgetRowViewModel>(),
                EmptyRowsMessage = dto.NoRowsMessage,
                Type = dto.Type,
                Icon = dto.Icon,
                Title = dto.Title,
                TooltipText = dto.TooltipText,
                FooterHtmlMessage = dto.FooterHtmlMessage,
                FooterButtonUrl = WidgetHelper.GetWidgetButtonUrl(dto.WidgetFooterButton),
                FooterButtonLabel = WidgetHelper.GetWidgetButtonLabel(dto.WidgetFooterButton),
                HeaderBackgroud = dto.HeaderBackgroud,
                IsHidden = dto.IsHidden,
                Url = dto.Url
            };
        }


        private ListWidgetRowViewModel ListWidgetRowDtoToListWidgetRowViewModel(ListWidgetRowDto dto)
        {
            return dto != null
                ? new ListWidgetRowViewModel
                {
                    Caption = dto.Caption,
                    Actions = dto.Actions != null
                        ? dto.Actions.Select(
                            a => new ListWidgetRowActionViewModel
                            {
                                ButtonClass = a.ButtonClass,
                                Caption = a.Caption,
                                Method = a.Method,
                                Url = a.Url
                            }).ToList()
                        : new List<ListWidgetRowActionViewModel>()
                }
                : default(ListWidgetRowViewModel);
        }
    }
}