﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Sales.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class InquiryResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal InquiryResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Sales.Resources.InquiryResource", typeof(InquiryResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Line.
        /// </summary>
        public static string BusinessLineLabel {
            get {
                return ResourceManager.GetString("BusinessLineLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The error occurred while trying to change inquiry status. Please try again or reload page..
        /// </summary>
        public static string CantChangeInquiryStatus {
            get {
                return ResourceManager.GetString("CantChangeInquiryStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lost.
        /// </summary>
        public static string CloseLost {
            get {
                return ResourceManager.GetString("CloseLost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Won.
        /// </summary>
        public static string CloseWon {
            get {
                return ResourceManager.GetString("CloseWon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer.
        /// </summary>
        public static string CustomerLabel {
            get {
                return ResourceManager.GetString("CustomerLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string DeleteLabel {
            get {
                return ResourceManager.GetString("DeleteLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string DescriptionLabel {
            get {
                return ResourceManager.GetString("DescriptionLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit .
        /// </summary>
        public static string EditLabel {
            get {
                return ResourceManager.GetString("EditLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exp. Marg..
        /// </summary>
        public static string ExpectedMarginLabel {
            get {
                return ResourceManager.GetString("ExpectedMarginLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency.
        /// </summary>
        public static string ExpectedRevenueCurrencyLabel {
            get {
                return ResourceManager.GetString("ExpectedRevenueCurrencyLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exp. Rev..
        /// </summary>
        public static string ExpectedRevenueLabel {
            get {
                return ResourceManager.GetString("ExpectedRevenueLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filters....
        /// </summary>
        public static string Filters {
            get {
                return ResourceManager.GetString("Filters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string InquiryDisplayNameLabel {
            get {
                return ResourceManager.GetString("InquiryDisplayNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Max Pre-Sales Budg..
        /// </summary>
        public static string MaxPreSalesSpendingLabel {
            get {
                return ResourceManager.GetString("MaxPreSalesSpendingLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Due Date.
        /// </summary>
        public static string OfferDueDateLabel {
            get {
                return ResourceManager.GetString("OfferDueDateLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        public static string OfferTypeLabel {
            get {
                return ResourceManager.GetString("OfferTypeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        public static string OtherTab {
            get {
                return ResourceManager.GetString("OtherTab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overview.
        /// </summary>
        public static string OverviewTab {
            get {
                return ResourceManager.GetString("OverviewTab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Process Status.
        /// </summary>
        public static string ProcessStatusLabel {
            get {
                return ResourceManager.GetString("ProcessStatusLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Importance.
        /// </summary>
        public static string ProjectImportanceLabel {
            get {
                return ResourceManager.GetString("ProjectImportanceLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Propability.
        /// </summary>
        public static string ProjectPropabilityLabel {
            get {
                return ResourceManager.GetString("ProjectPropabilityLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales Filter....
        /// </summary>
        public static string SalesFilter {
            get {
                return ResourceManager.GetString("SalesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales Filter.
        /// </summary>
        public static string SalesFilterTitle {
            get {
                return ResourceManager.GetString("SalesFilterTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales.
        /// </summary>
        public static string SalesPersonLabel {
            get {
                return ResourceManager.GetString("SalesPersonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales.
        /// </summary>
        public static string SalesTab {
            get {
                return ResourceManager.GetString("SalesTab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving.
        /// </summary>
        public static string SavingMessage {
            get {
                return ResourceManager.GetString("SavingMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inquiries.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Negotiation.
        /// </summary>
        public static string ToNegotiation {
            get {
                return ResourceManager.GetString("ToNegotiation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Oportunity.
        /// </summary>
        public static string ToOportunity {
            get {
                return ResourceManager.GetString("ToOportunity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Proposal.
        /// </summary>
        public static string ToProposal {
            get {
                return ResourceManager.GetString("ToProposal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Shortlisted.
        /// </summary>
        public static string ToShortlisted {
            get {
                return ResourceManager.GetString("ToShortlisted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View.
        /// </summary>
        public static string ViewLabel {
            get {
                return ResourceManager.GetString("ViewLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Watchers.
        /// </summary>
        public static string WatchersLabel {
            get {
                return ResourceManager.GetString("WatchersLabel", resourceCulture);
            }
        }
    }
}
