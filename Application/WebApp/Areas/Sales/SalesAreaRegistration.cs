﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Sales
{
    public class SalesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Sales";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Sales",
                "Sales/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            BundleConfig.RegisterSalesBundles(BundleTable.Bundles);
        }
    }
}