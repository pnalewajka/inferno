﻿declare var GridData: Dante.Sales.Inquiry.IInquiry[];

GlobalErrorHandling.init = () => { /* no global handling */ };

namespace Dante.Sales.Inquiry {
    export function Initialize(): void {
        const groups = [
            new Groups.LeadProcessGroup(),
            new Groups.OportunityProcessGroup(),
            new Groups.ProposalProcessGroup(),
            new Groups.ShortlistedProcessGroup(),
            new Groups.NegotiationsProcessGroup(),
            new Groups.ClosedWonProcessGroup(),
            new Groups.ClosedLostProcessGroup(),
        ];

        const renderElement = document.getElementById("java-script-render");

        if (renderElement !== null) {
            ReactDOM.render(<div><InquiryProcessBoard Items={GridData} Groups={groups} /></div>, renderElement);
        }
    }

    export function CustomerChanged(valuePicker: HTMLElement): void {
        const $valuePicker = $(valuePicker);
        const $form = $valuePicker.parents("form:first");
        const $displayNameInput = $("#display-name", $form);

        if ($displayNameInput.val().length > 0) {
            return;
        }

        const customerName = $("#customer-id", $valuePicker).text();

        $displayNameInput.val(`Inquiry for customer ${customerName}`);
    }

    export enum InquiryStatus {
        Lead = 0,
        Opportunity = 1,
        Proposal = 2,
        Shortlisted = 3,
        Negotiations = 4,
        ClosedWon = 5,
        ClosedLost = 6,
    }

    export const InquiryStatusResources: {
        Lead: string;
        Opportunity: string;
        Proposal: string;
        Shortlisted: string;
        Negotiations: string;
        ClosedWon: string;
        ClosedLost: string;
    } = Globals.enumResources.InquiryStatus;

    export const Translations: {
        ToOportunity: string;
        ToProposal: string;
        ToShortlisted: string;
        ToNegotiation: string;
        CloseWon: string;
        CloseLost: string;
        SavingMessage: string;
        CantChangeInquiryStatus: string;
        DeleteRecordConfirmMessage: string;
        DeleteRecordConfirmTitle: string;
        EditLabel: string;
        DeleteLabel: string;
        ViewLabel: string;
    } = Globals.resources;

    export interface IInquiry extends Dante.ProcessBoard.IProcessItem {
        businessLineOrgUnitId: number;
        businessLineOrgUnitName: string;
        customerDisplayName: string;
        customerId: number;
        description: string;
        displayName: string;
        expectedMargin: number;
        expectedRevenue: number;
        expectedRevenueCurrencyId: number;
        expectedRevenueCurrencyName: string;
        id: number;
        maxPreSalesSpending: number;
        offerDueDate: Date;
        offerTypeId: number;
        offerTypeName: string;
        processStatus: InquiryStatus;
        projectImportance: number;
        projectPropability: number;
        salesPersonId: number;
        salesPersonName: string;
        timestamp: string;
        watcherIds: number[];
        watchersNames: Dante.Utils.IIndexDictionary<string>;
    }

    class InquiryProcessBoard extends Dante.ProcessBoard.ProcessBoardBase<IInquiry> {
        private employeeIcon(props: { employeeId: number, employeeName: string } & React.Props<HTMLDivElement>): JSX.Element {
            const backgroundImage = `url(/Accounts/UserPicture/DownloadByEmployee?employeeId=${props.employeeId}&userPictureSize=Normal)`;

            return (
                <div className="request-item-user-container">
                    <div className="request-item-user hub-trigger" onClick={() => Hub.open(props.employeeId, "EmployeeValuePickers.Employee")} title={props.employeeName} style={{ backgroundImage }}>
                        {props.children}
                    </div>
                </div>
            );
        }

        protected renderItemContent(item: IInquiry, group: ProcessBoard.IProcessBoardGroup<IInquiry>): JSX.Element {
            const EmployeeIcon = this.employeeIcon;

            const editAction = new Actions.EditAction(item);

            return (
                <div className="inquiry-item inquiry-item-draft">
                    <div className="request-item">
                        <EmployeeIcon key={`${item.salesPersonId}_${item.$isReloading}`} employeeId={item.salesPersonId} employeeName={item.salesPersonName}>
                            {
                                item.$isReloading
                                    ? <i className="fa fa-cog fa-spin fa-fw" title={Translations.SavingMessage}></i>
                                    : null
                            }
                        </EmployeeIcon>
                        <div className="inquiry-item-description hidden-xs">
                            <div title={item.customerDisplayName} className={Dante.Utils.ClassNames({ "inquiry-item-title-editable": editAction.canExecuteAction() }, "inquiry-item-title")} onClick={() => super.handleProcessAction(item, editAction)}>
                                {item.customerDisplayName}
                            </div>
                            <div className="inquiry-item-name" title={item.displayName}>{item.displayName}</div>
                        </div>
                    </div>
                    <div className="inquiry-item-watchers-container hidden-xs">
                        <div className="inquiry-item-watchers">
                            {
                                item.watcherIds.map(w => <EmployeeIcon key={w} employeeId={w} employeeName={item.watchersNames[w]} />)
                            }
                        </div>
                    </div>
                </div>
            );
        }
    }
}

$(() => {
    $(".burger-box button.auto-card-index-edit").remove();
});
