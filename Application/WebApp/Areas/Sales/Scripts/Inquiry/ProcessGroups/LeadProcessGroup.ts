﻿namespace Dante.Sales.Inquiry.Groups {
    export class LeadProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.Lead;
        public name = InquiryStatusResources.Lead;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.Lead;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item),
                new Actions.EditAction(item),
                new Actions.DeleteAction(item),
                new Actions.ToOportunityAction(item)
            ];
        }
    }
}
