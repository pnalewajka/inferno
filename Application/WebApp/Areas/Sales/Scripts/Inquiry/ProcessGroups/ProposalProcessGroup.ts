﻿namespace Dante.Sales.Inquiry.Groups {
    export class ProposalProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.Proposal;
        public name = InquiryStatusResources.Proposal;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.Proposal;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item),
                new Actions.EditAction(item),
                new Actions.ToShortlistedAction(item)
            ];
        }

        public processAction(item: IInquiry) {
            return new Actions.ToProposalAction(item);
        }
    }
}
