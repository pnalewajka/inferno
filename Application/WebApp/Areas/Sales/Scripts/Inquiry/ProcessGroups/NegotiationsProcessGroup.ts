namespace Dante.Sales.Inquiry.Groups {
    export class NegotiationsProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.Negotiations;
        public name = InquiryStatusResources.Negotiations;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.Negotiations;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item),
                new Actions.EditAction(item),
                new Actions.CloseAction(item, InquiryStatus.ClosedLost),
                new Actions.CloseAction(item, InquiryStatus.ClosedWon)
            ];
        }

        public processAction(item: IInquiry) {
            return new Actions.ToNegotiationAction(item);
        }
    }
}
