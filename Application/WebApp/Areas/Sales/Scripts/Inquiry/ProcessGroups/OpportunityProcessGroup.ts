﻿namespace Dante.Sales.Inquiry.Groups {
    export class OportunityProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.Opportunity;
        public name = InquiryStatusResources.Opportunity;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.Opportunity;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item),
                new Actions.EditAction(item),
                new Actions.ToProposalAction(item)
            ];
        }

        public processAction(item: IInquiry) {
            return new Actions.ToOportunityAction(item);
        }
    }
}
