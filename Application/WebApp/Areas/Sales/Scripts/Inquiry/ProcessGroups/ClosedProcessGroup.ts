namespace Dante.Sales.Inquiry.Groups {
    export class ClosedWonProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.ClosedWon;
        public name = InquiryStatusResources.ClosedWon;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.ClosedWon;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item)
            ];
        }

        public processAction(item: IInquiry) {
            return new Actions.CloseAction(item, InquiryStatus.ClosedWon);
        }
    }

    export class ClosedLostProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.ClosedLost;
        public name = InquiryStatusResources.ClosedLost;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.ClosedLost;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item),
                new Actions.CloseAction(item, InquiryStatus.ClosedWon),
                new Actions.ToNegotiationAction(item),
            ];
        }

        public processAction(item: IInquiry) {
            return new Actions.CloseAction(item, InquiryStatus.ClosedLost);
        }
    }
}
