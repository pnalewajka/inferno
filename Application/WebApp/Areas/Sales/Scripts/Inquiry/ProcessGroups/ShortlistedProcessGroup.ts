namespace Dante.Sales.Inquiry.Groups {
    export class ShortlistedProcessGroup implements Dante.ProcessBoard.IProcessBoardGroup<IInquiry> {
        public groupType = InquiryStatus.Shortlisted;
        public name = InquiryStatusResources.Shortlisted;

        public isGroupItem(item: IInquiry): boolean {
            return item.processStatus === InquiryStatus.Shortlisted;
        }

        public availableProcessActions(item: IInquiry): Array<ProcessBoard.IItemProcessAction<IInquiry>> {
            return [
                new Actions.ViewAction(item),
                new Actions.EditAction(item),
                new Actions.ToNegotiationAction(item)
            ];
        }

        public processAction(item: IInquiry) {
            return new Actions.ToShortlistedAction(item);
        }
    }
}
