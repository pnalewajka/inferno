﻿namespace Dante.Sales.Inquiry.Actions {
    export class EditAction implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry) {
            this.processItem = item;
        }

        public processItem: IInquiry;
        public displayName = Translations.EditLabel;
        private formData: string | null = null;
        private errorDialogScreen: string | null = null;

        public optimisticUpdate(): Promise<IInquiry> {
            return new Promise<IInquiry>((resolve, reject) => {
                if (this.errorDialogScreen !== null) {
                    this.displayEditFormDialog(this.errorDialogScreen, resolve, reject);
                } else {
                    CommonDialogs.pleaseWait.show();

                    $.post(`/Sales/Inquiry/GetEditDialog/${this.processItem.id}`, (modalResponse: string) => {
                        CommonDialogs.pleaseWait.hide();

                        this.displayEditFormDialog(modalResponse, resolve, reject);
                    });
                }
            });
        }

        public serverUpdate(item: IInquiry): IInquiry | Promise<IInquiry> {
            return new Promise<IInquiry>((resolve, reject) => {
                $.ajax({
                    type: "POST",
                    url: `/Sales/Inquiry/HandleEditDialog/${this.processItem.id}`,
                    data: this.formData,
                    success: (response, statusText, jqXhr) => {
                        if (typeof response === "string") {
                            this.errorDialogScreen = response;
                            const validationErrors = $(response).find(".field-validation-error").map((i, e) => $(e).text()).get().join(", ");

                            reject(validationErrors);
                        } else {
                            $.get(`/Sales/Inquiry/GetInquiryData/${this.processItem.id}`).then(resolve);
                        }
                    }
                });
            });
        }

        public canExecuteAction(): boolean {
            if (Globals.navigation.currentUser.Roles.indexOf("CanEditSalesInquiry") === -1 || !!this.processItem.$loadingError || !!this.processItem.$isReloading) {
                return false;
            }

            return this.processItem.processStatus !== InquiryStatus.ClosedLost && this.processItem.processStatus !== InquiryStatus.ClosedWon;
        }

        private displayEditFormDialog(html: string, resolve: (item: IInquiry) => void, reject: (error: string) => void): void {
            Dialogs.showHtmlDialog(html, new Dialogs.DialogEventHandler({
                onSubmit: (actionData) => {
                    const customerDisplayName = $("#customer-id", actionData.dialog).text();
                    const displayName = $("#display-name", actionData.dialog).val();
                    const selesPersionName = $("#sales-person-id", actionData.dialog).text();
                    const salesPersonId = +$("[name='SalesPersonId']", actionData.dialog).val();
                    this.formData = actionData.formData;

                    CommonDialogs.pleaseWait.hide();

                    actionData.dialog.modal("hide");
                    resolve({
                        ...this.processItem, ... {
                            customerDisplayName, displayName, salesPersonId
                        }
                    });
                },
                eventHandler: (e) => {
                    if (!e.isOk) {
                        reject("cancel");
                    }
                }
            }), null);
        }
    }
}
