﻿namespace Dante.Sales.Inquiry.Actions {
    export class ToOportunityAction
        extends Base.BaseChangeStatusAction
        implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry) {
            super(item, InquiryStatus.Opportunity);
        }

        public displayName = Translations.ToOportunity;

        public canExecuteAction(): boolean {
            return super.hasAccessToChangeState() && this.processItem.processStatus === InquiryStatus.Lead;
        }
    }
}
