﻿namespace Dante.Sales.Inquiry.Actions.Base {
    export class BaseChangeStatusAction {
        public constructor(item: IInquiry, newState: InquiryStatus) {
            this.processItem = item;
            this.newInquiryStatus = newState;
        }

        public processItem: IInquiry;
        public newInquiryStatus: InquiryStatus;

        public optimisticUpdate(): Promise<IInquiry> {
            return Promise.resolve({ ...this.processItem, ...{ processStatus: this.newInquiryStatus } });
        }

        public serverUpdate(): IInquiry | Promise<IInquiry> {
            return new Promise<IInquiry>((resolve, reject) => {
                $.post(
                    Globals.resolveUrl("~/Sales/Inquiry/ChangeStatus"),
                    {
                        inquiryId: this.processItem.id,
                        newStatus: this.newInquiryStatus
                    })
                    .then((inquiry: IInquiry) => {
                        resolve({ ...inquiry, ...{ $isReloading: false } });
                    })
                    .fail(() => {
                        reject(Translations.CantChangeInquiryStatus);
                    });
            });
        }

        protected hasAccessToChangeState(): boolean {
            return Globals.navigation.currentUser.Roles.indexOf("CanChangeSalesInquiryStatus") !== -1;
        }
    }
}
