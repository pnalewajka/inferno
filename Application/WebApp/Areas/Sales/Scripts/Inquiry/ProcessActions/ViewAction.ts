﻿namespace Dante.Sales.Inquiry.Actions {
    export class ViewAction extends Actions.EditAction {
        public constructor(item: IInquiry) {
            super(item);
        }
        public displayName = Translations.ViewLabel;

        public optimisticUpdate(): Promise<IInquiry> {
            Dialogs.showDialog({
                actionUrl: `/Sales/Inquiry/GetViewDialog/${this.processItem.id}`,
            });

            return Promise.resolve(this.processItem);
        }

        public serverUpdate(item: IInquiry): IInquiry | Promise<IInquiry> {
            return this.processItem;
        }

        public canExecuteAction(): boolean {
            if (Globals.navigation.currentUser.Roles.indexOf("CanViewSalesInquiry") === -1) {
                return false;
            }

            return !super.canExecuteAction();
        }
    }
}
