﻿namespace Dante.Sales.Inquiry.Actions {
    export class ToNegotiationAction
        extends Base.BaseChangeStatusAction
        implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry) {
            super(item, InquiryStatus.Negotiations);
        }

        public displayName = Translations.ToNegotiation;

        public canExecuteAction(): boolean {
            return super.hasAccessToChangeState() && this.processItem.processStatus === InquiryStatus.Shortlisted;
        }
    }
}
