﻿namespace Dante.Sales.Inquiry.Actions {
    export class ToProposalAction
        extends Base.BaseChangeStatusAction
        implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry) {
            super(item, InquiryStatus.Proposal);
        }

        public displayName = Translations.ToProposal;

        public canExecuteAction(): boolean {
            return super.hasAccessToChangeState() && this.processItem.processStatus === InquiryStatus.Opportunity;
        }
    }
}
