﻿namespace Dante.Sales.Inquiry.Actions {
    export class CloseAction
        extends Base.BaseChangeStatusAction
        implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry, newStatus: InquiryStatus.ClosedWon | InquiryStatus.ClosedLost) {
            super(item, newStatus);
        }

        public processItem: IInquiry;
        public get displayName(): string {
            return this.newInquiryStatus === InquiryStatus.ClosedLost ? Translations.CloseLost : Translations.CloseWon;
        }

        public canExecuteAction(): boolean {
            return super.hasAccessToChangeState() && this.processItem.processStatus === InquiryStatus.Negotiations || this.processItem.processStatus === InquiryStatus.ClosedLost;
        }
    }
}
