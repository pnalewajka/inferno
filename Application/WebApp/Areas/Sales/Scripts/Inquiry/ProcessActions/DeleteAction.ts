﻿namespace Dante.Sales.Inquiry.Actions {
    export class DeleteAction implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry) {
            this.processItem = item;
        }

        public processItem: IInquiry;
        public displayName = Translations.DeleteLabel;

        public optimisticUpdate(): Promise<IInquiry> {
            return new Promise<IInquiry>((resolve, reject) => {
                CommonDialogs.confirm(Translations.DeleteRecordConfirmMessage, Translations.DeleteRecordConfirmTitle, (response) => {
                    if (response.isOk) {
                        resolve({ ...this.processItem, ...{ $deleted: true } });
                    }
                });
            });
        }

        public serverUpdate(item: IInquiry): IInquiry | Promise<IInquiry> {
            return new Promise<IInquiry>((resolve, reject) => {
                $.post(`/Sales/Inquiry/DeleteInquiry/${this.processItem.id}`)
                    .then((response: { isSuccessful: boolean, messages: string[] }) => {
                        resolve({ ...this.processItem, ...{ $deleted: response.isSuccessful, $loadingError: response.messages.join(" ") } });
                    });
            });
        }

        public canExecuteAction(): boolean {
            if (Globals.navigation.currentUser.Roles.indexOf("CanDeleteSalesInquiry") === -1) {
                return false;
            }

            return this.processItem.processStatus === InquiryStatus.Lead;
        }
    }
}
