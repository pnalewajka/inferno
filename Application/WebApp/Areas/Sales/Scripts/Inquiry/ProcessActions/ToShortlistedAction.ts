﻿namespace Dante.Sales.Inquiry.Actions {
    export class ToShortlistedAction
        extends Base.BaseChangeStatusAction
        implements ProcessBoard.IItemProcessAction<IInquiry> {
        public constructor(item: IInquiry) {
            super(item, InquiryStatus.Shortlisted);
        }

        public displayName = Translations.ToShortlisted;

        public canExecuteAction(): boolean {
            return super.hasAccessToChangeState() && this.processItem.processStatus === InquiryStatus.Proposal;
        }
    }
}
