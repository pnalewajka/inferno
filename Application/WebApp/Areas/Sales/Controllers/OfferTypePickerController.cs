﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Sales.Models;
using Smt.Atomic.WebApp.Areas.Sales.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Sales.Controllers
{
    [Identifier("Sales.OfferTypePickerController")]
    [AtomicAuthorize(SecurityRoleType.CanViewSalesOfferType)]
    public class OfferTypePickerController : CardIndexController<OfferTypeViewModel, OfferTypeDto>
    {
        public OfferTypePickerController(IOfferTypeCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = OfferTypeResource.Title;
        }
    }
}
