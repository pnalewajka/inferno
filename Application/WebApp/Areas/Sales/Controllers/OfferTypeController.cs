﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Sales.Models;
using Smt.Atomic.WebApp.Areas.Sales.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Sales.Controllers
{
    [Identifier("Sales.OfferTypeController")]
    [AtomicAuthorize(SecurityRoleType.CanViewSalesOfferType)]
    public class OfferTypeController : CardIndexController<OfferTypeViewModel, OfferTypeDto>
    {
        public OfferTypeController(IOfferTypeCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = OfferTypeResource.Title;

            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSalesOfferType;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSalesOfferType;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSalesOfferType;
            CardIndex.Settings.DeleteButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "!row.hasInquiries"
            };
        }
    }
}
