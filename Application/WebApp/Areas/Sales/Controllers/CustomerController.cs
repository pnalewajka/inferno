﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Sales.Models;
using Smt.Atomic.WebApp.Areas.Sales.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Sales.Controllers
{
    [Identifier("Sales.CustomerController")]
    [AtomicAuthorize(SecurityRoleType.CanViewSalesCustomers)]
    public class CustomerController : CardIndexController<CustomerViewModel, CustomerDto>
    {
        public CustomerController(ICustomerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = CustomerResources.Title;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSalesCustomers;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSalesCustomers;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSalesCustomers;
        }
    }
}