﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Sales.Consts;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Sales.Models;
using Smt.Atomic.WebApp.Areas.Sales.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models.GridViewConfiguration;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Sales.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSalesInquiry)]
    public class InquiryController : CardIndexController<InquiryViewModel, InquiryDto>
    {
        private readonly IInquiryService _inquiryService;
        private readonly IInquiryCardIndexDataService _cardIndexDataService;
        private readonly IClassMappingFactory _classMappingFactory;

        public InquiryController(IInquiryCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies, IInquiryService inquiryService)
            : base(cardIndexDataService, dependencies)
        {
            _inquiryService = inquiryService;
            _cardIndexDataService = cardIndexDataService;
            _classMappingFactory = dependencies.ClassMappingFactory;

            CardIndex.Settings.Title = InquiryResource.Title;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSalesInquiry;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSalesInquiry;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSalesInquiry;

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            var configureJsView = new JavaScriptRenderViewConfiguration<InquiryViewModel>(string.Empty, "react", "Dante.Sales.Inquiry.Initialize") { IsDefault = true };
            configureJsView.IncludeAllColumns();

            CardIndex.Settings.GridViewConfigurations = new List<IGridViewConfiguration> {
                configureJsView
            };

            Layout.Scripts.Add("~/scripts/react");
            Layout.Scripts.Add("~/scripts/react-ui");
            Layout.Scripts.Add("~/scripts/process-board");
            Layout.Scripts.Add("~/scripts/sales/inquiry");

            Layout.Css.Add("~/Content/processBoard.css");
            Layout.Css.Add("~/Areas/Sales/Content/Inquiry.css");

            Layout.EnumResources.Add<InquiryStatus>();
            Layout.Resources.AddFrom<InquiryResource>();

            SwitchAddAndEditToDialogs();

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SalesSearchAreaCodes.Customer,  InquiryResource.CustomerLabel),
                new SearchArea(SalesSearchAreaCodes.SalesPerson, InquiryResource.SalesPersonLabel),
                new SearchArea(SalesSearchAreaCodes.Description, InquiryResource.DescriptionLabel, false),
            };

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                ButtonText = InquiryResource.Filters,
                DisplayName = InquiryResource.ProjectImportanceLabel,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = CardIndexHelper.BuildEnumBasedFilters<ImportanceType>()
            });

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                ButtonText = InquiryResource.Filters,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<InquiryBusinessLineFilterViewModel>(SalesFilterCodes.Sales, InquiryResource.SalesFilter),
                }
            });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanChangeSalesInquiryStatus)]
        public JsonNetResult ChangeStatus(long inquiryId, InquiryStatus newStatus)
        {
            var changeStatusResult = _inquiryService.ChangeStatus(inquiryId, newStatus);

            if (changeStatusResult.IsSuccessful)
            {
                return GetInquiryData(inquiryId);
            }

            throw new BusinessException("Can't change Inquiry status");
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanViewSalesInquiry)]
        public JsonNetResult GetInquiryData(long id)
        {
            var inquiryDto = _cardIndexDataService.GetRecordById(id);
            var inquiryViewModel = _classMappingFactory.CreateMapping<InquiryDto, InquiryViewModel>().CreateFromSource(inquiryDto);

            return new JsonNetResult(inquiryViewModel)
            {
                SerializerSettings = JsonHelper.DefaultAjaxSettings
            };
        }

        [AtomicAuthorize(SecurityRoleType.CanDeleteSalesInquiry)]
        public JsonNetResult DeleteInquiry(long id)
        {
            var result = _cardIndexDataService.DeleteRecords(new[] { id });

            return new JsonNetResult(new
            {
                result.IsSuccessful,
                Messages = result.Alerts.Select(a => a.Message).ToArray()
            })
            {
                SerializerSettings = JsonHelper.DefaultAjaxSettings
            };
        }
    }
}
