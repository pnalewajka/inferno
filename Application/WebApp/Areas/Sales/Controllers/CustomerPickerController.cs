﻿using System.Web.Mvc;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Sales.Models;
using Smt.Atomic.WebApp.Areas.Sales.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Sales.Controllers
{
    [Identifier("SalesValuePickers.Customer")]
    [AtomicAuthorize(SecurityRoleType.CanViewSalesCustomers)]
    public class CustomerPickerController : ReadOnlyCardIndexController<CustomerViewModel, CustomerDto>
    {
        public CustomerPickerController(ICustomerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {

        }
    }
}