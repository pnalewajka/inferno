﻿using System.Linq;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class InquiryViewModelToInquiryDtoMapping : ClassMapping<InquiryViewModel, InquiryDto>
    {
        public InquiryViewModelToInquiryDtoMapping()
        {
            Mapping = v => new InquiryDto
            {
                Id = v.Id,
                CustomerId = v.CustomerId,
                DisplayName = v.DisplayName,
                ProcessStatus = v.ProcessStatus,
                BusinessLineOrgUnitId = v.BusinessLineOrgUnitId,
                BusinessLineOrgUnitName = v.BusinessLineOrgUnitName,
                Description = v.Description,
                SalesPersonId = v.SalesPersonId,
                SalesPersonName = v.SalesPersonName,
                ProjectPropability = v.ProjectPropability,
                ProjectImportance = v.ProjectImportance,
                ExpectedRevenue = v.ExpectedRevenue,
                ExpectedRevenueCurrencyId = v.ExpectedRevenueCurrencyId,
                ExpectedRevenueCurrencyName = v.ExpectedRevenueCurrencyName,
                ExpectedMargin = v.ExpectedMargin,
                MaxPreSalesSpending = v.MaxPreSalesSpending,
                OfferDueDate = v.OfferDueDate,
                OfferTypeId = v.OfferTypeId,
                OfferTypeName = v.OfferTypeName,
                WatcherIds = v.WatcherIds.ToList(),
                Timestamp = v.Timestamp
            };
        }
    }
}
