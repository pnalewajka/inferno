﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Sales.Resources;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class OfferTypeViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(OfferTypeResource.OfferName), typeof(OfferTypeResource))]
        public string OfferName { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasInquiries { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return OfferName;
        }
    }
}
