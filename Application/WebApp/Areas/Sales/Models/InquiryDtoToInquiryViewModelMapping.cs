﻿using System.Linq;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class InquiryDtoToInquiryViewModelMapping : ClassMapping<InquiryDto, InquiryViewModel>
    {
        public InquiryDtoToInquiryViewModelMapping()
        {
            Mapping = d => new InquiryViewModel
            {
                Id = d.Id,
                CustomerId = d.CustomerId,
                CustomerDisplayName = d.CustomerDisplayName,
                DisplayName = d.DisplayName,
                ProcessStatus = d.ProcessStatus,
                BusinessLineOrgUnitId = d.BusinessLineOrgUnitId,
                BusinessLineOrgUnitName = d.BusinessLineOrgUnitName,
                Description = d.Description,
                SalesPersonId = d.SalesPersonId,
                SalesPersonName = d.SalesPersonName,
                ProjectPropability = d.ProjectPropability,
                ProjectImportance = d.ProjectImportance,
                ExpectedRevenue = d.ExpectedRevenue,
                ExpectedRevenueCurrencyId = d.ExpectedRevenueCurrencyId,
                ExpectedRevenueCurrencyName = d.ExpectedRevenueCurrencyName,
                ExpectedMargin = d.ExpectedMargin,
                MaxPreSalesSpending = d.MaxPreSalesSpending,
                OfferDueDate = d.OfferDueDate,
                OfferTypeId = d.OfferTypeId,
                OfferTypeName = d.OfferTypeName,
                WatcherIds = d.WatcherIds.ToList(),
                WatchersNames = d.WatchersNames,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
