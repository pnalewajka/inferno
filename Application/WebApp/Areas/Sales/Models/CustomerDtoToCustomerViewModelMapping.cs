﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class CustomerDtoToCustomerViewModelMapping : ClassMapping<CustomerDto, CustomerViewModel>
    {
        public CustomerDtoToCustomerViewModelMapping()
        {
            Mapping = d => new CustomerViewModel
            {
                Id = d.Id,
                LegalName = d.LegalName,
                ShortName = d.ShortName,
                Timestamp = d.Timestamp
            };
        }
    }
}
