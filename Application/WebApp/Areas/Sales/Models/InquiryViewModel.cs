﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Sales.Controllers;
using Smt.Atomic.WebApp.Areas.Sales.Resources;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    [TabDescription(0, OverviewTab, nameof(InquiryResource.OverviewTab), typeof(InquiryResource))]
    [TabDescription(1, SalesTab, nameof(InquiryResource.SalesTab), typeof(InquiryResource))]
    [TabDescription(2, OthersTab, nameof(InquiryResource.OtherTab), typeof(InquiryResource))]
    [Identifier("Sales.InquiryViewModel")]
    public class InquiryViewModel
    {
        internal const string OverviewTab = "overview";
        internal const string SalesTab = "sales";
        internal const string OthersTab = "others";

        public long Id { get; set; }

        [ValuePicker(Type = typeof(CustomerController), OnSelectionChangedJavaScript = "Dante.Sales.Inquiry.CustomerChanged")]
        [DisplayNameLocalized(nameof(InquiryResource.CustomerLabel), typeof(InquiryResource))]
        [Visibility(VisibilityScope.Form)]
        [Render(Size = Size.Large)]
        [Required]
        [Tab(OverviewTab)]
        public long CustomerId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(InquiryResource.CustomerLabel), typeof(InquiryResource))]
        public string CustomerDisplayName { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.InquiryDisplayNameLabel), typeof(InquiryResource))]
        [Required]
        [StringLength(500)]
        [Tab(OverviewTab)]
        public string DisplayName { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.ProcessStatusLabel), typeof(InquiryResource))]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.Grid)]
        public InquiryStatus ProcessStatus { get; set; }

        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(InquiryResource.BusinessLineLabel), typeof(InquiryResource))]
        [Render(Size = Size.Large)]
        [Required]
        [Tab(OverviewTab)]
        public long? BusinessLineOrgUnitId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(InquiryResource.BusinessLineLabel), typeof(InquiryResource))]
        public string BusinessLineOrgUnitName { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(InquiryResource.SalesPersonLabel), typeof(InquiryResource))]
        [Render(Size = Size.Large)]
        [Required]
        [Tab(OverviewTab)]
        public long SalesPersonId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(InquiryResource.SalesPersonLabel), typeof(InquiryResource))]
        public string SalesPersonName { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.ProjectPropabilityLabel), typeof(InquiryResource))]
        [Render(Size = Size.Large)]
        [Tab(SalesTab)]
        public int? ProjectPropability { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.ProjectImportanceLabel), typeof(InquiryResource))]
        [Tab(SalesTab)]
        [Render(Size = Size.Large)]
        public ImportanceType ProjectImportance { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.ExpectedRevenueLabel), typeof(InquiryResource))]
        [Tab(SalesTab)]
        [Render(Size = Size.Large)]
        public decimal? ExpectedRevenue { get; set; }

        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(InquiryResource.ExpectedRevenueCurrencyLabel), typeof(InquiryResource))]
        [Visibility(VisibilityScope.Form)]
        [Tab(SalesTab)]
        [Render(Size = Size.Large)]
        public long? ExpectedRevenueCurrencyId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(InquiryResource.ExpectedRevenueCurrencyLabel), typeof(InquiryResource))]
        public string ExpectedRevenueCurrencyName { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.ExpectedMarginLabel), typeof(InquiryResource))]
        [Tab(SalesTab)]
        [Render(Size = Size.Large)]
        public decimal? ExpectedMargin { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.MaxPreSalesSpendingLabel), typeof(InquiryResource))]
        [Tab(SalesTab)]
        [Render(Size = Size.Large)]
        public decimal? MaxPreSalesSpending { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.OfferDueDateLabel), typeof(InquiryResource))]
        [Tab(SalesTab)]
        [Render(Size = Size.Large)]
        public DateTime? OfferDueDate { get; set; }

        [ValuePicker(Type = typeof(OfferTypePickerController))]
        [DisplayNameLocalized(nameof(InquiryResource.OfferTypeLabel), typeof(InquiryResource))]
        [Visibility(VisibilityScope.Form)]
        [Required]
        [Tab(OverviewTab)]
        [Render(Size = Size.Large)]
        public long OfferTypeId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(InquiryResource.OfferTypeLabel), typeof(InquiryResource))]
        public string OfferTypeName { get; set; }

        [RichText]
        [AllowHtml]
        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(InquiryResource.DescriptionLabel), typeof(InquiryResource))]
        [Tab(OthersTab)]
        public string Description { get; set; }


        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(InquiryResource.WatchersLabel), typeof(InquiryResource))]
        [Tab(OthersTab)]
        public IList<long> WatcherIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public IDictionary<long, string> WatchersNames { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
