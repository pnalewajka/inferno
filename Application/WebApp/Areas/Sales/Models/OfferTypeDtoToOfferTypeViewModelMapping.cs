﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class OfferTypeDtoToOfferTypeViewModelMapping : ClassMapping<OfferTypeDto, OfferTypeViewModel>
    {
        public OfferTypeDtoToOfferTypeViewModelMapping()
        {
            Mapping = d => new OfferTypeViewModel
            {
                Id = d.Id,
                OfferName = d.OfferName,
                HasInquiries = d.HasInquiries,
                Timestamp = d.Timestamp
            };
        }
    }
}
