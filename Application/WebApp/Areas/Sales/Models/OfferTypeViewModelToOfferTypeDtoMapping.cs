﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class OfferTypeViewModelToOfferTypeDtoMapping : ClassMapping<OfferTypeViewModel, OfferTypeDto>
    {
        public OfferTypeViewModelToOfferTypeDtoMapping()
        {
            Mapping = v => new OfferTypeDto
            {
                Id = v.Id,
                OfferName = v.OfferName,
                Timestamp = v.Timestamp
            };
        }
    }
}
