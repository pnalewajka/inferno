﻿using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class CustomerViewModelToCustomerDtoMapping : ClassMapping<CustomerViewModel, CustomerDto>
    {
        public CustomerViewModelToCustomerDtoMapping()
        {
            Mapping = v => new CustomerDto
            {
                Id = v.Id,
                LegalName = v.LegalName,
                ShortName = v.ShortName,
                Timestamp = v.Timestamp
            };
        }
    }
}
