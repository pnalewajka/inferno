﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Sales.Resources;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    public class CustomerViewModel
    {
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(CustomerResources.LegalNameLabel), typeof(CustomerResources))]
        [StringLength(250)]
        public string LegalName { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(CustomerResources.ShortNameLabel), typeof(CustomerResources))]
        [StringLength(50)]
        public string ShortName { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return ShortName;
        }
    }
}
