﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Sales.Controllers;
using Smt.Atomic.WebApp.Areas.Sales.Resources;

namespace Smt.Atomic.WebApp.Areas.Sales.Models
{
    [Identifier("Sales.InquiryBusinessLineFilterViewModel")]
    [DescriptionLocalized(nameof(InquiryResource.SalesFilterTitle), typeof(InquiryResource))]
    public class InquiryBusinessLineFilterViewModel
    {
        [DisplayNameLocalized(nameof(InquiryResource.BusinessLineLabel), typeof(InquiryResource))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] OrgUnitIds { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.SalesPersonLabel), typeof(InquiryResource))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] SalesPersonIds { get; set; }

        [DisplayNameLocalized(nameof(InquiryResource.OfferTypeLabel), typeof(InquiryResource))]
        [ValuePicker(Type = typeof(OfferTypePickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] OfferTypeIds { get; set; }

        public override string ToString()
        {
            return InquiryResource.SalesFilter;
        }
    }
}
