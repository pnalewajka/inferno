﻿using System;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Sales
{
    public class BundleConfig
    {
        internal static void RegisterSalesBundles(BundleCollection bundles)
        {
            var inquiryBundle = new ScriptBundle("~/scripts/sales/inquiry")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder(false)
            }.IncludeDirectory("~/Areas/Sales/Scripts/Inquiry", "*.js", true);

            bundles.Add(inquiryBundle);
        }
    }
}