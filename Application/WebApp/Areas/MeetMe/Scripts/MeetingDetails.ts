﻿module MeetingDetails {
    export function finishMeeting(grid: Grid.GridComponent) {

        CommonDialogs.confirm(Globals.resources.FinishMeetingConfirmationMessage,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    const meetingId = grid.getSelectedRowData().map(r => r.id.toString());
                    const url = `~/MeetMe/MeetingDetails/FinishMeeting?meetingIds=${meetingId}`;

                    Forms.submit(Globals.resolveUrl(url), "POST");
                }
            });
    }
}