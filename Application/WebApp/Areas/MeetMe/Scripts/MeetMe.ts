﻿module MeetMe {
    import EventArgs = Dialogs.EventArgs;

    export function showTimeLineDialog(baseUrl: string, grid: Grid.GridComponent): void {
        var [selectedUserId] = grid.getSelectedRowData().map(a => a.id);

        let url = UrlHelper.updateUrlParameter(baseUrl, "employeeId", selectedUserId);
        window.location.href = url;
    }
}