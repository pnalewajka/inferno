﻿module Feedback {
    export function submit(button: Element) {
        const returnUrlFieldName = "Atomic.ReturnUrl";
        const returnUrl = Globals.parameters["MeetMe.AddFeedbackEmptyContextCancelUrl"];

        var $button = $(button);

        var formId = $button.attr("form");
        var form = formId ? $(`form[id="${formId}"]`) : $button.closest("form");

        var urlInput = form.find(`input[name='${returnUrlFieldName}']`);

        if (urlInput.length === 0) {
            urlInput = $(`<input type="hidden" name="${returnUrlFieldName}" />`).appendTo(form);
        }

        urlInput.val(returnUrl);

        form.submit();
    }
}