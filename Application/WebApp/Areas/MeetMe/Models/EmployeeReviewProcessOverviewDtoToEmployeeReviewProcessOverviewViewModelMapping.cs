﻿using System.Linq;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class EmployeeReviewProcessOverviewDtoToEmployeeReviewProcessOverviewViewModelMapping : ClassMapping<EmployeeReviewProcessOverviewDto, EmployeeReviewProcessOverviewViewModel>
    {
        private readonly IClassMappingFactory _classMappingFactory;

        public EmployeeReviewProcessOverviewDtoToEmployeeReviewProcessOverviewViewModelMapping(IClassMappingFactory classMappingFactory)
        {
            _classMappingFactory = classMappingFactory;

            var employeeTimelineMeetingMapping = _classMappingFactory.CreateMapping<EmployeeTimelineMeetingDto, EmployeeTimelineMeetingViewModel>();
            var EmployeeTimelineFeedbackMapping = _classMappingFactory.CreateMapping<EmployeeTimelineFeedbackDto, EmployeeTimelineFeedbackViewModel>();
            var EmployeeTimelineEmploymentPeriodMapping = _classMappingFactory.CreateMapping<EmployeeTimelineEmploymentPeriodDto, EmployeeTimelineEmploymentPeriodViewModel>();
            var EmployeeTimelineProjectAllocationMapping = _classMappingFactory.CreateMapping<EmployeeTimelineProjectAllocationDto, EmployeeTimelineProjectAllocationViewModel>();
            var EmployeeTimelineLineManagerChangedMapping = _classMappingFactory.CreateMapping<EmployeeTimelineLineManagerChangedDto, EmployeeTimelineLineManagerChangedViewModel>();

            Mapping = dto => new EmployeeReviewProcessOverviewViewModel
            {
                Meetings = dto.Meetings.Select(m => employeeTimelineMeetingMapping.CreateFromSource(m)).ToList(),
                Feedbacks = dto.Feedbacks.Select(f => EmployeeTimelineFeedbackMapping.CreateFromSource(f)).ToList(),
                EmploymentPeriods = dto.EmploymentPeriods.Select(e => EmployeeTimelineEmploymentPeriodMapping.CreateFromSource(e)).ToList(),
                AllocationRequests = dto.AllocationRequests.Select(a => EmployeeTimelineProjectAllocationMapping.CreateFromSource(a)).ToList(),
                LineManagerChanges = dto.LineManagerChanges.Select(l => EmployeeTimelineLineManagerChangedMapping.CreateFromSource(l)).ToList(),
                EmployeeFullName = dto.EmployeeFullName
            };
        }
    }
}
