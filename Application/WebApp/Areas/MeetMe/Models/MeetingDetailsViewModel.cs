﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.MeetMe.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.MeetMe.Renders.Attributes;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingDetailsViewModel
    {
        public MeetingDetailsViewModel()
        {
            MeetingStatus = MeetingStatus.Pending;
            MeetingDate = DateTime.Now;
            Attachments = new List<DocumentViewModel>();
        }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(MeetingResources.MeetingDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringFormat("{0:d}")]
        public DateTime? MeetingDate { get; set; }

        [DisplayNameLocalized(nameof(MeetingResources.NextMeetingDueDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringFormat("{0:d}")]
        [ReadOnly(true)]
        public DateTime? MeetingDueDate { get; set; }

        [DisplayNameLocalized(nameof(MeetingResources.MeetingStatusLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ReadOnly(true)]
        public MeetingStatus MeetingStatus { get; set; }

        [DisplayNameLocalized(nameof(MeetingResources.MeetingAttachmentsLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Form)]
        [DocumentUpload(typeof(MeetingAttachmentMapping))]
        public List<DocumentViewModel> Attachments { get; set; }

        [RoleRequired(SecurityRoleType.CanViewMeetingNotes)]
        [DisplayNameLocalized(nameof(MeetingResources.MeetingsNotesLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        [NoteList]
        [NonSortable]
        public List<MeetingNoteViewModel> Notes { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanAddNote { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeEdited { get; set; }

        public override string ToString()
        {
            return string.Format(MeetingResources.MeetingDescription, MeetingDate);
        }
    }
}