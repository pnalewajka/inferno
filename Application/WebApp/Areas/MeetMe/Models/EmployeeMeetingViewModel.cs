﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class EmployeeMeetingViewModel : IGridViewRow
    {
        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long Id { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Visibility(VisibilityScope.Grid)]
        [Hub(nameof(Id))]
        public string EmployeeFullname { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(MeetingResources.EmployeeWorkStartDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? EmployeeWorkStartDate { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long? LineManagerId { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(MeetingResources.LineManagerLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Hub(nameof(LineManagerId))]
        public string LineManagerName { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(MeetingResources.LocationLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        public string LocationName { get; set; }

        [Order(4)]
        [Hint(nameof(MeetingResources.MeetingsDoneCountLabel), typeof(MeetingResources))]
        [DisplayNameLocalized(nameof(MeetingResources.MeetingsDoneCountLabel), nameof(MeetingResources.MeetingsDoneCountShortLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        public int MeetingsDoneCount { get; set; }

        [Order(5)]
        [Hint(nameof(MeetingResources.StatusLabel), typeof(MeetingResources))]
        [DisplayNameLocalized(nameof(MeetingResources.StatusLabel), nameof(MeetingResources.StatusShortLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        public MeetMeProcessStatus? NextMeetingStatus { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(MeetingResources.LastMeetingDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? LastMeetingDate { get; set; }

        [Order(7)]
        [DisplayNameLocalized(nameof(MeetingResources.EmployeeLastFeedback), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? LastFeedbackDate { get; set; }

        [Order(8)]
        [DisplayNameLocalized(nameof(MeetingResources.NextMeetingScheduleDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? NextMeetingScheduledDate { get; set; }

        [Order(9)]
        [DisplayNameLocalized(nameof(MeetingResources.NextMeetingDueDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? NextMeetingDueDate { get; set; }

        string IGridViewRow.RowCssClass { get { return NextMeetingStatus == MeetMeProcessStatus.Overdue ? CardIndexStyles.RowClassDanger : null; } }

        string IGridViewRow.RowTitle { get { return NextMeetingStatus == MeetMeProcessStatus.Overdue ? MeetingResources.Overdue : null; } }

        public override string ToString()
        {
            return EmployeeFullname;
        }
    }
}