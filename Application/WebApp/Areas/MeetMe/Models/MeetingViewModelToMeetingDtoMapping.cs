﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingViewModelToMeetingDtoMapping : ClassMapping<EmployeeMeetingViewModel, EmployeeMeetingDto>
    {
        public MeetingViewModelToMeetingDtoMapping()
        {
            Mapping = v => new EmployeeMeetingDto
            {
                Id = v.Id
            };
        }
    }
}
