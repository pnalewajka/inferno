﻿using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class EmployeeReviewProcessOverviewViewModel
    {
        public string EmployeeFullName { get; set; }

        public IList<EmployeeTimelineMeetingViewModel> Meetings { get; set; }

        public IList<EmployeeTimelineFeedbackViewModel> Feedbacks { get; set; }

        public IList<EmployeeTimelineEmploymentPeriodViewModel> EmploymentPeriods { get; set; }

        public IList<EmployeeTimelineProjectAllocationViewModel> AllocationRequests { get; set; }

        public IList<EmployeeTimelineLineManagerChangedViewModel> LineManagerChanges { get; set; }
    }
}