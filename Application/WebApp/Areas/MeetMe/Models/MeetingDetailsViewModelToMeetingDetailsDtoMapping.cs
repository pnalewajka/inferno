﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingDetailsViewModelToMeetingDtoMapping : ClassMapping<MeetingDetailsViewModel, MeetingDetailsDto>
    {
        public MeetingDetailsViewModelToMeetingDtoMapping()
        {
            Mapping = v => new MeetingDetailsDto
            {
                Id = v.Id,
                MeetingDueDate = v.MeetingDueDate,
                MeetingDate = v.MeetingDate,
                MeetingStatus = v.MeetingStatus,
                Attachments = v.Attachments
                    .Select(x => new DocumentDto
                    {
                        ContentType = x.ContentType,
                        DocumentName = x.DocumentName,
                        DocumentId = x.DocumentId,
                        TemporaryDocumentId = x.TemporaryDocumentId                        
                    }).ToList()
            };
        }
    }
}