﻿using System.Linq;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingDetailsDtoToMeetingDetailsViewModelMapping : ClassMapping<MeetingDetailsDto, MeetingDetailsViewModel>
    {
        private readonly IClassMapping<MeetingNoteDto, MeetingNoteViewModel> _meetingNoteDtoToMettingNoteViewModelMapping;
        public MeetingDetailsDtoToMeetingDetailsViewModelMapping(IClassMapping<MeetingNoteDto, MeetingNoteViewModel> meetingNoteDtoToMettingNoteViewModelMapping)
        {
            _meetingNoteDtoToMettingNoteViewModelMapping = meetingNoteDtoToMettingNoteViewModelMapping;
            Mapping = d => new MeetingDetailsViewModel
            {
                Id = d.Id,
                MeetingDueDate = d.MeetingDueDate,
                MeetingDate = d.MeetingDate,
                MeetingStatus = d.MeetingStatus,
                Attachments = d.Attachments
                    .Select(x => new DocumentViewModel
                    {
                        ContentType = x.ContentType,
                        DocumentName = x.DocumentName,
                        DocumentId = x.DocumentId,
                        TemporaryDocumentId = x.TemporaryDocumentId
                    }).ToList(),
                Notes = d.Notes
                    .Select(n => _meetingNoteDtoToMettingNoteViewModelMapping.CreateFromSource(n))
                    .ToList(),
                CanAddNote = d.CanAddNote,
                CanBeEdited = d.CanBeEdited
            };
        }
    }
}