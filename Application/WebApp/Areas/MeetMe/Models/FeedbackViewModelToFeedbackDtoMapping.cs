﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class FeedbackViewModelToFeedbackDtoMapping : ClassMapping<FeedbackViewModel, FeedbackDto>
    {
        public FeedbackViewModelToFeedbackDtoMapping()
        {
            Mapping = v => new FeedbackDto
            {
                Id = v.Id,
                RecieverId = v.ReceiverId,
                AuthorName = v.AuthorName,
                FeedbackContent = v.FeedbackContent,
                ReceiverName = v.ReceiverName,
                Visibility = v.Visibility
            };
        }
    }
}
