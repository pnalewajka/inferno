﻿using System;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public abstract class EmployeeTimelineEventViewModel
    {
        public abstract string Name { get; }

        public long EmployeeId { get; set; }

        public string EmployeeFullName { get; set; }

        public string EmployeeEmail { get; set; }

        public DateTime HappenedOn { get; set; }

        public bool IsScheduled { get; set; }
    }
}