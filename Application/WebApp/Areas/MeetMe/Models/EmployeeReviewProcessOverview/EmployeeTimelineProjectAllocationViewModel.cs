﻿using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineProjectAllocationViewModel : EmployeeTimelineEventViewModel
    {
        public string ProjectName { get; set; }

        public override string Name => MeetingOverviewResources.EmployeeTimelineProjectAllocationName;
    }
}