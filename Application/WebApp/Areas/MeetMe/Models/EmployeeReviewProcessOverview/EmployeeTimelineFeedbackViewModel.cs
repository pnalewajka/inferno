﻿using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineFeedbackViewModel : EmployeeTimelineEventViewModel
    {
        public string FeedbackContent { get; set; }

        public override string Name => MeetingOverviewResources.EmployeeTimelineFeedbackName;
    }
}