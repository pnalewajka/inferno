﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineProjectAllocationDtoToEmployeeTimelineProjectAllocationViewModelMapping : ClassMapping<EmployeeTimelineProjectAllocationDto, EmployeeTimelineProjectAllocationViewModel>
    {
        public EmployeeTimelineProjectAllocationDtoToEmployeeTimelineProjectAllocationViewModelMapping()
        {
            Mapping = d => new EmployeeTimelineProjectAllocationViewModel
            {
                EmployeeId = d.EmployeeId,
                HappenedOn = d.HappenedOn,
                EmployeeFullName = d.EmployeeFullName,
                ProjectName = d.ProjectName
            };
        }
    }
}