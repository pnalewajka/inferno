﻿using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineLineManagerChangedViewModel : EmployeeTimelineEventViewModel
    {
        public long CurrentLineManagerId { get; set; }

        public string CurrentLineManagerFullName { get; set; }

        public override string Name => MeetingOverviewResources.EmployeeTimelineLineManagerChanged;
    }
}