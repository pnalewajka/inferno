﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineFeedbackDtoToEmployeeTimelineFeedbackViewModelMapping : ClassMapping<EmployeeTimelineFeedbackDto, EmployeeTimelineFeedbackViewModel>
    {
        private readonly IPrincipalProvider _principalProvider;

        public EmployeeTimelineFeedbackDtoToEmployeeTimelineFeedbackViewModelMapping(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
            
            Mapping = d => new EmployeeTimelineFeedbackViewModel
            {
                EmployeeId = IsFeedbackAnonymous(d) ? BusinessLogicHelper.NonExistingId : d.AuthorId,
                HappenedOn = d.HappenedOn,
                EmployeeFullName = IsFeedbackAnonymous(d) ? BusinessLogicHelper.NonExistingOrAnonymousUserName : d.AuthorFullName,
                FeedbackContent = d.FeedbackContent,
            };
        }

        private bool IsFeedbackAnonymous(EmployeeTimelineFeedbackDto dto)
        {
            var currentEmployeeId = _principalProvider?.Current?.EmployeeId;

            return dto.ReceiverId == currentEmployeeId && dto.Visibility == FeedbackVisbility.AnonymousForReceiver;
        }
    }
}