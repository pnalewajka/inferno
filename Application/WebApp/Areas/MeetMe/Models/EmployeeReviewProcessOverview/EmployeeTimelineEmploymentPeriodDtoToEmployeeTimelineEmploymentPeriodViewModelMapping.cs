﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineEmploymentPeriodDtoToEmployeeTimelineEmploymentPeriodViewModelMapping : ClassMapping<EmployeeTimelineEmploymentPeriodDto, EmployeeTimelineEmploymentPeriodViewModel>
    {
        public EmployeeTimelineEmploymentPeriodDtoToEmployeeTimelineEmploymentPeriodViewModelMapping()
        {
            Mapping = d => new EmployeeTimelineEmploymentPeriodViewModel
            {
                EmployeeId = d.EmployeeId,
                HappenedOn = d.HappenedOn,
                EmployeeFullName = d.EmployeeFullName,
                JobTitleName = d.JobTitleName,
                Body = d.Body,
                Header = d.Header
            };
        }
    }
}