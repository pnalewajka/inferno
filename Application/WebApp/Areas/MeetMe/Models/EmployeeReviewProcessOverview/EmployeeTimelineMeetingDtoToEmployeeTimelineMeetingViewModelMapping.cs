﻿using System.Linq;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineMeetingDtoToEmployeeTimelineMeetingViewModelMapping : ClassMapping<EmployeeTimelineMeetingDto, EmployeeTimelineMeetingViewModel>
    {
        public EmployeeTimelineMeetingDtoToEmployeeTimelineMeetingViewModelMapping()
        {
            Mapping = d => new EmployeeTimelineMeetingViewModel
            {
                EmployeeId = d.EmployeeId,
                HappenedOn = d.HappenedOn,
                EmployeeFullName = d.EmployeeFullName,
                IsScheduled = d.IsScheduled,
                Attachments = d.Attachments
                    .Select(x => new DocumentViewModel
                    {
                        ContentType = x.ContentType,
                        DocumentName = x.DocumentName,
                        DocumentId = x.DocumentId,
                        TemporaryDocumentId = x.TemporaryDocumentId
                    }).ToList(),
                Notes = d.Notes
                    .Select(x => new EmployeeTimelineMeetingNoteViewModel
                    {
                        NoteBody = x.ContentBody,
                        HappenedOn = x.HappenedOn,
                        EmployeeId = x.EmployeeId,
                        EmployeeFullName = x.EmployeeFullName,
                        Attachments = x.Attachments
                            .Select(a => new DocumentViewModel
                            {
                                ContentType = a.ContentType,
                                DocumentName = a.DocumentName,
                                DocumentId = a.DocumentId,
                                TemporaryDocumentId = a.TemporaryDocumentId
                            }).ToList()
                    }).ToList()
            };
        }
    }
}