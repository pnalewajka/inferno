﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.EmployeeReviewProcessOverview
{
    public class EmployeeTimelineLineManagerChangedDtoToEmployeeTimelineLineManagerChangedViewModel
       : ClassMapping<EmployeeTimelineLineManagerChangedDto, EmployeeTimelineLineManagerChangedViewModel>
    {
        public EmployeeTimelineLineManagerChangedDtoToEmployeeTimelineLineManagerChangedViewModel()
        {
            Mapping = d => new EmployeeTimelineLineManagerChangedViewModel
            {
                HappenedOn = d.HappenedOn,
                EmployeeId = d.EmployeeId,
                EmployeeFullName = d.EmployeeFullName,
                CurrentLineManagerId = d.CurrentLineManagerId,
                CurrentLineManagerFullName = d.CurrentLineManagerFullName
            };
        }
    }
}