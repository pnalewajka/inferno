﻿using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineEmploymentPeriodViewModel : EmployeeTimelineEventViewModel
    {
        public string JobTitleName { get; set; }

        public override string Name => MeetingOverviewResources.EmployeeTimelineEmploymentPeriodName;

        public string Header { get; set; }

        public string Body { get; set; }
    }
}