﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineMeetingNoteViewModel : EmployeeTimelineEventViewModel
    {
        public EmployeeTimelineMeetingNoteViewModel()
        {
            Attachments = new List<DocumentViewModel>();
        }

        public string NoteBody { get; set; }

        public List<DocumentViewModel> Attachments { get; set; }

        public override string Name => MeetingOverviewResources.EmployeeTimelineMeetingNoteName;
    }
}