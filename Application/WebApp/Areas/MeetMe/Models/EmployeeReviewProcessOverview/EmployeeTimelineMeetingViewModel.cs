﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models.MeetingsOverview
{
    public class EmployeeTimelineMeetingViewModel : EmployeeTimelineEventViewModel
    {
        public EmployeeTimelineMeetingViewModel()
        {
            Attachments = new List<DocumentViewModel>();
            Notes = new List<EmployeeTimelineMeetingNoteViewModel>();
        }

        public List<DocumentViewModel> Attachments { get; set; }

        public List<EmployeeTimelineMeetingNoteViewModel> Notes { get; set; }

        public override string Name => MeetingOverviewResources.EmployeeTimelineMeetingName;
    }
}