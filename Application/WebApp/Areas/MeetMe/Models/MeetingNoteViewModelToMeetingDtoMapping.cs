﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingNoteViewModelToMeetingDtoMapping : ClassMapping<MeetingNoteViewModel, MeetingNoteDto>
    {
        public MeetingNoteViewModelToMeetingDtoMapping()
        {
            Mapping = v => new MeetingNoteDto
            {
                Id = v.Id,
                MeetingId = v.MeetingId,
                Status = v.Status,
                CreatedOn = v.CreatedOn,
                NoteBody = v.NoteBody,
                AuthorName = v.AuthorName,
                Attachments = v.Attachments.Select(
                    p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    }).ToList(),
            };
        }
    }
}
