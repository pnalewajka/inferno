﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingDtoToMeetingInvitationViewModelMapping : ClassMapping<MeetingInvitationDto, MeetingInvitationViewModel>
    {
        public MeetingDtoToMeetingInvitationViewModelMapping()
        {
            Mapping = d => new MeetingInvitationViewModel
            {
                Id = d.Id,
                ParticipantEmails = string.Join("; ", new[] { d.LineManagerEmail, d.EmployeeEmail }),
                MeetingDate = d.MeetingDate.HasValue ? d.MeetingDate.Value : d.MeetingDueDate.Value,
                AdditionalInvitationContent = d.AdditionalInvitationContent
            };
        }
    }
}