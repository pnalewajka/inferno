﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class FeedbackDtoToFeedbackViewModelMapping : ClassMapping<FeedbackDto, FeedbackViewModel>
    {
        public FeedbackDtoToFeedbackViewModelMapping()
        {
            Mapping = d => new FeedbackViewModel
            {
                Id = d.Id,
                ReceiverId = d.RecieverId,
                AuthorName = d.AuthorName,
                FeedbackContent = d.FeedbackContent,
                ReceiverName = d.ReceiverName,
                Visibility = d.Visibility
            };
        }
    }
}
