﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class FeedbackViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(MeetingResources.NoteEvaluatedLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.None)]
        public string ReceiverName { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Required]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=not-terminated-employees'")]
        public long ReceiverId { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(MeetingResources.NoteAuthorLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        public string AuthorName { get; set; }

        [Order(3)]
        [AllowHtml]
        [Visibility(VisibilityScope.FormAndGrid)]
        [MaxLength(4000)]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackLabel), typeof(MeetingResources))]
        [RichText(RichTextEditorPresets.Basic)]
        public string FeedbackContent { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackVisibilityLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Form)]
        public FeedbackVisbility Visibility { get; set; }

        public override string ToString()
        {
            return string.Format(MeetingResources.FeedbackDescription, AuthorName);
        }
    }
}
