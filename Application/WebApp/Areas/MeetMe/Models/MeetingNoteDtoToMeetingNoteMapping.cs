﻿using System.Linq;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingNoteDtoToMeetingNoteMapping : ClassMapping<MeetingNoteDto, MeetingNoteViewModel>
    {
        public MeetingNoteDtoToMeetingNoteMapping()
        {
            Mapping = d => new MeetingNoteViewModel
            {
                Id = d.Id,
                MeetingId = d.MeetingId,
                Status = d.Status,
                AuthorName = d.AuthorName,
                AuthorId = d.AuthorId,
                CreatedOn = d.CreatedOn,
                NoteBody = d.NoteBody,
                Attachments = d.Attachments.Select(p => new DocumentViewModel
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId
                }).ToList(),
                CanEdit = d.CanEdit
            };
        }
    }
}
