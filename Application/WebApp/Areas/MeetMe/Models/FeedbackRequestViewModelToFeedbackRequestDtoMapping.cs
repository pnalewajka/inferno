﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class FeedbackRequestViewModelToFeedbackRequestDtoMapping : ClassMapping<FeedbackRequestViewModel, FeedbackRequestDto>
    {
        public FeedbackRequestViewModelToFeedbackRequestDtoMapping()
        {
            Mapping = v => new FeedbackRequestDto
            {
                Id = v.Id,
                EvaluatedEmployeeId = v.EvaluatedEmployeeId,
                EvaluatorEmployeeId = v.EvaluatorEmployeeId != default(long)
                                      ? v.EvaluatorEmployeeId
                                      : !v.EvaluatorEmployeeIds.IsNullOrEmpty()
                                      ? v.EvaluatorEmployeeIds.First()
                                      : default(long),
                FeedbackContent = v.FeedbackContent,
                Comment = v.Comment,
                Visibility = v.Visibility,
                OriginScenario = v.OriginScenario,
                EvaluatorEmployeeIds = v.EvaluatorEmployeeIds
            };
        }
    }
}
