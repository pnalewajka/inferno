﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class EmployeeReviewProcessOverviewViewModelToEmployeeReviewProcessOverviewDtoMapping : ClassMapping<EmployeeReviewProcessOverviewViewModel, EmployeeReviewProcessOverviewDto>
    {
        
    }
}
