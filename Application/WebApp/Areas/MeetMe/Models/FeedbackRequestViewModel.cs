﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class FeedbackRequestViewModel
    {
        public FeedbackRequestViewModel()
        {
            OriginScenario = FeedbackOriginScenario.Requested;
        }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(1)]
        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=not-terminated-employees'")]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackEvaluatedEmployeeLabel), typeof(MeetingResources))]
        [UpdatesApprovers]
        public long EvaluatedEmployeeId { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackEvaluatorEmployeeLabel), typeof(MeetingResources))]
        public long EvaluatorEmployeeId { get; set; }

        [Order(3)]
        [ValuePicker(Type = typeof(EmployeePickerByProjectFilterController), OnResolveUrlJavaScript = "url += '&mode=not-terminated-employees'")]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackEvaluatorsEmployeeLabel), typeof(MeetingResources))]
        [UpdatesApprovers]
        public long[] EvaluatorEmployeeIds { get; set; }

        [AllowHtml]
        [Order(4)]
        [Visibility(VisibilityScope.Form)]
        [MaxLength(1000)]
        [DisplayNameLocalized(nameof(MeetingResources.EvaluatorCommentLabel), typeof(MeetingResources))]
        [RichText(RichTextEditorPresets.Basic)]
        [Prompt(nameof(MeetingResources.CommentContent),typeof(MeetingResources))]
        public string Comment { get; set; }

        [AllowHtml]
        [Order(5)]
        [Visibility(VisibilityScope.Form)]
        [MaxLength(4000)]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackLabel), typeof(MeetingResources))]
        [RichText(RichTextEditorPresets.Standard)]
        public string FeedbackContent { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(MeetingResources.FeedbackVisibilityLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Form)]
        public FeedbackVisbility Visibility { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public FeedbackOriginScenario OriginScenario { get; set; }
    }
}