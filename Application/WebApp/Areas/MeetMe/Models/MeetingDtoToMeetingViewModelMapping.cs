﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingDtoToMeetingViewModelMapping : ClassMapping<EmployeeMeetingDto, EmployeeMeetingViewModel>
    {
        public MeetingDtoToMeetingViewModelMapping()
        {
            Mapping = d => new EmployeeMeetingViewModel
            {
                Id = d.Id,
                EmployeeFullname = d.EmployeeFullname,
                EmployeeWorkStartDate = d.EmployeeWorkStartDate,
                LineManagerId = d.LineManagerId,
                LineManagerName = d.LineManagerName,
                LocationName = d.LocationName,
                MeetingsDoneCount = d.MeetingsDoneCount,
                NextMeetingStatus = d.MeetMeProcessStatus,
                LastMeetingDate = d.LastMeetingDate,
                NextMeetingScheduledDate = d.NextMeetingScheduledDate,
                NextMeetingDueDate = d.NextMeetingDueDate,
                LastFeedbackDate = d.LastFeedbackDate
            };
        }
    }
}
