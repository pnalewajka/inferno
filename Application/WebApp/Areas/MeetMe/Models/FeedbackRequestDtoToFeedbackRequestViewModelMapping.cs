﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class FeedbackRequestDtoToFeedbackRequestViewModelMapping : ClassMapping<FeedbackRequestDto, FeedbackRequestViewModel>
    {
        public FeedbackRequestDtoToFeedbackRequestViewModelMapping()
        {
            Mapping = d => new FeedbackRequestViewModel
            {
                Id = d.Id,
                EvaluatedEmployeeId = d.EvaluatedEmployeeId,
                EvaluatorEmployeeId = d.EvaluatorEmployeeId,
                FeedbackContent = d.FeedbackContent,
                Comment = d.Comment,
                Visibility = d.Visibility,
                OriginScenario = d.OriginScenario,
                EvaluatorEmployeeIds = d.EvaluatorEmployeeIds
            };
        }
    }
}
