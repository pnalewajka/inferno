﻿using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    public class MeetingInvitationViewModelToMeetingInvitationDtoMapping : ClassMapping<MeetingInvitationViewModel, MeetingInvitationDto>
    {
        public MeetingInvitationViewModelToMeetingInvitationDtoMapping()
        {
            Mapping = v => new MeetingInvitationDto
            {
                Id = v.Id,
                MeetingDueDate = v.MeetingDate,
                MeetingDate = v.MeetingDate,
                AdditionalInvitationContent = v.AdditionalInvitationContent
            };
        }
    }
}