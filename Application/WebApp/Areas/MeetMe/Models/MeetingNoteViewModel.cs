﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.MeetMe.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    [Identifier("MeetMe.MeetingNoteViewModel")]
    public class MeetingNoteViewModel
    {
        public MeetingNoteViewModel()
        {
            Attachments = new List<DocumentViewModel>();
        }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long MeetingId { get; set; }

        [Order(4)]
        [MaxLength(4000)]
        [Multiline]
        [DisplayNameLocalized(nameof(MeetingResources.NoteBodyLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Render(GridCssClass = "pre-wrap")]
        public string NoteBody { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(MeetingResources.NoteCreatedOnLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        [StringFormat("{0:d}")]
        public DateTime CreatedOn { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(MeetingResources.NoteStatusLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        public NoteStatus Status { get; set; }

        [Order(0)]
        [DisplayNameLocalized(nameof(MeetingResources.NoteAuthorLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Grid)]
        public string AuthorName { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(MeetingResources.NoteAttachementsLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DocumentUpload(
            typeof(MeetingNoteAttachmentMapping),
            new string[] {
                MimeHelper.AnyImage, MimeHelper.PdfFile,
                MimeHelper.OfficeDocumentWord, MimeHelper.OldOfficeDocumentWord,
            })]
        [DocumentList]
        [NonSortable]
        public List<DocumentViewModel> Attachments { get; set; }

        [Visibility(VisibilityScope.None)]
        public long AuthorId { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanEdit { get; set; }
    }
}
