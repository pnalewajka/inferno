﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Models
{
    [Identifier("Dialogs.MeetMe.MeetingInvitation")]
    public class MeetingInvitationViewModel
    {
        public long Id { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(MeetingResources.MeetingDateLabel), typeof(MeetingResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [TimePicker(15)]
        [Required]
        public DateTime MeetingDate { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(MeetingResources.ParticipantEmails), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        public string ParticipantEmails { get; set; }

        [AllowHtml]
        [Order(3)]
        [MaxLength(4000)]
        [Multiline]
        [DisplayNameLocalized(nameof(MeetingResources.AdditionalInvitationContent), typeof(MeetingResources))]
        [Visibility(VisibilityScope.Form)]
        [Prompt(nameof(MeetingResources.ProvideAdditionalInformation), typeof(MeetingResources))]
        [RichText(RichTextEditorPresets.Basic)]
        public string AdditionalInvitationContent { get; set; }
    }
}