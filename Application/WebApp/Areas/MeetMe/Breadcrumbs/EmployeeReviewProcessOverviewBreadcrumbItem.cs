﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.MeetMe.Controllers;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Breadcrumbs
{
    public class EmployeeReviewProcessOverviewBreadcrumbItem : BreadcrumbItem
    {
        public EmployeeReviewProcessOverviewBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            var controllerName = RoutingHelper.GetControllerName(typeof(EmployeeMeetingController));
            var references = Context.SiteMap.GetPath(controllerName, nameof(EmployeeMeetingController.List)).ToList();

            var items = references
                .Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText()))
                .Concat(new[] { new BreadcrumbItemViewModel(null, MeetingResources.MeetingsOverviewButtonLabel) })
                .ToList();

            var employeeFullName = ((EmployeeReviewProcessOverviewViewModel)breadcrumbContextProvider.Context.ViewModel).EmployeeFullName;

            if (!string.IsNullOrEmpty(employeeFullName))
            {
                items.Add(new BreadcrumbItemViewModel("", employeeFullName));
            }

            SetItems(items);
        }
    }
}