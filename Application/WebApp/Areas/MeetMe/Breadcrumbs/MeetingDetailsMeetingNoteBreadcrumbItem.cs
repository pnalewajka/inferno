﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Breadcrumbs
{
    public class MeetingDetailsMeetingNoteBreadcrumbItem : BreadcrumbItem
    {
        public MeetingDetailsMeetingNoteBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = MeetingResources.MeetingNotesPageName;
        }
    }
}