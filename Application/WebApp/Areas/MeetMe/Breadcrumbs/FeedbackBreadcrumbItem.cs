﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Breadcrumbs
{
    public class FeedbackBreadcrumbItem : BreadcrumbItem
    {
        public FeedbackBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = MeetingResources.MeetingFeedbackPageName;
        }
    }
}