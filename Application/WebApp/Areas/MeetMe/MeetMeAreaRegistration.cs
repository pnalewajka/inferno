﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.MeetMe
{
    public class MeetMeAreaRegistration : AreaRegistration
    {
        public override string AreaName => "MeetMe";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MeetMe_default2",
                "MeetMe/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}