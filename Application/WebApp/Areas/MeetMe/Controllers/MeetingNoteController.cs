﻿using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewMeetingNotes)]
    public class MeetingNoteController : SubCardIndexController<MeetingNoteViewModel, MeetingNoteDto, MeetingDetailsController>
    {
        private readonly IMeetingService _meetingService;
        private readonly IMeetingNoteCardIndexService _cardIndexDataService;

        public MeetingNoteController(IMeetingNoteCardIndexService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IMeetingService meetingService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _meetingService = meetingService;

            CardIndex.Settings.Title = MeetingResources.MeetingNotesPageName;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddMeetingNotes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditMeetingNotes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteMeetingNotes;
            CardIndex.Settings.DeleteButton.Confirmation.Message = MeetingResources.DeleteNoteConfirmation;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewMeetingNotes;
        }

        public override ActionResult List(GridParameters parameters)
        {
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Predicate = $"row.canEdit",
                Mode = AvailabilityMode.SingleRowSelected
            };

            return base.List(parameters);
        }

        protected override string GetParentUrl(object context)
        {
            var parentIdContext = context as ParentIdContext;

            if (parentIdContext == null)
            {
                return base.GetParentUrl(context);
            }

            var parrentViewModel = GetContextViewModel() as MeetingDetailsViewModel;

            CardIndex.Settings.AddButton.Visibility = CardIndex.Settings.EditButton.Visibility
                                                    = parrentViewModel.CanAddNote
                                                            ? CardIndex.Settings.AddButton.Visibility : CommandButtonVisibility.None;

            var employeeId = _meetingService.GetEmployeeIdByMeetingId(parentIdContext.ParentId);

            return Url.Action
            (
                nameof(MeetingDetailsController.List),
                RoutingHelper.GetControllerName<MeetingDetailsController>(),
                new RouteValueDictionary
                {
                    { RoutingHelper.AreaParameterName, RoutingHelper.GetAreaName<MeetingDetailsController>() },
                    { RoutingHelper.ParentIdParameterName, employeeId }
                }
            );
        }
    }
}