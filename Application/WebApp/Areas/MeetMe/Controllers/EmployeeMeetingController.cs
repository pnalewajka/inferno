﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Helpers;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployeeMeetings)]
    public class EmployeeMeetingController : CardIndexController<EmployeeMeetingViewModel, EmployeeMeetingDto>
    {
        private const int ExportPageSize = 1000;
        private readonly IPrincipalProvider _principalProvider;

        public EmployeeMeetingController(IEmployeeMeetingCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _principalProvider = baseControllerDependencies.PrincipalProvider;

            CardIndex.Settings.Title = MeetingResources.MeetMePageName;

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.FilterGroups.AddRange(GetFilters());
            CardIndex.Settings.ToolbarButtons.Add(GetMeetingsCommandButton());
            CardIndex.Settings.ToolbarButtons.Add(GetFeedbacksCommandButton());
            CardIndex.Settings.ToolbarButtons.Add(GetMeetingsOverviewCommandButton());
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportMeetings);
            Layout.Scripts.Add("~/Areas/MeetMe/Scripts/MeetMe.js");
        }

        private IEnumerable<FilterGroup> GetFilters()
        {
            yield return new FilterGroup
            {
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                            {
                                new Filter { Code = FilterCodes.MyEmployees, DisplayName = EmployeeResources.MyEmployees },
                                new Filter { Code = FilterCodes.MyDirectEmployees, DisplayName = EmployeeResources.MyDirectEmployees },
                                new Filter { Code = FilterCodes.AllEmployees, DisplayName = EmployeeResources.AllEmployees }
                            }
            };

            yield return new FilterGroup
            {
                DisplayName = EmployeeResources.EmployeeStatus,
                Type = FilterGroupType.RadioGroup,
                Filters = GetStatusFilters().ToList()
            };


            yield return new FilterGroup()
            {
                DisplayName = AllocationResources.OrganizationTitle,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                              {
                                  new ParametricFilter<OrgUnitFilterViewModel>(
                                      FilterCodes.OrgUnitsFilter,
                                      AllocationResources.EmployeeOrgUnitFilterName)
                              }
            };

            yield return new FilterGroup()
            {
                DisplayName = EmployeeResources.EmployeesTitle,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                              {
                                  new ParametricFilter<EmployeeLineManagerFilterViewModel>(
                                      Business.Allocation.Consts.FilterCodes.LineManagerFilter,
                                      EmployeeResources.EmployeeLineManagerFilterName)
                              }
            };
        }

        private IEnumerable<Filter> GetStatusFilters()
        {
            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
            {
                yield return new Filter { Code = FilterCodes.Working, DisplayName = EmployeeResources.Working };
                yield return new Filter { Code = FilterCodes.Terminated, DisplayName = EmployeeResources.Terminated };
            }
            else
            {
                yield return new Filter { Code = FilterCodes.Working, DisplayName = EmployeeResources.Working };
            }
        }

        private CommandButton GetMeetingsCommandButton()
        {
            return new ToolbarButton
            {
                Text = MeetingResources.MeetingsButtonLabel,
                OnClickAction = Url.UriActionGet(nameof(MeetingDetailsController.List), RoutingHelper.GetControllerName<MeetingDetailsController>(), KnownParameter.ParentId),
                Icon = FontAwesome.List,
                RequiredRole = SecurityRoleType.CanViewMeetingDetails,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };
        }

        private CommandButton GetFeedbacksCommandButton()
        {
            return new ToolbarButton
            {
                Text = MeetingResources.FeedbacksButtonLabel,
                OnClickAction = Url.UriActionGet(nameof(FeedbackController.List), RoutingHelper.GetControllerName<FeedbackController>(), KnownParameter.ParentId),
                Icon = FontAwesome.List,
                RequiredRole = SecurityRoleType.CanViewFeedbacks,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };
        }

        private CommandButton GetMeetingsOverviewCommandButton()
        {
            var performAction = Url.Action(nameof(EmployeeReviewProcessOverviewController.Index), RoutingHelper.GetControllerName<EmployeeReviewProcessOverviewController>());

            return new ToolbarButton
            {
                Text = MeetingResources.MeetingsOverviewButtonLabel,
                OnClickAction = new JavaScriptCallAction($"MeetMe.showTimeLineDialog('{performAction}', grid);"),
                Icon = FontAwesome.List,
                RequiredRole = SecurityRoleType.CanViewMeetingsOverview,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };
        }

        public override FilePathResult ExportToExcel(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = ExportPageSize;

            return base.ExportToExcel(parameters);
        }
    }
}