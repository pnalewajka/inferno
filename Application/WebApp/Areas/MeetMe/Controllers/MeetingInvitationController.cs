﻿using System.Web.Mvc;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanSendMeetingInvitation)]
    public class MeetingInvitationController : CardIndexController<MeetingInvitationViewModel, MeetingInvitationDto>
    {
        private IBaseControllerDependencies _baseControllerDependencies;
        private readonly IMeetingInvitationCardIndexService _meetingInvitationCardIndexService;

        public MeetingInvitationController(
            IBaseControllerDependencies baseControllerDependencies,
            IMeetingInvitationCardIndexService meetingInvitationCardIndexService)
            : base(meetingInvitationCardIndexService, baseControllerDependencies)
        {
            _baseControllerDependencies = baseControllerDependencies;
            _meetingInvitationCardIndexService = meetingInvitationCardIndexService;
        }

        public override ActionResult GetEditDialog(long id)
        {
            var dialogViewModel = (PartialViewResult)base.GetEditDialog(id);
            var dialogConfiguration = (DialogViewModel)dialogViewModel.Model;

            dialogConfiguration.Buttons.Clear();

            dialogConfiguration.Title = MeetingResources.SendInvitationLabel;

            dialogConfiguration.Buttons.Add(new DialogButton
            {
                Text = MeetingResources.Send,
                Tag = DialogHelper.SubmitButtonTag,
                IsDismissButton = true,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter)
            });
            dialogConfiguration.Buttons.Add(DialogHelper.GetCancelButton());

            return dialogViewModel;
        }

        public override ActionResult HandleEditDialog(MeetingInvitationViewModel viewModelRecord)
        {
            var mapping = _baseControllerDependencies.ClassMappingFactory.CreateMapping<MeetingInvitationViewModel, MeetingInvitationDto>();
            _meetingInvitationCardIndexService.SendInvitation(mapping.CreateFromSource(viewModelRecord));

            return base.HandleEditDialog(viewModelRecord);
        }
    }
}