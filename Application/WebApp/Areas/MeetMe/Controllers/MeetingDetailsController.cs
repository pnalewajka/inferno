﻿using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Helpers;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Areas.Notifications.Controllers;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewMeetingDetails)]
    public class MeetingDetailsController : SubCardIndexController<MeetingDetailsViewModel, MeetingDetailsDto, EmployeeMeetingController>
    {
        private readonly IMeetingDetailsCardIndexService _meetMeMeetingDataService;

        public MeetingDetailsController(IMeetingDetailsCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _meetMeMeetingDataService = cardIndexDataService;

            CardIndex.Settings.Title = MeetingResources.MeetingDetailsPageName;

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewEmployeeMeetings;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddMeetingDetails;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditMeetingDetails;
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Predicate = $"row.canBeEdited",
                Mode = AvailabilityMode.SingleRowSelected
            };
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteMeetingDetails;
            CardIndex.Settings.RowButtons.Add(GetMeetingsNotesButton());

            Layout.Resources.AddFrom<MeetingResources>();
            Layout.Scripts.Add("~/Areas/MeetMe/Scripts/MeetingDetails.js");
            CardIndex.Settings.ToolbarButtons.Add(FinishMeetingButton());
            CardIndex.Settings.ToolbarButtons.Add(AddNoteButton());
            CardIndex.Settings.ToolbarButtons.Add(SendInvitationButton());
            Layout.Css.Add("~/Areas/MeetMe/Content/meetMe.css");
            CardIndex.Settings.AllowMultipleRowSelection = false;

            SetDoubleClickAction();
        }

        private void SetDoubleClickAction()
        {
            var doubleClickUrl = Url.Action(nameof(MeetingNoteController.List), RoutingHelper.GetControllerName<MeetingNoteController>());

            CardIndex.Settings.OnRowDoubleClickedJavaScript = $"Forms.redirect('{doubleClickUrl}?parent-id=' + rowData.id)";
        }

        private CommandButton AddNoteButton()
        {
            return new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanAddMeetingNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.canAddNote",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Text = MeetingResources.AddNoteLabel,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(MeetingNoteController.GetAddDialog), RoutingHelper.GetControllerName<MeetingNoteController>(), KnownParameter.ParentId)),
            };
        }

        private CommandButton FinishMeetingButton()
        {
            return new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanEditMeetingDetails,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.meetingStatus !== {(int)MeetingStatus.Done}",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Text = MeetingResources.FinishMeetingButtonLabel,
                OnClickAction = new JavaScriptCallAction($"MeetingDetails.finishMeeting(grid);")
            };
        }

        private CommandButton GetMeetingsNotesButton()
        {
            return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewMeetingNotes,
                Text = MeetingResources.MeetingNoteActionButtonLable,
                OnClickAction = Url.UriActionGet(nameof(MeetingNoteController.List), RoutingHelper.GetControllerName<MeetingNoteController>(), KnownParameter.ParentId)
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanEditMeetingDetails)]
        public ActionResult FinishMeeting(long meetingIds, GridParameters parameters)
        {
            AddAlerts(_meetMeMeetingDataService.FinishMeeting(meetingIds));

            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override string GetParentUrl(object context)
        {
            return Url.Action(nameof(EmployeeMeetingController.List), RoutingHelper.GetControllerName<EmployeeMeetingController>(), new { filter = FilterCodes.Working });
        }

        private CommandButton SendInvitationButton()
        {
            return new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanSendMeetingInvitation,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.meetingStatus !== {(int)MeetingStatus.Done}",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Text = MeetingResources.SendInvitationLabel,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(MeetingInvitationController.GetEditDialog),
                RoutingHelper.GetControllerName<MeetingInvitationController>(),
                KnownParameter.SelectedId)),
            };
        }
    }
}