﻿using System.Web.Mvc;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Controllers
{
    public class EmployeeReviewProcessOverviewController : AtomicController
    {
        private readonly IEmployeeReviewProcessOverview _meetingsOverviewService;
        private readonly IClassMapping<EmployeeReviewProcessOverviewDto, EmployeeReviewProcessOverviewViewModel> _employeeReviewProcessOverviewMapping;

        public EmployeeReviewProcessOverviewController(
            IBaseControllerDependencies baseDependencies,
            IEmployeeReviewProcessOverview meetingsOverviewService,
            IClassMapping<EmployeeReviewProcessOverviewDto, EmployeeReviewProcessOverviewViewModel> employeeReviewProcessOverviewMapping)
            : base(baseDependencies)
        {
            _employeeReviewProcessOverviewMapping = employeeReviewProcessOverviewMapping;
            _meetingsOverviewService = meetingsOverviewService;

            Layout.PageTitle = MeetingOverviewResources.MeetMeOverviewPageTitle;
            Layout.Css.Add("~/Areas/MeetMe/Content/timeLine.css");
        }

        [HttpGet]
        [BreadcrumbBar("EmployeeReviewProcessOverview")]
        [AtomicAuthorize(SecurityRoleType.CanViewMeetingsOverview)]
        public ActionResult Index(long employeeId)
        {
            var viewModel = GetEmployeeTimelineEventViewModels(employeeId);

            return View(viewModel);
        }

        [HttpGet]
        [BreadcrumbBar("EmployeeReviewProcessOverview")]
        [AtomicAuthorize(SecurityRoleType.CanViewMyMeetingsOverview)]
        public ActionResult MyOverview()
        {
            var employeeId = GetCurrentPrincipal().EmployeeId;
            var viewModel = GetEmployeeTimelineEventViewModels(employeeId);

            return View(nameof(Index), viewModel);
        }

        private EmployeeReviewProcessOverviewViewModel GetEmployeeTimelineEventViewModels(long employeeId)
        {
            var employeeReviewProcessOverviewDto = new EmployeeReviewProcessOverviewDto();

            var employeeOverviewRestrictions = _meetingsOverviewService.CheckEmployeeOverviewRestrictions(employeeId);

            if (employeeOverviewRestrictions.IsSuccessful)
            {
                employeeReviewProcessOverviewDto = _meetingsOverviewService.GetEmployeeReviewProcessOverview(employeeId);
            }
            else
            {
                AddAlerts(employeeOverviewRestrictions.Alerts);
            }

            return !string.IsNullOrEmpty(employeeReviewProcessOverviewDto.EmployeeFullName)
                ? _employeeReviewProcessOverviewMapping.CreateFromSource(employeeReviewProcessOverviewDto)
                : new EmployeeReviewProcessOverviewViewModel();
        }
    }
}