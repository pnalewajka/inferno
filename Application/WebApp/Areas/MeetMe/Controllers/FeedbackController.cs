﻿using System.Web.Mvc;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.MeetMe.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewFeedbacks)]
    public class FeedbackController : SubCardIndexController<FeedbackViewModel, FeedbackDto, EmployeeMeetingController>
    {
        public FeedbackController(IFeedbackCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = MeetingResources.MeetingFeedbackPageName;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddFeedback;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewFeedbacks;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditFeedback;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteFeedback;

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<FeedbackViewModel>(a => a.ReceiverId).Visibility = Presentation.Renderers.Bootstrap.Enums.ControlVisibility.Hide;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<FeedbackViewModel>(a => a.ReceiverId).Visibility = Presentation.Renderers.Bootstrap.Enums.ControlVisibility.Hide;
            };

            CardIndex.Settings.ViewFormCustomization = form =>
            {
                form.GetControlByName<FeedbackViewModel>(a => a.ReceiverId).Visibility = Presentation.Renderers.Bootstrap.Enums.ControlVisibility.Hide;
            };

            CardIndex.Settings.DefaultNewRecord = DefaultNewRecord;
            Layout.Parameters.AddParameter<string>(ParameterKeys.MeetMeAddFeedbackEmptyContextCancelUrl);
            Layout.Scripts.Add("~/Areas/MeetMe/Scripts/Feedback.js");
        }

        private FeedbackViewModel DefaultNewRecord()
        {
            return new FeedbackViewModel
            {
                ReceiverId = Context.ParentId
            };
        }

        public override object GetContextViewModel()
        {
            if (Context.ParentId == 0)
            {
                return null;
            }

            return base.GetContextViewModel();
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/CardIndexAdd")]
        [AtomicAuthorize(SecurityRoleType.CanAddFeedback)]
        public override ActionResult Add()
        {
            if (Context.ParentId == 0)
            {
                HandleEmptyContext();
            }

            return base.Add();
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/CardIndexAdd")]
        [AtomicAuthorize(SecurityRoleType.CanAddFeedback)]
        public override ActionResult Add(FeedbackViewModel feedbackViewModel)
        {
            if (Context.ParentId == 0)
            {
                Context.ParentId = feedbackViewModel.ReceiverId;
                HandleEmptyContext();
            }
            
            return base.Add(feedbackViewModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewFeedbacks)]
        public override ActionResult View(long id)
        {
            return base.View(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewFeedbacks)]
        public override ActionResult List(GridParameters parameters)
        {
            return base.List(parameters);
        }

        private void HandleEmptyContext()
        {
            CardIndex.Settings.AddViewName = "~/Areas/MeetMe/Views/Feedback/Add.cshtml";
            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<FeedbackViewModel>(a => a.ReceiverId).Visibility = Presentation.Renderers.Bootstrap.Enums.ControlVisibility.Show;
            };
        }
    }
}