﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Renders
{
    public class NoteList : ICellContent
    {
        public string CssClass => string.Empty;

        public IEnumerable<MeetingNoteViewModel> MeetingNotes { get; private set; }

        public NoteList(IEnumerable<MeetingNoteViewModel> meetingNotes)
        {
            MeetingNotes = meetingNotes;
        }
    }
}