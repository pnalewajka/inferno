﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;

namespace Smt.Atomic.WebApp.Areas.MeetMe.Renders.Attributes
{
    public class NoteListAttribute : CellContentAttribute
    {
        protected override string TemplateCode => nameof(NoteList);

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            var meetingNotes = cellContentConverterContext.PropertyValue as IEnumerable<MeetingNoteViewModel>;

            return new NoteList(meetingNotes);
        }
    }
}