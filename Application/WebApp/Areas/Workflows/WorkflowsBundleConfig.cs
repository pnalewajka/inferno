﻿using System;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Workflows
{
    public class WorkflowsBundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            var bundle = new ScriptBundle("~/scripts/workflows/calculations")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder(false)
            };

            bundle.Include("~/Areas/Workflows/Scripts/CalculatorResults.js");
            bundle.IncludeDirectory("~/Areas/Workflows/Scripts/CalculationResults", "*.js", true);

            bundles.Add(bundle);
        }
    }
}
