﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.BonusRequest)]
    public class BonusRequestController
        : RequestGenericController
    {
        private readonly IRequestService _bonusRequestService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IProjectService _projectService;
        private readonly ICompanyService _companyService;
        private readonly ICurrencyService _currencyService;

        public BonusRequestController(
            BaseRequestControllerDependencies dependencies,
            ISystemParameterService systemParameterService,
            IProjectService projectService,
            ICompanyService companyService,
            ICurrencyService currencyService)
            : base(dependencies)
        {
            _bonusRequestService = dependencies.RequestServiceResolver.GetByServiceType(RequestType.BonusRequest);
            _systemParameterService = systemParameterService;
            _projectService = projectService;
            _companyService = companyService;
            _currencyService = currencyService;
        }

        protected override IList<ClientSideOperation<RequestViewModel>> RefreshFormAfterChange(RequestViewModel viewModel)
        {
            var operations = base.RefreshFormAfterChange(viewModel);
            var bonusRequest = (BonusRequestViewModel)viewModel.RequestObjectViewModel;

            // suggest absence project for holiday bonuses
            if (bonusRequest.Type == BonusType.Holiday && bonusRequest.ProjectId == BusinessLogicHelper.NonExistingId)
            {
                var absenceProjectApn = _systemParameterService.GetParameter<string>(ParameterKeys.HolidayBonusProjectApn);
                var bonusProject = _projectService.GetProjectByApn(absenceProjectApn);

                operations.Add(new ClientSideValuePickerOperation<RequestViewModel>(
                    InnerFieldExpression(e => e.ProjectId), new ValuePickerResponseViewModel
                    {
                        ElementId = bonusProject.Id,
                        ElementDescription = bonusProject.ClientProjectName,
                    }));
            }

            if (bonusRequest.EmployeeId != BusinessLogicHelper.NonExistingId)
            {
                var employeeCompany = _companyService.GetCompanyOrDefaultByEmployeeId(bonusRequest.EmployeeId);
                var companyCurrency = _currencyService.GetCurrencyById(employeeCompany.DefaultAdvancedPaymentCurrencyId);

                operations.Add(new ClientSideValuePickerOperation<RequestViewModel>(
                    InnerFieldExpression(e => e.CurrencyId), new ValuePickerResponseViewModel
                    {
                        ElementId = companyCurrency.Id,
                        ElementDescription = $"{companyCurrency.Symbol} ({companyCurrency.IsoCode})"
                    }));
            }

            return operations;
        }

        protected override object CreateDefaultNewRecord()
        {
            return CreateDefaultNewRecord(RequestType.BonusRequest, _bonusRequestService);
        }

        private Expression<Func<RequestViewModel, object>> InnerFieldExpression(
            Expression<Func<BonusRequestViewModel, object>> fieldExpression)
        {
            var innerFieldSelector = new BusinessLogic<BonusRequestViewModel, object>(fieldExpression);
            var baseFieldSelector = new BusinessLogic<RequestViewModel, object>(
                r => innerFieldSelector.Call((BonusRequestViewModel)r.RequestObjectViewModel));

            return baseFieldSelector.AsExpression();
        }
    }
}