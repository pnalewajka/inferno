﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.BusinessTripSettlementRequest)]
    public class BusinessTripSettlementRequestController
        : RequestGenericController
    {
        private readonly IRequestService _businessTripRequestService;
        private readonly IBusinessTripService _businessTripService;
        private readonly IBusinessTripSettlementService _settlementService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;
        private readonly IClassMapping<BusinessTripSettlementRequestMealDto, BusinessTripSettlementRequestMealViewModel> _mealDtoToViewModelMapping;
        private readonly IClassMapping<BusinessTripSettlementRequestViewModel, BusinessTripSettlementRequestDto> _requestViewModelToDtoMapping;

        public BusinessTripSettlementRequestController(
            BaseRequestControllerDependencies baseRequestDependencies,
            IBusinessTripService businessTripService,
            IBusinessTripParticipantsService businessTripParticipantsService,
            IBusinessTripSettlementService settlementService)
            : base(baseRequestDependencies)
        {
            _businessTripRequestService = baseRequestDependencies.RequestServiceResolver.GetByServiceType(RequestType.BusinessTripSettlementRequest);
            _businessTripService = businessTripService;
            _businessTripParticipantsService = businessTripParticipantsService;
            _settlementService = settlementService;
            _mealDtoToViewModelMapping = baseRequestDependencies.ClassMappingFactory.CreateMapping<BusinessTripSettlementRequestMealDto, BusinessTripSettlementRequestMealViewModel>();
            _requestViewModelToDtoMapping = baseRequestDependencies.ClassMappingFactory.CreateMapping<BusinessTripSettlementRequestViewModel, BusinessTripSettlementRequestDto>();

            Layout.Resources.AddFrom<BusinessTripSettlementRequestResources>();

            Layout.Scripts.Add("~/scripts/react");

            Layout.Css.Add("~/Areas/Workflows/Content/CalculatorResults.css");
            Layout.Css.Add("~/Areas/BusinessTrips/Content/SettlementRequest.css");

            Layout.Scripts.Add("~/scripts/workflows/calculations");
            Layout.Scripts.Add("~/Areas/Workflows/Scripts/BusinessTripSettlementRequest.js");
            Layout.Scripts.Add("~/Areas/BusinessTrips/Scripts/MealsTable.js");

            CardIndex.Settings.EditFormCustomization += FormCustomization;
            CardIndex.Settings.AddFormCustomization += FormCustomization;
            CardIndex.Settings.ViewFormCustomization += FormCustomization;

            CardIndex.Settings.ShowValidationAlerts = true;

            EnableRequestDocumentControl();
        }

        private void FormCustomization(FormRenderingModel form)
        {
            var viewModel = form.GetViewModel<RequestViewModel>();
            var settlementViewModel = (BusinessTripSettlementRequestViewModel)viewModel.RequestObjectViewModel;
            var participantId = settlementViewModel.BusinessTripParticipantId == default(long) ? (long?)null
                : settlementViewModel.BusinessTripParticipantId;

            if (participantId.HasValue)
            {
                GetRequestControlByName(form, r => r.BusinessTripId).IsReadOnly = true;
                GetRequestControlByName(form, r => r.BusinessTripParticipantId).IsReadOnly = true;

                var companyCostsControl = GetRequestControlByName(form, r => r.CompanyCosts) as RepeaterRenderingModel;

                if (companyCostsControl != null)
                {
                    var cardHolderControl = (ValuePickerRenderingModel)companyCostsControl.TemplateItem.Controls.Single(c =>
                        c.Name.EndsWith(nameof(BusinessTripSettlementRequestCompanyCostViewModel.CardHolderEmployeeId)));

                    cardHolderControl.Items.Add(new ListItem
                    {
                        DisplayName = settlementViewModel.DefaultCardHolderEmployeeName,
                        Id = settlementViewModel.DefaultCardHolderEmployeeId
                    });
                }
            }

            var settings = _settlementService.GetSettlementConfiguration(participantId);
            var isInternational = participantId.HasValue ? _businessTripService.IsParticipantBusinessTripInternational(participantId.Value) : false;
            var isDestinationTypeRequired = participantId.HasValue ? _businessTripParticipantsService.ShouldParticipantSpecifyDestinationType(participantId.Value) : false;

            foreach (var settlementOption in GetSettlementOperations(settlementViewModel, settings, isInternational || isDestinationTypeRequired))
            {
                var control = form.GetControlByName(settlementOption.FieldExpression);

                if (settlementOption is ClientSideVisibilityOperation<RequestViewModel>)
                {
                    var controlVisibility = (ClientSideVisibilityOperation<RequestViewModel>)settlementOption;

                    control.Visibility = controlVisibility.IsVisible ? ControlVisibility.Show : ControlVisibility.Hide;
                }
            }

            var requireDepartureArrival = settings.HasFlag(SettlementSectionsFeatureType.DepartureArrival);

            GetRequestControlByName(form, r => r.DepartureDateTime).IsRequired = requireDepartureArrival;
            GetRequestControlByName(form, r => r.ArrivalDateTime).IsRequired = requireDepartureArrival;
            GetRequestControlByName(form, r => r.CalculationResult).CanUseInSummary = true;

            var abroadTimeEntriesControl = GetRequestControlByName(form, r => r.AbroadTimeEntries) as RepeaterRenderingModel;

            if (abroadTimeEntriesControl != null)
            {
                var destinationTypeControl = abroadTimeEntriesControl.TemplateItem.Controls.Single(c =>
                    c.Name.EndsWith(nameof(BusinessTripSettlementRequestAbroadTimeEntryViewModel.DestinationType)));
           
                destinationTypeControl.IsRequired = isDestinationTypeRequired;
                destinationTypeControl.Visibility = isDestinationTypeRequired ? ControlVisibility.Show : ControlVisibility.Remove;
                
                if (isDestinationTypeRequired)
                {
                    var borderCrossingDateControl = abroadTimeEntriesControl.TemplateItem.Controls.Single(c =>
                        c.Name.EndsWith(nameof(BusinessTripSettlementRequestAbroadTimeEntryViewModel.DepartureDateTime)));

                    borderCrossingDateControl.Label = BusinessTripSettlementRequestResources.EntryArrivalDate;
                }

                foreach (var item in abroadTimeEntriesControl.Items)
                {
                    destinationTypeControl = item.Controls.Single(c => c.Name.EndsWith(nameof(BusinessTripSettlementRequestAbroadTimeEntryViewModel.DestinationType)));

                    destinationTypeControl.IsRequired = isDestinationTypeRequired;
                    destinationTypeControl.Visibility = isDestinationTypeRequired ? ControlVisibility.Show : ControlVisibility.Remove;

                    if (isDestinationTypeRequired)
                    {
                        var borderCrossingDateControl = item.Controls.Single(c => c.Name.EndsWith(nameof(BusinessTripSettlementRequestAbroadTimeEntryViewModel.DepartureDateTime)));

                        borderCrossingDateControl.Label = BusinessTripSettlementRequestResources.EntryArrivalDate;
                    }
                }
            }
        }

        protected override bool ShowSubmitButton(RequestViewModel model)
        {
            return base.ShowSubmitButton(model) || model.Status == RequestStatus.Rejected;
        }

        protected override object CreateDefaultNewRecord()
        {
            return CreateDefaultNewRecord(RequestType.BusinessTripSettlementRequest, _businessTripRequestService);
        }

        [HttpPost]
        public JsonNetResult UpdateMealsTable(RequestViewModel viewModel)
        {
            var meals = GetMeals(viewModel);

            var response = new OnValueChangeServerResponse<BusinessTripSettlementRequestViewModel>
            {
                ServerResultData = meals,
            };

            return response.ToJsonNetResult();
        }

        private List<BusinessTripSettlementRequestMealViewModel> GetMeals(RequestViewModel viewModel)
        {
            var businessTripSettlementRequestViewModel = (BusinessTripSettlementRequestViewModel)viewModel.RequestObjectViewModel;
            var dto = _requestViewModelToDtoMapping.CreateFromSource(businessTripSettlementRequestViewModel);

            var meals = _settlementService.GetMeals(dto)
                .Select(_mealDtoToViewModelMapping.CreateFromSource).ToList();

            PreserveMealValues(businessTripSettlementRequestViewModel.Meals, meals);

            return meals;
        }

        private IEnumerable<ClientSideOperation<RequestViewModel>> GetSettlementOperations(
            BusinessTripSettlementRequestViewModel settlementRequest,
            SettlementSectionsFeatureType settings,
            bool abroadTimeEntriesRequired)
        {
            var departureArrivalAllowed = settings.HasFlag(SettlementSectionsFeatureType.DepartureArrival);
            var ownCostsAllowed = settings.HasFlag(SettlementSectionsFeatureType.OwnCosts);
            var companyCostsAllowed = settings.HasFlag(SettlementSectionsFeatureType.CompanyCosts);
            var additionalAdvancePaymentsAllowed = settings.HasFlag(SettlementSectionsFeatureType.AdditionalAdvancePayments);
            var privateVehicleMileageAllowed = settings.HasFlag(SettlementSectionsFeatureType.PrivateVehicleMileage);
            var simplifiedPrivateVehicleMileageAllowed = settings.HasFlag(SettlementSectionsFeatureType.SimplifiedPrivateVehicleMileage);
            var hoursWorkedForCustomerAllowed = settings.HasFlag(SettlementSectionsFeatureType.HoursWorkedForCustomer);
            var mealsAllowed = settings.HasFlag(SettlementSectionsFeatureType.Meals);

            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).DepartureDateTime, departureArrivalAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).ArrivalDateTime, departureArrivalAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).AbroadTimeEntries, departureArrivalAllowed && abroadTimeEntriesRequired);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).OwnCosts, ownCostsAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).CompanyCosts, companyCostsAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).AdditionalAdvancePayments, additionalAdvancePaymentsAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).PrivateVehicleMileage, privateVehicleMileageAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).SimplifiedPrivateVehicleMileage, simplifiedPrivateVehicleMileageAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).HoursWorkedForClient, hoursWorkedForCustomerAllowed);
            yield return new ClientSideVisibilityOperation<RequestViewModel>(v => (v.RequestObjectViewModel as BusinessTripSettlementRequestViewModel).Meals, mealsAllowed);
        }

        private void PreserveMealValues(
            ICollection<BusinessTripSettlementRequestMealViewModel> clientMeals,
            ICollection<BusinessTripSettlementRequestMealViewModel> newMeals)
        {
            foreach (var clientMeal in clientMeals)
            {
                var newMeal = newMeals.SingleOrDefault(
                    m =>
                        (m.CountryId.HasValue && m.CountryId == clientMeal.CountryId)
                        || (m.Day.HasValue && m.Day == clientMeal.Day));

                if (newMeal != null)
                {
                    newMeal.BreakfastCount = clientMeal.BreakfastCount;
                    newMeal.LunchCount = clientMeal.LunchCount;
                    newMeal.DinnerCount = clientMeal.DinnerCount;
                }
            }
        }

        private ControlRenderingModel GetRequestControlByName(
            FormRenderingModel form,
            Expression<Func<BusinessTripSettlementRequestViewModel, object>> selector)
        {
            return base.GetRequestControlByName(form, selector);
        }
    }
}