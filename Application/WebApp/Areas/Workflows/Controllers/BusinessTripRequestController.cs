﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.BusinessTripRequest)]
    public class BusinessTripRequestController
        : RequestGenericController
    {
        private readonly IRequestService _businessTripRequestService;
        private readonly ICityService _cityService;
        private readonly IEmployeeService _employeeService;
        private readonly ILocationService _locationService;

        public BusinessTripRequestController(
            BaseRequestControllerDependencies baseRequestDependencies,
            ICityService cityService,
            IEmployeeService employeeService,
            ILocationService locationService)
            : base(baseRequestDependencies)
        {
            _businessTripRequestService = baseRequestDependencies.RequestServiceResolver.GetByServiceType(RequestType.BusinessTripRequest);
            _cityService = cityService;
            _employeeService = employeeService;
            _locationService = locationService;

            Layout.Scripts.Add("~/Areas/Workflows/Scripts/BusinessTripRequest.js");

            CardIndex.Settings.ViewFormCustomization += (FormRenderingModel form) =>
            {
                var model = form.GetViewModel<RequestViewModel>();
                var request = (BusinessTripRequestViewModel)model.RequestObjectViewModel;

                GetRequestControlByName(form, e => e.AreTravelArrangementsNeeded).Label =
                    BusinessTripRequestResources.AreTravelArrangementsNeededShort;

                if (!request.AreTravelArrangementsNeeded)
                {
                    GetRequestControlByName(form, e => e.TransportationPreferences).Visibility = ControlVisibility.Hide;
                    GetRequestControlByName(form, e => e.AccommodationPreferences).Visibility = ControlVisibility.Hide;
                    GetRequestControlByName(form, e => e.LuggageDeclarations).Visibility = ControlVisibility.Hide;
                    GetRequestControlByName(form, e => e.DepartureCityTaxiVouchers).Visibility = ControlVisibility.Hide;
                    GetRequestControlByName(form, e => e.DestinationCityTaxiVouchers).Visibility = ControlVisibility.Hide;
                    GetRequestControlByName(form, e => e.ItineraryDetails).Visibility = ControlVisibility.Hide;
                }

                if (!request.TransportationPreferences.HasFlag(MeansOfTransport.Taxi))
                {
                    GetRequestControlByName(form, e => e.DepartureCityTaxiVouchers).Visibility = ControlVisibility.Hide;
                    GetRequestControlByName(form, e => e.DestinationCityTaxiVouchers).Visibility = ControlVisibility.Hide;
                }

                if (!request.TransportationPreferences.HasFlag(MeansOfTransport.Plane))
                {
                    GetRequestControlByName(form, e => e.LuggageDeclarations).Visibility = ControlVisibility.Hide;
                }
            };
        }

        protected override object CreateDefaultNewRecord()
        {
            return CreateDefaultNewRecord(RequestType.BusinessTripRequest, _businessTripRequestService);
        }

        [HttpPost]
        public JsonNetResult UpdateEndedOn(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var businessTripRequest = (BusinessTripRequestViewModel)viewModel.RequestObjectViewModel;

            if (businessTripRequest.EndedOn < businessTripRequest.StartedOn)
            {
                response.Operations.Add(new ClientSideValueChangeOperation<RequestViewModel>(
                    v => ((BusinessTripRequestViewModel)v.RequestObjectViewModel).EndedOn,
                    businessTripRequest.StartedOn.ToShortDateString()));
            }

            return response.ToJsonNetResult();
        }

        [HttpPost]
        public JsonNetResult UpdateLuggageAvailability(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var businessTripRequestViewModel = (BusinessTripRequestViewModel)viewModel.RequestObjectViewModel;

            foreach (var operation in GetLuggageOperations(businessTripRequestViewModel))
            {
                response.Operations.Add(operation);
            }

            return response.ToJsonNetResult();
        }

        [HttpPost]
        public JsonNetResult UpdateTaxiVoucherAvailability(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var businessTripRequestViewModel = (BusinessTripRequestViewModel)viewModel.RequestObjectViewModel;

            foreach (var operation in GetTaxiVoucherOperations(businessTripRequestViewModel))
            {
                response.Operations.Add(operation);
            }

            return response.ToJsonNetResult();
        }

        [HttpPost]
        public JsonNetResult UpdateLuggageAndTaxiVoucherAvailability(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var businessTripRequestViewModel = (BusinessTripRequestViewModel)viewModel.RequestObjectViewModel;

            foreach (var operation in GetLuggageOperations(businessTripRequestViewModel))
            {
                response.Operations.Add(operation);
            }

            foreach (var operation in GetTaxiVoucherOperations(businessTripRequestViewModel))
            {
                response.Operations.Add(operation);
            }

            return response.ToJsonNetResult();
        }

        [HttpPost]
        public JsonNetResult UpdateDepartureCities(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var businessTripRequestViewModel = (BusinessTripRequestViewModel)viewModel.RequestObjectViewModel;

            foreach (var operation in GetUpdateDeparturesOperations(viewModel))
            {
                response.Operations.Add(operation);
            }

            return response.ToJsonNetResult();
        }

        private IEnumerable<ClientSideOperation<RequestViewModel>> GetUpdateDeparturesOperations(RequestViewModel viewModel)
        {
            var locationIds = _employeeService.GetEmployeesById(((BusinessTripRequestViewModel)viewModel.RequestObjectViewModel).ParticipantEmployeeIds)
                .Where(e => e.LocationId.HasValue)
                .Select(e => e.LocationId.Value)
                .ToArray();
            var cityIds = _locationService.GetLocationsOrDefaultByIds(locationIds)
                .Where(l => l.CityId.HasValue)
                .Select(l => l.CityId.Value).
                ToArray();
            var valuePickerResponseViewModels = _cityService.GetCitiesByIds(cityIds)
                .Distinct()
                .Select(c => new ValuePickerResponseViewModel
                {
                    ElementId = c.Id,
                    ElementDescription = $"{c.Name} ({c.CountryName})"
                })
                .ToArray();

            yield return new ClientSideMultiValuePickerOperation<RequestViewModel>(
                    v => ((BusinessTripRequestViewModel)v.RequestObjectViewModel).DepartureCityIds,
                    valuePickerResponseViewModels
                );
        }

        private IEnumerable<ClientSideOperation<RequestViewModel>> GetLuggageOperations(BusinessTripRequestViewModel viewModel)
        {
            yield return new ClientSideVisibilityOperation<RequestViewModel>(
                v => ((BusinessTripRequestViewModel)v.RequestObjectViewModel).LuggageDeclarations,
                viewModel.TransportationPreferences.HasFlag(MeansOfTransport.Plane)
            );
        }

        private IEnumerable<ClientSideOperation<RequestViewModel>> GetTaxiVoucherOperations(BusinessTripRequestViewModel viewModel)
        {
            yield return new ClientSideVisibilityOperation<RequestViewModel>(
                    v => ((BusinessTripRequestViewModel)v.RequestObjectViewModel).DepartureCityTaxiVouchers,
                        viewModel.TransportationPreferences.HasFlag(MeansOfTransport.Taxi)
                        && IsVoucherServiceAvaliable(viewModel.DepartureCityIds));

            yield return new ClientSideVisibilityOperation<RequestViewModel>(
                    v => ((BusinessTripRequestViewModel)v.RequestObjectViewModel).DestinationCityTaxiVouchers,
                        viewModel.TransportationPreferences.HasFlag(MeansOfTransport.Taxi)
                        && IsVoucherServiceAvaliable(new[] { viewModel.DestinationCityId }));
        }

        private bool IsVoucherServiceAvaliable(long[] destinationCityIds)
        {
            if (!destinationCityIds.IsNullOrEmpty())
            {
                foreach (var cityId in destinationCityIds)
                {
                    if (_cityService.GetCityOrDefaultById(cityId)?.IsVoucherServiceAvailable ?? false)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private ControlRenderingModel GetRequestControlByName(
            FormRenderingModel form,
            Expression<Func<BusinessTripRequestViewModel, object>> selector)
        {
            return base.GetRequestControlByName(form, selector);
        }
    }
}