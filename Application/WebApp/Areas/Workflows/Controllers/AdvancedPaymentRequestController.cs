﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.WebApp.Areas.BusinessTrips.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.AdvancedPaymentRequest)]
    public class AdvancedPaymentRequestController
        : RequestGenericController
    {
        private readonly ICompanyService _companyService;
        private readonly IBusinessTripService _businessTripService;
        private readonly ICurrencyService _currencyService;
        private readonly IClassMapping<CurrencyDto, CurrencyViewModel> _currencyDtoToViewModelMapping;

        public AdvancedPaymentRequestController(
            BaseRequestControllerDependencies baseRequestDependencies,
            ICompanyService companyService,
            IBusinessTripService businessTripService,
            ICurrencyService currencyService,
            IClassMapping<CurrencyDto, CurrencyViewModel> currencyDtoToViewModelMapping)
            : base(baseRequestDependencies)
        {
            _companyService = companyService;
            _businessTripService = businessTripService;
            _currencyService = currencyService;
            _currencyDtoToViewModelMapping = currencyDtoToViewModelMapping;

            Layout.Scripts.Add("~/Areas/Workflows/Scripts/AdvancedPaymentRequest.js");
        }

        protected override IList<ClientSideOperation<RequestViewModel>> RefreshFormAfterChange(RequestViewModel viewModel)
        {
            var result = base.RefreshFormAfterChange(viewModel);
            var inner = (AdvancedPaymentRequestViewModel)viewModel.RequestObjectViewModel;

            if (inner.ParticipantId != default(long))
            {
                var company = _companyService.GetCompanyOrDefaultByBtParticipantId(inner.ParticipantId);
                var currency = _currencyDtoToViewModelMapping.CreateFromSource(
                        _currencyService.GetCurrencyById(company.DefaultAdvancedPaymentCurrencyId));
                var businessTrip = _businessTripService.GetBusinessTripOrDefaultById(inner.BusinessTripId);
                var days = (decimal)Math.Ceiling((businessTrip.EndedOn - businessTrip.StartedOn).TotalDays) + 1;

                result.Add(new ClientSideValueChangeOperation<RequestViewModel>(
                    InnerFieldExpression(e => e.Amount), (days * company.DefaultAdvancedPaymentDailyAmount).ToString()));

                result.Add(new ClientSideValuePickerOperation<RequestViewModel>(
                    InnerFieldExpression(e => e.CurrencyId), new ValuePickerResponseViewModel
                    {
                        ElementId = currency.Id,
                        ElementDescription = currency.ToString(),
                    }));

                result.Add(new ClientSideValueChangeOperation<RequestViewModel>(
                    InnerFieldExpression(e => e.CurrencyDisplayName), currency.ToString()));
            }

            return result;
        }

        private Expression<Func<RequestViewModel, object>> InnerFieldExpression(
            Expression<Func<AdvancedPaymentRequestViewModel, object>> fieldExpression)
        {
            var innerFieldSelector = new BusinessLogic<AdvancedPaymentRequestViewModel, object>(fieldExpression);
            var baseFieldSelector = new BusinessLogic<RequestViewModel, object>(
                r => innerFieldSelector.Call((AdvancedPaymentRequestViewModel)r.RequestObjectViewModel));

            return baseFieldSelector.AsExpression();
        }
    }
}
