﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
    public class RequestHistoryController : ReadOnlyCardIndexController<RequestHistoryEntryViewModel, RequestHistoryEntryDto>
    {
        public RequestHistoryController(IRequestHistoryEntryCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = RequestResource.RequestHistoryTitle;

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
        }
    }
}