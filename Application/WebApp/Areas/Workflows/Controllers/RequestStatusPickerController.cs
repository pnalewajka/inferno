﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [Identifier("ValuePickers.RequestStatus")]
    [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
    public class RequestStatusPickerController : ReadOnlyCardIndexController<RequestStatusViewModel, RequestStatusDto>
    {
        public RequestStatusPickerController(IRequestStatusCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}