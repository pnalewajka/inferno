﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    public class DataChangeRequestController : Controller
    {
        public ActionResult RedirectToNewController()
        {
            const string oldControllerName = "DataChangeRequest";

            var areaName = RoutingHelper.GetAreaName(typeof(RequestController));
            var newControllerName = RoutingHelper.GetControllerName<RequestController>();
            var newUrl = Request.RawUrl.Replace($"{areaName}/{oldControllerName}", $"{areaName}/{newControllerName}");

            return RedirectPermanent(newUrl);
        }
    }
}
