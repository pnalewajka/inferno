﻿using System.Web.Mvc;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.ActionSets.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewActionSet)]
    public class ActionSetController : CardIndexController<ActionSetViewModel, ActionSetDto>
    {
        private readonly IActionSetCardIndexDataService _actionSetCardIndexDataService;
        private readonly IActionSetDefinitionResolvingService _actionSetDefinitionResolvingService;

        public ActionSetController(
            IActionSetCardIndexDataService actionSetCardIndexDataService,
            IActionSetDefinitionResolvingService actionSetDefinitionResolvingService,
            IBaseControllerDependencies dependencies)
            : base(actionSetCardIndexDataService, dependencies)
        {
            _actionSetCardIndexDataService = actionSetCardIndexDataService;
            _actionSetDefinitionResolvingService = actionSetDefinitionResolvingService;

            CardIndex.Settings.Title = ActionSetResources.Title;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ActionSetResources.RefreshDefinitionsButton,
                OnClickAction = Url.UriActionGet(nameof(RefreshDefinitions)),
                RequiredRole = SecurityRoleType.CanRefreshActionSetDefinitions,
            });

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddActionSet;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditActionSet;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteActionSet;
        }

        [AtomicAuthorize(SecurityRoleType.CanRefreshActionSetDefinitions)]
        public ActionResult RefreshDefinitions()
        {
            foreach (var definition in _actionSetDefinitionResolvingService.GetActionSetDefinitions())
            {
                _actionSetCardIndexDataService.CreateOrUpdateActionSet(
                    definition.Code,
                    definition.Description,
                    definition.TriggeringRequestType,
                    definition.TriggeringRequestStatuses,
                    definition.Definition);
            }

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }
    }
}
