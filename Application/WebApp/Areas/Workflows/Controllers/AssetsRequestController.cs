﻿using System;
using System.Linq;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.AssetsRequest)]
    public class AssetsRequestController
        : RequestGenericController
    {
        private readonly IRequestService _assetsRequestService;

        public AssetsRequestController(BaseRequestControllerDependencies baseRequestDependencies)
            : base(baseRequestDependencies)
        {
            _assetsRequestService = baseRequestDependencies.RequestServiceResolver.GetByServiceType(RequestType.AssetsRequest);

            Layout.Scripts.Add("~/Areas/Workflows/Scripts/AssetsRequest.js");

            CardIndex.Settings.EditFormCustomization += form =>
            {
                var repeaterItem = ((RepeaterRenderingModel)form.GetControlByName<RequestViewModel>(a => a.RequestObjectViewModel)).Items.First();

                var onboardingRequestIdControlName = $"{nameof(RequestViewModel.RequestObjectViewModel)}.{nameof(AssetsRequestViewModel.OnboardingRequestId)}";
                var onboardingRequestIdControl = (HiddenInputRenderingModel)repeaterItem.Controls.Single(v => v.Name == onboardingRequestIdControlName);

                long? onboardingId = onboardingRequestIdControl.Value != null ? long.Parse(Convert.ToString(onboardingRequestIdControl.Value)) : (long?)null;

                if (onboardingId.HasValue)
                {
                    var employeeIdControlName = $"{nameof(RequestViewModel.RequestObjectViewModel)}.{nameof(AssetsRequestViewModel.EmployeeId)}";
                    var employeeValuePickerControlList = repeaterItem.Controls.Single(v => v.Name == employeeIdControlName);
                    employeeValuePickerControlList.IsReadOnly = true;
                }
            };
        }

        protected override object CreateDefaultNewRecord()
        {
            return CreateDefaultNewRecord(RequestType.AssetsRequest, _assetsRequestService);
        }
    }
}