﻿using System;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Helpers;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.WebApp.Areas.PeopleManagement.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.OnboardingRequest)]
    public class OnboardingRequestController : RequestGenericController
    {
        private readonly IDocumentDownloadService _documentDownloadService;
        private readonly IOnboardingRequestService _onboardingRequestService;

        public OnboardingRequestController(
            BaseRequestControllerDependencies baseRequestDependencies,
            IOrgUnitService orgUnitService,
            IOnboardingRequestService onboardingRequestService,
            IDocumentDownloadService documentDownloadService)
            : base(baseRequestDependencies)
        {
            _documentDownloadService = documentDownloadService;
            _onboardingRequestService = onboardingRequestService;

            CardIndex.Settings.AddFormCustomization += form =>
            {
                var repeaterItem = ((RepeaterRenderingModel)form.GetControlByName<RequestViewModel>(a => a.RequestObjectViewModel)).Items.First();

                var lineManagerControlName = $"{nameof(RequestViewModel.RequestObjectViewModel)}.{nameof(OnboardingRequestViewModel.LineManagerEmployeeId)}";
                var lineManagerControl = repeaterItem.Controls.Single(c => c.Name == lineManagerControlName);

                lineManagerControl.IsReadOnly = true;
            };

            CardIndex.Settings.EditFormCustomization += form =>
            {
                var repeaterItem = ((RepeaterRenderingModel)form.GetControlByName<RequestViewModel>(a => a.RequestObjectViewModel)).Items.First();

                var lineManagerControlName = $"{nameof(RequestViewModel.RequestObjectViewModel)}.{nameof(OnboardingRequestViewModel.LineManagerEmployeeId)}";
                var lineManagerControl = repeaterItem.Controls.Single(c => c.Name == lineManagerControlName);

                var orgUnitControlName = $"{nameof(RequestViewModel.RequestObjectViewModel)}.{nameof(OnboardingRequestViewModel.OrgUnitId)}";
                var orgUnitControl = (ValuePickerRenderingModel)repeaterItem.Controls.Single(c => c.Name == orgUnitControlName);

                if (orgUnitControl.Ids.Any())
                {
                    var currentPrincipal = GetCurrentPrincipal();
                    var canAssignLineManager = currentPrincipal.IsInRole(SecurityRoleType.CanAssignLineManagerInOnboardingRequest);

                    var orgUnitManagerEmployeeId = orgUnitService.GetOrgUnitManagerId(orgUnitControl.Ids.First());
                    var isOrgUnitManager = currentPrincipal.EmployeeId == orgUnitManagerEmployeeId;

                    lineManagerControl.IsReadOnly = !canAssignLineManager && !isOrgUnitManager;
                }
                else
                {
                    lineManagerControl.IsReadOnly = true;
                }
            };

            Layout.Scripts.Add("~/Areas/Workflows/Scripts/OnboardingRequest.js");
        }

        protected override object CreateDefaultNewRecord()
        {
            return CreateDefaultNewRecord(RequestType.OnboardingRequest, _onboardingRequestService);
        }

        public JsonNetResult UpdateDetailsVisibility(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var onboardingRequestViewModel = (OnboardingRequestViewModel)viewModel.RequestObjectViewModel;

            response.Operations.Add(new ClientSideVisibilityOperation<RequestViewModel>(
                v => ((OnboardingRequestViewModel)v.RequestObjectViewModel).ReferralProgramDetails,
                onboardingRequestViewModel.IsComingFromReferralProgram));

            response.Operations.Add(new ClientSideVisibilityOperation<RequestViewModel>(
                v => ((OnboardingRequestViewModel)v.RequestObjectViewModel).RelocationPackageDetails,
                onboardingRequestViewModel.IsRelocationPackageNeeded));

            return response.ToJsonNetResult();
        }

        public JsonNetResult UpdateSuggestedLogin(RequestViewModel viewModel)
        {
            var response = new OnValueChangeServerResponse<RequestViewModel>();
            var onboardingRequestViewModel = (OnboardingRequestViewModel)viewModel.RequestObjectViewModel;

            var firstName = onboardingRequestViewModel.FirstName;
            var lastName = onboardingRequestViewModel.LastName;

            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                response.Operations.Add(new ClientSideValueChangeOperation<RequestViewModel>(
                    v => ((OnboardingRequestViewModel)v.RequestObjectViewModel).Login,
                    UserLoginHelper.GetSuggestedLogin(firstName, lastName)));
            }

            return response.ToJsonNetResult();
        }

        [HttpGet]
        public ActionResult DownloadForeignEmployeeDocument(long documentId, Guid secret)
        {
            var documentDownloadDto = _documentDownloadService.GetDocument("Documents.ForeignEmployeeDocuments", documentId, secret);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }

        [HttpGet]
        public ActionResult DownloadPayrollDocument(long documentId, Guid secret)
        {
            var documentDownloadDto = _documentDownloadService.GetDocument("Documents.PayrollDocuments", documentId, secret);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }

        [HttpGet]
        public ActionResult DownloadWelcomeAnnounceEmailPhoto(long documentId, Guid secret)
        {
            var documentDownloadDto = _documentDownloadService.GetDocument("Pictures.EmployeePhotoDocuments", documentId, secret);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }

        [HttpGet]
        public ActionResult ForeignEmployeeDocuments(long id, Guid secret)
        {
            var viewModel = new OnboardingDocumentsViewModel
            {
                ActionName = nameof(DownloadForeignEmployeeDocument),
                Documents = _onboardingRequestService.GetForeignEmployeeDocuments(id, secret),
                Secret = secret
            };

            return View("~/Areas/PeopleManagement/Views/OnboardingDocuments/List.cshtml", viewModel);
        }

        [HttpGet]
        public ActionResult PayrollDocuments(long id, Guid secret)
        {
            var viewModel = new OnboardingDocumentsViewModel
            {
                ActionName = nameof(DownloadPayrollDocument),
                Documents = _onboardingRequestService.GetPayrollDocuments(id, secret),
                Secret = secret
            };

            return View("~/Areas/PeopleManagement/Views/OnboardingDocuments/List.cshtml", viewModel);
        }

        [HttpGet]
        public ActionResult WelcomeAnnounceEmailPhotos(long id, Guid secret)
        {
            var viewModel = new OnboardingDocumentsViewModel
            {
                ActionName = nameof(DownloadWelcomeAnnounceEmailPhoto),
                Documents = _onboardingRequestService.GetWelcomeAnnounceEmailPhotos(id, secret),
                Secret = secret
            };

            return View("~/Areas/PeopleManagement/Views/OnboardingDocuments/List.cshtml", viewModel);
        }
    }
}
