﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.WebApp.Areas.MeetMe.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.FeedbackRequest)]
    public class FeedbackRequestController
        : RequestGenericController
    {
        private readonly IRequestService _feedbackRequestService;

        public FeedbackRequestController(BaseRequestControllerDependencies baseRequestDependencies)
            : base(baseRequestDependencies)
        {
            _feedbackRequestService = baseRequestDependencies.RequestServiceResolver.GetByServiceType(RequestType.FeedbackRequest);

            CardIndex.Settings.AddFormCustomization += form =>
            {
                GetRequestControlByName(form, f => f.FeedbackContent).Visibility = ControlVisibility.Hide;
                GetRequestControlByName(form, f => f.Visibility).Visibility = ControlVisibility.Hide;

                var commentFeedbackControl = GetRequestControlByName(form, f => f.Comment);
                commentFeedbackControl.IsRequired = true;
                commentFeedbackControl.RequiredMarker = true;
                commentFeedbackControl.RequiredValueErrorMessage = string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, commentFeedbackControl.GetLabelPlainText());

                GetRequestControlByName(form, f => f.EvaluatorEmployeeId).Visibility = ControlVisibility.Hide;

                var evaluatorEmployeeIdsControl = GetRequestControlByName(form, f => f.EvaluatorEmployeeIds);
                evaluatorEmployeeIdsControl.IsRequired = true;
                evaluatorEmployeeIdsControl.RequiredMarker = true;
                evaluatorEmployeeIdsControl.RequiredValueErrorMessage = string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, evaluatorEmployeeIdsControl.GetLabelPlainText());
            };

            CardIndex.Settings.EditFormCustomization += form =>
            {
                var feedbackControl = GetRequestControlByName(form, f => f.FeedbackContent);
                feedbackControl.IsRequired = true;
                feedbackControl.RequiredMarker = true;
                feedbackControl.RequiredValueErrorMessage = string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, feedbackControl.GetLabelPlainText());

                var feedbackVisibilityRuleControl = GetRequestControlByName(form, f => f.Visibility);
                feedbackVisibilityRuleControl.IsRequired = true;
                feedbackVisibilityRuleControl.RequiredMarker = true;
                feedbackVisibilityRuleControl.RequiredValueErrorMessage = string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, feedbackVisibilityRuleControl.GetLabelPlainText());

                var commentFeedbackControl = GetRequestControlByName(form, f => f.Comment);

                if (GetOriginScenario(form) == FeedbackOriginScenario.OwnInitiative)
                {
                    commentFeedbackControl.Visibility = ControlVisibility.Remove;
                    commentFeedbackControl.IsRequired = false;
                }
                else
                {
                    commentFeedbackControl.IsReadOnly = true;
                    commentFeedbackControl.IsRequired = true;
                }

                GetRequestControlByName(form, f => f.EvaluatedEmployeeId).IsReadOnly = true;

                var evaluatorControl = GetRequestControlByName(form, f => f.EvaluatorEmployeeId);
                evaluatorControl.IsReadOnly = true;
                evaluatorControl.Visibility = ControlVisibility.Hide;

                GetRequestControlByName(form, f => f.EvaluatorEmployeeIds).IsReadOnly = true;
            };

            CardIndex.Settings.ViewFormCustomization += form =>
            {
                var commentFeedbackControl = GetRequestControlByName(form, f => f.Comment);
                commentFeedbackControl.IsReadOnly = true;

                if (GetOriginScenario(form) == FeedbackOriginScenario.OwnInitiative)
                {
                    commentFeedbackControl.Visibility = ControlVisibility.Remove;
                }

                GetRequestControlByName(form, f => f.EvaluatorEmployeeId).Visibility = ControlVisibility.Hide;
            };
        }

        private FeedbackOriginScenario GetOriginScenario(FormRenderingModel form)
        {
            var requestModel = (RequestViewModel)form.ViewModel;
            var feedbackModel = (FeedbackRequestViewModel)requestModel.RequestObjectViewModel;

            return feedbackModel.OriginScenario;
        }

        protected override object CreateDefaultNewRecord()
        {
            return CreateDefaultNewRecord(RequestType.FeedbackRequest, _feedbackRequestService);
        }

        private ControlRenderingModel GetRequestControlByName(
           FormRenderingModel form,
           Expression<Func<FeedbackRequestViewModel, object>> selector)
        {
            return base.GetRequestControlByName(form, selector);
        }
    }
}