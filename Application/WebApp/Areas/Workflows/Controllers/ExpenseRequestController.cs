﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.ExpenseRequest)]
    public class ExpenseRequestController : RequestGenericController
    {
        public ExpenseRequestController(BaseRequestControllerDependencies baseRequestDependencies)
            : base(baseRequestDependencies)
        {
            EnableRequestDocumentControl();
        }

        public override ActionResult Add(RequestViewModel viewModelRecord)
        {
            AssignEmployee(viewModelRecord.RequestObjectViewModel as ExpenseRequestViewModel);

            return base.Add(viewModelRecord);
        }

        private void AssignEmployee(ExpenseRequestViewModel requesterRecord)
        {
            var currentPrincipal = GetCurrentPrincipal();

            requesterRecord.EmployeeId = currentPrincipal.EmployeeId;
        }
    }
}