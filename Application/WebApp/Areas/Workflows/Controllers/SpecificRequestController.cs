﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    public abstract class SpecificRequestController
         : RequestGenericController
    {
        public SpecificRequestController(BaseRequestControllerDependencies baseRequestDependencies)
            : base(baseRequestDependencies)
        {
            CardIndex.Settings.FilterGroups.Remove(
                CardIndex.Settings.FilterGroups.FirstOrDefault(t => t.DisplayName == RequestResources.RequestType));

            CardIndex.Settings.ExportToExcelUrl = OriginalUrl.Action(nameof(ExportToExcel));
            CardIndex.Settings.ExportToCsvUrl = OriginalUrl.Action(nameof(ExportToCsv));
            CardIndex.Settings.CopyToClipboardUrl = OriginalUrl.Action(nameof(CopyToClipboard));

            CardIndex.Settings.GridViewConfigurations = CreateViewConfigurations().ToList();
        }

        protected virtual IEnumerable<IGridViewConfiguration> CreateViewConfigurations()
        {
            yield break;
        }
    }
}