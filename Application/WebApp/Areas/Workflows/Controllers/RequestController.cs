﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Helpers;
using Smt.Atomic.WebApp.Models;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
    [PerformanceTest(nameof(RequestController.List))]
    public class RequestController : AtomicController
    {
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;

        public RequestController(
            IBaseControllerDependencies baseControllerDependencies,
            IRequestCardIndexDataService requestCardIndexDataService)
            : base(baseControllerDependencies)
        {
            _requestCardIndexDataService = requestCardIndexDataService;
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSubmitRequest)]
        public ActionResult Submit(long id)
        {
            return InternalRedirectToAction(id, nameof(RequestGenericController.Submit));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanApproveRequest)]
        public ActionResult Approve(long id)
        {
            return InternalRedirectToAction(id, nameof(RequestGenericController.Approve));
        }

        [HttpPost]
        public ActionResult ActionOnBehalf(ActionOnBehalfViewModel model)
        {
            return InternalRedirectToAction(model.Id, nameof(RequestGenericController.ActionOnBehalf));
        }

        [HttpPost]
        public ActionResult PerformActionOnBehalf(ActionOnBehalfViewModel model)
        {
            return InternalRedirectToAction(model.Id, nameof(RequestGenericController.PerformActionOnBehalf));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanRejectRequest)]
        public ActionResult Reject(long id)
        {
            return InternalRedirectToAction(id, nameof(RequestGenericController.Reject));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanRejectRequest)]
        public ActionResult PerformRejection(RejectRequestViewModel model)
        {
            return InternalRedirectToAction(model.Id, nameof(RequestGenericController.PerformRejection));
        }

        [HttpPost]
        public ActionResult GetApprovers(RequestViewModel model)
        {
            return InternalRedirectToAction(model.RequestType, nameof(RequestGenericController.GetApprovers));
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/RequestAdd")]
        [AtomicAuthorize(SecurityRoleType.CanAddRequest)]
        public ActionResult Add(RequestViewModel model)
        {
            return InternalRedirectToAction(model.RequestType, nameof(RequestGenericController.Add));
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/RequestAdd")]
        [AtomicAuthorize(SecurityRoleType.CanAddRequest)]
        public ActionResult Add()
        {
            return InternalRedirectToAction(
                RequestControllerHelper.ExtractRequestTypeFromUrlParameter(HttpContext),
                nameof(RequestGenericController.Add));
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/RequestEdit")]
        [AtomicAuthorize(SecurityRoleType.CanEditRequest)]
        public ActionResult Edit(RequestViewModel model)
        {
            return InternalRedirectToAction(model.Id, nameof(RequestGenericController.Edit));
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/RequestEdit")]
        [AtomicAuthorize(SecurityRoleType.CanEditRequest)]
        public ActionResult Edit(long id)
        {
            return InternalRedirectToAction(id, nameof(RequestGenericController.Edit));
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/RequestView")]
        [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
        public ActionResult View(long id)
        {
            return InternalRedirectToAction(id, nameof(RequestGenericController.View));
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/RequestEdit")]
        [AtomicAuthorize(SecurityRoleType.CanEditRequest)]
        public ActionResult EditBulk(BulkEditPropertyLookupViewModel model)
        {
            return InternalRedirectToAction(model.RecordIds.First(), nameof(RequestGenericController.EditBulk));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanDeleteRequest)]
        public ActionResult Delete(IList<long> ids)
        {
            return InternalRedirectToAction(null, nameof(RequestGenericController.Delete));
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex")]
        [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
        public ActionResult List(GridParameters parameters)
        {
            return InternalRedirectToAction(null, nameof(RequestGenericController.List));
        }

        [HttpGet]
        public ActionResult ExportToCsv(GridParameters parameters)
        {
            return InternalRedirectToAction(null, nameof(RequestGenericController.ExportToCsv));
        }

        [HttpGet]
        public ActionResult ExportToExcel(GridParameters parameters)
        {
            return InternalRedirectToAction(null, nameof(RequestGenericController.ExportToExcel));
        }

        [HttpGet]
        public ActionResult CopyToClipboard(GridParameters parameters)
        {
            return InternalRedirectToAction(null, nameof(RequestGenericController.CopyToClipboard));
        }

        private ActionResult InternalRedirectToAction(long requestId, string actionName)
        {
            var requestType = _requestCardIndexDataService.GetRequestTypeById(requestId);

            return InternalRedirectToAction(requestType, actionName);
        }

        private ActionResult InternalRedirectToAction(RequestType? requestType, string actionName)
        {
            var controllerType = RequestControllerHelper.GetControllerByType(requestType);

            return new TransferActionResult(controllerType, actionName);
        }
    }
}
