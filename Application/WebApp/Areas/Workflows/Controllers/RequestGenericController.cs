﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Helpers;
using Smt.Atomic.WebApp.Models;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;
using FilterCodes = Smt.Atomic.Business.Workflows.Consts.FilterCodes;
using RequestType = Smt.Atomic.CrossCutting.Common.Enums.RequestType;
using SearchAreaCodes = Smt.Atomic.Business.Workflows.Interfaces.SearchAreaCodes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
    public class RequestGenericController : CardIndexController<RequestViewModel, RequestDto>
    {
        private GridViewConfiguration<RequestViewModel> _simpleView;
        private TilesViewConfiguration<RequestViewModel> _tilesView;

        protected readonly IRequestWorkflowService RequestWorkflowService;
        protected readonly IRequestServiceResolver RequestServiceResolver;
        protected readonly IRequestCardIndexDataService RequestCardIndexDataService;
        protected readonly IClassMapping<RequestDto, RequestViewModel> RequestDtoToRequestViewModelMapping;
        protected readonly IClassMapping<RequestViewModel, RequestDto> RequestViewModelToRequestDtoMapping;
        protected readonly IRequestService RequestService;

        public UrlHelper OriginalUrl => new UrlHelper(
            System.Web.HttpContext.Current.Request.RequestContext);

        public override UrlHelper Url => new TransferUrlHelper<RequestController>(
            System.Web.HttpContext.Current.Request.RequestContext);

        protected override Type RestorePointControllerType => typeof(RequestController);

        protected virtual string AddAndSubmitConfirmDialogContent => RequestResources.AddAndSubmitConfirmDialogContent;
        protected virtual string AddAndSubmitConfirmDialogTitle => RequestResources.AddAndSubmitConfirmDialogTitle;
        protected virtual string EditAndSubmitConfirmDialogContent => RequestResources.EditAndSubmitConfirmDialogContent;
        protected virtual string EditAndSubmitConfirmDialogTitle => RequestResources.EditAndSubmitConfirmDialogTitle;
        protected virtual string SaveButtonText => RequestResources.SaveButtonText;
        protected virtual string SubmitButtonText => RequestResources.SubmitButtonText;

        public RequestGenericController(BaseRequestControllerDependencies baseRequestDependencies)
            : base(baseRequestDependencies.CardIndexDataService, baseRequestDependencies.Dependencies)
        {
            RequestCardIndexDataService = baseRequestDependencies.CardIndexDataService;
            RequestServiceResolver = baseRequestDependencies.RequestServiceResolver;
            RequestDtoToRequestViewModelMapping = baseRequestDependencies.ClassMappingFactory.CreateMapping<RequestDto, RequestViewModel>();
            RequestViewModelToRequestDtoMapping = baseRequestDependencies.ClassMappingFactory.CreateMapping<RequestViewModel, RequestDto>();
            RequestWorkflowService = baseRequestDependencies.RequestWorkflowService;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRequest;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRequest;
            CardIndex.Settings.EditButton.Availability = GetEditButtonAvailability();
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRequest;
            CardIndex.Settings.DeleteButton.Availability = GetDeleteButtonAvailability();
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRequest;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.Title = RequestResource.RequestTitle;
            CardIndex.Settings.DefaultNewRecord = CreateDefaultNewRecord;
            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations();

            CardIndex.Settings.ViewViewName = "View";
            CardIndex.Settings.EmptyListViewName = "~/Areas/Workflows/Views/EmptyRequestList.cshtml";

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = RequestResource.ChangeApprovers,
                RequiredRole = SecurityRoleType.CanChangeApproversInRequests,
                OnClickAction = Url.UriActionGet(
                    nameof(RequestApproversChangeServiceController.ChangeApprovers),
                    RoutingHelper.GetControllerName<RequestApproversChangeServiceController>(),
                    KnownParameter.SelectedId, "area",
                    RoutingHelper.GetAreaName(typeof(RequestApproversChangeServiceController))),
                Icon = FontAwesome.Edit,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            CardIndex.Settings.AddButton.OnClickAction = new DropdownAction
            {
                Items = GetAddActions().OrderBy(a => a.Text).ToList()
            };

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportRequests);

            CreateRowButtons();

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<RequestViewModel>(a => a.Documents).Visibility = ControlVisibility.Remove;
                form.GetControlByName<RequestViewModel>(x => x.RequesterFullName).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<RequestViewModel>(a => a.Documents).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.MyOpenRequests, DisplayName = RequestResources.MyOpenRequests },
                        new Filter { Code = FilterCodes.MyClosedRequests, DisplayName = RequestResources.MyClosedRequests },
                        new Filter { Code = FilterCodes.RequestsRequestedByMe, DisplayName = RequestResources.RequestsRequestedByMe },
                        new Filter { Code = FilterCodes.WatchedByMe, DisplayName = RequestResources.WatchedByMe },
                        new Filter { Code = FilterCodes.DecisionAwaiting, SubGroupName = RequestResources.MyDecision, DisplayName = RequestResources.DecisionAwaiting },
                        new Filter { Code = FilterCodes.DecisionDone, SubGroupName = RequestResources.MyDecision, DisplayName = RequestResources.DecisionDone },
                        new Filter { Code = FilterCodes.AllRequests, DisplayName = RequestResources.AllRequests },
                    }
                },
                new FilterGroup
                {
                    ButtonText = RequestResources.RequestType,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = GetRequestTypeFilter(CardIndexHelper.BuildEnumBasedFilters<RequestType>()).OrderBy(f => f.SubGroupName).ToList()
                },
                new FilterGroup
                {
                    ButtonText = RequestResource.Status,
                    DisplayName = RequestResource.Status,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<RequestStatus>()
                },
                new FilterGroup
                {
                    ButtonText = RequestResource.Others,
                    DisplayName = GeneralResources.DateFiltersGroupTitle,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.CurrentMonth, DisplayName = GeneralResources.CurrentMonth },
                        new Filter { Code = FilterCodes.PreviousMonth, DisplayName = GeneralResources.PreviousMonth },
                        new ParametricFilter<DateRangeFilterViewModel>(FilterCodes.DateRange, RequestResource.DateRangeFilterName),
                    }
                },
                new FilterGroup
                {
                    ButtonText = RequestResource.Others,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<ContractTypeFilterViewModel>(FilterCodes.ContractType, RequestResource.ContractTypeFilterName),
                    }
                },
                new FilterGroup
                {
                    ButtonText = RequestResource.Others,
                    DisplayName = RequestResources.Requester,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<CompanyFilterViewModel>(FilterCodes.RequesterCompany, RequestResource.CompanyFilterName),
                        new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.RequesterOrgUnit, RequestResource.OrgUnitFilterName),
                    }
                },
               new FilterGroup
               {
                   ButtonText = RequestResource.Others,
                   Type = FilterGroupType.RadioGroup,
                   Filters = new List<Filter>
                   {
                       new Filter
                       {
                           Code = FilterCodes.Attachments, DisplayName = RequestResource.Attachments
                       }
                   }
                },
               new FilterGroup
                {
                    ButtonText = RequestResource.Others,
                    DisplayName = RequestResource.AffectedUser,
                    Type = FilterGroupType.AndCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<AffectedUserFilterViewModel>(FilterCodes.AffectedUser, RequestResource.AffectedUserFilterName),
                    }
                },
            };

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.RequestingUser, RequestResources.RequestingUserLabel),
                new SearchArea(SearchAreaCodes.ApprovingUser, RequestResources.ApprovingUserLabel, false),
                new SearchArea(SearchAreaCodes.Watchers, RequestResources.WatchersLabel),
                new SearchArea(SearchAreaCodes.Description, RequestResources.DescriptionLabel, false)
            };

            Layout.Resources.AddFrom<RequestResource>("NoApprovers");
            Layout.Resources.AddFrom<GeneralResources>("And");
            Layout.Resources.AddFrom<GeneralResources>("Or");

            Layout.Css.Add("~/Areas/Workflows/Content/Request.css");
            Layout.Scripts.Add("~/Areas/Workflows/Scripts/WorkflowRequests.js");

            CardIndex.ConfigureAddButtons += (CardIndexRecordViewModel<RequestViewModel> model) =>
            {
                var requestService = baseRequestDependencies.RequestServiceResolver.GetByServiceType(model.ViewModelRecord.RequestType);

                if (requestService.ShouldStartAsDraft)
                {
                    model.Buttons.Add(FooterButtonBuilder.AddAndOpenEditButton(HttpUtility.UrlDecode(Url.Action(nameof(Edit), new { id = "{0}" })), SaveButtonText));
                }

                model.Buttons.GetAddButton()
                    .SetDisplayText(SubmitButtonText)
                    .SetSubmitParameter<RequestViewModel>((int)RequestSaveAction.SaveAndSubmit, m => m.SaveActionType)
                    .SetRequireConfirmation(AddAndSubmitConfirmDialogTitle, AddAndSubmitConfirmDialogContent);
            };

            CardIndex.ConfigureEditButtons += (CardIndexRecordViewModel<RequestViewModel> model) =>
            {
                var requestService = baseRequestDependencies.RequestServiceResolver.GetByServiceType(model.ViewModelRecord.RequestType);

                var editButton = model.Buttons.GetEditButton();
                
                if (requestService.ShouldStartAsDraft)
                {
                    model.Buttons.Add(FooterButtonBuilder.EditAndStayButton(SaveButtonText)
                        .SetSubmitParameter<RequestViewModel>((int)RequestSaveAction.Save, m => m.SaveActionType));
                }
                else
                {
                    editButton
                        .SetDisplayText(SaveButtonText)
                        .SetSubmitParameter<RequestViewModel>((int)RequestSaveAction.Save, m => m.SaveActionType);
                }

                if (ShowSubmitButton(model.ViewModelRecord))
                {
                    editButton
                        .SetDisplayText(SubmitButtonText)
                        .SetSubmitParameter<RequestViewModel>((int)RequestSaveAction.SaveAndSubmit, m => m.SaveActionType)
                        .SetRequireConfirmation(EditAndSubmitConfirmDialogTitle, EditAndSubmitConfirmDialogContent);
                }
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSubmitRequest)]
        public ActionResult Submit(long id)
        {
            try
            {
                var result = RequestWorkflowService.Submit(id);
                AddAlerts(result);
                var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

                return Redirect(listActionUrl);
            }
            catch (RequestMissingDataForStatusChangeBusinessException ex)
            {
                AddAlert(AlertType.Error, ex.Message, true);

                return Redirect(Url.Action(nameof(Edit), new { Id = id }));
            }
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanApproveRequest)]
        public ActionResult Approve(long id)
        {
            try
            {
                var result = RequestWorkflowService.Approve(id);
                AddAlerts(result);
                var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

                return Redirect(listActionUrl);
            }
            catch (RequestMissingDataForStatusChangeBusinessException ex)
            {
                AddAlert(AlertType.Error, ex.Message, true);

                return Redirect(Url.Action(nameof(Edit), new { Id = id }));
            }
        }

        [HttpPost]
        public ActionResult ActionOnBehalf(ActionOnBehalfViewModel model)
        {
            var dialogModel = new ModelBasedDialogViewModel<ActionOnBehalfViewModel>(model)
            {
                Title = ActionOnBehalfResources.Title
            };

            return PartialView("ActionOnBehalf", dialogModel);
        }

        [HttpPost]
        public ActionResult PerformActionOnBehalf(ActionOnBehalfViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return ActionOnBehalf(viewModel);
            }

            BusinessResult result = null;

            var approvalDto = RequestWorkflowService.GetApproval(viewModel.Id, viewModel.ApprovalId);

            if (approvalDto.Status != ApprovalStatus.Pending)
            {
                throw new InvalidOperationException("Approval is already accepted or rejected");
            }

            var onBehalfOfUserId = approvalDto.UserId;

            try
            {
                switch (viewModel.ForceAction)
                {
                    case OnBehalfAction.Approve:
                        result = RequestWorkflowService.ApproveOnBehalfOf(viewModel.Id, onBehalfOfUserId);
                        break;
                    case OnBehalfAction.Reject:
                        result = RequestWorkflowService.RejectOnBehalfOf(viewModel.Id, onBehalfOfUserId, viewModel.RejectionReason);
                        break;
                }

                AddAlerts(result);

                return DialogHelper.Reload();
            }
            catch (RequestMissingDataForStatusChangeBusinessException ex)
            {
                AddAlert(AlertType.Error, ex.Message, true);

                return DialogHelper.Redirect(this, Url.Action(nameof(Edit), new { viewModel.Id }));
            }
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanRejectRequest)]
        public ActionResult Reject(long id)
        {
            var model = new RejectRequestViewModel
            {
                Id = id,
                Reason = string.Empty,
            };

            var dialogModel = new ModelBasedDialogViewModel<RejectRequestViewModel>(model)
            {
                Title = RequestResource.RejectionReasonTitle
            };

            return PartialView(dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanRejectRequest)]
        public ActionResult PerformRejection(RejectRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = RequestWorkflowService.Reject(model.Id, model.Reason);

                AddAlerts(result);

                return DialogHelper.RedirectToList(this);
            }

            var dialogModel = new ModelBasedDialogViewModel<RejectRequestViewModel>(model)
            {
                Title = RequestResource.RejectionReasonTitle
            };

            return PartialView("Reject", dialogModel);
        }

        private IEnumerable<Filter> GetRequestTypeFilter(IEnumerable<Filter> filters)
        {
            var allRequests = RequestServiceResolver.GetAll();

            foreach (var filter in filters)
            {
                yield return new Filter
                {
                    Code = filter.Code,
                    DisplayName = filter.DisplayName,
                    SubGroupName = allRequests.FirstOrDefault(r =>
                        NamingConventionHelper.ConvertPascalCaseToHyphenated(r.RequestType.ToString()) == filter.Code)
                            .RequestTypeGroup.GetDescriptionOrValue()
                };
            }
        }

        private IEnumerable<ICommandButton> GetAddActions()
        {
            foreach (var requestService in RequestServiceResolver.GetAll().Where(CanBeAddedByUser))
            {
                var requestTypeUrlValue = NamingConventionHelper.ConvertPascalCaseToHyphenated(requestService.RequestType.ToString());

                yield return new CommandButton
                {
                    Text = requestService.RequestName,
                    Group = requestService.RequestTypeGroup.GetDescription(),
                    OnClickAction = Url.UriActionGet(
                        nameof(Add),
                        KnownParameter.Context,
                        RequestControllerHelper.RequestTypeUrlParameterName,
                        requestTypeUrlValue)
                };
            }
        }

        private bool CanBeAddedByUser(IRequestService requestService)
        {
            var principal = GetCurrentPrincipal();

            return requestService.CanBeStartedManually() &&
               requestService.CanBeAdded() &&
               requestService.RequiredRoleType.HasValue &&
               principal.IsInRole(requestService.RequiredRoleType.Value);
        }

        private void CheckRequiredRoleTypeForAdd(RequestType requestType)
        {
            var requiredRoleType = RequestServiceResolver.GetByServiceType(requestType).RequiredRoleType;

            if (requiredRoleType == null)
            {
                throw new BusinessException(WorkflowResources.CannotCreateRequestManually);
            }

            var principal = GetCurrentPrincipal();

            if (!principal.IsInRole(requiredRoleType.Value))
            {
                throw new MissingRequiredRoleException(new[] { requiredRoleType.ToString() });
            }
        }

        protected ControlRenderingModel GetRequestControlByName<TInnerViewModel>(
            FormRenderingModel form,
            Expression<Func<TInnerViewModel, object>> selector)
        {
            var requestObjectSelector = new BusinessLogic<RequestViewModel, TInnerViewModel>(
                r => (TInnerViewModel)r.RequestObjectViewModel);
            var propertySelector = new BusinessLogic<TInnerViewModel, object>(selector);

            var fullSelector = new BusinessLogic<RequestViewModel, object>(
                r => propertySelector.Call(requestObjectSelector.Call(r)));

            return form.GetControlByName(fullSelector.AsExpression());
        }

        protected virtual object CreateDefaultNewRecord()
        {
            var requestType = RequestControllerHelper.ExtractRequestTypeFromUrlParameter(HttpContext);
            var requestService = RequestServiceResolver.GetByServiceType(requestType);

            return CreateDefaultNewRecord(requestType, requestService);
        }

        protected virtual bool ShowSubmitButton(RequestViewModel model)
        {
            return model.Status == RequestStatus.Draft;
        }

        protected object CreateDefaultNewRecord(RequestType requestType, IRequestService requestService)
        {
            var requestObjectDto = requestService.GetDraft(HttpContext.Request.QueryString.ToDictionary());
            var currentPrincipal = GetCurrentPrincipal();

            var requestDto = new RequestDto
            {
                RequestType = requestService.RequestType,
                RequestObject = requestObjectDto,
                RequestingUserId = currentPrincipal.Id.GetValueOrDefault()
            };

            AssignRequester(requestDto);

            RequestWorkflowService.AddApprovers(requestDto);

            var result = RequestDtoToRequestViewModelMapping.CreateFromSource(requestDto);

            return result;
        }

        protected void EnableRequestDocumentControl()
        {
            CardIndex.Settings.AddFormCustomization += form =>
            {
                form.GetControlByName<RequestViewModel>(a => a.Documents).Visibility = ControlVisibility.Show;
            };

            CardIndex.Settings.EditFormCustomization += form =>
            {
                form.GetControlByName<RequestViewModel>(a => a.Documents).Visibility = ControlVisibility.Show;
            };
        }

        [HttpPost]
        public JsonNetResult GetApprovers(RequestViewModel viewModel)
        {
            var dto = RequestViewModelToRequestDtoMapping.CreateFromSource(viewModel);
            RequestWorkflowService.AddApprovers(dto);

            viewModel = RequestDtoToRequestViewModelMapping.CreateFromSource(dto);

            var serverResponse = new OnValueChangeServerResponse<RequestViewModel>
            {
                ServerResultData = new { viewModel.ApprovalGroups, viewModel.RequestObjectViewModel },
                Operations = RefreshFormAfterChange(viewModel),
            };

            return serverResponse.ToJsonNetResult();
        }

        protected virtual IList<ClientSideOperation<RequestViewModel>> RefreshFormAfterChange(RequestViewModel viewModel)
        {
            return new List<ClientSideOperation<RequestViewModel>>();
        }

        [HttpPost]
        [BreadcrumbBar("RequestCardIndex/RequestAdd")]
        public override ActionResult Add(RequestViewModel viewModelRecord)
        {
            CheckRequiredRoleTypeForAdd(viewModelRecord.RequestType);
            AssignRequester(viewModelRecord);

            return base.Add(viewModelRecord);
        }

        [HttpGet]
        [BreadcrumbBar("RequestCardIndex/RequestAdd")]
        public override ActionResult Add()
        {
            return base.Add();
        }

        public override ActionResult Delete([ModelBinder(typeof(CommaSeparatedListModelBinder))] IList<long> ids)
        {
            var requestDtos = RequestCardIndexDataService.GetRecordsByIds(ids.ToArray()).Select(r => r.Value).ToList();
            var currentUserId = GetCurrentPrincipal().Id.Value;

            foreach (var requestDto in requestDtos)
            {
                if (!RequestServiceResolver.GetByServiceType(requestDto.RequestType).CanBeDeleted(requestDto, currentUserId))
                {
                    throw new UnauthorizedAccessException();
                }
            }

            return base.Delete(ids);
        }

        [HttpPost]
        [BreadcrumbBar("RequestCardIndex/RequestEdit")]
        public override ActionResult Edit(RequestViewModel viewModelRecord)
        {
            var requestDto = RequestCardIndexDataService.GetRecordById(viewModelRecord.Id);

            if (!RequestServiceResolver.GetByServiceType(requestDto.RequestType).CanBeEdited(requestDto, GetCurrentPrincipal().Id.Value))
            {
                throw new UnauthorizedAccessException();
            }

            return base.Edit(viewModelRecord);
        }

        [HttpGet]
        [BreadcrumbBar("RequestCardIndex/RequestEdit")]
        public override ActionResult Edit(long id)
        {
            var requestDto = RequestCardIndexDataService.GetRecordById(id);

            if (!RequestServiceResolver.GetByServiceType(requestDto.RequestType).CanBeEdited(requestDto, GetCurrentPrincipal().Id.Value))
            {
                throw new UnauthorizedAccessException();
            }

            return base.Edit(id);
        }

        [HttpGet]
        [BreadcrumbBar("RequestCardIndex/RequestView")]
        public override ActionResult View(long id)
        {
            return base.View(id);
        }

        [HttpGet]
        [BreadcrumbBar("RequestCardIndex")]
        public override ActionResult List(GridParameters parameters)
        {
            SetDefaultView(Request.Browser.IsMobileDevice);

            return base.List(parameters);
        }

        private void AssignRequester(IRequesterData requesterRecord)
        {
            var currentPrincipal = GetCurrentPrincipal();

            requesterRecord.RequesterFullName =
                $"{currentPrincipal.FirstName} {currentPrincipal.LastName}".Trim();
            requesterRecord.RequestingUserId = currentPrincipal.Id.GetValueOrDefault();
        }

        private void CreateRowButtons()
        {
            IAtomicPrincipal currentPrincipal = GetCurrentPrincipal();

            var submitButton = new RowButton
            {
                Text = RequestResource.Submit,
                OnClickAction = Url.UriActionFormatPost(nameof(Submit), KnownParameter.SelectedId),
                Icon = FontAwesome.MailForward,
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanSubmitRequest,
                Availability = new RowButtonAvailability<RequestViewModel>
                {
                    Predicate = m => m.CanBeSubmitted
                }
            };

            var approveButton = new RowButton
            {
                Text = RequestResource.Approve,
                OnClickAction = Url.UriActionFormatPost(nameof(Approve), KnownParameter.SelectedId, null, "return-point", "workflows.request.list"),
                Icon = FontAwesome.Check,
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanApproveRequest,
                ButtonStyleType = CommandButtonStyle.Success,
                Availability = new RowButtonAvailability<RequestViewModel>
                {
                    Predicate = m => RequestWorkflowService.IsWaitingForDirectUserDecision(m.Id, currentPrincipal.Id.Value)
                }
            };

            var rejectButton = new RowButton
            {
                Text = RequestResource.Reject,
                OnClickAction = new JavaScriptCallAction($"Dialogs.showDialog({{ actionUrl:'{Url.Action(nameof(Reject))}', formData: {{ id: this.rowData[0].id }} }})"),
                Icon = FontAwesome.Close,
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanRejectRequest,
                ButtonStyleType = CommandButtonStyle.Danger,
                Availability = new RowButtonAvailability<RequestViewModel>
                {
                    Predicate = m => RequestWorkflowService.IsWaitingForDirectUserDecision(m.Id, currentPrincipal.Id.Value)
                }
            };

            var onBehalfButton = new RowButton
            {
                Text = ActionOnBehalfResources.ForceAction,
                OnClickAction = new JavaScriptCallAction($"Dialogs.showDialog({{ actionUrl:'{Url.Action("ActionOnBehalf")}', formData: {{ id: this.rowData[0].id }} }})"),
                Icon = FontAwesome.Forward,
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanApproveRequest,
                Availability = new RowButtonAvailability<RequestViewModel>
                {
                    Predicate = m =>
                        !RequestWorkflowService.CannotBeForcedStatuses.Contains(m.Status)
                        && m.IsWaitingForDecision
                        && (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanManageWorkflowsOnBehalf)
                            || RequestWorkflowService.IsWaitingForSubstituteUserDecision(m.Id, currentPrincipal.Id.Value))
                },
                ButtonStyleType = CommandButtonStyle.Link
            };

            CardIndex.Settings.RowButtons.Add(submitButton);
            CardIndex.Settings.RowButtons.Add(approveButton);
            CardIndex.Settings.RowButtons.Add(rejectButton);
            CardIndex.Settings.RowButtons.Add(onBehalfButton);
        }

        private IList<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            _simpleView =
                new GridViewConfiguration<RequestViewModel>(RequestResource.SimpleView, "simple");
            _simpleView.IncludeAllColumns();
            _simpleView.ExcludeColumn(r => r.Exception);
            _simpleView.ExcludeColumn(r => r.Company);
            _simpleView.ExcludeColumn(r => r.OrgUnit);
            _simpleView.ExcludeColumn(r => r.ContractType);
            _simpleView.ExcludeColumn(r => r.RejectionReason);
            _simpleView.ExcludeColumn(r => r.DocumentCount);
            _simpleView.ExcludeColumn(r => r.DeferredUntil);
            _simpleView.ExcludeColumn(r => r.AffectedUserFullNames);
            _simpleView.IsDefault = true;

            var extendedView =
                new GridViewConfiguration<RequestViewModel>(RequestResource.ExtendedView, "extended");
            extendedView.IncludeAllColumns();
            extendedView.ExcludeColumn(r => r.RejectionReason);
            extendedView.ExcludeColumn(r => r.Exception);
            extendedView.ExcludeColumn(r => r.DeferredUntil);
            extendedView.ExcludeColumn(r => r.AffectedUserFullNames);

            var administrationView =
               new GridViewConfiguration<RequestViewModel>(RequestResource.AdministrationView, "administration");
            administrationView.ExcludeAllColumns();
            administrationView.IncludeColumn(r => r.AffectedUserFullNames);
            administrationView.IncludeColumn(r => r.RequesterFullName);
            administrationView.IncludeColumn(r => r.RequestType);
            administrationView.IncludeColumn(r => r.Description);
            administrationView.IncludeColumn(r => r.DeferredUntil);
            administrationView.IncludeColumn(r => r.Exception);
            administrationView.IncludeColumn(r => r.RejectionReason);
            administrationView.IncludeColumn(r => r.ContractType);

            _tilesView = new TilesViewConfiguration<RequestViewModel>(RequestResource.TilesView, "tiles")
            {
                CustomViewName = "~/Areas/Workflows/Views/Request/_RequestTile.cshtml",
            };

            return new IGridViewConfiguration[]
            {
                _simpleView,
                extendedView,
                administrationView,
                _tilesView
            };
        }

        private ToolbarButtonAvailability GetEditButtonAvailability()
        {
            return new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "row.canBeEdited"
            };
        }

        private ToolbarButtonAvailability GetDeleteButtonAvailability()
        {
            return new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.AtLeastOneRowSelected,
                Predicate = "row.canBeDeleted"
            };
        }

        private void SetDefaultView(bool isMobileDevice)
        {
            _tilesView.IsDefault = isMobileDevice;
            _simpleView.IsDefault = !_tilesView.IsDefault;
        }
    }
}