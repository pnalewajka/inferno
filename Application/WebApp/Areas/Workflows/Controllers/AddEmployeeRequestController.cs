﻿using System.Linq;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Helpers;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.AddEmployeeRequest)]
    public class AddEmployeeRequestController
        : RequestGenericController
    {
        private readonly ICompanyService _companyService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;

        public AddEmployeeRequestController(
            BaseRequestControllerDependencies baseRequestDependencies,
            ICompanyService companyService,
            IOrgUnitService orgUnitService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService)
            : base(baseRequestDependencies)
        {
            _companyService = companyService;
            _orgUnitService = orgUnitService;
            _systemParameterService = systemParameterService;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;

            Layout.Scripts.Add("~/Areas/Workflows/Scripts/AddEmployeeRequest.js");
        }

        public JsonNetResult GetDefaultValuesFromOrgUnit(long orgUnitId)
        {
            var orgUnit = _orgUnitService.GetOrgUnitWithDefaultTimeTrackingMode(orgUnitId);

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                var absenceOnlyDefaultProjectName = orgUnit.AbsenceOnlyDefaultProjectId.HasValue
                    ? unitOfWork.Repositories.Projects.SelectById<Project, string>(
                        orgUnit.AbsenceOnlyDefaultProjectId.Value, ProjectBusinessLogic.ClientProjectName)
                    : null;
                var result = new
                {
                    orgUnit.TimeReportingMode,
                    orgUnit.AbsenceOnlyDefaultProjectId,
                    absenceOnlyDefaultProjectName
                };

                return new JsonNetResult(result) { SerializerSettings = JsonHelper.DefaultAjaxSettings };
            }
        }

        public JsonNetResult GetSuggestedEmailAndLogin(string firstName, string lastName, long? companyId)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
            {
                return null;
            }

            var standardLogin = UserLoginHelper.GetSuggestedLogin(firstName, lastName);
            var company = _companyService.GetAllCompanies().FirstOrDefault(c => c.Id == companyId);
            var defaultDomain = _systemParameterService.GetParameter<string>(ParameterKeys.UserDefaultEmailDomain);

            var result = new
            {
                Email = $"{standardLogin}@{company?.EmailDomain ?? defaultDomain}",
                Login = standardLogin
            };

            return new JsonNetResult(result) { SerializerSettings = JsonHelper.DefaultAjaxSettings };
        }
    }
}