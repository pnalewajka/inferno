﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Attributes;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.TaxDeductibleCostRequest)]
    public class TaxDeductibleCostRequestController : RequestGenericController
    {
        public TaxDeductibleCostRequestController(BaseRequestControllerDependencies baseRequestDependencies) 
            : base(baseRequestDependencies)
        {
            EnableRequestDocumentControl();
        }
    }
}
