﻿using System.Collections.Generic;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Models;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSubstitution)]
    public class SubstitutionController : CardIndexController<SubstitutionViewModel, SubstitutionDto>
    {
        public SubstitutionController(
            ISubstitutionCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies,
            ITimeService timeService)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = SubstitutionResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSubstitution;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSubstitution;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSubstitution;

            var currentDate = timeService.GetCurrentDate();

            CardIndex.Settings.DefaultNewRecord = () => new SubstitutionViewModel
            {
                From = currentDate,
                To = currentDate
            };

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = SubstitutionResources.Title,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<DateRangeFilterViewModel>(FilterCodes.DateRange, SubstitutionResources.SubstitutionsBetween)
                    }
                }
            };
        }
    }
}