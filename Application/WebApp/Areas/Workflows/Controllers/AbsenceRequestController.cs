﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Attributes;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [RequestController(RequestType.AbsenceRequest)]
    public class AbsenceRequestController
        : SpecificRequestController
    {
        private readonly IAbsenceService _absenceService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IEmployeeService _employeeService;

        public AbsenceRequestController(
            BaseRequestControllerDependencies baseRequestDependencies,
            IEmploymentPeriodService employmentPeriodService,
            IOrgUnitService orgUnitService,
            IAbsenceService absenceService,
            IEmployeeService employeeService)
            : base(baseRequestDependencies)
        {
            _absenceService = absenceService;
            _employmentPeriodService = employmentPeriodService;
            _employeeService = employeeService;

            Layout.Scripts.Add("~/Areas/Workflows/Scripts/AbsenceRequest.js");

            EnableRequestDocumentControl();

            CardIndex.Settings.FilterGroups.Add(new FilterGroup
            {
                ButtonText = RequestResources.AbsenceType,
                DisplayName = RequestResources.AbsenceType,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<AbsenceTypeFilterViewModel>(Business.Workflows.Consts.FilterCodes.AbsenceRequestType, RequestResources.AbsenceTypeFilterName)
                }
            });
        }

        protected override IEnumerable<IGridViewConfiguration> CreateViewConfigurations()
        {
            var defaultView =
                new GridViewConfiguration<RequestViewModel>(RequestResource.DefaultView, "default");

            defaultView.IncludeAllColumns();
            defaultView.ExcludeColumn(r => r.RequestType);
            defaultView.ExcludeColumn(r => r.Exception);
            defaultView.ExcludeColumn(r => r.Company);
            defaultView.ExcludeColumn(r => r.OrgUnit);
            defaultView.ExcludeColumn(r => r.ContractType);
            defaultView.ExcludeColumn(r => r.RejectionReason);
            defaultView.ExcludeColumn(r => r.DocumentCount);
            defaultView.IsDefault = true;

            yield return defaultView;
        }

        protected override IList<ClientSideOperation<RequestViewModel>> RefreshFormAfterChange(RequestViewModel viewModel)
        {
            var result = base.RefreshFormAfterChange(viewModel);
            var absenceRequest = (AbsenceRequestViewModel)viewModel.RequestObjectViewModel;

            if (absenceRequest.To < absenceRequest.From)
            {
                result.Add(
                    new ClientSideValueChangeOperation<RequestViewModel>(
                        v =>
                            ((AbsenceRequestViewModel)v.RequestObjectViewModel).To,
                            absenceRequest.From.ToShortDateString()));
            }

            absenceRequest.MaxAllowedHoursToReport = _employmentPeriodService
                .GetContractedHours(
                    _employeeService.GetEmployeeIdByUserId(absenceRequest.AffectedUserId).Value,
                    absenceRequest.From,
                    absenceRequest.To)
                .Sum(x => x.Hours);

            result.Add(
                new ClientSideValueChangeOperation<RequestViewModel>(
                    v =>
                        ((AbsenceRequestViewModel)v.RequestObjectViewModel).Hours,
                        absenceRequest.MaxAllowedHoursToReport.ToString()));

            result.Add(
                new ClientSideEditableOperation<RequestViewModel>(
                    v => ((AbsenceRequestViewModel)v.RequestObjectViewModel).Hours,
                    _absenceService.IsHourlyReportingAllowed(absenceRequest.AbsenceTypeId, absenceRequest.From, absenceRequest.To)));

            return result;
        }

        [AtomicAuthorize(SecurityRoleType.CanViewAbsenceRequest)]
        public override ActionResult List(GridParameters parameters)
        {
            return base.List(parameters);
        }
    }
}