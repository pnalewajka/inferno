﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Workflows.Controllers
{
    [Identifier("Pickers.Approval")]
    [AtomicAuthorize(SecurityRoleType.CanViewRequest)]
    public class ApprovalPickerController : ReadOnlyCardIndexController<ApprovalViewModel, ApprovalDto>
    {
        public ApprovalPickerController(IApprovalCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations().ToList();
        }

        private IEnumerable<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            var pickerView = new GridViewConfiguration<ApprovalViewModel>(RequestResource.DefaultView, "default");
            pickerView.IncludeColumn(a => a.ApproverFullName);
            pickerView.IncludeColumn(a => a.Status);
            pickerView.IsDefault = true;

            yield return pickerView;
        }
    }
}