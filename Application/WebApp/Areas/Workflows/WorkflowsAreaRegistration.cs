﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Workflows
{
    public class WorkflowsAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Workflows";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Workflows_before_renaming",
                "Workflows/DataChangeRequest/{*path}",
                new { controller = "DataChangeRequest", action = "RedirectToNewController" }
            );
            context.MapRoute(
                "Workflows_default",
                "Workflows/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            WorkflowsBundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}