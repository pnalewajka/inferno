﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Models.SubstitutionViewModel")]
    public class SubstitutionViewModel : IValidatableObject
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ReplacedUserFullName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string SubstituteUserFullName { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(SubstitutionResources.ReplacedUserLabel), typeof(SubstitutionResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url+'&is-actively-working=true'")]
        public long ReplacedUserId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(SubstitutionResources.SubstituteUserLabel), typeof(SubstitutionResources))]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url+'&is-actively-working=true'")]
        public long SubstituteUserId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(GeneralResources.From), typeof(GeneralResources))]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime From { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(GeneralResources.To), typeof(GeneralResources))]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime To { get; set; }

        public override string ToString()
        {
            return $"{ReplacedUserFullName} - {SubstituteUserFullName}, {From.ToShortDateString()} - {To.ToShortDateString()}";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ReplacedUserId == SubstituteUserId)
            {
                yield return new PropertyValidationResult(SubstitutionResources.UsersAreTheSame, () => SubstituteUserId);
            }

            if (From > To)
            {
                yield return new PropertyValidationResult(GeneralResources.ToDateShouldBeGreaterThanFromDate, () => To);
            }
        }
    }
}
