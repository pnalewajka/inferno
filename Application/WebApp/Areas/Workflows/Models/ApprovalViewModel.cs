﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ApprovalViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("status")]
        public ApprovalStatus Status { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription => Status.GetDescription();

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("email")]
        public string ApproverEmail { get; set; }

        [DisplayNameLocalized(nameof(ActionOnBehalfResources.ApproverFullName), typeof(ActionOnBehalfResources))]
        [JsonProperty("fullName")]
        public string ApproverFullName { get; set; }

        [JsonProperty("forcedByUserId")]
        public long? ForcedByUserId { get; set; }

        [JsonProperty("forcedByUserEmail")]
        public string ForcedByUserEmail { get; set; }

        [JsonProperty("forcedByUserFullName")]
        public string ForcedByUserFullName { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName
        {
            get
            {
                if (ForcedByUserId != null)
                {
                    return string.Format(ActionOnBehalfResources.OnBehalfOfApproverFormat, ForcedByUserFullName, ApproverFullName);
                }

                return ApproverFullName;
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
