﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestHistoryEntryDtoToRequestHistoryEntryViewModelMapping : ClassMapping<RequestHistoryEntryDto, RequestHistoryEntryViewModel>
    {
        private readonly IJiraService _jiraService = null;
        private readonly ISystemParameterService _systemParameterService;

        public RequestHistoryEntryDtoToRequestHistoryEntryViewModelMapping(
            IJiraService jiraService,
            ISystemParameterService systemParameterService)
        {
            _jiraService = jiraService;
            _systemParameterService = systemParameterService;

            Mapping = d => GetViewModel(d);
        }

        private string GetDescription(RequestHistoryEntryDto history)
        {
            var description = history.Type.GetDescription();

            switch (history.Type)
            {
                case RequestHistoryEntryType.Created:
                    return string.Format(description, history.ExecutingUserFullName);

                case RequestHistoryEntryType.Edited:
                    return string.Format(description, history.ExecutingUserFullName);

                case RequestHistoryEntryType.Approved:
                    return string.Format(description, history.ExecutingUserFullName, history.ApprovingUserFullName);

                case RequestHistoryEntryType.Rejected:
                    return string.Format(description, history.ExecutingUserFullName, history.ApprovingUserFullName);

                case RequestHistoryEntryType.WatchersAdded:
                    return string.Format(description, history.WatcherNames);

                case RequestHistoryEntryType.MailSent:
                    var recipients = JsonHelper.Deserialize<List<EmailRecipientDto>>(history.EmailRecipients ?? string.Empty);
                    var recipientAddresses = recipients != null && recipients.Count > 0
                        ? string.Join(", ", recipients.Select(r => r.EmailAddress))
                        : RequestResource.UnknownRecipients;

                    return string.Format(description, history.EmailSubject, recipientAddresses);

                case RequestHistoryEntryType.Executed:
                case RequestHistoryEntryType.ErrorOccurred:
                case RequestHistoryEntryType.JiraIssueCreated:
                    return description;

                default:
                    throw new ArgumentOutOfRangeException(nameof(history.Type));
            }
        }

        private RequestHistoryEntryViewModel GetViewModel(RequestHistoryEntryDto history)
        {
            var result = new RequestHistoryEntryViewModel
            {
                Id = history.Id,
                Date = history.Date,
                Description = GetDescription(history)
            };

            if (history.Type == RequestHistoryEntryType.Rejected)
            {
                result.RejectionReason = history.RejectionReason;
            }
            else if (history.Type == RequestHistoryEntryType.JiraIssueCreated)
            {
                using (var context = _jiraService.CreateReadWrite())
                {
                    var jiraUrl = _systemParameterService.GetParameter<string>(ParameterKeys.JiraReadWriteUrl);

                    try
                    {
                        var issue = context.GetIssueByKey(history.JiraIssueKey);

                        result.Description = string.Format(result.Description, issue.Summary);

                        result.JiraUrl = $"{jiraUrl}browse/{history.JiraIssueKey}";
                        result.JiraStatus = string.Format(RequestResource.HistoryStatus, issue.Status.Name);
                    }
                    catch (AggregateException exception)
                    {
                        const string unknown = "Unknown";

                        // This type of inner exception is being thrown for problems with connectivity, issue not found, etc.
                        if (!exception.InnerExceptions.Any(e => e is InvalidOperationException))
                        {
                            throw;
                        }

                        result.Description = string.Format(result.Description, unknown);
                        
                        result.JiraUrl = jiraUrl;
                        result.JiraStatus = unknown;
                    }
                }
            }

            return result;
        }
    }
}
