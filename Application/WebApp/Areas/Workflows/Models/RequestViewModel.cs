﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Workflows.DocumentMappings;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestViewModel : IRequesterData, IValidatableObject
    {
        public long Id { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RequestResource.RequesterUserId), typeof(RequestResource))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long RequestingUserId { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RequestResource.RequesterFullName), typeof(RequestResource))]
        [Hub(nameof(RequestingUserId))]
        public string RequesterFullName { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Workflows.Views.Shared.ApproverGroups")]
        public ApprovalGroupViewModel[] ApprovalGroups { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsWaitingForDecision { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RequestResource.Watchers), typeof(RequestResource))]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url+'&is-actively-working=true'")]
        public long[] WatchersUserIds { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(RequestResource.Status), typeof(RequestResource))]
        public RequestStatus Status { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RequestResources.DocumentsLabel), typeof(RequestResources))]
        [Hint("AvailableFormats", typeof(RequestResources))]
        [DocumentUpload(typeof(RequestDocumentMapping))]
        public List<DocumentViewModel> Documents { get; set; }

        [DisplayNameLocalized(nameof(RequestResource.Status), typeof(RequestResource))]
        public DisplayUrl GetStatus(UrlHelper helper)
        {
            return new DisplayUrl
            {
                Label = Status.GetDescription(),
                Action = GetUriActionToRequestHistory(helper),
                CssClass = "comment-column",
                IconCssClass = GetIconCssClass()
            };
        }

        [HiddenInput]
        [DisplayNameLocalized(nameof(RequestResource.RequestTypeLabel), typeof(RequestResource))]
        public RequestType RequestType { get; set; }

        [DisplayNameLocalized(nameof(RequestResource.RequestObjectViewModel), typeof(RequestResource))]
        [Visibility(VisibilityScope.Form)]
        [TypeResolution(IndicatorName: nameof(GetRequestObjectViewModelType))]
        public object RequestObjectViewModel { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        [DisplayNameLocalized(nameof(RequestResource.Description), typeof(RequestResource))]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public string RequestName { get; set; }

        [StringLength(255)]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RequestResource.RejectionReason), typeof(RequestResource))]
        public string RejectionReason { get; set; }

        [StringLength(512)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.Exception), typeof(RequestResource))]
        public string Exception { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.Company), typeof(RequestResource))]
        public string Company { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.OrgUnit), typeof(RequestResource))]
        public string OrgUnit { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.ContractType), typeof(RequestResource))]
        public string ContractType { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long[] AffectedUserIds { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.AffectedUserFullName), typeof(RequestResource))]
        [Hub(nameof(AffectedUserIds))]
        public string[] AffectedUserFullNames { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasHistoryEntries { get; set; }

        [NonSortable]
        [ApproversCell]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.Approvers), typeof(RequestResource))]
        public ApprovalGroupViewModel[] Approvers
        {
            get { return ApprovalGroups; }
        }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResource.DeferredUntil), typeof(RequestResource))]
        public DateTime? DeferredUntil { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public bool IsWaitingForDirectUserDecision { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public bool CanBeEdited { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public bool CanBeDeleted { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public bool CanBeSubmitted { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public RequestSaveAction SaveActionType { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [HiddenInput]
        public byte[] Timestamp { get; set; }

        [NonSortable]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(RequestResources.DocumentsLabel), typeof(RequestResources))]
        public long DocumentCount
        {
            get { return Documents.Count; }
        }

        public RequestViewModel()
        {
            ApprovalGroups = new ApprovalGroupViewModel[0];
            WatchersUserIds = new long[0];
            Documents = new List<DocumentViewModel>();
        }

        public Type GetRequestObjectViewModelType()
        {
            var requestServices = ReflectionHelper.ResolveAll<IRequestService>();
            var requestService = requestServices.SingleOrDefault(s => s.RequestType == RequestType);

            if (requestService == null)
            {
                throw new InvalidDataException($"Unknown data change request type: {RequestType}");
            }

            return TypeHelper.FindTypeInLoadedAssemblies(requestService.ViewModelTypeName);
        }

        private string GetIconCssClass()
        {
            switch (Status)
            {
                case RequestStatus.Draft:
                    return "fa fa-hourglass-o";

                case RequestStatus.Pending:
                    return "fa fa-hourglass-half";

                case RequestStatus.Approved:
                    return "fa fa-thumbs-o-up";

                case RequestStatus.Rejected:
                    return "fa fa-thumbs-o-down";

                case RequestStatus.Completed:
                    return "fa fa-check-circle-o";

                case RequestStatus.Error:
                    return "fa fa-exclamation-triangle";

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private UriAction GetUriActionToRequestHistory(UrlHelper helper)
        {
            if (!HasHistoryEntries)
            {
                return null;
            }

            return helper.UriActionGet(nameof(RequestHistoryController.List),
                RoutingHelper.GetControllerName<RequestHistoryController>(),
                KnownParameter.None,
                new[] { RoutingHelper.ParentIdParameterName, Id.ToString() });
        }

        public override string ToString()
        {
            return Description;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var documentTypes = new[] {
                MimeHelper.PdfFile,
                MimeHelper.OfficeDocumentWord,
                MimeHelper.PngImageFile,
                MimeHelper.JpegImageFile,
                MimeHelper.OldOfficeDocumentWord,
            };
            
            if (Documents.Any(d => !documentTypes.Contains(d.ContentType)))
            {
                yield return new ValidationResult(RequestResource.BadFileFormat);
            }
        }
    }
}