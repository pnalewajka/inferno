﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Models.ActionOnBehalfViewModel")]
    public class ActionOnBehalfViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ActionOnBehalfResources.OnBehalfOf), typeof(ActionOnBehalfResources))]
        [ValuePicker(Type = typeof(ApprovalPickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name = " + nameof(Id) + "]').val()")]
        public long ApprovalId { get; set; }

        [DisplayNameLocalized(nameof(ActionOnBehalfResources.Action), typeof(ActionOnBehalfResources))]
        [RequiredEnum(typeof(OnBehalfAction))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<OnBehalfAction>))]
        public OnBehalfAction ForceAction { get; set; }

        [Hint("OnlyForRejections", typeof(ActionOnBehalfResources))]
        [DisplayNameLocalized(nameof(RequestResource.RejectionReason), typeof(RequestResource))]
        [Multiline]
        public string RejectionReason { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var issues = new List<ValidationResult>();

            if (ForceAction == OnBehalfAction.Reject && string.IsNullOrWhiteSpace(RejectionReason))
            {
                issues.Add(new ValidationResult(
                    ActionOnBehalfResources.RejectionReasonRequired_ValidationError,
                    new List<string> { nameof(RejectionReason) }));
            }

            return issues;
        }
    }
}