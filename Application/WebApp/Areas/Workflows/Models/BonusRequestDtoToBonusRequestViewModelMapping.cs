﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class BonusRequestDtoToBonusRequestViewModelMapping : ClassMapping<BonusRequestDto, BonusRequestViewModel>
    {
        public BonusRequestDtoToBonusRequestViewModelMapping()
        {
            Mapping = d => new BonusRequestViewModel
            {
                Id = d.Id,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                ProjectId = d.ProjectId,
                EmployeeId = d.EmployeeId,
                BonusAmount = d.BonusAmount,
                CurrencyId = d.CurrencyId,
                Justification = d.Justification,
                Type = d.Type,
                Reinvoice = d.Reinvoice
            };
        }
    }
}