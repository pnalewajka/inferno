﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestStatusDtoToRequestStatusViewModelMapping : ClassMapping<RequestStatusDto, RequestStatusViewModel>
    {
        public RequestStatusDtoToRequestStatusViewModelMapping()
        {
            Mapping = d => new RequestStatusViewModel
            {
                Id = d.Id,
                Description = d.Description
            };
        }
    }
}
