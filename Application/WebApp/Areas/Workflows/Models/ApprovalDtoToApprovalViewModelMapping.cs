﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ApprovalDtoToApprovalViewModelMapping : ClassMapping<ApprovalDto, ApprovalViewModel>
    {
        public ApprovalDtoToApprovalViewModelMapping()
        {
            Mapping = d => new ApprovalViewModel
            {
                Id = d.Id,
                Status = d.Status,
                UserId = d.UserId,
                ApproverEmail = d.ApproverEmail,
                ApproverFullName = d.ApproverFullName,
                ForcedByUserId = d.ForcedByUserId,
                ForcedByUserEmail = d.ForcedByUserEmail,
                ForcedByUserFullName = d.ForcedByUserFullName,
            };
        }
    }
}
