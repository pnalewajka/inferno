﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestViewModelToRequestDtoMapping : ClassMapping<RequestViewModel, RequestDto>
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IDictionary<RequestType, IRequestService> _requestServices;

        public RequestViewModelToRequestDtoMapping(
            IClassMappingFactory classMappingFactory)
        {
            _classMappingFactory = classMappingFactory;
            _requestServices = ReflectionHelper.ResolveAll<IRequestService>().ToDictionary(s => s.RequestType);

            Mapping =
                v =>
                    new RequestDto
                    {
                        Id = v.Id,
                        RequestType = v.RequestType,
                        RequestObject = ConvertRequestRecordToDto(v),
                        RequestingUserId = v.RequestingUserId,
                        WatchersUserIds = v.WatchersUserIds.Distinct().ToArray(),
                        DeferredUntil = v.DeferredUntil,
                        RequesterFullName = v.RequesterFullName,
                        RejectionReason = v.RejectionReason,
                        Exception = v.Exception,
                        SaveActionType = v.SaveActionType,
                        Timestamp = v.Timestamp,
                        Documents = v.Documents
                        .Select(p => new DocumentDto
                        {
                            ContentType = p.ContentType,
                            DocumentName = p.DocumentName,
                            DocumentId = p.DocumentId,
                            TemporaryDocumentId = p.TemporaryDocumentId,
                        })
                      .ToArray(),
                    };
        }

        private IRequestObject ConvertRequestRecordToDto(RequestViewModel viewModelRecord)
        {
            var requestService = _requestServices[viewModelRecord.RequestType];
            var dtoType = requestService.DtoType;
            var viewModelToDtoMapping = _classMappingFactory.CreateMapping(viewModelRecord.RequestObjectViewModel.GetType(), dtoType);

            var requestObjectDto = (IRequestObject)viewModelToDtoMapping.CreateFromSource(viewModelRecord.RequestObjectViewModel);
            requestObjectDto.Id = viewModelRecord.Id;

            return requestObjectDto;
        }
    }
}
