﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestHistoryEntryViewModelToRequestHistoryEntryDtoMapping : ClassMapping<RequestHistoryEntryViewModel, RequestHistoryEntryDto>
    {
        public RequestHistoryEntryViewModelToRequestHistoryEntryDtoMapping()
        {
        }
    }
}
