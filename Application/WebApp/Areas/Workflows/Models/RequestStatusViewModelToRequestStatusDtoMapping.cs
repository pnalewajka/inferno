﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestStatusViewModelToRequestStatusDtoMapping : ClassMapping<RequestStatusViewModel, RequestStatusDto>
    {
        public RequestStatusViewModelToRequestStatusDtoMapping()
        {
            Mapping = v => new RequestStatusDto
            {
                Id = v.Id,
                Description = v.Description,
            };
        }
    }
}
