﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Filters.AbsenceType")]
    [DescriptionLocalized(nameof(AbsenceTypeResources.Title), typeof(AbsenceTypeResources))]
    public class AbsenceTypeFilterViewModel
    {
        [DisplayNameLocalized(nameof(AbsenceTypeResources.Title), typeof(AbsenceTypeResources))]
        [ValuePicker(Type = typeof(AbsenceTypePickerController))]
        [Render(Size = Size.Large)]
        public long AbsenceTypeId { get; set; }

        public override string ToString()
        {
            return AbsenceTypeResources.Title;
        }
    }
}