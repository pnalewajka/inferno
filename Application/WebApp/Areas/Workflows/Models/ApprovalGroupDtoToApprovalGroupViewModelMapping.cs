﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ApprovalGroupDtoToApprovalGroupViewModelMapping : ClassMapping<ApprovalGroupDto, ApprovalGroupViewModel>
    {
        public ApprovalGroupDtoToApprovalGroupViewModelMapping(IClassMapping<ApprovalDto, ApprovalViewModel> approvalMapping)
        {
            Mapping = d => new ApprovalGroupViewModel
            {
                Id = d.Id,
                Mode = d.Mode,
                Approvals = d.Approvals.Select(approvalMapping.CreateFromSource).ToArray(),
            };
        }
    }
}
