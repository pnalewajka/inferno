﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ExpenseRequestViewModelToExpenseRequestDtoMapping : ClassMapping<ExpenseRequestViewModel, ExpenseRequestDto>
    {
        public ExpenseRequestViewModelToExpenseRequestDtoMapping()
        {
            Mapping = v => new ExpenseRequestDto
            {
                Id = v.Id,
                ProjectId = v.ProjectId,
                EmployeeId = v.EmployeeId,
                Date = v.Date,
                Amount = v.Amount,
                CurrencyId = v.CurrencyId,
                CurrencyDisplayName = v.CurrencyDisplayName,
                Justification = v.Justification
            };
        }
    }
}