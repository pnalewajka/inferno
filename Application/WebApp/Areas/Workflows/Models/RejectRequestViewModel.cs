﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Models.RejectRequest")]
    public class RejectRequestViewModel
    {
        [HiddenInput]
        public long Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        [Multiline(5)]
        [DisplayNameLocalized(nameof(RequestResource.RejectionReason), typeof(RequestResource))]
        public string Reason { get; set; }
    }
}