﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestDtoToRequestViewModelMapping : ClassMapping<RequestDto, RequestViewModel>
    {
        private readonly IClassMappingFactory _classMappingFactory;
        IClassMapping<ApprovalGroupDto, ApprovalGroupViewModel> _approvalClassMapping;
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IRequestWorkflowService _requestWorkflowService;

        public RequestDtoToRequestViewModelMapping(
            IClassMappingFactory classMappingFactory,
            IRequestServiceResolver requestServiceResolver,
            IClassMapping<ApprovalGroupDto, ApprovalGroupViewModel> approvalClassMapping,
            IPrincipalProvider principalProvider)
        {
            _classMappingFactory = classMappingFactory;
            _approvalClassMapping = approvalClassMapping;
            _principalProvider = principalProvider;
            _requestServiceResolver = requestServiceResolver;
            _requestWorkflowService = ReflectionHelper.ResolveInterface<IRequestWorkflowService>();

            Mapping = d => GetViewModel(d);
        }

        private RequestViewModel GetViewModel(RequestDto d)
        {
            var requestService = _requestServiceResolver.GetByServiceType(d.RequestType);

            return new RequestViewModel
            {
                Id = d.Id,
                Status = d.Status,
                RequestType = d.RequestType,
                RequestingUserId = d.RequestingUserId,
                RequesterFullName = d.RequesterFullName,
                ApprovalGroups = d.ApprovalGroups.Select(_approvalClassMapping.CreateFromSource).ToArray(),
                WatchersUserIds = d.WatchersUserIds,
                DeferredUntil = d.DeferredUntil,
                RejectionReason = d.RejectionReason,
                Description = d.Description != null ? d.Description.ToString() : "",
                RequestName = GetRequestName(d, requestService),
                Exception = d.Exception,
                Company = d.Company,
                OrgUnit = d.OrgUnit,
                ContractType = d.ContractType != null ? d.ContractType.ToString() : "",
                AffectedUserIds = d.AffectedUserIds,
                AffectedUserFullNames = d.AffectedUserFullNames,
                HasHistoryEntries = d.HasHistoryEntries,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                RequestObjectViewModel = GetRequestObjectViewModel(d, requestService),
                IsWaitingForDirectUserDecision = GetIsWaitingForDirectUserDecision(d),
                IsWaitingForDecision = d.CurrentApprovalGroupId.HasValue
                                        && d.ApprovalGroups.Single(g => g.Id == d.CurrentApprovalGroupId).Approvals
                                            .All(a => a.Status != ApprovalStatus.Rejected),
                CanBeEdited = GetCanBeEdited(d, requestService),
                CanBeDeleted = GetCanBeDeleted(d, requestService),
                CanBeSubmitted = GetCanBeSubmitted(d, requestService),
                Documents = (d.Documents ?? new DocumentDto[0])
                .Select(
                 p => new DocumentViewModel
                 {
                     ContentType = p.ContentType,
                     DocumentName = p.DocumentName,
                     DocumentId = p.DocumentId,
                 })
                     .ToList(),
            };
        }

        private bool GetIsWaitingForDirectUserDecision(RequestDto requestDto)
        {
            return _requestWorkflowService.IsWaitingForDirectUserDecision(requestDto.Id, _principalProvider.Current.Id.Value);
        }

        private bool GetCanBeEdited(RequestDto requestDto, IRequestService requestService)
        {
            return requestService.CanBeEdited(requestDto, _principalProvider.Current.Id.Value);
        }

        private bool GetCanBeDeleted(RequestDto requestDto, IRequestService requestService)
        {
            return requestService.CanBeDeleted(requestDto, _principalProvider.Current.Id.Value);
        }

        private bool GetCanBeSubmitted(RequestDto requestDto, IRequestService requestService)
        {
            return requestService.CanBeSubmitted(requestDto, _principalProvider.Current.Id.Value);
        }

        private object GetRequestObjectViewModel(RequestDto requestDto, IRequestService requestService)
        {
            if (requestDto.RequestObject == null)
            {
                return null;
            }

            var dtoType = requestService.DtoType;
            var viewModelType = TypeHelper.FindTypeInLoadedAssemblies(requestService.ViewModelTypeName);
            var dtoToViewModelMapping = _classMappingFactory.CreateMapping(dtoType, viewModelType);

            return dtoToViewModelMapping.CreateFromSource(requestDto.RequestObject);
        }

        private string GetRequestName(RequestDto requestDto, IRequestService requestService)
        {
            return requestService.RequestName;
        }
    }
}
