﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestHistoryEntryViewModel
    {
        public long Id { get; set; }

        [StringFormat("{0:g}")]
        [Render(GridCssClass = "date-time-column")]
        [DisplayNameLocalized(nameof(RequestResource.HistoryDate), typeof(RequestResource))]
        [Visibility(VisibilityScope.Grid)]
        public DateTime Date { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(RequestResource.HistoryDescription), typeof(RequestResource))]
        [Visibility(VisibilityScope.Grid)]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(RequestResource.HistoryAdditionalInformation), typeof(RequestResource))]
        public DisplayUrl GetAdditionalInformation(UrlHelper helper)
        {
            return new DisplayUrl
            {
                Label = JiraStatus ?? RejectionReason,
                Action = JiraStatus != null ? new UriAction(JiraUrl) : null
            };
        }

        [Visibility(VisibilityScope.None)]
        public string JiraStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public string JiraUrl { get; set; }

        [Visibility(VisibilityScope.None)]
        public string RejectionReason { get; set; }
    }
}
