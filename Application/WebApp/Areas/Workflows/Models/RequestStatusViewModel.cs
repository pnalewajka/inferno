﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class RequestStatusViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(RequestResource.Status), typeof(RequestResource))]
        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}
