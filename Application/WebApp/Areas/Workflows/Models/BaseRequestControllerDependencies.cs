﻿using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class BaseRequestControllerDependencies : BaseRequestControllerDependencies<IRequestCardIndexDataService>
    {
        public BaseRequestControllerDependencies(IRequestCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies, IRequestWorkflowService requestWorkflowService, IRequestServiceResolver requestServiceResolver, IClassMappingFactory classMappingFactory) : base(cardIndexDataService, dependencies, requestWorkflowService, requestServiceResolver, classMappingFactory)
        {
        }
    }

    public class BaseRequestControllerDependencies<T> where T : IRequestCardIndexDataService
    {
        public BaseRequestControllerDependencies(T cardIndexDataService,
            IBaseControllerDependencies dependencies,
            IRequestWorkflowService requestWorkflowService,
            IRequestServiceResolver requestServiceResolver,
            IClassMappingFactory classMappingFactory)
        {
            CardIndexDataService = cardIndexDataService;
            Dependencies = dependencies;
            RequestWorkflowService = requestWorkflowService;
            RequestServiceResolver = requestServiceResolver;
            ClassMappingFactory = classMappingFactory;
        }

        public T CardIndexDataService { get; private set; }

        public IBaseControllerDependencies Dependencies { get; private set; }

        public IRequestWorkflowService RequestWorkflowService { get; private set; }

        public IRequestServiceResolver RequestServiceResolver { get; private set; }

        public IClassMappingFactory ClassMappingFactory { get; private set; }
    }
}