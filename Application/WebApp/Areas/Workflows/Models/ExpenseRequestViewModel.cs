﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Models.ExpenseRequestViewModel")]
    public class ExpenseRequestViewModel
    {
        public long Id { get; set; }

        [Required]
        [UpdatesApprovers]
        [Render(GridCssClass = "no-break-column")]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(ExpenseRequestResource.Project), typeof(ExpenseRequestResource))]
        public long ProjectId { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(ExpenseRequestResource.Employee), typeof(ExpenseRequestResource))]
        public long EmployeeId { get; set; }

        [Required]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "date-column")]
        public DateTime Date { get; set; }

        [Required]
        [MinValue(1)]
        [DisplayNameLocalized(nameof(ExpenseRequestResource.Amount), typeof(ExpenseRequestResource))]
        public decimal Amount { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(ExpenseRequestResource.Currency), typeof(ExpenseRequestResource))]
        public long CurrencyId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string CurrencyDisplayName { get; set; }

        [Required]
        [MaxLength(255)]
        [Multiline(3)]
        [Render(GridCssClass = "remaining-space-column")]
        [DisplayNameLocalized(nameof(ExpenseRequestResource.Justification), typeof(ExpenseRequestResource))]
        public string Justification { get; set; }
    }
}
