﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class TaxDeductibleCostRequestDtoToTaxDeductibleCostRequestViewModelMapping : ClassMapping<TaxDeductibleCostRequestDto, TaxDeductibleCostRequestViewModel>
    {
        public TaxDeductibleCostRequestDtoToTaxDeductibleCostRequestViewModelMapping()
        {
            Mapping = d => new TaxDeductibleCostRequestViewModel
            {
                Id = d.Id,
                Month = d.Month,
                Year = d.Year,
                AffectedUserFullName = d.AffectedUserFullName,
                ProjectId = d.ProjectId,
                WorkName = d.WorkName,
                Hours = d.Hours,
                Description = d.Description,
                UrlField = d.UrlField
            };
        }
    }
}