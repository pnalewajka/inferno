﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ApprovalGroupViewModelToApprovalGroupDtoMapping : ClassMapping<ApprovalGroupViewModel, ApprovalGroupDto>
    {
        public ApprovalGroupViewModelToApprovalGroupDtoMapping(IClassMapping<ApprovalViewModel, ApprovalDto> approvalMapping)
        {
            Mapping = v => new ApprovalGroupDto
            {
                Id = v.Id,
                Mode = v.Mode,
                Approvals = v.Approvals.Select(approvalMapping.CreateFromSource).ToArray(),
            };
        }
    }
}
