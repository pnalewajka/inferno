﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Models.BonusRequestViewModel")]
    public class BonusRequestViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "no-break-column")]
        [DisplayNameLocalized(nameof(BonusRequestResource.StartDate), typeof(BonusRequestResource))]
        public DateTime StartDate { get; set; }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "no-break-column")]
        [DisplayNameLocalized(nameof(BonusRequestResource.EndDate), typeof(BonusRequestResource))]
        public DateTime? EndDate { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BonusRequestResource.Type), typeof(BonusRequestResource))]
        [UpdatesApprovers]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(EnumBasedListItemProvider<BonusType>))]
        public BonusType Type { get; set; }

        [Required]
        [UpdatesApprovers]
        [Render(GridCssClass = "no-break-column")]
        [ValuePicker(Type = typeof(ProjectPickerController), OnResolveUrlJavaScript = "url += '&project-status=fixed&only-active-projects=true&only-projects-with-key=true'")]
        [DisplayNameLocalized(nameof(BonusRequestResource.Project), typeof(BonusRequestResource))]
        public long ProjectId { get; set; }

        [Required]
        [UpdatesApprovers]
        [Render(GridCssClass = "no-break-column")]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(BonusRequestResource.Employee), typeof(BonusRequestResource))]
        public long EmployeeId { get; set; }

        [Required]
        [MinValue(1)]
        [Hint(nameof(BonusRequestResource.BonusAmountFieldHint), typeof(BonusRequestResource))]
        [DisplayNameLocalized(nameof(BonusRequestResource.BonusAmount), typeof(BonusRequestResource))]
        public decimal BonusAmount { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(BonusRequestResource.Currency), typeof(BonusRequestResource))]
        public long CurrencyId { get; set; }

        [Required]
        [MaxLength(255)]
        [Multiline(3)]
        [Render(GridCssClass = "remaining-space-column")]
        [DisplayNameLocalized(nameof(BonusRequestResource.Justification), typeof(BonusRequestResource))]
        public string Justification { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmployeeFullName { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BonusRequestResource.Reinvoice), typeof(BonusRequestResource))]
        [RadioGroup(ItemProvider = typeof(YesNoItemProvider))]
        public bool Reinvoice { get; set; }

        public override string ToString()
        {
            return string.Format(BonusRequestResource.BonusDescription, EmployeeFullName, StartDate, EndDate);
        }
    }
}