﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    [Identifier("Models.TaxDeductibleCostRequestViewModel")]
    public class TaxDeductibleCostRequestViewModel
    {
        public long Id { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(TimeReportResources.AffectedUser), typeof(TimeReportResources))]
        public string AffectedUserFullName { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.MonthLabel), typeof(TimeReportResources))]
        [Range(1, 12, ErrorMessageResourceName = nameof(TimeReportResources.MonthRangeValidationMessage), ErrorMessageResourceType = typeof(TimeReportResources))]
        public int Month { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.YearLabel), typeof(TimeReportResources))]
        [Range(2016, 2050, ErrorMessageResourceName = nameof(TimeReportResources.YearRangeValidationMessage), ErrorMessageResourceType = typeof(TimeReportResources))]
        public int Year { get; set; }

        [DisplayNameLocalized(nameof(TaxDeductibleCostRequestResource.ProjectLabel), typeof(TaxDeductibleCostRequestResource))]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public long ProjectId { get; set; }

        [DisplayNameLocalized(nameof(TaxDeductibleCostRequestResource.WorkNameLabel), typeof(TaxDeductibleCostRequestResource))]
        public string WorkName { get; set; }

        [DisplayNameLocalized(nameof(TaxDeductibleCostRequestResource.HourLabel), typeof(TaxDeductibleCostRequestResource))]
        public decimal Hours { get; set; }

        [DisplayNameLocalized(nameof(RequestResource.Description), typeof(RequestResource))]
        [MaxLength(2000)]
        [Multiline]
        public string Description { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(TaxDeductibleCostRequestResource.UrlFieldLabel), typeof(TaxDeductibleCostRequestResource))]
        public string UrlField { get; set; }
    }
}