﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ExpenseRequestDtoToExpenseRequestViewModelMapping : ClassMapping<ExpenseRequestDto, ExpenseRequestViewModel>
    {
        public ExpenseRequestDtoToExpenseRequestViewModelMapping()
        {
            Mapping = d => new ExpenseRequestViewModel
            {
                Id = d.Id,
                ProjectId = d.ProjectId,
                Date = d.Date,
                EmployeeId = d.EmployeeId,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                CurrencyDisplayName = d.CurrencyDisplayName,
                Justification = d.Justification
            };
        }
    }
}
