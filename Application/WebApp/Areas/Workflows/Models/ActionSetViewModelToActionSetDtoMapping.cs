﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ActionSetViewModelToActionSetDtoMapping : ClassMapping<ActionSetViewModel, ActionSetDto>
    {
        public ActionSetViewModelToActionSetDtoMapping()
        {
            Mapping = v => new ActionSetDto
            {
                Id = v.Id,
                Code = v.Code,
                Description = v.Description,
                TriggeringRequestType = v.TriggeringRequestType,
                TriggeringRequestStatusIds = v.TriggeringRequestStatusIds,
                Definition = v.Definition,
                Timestamp = v.Timestamp
            };
        }
    }
}
