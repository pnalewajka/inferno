﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ActionSetDtoToActionSetViewModelMapping : ClassMapping<ActionSetDto, ActionSetViewModel>
    {
        public ActionSetDtoToActionSetViewModelMapping()
        {
            Mapping = d => new ActionSetViewModel
            {
                Id = d.Id,
                Code = d.Code,
                Description = d.Description,
                TriggeringRequestType = d.TriggeringRequestType,
                TriggeringRequestStatusIds = d.TriggeringRequestStatusIds,
                Definition = d.Definition,
                Timestamp = d.Timestamp
            };
        }
    }
}
