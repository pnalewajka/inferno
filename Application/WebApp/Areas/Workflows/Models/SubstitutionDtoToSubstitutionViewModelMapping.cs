﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class SubstitutionDtoToSubstitutionViewModelMapping : ClassMapping<SubstitutionDto, SubstitutionViewModel>
    {
        public SubstitutionDtoToSubstitutionViewModelMapping()
        {
            Mapping = d => new SubstitutionViewModel
            {
                Id = d.Id,
                ReplacedUserId = d.ReplacedUserId,
                SubstituteUserId = d.SubstituteUserId,
                ReplacedUserFullName = d.ReplacedUserFullName,
                SubstituteUserFullName = d.SubstituteUserFullName,
                From = d.From,
                To = d.To,
                Timestamp = d.Timestamp
            };
        }
    }
}
