﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class SubstitutionViewModelToSubstitutionDtoMapping : ClassMapping<SubstitutionViewModel, SubstitutionDto>
    {
        public SubstitutionViewModelToSubstitutionDtoMapping()
        {
            Mapping = v => new SubstitutionDto
            {
                Id = v.Id,
                ReplacedUserId = v.ReplacedUserId,
                SubstituteUserId = v.SubstituteUserId,
                ReplacedUserFullName =  v.ReplacedUserFullName,
                SubstituteUserFullName = v.SubstituteUserFullName,
                From = v.From,
                To = v.To,
                Timestamp = v.Timestamp
            };
        }
    }
}
