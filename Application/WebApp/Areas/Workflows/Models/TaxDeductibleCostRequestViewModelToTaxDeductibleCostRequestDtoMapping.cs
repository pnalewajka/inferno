﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class TaxDeductibleCostRequestViewModelToTaxDeductibleCostRequestDtoMapping : ClassMapping<TaxDeductibleCostRequestViewModel, TaxDeductibleCostRequestDto>
    {
        public TaxDeductibleCostRequestViewModelToTaxDeductibleCostRequestDtoMapping(IPrincipalProvider principalProvider)
        {
            Mapping = v => new TaxDeductibleCostRequestDto
            {
                Id = v.Id,
                Month = v.Month,
                Year = v.Year,
                AffectedUserId = principalProvider.Current.Id.Value,
                AffectedUserFullName = v.AffectedUserFullName,
                ProjectId = v.ProjectId,
                WorkName = v.WorkName,
                Hours = v.Hours,
                Description = v.Description,
                UrlField = v.UrlField
            };
        }
    }
}
