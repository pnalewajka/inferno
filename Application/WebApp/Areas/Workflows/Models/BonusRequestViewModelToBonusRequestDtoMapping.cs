﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class BonusRequestViewModelToBonusRequestDtoMapping : ClassMapping<BonusRequestViewModel, BonusRequestDto>
    {
        public BonusRequestViewModelToBonusRequestDtoMapping()
        {
            Mapping = v => new BonusRequestDto
            {
                Id = v.Id,
                StartDate = v.StartDate,
                EndDate = v.EndDate,
                ProjectId = v.ProjectId,
                EmployeeId = v.EmployeeId,
                BonusAmount = v.BonusAmount,
                CurrencyId = v.CurrencyId,
                Justification = v.Justification,
                Type = v.Type,
                Reinvoice = v.Reinvoice
            };
        }
    }
}