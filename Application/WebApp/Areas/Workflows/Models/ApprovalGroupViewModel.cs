﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ApprovalGroupViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("mode")]
        public ApprovalGroupMode Mode { get; set; }

        [JsonProperty("approvals")]
        public ApprovalViewModel[] Approvals { get; set; }

        public ApprovalGroupViewModel()
        {
            Approvals = new ApprovalViewModel[0];
        }

        private string getStatusSymbol(ApprovalStatus status)
        {
            switch (status)
            {
                case ApprovalStatus.Pending: return "?";
                case ApprovalStatus.Approved: return "+";
                case ApprovalStatus.Rejected: return "-";
                default: return string.Empty;
            }
        }

        public override string ToString()
        {
            return $"({string.Join($" {Mode.GetDescription()} ", Approvals.Select(a => $"{a.ApproverFullName}{getStatusSymbol(a.Status)}"))})";
        }
    }
}
