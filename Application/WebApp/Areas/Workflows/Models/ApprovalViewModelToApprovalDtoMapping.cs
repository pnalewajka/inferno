﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ApprovalViewModelToApprovalDtoMapping : ClassMapping<ApprovalViewModel, ApprovalDto>
    {
        public ApprovalViewModelToApprovalDtoMapping()
        {
            Mapping = v => new ApprovalDto
            {
                Id = v.Id,
                Status = v.Status,
                UserId = v.UserId,
                ForcedByUserId = v.ForcedByUserId,
            };
        }
    }
}
