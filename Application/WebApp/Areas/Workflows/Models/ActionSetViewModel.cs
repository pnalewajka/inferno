﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.Workflows.Models
{
    public class ActionSetViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [Required]
        [StringLength(100)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(ActionSetResources.CodeLabel), typeof(ActionSetResources))]
        public string Code { get; set; }

        [Order(1)]
        [Multiline(4)]
        [StringLength(500)]
        [DisplayNameLocalized(nameof(ActionSetResources.DescriptionLabel), typeof(ActionSetResources))]
        public string Description { get; set; }

        [Order(2)]
        [Required]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(ActionSetResources.TriggeringRequestTypeLabel), typeof(ActionSetResources))]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<RequestType>))]
        public RequestType? TriggeringRequestType { get; set; }

        [Order(3)]
        [Required]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ActionSetResources.TriggeringRequestStatusesLabel), typeof(ActionSetResources))]
        [ValuePicker(Type = typeof(RequestStatusPickerController))]
        public long[] TriggeringRequestStatusIds { get; set; }

        [Order(4)]
        [Required]
        [AllowHtml]
        [CodeEditor(SyntaxType.Razor)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ActionSetResources.DefinitionLabel), typeof(ActionSetResources))]
        public string Definition { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}
