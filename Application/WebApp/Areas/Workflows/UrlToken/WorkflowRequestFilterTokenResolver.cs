﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.WebApp.Areas.Workflows.UrlToken
{
    public class WorkflowRequestFilterTokenResolver : IUrlTokenResolver
    {
        private const string RequestScopeFilter = "%REQUEST-SCOPE-FILTER%";

        private readonly ISystemParameterService _systemParameterService;
        private readonly IRequestWorkflowService _requestWorkflowService;
        private readonly ITimeService _timeService;

        public WorkflowRequestFilterTokenResolver(
            ISystemParameterService systemParameterService,
            IRequestWorkflowService requestWorkflowService,
            ITimeService timeService)
        {
            _requestWorkflowService = requestWorkflowService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
        }

        public IEnumerable<string> GetTokens()
        {
            yield return RequestScopeFilter;
        }

        public string GetTokenValue(string token)
        {
            if (token == null || token != RequestScopeFilter)
            {
                throw new ArgumentNullException(nameof(token));
            }

            return GetDefaultDateRangeFilter();
        }

        private string GetDefaultDateRangeFilter()
        {
            var monthsOffset = _systemParameterService.GetParameter<int>(ParameterKeys.WorkflowRequestDefaultDateFilterMonthsOffset);
            var currentDate = _timeService.GetCurrentDate();
            var dateFrom = GetFilterDate(currentDate.AddMonths(-monthsOffset));
            var dateTo = GetFilterDate(currentDate.AddMonths(monthsOffset));
            var filter = $"{FilterCodes.MyOpenRequests},{FilterCodes.DateRange}&{FilterCodes.DateRange}.from={dateFrom}&{FilterCodes.DateRange}.to={dateTo}";

            return filter;
        }

        private string GetFilterDate(DateTime filterDate)
        {
            return filterDate.ToString("yyyy-MM-dd");
        }
    }
}
