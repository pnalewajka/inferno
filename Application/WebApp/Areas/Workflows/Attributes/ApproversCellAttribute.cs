﻿using System.Linq;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Workflows.Models;

namespace Smt.Atomic.WebApp.Areas.Workflows.Attributes
{
    public class ApproversCellAttribute : CellContentAttribute
    {
        private string customViewPath;

        public ApproversCellAttribute(string viewPath = null)
        {
            customViewPath = viewPath;
        }

        public class ApproverRenderingModel : CustomControlRenderingModel, ICellContent
        {
            public override string ToString()
            {
                var groups = ControlModel as ApprovalGroupViewModel[];

                return string.Join(" and ", groups.Select(g => g.ToString()));
            }
        }

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            return new ApproverRenderingModel { CssClass = "approval-groups", ControlModel = cellContentConverterContext.PropertyValue };
        }

        public override string ViewPath => customViewPath ?? "ApproverGroups.Column";

        protected override string TemplateCode => string.Empty;
    }
}