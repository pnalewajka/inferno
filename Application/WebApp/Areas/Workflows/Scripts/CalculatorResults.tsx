﻿namespace Calculator.View {

    export const Translations: {
        Calculations: string,
        CurrencyRateMultiplication: string,
        CurrencyExchangeRate: string,
        Description: string;
        RateValue: string,
        ExchangeRate: string,
        ResultValue: string,
        CurrencyCode: string,
        Date: string,
        Identifier: string,
        CalculationChangedMessage: string,
        NoCalculationsMessage: string,
        Sum: string,
        MultipleEmploymentPeriodsWarning: string,
        CalculationFailedError: string,
        DailyAllowanceRate: string,
        LumpSum: string,
        CountryName: string,
        MealsValue: string,
        Allowance: string,
        AllowanceValue: string,
        DailyAllowanceValue: string,
        DailyAllowanceIntiveGmbhValue: string,
        TotalHours: string,
        Accommodations: string,
        AllowanceUnits: string,
        KilometersMultiplication: string,
        Kilometers: string,
        HoursMultiplication: string,
        BusinessTripCompensationTotal: string,
        BusinessTripExpensesTotal: string,
    } = Globals.resources;

    export enum CostItemGroupType {
        OwnCosts = 1,
        HoursWorkedForClientBonus = 2,
        HoursWorked = 3,
        CompanyCosts = 4,
        AdvancePayments = 5,
        AdditionalAdvancePayment = 6,
        PrivateVehicleMileage = 7,
        DailyAllowances = 8,
        TimeTrackingTotal = 9,
        BusinessTripCompensationTotal = 10,
        BusinessTripExpensesTotal = 11,
        CompensationTotal = 12,
        LumpSum = 17,
    }

    export enum DiagnosticGroupType {
        Summary = 1,
        OwnCosts = 2,
        HoursWorkedForClientBonus = 3,
        HoursWorked = 4,
        LumpSum = 14,
    }

    export enum DiagnosticLevel {
        Error = -1,
        Critical = 1,
        Info = 2,
        Debug = 3
    }

    export enum RenderingHintType {
        PlainDump = 0,
        MultipleEmploymentPeriodsWarning = 1,
        HoursMultiplication = 2,
        CurrencyRateMultiplication = 3,
        CurrencyExchangeRate = 4,
        KilometersMultiplication = 5,
        DailyAllowanceRate = 6,
        DailyAllowanceValue = 7,
        OvertimeMultiplication = 8,
        LumpSumCalculation = 9,
        SickLeaveMultiplication = 10,
        CalculationFailedError = 11,
        CostCurrencyRateMultiplication = 12,
        IntiveGmbhDailyAllowanceValue = 19,
        IntiveGmbhCostCurrencyRateMultiplication = 20,
    }

    export interface ICostItem {
        group: CostItemGroupType;
        value: number;
        currencyIsoCode: string;
        diagnosticItems: IDiagnosticItem[];
    }

    export interface IDiagnosticItem {
        group: DiagnosticGroupType;
        level: DiagnosticLevel;
        type: RenderingHintType;
        values: string[];
    }

    export interface ICalculatorResult {
        costItems: ICostItem[];
        diagnosticItems: IDiagnosticItem[];
    }

    function forDiagnosticItems(diagnosticItems: IDiagnosticItem[]): void {
        let diagnosticMaxLevel = DiagnosticLevel.Info;

        const hashLevel = Number(window.location.hash.substr(1));

        if (!!hashLevel) {
            diagnosticMaxLevel = hashLevel;
        }

        const itemsToDisplay = diagnosticItems.filter(d => d.level <= diagnosticMaxLevel);
        if (itemsToDisplay.length > 0) {
            const $diagnosticContainer = $("<div class='row'></div>");

            $("[id='request-object-view-model']").append($diagnosticContainer);

            ReactDOM.render(<Ui.DiagnosticContainer items={itemsToDisplay} />, $diagnosticContainer[0]);
        }
    }

    function beforeContainer(controlName: string, element: JSX.Element): boolean {
        const $item = $(`[id='${controlName}']`);
        if (!$item.is(":visible")) {
            return false;
        }

        const $resultContainer = $("<div class='calculation-result-container-full'></div>");
        $item.parents(".form-group, .row").first().before($resultContainer);

        ReactDOM.render(<Ui.CalculationResultContainer formElementId={controlName}>{element}</Ui.CalculationResultContainer>, $resultContainer[0]);

        return true;
    }

    function forControl(controlName: string, elements: JSX.Element | JSX.Element[]): boolean {
        const $item = $(`[id='${controlName}']`);
        if (!$item.is(":visible")) {
            return false;
        }
        const isFieldset = $item.is("fieldset");
        const $controlContainer = isFieldset ? $item : $item.parents(".form-group, .row").first();
        const $resultContainer = $("<div class='calculation-result-container'></div>");

        if (!isFieldset) {
            $controlContainer.wrap("<div class='for-regular-control'></div>");
            $resultContainer.addClass("for-regular-control");
            $controlContainer.parent().css("position", "relative").append($resultContainer);
        } else {
            $controlContainer.css("position", "relative").append($resultContainer);
        }

        const calculationContainer = $resultContainer[0];

        ReactDOM.render((
            <Ui.CalculationResultContainer formElementId={controlName}>
                <div>
                    {Dante.Utils.ItemAsArray(elements).map((element, index) => <div key={index}>{element}</div>)}
                </div>
            </Ui.CalculationResultContainer>
        ), calculationContainer);

        return true;
    }

    export const FormChangedEventDispacher = new Atomic.Events.EventDispatcher<boolean>();

    export function Initialize(result: ICalculatorResult): void {
        forDiagnosticItems(result.diagnosticItems);

        const calculationsToDisplay: JSX.Element[] = [
            <Ui.HoursMultiplication costItems={result.costItems} group={CostItemGroupType.HoursWorkedForClientBonus} />,
            <Ui.DailyAllowanceValue costItems={result.costItems} group={CostItemGroupType.DailyAllowances} />,
            <Ui.IntiveGmbhDailyAllowanceValue costItems={result.costItems} group={CostItemGroupType.DailyAllowances} />,
            <Ui.DailyAllowanceRate costItems={result.costItems} group={CostItemGroupType.DailyAllowances} />,
            <Ui.LumpSumCalculation costItems={result.costItems} group={CostItemGroupType.LumpSum} />,
            <Ui.CostCurrencyRateMultiplicationComponent costItems={result.costItems} group={CostItemGroupType.OwnCosts} />,
            <Ui.IntiveGmbhCostCurrencyRateMultiplicationComponent costItems={result.costItems} group={CostItemGroupType.OwnCosts} />,
            <Ui.CurrencyExchangeRateComponent costItems={result.costItems} group={CostItemGroupType.OwnCosts} />,
            <Ui.CostCurrencyRateMultiplicationComponent costItems={result.costItems} group={CostItemGroupType.CompanyCosts} />,
            <Ui.IntiveGmbhCostCurrencyRateMultiplicationComponent costItems={result.costItems} group={CostItemGroupType.CompanyCosts} />,
            <Ui.CurrencyExchangeRateComponent costItems={result.costItems} group={CostItemGroupType.CompanyCosts} />,
            <Ui.CurrencyRateMultiplicationComponent costItems={result.costItems} group={[CostItemGroupType.AdvancePayments, CostItemGroupType.AdditionalAdvancePayment]} />,
            <Ui.CurrencyExchangeRateComponent costItems={result.costItems} group={[CostItemGroupType.AdvancePayments, CostItemGroupType.AdditionalAdvancePayment]} />,
            <Ui.KilometersMultiplication costItems={result.costItems} group={CostItemGroupType.PrivateVehicleMileage} />,
        ];

        if (calculationsToDisplay.length > 0) {
            calculationsToDisplay.unshift((
                <div>
                    <fieldset className="for-from-control">
                        <legend>{Translations.Calculations}</legend>
                    </fieldset>
                    <br />
                </div>
            ));

            forControl("request-object-view-model", calculationsToDisplay);

            $("#request-object-view-model").addClass("with-calculations");
        }

        beforeContainer("RequestObjectViewModel.business-trip-id",
            <Ui.CustomItemsTotals costItems={result.costItems} groups={[
                { displayLable: Translations.BusinessTripCompensationTotal, group: CostItemGroupType.BusinessTripCompensationTotal },
                { displayLable: Translations.BusinessTripExpensesTotal, group: CostItemGroupType.BusinessTripExpensesTotal },
            ]} />
        );

        // for any change in form
        $("[id='request-object-view-model'] [name]").on("change", () => {
            FormChangedEventDispacher.dispatch(false);
            $("#request-object-view-model").removeClass("with-calculations").addClass("with-calculations-changed");
        });
    }
}
