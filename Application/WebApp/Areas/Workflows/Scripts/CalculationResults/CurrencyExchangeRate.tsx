﻿namespace Calculator.View.Ui {
    export class CurrencyExchangeRateComponent extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.CurrencyExchangeRate);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.CurrencyExchangeRate);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.CurrencyCode),
                new Renderers.Definition(Calculator.View.Translations.Date),
                new Renderers.Definition(Calculator.View.Translations.ExchangeRate),
                new Renderers.Definition(Calculator.View.Translations.Identifier),
            ];
        }
    }
}
