﻿namespace Calculator.View.Ui {
    export class IntiveGmbhDailyAllowanceValue extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.DailyAllowanceIntiveGmbhValue);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.IntiveGmbhDailyAllowanceValue);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.Date),
                new Renderers.Definition(Calculator.View.Translations.AllowanceValue),
                new Renderers.Definition(Calculator.View.Translations.MealsValue),
                new Renderers.Definition(Calculator.View.Translations.ResultValue),
            ];
        }
    }
}
