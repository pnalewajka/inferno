﻿namespace Calculator.View.Ui {
    export class DailyAllowanceRate extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.DailyAllowanceRate);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.DailyAllowanceRate);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.CountryName),
                new Renderers.Definition(Calculator.View.Translations.Allowance),
                new Renderers.Definition(Calculator.View.Translations.ExchangeRate),
                new Renderers.Definition(Calculator.View.Translations.AllowanceValue),
            ];
        }
    }
}
