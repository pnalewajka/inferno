﻿namespace Calculator.View.Ui {
    export class DiagnosticContainer extends React.Component<{ items: Calculator.View.IDiagnosticItem[] }, { items: Calculator.View.IDiagnosticItem[] }> {
        public componentWillMount(): void {
            this.setState({
                items: this.props.items
            });

            Calculator.View.FormChangedEventDispacher.subscribe((isOpen: boolean) => {
                if (!isOpen) {
                    this.setState({ items: [] });
                }
            });
        }

        private diagnosticClassName(level: Calculator.View.DiagnosticLevel): string {
            return Dante.Utils.ClassNames({
                "alert-info": level === Calculator.View.DiagnosticLevel.Info,
                "alert-warning": level === Calculator.View.DiagnosticLevel.Critical,
                "alert-light": level === Calculator.View.DiagnosticLevel.Debug,
                "alert-danger": level === Calculator.View.DiagnosticLevel.Error,
            }, "alert");
        }

        private diagnosticMessage(item: Calculator.View.IDiagnosticItem): string | undefined {
            switch (item.type) {
                case Calculator.View.RenderingHintType.CalculationFailedError:
                    if (item.values.length === 0) {
                        return Calculator.View.Translations.CalculationFailedError;
                    } else {
                        return Calculator.View.Translations.CalculationFailedError + ": " + item.values.join(", ");
                    }
                case Calculator.View.RenderingHintType.MultipleEmploymentPeriodsWarning:
                    return Calculator.View.Translations.MultipleEmploymentPeriodsWarning;
            }

            return undefined;
        }

        private renderDiagnosticItem(item: Calculator.View.IDiagnosticItem, index: number): JSX.Element | undefined {
            const message = this.diagnosticMessage(item);

            if (message === undefined) {
                return undefined;
            }

            return (
                <div key={index} className={this.diagnosticClassName(item.level)} role="alert">
                    {message}
                </div>
            );
        }

        public render(): JSX.Element | null {
            if (!this.state.items || this.state.items.length === 0) {
                return null;
            }

            return (
                <div>
                    {this.state.items.map(this.renderDiagnosticItem.bind(this))}
                </div>
            );
        }
    }
}
