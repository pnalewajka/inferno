﻿namespace Calculator.View.Ui {
    export class IntiveGmbhCostCurrencyRateMultiplicationComponent extends SumDefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.CurrencyRateMultiplication);
        }

        protected calculateSum(props: ICalculationProps): number {
            return Parsers.ParseCostItems(props.costItems, props.group, this.props.hintType || RenderingHintType.IntiveGmbhCostCurrencyRateMultiplication).map(c => c.value).reduce((a, b) => a + b, 0);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.IntiveGmbhCostCurrencyRateMultiplication);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.Date),
                new Renderers.Definition(Calculator.View.Translations.Description),
                new Renderers.Definition(Calculator.View.Translations.RateValue),
                new Renderers.Definition(Calculator.View.Translations.ExchangeRate),
                new Renderers.Definition(Calculator.View.Translations.ResultValue),
            ];
        }
    }
}
