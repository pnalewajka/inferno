﻿namespace Calculator.View.Ui {
    export class CalculationResultContainer extends React.Component<{ formElementId: string }, { isOpen: boolean }> {
        private registerContainer(divElement: HTMLDivElement | null): void {
            if (divElement !== null) {
                const $container = $(divElement);
                const formElement = document.getElementById(this.props.formElementId);

                if (!!formElement) {
                    const $formElement = $(formElement);
                    ($formElement.is("fieldset") ? $formElement : $formElement.parents(".for-regular-control")).css("minHeight", $container.height());
                }
            }
        }

        private renderClosed(): JSX.Element {
            return (
                <span className="fa-stack fa-lg" title={Calculator.View.Translations.CalculationChangedMessage}>
                    <i className="fa fa-circle fa-stack-2x"></i>
                    <i className="fa fa-question fa-stack-1x fa-inverse"></i>
                </span>
            );
        }

        public componentWillMount(): void {
            this.setState({ isOpen: true });

            Calculator.View.FormChangedEventDispacher.subscribe((isOpen: boolean) => {
                this.setState({ isOpen });
            });
        }

        public render(): JSX.Element {
            return (
                <div className="calculation-result" ref={this.registerContainer.bind(this)}>
                    { this.state.isOpen ? this.props.children : this.renderClosed() }
                </div>
            );
        }
    }
}
