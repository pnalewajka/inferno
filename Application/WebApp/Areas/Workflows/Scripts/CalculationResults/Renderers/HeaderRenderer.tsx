﻿namespace Calculator.View.Ui.Renderers {
    export function renderCalculationHeader(definitions: Definition[]): JSX.Element | undefined {
        const headers = definitions
            .filter(d => d.bodyRenderer !== null)
            .map((d, i) => (
                <td key={i}>
                    {d.displayName}
                </td>
            ));

        return (
            <thead>
                <tr>
                    {headers}
                </tr>
            </thead>
        );
    }
}
