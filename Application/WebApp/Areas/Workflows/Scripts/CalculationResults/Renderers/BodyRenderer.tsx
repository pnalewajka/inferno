﻿namespace Calculator.View.Ui.Renderers {
    export function renderCalculationBody(values: string[][], definitions: Definition[]): JSX.Element | undefined {
        const rows = values.map((v, i) => (
            <tr key={i}>
                {definitions.map((d, index) => {
                    if (d.bodyRenderer === null) {
                        return undefined;
                    }
                    const content = d.bodyRenderer(v[index]);

                    return <td key={index}>{content}</td>;
                }).filter(c => c !== undefined)}
                </tr>
        ));

        return (
            <tbody>
                {
                    rows.length > 0
                        ? rows
                        : (
                            <tr>
                                <td colSpan={definitions.filter(d => !!d.bodyRenderer).length}>
                                    {Calculator.View.Translations.NoCalculationsMessage}
                                </td>
                            </tr>
                        )
                }
            </tbody>
            );
    }
}
