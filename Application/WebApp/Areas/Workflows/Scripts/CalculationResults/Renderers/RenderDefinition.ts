﻿namespace Calculator.View.Ui.Renderers {
    type BodyRenderer = (value: string) => string | JSX.Element | null;
    function defaultBodyRender(value: string): string | JSX.Element | null {
        return value;
    }

    export class Definition {
        public readonly displayName: string;
        public readonly bodyRenderer: BodyRenderer | null;

        public constructor(displayName: string, bodyRenderer: BodyRenderer | null = defaultBodyRender) {
            this.displayName = displayName;
            this.bodyRenderer = bodyRenderer;
        }
    }
}
