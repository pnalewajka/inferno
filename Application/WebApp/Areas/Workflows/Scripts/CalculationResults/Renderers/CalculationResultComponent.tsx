﻿namespace Calculator.View.Ui {
    export interface ICalculationProps {
        costItems: ICostItem[];
        group: CostItemGroupType | CostItemGroupType[];
        hintType?: RenderingHintType;
    }

    export interface ICalculationState {
        items: string[][];
    }

    export interface ICalculateSumState {
        sum: number;
    }

    export abstract class CalculationResultComponent<P extends ICalculationProps, S extends ICalculationState> extends Dante.Components.StateComponent<P, S> {
        protected calculationTitle: string;
        protected constructor(calculationTitle: string) {
            super();

            this.calculationTitle = calculationTitle;
        }

        public componentWillMount() {
            this.updateState(this.createState(this.props));
        }

        protected createState(props: P): Partial<S> {
            return {
                items: this.displayItems(props)
            } as dynamic;
        }

        protected abstract displayItems(props: P): string[][];

        protected abstract renderDefinitions(props: P): Renderers.Definition[];

        protected renderFooter(items: string[][], definitions: Renderers.Definition[]): JSX.Element | undefined {
            return undefined;
        }

        protected renderHeader(definitions: Renderers.Definition[]): JSX.Element | undefined {
            return Renderers.renderCalculationHeader(definitions);
        }

        protected renderBody(items: string[][], definitions: Renderers.Definition[]): JSX.Element | undefined {
            return Renderers.renderCalculationBody(items, definitions);
        }

        public render(): JSX.Element | null {
            const definitions: Renderers.Definition[] = this.renderDefinitions(this.props);

            const state = this.state as S;

            if (this.state.items.length === 0) {
                return null;
            }

            return (
                <fieldset>
                    <legend>{this.calculationTitle}</legend>
                    <table className="table table-striped table-dark">
                        {this.renderHeader(definitions)}
                        {this.renderBody(this.state.items, definitions)}
                        {this.renderFooter(this.state.items, definitions)}
                    </table>
                </fieldset>
            );
        }
    }

    export abstract class SumDefaultCalculationResultComponent extends CalculationResultComponent<ICalculationProps, ICalculationState & ICalculateSumState> {
        protected abstract calculateSum(props: ICalculationProps): number;

        protected createState(props: ICalculationProps): Partial<ICalculationState & ICalculateSumState> {
            return {
                items: this.displayItems(props),
                sum: this.calculateSum(props)
            } as Partial<ICalculationState & ICalculateSumState>;
        }

        protected renderFooter(items: string[][], definitions: Renderers.Definition[]): JSX.Element | undefined {
            return items.length > 1
                ? <tfoot>
                    <tr>
                        <td colSpan={definitions.length - 1}>{Calculator.View.Translations.Sum}</td>
                        <td><b>{this.state.sum.toFixed(2)}</b></td>
                    </tr>
                </tfoot>
                : undefined;
        }
    }

    export abstract class DefaultCalculationResultComponent extends CalculationResultComponent<ICalculationProps, ICalculationState> {
    }
}
