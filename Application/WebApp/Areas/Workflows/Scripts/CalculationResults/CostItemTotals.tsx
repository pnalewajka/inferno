﻿namespace Calculator.View.Ui {

    export interface ITotalGroupSection {
        displayLable: string;
        group: CostItemGroupType;
    }

    interface ITotalGroupState {
        displayLable: string;
        value?: ICostItem;
    }

    export class CustomItemsTotals extends React.Component<{ costItems: ICostItem[], groups: ITotalGroupSection[] }, { values: ITotalGroupState[] }> {

        public componentWillMount() {
            this.setState({
                values: this.props.groups.map(g => {
                    const [ value ] = this.props.costItems.filter(c => c.group === g.group);

                    const groupItem: ITotalGroupState = {
                        displayLable: g.displayLable,
                        value
                    };

                    return groupItem;
                })
            });
        }

        public render(): JSX.Element | null {
            const valuesToDisplay = this.state.values.filter(v => v.value !== undefined);

            if (valuesToDisplay.length === 0) {
                return null;
            }

            return (
                <div className="jumbotron">
                    {valuesToDisplay.map((v, i) => (
                        <h4 key={i} className="calculation-total">
                            <span>{v.displayLable} </span>
                            <b>{v.value !== undefined ? `${v.value.value.toFixed(2)} ${v.value.currencyIsoCode}` : 0}</b>
                        </h4>
                    ))}
                </div>
            );
        }
    }
}
