﻿namespace Calculator.View.Ui {
    export class HoursMultiplication extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.HoursMultiplication);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.HoursMultiplication);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.TotalHours),
                new Renderers.Definition(Calculator.View.Translations.ExchangeRate),
                new Renderers.Definition(Calculator.View.Translations.ResultValue),
            ];
        }
    }
}
