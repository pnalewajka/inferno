﻿namespace Calculator.View.Ui {
    export class DailyAllowanceValue extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.DailyAllowanceValue);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.DailyAllowanceValue);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.CountryName),
                new Renderers.Definition(Calculator.View.Translations.TotalHours),
                new Renderers.Definition(Calculator.View.Translations.AllowanceUnits),
                new Renderers.Definition(Calculator.View.Translations.ResultValue),
            ];
        }
    }
}
