﻿namespace Calculator.View.Ui {
    export class KilometersMultiplication extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.KilometersMultiplication);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.KilometersMultiplication);
        }

        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.Kilometers),
                new Renderers.Definition(Calculator.View.Translations.ExchangeRate),
                new Renderers.Definition(Calculator.View.Translations.ResultValue),
            ];
        }
    }
}
