﻿namespace Calculator.View.Ui {
    export class LumpSumCalculation extends DefaultCalculationResultComponent {
        public constructor() {
            super(Calculator.View.Translations.LumpSum);
        }

        protected displayItems(props: ICalculationProps): string[][] {
            return Parsers.ParseCostItemValues(this.props.costItems, this.props.group, this.props.hintType || RenderingHintType.LumpSumCalculation);
        }
        protected renderDefinitions(props: ICalculationProps): Renderers.Definition[] {
            return [
                new Renderers.Definition(Calculator.View.Translations.CountryName),
                new Renderers.Definition(Calculator.View.Translations.RateValue),
                new Renderers.Definition(Calculator.View.Translations.Accommodations),
                new Renderers.Definition(Calculator.View.Translations.ResultValue),
            ];
        }
    }
}
