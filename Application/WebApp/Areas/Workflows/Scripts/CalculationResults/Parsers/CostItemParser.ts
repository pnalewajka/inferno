﻿namespace Calculator.View.Ui.Parsers {
    export function ParseCostItems(costItems: Calculator.View.ICostItem[], group: CostItemGroupType | CostItemGroupType[], hitType: RenderingHintType): Calculator.View.ICostItem[] {
        const groups = Dante.Utils.ItemAsArray(group);

        return costItems.filter(c => groups.indexOf(c.group) !== -1 && c.diagnosticItems.some(d => d.type === hitType));
    }

    export function ParseCostItemValues(costItems: Calculator.View.ICostItem[], group: CostItemGroupType | CostItemGroupType[], hitType: RenderingHintType): string[][] {
        const groups = Dante.Utils.ItemAsArray(group);

        return Dante.Utils.SelectMany(
            costItems.filter(c => groups.indexOf(c.group) !== -1),
            c => c.diagnosticItems.filter(d => d.type === hitType)
                .map(d => d.values)
        );
    }
}
