﻿module BusinessTripRequest
{
    export function toggleArrangements(): void
    {
        const visibility = $("input[name='RequestObjectViewModel.AreTravelArrangementsNeeded']").is(':checked');

        const visibilityOperation = new ClientSideOperations.ClientSideVisibilityOperation();
        visibilityOperation.isVisible = visibility;

        visibilityOperation.targetElementId = "RequestObjectViewModel.transportation-preferences";
        visibilityOperation.apply();

        visibilityOperation.targetElementId = "RequestObjectViewModel.accommodation-preferences";
        visibilityOperation.apply();

        visibilityOperation.targetElementId = "RequestObjectViewModel.itinerary-details";
        visibilityOperation.apply();
    }

    export function initialize(): void
    {
        // Method first() is crucial because there are 7 checkboxes like this and we don't want to call server 7 times
        $("input[name='RequestObjectViewModel.TransportationPreferences'][type='checkbox']").first().removeData("last-value").trigger('change');

        $("input[name='RequestObjectViewModel.AreTravelArrangementsNeeded']").removeData("last-value").trigger("change");
    }

    export function updateApproversDeparturesAndVouchers(serverResultData: {
        ApprovalGroups: WorkflowRequests.ApprovalGroup[]
    }): void
    {
        WorkflowRequests.updateApprovers(serverResultData);
        $("[name='RequestObjectViewModel.DepartureCityIds']").removeData("last-value").trigger("change");
        $("[name='RequestObjectViewModel.ForceDepartureCitiesRecalculation']").removeData("last-value").trigger("change");
    }

    $(() => {
        initialize();
    });
}
