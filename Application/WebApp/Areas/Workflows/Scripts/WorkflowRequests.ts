﻿module WorkflowRequests
{
    export enum ApprovalStatus
    {
        Pending = 0,
        Approved = 1,
        Rejected = 2
    }

    export class Approval
    {
        public userId: number;
        public status: ApprovalStatus;
        public statusDescription: string;
        public email: string;
        public fullName: string;
        public forcedByUserId: number;
        public forcedByUserEmail: string;
        public forcedByUserFullName: string;
        public displayName: string;
    }

    export enum ApprovalGroupMode
    {
        And = 0,
        Or = 1
    }

    export class ApprovalGroup
    {
        public mode: ApprovalGroupMode;
        public approvals: Approval[];
    }

    export enum RequestType
    {
        SkillChangeRequest = 0,
        AddEmployeeRequest = 1,
        AbsenceRequest = 2,
        AddHomeOfficeRequest = 3,
        OvertimeRequest = 4,
        ProjectTimeReportApprovalRequest = 5,
        BusinessTripRequest = 10,
        AssetsRequest = 13
    }

    function addApprover(approver: Approval, container: JQuery)
    {
        var className = "";
        switch (approver.status) {
            case ApprovalStatus.Pending:
                className = "pending";
                break;
            case ApprovalStatus.Approved:
                className = "approved";
                break;
            case ApprovalStatus.Rejected:
                className = "rejected";
        }

        container.append(`<li title="${approver.statusDescription}"><div class="approver ${className}">${approver.displayName}</div></li>`);
    }

    function addSeparator(text: string, container: JQuery)
    {
        container.append(`<li>&nbsp;${text}&nbsp;</li>`);
    }

    function addApproverGroup(approverGroup: ApprovalGroup, container: JQuery)
    {
        var separatorText = "";
        switch (approverGroup.mode)
        {
            case ApprovalGroupMode.And:
                separatorText = Globals.resources.And;
                break;
            case ApprovalGroupMode.Or:
                separatorText = Globals.resources.Or;
                break;
        }

        if (container.children().last().length > 0) {
            container.append(`<span>&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;</span>`);
        }

        container.append(`<ul class="approvers"></ul>`);
        var groupElement = container.children().last();

        var isFirstApprover = true;
        approverGroup.approvals.forEach((approver: Approval) =>
        {
            if (!isFirstApprover)
            {
                addSeparator(separatorText, groupElement);
            }
            else
            {
                isFirstApprover = false;
            }

            addApprover(approver, groupElement);
        });
    }

    function addNoApproversMessage(container: JQuery)
    {
        container.append(`<ul class="approvers"><li class="empty-text">${Globals.resources.NoApprovers}</li></ul>`);
    }

    export function updateApprovers(serverResultData: { ApprovalGroups: ApprovalGroup[] }) {

        var approverGroupContainer = $(document.getElementById("approverGroupContainer") as HTMLElement);
        approverGroupContainer.children().remove(); // Clear old approver groups

        if (serverResultData.ApprovalGroups.length > 0)
        {
            serverResultData.ApprovalGroups.forEach((approverGroup: ApprovalGroup) => addApproverGroup(approverGroup, approverGroupContainer));
        }
        else
        {
            addNoApproversMessage(approverGroupContainer);
        }
    }
}
