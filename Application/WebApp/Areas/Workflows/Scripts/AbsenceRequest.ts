﻿module AbsenceRequest {
    let fromDate: string;
    let toDate: string;
    let affectedUser: number;

    interface IRequestObjectModel {
        AdditionalInformation: string;
        MaxAllowedHoursToReport: number;
    }

    export function resolveAbsenceTypeUrl(absenceId: number, url: string) {
        return `${url}&from=${fromDate}&to=${toDate}&on-behalf-of=${affectedUser}&current-employee-only=true`;
    }

    export function setAdditionalInformation(additionalInformation: string) {
        $("[id='RequestObjectViewModel.additional-information'] > div").html(additionalInformation);
    }

    function selectedAbsenceType(): number {
        let selectedTypeId: number = $("[name='RequestObjectViewModel.AbsenceTypeId']").val();
        return selectedTypeId;
    }

    function clearSelectedAbsenceType() {
        $("[name='RequestObjectViewModel.AbsenceTypeId']").val('');
        $("[id='RequestObjectViewModel.absence-type-id']").val('');
    }

    export function afterValueChange() {
        fromDate = getDatePickerDateString($("input[name='RequestObjectViewModel.From']"));
        toDate = getDatePickerDateString($("input[name='RequestObjectViewModel.To']"));
        validateAbsenceType();
    }

    function validateAbsenceType() {
        let selectedAbsence = selectedAbsenceType();

        if (!selectedAbsence) {
            return;
        }

        CommonDialogs.pleaseWait.show(20);
        $.get(Globals.resolveUrl(`~/ValuePicker/JsonList`), {
            listId: "ValuePickers.AbsenceType",
            from: fromDate,
            to: toDate,
            'on-behalf-of': affectedUser,
            'current-employee-only': true
        }, (available: { id: number, toString: string }[]) => {

            const isOnList = available.filter(a => a.id == selectedAbsence).length > 0;

            if (!isOnList) {
                clearSelectedAbsenceType();
            }

            CommonDialogs.pleaseWait.hide();
        });
    };

    function getDatePickerDateString($input: JQuery) {
        const dateFormat = Globals.getDateFormat();
        let date = $.fn.datepicker.DPGlobal.parseDate($input.val(), dateFormat);
        return UrlHelper.dateUrlParameter(date);
        
    }

    export function initialize(): void {
        affectedUser = $('[name="RequestObjectViewModel.AffectedUserId"]').change(function(this: JQuery) {
            affectedUser = $(this).val();
            validateAbsenceType();
        }).val();

        $("input[name='RequestObjectViewModel.Hours']").prop("readonly", true);

        fromDate = getDatePickerDateString($("input[name='RequestObjectViewModel.From']"));
        toDate = getDatePickerDateString($("input[name='RequestObjectViewModel.To']"));
    }

    export function handleUpdateApprovers(serverResultData: { ApprovalGroups: WorkflowRequests.ApprovalGroup[], RequestObjectViewModel: IRequestObjectModel }): void {

        WorkflowRequests.updateApprovers(serverResultData);

        var absenceRequest = serverResultData.RequestObjectViewModel;

        if (absenceRequest == null) {
            return;
        }

        if (absenceRequest.AdditionalInformation) {
            AbsenceRequest.setAdditionalInformation(absenceRequest.AdditionalInformation);
        }

        AbsenceRequest.afterValueChange();
    }

    $(() => {
        initialize();
    });
}
