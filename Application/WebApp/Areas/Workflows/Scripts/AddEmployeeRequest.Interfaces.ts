﻿declare module AddEmployeeRequest {
    export interface IOrgUnitDefaultValues {
        timeReportingMode: number;
        absenceOnlyDefaultProjectId: number;
        absenceOnlyDefaultProjectName: string;
    }

    export interface ISuggestedEmailAndLogin {
        email: string;
        login: string;
    }
}