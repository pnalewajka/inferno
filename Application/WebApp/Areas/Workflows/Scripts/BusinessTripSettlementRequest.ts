﻿module BusinessTripSettlementRequest {
    enum VehicleType {
        Car = 0,
        Motorcycle = 1,
        Moped = 2,
    }

    export function initialize(): void {
        onVehicleTypeChanged();
        initializeDefaultCurrency();
    }

    function initializeDefaultCurrency(): void {
        const defaultCurrencyIdStr: string | null = $("[name='RequestObjectViewModel.DefaultCurrencyId']").val();

        if (defaultCurrencyIdStr != null) {
            const currencyValuePickerObject: Grid.IRow = {
                id: parseInt(defaultCurrencyIdStr, 10),
                toString: $("[name='RequestObjectViewModel.DefaultCurrencyName']").val()
            };

            ValuePicker.setValue("template.RequestObjectViewModel.OwnCosts[].currency-id", currencyValuePickerObject);
            ValuePicker.setValue("template.RequestObjectViewModel.CompanyCosts[].currency-id", currencyValuePickerObject);
            ValuePicker.setValue("template.RequestObjectViewModel.AdditionalAdvancePayments[].currency-id", currencyValuePickerObject);
        }
    }

    export function onVehicleTypeChanged(): void {
        const value = parseInt($("[name='RequestObjectViewModel.PrivateVehicleMileage.VehicleType']").val());

        const operation = new ClientSideOperations.ClientSideVisibilityOperation();
        operation.isVisible = value == VehicleType.Car;
        operation.targetElementId = 'RequestObjectViewModel.PrivateVehicleMileage.is-car-engine-capacity-over900cc';
        operation.apply();
    }

    export function onPrivateVehicleMileageKilometersChange(): void {
        const inbound = parseFloat($("[name='RequestObjectViewModel.PrivateVehicleMileage.InboundKilometers']").val());
        const outbound = parseFloat($("[name='RequestObjectViewModel.PrivateVehicleMileage.OutboundKilometers']").val());
        const total = Math.round((inbound + outbound) * 100) / 100;

        $("[name='RequestObjectViewModel.PrivateVehicleMileage.TotalKilometers']").val(total || 0);
    }

    export function onParticipantChanged(): void {
        const participantId = parseInt($("[name='RequestObjectViewModel.BusinessTripParticipantId']").val());
        const paramsArray: string[] = [
            'request-type=business-trip-settlement-request',
            `participant-id=${participantId}`,
        ];

        Forms.redirect(`?${paramsArray.join("&")}`);
    }

    $(() => {
        initialize();
        var calculationResults = $("[id='RequestObjectViewModel.calculation-result']").val();

        if (!!calculationResults) {
            Calculator.View.Initialize(Dante.Utils.parseJson<Calculator.View.ICalculatorResult>(calculationResults));
        }
    });
}