﻿module AssetsRequest {
    const $assetTypeHardwareSetComment = $("[id='RequestObjectViewModel.asset-type-hardware-set-comment']");
    const $assetTypeHardwareSetId = $("[name='RequestObjectViewModel.AssetTypeHardwareSetId']");

    const $assetTypeHeadsetComment = $("[id='RequestObjectViewModel.asset-type-headset-comment']");
    const $assetTypeHeadsetId = $("[name='RequestObjectViewModel.AssetTypeHeadsetId']");

    const $assetTypeBackpackComment = $("[id='RequestObjectViewModel.asset-type-backpack-comment']");
    const $assetTypeBackpackId = $("[name='RequestObjectViewModel.AssetTypeBackpackId']");

    const $assetTypeOptionalHardwareComment = $("[id='RequestObjectViewModel.asset-type-optional-hardware-comment']");
    const $assetTypeOptionalHardwareIds = $("[name='RequestObjectViewModel.AssetTypeOptionalHardwareIds']");

    const $assetTypeSoftwareComment = $("[id='RequestObjectViewModel.asset-type-software-comment']");
    const $assetTypeSoftwareId = $("[name='RequestObjectViewModel.AssetTypeSoftwareId']");

    const $assetTypeOtherComment = $("[id='RequestObjectViewModel.asset-type-other-comment']");
    const $assetTypeOtherIds = $("[name='RequestObjectViewModel.AssetTypeOtherIds']");

    export function onAssetTypeHardwareSetPickerSelectionChanged() {
        showCommentFieldBasedOnIds($assetTypeHardwareSetId, $assetTypeHardwareSetComment);
    }

    export function onAssetTypeHeadsetPickerSelectionChanged() {
        showCommentFieldBasedOnIds($assetTypeHeadsetId, $assetTypeHeadsetComment);
    }

    export function onAssetTypeBackpackPickerSelectionChanged() {
        showCommentFieldBasedOnIds($assetTypeBackpackId, $assetTypeBackpackComment);
    }

    export function onAssetTypeOptionalHardwarePickerSelectionChanged() {
        showCommentFieldBasedOnIds($assetTypeOptionalHardwareIds, $assetTypeOptionalHardwareComment);
    }

    export function onAssetTypeSoftwarePickerSelectionChanged() {
        showCommentFieldBasedOnIds($assetTypeSoftwareId, $assetTypeSoftwareComment);
    }

    export function onAssetTypeOtherPickerSelectionChanged() {
        showCommentFieldBasedOnIds($assetTypeOtherIds, $assetTypeOtherComment);
    }

    function showCommentFieldBasedOnIds($idField: JQuery, $commentField: JQuery) {
        let assetTypeIds: number[] = $idField.val().split(",").map(Number);

        assetTypeIds = assetTypeIds.filter(a => !isNaN(a));

        if (assetTypeIds.length == 0) {
            hideAndCleanCommentField($commentField);

            return;
        }

        const methodPath = "~/PeopleManagement/AssetType/IsCommentRequiredForIds";

        $.post(Globals.resolveUrl(methodPath), {
            assetTypeIds: assetTypeIds
        }).then(function (data) {
            if (data.isCommentRequired === true) {
                $commentField.parents(".form-group")
                    .first()
                    .show()
                        .find(".control-hint")
                        .empty()
                        .append(data.hints.join("<br>"));
            }
            else {
                hideAndCleanCommentField($commentField);
            }
        });
    }

    function hideAndCleanCommentField($commentField: JQuery) {
        $commentField.val("");
        $commentField.parents(".form-group").first().hide();
    }

    export function initialize() {
        showCommentFieldBasedOnIds($assetTypeHardwareSetId, $assetTypeHardwareSetComment);
        showCommentFieldBasedOnIds($assetTypeHeadsetId, $assetTypeHeadsetComment);
        showCommentFieldBasedOnIds($assetTypeBackpackId, $assetTypeBackpackComment);
        showCommentFieldBasedOnIds($assetTypeOptionalHardwareIds, $assetTypeOptionalHardwareComment);
        showCommentFieldBasedOnIds($assetTypeSoftwareId, $assetTypeSoftwareComment);
        showCommentFieldBasedOnIds($assetTypeOtherIds, $assetTypeOtherComment);
    }

    $(() => {
        initialize();
    });
}
