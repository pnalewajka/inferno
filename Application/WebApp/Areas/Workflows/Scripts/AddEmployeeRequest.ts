﻿module AddEmployeeRequest {
    const $firstNameInput = $("[id='RequestObjectViewModel.first-name']");
    const $lastNameInput = $("[id='RequestObjectViewModel.last-name']");
    const $emailInput = $("[id='RequestObjectViewModel.email']");
    const $loginInput = $("[id='RequestObjectViewModel.login']");

    export function initialize(): void {
        $firstNameInput.change(onNameChanged);
        $lastNameInput.change(onNameChanged);
    }

    export function onCompanyPickerSelectionChanged() {
        onNameChanged();
    }

    export function onOrgUnitPickerSelectionChanged(valuePickerElement: Element) {
        const methodPath = "~/Workflows/AddEmployeeRequest/GetDefaultValuesFromOrgUnit";

        const timeReportingFieldName = "RequestObjectViewModel.TimeReporting";
        const absenceOnlyDefaultProjectIdFieldName = "RequestObjectViewModel.AbsenceOnlyDefaultProjectId";
        const $orgUnitInput = $(valuePickerElement).find("input[type='hidden']");

        CommonDialogs.pleaseWait.show(20);

        $.get(Globals.resolveUrl(methodPath), {
            orgUnitId: $orgUnitInput.val()
        }).then((data: IOrgUnitDefaultValues) => {
            const $selectpicker = $(`.selectpicker[name='${timeReportingFieldName}']`);
            const $projectValuePicker = $(`.value-picker[data-field-name='${absenceOnlyDefaultProjectIdFieldName}']`);

            $selectpicker.val(data.timeReportingMode);
            $selectpicker.selectpicker("render");

            if (data.absenceOnlyDefaultProjectId !== null) {
                $projectValuePicker.find("input[type='text']").val(data.absenceOnlyDefaultProjectName);
                $projectValuePicker.find("input[type='hidden']").val(data.absenceOnlyDefaultProjectId);
            } else {
                $projectValuePicker.find(".value-picker-remove-button").click();
            }

            CommonDialogs.pleaseWait.hide();
        });
    }

    function onNameChanged() {
        const methodPath = "~/Workflows/AddEmployeeRequest/GetSuggestedEmailAndLogin";

        const companyIdFieldName = "RequestObjectViewModel.CompanyId";
        const $companyValuePicker = $(`.value-picker[data-field-name='${companyIdFieldName}']`);

        const firstName = $firstNameInput.val();
        const lastName = $lastNameInput.val();
        const companyId = $companyValuePicker.find("input[type='hidden']").val();

        if (firstName && lastName) {
            CommonDialogs.pleaseWait.show(20);

            $.get(Globals.resolveUrl(methodPath), {
                firstName: firstName,
                lastName: lastName,
                companyId: companyId
            }).then((data: ISuggestedEmailAndLogin) => {
                if (data) {
                    $emailInput.val(data.email);
                    $loginInput.val(data.login);
                }

                CommonDialogs.pleaseWait.hide();
            });
        }
    }

    $(() => {
        initialize();
    });
}
