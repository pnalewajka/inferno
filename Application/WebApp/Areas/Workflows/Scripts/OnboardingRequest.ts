﻿module OnboardingRequest
{
    export function initialize(): void
    {
        $("input[name='RequestObjectViewModel.IsComingFromReferralProgram']").trigger('change');
        $("input[name='RequestObjectViewModel.IsRelocationPackageNeeded']").trigger('change');
    }

    $(() => {
        initialize();
    });
}
