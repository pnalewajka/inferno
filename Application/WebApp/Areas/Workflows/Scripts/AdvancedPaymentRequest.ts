﻿namespace AdvancedPaymentRequest {
    export function onCurrencyPickerSelectionChanged(valuePickerElement: Element) {
        const currencyDisplayName = $(valuePickerElement).find("input[type='text']").val();
        $("input[name='RequestObjectViewModel.CurrencyDisplayName']").val(currencyDisplayName);
    }
}
