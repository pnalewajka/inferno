using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Models;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Workflows.Breadcrumbs
{
    public class RequestEditBreadcrumbItem : BreadcrumbItem
    {
        public RequestEditBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            var viewModel =
                breadcrumbContextProvider.Context.ViewModel as CardIndexRecordViewModel<RequestViewModel>;

            if (viewModel != null)
            {
                DisplayName = string.Format(RequestResource.EditRequestTitleFormat, viewModel.ViewModelRecord.RequestName);

                var atomicController = breadcrumbContextProvider.Context.Controller as RequestController;
                
                if (atomicController != null)
                {
                    atomicController.Layout.PageTitle = atomicController.Layout.PageHeader = DisplayName;
                }
            }
        }
    }
}