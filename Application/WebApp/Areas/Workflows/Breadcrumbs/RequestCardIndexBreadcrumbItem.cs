﻿using System;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.Workflows.Breadcrumbs
{
    public class RequestCardIndexBreadcrumbItem : CardIndexBreadcrumbItem
    {
        public RequestCardIndexBreadcrumbItem(
            IBreadcrumbContextProvider breadcrumbContextProvider,
            IBreadcrumbService breadcrumbService)
            : base(breadcrumbContextProvider, breadcrumbService)
        {
        }

        protected override Type GetTargetControllerType()
        {
            return typeof(RequestController);
        }
    }
}