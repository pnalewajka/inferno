﻿module SkillTag {
    export function addSkillTag(grid: Grid.GridComponent) {
        var context = grid.gridContext;
        var parentId = UrlHelper.getQueryParameter(context, 'parent-id');
        var urlResolveCallback = ValuePicker.getContextResolveUrlCallback({ "parent-id": parentId, "should-exclude": true });

        ValuePicker.selectMultiple("ValuePickers.ResumeTechnicalSkillsTag", (records: Grid.IRow[]) => {
            if (records.length === 0) {
                return;
            }

            CommonDialogs.confirm(Globals.resources.AddSkillTagConfirmationMessage + Utils.getHtmlList(records),
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    var ids = records.map((v) => v.id.toString()).join(",");
                    var url = Strings.format("../JobProfileSkillTag/AddSkillTags?parentId={0}&skillTagIds={1}", parentId, ids);
                    Forms.submit(url, "POST");
                }
            });
        }, urlResolveCallback);
    }
}