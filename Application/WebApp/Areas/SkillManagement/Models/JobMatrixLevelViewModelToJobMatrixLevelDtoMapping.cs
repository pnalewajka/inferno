﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixLevelViewModelToJobMatrixLevelDtoMapping : ClassMapping<JobMatrixLevelViewModel, JobMatrixLevelDto>
    {
        public JobMatrixLevelViewModelToJobMatrixLevelDtoMapping()
        {
            Mapping = v => new JobMatrixLevelDto
            {
                Code = v.Code,
                Description = v.Description,
                JobMatrixId = v.JobMatrixId,
                Level = v.Level,
                Id = v.Id,
                Timestamp = v.Timestamp,
            };
        }
    }
}
