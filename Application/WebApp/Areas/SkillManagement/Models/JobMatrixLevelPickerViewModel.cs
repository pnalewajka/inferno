﻿using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixLevelPickerViewModel
    {
        [DisplayNameLocalized(nameof(JobMatrixResources.CodeLabel), typeof(JobMatrixResources))]
        [Order(1)]
        public string Code { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.DescriptionLabel), typeof(JobMatrixResources))]
        [Order(2)]
        [NonSortable]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}