﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobTitleDtoToJobTitleViewModelMapping : ClassMapping<JobTitleDto, JobTitleViewModel>
    {
        public JobTitleDtoToJobTitleViewModelMapping()
        {
            Mapping = d => new JobTitleViewModel
            {
                Id = d.Id,
                Name = d.Name,
                JobMatrixLevelId = d.JobMatrixLevelId,
                DefaultProfileIds = d.DefaultProfileIds,
                Timestamp = d.Timestamp,
                IsActive = d.IsActive
            };
        }
    }
}
