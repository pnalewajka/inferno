﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class SkillDtoToSkillViewModelMapping : ClassMapping<SkillDto, SkillViewModel>
    {
        public SkillDtoToSkillViewModelMapping()
        {
            Mapping = d => new SkillViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                SkillTagIds = d.SkillTagIds.Length > 0 ? d.SkillTagIds : null
            };
        }
    }
}
