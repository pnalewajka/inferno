﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobProfileDtoToJobProfileViewModelMapping : ClassMapping<JobProfileDto, JobProfileViewModel>
    {
        public JobProfileDtoToJobProfileViewModelMapping()
        {
            Mapping = d => new JobProfileViewModel
            {
                Name = d.Name,
                Description = d.Description,
                Id = d.Id,
                CustomOrder = d.CustomOrder,
                SkillTagNames = d.SkillTagNames,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
