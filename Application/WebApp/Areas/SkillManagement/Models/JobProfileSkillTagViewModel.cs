﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobProfileSkillTagViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long JobProfileId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long SkillTagId { get; set; }

        [DisplayNameLocalized(nameof(SkillResources.SkillTagLabel), typeof(SkillResources))]
        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(SkillResources.DescriptionLabel), typeof(SkillResources))]
        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public long CustomOrder { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
