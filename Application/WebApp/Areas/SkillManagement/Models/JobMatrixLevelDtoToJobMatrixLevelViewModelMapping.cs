﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixLevelDtoToJobMatrixLevelViewModelMapping : ClassMapping<JobMatrixLevelDto, JobMatrixLevelViewModel>
    {
        public JobMatrixLevelDtoToJobMatrixLevelViewModelMapping()
        {
            Mapping = d => new JobMatrixLevelViewModel
            {
                Code = d.Code,
                Description = d.Description,
                JobMatrixId = d.JobMatrixId,
                Level = d.Level,
                Id = d.Id,
                Timestamp = d.Timestamp,
            };
        }
    }
}
