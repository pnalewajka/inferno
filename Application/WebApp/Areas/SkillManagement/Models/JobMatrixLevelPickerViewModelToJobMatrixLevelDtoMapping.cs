﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixLevelPickerViewModelToJobMatrixLevelDtoMapping : ClassMapping<JobMatrixLevelPickerViewModel, JobMatrixLevelDto>
    {
        public JobMatrixLevelPickerViewModelToJobMatrixLevelDtoMapping()
        {
            Mapping = v => new JobMatrixLevelDto
            {
                Code = v.Code,
                Description = v.Description,
                Id = v.Id
            };
        }
    }
}