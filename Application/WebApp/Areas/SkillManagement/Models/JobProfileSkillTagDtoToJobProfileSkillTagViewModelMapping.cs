﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobProfileSkillTagDtoToJobProfileSkillTagViewModelMapping : ClassMapping<JobProfileSkillTagDto, JobProfileSkillTagViewModel>
    {
        public JobProfileSkillTagDtoToJobProfileSkillTagViewModelMapping()
        {
            Mapping = d => new JobProfileSkillTagViewModel
            {
                Id = d.Id,
                JobProfileId = d.JobProfileId,
                SkillTagId = d.SkillTagId,
                Name = d.Name,
                CustomOrder = d.CustomOrder,
                Description = d.Description
            };
        }
    }
}
