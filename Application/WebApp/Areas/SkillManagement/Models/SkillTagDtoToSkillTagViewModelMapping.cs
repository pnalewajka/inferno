﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class SkillTagDtoToSkillTagViewModelMapping : ClassMapping<SkillTagDto, SkillTagViewModel>
    {
        public SkillTagDtoToSkillTagViewModelMapping()
        {
            Mapping = d => new SkillTagViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Timestamp = d.Timestamp,
                TagType = d.TagType
            };
        }
    }
}
