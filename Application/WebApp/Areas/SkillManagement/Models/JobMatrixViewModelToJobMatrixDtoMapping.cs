﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixViewModelToJobMatrixDtoMapping : ClassMapping<JobMatrixViewModel, JobMatrixDto>
    {
        public JobMatrixViewModelToJobMatrixDtoMapping()
        {
            Mapping = v => new JobMatrixDto
            {
                Code = v.Code,
                Description = v.Description,
                Name = v.Name,
                Id = v.Id,
                Timestamp = v.Timestamp
            };
        }
    }
}
