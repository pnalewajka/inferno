﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixLevelViewModel
    {
        [DisplayNameLocalized(nameof(JobMatrixResources.DescriptionLabel), typeof(JobMatrixResources))]
        [Order(4)]
        [NonSortable]
        [StringLength(2048)]
        [Required]
        [Multiline(5)]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.CodeLabel), typeof(JobMatrixResources))]
        [Order(1)]
        [StringLength(32)]
        [Required]
        public string Code { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(JobMatrixPickerController))]
        [DisplayNameLocalized(nameof(JobMatrixResources.JobMatrixLabel), typeof(JobMatrixResources))]
        [Order(3)]
        public long? JobMatrixId { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.LevelLabel), typeof(JobMatrixResources))]
        [Order(5)]
        [Required]
        [Range(1, 10)]
        public int Level { get; set; }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}
