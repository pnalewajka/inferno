﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobProfileSkillTagViewModelToJobProfileSkillTagDtoMapping : ClassMapping<JobProfileSkillTagViewModel, JobProfileSkillTagDto>
    {
        public JobProfileSkillTagViewModelToJobProfileSkillTagDtoMapping()
        {
            Mapping = v => new JobProfileSkillTagDto
            {
                Id = v.Id,
                JobProfileId = v.JobProfileId,
                SkillTagId = v.SkillTagId,
                Name = v.Name,
                CustomOrder = v.CustomOrder,
                Description = v.Description
            };
        }
    }
}