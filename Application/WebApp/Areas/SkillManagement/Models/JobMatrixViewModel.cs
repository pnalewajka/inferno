﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixViewModel
    {
        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public long Id { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.NameLabel), typeof(JobMatrixResources))]
        [Order(1)]
        [NonSortable]
        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.DescriptionLabel), typeof(JobMatrixResources))]
        [Order(3)]
        [NonSortable]
        [StringLength(64)]
        [Required]
        public string Description { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.CodeLabel), typeof(JobMatrixResources))]
        [Order(2)]
        [StringLength(32)]
        [Required]
        public string Code { get; set; }

        public override string ToString()
        {
            return $"{Code} {Description}";
        }
    }
}
