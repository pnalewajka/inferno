﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixDtoToJobMatrixViewModelMapping : ClassMapping<JobMatrixDto, JobMatrixViewModel>
    {
        public JobMatrixDtoToJobMatrixViewModelMapping()
        {
            Mapping = d => new JobMatrixViewModel
            {
                Code = d.Code,
                Description = d.Description,
                Name = d.Name,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}
