﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobTitleViewModelToJobTitleDtoMapping : ClassMapping<JobTitleViewModel, JobTitleDto>
    {
        public JobTitleViewModelToJobTitleDtoMapping()
        {
            Mapping = v => new JobTitleDto
            {
                Id = v.Id,
                Name = v.Name,
                JobMatrixLevelId = v.JobMatrixLevelId,
                DefaultProfileIds = v.DefaultProfileIds,
                Timestamp = v.Timestamp,
                IsActive = v.IsActive
            };
        }
    }
}
