﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class SkillTagViewModelToSkillTagDtoMapping : ClassMapping<SkillTagViewModel, SkillTagDto>
    {
        public SkillTagViewModelToSkillTagDtoMapping()
        {
            Mapping = v => new SkillTagDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                Timestamp = v.Timestamp,
                TagType = v.TagType
            };
        }
    }
}
