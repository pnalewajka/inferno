﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Attributes.CellContent;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobTitleViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(JobMatrixResources.NameLabel), typeof(JobMatrixResources))]
        [Order(1)]
        public LocalizedString Name { get; set; }

        [DisplayNameLocalized(nameof(JobMatrixResources.JobMatrixLevelLabel), typeof(JobMatrixResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController))]
        [Order(2)]
        public long? JobMatrixLevelId { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(JobMatrixResources.DefaultProfilesLabel), typeof(JobMatrixResources))]
        [ValuePicker(Type = typeof(ProfilePickerController), OnResolveUrlJavaScript = "url += '&should-exclude=true'")]
        public long[] DefaultProfileIds { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(JobMatrixResources.IsActive), typeof(JobMatrixResources))]
        [NonSortable]
        [IconizedIsActive]
        public bool IsActive { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}