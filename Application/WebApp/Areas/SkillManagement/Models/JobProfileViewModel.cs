﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobProfileViewModel
    {
        [Order(1)]
        [Required]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(JobProfilesResources.NameLabel), typeof(JobProfilesResources))]
        public string Name { get; set; }

        [Order(2)]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(JobProfilesResources.DescriptionLabel), typeof(JobProfilesResources))]
        [NonSortable]
        public string Description { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobProfilesResources.CustomOrderLabel), typeof(JobProfilesResources))]
        [Range(0, long.MaxValue)]
        public long? CustomOrder { get; set; }
  
        [DisplayNameLocalized(nameof(SkillResources.SkillTagLabel), typeof(SkillResources))]
        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        public string[] SkillTagNames { get; set; }

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
