﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.Allocation.Models
{
    [Identifier("Filters.SkillTagFilter")]
    [DescriptionLocalized("EmployeeSkillsFilterDialogTitle", typeof(EmployeeResources))]
    public class SkillTagFilterViewModel
    {
        [DisplayNameLocalized(nameof(SkillResources.SkillTagLabel), typeof(SkillResources))]
        [ValuePicker(Type = typeof(SkillTagPickerController))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        public long[] SkillTagIds { get; set; }

        public override string ToString()
        {
            return SkillResources.SkillTagLabel;
        }
    }
}