﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobMatrixLevelDtoToJobMatrixLevelPickerViewModelMapping : ClassMapping<JobMatrixLevelDto, JobMatrixLevelPickerViewModel>
    {
        public JobMatrixLevelDtoToJobMatrixLevelPickerViewModelMapping()
        {
            Mapping = d => new JobMatrixLevelPickerViewModel
            {
                Code = d.Code,
                Description = d.Description,
                Id = d.Id
            };
        }
    }
}