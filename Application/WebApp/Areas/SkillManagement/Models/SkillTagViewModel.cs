﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    [Identifier("Models.SkillTagViewModel")]
    public class SkillTagViewModel
    {
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(SkillResources.NameLabel), typeof(SkillResources))]
        public LocalizedString Name { get; set; }

        [DisplayNameLocalized(nameof(SkillResources.DescriptionLabel), typeof(SkillResources))]
        [NonSortable]
        public string Description { get; set; }
        
        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [RequiredEnum(typeof(SkillTagType))]
        [DisplayNameLocalized(nameof(SkillResources.SkillTagTypesLabel), typeof(SkillResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<SkillTagType>))]
        public SkillTagType TagType { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
