﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class SkillViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(0)]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(SkillResources.NameLabel), typeof(SkillResources))]
        public string Name { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(SkillResources.DescriptionLabel), typeof(SkillResources))]
        [NonSortable]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(SkillResources.SkillTagLabel), typeof(SkillResources))]
        [TypeAhead(Type = typeof(SkillTagPickerController))]
        [NonSortable]
        public long[] SkillTagIds { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
