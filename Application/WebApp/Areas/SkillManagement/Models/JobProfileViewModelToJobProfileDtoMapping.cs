﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class JobProfileViewModelToJobProfileDtoMapping : ClassMapping<JobProfileViewModel, JobProfileDto>
    {
        public JobProfileViewModelToJobProfileDtoMapping()
        {
            Mapping = v => new JobProfileDto
            {
                Name = v.Name,
                Description = v.Description,
                CustomOrder = v.CustomOrder,
                SkillTagNames = v.SkillTagNames != null ? v.SkillTagNames : new string[0],
                Id = v.Id,
                Timestamp = v.Timestamp
            };
        }
    }
}
