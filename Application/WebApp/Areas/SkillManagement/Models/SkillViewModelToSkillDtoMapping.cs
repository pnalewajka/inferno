﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.SkillManagement.Models
{
    public class SkillViewModelToSkillDtoMapping : ClassMapping<SkillViewModel, SkillDto>
    {
        public SkillViewModelToSkillDtoMapping()
        {
            Mapping = v => new SkillDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                Timestamp = v.Timestamp,
                SkillTagIds = v.SkillTagIds != null && v.SkillTagIds.Length > 0 ? v.SkillTagIds : new long[] { }
            };
        }
    }
}
