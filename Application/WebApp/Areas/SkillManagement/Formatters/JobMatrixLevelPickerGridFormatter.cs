﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Formatters
{
    public class JobMatrixLevelPickerGridFormatter : IListItemFormatter<JobMatrixLevelPickerViewModel>
    {
        public string Format(JobMatrixLevelPickerViewModel viewModel)
        {
            return viewModel.Code;
        }
    }
}