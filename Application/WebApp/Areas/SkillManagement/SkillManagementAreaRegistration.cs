﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.SkillManagement
{
    public class SkillManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SkillManagement";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SkillManagement_default2",
                "SkillManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}