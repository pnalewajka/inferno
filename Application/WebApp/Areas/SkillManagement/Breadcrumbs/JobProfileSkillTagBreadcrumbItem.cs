﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Breadcrumbs
{
    public class JobProfileSkillTagBreadcrumbItem : BreadcrumbItem
    {
        public JobProfileSkillTagBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = JobProfilesResources.ManageTagSkills;
        }
    }
}