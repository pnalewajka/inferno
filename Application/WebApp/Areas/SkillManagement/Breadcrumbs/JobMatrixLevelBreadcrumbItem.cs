﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Breadcrumbs
{
    public class JobMatrixLevelBreadcrumbItem : BreadcrumbItem
    {
        public JobMatrixLevelBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = JobMatrixResources.JobMatrixLevelTitle;
        }
    }
}