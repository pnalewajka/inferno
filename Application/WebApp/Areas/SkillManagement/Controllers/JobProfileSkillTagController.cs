﻿using System.Web.Mvc;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewJobProfiles)]
    public class JobProfileSkillTagController : SubCardIndexController<JobProfileSkillTagViewModel, JobProfileSkillTagDto, JobProfileController>
    {
        private readonly IJobProfileSkillTagCardIndexDataService _jobProfileSkillTagCardIndexDataService;

        public JobProfileSkillTagController(
            IJobProfileSkillTagCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _jobProfileSkillTagCardIndexDataService = cardIndexDataService;

            CardIndex.Settings.IsContextRequired = true;            
            CardIndex.Settings.Title = JobProfilesResources.ManageTagSkills;

            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobProfiles;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;

            Layout.Resources.AddFrom<JobProfilesResources>("AddSkillTagConfirmationMessage");
            Layout.Scripts.Add("~/Areas/SkillManagement/Scripts/SkillTag.js"); 

            CardIndex.Settings.AddButton.OnClickAction = new JavaScriptCallAction("SkillTag.addSkillTag(grid);");
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobProfiles;

            CardIndex.Settings.RowButtons.Add(
                new RowButton()
                {
                    RequiredRole = SecurityRoleType.CanEditJobProfiles,
                    Text = JobProfilesResources.MoveUpTitle,
                    OnClickAction = Url.UriActionPost(nameof(JobProfileSkillTagController.MoveUp), RoutingHelper.GetControllerName<JobProfileSkillTagController>(), KnownParameter.SelectedId),
                    Icon = FontAwesome.ArrowCircleUp
                });

            CardIndex.Settings.RowButtons.Add(
                new RowButton()
                {
                    RequiredRole = SecurityRoleType.CanEditJobProfiles,
                    Text = JobProfilesResources.MoveDownTitle,
                    OnClickAction = Url.UriActionPost(nameof(JobProfileSkillTagController.MoveDown), RoutingHelper.GetControllerName<JobProfileSkillTagController>(), KnownParameter.SelectedId),
                    Icon = FontAwesome.ArrowCircleDown
                });
        }

        [HttpPost]
        public ActionResult MoveUp(long id)
        {
            _jobProfileSkillTagCardIndexDataService.MoveSkillTagUpOrder(id);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        public ActionResult MoveDown(long id)
        {
            _jobProfileSkillTagCardIndexDataService.MoveSkillTagDownOrder(id);
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanEditJobProfiles)]
        public ActionResult AddSkillTags(long parentId, IList<long> skillTagIds)
        {
            _jobProfileSkillTagCardIndexDataService.AddSkillTags(parentId, skillTagIds);
           var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

           return new RedirectResult(listActionUrl);
        }
    }
}