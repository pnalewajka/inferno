﻿using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [Identifier("ValuePickers.ResumeTechnicalSkillsTag")]
    public class ResumeTechnicalSkillsTagPickerController : SkillTagController
    {
        public ResumeTechnicalSkillsTagPickerController(ISkillTagCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }

        public override ActionResult List(GridParameters parameters)
        {
            if (parameters.Filters == null)
            {
                parameters.Filters = FilterCodes.ResumeTechnicalSkillsTagFilter;
            }

            return base.List(parameters);
        }
    }
}