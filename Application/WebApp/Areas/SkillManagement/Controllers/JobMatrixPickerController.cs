﻿using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [Identifier("ValuePickers.JobMatrix")]
    public class JobMatrixPickerController : JobMatrixController
    {
        public JobMatrixPickerController(IJobMatrixCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}