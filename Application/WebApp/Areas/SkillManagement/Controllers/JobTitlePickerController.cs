﻿using System.Collections.Generic;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [Identifier("ValuePickers.JobTitle")]
    public class JobTitlePickerController : JobTitleController
    {
        public JobTitlePickerController(IJobTitleCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configuration = new GridViewConfiguration<JobTitleViewModel>(JobMatrixResources.JobTitlesTitle, string.Empty)
            {
                IsDefault = true
            };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(j => j.DefaultProfileIds);
            configuration.ExcludeColumn(j => j.IsActive);

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}