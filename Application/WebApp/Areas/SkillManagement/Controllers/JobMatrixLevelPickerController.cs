﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [Identifier("ValuePickers.JobMatrixLevel")]
    [AtomicAuthorize(SecurityRoleType.CanViewJobMatrixLevel)]
    public class JobMatrixLevelPickerController : CardIndexController<JobMatrixLevelPickerViewModel, JobMatrixLevelDto>
    {
        public JobMatrixLevelPickerController(IJobMatrixLevelCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobMatrixLevel;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditJobMatrixLevel;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobMatrixLevel;

            CardIndex.Settings.Title = JobMatrixResources.JobMatrixLevelTitle;
        }
    }
}