﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewJobMatrix)]
    public class JobMatrixController : CardIndexController<JobMatrixViewModel, JobMatrixDto>
    {
        public JobMatrixController(IJobMatrixCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobMatrix;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditJobMatrix;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobMatrix;

            CardIndex.Settings.Title = JobMatrixResources.JobMatrixTitle;

            var jobMatrixLevelsButton = new ToolbarButton
            {
                Text = JobMatrixResources.JobMatrixLevelsButton,
                Group = JobMatrixResources.JobMatrixLevelsButton,
                OnClickAction = Url.UriActionGet("List", "JobMatrixLevel", KnownParameter.ParentId),
                RequiredRole = SecurityRoleType.CanViewJobMatrixLevel,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };

            CardIndex.Settings.ToolbarButtons.Add(jobMatrixLevelsButton);
        }
    }
}