﻿using System.Collections.Generic;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewJobProfiles)]
    public class JobProfileController : CardIndexController<JobProfileViewModel, JobProfileDto>
    {
        public JobProfileController(IJobProfileCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobProfiles;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditJobProfiles;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobProfiles;
            CardIndex.Settings.Title = JobProfilesResources.JobProfilesTitle;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportJobProfiles);
            CardIndex.Settings.AddImport<JobProfileViewModel, JobProfileDto>(cardIndexDataService,
                JobProfilesResources.ImportButtomName, SecurityRoleType.CanEditJobProfiles);

            CardIndex.Settings.ToolbarButtons.Add(
                new ToolbarButton()
                {
                    Text = JobProfilesResources.ManageTagSkills,
                    RequiredRole = SecurityRoleType.CanEditJobProfiles,
                    Icon = FontAwesome.Cog,
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.SingleRowSelected,
                    },
                    OnClickAction = Url.UriActionGet(nameof(JobProfileSkillTagController.List), RoutingHelper.GetControllerName<JobProfileSkillTagController>(), KnownParameter.ParentId),
                });

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<SkillTagFilterViewModel>(FilterCodes.SkillTagFilter, SkillResources.SkillTagFilterName)
                    }
                }
            };
        }
    }
}