﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewJobTitles)]

    public class JobTitleController : CardIndexController<JobTitleViewModel, JobTitleDto, JobTitleContext>
    {
        public JobTitleController(IJobTitleCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobTitles;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditJobTitles;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobTitles;

            CardIndex.Settings.Title = JobMatrixResources.JobTitlesTitle;
        }
    }
}