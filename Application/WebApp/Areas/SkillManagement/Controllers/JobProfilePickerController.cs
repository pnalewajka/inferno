﻿using System.Collections.Generic;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [Identifier("ValuePickers.JobProfile")]
    public class JobProfilePickerController : JobProfileController
    {
        public JobProfilePickerController(IJobProfileCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            CardIndex.Settings.PageSize = 15;
            base.PrepareForValuePicker(allowMultipleRowSelection);
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configuration = new GridViewConfiguration<JobProfileViewModel>(JobProfilesResources.JobProfilesTitle, "JobProfilePicker")
            {
                IsDefault = true
            };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(j => j.SkillTagNames);

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}