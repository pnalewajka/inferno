﻿using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [Identifier("ValuePickers.KeyTechnologyTag")]
    public class KeyTechnologyTagPickerController : SkillTagController
    {
        public KeyTechnologyTagPickerController(ISkillTagCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.AndCheckboxGroup,
                    Visibility = CommandButtonVisibility.None,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.KeyTechnologyTagFilter, CanBeSwitchedOff = false }
                    }
                }
            };
        }
    }
}