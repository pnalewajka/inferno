﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSkillTags)]
    public class SkillTagController : CardIndexController<SkillTagViewModel, SkillTagDto>
    {
        public SkillTagController(ISkillTagCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSkillTags;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSkillTags;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSkillTags;
            CardIndex.Settings.Title = SkillResources.SkillTagLabel;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportSkillTags);
            CardIndex.Settings.AddImport<SkillTagViewModel, SkillTagDto>(cardIndexDataService,
                SkillResources.ImportButtomName, SecurityRoleType.CanAddSkillTags);
        }
    }
}