﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;
using Smt.Atomic.WebApp.Areas.SkillManagement.Resources;
using Smt.Atomic.WebApp.Controllers;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.SkillManagement.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewSkills)]
    public class SkillController : CardIndexController<SkillViewModel, SkillDto>
    {
        public SkillController(ISkillCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddSkills;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditSkills;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteSkills;
            CardIndex.Settings.Title = SkillResources.SkillsTitle;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportSkills);
            CardIndex.Settings.AddImport<SkillViewModel, SkillDto>(cardIndexDataService, SkillResources.ImportButtomName, SecurityRoleType.CanAddSkills);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<SkillTagFilterViewModel>(FilterCodes.SkillTagFilter, SkillResources.SkillTagFilterName)
                    }
                }
            };
        }
    }
}