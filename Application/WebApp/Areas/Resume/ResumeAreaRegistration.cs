﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Resume
{
    public class ResumeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Resume";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Resume_default",
                "Resume/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            RegisterBundles();
        }

        private void RegisterBundles()
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}