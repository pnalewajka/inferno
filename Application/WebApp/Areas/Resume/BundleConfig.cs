﻿using System;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Resume
{
    public class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/resume/presentation") { ConcatenationToken = Environment.NewLine }
                 .Include(
                     "~/Areas/Resume/Scripts/Components/Presentation/CompanyComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/EducationComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/LanguageComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/ProjectComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/DetailsComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/SummaryComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/TechnicalSkillAutoComplete.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/TechnicalSkillComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/TechnicalSkillPillComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/TrainingComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/ConsentClauseForDataProcessingComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/SuggestionsGroupComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/SuggestedSkillPillComponent.js",
                     "~/Areas/Resume/Scripts/Components/Presentation/AdditionalSkillComponent.js"
                 ));

            bundles.Add(new ScriptBundle("~/resume/containers") { ConcatenationToken = Environment.NewLine }
                 .Include(
                     "~/Areas/Resume/Scripts/Components/Containers/TechnicalSkillTableComponent.js",
                     "~/Areas/Resume/Scripts/Components/Containers/CompanyTableComponent.js",
                     "~/Areas/Resume/Scripts/Components/Containers/EducationTableComponent.js",
                     "~/Areas/Resume/Scripts/Components/Containers/LanguageTableComponent.js",
                     "~/Areas/Resume/Scripts/Components/Containers/TrainingTableComponent.js",
                     "~/Areas/Resume/Scripts/Components/Containers/SuggestionsDialogComponent.js",
                     "~/Areas/Resume/Scripts/Components/Containers/AdditionalSkillTableComponent.js"
                 ));

            bundles.Add(new ScriptBundle("~/resume/share") { ConcatenationToken = Environment.NewLine }
                .Include(
                    "~/Areas/Resume/Scripts/Components/Share/DatePicker.js",
                    "~/Areas/Resume/Scripts/Components/Share/Pill.js",
                    "~/Areas/Resume/Scripts/Components/Share/CheckboxInput.js"
                ));

            bundles.Add(new ScriptBundle("~/resume/entry") { ConcatenationToken = Environment.NewLine }
                 .Include(
                     "~/Areas/Resume/Scripts/util.js",
                     "~/Areas/Resume/Scripts/Interfaces.js",
                     "~/Areas/Resume/Scripts/ResumeApiService.js",
                     "~/Areas/Resume/Scripts/Resume.js",
                     "~/Areas/Resume/Scripts/ResumeActionTypes.js"
                 ));


            bundles.Add(new StyleBundle("~/resume/styles")
                            .Include
                            (
                                "~/Areas/Resume/Content/resume.css"
                            ));
        }
    }
}