﻿using System;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using System.Linq;
using System.Globalization;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class MyResumeDtoToMyResumeViewModelMapping : ClassMapping<MyResumeDto, MyResumeViewModel>
    {
        public MyResumeDtoToMyResumeViewModelMapping()
        {
            Mapping = d => new MyResumeViewModel
            {
                Resume = new ResumeViewModel
                {
                    Id = d.Id,
                    EmployeeId = d.EmployeeId,
                    EmlpoyeeName = d.EmlpoyeeName,
                    AvailabilityInMonths = d.AvailabilityInDays / 30,
                    Summary = d.Summary,
                    CivoResumeVersionId = d.CivoResumeVersionId,
                    Timestamp = d.Timestamp,
                    YearsOfExperience = d.YearsOfExperience,
                    Position = d.Position,
                    IsConsentClauseForDataProcessingChecked = d.IsConsentClauseForDataProcessingChecked
                },
                ResumeTechnicalSkills = d.ResumeTechnicalSkills == null ? null : d.ResumeTechnicalSkills.Select(t => new ResumeTechnicalSkillViewModel
                {
                    Id = t.Id,
                    ExpertiseLevel = t.ExpertiseLevel,
                    ExpertisePeriod = t.ExpertisePeriod,
                    ExpertiseLevelName =  EnumExtensions.GetDescription(t.ExpertiseLevel, CultureInfo.CurrentUICulture) ,
                    ExpertisePeriodName =  EnumExtensions.GetDescription(t.ExpertisePeriod, CultureInfo.CurrentUICulture) ,
                    TechnicalSkillId = t.TechnicalSkillId,
                    Name = t.Name,
                    SkillTagId = t.SkillTagId,
                    SkillTagName = t.SkillTagName,
                    Timestamp = t.Timestamp,
                    CreatedOn = t.CreatedOn
                }).OrderByDescending(t=>t.ExpertiseLevel).ToList(),
                ResumeEducations = d.ResumeEducations == null ? null : d.ResumeEducations.Select(e => new ResumeEducationViewModel
                {
                    Id = e.Id,
                    Degree = e.Degree,
                    From = e.From,
                    To = e.To,
                    Specialization = e.Specialization,
                    UniversityName = e.UniversityName,
                    Timestamp = e.Timestamp
                }).OrderByDescending(e => e.From).ToList(),
                ResumeAdditionalSkills = d.ResumeAdditionalSkills == null ? null : d.ResumeAdditionalSkills.Select(a => new ResumeAdditionalSkillViewModel
                {
                    Id = a.Id,
                    Name = a.Name,
                    Timestamp = a.Timestamp
                }).ToList(),
                ResumeLanguages = d.ResumeLanguages == null ? null : d.ResumeLanguages.Select(l => new ResumeLanguageViewModel
                {
                    Id = l.Id,
                    LanguageId = l.LanguageId,
                    LanguageName = l.LanguageName,
                    Level = (int)l.Level,
                    LevelName = EnumExtensions.GetDescription(l.Level, CultureInfo.CurrentUICulture),
                    Timestamp = l.Timestamp
                }).ToList(),
                ResumeTrainings = d.ResumeTrainings == null ? null : d.ResumeTrainings.Select(t => new ResumeTrainingViewModel
                {
                    Id = t.Id,
                    Title = t.Title,
                    Date = t.Date,
                    Institution = t.Institution,
                    Type = (int)t.Type,
                    TypeName = EnumExtensions.GetDescription(t.Type, CultureInfo.CurrentUICulture),
                    Timestamp = t.Timestamp
                }).OrderByDescending(t => t.Date).ToList(),
                ResumeCompanies = d.ResumeCompanies == null ? null : d.ResumeCompanies.Select(c => new ResumeCompanyViewModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    From = c.From,
                    To = c.To,
                    Timestamp = c.Timestamp,
                    ResumeProjects = c.ResumeProjects == null ? null : c.ResumeProjects.Select(p => new ResumeProjectViewModel
                    {
                        Id = p.Id,
                        From = p.From,
                        To = p.To,
                        Name = p.Name,
                        ProjectId = p.ProjectId,
                        IsProjectConfidential = p.IsProjectConfidential,
                        ProjectDescription = p.ProjectDescription,
                        Roles = p.Roles,
                        Duties = p.Duties,
                        Industries = p.Industries == null ? new ResumeProjectIndustriesViewModel[0] : 
                            p.Industries.Select(i=>new ResumeProjectIndustriesViewModel { Id = i.Id, Name = i.Name }).ToArray(),
                        TechnicalSkills = p.TechnicalSkills == null ? new  ResumeTechnicalSkillViewModel[0] :
                            p.TechnicalSkills.Select(t=> new ResumeTechnicalSkillViewModel { Id = t.Id, Name = t.Name }).ToArray(),
                        Timestamp = p.Timestamp
                    }).OrderByDescending(p => p.From).ToList()
                }).OrderByDescending(c => c.To ?? DateTime.MaxValue).ThenByDescending(c => c.From).ToList(),
                ResumeRejectedSkillSuggestions = d.ResumeRejectedSkillSuggestions
            };
        }
    }
}
