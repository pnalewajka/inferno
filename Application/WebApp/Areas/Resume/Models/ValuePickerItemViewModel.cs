﻿namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ValuePickerItemViewModel
    {
        public int Value { get; set; }

        public string Name { get; set; }
    }
}