﻿using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeDictionariesViewModel
    {
        public IList<DictionaryItemViewModel> Languages { get; set; }

        public IList<ValuePickerItemViewModel> LanguageLevels { get; set; }

        public IList<ValuePickerItemViewModel> ExpertiseLevels { get; set; }

        public IList<ValuePickerItemViewModel> ExpertisePeriods { get; set; }

        public IList<DictionaryItemViewModel> Industries { get; set; }

        public IList<DictionaryItemViewModel> Companies { get; set; }

        public SuggestedSkillsGroupViewModel Suggestion { get; set; }

        public IList<SkillViewModel> Skills { get; set; }

        public IList<SkillTagValueViewModel> SkillTags { get; set; }

        public IList<ValuePickerItemViewModel> TrainingTypes { get; set; }
    }
}