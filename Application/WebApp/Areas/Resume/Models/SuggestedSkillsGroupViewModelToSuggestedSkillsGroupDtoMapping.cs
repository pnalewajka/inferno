﻿using System.Linq;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class SuggestedSkillsGroupViewModelToSuggestedSkillsGroupDtoMapping : ClassMapping<SuggestedSkillsGroupViewModel, SuggestedSkillsGroupDto>
    {
        public SuggestedSkillsGroupViewModelToSuggestedSkillsGroupDtoMapping()
        {
            Mapping = v => new SuggestedSkillsGroupDto
            {
                SuggestionReason = v.SuggestionReason,
                SkillSuggestions = v.SkillSuggestions == null ? null : v.SkillSuggestions.Select(s => new SuggestedSkillDto
                {
                    SkillId = s.SkillId,
                    Name = s.Name,
                    ExpertiseLevel = s.ExpertiseLevel,
                    ExpertisePeriod = s.ExpertisePeriod,
                    SkillTagId = s.SkillTagId,
                    SkillTagName = s.SkillTagName
                }).ToList()
            };
        }
    }
}
