﻿using System;
using System.Collections.Generic;
namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class MyResumeViewModel
    {
        public string Title { get; set; }

        public ResumeViewModel Resume { get; set; }

        public IList<ResumeTechnicalSkillViewModel> ResumeTechnicalSkills { get; set; }

        public IList<ResumeCompanyViewModel> ResumeCompanies { get; set; }

        public IList<ResumeLanguageViewModel> ResumeLanguages { get; set; }

        public IList<ResumeTrainingViewModel> ResumeTrainings { get; set; }

        public IList<ResumeEducationViewModel> ResumeEducations { get; set; }

        public IList<long> ResumeRejectedSkillSuggestions { get; set; }

        public IList<ResumeAdditionalSkillViewModel> ResumeAdditionalSkills { get; set; }

        public ResumeDictionariesViewModel ResumeDictionaries { get; set; }

        public bool IsMine { get; set; }
    }
}
