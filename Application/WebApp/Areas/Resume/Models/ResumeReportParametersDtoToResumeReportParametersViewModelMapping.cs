﻿using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeReportParametersDtoToResumeReportParametersViewModelMapping : ClassMapping<ResumeReportParametersDto, ResumeReportParametersViewModel>
    {
        public ResumeReportParametersDtoToResumeReportParametersViewModelMapping()
        {
            Mapping = d => new ResumeReportParametersViewModel
            {
               EmployeeId = d.EmployeeId
            };
        }
    }
}