﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    [Identifier("Modals.ShareResume")]
    public class ShareResumeViewModel
    {
        [Required]
        [HiddenInput]
        public long EmployeeId { get; set; }

        [Required]
        public string Reason { get; set; }
    }
}