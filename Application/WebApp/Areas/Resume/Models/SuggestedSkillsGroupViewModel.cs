﻿using System;
using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.Resume.Models;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class SuggestedSkillsGroupViewModel
    {
        public string SuggestionReason { get; set; }

        public IList<SkillSuggestionViewModel> SkillSuggestions { get; set; }
    }
}
