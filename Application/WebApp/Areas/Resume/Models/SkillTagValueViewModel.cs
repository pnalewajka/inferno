﻿namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class SkillTagValueViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long SkillTagOrder { get; set; }
    }
}