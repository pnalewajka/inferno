﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class SkillSuggestionViewModel
    {
        public long SkillId { get; set; }

        public string Name { get; set; }

        public SkillExpertiseLevel ExpertiseLevel { get; set; }

        public string ExpertiseLevelName { get; set; }

        public SkillExpertisePeriod ExpertisePeriod { get; set; }

        public string ExpertisePeriodName { get; set; }

        public string SkillTagName { get; set; }

        public long SkillTagId { get; set; }
    }
}