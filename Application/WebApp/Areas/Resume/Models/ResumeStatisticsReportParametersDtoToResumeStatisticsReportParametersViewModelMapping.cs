﻿using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeStatisticsReportParametersDtoToResumeStatisticsReportParametersViewModelMapping : ClassMapping<ResumeStatisticsReportParametersDto , ResumeStatisticsReportParametersViewModel>
    {
        public ResumeStatisticsReportParametersDtoToResumeStatisticsReportParametersViewModelMapping()
        {
            Mapping = v => new ResumeStatisticsReportParametersViewModel
            {
                JobProfileIds = v.JobProfileIds,
                LineManagerIds = v.LineManagerIds,
                LocationIds = v.LocationIds,
                OrgUnitIds = v.OrgUnitIds,
                SkillIds = v.SkillIds
            };
        }
    }
}