﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    [Identifier("Models.ResumeReportParameters")]
    public class ResumeReportParametersViewModel
    {
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeText), typeof(ReportResources))]
        public long EmployeeId { get; set; }
    }
}