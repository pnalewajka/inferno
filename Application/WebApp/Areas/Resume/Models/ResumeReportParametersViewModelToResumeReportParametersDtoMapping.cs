﻿using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeReportParametersViewModelToResumeReportParametersDtoMapping : ClassMapping<ResumeReportParametersViewModel, ResumeReportParametersDto>
    {
        public ResumeReportParametersViewModelToResumeReportParametersDtoMapping()
        {
            Mapping = d => new ResumeReportParametersDto
            {
                EmployeeId = d.EmployeeId
            };
        }
    }
}