﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeProjectViewModel
    {
        public long Id { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public string Roles { get; set; }

        public string Duties { get; set; }

        public bool IsProjectConfidential { get; set; }

        public string ProjectDescription { get; set; }

        public string Name { get; set; }

        public long? ProjectId { get; set; }

        public ResumeTechnicalSkillViewModel[] TechnicalSkills { get; set; }

        public ResumeProjectIndustriesViewModel[] Industries { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
