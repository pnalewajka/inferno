﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    [Identifier("Models.ResumeStatisticsReportParameters")]
    public class ResumeStatisticsReportParametersViewModel
    {
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long[] LocationIds { get; set; }

        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        public long[] LineManagerIds { get; set; }

        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnit), nameof(EmployeeResources.EmployeeOrgUnitShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long[] OrgUnitIds { get; set; }

        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeSkillsLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(SkillPickerController))]
        public long[] SkillIds { get; set; }

        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long[] JobProfileIds { get; set; }
    }
}