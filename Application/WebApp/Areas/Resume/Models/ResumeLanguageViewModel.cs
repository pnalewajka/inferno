﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeLanguageViewModel
    {
        public long Id { get; set; }

        public long LanguageId { get; set; }

        public string LanguageName { get; set; }

        public string LevelName { get; set; }

        public int Level { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
