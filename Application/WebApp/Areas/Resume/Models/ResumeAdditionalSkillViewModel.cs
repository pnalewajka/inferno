﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeAdditionalSkillViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public byte[] Timestamp { get; set; }
    }
}