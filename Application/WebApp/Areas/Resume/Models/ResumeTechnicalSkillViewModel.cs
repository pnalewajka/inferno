﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeTechnicalSkillViewModel
    {
        public long Id { get; set; }

        public long TechnicalSkillId { get; set; }

        public string Name { get; set; }

        public SkillExpertiseLevel ExpertiseLevel { get; set; }

        public string ExpertiseLevelName { get; set; }

        public SkillExpertisePeriod ExpertisePeriod { get; set; }

        public string ExpertisePeriodName { get; set; }

        public string SkillTagName { get; set; }

        public long SkillTagId { get; set; }

        public byte[] Timestamp { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
