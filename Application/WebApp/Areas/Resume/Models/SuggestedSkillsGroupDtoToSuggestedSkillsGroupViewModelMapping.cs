﻿using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class SuggestedSkillsGroupDtoToSuggestedSkillsGroupViewModelMapping : ClassMapping<SuggestedSkillsGroupDto, SuggestedSkillsGroupViewModel>
    {
        public SuggestedSkillsGroupDtoToSuggestedSkillsGroupViewModelMapping()
        {
            Mapping = d => new SuggestedSkillsGroupViewModel
            {
                SuggestionReason = d.SuggestionReason,
                SkillSuggestions = d.SkillSuggestions == null ? null : d.SkillSuggestions.Select(s => new SkillSuggestionViewModel
                {
                    SkillId = s.SkillId,
                    Name = s.Name,
                    ExpertiseLevel = s.ExpertiseLevel,
                    ExpertiseLevelName = EnumExtensions.GetDescription(s.ExpertiseLevel, CultureInfo.CurrentUICulture),
                    ExpertisePeriod = s.ExpertisePeriod,
                    ExpertisePeriodName = EnumExtensions.GetDescription(s.ExpertisePeriod, CultureInfo.CurrentUICulture),
                    SkillTagId = s.SkillTagId,
                    SkillTagName = s.SkillTagName
                }).ToList()
            };
        }
    }
}
