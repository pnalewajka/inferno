﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeCompanyViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
        
        public DateTime From { get; set; }
        
        public DateTime? To { get; set; }

        public IList<ResumeProjectViewModel> ResumeProjects { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
