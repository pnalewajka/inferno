﻿using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class MyResumeViewModelToMyResumeDtoMapping : ClassMapping<MyResumeViewModel, MyResumeDto>
    {
        public MyResumeViewModelToMyResumeDtoMapping()
        {
            Mapping = v => new MyResumeDto
            {
                Id = v.Resume.Id,
                EmployeeId = v.Resume.EmployeeId,
                EmlpoyeeName = v.Resume.EmlpoyeeName,
                AvailabilityInDays = v.Resume.AvailabilityInMonths * 30,
                Summary = v.Resume.Summary,
                CivoResumeVersionId = v.Resume.CivoResumeVersionId,
                Timestamp = v.Resume.Timestamp,
                YearsOfExperience = v.Resume.YearsOfExperience,
                Position = v.Resume.Position,
                IsConsentClauseForDataProcessingChecked = v.Resume.IsConsentClauseForDataProcessingChecked,
                ResumeTechnicalSkills = v.ResumeTechnicalSkills != null ? v.ResumeTechnicalSkills.Select(t => new ResumeTechnicalSkillDto
                {
                    Id = t.Id,
                    ExpertiseLevel = (SkillExpertiseLevel)t.ExpertiseLevel,
                    ExpertisePeriod = (SkillExpertisePeriod)t.ExpertisePeriod,
                    TechnicalSkillId = t.TechnicalSkillId,
                    SkillTagName = t.SkillTagName,
                    Name = t.Name,
                    SkillTagId = t.SkillTagId,
                    Timestamp = t.Timestamp,
                    CreatedOn = t.CreatedOn
                }).ToList() : null,
                ResumeEducations = v.ResumeEducations != null ? v.ResumeEducations.Select(e => new ResumeEducationDto
                {
                    Id = e.Id,
                    Degree = e.Degree,
                    From = e.From,
                    To = e.To,
                    Specialization = e.Specialization,
                    UniversityName = e.UniversityName,
                    Timestamp = e.Timestamp
                }).ToList() : null,
                ResumeAdditionalSkills = v.ResumeAdditionalSkills != null ? v.ResumeAdditionalSkills.Select(a => new ResumeAdditionalSkillDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Timestamp = a.Timestamp
                }).ToList() : null,
                ResumeLanguages = v.ResumeLanguages != null ? v.ResumeLanguages.Select(l => new ResumeLanguageDto
                {
                    Id = l.Id,
                    LanguageId = l.LanguageId,
                    Level = (LanguageReferenceLevel)l.Level,
                    Timestamp = l.Timestamp
                }).ToList() : null,
                ResumeTrainings = v.ResumeTrainings != null ? v.ResumeTrainings.Select(t => new ResumeTrainingDto
                {
                    Id = t.Id,
                    Title = t.Title,
                    Date = t.Date,
                    Institution = t.Institution,
                    Type = (TrainingType)t.Type,
                    Timestamp = t.Timestamp
                }).ToList() : null,
                ResumeCompanies = v.ResumeCompanies != null ? v.ResumeCompanies.Select(c => new ResumeCompanyDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    From = c.From,
                    To = c.To,
                    Timestamp = c.Timestamp,
                    ResumeProjects = c.ResumeProjects != null ? c.ResumeProjects.Select(p => new ResumeProjectDto
                    {
                        Id = p.Id,
                        From = p.From,
                        To = p.To,
                        Name = p.Name,
                        ProjectId = p.ProjectId.HasValue && p.ProjectId==0 ? null : p.ProjectId,
                        ProjectDescription = p.ProjectDescription,
                        Roles = p.Roles,
                        Duties = p.Duties,
                        Industries = p.Industries == null ? new ResumeProjectIndustryDto[0] :
                            p.Industries.Select(i => new ResumeProjectIndustryDto { Id = i.Id, Name = i.Name }).ToArray(),
                        TechnicalSkills = p.TechnicalSkills == null ? new ResumeProjectSkillDto[0] :
                            p.TechnicalSkills.Select(i => new ResumeProjectSkillDto { Id = i.Id, Name = i.Name }).ToArray(),
                        Timestamp = p.Timestamp
                    }).ToList() : null
                }).ToList() : null,
                ResumeRejectedSkillSuggestions = v.ResumeRejectedSkillSuggestions
            };
        }
    }
}
