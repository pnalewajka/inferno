﻿namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class DictionaryItemViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}