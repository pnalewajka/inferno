﻿using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeStatisticsReportParametersViewModelToResumeStatisticsReportParametersDtoMapping : ClassMapping<ResumeStatisticsReportParametersViewModel, ResumeStatisticsReportParametersDto>
    {
        public ResumeStatisticsReportParametersViewModelToResumeStatisticsReportParametersDtoMapping()
        {
            Mapping = d => new ResumeStatisticsReportParametersDto
            {
                JobProfileIds = d.JobProfileIds,
                LineManagerIds = d.LineManagerIds,
                LocationIds = d.LocationIds,
                OrgUnitIds = d.OrgUnitIds,
                SkillIds = d.SkillIds
            };
        }
    }
}