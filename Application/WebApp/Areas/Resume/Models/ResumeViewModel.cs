﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeViewModel
    {
        public long Id { get; set; }

        public long? EmployeeId { get; set; }

        public string EmlpoyeeName { get; set; }

        public int AvailabilityInMonths { get; set; }

        public string Summary { get; set; }

        public long? CivoResumeVersionId { get; set; }

        public string YearsOfExperience { get; set; }

        public string Position { get; set; }

        public byte[] Timestamp { get; set; }

        public bool IsConsentClauseForDataProcessingChecked { get; set; }
    }
}