﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeEducationViewModel
    {
        public long Id { get; set; }

        public string UniversityName { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public string Degree { get; set; }

        public string Specialization { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
