﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Smt.Atomic.WebApp.Areas.Resume.Models
{
    public class ResumeTrainingViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }
        
        public DateTime Date { get; set; }

        public string Institution { get; set; }

        public int Type { get; set; }

        public string TypeName { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
