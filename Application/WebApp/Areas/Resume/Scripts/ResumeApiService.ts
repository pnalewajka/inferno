﻿module ResumeApi {
    let Skills: SkillsDictionary[];
    let SkillTags: SkillTagDictionary[];
    let Languages: DictionaryValues[];
    let Companies: DictionaryValues[];
    let Industries: DictionaryValues[];

    let ExpertiseLevels: EnumValues[];
    let ExpertisePeriods: EnumValues[];
    let LanguageLevels: EnumValues[];
    let TrainingTypes: EnumValues[];
    let Suggestion: ResumeSuggestion;

    let DegreeList: string[] = [
        "None",
        "Dipl. Inform.",
        "B.E.- Bachelor of Engineering",
        "B.A.– Bachelor of Arts",
        "B.Sc.– Bachelor of Science",
        "M.A.– Master of Art",
        "M.Sc.– Master of Science",
        "Ph.D.– Doctor of Philosophy",
        "Postgraduate Studies"
    ];

    export function InitializeDictionaries(data: any) {
        ExpertiseLevels = data.ExpertiseLevels;
        ExpertisePeriods = data.ExpertisePeriods;
        Languages = data.Languages;
        LanguageLevels = data.LanguageLevels;
        TrainingTypes = data.TrainingTypes;
        SkillTags = data.SkillTags;
        Skills = data.Skills;
        Companies = data.Companies;
        Industries = data.Industries;
        Suggestion = data.Suggestion;
        Languages.unshift({ Id: 0, Name: "" });
    }

    export function SaveResume<T>(model: MyResume): Promise<T> {
        var scriptURL = "/Resume/ResumeApi/SaveMyResume";

        return new Promise((resolve, reject) => {
            $.post(scriptURL, model)
                .then(data => resolve(JSON.parse(data)))
                .fail(reject);
        });
    }

    export function GenerateMyResumes(employeeIds: Array<number>) {
        const url = `/Allocation/Employee/GenerateResumes?employeeIds=${employeeIds}`;
        Dialogs.showDialog({ actionUrl: url });
    }

    export function GenerateResumes(grid: Grid.GridComponent) {
        let employees = grid.getSelectedRowData<Grid.IRow>();

        if (employees.length > 0) {
            GenerateMyResumes(employees.map(({ id }) => id));
        }
    }

    export function GetProjects(name: string, callback: (projects: ProjectsDictionary[] | undefined) => void) {
        $.get(`/Resume/ResumeApi/GetProjects?topProjects=5&nameFilter=${name}`, {}, callback);
    }

    export function GetProjectDescription(id: number, callback: (description: string) => void) {
        $.get(`/Resume/ResumeApi/GetProjectDescription/${id}`, {}, callback);
    }

    export function GetSkills(): SkillsDictionary[] {
        return Skills;
    }

    export function GetSkillExpertiseLevels(): EnumValues[] {
        return ExpertiseLevels;
    }

    export function GetSkillExpertisePeriods(): EnumValues[] {
        return ExpertisePeriods;
    }

    export function GetSkillTags(): SkillTagDictionary[] {
        if (SkillTags.length > 0) {
            return SkillTags.sort((a, b) => {
                if (a.SkillTagOrder > b.SkillTagOrder) {
                    return 1;
                } else if (a.SkillTagOrder < b.SkillTagOrder) {
                    return -1;
                } else {
                    return 0;
                }
            });
        }
        return SkillTags;
    }

    export function GetLanguages(): DictionaryValues[] {
        return Languages;
    }

    export function GetDegrees(): string[] {
        return DegreeList;
    }

    export function GetLanguageLevels(): EnumValues[] {
        return LanguageLevels;
    }

    export function GetTrainingTypes(): EnumValues[] {
        return TrainingTypes;
    }

    export function GetCompanies(): DictionaryValues[] {
        return Companies;
    }

    export function GetIndustries(): DictionaryValues[] {
        return Industries;
    }

    export function GetSuggestion(): ResumeSuggestion {
        return Suggestion;
    }

    export function GetCompletnessPercentage(store: ResumeState): number {
        var completness: number = 0;

        if (store.Content.Resume.Summary && store.Content.Resume.Summary.length >= 80)
            completness += 20;

        if (store.Content.ResumeCompanies.length > 0) {
            completness += 20;

            var projects = store.Content.ResumeCompanies.map(p => {
                return p.ResumeProjects;
            }).reduce((a, b) => { return a.concat(b) });

            if (projects.length > 0) {
                completness += 20;
            }
        }

        if (store.Content.ResumeTechnicalSkills.length > 4) {
            completness += 20;
        }

        if (store.Content.ResumeEducations.length > 0) {
            completness += 10;
        }

        if (store.Content.ResumeLanguages.length > 0) {
            completness += 10;
        }

        return completness;
    }

    export function OpenResumeShareDialog(url: string) {
        Dialogs.showDialog({
            actionUrl: url,
            eventHandler: e => {
                if (e.buttonTag !== "cancel") {
                    ShowUrlForResumeShareDialog(url);
                }
            }
        });
    }

    function ShowUrlForResumeShareDialog(url: string) {
        Dialogs.showDialog({ actionUrl: url });
    }
}