﻿class LanguageTableComponent extends React.Component<ResumeLanguageTableProps, ResumeLanguageTableState> {
    constructor(props: ResumeLanguageTableProps, context: ResumeLanguageTableState) {
        super(props, context);
        this.state = {
            Data: []
        };
    }

    componentWillReceiveProps(props: ResumeLanguageTableProps) {
        this.setState({
            Data: props.Data
        });
    }

    componentDidUpdate(prevProps: Readonly<ResumeLanguageTableProps>, prevState: Readonly<ResumeLanguageTableState>, prevContext: any): void {
        this.props.StateChanged(this.state.Data);
    }

    updateLanguageProps = (stateData: MyResumeLanguage[], index: number, newProps: any): MyResumeLanguage[] => {
        let newElements = stateData.map((c, ind) => {
            if (ind != index) {
                return c;
            }
            else {
                return ResumeHelper.MergeElement(c, newProps);
            }
        });

        return newElements;
    }

    onLanguageActionHandler = (args: ActionArgs<DictionaryValues>) => {
        switch (args.Type) {
            case ResumeActionsTypes.AddNewLanguage:
                this.setState(prev => {
                    return ResumeHelper.MergeElement(this.state, {
                        Data: [...prev.Data, {
                            Id: 0,
                            LanguageId: 0,
                            LanguageName: "",
                            Level: 0,
                            LevelName: "",
                            Timestamp: ""
                        }]
                    });
                });
                break;
            case ResumeActionsTypes.RemoveLanguage:
                this.setState(prev => {
                    let newElements = [...prev.Data];
                    newElements.splice(args.Index!, 1);
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeLanguageLevel:
                this.setState(prev => {
                    let newElements = this.updateLanguageProps([...prev.Data], args.Index!, { Level: args.Data!.Id, LevelName: args.Data!.Name });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeLanguageName:
                this.setState(prev => {
                    let newElements = this.updateLanguageProps([...prev.Data], args.Index!, { LanguageId: args.Data!.Id, LanguageName: args.Data!.Name });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
        }
    }

    render() {
        return (
            <div id="resume-languages">
                <div className="resume-languages container">
                    <div className="resume-explanation col-sm-12">
                        {Globals.resources.ResumeLanguagesExplanation}
                        <span
                            className={'resume-languages-add-btn fa fa-plus'}
                            title={Globals.resources.LanguagesAddLanguage}
                            onClick={() => this.onLanguageActionHandler({ Type: ResumeActionsTypes.AddNewLanguage })}> </span>
                    </div>
                    <div className="row head">
                        <div className="col-sm-6">{Globals.resources.LanguagesLanguage}</div>
                        <div className="col-sm-5">{Globals.resources.LanguagesLevel}</div>
                        <div className="col-sm-1"></div>
                    </div>
                    {
                        this.state.Data.map((language, index) => <LanguageComponent key={index}
                            Data={language}
                            OnLanguageAction={this.onLanguageActionHandler}
                            Index={index} />)
                    }
                </div>
            </div>
        );
    }
}