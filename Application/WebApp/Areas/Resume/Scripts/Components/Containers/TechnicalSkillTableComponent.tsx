﻿class TechnicalSkillTableComponent extends React.Component<ResumeTechnicalSkillTableProps, ResumeTechnicalSkillTableState> {
    SkillTags: ISkillTag[] = [];

    constructor(props: ResumeTechnicalSkillTableProps, context: ResumeTechnicalSkillTableState) {
        super(props, context);
        this.state = {
            Data: {},
        };
    }

    componentWillReceiveProps(props: ResumeTechnicalSkillTableProps) {
        if (this.SkillTags.length === 0) {
            for (let tag of ResumeApi.GetSkillTags()) {
                this.SkillTags.push({ SkillTag: tag.Name, Name: tag.Name, SkillTagOrder: tag.SkillTagOrder, Data: () => { return this.state.Data[tag.Name] || [] } });
            }
        }

        let Data: any = {};
        for (let tag of ResumeApi.GetSkillTags()) {
            Data[tag.Name] = props.Data.filter(f => f.SkillTagName == tag.Name);
        }
        this.setState({ Data: Data });
    }

    componentDidUpdate(prevProps: Readonly<ResumeTechnicalSkillTableProps>, prevState: Readonly<ResumeTechnicalSkillTableState>, prevContext: any): void {
        if (this.SkillTags.length > 0) {
            let technicalSkills = this.SkillTags.map(tag => this.state.Data[tag.SkillTag])
                .reduce((a, b) => a.concat(b));

            this.props.StateChanged(technicalSkills);
        }
    }

    DiscardEditing = (index: number, skill: MyResumeTechnicalSkill) => {
        this.CloseSkillsEditingForm(index, skill);
    }

    RemoveComponent = (event: MouseEvent, index: number, skill: MyResumeTechnicalSkill) => {
        this.setState(prev => {
            let newState = ResumeHelper.MergeElement({}, prev.Data);
            newState[skill.SkillTagName] = newState[skill.SkillTagName].filter((s: MyResumeTechnicalSkill) => s != skill);
            return ResumeHelper.MergeElement(this.state, { Data: newState });
        });

        event.stopPropagation();
    }

    SaveComponent = (index: number, skill: MyResumeTechnicalSkill): void => {
        if (skill.ExpertiseLevel == 0 || skill.ExpertisePeriod == 0) {
            return;
        }

        if (this.ExistDuplicateTechnicalSkillName(skill.Name, index, skill)) {
            return;
        }

        if (skill.TechnicalSkillId == 0) {
            if (!this.SetTechnicalSkillIdByName(name, index, skill)) {
                return;
            }
        }

        this.UpdateSkillState(index, skill);
    }

    TechnicalSkillChange = (name: string, index: number, skill: MyResumeTechnicalSkill) => {
        this.SetTechnicalSkillIdByName(name, index, skill);
    }

    AutoCompleteClicked = (suggestion: string, id: number, index: number, skill: MyResumeTechnicalSkill) => {
        let skillCopy = ResumeHelper.MergeElement({}, skill) as MyResumeTechnicalSkill;
        skillCopy.Name = suggestion;
        skillCopy.TechnicalSkillId = id;
        skillCopy.Error = false;
        this.UpdateSkillState(index, skillCopy);
    }

    UpdateSkillState = (index: number, skill: MyResumeTechnicalSkill) => {
        this.setState(prev => {
            let newState = ResumeHelper.MergeElement({}, prev.Data);
            if (index === -1) {
                newState[skill.SkillTagName] = [...newState[skill.SkillTagName], skill]
            } else {
                newState[skill.SkillTagName][index] = { ...newState[skill.SkillTagName][index], ...skill };
            }

            return { ...this.state, ...{ Data: newState } };
        });
    }

    CloseSkillsEditingForm = (index: number, skill: MyResumeTechnicalSkill) => {

        this.setState(prev => {
            let newState = ResumeHelper.MergeElement({}, prev.Data);

            for (let index in newState) {
                newState[index] = newState[index].map((x: any) => {
                    delete x.Edit;

                    return x;
                });
            }

            return ResumeHelper.MergeElement(this.state, { Data: newState });
        });
    }

    SetTechnicalSkillIdByName = (name: string, index: number, skill: MyResumeTechnicalSkill): boolean => {
        let skillCopy = ResumeHelper.MergeElement({}, skill) as MyResumeTechnicalSkill;
        skillCopy.TechnicalSkillId = 0;
        skillCopy.Name = name;

        let typedSkill = ResumeApi.GetSkills().filter(s => s.SkillTagIds.indexOf(skillCopy.SkillTagId) >= 0 && s.Name == skillCopy.Name);
        if (typedSkill.length == 0) {
            skillCopy.Error = true;

            this.UpdateSkillState(index, skillCopy);

            return false;
        }

        skillCopy.Error = false;
        skillCopy.TechnicalSkillId = typedSkill[0].Id;

        this.UpdateSkillState(index, skillCopy);

        return true;
    }

    ExistDuplicateTechnicalSkillName = (name: string, index: number, skill: MyResumeTechnicalSkill): boolean => {
        const exists = this.state.Data[skill.SkillTagName].filter((a, i) => a.Name === skill.Name && index !== i).length > 0;

        return exists;
    }


    GetEnumNameFromValue(value: number, values: EnumValues[]): string {
        if (values == null || values.length == 0) {
            return "";
        }

        var filtered = values.filter(e => e.Value == value);

        if (filtered.length > 0) {
            return filtered[0].Name;
        }

        return "";
    }
    GetExistingSkillNames = (): string[] => {
        if (this.SkillTags.length === 0) {
            return [];
        }
        return this.SkillTags.map(tag => {
            return this.state.Data[tag.SkillTag].filter(skill => skill.Edit == null || !skill.Edit).map(skill => skill.Name.toLowerCase())
        }).reduce((a, b) => {
            return a.concat(b);
        });
    }

    public AnyElementInEditMode = (): boolean => {
        return this.SkillTags.filter(s => this.state.Data[s.SkillTag].filter(ts => ts.Edit == true).length > 0).length > 0;
    }

    render() {
        if (this.SkillTags.length === 0) {
            return (
                <div className="resume-no-skill-tags-info">{Globals.resources.NoSkillTagsInfo}</div>
            );
        }

        return (
            <div id="resume-technical-skills">
                <div>
                    <div className="resume-explanation skills col-sm-12">{Globals.resources.ResumeTechnicalSkillsExplanation}</div>
                    <div className="intv-technical-skills">
                        {
                            this.SkillTags
                                .map((tag, index) => <TechnicalSkillComponent key={index}
                                    SkillTag={tag.SkillTag}
                                    Name={tag.Name}
                                    Data={tag.Data()}
                                    ExistingSkillNames={this.GetExistingSkillNames()}
                                    RemoveComponent={this.RemoveComponent}
                                    DiscardEditing={this.DiscardEditing}
                                    SaveChanges={this.SaveComponent}
                                    Index={index} />)
                        }
                    </div>
                </div>
            </div>
        );
    }
}