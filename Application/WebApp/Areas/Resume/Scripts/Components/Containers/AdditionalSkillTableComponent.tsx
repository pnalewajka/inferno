﻿class AdditionalSkillTableComponent extends React.Component<ResumeAdditionalSkillTableProps, ResumeAdditionalSkillTableState> {

    constructor(props: ResumeAdditionalSkillTableProps, context: ResumeAdditionalSkillTableState) {
        super(props, context);
          this.state = {
            Data: []
        };
    }

    public componentWillReceiveProps(props: ResumeAdditionalSkillTableProps): void {
        this.setState({
            Data: props.Data,           
        });
    }

    private addComponent = (): void => {
        if (this.anyElementInEditMode()) {
            return;
        }

        let additionalSkill: MyResumeAdditionalSkill = {
            Id: 0,
            Name: ""
        }

        this.setState(prev => {
            return ResumeHelper.MergeElement(this.state, {
                Data: [...prev.Data, {
                    Id: 0,
                    Name: "",
                    Edit: true
                }]
            });
        });
    }

    private additionalSkillChange = (data: string, index: number): void => {
        this.setState(prev => {
            let newElements = this.updateSkillProps([...prev.Data], index!, { Name: data });
            return ResumeHelper.MergeElement(this.state, { Data: newElements });
        });
    }

    private updateSkillProps = (stateData: MyResumeAdditionalSkill[], index: number, newProps: any): MyResumeAdditionalSkill[] => {
        let newElements = stateData.map((c, ind) => {
            if (ind != index) {
                return c;
            }
            else {
                return ResumeHelper.MergeElement(c, newProps);
            }
        });

        return newElements;
    }

    private removeComponent = (event: React.MouseEvent<HTMLSpanElement>, index: number, skill: MyResumeAdditionalSkill): void => {
        this.setState(prev => {
            let newElements = [...prev.Data];
            newElements.splice(index, 1);
            return ResumeHelper.MergeElement(this.state, { Data: newElements });
        });
        event.stopPropagation();
    }

    private saveComponent = (event: React.MouseEvent<HTMLSpanElement>, index: number, skill: MyResumeAdditionalSkill): void => {
        if (skill.Name.length === 0) {
            return;
        }

        let skillCopy = ResumeHelper.MergeElement({}, skill) as MyResumeAdditionalSkill;
        skillCopy.Edit = false;
        this.setState(prev => {
            let newElements = this.updateSkillProps([...prev.Data], index, { Name: skillCopy.Name, Edit: false });
            return ResumeHelper.MergeElement(this.state, { Data: newElements });
        });

        event.stopPropagation();
    }

    public componentDidUpdate(prevProps: Readonly<ResumeAdditionalSkillTableProps>, prevState: Readonly<ResumeAdditionalSkillTableState>, prevContext: any): void {
        this.props.StateChanged(this.state.Data);
    }

    public anyElementInEditMode = (): boolean => {
        return this.state.Data.filter(as => as.Edit == true).length > 0;
   }

    public render(): JSX.Element {

        return (
            <div id="resume-additional-skills">
                <div>
                    <div className="resume-additional-skill-explanation col-sm-12">
                        {Globals.resources.ResumeAdditionalSkillsExplanation}
                    </div>
                    <div className="intv-technical-skills">
                        <div className="intv-technical-skills-row">
                            <div className="intv-technical-skills-row-name">
                                {Globals.resources.AdditionalSkillsHeader}
                            </div>
                            <div onClick={(event) => this.addComponent()} className="intv-technical-skills-row-content">
                                {
                                    this.state.Data.map((skill, index) =>
                                        <AdditionalSkillComponent key={index}
                                            Name={skill.Name}
                                            Data={skill}
                                            Edit={skill.Edit}
                                            AddComponent={this.addComponent}
                                            AdditionalSkillChange={this.additionalSkillChange}
                                            SaveChanges={this.saveComponent}
                                            RemoveComponent={this.removeComponent}
                                            Index={index} />)
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}