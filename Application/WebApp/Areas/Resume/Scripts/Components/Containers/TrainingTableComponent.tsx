﻿class TrainingTableComponent extends React.Component<ResumeTrainingTableProps, ResumeTrainingTableState> {
    constructor(props: ResumeTrainingTableProps, context: ResumeTrainingTableState) {
        super(props, context);
        this.state = {
            Data: []
        };
    }

    componentWillReceiveProps(props: ResumeTrainingTableProps) {
        this.setState({
            Data: props.Data
        });
    }

    componentDidUpdate(prevProps: Readonly<ResumeTrainingTableProps>, prevState: Readonly<ResumeTrainingTableState>, prevContext: any): void {
        this.props.StateChanged(this.state.Data);
    }

    updateTrainingProps = (stateData: MyResumeTraining[], index: number, newProps: any): MyResumeTraining[] => {
        let newElements = stateData.map((c, ind) => {
            if (ind != index) {
                return c;
            }
            else {
                return ResumeHelper.MergeElement(c, newProps);
            }
        });

        return newElements;
    }

    onTrainignActionHandler = (args: ActionArgs<any>) => {
        switch (args.Type) {
            
            case ResumeActionsTypes.AddNewTraining:
                this.setState(prev => {
                    return ResumeHelper.MergeElement(this.state, {
                        Data: [...prev.Data, {
                            Id: 0,
                            Title: "",
                            Date: "",
                            Institution: "",
                            Type: 0,
                            TypeName: "",
                            Timestamp: ""
                        }] });
                });

                this.props.OnRecordAdded();
                break;
            case ResumeActionsTypes.RemoveTraining:
                this.setState(prev => {
                    let newElements = [...prev.Data];
                    newElements.splice(args.Index as number, 1);
                    
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeTrainingDate:
                this.setState(prev => {
                    let newElements = this.updateTrainingProps([...prev.Data], args.Index as number, { Date: args.Data.Date });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeTrainingInstitution:
                this.setState(prev => {
                    let newElements = this.updateTrainingProps([...prev.Data], args.Index as number, { Institution: args.Data.Institution });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeTrainingTitle:
                this.setState(prev => {
                    let newElements = this.updateTrainingProps([...prev.Data], args.Index as number, { Title: args.Data.Title });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeTrainingType:
                this.setState(prev => {
                    let newElements = this.updateTrainingProps([...prev.Data], args.Index as number, { TypeName: args.Data.TypeName, Type: args.Data.Type });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
        }
    }


    render() {
        return (
            <div id="resume-training">
                <div className="resume-training container">
                    <div className="resume-explanation col-sm-12">
                        {Globals.resources.ResumeCoursesExplanation}
                        <span
                            title={Globals.resources.CoursesAddTraining}
                            className='fa fa-plus resume-training-add-btn'
                            onClick={() => this.onTrainignActionHandler({ Type: ResumeActionsTypes.AddNewTraining })}>
                        </span>
                    </div>
                    <div className="row head">
                        <div className="col-sm-3">{Globals.resources.CoursesTitle}</div>
                        <div className="col-sm-2">{Globals.resources.CoursesDate}</div>
                        <div className="col-sm-3">{Globals.resources.CoursesInstitution}</div>
                        <div className="col-sm-3">{Globals.resources.CoursesType}</div>
                        <div className="col-sm-1"></div>
                    </div>
                    {
                        this.state.Data.map((training, index) => <TrainingComponent key={index}
                            Data={training}
                            OnTrainingAction={this.onTrainignActionHandler}
                            Index={index} />)
                    }
                </div>
            </div>
        );
    }
}