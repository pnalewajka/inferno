﻿class EducationTableComponent extends React.Component<ResumeEducationTableProps, ResumeEducationTableState> {
    constructor(props: ResumeEducationTableProps, context: ResumeEducationTableState) {
        super(props, context);
        this.state = {
            Data: []
        };
    }

    componentWillReceiveProps(props: ResumeEducationTableProps) {
        this.setState({
            Data: props.Data
        });
    }

    componentDidUpdate(prevProps: Readonly<ResumeEducationTableProps>, prevState: Readonly<ResumeEducationTableState>, prevContext: any): void {
        this.props.StateChanged(this.state.Data);
    }

    updateEducationProps = (stateData: MyResumeEducation[], index: number, newProps: any): MyResumeEducation[] => {
        let newElements = stateData.map((c, ind) => {
            if (ind != index) {
                return c;
            }
            else {
                return ResumeHelper.MergeElement(c, newProps);
            }
        });

        return newElements;
    }

    onEducationActionHandler = (args: ActionArgs<string>) => {
        switch (args.Type) {
            case ResumeActionsTypes.AddNewEducation:
                this.setState(prev => {
                    return ResumeHelper.MergeElement(this.state, {
                        Data: [...prev.Data, {
                            Id: 0,
                            UniversityName: "",
                            From: "",
                            To: null,
                            Degree: "None",
                            Specialization: "",
                            Timestamp: ""
                            }
                        ]
                    });
                });
                break;
            case ResumeActionsTypes.RemoveEducation:
                this.setState(prev => {
                    let newElements = [...prev.Data];
                    newElements.splice(args.Index!, 1);
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeEducationDegree:
                this.setState(prev => {
                    let newElements = this.updateEducationProps([...prev.Data], args.Index!, { Degree: args.Data });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeEducationFrom:
                this.setState(prev => {
                    let newElements = this.updateEducationProps([...prev.Data], args.Index!, { From: args.Data });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeEducationTo:
                this.setState(prev => {
                    let newElements = this.updateEducationProps([...prev.Data], args.Index!, { To: args.Data });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeEducationSpecialization:
                this.setState(prev => {
                    let newElements = this.updateEducationProps([...prev.Data], args.Index!, { Specialization: args.Data });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.ChangeEducationUniversityName:
                this.setState(prev => {
                    let newElements = this.updateEducationProps([...prev.Data], args.Index!, { UniversityName: args.Data });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
        }
    }

    render() {
        return (
            <div id="resume-education">
                <div className="resume-education container">
                    
                    <div className="resume-explanation col-sm-12">
                        {Globals.resources.ResumeEducationExplanation}
                        <span
                            className="fa fa-plus resume-education-add-btn"
                            title={Globals.resources.EducationAddEducation}
                            onClick={() => this.onEducationActionHandler({ Type: ResumeActionsTypes.AddNewEducation })}>
                        </span>
                    </div>
                    <div className="row head">
                        <div className="col-sm-2">{Globals.resources.EducationFrom}</div>
                        <div className="col-sm-2">{Globals.resources.EducationTo}</div>
                        <div className="col-sm-3">{Globals.resources.EducationUniversity}</div>
                        <div className="col-sm-2">{Globals.resources.EducationSpecialization}</div>
                        <div className="col-sm-2">{Globals.resources.EducationDegree}</div>
                        <div className="col-sm-1"></div>
                    </div>
                    {
                        this.state.Data.map((education, index) => <EducationComponent key={index}
                            Data={education}
                            OnEducationAction={this.onEducationActionHandler}
                            Index={index} />)
                    }
                </div>
            </div>
        );
    }
}