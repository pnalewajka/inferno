﻿class SuggestionsDialogComponent extends React.Component<ResumeSuggestionsDialogProps, ResumeSuggestionsDialogState> {
    public constructor(props: ResumeSuggestionsDialogProps, context: ResumeSuggestionsDialogState) {
        super(props, context);
        this.state = {
            data: {
                SuggestionReason: "",
                SkillSuggestions: []
            },
            isClosed: false
        };
    }

    public componentWillReceiveProps(props: ResumeSuggestionsDialogProps): void {
        this.setState(state => {
            return {
                data: ResumeApi.GetSuggestion()
            }
        });
    }

    private onCloseProposedSkillsModal(): void {
        this.setState(state => {
            return {
                isClosed: true
            }
        });
    }

    private statusChange(skill: SuggestedSkill): void {
        this.setState(prev => {
            skill.Confirmed = !skill.Confirmed;
            return prev;
        });
    }

    private expertiseLevelChange(value: string, name: string, skill: MyResumeTechnicalSkill): void {
        this.setState(prev => {
            skill.ExpertiseLevel = Number(value);
            skill.ExpertiseLevelName = name;
            return prev;
        });
    }

    private expertisePeriodChange(value: string, name: string, skill: MyResumeTechnicalSkill): void {
        this.setState(prev => {
            skill.ExpertisePeriod = Number(value);
            skill.ExpertisePeriodName = name;
            return prev;
        });
    }

    private onSubmit(): void {
        if (this.selectedSkillsAreValid()) {
            this.props.submit(this.state.data.SkillSuggestions);
            this.onCloseProposedSkillsModal();
        }
    }

    private selectedSkillsAreValid(): boolean {
        return !this.state.data.SkillSuggestions.some(s => s.Confirmed == true && (s.ExpertiseLevel == 0 || s.ExpertisePeriod == 0));
    }

    public render(): JSX.Element{
        return (
            <div className={this.state.data.SkillSuggestions.length > 0 && !this.state.isClosed ? "modal show" : "modal fade"} id="suggestedSkillsModal" role="dialog" >
                <div className="resume-modal-vertical-alignment-helper">
                    <div className="resume-suggestions-modal">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" onClick={e => this.onCloseProposedSkillsModal()}>&times;</button>
                                <h4 className="modal-title">{this.state.data.SuggestionReason}</h4>
                            </div>
                            <div className="modal-body" ref="modalbody">
                                {
                                    this.state.data.SkillSuggestions.map((skill, index) => (
                                        <SuggestedSkillPillComponent key={index}
                                            data={skill}
                                            expertiseLevelChange={this.expertiseLevelChange.bind(this)} expertisePeriodChange={this.expertisePeriodChange.bind(this)}
                                            statusChange={this.statusChange.bind(this)} />
                                    ))
                                }
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" onClick={e => this.onSubmit()}>Ok</button>
                                <button type="button" className="btn btn-default" onClick={e => this.onCloseProposedSkillsModal()}>{Globals.resources.RemindMeLater}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}