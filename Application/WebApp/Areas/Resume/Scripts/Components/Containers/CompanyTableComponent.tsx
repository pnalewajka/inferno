﻿class CompanyTableComponent extends React.Component<ResumeCompanyTableProps, ResumeCompanyTableState> {
    constructor(props: ResumeCompanyTableProps, context: ResumeCompanyTableState) {
        super(props, context);
        this.state = {
            Data: []
        };
    };

    componentWillReceiveProps(props: ResumeCompanyTableProps) {
        this.setState({
            Data: props.Data
        });
    };

    componentDidUpdate(prevProps: Readonly<ResumeCompanyTableProps>, prevState: Readonly<ResumeCompanyTableState>, prevContext: any): void {
        this.props.StateChanged(this.state);
    };

    updateCompanyProps = (stateData: MyResumeCompany[], index: number, newProps: any): MyResumeCompany[] => {
        let newElements = stateData.map((c, ind) => {
            if (ind !== index) {
                return c;
            } else {
                return ResumeHelper.MergeElement(c, newProps);
            }
        });

        return newElements;
    };

    updateProjectProps = (companyIndex: number, projectIndex: number, newProps: any): void => {
        this.setState(prev => {
            let newState = [...prev.Data].map((c, cindex) => {
                if (cindex === companyIndex) {
                    return ResumeHelper.MergeElement(c, {
                        ResumeProjects: [...c.ResumeProjects].map((p, pindex) => {
                            if (pindex === projectIndex) {
                                return ResumeHelper.MergeElement(p, newProps);
                            } else {
                                return p;
                            }
                        })
                    });
                } else {
                    return c;
                }
            });

            return ResumeHelper.MergeElement(this.state, newState);
        });
    };

    onCompanyActionHandler = (args: ActionArgs<any>) => {
        switch (args.Type) {
            case ResumeActionsTypes.AddNewCompany:
                this.setState(prev => {

                    return ResumeHelper.MergeElement(this.state, {
                        Data: [
                            {
                                Id: 0,
                                Name: "",
                                From: "",
                                ResumeProjects: [],
                                Timestamp: ""
                            },
                            ...prev.Data
                        ]
                    });
                });
                break;

            case ResumeActionsTypes.ChangeCompanyName:
                this.setState(prev => {
                    let newElements = [...prev.Data.map((c, index) => {
                        if (index !== args.Data.index) {
                            return c;
                        } else {
                            return ResumeHelper.MergeElement(c, {
                                Name: args.Data.companyName,
                                NameInEditMode: true,
                                ResumeProjects: this.isOurCompany(args.Data.companyName) ?
                                    [...c.ResumeProjects] :
                                    [...c.ResumeProjects.map(p => ResumeHelper.MergeElement(p, { ProjectId: 0, HintedProjects: [] }))]
                            });
                        }
                    })];

                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;

            case ResumeActionsTypes.PickCompanyName:
                this.setState(prev => {
                    let newElements = this.updateCompanyProps([...prev.Data], args.Data.index, {
                        Name: args.Data.companyName,
                        ShowSuggestions: false,
                        NameInEditMode: false
                    });

                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;

            case ResumeActionsTypes.RemoveCompany:
                CommonDialogs.confirm(Globals.resources.RemoveCompanyConfirm, "", (eventArgs) => {
                    if (eventArgs.isOk) {
                        this.setState(prev => {
                            let newElements = [...prev.Data];
                            newElements.splice(args.Data.index, 1);
                            return ResumeHelper.MergeElement(this.state, { Data: newElements });
                        });
                    }
                });
                break;

            case ResumeActionsTypes.ChangeCompanyPeriodFrom:
                this.setState(prev => {
                    let newElements = this.updateCompanyProps([...prev.Data], args.Data.index, { From: args.Data.from });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;

            case ResumeActionsTypes.ChangeCompanyPeriodTo:
                this.setState(prev => {
                    let newElements = this.updateCompanyProps([...prev.Data], args.Data.index, { To: args.Data.to });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;

            case ResumeActionsTypes.ShowCompanySuggestions:
                this.setState(prev => {
                    let newElements = this.updateCompanyProps([...prev.Data], args.Data.index, { ShowSuggestions: true });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
            case ResumeActionsTypes.HideCompanySuggestions:
                this.setState(prev => {
                    let newElements = this.updateCompanyProps([...prev.Data], args.Data.index, { ShowSuggestions: false });
                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;
        }
    };

    onProjectActionHandler = (args: ActionArgs<any>) => {
        switch (args.Type) {
            case ResumeActionsTypes.AddNewProject:
                this.setState(prev => {
                    let newProject: MyResumeProjects = {
                        Id: 0,
                        ProjectId: 0,
                        Name: "",
                        From: "",
                        Roles: "",
                        Duties: "",
                        ProjectDescription: "",
                        TechnicalSkills: [],
                        Industries: [],
                        IsProjectConfidential: false,
                        Timestamp: ""
                    };

                    if (this.isOurCompany(args.Data.company.Name.toLowerCase().trim())) {
                        ResumeApi.GetProjects("", data => {
                            newProject.HintedProjects = data;
                            this.forceUpdate();
                        });
                    }

                    let newElements = [...prev.Data.map((c, ind) => {
                        if (ind !== args.Data.index) {
                            return c;
                        } else {
                            return ResumeHelper.MergeElement(c, {
                                ResumeProjects: [newProject, ...c.ResumeProjects]
                            });
                        }
                    })];

                    return ResumeHelper.MergeElement(this.state, { Data: newElements });
                });
                break;

            case ResumeActionsTypes.RemoveProject:
                CommonDialogs.confirm(Globals.resources.RemoveProjectConfirm, "", (eventArgs) => {
                    if (eventArgs.isOk) {
                        this.setState(prev => {
                            let newState = [...prev.Data].map((c, cindex) => {
                                if (cindex === args.Data.companyIndex) {
                                    const rp = [...args.Data.company.ResumeProjects];
                                    rp.splice(args.Data.index, 1);
                                    return ResumeHelper.MergeElement(c, {
                                        ResumeProjects: rp
                                    });
                                } else {
                                    return c;
                                }
                            });

                            return ResumeHelper.MergeElement(this.state, newState);
                        });
                    }
                });
                break;

            case ResumeActionsTypes.ChangeProjectName:
                if (this.isOurCompany(args.Data.company.Name.toLowerCase().trim()) && args.Data.hasUserStoppedTyping) {
                    ResumeApi.GetProjects(args.Data.projectName, (filteredProjects) => {
                        this.updateProjectProps(args.Data.companyIndex, args.Data.index, {
                            Name: args.Data.projectName,
                            ProjectId: args.Data.projectId,
                            IsProjectConfidential: args.Data.isProjectConfidential,
                            HintedProjects: filteredProjects
                        });
                    });
                } else {
                    this.updateProjectProps(args.Data.companyIndex, args.Data.index, {
                        Name: args.Data.projectName,
                        ProjectId: args.Data.projectId,
                        IsProjectConfidential: args.Data.isProjectConfidential,
                        HintedProjects: []
                    });
                }
                break;

            case ResumeActionsTypes.PickProject:
                ResumeApi.GetProjectDescription(args.Data.projectId, (description) => {
                    this.updateProjectProps(args.Data.companyIndex, args.Data.index, {
                        Name: args.Data.projectName,
                        ProjectId: args.Data.projectId,
                        ProjectDescription: description,
                        IsProjectConfidential: args.Data.isProjectConfidential,
                        HintedProjects: []
                    });
                });
                break;

            case ResumeActionsTypes.ChangeProjectRoles:
                this.updateProjectProps(args.Data.companyIndex, args.Data.index, { Roles: args.Data.roles });
                break;

            case ResumeActionsTypes.ChangeProjectDuties:
                this.updateProjectProps(args.Data.companyIndex, args.Data.index, { Duties: args.Data.duties });
                break;

            case ResumeActionsTypes.ChangeProjectDescription:
                this.updateProjectProps(args.Data.companyIndex, args.Data.index, { ProjectDescription: args.Data.projectDescription });
                break;

            case ResumeActionsTypes.ChangeProjectPeriodFrom:
                this.updateProjectProps(args.Data.companyIndex, args.Data.index, { From: args.Data.from });
                break;

            case ResumeActionsTypes.ChangeProjectPeriodTo:
                this.updateProjectProps(args.Data.companyIndex, args.Data.index, { To: args.Data.to });
                break;

            case ResumeActionsTypes.AddProjectSkill:
                if (this.state.Data[args.Data.companyIndex].ResumeProjects[args.Data.projectIndex].TechnicalSkills
                    .filter(x => x.Id === 0).length > 0) {
                    return;
                }

                this.setState(prev => {
                    let newState = ResumeHelper.MergeElement({}, prev);
                    newState.Data[args.Data.companyIndex].ResumeProjects[args.Data.projectIndex].TechnicalSkills.push({ Id: 0, Name: "" });
                    return newState;
                });
                break;
            case ResumeActionsTypes.RemoveProjectSkill:
                this.setState(prev => {
                    let newState = ResumeHelper.MergeElement({}, prev);
                    newState.Data[args.Data.companyIndex].ResumeProjects[args.Data.projectIndex].TechnicalSkills.splice(
                        args.Data.skillIndex,
                        1);

                    return newState;
                });

                break;
            case ResumeActionsTypes.UpdateProjectSkill:
                if (args.Data.project.TechnicalSkills.filter((x: any) => x.Id === args.Data.id).length > 0) {
                    return;
                }

                this.setState(prev => {
                    let newElements = [...args.Data.project.TechnicalSkills];
                    newElements[args.Data.index] = { Id: args.Data.id, Name: args.Data.value };
                    args.Data.project.TechnicalSkills = newElements;

                    return { Data: [...prev.Data] };
                });
                break;
            case ResumeActionsTypes.AddProjectIndustry:
                if (this.state.Data[args.Data.companyIndex].ResumeProjects[args.Data.projectIndex].Industries
                    .filter(x => x.Id === 0).length > 0) {
                    return;
                }

                this.setState(prev => {
                    let newState = ResumeHelper.MergeElement({}, prev);
                    newState.Data[args.Data.companyIndex].ResumeProjects[args.Data.projectIndex].Industries.push({ Id: 0, Name: "" });
                    return newState;
                });
                break;
            case ResumeActionsTypes.RemoveProjectIndustry:
                this.setState(prev => {
                    let newState = ResumeHelper.MergeElement({}, prev);
                    newState.Data[args.Data.companyIndex].ResumeProjects[args.Data.projectIndex].Industries.splice(args.Data.industryIndex, 1);

                    return newState;
                });
                break;
            case ResumeActionsTypes.UpdateProjectIndustry:
                if (args.Data.project.Industries.filter((x: any) => x.Id === args.Data.id).length > 0) {
                    return;
                }

                this.setState(prev => {
                    let newElements = [...args.Data.project.Industries];
                    newElements[args.Data.index] = { Id: args.Data.id, Name: args.Data.value };
                    args.Data.project.Industries = newElements;

                    return { Data: [...prev.Data] };
                });
                break;
        }
    };

    isOurCompany = (name: string): boolean => {
        return ResumeApi.GetCompanies().filter(c => c.Name.toLowerCase().trim() === name.toLowerCase().trim()).length > 0;
    };

    render() {
        return (
            <div id="resume-work-experience">
                <div className="resume-work-experience container">
                    <div className="resume-explanation col-sm-12">
                        {Globals.resources.ResumeExpirienceExplanation}
                        <span title={Globals.resources.ExperienceAddCompany} className="fa fa-plus add-button-on-nav"
                            onClick={() => this.onCompanyActionHandler({ Type: ResumeActionsTypes.AddNewCompany })}>
                        </span>
                    </div>
                    {
                        this.state.Data.map(
                            (company, index) =>
                                <CompanyComponent
                                    key={index}
                                    Data={company}
                                    Index={index}
                                    OnCompanyAction={this.onCompanyActionHandler}
                                    OnProjectAction={this.onProjectActionHandler}
                                />)
                    }
                </div>
            </div>
        );
    }
}