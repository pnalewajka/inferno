﻿const ConsentClauseForDataProcessingComponent: React.StatelessComponent<ResumeConsentClauseForDataProcessingProps> = (props: ResumeConsentClauseForDataProcessingProps) => {
    var clauseForDataProcessing = [Globals.parameters["Resume.IntiveIs"], Globals.parameters["Resume.ConsentClauseForDataProcessing"]];

    if (!props.Show) {
        return (<div className="resume-data-processin-clause container"></div>);
    }

    return (
        <div id="resume-data-processing-clause" className="resume-data-processing-clause">
            <CheckboxInput onCheckChanged={props.StateChanged} multiLineLabel={clauseForDataProcessing} label="" checked={props.Data.IsConsentClauseForDataProcessingChecked} />
        </div>
    );
}
