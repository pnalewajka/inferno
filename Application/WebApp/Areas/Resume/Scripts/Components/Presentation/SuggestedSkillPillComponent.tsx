﻿const SuggestedSkillPillComponent: React.StatelessComponent<SuggestedSkillPillProps> = (props: SuggestedSkillPillProps) => {
    return (
        <div className="intv-suggested-skill-pill" >
            <label className="intv-suggested-skill-pill-checkbox-container" >
                <span className={props.data.Confirmed ? "intv-suggested-skill-pill-checkmark-checked" : "intv-suggested-skill-pill-checkmark"} onClick={e => props.statusChange(props.data)}></span>
            </label>
            <span className="intv-suggested-skill-pill-name" onClick={e => props.statusChange(props.data)}>{props.data.Name}</span>
            <div className="intv-suggested-skill-pill-select">
                <select required className="intv-suggested-skills-row-content-select" value={props.data.ExpertiseLevel} onChange={(event) => props.expertiseLevelChange(event.target.value, event.target.options[event.target.selectedIndex].text, props.data)}>
                    {
                        ResumeApi.GetSkillExpertiseLevels().map(level =>
                            <option key={level.Value} value={level.Value > 0 ? level.Value : ""}>{level.Name}</option>
                        )
                    }
                </select>
                <select required className="intv-suggested-skills-row-content-select" value={props.data.ExpertisePeriod} onChange={(event) => props.expertisePeriodChange(event.target.value, event.target.options[event.target.selectedIndex].text, props.data)}>
                    {
                        ResumeApi.GetSkillExpertisePeriods().map(level =>
                            <option key={level.Value} value={level.Value > 0 ? level.Value : ""}>{level.Name}</option>
                        )
                    }
                </select>
            </div>
        </div>
    );
}
