﻿const TrainingComponent: React.StatelessComponent<ResumeTrainingProps> = (props: ResumeTrainingProps) => {
    return (
        <div className="row">
            <div className="col-sm-3">
                <input required type="text" value={props.Data.Title} onChange={e => props.OnTrainingAction({ Type: ResumeActionsTypes.ChangeTrainingTitle, Index: props.Index, Data: { Title: e.target.value } })} />
            </div>
            <div className="col-sm-2">
                <DatePicker required={true} value={props.Data.Date} yearView={true} onChange={v => props.OnTrainingAction({ Type: ResumeActionsTypes.ChangeTrainingDate, Index: props.Index, Data: { Date: v } })} />
            </div>
            <div className="col-sm-3">
                <input required type="text" value={props.Data.Institution} onChange={e => props.OnTrainingAction({ Type: ResumeActionsTypes.ChangeTrainingInstitution, Index: props.Index, Data: { Institution: e.target.value } })} />
            </div>
            <div className="col-sm-3">
                <select
                    onChange={e => props.OnTrainingAction({ Type: ResumeActionsTypes.ChangeTrainingType, Index: props.Index, Data: { Type: Number(e.target.value), TypeName: e.target.options[e.target.selectedIndex].textContent } })} 
                    value={props.Data.Type}>
                    {
                        ResumeApi.GetTrainingTypes().map(type =>
                            <option key={type.Value} value={type.Value}>{type.Name}</option>
                        )
                    }
                </select>
            </div>
            <div className="col-sm-1">
                <span onClick={e => props.OnTrainingAction({ Type: ResumeActionsTypes.RemoveTraining, Index: props.Index })} className="fa fa-minus-square"></span>
            </div>
        </div>
    );
}
