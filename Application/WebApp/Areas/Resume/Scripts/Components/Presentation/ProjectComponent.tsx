﻿class ProjectComponent extends React.Component<ResumeProjectProps, { isCollapsed: boolean, lastInputChangeOn: Date}> {
    constructor() {
        super();
        this.state = { isCollapsed: false, lastInputChangeOn: new Date() };
    }

    readonly inputOnChangeDelayTime = 1000;
    projectNameInput: HTMLInputElement;

    changeProjectName(props: ResumeProjectProps, hasUserStoppedTyping: boolean) {
        props.OnProjectAction({
            Type: ResumeActionsTypes.ChangeProjectName,
            Data: {
                company: props.Company,
                index: props.Index,
                companyIndex: props.CompanyIndex,
                projectName: this.projectNameInput.value,
                projectId: 0,
                isProjectConfidential: false,
                hasUserStoppedTyping: hasUserStoppedTyping
            }
        });
    }

    onInputChanged(props: ResumeProjectProps) {
        this.changeProjectName(props, false);

        if (this.getTimeFromLastInputChange() > this.inputOnChangeDelayTime) {
            setTimeout(() => {
                this.changeProjectName(props, true);
            }, this.inputOnChangeDelayTime);
        }

        this.setState({
            isCollapsed: this.state.isCollapsed,
            lastInputChangeOn: new Date()
        });
    }

    getTimeFromLastInputChange() : number {
        const currentDate = new Date();
        const timeDiference = currentDate.getTime() - this.state.lastInputChangeOn.getTime();

        return timeDiference;
    }

    render() {
        let props = this.props;

        return (
              <div id={`resume-work-experience-company-project-${props.CompanyIndex}-${props.Index}`} className="resume-work-experience-company-project">
                <div className="row project-header">
                    <div className="col-sm-1">
                        <span onClick={() => { this.setState(ResumeHelper.MergeElement(this.state, { isCollapsed: !this.state.isCollapsed })); }} className={`fa fa-caret-square-o-${this.state.isCollapsed ? "down" : "up"}`}></span>
                    </div>
                    <div className="col-sm-1">
                        <span>{Globals.resources.ExperienceProjectName}</span>
                    </div>
                    <div className="col-sm-3">
                        {
                            (<input type="text" ref={input => this.projectNameInput = input as HTMLInputElement} onChange={() => this.onInputChanged(props)}
                                value={props.Data.Name} className="limit-size" />)
                        }

                        {props.Data.HintedProjects && (
                            <div className="intv-autocomplete">
                                <ul className="intv-autocomplete-suggestions">
                                    {props.Data.HintedProjects.map((suggestion) =>
                                        <li
                                            onMouseDown={() => props.OnProjectAction({ Type: ResumeActionsTypes.PickProject, Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex, projectName: suggestion.Name, projectId: suggestion.Id, isProjectConfidential: suggestion.IsProjectConfidential } })}
                                            className="intv-autocomplete-suggestions-suggestion"
                                            key={suggestion.Id} value={suggestion.Id}>{suggestion.Name}</li>
                                    )}
                                </ul>
                            </div>)
                        }

                    </div>
                    <div className="col-sm-2 resume-work-experience-time-period">
                        <span>{Globals.resources.ExperienceTimePeriod}</span>
                    </div>
                    <div className="col-sm-2">
                        <DatePicker required={true} onChange={date => props.OnProjectAction({
                            Type: ResumeActionsTypes.ChangeProjectPeriodFrom,
                            Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex, from: date }
                        })} value={props.Data.From} />
                    </div>
                    <div className="col-sm-2">
                        <DatePicker onChange={date => props.OnProjectAction({
                            Type: ResumeActionsTypes.ChangeProjectPeriodTo,
                            Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex, to: date }
                        })} value={props.Data.To} />
                    </div>
                    <div className="col-sm-1 no-icon-margin">
                        <span className="fa fa-trash" onClick={() => props.OnProjectAction({
                            Type: ResumeActionsTypes.RemoveProject,
                            Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex }
                        })}></span>
                    </div>
                </div>

                {this.state.isCollapsed ?
                    null :
                    <div>
                        <div className="row">
                            <div className="col-sm-2">
                                {Globals.resources.ExperienceRoles}
                            </div>
                            <div className="col-sm-10">
                                <textarea required
                                    onChange={e => props.OnProjectAction({ Type: ResumeActionsTypes.ChangeProjectRoles, Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex, roles: e.target.value } })}
                                    value={props.Data.Roles} />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-2">
                                {Globals.resources.ExperienceDuties}
                            </div>
                            <div className="col-sm-10">
                                <textarea required
                                    onChange={e => props.OnProjectAction({ Type: ResumeActionsTypes.ChangeProjectDuties, Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex, duties: e.target.value } })}
                                    value={props.Data.Duties} />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-2">
                                {Globals.resources.ExperienceProjectDescription}
                            </div>
                            <div className="col-sm-10">
                                <textarea required readOnly={props.Data.IsProjectConfidential}
                                    onChange={e => props.OnProjectAction({ Type: ResumeActionsTypes.ChangeProjectDescription, Data: { company: props.Company, index: props.Index, companyIndex: props.CompanyIndex, projectDescription: e.target.value } })}
                                    value={props.Data.ProjectDescription} />
                            </div>
                        </div>

                        <div className="row pointer" onClick={() => props.OnProjectAction({ Type: ResumeActionsTypes.AddProjectSkill, Data: { companyIndex: props.CompanyIndex, projectIndex: props.Index } })}>
                            <div className="col-sm-2">
                                {Globals.resources.ExperienceSkillset}
                            </div>
                            <div className="col-sm-10">
                                {props.Data.TechnicalSkills.map((skill, index) =>
                                    <Pill
                                        key={skill.Id}
                                        Index={index}
                                        DefaultValue={skill.Id ? { Value: skill.Name, Id: skill.Id } : undefined}
                                        Placeholder={Globals.resources.ChooseTechnologyHint}
                                        Suggestions={ResumeApi.GetSkills().map(x => ({ Value: x.Name, Id: x.Id })).filter(x => props.Data.TechnicalSkills.filter(y => y.Id === x.Id).length === 0)}
                                        OnChange={(id, value) => props.OnProjectAction({ Type: ResumeActionsTypes.UpdateProjectSkill, Data: { project: props.Data, index: index, id: id, value: value } })}
                                        OnRemove={() => props.OnProjectAction({ Type: ResumeActionsTypes.RemoveProjectSkill, Data: { companyIndex: props.CompanyIndex, projectIndex: props.Index, skillIndex: index } })}
                                        Discard={() => props.OnProjectAction({ Type: ResumeActionsTypes.RemoveProjectSkill, Data: { companyIndex: props.CompanyIndex, projectIndex: props.Index, skillIndex: index } })}

                                    />)}
                            </div>
                        </div>

                        <div className="row pointer" onClick={() => props.OnProjectAction({ Type: ResumeActionsTypes.AddProjectIndustry, Data: { companyIndex: props.CompanyIndex, projectIndex: props.Index } })}>
                            <div className="col-sm-2">
                                {Globals.resources.ExperienceIndustry}
                            </div>
                            <div className="col-sm-10">
                                {props.Data.Industries.map((industry, index) =>
                                    <Pill
                                        key={industry.Id}
                                        Index={index}
                                        DefaultValue={industry.Id ? { Value: industry.Name, Id: industry.Id } : undefined}
                                        Placeholder={Globals.resources.ChooseBranchIndustryHint}
                                        SuggestionsLength={20}
                                        Suggestions={ResumeApi.GetIndustries().map(x => ({ Value: x.Name, Id: x.Id })).filter(x => props.Data.Industries.filter(y => y.Id === x.Id).length === 0)}
                                        OnChange={(id, value) => props.OnProjectAction({ Type: ResumeActionsTypes.UpdateProjectIndustry, Data: { project: props.Data, index: index, id: id, value: value } })}
                                        OnRemove={() => props.OnProjectAction({ Type: ResumeActionsTypes.RemoveProjectIndustry, Data: { companyIndex: props.CompanyIndex, projectIndex: props.Index, industryIndex: index } })}
                                        Discard={() => props.OnProjectAction({ Type: ResumeActionsTypes.RemoveProjectIndustry, Data: { companyIndex: props.CompanyIndex, projectIndex: props.Index, industryIndex: index } })}

                                    />)}
                            </div>
                        </div>
                    </div>}
            </div>
        );

    }
}
