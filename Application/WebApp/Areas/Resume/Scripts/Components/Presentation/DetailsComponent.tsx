﻿const DetailsComponent: React.StatelessComponent<ResumeDetailsProps> = (props: ResumeDetailsProps) => {
    return (
        <div className="resume-basic-data container">
            <div className="resume-basic-data-row row">
                <div className="resume-basic-data-row-name col-sm-2">{Globals.resources.EmployeeName}</div>
                <div className="resume-basic-data-row-content col-sm-10">
                    <input readOnly type="text" value={props.Data.EmlpoyeeName} />
                </div>
            </div>
            <div className="resume-basic-data-row row">
                <div className="resume-basic-data-row-name col-sm-2">{Globals.resources.EmployeePosition}</div>
                <div className="resume-basic-data-row-content col-sm-10">
                    <input readOnly type="text" value={props.Data.Position} />
                </div>
            </div>
            <div className="resume-basic-data-row row">
                <div className="resume-basic-data-row-name col-sm-2">{Globals.resources.EmployeeYearOfExperience}</div>
                <div className="resume-basic-data-row-content col-sm-10">
                    <input readOnly type="text" value={props.Data.YearsOfExperience} />
                </div>
            </div>
        </div>
    );
}

