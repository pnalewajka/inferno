﻿const AdditionalSkillComponent: React.StatelessComponent<ResumeAdditionalSkillProps> = (props: ResumeAdditionalSkillProps) => {
    if (props.Edit != null && props.Edit) {     
        return (
            <div className="intv-technical-skills-row-content-new-skill" >
                <input required autoFocus={true}
                    onChange={e => props.AdditionalSkillChange(e.target.value, props.Index)}
                    value={props.Data.Name}
                    className="intv-technical-skills-row-content-new-skill-input" type="text" placeholder={Globals.resources.ChooseAdditionalSkillHint} />
                {
                    props.Data.Error != null && props.Data.Error ?
                        <span className="intv-technical-skills-row-content-new-skill-error fa fa-exclamation-triangle"></span>
                        :
                        null
                }
                <span onClick={(event) => props.SaveChanges(event, props.Index, props.Data)} className="intv-technical-skills-row-content-new-skill-accept fa fa-check"></span>
                <span onClick={(event) => props.RemoveComponent(event, props.Index, props.Data)} className="intv-technical-skills-row-content-new-skill-close fa fa-times"></span>
            </div>
        );
    }
    else {
        return (
            <div className="intv-technical-skills-row-content-pill">
                <span className="intv-technical-skills-row-content-pill-name">{props.Data.Name}</span>
                <span onClick={(event) => props.RemoveComponent(event, props.Index, props.Data)} className="intv-technical-skills-row-content-pill-close fa fa-times"></span>
            </div>
        );
    }
}