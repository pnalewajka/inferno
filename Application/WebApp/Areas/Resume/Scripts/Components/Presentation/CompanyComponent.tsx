﻿class CompanyComponent extends React.Component<ResumeCompanyProps, { isCollapsed: boolean }> {
    constructor() {
        super();
        this.state = { isCollapsed: false };
    }

    render() {
        let tagVisible = false;
        let props = this.props;

        return (
                <div id={`resume-work-experience-company-${props.Index}`} className="resume-work-experience-company">
                <div className="resume-work-experience-company-basic-data row">
                    <div className="row">
                        <div className="col-sm-1">
                            <span onClick={() => { this.setState(ResumeHelper.MergeElement(this.state, { isCollapsed: !this.state.isCollapsed })); }} className={`fa fa-caret-square-o-${this.state.isCollapsed ? "down" : "up"}`}></span>
                            </div>
                        <div className="col-sm-1">
                            <span>{Globals.resources.ExperienceCompanyName}</span>
                        </div>
                        <div className="col-sm-3">
                            {
                                (<input required onBlur={e => props.OnCompanyAction({ Type: ResumeActionsTypes.HideCompanySuggestions, Data: { index: props.Index } })}
                                    onFocus={e => props.OnCompanyAction({ Type: ResumeActionsTypes.ShowCompanySuggestions, Data: { index: props.Index } })}
                                    type="text" value={props.Data.Name} onChange={e => props.OnCompanyAction({ Type: ResumeActionsTypes.ChangeCompanyName, Data: { companyName: e.target.value, index: props.Index } })}
                                    className="limit-size" />)
                            }

                            {props.Data.ShowSuggestions && <div className="intv-autocomplete">
                                <ul className="intv-autocomplete-suggestions">
                                    {
                                        ResumeApi.GetCompanies()
                                            .filter(c => c.Name.toLowerCase().trim().indexOf(props.Data.Name.toLowerCase().trim()) == 0)
                                            .map((suggestion) =>
                                                <li
                                                    onMouseDown={() => props.OnCompanyAction({ Type: ResumeActionsTypes.PickCompanyName, Data: { companyName: suggestion.Name, index: props.Index } })}
                                                    className="intv-autocomplete-suggestions-suggestion"
                                                    key={suggestion.Name} value={suggestion.Name}>{suggestion.Name}</li>
                                            )
                                    }
                                </ul>
                            </div>}
                        </div>
                        <div className="col-sm-2 resume-work-experience-time-period">
                            <span>{Globals.resources.ExperienceTimePeriod}</span>
                        </div>
                        <div className="col-sm-2">
                            <DatePicker required={true} onChange={date => props.OnCompanyAction({ Type: ResumeActionsTypes.ChangeCompanyPeriodFrom, Data: { index: props.Index, from: date } })} value={props.Data.From} />
                        </div>
                        <div className="col-sm-2">
                            <DatePicker onChange={date => props.OnCompanyAction({ Type: ResumeActionsTypes.ChangeCompanyPeriodTo, Data: { index: props.Index, to: date } })} value={props.Data.To} />
                        </div>
                        <div className="col-sm-1">
                            <span title={Globals.resources.AddProject}
                                onClick={() => props.OnProjectAction({ Type: ResumeActionsTypes.AddNewProject, Data: { index: props.Index, company: props.Data } })}
                                className="resume-work-experience-company-add btn fa fa-plus">
                            </span>
                            <span onClick={() => props.OnCompanyAction({ Type: ResumeActionsTypes.RemoveCompany, Data: { index: props.Index } })}
                                title={Globals.resources.RemoveCompany}
                                className="resume-work-experience-company-remove btn fa fa-trash"></span>
                        </div>
                    </div>
                </div>

                {this.state.isCollapsed ?
                    null :
                    <div>
                        { props.Data.ResumeProjects.map((project, index) => <ProjectComponent OnProjectAction={props.OnProjectAction} CompanyIndex={props.Index} Index={index} Company={props.Data} key={index} Data={project} />) }
                    </div>
                }
            </div>
        );
    }
}

