﻿class TechnicalSkillComponent extends React.Component<ResumeTechnicalSkillProps, { isAdding: boolean }> {
    public constructor() {
        super();
        this.state = { isAdding: false };
    }
    public render(): JSX.Element {
        const props = this.props;

        return (
            <div className="intv-technical-skills-row">
                <div className="intv-technical-skills-row-name">
                    {props.Name}
                </div>
                <div onClick={this.openAddForm.bind(this)} className="intv-technical-skills-row-content">
                    {
                        props.Data.map((pill, index) => <TechnicalSkillPillComponent key={index}
                            Data={pill}
                            ExistingSkillNames={props.ExistingSkillNames}
                            SaveChanges={props.SaveChanges}
                            RemoveComponent={props.RemoveComponent}
                            DiscardEditing={props.DiscardEditing}
                            Index={index} />)
                    }
                    {
                        this.state.isAdding ?
                            <TechnicalSkillPillComponent
                                Data={this.defaultNewSkill()}
                                ExistingSkillNames={props.ExistingSkillNames}
                                SaveChanges={(index: number, skill: MyResumeTechnicalSkill) => { this.setState({ isAdding: false }); props.SaveChanges(index, skill); }}
                                RemoveComponent={() => { this.setState({ isAdding: false }); }}
                                DiscardEditing={() => { this.setState({ isAdding: false }); }}
                                Index={-1} />
                            : null
                    }
                </div>
            </div>
        );
    }

    private defaultNewSkill(): MyResumeTechnicalSkill {
        const [skillTagId] = ResumeApi.GetSkillTags().filter(s => s.Name === this.props.SkillTag).map(s => s.Id);

        return {
            Id: 0,
            TechnicalSkillId: 0,
            Name: "",
            ExpertiseLevel: 0,
            ExpertisePeriod: 0,
            SkillTagName: this.props.SkillTag,
            SkillTagId: skillTagId,
            Timestamp: "",
            Index: -1
        };
    }

    private openAddForm(event: MouseEvent): void {
        event.preventDefault();

        this.setState({ isAdding: true });
    }
}
