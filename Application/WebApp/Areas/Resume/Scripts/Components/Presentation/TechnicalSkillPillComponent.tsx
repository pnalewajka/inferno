﻿const events = new Atomic.Events.EventDispatcher<HTMLElement>();

document.addEventListener("click", (e) => {
    events.dispatch(e.target as HTMLElement);
}, false);

class TechnicalSkillPillComponent extends React.Component<ResumeTechnicalSkillPillProps, { Data: MyResumeTechnicalSkill, edit: boolean }> {
    activeNode: HTMLDivElement | null = null;

    private handleExternalDiscard(element: HTMLElement) {
        if (this.activeNode !== null && !this.activeNode.contains(element) ) {
                this.discardLocalChanges();
            }
        }

    public componentWillMount() {
        this.setState({ Data: { ...this.props.Data }, edit: this.props.Index === -1 });
        events.subscribe(this.handleExternalDiscard.bind(this));
    }

    public componentWillUnmount(): void {
        events.unsubscribe(this.handleExternalDiscard.bind(this))
    }

    private updateStateDataName(newName: string): void {
        this.setState({ Data: { ...this.state.Data, ...{ Name: newName } } });
    }

    private setEditMode(edit: boolean): void {
        this.setState({ ...this.state, ...{ edit } });
    }

    private getSkillLevelDisplayName(level: number): string {
        const [skillLevel] = ResumeApi.GetSkillExpertiseLevels().filter(l => l.Value == level);
        return !skillLevel ? "" : skillLevel.Name;
    }

    private getExpertiseDisplayName(level: number): string {
        const [periodLevel] = ResumeApi.GetSkillExpertisePeriods().filter(l => l.Value == level);

        return !periodLevel ? "" : periodLevel.Name;
    }

    private saveLocalChanges(): void {
        const [skill] = ResumeApi.GetSkills().filter(s => s.Name === this.state.Data.Name);
        if (!skill) {
            AlertMessages.addError("Field is empty")
            return;
        }

        this.props.SaveChanges(this.props.Index, { ...this.state.Data, ...{ TechnicalSkillId: skill.Id, Id: 0, Timestamp: null } });

        this.setEditMode(false);
    }

    private discardLocalChanges(): void {

        this.setState({
            Data: { ...this.props.Data },
            edit: false
        });

        this.props.DiscardEditing(this.state.Data.Index, this.state.Data);
    }

    public render(): JSX.Element {

        const props = this.props;

        if (this.state.edit) {
            return (
                <div
                    ref={node => this.activeNode = node}
                    onClick={(e) => { e.stopPropagation(); }}
                    className={Dante.Utils.ClassNames({
                        'field-has-error': props.Data.Error ? true : false,
                        'field-is-editing': props.Data.Edit ? true : false
                    }, 'intv-technical-skills-row-content-new-skill')} >

                    <input required autoFocus={true}
                        onChange={(event) => this.updateStateDataName(event.target.value)}

                        value={this.state.Data.Name}
                        className="intv-technical-skills-row-content-new-skill-input" type="text" placeholder={Globals.resources.ChooseTechnologyHint} />

                    {
                        props.Data.Error != null && props.Data.Error ?
                            <span className="intv-technical-skills-row-content-new-skill-error fa fa-exclamation-triangle"></span>
                            :
                            null
                    }
                    <ExpertiseControl value={this.state.Data.ExpertiseLevel} optionValues={ResumeApi.GetSkillExpertiseLevels()}
                        index={this.state.Data.Index} data={this.state.Data} changeFunction={(ExpertiseLevel: number) => { this.setState({ Data: { ...this.state.Data, ...{ ExpertiseLevel } } }) }} />

                    <ExpertiseControl value={this.state.Data.ExpertisePeriod} optionValues={ResumeApi.GetSkillExpertisePeriods()}
                        index={this.state.Data.Index} data={this.state.Data} changeFunction={(ExpertisePeriod: number) => { this.setState({ Data: { ...this.state.Data, ...{ ExpertisePeriod } } }) }} />

                    <span onClick={(event) => { this.saveLocalChanges() }} className="intv-technical-skills-row-content-new-skill-accept fa fa-check"></span>
                    <span onClick={(event) => { this.discardLocalChanges(); }} className="intv-technical-skills-row-content-new-skill-close fa fa-times"></span>

                    <TechnicalSkillAutoComplete Data={this.state.Data} ExistingSkillNames={props.ExistingSkillNames} AutoCompleteClicked={this.updateStateDataName.bind(this)} Index={props.Index} />
                </div>
            );
        }
        else {
            return (
                <div className="intv-technical-skills-row-content-pill" onClick={(e) => { e.stopPropagation(); this.setEditMode(true); }}>
                    <span className="intv-technical-skills-row-content-pill-name">{props.Data.Name}</span>
                    <span className="intv-technical-skills-row-content-pill-seniority-level">{this.getSkillLevelDisplayName(this.props.Data.ExpertiseLevel)}</span>
                    <span className="intv-technical-skills-row-content-pill-seniority-level">{this.getExpertiseDisplayName(this.props.Data.ExpertisePeriod)}</span>
                    <span onClick={(event) => props.RemoveComponent(event, props.Index, props.Data)} className="intv-technical-skills-row-content-pill-close fa fa-times"></span>
                </div>
            );
        }
    }
}

const ExpertiseControl: React.StatelessComponent<{ value: number, optionValues: EnumValues[], index: number, data: MyResumeTechnicalSkill, changeFunction: Function }> =
    (props: { value: number, optionValues: EnumValues[], index: number, data: MyResumeTechnicalSkill, changeFunction: Function }) => {

        if (props.value == null) {
            return null;
        }

        return (<select required
            value={props.value}
            onChange={(event) => props.changeFunction(event.target.value, event.target.options[event.target.selectedIndex].textContent, props.index, props.data)}
            className="intv-technical-skills-row-content-new-skill-select">
            {
                props.optionValues.map(level =>
                    <option key={level.Value} value={level.Value > 0 ? level.Value : ""}>{level.Name}</option>
                )
            }
        </select>);
    }