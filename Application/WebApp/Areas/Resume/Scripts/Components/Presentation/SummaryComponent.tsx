﻿const SummaryComponent: React.StatelessComponent<ResumeDetailsProps> = (props: ResumeDetailsProps) => {

    if (!props.Data.Summary) {
        props.Data.Summary = "";
    }

    return (
        <div className="resume-summary container">
            <div className="resume-summary-row row">
                <div className="resume-summary-row-name col-sm-12" dangerouslySetInnerHTML={{ __html: Globals.resources.ResumeExplanation}}>
                </div>
                <div className="resume-summary-row-content col-sm-12">
                    <textarea value={props.Data.Summary} onChange={(event) => props.DetailsChange(event.target.value)} maxLength={4000}></textarea>
                </div>
            </div>
        </div>
    );
}
