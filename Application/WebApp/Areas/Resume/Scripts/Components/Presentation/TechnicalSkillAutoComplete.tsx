﻿const TechnicalSkillAutoComplete: React.StatelessComponent<ResumeTechnicalSkillAutoComplete> = (props: ResumeTechnicalSkillAutoComplete) => {
    const suggestions = ResumeApi.GetSkills();

    const filterSuggestions = (): any[] => {
        let tagSuggestions = suggestions
            .filter((suggestion) => suggestion.SkillTagIds.indexOf(props.Data.SkillTagId) >= 0)

        return tagSuggestions
            .filter((suggestion) => suggestion.Name.toLowerCase().indexOf(props.Data.Name.toLowerCase()) === 0)
            .filter((suggestion) => suggestion.Name.toLowerCase() !== props.Data.Name.toLowerCase())
            .filter((suggestion) => props.ExistingSkillNames.indexOf(suggestion.Name.toLowerCase()) < 0);
    };
    const displaySuggestions = filterSuggestions();

    return (
        (props.Data.Name == null || props.Data.Name == "" || displaySuggestions.length === 0) ?
            null :
            <div className="intv-autocomplete">
                <ul className="intv-autocomplete-suggestions">
                    {
                        displaySuggestions
                            .map((suggestion) =>
                                <li
                                    onClick={() => props.AutoCompleteClicked(suggestion.Name, suggestion.Id, props.Index, props.Data)}
                                    className="intv-autocomplete-suggestions-suggestion"
                                    key={suggestion.Name} value={suggestion.Value}>{suggestion.Name}</li>
                            )
                    }
                </ul>
            </div>
    );
}