﻿const EducationComponent: React.StatelessComponent<ResumeEducationProps> = (props: ResumeEducationProps) => {
    return (
        <div className="row">
            <div className="col-sm-2">
                <DatePicker required={true} yearView={true} onChange={v => props.OnEducationAction({ Type: ResumeActionsTypes.ChangeEducationFrom, Index: props.Index, Data: v })} value={props.Data.From} />
            </div>
            <div className="col-sm-2">
                <DatePicker yearView={true} onChange={v => props.OnEducationAction({ Type: ResumeActionsTypes.ChangeEducationTo, Index: props.Index, Data: v })} value={props.Data.To} />
            </div>
            <div className="col-sm-3">
                <input required type="text" value={props.Data.UniversityName} onChange={e => props.OnEducationAction({ Type: ResumeActionsTypes.ChangeEducationUniversityName, Data: e.target.value, Index: props.Index })} />
            </div>
            <div className="col-sm-2">
                <input required type="text" value={props.Data.Specialization} onChange={e => props.OnEducationAction({ Type: ResumeActionsTypes.ChangeEducationSpecialization, Data: e.target.value, Index: props.Index })} />
            </div>
            <div className="col-sm-2">
                <select
                    onChange={e => props.OnEducationAction({ Type: ResumeActionsTypes.ChangeEducationDegree, Data: e.target.value, Index: props.Index })}
                    value={props.Data.Degree}>
                    {
                        ResumeApi.GetDegrees().map((degree, index) =>
                            <option key={index} value={degree}>{degree}</option>
                        )
                    }
                </select>
            </div>
            <div className="col-sm-1">
                <span onClick={event => props.OnEducationAction({ Type: ResumeActionsTypes.RemoveEducation, Index: props.Index })} className="fa fa-trash"></span>
            </div>
        </div>
    );
}

