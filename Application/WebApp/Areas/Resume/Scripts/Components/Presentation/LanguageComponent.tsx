﻿const LanguageComponent: React.StatelessComponent<ResumeLanguageProps> = (props: ResumeLanguageProps) => {
    return (
        <div className="row">
            <div className="col-sm-6">
                <select required
                    onChange={e => props.OnLanguageAction({ Type: ResumeActionsTypes.ChangeLanguageName, Data: { Id: Number(e.target.value), Name: e.target.options[e.target.selectedIndex].textContent! }, Index: props.Index })}
                    value={props.Data.LanguageId}>
                    {
                        ResumeApi.GetLanguages().map((language, index) =>
                            <option key={index} value={language.Id > 0 ? language.Id : ""}>{language.Name}</option>
                        )
                    }
                </select>
            </div>
            <div className="col-sm-5">
                <select required
                    onChange={e => props.OnLanguageAction({ Type: ResumeActionsTypes.ChangeLanguageLevel, Data: { Id: Number(e.target.value), Name: e.target.options[e.target.selectedIndex].textContent! }, Index: props.Index })}
                    value={props.Data.Level}>
                    {
                        ResumeApi.GetLanguageLevels().map((level, index) =>
                            <option key={index} value={level.Value > 0 ? level.Value : ""}>{level.Name}</option>
                        )
                    }
                </select>
            </div>
            <div className="col-sm-1">
                <span onClick={event => props.OnLanguageAction({ Type: ResumeActionsTypes.RemoveLanguage, Index: props.Index })} className="fa fa-trash"></span>
            </div>
        </div>
    );
}