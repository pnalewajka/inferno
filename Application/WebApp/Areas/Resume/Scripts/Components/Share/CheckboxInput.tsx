﻿interface ICheckboxInputProps {
    checked: boolean;
    onCheckChanged: (isChecked: boolean) => void;
    label: string;
    multiLineLabel: string[];
}

interface ICheckboxInputState {
    checked: boolean;
}

class CheckboxInput extends React.Component<ICheckboxInputProps, ICheckboxInputState> {
    constructor(props: ICheckboxInputProps) {
        super(props);
        this.state = {
            checked: props.checked
        };
    }

    handleChanged = () => {
        this.setState({ checked: !this.state.checked }, () => {
            this.props.onCheckChanged(this.state.checked);    
        });
    };

    componentWillReceiveProps() {
        this.setState({ checked: this.props.checked });
    }

    render() {
        const { checked } = this.state;
        if (this.props.multiLineLabel && this.props.multiLineLabel.length) {
            return (
                <label>
                    <input type="checkbox" checked={checked} onChange={this.handleChanged} />
                    {this.props.multiLineLabel.map((line) => <span key={line}>{line}<br /></span>)}
                </label>
            );
        } else {
            return (
                <label>
                    <input type="checkbox" checked={checked} onChange={this.handleChanged} />
                    {this.props.label}
                </label>
            );
        }
    }
}