﻿interface PillProps {
    DefaultValue?: { Value: string, Id: number };
    Index: number;
    Placeholder: string;
    SuggestionsLength?: number;
    Suggestions: { Value: string, Id: number }[];
    OnChange: (id: number, value: string) => void;
    OnRemove: () => void;
    Discard: () => void;
}

interface PillState {
    Value: string;
    Id: number;
    Filter: string;
    IsError: boolean;
    IsEdit: boolean;
    ShowSuggestions: boolean;
}

class Pill extends React.Component<PillProps, PillState> {

    constructor(props: PillProps, state: PillState) {
        super(props, state);
        if (props.DefaultValue) {
            this.state = {
                Value: props.DefaultValue.Value,
                Id: props.DefaultValue.Id,
                Filter: props.DefaultValue.Value,
                IsError: false,
                IsEdit: false,
                ShowSuggestions: false
            };
        } else {
            this.state = {
                Value: '',
                Id: 0,
                Filter: '',
                IsError: true,
                IsEdit: true,
                ShowSuggestions: false
            };
        }
    }

    nameInput: HTMLInputElement | null;
    componentDidMount() {
        if (this.nameInput != null) {
            this.nameInput.focus();
        }
    }

    onFilterChange = (event: React.ChangeEvent<HTMLSpanElement>, query: string) => {

        let suggestions = this.props.Suggestions.filter(x => x.Value === query);

        if (suggestions.length > 0) {
            this.onChooseSuggestion(event, suggestions[0]);
            return;
        }

        this.setState(current => {
            current.Filter = query;
            current.ShowSuggestions = true;
            current.IsError = true;

            return $.extend(true, {}, current);
        });
    }

    onChooseSuggestion = (event: React.SyntheticEvent<HTMLSpanElement>, suggestion: { Value: string, Id: number }) => {
        event.stopPropagation();

        this.setState(current => {
            current.Filter = current.Value = suggestion.Value;
            current.Id = suggestion.Id;
            current.ShowSuggestions = false;
            current.IsError = false;

            return $.extend(true, {}, current);
        });
    }

    setEditMode = (event: React.MouseEvent<HTMLSpanElement>) => {
        event.stopPropagation();

        this.setState(current => {
            current.IsEdit = true;
            return $.extend(true, {}, current);
        });
    }

    save = (event: React.MouseEvent<HTMLSpanElement>) => {
        event.stopPropagation();

        if (this.state.IsError) {
            return;
        }

        this.props.OnChange(this.state.Id, this.state.Value);

        this.setState(current => {
            current.IsEdit = false;
            return $.extend(true, {}, current);
        });
    }

    remove = (event: React.MouseEvent<HTMLSpanElement>) => {
        event.stopPropagation();

        this.props.OnRemove();
    }

    discard = (event: React.MouseEvent<HTMLSpanElement>) => {
        event.stopPropagation();
        this.props.Discard();
    }

    render() {
        if (this.state.IsEdit) {
            return (
                <div className="intv-technical-skills-row-content-new-skill" onClick={e => e.stopPropagation()} >
                    <input
                        onChange={(event) => this.onFilterChange(event, event.target.value)}
                        value={this.state.Filter}
                        placeholder={this.props.Placeholder}
                        className="intv-technical-skills-row-content-new-skill-input" type="text"
                        ref={(input) => { this.nameInput = input }} />
                    {this.state.IsError && <span className="intv-technical-skills-row-content-new-skill-error fa fa-exclamation-triangle"></span>}

                    <span onClick={e => { this.save(e); }} className="intv-technical-skills-row-content-new-skill-accept fa fa-check"></span>
                    <span onClick={e => { this.discard(e); }} className="intv-technical-skills-row-content-new-skill-close fa fa-times"></span>
                    {
                        this.state.ShowSuggestions && <div className="intv-autocomplete">
                            <ul className="intv-autocomplete-suggestions">
                                {
                                    this.props.Suggestions
                                        .filter(x => this.state.Filter ? x.Value.toLowerCase().indexOf(this.state.Filter.toLowerCase()) > -1 : true)
                                        .slice(0, this.props.SuggestionsLength ? this.props.SuggestionsLength : 5)
                                        .map((suggestion) =>
                                            <li
                                                onClick={(e) => this.onChooseSuggestion(e, suggestion)}
                                                className="intv-autocomplete-suggestions-suggestion"
                                                key={suggestion.Id} value={suggestion.Value}>{suggestion.Value}</li>
                                        )
                                }
                            </ul>
                        </div>
                    }
                </div>
            );
        }

        return (
            <div onClick={e => { this.setEditMode(e); }} className="intv-technical-skills-row-content-pill">
                <span className="intv-technical-skills-row-content-pill-name">{this.props.DefaultValue!.Value}</span>
                <span onClick={e => { this.remove(e); }} className="intv-technical-skills-row-content-pill-close fa fa-times"></span>
            </div>
        );
    }
}