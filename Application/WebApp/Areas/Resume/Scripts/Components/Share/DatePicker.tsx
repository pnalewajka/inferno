﻿interface DatePickerProps {
    onChange: (date: string) => void;
    value?: string;
    yearView?: boolean;
    required?: boolean;
}

class DatePicker extends React.Component<DatePickerProps, {}> {

    private _input: HTMLElement;
    private _dateFormat: string;
    private _locale: string;
    private _yearView?: boolean;

    constructor(props: DatePickerProps) {
        super(props);
        this._dateFormat = props.yearView ? "yyyy" : "mm yyyy";
        this._locale = Globals.getDatePickerLocale();
        this._yearView = props.yearView;
    }

    componentDidMount(): void {
        $(this._input).datepicker({
            language: this._locale,
            autoclose: true,
            format: this._dateFormat,
            changeMonth: this._yearView ? false : true,
            viewMode: this._yearView ? "years" : "months",
            minViewMode: this._yearView ? "years" : "months"
        }).on('hide', (e: any) => {
            e.date.setMinutes(e.date.getMinutes() - e.date.getTimezoneOffset());
            this.props.onChange(e.date.toISOString().slice(0, 10));
        });
    }

    getValue = (): string => {
        if (this.props.value) {

            if (this.props.value.length > 8) {

                var dt = new Date(this.props.value);

                dt.setMinutes(dt.getMinutes() - dt.getTimezoneOffset());

                if (this._yearView) {
                    return dt.getFullYear().toString();
                }

                return (dt.getMonth() < 9 ? "0" : "") + (dt.getMonth() + 1) + " " + dt.getFullYear();
            }

            return this.props.value;
        }

        return "";
    }

    render() {
        return (
            <input required={this.props.required} ref={(input) => { this._input = input! }} onChange={e => this.props.onChange(e.target.value)} value={this.getValue()} type="text" placeholder={this._dateFormat} />
            );
    }
}