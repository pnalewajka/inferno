﻿class ResumeComponent extends React.Component<ResumeProps, ResumeState> {

    private store: ResumeState;

    constructor(props: ResumeProps) {
        super(props);
        this.state = {
            Content: props.Data,
            ModalError: ""
        };
        this.store = this.state;
    };

    componentDidUpdate(prevProps: Readonly<ResumeProps>, prevState: Readonly<ResumeState>, prevContext: any): void {
        this.setCompletnessState();
    }

    componentDidMount() {
        if ($("#resume-root").data("current-culture") !== "en-US") {
            $("#resume-info-english-only").show();
            $("#resume-info-english-only-text").text(Globals.resources.WarningTextInEnglishOnly);
        } else {
            $("#resume-info-english-only").hide();
        }

        $("#success-alert").hide();
        this.setCompletnessState();
    };

    handleAvailabilityChange = (value: string) => {
        if (value.length === 0 || ResumeHelper.VerifyIsNumeric(value)) {
            this.setState(prev => { prev.Content.Resume.AvailabilityInMonths = Number(value); return prev; });
            this.store.Content.Resume.AvailabilityInMonths = this.state.Content.Resume.AvailabilityInMonths;
        }
    };

    handleSummaryChange = (value: string) => {
        this.setState(prev => { prev.Content.Resume.Summary = value; return prev; });
        this.store.Content.Resume.Summary = this.state.Content.Resume.Summary;
    };

    handleTechnicalSkillsChanged = (skills: MyResumeTechnicalSkill[]) => {
        this.store.Content.ResumeTechnicalSkills = skills;
        this.setCompletnessState();
    };

    handleAdditionalSkillsChanged = (additionalSkills: MyResumeAdditionalSkill[]) => {
        this.store.Content.ResumeAdditionalSkills = additionalSkills;
        this.setCompletnessState();
    };

    handleLanguagesChanged = (languages: MyResumeLanguage[]) => {
        this.store.Content.ResumeLanguages = languages;
        this.setCompletnessState();
    };

    handleEducationChanged = (educations: MyResumeEducation[]) => {
        this.store.Content.ResumeEducations = educations;
        this.setCompletnessState();
    };

    handleTrainingChanged = (trainings: MyResumeTraining[]) => {
        this.store.Content.ResumeTrainings = trainings;
        this.setCompletnessState();
    };

    handleCompanyTableChanged = (companyTable: ResumeCompanyTableState) => {
        this.store.Content.ResumeCompanies = companyTable.Data;
        this.setCompletnessState();
    };

    handleConsentClauseForDataProcessingChanged = (value: boolean) => {
        this.store.Content.Resume.IsConsentClauseForDataProcessingChecked = value;
        this.setCompletnessState();
    };

    saveMyResume: () => Promise<MyResume> = () => {
        const $saveButtons = $(".form-footer input.btn").attr("disabled", "disabled");

        return ResumeApi.SaveResume<MyResume>(this.store.Content)
            .then((data) => {
                return new Promise<MyResume>(resolve => {
                    this.setState({ ...this.state, ...{ Content: data } });
                    ResumeHelper.Alert(Globals.resources.Saved);

                    this.store = this.state;
                    $saveButtons.removeAttr("disabled");

                    resolve(data);
                });
            }).catch(() => {
                ResumeHelper.Alert(Globals.resources.SaveError, AlertMessages.AlertType.Error);
                $saveButtons.removeAttr("disabled");

                return Promise.reject(Globals.resources.SaveError);
            });
    }

    handleSubmit = () => {
        if (!this.errorExists()) {
            this.saveMyResume();
        }
    };

    handleSubmitAndGenerate = () => {
        if (!this.errorExists()) {
            this.saveMyResume().then(() => {
                if (this.store.Content.Resume.IsConsentClauseForDataProcessingChecked) {
                    ResumeApi.GenerateMyResumes([this.store.Content.Resume.EmployeeId]);
                } else {
                    this.displayConsentClauseForDataProcessingNotCheckedWarning();
                }
            });
        }
    };

    setCompletnessState() {
        var percentege = ResumeApi.GetCompletnessPercentage(this.state);

        if (percentege != this.state.Content.FillPercentage) {
            var content = $.extend(this.state, {});

            content.Content.FillPercentage = percentege;

            this.setState(prev => { return content; });

            this.store.Content.FillPercentage = percentege;
        }
    }

    errorExists = (): boolean => {
        let error = "";
        let scroolIntoDiv = "";

        let allProjects = this.store.Content.ResumeCompanies.length == 0 ? [] : this.store.Content.ResumeCompanies.map(c => {
            return c.ResumeProjects;
        }).reduce((a, b) => {
            return a.concat(b);
        });

        this.store.Content.ResumeCompanies.forEach(c => c.Name = c.Name.trim());

        allProjects.forEach(p => {
            p.Name = p.Name !== null ? p.Name.trim() : "";
            p.Roles = p.Roles !== null ? p.Roles.trim() : "";
            p.Duties = p.Duties !== null ? p.Duties.trim() : "";
            p.ProjectDescription = p.ProjectDescription !== null ? p.ProjectDescription.trim() : "";
        });

        this.store.Content.ResumeEducations.forEach(e => {
            e.Degree = e.Degree.trim();
            e.Specialization = e.Specialization.trim();
            e.UniversityName = e.UniversityName.trim();
        });

        this.store.Content.ResumeTrainings.forEach(t => {
            t.Institution = t.Institution.trim();
            t.Title = t.Title.trim();
        });

        let companiesRequiredFields = this.store.Content.ResumeCompanies.filter(l => !l.Name || !l.From);
        let companiesIncorectDates = this.store.Content.ResumeCompanies.filter(l => !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.From) || (l.To && !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.To)));
        let companiesOutOfRangeDates = this.store.Content.ResumeCompanies.filter(l => !ResumeHelper.VerifyDateIsInRange(l.From) || (l.To && !ResumeHelper.VerifyDateIsInRange(l.To)));
        let projectsRequiredFields = allProjects.filter(l => !l.Roles || !l.ProjectDescription || !l.Duties || !l.From);
        let projectsIncorectDates = allProjects.filter(l => !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.From) || (l.To && !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.To)));
        let projectsOutOfRangeDates = allProjects.filter(l => !ResumeHelper.VerifyDateIsInRange(l.From) || (l.To && !ResumeHelper.VerifyDateIsInRange(l.To)));
        let projectSkillsInEditMode = allProjects.filter(l => l.Industries.filter(i => i.Id == 0).length > 0 || l.TechnicalSkills.filter(s => s.Id == 0).length > 0);

        if (this.store.Content.ResumeTechnicalSkills.filter(f => f.Edit).length > 0) {
            error = Globals.resources.AlertTechnicalSkillsInEditMode;
            scroolIntoDiv = "resume-technical-skills";
        } else if (this.store.Content.ResumeAdditionalSkills.filter(f => f.Edit).length > 0) {
            error = Globals.resources.AlertAdditionalSkillsInEditMode;
            scroolIntoDiv = "resume-additional-skills";
        } else if (this.store.Content.ResumeLanguages.filter(l => l.LanguageId == 0 || l.Level == 0).length > 0) {
            error = Globals.resources.AlertLanguagesInEditMode;
            scroolIntoDiv = "resume-languages";
        } else if (this.store.Content.ResumeEducations.filter(l => !l.Degree || !l.Specialization || !l.UniversityName || !l.From).length > 0) {
            error = Globals.resources.AlertEducationRequiredFields;
            scroolIntoDiv = "resume-education";
        } else if (this.store.Content.ResumeEducations.filter(l => !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.From) || (l.To && !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.To))).length > 0) {
            error = Globals.resources.AlertEducationIncorectDate;
            scroolIntoDiv = "resume-education";
        } else if (this.store.Content.ResumeEducations.filter(l => !ResumeHelper.VerifyDateIsInRange(l.From) || (l.To && !ResumeHelper.VerifyDateIsInRange(l.To))).length > 0) {
            error = Globals.resources.AlertEducationIncorrectDateRange;
            scroolIntoDiv = "resume-education";
        } else if (companiesOutOfRangeDates.length > 0) {
            error = Globals.resources.AlertCompanyIncorrectDateRange;
            scroolIntoDiv = "resume-work-experience-company-" + this.store.Content.ResumeCompanies.indexOf(companiesOutOfRangeDates[0]);
        } else if (this.store.Content.ResumeTrainings.filter(l => !l.Institution || !l.Title || !l.Date).length > 0) {
            error = Globals.resources.AlertTrainingRequiredFields;
            scroolIntoDiv = "resume-training";
        } else if (this.store.Content.ResumeTrainings.filter(l => !ResumeHelper.VerifyIsMoreThan20CenturyDate(l.Date)).length > 0) {
            error = Globals.resources.AlertTrainingIncorectDate;
            scroolIntoDiv = "resume-training";
        } else if (this.store.Content.ResumeTrainings.filter(l => !ResumeHelper.VerifyDateIsInRange(l.Date)).length > 0) {
            error = Globals.resources.AlertTrainingIncorrectDateRange;
            scroolIntoDiv = "resume-training";
        } else if (companiesRequiredFields.length > 0) {
            error = Globals.resources.AlertCompanyRequiredFields;
            scroolIntoDiv = "resume-work-experience-company-" + this.store.Content.ResumeCompanies.indexOf(companiesRequiredFields[0]);
        } else if (companiesIncorectDates.length > 0) {
            error = Globals.resources.AlertCompanyIncorectDate;
            scroolIntoDiv = "resume-work-experience-company-" + this.store.Content.ResumeCompanies.indexOf(companiesIncorectDates[0]);
        } else if (projectsRequiredFields.length > 0) {
            error = Globals.resources.AlertProjectRequiredFields;
            scroolIntoDiv = this.getProjectHtmlElementId(projectsRequiredFields[0]);
        } else if (projectsIncorectDates.length > 0) {
            error = Globals.resources.AlertProjectIncorectDate;
            scroolIntoDiv = this.getProjectHtmlElementId(projectsIncorectDates[0]);
        } else if (projectsOutOfRangeDates.length > 0) {
            error = Globals.resources.AlertProjectIncorrectDateRange;
            scroolIntoDiv = this.getProjectHtmlElementId(projectsOutOfRangeDates[0]);
        } else if (projectSkillsInEditMode.length > 0) {
            error = Globals.resources.AlertFinishEditingProjectSkillsAndIndustries;
            scroolIntoDiv = this.getProjectHtmlElementId(projectSkillsInEditMode[0]);
        }

        if (error != "") {

            let $editing = document.getElementsByClassName('field-is-editing');
            let $el = $editing && $editing[0] ? $editing[0] : null;

            if (!$el) {
                $el = document.getElementsByClassName(scroolIntoDiv)[0];
            }

            if ($el !== null) {
                ($el as HTMLElement).scrollIntoView();
            }

            this.setState(prev => {
                return {
                    ModalError: error
                }
            });
        }

        return error != "";
    };

    displayConsentClauseForDataProcessingNotCheckedWarning = (): void => {
        const clauseElement = document.getElementById("resume-data-processing-clause");

        if (clauseElement !== null) {
            (clauseElement as HTMLElement).scrollIntoView();
        }

        this.setState(prev => {
            return {
                ModalError: Globals.resources.AlertClauseForDataProcessingNotChecked
            }
        });
    };

    scrollToBottom = (): void => {
        setTimeout(`var objDiv = document.getElementById("resume-content");
                    objDiv.scrollTop = objDiv.scrollHeight;`, 100);
    }

    getProjectHtmlElementId = (project: MyResumeProjects): string => {
        var company = this.store.Content.ResumeCompanies.filter(c => c.ResumeProjects.indexOf(project) >= 0)[0];
        var companyIndex = this.store.Content.ResumeCompanies.indexOf(company);
        var projectIndex = company.ResumeProjects.indexOf(project);

        return `resume-work-experience-company-project-${companyIndex}-${projectIndex}`;
    }

    closeModalError = (): void => {
        this.setState(prev => {
            return {
                ModalError: ""
            }
        });
    };

    submitSuggestions = (skills: SuggestedSkill[]): void => {
        var confirmedSkills = skills.filter(s => s.Confirmed);
        var declinedSkills = skills.filter(s => !s.Confirmed);

        confirmedSkills.forEach(skill => {
            this.store.Content.ResumeTechnicalSkills.push(this.mapSuggestionToTechnicalSkill(skill));
        });
        this.store.Content.ResumeRejectedSkillSuggestions = declinedSkills.map(s => s.SkillId);

        this.saveMyResume()
            .then(() => {
                if (confirmedSkills.length > 0) {
                    ResumeHelper.Alert(Globals.resources.AddedSuggestedSkills);
                }
            });
    }

    mapSuggestionToTechnicalSkill = (skill: SuggestedSkill): MyResumeTechnicalSkill => {
        return {
            Id: 0,
            TechnicalSkillId: skill.SkillId,
            Name: skill.Name,
            ExpertiseLevel: skill.ExpertiseLevel,
            ExpertisePeriod: skill.ExpertisePeriod,
            ExpertiseLevelName: skill.ExpertiseLevelName,
            ExpertisePeriodName: skill.ExpertisePeriodName,
            SkillTagName: skill.SkillTagName,
            SkillTagId: skill.SkillTagId,
            Timestamp: "",
            Edit: false,
            Index: 0
        }
    }

    getImagesPath = (imageFile: string): string => {
        return `/Areas/Resume/Content/images/${imageFile}.png`;
    };

    render() {
        return (
            <div>
                <div id="resume-header" className="resume-margin block">

                    <div className="resume-alert-cell col-sm-3">
                        <h3>{this.state.Content.Title}</h3>
                    </div>

                    <div className="resume-alert-cell col-sm-6">

                        <div id="resume-info-completness" className={this.state.Content.FillPercentage == 100 ? "alert alert-success resume-alert-cell" : "alert alert-info resume-alert-cell"} >
                            {Globals.resources.Completeness}: <b>{this.state.Content.FillPercentage}%</b>
                        </div>
                        <div id="resume-info-english-only" className="alert alert-danger resume-alert-cell">
                            <b id="resume-info-english-only-text"></b>
                        </div>

                    </div>

                    <div className="alert col-sm-3" id="alert-template">
                        <strong></strong>
                    </div>
                    <br />
                </div>

                <div id="resume-content" className="resume-content">
                    <div className="resume-margin">
                        <div id="resume-basic-data">
                            <DetailsComponent Data={this.state.Content.Resume} DetailsChange={this.handleAvailabilityChange} />
                        </div>
                        <div id="resume-summary">
                            <SummaryComponent Data={this.state.Content.Resume} DetailsChange={this.handleSummaryChange} />
                        </div>

                        <ImageHeaderComponent imageUrl={this.getImagesPath("skills")} title={Globals.resources.SkillsHeader} />
                        <TechnicalSkillTableComponent Data={this.state.Content.ResumeTechnicalSkills} StateChanged={this.handleTechnicalSkillsChanged} />
                        <AdditionalSkillTableComponent Data={this.state.Content.ResumeAdditionalSkills} StateChanged={this.handleAdditionalSkillsChanged} />

                        <ImageHeaderComponent imageUrl={this.getImagesPath("experience")} title={Globals.resources.ExperienceHeader} width="80px" />
                        <CompanyTableComponent Data={this.state.Content.ResumeCompanies} StateChanged={this.handleCompanyTableChanged} />

                        <ImageHeaderComponent imageUrl={this.getImagesPath("languages")} title={Globals.resources.LanguagesHeader} width="100px" />
                        <LanguageTableComponent Data={this.state.Content.ResumeLanguages} StateChanged={this.handleLanguagesChanged} />

                        <ImageHeaderComponent imageUrl={this.getImagesPath("education")} title={Globals.resources.EducationHeader} />
                        <EducationTableComponent Data={this.state.Content.ResumeEducations} StateChanged={this.handleEducationChanged} />

                        <ImageHeaderComponent imageUrl={this.getImagesPath("courses")} title={Globals.resources.CoursesHeader} width="80px" />
                        <TrainingTableComponent Data={this.state.Content.ResumeTrainings} StateChanged={this.handleTrainingChanged} OnRecordAdded={this.scrollToBottom} />

                        <ConsentClauseForDataProcessingComponent Show={this.props.Data.IsMine} Data={this.state.Content.Resume} StateChanged={this.handleConsentClauseForDataProcessingChanged} />
                    </div>
                </div>
                <div className="resume-margin">
                    <div className="form-footer">
                        <br />
                        <div className="footer-sizer">

                            <div className="pull-right">
                                <input type="submit" className="btn btn-primary btn-primary-submit" value={Globals.resources.Save} form="main-form"
                                    onClick={(event) => this.handleSubmit()} data-hotkey="ctrl+return" />

                                <input type="submit" className="btn btn-primary btn-primary-submit" value={Globals.resources.SaveAndGenerate} form="main-form"
                                    onClick={(event) => this.handleSubmitAndGenerate()} data-hotkey="ctrl+return" />

                            </div>
                        </div>
                    </div>
                </div>

                <SuggestionsDialogComponent submit={this.submitSuggestions} />

                <div className={this.state.ModalError ? "modal show" : "modal fade"} id="warningModal" role="dialog">
                    <div className="resume-modal-vertical-alignment-helper">
                        <div className="resume-modal-warning">

                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" onClick={e => this.closeModalError()}>&times;</button>
                                    <h4 className="modal-title">Warning</h4>
                                </div>
                                <div className="modal-body" ref="modalbody">
                                    <p>{this.state.ModalError || ""}</p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" onClick={e => this.closeModalError()}>Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const ImageHeaderComponent: React.StatelessComponent<{ imageUrl: string, title: string, width?: string }> =
    (imageData: { imageUrl: string, title: string, width?: string }) => {

        return (
            <div className="resume-header">
                <hr />
                {imageData.width ?
                    (<img className="resume-image-header" src={imageData.imageUrl} width={imageData.width} />) :
                    (<img className="resume-image-header" src={imageData.imageUrl} />)
                }
                <span>{imageData.title}</span>
            </div>
        );
    }

$(window).resize(function () {
    setContentHeight();
});

var setContentHeight = () => {
    let height = $(document).outerHeight(true) - ($('.header-section').outerHeight(true) +
        $('.footer-section').outerHeight(true) + $('.form-footer').outerHeight(true) + $('#resume-header').outerHeight(true));
    $('#resume-content').height(height);
}

namespace Resume {
    export function Initialize(data: any) {
        ResumeApi.InitializeDictionaries(data.ResumeDictionaries);
        ReactDOM.render(
            <ResumeComponent Data={data} />,
            document.getElementById('resume-root')
        );
        setContentHeight();
    }
}