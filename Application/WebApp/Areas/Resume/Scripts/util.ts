
module ResumeHelper {
    "use strict";
    export const VerifyIsNumeric = (value: string): boolean => {
        return /^\d*$/.test(value);
    };

    export const VerifyDateIsInRange = (value: string): boolean => {

        if (value == null || !Date.parse(value)) {
            return false;
        }

        let date = new Date(value);
        let curdate = new Date();

        return !(isNaN(date.getFullYear()) || Math.abs(date.getFullYear() - curdate.getFullYear()) > 100);
    };

    export const VerifyIsMoreThan20CenturyDate = (value: string): boolean => {
        if (value == null) {
            return false;
        }

        let date = new Date(value);

        if (!(isNaN(date.getFullYear()) || date.getFullYear() < 1900)) {
            return true;
        }

        var partsOfStr = value.trim().split(' ');

        if (partsOfStr.length < 2) {
            partsOfStr.unshift("1");
        }

        date = new Date(parseInt(partsOfStr[1]), parseInt(partsOfStr[0]));

        return !(isNaN(date.getFullYear()) || date.getFullYear() < 1900);
    };

    export const MergeElement = (target: any, source: any): any => {
        return $.extend(target, source);
    };
    const hideAlertDelay = new Atomic.Debouncer(() => {
        const $container = $("#alert-template");
        const $messagePlaceholder = $container.find("strong");

        $messagePlaceholder.text("");
        $container.hide();
    }, 2000);

    export const Alert = (message: string, type: AlertMessages.AlertType = AlertMessages.AlertType.Success): void =>
    {
        const defaultClassNames = "alert col-sm-3";

        const $container = $("#alert-template");
        const $messagePlaceholder = $container.find("strong");

        // reset alert
        $messagePlaceholder.text("");
        $container.attr("class", defaultClassNames);

        const className = type === AlertMessages.AlertType.Success
            ? "alert-success"
            : "alert-danger";

        $container.addClass(className)
        $messagePlaceholder.text(message);
        $container
            .alert()
            .show(0, () => {
                hideAlertDelay.execute();
            });
    }
}