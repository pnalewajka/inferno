﻿module ResumeActionsTypes {

    export const AddNewCompany = "ADD_NEW_COMPANY_ACTION";
    export const RemoveCompany = "REMOVE_COMPANY_ACTION";
    export const ChangeCompanyName = "CHANGE_COMPANY_NAME_ACTION";
    export const ChangeCompanyPeriodFrom = "CHANGE_COMPANY_PERIOD_FROM_ACTION";
    export const ChangeCompanyPeriodTo = "CHANGE_COMPANY_PERIOD_TO_ACTION";
    export const ShowCompanySuggestions = "SHOW_COMPANY_SUGGESTIONS";
    export const HideCompanySuggestions = "HIDE_COMPANY_SUGGESTIONS";
    export const PickCompanyName = "PICK_COMPANY_NAME_ACTION";

    export const AddNewProject = "ADD_NEW_PROJECT_ACTION";
    export const RemoveProject = "REMOVE_PROJECT_ACTION";
    export const ChangeProjectName = "CHANGE_PROJECT_NAME_ACTION";
    export const ChangeProjectPeriodFrom = "CHANGE_PROJECT_PERIOD_FROM_ACTION";
    export const ChangeProjectPeriodTo = "CHANGE_PROJECT_PERIOD_TO_ACTION";
    export const ChangeProjectRoles = "CHANGE_PROJECT_ROLES_ACTION";
    export const ChangeProjectDuties = "CHANGE_PROJECT_DUTIES_ACTION";
    export const ChangeProjectDescription = "CHANGE_PROJECT_DESCRIPTION_ACTION";
    export const ChangeProjectSkillset = "CHANGE_PROJECT_SKILLSET_ACTION";
    export const ChangeProjectBranch = "CHANGE_PROJECT_BRANCH_ACTION";
    export const AddProjectSkill = "ADD_PROJECT_SKILL_ACTION";
    export const RemoveProjectSkill = "REMOVE_PROJECT_SKILL_ACTION";
    export const UpdateProjectSkill = "UPDATE_PROJECT_SKILL_ACTION";
    export const PickProject = "PICK_PROJECT_ACTION";
    export const AddProjectIndustry = "ADD_PROJECT_INDUSTRY_ACTION";
    export const RemoveProjectIndustry = "REMOVE_PROJECT_INDUSTRY_ACTION";
    export const UpdateProjectIndustry = "UPDATE_PROJECT_INDUSTRY_ACTION";

    export const AddNewTraining = "ADD_NEW_TRAINING_ACTION";
    export const RemoveTraining = "REMOVE_TRAINING_ACTION";
    export const ChangeTrainingTitle = "CHANGE_TRAINING_TITLE_ACTION";
    export const ChangeTrainingDate = "CHANGE_TRAINING_DATE_ACTION";
    export const ChangeTrainingInstitution = "CHANGE_TRAINING_INSTITUTION_ACTION";
    export const ChangeTrainingType = "CHANGE_TRAINING_TYPE_ACTION";

    export const AddNewEducation = "ADD_NEW_EDUCATION_ACTION";
    export const RemoveEducation = "REMOVE_EDUCATION_ACTION";
    export const ChangeEducationUniversityName = "CHANGE_EDUCATION_UNIVERSITYNAME_ACTION";
    export const ChangeEducationDegree = "CHANGE_EDUCATION_DEGREE_ACTION";
    export const ChangeEducationSpecialization = "CHANGE_EDUCATION_SPECIALIZATION_ACTION";
    export const ChangeEducationFrom = "CHANGE_EDUCATION_FROM_ACTION";
    export const ChangeEducationTo = "CHANGE_EDUCATION_TO_ACTION";

    export const AddNewLanguage = "ADD_NEW_LANGUAGE_ACTION";
    export const RemoveLanguage = "REMOVE_LANGUAGE_ACTION";
    export const ChangeLanguageName = "CHANGE_LANGUAGE_NAME_ACTION";
    export const ChangeLanguageLevel = "CHANGE_LANGUAGE_LEVEL_ACTION";

}