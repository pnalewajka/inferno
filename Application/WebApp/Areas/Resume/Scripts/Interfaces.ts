﻿interface ActionArgs<T> {
    Type: string;
    Index?: number;
    Data?: T;
}

//dictionary
interface DictionaryValues {
    Id: number;
    Name: string;
}
interface ProjectsDictionary {
    Id: number;
    Name: string;
    IsProjectConfidential: boolean;
}
interface EnumValues {
    Value: number;
    Name: string;
}
interface SkillsDictionary {
    Id: number;
    Name: string;
    SkillTagIds: number[];
}

interface SkillTagDictionary {
    Id: number;
    Name: string;
    SkillTagOrder: number;
}

interface ISkillTag {
    SkillTag: string;
    Name: string;
    SkillTagOrder: number;
    Data: () => MyResumeTechnicalSkill[];
}

interface MyResume {
    FillPercentage: number;
    Title: string;
    Resume: MyResumeDetail;
    ResumeTechnicalSkills: MyResumeTechnicalSkill[];
    ResumeCompanies: MyResumeCompany[];
    ResumeLanguages: MyResumeLanguage[];
    ResumeTrainings: MyResumeTraining[];
    ResumeEducations: MyResumeEducation[];
    ResumeRejectedSkillSuggestions?: number[];
    ResumeAdditionalSkills: MyResumeAdditionalSkill[];
    Timestamp: string;
    IsMine: boolean;
}

interface MyResumeDetail {
    Id: number;
    EmployeeId: number;
    CivoResumeVersionId?: number;
    EmlpoyeeName: string;
    AvailabilityInMonths: number;
    Summary: string;
    YearsOfExperience: string;
    Position: string;
    Timestamp: string;
    IsConsentClauseForDataProcessingChecked: boolean;
}

interface MyResumeTechnicalSkill {
    Id: number;
    TechnicalSkillId: number;
    Name: string;
    ExpertiseLevel: number;
    ExpertiseLevelName?: string;
    ExpertisePeriod: number;
    ExpertisePeriodName?: string;
    SkillTagName: string;
    SkillTagId: number;
    Timestamp: string;
    Edit?: boolean;
    Error?: boolean;
    CreatedOn?: Date;
    Index: number;
}

interface ResumeSuggestion {
    SuggestionReason: string;
    SkillSuggestions: SuggestedSkill[];
}

interface SuggestedSkill {
    SkillId: number,
    Name: string,
    ExpertiseLevel: number,
    ExpertiseLevelName: string,
    ExpertisePeriod: number,
    ExpertisePeriodName: string,
    SkillTagId: number,
    SkillTagName: string,
    Confirmed?: boolean
}

interface MyResumeCompany {
    Id: number;
    Name: string;
    From: string;
    To?: string;
    ResumeProjects: MyResumeProjects[];
    Timestamp: string;
    ShowSuggestions?: boolean;
    NameInEditMode?: boolean
}

interface MyResumeProjects {
    Id: number;
    ProjectId: number;
    Name: string;
    From: string;
    To?: string;
    Roles: string;
    Duties: string;
    ProjectDescription: string;
    TechnicalSkills: MyResumeProjectsSkills[];
    Industries: MyResumeProjectsIndustries[];
    Timestamp: string;
    IsProjectConfidential: boolean;
    HintedProjects?: ProjectsDictionary[];
}

interface MyResumeProjectsSkills extends DictionaryValues { }

interface MyResumeProjectsIndustries extends DictionaryValues { }

interface MyResumeLanguage {
    Id: number;
    LanguageId: number;
    LanguageName: string;
    Level: number;
    LevelName: string;
    Timestamp: string;
}

interface MyResumeTraining {
    Id: number;
    Title: string
    Date: string;
    Institution: string;
    Type: number;
    TypeName: string;
    Timestamp: string;
}

interface MyResumeEducation {
    Id: number;
    UniversityName: string;
    From: string;
    To: string;
    Degree: string;
    Specialization: string;
    Timestamp: string;
}

interface MyResumeAdditionalSkill {
    Id: number;
    Name: string;
    Edit?: boolean;
    Error?: boolean;
}

//state
interface ResumeState { Content: MyResume, ModalError?: string }

interface ResumeTechnicalSkillTableState {
    Data: { [tag: string]: MyResumeTechnicalSkill[] };
}

interface ResumeCompanyTableState { Data: MyResumeCompany[]; }

interface ResumeSuggestionsDialogState {
    data: ResumeSuggestion;
    isClosed: boolean;
}

interface ResumeLanguageTableState { Data: MyResumeLanguage[]; }

interface ResumeEducationTableState { Data: MyResumeEducation[]; }

interface ResumeTrainingTableState { Data: MyResumeTraining[]; }

interface ResumeAdditionalSkillTableState { Data: MyResumeAdditionalSkill[]; }

//props resume
interface ResumeProps { Data: MyResume }

//props details
interface ResumeDetailsProps { Data: MyResumeDetail; DetailsChange: Function; }

//props technical skills
interface ResumeTechnicalSkillAutoComplete {
    Data: MyResumeTechnicalSkill;
    ExistingSkillNames: string[];
    AutoCompleteClicked: Function;
    Index: number;
}

interface ResumeTechnicalSkillTableProps {
    Data: MyResumeTechnicalSkill[];
    StateChanged: Function;
}

interface ResumeSuggestionsDialogProps {
    submit: Function;
}

interface ResumeTechnicalSkillFlow {
    ExistingSkillNames: string[];
    SaveChanges: Function;
    DiscardEditing: Function;
    RemoveComponent: Function;
    Index: number;
}

interface ResumeTechnicalSkillPillProps extends ResumeTechnicalSkillFlow {
    Data: MyResumeTechnicalSkill;
}

interface ResumeTechnicalSkillProps extends ResumeTechnicalSkillFlow {
    Name: string;
    Data: MyResumeTechnicalSkill[];
    SkillTag: string;
}

interface SuggestedSkillPillProps {
    data: SuggestedSkill;
    expertiseLevelChange: Function;
    expertisePeriodChange: Function;
    statusChange: Function;
}

//props companies
interface ResumeCompanyTableProps {
    Data: MyResumeCompany[];
    StateChanged: (state: ResumeCompanyTableState) => void;
}

interface ResumeCompanyProps {
    Data: MyResumeCompany;
    Index: number;
    OnCompanyAction: (args: ActionArgs<any>) => void;
    OnProjectAction: (args: ActionArgs<any>) => void;
}

interface ResumeProjectProps {
    Company: MyResumeCompany;
    Data: MyResumeProjects;
    CompanyIndex: number;
    Index: number;
    OnProjectAction: (args: ActionArgs<any>) => void;
}

//props languages
interface ResumeLanguageTableProps {
    Data: MyResumeLanguage[];
    StateChanged: Function;
}

interface ResumeLanguageProps {
    Data: MyResumeLanguage;
    OnLanguageAction: (args: ActionArgs<DictionaryValues>) => void;
    Index: number;
}

//props education
interface ResumeEducationTableProps {
    Data: MyResumeEducation[];
    StateChanged: Function;
}

interface ResumeEducationProps {
    Data: MyResumeEducation;
    OnEducationAction: (args: ActionArgs<string>) => void;
    Index: number;
}

//props additional skills
type StateChangedCallback = (state: MyResumeAdditionalSkill[]) => void;
type AddComponentCallback = () => void;
type SaveChangesCallback = (event: React.MouseEvent<HTMLSpanElement>, index: number, skill: MyResumeAdditionalSkill) => void;
type RemoveComponentCallback = (event: React.MouseEvent<HTMLSpanElement>, index: number, skill: MyResumeAdditionalSkill) => void;
type AdditionalSkillChangeCallback = (data: string, index: number) => void;

interface ResumeAdditionalSkillTableProps {
    Data: MyResumeAdditionalSkill[];
    StateChanged: StateChangedCallback;
}

interface ResumeAdditionalSkillFlow {
    AddComponent: AddComponentCallback;
    SaveChanges: SaveChangesCallback;
    RemoveComponent: RemoveComponentCallback;
    Index: number;
}

interface ResumeAdditionalSkillProps extends ResumeAdditionalSkillFlow{
    Edit?: boolean;
    Name: string;
    Data: MyResumeAdditionalSkill;
    AdditionalSkillChange: AdditionalSkillChangeCallback;
}

//props trainnig
interface ResumeTrainingTableProps {
    Data: MyResumeTraining[];
    StateChanged: Function;
    OnRecordAdded: Function;
}

interface ResumeTrainingProps {
    Data: MyResumeTraining;
    OnTrainingAction: (args: ActionArgs<any>) => void;
    Index: number;
}

interface ResumeConsentClauseForDataProcessingProps {
    Data: MyResumeDetail;
    Show: boolean;
    StateChanged: (isChecked: boolean) => void;
}