﻿using System.Web.Mvc;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Resume.Models;
using Smt.Atomic.WebApp.Areas.Resume.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;

namespace Smt.Atomic.WebApp.Areas.Resume.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanEditMyResume)]
    public class MyResumeController : BaseResumeController
    {
        public MyResumeController(IBaseControllerDependencies baseDependencies,
            IClassMapping<IndustryDto, IndustryViewModel> industryDtoToIndustryViewModelClassMapping,
            IClassMapping<SkillDto, SkillViewModel> skillDtoToSkillViewModelClassMapping,
            IClassMapping<SkillTagDto, SkillTagViewModel> skillTagDtoToSkillTagViewModelClassMapping,
            IClassMapping<LanguageDto, LanguageViewModel> languageDtoToLanguageViewModelClassMapping,
            IClassMapping<SuggestedSkillsGroupDto, SuggestedSkillsGroupViewModel> suggestedSkillsGroupDtoToSuggestedSkillsGroupViewModel,
            IClassMapping<MyResumeDto, MyResumeViewModel> resumeDtoToResumeViewModelClassMapping,
            IResumeService resumeService) :
            base(baseDependencies,
                industryDtoToIndustryViewModelClassMapping,
                skillDtoToSkillViewModelClassMapping,
                skillTagDtoToSkillTagViewModelClassMapping,
                languageDtoToLanguageViewModelClassMapping,
                suggestedSkillsGroupDtoToSuggestedSkillsGroupViewModel,
                resumeDtoToResumeViewModelClassMapping,
                resumeService)
        {
        }

        public ActionResult Index()
        {
            var employeeId = _principalProvider.Current.EmployeeId;

            if (employeeId == -1)
            {
                throw new BusinessException(ResumeResources.MyResumeNotAvailable);
            }

            var resumeViewModel = GetResume(employeeId, ResumeResources.TitleMyResume);

            return View("~/Areas/Resume/Views/MyResume/Index.cshtml", resumeViewModel);
        }
    }
}