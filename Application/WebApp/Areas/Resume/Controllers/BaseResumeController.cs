﻿using System;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Resume.Models;
using Smt.Atomic.WebApp.Areas.Resume.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;

namespace Smt.Atomic.WebApp.Areas.Resume.Controllers
{
    public abstract class BaseResumeController : AtomicController
    {
        protected readonly IPrincipalProvider _principalProvider;
        protected readonly IResumeService ResumeService;
        private readonly IClassMapping<IndustryDto, IndustryViewModel> _industryDtoToIndustryViewModelClassMapping;
        private readonly IClassMapping<SkillDto, SkillViewModel> _skillDtoToSkillViewModelClassMapping;
        private readonly IClassMapping<SkillTagDto, SkillTagViewModel> _skillTagDtoToSkillTagViewModelClassMapping;
        private readonly IClassMapping<LanguageDto, LanguageViewModel> _languageDtoToLanguageViewModelClassMapping;
        private readonly IClassMapping<SuggestedSkillsGroupDto, SuggestedSkillsGroupViewModel> _suggestedSkillsGroupsDtoToSuggestedSkillsGroupViewModel;
        private readonly IClassMapping<MyResumeDto, MyResumeViewModel> _resumeDtoToResumeViewModelClassMapping;

        public BaseResumeController(IBaseControllerDependencies baseDependencies,
            IClassMapping<IndustryDto, IndustryViewModel> industryDtoToIndustryViewModelClassMapping,
            IClassMapping<SkillDto, SkillViewModel> skillDtoToSkillViewModelClassMapping,
            IClassMapping<SkillTagDto, SkillTagViewModel> skillTagDtoToSkillTagViewModelClassMapping,
            IClassMapping<LanguageDto, LanguageViewModel> languageDtoToLanguageViewModelClassMapping,
            IClassMapping<SuggestedSkillsGroupDto, SuggestedSkillsGroupViewModel> suggestedSkillsGroupDtoToSuggestedSkillsGroupViewModel,
            IClassMapping<MyResumeDto, MyResumeViewModel> resumeDtoToResumeViewModelClassMapping,
            IResumeService resumeService) : base(baseDependencies)
        {
            _principalProvider = baseDependencies.PrincipalProvider;
            _industryDtoToIndustryViewModelClassMapping = industryDtoToIndustryViewModelClassMapping;
            _skillDtoToSkillViewModelClassMapping = skillDtoToSkillViewModelClassMapping;
            _skillTagDtoToSkillTagViewModelClassMapping = skillTagDtoToSkillTagViewModelClassMapping;
            _languageDtoToLanguageViewModelClassMapping = languageDtoToLanguageViewModelClassMapping;
            _suggestedSkillsGroupsDtoToSuggestedSkillsGroupViewModel = suggestedSkillsGroupDtoToSuggestedSkillsGroupViewModel;
            _resumeDtoToResumeViewModelClassMapping = resumeDtoToResumeViewModelClassMapping;
            ResumeService = resumeService;

            Layout.PageHeader = string.Empty;
            Layout.PageTitle = ResumeResources.Title;
            Layout.Resources.AddFrom<ResumeResources>();

            Layout.Parameters.AddParameter<string>(ParameterKeys.ResumeConsentClauseForDataProcessing, new CultureDependedParameterContext() { CurrentCulture = CultureInfo.CurrentCulture.Name });
            Layout.Parameters.AddParameter<string>(ParameterKeys.ResumeIntiveIs, new CultureDependedParameterContext() { CurrentCulture = CultureInfo.CurrentCulture.Name });
        }

        protected MyResumeViewModel GetResume(long id, string title)
        {
            var employeeResumeDto = ResumeService.GetEmployeeResumeDto(id);
            var employeeResumeViewModel = _resumeDtoToResumeViewModelClassMapping.CreateFromSource(employeeResumeDto);
            employeeResumeViewModel.Title = title;
            employeeResumeViewModel.ResumeDictionaries = GetResumeDictionaries(id);
            employeeResumeViewModel.IsMine = GetCurrentPrincipal().EmployeeId == id;

            return employeeResumeViewModel;
        }

        private ResumeDictionariesViewModel GetResumeDictionaries(long id)
        {
            var principalId = GetCurrentPrincipal().EmployeeId;
            var resumeOwnerId = id;

            var result = new ResumeDictionariesViewModel();

            var languagesDto = ResumeService.GetLanguages();
            result.Languages = languagesDto.Select(s => new DictionaryItemViewModel { Id = s.Id, Name = s.Name.ToString() }).ToList();

            var languageLevels = Enum.GetValues(typeof(LanguageReferenceLevel)).Cast<LanguageReferenceLevel>().ToList();
            result.LanguageLevels = languageLevels.Select(t => new ValuePickerItemViewModel { Value = (int)t, Name = t == LanguageReferenceLevel.None ? "" : EnumExtensions.GetDescription(t) }).ToList();

            var trainingTypes = Enum.GetValues(typeof(TrainingType)).Cast<TrainingType>().ToList();
            result.TrainingTypes = trainingTypes.Select(t => new ValuePickerItemViewModel { Value = (int)t, Name = EnumExtensions.GetDescription(t) }).ToList();

            var expertLevels = Enum.GetValues(typeof(SkillExpertiseLevel)).Cast<SkillExpertiseLevel>().ToList();
            result.ExpertiseLevels = expertLevels.Select(t => new ValuePickerItemViewModel { Value = (int)t, Name = t == SkillExpertiseLevel.None ? "" : EnumExtensions.GetDescription(t) }).ToList();

            var expertPeriods = Enum.GetValues(typeof(SkillExpertisePeriod)).Cast<SkillExpertisePeriod>().ToList();
            result.ExpertisePeriods = expertPeriods.Select(t => new ValuePickerItemViewModel { Value = (int)t, Name = t == SkillExpertisePeriod.None ? "" : EnumExtensions.GetDescription(t) }).ToList();

            var skillTagsDto = ResumeService.GetEmployeeSkillTags(resumeOwnerId);
            result.SkillTags = skillTagsDto.Select(s => new SkillTagValueViewModel { Id = s.SkillTagId, Name = s.Name.ToString(), SkillTagOrder = s.CustomOrder }).ToList();

            var skillsDto = ResumeService.GetSkills();
            result.Skills = skillsDto.Select(i => _skillDtoToSkillViewModelClassMapping.CreateFromSource(i)).ToList();

            var industriesDto = ResumeService.GetIndustries();
            result.Industries = industriesDto.Select(s => new DictionaryItemViewModel { Id = s.Id, Name = s.Name.ToString() }).ToList();

            var companies = ResumeService.GetCompanies();
            result.Companies = companies.Select(c => new DictionaryItemViewModel { Id = c.Id, Name = c.Name }).ToList();

            SuggestedSkillsGroupDto skillSuggestion = null;

            //suggest only if it is edited by owner
            if (resumeOwnerId == principalId)
            {
                skillSuggestion = ResumeService.GetEmployeeSuggestedSkillsGroup(resumeOwnerId);
            }
            else
            {
                skillSuggestion = SuggestedSkillsGroupDto.Empty();
            }

            result.Suggestion = _suggestedSkillsGroupsDtoToSuggestedSkillsGroupViewModel.CreateFromSource(skillSuggestion);

            return result;
        }
    }
}