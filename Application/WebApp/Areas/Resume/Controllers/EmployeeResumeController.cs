﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.Resume.Models;
using Smt.Atomic.WebApp.Areas.Resume.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Models;

namespace Smt.Atomic.WebApp.Areas.Resume.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewEmployeesResume)]
    public class EmployeeResumeController : BaseResumeController
    {
        private readonly IEmployeeService _employeeService;
        private readonly IReportingService _reportingService;

        public EmployeeResumeController(IBaseControllerDependencies baseDependencies,
            IClassMapping<IndustryDto, IndustryViewModel> industryDtoToIndustryViewModelClassMapping,
            IClassMapping<SkillDto, SkillViewModel> skillDtoToSkillViewModelClassMapping,
            IClassMapping<SkillTagDto, SkillTagViewModel> skillTagDtoToSkillTagViewModelClassMapping,
            IClassMapping<LanguageDto, LanguageViewModel> languageDtoToLanguageViewModelClassMapping,
            IClassMapping<SuggestedSkillsGroupDto, SuggestedSkillsGroupViewModel> suggestedSkillsGroupDtoToSuggestedSkillsGroupViewModel,
            IClassMapping<MyResumeDto, MyResumeViewModel> resumeDtoToResumeViewModelClassMapping,
            IResumeService resumeService,
            IEmployeeService employeeService,
            IReportingService reportingService)
            : base(baseDependencies,
                industryDtoToIndustryViewModelClassMapping,
                skillDtoToSkillViewModelClassMapping,
                skillTagDtoToSkillTagViewModelClassMapping,
                languageDtoToLanguageViewModelClassMapping,
                suggestedSkillsGroupDtoToSuggestedSkillsGroupViewModel,
                resumeDtoToResumeViewModelClassMapping,
                resumeService)
        {
            _employeeService = employeeService;
            _reportingService = reportingService;
        }

        [AtomicAuthorize(SecurityRoleType.CanEditEmployeesResume)]
        public ActionResult Index(long id)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(id);

            if (employee == null)
            {
                throw new BusinessException(ResumeResources.EmployeeResumeNotAvailable);
            }

            var resumeViewModel = GetResume(id, $"{employee.FirstName} {employee.LastName}");

            return View("~/Areas/Resume/Views/MyResume/Index.cshtml", resumeViewModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanShareEmployeesResume)]
        public PartialViewResult Share(long id)
        {
            var existingToken = ResumeService.GetResumeShareToken(
                sharerEmployeeId: _principalProvider.Current.EmployeeId,
                ownerEmployeeId: id);

            if (existingToken.HasValue)
            {
                return ShowShareUrl(existingToken.Value);
            }

            var model = new ShareResumeViewModel
            {
                EmployeeId = id,
            };

            var dialogModel = new ModelBasedDialogViewModel<ShareResumeViewModel>(model)
            {
                Title = ResumeResources.ShareResumeTitle,
            };

            return PartialView("Share", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanShareEmployeesResume)]
        public ActionResult PerformShare(ShareResumeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var token = ResumeService.CreateResumeShareToken(
                    sharerEmployeeId: _principalProvider.Current.EmployeeId,
                    employeeId: model.EmployeeId,
                    reason: model.Reason);

                // close the old modal and refresh it
                return DialogHelper.HandleEvent("common-dialog-show-model-shareresumeviewmodel", "", null);
            }

            return Share(model.EmployeeId);
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanDownloadSharedEmployeeResume)]
        public FileContentResult DownloadShared(Guid token)
        {
            var employeeId = ResumeService.DownloadFromResumeShareToken(token);

            var resume = (ArchivableFileContentResult)_reportingService
                .PerformReport(
                        EmployeeResumeHelper.ResumeReportDataSource,
                    new ResumeReportParametersViewModel { EmployeeId = employeeId })
                .Render();

            return new FileContentResult(resume.Content, resume.ContentType)
            {
                FileDownloadName = resume.FileName,
            };
        }

        private PartialViewResult ShowShareUrl(Guid token)
        {
            var model = new ModelBasedDialogViewModel<Guid>(token, StandardButtons.None)
            {
                Title = ResumeResources.ShareUrlTitle,
            };

            return PartialView("ShowShareUrl", model);
        }
    }
}