﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Resume.Models;

namespace Smt.Atomic.WebApp.Areas.Resume.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanEditMyResume)]
    public class ResumeApiController : AtomicController
    {
        private readonly IResumeService _resumeService;
        private readonly IClassMapping<MyResumeDto, MyResumeViewModel> _resumeDtoToResumeViewModelClassMapping;
        private readonly IClassMapping<MyResumeViewModel, MyResumeDto> _resumeViewModelToResumeDtoClassMapping;

        public ResumeApiController(IBaseControllerDependencies baseControllerDependencies,
            IClassMapping<MyResumeDto, MyResumeViewModel> resumeDtoToResumeViewModelClassMapping,
            IClassMapping<MyResumeViewModel, MyResumeDto> resumeViewModelToResumeDtoClassMapping,
            IResumeService resumeService) : base(baseControllerDependencies)
        {
            _resumeDtoToResumeViewModelClassMapping = resumeDtoToResumeViewModelClassMapping;
            _resumeViewModelToResumeDtoClassMapping = resumeViewModelToResumeDtoClassMapping;
            _resumeService = resumeService;
        }

        [HttpPost]
        public ActionResult SaveMyResume(MyResumeViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new BusinessException(string.Join(", ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));
            }

            var myResumeDto = _resumeViewModelToResumeDtoClassMapping.CreateFromSource(viewModel);

            _resumeService.SubmitResume(myResumeDto);

            return GetResume(viewModel.Resume.EmployeeId.Value, viewModel.Title);
        }

        public JsonResult GetProjects(int topProjects, string nameFilter = null)
        {
            var projectsDto = _resumeService.GetProjects(topProjects, nameFilter);

            var projectsViewModel = projectsDto.Select(p => new
            {
                Id = p.Id,
                Name = p.ClientProjectName,
                IsProjectConfidential = p.Disclosures != CrossCutting.Business.Enums.ProjectDisclosures.WeCanTalkAboutProject
            }).ToList();

            return Json(projectsViewModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectDescription(int id)
        {
            var projectDescription = _resumeService.GetProjectDescription(id);

            return Json(projectDescription, JsonRequestBehavior.AllowGet);
        }

        private JsonResult GetResume(long id, string title)
        {
            var employeeResumeDto = _resumeService.GetEmployeeResumeDto(id);
            var employeeResumeViewModel = _resumeDtoToResumeViewModelClassMapping.CreateFromSource(employeeResumeDto);
            employeeResumeViewModel.Title = title;
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver() };

            return Json(JsonConvert.SerializeObject(employeeResumeViewModel, settings), JsonRequestBehavior.AllowGet);
        }
    }

    public class NullToEmptyStringResolver : Newtonsoft.Json.Serialization.DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return type.GetProperties()
                    .Select(p =>
                    {
                        var jp = base.CreateProperty(p, memberSerialization);
                        jp.ValueProvider = new NullToEmptyStringValueProvider(p);
                        return jp;
                    }).ToList();
        }
    }

    public class NullToEmptyStringValueProvider : Newtonsoft.Json.Serialization.IValueProvider
    {
        PropertyInfo _MemberInfo;
        public NullToEmptyStringValueProvider(PropertyInfo memberInfo)
        {
            _MemberInfo = memberInfo;
        }

        public object GetValue(object target)
        {
            object result = _MemberInfo.GetValue(target);
            if (_MemberInfo.PropertyType == typeof(string) && result == null) result = "";
            return result;

        }

        public void SetValue(object target, object value)
        {
            _MemberInfo.SetValue(target, value);
        }
    }
}