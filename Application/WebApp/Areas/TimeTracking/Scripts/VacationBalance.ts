﻿module VacationBalance {

    export interface IVacationBalance extends Grid.IRow {
        employeeId: number;
    }

    export function addNewVacationBalance(url: string, grid: Grid.GridComponent) {
        let rows = grid.getSelectedRowData<IVacationBalance>();

        if (rows.length === 1) {
            url = `${url}?id=${rows[0].employeeId}`;
        }

        Forms.redirect(url);
    }
}