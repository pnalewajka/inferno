﻿module TimeTracking.Header {
    export class TimeReportHeader extends React.Component<{}, { toolbar: Store.Toolbar.IToolbarStoreState, isLoading: boolean }> {

        private toolbar: HTMLElement;

        componentWillMount() {
            this.updateData();
            TimeTracking.Store.defaultStore().subscribe(this.updateData.bind(this));
        }

        updateData() {
            const { toolbar, monitor } = TimeTracking.Store.defaultStore().getState();
            this.setState($.extend({}, this.state, { toolbar, isLoading: monitor.isLoading }));
        }

        componentDidMount() {
            HamburgerButtons.initMenus($(this.toolbar));
        }

        reorderToggle() {
            const store = TimeTracking.Store.defaultStore();
            store.dispatch(Actions.toggleisMovable());
        }
        getStatusLabel(): string {

            switch (this.state.toolbar.status) {
                case -1:
                    return Sheet.Translations.TimeReportStatusNotStarted;
                case 0:
                    return Sheet.Translations.TimeReportStatusDraft;
                case 1:
                    return Sheet.Translations.TimeReportStatusSubmitted;
                case 2:
                    return Sheet.Translations.TimeReportStatusDraftAcepted;
                case 3:
                    return Sheet.Translations.TimeReportStatusDraftRejected;
                default:
                    return "";
            }
        }

        private reopenTimeReport() {
            Dialogs.showDialog({
                actionUrl: Service.urls.ReopenTimeReport as string, formData: {
                    employeeId: this.state.toolbar.employeeId,
                    year: this.state.toolbar.year,
                    month: this.state.toolbar.month
                }, eventHandler: (result) => { }
            });
        }

        render() {
            if (!this.state || this.state.isLoading) {
                return null;
            }

            const statusClass = `status${this.state.toolbar.status === 3 ? " rejected" : ""}`;

            const importSources: Store.Toolbar.IImportSource[] = [
                ...this.state.toolbar.importSources.map(s => {
                    s.onSuccess = data => {
                        if (!!data.ImportAnyData && !!data.LoadUrl) {
                            CommonDialogs.pleaseWait.show();
                            let store = TimeTracking.Store.defaultStore();

                            $.get(data.LoadUrl)
                                .then(data => {
                                    store.dispatch(TimeTracking.Actions.loadData(data._payload));
                                    CommonDialogs.pleaseWait.hide();
                                });
                        }
                    };

                    return s;
                })
            ];

            const reopenAllowed = !Strings.isNullOrEmpty(Service.urls.ReopenTimeReport)
                && this.state.toolbar.canReopenTimeReport;

            return (
                <div>
                    <div className="container-fluid">
                        <div className="row grid-toolbar" ref={toolbar => this.toolbar = toolbar as HTMLElement}>
                            <div className="col-sm-12">
                                <div className="burger-box react-toolbar">
                                    <div className="burger-slices">
                                        {this.state.toolbar.canSave ? (<TimeReportSaveButton />) : null}
                                        {this.state.toolbar.canSubmit ? (<TimeReportSendButton />) : null}
                                        {
                                            this.state.toolbar.isReadOnly || importSources.length == 0
                                                ? null
                                                : <Ui.ButtonDropdown confirm={ConfirmChanges} label={Sheet.Translations.ImportButtonText} items={importSources} />
                                        }
                                        {
                                            this.state.toolbar.navigationActions.length > 0
                                                ? <Ui.ButtonDropdown confirm={ConfirmChanges} label={Sheet.Translations.RequestButtonText} items={this.state.toolbar.navigationActions} />
                                                : null
                                        }
                                        <TimeReportTimeCommandButton {... {
                                            currentYear: this.state.toolbar.year,
                                            currentMonth: this.state.toolbar.month,
                                            nextMonth: this.state.toolbar.canSeeNextMonth,
                                            prevMonth: this.state.toolbar.canSeePreviousMonth
                                        }} />
                                        {
                                            reopenAllowed
                                                ? <Ui.ButtonGroup>
                                                    <Ui.Button
                                                        icon="fa-star"
                                                        text={Sheet.Translations.ReopenTimeReport}
                                                        onClick={this.reopenTimeReport.bind(this)} />
                                                </Ui.ButtonGroup>
                                                : null
                                        }
                                        <Ui.ButtonGroup>
                                            <Ui.Button
                                                icon="fa-arrows-v"
                                                text={Sheet.Translations.ReorderButton}
                                                onClick={this.reorderToggle.bind(this)} />
                                        </Ui.ButtonGroup>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}