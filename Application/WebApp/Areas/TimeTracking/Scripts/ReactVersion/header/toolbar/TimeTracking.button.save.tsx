﻿module TimeTracking.Header {

    export class TimeReportSaveButton extends React.Component<{}, { pending: boolean }> {

        constructor() {
            super();

            this.state = { pending: false };
        }

        

        saveReport() {
            const store = Store.defaultStore();
            
            Service.saveReport(store.getState())
                .then((data) => {
                    store.dispatch(Actions.clearData());
                    store.dispatch(Actions.loadData(data));
                });
        }

        render() {
            return (
                <Ui.ButtonGroup>
                    <Ui.Button
                        primary={true}
                        disabled={this.state.pending}
                        text={Sheet.Translations.SaveButtonText}
                        onClick={this.saveReport.bind(this)} />
                </Ui.ButtonGroup>
            );
        }
    }
}
