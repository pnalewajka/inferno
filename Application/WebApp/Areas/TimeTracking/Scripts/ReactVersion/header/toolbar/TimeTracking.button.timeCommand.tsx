﻿module TimeTracking.Header {

    export function ConfirmChanges(): JQueryPromise<any> {
        var deffered = $.Deferred();

        var { monitor } = Store.defaultStore().getState();

        if (monitor.hasUnsavedChanges) {
            CommonDialogs.confirm(Sheet.Translations.DirtyFormConfirmText, Sheet.Translations.DirtyFormConfirmTitleText, result => {
                if (result.isOk) {
                    Store.defaultStore().dispatch({ type: TimeTracking.Actions.TimeTrackingActionType.ConfirmChanges });
                    deffered.resolve();
                } else {
                    deffered.reject();
                }
            });
        } else {
            deffered.resolve();
        }

        return deffered.promise();
    }

    export class TimeReportTimeCommandButton extends React.Component<{ currentYear: number, currentMonth: number, nextMonth: boolean, prevMonth: boolean }, {}> {

        constructor() {
            super();

            this.loadPrevious = this.loadPrevious.bind(this);
            this.loadNext = this.loadNext.bind(this);
            this.loadCurrent = this.loadCurrent.bind(this);
        }

        loadPrevious() {
            ConfirmChanges().then(() => {
                const month = this.props.currentMonth == 1 ? 12 : this.props.currentMonth - 1;
                const year = this.props.currentMonth == 1 ? this.props.currentYear - 1 : this.props.currentYear;

                window.location.search = UrlHelper.updateUrlParameters(window.location.search, `year=${year}&month=${month}`);
            });
        }

        loadNext() {
            ConfirmChanges().then(() => {
                const month = this.props.currentMonth == 12 ? 1 : this.props.currentMonth + 1;
                const year = this.props.currentMonth == 12 ? this.props.currentYear + 1 : this.props.currentYear;

                window.location.search = UrlHelper.updateUrlParameters(window.location.search, `year=${year}&month=${month}`);
            });
        }

        loadCurrent() {
            ConfirmChanges().then(() => {
                const search = UrlHelper.updateUrlParameters(window.location.search, 'year=&month=');

                window.location.search = UrlHelper.removeEmptyUrlParameters(search);
            });
        }

        render() {

            return (
                <div className="btn-group">
                    <button type="button" className="btn btn-default" onClick={this.loadPrevious.bind(this)} disabled={!this.props.prevMonth} > <i className="fa fa-caret-left" aria-hidden="true"></i> {Sheet.Translations.PreviousMonth}</button>
                    <button type="button" className="btn btn-default" onClick={this.loadCurrent.bind(this)}><i className="fa fa-map-marker" aria-hidden="true"></i> {Sheet.Translations.CurrentMonth}</button>
                    <button type="button" className="btn btn-default" onClick={this.loadNext.bind(this)} disabled={!this.props.nextMonth} > <i className="fa fa-caret-right" aria-hidden="true"></i> {Sheet.Translations.NextMonth}</button>
                </div>
            )

        }

    }

}