﻿namespace TimeTracking.Header {
    export class TimeReportSendButton extends React.Component<{}, {}> {
        render() {
            return (
                <Ui.ButtonGroup>
                    <Ui.Button
                        text={Sheet.Translations.SubmitButtonText}
                        onClick={this.trySubmitReport.bind(this)} />
                </Ui.ButtonGroup>
            );
        }

        getConfirmMessage(warnings: string[]): string {
            if (!!warnings && warnings.length > 0) {
                return `<div>${Sheet.Translations.SubmitConfirmMessage}<ul>${warnings.map(w => `<li>${w}</li>`).join('')}</ul></div>`;
            } else {
                return Sheet.Translations.SubmitConfirmMessage;
            }
        }

        trySubmitReport(): void {

            const store = Store.defaultStore();
            const state = store.getState();

            Service.submitConfirm({
                employeeId: state.toolbar.employeeId,
                year: state.toolbar.year,
                month: state.toolbar.month
            }).then((r: string[]) => {
                var deferred = $.Deferred();

                CommonDialogs.confirm(this.getConfirmMessage(r), Sheet.Translations.SubmitConfirmTitle, r => {
                    if (r.isOk) {
                        Service.submitReport(state)
                            .then(deferred.resolve)
                            .fail(deferred.reject);
                    } else {
                        deferred.reject();
                    }
                });

                return deferred.promise();

            }).then((data) => {
                store.dispatch(Actions.clearData());
                store.dispatch(Actions.loadData(data));
            });
        }
    }
}
