﻿module TimeTracking.Header {

    export class TimeReportToolbar extends React.Component<{ canSave: boolean, canSubmit: boolean }, {}> {
        render() {
            return (
                <div className="burger-box react-toolbar">
                    <div className="burger-slices">
                        {this.props.canSave ? (<TimeReportSaveButton />) : null}
                        {this.props.canSubmit ? (<TimeReportSendButton />) : null}
                    </div>
                </div>
            );
        }
    }
}
