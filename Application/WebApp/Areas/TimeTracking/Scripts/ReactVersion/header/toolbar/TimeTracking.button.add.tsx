﻿module TimeTracking.Header {

    export class TimeReportAddButton extends React.Component<{ actions: any[] }, {}> {

        private dropdownButton: HTMLButtonElement;

        constructor() {
            super();

            this.toggleDropdown = this.toggleDropdown.bind(this);
        }

        toggleDropdown() {
            $(this.dropdownButton).dropdown();
        }

        render() {

            const actions = this.props.actions.map((action, index) => {
                return (
                    <li className="" key={index}>
                        <a href={action.OnClickUrl} target="_blank" data-action="navigate">{action.Text}</a>
                    </li>
                )
            })

            return (
                <div className="btn-group">
                    <button className="btn dropdown-toggle btn-default" type="button" data-toggle="dropdown" ref={btn => this.dropdownButton = btn!} onClick={this.toggleDropdown}>
                        <i className="fa fa-plus" aria-hidden="true"></i> {Sheet.Translations.AddButtonText} <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu" role="menu">
                        {actions}
                    </ul>
                </div>
            )
        }
    }
}
