﻿module TimeTracking.Header {

    interface IBreadcrumbState {
        title: string
    }

    export class Breadcrumb extends React.Component<{}, IBreadcrumbState> {
        unsubscriber: Redux.Unsubscribe | null = null;

        componentWillMount() {
            this.setState({ title: '' });
            this.unsubscriber = Store.defaultStore().subscribe(() => {
                const { toolbar } = Store.defaultStore().getState();
                this.setState({ title: toolbar.title });
            });
        }

        shouldComponentUpdate(nextProps: {}, nextState: IBreadcrumbState) {
            return this.state.title !== nextState.title;
        }

        componentWillUpdate(nextProps: {}, nextState: IBreadcrumbState) {
            document.title = nextState.title;
        }

        render() {
            if (!this.state || !this.state.title) {
                return null;
            }

            return (
                <div className="breadcrumb-container">
                    <li className="active">{Sheet.Translations.TimeReportTitle}</li>
                    <li className="active">{this.state.title}</li>
                </div>
            );
        }
    }
}
