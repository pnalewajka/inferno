﻿
namespace TimeTracking.Sheet {
    
    interface IProjectState {
        days: Store.DateRange.IDay[];
        project: Store.Projects.IProject;
        tasks: Store.Tasks.ITask[];
        isCollapsed: boolean | undefined;
        movability: Store.Direction;
        isMovable: boolean | undefined;
    }

    interface IProjectsState {
        days: Store.DateRange.IDay[];
        projects: Store.Projects.IProject[];
        tasks: Store.Tasks.ITask[];
        projectsCollapsed: Store.Toolbar.IProjectsCollapsed;
        isMovable?: boolean;
    }

    function onProjectChange(projectId: number, newProjectId: number, newProjectName: string): boolean {

        // Avoid adding already existing project (avoid event too)
        if (Store.defaultStore().getState().projects.projects.some(p => p.ProjectId === newProjectId)) {
            if (newProjectId !== projectId) {
                AlertMessages.addInformation(Sheet.Translations.ProjectAlreadyPresent);
            }

            return false;
        }

        TimeReportTimeSheet.loadProjectMetadata(newProjectId).then((metadata: TimeTrackingReact.IProjectMetadata) => {
            Store.defaultStore().dispatch(Actions.changeProject(projectId, newProjectId, newProjectName, metadata));
        });

        return true;
    }

    const ProjectButtonsContainer: React.StatelessComponent<IProjectState> = (props: IProjectState): JSX.Element => {

        const hideButtons = props.project.IsReadOnly
            || props.tasks.some(t => t.IsReadOnly || Utils.Enumerate(t.Days).some(d => d.IsReadOnly && d.Hours > 0));

        return (
            <div className="btn-container">
                {!props.project.IsReadOnly ? null :
                    <div className="project-name-container">
                        <div className="project-indicator-icons">
                            <i className="fa fa-lock" title={Sheet.Translations.ProjectIsReadOnly}></i>
                        </div>
                    </div>
                }
                {props.project.IsReadOnly ? null :
                    <div>
                        <TaskButton icon="search" onClick={() => {
                            ValuePicker.selectSingle("TimeTracking.EmployeeProjects", (record) => {
                                onProjectChange(props.project.ProjectId, record.id, record.toString);
                            }, url => {
                                const searchQuery = Service.contextSearchQuery();

                                return `${url}&${searchQuery}`;
                            });
                        }}>
                        </TaskButton>
                        <TaskButton icon="plus" onClick={() => {
                            Store.defaultStore().dispatch(Actions.addTask(props.project.ProjectId));
                            if (props.isCollapsed) {
                                Store.defaultStore().dispatch(Actions.toggleCollapseProject(props.project.ProjectId));
                            } 
                        }}>
                        </TaskButton>
                        {hideButtons ? null :
                            <TaskButton icon="trash-o" onClick={() => {
                                CommonDialogs.confirm(Sheet.Translations.ConfirmDeleteProject, Sheet.Translations.ConfirmDialogTitle, a => {
                                    if (a.isOk) {
                                        Store.defaultStore().dispatch(Actions.removeProject(props.project.ProjectId));
                                    }
                                });
                            }}>}
                            </TaskButton>}
                    </div>}
            </div>
        );
    };

    let ProjectNameRenderComponent: React.StatelessComponent<IProjectState> = (props: IProjectState) => {
        const doubleMovable = Enum.hasFlag(props.movability, Store.Direction.Up | Store.Direction.Down);
        const { isMovable } = props;
        
        return (
            <td className="key-column name project-name static-column">
                <span>
                    {!isMovable && <span className={'project-other-task-buttons'}>
                        <TaskButton
                            className='btn-toggle-tasks'
                            icon={props.isCollapsed ? 'angle-right' : 'angle-down'}
                            onClick={() => {
                                Store.defaultStore().dispatch(Actions.toggleCollapseProject(props.project.ProjectId));
                            }}>
                        </TaskButton>
                    </span>}

                    {isMovable && <span className={Dante.Utils.ClassNames({ 'double-project-move': doubleMovable }, 'project-reorder-box')}>
                        {Enum.hasFlag(props.movability, Store.Direction.Up) && <TaskButton
                            className=' btn-project-move-up'
                            icon={'arrow-up'}
                            onClick={() => {
                                Store.defaultStore().dispatch(Actions.moveProjectUp(props.project.ProjectId));
                            }} />}
                        {Enum.hasFlag(props.movability, Store.Direction.Down) && <TaskButton
                            className=' btn-project-move-down'
                            icon={'arrow-down'}
                            onClick={() => {
                                Store.defaultStore().dispatch(Actions.moveProjectDown(props.project.ProjectId));
                            }} />
                        }
                    </span>}
                </span>
                <ProjectNameComponent {...{
                    project: props.project, onChange: (newProjectId, newProjectName) => {
                        const haveChanged = onProjectChange(props.project.ProjectId, newProjectId, newProjectName as string);

                        return haveChanged ? newProjectName as string : props.project.ProjectName;
                    }
                }} />
                <ProjectButtonsContainer
                    {...{
                        isMovable: props.isMovable,
                        movability: props.movability,
                        project: props.project,
                        tasks: props.tasks, days: props.days, isCollapsed: props.isCollapsed
                    }} />
            </td>
        );
    }

    let ProjectRenderComponent: React.StatelessComponent<IProjectState> = (props: IProjectState) => {
        return (
            <tr className={"project-row time-sheet-row"}>
                <ProjectNameRenderComponent
                    {... {
                        isMovable: props.isMovable,
                        movability: props.movability,
                        project: props.project,
                        tasks: props.tasks, days: props.days, isCollapsed: props.isCollapsed
                    }} />
                <td className="hours-sum hours-summary-column">
                    <SummaryProject projectId={props.project.ProjectId} />
                </td>
                {
                    props.days.map(d =>
                        <td key={d.Date.toString()}
                            className={[...dayCssClasses({ day: d }), "hours"].join(" ")}>
                            <SummaryProjectAndDay projectId={props.project.ProjectId} day={d.Date} />
                        </td>)
                }
            </tr>
        );
    };

    export class ProjectsComponent extends React.Component<{}, IProjectsState> {
        storeSubscribers: Redux.Unsubscribe | null = null;

        componentWillMount() {
            this.setState({
                days: [],
                tasks: [],
                projects: [],
                projectsCollapsed: {},
            });

            this.storeSubscribers = Store.defaultStore().subscribe(this.setStoreState.bind(this));
            this.setStoreState();
        }

        componentWillUnmount() {
            if (this.storeSubscribers != null)
                this.storeSubscribers();
            this.storeSubscribers = null;
        }

        shouldComponentUpdate(nextProps: {}, nextState: IProjectsState) {
            const shouldUpdate =
                this.state.tasks.map(t => t.Key).join() !== nextState.tasks.map(t => t.Key).join()
                || this.state.projects.map(t => t.ProjectId).join() !== nextState.projects.map(t => t.ProjectId).join()
                || this.state.days.length !== nextState.days.length
                || !Dante.Utils.AreEqual(this.state.projectsCollapsed, nextState.projectsCollapsed)
                || this.state.isMovable !== nextState.isMovable;

            return shouldUpdate;
        }

        setStoreState(): void {
            const start = Date.now();
            let { dateRange, projects, tasks, toolbar: { projectsCollapsed, isMovable } } = Store.defaultStore().getState();

            let newTsks: Store.Tasks.ITask[] = [];

            for (var taskKey in tasks.tasks) {
                newTsks.push(tasks.tasks[taskKey]);
            }

            let newState: IProjectsState = {
                days: [...dateRange.days],
                projects: [...projects.projects],
                tasks: newTsks,
                projectsCollapsed,
                isMovable
            };

            if (!this.state) {
                this.setState(newState);
            } else {
                this.setState($.extend({}, this.state, newState));
            }
        }

        private getMovability(index: number, projectCount: number) {
            let movability = Store.Direction.None;
           
            if (index > 0) {
                movability |= Store.Direction.Up;
            }

            if (index < projectCount - 1) {
                movability |= Store.Direction.Down;
            }

            return movability;
        }

        render() {
            let elements: JSX.Element[] = [];
            let { projects } = this.state;
            projects.forEach((project, index) => {
                const isCollapsed = this.state.projectsCollapsed[project.ProjectId] || this.state.isMovable;
                const tasks = this.state.tasks.filter(task => task.ProjectId === project.ProjectId && !isCollapsed);
                
                elements.push(
                    <ProjectRenderComponent
                        isMovable={this.state.isMovable}
                        movability={this.getMovability(index, projects.length)}
                        key={`p_${project.ProjectId}`}
                        {...{ project, days: this.state.days, tasks: tasks, isCollapsed }} />
                );

                tasks.forEach(task => {
                    elements.push(<TaskRenderComponent key={`t_${task.Key}`} {...{ task, days: this.state.days }} />);
                });
            });

            return elements;
        }
    }
    
    class FixedColumnCell extends React.Component<React.HTMLProps<any>, Store.Layout.ILayoutPosition> {
        unsubscribe: Redux.Unsubscribe | null = null;

        componentWillMount() {
            this.unsubscribe = Store.defaultLayoutStore().subscribe(this.updateState.bind(this));
            this.updateState();
        }

        componentWillUnmount() {
            if (this.unsubscribe != null)
                this.unsubscribe();
        }

        updateState() {
            const { position } = Store.defaultLayoutStore().getState();
            this.setState(position);
        }

        shouldComponentUpdate(nextProps: React.HTMLProps<any>, nextState: Store.Layout.ILayoutPosition) {

            return this.state.top !== nextState.top && nextState.left > 0;
        }

        render() {
            const newProps = Utils.Assign<{}>(this.props, { style: Utils.Assign(this.props.style || {}, { top: -(this.state.top || 0) }) });

            return <td {...newProps}>{this.props.children}</td>;
        }
    }
}
