﻿namespace TimeTracking.Sheet {
    export interface IProjectNameProps {
        project?: Store.Projects.IProject;
        onChange?: (projectId: number, projectName?: string) => string;
        placeholder?: string;
    }

    export interface IProjectNameState {
        projectName: string;
        apn: string;
        kupferwerkProjectId: string;
        isReadOnly: boolean;
    }

    var options: Twitter.Typeahead.Options = { minLength: 1 };

    var remoteOptions: Twitter.Typeahead.Dataset<MyTimeReport.IProject> = {
        name: "projects",
        display: "Name",
        templates: {
            notFound: $("#notFound-template").html(),
            pending: $("#loading-template").html(),
            suggestion: q => {
                var subNameParts = [
                    q.ClientName,
                    q.PetsCode,
                    q.ProjectManagerName,
                    q.JiraCode
                ]
                    .filter(c => !!c && c.length > 0);

                return `
                <div class="tt-suggestion">
                    <b>${q.Name}</b>
                    <span>${subNameParts.join(" | ")}</span>
                </div>`;
            }
        },
        limit: 5,
        source: (q, cb, acb) => {
            const searchQuery = Service.contextSearchQuery();
            return $.get(
                Globals.resolveUrl(`~/ValuePicker/JsonList?${searchQuery}`), {
                    listId: "TimeTracking.EmployeeProjects",
                    order: "project-short-name",
                    filter: "all-projects",
                    search: q
                })
                .then(acb as ((p: MyTimeReport.IProject[]) => void));
        }
    };

    export class ProjectNameComponent extends React.Component<IProjectNameProps, IProjectNameState> {
        componentWillMount() {
            if (!!this.props.project) {
                this.setState({
                    projectName: this.props.project.ProjectName,
                    isReadOnly: this.props.project.IsReadOnly,
                    apn: this.props.project.Apn || Sheet.Translations.None,
                    kupferwerkProjectId: (this.props.project.KupferwerkProjectId || Sheet.Translations.None).toString()
                });
            } else {
                this.setState({ projectName: '', isReadOnly: false, apn: '', kupferwerkProjectId: '' });
            }
        }

        attachTypehead(element: HTMLInputElement) {
            const $element = $(element);
            $element.val(this.state.projectName);
            let $dropdownMenu: JQuery;
            $element.typeahead(options, remoteOptions)
                .on("typeahead:select", (e, selectedItem: MyTimeReport.IProject) => {

                    if (!!this.props.onChange) {
                        const newName = this.props.onChange(selectedItem.Id, selectedItem.Name);
                        this.setState($.extend({}, this.state, { projectName: newName }));
                        $element.typeahead("val", newName);
                    } else {
                        this.setState($.extend({}, this.state, { projectName: selectedItem.Name }));
                        $element.typeahead("val", selectedItem.Name);
                    }
                })
                .on("typeahead:render", (e) => {
                    const $element = $(e.target);
                    var eOffset = $element.offset();

                    if (eOffset.top + $dropdownMenu.height() > $(window).height()) {
                        $dropdownMenu.css({
                            'display': 'block',
                            'top': eOffset.top + $element.outerHeight() - $dropdownMenu.height(),
                            'left': eOffset.left + $element.outerWidth()
                        });
                    } else {
                        $dropdownMenu.css({
                            'display': 'block',
                            'top': eOffset.top + $element.outerHeight(),
                            'left': eOffset.left
                        });
                    }

                })
                .on('typeahead:open', (e) => {
                    $dropdownMenu = $(e.target).parent().find('.tt-menu');
                    if ($dropdownMenu.length > 0) {
                        $('body').append($dropdownMenu.detach());
                    }
                })
                .on('typeahead:close', (e) => {
                    $(e.target).append($dropdownMenu.detach());
                })
                .blur(() => {
                    $element.typeahead("val", this.state.projectName);
                });
        }

        shouldComponentUpdate(nextProps: IProjectNameProps, nextState: IProjectNameState): boolean {
            return (!!nextProps.project
                && nextProps.project.ProjectId !== (this.props.project as Store.Projects.IProject).ProjectId)
                || nextState.isReadOnly !== this.state.isReadOnly;
        }

        projectDescription(): string {
            return Strings.format(Sheet.Translations.ProjectTooltipTitle, this.state.projectName, this.state.apn, this.state.kupferwerkProjectId);
        }

        render() {
            if (this.state.isReadOnly) {
                return <input
                    type="text"
                    title={this.projectDescription()}
                    spellCheck={false}
                    readOnly={true}
                    value={this.state.projectName}
                />;
            }

            return <input
                type="text"
                title={this.projectDescription()}
                spellCheck={false}
                placeholder={this.props.placeholder}
                readOnly={this.state.isReadOnly}
                ref={this.attachTypehead.bind(this)}
            />;
        }
    }
}