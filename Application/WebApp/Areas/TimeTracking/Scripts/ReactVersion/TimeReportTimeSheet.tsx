﻿namespace TimeTracking.Sheet {
    interface ISheetState {
        days: Store.DateRange.IDay[];
        weeks: Store.DateRange.IWeek[];
        isLoaded: boolean;
        isLoading: boolean;
        isReadOnly: boolean;
        isMobile: boolean;
    }

    export class TimeReportTimeSheet extends React.Component<{}, ISheetState> {
        componentWillMount() {
            var store = Store.defaultStore();
            this.setStoreState();
            store.subscribe(this.setStoreState.bind(this));

            $(window).on('beforeunload', () => {
                const { monitor } = store.getState();

                if (monitor.hasUnsavedChanges && !monitor.changesConfirmed) {
                    return Sheet.Translations.DirtyFormConfirmText;
                }

                return;
            });
        }

        setStoreState(): void {
            
            let { dateRange, projects, toolbar, monitor } = TimeTracking.Store.defaultStore().getState();

            let newState = {
                days: dateRange.days,
                weeks: dateRange.weeks,
                isLoaded: toolbar.isLoaded,
                isLoading: monitor.isLoading,
                isReadOnly: toolbar.isReadOnly,
                isMobile: toolbar.isMobile,
            };
            if (toolbar.isMobile) {
                $(".content.container-fluid").addClass("mobile-version");
            }

            if (!this.state) {
                this.setState(newState);
            } else {
                this.setState($.extend({}, this.state, newState));
            }
        }

        shouldComponentUpdate(nextProps: {}, nextState: ISheetState) {
            const shouldUpdate = this.state.days.length !== nextState.days.length
                || this.state.isLoading !== nextState.isLoading
                || this.state.isReadOnly !== nextState.isReadOnly;
            return shouldUpdate;
        }

        private weekDescription(week: Store.DateRange.IWeek): string {
            return Strings.format(week.WeekDays >= 2 ? Sheet.Translations.WeekHeader : Sheet.Translations.ShortWeekHeader, week.WeekNumber);
        }

        private dayDescription(day: Store.DateRange.IDay): JSX.Element {
            const dayOfMonth = day.Date.getDate();
            const dayOfWeek = day.Date.getDay();

            return (
                <th key={dayOfMonth} className={[...dayCssClasses({ day }), "day-number day-name"].join(" ")}>
                    <span>{dayOfMonth}</span>
                    <OvertimeMarker day={day} />
                    <div>{Sheet.Translations.DayDictionary[dayOfWeek % 7]}</div>
                </th>
            );
        }

        public static loadProjectMetadata(projectId: number, callback?: () => void): JQueryPromise<any> {
            CommonDialogs.pleaseWait.show(20);
            const { toolbar } = Store.defaultStore().getState();
            return $.get(Globals.resolveUrl(`/TimeTracking/MyTimeReport/GetProjectMetadata?employeeId=${toolbar.employeeId}&year=${toolbar.year}&month=${toolbar.month}&projectId=${projectId}`))
                .always(data => { CommonDialogs.pleaseWait.hide(); });
        }

        private addProject(projectId: number, projectName: string): void {

            // Avoid adding already existing project (avoid event too)
            if (Store.defaultStore().getState().projects.projects.some(p => p.ProjectId === projectId)) {
                AlertMessages.addInformation(Sheet.Translations.ProjectAlreadyPresent);
                return;
            }

            TimeReportTimeSheet.loadProjectMetadata(projectId).then((metadata: TimeTrackingReact.IProjectMetadata) => {
                Store.defaultStore().dispatch(Actions.addProject(projectId, projectName, metadata));
            });
        }

        containerItem: HTMLDivElement;

        render() {
            if (this.state.isLoading || !this.state.isLoaded) {
                return <Dante.Ui.ProgressIndicator />                
            }
            
            return (
                <FixedColumnTable enable={!this.state.isMobile} >
                    <table className="time-sheet fixed-table" id="time-sheet">
                        <thead className="time-sheet-header">
                            <tr className="week-number-row time-sheet-row">
                                <th className="week-key-column" colSpan={2}></th>
                                {this.state.weeks.map(w => <th colSpan={w.WeekDays} key={w.WeekNumber} className="week-number">{this.weekDescription(w)}</th>)}
                            </tr>
                            <tr className="day-number-row time-sheet-row">
                                <th className="key-column">{Sheet.Translations.NameColumnHeader}</th>
                                <th className="hours-summary-column">&Sigma;</th>
                                {this.state.days.map(d => this.dayDescription(d))}
                            </tr>
                        </thead>
                        <AdjustedTableBody enabled={!this.state.isMobile}>
                            <ClockCardComponent />
                            <ProjectsComponent />
                        </AdjustedTableBody>
                        <tfoot className="time-sheet-footer">
                            {
                                this.state.isReadOnly ? null :
                                    <tr className="hours-summary-row add-project-row time-sheet-row">
                                        <td className="key-column name project-name">
                                            <ProjectNameComponent
                                                {...{ placeholder: Sheet.Translations.AddProjectPlaceholderText, onChange: (id, name) => { this.addProject(id, name as string); return ""; } }} />
                                            <div className="btn-container">
                                                <div>
                                                    <TaskButton icon="search" onClick={() => {
                                                        ValuePicker.selectSingle("TimeTracking.EmployeeProjects", (record: Grid.IRow) => {
                                                            this.addProject(record.id, record.toString)
                                                        }, url => {
                                                            const searchQuery = Service.contextSearchQuery();

                                                            return `${url}&${searchQuery}`;
                                                        });
                                                    }} />
                                                </div>
                                            </div>
                                        </td>
                                        <td colSpan={this.state.days.length + 1} className=""></td>
                                    </tr>
                            }
                            <tr className="hours-summary-row summary-row time-sheet-row">
                                <td className="key-column name project-name contracted-hours">
                                    <ContractedHours />
                                </td>
                                <td className="hours-sum hours-summary-column"><SummaryTotal /></td>
                                {this.state.days.map(d =>
                                    <td key={d.Date.getDate()} className={[...dayCssClasses({ day: d }), "hours-sum"].join(" ")}>
                                        <SummaryDay day={d.Date} />
                                    </td>)}
                            </tr>
                        </tfoot>
                    </table>
                    <FakeScrolls enable={!this.state.isMobile} />
                </FixedColumnTable>
            );
        }
    }

    export class FixedColumnTable extends React.Component<{ enable: boolean }, { isScrolled: boolean }> {
        private element: HTMLElement;

        private onContainerScroll(): void {
            this.setState({
                isScrolled: this.element.scrollLeft !== 0 && FixedColumnTable.handleScrollInternaly
            });
        }

        public static get handleScrollInternaly(): boolean {
            const $window = $(window);

            return $window.width() > 479 && $window.height() > 319 ;
        }

        public componentWillMount(): void {
            this.setState({ isScrolled: false });
        }

        public render(): JSX.Element {
            if (!this.props.enable) {
                return this.props.children as JSX.Element;
            }
            return (
                <div
                    ref={item => { this.element = item as HTMLElement; }}
                    onScroll={() => { this.onContainerScroll(); }}
                    className={Dante.Utils.ClassNames({
                        'scrolled': this.state.isScrolled
                    }, 'grid-time-sheet')}>
                    {this.props.children}
                </div>
            );
        }
    }
}
