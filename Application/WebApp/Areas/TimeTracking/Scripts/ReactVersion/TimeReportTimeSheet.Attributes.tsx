﻿namespace TimeTracking.Sheet {
    export interface IProjectAttributesProps {
        taskKey: number;
        projectId: number;
    }

    export interface IProjectAttributesState {
        task: Store.Tasks.ITask;
        selectedAttributesValueIds: Array<number | null>;
        attributes: TimeTrackingReact.IProjectAttribute[];
    }

    export class ProjectAttributes extends Sheet.DefaultStoreComponent<IProjectAttributesProps, IProjectAttributesState> {

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            let { projects, tasks } = state;

            let task = tasks.tasks[this.props.taskKey];

            if (!task) {
                return;
            }

            let [project] = projects.projects.filter(p => p.ProjectId === task.ProjectId);

            this.updateState({
                attributes: [...project.Attributes],
                task: task,
                selectedAttributesValueIds: [...task.SelectedAttributesValueIds]
            });
        }

        private selectedAttributeName(index: number): string {
            const attributes = this.state.attributes[index];
            const selectedValue = this.state.selectedAttributesValueIds[index];

            const [selectedAttribute] = attributes.PossibleValues.filter(pv => pv.Id === selectedValue);
            return !!selectedAttribute ? selectedAttribute.Name : this.state.attributes[index].Name;
        }

        private attributeName(index: number): string {
            const attributes = this.state.attributes[index];
            const selectedValue = this.state.selectedAttributesValueIds[index];
            const [selectedAttribute] = attributes.PossibleValues.filter(pv => pv.Id === selectedValue);

            return this.state.attributes[index].Name;
        }

        selectAttribute(index: number, selectedAttributeId: number): void {
            const newAttributeValueIds = this
                .state
                .attributes
                .map((a, i) => i === index
                    ? selectedAttributeId
                    : this.state.task.SelectedAttributesValueIds[i]);
            this.dispatchToStore(Actions.changeAttributes(this.props.taskKey, newAttributeValueIds));
        }

        isAttributeValueSelected(index: number, attributeId: number): boolean {
            const attributes = this.state.attributes[index];
            const selectedValue = this.state.selectedAttributesValueIds[index];

            return selectedValue == attributeId;
        }

        private calculateItemsWidth(item: HTMLDivElement, childSelector: string): number {
            var totalWidth = $(childSelector, item).toArray().reduce((a, b, c) => {
                return a + $(b).outerWidth(true) ;
            }, 5);

            $(item).css({ width: totalWidth, minWidth: totalWidth, maxWidth: totalWidth });
            return totalWidth;
        }

        calculateWidth(item: HTMLDivElement): number {
            return this.calculateItemsWidth(item, ".btn-group");
        }

        calculateReadonlyWidth(item: HTMLDivElement): number {
            return this.calculateItemsWidth(item, "span");
        }

        attributesKey(): string {
            return this.state.attributes.map((a, i) => this.selectedAttributeName(i)).join();
        }

        render() {
            if (this.state.attributes.length == 0) {
                return null;
            }

            if (this.state.task.IsReadOnly || Utils.Enumerate(this.state.task.Days).some(d => d.IsReadOnly)) {
                return (
                    <div className="attributes-container" ref={this.calculateReadonlyWidth.bind(this)} key={this.attributesKey()}>
                        {this.state.attributes.map((a, i) => <span title={this.attributeName(i)} key={a.Id}>{this.selectedAttributeName(i)}</span>)}
                    </div>
                );
            }

            return (
                <div className="attributes-container" ref={this.calculateWidth.bind(this)} key={this.attributesKey()}>
                    {this.state.attributes.map((a, i) =>
                        <div className="btn-group" key={a.Id} >
                            <div className="button-dropdown-content">
                                <button id="3" className="btn btn-combobox btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span className="caret"></span>&nbsp;
                                    <span>{this.selectedAttributeName(i)}</span>
                                </button>
                                <ul id="taskRowProjectAttributeContextMenu" className="dropdown-menu filter-menu" role="menu">
                                    {a.PossibleValues.map(v => (
                                        <li role="presentation" key={v.Id}>
                                            <a className={Utils.ClassNames({ "filter-menu-item project-attribute-variant": true, "radio": this.isAttributeValueSelected(i, v.Id) })} onClick={e => { e.preventDefault(); this.selectAttribute(i, v.Id) }} href="#" role="menuitem">
                                                <span>{v.Name}</span>
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    )}
                </div>
            );
        }
    }
}
