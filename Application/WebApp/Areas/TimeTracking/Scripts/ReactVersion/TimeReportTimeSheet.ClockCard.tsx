﻿namespace TimeTracking.Sheet {
    type ITime = Store.ClockCard.ITime;

    export enum ClockCardRowType {
        In = 0,
        Out = 1,
        Break = 2
    }

    interface IClockCardButtonsContainerProps {
        rowType: ClockCardRowType;
    }

    interface IClockCardButtonsContainerState {
        isReadOnly: boolean;
    }

    interface IClockCardComponentState {
        days: Store.DateRange.IDay[];
        isReadOnly: boolean;
        isRequired: boolean;
    }

    interface IClockCardTimeComponentProps {
        date: Date;
    }

    interface IClockCardTimeComponentState {
        isReadOnly: boolean;
        storedTime: ITime;
        time: string;
        tooltipMessage: string;
    }

    interface IClockCardOutTimeComponentState extends IClockCardTimeComponentState {
        inTimeGreaterThanOutTime: boolean;
    }

    export class ClockCardButtonsContainer extends DefaultStoreComponent<IClockCardButtonsContainerProps, IClockCardButtonsContainerState> {
        calculateTime(): void {
            this.dispatchToStore(Actions.calculateTime(this.props.rowType));
        }

        fillTimeWithLastValue(): void {
            this.dispatchToStore(Actions.fillTime(this.props.rowType));
        }

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { clockCard } = state;

            this.updateState({
                isReadOnly: clockCard.isReadOnly
            });
        }

        render() {
            return (
                <div className="btn-container">
                    {this.state.isReadOnly ? (
                        <div className="clock-card-indicator-icons">
                            <i title={Sheet.Translations.ClockCardRowIsReadOnly} className="fa fa-lock"></i>
                        </div>
                    ) : (
                        <Ui.ButtonDropdown additionalClassName="btn-xs" items={[
                            { url: "#", name: Sheet.Translations.CalculateTimeLabel, action: () => { this.calculateTime(); } },
                            { url: "#", name: Sheet.Translations.FillTimeWithLastValueLabel, action: () => { this.fillTimeWithLastValue(); } }
                        ]} />
                    )}
                </div>
            );
        };
    };

    export class ClockCardComponent extends DefaultStoreComponent<{}, IClockCardComponentState> {
        readonly rows = [
            { rowType: ClockCardRowType.In, label: Sheet.Translations.ClockCardIn },
            { rowType: ClockCardRowType.Out, label: Sheet.Translations.ClockCardOut },
            { rowType: ClockCardRowType.Break, label: Sheet.Translations.ClockCardBreak }
        ];

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { clockCard, dateRange } = state;

            this.updateState({
                days: dateRange.days,
                isReadOnly: clockCard.isReadOnly,
                isRequired: clockCard.isRequired
            });
        }

        getComponent(rowType: ClockCardRowType, day: Store.DateRange.IDay): JSX.Element {
            switch (rowType) {
                case ClockCardRowType.In: {
                    return <ClockCardInTimeComponent date={day.Date} />;
                }

                case ClockCardRowType.Out: {
                    return <ClockCardOutTimeComponent date={day.Date} />;
                }

                case ClockCardRowType.Break: {
                    return <ClockCardBreakDurationComponent date={day.Date} />;
                }
            }
        }

        render() {
            if (!this.state.isRequired) {
                return null;
            }

            let classes = ["clock-card-time"];

            if (this.state.isReadOnly) {
                classes.push("read-only-cell");
            }

            return this.rows.map(r =>
                <tr className="time-sheet-row clock-card-row" key={r.rowType}>
                    <td className="key-column name clock-card-name">
                        <div className="clock-card-name-container">
                            <span className="clock-card-name-label">{r.label}</span>
                            <ClockCardButtonsContainer rowType={r.rowType} />
                        </div>
                    </td>
                    <td className="hours-summary-column hours-sum"></td>
                    {this.state.days.map(d =>
                        <td key={d.Date.getDate()} className={[...dayCssClasses({ day: d }), ...classes].join(" ")}>
                            {this.getComponent(r.rowType, d)}
                        </td>)
                    }
                </tr>
            );
        }
    }

    abstract class ClockCardTimeComponent<S extends IClockCardTimeComponentState> extends DefaultStoreComponent<IClockCardTimeComponentProps, S> {
        abstract rowType: ClockCardRowType;

        formatTime(date: ITime | null): string {
            if (date == null) {
                return "";
            }

            return DateHelper.toTimeString(date as Date);
        }

        abstract getTime(day: Store.ClockCard.IClockCardDay): ITime | undefined;

        hasValidationErrors(): boolean {
            return this.parseTime(this.state.time) === undefined;
        }

        hasValidationWarnings(): boolean {
            return false;
        }

        parseTime(time: string): ITime | undefined | null {
            if (Strings.isNullOrEmpty(time)) {
                return null;
            }

            const timeParts = time.split(":");

            if (timeParts.length > 2) {
                return undefined;
            }

            const [hours, minutes] = timeParts; 
            const formatter = new Intl.NumberFormat(undefined, { minimumIntegerDigits: 2 });

            let result = DateHelper.parseDateTime(this.props.date, `${formatter.format(Number(hours))}:${formatter.format(Number(minutes || "0"))}`);

            if (isNaN(result.getTime())) {
                return undefined;
            }

            return result;
        }

        tryToSetNewTime(): void {
            if (this.state.isReadOnly) {
                return;
            }

            var newTime = this.parseTime(this.state.time);
            
            if (newTime === undefined) {
                newTime = this.state.storedTime;
            }

            this.updateState({
                storedTime: newTime,
                time: this.formatTime(newTime)
            } as S);

            this.dispatchToStore(Actions.changeTime(this.props.date, this.rowType, newTime));
        }

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { clockCard } = state;
            const time = this.getTime(clockCard.days[this.props.date.getDate()]);

            this.updateState({
                isReadOnly: clockCard.isReadOnly,
                storedTime: time,
                time: this.formatTime(time !== undefined ? time : null),
                tooltipMessage: clockCard.isReadOnly ? Sheet.Translations.ReadOnlyHours : null
            } as S);
        }

        render() {
            return (
                <Ui.Tooltip text={this.state.tooltipMessage}>
                    <input
                        type="text"
                        value={this.state.time}
                        readOnly={this.state.isReadOnly}
                        className={Utils.ClassNames({
                            "readonly": this.state.isReadOnly,
                            "validation-error": this.hasValidationErrors(),
                            "validation-warning": this.hasValidationWarnings()
                        })}
                        onBlur={e => { this.tryToSetNewTime(); }}
                        onChange={e => {
                            e.preventDefault();
                            this.updateState({ time: e.target.value } as S);
                        }}
                    />
                </Ui.Tooltip>
            );
        }
    }

    export class ClockCardInTimeComponent extends ClockCardTimeComponent<IClockCardTimeComponentState> {
        rowType = ClockCardRowType.In;

        getTime(day: Store.ClockCard.IClockCardDay): ITime | undefined {
            return day.inTime;
        }
    }

    export class ClockCardOutTimeComponent extends ClockCardTimeComponent<IClockCardOutTimeComponentState> {
        rowType = ClockCardRowType.Out;

        getTime(day: Store.ClockCard.IClockCardDay): ITime | undefined {
            return day.outTime;
        }

        hasValidationWarnings(): boolean {
            return this.state.inTimeGreaterThanOutTime;
        }

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { clockCard } = state;
            const day = clockCard.days[this.props.date.getDate()];
            const outTime = this.getTime(day);
            const inTimeGreaterThanOutTime = day.inTime && outTime && day.inTime >= outTime;

            this.updateState({
                isReadOnly: clockCard.isReadOnly,
                inTimeGreaterThanOutTime: inTimeGreaterThanOutTime,
                storedTime: outTime,
                time: this.formatTime(outTime !== undefined ? outTime : null),
                tooltipMessage: clockCard.isReadOnly ? Sheet.Translations.ReadOnlyHours :
                    inTimeGreaterThanOutTime ? Sheet.Translations.InTimeGreaterThanOutTime : undefined
            });
        }
    }

    export class ClockCardBreakDurationComponent extends ClockCardTimeComponent<IClockCardTimeComponentState> {
        rowType = ClockCardRowType.Break;

        getTime(day: Store.ClockCard.IClockCardDay): ITime | undefined {
            return day.breakDuration;
        }
    }
}
