﻿namespace TimeTracking.Sheet {
    export class TaskIcons extends Sheet.DefaultStoreComponent<{ taskKey: number }, { isReadOnly: boolean, taskStatus: Store.Tasks.TaskStatus, isOvertime: boolean }> {

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { tasks } = state;

            const currentTask = tasks.tasks[this.props.taskKey];
            if (!currentTask) {
                return;
            }

            this.setState(Utils.Assign(this.state, {
                isReadOnly: currentTask.IsReadOnly,
                taskStatus: currentTask.Status,
                isOvertime: currentTask.OvertimeVariant != 0
            }));
        }

        render() {
            return (
                <div className="task-indicator-icons">
                    {this.state.taskStatus == Store.Tasks.TaskStatus.Submitted ? <i title={Sheet.Translations.TimeReportStatusSubmitted} className="task-icon fa fa-hourglass-end"></i> : null}
                    {this.state.isOvertime ? <i title={Sheet.Translations.OvertimeTooltip} className="fa fa-clock-o overtime-indicator"></i> : null}
                    {this.state.taskStatus == Store.Tasks.TaskStatus.Accepted ? <i title={Sheet.Translations.TimeReportStatusDraftAcepted} className="task-icon fa fa-thumbs-o-up"></i> : null}
                    {this.state.isReadOnly ? <i title={Sheet.Translations.TaskIsReadOnly} className="task-icon fa fa-lock"></i> : null}
                </div>
            );
        }
    }
}
