﻿namespace TimeTracking.Sheet {

    export interface ITaskComponentProps {
        task: Store.Tasks.ITask;
        days: Store.DateRange.IDay[];
    }

    export const TaskButton: React.StatelessComponent<any> = (props: { onClick: () => {}, icon: string, className?: string }): JSX.Element => {
        const classes = Dante.Utils.CombineClassNames(["btn", "btn-default", "btn-xs", props.className]);

        return <button className={classes} onClick={props.onClick}>
            <i className={`fa fa-${props.icon}`}></i>
        </button>;
    }

    export const TaskButtonsContainer: React.StatelessComponent<any> = (props: { task: Store.Tasks.ITask }): JSX.Element => {

        const hideButtons = props.task.IsReadOnly || Utils.Enumerate(props.task.Days).some(d => d.IsReadOnly && d.Hours > 0);

        return (
            <div className="btn-container">
                <TaskIcons taskKey={props.task.Key} />
                {hideButtons ? null : <TaskProperties taskKey={props.task.Key} />}
                {hideButtons ? null :
                    <TaskButton icon="trash-o" onClick={() => {
                        CommonDialogs.confirm(Sheet.Translations.ConfirmDeleteTask, Sheet.Translations.ConfirmDialogTitle, a => {
                            if (a.isOk) {
                                Store.defaultStore().dispatch(Actions.removeTask(props.task.Key));
                            }
                        });
                    }}>
                    </TaskButton>
                }
            </div>
        );
    };

    export class TaskRenderComponent extends React.Component<ITaskComponentProps>
    {
        public componentDidMount(): void
        {
            const element = $(ReactDOM.findDOMNode<HTMLElement>(this));

            // element events
            element.focus(() => element.toggleClass("focused", true));
            element.blur(() => element.toggleClass("focused", false));

            // child events
            element.on("focus", "*", () => element.toggleClass("focused", true));
            element.on("blur", "*", () => element.toggleClass("focused", false));
        }

        public render(): JSX.Element
        {
            return (
                <tr className="task-row time-sheet-row" tabIndex={this.props.task.Key}>
                    <td className="key-column name">
                        <div className="task-name-container">
                            <TaskNameComponent task={this.props.task} placeholder={Sheet.Translations.TaskNamePlaceholderText} />
                            <ProjectAttributes taskKey={this.props.task.Key} projectId={this.props.task.ProjectId} />
                            <TaskButtonsContainer task={this.props.task} />
                        </div>
                    </td>
                    <td className="hours-sum hours-summary-column">
                        <SummaryTask taskKey={this.props.task.Key} />
                    </td>
                    {this.props.days.map(d => {
                        const value = this.props.task.Days[d.Date.getDate()];
                        const hours = !!value ? value.Hours : 0;
                        return <td key={d.Date.toString()} className={[
                            ...dayCssClasses({ day: d, task: this.props.task }), "hours"
                        ].join(" ")}>
                            <TaskHoursComponent taskKey={this.props.task.Key} day={d.Date} />
                        </td>;
                    })}
                </tr>
            );
        }
    }

    interface ITaskNameComponentState {
        taskName: string;
        isReadOnly: boolean;
    }

    interface ITaskHoursSetState {
        hours: string;
        storedHours: number;
    }

    interface ITaskHoursComponentState extends ITaskHoursSetState {
        projectId: number;
        overtimeVariant: Sheet.OvertimeVariantEnum;
        isReadOnly: boolean;
    }

    class TaskHoursComponent extends React.Component<{ taskKey: number, day: Date }, ITaskHoursComponentState> {
        storeSubscribers: Redux.Unsubscribe | null = null;

        componentWillMount() {
            this.storeSubscribers = Store.defaultStore().subscribe(this.setStoreState.bind(this));
            this.setStoreState();
        }

        componentWillUnmount() {
            if (this.storeSubscribers != null)
                this.storeSubscribers();

            this.storeSubscribers = null;
        }

        setStoreState() {
            if (!this.storeSubscribers) {
                return;
            }

            const propsDate = this.props.day.getDate();
            const { tasks, projects } = Store.defaultStore().getState();

            const task = tasks.tasks[this.props.taskKey];

            if (!task) {
                return;
            }

            const day = task.Days[propsDate];
            const hours = !!day ? Math.max(day.Hours, 0) : 0;
            const displayHours = hours <= 0 ? "" : hours.toString();
            const project = projects.projects.filter(p => p.ProjectId === task.ProjectId)[0];
            const isReadOnly = task.IsReadOnly
                || (project.ReportingStartsOn != null
                        && project.ReportingStartsOn.getTime() > this.props.day.getTime());

            if (!this.state || this.state.hours !== displayHours || this.state.isReadOnly !== isReadOnly)
            {
                const newState: ITaskHoursComponentState = {
                    hours: displayHours,
                    storedHours: hours,
                    projectId: task.ProjectId,
                    isReadOnly: isReadOnly,
                    overtimeVariant: task.OvertimeVariant
                };

                this.setState($.extend({}, this.state, newState));
            }
        }

        public shouldComponentUpdate(nextProps: { taskKey: number, day: Date }, nextState: ITaskHoursComponentState): boolean {
            const shouldUpdate = this.state.hours !== nextState.hours || this.state.isReadOnly != nextState.isReadOnly;
            return shouldUpdate;
        }

        private parseHourMinuteFormat(hoursAsString: string): number {
            if (hoursAsString === null || hoursAsString === undefined) {
                return NaN;
            }

            var hourElements = hoursAsString.split(':');

            if (hourElements.length != 2) {
                return NaN;
            }

            if (hourElements[1] != null && hourElements[1].length == 1) {
                hourElements[1] += "0";
            }

            var hours = Number(hourElements[0]);
            var minutes = Number(hourElements[1]);

            if (hours === NaN || minutes === NaN) {
                return NaN;
            }

            return hours + Math.round(minutes / 60 * 100) / 100;
        }

        private parseHours(hours: string): number {
            const options: ((hours: string) => number)[] = [
                hours => Number(hours),
                hours => Number(hours.replace(/,/g, '.')),
                hours => Number(hours.replace(/./g, ',')),
                hours => this.parseHourMinuteFormat(hours)
            ];

            let parsedHours = NaN;

            options.forEach(option => {
                const h = option(hours);

                if (!isNaN(h)) {
                    parsedHours = h;
                }
            });

            return parsedHours;
        }

        setHours(hours: string): void {
            if (this.state.isReadOnly) {
                return;
            }

            let newHours = this.parseHours(hours);

            if (!isNaN(newHours)) {
                newHours = Math.max(0, newHours);
                Store.defaultStore().dispatch(Actions.changeHours(this.props.taskKey, this.props.day, this.state.storedHours, newHours));
                this.setState($.extend({}, this.state, { hours, storedHours: newHours }));
            } else {
                this.setState($.extend({}, this.state, { hours }));
            }
        }

        hasValidationErrors(): boolean {
            return this.state.storedHours > 24
                || this.state.storedHours < 0;
        }

        render() {
            return (
                <Ui.Tooltip text={this.state.isReadOnly ? Sheet.Translations.ReadOnlyHours : null}>
                    <input
                        type="text"
                        className={Utils.ClassNames({
                            "validation-error": this.hasValidationErrors(),
                            "readonly": this.state.isReadOnly
                        })}
                        readOnly={this.state.isReadOnly}
                        value={this.state.hours}
                        onBlur={() => {
                            this.setState($.extend({}, this.state, { hours: this.state.storedHours === 0 ? "" : this.state.storedHours.toString() }));
                        }}
                        onChange={(e) => {
                            e.preventDefault();
                            this.setHours(e.target.value);
                        }} />
                </Ui.Tooltip>
            );
        }
    }

    export class TaskNameComponent extends React.Component<{ task: Store.Tasks.ITask, placeholder: string }, ITaskNameComponentState> {
        storeSubscribers: Redux.Unsubscribe | null;

        componentWillMount() {
            this.setState({ taskName: this.props.task.Name, isReadOnly: this.props.task.IsReadOnly });
            this.storeSubscribers = Store.defaultStore().subscribe(this.setStoreState.bind(this));
        }

        public componentDidMount()
        {
            if (!this.state.taskName || this.state.taskName.length === 0)
            {
                const element = ReactDOM.findDOMNode(this) as HTMLElement;
                Events.components().dispatch(Actions.scrollTo(element));
                element.focus();
            }
        }

        componentWillUnmount() {
            if (this.storeSubscribers != null)
                this.storeSubscribers();

            this.storeSubscribers = null;
        }

        setStoreState() {
            if (!this.storeSubscribers) {
                return;
            }

            const { tasks } = Store.defaultStore().getState();
            const task = tasks.tasks[this.props.task.Key];
            const isReadOnly = task.IsReadOnly || Utils.Enumerate(task.Days).some(d => d.IsReadOnly && d.Hours > 0);

            if (!!task) {
                this.setState($.extend({}, this.state, { taskName: task.Name, isReadOnly: isReadOnly }));
            }
        }

        shouldComponentUpdate(nextProps: { task: Store.Tasks.ITask }, nextState: ITaskNameComponentState) {
            return this.state.taskName !== nextState.taskName || this.state.isReadOnly !== nextState.isReadOnly;
        }

        setTaskName(taskName: string): void {
            taskName = this.cleanupTaskName(taskName);
            this.setState($.extend({}, this.state, { taskName }));
            Store.defaultStore().dispatch(Actions.changeTaskName(this.props.task.Key, taskName));
        }

        cleanupTaskName(taskName: string) {
            var newVal = (taskName || "").replace(/[^a-z0-9äéöüßąćęłńóśźż\s,.:\'\"\*\-\+\/\\]+/gi, "");
            return newVal;
        }

        render() {
            return <input
                type="text"
                className="name"
                value={this.state.taskName}
                title={this.state.taskName}
                readOnly={this.state.isReadOnly}
                placeholder={this.props.placeholder}
                onChange={(e) => {
                    e.preventDefault();
                    this.setTaskName(e.target.value);
                }} />;
        }
    }
}
