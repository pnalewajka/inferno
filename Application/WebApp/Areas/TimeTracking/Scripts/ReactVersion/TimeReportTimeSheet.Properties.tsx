﻿namespace TimeTracking.Sheet {

    export enum OvertimeVariantEnum {
        Regular = 0 ,
        NormalOvertime = 1 << 1,
        LateNightOvertime = 1 << 2,
        WeekendOvertime = 1 << 3,
        HolidayOvertime = 1 << 4,
        OvertimeBank = 1 << 5
    }

    interface IOvertimeOption {
        value: number;
        enumValue: OvertimeVariantEnum;
        name: string;
    }

    interface ITaskPropertiesState {
        task: Store.Tasks.ITask,
        overtimeOptions: IOvertimeOption[]
    }

    export const filterAllowedOvertimeOptions = (allowedEmployeeOvertime: OvertimeVariantEnum, allowedProjectOvertime: OvertimeVariantEnum): IOvertimeOption[] => {
        const allOvertimeOptions: IOvertimeOption[] = [
            { value: 1, enumValue: OvertimeVariantEnum.NormalOvertime, name: Sheet.Translations.HourlyRateTypeOverTimeNormal },
            { value: 2, enumValue: OvertimeVariantEnum.LateNightOvertime, name: Sheet.Translations.HourlyRateTypeOverTimeLateNight },
            { value: 3, enumValue: OvertimeVariantEnum.WeekendOvertime, name: Sheet.Translations.HourlyRateTypeOverTimeWeekend },
            { value: 4, enumValue: OvertimeVariantEnum.HolidayOvertime, name: Sheet.Translations.HourlyRateTypeOverTimeHoliday },
            { value: 5, enumValue: OvertimeVariantEnum.OvertimeBank, name: Sheet.Translations.HourlyRateTypeOverTimeBank },
        ];

        return allOvertimeOptions.filter(o => allowedEmployeeOvertime & allowedProjectOvertime & o.enumValue);
    }

    export class TaskProperties extends DefaultStoreComponent<{ taskKey: number }, ITaskPropertiesState> {

        updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const regularOvertimeOption: IOvertimeOption = { value: 0, enumValue: OvertimeVariantEnum.Regular, name: Sheet.Translations.HourlyRateTypeRegular };
 
            const { tasks: { tasks }, toolbar: { allowedOvertime }, projects: { projects } } = state;
 
            const task = tasks[this.props.taskKey];

            if (!task) {
                return;
            }
            const [ project ] = projects.filter(p => p.ProjectId === task.ProjectId);

            if (!project) {
                return;
            }

            this.updateState({
                task,
                overtimeOptions: [regularOvertimeOption, ...filterAllowedOvertimeOptions(allowedOvertime, project.AllowedProjectOvertime)]
            });
        }

        changeOvertimeVariant(task: Store.Tasks.ITask, variant: OvertimeVariantEnum): void {
            if (task.OvertimeVariant === variant) {
                return;
            }

            super.dispatchToStore(Actions.changeOvertimeVariant(task.Key, variant));
        }

        fillContractedHours(task: Store.Tasks.ITask): void {
            
            super.dispatchToStore(Actions.fillContractedHours(task.Key));
        }

        cloneTask(task: Store.Tasks.ITask): void {
            super.dispatchToStore(Actions.cloneTask(task.Key));
        }

        splitOvertime(task: Store.Tasks.ITask): void {
            super.dispatchToStore(Actions.splitOvertime(task.Key));
        }
        
        render() {
            return (
                <div className="btn-group">
                    <button className="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" type="button">
                        <span className="caret"></span>
                    </button>
                    <ul id="taskRowContextMenu" className="dropdown-menu filter-menu" role="menu">
                        <li className="dropdown-header" role="presentation">{Sheet.Translations.EntryTypeHeader}</li>
                        {this.state.overtimeOptions.map(o => {
                            return (
                                <li role="presentation" key={o.value}>
                                    <a
                                        onClick={e => { e.preventDefault(); this.changeOvertimeVariant(this.state.task, o.value); }}
                                        className={`filter-menu-item overtime-variant ${!!this.state.task && this.state.task.OvertimeVariant === o.value ? "radio" : ""}`}
                                        href="#"
                                        role="menuitem">
                                        <span>{o.name}</span>
                                    </a>
                                </li>
                            );
                        })}

                        <li className="divider"></li>

                        <li role="presentation">
                            <a className="clone-task" href="#" onClick={e => { e.preventDefault(); this.cloneTask(this.state.task); }} >{Sheet.Translations.Clone}</a>

                        </li>

                        <li role="presentation">
                            <a className="split-overtime" href="#" onClick={e => { e.preventDefault(); this.splitOvertime(this.state.task); }} >{Sheet.Translations.SplitOvertime}</a>

                        </li>

                        <li role="presentation">
                            <a className="fill-contracted-hours" href="#" onClick={e => { e.preventDefault(); this.fillContractedHours(this.state.task); }} >{Sheet.Translations.FillContractedHoursLabel}</a>
                        </li>
                    </ul>
                </div>
            );
        }
    };
}
