﻿namespace TimeTracking.Sheet {
    enum MarkerTypeEnum {
        Hide = 0,
        NotMarked = 1,
        NoApprovals = 2,
        Overtime = 3,
        NoContractedHours = 4,
    }

    export class OvertimeMarker extends React.Component<{ day: Store.DateRange.IDay }, { markerType: MarkerTypeEnum }> {

        constructor() {
            super();
            this.state = { markerType: MarkerTypeEnum.Hide };
        }

        storeSubscribers: Redux.Unsubscribe | null = null;

        componentWillMount() {
            this.setState({ markerType: MarkerTypeEnum.Hide });
            this.storeSubscribers = Store.defaultStore().subscribe(this.setStoreState.bind(this));
            this.setStoreState();
        }

        componentWillUnmount() {
            if (this.storeSubscribers != null)
                this.storeSubscribers();
            this.storeSubscribers = null;
        }

        shouldComponentUpdate(nextProps: { day: Store.DateRange.IDay }, nextState: { markerType: MarkerTypeEnum }) {
            return nextState.markerType !== this.state.markerType;
        }

        setStoreState(): void {
            if (!this.storeSubscribers) {
                return;
            }

            const date = this.props.day.Date.getDate();
            const { summaries, overtimes } = Store.defaultStore().getState();
            const { summaryByDate, summaryOvertimeByDate } = summaries;
            const overtimeHoursForDate = summaryOvertimeByDate[date] || 0;
            const hoursForDate = summaryByDate[date] || 0;

            const approvers = overtimes.overtimes.filter(f => f.DateFrom.getDate() <= date && date <= f.DateTo.getDate());
            if (hoursForDate > 0 && this.props.day.ContractedHours === 0) {
                this.setState({ markerType: MarkerTypeEnum.NoContractedHours });
            } else if (!!overtimeHoursForDate && approvers.length === 0) {
                this.setState({ markerType: MarkerTypeEnum.NoApprovals });
            } else if (overtimeHoursForDate > 0) {
                this.setState({ markerType: MarkerTypeEnum.Overtime });
            } else if (hoursForDate > this.props.day.ContractedHours) {
                this.setState({ markerType: MarkerTypeEnum.NotMarked });
            }
            else {
                this.setState({ markerType: MarkerTypeEnum.Hide });
            }
        }

        private markerClass(markerType: MarkerTypeEnum): string {
            switch (markerType) {
                case MarkerTypeEnum.NotMarked:
                case MarkerTypeEnum.NoContractedHours:
                    return "ot-not-marked";
                case MarkerTypeEnum.NoApprovals:
                    return "ot-no-approvals";
                case MarkerTypeEnum.Overtime:
                    return "ot";
                default:
                    return "";
            }
        }

        private markerTitle(markerType: MarkerTypeEnum): string {
            switch (markerType) {
                case MarkerTypeEnum.NotMarked:
                    return Sheet.Translations.OvertimeNotMarkedMessage;
                case MarkerTypeEnum.NoApprovals:
                    return Sheet.Translations.OvertimeNotMarkedMessage;
                case MarkerTypeEnum.Overtime:
                    return Sheet.Translations.OvertimeTooltip;
                case MarkerTypeEnum.NoContractedHours:
                    return Sheet.Translations.NoHoursContractedMessage;
                default:
                    return "";
            }
        }

        render() {
            if (this.state.markerType === MarkerTypeEnum.Hide) {
                return null;
            }

            return (
                <Ui.Tooltip text={this.markerTitle(this.state.markerType)} className={"tt-marker"}>
                    <i className={`fa fa-clock-o ${this.markerClass(this.state.markerType)}`}></i>
                </Ui.Tooltip>
            );
        }
    }
}
