﻿namespace TimeTracking.Sheet {

    export let Translations: TimeTrackingReact.ITranslations;

    export function Initialize(urls: any, translations: TimeTrackingReact.ITranslations): void {
        Translations = translations;
        Service.urls = urls;

        ReactDOM.render(
            <TimeReportTimeSheet />,
            document.getElementById("time-tracking-component")
        );
        ReactDOM.render(
            <Header.TimeReportHeader />,
            document.getElementById("time-tracking-report-header")
        );

        const breadcrumbElement = document.getElementsByClassName("breadcrumb")[0];
        if (!!breadcrumbElement) {
            ReactDOM.render(
                <Header.Breadcrumb />,
                breadcrumbElement
            );
        }

        Ui.handleDropdownPosition();

        let store = TimeTracking.Store.defaultStore();

        Service.loadReport()
            .then(reportData => {
                const action = TimeTracking.Actions.loadData(reportData);
                store.dispatch(action);
                Help.initResources($('body'), false);
            });
    }

    export function dayCssClasses(input: { task?: Store.Tasks.ITask, day: Store.DateRange.IDay }): string[] {
        let cssArray: string[] = [];

        if (input.day.IsWeekend) {
            cssArray.push("weekend");
        }

        if (input.day.IsHoliday) {
            cssArray.push("holiday");
        }

        if (!input.day.IsWeekend && !input.day.IsHoliday) {
            cssArray.push("working-day");
        }

        if (input.day.IsLastDayOfWeek) {
            cssArray.push("end-of-week");
        }

        if (!!input.task && input.task.IsReadOnly) {
            cssArray.push("read-only-cell");
        }

        return cssArray;
    }

    export abstract class StoreComponent<R, P, S> extends React.Component<P, S> {
        private storeUnsubscriber: Redux.Unsubscribe | null = null;

        constructor(protected store: Redux.Store<R>) {
            super();
            this.state = {} as S;
        }

        componentWillMount() {
            this.storeUnsubscriber = this.store.subscribe(this.reciveStateFromStore.bind(this));
            this.reciveStateFromStore();
        }

        componentWillUnmount() {
            if (this.storeUnsubscriber != null) {
                this.storeUnsubscriber();
            }

            this.storeUnsubscriber = null;
        }

        private reciveStateFromStore(): void {
            if (!!this.storeUnsubscriber) {
                this.updateFromStore(this.store.getState());
            }
        }

        abstract updateFromStore(state: R): void;

        updateState(nextStatePartial: Partial<S>): void {
            const newState = Utils.Assign(this.state, nextStatePartial);

            if (this.stateChanged(newState)) {
                this.setState(newState);
            }
        }

        shouldComponentUpdate(nextProps: P, nextState: S): boolean {
            return this.propsChanged(nextProps) || this.stateChanged(nextState);
        }

        propsChanged(nextProps: P): boolean {
            return JSON.stringify(nextProps) != JSON.stringify(this.props);
        }

        stateChanged(nextState: S): boolean {
            return JSON.stringify(nextState) != JSON.stringify(this.state);
        }

        dispatchToStore(action: Redux.Action): void {
            this.store.dispatch(action);
        }
    }

    export abstract class DefaultStoreComponent<P, S> extends StoreComponent<Store.ITimeTrackingStoreState, P, S> {
        constructor() {
            super(Store.defaultStore());
        }
    }
}
