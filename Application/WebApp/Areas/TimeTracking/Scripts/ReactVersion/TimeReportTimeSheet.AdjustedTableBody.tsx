﻿namespace TimeTracking.Sheet {

    interface IAdjustedTableState {
        width: number;
        height: number;
        positionTop: number;
        containerHeight: number;
        contentWidth: number;
    }

    export class AdjustedTableBody extends React.Component<{ enabled: boolean }, IAdjustedTableState>{
        render() {
            return (
                <tbody className="time-sheet-body" ref={this.storeTable.bind(this)}>
                    {this.props.children}
                </tbody>
            );
        }

        tableElement: JQuery;
        storeTable(tableElement: HTMLTableSectionElement): JQuery {
            this.tableElement = $(tableElement);
            return this.tableElement;
        }

        componentWillMount() {
            const debounce = Dante.Utils.Debounce(() => {
                this.recalculateViewValues();
            });

            window.addEventListener("resize", debounce);

            Store.defaultStore().subscribe(debounce);

            debounce();
        }

        staticWidth(width: number, minWidth: number = 270) {
            if (width < minWidth) {
                width = minWidth;
            }

            return {
                width: width, maxWidth: width, minWidth: width
            };
        }

        bodyHeight(): number {
            const $table = this.tableElement.parents(".time-sheet");
            const $header = $table.find(".time-sheet-header");
            const $footer = $table.find(".time-sheet-footer");
            const $body = $table.find(".time-sheet-body");
            const $container = $table.parents(".content-section");
            return $container.height() - $header.height() - $footer.height();
        }

        recalculateViewValues() {
            const keyColumnWidth = AdjustedTableBody.getKeyColumnWidth();
            const tableWidth = this.getTableWidth();

            if (this.props.enabled) {
                const bodyHeight = this.bodyHeight();
                this.tableElement
                    .css({ maxHeight: bodyHeight - 20 });
            } else {
                this.tableElement
                    .css({ maxHeight: "auto" });
            }
            const $table = this.tableElement.parents(".time-sheet");

            if (tableWidth != null) {
                $table.width(tableWidth + 15);
            }

            $(".time-sheet-header .time-sheet-row", $table).find(".key-column, .week-key-column")
                .css(this.staticWidth(keyColumnWidth));

            $(".time-sheet-footer .time-sheet-row .key-column", $table)
                .css(this.staticWidth(keyColumnWidth));
        }

        getTableWidth(): number | null {
            let sum = 0;

            $("tr:first td", this.tableElement).each((a, b) => { sum += $(b).outerWidth() });

            return sum <= 0 ? null : sum;
        }

        static getKeyColumnWidth(): number {
            let maxWidth = 0;
            const $table = $(".time-sheet");

            $(".time-sheet-body .time-sheet-row .key-column", $table)
                .each((index: number, elem: Element) => {
                    const width = $(elem).outerWidth();
                    if (width > maxWidth) {
                        maxWidth = width;
                    }
                });

            if (!maxWidth) {
                return $(".time-sheet-footer .time-sheet-row .key-column", $table).width();
            }

            return maxWidth;
        }
    }

    export interface IPoint {
        x?: number;
        y?: number;
    }

    export interface IProps {
        enable: boolean
    }

    export interface IState {
        width: number,
        height: number,
        contentWidth: number,
        contentHeight: number,
        handleInternally: boolean
    }

    export class FakeScrolls extends React.Component<IProps, IState> {
        componentWillMount() {
            this.setState({ width: 0, height: 0, contentWidth: 0, contentHeight: 0, handleInternally: FixedColumnTable.handleScrollInternaly });

            this.$window = $(window).on("resize", this.storeOnSubscribeDebounce);

            Store.defaultStore().subscribe(this.storeOnSubscribeDebounce);
            Events.components().subscribe(e => this.handleComponentEvent(e)); // maintain scope

            this.storeOnSubscribeDebounce();
        }

        public handleComponentEvent(event: Actions.ITimeTrackingAction) {
            if (this.$componentElement == null) {
                return;
            }

            if (event.type === Actions.TimeTrackingActionType.ScrollTo) {
                const containerRect = this.$tbody.get(0).getBoundingClientRect();
                const element = (event.data as Actions.IScrollToActionData).element;

                const rect = this.getAbsoluteRect(element);
                const top = rect.top - containerRect.top;
                const moveToBottom = rect.height - containerRect.height + 5;

                this.setScroll({
                    y: top + moveToBottom, // margin
                });
            }
        }

        public getAbsoluteRect(element: HTMLElement): ClientRect {

            const scroll = {
                x: this.$componentElement == null ? 0 : this.$componentElement.scrollLeft(),
                y: this.$componentElement == null ? 0 : this.$componentElement.scrollTop(),
            };

            this.setScroll({ x: 0, y: 0 }); // no scroll
            const elementRect = element.getBoundingClientRect(); // get absolute bounds
            this.setScroll(scroll); // restore scroll

            return elementRect;
        }

        public setScroll(point: IPoint): void {
            if (this.$componentElement == null) {
                return;
            }

            if (point.x !== undefined) {
                this.$componentElement.scrollLeft(point.x);
            }

            if (point.y !== undefined) {
                this.$componentElement.scrollTop(point.y);
            }

            this.recalculateScrolls();
            this.onContainerScroll();
        }

        storeOnSubscribeDebounce = Dante.Utils.Debounce(() => {
            this.setState(Dante.Utils.Assign(this.state, { handleInternally: FixedColumnTable.handleScrollInternaly }));

            if (this.$componentElement == null) {
                return;
            }

            if (!this.$tbody || this.$tbody.length === 0) {
                this.$tbody = $("#time-sheet tbody");

                this.$containerElement.bind("mousewheel DOMMouseScroll", (event) => {
                    if (this.$componentElement == null) {
                        return;
                    }

                    const scrollStep = 60;
                    const old = this.$componentElement.scrollTop();
                    const direction = ((event.originalEvent as any).wheelDelta || (event.originalEvent as any).detail * -1) < 0 ? scrollStep : -scrollStep;
                    this.$componentElement.scrollTop(old + direction);
                });

                this.$containerElement.bind("scroll", () => {
                    this.$containerElement.scrollLeft(this.$containerElement.scrollLeft());
                });

                this.$tbody.bind("scroll", () => {
                    this.$containerElement.scrollTop(this.$tbody.scrollTop());
                });
            }

            this.recalculateScrolls();
            this.onContainerScroll();
        }, 5);


        recalculateSize() {

            const cellWidth = 40;
            const cellHeight = 30;

            const width = this.$containerElement.width() - AdjustedTableBody.getKeyColumnWidth() + 15 - cellWidth;
            const height = this.$window.height() - this.$tbody.offset().top - cellHeight;
            const $timeSheet = $("#time-sheet");
            const $timeSheetContainer = $timeSheet.parents(".content.container-fluid");

            const widthDifference = (this.$tbody.find('tr:first td').length * cellWidth + AdjustedTableBody.getKeyColumnWidth()) - $timeSheetContainer.width() - cellHeight;
            const heightDifference = this.$tbody.find('tr').length * (cellHeight + 2) - this.$tbody.height() - cellHeight;

            const newSize = {
                height,
                width,
                contentWidth: width + widthDifference,
                contentHeight: height + heightDifference,
            };

            return newSize;
        }

        recalculateScrolls() {
            this.setState(this.recalculateSize());
        }

        $window: JQuery;
        $componentElement: JQuery | null;
        $containerElement: JQuery;
        $tbody: JQuery;

        onContainerScroll() {
            if (this.$componentElement == null) {
                return;
            }

            const scrollOffset = {
                top: this.$componentElement.scrollTop(),
                left: this.$componentElement.scrollLeft()
            };

            this.$tbody.scrollTop(scrollOffset.top);
            this.$containerElement.scrollLeft(scrollOffset.left);
            this.recalculateScrollValues($("table"), scrollOffset.left);
        }

        recalculateScrollValues($table: JQuery, scrollLeft: number) {
            $(".key-column, .week-key-column, .hours-summary-column", $table).css("transform", `translate(${scrollLeft}px, 0)`);
        }

        shouldComponentUpdate(nextProps: IProps, nextState: IState) {
            return this.state.height != nextState.height
                || this.state.width != nextState.width
                || this.state.contentWidth != nextState.contentWidth
                || this.state.contentHeight != nextState.contentHeight
                || this.state.handleInternally != nextState.handleInternally
                || this.props.enable != nextProps.enable;
        }

        render() {
            if (!this.state || !this.state.handleInternally || !this.props.enable) {
                return null;
            }

            return <div
                key={this.state.handleInternally.toString()}
                className="scrollbar-container"
                style={{ width: this.state.width, height: this.state.height }}>
                <div
                    ref={(element) => {
                        if (element == null) {
                            this.$componentElement = null;
                        } else {
                            this.$componentElement = $(element as HTMLElement);
                            this.$containerElement = this.$componentElement.parents(".content.container-fluid");
                            this.storeOnSubscribeDebounce();
                        }
                    }}
                    onScroll={Dante.Utils.Debounce(this.onContainerScroll.bind(this))}
                    className="scrollbar-container-body" style={{ width: this.state.width, height: this.state.height }}>
                    <div style={{ width: this.state.contentWidth, height: this.state.contentHeight }}></div>
                </div>
            </div>;
        }
    }
}
