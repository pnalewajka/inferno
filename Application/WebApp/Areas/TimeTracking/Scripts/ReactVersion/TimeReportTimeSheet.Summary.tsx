﻿namespace TimeTracking.Sheet {
    interface ISummaryHoursState {
        value: number;
        showWarning: boolean;
        tooltipMessage: string | null;
    }

    abstract class SummaryBaseComponent<P> extends DefaultStoreComponent<P, ISummaryHoursState> {
        protected get defaultDisplayValue(): string {
            return "-";
        }

        constructor() {
            super();
            this.state = { value: 0, showWarning: false, tooltipMessage: null };
        }

        public render() {
            return (
                <Ui.Tooltip text={this.state.tooltipMessage}>
                    <span className={Utils.ClassNames({ "validation-warning": this.state.showWarning })}>
                        {this.state.value === 0 || isNaN(this.state.value) ? this.defaultDisplayValue : Dante.Utils.DisplayNumber(this.state.value)}
                    </span>
                </Ui.Tooltip>
            );
        }
    }

    export class SummaryProjectAndDay extends SummaryBaseComponent<{ day: Date, projectId: number }> {
        public updateFromStore(state: Store.ITimeTrackingStoreState): void {
            let { summaries } = state;

            const date = this.props.day.getDate();
            const value = (summaries.summaryByProjectAndDate[this.props.projectId] || [])[date];

            this.updateState({ value });
        }
    }

    export class SummaryProject extends SummaryBaseComponent<{ projectId: number }> {
        public updateFromStore(state: Store.ITimeTrackingStoreState): void {
            let { summaries } = state;

            let value = summaries.summaryByProject[this.props.projectId];

            this.updateState({ value });
        }
    }

    export class SummaryDay extends SummaryBaseComponent<{ day: Date }> {

        private isExceedingWorkingLimit(hours: number, limit?: number): boolean {
            return limit != null && hours > limit;
        }

        private isClockCardIncompatibleWithTimeReport(hours: number, day: Store.ClockCard.IClockCardDay): boolean {
            if (!day.inTime || !day.outTime || !day.breakDuration) {
                return false;
            }

            var outTime = Date.UTC(1970, 0, 1, day.outTime.getUTCHours(), day.outTime.getUTCMinutes());
            var inTime = Date.UTC(1970, 0, 1, day.inTime.getUTCHours(), day.inTime.getUTCMinutes());
            var breakDuration = Date.UTC(1970, 0, 1, day.breakDuration.getUTCHours(), day.breakDuration.getUTCMinutes());

            var result = (outTime - inTime - breakDuration) / 1000 / 60;

            return Math.abs(result - hours * 60) >= 1.0;
        }

        private getTooltipMessage(isExceedingWorkingLimit: boolean, isClockCardIncompatibleWithTimeReport: boolean, reportHoursOnAbsenceDay: boolean): string {
            var options = [
                { visible: isExceedingWorkingLimit, message: Sheet.Translations.WorkingLimitExceeded },
                { visible: isClockCardIncompatibleWithTimeReport, message: Sheet.Translations.ClockCardIncompatibleWithTimeReport },
                { visible: reportHoursOnAbsenceDay, message: Sheet.Translations.ReportedHoursOnAbsenceDay }
            ];

            return options.filter(o => o.visible).map(o => o.message).join("; ");
        }

        public updateFromStore(state: Store.ITimeTrackingStoreState): void {
            let { summaries, dateRange, clockCard } = state;

            const date = this.props.day.getDate();
            const dateSummary = summaries.summaryByDate[date] || 0;
            const absenceSummary = summaries.summaryAbsenceByDate[date] || 0;
            const workingLimitSummary = summaries.summaryNotPassiveWorkingHoursByDate[date];
            const dayDetails = dateRange.days[date];
            const isExceedingWorkingLimit = this.isExceedingWorkingLimit(workingLimitSummary || 0, !!dayDetails ? dayDetails.DailyWorkingLimit : undefined);
            const isClockCardIncompatibleWithTimeReport = this.isClockCardIncompatibleWithTimeReport(dateSummary - absenceSummary, clockCard.days[date]);

            const reportHoursOnAbsenceDay = dateSummary != absenceSummary && absenceSummary != 0;

            this.updateState({
                value: !!dateSummary ? dateSummary : 0,
                showWarning: isExceedingWorkingLimit || isClockCardIncompatibleWithTimeReport || reportHoursOnAbsenceDay,
                tooltipMessage: this.getTooltipMessage(isExceedingWorkingLimit, isClockCardIncompatibleWithTimeReport, reportHoursOnAbsenceDay)
            });
        }
    }

    export class SummaryTask extends SummaryBaseComponent<{ taskKey: number }> {
        public updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { summaries } = state;
            const value = summaries.summaryByTask[this.props.taskKey];

            this.updateState({ value });
        }
    }

    export class SummaryTotal extends SummaryBaseComponent<{}> {
        public updateFromStore(state: Store.ITimeTrackingStoreState): void {
            const { summaries } = state;

            this.updateState({ value: summaries.summary });
        }
    }
}
