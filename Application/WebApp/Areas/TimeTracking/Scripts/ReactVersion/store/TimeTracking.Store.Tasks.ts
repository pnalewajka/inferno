﻿module TimeTracking.Store.Tasks {

    export enum TaskStatus {
        NotStarted = -1,
        Draft = 0,
        Submitted = 1,
        Accepted = 2,
        Rejected = 3
    }

    export interface ITaskDictionary {
        [index: number]: ITask;
    }

    export interface IDayDictionary {
        [day: number]: ITaskDay
    }

    export interface ITaskDay {
        Day: Date;
        Hours: number;
        IsReadOnly: boolean;
    }

    export interface ITask {
        Id?: number;
        Key: number;
        ProjectId: number;
        AbsenceTypeId?: number;
        IsReadOnly: boolean;
        Name: string;
        OvertimeVariant: Sheet.OvertimeVariantEnum;
        Days: IDayDictionary;
        Status: TaskStatus;
        ImportSourceId?: number;
        ImportedTaskCode?: string;
        ImportedTaskType?: string;
        SelectedAttributesValueIds: Array<number | null>;
    }

    export interface ITasksStoreState {
        tasks: ITaskDictionary;
        taskCounter: number;
        allowedEmployeeOvertime: Sheet.OvertimeVariantEnum;
    }

    function taskDayHours(dates: Date[], days?: TimeTrackingReact.ITimeTrackingTaskDay[]): ITaskDay[] {
        const taskDaysDictionary: Store.Summaries.IDictionary<number, TimeTrackingReact.ITimeTrackingTaskDay> = [];

        if (!!days) {
            for (var i = 0; i < days.length; i++) {
                taskDaysDictionary[Utils.withoutTimezoneOffset(days[i].Day).getDate()] = days[i];
            }
        }

        let taskDates: ITaskDay[] = [];

        dates.forEach(d => {
            const item = taskDaysDictionary[d.getDate()];
            let taskDay: ITaskDay = {
                Day: d,
                Hours: !!item ? item.Hours : 0,
                IsReadOnly: !!item ? item.IsReadOnly == true : false
            };
            taskDates[d.getDate()] = taskDay;
        });

        return taskDates;
    }

    function loadTasks(viewModel: TimeTrackingReact.ITimeTrackingViewModel): ITasksStoreState {
        let tasks: ITask[] = [];
        let taskCounter: number = 1;

        viewModel.Projects.forEach(project => {
            project.Tasks.forEach(task => {
                const key = taskCounter++;

                let newTask: ITask = {
                    Id: task.Id,
                    Key: key,
                    ProjectId: project.ProjectId,
                    IsReadOnly: task.IsReadOnly == true,
                    Name: task.Name,
                    Status: task.Status as TaskStatus,
                    ImportSourceId: task.ImportSourceId,
                    ImportedTaskCode: task.ImportedTaskCode,
                    ImportedTaskType: task.ImportedTaskType,
                    OvertimeVariant: task.OvertimeVariant,
                    Days: taskDayHours(viewModel.Days.map(d => Utils.withoutTimezoneOffset(d.Date)), task.Days),
                    SelectedAttributesValueIds: project.Attributes.map((m, i) => {
                        let selectedValaue = task.SelectedAttributesValueIds[i];
                        if (selectedValaue !== null) {
                            return selectedValaue;
                        }

                        return m.DefaultValueId || null;
                    }) 
                };
                tasks[key] = newTask;
            });
        });

        return {
            tasks, taskCounter, allowedEmployeeOvertime: viewModel.AllowedOvertime
        };
    }

    function changeName(state: ITasksStoreState, key: number, newName: string): ITasksStoreState {
        state.tasks[key] = Utils.Assign(state.tasks[key], { Name: newName });

        return state;
    }

    function changeHours(state: ITasksStoreState, parameters: Actions.IChangeHoursActionData): ITasksStoreState {
        const date = parameters.date.getDate();

        state.tasks[parameters.task.Key].Days[date] = Utils.Assign(state.tasks[parameters.task.Key].Days[date], { Hours: parameters.newHours, Day: parameters.date });

        return state;
    }

    function updateTasks(state: ITasksStoreState, taskKey: number, taskPartial: any): ITasksStoreState {
        state.tasks[taskKey] = Utils.Assign(state.tasks[taskKey], taskPartial);

        return state;
    }

    function cloneTask(state: ITasksStoreState, task: ITask): ITasksStoreState {
        let newState = Utils.Clone(state);
        newState.taskCounter++;

        const clonedTask = Utils.Clone(newState.tasks[task.Key]);
        clonedTask.Key = newState.taskCounter;
        clonedTask.Id = undefined;

        newState.tasks[newState.taskCounter] = clonedTask;

        return newState;
    }

    function fillHours(state: ITasksStoreState, project: Store.Projects.IProject, task: ITask, days: Store.DateRange.IDay[]): ITasksStoreState {

        if (task.IsReadOnly) {
            return state;
        }

        const contractedDays: ITaskDay[] = [];

        // Copy old days
        days.forEach(d => {
            const taskDay = task.Days[d.Date.getDate()];
            contractedDays[d.Date.getDate()] = {
                Day: d.Date, Hours: taskDay ? taskDay.Hours : 0, IsReadOnly: taskDay ? taskDay.IsReadOnly : d.IsReadOnly
            }
        });

        // Set contracted hours for NOT READONLY days
        days.filter(d => !d.IsReadOnly
            && (project.ReportingStartsOn == null
                || project.ReportingStartsOn.getTime() <= d.Date.getTime())).forEach(d =>
        {
            const taskDay = task.Days[d.Date.getDate()];
            contractedDays[d.Date.getDate()] = {
                Day: d.Date, Hours: d.ContractedHours, IsReadOnly: taskDay ? taskDay.IsReadOnly : d.IsReadOnly
            };
        });

        return updateTasks(state, task.Key, {
            Days: contractedDays
        });
    }

    function removeTask(state: ITasksStoreState, parameters: Actions.IRemoveTaskActionData): ITasksStoreState {
        let newState: ITasksStoreState = Utils.Clone(state);

        delete newState.tasks[parameters.task.Key];

        return newState;
    }

    function addTask(state: ITasksStoreState, data: Actions.IAddTaskActionData): ITasksStoreState {
        let newState: ITasksStoreState = Utils.Clone(state);
        newState.taskCounter++;

        newState.tasks[newState.taskCounter] = {
            Key: newState.taskCounter,
            ProjectId: data.project.ProjectId,
            AbsenceTypeId: undefined,
            IsReadOnly: false,
            Status: TaskStatus.Draft,
            Name: "",
            OvertimeVariant: Sheet.OvertimeVariantEnum.Regular,
            Days: [],
            SelectedAttributesValueIds: data.project.Attributes.map(a => a.DefaultValueId || null)
        };

        return newState;
    }

    function addProject(state: ITasksStoreState, parameters: Actions.IAddProjectActionData): ITasksStoreState {
        let newState = Utils.Clone(state);
        newState.taskCounter++;

        const newTask: ITask = {
            Key: newState.taskCounter,
            ProjectId: parameters.projectId,
            IsReadOnly: false,
            Status: TaskStatus.Draft,
            Name: '',
            OvertimeVariant: Sheet.OvertimeVariantEnum.Regular,
            Days: taskDayHours(parameters.days.map(d => d.Date)),
            SelectedAttributesValueIds: parameters.metadata.ProjectAttributes.map(a => a.DefaultValueId || null)
        };

        newState.tasks[newState.taskCounter] = newTask;

        return newState;
    }

    function changeProject(state: ITasksStoreState, oldProjectId: number, newProjectId: number, newProjectMetadata: TimeTrackingReact.IProjectMetadata) {

        const reportingStartsOn = newProjectMetadata.ReportingStartsOn != null
            ? new Date(newProjectMetadata.ReportingStartsOn).getTime()
            : null;

        Utils.Enumerate(state.tasks).filter(t => t.ProjectId == oldProjectId).forEach(task =>
        {
            state.tasks[task.Key].ProjectId = newProjectId;
            task.SelectedAttributesValueIds = newProjectMetadata.ProjectAttributes.map(a => a.DefaultValueId || null);

            Utils.Enumerate(state.tasks[task.Key].Days).forEach(d => {
                d.IsReadOnly = reportingStartsOn != null && reportingStartsOn > d.Day.getTime();
            });
        });

        return state;
    }

    function removeProject(state: ITasksStoreState, projectId: number) {
        let newState: ITasksStoreState = Utils.Clone(state);

        for (let key in state.tasks) {
            if (state.tasks[key].ProjectId == projectId) {
                delete state.tasks[key];
            }
        }

        return newState;
    }

    function loadData(state: ITasksStoreState, data: Actions.ILoadDataActionData): ITasksStoreState {
        return Utils.Assign(state, loadTasks(data.viewModel));
    }

    function clearData(state: ITasksStoreState): ITasksStoreState {
        return Utils.Assign(state, {
            tasks: [],
            taskCounter: 1
        });
    }

    function splitOvertime(state: ITasksStoreState, data: Actions.ISplitOvertimeActionData): ITasksStoreState {
        var newState = Utils.Clone(state);
        let currentTask = newState.tasks[data.task.Key];
        let newTaskDays: Tasks.ITaskDay[] = [];
        const [defaultOvertimeOption] = Sheet.filterAllowedOvertimeOptions(state.allowedEmployeeOvertime, data.project.AllowedProjectOvertime);

        data.rangeDays.forEach(d => {
            let taskDayEntry = currentTask.Days[d.Date.getDate()];
            const contractedHours = d.ContractedHours;
            const isExceeding = taskDayEntry.Hours > contractedHours;
            newTaskDays[d.Date.getDate()] = {
                Day: d.Date,
                Hours: isExceeding ? taskDayEntry.Hours - contractedHours : 0,
                IsReadOnly: taskDayEntry.IsReadOnly
            };

            taskDayEntry.Hours = isExceeding ? contractedHours : taskDayEntry.Hours;
        });

        newState.taskCounter++;

        newState.tasks[newState.taskCounter] = {
            Key: newState.taskCounter,
            ProjectId: data.task.ProjectId,
            IsReadOnly: false,
            Name: data.task.Name,
            OvertimeVariant: defaultOvertimeOption ? defaultOvertimeOption.value : data.task.OvertimeVariant,
            Days: newTaskDays,
            Status: TaskStatus.Draft,
            SelectedAttributesValueIds: [...data.task.SelectedAttributesValueIds]
        };

        return newState;
    }

    export function tasksStore(state: ITasksStoreState = { tasks: [], taskCounter: 1, allowedEmployeeOvertime: Sheet.OvertimeVariantEnum.NormalOvertime }, action: Actions.ITimeTrackingAction): ITasksStoreState {
        switch (action.type) {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return loadData(state, action.data as Actions.ILoadDataActionData);
                }

            case Actions.TimeTrackingActionType.ClearData:
                {
                    return clearData(state);
                }

            case Actions.TimeTrackingActionType.SplitOvertime:
                {
                    return splitOvertime(state, action.data as Actions.ISplitOvertimeActionData);
                }

            case Actions.TimeTrackingActionType.ChangeHours:
                {
                    return changeHours(state, action.data as Actions.IChangeHoursActionData);
                }

            case Actions.TimeTrackingActionType.ChangeTaskName:
                {
                    const data = action.data as Actions.IChangeTaskNameActionData;
                    return changeName(state, data.task.Key, data.newName);
                }

            case Actions.TimeTrackingActionType.RemoveTask:
                {
                    return removeTask(state, action.data as Actions.IRemoveTaskActionData);
                }

            case Actions.TimeTrackingActionType.AddTask:
                {
                    return addTask(state, action.data as Actions.IAddTaskActionData);
                }

            case Actions.TimeTrackingActionType.AddProject:
                {
                    return addProject(state, action.data as Actions.IAddProjectActionData);
                }

            case Actions.TimeTrackingActionType.ChangeProject:
                {
                    const data = action.data as Actions.IChangeProjectActionData;
                    return changeProject(state, data.project.ProjectId, data.newProjectId, data.newProjectMetadata);
                }

            case Actions.TimeTrackingActionType.RemoveProject:
                {
                    const data = action.data as Actions.IRemoveProjectActionData;
                    return removeProject(state, data.project.ProjectId);
                }

            case Actions.TimeTrackingActionType.ChangeAttributes:
                {
                    const data = action.data as Actions.IChangeAttributesActionData;
                    return updateTasks(state, data.task.Key, {
                        SelectedAttributesValueIds: data.newAttributeValueIds
                    });
                }

            case Actions.TimeTrackingActionType.ChangeOvertimeVariant:
                {
                    const data = action.data as Actions.IChangeOvertimeVariantActionData;
                    return updateTasks(state, data.task.Key, {
                        OvertimeVariant: data.newOvertimeVariant
                    });
                }

            case Actions.TimeTrackingActionType.CloneTask:
                {
                    const data = action.data as Actions.ICloneTaskActionData;
                    return cloneTask(state, data.task);
                }

            case Actions.TimeTrackingActionType.FillContractedHours:
                {
                    const data = action.data as Actions.IFillContractedHoursActionData;
                    return fillHours(state, data.project, data.task, data.days);
                }
        }

        return state;
    }
}
