﻿namespace TimeTracking.Store.Monitor {
    export interface IMonitorStoreState {
        lastChange: Date;
        hasUnsavedChanges: boolean;
        changesConfirmed: boolean;
        isLoading: boolean;
    }

    const defaultState: IMonitorStoreState = {
        lastChange: new Date(),
        hasUnsavedChanges: false,
        changesConfirmed: false,
        isLoading: false,
    };

    function clarMonitor(state: IMonitorStoreState) {
        return Utils.Assign(state, { lastChange: new Date(), hasUnsavedChanges: false, changesConfirmed: false, isLoading: false });
    }

    function detectChange(state: IMonitorStoreState) {
        return Utils.Assign(state, { lastChange: new Date(), hasUnsavedChanges: true, changesConfirmed: false });
    }

    function confirmChanges(state: IMonitorStoreState) {
        return Utils.Assign(state, { changesConfirmed: true });
    }

    export function monitorStore(state: IMonitorStoreState = defaultState, action: Actions.ITimeTrackingAction): IMonitorStoreState {
        switch (action.type) {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return clarMonitor(state);
                }
            case Actions.TimeTrackingActionType.ConfirmChanges:
                {
                    return confirmChanges(state);
                }
            case Actions.TimeTrackingActionType.LoadStarted:
            case Actions.TimeTrackingActionType.LoadEnded:
                {
                    return Utils.Assign(state, { isLoading: action.type === Actions.TimeTrackingActionType.LoadStarted });
                }
            case Actions.TimeTrackingActionType.ToggleCollapseProject:
                {
                    return state;
                }
            default:
                {
                    return detectChange(state);
                }
        }
    }
}
