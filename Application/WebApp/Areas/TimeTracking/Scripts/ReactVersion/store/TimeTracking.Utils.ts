﻿namespace TimeTracking.Utils {
    export function Clone<T>(obj: T): T {
        if (null == obj || "object" != typeof obj || obj instanceof Date) {
            return obj;
        }

        var copy = obj.constructor();

        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = Clone(obj[attr]);
            }
        }

        return copy;
    }

    export function Assign<T>(source: T, options: Partial<T>, ...extraOptions: Array<Partial<T>>): T {
        return $.extend(Clone(source), options, ...extraOptions) as T;
    }

    export function Enumerate<T>(source: { [index: number]: T }): T[] {
        let outputArray: T[] = [];

        for (var item in source) {
            const arrayItem = source[item];
            if (!!arrayItem) {
                outputArray.push(arrayItem);
            }
        }

        return outputArray;
    }

    export function ClassNames(input: { [name: string]: boolean }): string {
        let outputClass: string[] = [];
        for (var item in input) {
            if (input[item]) {
                outputClass.push(item);
            }
        }

        return outputClass.join(' ');
    }

    export function ChildrenChanged(a: React.ReactChild[], b: React.ReactChild[]): boolean {
        const aaa = a.map(aa => (aa as any)["key"]).join();
        const bbb = b.map(bb => (bb as any)["key"]).join();

        return aaa != bbb;
    }

    export function AreEqual<T>(obj1: T, obj2: T): boolean {
        return JSON.stringify(obj1) === JSON.stringify(obj2);
    }

    export function Log(message?: any, ...params: any[]) {
        console.log(message, params);
    }

    export function Range(size: number): number[] {
        const range: number[] = [];

        for (let i = 0; i < size; i++) {
            range.push(i);
        }

        return range;
    }

    export function PadLeft(value: number, size: number): string {
        const s = value.toString();
        
        return Range(size - s.length).reduce((a) => "0" + a, s);
    }

    export function withoutTimezoneOffset(dateString: string): Date {
        const offset = new Date().getTimezoneOffset() / 60;
        if (offset < 0) {
            return new Date(dateString);
        }

        const toParse = `${dateString.substr(0, 10)}T${PadLeft(offset, 2)}:00:00Z`;

        return new Date(toParse);
    }

    export function withTimezoneOffset(date: Date): string {
        const result = `${date.getFullYear()}-${PadLeft(date.getMonth() + 1, 2)}-${PadLeft(date.getDate(), 2)}`;

        return result;
    }
}
