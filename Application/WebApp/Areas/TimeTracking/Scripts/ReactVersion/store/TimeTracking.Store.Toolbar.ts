﻿module TimeTracking.Store.Toolbar {
    export interface IImportSource extends Ui.IButtonDropdownItem {

    }

    export interface INavigationAction extends Ui.IButtonDropdownItem {

    }

    export interface IProjectsCollapsed {
        [projectId: number]: boolean;
    }

    export interface IToolbarStoreState {
        title: string;
        employeeId: number;
        employeeName: string;
        canReopenTimeReport: boolean,
        canImport: boolean,
        canSave: boolean,
        canSeeNextMonth: boolean,
        canSeePreviousMonth: boolean,
        canSubmit: boolean,
        status: number;
        year: number;
        month: number;
        importSources: IImportSource[],
        navigationActions: INavigationAction[],
        isReadOnly: boolean;
        isLoaded: boolean;
        allowedOvertime: Sheet.OvertimeVariantEnum;
        isMobile: boolean;
        projectsCollapsed: IProjectsCollapsed;
        isMovable: boolean;
    }

    const defaultToolbarState: IToolbarStoreState = {
        title: '',
        employeeId: 0,
        employeeName: '',
        canReopenTimeReport: false,
        canImport: false,
        canSave: false,
        canSeeNextMonth: false,
        canSeePreviousMonth: false,
        canSubmit: false,
        status: -1,
        year: 0,
        month: 0,
        importSources: [],
        navigationActions: [],
        isReadOnly: true,
        isLoaded: false,
        allowedOvertime: TimeTracking.Sheet.OvertimeVariantEnum.Regular,
        isMobile: false,
        projectsCollapsed: {},
        isMovable: false
    };

    function loadData(state: IToolbarStoreState, action: Actions.ITimeTrackingAction): IToolbarStoreState {
        const typedAction = action as any;
        const projectsCollapsedValue = localStorage !== undefined ? localStorage.getItem("projectsCollapsed") : null;
        return Utils.Assign(state, {
            isLoaded: true,
            isMobile: typedAction.data.isMobile,
            title: typedAction.data.title,
            employeeName: typedAction.data.viewModel.EmployeeFullName,
            employeeId: typedAction.data.viewModel.EmployeeId,
            canReopenTimeReport: typedAction.data.toolbarOptions.CanReopenTimeReport,
            canImport: typedAction.data.toolbarOptions.CanImport,
            canSave: typedAction.data.toolbarOptions.CanSave,
            canSeeNextMonth: typedAction.data.toolbarOptions.CanSeeNextMonth,
            canSeePreviousMonth: typedAction.data.toolbarOptions.CanSeePreviousMonth,
            canSubmit: typedAction.data.toolbarOptions.CanSubmit,
            status: typedAction.data.viewModel.Status,
            year: typedAction.data.viewModel.Year,
            month: typedAction.data.viewModel.Month,
            isReadOnly: typedAction.data.viewModel.IsReadOnly,
            allowedOvertime: typedAction.data.viewModel.AllowedOvertime,
            importSources: (typedAction.data.toolbarOptions.ImportSources || []).map((i: any) => {
                return {
                    name: i.Name,
                    url: i.Url,
                    method: 'POST'
                };
            }),
            navigationActions: (typedAction.data.toolbarOptions.NavigationActions || []).map((i: any) => {
                return {
                    name: i.Text,
                    url: i.OnClickUrl,
                    method: 'GET'
                };
            }),
            projectsCollapsed: projectsCollapsedValue !== null ? JSON.parse(projectsCollapsedValue) : {},
        });
    };


    function toggleCollapseProject(state: IToolbarStoreState, action: Actions.IToggleCollapseProject): IToolbarStoreState {

        const projectsCollapsed = {
            ...state.projectsCollapsed,
            [action.projectId]: !state.projectsCollapsed[action.projectId]
        }

        if (localStorage !== undefined) {
            localStorage.setItem("projectsCollapsed", JSON.stringify(projectsCollapsed));
        }

        return { ...state, projectsCollapsed }
    }

    function toggleProjectisMovable(state: IToolbarStoreState): IToolbarStoreState {
        return { ...state, isMovable: !state.isMovable };
    }
      
    export function toolbarStore(state: IToolbarStoreState = defaultToolbarState, action: Actions.ITimeTrackingAction): IToolbarStoreState
    {
        switch (action.type)
        {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return loadData(state, action);
                }

            case Actions.TimeTrackingActionType.ToggleCollapseProject:
                {
                    return toggleCollapseProject(state, action.data as Actions.IToggleCollapseProject);
                }
            case Actions.TimeTrackingActionType.ToggleProjectsIsMovable:
                {
                    return toggleProjectisMovable(state);
                }
        }

        return state;
    }
}
