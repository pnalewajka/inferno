﻿namespace TimeTracking.Store.ClockCard {
    export interface ITime {
        getUTCHours(): number;
        getUTCMinutes(): number;
    }

    export interface IClockCardDay {
        date: Date;
        inTime?: ITime;
        outTime?: ITime;
        breakDuration?: ITime;
    }

    export interface IClockCardDayDictionary extends Array<IClockCardDay> {
        [day: number]: IClockCardDay;
    }

    export interface IClockCardStoreState {
        days: IClockCardDayDictionary;
        isReadOnly: boolean;
        isRequired: boolean;
    }

    const defaultClockCardState: IClockCardStoreState = {
        days: [],
        isReadOnly: true,
        isRequired: false
    }

    export function clockCardStore(state: IClockCardStoreState = defaultClockCardState, action: Actions.ITimeTrackingAction): IClockCardStoreState {
        switch (action.type) {
            case Actions.TimeTrackingActionType.LoadData: {
                const data = action.data as Actions.ILoadDataActionData;

                return Utils.Assign(state, {
                    days: prepareDictionary(data.viewModel.Days.map(d => Utils.withoutTimezoneOffset(d.Date)), data.viewModel.ClockCardDailyEntries),
                    isReadOnly: data.viewModel.IsReadOnly,
                    isRequired: data.viewModel.IsClockCardRequired
                });
            }

            case Actions.TimeTrackingActionType.ChangeTime: {
                const data = action.data as Actions.IChangeTimeActionData;

                const newState = Utils.Clone(state);
                const day = newState.days[data.date.getDate()];

                setTime(day, data.rowType, data.newTime);

                return newState;
            }

            case Actions.TimeTrackingActionType.FillTime: {
                const data = action.data as Actions.IFillTimeActionData;

                const lastFilled = state.days.filter(d => getTime(d, data.rowType)).slice(-1)[0];

                if (lastFilled === undefined) {
                    return state;
                }

                const lastFilledDay = lastFilled.date.getDate();
                const lastFilledTime = getTime(lastFilled, data.rowType);

                const newState = Utils.Clone(state);

                newState.days.forEach(d => {
                    const day = d.date.getDate();

                    if (day > lastFilledDay && data.days[day - 1].ContractedHours > 0) {
                        setTime(d, data.rowType, lastFilledTime);
                    }
                });

                return newState;
            }

            case Actions.TimeTrackingActionType.CalculateTime: {
                const data = action.data as Actions.ICalculateTimeActionData;

                const times = [
                    { rowType: TimeTracking.Sheet.ClockCardRowType.In, multiplier: -1 },
                    { rowType: TimeTracking.Sheet.ClockCardRowType.Out, multiplier: 1 },
                    { rowType: TimeTracking.Sheet.ClockCardRowType.Break, multiplier: -1 }
                ];
                const filledTimes = times.filter(t => t.rowType !== data.rowType);

                const newState = Utils.Clone(state);

                newState.days.forEach(d => {
                    if (filledTimes.some(t => getTime(d, t.rowType) === undefined)) {
                        return;
                    }

                    const reportedMiliseconds = filledTimes.reduce((r, t) => {
                        const time = getTime(d, t.rowType);

                        return r + t.multiplier * Date.UTC(1970, 0, 1, time.getUTCHours(), time.getUTCMinutes());
                    }, 0);

                    const date = d.date;
                    const missingMinutes = Math.abs(reportedMiliseconds / 1000 / 60 - data.days[date.getDate() - 1].ContractedHours * 60);

                    setTime(d, data.rowType, new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, missingMinutes)));
                });

                return newState;
            }
        }

        return state;
    }

    function getTime(day: IClockCardDay, rowType: TimeTracking.Sheet.ClockCardRowType): ITime {
        switch (rowType) {
            case TimeTracking.Sheet.ClockCardRowType.In: {
                return day.inTime as ITime;
            }

            case TimeTracking.Sheet.ClockCardRowType.Out: {
                return day.outTime as ITime;
            }

            case TimeTracking.Sheet.ClockCardRowType.Break: {
                return day.breakDuration as ITime;
            }
        }

        throw new Error("Invalid Row Type");
    }

    function setTime(day: IClockCardDay, rowType: TimeTracking.Sheet.ClockCardRowType, newTime: ITime | null): void
    {
        switch (rowType) {
            case TimeTracking.Sheet.ClockCardRowType.In: {
                day.inTime = newTime != null ? newTime : undefined;
                break;
            }

            case TimeTracking.Sheet.ClockCardRowType.Out: {
                day.outTime = newTime != null ? newTime : undefined;
                break;
            }

            case TimeTracking.Sheet.ClockCardRowType.Break: {
                day.breakDuration = newTime != null ? newTime : undefined;
                break;
            }
        }
    }

    function prepareDictionary(dates: Date[], entries?: TimeTrackingReact.IClockCardDailyEntry[]): IClockCardDayDictionary {
        const dictionary: IClockCardDayDictionary = [];

        if (!!entries) {
            entries.forEach(e => {
                const date = Utils.withoutTimezoneOffset(e.Day);

                dictionary[date.getDate()] = {
                    date: date,
                    inTime: e.InTime !== null ? new Date(e.InTime) : undefined,
                    outTime: e.OutTime !== null ? new Date(e.OutTime) : undefined,
                    breakDuration: e.BreakDuration !== null ? DateHelper.parseDateTime(date, e.BreakDuration) : undefined,
                };
            });
        }

        dates.filter(d => !(d.getDate() in dictionary)).forEach(d => {
            dictionary[d.getDate()] = {
                date: d,
            };
        });

        return dictionary;
    }
}
