﻿namespace TimeTracking.Store.Layout
{
    export interface ILayoutPosition
    {
        top: number;
        left: number;
    }

    export interface ILayoutStoreState
    {
        position: ILayoutPosition;
    }

    const defaultLayoutState: ILayoutStoreState = {
        position:
        {
            top: 0,
            left: 0
        }
    };

    export function layoutStore(state: ILayoutStoreState = defaultLayoutState, newState: ILayoutStoreState): ILayoutStoreState
    {
        return Utils.Assign(state, newState);
    }
}
