﻿namespace TimeTracking.Service {
    export let urls: {
        LoadReport: string;
        SaveReport?: string;
        SubmitReport?: string;
        ConfirmReport?: string;
        ReopenTimeReport?: string;
    };

    const debounce = new Atomic.Debouncer(() => {
        $(".alert-container .alert-success").alert("close");
    }, 3000);

    function handleAlerts(response: any) {
        if (response._alerts != null) {
            $(".alert-container .alert").alert("close");
        }
        (response._alerts || []).forEach((a: any) => {
            AlertMessages.addAlert(a.Type, a.Message);
        });

        $(".alert-container .alert").trigger("add.bs.alert");

        debounce.execute();

        $(".alert-success")
            .on("mouseenter", () => debounce.clearExistingTimeout())
            .on("mouseleave", () => debounce.execute());
    }

    function clockCardDailyEntries(days: Store.ClockCard.IClockCardDayDictionary): TimeTrackingReact.IClockCardDailyEntry[] {
        return days.reduce((result: TimeTrackingReact.IClockCardDailyEntry[], day) => {
            const date = new Date(day.date);
            date.setHours(0, 0, 0, 0);

            result.push({
                Day: Utils.withTimezoneOffset(date),
                InTime: day.inTime !== undefined ? DateHelper.toTimeString(day.inTime as Date) : null,
                OutTime: day.outTime !== undefined ? DateHelper.toTimeString(day.outTime as Date) : null,
                BreakDuration: day.breakDuration !== undefined ? DateHelper.toTimeString(day.breakDuration as Date) : null
            });

            return result;
        }, []);
    }

    function taskDays(days: Store.Tasks.IDayDictionary): TimeTrackingReact.ITimeTrackingTaskDay[] {

        const separator = Globals.getDecimalSeparator();

        let roundHourValues = (input: number) => (+(input || 0)).toString().replace(/,/g, separator).replace(/\./g, separator);

        return Utils.Enumerate(days)
            .filter(day => day.Hours > 0)
            .map(day => {

                const item: TimeTrackingReact.ITimeTrackingTaskDay = {
                    Day: Utils.withTimezoneOffset(day.Day),
                    Hours: roundHourValues(day.Hours) as any // for saving purposes this need to be casted to string and can"t be number
                };

                return item;
            });
    }

    function projectTasks(tasks: Store.Tasks.ITaskDictionary, projectId: number): TimeTrackingReact.ITimeTrackingTask[] {
        const saveTaskCollection: TimeTrackingReact.ITimeTrackingTask[] = [];
        for (var taskKey in tasks) {
            const task = tasks[taskKey];
            if (task.ProjectId === projectId) {
                let saveTask: TimeTrackingReact.ITimeTrackingTask = {
                    Id: task.Id,
                    Name: task.Name,
                    OvertimeVariant: task.OvertimeVariant,
                    SelectedAttributesValueIds: task.SelectedAttributesValueIds,
                    Days: taskDays(task.Days),
                    ImportSourceId: task.ImportSourceId,
                    ImportedTaskCode: task.ImportedTaskCode,
                    ImportedTaskType: task.ImportedTaskType,
                };

                saveTaskCollection.push(saveTask);
            }
        }
        return saveTaskCollection;
    }

    export function getTimeReportData(storeState: Store.ITimeTrackingStoreState): TimeTrackingReact.ISaveTimeTrackingViewModel {
        const { clockCard, projects, tasks, toolbar } = storeState;

        let model: TimeTrackingReact.ISaveTimeTrackingViewModel = {
            EmployeeId: toolbar.employeeId,
            ClockCardDailyEntries: clockCardDailyEntries(clockCard.days),
            TimeZoneOffsetMinutes: -(new Date().getTimezoneOffset()),
            Projects: projects.projects.map(project => {
                let saveProject: TimeTrackingReact.ITimeTrackingProject = {
                    ProjectName: project.ProjectName,
                    ProjectId: project.ProjectId,
                    Attributes: [],
                    IsReadOnly: false,
                    ReportingStartsOn: null,
                    Tasks: projectTasks(tasks.tasks, project.ProjectId),
                };

                return saveProject;
            }),
            Year: toolbar.year,
            Month: toolbar.month
        };

        return model;
    }

    function asynHandler(action: () => JQueryPromise<any>, silent = false): JQueryPromise<any> {
        if (!silent) {
            CommonDialogs.pleaseWait.show();
        }

        var deferred = $.Deferred();

        action()
            .fail((a: any) => {
                const response = JSON.parse(a.responseText);
                handleAlerts(response);

                if (!silent) {
                    CommonDialogs.pleaseWait.hide();
                }

                deferred.reject(response._payload || response);

                return response;
            })
            .done((response: any) => {
                handleAlerts(response);

                if (!silent) {
                    CommonDialogs.pleaseWait.hide();
                }

                if (!!response._statusCode && response._statusCode !== 200) {
                    deferred.reject(response._payload || response);
                } else {
                    deferred.resolve(response._payload || response);
                }

                return response;
            });

        return deferred.promise();
    }

    function ajaxGet(url: string, silent = false): JQueryPromise<any> {
        return asynHandler(() => $.ajax(url, {
            cache: false,
            error: (xhr: XMLHttpRequest) => {
                if (xhr.status !== 200) {
                    GlobalErrorHandling.handleAjaxErrorInBody(xhr.responseText, $(".content-section"));
                }
            }
        }), silent);
    }

    function ajaxPost(url: string, model?: any): JQueryPromise<any> {
        return asynHandler(() => $.post(url, model));
    }

    export function loadReport() {
        Store.defaultStore().dispatch({ type: Actions.TimeTrackingActionType.LoadStarted });
        return ajaxGet(urls.LoadReport, true);
    }

    export function saveReport(store: Store.ITimeTrackingStoreState): JQueryPromise<any> {
        return ajaxPost(urls.SaveReport as string, getTimeReportData(store));
    }

    export function submitReport(store: Store.ITimeTrackingStoreState): JQueryPromise<any> {
        return ajaxPost(urls.SubmitReport as string, getTimeReportData(store));
    }

    export function submitConfirm(model: { employeeId: number, year: number, month: number }): JQueryPromise<string[]> {
        return ajaxPost(urls.ConfirmReport as string, model);
    }

    export function contextSearchQuery(): string {
        const { toolbar } = Store.defaultStore().getState();
        const timeTrackingProjectPickerMode = 0; // See: TimeTrackingProjectPickerContext.cs

        return `mode=${timeTrackingProjectPickerMode}&year=${toolbar.year}&month=${toolbar.month}`;
    }
}
