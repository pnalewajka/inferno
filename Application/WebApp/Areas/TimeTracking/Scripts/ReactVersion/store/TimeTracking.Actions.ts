﻿namespace TimeTracking.Actions {

    export interface IAction<TType> extends Redux.Action {
        readonly type: TType;
    }

    interface ITaskChangeActionData {
        project: Store.Projects.IProject;
        task: Store.Tasks.ITask;
        taskCounter: number;
    }

    export enum TimeTrackingActionType {
        LoadData,
        ClearData,
        AddProject,
        ChangeProject,
        RemoveProject,
        ChangeHours,
        ChangeTime,
        RemoveTask,
        AddTask,
        CloneTask,
        SplitOvertime,
        FillContractedHours,
        FillTime,
        CalculateTime,
        ChangeAttributes,
        ChangeOvertimeVariant,
        ChangeTaskName,
        ChangeScroll,
        ConfirmChanges,
        LoadStarted,
        LoadEnded,
        ScrollTo,
        ToggleCollapseProject,
        ReorderProjectUp,
        ReorderProjectDown,
        ToggleProjectsIsMovable,
    }

    export interface ITimeTrackingActionData { }

    export interface ITimeTrackingAction extends IAction<TimeTrackingActionType> {
        data: ITimeTrackingActionData;
    }

    export interface ITimeTrackingTypedAction<TData extends ITimeTrackingActionData> extends ITimeTrackingAction {
        data: TData;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IAddProjectActionData extends ITimeTrackingActionData {
        projectId: number;
        projectName: string;
        metadata: TimeTrackingReact.IProjectMetadata;
        days: Store.DateRange.IDay[];
    }

    export function addProject(projectId: number, projectName: string, metadata: TimeTrackingReact.IProjectMetadata)
        : ITimeTrackingTypedAction<IAddProjectActionData>
    {
        const { dateRange } = Store.defaultStore().getState();
        console.log("set metadata", metadata);

        return {
            type: TimeTrackingActionType.AddProject,
            data: {
                projectId: projectId,
                projectName: projectName,
                metadata: metadata,
                days: [...dateRange.days],
            },
        };
    }



    export function moveProjectUp(projectId: number)
        : ITimeTrackingTypedAction<IReorderProjectUp> {
        return {
            type: TimeTrackingActionType.ReorderProjectUp,
            data: {
                projectId: projectId,
            },
        };
    }


    export function moveProjectDown(projectId: number)
        : ITimeTrackingTypedAction<IReorderProjectDown> {
        return {
            type: TimeTrackingActionType.ReorderProjectDown,
            data: {
                projectId: projectId,
            },
        };
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IChangeHoursActionData extends ITimeTrackingActionData {
        task: Store.Tasks.ITask;
        project: Store.Projects.IProject;
        date: Date;
        oldHours: number;
        newHours: number;
    }

    export function changeHours(taskKey: number, date: Date, oldHours: number, newHours: number): ITimeTrackingTypedAction<IChangeHoursActionData> {

        const state = Store.defaultStore().getState();
        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);

        return {
            type: TimeTrackingActionType.ChangeHours,
            data: {
                task: task,
                project: project,
                date: date,
                oldHours: oldHours,
                newHours: newHours,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IChangeTimeActionData extends ITimeTrackingActionData {
        date: Date;
        rowType: TimeTracking.Sheet.ClockCardRowType;
        newTime: Store.ClockCard.ITime | null;
    }

    export function changeTime(date: Date, rowType: TimeTracking.Sheet.ClockCardRowType, newTime: Store.ClockCard.ITime | null): ITimeTrackingTypedAction<IChangeTimeActionData> {
        return {
            type: TimeTrackingActionType.ChangeTime,
            data: {
                date: date,
                rowType: rowType,
                newTime: newTime
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IRemoveTaskActionData extends ITimeTrackingActionData {
        task: Store.Tasks.ITask;
        project: Store.Projects.IProject;
        isLastTask: boolean;
    }

    export function removeTask(taskKey: number): ITimeTrackingTypedAction<IRemoveTaskActionData> {

        const state = Store.defaultStore().getState();
        
        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);

        let taskCount = 0;
        for (var t in state.tasks.tasks) {
            if (state.tasks.tasks[t].ProjectId === task.ProjectId) {
                taskCount++;
            }
        }

        return {
            type: TimeTrackingActionType.RemoveTask,
            data: {
                task: task,
                project: project,
                isLastTask: taskCount === 1,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface ILayoutPositionActionData extends Store.Layout.ILayoutPosition, ITimeTrackingActionData { }

    export function changePositionAction(top: number, left: number): ITimeTrackingTypedAction<ILayoutPositionActionData> {
        return {
            type: TimeTrackingActionType.ChangeScroll,
            data: {
                top: top,
                left: left,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export function clearData(): ITimeTrackingTypedAction<ITimeTrackingActionData> {
        return {
            type: TimeTrackingActionType.ClearData,
            data: {}
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface ILoadDataActionData
    {
        viewModel: TimeTrackingReact.ITimeTrackingViewModel;
    }

    export function loadData(payload: any): ITimeTrackingTypedAction<ILoadDataActionData> {
        return {
            type: TimeTrackingActionType.LoadData,
            data: payload
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IChangeAttributesActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
        task: Store.Tasks.ITask;
        previousAttributeValueIds: Array<number | null>;
        newAttributeValueIds: Array<number | null>;
    }

    export function changeAttributes(taskKey: number, newAttributeValueIds: Array<number | null>)
        : ITimeTrackingTypedAction<IChangeAttributesActionData>
    {
        const state = Store.defaultStore().getState();

        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);

        return {
            type: TimeTrackingActionType.ChangeAttributes,
            data: {
                task: task,
                project: project,
                previousAttributeValueIds: [...task.SelectedAttributesValueIds],
                newAttributeValueIds: [...newAttributeValueIds],
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    export function toggleisMovable(): ITimeTrackingAction {
        return {
            type: TimeTrackingActionType.ToggleProjectsIsMovable,
            data: {}
        }
    }
    export interface IChangeTaskNameActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
        task: Store.Tasks.ITask;
        newName: string;
    }

    export function changeTaskName(taskKey: number, newName: string)
        : ITimeTrackingTypedAction<IChangeTaskNameActionData>
    {
        const state = Store.defaultStore().getState();

        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);

        return {
            type: TimeTrackingActionType.ChangeTaskName,
            data: {
                task: task,
                project: project,
                newName: newName,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IChangeOvertimeVariantActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
        task: Store.Tasks.ITask;
        newOvertimeVariant: TimeTracking.Sheet.OvertimeVariantEnum;
    }

    export function changeOvertimeVariant(taskKey: number, newOvertimeVariant: TimeTracking.Sheet.OvertimeVariantEnum)
        : ITimeTrackingTypedAction<IChangeOvertimeVariantActionData>
    {
        const state = Store.defaultStore().getState();

        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);

        return {
            type: TimeTrackingActionType.ChangeOvertimeVariant,
            data: {
                task: task,
                project: project,
                newOvertimeVariant: newOvertimeVariant,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IFillContractedHoursActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
        task: Store.Tasks.ITask;
        days: Store.DateRange.IDay[];
    }

    export function fillContractedHours(taskKey: number)
        : ITimeTrackingTypedAction<IFillContractedHoursActionData>
    {
        const state = Store.defaultStore().getState();

        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);
        const days = state.dateRange.days;

        return {
            type: TimeTrackingActionType.FillContractedHours,
            data: {
                task: task,
                project: project,
                days: days,
            },
        };
    }

    export interface ISplitOvertimeActionData extends ITimeTrackingActionData, ITaskChangeActionData {
        rangeDays: Store.DateRange.IDay[];
    }

    export function splitOvertime(taskKey: number): ITimeTrackingTypedAction<ISplitOvertimeActionData> {
        const { tasks, projects, dateRange } = Store.defaultStore().getState();

        const task = tasks.tasks[taskKey];
        const [project] = projects.projects.filter(p => p.ProjectId === task.ProjectId);
        
        return {
            type: TimeTrackingActionType.SplitOvertime,
            data: {
                task,
                project,
                rangeDays: dateRange.days,
                taskCounter: tasks.taskCounter
            }
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IFillTimeActionData extends ITimeTrackingActionData {
        days: Store.DateRange.IDay[];
        rowType: TimeTracking.Sheet.ClockCardRowType;
    }

    export function fillTime(rowType: TimeTracking.Sheet.ClockCardRowType): ITimeTrackingTypedAction<IFillTimeActionData> {
        const { dateRange } = Store.defaultStore().getState();

        return {
            type: TimeTrackingActionType.FillTime,
            data: {
                days: dateRange.days,
                rowType: rowType
            }
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface ICalculateTimeActionData extends ITimeTrackingActionData {
        days: Store.DateRange.IDay[];
        rowType: TimeTracking.Sheet.ClockCardRowType;
    }

    export function calculateTime(rowType: TimeTracking.Sheet.ClockCardRowType): ITimeTrackingTypedAction<ICalculateTimeActionData> {
        const { dateRange } = Store.defaultStore().getState();

        return {
            type: TimeTrackingActionType.CalculateTime,
            data: {
                days: dateRange.days,
                rowType: rowType
            }
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface ICloneTaskActionData extends ITimeTrackingActionData, ITaskChangeActionData {
    }

    export function cloneTask(taskKey: number)
        : ITimeTrackingTypedAction<ICloneTaskActionData>
    {
        const state = Store.defaultStore().getState();

        const task = state.tasks.tasks[taskKey];
        const [project] = state.projects.projects.filter(p => p.ProjectId === task.ProjectId);

        return {
            type: TimeTrackingActionType.CloneTask,
            data: {
                task: task,
                project: project,
                taskCounter: state.tasks.taskCounter
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IChangeProjectActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
        newProjectId: number;
        newProjectName: string;
        newProjectMetadata: TimeTrackingReact.IProjectMetadata;
        oldTasks: Store.Tasks.ITask[];
    }

    export function changeProject(projectId: number, newProjectId: number, newProjectName: string, newProjectMetadata: TimeTrackingReact.IProjectMetadata)
        : ITimeTrackingTypedAction<IChangeProjectActionData>
    {
        const state = Store.defaultStore().getState();
        const [project] = state.projects.projects.filter(p => p.ProjectId === projectId);
        const oldTasks: Store.Tasks.ITask[] = [];

        for (let k in state.tasks.tasks) {
            const task = state.tasks.tasks[k];
            if (task.ProjectId === projectId) {
                oldTasks.push(task);
            }
        }

        return {
            type: TimeTrackingActionType.ChangeProject,
            data: {
                project: project,
                newProjectId: newProjectId,
                newProjectName: newProjectName,
                newProjectMetadata: newProjectMetadata,
                oldTasks: oldTasks,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IAddTaskActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
    }

    export function addTask(projectId: number)
        : ITimeTrackingTypedAction<IAddTaskActionData>
    {
        const state = Store.defaultStore().getState();
        const [project] = state.projects.projects.filter(p => p.ProjectId === projectId);

        return {
            type: TimeTrackingActionType.AddTask,
            data: {
                project: project,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IRemoveProjectActionData extends ITimeTrackingActionData {
        project: Store.Projects.IProject;
        tasks: Store.Tasks.ITask[];
    }

    export function removeProject(projectId: number)
        : ITimeTrackingTypedAction<IRemoveProjectActionData>
    {
        const state = Store.defaultStore().getState();
        const [project] = state.projects.projects.filter(p => p.ProjectId === projectId);
        const tasks: Store.Tasks.ITask[] = [];

        for (let k in state.tasks.tasks) {
            const task = state.tasks.tasks[k];
            if (task.ProjectId === projectId) {
                tasks.push(task);
            }
        }

        return {
            type: TimeTrackingActionType.RemoveProject,
            data: {
                project: project,
                tasks: tasks,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IScrollToActionData extends ITimeTrackingActionData {
        element: HTMLElement;
    }

    export function scrollTo(element: HTMLElement)
        : ITimeTrackingTypedAction<IScrollToActionData>
    {
        return {
            type: TimeTrackingActionType.ScrollTo,
            data: {
                element: element,
            },
        };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    export interface IToggleCollapseProject extends ITimeTrackingActionData {
        projectId: number;
    }

    export interface IReorderProjectUp extends ITimeTrackingActionData {
        projectId: number;
    }

    export interface IReorderProjectDown extends ITimeTrackingActionData {
        projectId: number;
    }

    export function toggleCollapseProject(projectId: number): ITimeTrackingTypedAction<IToggleCollapseProject> {
        return {
            type: TimeTrackingActionType.ToggleCollapseProject,
            data: {
                projectId
            }
        }
    }
}
