﻿module TimeTracking.Store.Overtimes {

    export interface IOvertime {
        ProjectId: number;
        DateFrom: Date;
        DateTo: Date;
        HourLimit: number;
    }

    export interface IOvertimeStoreState {
        overtimes: IOvertime[];
    }

    const defaultOvertimesState: IOvertimeStoreState = {
        overtimes: []
    };

    function loadData(state: IOvertimeStoreState, data: Actions.ILoadDataActionData): IOvertimeStoreState {
        return {
            overtimes: [...data.viewModel.OvertimeApprovals.map(o => {
                return {
                    ProjectId: o.ProjectId,
                    DateFrom: new Date(o.DateFrom),
                    DateTo: new Date(o.DateTo),
                    HourLimit: o.HourLimit
                };
            })]
        };
    }

    export function overtimeStore(state: IOvertimeStoreState = defaultOvertimesState, action: Actions.ITimeTrackingAction): IOvertimeStoreState
    {
        switch (action.type)
        {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return loadData(state, action.data as Actions.ILoadDataActionData);
                }
        }

        return state;
    }
}
