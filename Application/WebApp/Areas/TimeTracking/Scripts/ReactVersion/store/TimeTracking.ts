﻿module TimeTracking.Store {

    export enum Direction {
        None = 0,
        Up = 1 << 0,
        Down = 1 << 1,
    }

    export interface ITimeTrackingStoreState {
        dateRange: DateRange.IDateRangeState,
        projects: Projects.IProjectsStoreState,
        summaries: Summaries.ISummariesStoreState,
        tasks: Tasks.ITasksStoreState,
        toolbar: Toolbar.IToolbarStoreState,
        overtimes: Overtimes.IOvertimeStoreState,
        monitor: Monitor.IMonitorStoreState,
        clockCard: ClockCard.IClockCardStoreState,
    }

    const countTimeMiddlewere: Redux.Middleware = (store: any) => (next: any) => (action: any) => {
        let start = Date.now();
        let result = next(action);
        var end = Date.now();
        var difference = new Date(end - start);
        console.log(`Dispach time ${end - start}ms`);
        return result;
    }

    const logger = (store: any) => (next: any) => (action: any) => {
        console.log('dispatching', action);
        let result = next(action);

        return result;
    }

    const cloneState = (reducer: any) => (state: any, action: any) => {
        const nextState = Utils.Clone(state);
        return reducer(nextState, action);
    }

    export const defaultStore = Dante.Utils.Lazy(() => {
        const defaultStoreState = {
            dateRange: DateRange.dateRangeStore as Redux.Reducer<DateRange.IDateRangeState>,
            projects: Store.Projects.projectsStore as Redux.Reducer<Store.Projects.IProjectsStoreState>,
            tasks: Tasks.tasksStore as Redux.Reducer<Tasks.ITasksStoreState>,
            summaries: Summaries.summaryStore as Redux.Reducer<Summaries.ISummariesStoreState>,
            toolbar: Toolbar.toolbarStore as Redux.Reducer<Toolbar.IToolbarStoreState>,
            overtimes: Overtimes.overtimeStore as Redux.Reducer<Overtimes.IOvertimeStoreState>,
            monitor: Monitor.monitorStore as Redux.Reducer<Monitor.IMonitorStoreState>,
            clockCard: ClockCard.clockCardStore as Redux.Reducer<ClockCard.IClockCardStoreState>,
        } as Redux.ReducersMapObject;

        return Redux.createStore<ITimeTrackingStoreState>(
            cloneState(Redux.combineReducers<ITimeTrackingStoreState>(defaultStoreState))
            , Redux.applyMiddleware(countTimeMiddlewere, logger));
    });

    export const defaultLayoutStore = Dante.Utils.Lazy(() => Redux.createStore<Store.Layout.ILayoutStoreState>(Store.Layout.layoutStore as Redux.Reducer<Store.Layout.ILayoutStoreState>));

    function handleAlerts(response: any) {
        if (!!response._alerts) {
            $('.alert-container').empty();
        }

        (response._alerts || []).forEach((a: any) => {
            AlertMessages.addAlert(a.Type, a.Message);
        });
    }

    export function ajaxPost(url: string, model?: any): JQueryPromise<any> {
        CommonDialogs.pleaseWait.show();
        var deferred = $.Deferred();

        $.post(url, model)
            .fail((a: any) => {
                const response = JSON.parse(a.responseText);
                handleAlerts(response);
                CommonDialogs.pleaseWait.hide();
                deferred.reject(response._payload || response);

                return response;
            })
            .done((response: any) => {
                handleAlerts(response);
                CommonDialogs.pleaseWait.hide();

                if (!!response._statusCode && response._statusCode !== 200) {
                    deferred.reject(response._payload || response);
                } else {
                    deferred.resolve(response._payload || response);
                }

                return response;
            });

        return deferred.promise();
    }
}
