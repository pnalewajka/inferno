﻿namespace TimeTrackingReact {
    export interface IServerResponce {
        title: string;
        viewModel: ITimeTrackingViewModel;
        isMobile: boolean;
    }

    export interface ISaveTimeTrackingViewModel {
        EmployeeId: number;
        ClockCardDailyEntries: IClockCardDailyEntry[];
        Projects: ITimeTrackingProject[];
        Month: number;
        Year: number;
        TimeZoneOffsetMinutes: number;
    }

    // Source: CrossCutting\Business\Enums\TimeReportStatus.cs
    export enum TimeReportStatus
    {
        NotStarted = -1,
        Draft = 0,
        Submitted = 1,
        Accepted = 2,
        Rejected = 3
    }

    export interface ITimeTrackingViewModel extends ISaveTimeTrackingViewModel {
        AllowedOvertime: TimeTracking.Sheet.OvertimeVariantEnum;
        CanBeReopened: boolean;
        Days: ITimeTrackingDay[];
        EmployeeFullName: string;
        IsMyTimeReport: boolean;
        IsReadOnly: boolean;
        IsClockCardRequired: boolean;
        IsSubmitRequest: boolean;
        IsTimeReportAvailablePreviousMonth: boolean;

        Status: TimeReportStatus;
        TimeReportId: number;

        OvertimeApprovals: any[];
        Weeks: any[];

        TimeZoneDisplayName?: string;
        TimeZoneOffsetMinutes: number;
    }

    export interface ITimeTrackingTaskDay {
        Day: string;
        Hours: number;
        IsReadOnly?: boolean;
    }

    export interface IProjectAttributeValue {
        Id: number;
        Name: string;
        Features: TimeTracking.Store.Projects.AttributeValueFeature;
    }

    export interface IProjectAttribute {
        DefaultValueId: number | undefined;
        Description: string;
        Id: number;
        Name: string;
        PossibleValues: IProjectAttributeValue[];
    }

    export interface ITimeTrackingTask {
        Id?: number;
        Name: string;
        OvertimeVariant: number;
        IsReadOnly?: boolean;
        SelectedAttributesValueIds: Array<number | null>;
        Status?: number;
        ImportedTaskCode?: string;
        ImportedTaskType?: string;
        ImportSourceId?: number;
        AbsenceTypeId?: number;
        Days: ITimeTrackingTaskDay[];
    }

    export interface ITimeTrackingProject {
        ProjectName: string;
        ProjectId: number;
        Attributes: IProjectAttribute[];
        IsReadOnly: boolean;
        Tasks: ITimeTrackingTask[];
        ReportingStartsOn: Date | null;
        AllowedProjectOvertime?: TimeTracking.Sheet.OvertimeVariantEnum;
    }

    export interface IOvertimeApproval {
        DateFrom: Date;
        DateTo: Date;
        EmployeeId: number;
        HourLimit: number;
        ProjectId: number;
    }

    export interface IProjectMetadata {
        ProjectId: number;
        ProjectName: string;
        Apn: string;
        KupferwerkProjectId: string;
        OvertimeApprovals: IOvertimeApproval[];
        ProjectAttributes: IProjectAttribute[];
        AllowedProjectOvertime: TimeTracking.Sheet.OvertimeVariantEnum;
        ReportingStartsOn: string | null;
    }

    export interface ITimeTrackingDay {
        Date: string;
        ContractedHours: number;
        IsWeekend: boolean;
        IsLastDayOfWeekend: boolean;
        IsHoliday: boolean;
        IsReadOnly: boolean;
    }

    export interface IClockCardDailyEntry {
        Day: string;
        InTime: string | null;
        OutTime: string | null;
        BreakDuration: string | null;
    }

    export interface IResponse<T> {
        _payload: T;
        _alerts: IResponseAlert[];
    }

    export interface IResponseAlert {
        IsDismissable: boolean;
        Message: string;
        Type: AlertMessages.AlertType;
    }

    export interface ITranslations {
        NoHoursContractedMessage: string;
        AddProjectPlaceholderText: string;
        TaskNamePlaceholderText: string;
        NameColumnHeader: string;
        WeekHeader: string;
        ShortWeekHeader: string;
        ConfirmDialogTitle: string;
        ConfirmDeleteTask: string;
        ConfirmDeleteProject: string;
        TaskIsReadOnly: string;
        ProjectIsReadOnly: string;
        TimeReportStatusNotStarted: string;
        TimeReportStatusDraft: string;
        RequestButtonText: string;
        ProjectTooltipTitle: string;
        None: string;
        TimeReportStatusSubmitted: string;
        TimeReportStatusDraftAcepted: string;
        TimeReportStatusDraftRejected: string;
        SaveButtonText: string;
        AddButtonText: string;
        ImportButtonText: string;
        SubmitButtonText: string;
        SubmitConfirmMessage: string;
        SubmitConfirmTitle: string;
        CurrentMonth: string;
        PreviousMonth: string;
        NextMonth: string;
        ReorderButton: string;
        AddLastMonthProjects: string;
        ReopenTimeReport: string;
        TimeReportTitle: string;
        ContractedHoursLabel: string;
        RegularHoursLabel: string;
        HourlyRateTypeOverTimeHoliday: string;
        HourlyRateTypeOverTimeLateNight: string;
        HourlyRateTypeOverTimeNormal: string;
        HourlyRateTypeOverTimeWeekend: string;
        HourlyRateTypeRegular: string;
        HourlyRateTypeOverTimeBank: string;
        EntryTypeHeader: string;
        OvertimeTooltip: string;
        NoOvertimeApprovalsMessage: string;
        OvertimeDetectedMessage: string;
        OvertimeExceededMessage: string;
        OvertimeNotMarkedMessage: string;
        Clone: string;
        SplitOvertime: string;
        FillContractedHoursLabel: string;
        WorkingLimitExceeded: string;
        DirtyFormConfirmTitleText: string;
        DirtyFormConfirmText: string;
        ReadOnlyHours: string;
        ProjectAlreadyPresent: string;
        ClockCardIn: string;
        ClockCardOut: string;
        ClockCardBreak: string;
        InTimeGreaterThanOutTime: string;
        ClockCardIncompatibleWithTimeReport: string;
        ClockCardRowIsReadOnly: string;
        FillTimeWithLastValueLabel: string;
        CalculateTimeLabel: string;
        ReportedHoursOnAbsenceDay: string;
        DayDictionary: { [id: number]: string };
    }

}
