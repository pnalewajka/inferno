﻿namespace TimeTracking.Store.Summaries {
    export interface ISummaryProjectEntry {
        projectId: number;
        date: Date;
        value: number;
    }

    export interface ISummaryDictionary extends IDictionary<number, number> {
    }

    export interface IDictionary<K, T> {
        [index: number]: T;
    }

    export interface ISummariesStoreState {
        summaryByProject: ISummaryDictionary;
        summaryByDate: ISummaryDictionary;
        summaryByTask: ISummaryDictionary;
        summaryRegularByDate: ISummaryDictionary;
        summaryAbsenceByDate: ISummaryDictionary;
        summaryOvertimeByDate: ISummaryDictionary;
        summaryNotPassiveWorkingHoursByDate: ISummaryDictionary;
        summaryByProjectAndDate: IDictionary<number, IDictionary<number, number>>;
        summaryContractedHours: number;
        summary: number;
    }

    const defaultState: ISummariesStoreState = {
        summaryByProject: [],
        summaryByDate: [],
        summaryByTask: [],
        summaryRegularByDate: [],
        summaryAbsenceByDate: [],
        summaryOvertimeByDate: [],
        summaryNotPassiveWorkingHoursByDate: [],
        summaryByProjectAndDate: [],
        summaryContractedHours: 0,
        summary: 0,
    };

    function getProjectPassiveWorkingTimeAttributeValues(project: Store.Projects.IProject): Array<number | null> {
        const passiveWorkingTimeAttributeValueIds = project.Attributes
            .map(a => a.PossibleValues)
            .reduce((p, c) => p.concat(c), [])
            .filter(v => Enum.hasFlag(v.Features, Store.Projects.AttributeValueFeature.PassiveWorkingTime))
            .map(v => v.Id);

        return passiveWorkingTimeAttributeValueIds;
    }

    function isPassiveWorkingHours(project: Store.Projects.IProject, task: Store.Tasks.ITask | TimeTrackingReact.ITimeTrackingTask) {
        const passiveWorkingTimeAttributeValueIds = getProjectPassiveWorkingTimeAttributeValues(project);

        return task.SelectedAttributesValueIds.some(
            v => v !== undefined
                && passiveWorkingTimeAttributeValueIds.indexOf(v) !== -1);
    }

    function getNotPassiveWorkingHours(project: Store.Projects.IProject, task: TimeTrackingReact.ITimeTrackingTask, date: number): number {
        if (isPassiveWorkingHours(project, task)) {
            return 0;
        }

        const taskDay = task.Days.filter(d => Utils.withoutTimezoneOffset(d.Day).getDate() === date);

        return taskDay[0] !== undefined
            ? taskDay[0].Hours
            : 0;
    }

    function calculateSummary(calculatedState: ISummariesStoreState, data: Actions.ILoadDataActionData): ISummariesStoreState {
        const viewModel = data.viewModel;
        const start = Date.now();

        const stateValue: ISummariesStoreState = Utils.Clone(defaultState);

        const dates: Date[] = viewModel.Days.map(d => Utils.withoutTimezoneOffset(d.Date));
        stateValue.summaryContractedHours = viewModel.Days.map(d => d.ContractedHours).reduce((a, b) => a + b, 0);

        let taskKey = 0;

        viewModel.Projects.forEach(project => {
            stateValue.summaryByProject[project.ProjectId] = stateValue.summaryByProject[project.ProjectId] || 0;

            project.Tasks.forEach(task => {
                taskKey++;
                stateValue.summaryByTask[taskKey] = 0;

                const isAbsenceTask = !!task.AbsenceTypeId;

                dates.forEach(day => {
                    const date = day.getDate();
                    const [taskDay] = task.Days.filter(td => Utils.withoutTimezoneOffset(td.Day).getDate() === date);
                    const hours = !!taskDay ? taskDay.Hours : 0;

                    stateValue.summaryByDate[date] = (stateValue.summaryByDate[date] || 0) + hours;
                    stateValue.summaryRegularByDate[date] = (stateValue.summaryRegularByDate[date] || 0) + (task.OvertimeVariant === Sheet.OvertimeVariantEnum.Regular ? hours : 0);
                    stateValue.summaryOvertimeByDate[date] = (stateValue.summaryOvertimeByDate[date] || 0) + (task.OvertimeVariant === Sheet.OvertimeVariantEnum.Regular ? 0 : hours);
                    stateValue.summaryNotPassiveWorkingHoursByDate[date] = (stateValue.summaryNotPassiveWorkingHoursByDate[date] || 0) + getNotPassiveWorkingHours(project as Store.Projects.IProject, task, date);
                    stateValue.summaryByTask[taskKey] += hours;
                    stateValue.summaryByProject[project.ProjectId] += hours;
                    stateValue.summary += hours;

                    if (isAbsenceTask) {
                        stateValue.summaryAbsenceByDate[date] = (stateValue.summaryAbsenceByDate[date] || 0) + hours;
                    }

                    if (!stateValue.summaryByProjectAndDate[project.ProjectId]) {
                        stateValue.summaryByProjectAndDate[project.ProjectId] = [];
                    }

                    if (!stateValue.summaryByProjectAndDate[project.ProjectId][date]) {
                        stateValue.summaryByProjectAndDate[project.ProjectId][date] = hours;
                    } else {
                        stateValue.summaryByProjectAndDate[project.ProjectId][date] += hours;
                    }
                });
            });
        });

        return stateValue;
    }

    function addProject(state: ISummariesStoreState, action: Actions.IAddProjectActionData): ISummariesStoreState {
        const newState: ISummariesStoreState = Utils.Clone(state);

        newState.summaryByProject[action.projectId] = 0;
        newState.summaryByProjectAndDate[action.projectId] = [];

        action.days.forEach(d => {
            newState.summaryByProjectAndDate[action.projectId][d.Date.getDate()] = 0;
        });

        return newState;
    }

    function changeHours(state: ISummariesStoreState, parameters: Actions.IChangeHoursActionData): ISummariesStoreState {
        const difference = parameters.newHours - parameters.oldHours;

        const byDate = state.summaryByDate[parameters.date.getDate()] || 0;
        const byProject = state.summaryByProject[parameters.project.ProjectId] || 0;
        const notPassiveByDate = state.summaryNotPassiveWorkingHoursByDate[parameters.date.getDate()] || 0;

        const byDateAndProject = (state.summaryByProjectAndDate[parameters.project.ProjectId] || [])[parameters.date.getDate()] || 0;

        const newState: ISummariesStoreState = Utils.Clone(state);

        newState.summaryByTask[parameters.task.Key] = (newState.summaryByTask[parameters.task.Key] || 0) + difference;
        newState.summary = state.summary + difference;

        newState.summaryByDate[parameters.date.getDate()] = byDate + difference;

        if (parameters.task.OvertimeVariant === Sheet.OvertimeVariantEnum.Regular) {
            newState.summaryRegularByDate[parameters.date.getDate()] = byDate + difference;
        } else {
            newState.summaryOvertimeByDate[parameters.date.getDate()] = byDate + difference;
        }

        if (!isPassiveWorkingHours(parameters.project, parameters.task)) {
            newState.summaryNotPassiveWorkingHoursByDate[parameters.date.getDate()] = notPassiveByDate + difference;
        }

        newState.summaryByProject[parameters.project.ProjectId] = byProject + difference;

        newState.summaryByProjectAndDate[parameters.project.ProjectId][parameters.date.getDate()] = byDateAndProject + difference;

        return newState;
    }

    function sumHours(days: { [index: number]: Store.Tasks.ITaskDay }): number {
        let sum = 0;

        for (const day in days) {
            if (days.hasOwnProperty(day)) {
                sum += days[day].Hours;
            }
        }

        return sum;
    }

    function removeTaskSummaries(state: ISummariesStoreState, data: Actions.IRemoveTaskActionData): ISummariesStoreState {
        const newState: ISummariesStoreState = Utils.Clone(state);

        const allHours = sumHours(data.task.Days);

        newState.summary -= allHours;

        newState.summaryByProject[data.task.ProjectId] -= allHours;

        for (const day in data.task.Days) {
            if (data.task.Days.hasOwnProperty(day)) {
                const dayItem = data.task.Days[day];
                const date = dayItem.Day.getDate();
                const dayHours = dayItem.Hours;

                newState.summaryByDate[date] -= dayHours;

                if (data.task.OvertimeVariant === Sheet.OvertimeVariantEnum.Regular) {
                    newState.summaryRegularByDate[date] -= dayHours;
                } else {
                    newState.summaryOvertimeByDate[date] -= dayHours;
                }

                if (!isPassiveWorkingHours(data.project, data.task)) {
                    newState.summaryNotPassiveWorkingHoursByDate[date] -= dayHours;
                }

                newState.summaryByProjectAndDate[data.task.ProjectId][date] -= dayHours;
            }
        }

        return newState;
    }

    function changeProject(state: ISummariesStoreState, data: Actions.IChangeProjectActionData): ISummariesStoreState {
        const newState: ISummariesStoreState = Utils.Clone(state);

        newState.summaryByProject[data.newProjectId] = newState.summaryByProject[data.project.ProjectId];
        delete newState.summaryByProject[data.project.ProjectId];

        newState.summaryByProjectAndDate[data.newProjectId] = newState.summaryByProjectAndDate[data.project.ProjectId];
        delete newState.summaryByProjectAndDate[data.project.ProjectId];

        // Remove hours from old attributes

        for (const key in data.oldTasks) {
            if (data.oldTasks.hasOwnProperty(key)) {
                const task = data.oldTasks[key];

                if (task.ProjectId !== data.project.ProjectId) {
                    continue;
                }

                if (!isPassiveWorkingHours(data.project, task)) {
                    for (const day in task.Days) {
                        if (task.Days.hasOwnProperty(day)) {
                            const dayItem = task.Days[day];

                            const date = dayItem.Day.getDate();
                            const dayHours = dayItem.Hours;

                            newState.summaryNotPassiveWorkingHoursByDate[date] -= dayHours;
                        }
                    }
                }
            }
        }

        return newState;
    }

    function removeProject(state: ISummariesStoreState, data: Actions.IRemoveProjectActionData): ISummariesStoreState {
        const projectId = data.project.ProjectId;
        const newState: ISummariesStoreState = Utils.Clone(state);

        const hours = newState.summaryByProject[projectId] || 0;

        newState.summary -= hours;

        for (const date in state.summaryByProjectAndDate[projectId]) {
            if (state.summaryByProjectAndDate[projectId].hasOwnProperty(date)) {
                const value = state.summaryByProjectAndDate[projectId][date];
                newState.summaryByDate[date] -= value;
            }
        }

        // Remove hours from old attributes

        for (const key in data.tasks) {
            if (data.tasks.hasOwnProperty(key)) {
                const task = data.tasks[key];

                if (!isPassiveWorkingHours(data.project, task)) {
                    for (const day in task.Days) {
                        if (task.Days.hasOwnProperty(day)) {
                            const dayItem = task.Days[day];
                            const date = dayItem.Day.getDate();
                            const dayHours = dayItem.Hours;

                            newState.summaryNotPassiveWorkingHoursByDate[date] -= dayHours;
                        }
                    }
                }
            }
        }

        delete newState.summaryByProject[projectId];
        delete newState.summaryByProjectAndDate[projectId];

        return newState;
    }

    function changeTaskOvertimeVariant(state: ISummariesStoreState, task: Store.Tasks.ITask, variant: Sheet.OvertimeVariantEnum): ISummariesStoreState {
        const wasOvertime = task.OvertimeVariant !== Sheet.OvertimeVariantEnum.Regular;
        const isOvertime = variant !== Sheet.OvertimeVariantEnum.Regular;

        if (wasOvertime !== isOvertime) {
            const newState: ISummariesStoreState = Utils.Clone(state);
            for (const day in task.Days) {
                if (task.Days.hasOwnProperty(day)) {
                    const dayItem = task.Days[day];
                    const date = dayItem.Day.getDate();
                    const dayHours = dayItem.Hours;

                    if (variant === Sheet.OvertimeVariantEnum.Regular) {
                        newState.summaryOvertimeByDate[date] -= dayHours;
                        newState.summaryRegularByDate[date] += dayHours;
                    } else {
                        newState.summaryOvertimeByDate[date] += dayHours;
                        newState.summaryRegularByDate[date] -= dayHours;
                    }
                }
            }

            return newState;
        }

        return state;
    }

    function cloneTask(state: ISummariesStoreState, data: Actions.ICloneTaskActionData): ISummariesStoreState {
        const newState: ISummariesStoreState = Utils.Clone(state);

        const { task, project } = data;
        let taskSumHours = 0;

        for (const day in task.Days) {
            if (task.Days.hasOwnProperty(day)) {
                const dayItem = task.Days[day];
                const date = dayItem.Day.getDate();
                const dayHours = dayItem.Hours;

                newState.summary += dayHours;
                newState.summaryByProject[task.ProjectId] += dayHours;

                newState.summaryByDate[date] = (newState.summaryByDate[date] || 0) + dayHours;
                newState.summaryOvertimeByDate[date] = (newState.summaryOvertimeByDate[date] || 0) + task.OvertimeVariant !== Sheet.OvertimeVariantEnum.Regular ? dayHours : 0;
                newState.summaryRegularByDate[date] += (newState.summaryRegularByDate[date] || 0) + task.OvertimeVariant === Sheet.OvertimeVariantEnum.Regular ? dayHours : 0;
                newState.summaryByProjectAndDate[task.ProjectId][date] += dayHours;
                newState.summaryNotPassiveWorkingHoursByDate[date] += !isPassiveWorkingHours(project, task) ? dayHours : 0;

                taskSumHours += dayHours;
            }
        }

        newState.summaryByTask[data.taskCounter + 1] = taskSumHours;

        return newState;
    }

    function fillContractedHours(state: ISummariesStoreState, project: Store.Projects.IProject, task: Store.Tasks.ITask, days: Store.DateRange.IDay[]): ISummariesStoreState {
        const taskDays = task.Days;

        const newState: ISummariesStoreState = Utils.Clone(state);

        const taskHours = newState.summaryByTask[task.Key];
        const contractedHours = days.map(d => d.ContractedHours).reduce((a, b) => a + b, 0);

        newState.summary += contractedHours - taskHours;
        newState.summaryByTask[task.Key] = contractedHours;
        newState.summaryByProject[task.ProjectId] += contractedHours - taskHours;

        days.filter(
            d => !d.IsReadOnly
                && (project.ReportingStartsOn === undefined
                    || project.ReportingStartsOn.getTime() <= d.Date.getTime()))
            .forEach(d => {
                const date = d.Date.getDate();
                const taskHoursInDate = taskDays[date];
                const contractedHoursInDate = d.ContractedHours;
                newState.summaryByDate[date] += contractedHoursInDate - (!!taskHoursInDate ? taskHoursInDate.Hours : 0);

                if (task.OvertimeVariant === Sheet.OvertimeVariantEnum.Regular) {
                    newState.summaryRegularByDate[date] -= (!!taskHoursInDate ? taskHoursInDate.Hours : 0) + contractedHoursInDate;
                } else {
                    newState.summaryOvertimeByDate[date] = (!!taskHoursInDate ? taskHoursInDate.Hours : 0) + contractedHoursInDate;
                }

                const projectHoursForDay = newState.summaryByProjectAndDate[task.ProjectId][date];

                if (!!projectHoursForDay) {
                    newState.summaryByProjectAndDate[task.ProjectId][date] -= (!!taskHoursInDate ? taskHoursInDate.Hours : 0);
                    newState.summaryByProjectAndDate[task.ProjectId][date] += contractedHoursInDate;
                } else {
                    newState.summaryByProjectAndDate[task.ProjectId][date] = contractedHoursInDate;
                }

                if (!isPassiveWorkingHours(project, task)) {
                    newState.summaryNotPassiveWorkingHoursByDate[date] += contractedHoursInDate;
                }
            });

        return newState;
    }

    function splitOvertime(state: ISummariesStoreState, data: Actions.ISplitOvertimeActionData): ISummariesStoreState {
        const newState = Utils.Clone(state);

        const currentTask = data.task;
        let newTaskSummary = 0;

        data.rangeDays.forEach(d => {
            const dateKey = d.Date.getDate();
            const taskDayEntry = currentTask.Days[dateKey];
            const contractedHours = d.ContractedHours;
            const isExciding = taskDayEntry.Hours > contractedHours;
            const overtimeHours = isExciding ? taskDayEntry.Hours - contractedHours : 0;
            newTaskSummary += overtimeHours;
            newState.summaryOvertimeByDate[dateKey] += overtimeHours;
            newState.summaryRegularByDate[dateKey] -= overtimeHours;
        });

        newState.summaryByTask[data.taskCounter + 1] = newTaskSummary;

        return newState;
    }

    function clearData(state: ISummariesStoreState): ISummariesStoreState {
        return {
            summaryByProject: [],
            summaryByDate: [],
            summaryByTask: [],
            summaryRegularByDate: [],
            summaryAbsenceByDate: [],
            summaryOvertimeByDate: [],
            summaryNotPassiveWorkingHoursByDate: [],
            summaryByProjectAndDate: [],
            summary: 0,
            summaryContractedHours: 0
        };
    }

    function changeAttributes(state: ISummariesStoreState, data: Actions.IChangeAttributesActionData): ISummariesStoreState {
        const newState: ISummariesStoreState = Utils.Clone(state);

        const oldTask = Utils.Clone(data.task);
        oldTask.SelectedAttributesValueIds = data.previousAttributeValueIds as number[];

        const newTask = Utils.Clone(data.task);
        newTask.SelectedAttributesValueIds = data.newAttributeValueIds as number[];

        const isOldPassive = isPassiveWorkingHours(data.project, oldTask);
        const isNewPassive = isPassiveWorkingHours(data.project, newTask);

        if (isOldPassive !== isNewPassive) {
            const multiplier = isOldPassive && !isNewPassive ? 1 : -1;

            for (const day in data.task.Days) {
                if (data.task.Days.hasOwnProperty(day)) {
                    const dayItem = data.task.Days[day];
                    const date = dayItem.Day.getDate();
                    const dayHours = dayItem.Hours;

                    newState.summaryNotPassiveWorkingHoursByDate[date] += dayHours * multiplier;
                }
            }
        }

        return newState;
    }

    export function summaryStore(state: ISummariesStoreState = defaultState, action: Actions.ITimeTrackingAction): ISummariesStoreState {
        switch (action.type) {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return calculateSummary(defaultState, action.data as Actions.ILoadDataActionData);
                }

            case Actions.TimeTrackingActionType.ClearData:
                {
                    return clearData(state);
                }

            case Actions.TimeTrackingActionType.SplitOvertime:
                {
                    return splitOvertime(state, action.data as Actions.ISplitOvertimeActionData);
                }

            case Actions.TimeTrackingActionType.ChangeHours:
                {
                    return changeHours(state, action.data as Actions.IChangeHoursActionData);
                }

            case Actions.TimeTrackingActionType.AddProject:
                {
                    return addProject(state, action.data as Actions.IAddProjectActionData);
                }

            case Actions.TimeTrackingActionType.RemoveTask:
                {
                    return removeTaskSummaries(state, action.data as Actions.IRemoveTaskActionData);
                }

            case Actions.TimeTrackingActionType.ChangeProject:
                {
                    return changeProject(state, action.data as Actions.IChangeProjectActionData);
                }

            case Actions.TimeTrackingActionType.RemoveProject:
                {
                    return removeProject(state, action.data as Actions.IRemoveProjectActionData);
                }

            case Actions.TimeTrackingActionType.ChangeOvertimeVariant:
                {
                    const data = action.data as Actions.IChangeOvertimeVariantActionData;

                    return changeTaskOvertimeVariant(state, data.task, data.newOvertimeVariant);
                }

            case Actions.TimeTrackingActionType.CloneTask:
                {
                    return cloneTask(state, action.data as Actions.ICloneTaskActionData);
                }

            case Actions.TimeTrackingActionType.ChangeAttributes:
                {
                    return changeAttributes(state, action.data as Actions.IChangeAttributesActionData);
                }

            case Actions.TimeTrackingActionType.FillContractedHours:
                {
                    const data = action.data as Actions.IFillContractedHoursActionData;

                    return fillContractedHours(state, data.project, data.task, data.days);
                }
        }

        return state;
    }
}
