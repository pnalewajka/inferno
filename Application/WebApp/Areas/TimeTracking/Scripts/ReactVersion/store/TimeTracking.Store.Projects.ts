﻿namespace TimeTracking.Store.Projects {
    export interface IProject {
        Apn?: string;
        IsReadOnly: boolean;
        ProjectId: number;
        KupferwerkProjectId?: string;
        ProjectName: string;
        ReportingStartsOn?: Date;
        AllowedProjectOvertime: TimeTracking.Sheet.OvertimeVariantEnum;
        Attributes: TimeTrackingReact.IProjectAttribute[];
    }

    export enum AttributeValueFeature {
        None = 0,
        NonBillable = 1 << 0,
        PassiveWorkingTime = 1 << 1,
    }

    export interface IProjectsStoreState {
        projects: IProject[];
    }

    function addProject(state: IProjectsStoreState, data: Actions.IAddProjectActionData): IProjectsStoreState {
        const newProject: IProject = {
            ProjectId: data.projectId,
            ProjectName: data.projectName,
            Apn: data.metadata.Apn,
            KupferwerkProjectId: data.metadata.KupferwerkProjectId,
            IsReadOnly: false,
            ReportingStartsOn: new Date(data.metadata.ReportingStartsOn as string),
            Attributes: [...data.metadata.ProjectAttributes],
            AllowedProjectOvertime: data.metadata.AllowedProjectOvertime
        };

        return { projects: [...state.projects, newProject] };
    }

    function moveProject(state: IProjectsStoreState, projectId: number, direction: Direction) {
        const { projects } = Store.defaultStore().getState().projects;
        const index = Dante.Utils.findIndex(projects, p => p.ProjectId === projectId);
        if (index !== undefined) {
            const directionOffset = direction === Direction.Up ? -1 : 1;
            const orderedProjects = Dante.Utils.swapElements(projects, index, index + directionOffset);
            return { projects: orderedProjects };
        }
        return state;
    }

    function moveProjectUp(state: IProjectsStoreState, data: Actions.IReorderProjectUp): IProjectsStoreState {
        return moveProject(state, data.projectId, Direction.Up);
    }

    function moveProjectDown(state: IProjectsStoreState, data: Actions.IReorderProjectDown): IProjectsStoreState {
        return moveProject(state, data.projectId, Direction.Down);
    }

    function removeTask(state: IProjectsStoreState, data: Actions.IRemoveTaskActionData): IProjectsStoreState {
        if (data.isLastTask) {
            return Utils.Assign(state, {
                projects: [...state.projects.filter(t => t.ProjectId !== data.task.ProjectId)]
            });
        }

        return state;
    }

    function changeProject(state: IProjectsStoreState, data: Actions.IChangeProjectActionData): IProjectsStoreState {
        return Utils.Assign(state, {
            projects: [
                ...state.projects.map(p => {
                    if (p.ProjectId === data.project.ProjectId) {
                        return Utils.Assign(p, {
                            ProjectId: data.newProjectId,
                            ProjectName: data.newProjectName,
                            Apn: data.newProjectMetadata.Apn,
                            KupferwerkProjectId: data.newProjectMetadata.KupferwerkProjectId,
                            Attributes: data.newProjectMetadata.ProjectAttributes,
                            ReportingStartsOn: new Date(data.newProjectMetadata.ReportingStartsOn as string),
                            IsReadOnly: false,
                        });
                    }

                    return p;
                })]
        });
    }

    function removeProject(state: IProjectsStoreState, data: Actions.IRemoveProjectActionData): IProjectsStoreState {
        return Utils.Assign(state, {
            projects: [...state.projects.filter(t => t.ProjectId !== data.project.ProjectId)]
        });
    }

    function loadData(state: IProjectsStoreState, data: Actions.ILoadDataActionData): IProjectsStoreState {

        const allProjects: IProjectsStoreState = { projects: [...data.viewModel.Projects] } as IProjectsStoreState;
        allProjects.projects.forEach(p => p.ReportingStartsOn = new Date(p.ReportingStartsOn as Date));

        return allProjects;
    }

    function clearData(state: IProjectsStoreState): IProjectsStoreState {
        return {
            projects: []
        };
    }

    export function projectsStore(state: IProjectsStoreState = { projects: [] }, action: Actions.ITimeTrackingAction): IProjectsStoreState
    {
        switch (action.type)
        {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return loadData(state, action.data as Actions.ILoadDataActionData);
                }

            case Actions.TimeTrackingActionType.ClearData:
                {
                    return clearData(state);
                }

            case Actions.TimeTrackingActionType.AddProject:
                {
                    return addProject(state, action.data as Actions.IAddProjectActionData);
                }

            case Actions.TimeTrackingActionType.ReorderProjectUp:
                {
                    return moveProjectUp(state, action.data as Actions.IReorderProjectUp);
                }
            case Actions.TimeTrackingActionType.ReorderProjectDown:
                {
                    return moveProjectDown(state, action.data as Actions.IReorderProjectDown);
                }
            case Actions.TimeTrackingActionType.AddProject:
                {
                    return addProject(state, action.data as Actions.IAddProjectActionData);
                }

            case Actions.TimeTrackingActionType.ChangeProject:
                {
                    return changeProject(state, action.data as Actions.IChangeProjectActionData);
                }

            case Actions.TimeTrackingActionType.RemoveTask:
                {
                    return removeTask(state, action.data as Actions.IRemoveTaskActionData);
                }

            case Actions.TimeTrackingActionType.RemoveProject:
                {
                    return removeProject(state, action.data as Actions.IRemoveProjectActionData);
                }

        }

        return state;
    }
}
