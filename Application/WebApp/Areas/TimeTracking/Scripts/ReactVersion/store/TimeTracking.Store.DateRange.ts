﻿module TimeTracking.Store.DateRange {
    export interface IDay {
        Date: Date;
        ContractedHours: number;
        DailyWorkingLimit?: number;
        IsWeekend: boolean;
        IsHoliday: boolean;
        IsLastDayOfWeek: boolean;
        IsReadOnly: boolean;
        HolidayName: string;
    }

    export interface IWeek {
        WeekDays: number;
        WeekNumber: number;
    }

    export interface IDateRangeState {
        days: IDay[];
        weeks: IWeek[];
    }

    function loadData(state: IDateRangeState, data: Actions.ILoadDataActionData): IDateRangeState {
        return {
            days: data.viewModel.Days.map(d => $.extend({}, d, { Date: Utils.withoutTimezoneOffset(d.Date) })),
            weeks: data.viewModel.Weeks
        };
    }

    export function dateRangeStore(state: IDateRangeState = { days: [], weeks: [] }, action: Actions.ITimeTrackingAction): IDateRangeState
    {
        switch (action.type)
        {
            case Actions.TimeTrackingActionType.LoadData:
                {
                    return loadData(state, action.data as Actions.ILoadDataActionData);
                }
        }

        return state;
    }
}
