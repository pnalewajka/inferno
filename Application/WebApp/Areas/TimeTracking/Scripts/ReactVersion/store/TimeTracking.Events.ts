﻿module TimeTracking.Events
{
    export const components = Dante.Utils.Lazy(() => new Atomic.Events.EventDispatcher<Actions.ITimeTrackingAction>());
}
