﻿namespace TimeTracking.Sheet {
    export class ContractedHours extends React.Component<{}, { contractedHours: number, reportedHours: number }> {
        unsubscriber: Redux.Unsubscribe | null = null;

        componentWillMount() {
            this.unsubscriber = Store.defaultStore().subscribe(this.updateState.bind(this));
            this.updateState();
        }

        updateState() {
            const { summaries } = Store.defaultStore().getState();
            this.setState({ contractedHours: summaries.summaryContractedHours, reportedHours: summaries.summary });
        }

        componentWillUnmount() {
            if (this.unsubscriber != null)
                this.unsubscriber();
        }

        shouldComponentUpdate(nextProps: {}, nextState: { contractedHours: number, reportedHours: number }) {
            return this.state.reportedHours !== nextState.reportedHours || this.state.contractedHours !== nextState.contractedHours;
        }

        displayValue(value: number): string {
            if (value === 0) {
                return "0";
            }

            return (+(Math.round(value * 100) / 100).toFixed(2)).toString();
        }

        render() {
            if (!this.state) {
                return null;
            }

            let reportProgress: number = this.state.contractedHours === 0 ? 0 : this.state.reportedHours * 100 / this.state.contractedHours;
            let overContractedHours: number = this.state.reportedHours - this.state.contractedHours;

            if (reportProgress > 100) {
                reportProgress = 100;
            } else if (reportProgress < 0) {
                reportProgress = 0;
            }
            var cssClass = Utils.ClassNames(
                {
                    "container-progress": true,
                    "warning": reportProgress < 30
                }
            );

            return (
                <div className="time-report-progress-bar">
                    <span className={cssClass} style={{ width: `${reportProgress}%` }}>
                    </span>
                    <div className="container-wrapper">
                        <span className="container-summary">
                            {`${Sheet.Translations.ContractedHoursLabel} ${this.displayValue(this.state.contractedHours || 0)}h`}
                        </span>
                        {
                            overContractedHours > 0
                                ? <Ui.Tooltip text={Sheet.Translations.OvertimeTooltip}>
                                    <span className="container-summary container-summary-overtime">{`(+${Dante.Utils.DisplayNumber(overContractedHours)}h)`}</span>
                                </Ui.Tooltip>
                                : null
                        }
                        <span className="container-sigma">&Sigma;</span>
                    </div>
                </div>
            );
        }
    }
}