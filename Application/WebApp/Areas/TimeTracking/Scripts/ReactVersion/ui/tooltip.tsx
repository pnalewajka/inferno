﻿module TimeTracking.Ui {
    export interface ITooltipProps extends React.HTMLProps<any> {
        text: string | null;
    }

    export class Tooltip extends React.Component<ITooltipProps, {}> {
        private $item: JQuery;

        private atachTooltip(item: HTMLElement | null, text: string | null): void {
            if (item != null && !Strings.isNullOrEmpty(text)) {
                this.$item = $(item).tooltip({ container: $("body"), title: () => text });
            } else if (this.$item != null) {
                this.$item.tooltip("destroy");
            }
        }

        render() {
            return (
                <div
                    className={`ui-tooltip ${this.props.className}`}
                    key={Strings.isNullOrEmpty(this.props.text) ? "empty_" : `not_empty_${this.props.text}`}
                    ref={item => this.atachTooltip(item as HTMLElement, this.props.text)} >
                    {this.props.children}
                </div>
            );
        }
    }
}
