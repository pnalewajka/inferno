﻿module TimeTracking.Ui {
    export interface IButtonProps {
        text: string;
        icon?: string;
        disabled?: boolean;
        primary?: boolean;
        onClick: () => void;
    }

    export let Button: React.StatelessComponent<IButtonProps>
        = (props: IButtonProps) =>
            (
                <button
                    className={`btn btn-${!!props.primary ? "primary": "default" }` }
                    disabled={props.disabled}
                    onClick={props.onClick}>
                    {!!props.icon ? <i className={`fa ${props.icon}`}></i> : null}
                    {props.text}
                </button>
            );
}