﻿module TimeTracking.Ui {
    export interface IButtonGroupProps {
        children?: React.ReactNode;
    }

    export let ButtonGroup: React.StatelessComponent<IButtonGroupProps> = (props: IButtonGroupProps) => <div className="btn-group">{props.children}</div>;
}