﻿module TimeTracking.Ui {
    export interface IButtonDropdownItem {
        url: string;
        name: string;
        method?: string;
        action?: () => void;
        onSuccess?: (data: any) => void;
    }
    export interface IButtonDropdownProps {
        label?: string;
        additionalClassName?: string;
        confirm?: () => JQueryPromise<any>;
        items: IButtonDropdownItem[];
    }

    export let ButtonDropdown: React.StatelessComponent<IButtonDropdownProps> = (props: IButtonDropdownProps) => (
        <div className="btn-group">
            <button type="button" className={`btn dropdown-toggle btn-default ${props.additionalClassName}`} data-toggle="dropdown">
                {props.label} <span className="caret"></span>
            </button>
            <ul className="dropdown-menu" role="menu">
                {props.items.map(i => (
                    <li key={i.name}>
                        <a
                            href={i.url}
                            onClick={(e) => {
                                e.preventDefault();
                                (props.confirm ? props.confirm() : resolvedPromise()).then(() => {
                                    i.action ? i.action() : navigate(i);
                                });
                            }}
                            data-new-tab="false">{i.name}</a>
                    </li>
                ))}
            </ul>
        </div>
    );

    function resolvedPromise(): JQueryPromise<any> {
        var deffered = $.Deferred();
        deffered.resolve();
        return deffered.promise();
    }

    function navigate(item: IButtonDropdownItem): void {
        if (item.method === "GET") {
            window.location.href = item.url;
        } else {
            Store.ajaxPost(item.url).then(data => {
                if (!!item.onSuccess) {
                    item.onSuccess(data);
                }
            });
        }
    }

    export function handleDropdownPosition() {
        var dropdownMenu: JQuery;

        $(window).on('show.bs.dropdown', function (e) {
            const $element = $(e.target);
            if ($element.parents('.time-report-page').length > 0) {

                dropdownMenu = $element.find('.dropdown-menu');
                if (dropdownMenu.length > 0) {
                    $('body').append(dropdownMenu.detach());

                    var eOffset = $element.offset();

                    if (eOffset.top + dropdownMenu.height() > $(window).height()) {
                        dropdownMenu.css({
                            'display': 'block',
                            'top': eOffset.top - dropdownMenu.height() - $element.height(),
                            'left': eOffset.left
                        });
                    } else {
                        dropdownMenu.css({
                            'display': 'block',
                            'top': eOffset.top + $element.height(),
                            'left': eOffset.left
                        });
                    }
                }
            }
        });

        $(window).on('hide.bs.dropdown', function (e) {
            const $element = $(e.target);
            if ($element.parents('.time-report-page').length > 0) {
                $element.append(dropdownMenu.detach());
                dropdownMenu.hide();
            }
        });
    }
}