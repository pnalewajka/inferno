﻿declare module TimeReport {
    export interface ITimeReportViewModel extends Grid.IRow {
        url: string;
        editOnBehalfUrl: string;
        employeeId: string;
        year: string;
        month: string;
    }
}