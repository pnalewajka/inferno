﻿interface ITableIndex {
    tr: number;
    td: number;
}

namespace AbsenceSchedule {
    let totalAbsenceHours: number = 0;
    let isMouseDown: boolean = false;
    let startIndex: ITableIndex | null;

    export function onMouseDownOnCell(event: JQueryEventObject) {
        isMouseDown = true;
        const trIndex = $(event).closest("tr").index();
        const tdIndex = $(event).closest("td").index();
        startIndex = { tr: trIndex - 1, td: tdIndex - 1 };
    }

    export function onMouseUpOnBody(event: JQueryEventObject) {
        isMouseDown = false;
        startIndex = null;
    }

    export function onHover(event: EventTarget) {
        if (!isMouseDown) {
            return;
        }

        const target = $(event);

        const trIndex = target.closest("tr").index();
        const tdIndex = target.closest("td").index();

        if (startIndex === null) {
            return;
        }

        const parentId = target.attr("tbody-id");
        selectInRange(startIndex, { tr: trIndex, td: tdIndex }, parentId);
    }

    export function tableCellClick(event: JQueryEventObject): void {
        const target = $(event);

        const isMarked = target.attr("is-marked") === "true";

        selectCell(target, isMarked);
    }

    function updateTotalAbsenceHours(hourChange: number): void {
        totalAbsenceHours += hourChange;
        totalAbsenceHours = parseFloat(totalAbsenceHours.toFixed(2));

        $("#total-absence-container").css("visibility", totalAbsenceHours === 0 ? "hidden" : "visible");
        $("#total-absence-hours").html(totalAbsenceHours.toString());
    }

    function toggleTableCell(target: JQuery, mark: boolean): void {
        target.toggleClass("select-cell", !mark);
        target.attr("is-marked", (!mark).toString());
    }

    function selectCell(target: JQuery, unmark: boolean) {
        if (target.hasClass("weekend")) {
            return;
        }

        const hours = (unmark ? -1 : 1) * Number(target.attr("working-hours"));

        if (hours === undefined || isNaN(hours)) {
            return;
        }

        updateTotalAbsenceHours(hours);
        toggleTableCell(target, unmark);
    }

    function selectInRange(start: ITableIndex, end: ITableIndex, parentId: string) {
        for (let td = end.td - start.td; td > 0; td--) {
            for (let tr = end.tr - start.tr; tr > 0; tr--) {
                const cell = $(`#${parentId} tr:eq(${start.tr + tr}) td:eq(${start.td + td})`);
                const isMarked = cell.attr("is-marked") === "true";

                if (isMarked) {
                    continue;
                }

                selectCell(cell, false);
            }
        }
    }
}

$(document).ready(() => {
    $("td").hover((event) => {
        if (event.currentTarget == null) {
            return;
        }
        AbsenceSchedule.onHover(event.currentTarget);
    });
});
