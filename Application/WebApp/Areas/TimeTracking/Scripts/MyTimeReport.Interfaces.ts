﻿declare module MyTimeReport {

    export interface IProject {
        Id: number;
        Name: string;
        ClientName?: string;
        PetsCode?: string;
        ProjectManagerName?: string;
        JiraCode?: string;
    }
    
    export interface ITimeTrackingTaskEntry {
        Day: Date;
        Hours: number;
    }

    export interface ITimeTrackingTask {
        Id?: number;
        ParentProject?: any;
        Name: string;
        AbsenceTypeId: number;
        ImportSourceId: number;
        ImportedTaskCode: string;
        ImportedTaskType: string;
        OvertimeVariant: number;
        Days: ITimeTrackingTaskEntry[];
        SelectedAttributesValueIds: number[];
    }

    export interface ITimeTrackingProject {
        ProjectId: number;
        ProjectName: string;
        IsReadOnly?: boolean,
        Tasks: ITimeTrackingTask[];
    }

    export interface IDayData {
        contractedHours: number;
        usedOvertimeHours: number;
        usedRegularHours: number;
    }

    export interface IDictionary<T> {
        [index: string]: T;

    }

    export interface IDayDataDictionary extends IDictionary<IDayData> {

    }

    export interface ITimeTrackingViewModel {
        projects: ITimeTrackingProject[];
        days: IDayDataDictionary;
        overtimeApprovals: IOvertimeApproval[];
    }

    export interface IChangePromise {
        pristine: () => void;
        dirty?: () => void;
    }

    export interface IOvertimeApproval {
        dateFrom: Date;
        dateTo: Date;
        hourLimit: number;
        projectId: number;

        hourLimitUsed?: number;
        overtimeHourUsed?: number;

        excitedAfter?: Date;
    }
}
