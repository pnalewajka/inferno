﻿module ProjectDashboard {
    import EventArgs = Dialogs.EventArgs;
    export interface IProjectDashboardRow extends Grid.IRow {
        employeeId: number;
        projectId: number;
        requestId: number;
    }

    export function showReportDialog(grid: Grid.GridComponent, dialogBaseUrl: string): void {
        var gridData = grid.getSelectedRowData<IProjectDashboardRow>();
        var employeeIds = gridData.map(r => r.employeeId).join(",");
        var projectIds = gridData.map(r => r.projectId).join(",");

        dialogBaseUrl = UrlHelper.updateUrlParameter(dialogBaseUrl, "project-ids", projectIds);
        dialogBaseUrl = UrlHelper.updateUrlParameter(dialogBaseUrl, "employee-ids", employeeIds);

        Dialogs.showDialog({
            actionUrl: dialogBaseUrl, 
            eventHandler: (eventArgs: EventArgs) => { }
        });
    }

    export function changeFilterDate(year: number, month: number) {
        let urlSearch = window.location.search;
        urlSearch = UrlHelper.updateUrlParameter(urlSearch, "year", year.toString());
        urlSearch = UrlHelper.updateUrlParameter(urlSearch, "month", month.toString());
        window.location.search = urlSearch;
    }

    export function viewRequest(rowData: IProjectDashboardRow, baseUrl: string): void {
        if (!rowData.requestId) {
            return;
        }

        window.location.href = UrlHelper.updateUrlParameter(baseUrl, "id", rowData.requestId.toString());
    }
}