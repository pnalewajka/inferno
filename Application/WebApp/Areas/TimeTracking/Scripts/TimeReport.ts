﻿/// <reference path="../../../Scripts/Atomic/CommonDialogs.ts"/>
/// <reference path="MyTimeReport.ts" />

module TimeReport {
    export function viewTimeReport(grid: Grid.GridComponent) {
        var selectedReports = grid.getSelectedRowData<ITimeReportViewModel>();

        if (selectedReports.length !== 1) {
            return;
        }

        var selectedReport = selectedReports[0];

        Forms.redirect(selectedReport.url);
    }

    export function editOnBehalf(grid: Grid.GridComponent) {
        var selectedReports = grid.getSelectedRowData<ITimeReportViewModel>();

        if (selectedReports.length !== 1) {
            return;
        }

        var selectedReport = selectedReports[0];

        Forms.redirect(selectedReport.editOnBehalfUrl);
    }

    export function statistics(grid: Grid.GridComponent, statisticsUrl: string) {
        var selectedReports = grid.getSelectedRowData<ITimeReportViewModel>();

        if (selectedReports.length !== 1) {
            return;
        }
        var selectedReport = selectedReports[0];

        Dialogs.showDialog({
            actionUrl: statisticsUrl,
            formData:
            {
                "employeeId": selectedReport.employeeId,
                "year": selectedReport.year,
                "month": selectedReport.month
            }
        });

    }

    export function reopenTimeReport(grid: Grid.GridComponent, reopenUrl: string) {
        var selectedReports = grid.getSelectedRowData<ITimeReportViewModel>();

        if (selectedReports.length !== 1) {
            return;
        }
        var selectedReport = selectedReports[0];

        Dialogs.showDialog({
            actionUrl: reopenUrl,
            formData:
            {
                "employeeId": selectedReport.employeeId,
                "year": selectedReport.year,
                "month": selectedReport.month
            }
        });
    }

    export function TilesOverflowing(): void {
        $.each($('.properties'), function (this: JQuery, index, value) {
            if (value.offsetHeight > 40) {
                $(this).find('.properties-text').addClass('hidden-text');
            }

            $(this).css('height', 20);
        });
    };

    export function initializeOvertimeIndicators() {
        $(".task-row").each(function (this: JQuery) {
            changeIndicatorIconsVisibility($(this));
        });
    }

    export function changeIndicatorIconsVisibility($taskRow: JQuery) {
        const $overtimeIndicator = $taskRow.find(".overtime-indicator");

        const $taskOvertimeVariantInput = $taskRow.find("input.overtime-variant");
        const overtimeVariant = $taskOvertimeVariantInput.val();
        const indicatorVisible = overtimeVariant > 0;

        $overtimeIndicator.toggle(indicatorVisible);
    }

    export function sendToControlling(grid: Grid.GridComponent) {
        let records = grid.getSelectedRowData<Grid.IRow>();
        let htmlList = Utils.getHtmlList(records);
        let confirmationBody;
        let url = "../TimeReport/SendToControlling";

        if (htmlList.length > 0) {
            confirmationBody = Globals.resources.SendSelectedTimeReportsToControlling + htmlList;

            let ids = records.map((v) => v.id.toString()).join(",");
            url = `${url}?ids=${ids}`;
        } else {
            confirmationBody = Globals.resources.SendAllTimeReportsToControlling;

            let year = UrlHelper.getQueryParameter(grid.gridContext, 'time-report-year');
            let month = UrlHelper.getQueryParameter(grid.gridContext, 'time-report-month');

            if (year && month) {
                url = `${url}?year=${year}&month=${month}`;
            }
        }

        CommonDialogs.confirm(
            confirmationBody,
            Globals.resources.SendToControlling,
            (eventArgs) => {
                if (eventArgs.isOk) {
                    Forms.submit(url, "POST");
                }
            });
    }

    export function sendInvoiceNotification(grid: Grid.GridComponent) {
        const records = grid.getSelectedRowData<Grid.IRow>();
        const htmlList = Utils.getHtmlList(records);

        let confirmationBody;
        let url = "../TimeReport/SendInvoiceNotification";

        if (htmlList.length > 0) {
            confirmationBody = Globals.resources.SendSelectedInvoiceNotifications + htmlList;

            url = `${url}?ids=${records.map((v) => v.id.toString()).join(",")}`;
        } else {
            confirmationBody = Globals.resources.SendAllInvoiceNotifications;

            const year = UrlHelper.getQueryParameter(grid.gridContext, 'time-report-year');
            const month = UrlHelper.getQueryParameter(grid.gridContext, 'time-report-month');

            if (year && month) {
                url = `${url}?year=${year}&month=${month}`;
            }
        }

        CommonDialogs.confirm(
            confirmationBody,
            Globals.resources.SendInvoiceNotification,
            (eventArgs) => {
                if (eventArgs.isOk) {
                    Forms.submit(url, "POST");
                }
            });
    }

    export function acceptInvoicesForPayment(grid: Grid.GridComponent) {
        const records = grid.getSelectedRowData<Grid.IRow>();
        const htmlList = Utils.getHtmlList(records);

        if (htmlList.length > 0) {
            CommonDialogs.confirm(
                Globals.resources.AcceptSelectedInvoicesForPayment + htmlList,
                Globals.resources.AcceptInvoiceForPayment,
                (eventArgs) => {
                    if (eventArgs.isOk) {
                        const url = `../TimeReport/AcceptInvoicesForPayment?ids=${records.map((v) => v.id.toString()).join(",")}`;

                        Forms.submit(url, "POST");
                    }
                });
        }
    }

    export function rejectInvoices(grid: Grid.GridComponent, actionUrl: string) {
        const records = grid.getSelectedRowData<Grid.IRow>();
        const htmlList = Utils.getHtmlList(records);

        Dialogs.showDialog({
            actionUrl: actionUrl,
            formData:
            {
                "timeReportIds": records.map((v) => v.id.toString()).join(","),
                "message": Globals.resources.RejectSelectedInvoices + htmlList
            }
        });
    }

    export function uploadInvoice(grid: Grid.GridComponent, actionUrl: string) {
        var selectedReports = grid.getSelectedRowData<ITimeReportViewModel>();

        if (selectedReports.length !== 1) {
            return;
        }

        var selectedReport = selectedReports[0];

        actionUrl = UrlHelper.updateUrlParameters(actionUrl,
            `year=${selectedReport.year}&month=${selectedReport.month}&employeeId=${selectedReport.employeeId}`);

        Forms.redirect(actionUrl);
    }
};

$(() => {
    TimeReport.TilesOverflowing();
    MyTimeReport.initializeOvertimeIndicators();
});