﻿module MyTimeReport {

    export var overtimeApprovals: IOvertimeApproval[];

    const contextMenu: JQuery = $("#taskRowContextMenu");
    const errorClass: string = "tt-error";
    const overtimeDetectedMessage = Globals.resources["OvertimeDetectedMessage"];

    const taskHoursInvalidValueMessage = Globals.resources["TaskHoursInvalidValueMessage"];

    const noHoursContractedMessage = Globals.resources["NoHoursContractedMessage"];
    const overtimeExcitedMessage = Globals.resources["OvertimeExceededMessage"];
    const overtimeNotMarkedMessage = Globals.resources["OvertimeNotMarkedMessage"];
    const noOvertimeApprovalsMessage = Globals.resources["NoOvertimeApprovalsMessage"];
    const vacationDayValidationMessage = Globals.resources["VacationDayValidationMessage"];

    export var projectControllerIdentifier = "TimeTracking.EmployeeProjects";

    export var $fixedColumns = $(".fix-horizontal", $(".grid-time-sheet .time-sheet"));

    function addInvalidClass(element: JQuery): JQuery {
        if (!element.hasClass(errorClass)) {
            element.addClass(errorClass);
        }
        return element;
    }

    function removeInvalidClass(element: JQuery): JQuery {
        return element.removeClass(errorClass);
    }

    function validateHours($hoursInputs: JQuery): boolean {
        let result = true;

        $hoursInputs.each((index, element) => {
            const $hoursInput = $(element);
            const value: number = parseHours($hoursInput.val());

            if (value > 24 || value < 0) {
                addInvalidClass($hoursInput);
                result = false;
            } else {
                removeInvalidClass($hoursInput);
                result = result && true;
            }
        });

        return result;
    }

    function recalculateProjectHours($projectRow: JQuery): void {
        const hoursDictionary = calculateProjectHours($projectRow);
        let hoursSum = 0;

        $(".project-row td.hours[data-day]", $projectRow).each((index, element) => {
            const $element = $(element);
            const dateKey: number = $element.data("day");

            $("span", $element).text(getFormattedHours(hoursDictionary[dateKey], true) || '-');
            hoursSum += parseHours($element.text());
        });

        $(".project-hour-sum", $projectRow).text(getFormattedHours(hoursSum));
    }

    function calculateProjectHours(projectRow: JQuery): { [key: number]: number } {
        let result: { [key: number]: number } = {};

        $(".task-row input.hours[data-day]", projectRow).each((index, element) => {
            const dayKey: number = $(element).data("day");
            const dayHours: number = parseHours($(element).val());

            result[dayKey] = (result[dayKey] || 0) + dayHours;
        });

        return result;
    }

    function getFormattedHours(input: number, changeZerosToDashes: boolean = false): string {
        const separator = Globals.getDecimalSeparator();

        let roundedHourValues = roundHourValue(input).toString().replace(/,/g, separator).replace(/\./g, separator);

        return changeZerosToDashes
            ? roundedHourValues.replace(/^0$/, '-')
            : roundedHourValues;
    }

    function roundHourValue(input: number): number {
        return +(Math.round(input * 100) / 100).toFixed(2);
    }

    function parseHours(input: any): number {
        const value = parseFloat(parseFloat(input.toString().replace(/,/g, ".")).toFixed(2));
        return value === NaN || !value ? 0 : value;
    }

    function convertHoursValue(this: JQuery): void {
        const $input = $(this);
        const value = parseHours($input.val());

        if ($input.val().toString() !== value.toString() || $input.val() == 0) {
            if (value != 0) {
                $input.val(value);
            }
            else {
                $input.val('');
            }

            $input.trigger("change");
        }
    }

    function hourInputFocusSelectHandler(this: JQuery) {
        const $input = $(this);

        if ($input.val() === "0") {
            $input.select();
        }
    }

    function hourInputEnterPressHandler(this: JQuery, event: JQueryEventObject) {
        const $input = $(this);

        if (event.type === "keyup" && event.which === 13) {
            $input.parent().next('td.hours').find('input.hours').select();
        }
    }

    function hourInputChangedHandler(this: JQuery) {
        const $input = $(this);
        const currentVal = $input.val();
        const oldValueName = 'inputChangeOldValue';

        var oldValue = $input.data(oldValueName) || "";

        if (currentVal === oldValue) {
            return;
        }

        handleHourInputChange($input);
        $input.data(oldValueName, currentVal);
    }

    function handleHourInputChange($input: JQuery) {

        const $projectRow = elementProjectContainer($input);
        const dayIndex = $input.data("day");

        const $dayTaskHours = $projectRow.find(`input[data-day='${dayIndex}']`);
        let hoursSum = 0;
        let isValidDay = true;

        $dayTaskHours.each((i, dayHour) => {
            const $dayInput = $(dayHour);
            hoursSum += parseHours($dayInput.val());
            isValidDay = validateHours($input) && isValidDay;
        });

        const formatedHoursSum = getFormattedHours(hoursSum, true);

        $projectRow.find(`.project-row .hours[data-day='${dayIndex}']`).find("span").text(formatedHoursSum);

        const $headerDayCell = $(`th.day-number[data-day='${dayIndex}'][data-contracted-hours]`);

        if (!isValidDay) {
            addInvalidClass($headerDayCell);
        } else {
            removeInvalidClass($headerDayCell);
        }

        recalculateHoursSummary();
        validateVacationsHours();
        recalculateTaskAndProjectHoursSummary($input);
    }

    function validateVacationsHours(form: ITimeTrackingViewModel = getSubmitObject()) {
        var vacationDays = getVacationDays();
        var vacationDaysReported: number[] = [];
        var $headers = $("#time-sheet thead .day-number[data-day]").removeClass("vacation-day-validation");

        form.projects.forEach(p => {
            p.Tasks
                .filter(t => !t.AbsenceTypeId)
                .forEach(t => {
                    var reporedOnVacation = t.Days
                        .map((d, i) => { return { Index: i, Day: d.Day, Hours: d.Hours } })
                        .filter((d) => vacationDays.indexOf(d.Index) !== -1 && d.Hours > 0);
                    reporedOnVacation.forEach(d => {
                        if (vacationDaysReported.indexOf(d.Index) === -1) {
                            vacationDaysReported.push(d.Index);
                        }
                    });
                });
        });

        vacationDaysReported.forEach(index => {
            $headers.filter(`[data-day="${index}"]`).addClass("vacation-day-validation");
        });
    }

    function recalculateHoursSummary() {
        let hoursSum = 0;
        $("#time-sheet thead th.day-number[data-day]").each((index, element) => {
            let dayIndex = $(element).data("day");
            let dayHoursSum = 0;
            $(`.project-row .hours[data-day='${dayIndex}'] span`).each((pIndex, pElement) => {
                const $day = $(pElement);
                dayHoursSum += parseHours($day.text());
            });

            const $daySummary = $(`.hours-sum[data-day='${dayIndex}']`);
            const parseDayHoursSum = getFormattedHours(dayHoursSum, true);

            $daySummary.text(parseDayHoursSum);
            hoursSum += dayHoursSum;
        });

        $(".hours-summary-row .hours-summary-column").text(getFormattedHours(hoursSum));
    }

    function recalculateTaskAndProjectHoursSummary(input: JQuery) {
        let taskHoursSum = 0;
        const $taskRow = elementTaskContainer(input);
        $taskRow.find("input.hours").each((index, element) => {
            const $input = $(element);
            taskHoursSum += parseHours($input.val());
        });
        var parseTaskHoursSum = getFormattedHours(taskHoursSum, true);
        $taskRow.find(".hours-summary-column").text(parseTaskHoursSum);


        const $projectRow = elementProjectContainer(input);

        let projectHoursSum = 0;
        $projectRow.find(".project-row .hours span").each((index, element) => {
            const $day = $(element);
            projectHoursSum += parseHours($day.text());
        });

        parseTaskHoursSum = getFormattedHours(projectHoursSum, true);
        $projectRow.find(".project-row .hours-summary-column").text(parseTaskHoursSum);
    }

    var options: Twitter.Typeahead.Options = { minLength: 1 };

    var remoteOptions: Twitter.Typeahead.Dataset<IProject> = {
        name: "projects",
        display: "Name",
        templates: {
            notFound: $("#notFound-template").html(),
            pending: $("#loading-template").html(),
            suggestion: q => {
                var subNameParts = [
                    q.ClientName,
                    q.PetsCode,
                    q.ProjectManagerName,
                    q.JiraCode
                ]
                    .filter(c => !!c && c.length > 0);

                return `
                <div class="tt-suggestion">
                    <b>${q.Name}</b>
                    <span>${subNameParts.join(" | ")}</span>
                </div>`;
            }
        },
        limit: 5,
        source: (q, cb, acb) => {
            return $.get(Globals.resolveUrl(`~/ValuePicker/JsonList`)
                , {
                    listId: MyTimeReport.projectControllerIdentifier,
                    order: "project-short-name",
                    filter: "all-projects",
                    search: q
                })
                .then(acb!);
        }
    };

    export function initFooterEventHandlers(container?: Element | JQuery | string) {
        $("tfoot .project-name input[type='text']", container || document.body)
            .each((index, element) => {
                const $element = $(element);

                $element.typeahead(options, remoteOptions)
                    .on("typeahead:select", (e, selectedItem: IProject) => {
                        MyTimeReport.addNewProject(selectedItem);
                        $element.typeahead("val", "");
                    })
                    .blur(() => {
                        $element.typeahead("val", "");
                    });
            });

        $(".alert-container .close").bind("click", function () {
            setTimeout(function () { $(window).trigger('resize'); }, 250);
        });
    }

    function cleanupTaskName(inputElement: JQuery) {
        var oldVal = inputElement.val();

        // Same as TimeReportRowHelper.cs : RemoveNotAllowedCharacters()
        var newVal = oldVal.replace(/[^a-z0-9äéöüßąćęłńóśźż\s,.:\'\"\*\-\+\/\\]+/gi, "");

        if (newVal !== oldVal) {
            inputElement.val(newVal);
        }
    }

    function hourskeyUpDown(this: JQuery, $event: any): void {
        const $input = $(this);
        let value = parseHours($input.val());
        switch ($event.which) {
            case 38: // Up
                $event.preventDefault();
                value++;
                $input.val(roundHourValue(value));
                break;
            case 40: // Down
                $event.preventDefault();
                value--;
                $input.val(roundHourValue(value));
                break;
        }
    }

    export function initTypeaHeadControlsAndEventHandler(container?: Element | JQuery | string) {
        $(".project-name input[type='text']", container || $("tbody", document.body))
            .each((index, element) => {
                const $element = $(element);
                const $inputParent = $element.parents("tbody:first");

                let selectedId: number = $inputParent.data("project-id");
                let selectedName: string = $inputParent.data("project-name");

                $element.typeahead(options, remoteOptions)
                    .on("typeahead:select", (e, selectedItem: IProject) => {
                        selectedId = selectedItem.Id;
                        selectedName = selectedItem.Name;

                        $inputParent.data("project-id", selectedId);
                        $inputParent.data("project-name", selectedName);

                        loadProjectMetadata(selectedItem.Id);
                        updateProjectAttributes($inputParent, selectedItem.Id);
                    })
                    .on("typeahead:change", (e, selectedItem: string) => {

                        if (selectedName !== selectedItem) {
                            $element.typeahead("val", selectedName);
                        }
                    });
            });
    }

    function updateProjectAttributes($parent: JQuery, projectId: number): void
    {
        const containers = $parent.find('.attributes-container');

        loadTaskProject(projectId, (taskHtml) =>
        {
            let serverContainer = $(document.createElement('div'));
            serverContainer.html(taskHtml);
            serverContainer = serverContainer.find('.attributes-container:first');

            containers.each((index, element) =>
            {
                $(element).html(serverContainer.html());
            });

            initializeAttributes($parent);
        });
    }

    export function initTaskRowNameWidth() {
        $(".task-row").each(function(this: JQuery) {
            let attributesContainerWidth = $(this).find('.attributes-container').width();
            $(this).find('.task-name-container').width((index, value) => (value - attributesContainerWidth));
        });
    }

    export function initControlsAndEventHandlers(container?: Element | JQuery | string) {
        $("td.hours input", container || document.body)
            .click(hourInputChangedHandler)
            .change(hourInputChangedHandler)
            .keyup(hourInputChangedHandler)
            .keydown(hourskeyUpDown)
            .mouseup(hourInputChangedHandler)
            .keyup(recalculateOvertimes)
            .blur(convertHoursValue)
            .focus(hourInputFocusSelectHandler)
            .keyup(hourInputEnterPressHandler);

        $("td.name input.name", container || document.body)
            .focus(function (this: JQuery, e: JQueryEventObject) {
                const indicators = $(this).siblings("div.task-indicator-icons");
                indicators.fadeOut();
            }).blur(function (this: JQuery, e: JQueryEventObject) {
                const indicators = $(this).siblings("div.task-indicator-icons");
                indicators.fadeIn();
            }).bind("change keydown keyup", function (this: JQuery, e: JQueryEventObject) {
                cleanupTaskName($(this));
            });

        $(".task-row td.hours .tt-marker", container || document.body).tooltip({ title: taskHoursInvalidValueMessage, container: 'body' });

        $("thead th.day-number[data-contracted-hours] .tt-marker", container || document.body).each((index, element) => {
            const $element = $(element);
            const $elementCell = $element.parent();
            const contractedHours: number = $elementCell.data("contracted-hours");

            $element.tooltip({
                title: () => {
                    if ($element.parent().hasClass("vacation-day-validation")) {
                        return vacationDayValidationMessage;
                    }
                    if (contractedHours == 0) {
                        return noHoursContractedMessage;
                    }
                    if ($elementCell.hasClass("ot-excited")) {
                        return overtimeExcitedMessage;
                    } else if ($elementCell.hasClass("ot-not-marked")) {
                        return overtimeNotMarkedMessage;
                    } else if ($elementCell.hasClass("ot-no-approvals")) {
                        return noOvertimeApprovalsMessage;
                    }
                    return overtimeDetectedMessage;
                },
                container: 'body'
            });
        });

        $(".btn-container", container || document.body)
            .on("click", ".dropdown-menu li a.overtime-variant", function (this: JQuery, e: JQueryEventObject) {
                e.preventDefault();
                const $button = $(this);
                onAssignOvertimeVariantClicked($button);
                recalculateOvertimes();
            }).on("click", ".dropdown-menu li a.clone-task", function (this: JQuery, e: JQueryEventObject) {
                e.preventDefault();
                const $button = $(this);
                var cloned = onCloneTaskClicked($button);
                recalculateOvertimes();
                recalculateTaskAndProjectHoursSummary(cloned);
                recalculateHoursSummary();
            }).on("click", ".dropdown-menu li a.split-overtime", function (this: JQuery, e: JQueryEventObject) {
                e.preventDefault();
                const $button = $(this);
                var cloned = onSplitOvertimeClicked($button);

                if (!!cloned) {
                    recalculateOvertimes();
                    recalculateTaskAndProjectHoursSummary($button);
                    recalculateTaskAndProjectHoursSummary(cloned);
                    recalculateHoursSummary();
                }

            }).on("click", ".dropdown-menu li a.fill-contracted-hours", function (this: JQuery, e: JQueryEventObject) {
                e.preventDefault();
                const $button = $(this);
                onFillContractedHours($button);
                recalculateOvertimes();
            });

        initializeAttributes(container || document.body);

        MyTimeReport.initializeOvertimeIndicators();
    }

    function initializeAttributes(container: string | Element | JQuery) {
        $(".attributes-container", container || document.body)
            .on("click", ".dropdown-menu li a.project-attribute-variant", function (this: JQuery, e: JQueryEventObject) {
                e.preventDefault();
                const $button = $(this);
                onAssignProjectAttributeVariantClicked($button);
            });
    }

    export function initializeOvertimeIndicators() {
        $(".task-row").each(function (this: JQuery) {
            changeIndicatorIconsVisibility($(this));
        });
    }

    export function changeIndicatorIconsVisibility($taskRow: JQuery) {
        const $overtimeIndicator = $taskRow.find(".overtime-indicator");

        const $taskOvertimeVariantInput = $taskRow.find("input.overtime-variant");
        const overtimeVariant = $taskOvertimeVariantInput.val();
        const indicatorVisible = overtimeVariant > 0;

        $overtimeIndicator.toggle(indicatorVisible);
    }

    export function dayIndexDictionary(): { [index: number]: Date } {
        let results: { [index: number]: Date } = {}
        $("#time-sheet thead th.day-number[data-date]")
            .each((index, element) => {
                results[index] = $(element).data("date");
            });
        return results;
    };

    export function getAbsenceDays(calculatedForm: ITimeTrackingViewModel = getSubmitObject()): number[] {
        var absenceDays: number[] = [];

        calculatedForm.projects.forEach(p => {
            p.Tasks
                .filter(t => !!t.AbsenceTypeId && t.AbsenceTypeId !== 0)
                .forEach(t => {
                    t.Days
                        .forEach((value, index) => {
                            if (value.Hours > 0) {
                                absenceDays.push(index);
                            }
                        });
                });
        });

        return absenceDays;
    }

    export function getSubmitObject(): ITimeTrackingViewModel {
        const $timeSheet = $("#time-sheet");
        let results: ITimeTrackingViewModel = { projects: [], days: initDayData(), overtimeApprovals: [...(overtimeApprovals || [])] };

        results.overtimeApprovals.forEach(oa => {
            oa.hourLimitUsed = 0;
            oa.overtimeHourUsed = 0;
            oa.excitedAfter = undefined;
        });

        const dayDictionary = dayIndexDictionary();

        $("tbody", $timeSheet).each((projectIndex, projectElement) => {
            const $projectRow = $(projectElement);
            const projectId: number = $projectRow.data("project-id");
            const projectName: string = $projectRow.data("project-name");

            var tasks: ITimeTrackingTask[] = [];

            $(".task-row", $projectRow).each((taskIndex, taskElement) => {
                const $taskRow = $(taskElement);
                const taskId: number = $taskRow.data("task-id");
                const taskName: string = $("input[type='text'].name", $taskRow).val();
                const taskOvertimeVariant: number = $("input[type='hidden'].overtime-variant", $taskRow).val();
                const absenceTypeElement: JQuery = $("input[type='hidden'].absence-type-id", $taskRow);
                const absenceTypeId: number = absenceTypeElement.length ? absenceTypeElement.val() : null;
                const importSourceElement: JQuery = $("input[type='hidden'].import-source-id", $taskRow);
                const importSourceId: number = importSourceElement.length ? importSourceElement.val() : null;
                const importedTaskCodeElement: JQuery = $("input[type='hidden'].imported-task-code", $taskRow);
                const importedTaskCode: string = importedTaskCodeElement.length ? importedTaskCodeElement.val() : null;
                const importedTaskTypeElement: JQuery = $("input[type='hidden'].imported-task-type", $taskRow);
                const importedTaskType: string = importedTaskTypeElement.length ? importedTaskTypeElement.val() : null;
                const isOvertime: boolean = taskOvertimeVariant != 0;
                const selectedAttributes: number[] = [];
                $taskRow.find(".attributes-container input[type='hidden']").each((index, element) => {
                    if ($(element).val() != "") {
                        selectedAttributes.push(Number($(element).val()));
                    }
                });

                let task: ITimeTrackingTask = {
                    Id: taskId,
                    Name: taskName,
                    OvertimeVariant: taskOvertimeVariant,
                    AbsenceTypeId: absenceTypeId,
                    Days: [],
                    SelectedAttributesValueIds: selectedAttributes,
                    ImportSourceId: importSourceId,
                    ImportedTaskCode: importedTaskCode,
                    ImportedTaskType: importedTaskType,
                };

                $("td.hours", $taskRow).each((dayIndex, dayElement) => {
                    const $dayCell = $(dayElement);
                    const hours = parseHours($("input.hours", $dayCell).val());
                    var dayEntry: ITimeTrackingTaskEntry = {
                        Hours: hours,
                        Day: dayDictionary[dayIndex]
                    };
                    var dayDataEntry = results.days[dayEntry.Day.toString()];

                    if (isOvertime) {
                        dayDataEntry.usedOvertimeHours += dayEntry.Hours;
                    } else {
                        dayDataEntry.usedRegularHours += dayEntry.Hours;
                    }

                    results.overtimeApprovals
                        .filter(oa => dateIsInRange(dayEntry.Day, oa.dateFrom, oa.dateTo) && oa.projectId == projectId)
                        .forEach(oa => {
                            if (isOvertime) {
                                oa.overtimeHourUsed = (oa.overtimeHourUsed || 0) + dayEntry.Hours;

                                if (!oa.excitedAfter && oa.overtimeHourUsed > oa.hourLimit) {
                                    oa.excitedAfter = dayEntry.Day;
                                }
                            } else {
                                oa.hourLimitUsed = (oa.hourLimitUsed || 0) + dayEntry.Hours;
                            }
                        });

                    task.Days.push(dayEntry);
                });

                tasks.push(task);
            });

            results.projects.push({ ProjectId: projectId, ProjectName: projectName, Tasks: tasks });
        });

        return results;
    }

    export function setFormPristine(viewModel: ITimeTrackingViewModel = getSubmitObject()) {
        const $form = formContainer();
        $form.data("origin", viewModel);
    }

    export function clearInitialPristine() {
        const $form = formContainer();
        $form.removeData("dirty");
    }

    export function getVacationDays(): number[] {
        const $form = formContainer();
        const storedVacations: number[] = $form.data("vacation");

        return storedVacations;
    }

    export function setVacationCache(viewModel: ITimeTrackingViewModel = getSubmitObject()) {
        const $form = formContainer();
        var absenceDays = getAbsenceDays(viewModel);
        $form.data("vacation", absenceDays);
    }


    export function recalculateOvertimes(this: any) {
        if ($(this).prop("type") !== "text") {
            return;
        }

        const $input = $(this);
        const currentVal = $input.val();
        const oldValueName = 'recalculateOvertimesOldValue';

        var oldValue = $input.data(oldValueName) || "";

        if (currentVal === oldValue) {
            return;
        }

        recalculateAllOvertimes();
        $input.data(oldValueName, currentVal);
    }

    export function recalculateAllOvertimes(): void {
        var submitObject = getSubmitObject();
        validateVacationsHours(submitObject);
        $("#time-sheet thead th.day-number[data-date]")
            .removeClass("ot")
            .removeClass("ot-not-marked")
            .removeClass("ot-excited")
            .removeClass("ot-no-approvals")
            .each((index, element) => {
                const $element = $(element);
                var date: Date = $element.data("date");
                const dateData = submitObject.days[date.toString()];
                const overtimeApprovesForDay = submitObject.overtimeApprovals.filter(oa =>
                    dateIsInRange(date, oa.dateFrom, oa.dateTo));

                if (dateData.usedOvertimeHours + dateData.usedRegularHours > dateData.contractedHours || dateData.usedOvertimeHours > 0) {
                    $element.addClass("ot");

                    if (dateData.usedRegularHours > dateData.contractedHours) {
                        $element.addClass("ot-not-marked");
                    } else if (overtimeApprovesForDay.length === 0) {
                        $element.addClass("ot-no-approvals");
                    }
                }
            });
    }

    function handleSumitConfirm(promiseHandler: IChangePromise): void {
        var submitConfirmUrl = Globals.resolveUrl("/TimeTracking/MyTimeReport/SubmitConfirmDialog");

        Dialogs.showDialog({
            actionUrl: submitConfirmUrl,
            formData: { year: getYear(), month: getMonth(), employeeId: getEmployeeId() },
            onBeforeShow: dialogBeforeShow => {
                var submitBtn = dialogBeforeShow.find("[data-button = 'submit']");
                submitBtn.click(() => {
                    dialogBeforeShow.hide();
                    setFormPristine();
                    clearInitialPristine();
                    promiseHandler.pristine();
                });
            }
        });
    }

    export function handleIfNotDirty(promiseHandler: IChangePromise): void {
        if (isFormDirty()) {
            const dirtyConfirm = Globals.resources["DirtyFormConfirmText"];
            const dirtyConfirmTitle = Globals.resources["DirtyFormConfirmTitleText"];

            CommonDialogs.confirm(dirtyConfirm, dirtyConfirmTitle, (a) => {
                if (a.buttonTag === "ok") {
                    setFormPristine();
                    clearInitialPristine();
                    promiseHandler.pristine();
                } else if (!!promiseHandler.dirty) {
                    promiseHandler.dirty();
                }
            });
        } else {
            promiseHandler.pristine();
        }
    }

    export function isFormDirty(): boolean {
        const $form = formContainer();

        if (!!$form.data('dirty')) {
            return true;
        }

        const viewModel = getSubmitObject();
        const originalData = $form.data("origin");

        if (!originalData) {
            return false;
        }

        return JSON.stringify(viewModel) !== JSON.stringify(originalData);
    }

    export function submitRequest() {
        handleSumitConfirm({
            pristine: () => {
                submit(true);
            }
        });
    }

    export function submitForm() {
        submit(false);
    }

    function submit(isSubmitRequest: boolean = false) {
        $(window).off('beforeunload', pageLeaveCheck);
        const $form = formContainer();
        const $formFieldsContainer = $(".fields-container", $form);
        $formFieldsContainer.empty();

        const viewModel = getSubmitObject();

        var $hiddenTemplate = $("<input type='hidden' />");
        $form.find("#IsSubmitRequest").val(isSubmitRequest.toString());

        for (let index in viewModel.projects) {
            const project = viewModel.projects[index];
            $formFieldsContainer.append($hiddenTemplate.clone().attr("name", `Projects[${index}].ProjectId`).val(project.ProjectId));
            $formFieldsContainer.append($hiddenTemplate.clone().attr("name", `Projects[${index}].ProjectName`).val(project.ProjectName));

            for (let taskIndex in project.Tasks) {
                const task = project.Tasks[taskIndex];
                const taskPath = `Projects[${index}].Tasks[${taskIndex}]`;

                if (task.Id) {
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.Id`).val(task.Id));
                }

                $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.Name`).val(task.Name));
                $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.OvertimeVariant`).val(task.OvertimeVariant));

                if (task.AbsenceTypeId) {
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.AbsenceTypeId`).val(task.AbsenceTypeId));
                }

                if (task.ImportSourceId) {
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.ImportSourceId`).val(task.ImportSourceId));
                }

                if (task.ImportedTaskCode) {
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.ImportedTaskCode`).val(task.ImportedTaskCode));
                }

                if (task.ImportedTaskType) {
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.ImportedTaskType`).val(task.ImportedTaskType));
                }

                for (let dayEntryIndex in task.Days) {
                    const dayEntry = task.Days[dayEntryIndex];
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.Days[${dayEntryIndex}].Day`).val(dayEntry.Day.toString()));
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.Days[${dayEntryIndex}].Hours`).val(getFormattedHours(dayEntry.Hours)));
                }

                for (let valuesIndex in task.SelectedAttributesValueIds) {
                    $formFieldsContainer.append($hiddenTemplate.clone().attr("name", taskPath + `.SelectedAttributesValueIds[${valuesIndex}]`).val(task.SelectedAttributesValueIds[valuesIndex]));
                }
            }
        }

        $form.submit();
    }

    function recalculateFooterOverflow(): void {
        $('.grid-time-sheet').trigger('resize').trigger('scroll');
    }

    function recalculateFixedColumns(): void {
        $fixedColumns = $(".fix-horizontal", $(".grid-time-sheet .time-sheet"));
    }

    export function onDeleteTaskClicked(element: HTMLElement) {
        CommonDialogs.confirm(Globals.resources.ConfirmDeleteTask, Globals.resources.ConfirmDialogTitle, event => {
            if (event.isOk) {
                let $taskRow = elementTaskContainer(element);
                let $projectRow = elementProjectContainer($taskRow);

                $taskRow.remove();
                if ($projectRow.find(".task-row").length === 0) {
                    $projectRow.remove();
                }

                recalculateOvertimes();
                recalculateProjectHours($projectRow);
                recalculateHoursSummary();
                recalculateFooterOverflow();
            }
        });
    }

    export function onDeleteProjectClicked(element: HTMLElement) {
        CommonDialogs.confirm(Globals.resources.ConfirmDeleteProject, Globals.resources.ConfirmDialogTitle, event => {
            if (event.isOk) {
                let $projectRow = elementProjectContainer(element);
                $projectRow.remove();
                recalculateOvertimes();
                recalculateHoursSummary();
                recalculateFooterOverflow();
            }
        });
    }

    export function onAddTaskClicked(element: HTMLElement) {
        const $inputParent = $(element).parents("tbody:first");
        loadTaskProject($inputParent.data("project-id"), (taskHtml) => {
            const $projectRow = elementProjectContainer(element);
            const $taskRow = $projectRow.append(taskHtml);

            let attributesContainerWidth = $taskRow.find('.attributes-container:first').width();
            $taskRow.find('.task-name-container:last').width((index, value) => (value - attributesContainerWidth));

            initControlsAndEventHandlers($taskRow);
            recalculateFooterOverflow();
            recalculateFixedColumns();

            const taskInput = $("input[type='text'].name", $taskRow.find(".task-row:last")).get(0);
            setTimeout(() => { taskInput.focus(); }, 1); // Wait to DOM load
        });
    }

    export function onRowContextMenuClicked(element: HTMLElement) {
        const $taskRow = $(element).parents(".task-row");
        const taskOvertimeVariantInputElement = $taskRow.find("input.overtime-variant:first");
        const overtimeVariant = Number(taskOvertimeVariantInputElement.val());

        contextMenu.find("a.overtime-variant").each((index, menuItemElement) => {
            const $menuItem = $(menuItemElement);

            if ($menuItem.data("overtime-variant") === overtimeVariant) {
                $menuItem.addClass("radio");
            } else {
                $menuItem.removeClass("radio");
            }
        });

        $(element).after(contextMenu).dropdown();
    }

    export function onAttributeContextMenuClicked(element: HTMLElement) {
        var attributeContextMenu: JQuery = $(element).siblings('#taskRowProjectAttributeContextMenu');

        attributeContextMenu.find("a.project-attribute-variant").each((index, menuItemElement) => {
            const $menuItem = $(menuItemElement);
            const taskAttributeValueVariantInputElement = $(element).siblings("input");
            const attributeValueVariant = Number(taskAttributeValueVariantInputElement.val());

            if ($menuItem.data('attribute-value') === attributeValueVariant) {
                $menuItem.addClass("radio");
            } else {
                $menuItem.removeClass("radio");
            }
        });

        $(element).after(attributeContextMenu).dropdown();
    }

    export function onAssignOvertimeVariantClicked(button: JQuery) {
        const $taskRow = elementTaskContainer(button);
        const taskOvertimeVariantInputElement = $taskRow.find("input.overtime-variant:first");
        const overtimeVariant = $(button).data("overtime-variant");

        taskOvertimeVariantInputElement.val(overtimeVariant);
        changeIndicatorIconsVisibility($taskRow);
    }

    export function onAssignProjectAttributeVariantClicked(button: JQuery) {
        const $container = $(button).parents('.button-dropdown-content');
        const projectAttributeInput = $container.find("input");
        const $buttonContainer = $container.find("button span:not(.caret)");
        const projectAttributeVariant = $(button).data('attribute-value');
        $buttonContainer.text(button.text());
        projectAttributeInput.val(projectAttributeVariant);
    }

    export function onCloneTaskClicked(button: JQuery): JQuery {
        const $taskRow = elementTaskContainer(button);
        var $taskClone = $taskRow.clone(true);
        $taskClone.insertAfter($taskRow);

        $("input.name", $taskClone).select();

        return $taskClone;
    }

    export function onSplitOvertimeClicked(button: JQuery): JQuery | undefined {
        const $taskRow = elementTaskContainer(button);
        let hoursDictionary: { Hours: number, ContractedHours: number }[] = [];
        $taskRow.find("input.hours").each((index, day) => {
            const $input = $(day);
            hoursDictionary.push({
                Hours: parseHours($input.val()),
                ContractedHours: parseHours($input.data("contracted-hours"))
            });
        });

        if (hoursDictionary.filter(i => i.Hours > i.ContractedHours).length > 0) {
            const $clonedTask = onCloneTaskClicked(button);
            for (var dayIndex in hoursDictionary) {
                const day = hoursDictionary[dayIndex];
                const $dayInput = $clonedTask.find(`[data-day="${dayIndex}"]`);
                const $originalDayInput = $taskRow.find(`[data-day="${dayIndex}"]`);

                if (day.ContractedHours < day.Hours) {
                    $originalDayInput.val(day.ContractedHours);
                    $dayInput.val(day.Hours - day.ContractedHours);
                } else {
                    $dayInput.val("");
                }
            }

            return $clonedTask;
        } else {
            CommonDialogs.alert({ 
                htmlMessage: Globals.resources["SplitNotNeeded"], 
                title: "", 
                eventHandler: undefined, 
                iconName: "", 
                okButtonText: "" 
                });
            return;
        }
    }

    export function onFillContractedHours(button: JQuery) {
        const $taskRow = elementTaskContainer(button);

        $taskRow.find("input.hours").each((index, day) => {
            const epsilon = 0.001;
            const $input = $(day);
            let hour = parseHours($input.val());
            let contractedHour = parseHours($input.data("contracted-hours"));

            if (hour === 0 && contractedHour - epsilon > 0) {
                $input.val(contractedHour);
                $input.trigger("change");
            }
        });
    }

    export function loadProjectMetadata(projectId: number, callback?: () => void): void {
        CommonDialogs.pleaseWait.show(20);

        $.get(Globals.resolveUrl(`/TimeTracking/MyTimeReport/GetProjectMetadata?projectId=${projectId}&year=${getYear()}&month=${getMonth()}&employeeId=${getEmployeeId()}`), (data: any) => {

            MyTimeReport.overtimeApprovals = MyTimeReport.overtimeApprovals.filter(p => p.projectId != projectId);

            const overtimeApprovals: any[] = data.OvertimeApprovals;

            overtimeApprovals.forEach(item => MyTimeReport.overtimeApprovals.push({
                dateFrom: item.DateFrom,
                dateTo: item.DateTo,
                hourLimit: item.HourLimit,
                projectId: item.ProjectId
            }));

            if (!!callback) {
                callback();
            }

            CommonDialogs.pleaseWait.hide();
        });
    }

    export function loadDefaultProject(projectId: number, callback: (data: any) => void): void {
        CommonDialogs.pleaseWait.show(20);

        $.post(Globals.resolveUrl(`/TimeTracking/MyTimeReport/GetDefaultProjectTemplate?projectId=${projectId}&year=${getYear()}&month=${getMonth()}&employeeId=${getEmployeeId()}`), (data: any) => {

            callback(data);

            CommonDialogs.pleaseWait.hide();
        });
    }

    export function loadTaskProject(projectId: number, callback: (data: any) => void): void {
        CommonDialogs.pleaseWait.show(20);

        $.post(Globals.resolveUrl(`/TimeTracking/MyTimeReport/GetDefaultTaskByProjectId?projectId=${projectId}&year=${getYear()}&month=${getMonth()}&employeeId=${getEmployeeId()}`), (data: any) => {

            callback(data);

            CommonDialogs.pleaseWait.hide();
        });
    }

    export function addNewProject(project: IProject): void {
        loadProjectMetadata(project.Id, () => {
            loadDefaultProject(project.Id, (projectHtml) => {
                const $newProject = $(projectHtml).insertBefore($(".time-sheet tfoot"));

                initControlsAndEventHandlers($newProject);
                initTypeaHeadControlsAndEventHandler($newProject);
                recalculateFooterOverflow();
                recalculateFixedColumns();

                const taskInput = $("input[type='text'].name", $newProject.find(".task-row:last")).get(0);
                setTimeout(() => { taskInput.focus(); }, 1); // Wait to DOM load
            });
        });
    }

    export function openProjectValuePickerForNew() {
        ValuePicker.selectSingle(MyTimeReport.projectControllerIdentifier, (record) => {
            MyTimeReport.addNewProject({ Id: record.id, Name: record.toString });
            recalculateFooterOverflow();
        });
    }

    export function openProjectValuePicker(button: Element) {
        ValuePicker.selectSingle(MyTimeReport.projectControllerIdentifier, (record) => {
            loadProjectMetadata(record.id, () => {
                const $inputParent = $(button).parents("tbody:first");

                $inputParent.data("project-id", record.id);
                $inputParent.data("project-name", record.toString);

                $(".project-name input[type='text']", $inputParent)
                    .val(record.toString)
                    .typeahead("val", record.toString);
            });

            recalculateFooterOverflow();
        });
    }

    export function preventMainMenuNavigation() {
        $("#main-menu li a:not([data-toggle]), a.navbar-brand[href], .breadcrumb a[href]").click(function (this: JQuery, e: MouseEvent) {
            const $link = $(this);
            const hrefUrl = $link.attr("href");

            e.preventDefault();
            e.stopPropagation();

            handleIfNotDirty({
                pristine: () => {
                    window.location.href = hrefUrl;
                }
            });
        });

        $(window).on('beforeunload', pageLeaveCheck);
    }

    function pageLeaveCheck() {
        if (isFormDirty()) {
            return Globals.resources["DirtyFormConfirmText"];
        }

        return undefined;
    }

    function getYear(): number {
        return $("#Year").val();
    }

    function getMonth(): number {
        return $("#Month").val();
    }

    function getEmployeeId(): number {
        return $("#EmployeeId").val();
    }

    function dateIsInRange(date: string | Date, from: string | Date, to: string | Date): boolean {
        if (from == null && to == null) {
            return true;
        }

        const dateFormat = Globals.getDateFormat();
        const dateTime = $.fn.datepicker.DPGlobal.parseDate(date.toString(), dateFormat).getTime();
        const fromTime = new Date(from.toString()).getTime();
        const toTime = new Date(to.toString()).getTime();

        return dateTime >= fromTime && dateTime <= toTime;
    }

    function initDayData(): IDayDataDictionary {
        let result: IDayDataDictionary = {};
        $("#time-sheet thead th.day-number[data-date][data-day][data-contracted-hours]")
            .each((index, element) => {
                const $element = $(element);
                const key: string = $element.data("date");
                const value: number = $element.data("contracted-hours");

                const resultValue: IDayData = {
                    contractedHours: value,
                    usedOvertimeHours: 0,
                    usedRegularHours: 0
                };

                result[key] = resultValue;
            });

        return result;
    }

    function formContainer(): JQuery {
        return $("#my-time-report-form");
    }

    function elementProjectContainer(element: Element | JQuery): JQuery {
        return $(element).parents("tbody:first");
    }

    function elementTaskContainer(element: Element | JQuery): JQuery {
        var $element = $(element);

        return $element.is('.task-row') ? $element: $element.parents(".task-row:first");
    }

    export function initNumericInputs() {
        const $inputs = $("#time-sheet").on("keydown", "input.hours", event => {
            if (event.key === "e" || event.key === "E") {
                event.preventDefault();
            }
        }).find("input.hours");

        validateHours($inputs);
    }

    export function toolbarMenuEvents(): void {
        $(".row.grid-toolbar .btn-group [data-action]")
            .filter("button[data-handler]")
            .click(function (this: JQuery) {
                const $button = $(this);
                const jsEventCode = $button.data("handler");

                Utils.executeHandler(jsEventCode, {});
            })
            .end()
            .filter("a[data-url], button[data-url]")
            .click(function (this: JQuery) {
                const $button = $(this);
                handleIfNotDirty({
                    pristine: () => {
                        const url = $button.data("url");
                        const method = $button.data("method");
                        const isNewTab = $button.data("new-tab");
                        $(window).off('beforeunload', pageLeaveCheck);
                        Forms.submit(url, method, isNewTab);
                    }
                });
            });
    }

    function calcFooterPostion(): number {
        var bottom = $("table").position().top + $("table").offset().top + $("table").outerHeight(true);
        var $el = $(".grid-time-sheet .time-sheet"),
            scrollTop = $(".grid-time-sheet").scrollTop(),
            scrollBot = scrollTop + $(".grid-time-sheet").height(),
            elTop = $el.offset().top,
            elBottom = elTop + $el.outerHeight(),
            visibleTop = elTop < scrollTop ? scrollTop : elTop,
            visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
        var scrollBarHeight = 17;

        var diff = bottom - visibleBottom - visibleTop + scrollBarHeight;

        return diff;
    }

    function isElementFullyInView(element: HTMLElement) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height() - $('.footer-section').height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();
        return ((pageTop < elementTop) && (pageBottom > elementBottom));
    };

    export function scrollMyTimeReport(): void {
        const $table = $(".grid-time-sheet .time-sheet");
        const $fixedHeaders = $(".fix-vertical", $table);
        const $fixedFooters = $(".fix-vertical-footer", $table);

        var diff = calcFooterPostion();

        var $gridSheet = $(".grid-time-sheet")
            .scroll(function () {
                const position = $(".grid-time-sheet table:last").position();
                $fixedColumns.css("left", position.left * -1);
                $fixedHeaders.css("top", position.top * -1);
                $fixedFooters.css("bottom", Math.max(0, (diff - position.top * -1)));
            });

        $(window).resize(() => {
            setTimeout(() => {
                diff = calcFooterPostion();
                var isElementInView = isElementFullyInView($('table')[0]);

                if (isElementInView) {
                    diff = 0;
                }
                else {
                    diff = calcFooterPostion();
                }

                $gridSheet.trigger('scroll');
            }, 1);
        });

        $gridSheet.trigger('scroll');
    }
};

$(() => {
    MyTimeReport.toolbarMenuEvents();
    MyTimeReport.initControlsAndEventHandlers();
    MyTimeReport.initTypeaHeadControlsAndEventHandler();
    MyTimeReport.initTaskRowNameWidth();
    MyTimeReport.initFooterEventHandlers();
    MyTimeReport.initNumericInputs();
    var formData = MyTimeReport.getSubmitObject();
    MyTimeReport.setFormPristine(formData);
    MyTimeReport.setVacationCache(formData);
    MyTimeReport.preventMainMenuNavigation();
    MyTimeReport.scrollMyTimeReport();
    MyTimeReport.recalculateAllOvertimes();
});