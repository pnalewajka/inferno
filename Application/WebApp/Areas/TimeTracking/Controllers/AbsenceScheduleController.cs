﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewAbsenceSchedule)]
    public class AbsenceScheduleController : AtomicController
    {
        private readonly IAbsenceScheduleService _absenceScheduleService;
        private readonly IEmployeeService _employeeService;
        private readonly IProjectService _projectService;
        private readonly ITimeService _timeService;
        private readonly IEmploymentPeriodService _employmentPeriodService;

        public AbsenceScheduleController(
            IEmployeeService employeeService,
            IBaseControllerDependencies baseDependencies,
            ITimeService timeService,
            IAbsenceScheduleService absenceScheduleService,
            IProjectService projectService,
            IEmploymentPeriodService employmentPeriodService)
            : base(baseDependencies)
        {
            _employeeService = employeeService;
            _absenceScheduleService = absenceScheduleService;
            _projectService = projectService;
            _timeService = timeService;
            _employmentPeriodService = employmentPeriodService;

            Layout.PageTitle = AbsenceScheduleResources.Title;
            Layout.Css.Add("~/Areas/TimeTracking/Content/AbsenceSchedule.css");
            Layout.Scripts.Add("~/Areas/TimeTracking/Scripts/ScheduleList.js");
        }

        [HttpGet]
        public ActionResult Configure()
        {
            var viewModel = new AbsenceScheduleViewModel();

            ViewModelHelper.TryUpdateModel(this, viewModel, clearValidationErrors: true);

            return View("~/Areas/TimeTracking/Views/AbsenceSchedule/AbsenceSchedule.cshtml", viewModel);
        }

        [HttpPost]
        public ActionResult Configure(AbsenceScheduleViewModel absenceScheduleViewModel)
        {
            if (ModelState.IsValid)
            {
                var query = nameof(ScheduleList);
                var users = string.Join(",", _employeeService.GetEmployeesById(absenceScheduleViewModel.EmployeeIds).Select(e => e.Acronym));
                var projects = string.Join(",", absenceScheduleViewModel.ProjectIds);
                var periodStartDate = string.Join(",", absenceScheduleViewModel.PeriodStartDate.ToShortDateString());
                var periodEndDate = string.Join(",", absenceScheduleViewModel.PeriodEndDate.ToShortDateString());

                var parameters = string.Join("&", new[] { $"{nameof(users)}={users}", $"{nameof(projects)}={projects}",
                    $"{nameof(periodStartDate)}={periodStartDate}",  $"{nameof(periodEndDate)}={periodEndDate}" }
                    .Where(x => x.Length > x.IndexOf("=", StringComparison.Ordinal) + 1));

                if (!string.IsNullOrEmpty(parameters))
                {
                    query = $"{query}?{parameters}";
                }

                //usage of Redirect instead of RedirectToAction is crucial here, because normal Redirect will not encode commas in final URL
                return Redirect(query);
            }

            return View("~/Areas/TimeTracking/Views/AbsenceSchedule/AbsenceSchedule.cshtml", absenceScheduleViewModel);
        }

        [HttpGet]
        [BreadcrumbBar("AbsenceScheduleList")]
        public ActionResult ScheduleList(string users, string periodStartDate, string periodEndDate, [ModelBinder(typeof(CommaSeparatedArrayModelBinder))]long[] projects)
        {
            var periodStart = periodStartDate != null
                ? DateTime.Parse(periodStartDate)
                : DateHelper.BeginningOfMonth(_timeService.GetCurrentDate());

            var periodEnd = periodEndDate != null
                ? DateTime.Parse(periodEndDate)
                : DateHelper.EndOfMonth(_timeService.GetCurrentDate());

            var selectedAcronyms = GetValues(users);

            var principal = GetCurrentPrincipal();

            if ((projects == null || !projects.Any()) && !selectedAcronyms.Any())
            {
                selectedAcronyms = _employeeService.GetEmployeesById(new[] { principal.EmployeeId }).Select(e => e.Acronym).ToList();

                if (principal.Id != null)
                {
                    projects = _projectService.GetProjectIdsByUserId(principal.Id.Value, _timeService.GetCurrentDate()).ToArray();
                }
            }

            var scheduleDto = _absenceScheduleService.CalculateSchedule(selectedAcronyms, projects, periodStart, periodEnd);

            var model = new ScheduleListViewModel
            {
                PeriodStartDate = scheduleDto.PeriodStartDate,
                PeriodEndDate = scheduleDto.PeriodEndDate,
                CurrentDate = _timeService.GetCurrentDate()
            };

            model.Employees = scheduleDto.Employees.Select(e => new EmployeeScheduleListViewModel
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                HolidayDays = e.HolidayDays,
                HomeOfficeDays = e.HomeOfficeDays,
                AbsenceDays = e.AbsenceDays,
                NotContractedDays = e.NotContractedDays,
                ContractedWorkingHours = _employmentPeriodService.GetContractedHours(e.Id, model.PeriodStartDate, model.PeriodEndDate)
            }).ToList();

            return View(model);
        }

        private List<string> GetValues(string value)
        {
            return (value ?? string.Empty)
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Where(s => !s.Equals(ModelBinderConstants.EmptyValueMarker))
                .ToList();
        }
    }
}