﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Identifier("ValuePickers.ContractType")]
    public class ContractTypePickerController : ContractTypeController
    {
        private const string MinimalViewId = "minimal";

        public ContractTypePickerController(IContractTypeCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<ContractTypeViewModel>(MinimalViewId, MinimalViewId) { IsDefault = true };

            configuration.IncludeColumns(new List<Expression<Func<ContractTypeViewModel, object>>>
            {
                x => x.Name,
                x => x.IsActive,
            });

            configurations.Add(configuration);

            return configurations;
        }
    }
}