using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Helpers;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Resources;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTimeTrackingProjectDashboard)]
    public class TimeTrackingDashboardController : CardIndexController<TimeReportProjectDetailsViewModel, TimeReportProjectDetailsDto, TimeTrackingDashboardContext>
    {
        private readonly ITimeService _timeService;

        public TimeTrackingDashboardController(ITimeTrackingDashboardCardIndexDataService cardIndexDataService, ITimeService timeService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _timeService = timeService;

            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            Filters();

            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations();

            CardIndex.Settings.RowButtons.Add(new CommandButton
            {
                Text = ProjectDashboardResources.ViewRequestButtonText,
                OnClickAction = new JavaScriptCallAction($"ProjectDashboard.viewRequest(this.rowData[0], '{ Url.Action("View", "Request", new { area = "Workflows" }) }')"),
                Icon = FontAwesome.Edit,
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanViewRequest,
                Availability = new RowButtonAvailability<TimeReportProjectDetailsViewModel>
                {
                    Predicate = model => model.IsRequestAvailable
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = ProjectDashboardResources.TimeReportGroupName,
                OnClickAction = new DropdownAction
                {
                    Items = new List<ICommandButton>
                    {
                        new CommandButton
                        {
                            Text = ProjectDashboardResources.TimeReportDailyName,
                            OnClickAction = new JavaScriptCallAction($"ProjectDashboard.showReportDialog(grid, '{Url.Action("ConfigureReportDialog", "ReportExecution", new { area = "Reporting", reportCode = "TimeTrackingDailyHours"  })}')"),
                            RequiredRole = SecurityRoleType.CanGenerateTimeTrackingDailyHoursReport,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.Always
                            }
                        },
                        new CommandButton
                        {
                            Text = ProjectDashboardResources.TimeReportTaskName,
                            RequiredRole = SecurityRoleType.CanGenerateTimeTrackingTasksReport,
                            OnClickAction = new JavaScriptCallAction($"ProjectDashboard.showReportDialog(grid, '{Url.Action("ConfigureReportDialog", "ReportExecution", new { area = "Reporting", reportCode = "TimeTrackingTasks"  })}')"),
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.Always
                            }
                        }
                    }
                }
            });
            
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportTimeTrackingDashboard);
            Layout.Scripts.Add("~/Areas/TimeTracking/Scripts/ProjectDashboard.js");
        }

        [BreadcrumbBar("TimeTrackingDashboardList")]
        public override ActionResult List(GridParameters parameters)
        {
            SetContext();

            CardIndex.Settings.Title = string.Format(ProjectDashboardResources.PageTitleFormat, Context.Year, Context.Month);

            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretLeft, LayoutResources.PreviousMonthShort, -1));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.MapMarker, LayoutResources.CurrentMonth, null));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretRight, LayoutResources.NextMonthShort, 1));

            return base.List(parameters);
        }

        public override string CopyToClipboard(GridParameters parameters)
        {
            SetContext();

            return base.CopyToClipboard(parameters);
        }

        public override FilePathResult ExportToCsv(GridParameters parameters)
        {
            SetContext();

            return base.ExportToCsv(parameters);
        }

        public override FilePathResult ExportToExcel(GridParameters parameters)
        {
            SetContext();

            return base.ExportToExcel(parameters);
        }
        

        private CommandButton CreateMonthChangeButton(string icon, string text, int? monthsToAdd)
        {
            DateTime selectedMonthDate;

            if (monthsToAdd.HasValue)
            {
                if (Context.Year.HasValue && Context.Month.HasValue)
                {
                    selectedMonthDate = new DateTime(Context.Year.Value, Context.Month.Value, 1);
                }
                else
                {
                    selectedMonthDate = _timeService.GetCurrentDate();
                }

                selectedMonthDate = selectedMonthDate.AddMonths(monthsToAdd.Value);
            }
            else
            {
                selectedMonthDate = _timeService.GetCurrentDate();
            }

            return new ToolbarButton
            {
                Group = "date-navigation",
                Icon = icon,
                Text = text,
                OnClickAction = new JavaScriptCallAction($"ProjectDashboard.changeFilterDate({selectedMonthDate.Year}, {selectedMonthDate.Month})")
            };
        }

        private IList<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            var defaultView =
                new GridViewConfiguration<TimeReportProjectDetailsViewModel>(RequestResource.DefaultView, "default");

            defaultView.IncludeAllColumns();
            defaultView.ExcludeColumn(r => r.OrgUnitId);
            defaultView.ExcludeColumn(r => r.OvertimeHolidayHours);
            defaultView.ExcludeColumn(r => r.OvertimeLateNightHours);
            defaultView.ExcludeColumn(r => r.OvertimeNormalHours);
            defaultView.ExcludeColumn(r => r.OvertimeWeekendHours);
            defaultView.IsDefault = true;

            var extendedView =
                new GridViewConfiguration<TimeReportProjectDetailsViewModel>(RequestResource.ExtendedView, "extended");

            extendedView.IncludeAllColumns();
            extendedView.ExcludeColumn(r => r.OvertimeTotals);

            return new IGridViewConfiguration[]
            {
                defaultView,
                extendedView,
            };
        }

        private void Filters()
        {
            CardIndex.Settings.FilterGroups = new[]
            {
                new FilterGroup
                {
                    DisplayName = ProjectDashboardResources.ProjectFilterGroupName,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = ProjectDashboardFilterCodes.MyProjects, DisplayName = ProjectDashboardResources.MyProjectsFilterName },
                        new Filter { Code = ProjectDashboardFilterCodes.AllProjects, DisplayName = ProjectDashboardResources.AllProjectsFilterName },
                    }
                },
                new FilterGroup
                {
                    DisplayName = ProjectDashboardResources.FilterGroupName,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<OrgUnitFilterViewModel>(ProjectDashboardFilterCodes.ByOrgUnit, ProjectDashboardResources.OrgUnitFilterName),
                        new ParametricFilter<AllocationRequestProjectsAndEmployeesFilterViewModel>(
                            ProjectDashboardFilterCodes.ByProject, ProjectDashboardResources.EmployeesAndProjectsFilterName)
                    }
                },
            };
        }

        private void SetContext()
        {
            if (!Context.IsSet)
            {
                var selectedMonthDate = _timeService.GetCurrentDate();

                Context.Year = selectedMonthDate.Year;
                Context.Month = selectedMonthDate.Month;
            }
        }
    }
}