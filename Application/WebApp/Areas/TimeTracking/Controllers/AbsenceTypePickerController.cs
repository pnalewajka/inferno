﻿using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewAbsenceType)]
    [Identifier("ValuePickers.AbsenceType")]
    public class AbsenceTypePickerController  : CardIndexController<AbsenceTypeViewModel, AbsenceTypeDto, AbsenceTypeContext>
    {
        private readonly IAbsenceTypeCardIndexDataService _cardIndexDataService;
        private readonly IBaseControllerDependencies _dependencies;
        private readonly IAnonymizationService _anonymizationService;

        public AbsenceTypePickerController(IAbsenceTypeCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies dependencies,
            IAnonymizationService anonymizationService)
            : base(cardIndexDataService, dependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dependencies = dependencies;
            _anonymizationService = anonymizationService;

            CardIndex.Settings.Title = AbsenceTypeResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddAbsenceType;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditAbsenceType;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteAbsenceType;
        }

        public override ActionResult List(GridParameters parameters)
        {
            if (Context.CurrentEmployeeOnly && parameters.SearchPhrase.IsEmpty())
            {
                CardIndex.Settings.EmptyListMessage = AbsenceTypeResources.NoAbsencesForThisEmployeeMessage;
            }

            return base.List(parameters);
        }

        public override ActionResult JsonList(string formatterId, GridParameters parameters)
        {
            var classMappingDto = _dependencies.ClassMappingFactory.CreateMapping<AbsenceTypeDto, AbsenceTypeViewModel>();
            var classMappingViewModel = _dependencies.ClassMappingFactory.CreateMapping<AbsenceTypeViewModel, AbsenceTypeDto>();

            var search = parameters.ToCriteria(
                classMappingViewModel,
                _anonymizationService,
                Enumerable.Empty<string>(), 
                CardIndex.Settings.FilterGroups);

            var records = _cardIndexDataService
                .GetRecords(search)
                .Rows
                .Select(classMappingDto.CreateFromSource)
                .Select(vm => new { id = vm.Id, toString = vm.ToString() })
                .ToList();

            return new JsonNetResult(records);
        }
    }
}