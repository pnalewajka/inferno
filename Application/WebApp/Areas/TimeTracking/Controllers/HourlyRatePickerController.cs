﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Identifier("ValuePickers.HourlyRates")]
    [AtomicAuthorize(SecurityRoleType.CanViewTimeReport)]
    public class HourlyRatePickerController : ReadOnlyCardIndexController<HourlyRateViewModel, HourlyRateDto>
    {
        public HourlyRatePickerController(IHourlyRateCardIndexService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}