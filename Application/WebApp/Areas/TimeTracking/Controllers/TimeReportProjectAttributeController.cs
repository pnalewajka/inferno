﻿using System.Linq;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTimeReportProjectAttribute)]
    public class TimeReportProjectAttributeController : CardIndexController<TimeReportProjectAttributeViewModel, TimeReportProjectAttributeDto>
    {
        public TimeReportProjectAttributeController(
            ITimeReportProjectAttributeCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = TimeReportProjectAttributeResources.CardIndexTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddTimeReportProjectAttribute;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditTimeReportProjectAttribute;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteTimeReportProjectAttribute;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportProjectAttributeResources.ValuesButton,
                Group = TimeReportProjectAttributeResources.ValuesButton,
                OnClickAction = Url.UriActionGet("List", "TimeReportProjectAttributeValue", KnownParameter.ParentId),
                RequiredRole = SecurityRoleType.CanViewJobMatrixLevel,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            CardIndex.Settings.AddFormCustomization += FormCustomization;
        }

        protected void FormCustomization(FormRenderingModel model)
        {
            var control = model.Controls.Single(c => c.Name == nameof(TimeReportProjectAttributeViewModel.DefaultValueId));
            control.Visibility = ControlVisibility.Hide;
        }
    }
}