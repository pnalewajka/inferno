﻿using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTimeReportProjectAttributeValuePicker)]
    [Identifier("Pickers.TimeTracking.TimeReportProjectAttributeValue")]
    public class TimeReportProjectAttributeValuePickerController
        : TimeReportProjectAttributeValueController
    {
        public TimeReportProjectAttributeValuePickerController(ITimeReportProjectAttributeValueCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}