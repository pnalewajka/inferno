﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Newtonsoft.Json;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.ReflectionApi.Builders;
using Smt.Atomic.Presentation.ReflectionApi.Models;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Areas.Workflows.Controllers;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewMyTimeReport, SecurityRoleType.CanViewMyTimeReportBeta,
        SecurityRoleType.CanEditReportOnBehalf)]
    public class MyTimeReportController : BaseEmployeeTimeReportController
    {
        private const string NoCodeImportCode = "";

        private readonly IClassMappingFactory _classMappingFactory;
        private readonly ITimeReportImportSourceService _importSourceService;
        private readonly IRequestCardIndexDataService _requestService;
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly ILogger _logger;
        private readonly ITimeReportService _timeReportService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;

        public MyTimeReportController(IBaseControllerDependencies baseDependencies,
            ITimeReportService timeReportService,
            ITimeService timeService,
            IOvertimeApprovalDataService overtimeApprovalDataService,
            ITimeReportImportSourceService importSourceService,
            IRequestCardIndexDataService requestService,
            IClassMappingFactory classMappingFactory,
            IRequestServiceResolver requestServiceResolver,
            IProjectService projectService,
            ITimeReportProjectAttributeService attributeService,
            ILogger logger,
            IAlertService alertService,
            IEmploymentPeriodService employmentPeriodService,
            ISystemParameterService systemParameterService)
            : base(
                baseDependencies,
                overtimeApprovalDataService,
                classMappingFactory,
                timeService,
                timeReportService,
                projectService,
                attributeService,
                alertService,
                employmentPeriodService
                )
        {
            _timeReportService = timeReportService;
            _timeService = timeService;
            _importSourceService = importSourceService;
            _requestService = requestService;
            _classMappingFactory = classMappingFactory;
            _requestServiceResolver = requestServiceResolver;
            _logger = logger;
            _systemParameterService = systemParameterService;
        }

        private MyTimeReportViewModel GetCurrentTimeReportViewModel(long employeeId, int year, byte month)
        {
            var employeeTimeReportDto = _timeReportService.GetMyTimeReport(employeeId, year, month);
            var viewModel = ToViewModel(employeeTimeReportDto);
            viewModel = AddModelMetada(viewModel, nameof(Report));

            return viewModel;
        }

        public ActionResult Report(int? year, byte? month, long? employeeId)
        {
            var defaultDate = _timeService.GetCurrentDate();

            if (year == null)
            {
                year = defaultDate.Year;
            }

            if (month == null)
            {
                month = (byte)defaultDate.Month;
            }

            if (IsTimeReportMaxDateExceeded(year.Value, month.Value))
            {
                throw new BusinessException(TimeReportResources.MaximumReportDateExceeded);
            }

            var principal = GetCurrentPrincipal();
            var reportEmployeeId = employeeId.HasValue ? employeeId.Value : principal.EmployeeId;
            var viewModel = GetCurrentTimeReportViewModel(reportEmployeeId, year.Value, month.Value);

            var importSourceDtos = _importSourceService.GetImportSources().ToList();

            var canViewRequest = principal.EmployeeId == viewModel.EmployeeId
                ? principal.IsInRole(SecurityRoleType.CanViewMyTimeReport)
                : principal.IsInRole(SecurityRoleType.CanViewOthersTimeReport);

            if (!canViewRequest)
            {
                if (principal.EmployeeId == viewModel.EmployeeId)
                {
                    throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanViewMyTimeReport.ToString() });
                }
                throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanViewOthersTimeReport.ToString() });
            }

            var canEditRequest = !viewModel.IsReadOnly && (principal.EmployeeId == viewModel.EmployeeId
                    ? principal.IsInRole(SecurityRoleType.CanSaveMyTimeReport)
                    : principal.IsInRole(SecurityRoleType.CanEditReportOnBehalf));

            if (!canEditRequest)
            {
                SetReadOnlyReport(viewModel);
            }

            var importSources = importSourceDtos.Select(s => new
            {
                Name = s.Name.ToString(),
                s.Code,
                Url = Url.Action(nameof(ImportFromSource),
                    new
                    {
                        inputSourceId = s.Id,
                        viewModel.EmployeeId,
                        viewModel.Year,
                        viewModel.Month
                    })
            }).ToList();

            importSources.Add(new
            {
                Name = TimeReportResources.AddLastMonthProjects,
                Code = NoCodeImportCode,
                Url = Url.Action(nameof(AddProjectsFromPreviousMonth),
                    new
                    {
                        viewModel.EmployeeId,
                        viewModel.Year,
                        viewModel.Month
                    })
            });

            importSources.Add(new
            {
                Name = TimeReportResources.AddLastMonthProjectsWithTasks,
                Code = NoCodeImportCode,
                Url = Url.Action(nameof(AddProjectsFromPreviousMonth),
                    new
                    {
                        viewModel.EmployeeId,
                        viewModel.Year,
                        viewModel.Month,
                        importTasks = true,
                    })
            });

            var toolbarOptions = new
            {
                CanSave = canEditRequest,
                CanSubmit = !viewModel.IsReadOnly && (principal.EmployeeId == viewModel.EmployeeId
                    ? principal.IsInRole(SecurityRoleType.CanSubmitMyTimeReport)
                    : principal.IsInRole(SecurityRoleType.CanEditReportOnBehalf)),
                CanImport = !viewModel.IsReadOnly && (principal.EmployeeId == viewModel.EmployeeId
                    ? principal.IsInRole(SecurityRoleType.CanImportMyTimeReport)
                    : principal.IsInRole(SecurityRoleType.CanEditReportOnBehalf)),
                ImportSources = importSources,
                CanSeePreviousMonth =
                    _timeReportService.IsPreviousMonthAvailable(viewModel.EmployeeId, viewModel.Year, viewModel.Month),
                CanSeeNextMonth =
                    !IsTimeReportMaxDateExceeded(new DateTime(viewModel.Year, viewModel.Month, 1).AddMonths(1)),
                NavigationActions = GetAddActions(viewModel)
            };

            return Ok(new
            {
                isMobile = Request.Browser.IsMobileDevice,
                viewModel,
                toolbarOptions,
                title = GetFormattedPageHeader(year.Value, month.Value, viewModel.Status, viewModel.EmployeeFullName)
            });
        }

        [AtomicAuthorize(SecurityRoleType.CanImportMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult ImportFromSource(long employeeId, long inputSourceId, int year, byte month)
        {
            CheckRoleForEditingOnBehalf(employeeId);

            var timeReport = _importSourceService.GetTimeReport(inputSourceId, employeeId, year, month);
            var importResult = _timeReportService.UpdateTimeReportFromImportSource(timeReport);

            AddAlerts(importResult.Alerts);

            return Ok(new
            {
                ImportAnyData = importResult.IsSuccessful,
                LoadUrl = Url.Action("Report", new
                {
                    Year = year,
                    Month = month,
                    EmployeeId = employeeId
                })
            });
        }

        [AtomicAuthorize(SecurityRoleType.CanSubmitMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult AddProjectsFromPreviousMonth(MyTimeReportViewModel myTimeReportViewModel, bool importTasks = false)
        {
            var year = myTimeReportViewModel.Year;
            var month = myTimeReportViewModel.Month;
            var employeeId = myTimeReportViewModel.EmployeeId;

            CheckRoleForEditingOnBehalf(employeeId);

            _timeReportService.AddEmployeeProjectsFromPreviousMonth(employeeId, year, month, importTasks);

            AddAlert(AlertType.Success, TimeReportResources.ProjectsHaveBeenImported);

            return Ok(new
            {
                ImportAnyData = true,
                LoadUrl = Url.Action("Report", new
                {
                    myTimeReportViewModel.Year,
                    myTimeReportViewModel.Month,
                    myTimeReportViewModel.EmployeeId
                })
            });
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSaveMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        public ActionResult SetDailyHours(long employeeId, long projectId, long? taskId, string taskName, decimal hours,
            DateTime day, long?[] attributeValueIds, HourlyRateType hourlyRateType = HourlyRateType.Regular)
        {
            var currentReport = GetCurrentTimeReportViewModel(employeeId, day.Year, (byte)day.Month);

            var project = currentReport.Projects.SingleOrDefault(p => p.ProjectId == projectId);

            if (project == null)
            {
                project = new MyTimeReportProjectViewModel { ProjectId = projectId };
                currentReport.Projects.Add(project);
            }

            MyTimeReportTaskViewModel task = null;

            if (taskId.HasValue)
            {
                task = project.Tasks.Single(t => t.Id == taskId.Value);
            }
            else
            {
                task = new MyTimeReportTaskViewModel();
                project.Tasks.Add(task);
            }

            var taskDay = task.Days.SingleOrDefault(d => d.Day == day);

            if (taskDay == null)
            {
                taskDay = new MyTimeReportDailyEntryViewModel { Day = day };
                task.Days.Add(taskDay);
            }

            task.Name = taskName;
            task.SelectedAttributesValueIds = attributeValueIds ?? Array.Empty<long?>();
            task.OvertimeVariant = hourlyRateType;
            taskDay.Hours = hours;

            return SaveTimeReport(currentReport);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSaveMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        public ActionResult DeleteTaskById(long employeeId, int year, byte month, long taskId)
        {
            var currentReport = GetCurrentTimeReportViewModel(employeeId, year, month);

            foreach (var project in currentReport.Projects)
            {
                project.Tasks = project.Tasks.Where(t => t.Id != taskId).ToList();
            }

            return SaveTimeReport(currentReport);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSaveMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        public ActionResult DeleteProjectById(long employeeId, int year, byte month, long projectId)
        {
            var currentReport = GetCurrentTimeReportViewModel(employeeId, year, month);

            currentReport.Projects = currentReport.Projects
                .Where(p => p.ProjectId != projectId)
                .ToList();

            return SaveTimeReport(currentReport);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSubmitMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        public ActionResult Submit(long employeeId, int year, byte month)
        {
            var currentReport = GetCurrentTimeReportViewModel(employeeId, year, month);

            return SubmitTimeReport(currentReport);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonNetResult MobileVersions()
        {
            var versions = _systemParameterService.GetParameter<string>(ParameterKeys.TimeTrackingMobileVersions);

            return new JsonNetResult(versions);
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanViewMyTimeReport)]
        public JsonNetResult HourlyRateTypes(bool localized = true)
        {
            var metadata = new EnumMetadataBuilder().GetMetadata<HourlyRateType>();
            var result = new JsonNetResult(metadata);
            var settings = result.SerializerSettings;

            if (localized)
            {
                settings.Converters = settings.Converters != null
                    ? settings.Converters
                    : new List<JsonConverter>();

                settings.Converters.Add(new LocalizedStringToStringConverter());
            }

            return result;
        }

        private ProjectTimeReportApprovalRequestDto CreateApprovalRequestDto(long employeeId, long projectId,
            IList<MyTimeReportRowDto> tasks)
        {
            var daysWithHours = tasks.SelectMany(t => t.Days).Where(d => d.Hours != 0).ToList();

            var rowToTaskMapping = _classMappingFactory.CreateMapping<MyTimeReportRowDto, TimeReportTaskApprovalDto>();

            return new ProjectTimeReportApprovalRequestDto
            {
                From = daysWithHours.Min(d => d.Day),
                To = daysWithHours.Max(d => d.Day),
                EmployeeId = employeeId,
                ProjectId = projectId,
                Tasks = tasks.Select(t => rowToTaskMapping.CreateFromSource(t)).ToList()
            };
        }

        private bool Submit(int year, byte month, long employeeId)
        {
            var validationAlerts = _timeReportService.ValidateFullTimeReport(employeeId, year, month);
            AddAlerts(validationAlerts);

            if (validationAlerts.ContainsErrors)
            {
                return false;
            }

            var prepareAlerts =
                _timeReportService.OnSubmit(year, month, employeeId);

            AddAlerts(prepareAlerts);

            if (prepareAlerts.ContainsErrors)
            {
                return false;
            }

            var timeReportDto = _timeReportService.GetMyTimeReport(employeeId, year, month);

            var pendingRows = timeReportDto.Rows
                .Where(r => r.Status == TimeReportStatus.Submitted)
                .ToList();

            var projectsTasks = timeReportDto.Rows
                .Where(r => r.Status != TimeReportStatus.Submitted && r.Status != TimeReportStatus.Accepted)
                .Where(r => r.Days.Any())
                .Where(r => r.Days.Sum(d => d.Hours) > 0)
                .GroupBy(r => r.ProjectId, rowDto => rowDto,
                    (projectId, rowsDto) => new { ProjectId = projectId, Tasks = rowsDto.ToList() });

            var requests = projectsTasks
                .Select(projectTasks => CreateApprovalRequestDto(employeeId, projectTasks.ProjectId, projectTasks.Tasks))
                .ToList();

            if (pendingRows.Any() || requests.Any())
            {
                var result = _requestService.CreateRequestsExternally(
                    RequestType.ProjectTimeReportApprovalRequest, requests);

                if (result.IsSuccessful)
                {
                    _timeReportService.UpdateTimeReportStatus(timeReportDto.TimeReportId, TimeReportStatus.Submitted);
                }

                AddAlerts(result.Alerts);
            }
            else
            {
                _timeReportService.UpdateTimeReportStatus(timeReportDto.TimeReportId, TimeReportStatus.Accepted);
            }

            return true;
        }

        private bool Save(MyTimeReportViewModel myTimeReportViewModel)
        {
            var originalTimeReportDto = _timeReportService.GetMyTimeReport(myTimeReportViewModel.EmployeeId,
                myTimeReportViewModel.Year, myTimeReportViewModel.Month);

            var myTimeReportViewModelToTimeReportDtoMapping =
                _classMappingFactory.CreateMapping<MyTimeReportViewModel, MyTimeReportDto>();

            var employeeTimeReportDto =
                myTimeReportViewModelToTimeReportDtoMapping.CreateFromSource(myTimeReportViewModel);

            var onSaveViolations = _timeReportService.CheckOnSaveViolations(employeeTimeReportDto, originalTimeReportDto);

            if (!ModelState.IsValid || !onSaveViolations.IsSuccessful)
            {
                if (onSaveViolations.Alerts.Any())
                {
                    AddAlerts(onSaveViolations.Alerts);
                }

                ModelState.Values.ForEach(v => v.Errors.ForEach(e => AddAlert(AlertType.Error, e.ErrorMessage)));

                myTimeReportViewModel = AddModelMetada(myTimeReportViewModel, nameof(Save));

                var timeReportDtoToMyTimeReportViewModelMapping =
                    _classMappingFactory.CreateMapping<MyTimeReportDto, MyTimeReportViewModel>();

                var originalTimeReportViewModel =
                    timeReportDtoToMyTimeReportViewModelMapping.CreateFromSource(originalTimeReportDto);

                myTimeReportViewModel.Days = originalTimeReportViewModel.Days;

                Layout.PageHeader =
                    Layout.PageTitle =
                        GetFormattedPageHeader(myTimeReportViewModel.Year, myTimeReportViewModel.Month,
                            myTimeReportViewModel.Status, myTimeReportViewModel.EmployeeFullName);

                return false;
            }

            var userTimeZone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(t => t.BaseUtcOffset == TimeSpan.FromMinutes(employeeTimeReportDto.TimeZoneOffsetMinutes));
            employeeTimeReportDto.TimeZoneDisplayName = userTimeZone != null ? userTimeZone.DisplayName : string.Empty;

            _timeReportService.UpdateFullTimeReport(employeeTimeReportDto);

            return true;
        }

        [AtomicAuthorize(SecurityRoleType.CanSubmitMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult SubmitConfirmDialog(long employeeId, int year, byte month)
        {
            CheckRoleForEditingOnBehalf(employeeId);

            var warnings = _timeReportService.GetUserTimeReportSubmitWarnings(employeeId, year, month).ToList();

            return Json(warnings);
        }

        [AtomicAuthorize(SecurityRoleType.CanSubmitMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult SubmitTimeReport(MyTimeReportViewModel myTimeReportViewModel)
        {
            CheckRoleForEditingOnBehalf(myTimeReportViewModel.EmployeeId);

            if (!Save(myTimeReportViewModel))
            {
                LogSaveActionFail(myTimeReportViewModel);
                
                return BadResult();
            }

            LogSaveActionSuccess(myTimeReportViewModel);

            if (Submit(myTimeReportViewModel.Year, myTimeReportViewModel.Month, myTimeReportViewModel.EmployeeId))
            {
                AddAlert(AlertType.Success, TimeReportResources.MyTimeReportSavedSuccessAlert);
            }

            return Report(myTimeReportViewModel.Year, myTimeReportViewModel.Month, myTimeReportViewModel.EmployeeId);
        }

        [AtomicAuthorize(SecurityRoleType.CanSaveMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult SaveTimeReport(MyTimeReportViewModel myTimeReportViewModel)
        {
            CheckRoleForEditingOnBehalf(myTimeReportViewModel.EmployeeId);

            var isSaved = Save(myTimeReportViewModel);

            if (isSaved)
            {
                LogSaveActionSuccess(myTimeReportViewModel);
                AddAlert(AlertType.Success, TimeReportResources.MyTimeReportSavedSuccessAlert);
                
                return Report(myTimeReportViewModel.Year, myTimeReportViewModel.Month, myTimeReportViewModel.EmployeeId);
            }

            LogSaveActionFail(myTimeReportViewModel);
            AddMessageAtTop(AlertType.Error, TimeReportResources.TimeReportWasNotSavedMessage);

            return BadResult();
        }

        private string GetAddRequestUrl(RequestType requestType, MyTimeReportViewModel viewModel)
        {
            var requestTypeUrlValue = NamingConventionHelper.ConvertPascalCaseToHyphenated(requestType.ToString());
            var url = Url.Action("Add", "Request", new RouteValueDictionary
            {
                {"area", "Workflows"},
                {RequestControllerHelper.RequestTypeUrlParameterName, requestTypeUrlValue},
                {"request-object-view-model.employeeId", viewModel.EmployeeId},
                {"request-object-view-model.year", viewModel.Year},
                {"request-object-view-model.month", viewModel.Month}
            });

            return url;
        }

        private Dictionary<RequestType, string> RequestActions(MyTimeReportViewModel viewModel)
        {
            var dictionary = new Dictionary<RequestType, string>
            {
                {RequestType.AbsenceRequest, WorkflowResources.AbsenceRequestName},
                {RequestType.OvertimeRequest, WorkflowResources.OvertimeRequestName},
                {RequestType.AddHomeOfficeRequest, WorkflowResources.AddHomeOfficeRequesName}
            };

            if (viewModel.Status == TimeReportStatus.Accepted
                || viewModel.Status == TimeReportStatus.Submitted
                || viewModel.Status == TimeReportStatus.Rejected)
            {
                dictionary.Add(RequestType.ReopenTimeTrackingReportRequest, WorkflowResources.ReopenTimeTrackingReportRequestName);
            }

            return dictionary;
        }

        private IEnumerable<object> GetAddActions(MyTimeReportViewModel viewModel)
        {
            var principal = GetCurrentPrincipal();
            var requestActions = RequestActions(viewModel);

            foreach (var requestAction in requestActions)
            {
                var actionRole = _requestServiceResolver.GetByServiceType(requestAction.Key).RequiredRoleType;

                if (!actionRole.HasValue || principal.IsInRole(actionRole.Value))
                {
                    yield return new
                    {
                        Text = requestAction.Value,
                        OnClickUrl = GetAddRequestUrl(requestAction.Key, viewModel)
                    };
                }
            }
        }

        public ActionResult Index(int? year, byte? month, long? employeeId)
        {
            Layout.PageHeaderViewName = "~/Areas/TimeTracking/Views/MyTimeReport/CustomHeader.cshtml";

            ViewBag.serviceUrls = new
            {
                LoadReport = Url.Action("Report", new { year, month, employeeId }),
                SaveReport = Url.Action("SaveTimeReport", new { year, month, employeeId }),
                SubmitReport = Url.Action("SubmitTimeReport", new { year, month, employeeId }),
                ConfirmReport = Url.Action("SubmitConfirmDialog", new { year, month, employeeId })
            };

            return View("~/Areas/TimeTracking/Views/MyTimeReport/Index.cshtml");
        }

        [PerformanceTest()]
        public ActionResult MyTimeReport(int? year, byte? month, long? employeeId)
        {
            return Index(year, month, employeeId);
        }

        protected override IEnumerable<CommandButton> TimesheetImportCommandButtons(MyTimeReportViewModel viewModel,
            string actionName)
        {
            yield break;
        }

        protected override IEnumerable<TimeTrackingImportSourceViewModel> GetImportSources()
        {
            yield break;
        }

        private void LogSaveActionSuccess(MyTimeReportViewModel myTimeReportViewModel)
        {
            _logger.Warn($"TimeReportSaveSuccess: ({GetActionLogMessage(myTimeReportViewModel)})");
        }

        private void LogSaveActionFail(MyTimeReportViewModel myTimeReportViewModel)
        {
            _logger.Warn($"TimeReportSaveFail: ({GetActionLogMessage(myTimeReportViewModel)})");
        }

        private string GetActionLogMessage(MyTimeReportViewModel myTimeReportViewModel)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendFormat($"Employee: {myTimeReportViewModel.EmployeeId};");
                stringBuilder.AppendFormat($"User: {CurrentPrincipal.Login};");
                stringBuilder.AppendFormat($"Year: {myTimeReportViewModel.Year};");
                stringBuilder.AppendFormat($"Month: {myTimeReportViewModel.Month};");

                foreach (var project in myTimeReportViewModel.Projects)
                {
                    stringBuilder.AppendFormat(
                        $"Project: {project.ProjectId}, Hours: {project.Tasks.Select(t => t.Days.Sum(d => d.Hours)).Sum()};");
                }

                return stringBuilder.ToString();
            }
            catch
            {
                return $"Can't serialize {nameof(MyTimeReportViewModel)}";
            }
        }
    }
}