using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Resources;

using AtomicAction = Smt.Atomic.Presentation.Renderers.Toolbar.Actions.Action;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    public abstract class BaseEmployeeTimeReportController : AtomicController
    {
        protected readonly IAtomicPrincipal CurrentPrincipal;
        protected readonly IClassMappingFactory ClassMappingFactory;
        protected readonly IOvertimeApprovalDataService OvertimeApprovalDataService;
        protected readonly ITimeService TimeService;
        protected readonly ITimeReportService TimeReportService;
        protected readonly IEmploymentPeriodService EmploymentPeriodService;
        private readonly IProjectService _projectService;
        private readonly ITimeReportProjectAttributeService _attributeService;
        private readonly IAlertService _alertService;

        protected BaseEmployeeTimeReportController(
            IBaseControllerDependencies baseDependencies,
            IOvertimeApprovalDataService overtimeApprovalDataService,
            IClassMappingFactory classMappingFactory,
            ITimeService timeService,
            ITimeReportService timeReportService,
            IProjectService projectService,
            ITimeReportProjectAttributeService attributeService,
            IAlertService alertService,
            IEmploymentPeriodService employmentPeriodService
            ) : base(baseDependencies)
        {
            CurrentPrincipal = GetCurrentPrincipal();
            OvertimeApprovalDataService = overtimeApprovalDataService;
            ClassMappingFactory = classMappingFactory;
            TimeService = timeService;
            TimeReportService = timeReportService;
            EmploymentPeriodService = employmentPeriodService;
            _projectService = projectService;
            _attributeService = attributeService;
            _alertService = alertService;

            Layout.Css.Add("~/Areas/TimeTracking/Content/MyTimeReport.css");

            Layout.Resources.AddFrom<TimeReportResources>();

            Layout.Resources.AddFrom<EnumResources>("TimeReportStatusNotStarted");
            Layout.Resources.AddFrom<EnumResources>("TimeReportStatusDraft");
            Layout.Resources.AddFrom<EnumResources>("TimeReportStatusSubmitted");
            Layout.Resources.AddFrom<EnumResources>("TimeReportStatusDraftAcepted");
            Layout.Resources.AddFrom<EnumResources>("TimeReportStatusDraftRejected");
            Layout.Resources.AddFrom<EnumResources>("HourlyRateTypeRegular");
            Layout.Resources.AddFrom<EnumResources>("HourlyRateTypeOverTimeNormal");
            Layout.Resources.AddFrom<EnumResources>("HourlyRateTypeOverTimeLateNight");
            Layout.Resources.AddFrom<EnumResources>("HourlyRateTypeOverTimeWeekend");
            Layout.Resources.AddFrom<EnumResources>("HourlyRateTypeOverTimeHoliday");
            Layout.Resources.AddFrom<EnumResources>("HourlyRateTypeOverTimeBank");
            Layout.Resources.AddFrom<LayoutResources>("CurrentMonth");
            Layout.Resources.AddFrom<LayoutResources>("NextMonth");
            Layout.Resources.AddFrom<LayoutResources>("PreviousMonth");
            Layout.Resources.AddFrom<AtomicResources>("AddButtonText");
        }

        protected abstract IEnumerable<TimeTrackingImportSourceViewModel> GetImportSources();


        [AtomicAuthorize(SecurityRoleType.CanSaveMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpGet]
        public ActionResult GetProjectMetadata(long employeeId, long projectId, int year, int month)
        {
            CheckRoleForEditingOnBehalf(employeeId);

            var project = _projectService.GetProjectById(projectId);

            var model = new ProjectMetadataViewModel
            {
                ProjectId = project.Id,
                ProjectName = project.ClientProjectName,
                AllowedProjectOvertime = project.AllowedOvertimeTypes,
                Apn = project.Apn,
                KupferwerkProjectId = project.KupferwerkProjectId,
                OvertimeApprovals = GetOvertimeApprovals(new[] { projectId }, employeeId, year, month),
                ProjectAttributes = GetProjectAttributes(projectId),
                ReportingStartsOn = project.ReportingStartsOn,
            };

            return new JsonNetResult(model);
        }

        protected void CheckRoleForEditingOnBehalf(long employeeId)
        {
            if (CurrentPrincipal.EmployeeId != employeeId && !CurrentPrincipal.IsInRole(SecurityRoleType.CanEditReportOnBehalf))
            {
                throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanEditReportOnBehalf.ToString() });
            }
        }

        protected IList<MyTimeReportProjectAttributeViewModel> GetProjectAttributes(long projectId)
        {
            var attributeDtos = _attributeService.GetAttributesByProjectId(projectId);
            var mapping =
                ClassMappingFactory.CreateMapping<TimeReportProjectAttributeDto, MyTimeReportProjectAttributeViewModel>();

            return attributeDtos.Select(mapping.CreateFromSource).ToList();
        }

        protected MyTimeReportViewModel AddModelMetada(MyTimeReportViewModel model, string actionName, bool forceReadOnly = false)
        {
            model.IsClockCardRequired = EmploymentPeriodService.IsClockCardRequiredInGivenMonth(model.EmployeeId, model.Year, model.Month);
            model.IsMyTimeReport = CurrentPrincipal.EmployeeId == model.EmployeeId;
            model.IsReadOnly |= forceReadOnly;
            model.ImportSources = GetImportSources().ToList();
            model.IsTimeReportAvailablePreviousMonth = TimeReportService.IsPreviousMonthAvailable(model.EmployeeId, model.Year, model.Month);
            model.CanBeReopened = TimeReportService.CanBeReopened(model.EmployeeId, model.Year, model.Month);
            model.OvertimeApprovals = GetOvertimeApprovals(model).ToList();
            model.ToolbarButtons = new CommandButtonsViewModel(TimesheetImportCommandButtons(model, actionName));

            return model;
        }

        protected void SetReadOnlyReport(MyTimeReportViewModel model)
        {
            model.IsReadOnly = true;

            foreach (var project in model.Projects)
            {
                project.IsReadOnly = true;

                foreach (var task in project.Tasks)
                {
                    task.IsReadOnly = true;

                    foreach (var day in task.Days)
                    {
                        day.IsReadOnly = true;
                    }
                }
            }
        }

        protected MyTimeReportViewModel AddModelMetada(MyTimeReportDto timeReportDto, string actionName, bool forceReadOnly = false)
        {
            var timeReportDtoToMyTimeReportViewModelMapping =
                ClassMappingFactory.CreateMapping<MyTimeReportDto, MyTimeReportViewModel>();

            var viewModel = timeReportDtoToMyTimeReportViewModelMapping.CreateFromSource(timeReportDto);

            return AddModelMetada(viewModel, actionName, forceReadOnly);
        }

        protected IEnumerable<OvertimeApprovalViewModel> GetOvertimeApprovals(IReadOnlyCollection<long> projectIds,
            long employeeId, int year, int month)
        {
            var overtimeApprovalClassMapping =
                ClassMappingFactory.CreateMapping<OvertimeApprovalDto, OvertimeApprovalViewModel>();

            var from = new DateTime(year, month, 1);
            var to = DateHelper.EndOfMonth(from);

            var overtimeApprovalDtos = OvertimeApprovalDataService.OvertimeApprovals(projectIds, from, to, employeeId);
            var approvals = overtimeApprovalDtos.Select(overtimeApprovalClassMapping.CreateFromSource).ToList();

            foreach (var projectId in projectIds.Where(p => approvals.All(a => a.ProjectId != p)))
            {
                approvals.Add(new OvertimeApprovalViewModel
                {
                    DateFrom = from,
                    DateTo = to,
                    ProjectId = projectId,
                    HourLimit = 0,
                    EmployeeId = employeeId
                });
            }

            return approvals;
        }

        protected IEnumerable<OvertimeApprovalViewModel> GetOvertimeApprovals(
            MyTimeReportViewModel myTimeReportViewModel)
        {
            var projectIds = myTimeReportViewModel.Projects.Select(p => p.ProjectId).ToList();

            return GetOvertimeApprovals(projectIds, myTimeReportViewModel.EmployeeId, myTimeReportViewModel.Year,
                myTimeReportViewModel.Month);
        }

        protected string GetFormattedPageHeader(MyTimeReportViewModel myTimeReportViewModel)
        {
            var statusDescription = myTimeReportViewModel.Status.GetDescription().ToLower();

            return myTimeReportViewModel.IsMyTimeReport
                ? string.Format(TimeReportResources.MyTimeReportPageTitleFormat, new DateTime(myTimeReportViewModel.Year, myTimeReportViewModel.Month, 1), statusDescription)
                : string.Format(TimeReportResources.TimeReportPageTitleFormat, new DateTime(myTimeReportViewModel.Year, myTimeReportViewModel.Month, 1), myTimeReportViewModel.EmployeeFullName, statusDescription);
        }

        protected abstract IEnumerable<CommandButton> TimesheetImportCommandButtons(MyTimeReportViewModel viewModel, string actionName);

        protected IEnumerable<CommandButton> TimeCommandButtons(MyTimeReportViewModel viewModel, string actionController)
        {
            var currentDate = TimeService.GetCurrentDate();
            var currentSetMonth = new DateTime(viewModel.Year, viewModel.Month, 1);
            var prevMonth = currentSetMonth.AddMonths(-1);
            var nextMonth = currentSetMonth.AddMonths(1);
            var requiredRole = IsMyTimeReport(viewModel.EmployeeId)
                ? SecurityRoleType.CanViewMyTimeReport
                : SecurityRoleType.CanViewOthersTimeReport;

            var prevCommandButton = new ToolbarButton
            {
                Text = LayoutResources.PreviousMonth,
                Icon = FontAwesome.CaretLeft,
                OnClickAction = GetOnClickAction(prevMonth, viewModel.EmployeeId, actionController),
                RequiredRole = requiredRole,
                Group = "nextPrev"
            };

            if (!viewModel.IsTimeReportAvailablePreviousMonth)
            {
                prevCommandButton.Availability = new DisabledCommandButtonAvailability();
                prevCommandButton.OnClickAction = new JavaScriptCallAction("return false;");
            }
            yield return prevCommandButton;

            yield return new ToolbarButton
            {
                Text = LayoutResources.CurrentMonth,
                Icon = FontAwesome.MapMarker,
                OnClickAction = GetOnClickAction(currentDate, viewModel.EmployeeId, actionController),
                RequiredRole = requiredRole,
                Group = "nextPrev"
            };

            var nextCommandButton = new ToolbarButton
            {
                Text = LayoutResources.NextMonth,
                Icon = FontAwesome.CaretRight,
                OnClickAction = GetOnClickAction(nextMonth, viewModel.EmployeeId, actionController),
                RequiredRole = requiredRole,
                Group = "nextPrev"
            };

            if (IsTimeReportMaxDateExceeded(nextMonth.Year, (byte)nextMonth.Month))
            {
                nextCommandButton.Availability = new DisabledCommandButtonAvailability();
                nextCommandButton.OnClickAction = new JavaScriptCallAction("return false;");
            }

            yield return nextCommandButton;
        }

        protected IEnumerable<CommandButton> TimeReportSupervisorButtons(MyTimeReportViewModel viewModel)
        {
            var openedTimeReportCommandButton = new ToolbarButton
            {
                Text = TimeReportResources.ReopenTimeReport,
                Icon = FontAwesome.Star,
                RequiredRole = SecurityRoleType.CanSuperviseTimeReport,
            };

            if (TimeReportService.CanBeReopened(viewModel.EmployeeId, viewModel.Year, viewModel.Month))
            {
                openedTimeReportCommandButton.OnClickAction =
                    new JavaScriptCallAction(
                        $"Dialogs.showDialog('{{ actionUrl: {Url.Action("ReopenTimeReport", "TimeReport")}', formData: {{ employeeId: {viewModel.EmployeeId}, year: {viewModel.Year}, month: {viewModel.Month} }} }})");
            }
            else
            {
                openedTimeReportCommandButton.Availability = new DisabledCommandButtonAvailability();
                openedTimeReportCommandButton.OnClickAction = new JavaScriptCallAction("return false;");
            }

            yield return openedTimeReportCommandButton;
        }

        private AtomicAction GetOnClickAction(DateTime date, long employeeId, string actionController)
        {
            if (IsMyTimeReport(employeeId))
            {
                return Url.UriActionGet(actionController, KnownParameter.None,
                    nameof(MyTimeReportViewModel.Year), date.Year.ToString(),
                    nameof(MyTimeReportViewModel.Month), date.Month.ToString()
                );
            }

            return Url.UriActionGet(actionController, KnownParameter.None,
                nameof(MyTimeReportViewModel.Year), date.Year.ToString(),
                nameof(MyTimeReportViewModel.Month), date.Month.ToString(),
                nameof(MyTimeReportViewModel.EmployeeId), employeeId.ToString()
            );
        }

        protected bool IsTimeReportMaxDateExceeded(DateTime dateTime)
        {

            var maxAllowedDate = DateHelper.BeginningOfMonth(TimeService.GetCurrentDate().AddMonths(1));
            var requested = DateHelper.BeginningOfMonth(dateTime);

            return requested > maxAllowedDate;
        }

        protected bool IsTimeReportMaxDateExceeded(int requestedYear, byte requestedMonth)
        {
            return IsTimeReportMaxDateExceeded(new DateTime(requestedYear, requestedMonth, 1));
        }

        protected bool IsMyTimeReport(long employeeId)
        {
            return CurrentPrincipal.EmployeeId == employeeId;
        }

        protected MyTimeReportViewModel ToViewModel(MyTimeReportDto model)
        {
            var timeReportDtoToMyTimeReportViewModelMapping =
                ClassMappingFactory.CreateMapping<MyTimeReportDto, MyTimeReportViewModel>();

            return timeReportDtoToMyTimeReportViewModelMapping.CreateFromSource(model);
        }


        protected string GetFormattedPageHeader(int year, int month, TimeReportStatus status, string employeeName)
        {
            return string.Format(TimeReportResources.TimeReportPageTitleFormat, new DateTime(year, month, 1),
                employeeName, status.GetDescription());
        }

        protected void AddMessageAtTop(AlertType alertType, string message, bool isDismissable = true)
        {
            var currentAlerts = _alertService.GetAlerts();
            _alertService.ClearAlerts();
            AddAlert(alertType, message, isDismissable);

            foreach (var alertViewModel in currentAlerts)
            {
                AddAlert(alertViewModel.Type, alertViewModel.Message, alertViewModel.IsDismissable);
            }
        }

        protected ActionResult BadResult()
        {
            var alerts = _alertService.GetAlerts();
            _alertService.ClearAlerts();

            return new JsonNetResult(new { _alerts = alerts, _statusCode = 400 });
        }

        protected ActionResult Ok<T>(T model)
        {
            var alerts = _alertService.GetAlerts();
            _alertService.ClearAlerts();

            return new JsonNetResult(new { _payload = model, _alerts = alerts, _statusCode = 200 });
        }

        protected ActionResult Ok()
        {
            return Ok(new object());
        }
    }
}