﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Identifier("TimeTrackingValuePickers.ImportSource")]
    [AtomicAuthorize(SecurityRoleType.CanViewImportSource)]
    public class TimeTrackingImportSourceController : CardIndexController<TimeTrackingImportSourceViewModel, TimeTrackingImportSourceDto>
    {
        public TimeTrackingImportSourceController(ITimeTrackingImportSourceCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
           : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = ImportSourceResources.ImportSourcePageTitle;

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewImportSource;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddImportSource;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditImportSource;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteImportSource;
        }
    }
}
