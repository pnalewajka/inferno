﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Identifier("TimeTracking.EmployeeProjects")]
    [AtomicAuthorize(SecurityRoleType.CanSearchTimeTrackingProjects)]
    public class TimeTrackingProjectPickerController :
        ReadOnlyCardIndexController<TimeTrackingProjectViewModel, TimeTrackingProjectDto, TimeTrackingProjectPickerContext>
    {
        private readonly ITimeTrackingProjectCardIndexDataService _cardIndexDataService;

        private readonly IClassMapping<TimeTrackingProjectDto, TimeTrackingProjectViewModel>
            _timeTrackingProjectViewModelClassMapping;

        public TimeTrackingProjectPickerController(ITimeTrackingProjectCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IClassMapping<TimeTrackingProjectDto, TimeTrackingProjectViewModel> timeTrackingProjectViewModelClassMapping)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _timeTrackingProjectViewModelClassMapping = timeTrackingProjectViewModelClassMapping;
        }

        public override ActionResult JsonList(string formatterId, GridParameters parameters)
        {
            var queryCriteria = new QueryCriteria() { Context = parameters.Context };
            var searchPhrase = parameters.SearchPhrase;

            if (!string.IsNullOrEmpty(searchPhrase))
            {
                queryCriteria.SearchPhrase = searchPhrase;
            }

            var records = _cardIndexDataService.GetRecords(queryCriteria);
            var viewModelRecords =
                records.Rows.Select(_timeTrackingProjectViewModelClassMapping.CreateFromSource).ToList();

            return new JsonNetResult(viewModelRecords);
        }
    }
}
