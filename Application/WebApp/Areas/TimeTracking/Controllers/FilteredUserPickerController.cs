﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Business.Accounts.Interfaces;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.WebApp.Models;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Identifier("ValuePickers.FilteredUser")]
    [AtomicAuthorize(SecurityRoleType.CanViewUsersPicker)]
    public class FilteredUserPickerController
        : ReadOnlyCardIndexController<UserViewModel, UserDto, FilteredUserPickerContext>
        , IHubController
    {
        private const string MinimalViewId = "minimal";
        private readonly IEmployeeService _employeeService;
        private readonly IClassMapping<EmployeeDto, EmployeeViewModel> _employeeDtoToViewModelMapping;

        public FilteredUserPickerController(
            IFilteredUserPickerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IEmployeeService employeeService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _employeeService = employeeService;
            _employeeDtoToViewModelMapping = baseControllerDependencies.ClassMappingFactory.CreateMapping<EmployeeDto, EmployeeViewModel>();

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            CardIndex.Settings.ShouldHideFilters = true;
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<UserViewModel>(UserResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<UserViewModel, object>>>
            {
                c => c.FirstName,
                x => x.LastName,
                x => x.Login,
                x => x.Email
            });
            configurations.Add(configuration);

            return configurations;
        }

        public override void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            base.PrepareForValuePicker(allowMultipleRowSelection);

            Layout.PageTitle = allowMultipleRowSelection
                ? AuthenticationResources.ValuePickerMultipleTitle
                : AuthenticationResources.ValuePickerSingleTitle;
        }

        public PartialViewResult Hub(long id)
        {
            var user = CardIndex.GetRecordById(id);
            var employee = _employeeService.GetEmployeeOrDefaultByUserId(id);

            var model = new UserEmployeeHubViewModel(GetCurrentPrincipal())
            {
                Employee = employee != null ? _employeeDtoToViewModelMapping.CreateFromSource(employee) : null,
                User = user,
            };

            var dialogModel = new ModelBasedDialogViewModel<UserEmployeeHubViewModel>(model, StandardButtons.None)
            {
                ClassName = "user-employee-hub",
            };

            return PartialView(
                "~/Views/Shared/_UserEmployeeHub.cshtml",
                dialogModel);
        }
    }
}