﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Compensation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Models;
using Smt.Atomic.WebApp.Areas.ServiceOperations.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Resources;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTimeReport)]
    public class TimeReportController : ReadOnlyCardIndexController<TimeReportViewModel, TimeReportDto, TimeReportContext>
    {
        private readonly ITimeReportService _timeReportService;
        private readonly ITimeService _timeService;
        private readonly ITimeReportInvoiceService _timeReportInvoiceService;
        private readonly ITimeReportControllingSynchronizationService _timeReportControllingSynchronizationService;
        private readonly ITimeReportInvoiceNotificationService _timeReportInvoiceNotificationService;

        public TimeReportController(ITimeReportCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies,
            ITimeReportService timeReportService,
            ITimeService timeService,
            ITimeReportInvoiceService timeReportInvoiceService,
            ITimeReportInvoiceNotificationService timeReportInvoiceNotificationService,
            ITimeReportControllingSynchronizationService timeReportControllingSynchronizationService)
            : base(cardIndexDataService, dependencies)
        {
            _timeReportService = timeReportService;
            _timeService = timeService;
            _timeReportInvoiceService = timeReportInvoiceService;
            _timeReportInvoiceNotificationService = timeReportInvoiceNotificationService;
            _timeReportControllingSynchronizationService = timeReportControllingSynchronizationService;

            CardIndex.Settings.Title = TimeReportResources.TimeReportTitle;
            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations().ToList();
            CardIndex.Settings.FilterGroups.AddRange(CreateWorkReportsScopeFilteringGroup());
            CardIndex.Settings.FilterGroups.AddRange(OthersFilteringGroup());

            CardIndex.Settings.ViewButton.OnClickAction = new JavaScriptCallAction($"TimeReport.viewTimeReport(grid)");
            CardIndex.Settings.ViewButton.Availability = new ToolbarButtonAvailability
            {
                Predicate = $"row.status != {(int)TimeReportStatus.NotStarted}",
                Mode = AvailabilityMode.SingleRowSelected
            };

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportTimeTrackingReports);

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.ReopenTimeReport,
                Icon = FontAwesome.Star,
                OnClickAction = new JavaScriptCallAction($"TimeReport.reopenTimeReport(grid, '{Url.Action(nameof(ReopenTimeReport))}')"),
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanSuperviseTimeReport,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.canBeReopened",
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            var employeeId = GetCurrentPrincipal().EmployeeId;

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.EditOnBehalf,
                Icon = FontAwesome.Edit,
                OnClickAction = new JavaScriptCallAction("TimeReport.editOnBehalf(grid)"),
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanEditReportOnBehalf,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.employeeId != {employeeId}",
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            var invoiceActionUrl = Url.Action(
                nameof(InvoiceController.Invoice), RoutingHelper.GetControllerName<InvoiceController>(),
                new RouteValueDictionary { [RoutingHelper.AreaParameterName] = RoutingHelper.GetAreaName(typeof(InvoiceController)) });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.UploadInvoiceOnBehalf,
                Icon = FontAwesome.Upload,
                OnClickAction = new JavaScriptCallAction($"TimeReport.uploadInvoice(grid, '{invoiceActionUrl}');"),
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanUploadInvoiceOnBehalf,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.invoiceStatus != {(int)InvoiceStatus.InvoiceNotApplicable}",
                    Mode = AvailabilityMode.SingleRowSelected
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.ChangeProjectButtonText,
                RequiredRole = SecurityRoleType.CanReplaceProjectInReports,
                OnClickAction = Url.UriActionGet(nameof(TimeReportChangeServiceController.ChangeTimeReports), RoutingHelper.GetControllerName<TimeReportChangeServiceController>(),
                                                 KnownParameter.None, "area", RoutingHelper.GetAreaName(typeof(TimeReportChangeServiceController))),
                Icon = FontAwesome.Edit,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.SendToControlling,
                OnClickAction = new JavaScriptCallAction("TimeReport.sendToControlling(grid);"),
                Icon = FontAwesome.Forward,
                RequiredRole = SecurityRoleType.CanSendTimeReportsToControlling,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.SendInvoiceNotification,
                OnClickAction = new JavaScriptCallAction("TimeReport.sendInvoiceNotification(grid);"),
                Icon = FontAwesome.EnvelopeO,
                RequiredRole = SecurityRoleType.CanSendInvoiceNotification,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.AcceptInvoiceForPayment,
                OnClickAction = new JavaScriptCallAction("TimeReport.acceptInvoicesForPayment(grid);"),
                Icon = FontAwesome.Money,
                RequiredRole = SecurityRoleType.CanChangeInvoiceStatus,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.RejectInvoice,
                OnClickAction = new JavaScriptCallAction($"TimeReport.rejectInvoices(grid, '{Url.Action(nameof(RejectInvoices))}');"),
                Icon = FontAwesome.Ban,
                RequiredRole = SecurityRoleType.CanChangeInvoiceStatus,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected
                }
            });

            CardIndex.Settings.ToolbarButtons.Add(new ToolbarButton
            {
                Text = TimeReportResources.Statistics,
                Icon = FontAwesome.BarChart,
                OnClickAction = new JavaScriptCallAction($"TimeReport.statistics(grid, '{Url.Action(nameof(Statistics))}')"),
                Visibility = CommandButtonVisibility.Grid,
                RequiredRole = SecurityRoleType.CanSuperviseTimeReport,
            });

            Layout.Css.Add("~/Areas/TimeTracking/Content/TimeReport.css");
            Layout.Scripts.Add("~/Areas/TimeTracking/Scripts/TimeReport.js");

            Layout.Resources.AddFrom<TimeReportResources>("ReopenDialogMessage");
            Layout.Resources.AddFrom<TimeReportResources>("RejectReasonLabel");
            Layout.Resources.AddFrom<GeneralResources>("AreYouSure");
            Layout.Resources.AddFrom<TimeReportResources>("SendToControlling");
            Layout.Resources.AddFrom<TimeReportResources>("SendSelectedTimeReportsToControlling");
            Layout.Resources.AddFrom<TimeReportResources>("SendAllTimeReportsToControlling");
            Layout.Resources.AddFrom<TimeReportResources>(nameof(TimeReportResources.SendInvoiceNotification));
            Layout.Resources.AddFrom<TimeReportResources>(nameof(TimeReportResources.SendSelectedInvoiceNotifications));
            Layout.Resources.AddFrom<TimeReportResources>(nameof(TimeReportResources.SendAllInvoiceNotifications));
            Layout.Resources.AddFrom<TimeReportResources>(nameof(TimeReportResources.AcceptInvoiceForPayment));
            Layout.Resources.AddFrom<TimeReportResources>(nameof(TimeReportResources.AcceptSelectedInvoicesForPayment));
            Layout.Resources.AddFrom<TimeReportResources>(nameof(TimeReportResources.RejectSelectedInvoices));
        }

        [BreadcrumbBar("TimeReportList")]
        public override ActionResult List(GridParameters parameters)
        {
            var timeReportContext = (TimeReportContext)parameters.Context;
            var filters = FiltersHelper.ParseFilterString(parameters.Filters);

            if (!filters.Any(f => f.Codes.Any(c => c == FilterCodes.MyTimeReports)))
            {
                var contextDate = GetContextDate();
                var currentDate = _timeService.GetCurrentDate();

                CardIndex.Settings.ToolbarButtons.InsertRange(0, GetTimeCommandButtons(currentDate, contextDate));

                timeReportContext.TimeReportYear = contextDate.Year;
                timeReportContext.TimeReportMonth = (byte)contextDate.Month;
            }
            else
            {
                timeReportContext.TimeReportYear = default(int);
                timeReportContext.TimeReportMonth = default(byte);
            }

            return base.List(parameters);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSuperviseTimeReport)]
        public ActionResult ReopenTimeReport(TimeReportReopenViewModel viewModel, bool forceSetProjects = true)
        {
            if (forceSetProjects)
            {
                viewModel.ProjectsToReopen =
                    _timeReportService.GetReopenableProjectTimeReportApprovalRequests(
                        viewModel.EmployeeId,
                        viewModel.Year,
                        (byte)viewModel.Month)
                            .Select(p => p.Value.ProjectId)
                            .Distinct()
                            .ToArray();
            }

            var dialogModel = new ModelBasedDialogViewModel<TimeReportReopenViewModel>(viewModel)
            {
                Title = TimeReportResources.ReopenTimeReport,
            };

            return PartialView("ReopenTimeReport", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSuperviseTimeReport)]
        public ActionResult Statistics(TimeReportViewModel viewModel)
        {
            var dto = _timeReportService.GetTimeReport(viewModel.EmployeeId, viewModel.Year, viewModel.Month);

            var statisticsViewModel = new TimeReportStatisticsViewModel
            {
                EmployeeFullName = $"{dto.EmployeeFirstName} {dto.EmployeeLastName}",
                Year = dto.Year,
                Month = dto.Month,
                ContractedHours = dto.ContractedHours,
                ReportedHours = dto.ReportedNormalHours,
                OvertimeHours = dto.OvertimeHours,
                TaxDeductHours = dto.TaxDeductHours,
                BillableHours = dto.BillableHours,
                NonBillableHours = dto.NonBillableHours,
                HoursPerOvertimeType = dto.HoursPerOvertimeType,
            };

            var dialogModel = new ModelBasedDialogViewModel<TimeReportStatisticsViewModel>(statisticsViewModel, StandardButtons.Close)
            {
                Title = TimeReportResources.Statistics,
            };

            return PartialView("TimeReportStatistics", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSuperviseTimeReport)]
        public ActionResult PerformReopenTimeReport(TimeReportReopenViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return ReopenTimeReport(model, false);
            }

            var result = _timeReportService.ReopenTimeReport(model.EmployeeId, model.Year, (byte)model.Month, model.Reason, model.ProjectsToReopen);

            AddAlerts(result.Alerts);

            return DialogHelper.Reload();
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSendTimeReportsToControlling)]
        public ActionResult SendToControlling(IList<long> ids = null, int? year = null, int? month = null)
        {
            if (!ids.IsNullOrEmpty())
            {
                _timeReportControllingSynchronizationService.ForceSynchronization(ids);

                return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
            }

            if (year == null || month == null)
            {
                var contextDate = GetContextDate();

                year = contextDate.Year;
                month = contextDate.Month;
            }

            _timeReportControllingSynchronizationService.MarkAllForSynchronization(year.Value, month.Value);

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSendInvoiceNotification)]
        public ActionResult SendInvoiceNotification(IList<long> ids = null, int? year = null, int? month = null)
        {
            if (!ids.IsNullOrEmpty())
            {
                _timeReportInvoiceNotificationService.MarkForRecalculating(ids);

                return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
            }

            if (year == null || month == null)
            {
                var contextDate = GetContextDate();

                year = contextDate.Year;
                month = contextDate.Month;
            }

            _timeReportInvoiceNotificationService.MarkAllForRecalculating(year.Value, month.Value);

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanChangeInvoiceStatus)]
        public ActionResult AcceptInvoicesForPayment(IList<long> ids)
        {
            var result = _timeReportInvoiceService.AcceptInvoicesForPayment(ids);
        
            AddAlerts(result);

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanChangeInvoiceStatus)]
        public ActionResult RejectInvoices(RejectInvoicesDialogViewModel viewModel)
        {
            var model = new RejectInvoicesViewModel
            {
                TimeReportIds = viewModel.TimeReportIds,
                Message = viewModel.Message
            };

            var dialogViewModel = new ModelBasedDialogViewModel<RejectInvoicesViewModel>(model)
            {
                Title = TimeReportResources.RejectInvoice,
                LabelWidth = LabelWidth.Large
            };

            return PartialView("RejectInvoices", dialogViewModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanChangeInvoiceStatus)]
        public ActionResult PerformInvoicesRejection(RejectInvoicesViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return RejectInvoices(new RejectInvoicesDialogViewModel
                {
                    TimeReportIds = viewModel.TimeReportIds,
                    Message = viewModel.Message
                });
            }

            var result = _timeReportInvoiceService.RejectInvoices(viewModel.TimeReportIds, viewModel.Justification);

            AddAlerts(result);

            return DialogHelper.Reload();
        }

        private IEnumerable<FilterGroup> OthersFilteringGroup()
        {
            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.OthersFilteringGroupButtonText,
                DisplayName = TimeReportResources.ProgressFilteringGroupDisplayName,
                Type = FilterGroupType.OrCheckboxGroup,
                Filters = new List<Filter>
                {
                    new Filter { DisplayName = TimeReportResources.InProgressTimeReportDisplayName, Code = FilterCodes.InProgressReports },
                    new Filter { DisplayName = TimeReportResources.CompletedTimeReportDisplayName, Code = FilterCodes.CompletedReports }
                }
            };

            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.OthersFilteringGroupButtonText,
                DisplayName = TimeReportResources.OthersFilteringGroupDisplayName,
                Type = FilterGroupType.AndCheckboxGroup,
                Filters = new List<Filter>
                {
                    new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnitsFilter, EmployeeResources.EmployeeOrgUnitFilterName),
                    new ParametricFilter<CompanyFilterViewModel>(FilterCodes.CompanyFilter, EmployeeResources.EmployeeCompanyFilterName),
                    new ParametricFilter<TimeReportContractTypeFilterViewModel>(FilterCodes.ContractTypeFilter, EmployeeResources.EmployeeContractTypeFilterName)
                }
            };
        }

        private IEnumerable<FilterGroup> CreateWorkReportsScopeFilteringGroup()
        {
            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.ScopeFilteringGroupButtonText,
                DisplayName = TimeReportResources.ScopeFilteringGroupDisplayName,
                Filters = new List<Filter>
                {
                    new Filter
                    {
                        DisplayName = TimeReportResources.AllEmployeeTimeReportDisplayName,
                        Code = FilterCodes.AllEmployeesTimeReports
                    },
                    new Filter
                    {
                        DisplayName = TimeReportResources.MyEmployeeTimeReportDisplayName,
                        Code = FilterCodes.MyEmployees
                    },
                    new Filter
                    {
                        DisplayName = TimeReportResources.MyDirectEmployeeTimeReportDisplayName,
                        Code = FilterCodes.MyDirectEmployees
                    },
                    new Filter
                    {
                        DisplayName = TimeReportResources.MyTimeReportDisplayName,
                        Code = FilterCodes.MyTimeReports
                    },
                    new Filter
                    {
                        DisplayName = ProjectResources.MyProjects,
                        Code = FilterCodes.MyProjects
                    }
                }
            };

            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.ScopeFilteringGroupButtonText,
                DisplayName = TimeReportResources.StatusFilteringGroupDisplayName,
                Filters = CardIndexHelper.BuildEnumBasedFilters<TimeReportStatus>(),
                Type = FilterGroupType.OrCheckboxGroup
            };

            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.InvoiceFilteringGroupButtonText,
                DisplayName = TimeReportResources.InvoiceStatusFilteringGroupDisplayName,
                Filters = CardIndexHelper.BuildEnumBasedFilters<InvoiceStatus>(),
                Type = FilterGroupType.OrCheckboxGroup
            };


            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.ScopeFilteringGroupButtonText,
                DisplayName = TimeReportResources.ControllingStatusFilteringGroupDisplayName,
                Filters = CardIndexHelper.BuildEnumBasedFilters<ControllingStatus>(),
                Type = FilterGroupType.OrCheckboxGroup
            };

            yield return new FilterGroup
            {
                ButtonText = TimeReportResources.InvoiceFilteringGroupButtonText,
                DisplayName = TimeReportResources.NotificationStatusFilter,
                Filters = CardIndexHelper.BuildEnumBasedFilters<InvoiceNotificationStatus>(),
                Type = FilterGroupType.OrCheckboxGroup
            };
        }

        private IEnumerable<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            var defaultView = new GridViewConfiguration<TimeReportViewModel>(TimeReportResources.DefaultViewName, "default");
            defaultView.IncludeColumn(m => m.EmployeeAcronym);
            defaultView.IncludeColumn(m => m.EmployeeFullName);
            defaultView.IncludeColumn(m => m.Year);
            defaultView.IncludeColumn(m => m.DisplayMonth);
            defaultView.IncludeColumn(m => m.Progress);
            defaultView.IncludeColumn(m => m.Status);
            defaultView.IncludeMethodColumn(nameof(TimeReportViewModel.GetControllingStatus));
            defaultView.IncludeColumn(m => m.ModifiedOn);
            defaultView.IncludeColumn(m => m.ProjectStatuses);
            defaultView.IncludeColumn(m => m.TotalHours);
            defaultView.IncludeColumn(m => m.OvertimeHours);
            defaultView.IncludeColumn(m => m.RegularHours);
            defaultView.IncludeColumn(m => m.OrgUnitName);
            defaultView.IncludeColumn(m => m.LineManagerName);

            yield return defaultView;

            var tilesViewConfiguration = new TilesViewConfiguration<TimeReportViewModel>(TimeReportResources.TileViewName, "tiles");
            tilesViewConfiguration.IncludeAllColumns();
            tilesViewConfiguration.CustomViewName = "~/Areas/TimeTracking/Views/TimeReport/_TimeReportTile.cshtml";

            yield return tilesViewConfiguration;

            var controllingView = new GridViewConfiguration<TimeReportViewModel>(TimeReportResources.ControllingViewName, "controlling");
            controllingView.IncludeColumn(m => m.EmployeeAcronym);
            controllingView.IncludeColumn(m => m.EmployeeFullName);
            controllingView.IncludeColumn(m => m.Year);
            controllingView.IncludeColumn(m => m.DisplayMonth);
            controllingView.IncludeColumn(m => m.Progress);
            controllingView.IncludeColumn(m => m.Status);
            controllingView.IncludeMethodColumn(nameof(TimeReportViewModel.GetControllingStatus));
            controllingView.IncludeColumn(m => m.ControllingErrorMessage);
            controllingView.IncludeColumn(m => m.ModifiedOn);
            controllingView.IncludeColumn(m => m.TotalHours);
            controllingView.IncludeColumn(m => m.OvertimeHours);
            controllingView.IncludeColumn(m => m.TaxDeductionPercent);
            controllingView.IncludeColumn(m => m.RegularHours);

            yield return controllingView;

            var invoiceNotificationView = new GridViewConfiguration<TimeReportViewModel>(TimeReportResources.InvoiceNotificationViewName, "invoice-notification");
            invoiceNotificationView.IncludeColumn(m => m.EmployeeAcronym);
            invoiceNotificationView.IncludeColumn(m => m.EmployeeFullName);
            invoiceNotificationView.IncludeColumn(m => m.Year);
            invoiceNotificationView.IncludeColumn(m => m.DisplayMonth);
            invoiceNotificationView.IncludeColumn(m => m.Progress);
            invoiceNotificationView.IncludeColumn(m => m.Status);
            invoiceNotificationView.IncludeMethodColumn(nameof(TimeReportViewModel.GetInvoiceNotificationStatus));
            invoiceNotificationView.IncludeColumn(m => m.InvoiceNotificationErrorMessage);
            invoiceNotificationView.IncludeColumn(m => m.ModifiedOn);
            invoiceNotificationView.IncludeColumn(m => m.TotalHours);

            yield return invoiceNotificationView;

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewContractorInvoices))
            {
                var invoiceView = new GridViewConfiguration<TimeReportViewModel>(TimeReportResources.InvoiceViewName, "invoice");

                invoiceView.IncludeColumn(m => m.EmployeeAcronym);
                invoiceView.IncludeColumn(m => m.EmployeeFullName);
                invoiceView.IncludeColumn(m => m.Year);
                invoiceView.IncludeColumn(m => m.DisplayMonth);
                invoiceView.IncludeColumn(m => m.Status);
                invoiceView.IncludeMethodColumn(nameof(TimeReportViewModel.GetInvoiceStatus));
                invoiceView.IncludeColumn(m => m.InvoiceErrorMessage);
                invoiceView.IncludeColumn(m => m.InvoiceAmount);
                invoiceView.IncludeColumn(m => m.InvoiceDocuments);
                invoiceView.IncludeColumn(m => m.ModifiedOn);

                yield return invoiceView;
            }

            var payrollView = new GridViewConfiguration<TimeReportViewModel>(TimeReportResources.PayrollViewName, "payroll");
            payrollView.IncludeColumn(m => m.EmployeeAcronym);
            payrollView.IncludeColumn(m => m.EmployeeFullName);
            payrollView.IncludeColumn(m => m.Year);
            payrollView.IncludeColumn(m => m.DisplayMonth);
            payrollView.IncludeColumn(m => m.Progress);
            payrollView.IncludeColumn(m => m.Status);
            payrollView.IncludeMethodColumn(nameof(TimeReportViewModel.GetControllingStatus));
            payrollView.IncludeColumn(m => m.ModifiedOn);
            payrollView.IncludeColumn(m => m.ProjectStatuses);
            payrollView.IncludeColumn(m => m.TotalHours);
            payrollView.IncludeColumn(m => m.OvertimeHours);
            payrollView.IncludeColumn(m => m.RegularHours);
            payrollView.IncludeColumn(m => m.TaxDeductionPercent);
            payrollView.IncludeColumn(m => m.ContractTypeName);

            yield return payrollView;
        }

        private UriAction GetUriActionForDate(DateTime? date)
        {
            IDictionary<string, string> parameters =
                UrlFieldsHelper.GetObjectPropertyToUrlFieldMapping(typeof(TimeReportContext));

            var query = new NameValueCollection(Url.RequestContext.HttpContext.Request.QueryString);

            if (date.HasValue)
            {
                query[parameters[nameof(TimeReportContext.TimeReportYear)]] = date.Value.Year.ToString();
                query[parameters[nameof(TimeReportContext.TimeReportMonth)]] = date.Value.Month.ToString();
            }
            else
            {
                query.Remove(parameters[nameof(TimeReportContext.TimeReportYear)]);
                query.Remove(parameters[nameof(TimeReportContext.TimeReportMonth)]);
            }

            var queryParams = query.ToDictionary().SelectMany(p => new[] { p.Key, Uri.UnescapeDataString(p.Value.ToString()) }).ToArray();
            var url = Url.UriActionGet(nameof(List), KnownParameter.None, queryParams).Url;

            url = url.Replace("%2C", ",");

            return new UriAction(url);
        }

        private IEnumerable<CommandButton> GetTimeCommandButtons(DateTime currentDate, DateTime contextDate)
        {
            yield return new ToolbarButton
            {
                Text = LayoutResources.PreviousMonthShort,
                Icon = FontAwesome.CaretLeft,
                OnClickAction = GetUriActionForDate(contextDate.AddMonths(-1)),
                Group = "nextPrev"
            };

            yield return new ToolbarButton
            {
                Text = LayoutResources.CurrentMonth,
                Icon = FontAwesome.MapMarker,
                OnClickAction = GetUriActionForDate(currentDate),
                Group = "nextPrev"
            };

            yield return new ToolbarButton
            {
                Text = LayoutResources.NextMonthShort,
                Icon = FontAwesome.CaretRight,
                OnClickAction = GetUriActionForDate(contextDate.AddMonths(1)),
                Group = "nextPrev"
            };
        }

        private DateTime GetContextDate()
        {
            var contextDate =
                Context.TimeReportMonth == 0 || Context.TimeReportYear == 0 ?
                _timeService.GetCurrentDate() :
                DateHelper.BeginningOfMonth(Context.TimeReportYear, Context.TimeReportMonth);

            return contextDate;
        }
    }
}