﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewCalendarAbsence)]
    public class CalendarAbsenceController : CardIndexController<CalendarAbsenceViewModel, CalendarAbsenceDto>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ICalendarAbsenceService _absenceService;

        public CalendarAbsenceController(
            ICalendarAbsenceCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IPrincipalProvider principalProvider,
            ICalendarAbsenceService absenceService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _principalProvider = principalProvider;
            _absenceService = absenceService;
            CardIndex.Settings.Title = CalendarAbsenceResources.CalendarsAbsence;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddCalendarAbsence;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditCalendarAbsence;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteCalendarAbsence;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewCalendarAbsence;

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = CalendarAbsenceResources.FilterButtonLabel,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.MyAbsenceCalendars,
                            DisplayName = CalendarAbsenceResources.MyCalendarsLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.AllAbsenceCalendars,
                            DisplayName = CalendarAbsenceResources.AllCalendarsLabel
                        }
                    }
                }
            };
        }

        public override ActionResult Edit(long id)
        {
            VerifyRole(id, CalendarAbsenceResources.EditForbidden);

            return base.Edit(id);
        }

        public override ActionResult Edit(CalendarAbsenceViewModel viewModelRecord)
        {
            VerifyRole(viewModelRecord.Id, CalendarAbsenceResources.EditForbidden);

            return base.Edit(viewModelRecord);
        }

        public override ActionResult View(long id)
        {
            VerifyRole(id, CalendarAbsenceResources.ViewForbidden);

            return base.View(id);
        }

        private void VerifyRole(long calendarAbsenceId, string message)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllCalendarAbsence) &&
                !_absenceService.IsUserOwnerCalendarAbsence(_principalProvider.Current.Id.Value, calendarAbsenceId))
            {
                throw new BusinessException(message);
            }
        }

    }
}