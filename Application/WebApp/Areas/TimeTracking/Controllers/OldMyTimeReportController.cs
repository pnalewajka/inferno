﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Obsolete("Use: " + nameof(MyTimeReportController), true)]
    [AtomicAuthorize(SecurityRoleType.CanViewMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
    public class OldMyTimeReportController : BaseEmployeeTimeReportController
    {
        private readonly IAlertService _alertService;
        private readonly ITimeReportProjectAttributeService _attributeService;
        private readonly ITimeReportImportSourceService _importSourceService;
        private readonly ILogger _logger;

        private readonly IClassMapping<MyTimeReportDayDescriptionDto, MyTimeReportDayDescriptionViewModel>
            _myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel;

        private readonly IClassMapping<MyTimeReportDto, MyTimeReportViewModel> _myTimeReportDtoToMyTimeReportViewModel;
        private readonly IProjectService _projectService;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly IRequestServiceResolver _requestServiceResolver;

        private readonly IClassMapping<TimeReportProjectAttributeDto, MyTimeReportProjectAttributeViewModel>
            _timeReportProjectAttributeDtoToMyTimeReportProjectAttributeViewModel;

        private readonly ITimeReportService _timeReportService;
        private readonly ITimeService _timeService;

        private readonly IClassMapping<TimeTrackingProjectDto, MyTimeReportProjectViewModel>
            _timeTrackingProjectDtoToMyTimeReportProjectViewModel;

        public OldMyTimeReportController(IBaseControllerDependencies baseDependencies,
            ITimeReportService timeReportService,
            ITimeService timeService,
            IOvertimeApprovalDataService overtimeApprovalDataService,
            ITimeReportImportSourceService importSourceService,
            IRequestCardIndexDataService requestCardIndexDataService,
            IClassMappingFactory classMappingFactory,
            IRequestServiceResolver requestServiceResolver,
            IAlertService alertService,
            IProjectService projectService,
            ILogger logger,
            IClassMapping<MyTimeReportDto, MyTimeReportViewModel> myTimeReportDtoToMyTimeReportViewModel,
            IClassMapping<TimeTrackingProjectDto, MyTimeReportProjectViewModel>
                timeTrackingProjectDtooMyTimeReportProjectViewModel,
            IClassMapping<MyTimeReportDayDescriptionDto, MyTimeReportDayDescriptionViewModel>
                myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel,
            IClassMapping<TimeReportProjectAttributeDto, MyTimeReportProjectAttributeViewModel>
                timeReportProjectAttributeDtoToMyTimeReportProjectAttributeViewModel,
            ITimeReportProjectAttributeService attributeService,
            IEmploymentPeriodService employmentPeriodService)
            : base(
                baseDependencies, overtimeApprovalDataService, classMappingFactory, timeService, timeReportService,
                projectService, attributeService, alertService, employmentPeriodService)
        {
            _requestServiceResolver = requestServiceResolver;
            _alertService = alertService;
            _projectService = projectService;
            _logger = logger;
            _timeReportService = timeReportService;
            _timeService = timeService;
            _importSourceService = importSourceService;
            _requestCardIndexDataService = requestCardIndexDataService;
            _myTimeReportDtoToMyTimeReportViewModel = myTimeReportDtoToMyTimeReportViewModel;
            _timeTrackingProjectDtoToMyTimeReportProjectViewModel = timeTrackingProjectDtooMyTimeReportProjectViewModel;
            _myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel =
                myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel;
            _timeReportProjectAttributeDtoToMyTimeReportProjectAttributeViewModel =
                timeReportProjectAttributeDtoToMyTimeReportProjectAttributeViewModel;
            _attributeService = attributeService;

            Layout.Resources.AddFrom<TimeReportResources>("ConfirmDeleteProject");
            Layout.Resources.AddFrom<TimeReportResources>("ConfirmDeleteTask");
        }

        private void Submit(int year, byte month, long employeeId)
        {
            var validationAlerts = _timeReportService.ValidateFullTimeReport(employeeId, year, month);
            AddAlerts(validationAlerts);

            if (validationAlerts.ContainsErrors)
            {
                return;
            }

            var prepareAlerts =
                _timeReportService.OnSubmit(year, month, employeeId);

            AddAlerts(prepareAlerts);

            if (prepareAlerts.ContainsErrors)
            {
                return;
            }

            var timeReportDto = _timeReportService.GetMyTimeReport(employeeId, year, month);
            var requestsToCreate = new List<ProjectTimeReportApprovalRequestDto>();
            var hasAnyPendingRequest =
                timeReportDto.Rows.Any(r => r.Days.Any() && r.Status == TimeReportStatus.Submitted);

            var projectsTasks = timeReportDto.Rows
                .Where(
                    r => r.Days.Any() && r.Status != TimeReportStatus.Submitted && r.Status != TimeReportStatus.Accepted)
                .Where(r => r.Days.Sum(d => d.Hours) > 0)
                .GroupBy(r => r.ProjectId, rowDto => rowDto,
                    (projectId, rowsDto) => new { ProjectId = projectId, Tasks = rowsDto.ToList() });

            foreach (var projectTasks in projectsTasks)
            {
                requestsToCreate.Add(CreateApprovalRequestDto(employeeId, projectTasks.ProjectId, projectTasks.Tasks));
            }

            if (requestsToCreate.Count > 0 || hasAnyPendingRequest)
            {
                var result = _requestCardIndexDataService.CreateRequestsExternally(
                    RequestType.ProjectTimeReportApprovalRequest, requestsToCreate);

                if (result.IsSuccessful)
                {
                    _timeReportService.UpdateTimeReportStatus(timeReportDto.TimeReportId, TimeReportStatus.Submitted);
                }

                AddAlerts(result.Alerts);
            }
            else
            {
                _timeReportService.UpdateTimeReportStatus(timeReportDto.TimeReportId, TimeReportStatus.Accepted);
            }
        }

        [HttpGet]
        [BreadcrumbBar("MyTimeReport")]
        public ActionResult MyTimeReport(int? year, byte? month)
        {
            return GetTimeReport(year, month, CurrentPrincipal.EmployeeId);
        }

        [HttpGet]
        [BreadcrumbBar("MyTimeReport")]
        [AtomicAuthorize(SecurityRoleType.CanEditReportOnBehalf)]
        public ActionResult OnBehalf(long employeeId, int? year, byte? month)
        {
            return GetTimeReport(year, month, employeeId);
        }

        private ActionResult GetTimeReport(int? year, byte? month, long employeeId)
        {
            var defaultDate = _timeService.GetCurrentDate();

            if (year == null)
            {
                year = defaultDate.Year;
            }

            if (month == null)
            {
                month = (byte)defaultDate.Month;
            }

            if (IsTimeReportMaxDateExceeded(year.Value, month.Value))
            {
                throw new BusinessException(TimeReportResources.MaximumReportDateExceeded);
            }

            var employeeTimeReportDto = _timeReportService.GetMyTimeReport(employeeId, year.Value, month.Value);
            var viewModel = AddModelMetada(employeeTimeReportDto, nameof(MyTimeReport));

            Layout.PageTitle = GetFormattedPageHeader(viewModel);

            Layout.PageHeader = viewModel.IsReadOnly
                ? $"{Layout.PageTitle} <i class=\"fa fa-lock\" title=\"{TimeReportResources.ReportIsReadOnly}\"></i>"
                : Layout.PageTitle;

            return View("MyTimeReport", viewModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanSaveMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        [BreadcrumbBar("MyTimeReport")]
        public ActionResult SaveTimeReport(MyTimeReportViewModel myTimeReportViewModel)
        {
            CheckRoleForEditingOnBehalf(myTimeReportViewModel.EmployeeId);

            var isSaved = Save(myTimeReportViewModel);

            if (isSaved)
            {
                if (myTimeReportViewModel.IsSubmitRequest)
                {
                    Submit(myTimeReportViewModel.Year, myTimeReportViewModel.Month, myTimeReportViewModel.EmployeeId);
                }
                else
                {
                    AddAlert(AlertType.Success, TimeReportResources.MyTimeReportSavedSuccessAlert);
                }

                LogSaveActionSuccess(myTimeReportViewModel);

                return IsMyTimeReport(myTimeReportViewModel.EmployeeId)
                    ? RedirectToAction(nameof(MyTimeReport),
                        new { myTimeReportViewModel.Year, myTimeReportViewModel.Month })
                    : RedirectToAction(nameof(OnBehalf),
                        new
                        {
                            myTimeReportViewModel.Year,
                            myTimeReportViewModel.Month,
                            myTimeReportViewModel.EmployeeId
                        });
            }

            AddMessageAtTop(AlertType.Error, TimeReportResources.TimeReportWasNotSavedMessage);

            LogSaveActionFail(myTimeReportViewModel);

            var originalMyTimeReportDto = _timeReportService.GetMyTimeReport(myTimeReportViewModel.EmployeeId,
                myTimeReportViewModel.Year, myTimeReportViewModel.Month);

            var originalMyTimeReportViewModel =
                _myTimeReportDtoToMyTimeReportViewModel.CreateFromSource(originalMyTimeReportDto);

            myTimeReportViewModel.IsReadOnly = originalMyTimeReportViewModel.IsReadOnly;
            myTimeReportViewModel.Status = originalMyTimeReportViewModel.Status;
            myTimeReportViewModel.Weeks = originalMyTimeReportViewModel.Weeks;
            myTimeReportViewModel.EmployeeFullName = originalMyTimeReportViewModel.EmployeeFullName;

            foreach (var project in myTimeReportViewModel.Projects)
            {
                var originalProjectViewModel =
                    originalMyTimeReportViewModel.Projects.FirstOrDefault(p => p.ProjectId == project.ProjectId);

                if (originalProjectViewModel != null)
                {
                    project.Attributes = originalProjectViewModel.Attributes;
                    project.IsReadOnly = originalProjectViewModel.IsReadOnly;
                }
                else
                {
                    project.Attributes = GetProjectAttributes(project.ProjectId);
                }

                foreach (var task in project.Tasks)
                {
                    var originalTaskViewModel = originalProjectViewModel?.Tasks.FirstOrDefault(t => t.Id == task.Id);

                    if (originalTaskViewModel != null)
                    {
                        task.IsReadOnly = originalTaskViewModel.IsReadOnly;
                        task.Status = originalTaskViewModel.Status;
                    }
                }
            }

            ViewBag.InitializeAsDirty = true;

            return View("MyTimeReport", myTimeReportViewModel);
        }

        private void LogSaveActionSuccess(MyTimeReportViewModel myTimeReportViewModel)
        {
            _logger.Warn($"TimeReportSaveSuccess: ({GetActionLogMessage(myTimeReportViewModel)})");
        }

        private void LogSaveActionFail(MyTimeReportViewModel myTimeReportViewModel)
        {
            _logger.Warn($"TimeReportSaveFail: ({GetActionLogMessage(myTimeReportViewModel)})");
        }

        private string GetActionLogMessage(MyTimeReportViewModel myTimeReportViewModel)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendFormat($"Employee: {myTimeReportViewModel.EmployeeId};");
                stringBuilder.AppendFormat($"User: {CurrentPrincipal.Login};");
                stringBuilder.AppendFormat($"Year: {myTimeReportViewModel.Year};");
                stringBuilder.AppendFormat($"Month: {myTimeReportViewModel.Month};");

                foreach (var project in myTimeReportViewModel.Projects)
                {
                    stringBuilder.AppendFormat(
                        $"Project: {project.ProjectId}, Hours: {project.Tasks.Select(t => t.Days.Sum(d => d.Hours)).Sum()};");
                }

                return stringBuilder.ToString();
            }
            catch
            {
                return $"Can't serialize {nameof(MyTimeReportViewModel)}";
            }
        }

        private bool Save(MyTimeReportViewModel myTimeReportViewModel)
        {
            var originalTimeReportDto = _timeReportService.GetMyTimeReport(myTimeReportViewModel.EmployeeId,
                myTimeReportViewModel.Year, myTimeReportViewModel.Month);

            var myTimeReportViewModelToTimeReportDtoMapping =
                ClassMappingFactory.CreateMapping<MyTimeReportViewModel, MyTimeReportDto>();

            var employeeTimeReportDto =
                myTimeReportViewModelToTimeReportDtoMapping.CreateFromSource(myTimeReportViewModel);

            var violationsResults = _timeReportService.CheckOnSaveViolations(employeeTimeReportDto,
                originalTimeReportDto);

            if (!ModelState.IsValid || !violationsResults.IsSuccessful)
            {
                if (violationsResults.Alerts.Any())
                {
                    AddAlerts(violationsResults.Alerts);
                }

                ModelState.Values.ForEach(v => v.Errors.ForEach(e => AddAlert(AlertType.Error, e.ErrorMessage)));

                myTimeReportViewModel = AddModelMetada(myTimeReportViewModel, nameof(MyTimeReport));

                var timeReportDtoToMyTimeReportViewModelMapping =
                    ClassMappingFactory.CreateMapping<MyTimeReportDto, MyTimeReportViewModel>();

                var originalTimeReportViewModel =
                    timeReportDtoToMyTimeReportViewModelMapping.CreateFromSource(originalTimeReportDto);

                myTimeReportViewModel.Days = originalTimeReportViewModel.Days;

                Layout.PageHeader = Layout.PageTitle = GetFormattedPageHeader(myTimeReportViewModel);

                return false;
            }

            _timeReportService.UpdateFullTimeReport(employeeTimeReportDto);

            return true;
        }

        [AtomicAuthorize(SecurityRoleType.CanImportMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult ImportFromSource(long employeeId, long inputSourceId, int year, byte month)
        {
            CheckRoleForEditingOnBehalf(employeeId);

            var timeReport = _importSourceService.GetTimeReport(inputSourceId, employeeId, year, month);
            var importResult = _timeReportService.UpdateTimeReportFromImportSource(timeReport);

            AddAlerts(importResult.Alerts);

            return IsMyTimeReport(employeeId)
                ? RedirectToAction(nameof(MyTimeReport),
                    new { Year = year, Month = month })
                : RedirectToAction(nameof(OnBehalf),
                    new { Year = year, Month = month, EmployeeId = employeeId });
        }

        [HttpPost]
        public PartialViewResult GetDefaultProjectTemplate(int employeeId, long projectId, int year, byte month)
        {
            var myTimeReportProjectViewModel = GetMyTimeReportProjectViewModel(projectId);
            myTimeReportProjectViewModel.Tasks.Add(new MyTimeReportTaskViewModel());

            var myTimeReportDayDescriptions = GetMyTimeReportDayDescriptions(employeeId, year, month);

            var projectRowViewModel = new ProjectRowViewModel
            {
                Project = myTimeReportProjectViewModel,
                Days = myTimeReportDayDescriptions
            };

            return PartialView("_Project", projectRowViewModel);
        }

        [HttpPost]
        public PartialViewResult GetDefaultTaskByProjectId(int employeeId, long projectId, int year, byte month)
        {
            var myTimeReportProjectViewModel = GetMyTimeReportProjectViewModel(projectId);
            myTimeReportProjectViewModel.Tasks.Add(new MyTimeReportTaskViewModel());

            var myTimeReportDayDescriptions = GetMyTimeReportDayDescriptions(employeeId, year, month);

            var taskRowViewModel = new TaskRowViewModel
            {
                ParentProject = myTimeReportProjectViewModel,
                Days = myTimeReportDayDescriptions,
                Task = new MyTimeReportTaskViewModel()
            };

            return PartialView("_Task", taskRowViewModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanSubmitMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult SubmitConfirmDialog(long employeeId, int year, byte month)
        {
            CheckRoleForEditingOnBehalf(employeeId);

            var warnings = _timeReportService.GetUserTimeReportSubmitWarnings(employeeId, year, month).ToList();

            var dialogModel = new ModelBasedDialogViewModel<TimeReportSubmitViewModel>(
                new TimeReportSubmitViewModel(warnings))
            {
                Title = TimeReportResources.SubmitConfirmTitle
            };

            return PartialView(dialogModel);
        }

        [AtomicAuthorize(SecurityRoleType.CanSubmitMyTimeReport, SecurityRoleType.CanEditReportOnBehalf)]
        [HttpPost]
        public ActionResult AddProjectsFromPreviousMonth(MyTimeReportViewModel myTimeReportViewModel)
        {
            var year = myTimeReportViewModel.Year;
            var month = myTimeReportViewModel.Month;
            var employeeId = myTimeReportViewModel.EmployeeId;

            CheckRoleForEditingOnBehalf(employeeId);

            _timeReportService.AddEmployeeProjectsFromPreviousMonth(employeeId, year, month);

            return RedirectToAction(
                IsMyTimeReport(employeeId)
                    ? nameof(MyTimeReport)
                    : nameof(OnBehalf),
                new { Year = year, Month = month, EmployeeId = employeeId });
        }

        protected override IEnumerable<CommandButton> TimesheetImportCommandButtons(MyTimeReportViewModel viewModel,
            string actionName)
        {
            var isMyTimeReport = IsMyTimeReport(viewModel.EmployeeId);

            if (!viewModel.IsReadOnly)
            {
                yield return new ToolbarButton
                {
                    Text = TimeReportResources.SaveButtonText,
                    OnClickAction = new JavaScriptCallAction("MyTimeReport.submitForm"),
                    RequiredRole = isMyTimeReport
                        ? SecurityRoleType.CanSaveMyTimeReport
                        : SecurityRoleType.CanEditReportOnBehalf,
                    IsDefault = true,
                    Group = "save"
                };

                yield return new ToolbarButton
                {
                    Text = TimeReportResources.SubmitButtonText,
                    OnClickAction = new JavaScriptCallAction("MyTimeReport.submitRequest"),
                    RequiredRole = isMyTimeReport
                        ? SecurityRoleType.CanSubmitMyTimeReport
                        : SecurityRoleType.CanEditReportOnBehalf,
                    Group = "submit"
                };

                yield return new ToolbarButton
                {
                    Text = TimeReportResources.ImportButtonText,
                    OnClickAction = new DropdownAction
                    {
                        Items = GetImportActions(viewModel).OrderBy(action => action.Text).ToList()
                    },
                    Group = "import"
                };
            }

            yield return new ToolbarButton
            {
                Text = TimeReportResources.RequestButtonText,
                OnClickAction = new DropdownAction
                {
                    Items = GetRequestActions(viewModel).OrderBy(a => a.Text).ToList()
                },
                RequiredRole = SecurityRoleType.CanAddRequest,
                Group = "request"
            };

            foreach (
                var timeButton in
                    TimeCommandButtons(viewModel, isMyTimeReport ? nameof(MyTimeReport) : nameof(OnBehalf)))
            {
                yield return timeButton;
            }
        }

        protected override IEnumerable<TimeTrackingImportSourceViewModel> GetImportSources()
        {
            var importSourceDtos = _importSourceService.GetImportSources();
            var classMapping =
                ClassMappingFactory.CreateMapping<TimeTrackingImportSourceDto, TimeTrackingImportSourceViewModel>();

            return importSourceDtos.Select(classMapping.CreateFromSource).ToList();
        }

        private IEnumerable<ICommandButton> GetImportActions(MyTimeReportViewModel viewModel)
        {
            var isMyTimeReport = IsMyTimeReport(viewModel.EmployeeId);

            if (viewModel.ImportSources.Any())
            {
                var requiredRole = isMyTimeReport
                    ? SecurityRoleType.CanImportMyTimeReport
                    : SecurityRoleType.CanEditReportOnBehalf;

                foreach (var importSource in viewModel.ImportSources)
                {
                    yield return new CommandButton
                    {
                        Text = importSource.Name.ToString(),
                        RequiredRole = requiredRole,
                        OnClickAction = Url.UriActionPost(
                            nameof(ImportFromSource),
                            KnownParameter.None,
                            "inputSourceId",
                            importSource.Id.ToString(),
                            nameof(MyTimeReportViewModel.EmployeeId),
                            viewModel.EmployeeId.ToString(),
                            nameof(MyTimeReportViewModel.Year),
                            viewModel.Year.ToString(),
                            nameof(MyTimeReportViewModel.Month),
                            viewModel.Month.ToString())
                    };
                }
            }

            yield return new CommandButton
            {
                Text = TimeReportResources.AddLastMonthProjects,
                OnClickAction = Url.UriActionPost(
                    nameof(AddProjectsFromPreviousMonth),
                    KnownParameter.None,
                    nameof(MyTimeReportViewModel.EmployeeId),
                    viewModel.EmployeeId.ToString(),
                    nameof(MyTimeReportViewModel.Year),
                    viewModel.Year.ToString(),
                    nameof(MyTimeReportViewModel.Month),
                    viewModel.Month.ToString()),
                RequiredRole = isMyTimeReport
                    ? SecurityRoleType.CanSaveMyTimeReport
                    : SecurityRoleType.CanEditReportOnBehalf
            };
        }

        private IEnumerable<ICommandButton> GetRequestActions(MyTimeReportViewModel viewModel)
        {
            yield return new CommandButton
            {
                Text = TimeReportResources.AbsenceRequestButtonText,
                OnClickAction = new UriAction(GetAddRequestUrl(RequestType.AbsenceRequest, viewModel)),
                RequiredRole = _requestServiceResolver.GetByServiceType(RequestType.AbsenceRequest).RequiredRoleType
            };

            yield return new CommandButton
            {
                Text = TimeReportResources.OvertimeRequestButtonText,
                OnClickAction = new UriAction(GetAddRequestUrl(RequestType.OvertimeRequest, viewModel)),
                RequiredRole = _requestServiceResolver.GetByServiceType(RequestType.OvertimeRequest).RequiredRoleType
            };

            yield return new CommandButton
            {
                Text = TimeReportResources.HomeOfficeRequestButtonText,
                OnClickAction = new UriAction(GetAddRequestUrl(RequestType.AddHomeOfficeRequest, viewModel)),
                RequiredRole =
                    _requestServiceResolver.GetByServiceType(RequestType.AddHomeOfficeRequest).RequiredRoleType
            };

            var canRequestReopen = IsMyTimeReport(viewModel.EmployeeId) && viewModel.CanBeReopened;

            if (canRequestReopen)
            {
                yield return new CommandButton
                {
                    Text = TimeReportResources.ReopenTimeTrackinReportRequestButtonText,
                    OnClickAction =
                        new UriAction(GetAddRequestUrl(RequestType.ReopenTimeTrackingReportRequest, viewModel)),
                    RequiredRole =
                        _requestServiceResolver.GetByServiceType(RequestType.ReopenTimeTrackingReportRequest)
                            .RequiredRoleType
                };
            }
        }

        private string GetAddRequestUrl(RequestType requestType, MyTimeReportViewModel viewModel)
        {
            var requestTypeUrlValue = NamingConventionHelper.ConvertPascalCaseToHyphenated(requestType.ToString());
            var url = Url.Action("Add", "Request", new RouteValueDictionary
            {
                {"area", "Workflows"},
                {RequestControllerHelper.RequestTypeUrlParameterName, requestTypeUrlValue},
                {"request-object-view-model.employeeId", viewModel.EmployeeId},
                {"request-object-view-model.year", viewModel.Year},
                {"request-object-view-model.month", viewModel.Month}
            });

            return url;
        }

        private ProjectTimeReportApprovalRequestDto CreateApprovalRequestDto(long employeeId, long projectId,
            IList<MyTimeReportRowDto> tasks)
        {
            var daysWithHours = tasks.SelectMany(t => t.Days).Where(d => d.Hours != 0).ToList();

            var rowToTaskMapping = ClassMappingFactory.CreateMapping<MyTimeReportRowDto, TimeReportTaskApprovalDto>();

            return new ProjectTimeReportApprovalRequestDto
            {
                From = daysWithHours.Min(d => d.Day),
                To = daysWithHours.Max(d => d.Day),
                EmployeeId = employeeId,
                ProjectId = projectId,
                Tasks = tasks.Select(t => rowToTaskMapping.CreateFromSource(t)).ToList()
            };
        }

        public MyTimeReportProjectViewModel GetMyTimeReportProjectViewModel(long projectId)
        {
            var timeTrackingProjectDto = _timeReportService.GetTimeTimeTrackingProjectDtoById(projectId);
            var myTimeReportProjectViewModel =
                _timeTrackingProjectDtoToMyTimeReportProjectViewModel.CreateFromSource(timeTrackingProjectDto);

            return myTimeReportProjectViewModel;
        }

        private IList<MyTimeReportDayDescriptionViewModel> GetMyTimeReportDayDescriptions(int employeeId, int year,
            byte month)
        {
            return TimeReportService.CalculateDayDescriptions(year, month, employeeId)
                .Select(_myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel.CreateFromSource)
                .ToList();
        }
    }
}