﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTimeReportProjectAttributeValue)]
    public class TimeReportProjectAttributeValueController : SubCardIndexController<TimeReportProjectAttributeValueViewModel, TimeReportProjectAttributeValueDto, TimeReportProjectAttributeController>
    {
        public TimeReportProjectAttributeValueController(
            ITimeReportProjectAttributeValueCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = TimeReportProjectAttributeResources.ValueCardIndexTitle;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddTimeReportProjectAttributeValue;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditTimeReportProjectAttributeValue;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteTimeReportProjectAttributeValue;
        }
    }
}