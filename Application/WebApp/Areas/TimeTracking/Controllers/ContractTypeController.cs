﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewContractType)]
    public class ContractTypeController : CardIndexController<ContractTypeViewModel, ContractTypeDto, ContractTypeContext>
    {
        public ContractTypeController(IContractTypeCardIndexDataService cardIndexDataService, IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = ContractTypeResource.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddContractType;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditContractType;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteContractType;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportContractType);
            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations();
        }

        private IList<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            var defaultView =
                new GridViewConfiguration<ContractTypeViewModel>(ContractTypeResource.DefaultView, "default");
            defaultView.IncludeColumns(new List<Expression<Func<ContractTypeViewModel, object>>>
            {
                x => x.Name,
                x => x.Description,
                x => x.EmploymentType,
                x => x.IsActive,
            });
            defaultView.IsDefault = true;

            var extendedView =
                new GridViewConfiguration<ContractTypeViewModel>(ContractTypeResource.ExtendedView, "extended");
            extendedView.IncludeAllColumns();

            var advancedView =
                new GridViewConfiguration<ContractTypeViewModel>(ContractTypeResource.AdvancedView, "advanced");
            advancedView.IncludeColumns(new List<Expression<Func<ContractTypeViewModel, object>>>
            {
                x => x.Name,
                x => x.Description,
                x => x.EmploymentType,
                x => x.IsActive,
                x => x.ActiveDirectoryCode,
            });

            return new IGridViewConfiguration[]
            {
                defaultView,
                extendedView,
                advancedView,
            };
        }
    }
}
