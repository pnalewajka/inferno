﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewMyVacationBalance)]
    public class MyVacationBalanceController : ReadOnlyCardIndexController<MyVacationBalanceViewModel, VacationBalanceDto, ParentIdContext>
    {
        private const string RequestTypeUrlParameterName = "request-type";

        private readonly IMyVacationBalanceCardIndexDataService _myVacationBalanceService;

        public MyVacationBalanceController(
            IMyVacationBalanceCardIndexDataService myVacationBalanceService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(myVacationBalanceService, baseControllerDependencies)
        {
            _myVacationBalanceService = myVacationBalanceService;

            CardIndex.Settings.Title = MyVacationBalanceResources.MyVacationBalanceTitle;
            CreateAbsenceRequestButton();
            InitializeFilters();
        }

        private void CreateAbsenceRequestButton()
        {
            var requestTypeUrlValue = NamingConventionHelper.ConvertPascalCaseToHyphenated(RequestType.AbsenceRequest.ToString());
            var button = new ToolbarButton()
            {
                Text = AbsenceRequestResources.RequestAbsence,
                RequiredRole = SecurityRoleType.CanAddRequest,
                Icon = FontAwesome.PlusCircle,
                IsDefault = false,
                OnClickAction = Url.UriActionGet("Add", "Request", KnownParameter.None, "area", "Workflows", RequestTypeUrlParameterName, requestTypeUrlValue)
            };

            CardIndex.Settings.ToolbarButtons.Add(button);
        }

        private void InitializeFilters()
        {
            var yearFilters = _myVacationBalanceService.GetFilterYears()
                                                     .Select(CreateFilter);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    ButtonText = MyVacationBalanceResources.YearButtonText,
                    Filters = yearFilters.ToList(),
                    Type = FilterGroupType.RadioGroup
                }
            };
        }

        private Filter CreateFilter(FilterYearDto filterYear)
        {
            return new Filter
            {
                Code = filterYear.GetFilterCode(),
                DisplayName = filterYear.Year.ToInvariantString()
            };
        }
    }
}