﻿using System.Web.Mvc;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewAbsenceType)]
    public class AbsenceTypeController : CardIndexController<AbsenceTypeViewModel, AbsenceTypeDto>
    {
        private readonly IAbsenceService _absenceService;

        public AbsenceTypeController(
            IAbsenceService absenceService,
            IAbsenceTypeCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies dependencies)
            : base(cardIndexDataService, dependencies)
        {
            _absenceService = absenceService;

            CardIndex.Settings.Title = AbsenceTypeResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddAbsenceType;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditAbsenceType;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteAbsenceType;
        }
    }
}