using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewOthersTimeReport)]
    public class EmployeeTimeReportController : BaseEmployeeTimeReportController
    {
        private readonly ITimeReportService _timeReportService;
        private readonly ITimeService _timeService;

        public EmployeeTimeReportController(IBaseControllerDependencies baseDependencies,
            ITimeReportService timeReportService,
            IOvertimeApprovalDataService overtimeApprovalDataService,
            IClassMappingFactory classMappingFactory,
            ITimeService timeService,
            ITimeReportProjectAttributeService attributeService,
            IProjectService projectService,
            IAlertService alertService,
            IEmploymentPeriodService employmentPeriodService)
            : base(baseDependencies, overtimeApprovalDataService, classMappingFactory, timeService, timeReportService, projectService, attributeService, alertService, employmentPeriodService)
        {
            _timeReportService = timeReportService;
            _timeService = timeService;
        }

        [HttpGet]
        [BreadcrumbBar("TimeTracking/EmployeeTimeReport")]
        public ActionResult TimeReport(long id, int? year, byte? month)
        {
            Layout.PageHeaderViewName = "~/Areas/TimeTracking/Views/MyTimeReport/CustomHeader.cshtml";

            ViewBag.serviceUrls = new
            {
                LoadReport = Url.Action("Report", new { id, year, month }),
                ReopenTimeReport = Url.Action("ReopenTimeReport", "TimeReport")
            };

            return View("~/Areas/TimeTracking/Views/MyTimeReport/Index.cshtml");
        }

        public ActionResult Report(int? year, byte? month, long id)
        {
            var defaultDate = _timeService.GetCurrentDate();

            if (year == null)
            {
                year = defaultDate.Year;
            }

            if (month == null)
            {
                month = (byte)defaultDate.Month;
            }

            if (IsTimeReportMaxDateExceeded(year.Value, month.Value))
            {
                throw new BusinessException(TimeReportResources.MaximumReportDateExceeded);
            }

            var currentUserIsSupervisor = CurrentPrincipal.IsInRole(SecurityRoleType.CanSuperviseTimeReport);
            var employeeTimeReportDto = _timeReportService.GetMyTimeReport(id, year.Value, month.Value);
            var viewModel = ToViewModel(employeeTimeReportDto);

            SetReadOnlyReport(viewModel);

            viewModel.IsClockCardRequired = EmploymentPeriodService.IsClockCardRequiredInGivenMonth(viewModel.EmployeeId, year.Value, month.Value);
            viewModel.IsMyTimeReport = IsMyTimeReport(viewModel.EmployeeId);
            viewModel.CanBeReopened = TimeReportService.CanBeReopened(viewModel.EmployeeId, viewModel.Year, viewModel.Month);

            var toolbarOptions = new
            {
                CanSave = false,
                CanSubmit = false,
                CanImport = false,
                CanReopenTimeReport = viewModel.CanBeReopened && currentUserIsSupervisor,
                CanSeePreviousMonth = _timeReportService.IsPreviousMonthAvailable(id, viewModel.Year, viewModel.Month),
                CanSeeNextMonth = !IsTimeReportMaxDateExceeded(new DateTime(viewModel.Year, viewModel.Month, 1).AddMonths(1)),
            };

            return Ok(new
            {
                viewModel,
                toolbarOptions,
                title = GetFormattedPageHeader(viewModel.Year, viewModel.Month, viewModel.Status, viewModel.EmployeeFullName)
            });
        }

        [HttpGet]
        [BreadcrumbBar("TimeTracking/EmployeeTimeReport")]
        public ActionResult ReadOnlyTimeReport(long employeeId, int? year, byte? month)
        {
            return TimeReport(employeeId, year, month);
        }

        protected override IEnumerable<TimeTrackingImportSourceViewModel> GetImportSources()
        {
            yield break;
        }

        protected override IEnumerable<CommandButton> TimesheetImportCommandButtons(MyTimeReportViewModel viewModel,
            string actionName)
        {
            foreach (var timeButton in TimeCommandButtons(viewModel, actionName))
            {
                yield return timeButton;
            }

            foreach (var timeButton in TimeReportSupervisorButtons(viewModel))
            {
                yield return timeButton;
            }
        }
    }
}