﻿using System.Collections.Generic;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Business.Allocation.Consts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewOvertimeBank)]
    public class OvertimeBankController : CardIndexController<OvertimeBankViewModel, OvertimeBankDto>
    {
        public OvertimeBankController(IOvertimeBankCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies dependencies, ITimeService timeService)
            : base(cardIndexDataService, dependencies)
        {
            CardIndex.Settings.Title = OvertimeBankResources.OvertimeBankTitle;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewOvertimeBank;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddOvertimeBank;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditOvertimeBank;
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "!row.isAutomatic"
            };

            CardIndex.Settings.FilterGroups.AddRange(GetScopeFilterGroup(OvertimeBankResources.ScopeFilterTitle));
            CardIndex.Settings.DefaultNewRecord =
                () => new OvertimeBankViewModel {EffectiveOn = timeService.GetCurrentDate()};

            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteOvertimeBank;
            CardIndex.Settings.DeleteButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "!row.isAutomatic"
            };

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportOvertimeBank);
        }

        private IEnumerable<FilterGroup> GetScopeFilterGroup(string groupName)
        {
            yield return new FilterGroup
            {
                ButtonText = groupName,
                DisplayName = OvertimeBankResources.EmployeeFilterTitle,
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.ValuePickerAndGrid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.MyEmployees, DisplayName = OvertimeBankResources.MyEmployees},
                    new Filter {Code = FilterCodes.AllEmployees, DisplayName = OvertimeBankResources.AllEmployees}
                }
            };
        }
    }
}
