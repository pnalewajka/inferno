﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewVacationBalance)]
    public class VacationBalanceController : CardIndexController<VacationBalanceViewModel, VacationBalanceDto>
    {
        private readonly ITimeService _timeService;
        private readonly IVacationBalanceCardIndexDataService _vacationBalanceService;

        public VacationBalanceController(IVacationBalanceCardIndexDataService vacationBalanceService, IBaseControllerDependencies baseControllerDependencies, ITimeService timeService)
            : base(vacationBalanceService, baseControllerDependencies)
        {
            _timeService = timeService;
            _vacationBalanceService = vacationBalanceService;
            
            CardIndex.Settings.Title = VacationBalanceResources.VacationBalanceTitle;
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportVacations);
            CardIndex.Settings.AddImport<VacationBalanceImportViewModel, VacationBalanceDto>(vacationBalanceService
                                                                                            , VacationBalanceResources.ImportButtonText
                                                                                            , SecurityRoleType.CanAddVacationBalance
                                                                                            , ImportBehavior.InsertOrUpdate);

            CardIndex.Settings.AddButton.OnClickAction = new JavaScriptCallAction($"VacationBalance.addNewVacationBalance('{Url.Action("Add")}', this.grid)");

            CardIndex.Settings.DeleteButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = "row.canBeDeleted"
            };

            Layout.Scripts.Add("~/Areas/TimeTracking/Scripts/VacationBalance.js");

            SetPrivileges();
            InitializeFilters();
        }

        public override ActionResult List(GridParameters parameters)
        {
            AdjustFilterYearButtonName(parameters);

            return base.List(parameters);
        }

        public override ActionResult Add()
        {
            if (Request.QueryString["id"] != null)
            {
                Func<VacationBalanceViewModel> defaultRecord = () =>
                {
                    var record = GetDefaultNewRecord();
                    record.EmployeeId = long.Parse(Request.QueryString["id"]);
                    return record;
                };

                CardIndex.Settings.DefaultNewRecord = defaultRecord;
            }

            return base.Add();
        }

        private void SetPrivileges()
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddVacationBalance;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditVacationBalance;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteVacationBalance;
        }

        private VacationBalanceViewModel GetDefaultNewRecord()
        {
            return new VacationBalanceViewModel
            {
                EffectiveOn = _timeService.GetCurrentTime()
            };
        }

        private void AdjustFilterYearButtonName(GridParameters parameters)
        {
            var year = FilterYearDto.TryGetYear(parameters.Filters);

            if (year.HasValue && CardIndex.Settings.FilterGroups.Any())
            {
                CardIndex.Settings.FilterGroups.First().ButtonText = string.Format(VacationBalanceResources.YearButtonWithYearText, year);
            }
        }

        private void InitializeFilters()
        {
            var yearFilters = _vacationBalanceService.GetFilterYears()
                                                     .Select(CreateFilter);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    ButtonText = VacationBalanceResources.YearButtonText,
                    Filters = yearFilters.ToList(),
                    Type = FilterGroupType.RadioGroup
                },
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.AllVacations, DisplayName = VacationBalanceResources.AllVacations },
                        new Filter { Code = FilterCodes.MyEmployeeVacations, DisplayName = VacationBalanceResources.MyEmployeeVacations },
                        new Filter { Code = FilterCodes.MyDirectEmployeeVacations, DisplayName = VacationBalanceResources.MyDirectEmployeeVacations },
                        new Filter { Code = FilterCodes.MyVacations, DisplayName = VacationBalanceResources.MyVacations }
                    }
                }
            };
        }

        private Filter CreateFilter(FilterYearDto filterYear)
        {
            return new Filter
            {
                Code = filterYear.GetFilterCode(),
                DisplayName = filterYear.Year.ToInvariantString()
            };
        }
    }
}