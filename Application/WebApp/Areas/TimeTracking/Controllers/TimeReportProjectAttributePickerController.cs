﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Controllers
{
    [Identifier("Pickers.TimeTracking.TimeReportProjectAttribute")]
    [AtomicAuthorize(SecurityRoleType.CanViewTimeReportProjectAttributePicker)]
    public class TimeReportProjectAttributePickerController
        : TimeReportProjectAttributeController
    {
        public TimeReportProjectAttributePickerController(ITimeReportProjectAttributeCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}