﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Helpers
{
    public static class TimeReportStatusHelper
    {
        public static string GetIconForStatus(TimeReportStatus status)
        {
            switch (status)
            {
                case TimeReportStatus.Accepted:
                    return "fa-thumbs-o-up";
                case TimeReportStatus.Rejected:
                    return "fa-thumbs-o-down";
                case TimeReportStatus.NotStarted:
                    return "fa-hourglass-start";
                case TimeReportStatus.Draft:
                    return "fa-hourglass-half";
                case TimeReportStatus.Submitted:
                    return "fa-hourglass-end";
                default:
                    return "question-circle";
            }
        }
    }
}