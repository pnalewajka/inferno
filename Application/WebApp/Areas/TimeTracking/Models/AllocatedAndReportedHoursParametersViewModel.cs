﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Formatters;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("ViewModel.AllocatedAndReportedHoursParametersViewModel")]
    public class AllocatedAndReportedHoursParametersViewModel
    {
        [Order(1)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobProfileLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public long[] ProfileIds { get; set; }

        [Order(2)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long[] EmployeeOrgUnitIds { get; set; }

        [Order(3)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.LocationText), typeof(ReportResources))]
        public long[] LocationIds { get; set; }

        [Order(4)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManager), nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ManagerPickerController), OnResolveUrlJavaScript = "url=url + '&role=" + nameof(SecurityRoleType.CanBeAssignedAsLineManager) + "'")]
        public long[] LineManagerIds { get; set; }

        [Order(5)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLabel), nameof(EmployeeResources.EmployeeJobMatrixShortLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController), GridFormatterType = typeof(JobMatrixLevelPickerGridFormatter))]
        public long[] JobMatrixIds { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeePlaceOfWork), nameof(EmployeeResources.EmployeePlaceOfWorkShort), typeof(EmployeeResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<PlaceOfWork>))]
        public PlaceOfWork? PlaceOfWork { get; set; }

        [Order(7)]
        [DisplayNameLocalized(nameof(ProjectResources.UtilizationCategoryLabel), typeof(ProjectResources))]
        [ValuePicker(Type = typeof(UtilizationCategoryPickerController))]
        public long[] UtilizationCategoryIds { get; set; }

        [Render(Size = Size.Medium)]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<BillabilityFiltering>), EmptyItemBehavior = EmptyItemBehavior.NotAvailable)]
        [DisplayNameLocalized(nameof(EmployeeResources.Billability), typeof(EmployeeResources))]
        public BillabilityFiltering BillabilityFiltering { get; set; }
    }    
}
