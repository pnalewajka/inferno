﻿using Smt.Atomic.Presentation.Renderers.CardIndex;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class DisplayProjectStatus : ICellContent
    {
        public string CssClass { get; set; }

        public ProjectStatusViewModel[] Statuses { get; set; }

        public override string ToString()
        {
            return string.Join(", ", Statuses.Select(s => $"{s.Name} ({s.Status})"));
        }
    }
}