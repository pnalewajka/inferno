﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class HourlyRateViewModelToHourlyRateDtoMapping : ClassMapping<HourlyRateViewModel, HourlyRateDto>
    {
        public HourlyRateViewModelToHourlyRateDtoMapping()
        {
            Mapping = v => new HourlyRateDto
            {
                Id = v.Id,
                Description = v.Description
            };
        }
    }
}
