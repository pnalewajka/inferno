﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectAttributeDtoToTimeReportProjectAttributeViewModelMapping : ClassMapping<TimeReportProjectAttributeDto, TimeReportProjectAttributeViewModel>
    {
        public TimeReportProjectAttributeDtoToTimeReportProjectAttributeViewModelMapping()
        {
            Mapping = d => new TimeReportProjectAttributeViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                DefaultValueId = d.DefaultValueId,
            };
        }
    }
}
