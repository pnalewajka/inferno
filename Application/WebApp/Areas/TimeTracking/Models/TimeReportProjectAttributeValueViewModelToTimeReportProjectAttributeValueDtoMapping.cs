﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectAttributeValueViewModelToTimeReportProjectAttributeValueDtoMapping : ClassMapping<TimeReportProjectAttributeValueViewModel, TimeReportProjectAttributeValueDto>
    {
        public TimeReportProjectAttributeValueViewModelToTimeReportProjectAttributeValueDtoMapping()
        {
            Mapping = v => new TimeReportProjectAttributeValueDto
            {
                Id = v.Id,
                AttributeId = v.AttributeId,
                Name = v.Name,
                Description = v.Description,
                Features = v.Features,
            };
        }
    }
}
