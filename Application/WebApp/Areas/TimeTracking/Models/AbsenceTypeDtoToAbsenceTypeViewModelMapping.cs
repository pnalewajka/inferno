﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceTypeDtoToAbsenceTypeViewModelMapping : ClassMapping<AbsenceTypeDto, AbsenceTypeViewModel>
    {
        public AbsenceTypeDtoToAbsenceTypeViewModelMapping()
        {
            Mapping = d => new AbsenceTypeViewModel
            {
                Id = d.Id,
                Code = d.Code,
                Name = d.Name,
                Description = d.Description,
                EmployeeInstruction = d.EmployeeInstruction,
                KbrCode = d.KbrCode,
                IsVacation = d.IsVacation,
                IsTimeOffForOvertime = d.IsTimeOffForOvertime,
                RequiresHRApproval = d.RequiresHRApproval,
                Timestamp = d.Timestamp,
                Order = d.Order,
                AllowHourlyReporting = d.AllowHourlyReporting,
                AbsenceProjectId = d.AbsenceProjectId,
            };
        }
    }
}
