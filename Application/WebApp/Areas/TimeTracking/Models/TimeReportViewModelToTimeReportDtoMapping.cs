﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportViewModelToTimeReportDtoMapping : ClassMapping<TimeReportViewModel, TimeReportDto>
    {
        public TimeReportViewModelToTimeReportDtoMapping()
        {
            Mapping = v => new TimeReportDto
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                Year = v.Year,
                Month = v.Month,
                Status = v.Status,
                ControllingStatus = v.ControllingStatus,
                ControllingErrorMessage = v.ControllingErrorMessage,
                InvoiceNotificationStatus = v.InvoiceNotificationStatus,
                InvoiceNotificationErrorMessage = v.InvoiceNotificationErrorMessage,
                InvoiceStatus = v.InvoiceStatus,
                InvoiceIssues = v.InvoiceIssues,
                InvoiceRejectionReason = v.InvoiceRejectionReason,
                Timestamp = v.Timestamp
            };
        }
    }
}
