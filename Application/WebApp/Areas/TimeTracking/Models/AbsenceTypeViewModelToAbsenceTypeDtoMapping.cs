﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceTypeViewModelToAbsenceTypeDtoMapping : ClassMapping<AbsenceTypeViewModel, AbsenceTypeDto>
    {
        public AbsenceTypeViewModelToAbsenceTypeDtoMapping()
        {
            Mapping = v => new AbsenceTypeDto
            {
                Id = v.Id,
                Code = v.Code,
                Name = v.Name,
                Description = v.Description,
                EmployeeInstruction = v.EmployeeInstruction,
                KbrCode = v.KbrCode,
                IsVacation = v.IsVacation,
                IsTimeOffForOvertime = v.IsTimeOffForOvertime,
                RequiresHRApproval = v.RequiresHRApproval,
                Timestamp = v.Timestamp,
                Order = v.Order,
                AllowHourlyReporting = v.AllowHourlyReporting,
                AbsenceProjectId = v.AbsenceProjectId,
            };
        }
    }
}
