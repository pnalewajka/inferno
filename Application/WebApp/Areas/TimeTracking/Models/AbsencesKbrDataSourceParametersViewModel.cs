﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.AbsencesKbrReportParametres")]
    public class AbsencesKbrDataSourceParametersViewModel : AbsencesDataSourceParametersBaseViewModel
    {
        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime AbsencesFrom { get; set; }

        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime AbsencesTo { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(ReportResources.ApprovedFromDateText), typeof(ReportResources))]
        public DateTime? ApprovedFrom { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(ReportResources.ApprovedToDateText), typeof(ReportResources))]
        public DateTime? ApprovedTo { get; set; }
    }
}
