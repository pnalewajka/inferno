﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsencesKbrDataSourceParametersViewModelToAbsencesKbrDataSourceParametersDtoMapping : ClassMapping<AbsencesKbrDataSourceParametersViewModel, AbsencesKbrDataSourceParametersDto>
    {
        public AbsencesKbrDataSourceParametersViewModelToAbsencesKbrDataSourceParametersDtoMapping()
        {
            Mapping = v => new AbsencesKbrDataSourceParametersDto
            {
                AbsencesFrom = v.AbsencesFrom,
                AbsencesTo = v.AbsencesTo,
                ApprovedFrom = v.ApprovedFrom,
                ApprovedTo = v.ApprovedTo,
                IsVacation = v.IsVacation,
                AbsenceTypeIds = v.AbsenceTypeIds,
                EmploymentType = v.EmploymentType,
                ContractTypeIds = v.ContractTypeIds,
                CompanyIds = v.CompanyIds,
                OrgUnitIds = v.OrgUnitIds,
                LocationIds = v.LocationIds
            };
        }
    }
}
