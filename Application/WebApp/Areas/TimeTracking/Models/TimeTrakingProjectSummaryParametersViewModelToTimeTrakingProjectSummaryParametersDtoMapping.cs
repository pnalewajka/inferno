﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrakingProjectSummaryParametersViewModelToTimeTrakingProjectSummaryParametersDtoMapping : ClassMapping<TimeTrakingProjectSummaryParametersViewModel, TimeTrakingProjectSummaryParametersDto>
    {
        public TimeTrakingProjectSummaryParametersViewModelToTimeTrakingProjectSummaryParametersDtoMapping()
        {
            Mapping = vm => new TimeTrakingProjectSummaryParametersDto
            {
                ProjectId = vm.ProjectId,
                StartDate = vm.StartDate,
                EndDate = vm.EndDate
            };
        }
    }
}
