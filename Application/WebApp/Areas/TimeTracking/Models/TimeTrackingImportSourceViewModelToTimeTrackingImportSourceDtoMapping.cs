﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingImportSourceViewModelToTimeTrackingImportSourceDtoMapping : ClassMapping<TimeTrackingImportSourceViewModel, TimeTrackingImportSourceDto>
    {
        public TimeTrackingImportSourceViewModelToTimeTrackingImportSourceDtoMapping()
        {
            Mapping =
                v =>
                    new TimeTrackingImportSourceDto
                    {
                        Id = v.Id,
                        Code = v.Code,
                        Name = v.Name,
                        ImportStrategyIdentifier = v.ImportStrategyIdentifier,
                        Configuration = v.Configuration
                    };
        }
    }
}
