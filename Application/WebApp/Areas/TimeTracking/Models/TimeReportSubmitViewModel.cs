﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.TimeReportSubmit")]
    public class TimeReportSubmitViewModel
    {
        public IReadOnlyCollection<string> Warnings { get; set; }

        public TimeReportSubmitViewModel(IReadOnlyCollection<string> warnings)
        {
            Warnings = warnings;
        }
    }
}