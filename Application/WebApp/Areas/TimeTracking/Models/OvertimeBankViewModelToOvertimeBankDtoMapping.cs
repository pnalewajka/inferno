﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeBankViewModelToOvertimeBankDtoMapping : ClassMapping<OvertimeBankViewModel, OvertimeBankDto>
    {
        public OvertimeBankViewModelToOvertimeBankDtoMapping()
        {
            Mapping = v => new OvertimeBankDto
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                EmployeeFirstName = v.EmployeeFirstName,
                EmployeeLastName = v.EmployeeLastName,
                RequestId = v.RequestId,
                OvertimeChange = v.OvertimeChange,
                EffectiveOn = v.EffectiveOn,
                OvertimeBalance = v.OvertimeBalance,
                Comment = v.Comment,
                Timestamp = v.Timestamp
            };
        }
    }
}
