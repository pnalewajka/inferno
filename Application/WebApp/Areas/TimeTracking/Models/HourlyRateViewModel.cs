﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class HourlyRateViewModel
    {
        public long Id { get; set; }
        
        [DisplayNameLocalized(nameof(TimeReportResources.HourlyRateDescriptionLabel), typeof(TimeReportResources))]
        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}
