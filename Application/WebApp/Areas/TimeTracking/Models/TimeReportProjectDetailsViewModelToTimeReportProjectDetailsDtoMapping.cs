﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectDetailsViewModelToTimeReportProjectDetailsDtoMapping : ClassMapping<TimeReportProjectDetailsViewModel, TimeReportProjectDetailsDto>
    {
        public TimeReportProjectDetailsViewModelToTimeReportProjectDetailsDtoMapping()
        {
            Mapping = v => new TimeReportProjectDetailsDto
            {
                EmployeeLastName = v.EmployeeLastName,
                EmployeeFirstName = v.EmployeeFirstName,
                OrgUnitId = v.OrgUnitId,
                ProjectId = v.ProjectId,
                TimeReportStatus = v.TimeReportStatus,
                OvertimeTotals = v.OvertimeTotals,
                OvertimeNormalHours = v.OvertimeNormalHours,
                OvertimeLateNightHours = v.OvertimeLateNightHours,
                OvertimeWeekendHours = v.OvertimeWeekendHours,
                OvertimeHolidayHours = v.OvertimeHolidayHours,
                TotalHours = v.TotalHours,
                TimeReportId = v.TimeReportId
            };
        }
    }
}
