﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Filters.AffectedUserFilter")]
    [DescriptionLocalized("AffectedUserFilterDialogTitle", typeof(RequestResource))]
    public class AffectedUserFilterViewModel
    {
        [DisplayNameLocalized(nameof(RequestResource.AffectedUserLabel), typeof(RequestResource))]
        [ValuePicker(Type = typeof(UserPickerController), DialogWidth = "1000px")]
        [Render(Size = Size.Large)]
        public long AffectedUserId { get; set; }

        public override string ToString()
        {
            return RequestResource.AffectedUserLabel;
        }
    }
}