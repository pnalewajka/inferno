﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectAttributeValueDtoToTimeReportProjectAttributeValueViewModelMapping : ClassMapping<TimeReportProjectAttributeValueDto, TimeReportProjectAttributeValueViewModel>
    {
        public TimeReportProjectAttributeValueDtoToTimeReportProjectAttributeValueViewModelMapping()
        {
            Mapping = d => new TimeReportProjectAttributeValueViewModel
            {
                Id = d.Id,
                AttributeId = d.AttributeId,
                Name = d.Name,
                Description = d.Description,
                Features = d.Features,
            };
        }
    }
}
