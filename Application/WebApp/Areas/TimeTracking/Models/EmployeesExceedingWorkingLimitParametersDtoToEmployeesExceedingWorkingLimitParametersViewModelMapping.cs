﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class EmployeesExceedingWorkingLimitParametersDtoToEmployeesExceedingWorkingLimitParametersViewModelMapping : ClassMapping<EmployeesExceedingWorkingLimitParametersDto, EmployeesExceedingWorkingLimitParametersViewModel>
    {
        public EmployeesExceedingWorkingLimitParametersDtoToEmployeesExceedingWorkingLimitParametersViewModelMapping()
        {
            Mapping = d => new EmployeesExceedingWorkingLimitParametersViewModel
            {
                Year = d.Year,
                Month = d.Month,
                OrgUnitIds = d.OrgUnitIds,
                LocationIds = d.LocationIds,
                ContractTypeIds = d.ContractTypeIds
            };
        }
    }
}
