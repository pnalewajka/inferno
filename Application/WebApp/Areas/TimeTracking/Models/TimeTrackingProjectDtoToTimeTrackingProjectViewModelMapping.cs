﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingProjectDtoToTimeTrackingProjectViewModelMapping : ClassMapping<TimeTrackingProjectDto, TimeTrackingProjectViewModel>
    {
        public TimeTrackingProjectDtoToTimeTrackingProjectViewModelMapping()
        {
            Mapping = d => new TimeTrackingProjectViewModel
            {
                Id = d.Id,
                Name = d.Name,
                ClientName = d.ClientName,
                PetsCode = d.PetsCode,
                ProjectManagerName = d.ProjectManagerName,
                JiraCode = d.JiraCode,
                Attributes = d.Attributes.Select(a => new MyTimeReportProjectAttributeViewModel
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    DefaultValueId = a.DefaultValueId,
                    PossibleValues = a.PossibleValues.Select(v => new MyTimeReportProjectAttributeValueViewModel
                    {
                        Id = v.Id,
                        Name = v.Name,
                        Description = v.Description,
                    }).ToList()
                }).ToArray(),
            };
        }
    }
}
