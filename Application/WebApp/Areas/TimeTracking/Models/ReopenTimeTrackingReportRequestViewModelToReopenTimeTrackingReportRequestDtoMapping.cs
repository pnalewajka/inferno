﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ReopenTimeTrackingReportRequestViewModelToReopenTimeTrackingReportRequestDtoMapping : ClassMapping<ReopenTimeTrackingReportRequestViewModel, ReopenTimeTrackingReportRequestDto>
    {
        public ReopenTimeTrackingReportRequestViewModelToReopenTimeTrackingReportRequestDtoMapping()
        {
            Mapping = v => new ReopenTimeTrackingReportRequestDto
            {
                Month = v.Month,
                Year = v.Year,
                ProjectIds = v.ProjectIds,
                Reason = v.Reason
            };
        }
    }
}
