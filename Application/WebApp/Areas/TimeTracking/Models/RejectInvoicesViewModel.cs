﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.RejectInvoices")]
    public class RejectInvoicesViewModel
    {
        [Required]
        [Multiline(5)]
        [StringLength(256)]
        [DisplayNameLocalized(nameof(TimeReportResources.Justification), typeof(TimeReportResources))]
        public string Justification { get; set; }

        [HiddenInput]
        public List<long> TimeReportIds { get; set; }

        [AllowHtml]
        [HiddenInput]
        [HtmlSanitize]
        public string Message { get; set; }
    }
}
