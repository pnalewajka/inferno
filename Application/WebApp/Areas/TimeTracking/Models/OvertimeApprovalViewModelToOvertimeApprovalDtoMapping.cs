﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeApprovalViewModelToOvertimeApprovalDtoMapping : ClassMapping<OvertimeApprovalViewModel, OvertimeApprovalDto>
    {
        public OvertimeApprovalViewModelToOvertimeApprovalDtoMapping()
        {
            Mapping = v => new OvertimeApprovalDto
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                ProjectId = v.ProjectId,
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                HourLimit = v.HourLimit,
                RequestId = v.RequestId,
                Timestamp = v.Timestamp
            };
        }
    }
}
