﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class WorkingStudentDataSourceParametersViewModelToWorkingStudentDataSourceParametersDtoMapping : ClassMapping<WorkingStudentDataSourceParametersViewModel, WorkingStudentDataSourceParametersDto>
    {
        public WorkingStudentDataSourceParametersViewModelToWorkingStudentDataSourceParametersDtoMapping()
        {
            Mapping = v => new WorkingStudentDataSourceParametersDto
            {
                From = v.From,
                To = v.To,
                Status = v.Status,
                EmployeeIds = v.EmployeeIds,
                OrgUnitIds = v.EmployeeOrgUnitIds,
                LocationIds = v.LocationIds,
                ContractTypeIds = v.ContractTypeIds,
                IsMyEmployees = v.MyEmployees
            };
        }
    }
}
