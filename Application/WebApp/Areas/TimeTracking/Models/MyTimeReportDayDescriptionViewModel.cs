﻿using System;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportDayDescriptionViewModel
    {
        public DateTime Date { get; set; }

        public decimal ContractedHours { get; set; }

        public bool IsWeekend
        {
            get
            {
                var dayOfWeek = Date.DayOfWeek;

                return dayOfWeek == DayOfWeek.Sunday
                       || dayOfWeek == DayOfWeek.Saturday;
            }
        }

        public bool IsLastDayOfWeek => Date.DayOfWeek == DayOfWeek.Sunday;

        public bool IsHoliday { get; set; }

        public string HolidayName { get; set; }

        public decimal? DailyWorkingLimit { get; set; }

        public string CssClasses
        {
            get
            {
                var holidayClass = IsHoliday ? "holiday" : string.Empty;
                var weekendClass = IsWeekend ? "weekend" : string.Empty;
                var endOfWeekClass = IsLastDayOfWeek ? "end-of-week" : string.Empty;

                var dayClass = $"{holidayClass} {weekendClass} {endOfWeekClass}";

                return string.IsNullOrEmpty(dayClass.Trim()) ? "working-day" : dayClass.Trim();
            }
        }
    }
}