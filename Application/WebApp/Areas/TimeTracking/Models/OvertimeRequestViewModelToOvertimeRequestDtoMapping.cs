﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeRequestViewModelToOvertimeRequestDtoMapping : ClassMapping<OvertimeRequestViewModel, OvertimeRequestDto>
    {
        public OvertimeRequestViewModelToOvertimeRequestDtoMapping()
        {
            Mapping = v => new OvertimeRequestDto
            {
                ProjectId = v.ProjectId,
                From = v.From,
                To = v.To.AddDays(1).AddSeconds(-1),
                HourLimit = v.HourLimit,
                Comment = v.Comment,
            };
        }
    }
}
