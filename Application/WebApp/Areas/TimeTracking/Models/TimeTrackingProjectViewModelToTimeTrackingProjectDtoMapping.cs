﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingProjectViewModelToTimeTrackingProjectDtoMapping : ClassMapping<TimeTrackingProjectViewModel, TimeTrackingProjectDto>
    {
        public TimeTrackingProjectViewModelToTimeTrackingProjectDtoMapping()
        {
            Mapping = v => new TimeTrackingProjectDto
            {
                Id = v.Id,
                Name = v.Name
            };
        }
    }
}
