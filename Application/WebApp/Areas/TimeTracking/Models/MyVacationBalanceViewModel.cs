﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyVacationBalanceViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(MyVacationBalanceResources.EffectiveOn), typeof(MyVacationBalanceResources))]
        [Order(1)]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "date-column")]
        [Visibility(VisibilityScope.FormAndGrid)]
        public DateTime EffectiveOn { get; set; }

        [DisplayNameLocalized(nameof(MyVacationBalanceResources.Operation), typeof(MyVacationBalanceResources))]
        [Order(2)]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        public VacationOperation Operation { get; set; }

        [DisplayNameLocalized(nameof(MyVacationBalanceResources.RegularVacationHourChange), typeof(MyVacationBalanceResources))]
        [Order(3)]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        public decimal HourChange { get; set; }

        [DisplayNameLocalized(nameof(MyVacationBalanceResources.HourBalanceAfterChange), typeof(MyVacationBalanceResources))]
        [Order(4)]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        public decimal TotalHourBalance { get; set; }

        [DisplayNameLocalized(nameof(MyVacationBalanceResources.Comment), typeof(MyVacationBalanceResources))]
        [Order(5)]
        [NonSortable]
        [Visibility(VisibilityScope.FormAndGrid)]
        public string Comment { get; set; }
    }
}