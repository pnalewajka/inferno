﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingDataSourceParametersDtoToTimeTrackingDataSourceParametersViewModelMapping : ClassMapping<TimeTrackingDataSourceParametersDto, TimeTrackingDataSourceParametersViewModel>
    {
        public TimeTrackingDataSourceParametersDtoToTimeTrackingDataSourceParametersViewModelMapping()
        {
            Mapping = d => new TimeTrackingDataSourceParametersViewModel
            {
                From = d.From,
                To = d.To,
                Status = d.Status,
                EmploymentType = d.EmploymentType,
                ProjectType = d.ProjectType,
                ProjectIds = d.ProjectIds,
                EmployeeIds = d.EmployeeIds,
                ProjectOrgUnitIds = d.ProjectOrgUnitIds,
                ProjectTagsIds = d.ProjectTagsIds,
                EmployeeOrgUnitIds = d.EmployeeOrgUnitIds,
                HourlyRateTypes = d.HourlyRateTypes,
                ProjectClientIds = d.ProjectClientIds,
                LocationIds = d.LocationIds,
                ContractTypeIds = d.ContractTypeIds,
                MyEmployees = d.MyEmployees,
            };
        }
    }
}
