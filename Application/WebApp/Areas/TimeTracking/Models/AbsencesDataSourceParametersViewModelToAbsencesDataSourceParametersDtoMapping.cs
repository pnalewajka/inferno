﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsencesDataSourceParametersViewModelToAbsencesDataSourceParametersDtoMapping : ClassMapping<AbsencesDataSourceParametersViewModel, AbsencesDataSourceParametersDto>
    {
        public AbsencesDataSourceParametersViewModelToAbsencesDataSourceParametersDtoMapping()
        {
            Mapping = v => new AbsencesDataSourceParametersDto
            {
                ApprovedFrom = v.ApprovedFrom,
                ApprovedTo = v.ApprovedTo,
                IsVacation = v.IsVacation,
                AbsenceTypeIds = v.AbsenceTypeIds,
                EmploymentType = v.EmploymentType,
                ContractTypeIds = v.ContractTypeIds,
                CompanyIds = v.CompanyIds,
                OrgUnitIds = v.OrgUnitIds,
                LocationIds = v.LocationIds
            };
        }
    }
}
