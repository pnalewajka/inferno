﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeApprovalDtoToOvertimeApprovalViewModelMapping : ClassMapping<OvertimeApprovalDto, OvertimeApprovalViewModel>
    {
        public OvertimeApprovalDtoToOvertimeApprovalViewModelMapping()
        {
            Mapping = d => new OvertimeApprovalViewModel
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                ProjectId = d.ProjectId,
                DateFrom = d.DateFrom,
                DateTo = d.DateTo,
                HourLimit = d.HourLimit,
                RequestId = d.RequestId,
                Timestamp = d.Timestamp
            };
        }
    }
}
