﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyVacationBalanceViewModelToVacationBalanceDtoMapping : ClassMapping<MyVacationBalanceViewModel, VacationBalanceDto>
    {
        public MyVacationBalanceViewModelToVacationBalanceDtoMapping()
        {
            Mapping = v => new VacationBalanceDto
            {
                Id = v.Id,
                EffectiveOn = v.EffectiveOn,
                Operation = v.Operation,
                Comment = v.Comment,
                HourChange = v.HourChange,
            };
        }
    }
}
