﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeRequestDtoToOvertimeRequestViewModelMapping : ClassMapping<OvertimeRequestDto, OvertimeRequestViewModel>
    {
        public OvertimeRequestDtoToOvertimeRequestViewModelMapping()
        {
            Mapping = d => new OvertimeRequestViewModel
            {
                ProjectId = d.ProjectId,
                From = d.From,
                To = d.To,
                HourLimit = d.HourLimit,
                Comment = d.Comment,
            };
        }
    }
}
