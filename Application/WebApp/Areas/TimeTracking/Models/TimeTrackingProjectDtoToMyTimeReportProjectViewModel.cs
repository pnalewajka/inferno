﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingProjectDtoToMyTimeReportProjectViewModel : ClassMapping<TimeTrackingProjectDto, MyTimeReportProjectViewModel>
    {
        public TimeTrackingProjectDtoToMyTimeReportProjectViewModel(
            IClassMapping<TimeReportProjectAttributeDto, MyTimeReportProjectAttributeViewModel> attributeDtoToAttributeViewModelMapping)
        {
            Mapping = dto => new MyTimeReportProjectViewModel
            {
                ProjectId = dto.Id,
                ProjectName = dto.Name,
                Attributes = dto.Attributes.Select(attributeDtoToAttributeViewModelMapping.CreateFromSource).ToList(),
                Tasks = new List<MyTimeReportTaskViewModel>(),
                Apn = dto.Apn,
                KupferwerkProjectId = dto.KupferwerkProjectId,
                ReportingStartsOn = dto.ReportingStartsOn,
            };
        }
    }
}