﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class JiraIssueToTimeReportRowMapperViewModelToJiraIssueToTimeReportRowMapperDtoMapping : ClassMapping<JiraIssueToTimeReportRowMapperViewModel, JiraIssueToTimeReportRowMapperDto>
    {
        public JiraIssueToTimeReportRowMapperViewModelToJiraIssueToTimeReportRowMapperDtoMapping()
        {
            Mapping = v => new JiraIssueToTimeReportRowMapperDto
            {
                Id = v.Id,
                Name = v.Name,
            };
        }
    }
}
