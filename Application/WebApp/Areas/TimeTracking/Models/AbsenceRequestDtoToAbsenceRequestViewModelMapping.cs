﻿using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceRequestDtoToAbsenceRequestViewModelMapping : ClassMapping<AbsenceRequestDto, AbsenceRequestViewModel>
    {
        public AbsenceRequestDtoToAbsenceRequestViewModelMapping(IAbsenceService absenceService)
        {
            Mapping = dto => 
            new AbsenceRequestViewModel
            {
                AffectedUserId = dto.AffectedUserId,
                Comment = dto.Comment,
                From = dto.From,
                To = dto.To,
                AbsenceTypeId = dto.AbsenceTypeId,
                SubstituteUserId = dto.SubstituteUserId,
                AdditionalInformation = dto.AbsenceTypeId != 0
                    ? absenceService.GetAbsenceTypeDescriptionByAbsenceTypeId(dto.AbsenceTypeId, dto.AffectedUserId)
                    : AbsenceRequestResources.AbsenceTypeHint,
                Hours = dto.Hours
            };
        }
    }
}