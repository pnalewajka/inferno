﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportProjectViewModel
    {
        public MyTimeReportProjectViewModel()
        {
            Tasks = new List<MyTimeReportTaskViewModel>();
            Attributes = new List<MyTimeReportProjectAttributeViewModel>();
        }

        public long ProjectId { set; get; }
        public string ProjectName { get; set; }
        public bool IsReadOnly { get; set; }
        public IList<MyTimeReportTaskViewModel> Tasks { get; set; }
        public IList<MyTimeReportProjectAttributeViewModel> Attributes { get; set; }
        public string Apn { get; set; }
        public string KupferwerkProjectId { get; set; }
        public string PetsCode { get; set; }
        public DateTime? ReportingStartsOn { get; set; }
        public OverTimeTypes AllowedProjectOvertime { get; set; }

        public decimal GetHours(DateTime day)
        {
            return Tasks.SelectMany(t => t.Days).Where(d => d.Day == day).Sum(d => d.Hours);
        }

        public string GetProjectTooltipTitle()
        {
            return string.Format(TimeReportResources.ProjectTooltipTitle,
                ProjectName,
                string.IsNullOrEmpty(Apn) ? TimeReportResources.None : Apn,
                string.IsNullOrEmpty(KupferwerkProjectId) ? TimeReportResources.None : KupferwerkProjectId);
        }
    }
}