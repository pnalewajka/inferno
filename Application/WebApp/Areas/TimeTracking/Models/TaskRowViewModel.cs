﻿using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.TimeTracking.Helpers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TaskRowViewModel
    {
        public MyTimeReportProjectViewModel ParentProject { get; set; }
        public MyTimeReportTaskViewModel Task { get; set; }
        public IList<MyTimeReportDayDescriptionViewModel> Days { get; set; }

        public string GetStatusIconClass()
        {
            return TimeReportStatusHelper.GetIconForStatus(Task.Status);
        }
    }
}