﻿namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportWeekDescriptionViewModel
    {
        public int WeekNumber { get; set; }

        public int WeekDays { get; set; }
    }
}