﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportDtoToTimeReportViewModelMapping : ClassMapping<TimeReportDto, TimeReportViewModel>
    {
        private readonly IPrincipalProvider _principalProvider;

        public TimeReportDtoToTimeReportViewModelMapping(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;

            Mapping = d => new TimeReportViewModel
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                EmployeeFullName = $"{d.EmployeeFirstName} {d.EmployeeLastName}",
                EmployeeAcronym = d.EmployeeAcronym,
                EmployeeEmail = d.EmployeeEmail,
                ProjectStatuses = d.ProjectStatusDtos.Select(x => new ProjectStatusViewModel { Id = x.ProjectId, Name = x.Name, Status = x.Status }).ToArray(),
                Year = d.Year,
                Month = d.Month,
                ContractedHours = d.ContractedHours,
                ReportedNormalHours = d.ReportedNormalHours,
                NoServiceHours = d.NoServiceHours,
                Status = d.Status,
                ControllingStatus = d.ControllingStatus,
                ControllingErrorMessage = d.ControllingErrorMessage,
                InvoiceNotificationStatus = d.InvoiceNotificationStatus,
                InvoiceNotificationErrorMessage = d.InvoiceNotificationErrorMessage,
                InvoiceStatus = d.InvoiceStatus,
                InvoiceIssues = d.InvoiceIssues,
                InvoiceRejectionReason = d.InvoiceRejectionReason,
                InvoiceAmount = GetInvoiceAmount(d),
                InvoiceDocuments = d.InvoiceDocument == null
                    ? new List<DocumentViewModel>()
                    : new[] { new DocumentViewModel
                    {
                        ContentType = d.InvoiceDocument.ContentType,
                        DocumentName = d.InvoiceDocument.DocumentName,
                        DocumentId = d.InvoiceDocument.DocumentId,
                    }}
                    .ToList(),
                ModifiedOn = d.ModifiedOn,
                TotalHours = d.TotalHours,
                RegularHours = d.RegularHours,
                OvertimeHours = d.OvertimeHours,
                TaxDeductHours = d.TaxDeductHours,
                Timestamp = d.Timestamp,
                Url = GetTimeReportUrl(d, false),
                EditOnBehalfUrl = GetTimeReportUrl(d, true),
                CanBeReopened = d.CanBeReopened,
                ContractTypeName = d.ContractTypeName,
                OrgUnitName = d.OrgUnitName,
                LineManagerName = d.LineManagerName,
                LineManagerId = d.LineManagerId,
            };
        }

        public string GetInvoiceAmount(TimeReportDto timeReportDto)
        {
            if (string.IsNullOrEmpty(timeReportDto.InvoiceCalculation))
            {
                return null;
            }

            var calculationResult = JsonHelper.Deserialize<CalculationResult>(timeReportDto.InvoiceCalculation);
            var totalCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.CompensationTotal);

            var amount = totalCostItems.Sum(g => g.Value);
            var currency = totalCostItems.Select(g => g.CurrencyIsoCode).Distinct().Single();

            return CurrencyAmount.CreateValueAmount(amount, currency).ToString();
        }

        private string GetTimeReportUrl(TimeReportDto timeReportDto, bool onBehalf)
        {
            var areaName = RoutingHelper.GetAreaName(typeof(MyTimeReportController));
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            string controllerName;

            if (timeReportDto.EmployeeId == _principalProvider.Current.EmployeeId)
            {
                controllerName = RoutingHelper.GetControllerName<MyTimeReportController>();
                return urlHelper.Action(nameof(MyTimeReportController.Index), controllerName,
                    new { Area = areaName, year = timeReportDto.Year, month = timeReportDto.Month });
            }

            if (onBehalf)
            {
                controllerName = RoutingHelper.GetControllerName<MyTimeReportController>();
                return urlHelper.Action(nameof(MyTimeReportController.Index), controllerName, new
                {
                    Area = areaName,
                    year = timeReportDto.Year,
                    month = timeReportDto.Month,
                    employeeId = timeReportDto.EmployeeId
                });
            }

            controllerName = RoutingHelper.GetControllerName<EmployeeTimeReportController>();
            return urlHelper.Action(nameof(EmployeeTimeReportController.TimeReport), controllerName,
                new { Area = areaName, id = timeReportDto.EmployeeId, year = timeReportDto.Year, month = timeReportDto.Month });
        }
    }
}
