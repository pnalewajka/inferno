﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportProjectAttributeViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long? DefaultValueId { get; set; }

        public IList<MyTimeReportProjectAttributeValueViewModel> PossibleValues { get; set; }

        public MyTimeReportProjectAttributeViewModel()
        {
            PossibleValues = new List<MyTimeReportProjectAttributeValueViewModel>();
        }
    }
}
