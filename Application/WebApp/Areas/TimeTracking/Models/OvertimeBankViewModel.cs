﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeBankViewModel 
    {
        public long Id { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(OvertimeBankResources.Employee), typeof(OvertimeBankResources))]
        public long EmployeeId { get; set; }

        [Order(1)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(OvertimeBankResources.FirstNameLabel), typeof(OvertimeBankResources))]
        public string EmployeeFirstName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(OvertimeBankResources.LastNameLabel), typeof(OvertimeBankResources))]
        public string EmployeeLastName { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? RequestId { get; set; }

        [Order(3)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "column-width-large date-column")]
        [DisplayNameLocalized(nameof(OvertimeBankResources.EffectiveOnLabel), typeof(OvertimeBankResources))]
        public DateTime EffectiveOn { get; set; }

        [Order(4)]
        [Range(-100,100)]
        [DisplayNameLocalized(nameof(OvertimeBankResources.OvertimeChangeLabel), typeof(OvertimeBankResources))]
        public decimal OvertimeChange { get; set; }

        [Order(5)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(OvertimeBankResources.OvertimeBalanceLabel), typeof(OvertimeBankResources))]
        public decimal OvertimeBalance { get; set; }

        [Order(6)]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(OvertimeBankResources.Comment), typeof(OvertimeBankResources))]
        public string Comment { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsAutomatic { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return String.Format($"{EmployeeFirstName} {EmployeeLastName} {OvertimeBankResources.OvertimeChangeLabel}: {OvertimeChange}");
        }
    }
}
