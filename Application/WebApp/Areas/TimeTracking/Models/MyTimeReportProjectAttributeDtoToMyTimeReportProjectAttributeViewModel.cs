﻿using System.Linq;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportProjectAttributeDtoToMyTimeReportProjectAttributeViewModel : ClassMapping<TimeReportProjectAttributeDto, MyTimeReportProjectAttributeViewModel>
    {
        public MyTimeReportProjectAttributeDtoToMyTimeReportProjectAttributeViewModel()
        {
            Mapping = dto => new MyTimeReportProjectAttributeViewModel
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                DefaultValueId = dto.DefaultValueId,
                PossibleValues = dto.PossibleValues.Select(v => new MyTimeReportProjectAttributeValueViewModel
                {
                    Id = v.Id,
                    Name = v.Name,
                    ClassName = NamingConventionHelper.ConvertPascalCaseToHyphenated(v.Name),
                    Description = v.Description,
                    Features = v.Features,
                }).ToList()
            };
        }
    }
}