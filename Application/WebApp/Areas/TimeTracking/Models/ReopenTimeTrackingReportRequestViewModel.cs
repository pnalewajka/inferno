﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.ReopenTimeTrackingReportRequest")]
    public class ReopenTimeTrackingReportRequestViewModel
    {
        [HiddenInput]
        public long EmployeeId { get; set; }

        [HiddenInput]
        [Range(1, 12)]
        public int Month { get; set; }

        [HiddenInput]
        public int Year { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ProjectsToReopen), typeof(TimeReportResources))]
        [Order(0)]
        [ValuePicker(Type = typeof(TimeTrackingProjectPickerController), OnResolveUrlJavaScript = "url=url + '&mode=1&employee-id=' + $(\"[name='RequestObjectViewModel." + nameof(EmployeeId) + "']\").val() + '&year=' + $(\"[name='RequestObjectViewModel." + nameof(Year) + "']\").val() + '&month=' + $(\"[name='RequestObjectViewModel." + nameof(Month) + "']\").val()")]
        [UpdatesApprovers]
        public long[] ProjectIds { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ReopenReason), typeof(TimeReportResources))]
        [StringLength(255)]
        [Multiline(5)]
        [Required]
        public string Reason { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.Period), typeof(TimeReportResources))]
        [ReadOnly(true)]
        [Order(6)]
        public string Period => new DateTime(Year, Month, 1).ToString("MMM yyyy");
    }
}
