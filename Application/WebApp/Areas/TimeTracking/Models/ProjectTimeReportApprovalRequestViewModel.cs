﻿using System;
using System.ComponentModel;
using System.Linq;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using ProjectPickerController = Smt.Atomic.WebApp.Areas.Allocation.Controllers.ProjectPickerController;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ProjectTimeReportApprovalRequestViewModel
    {
        public ProjectTimeReportApprovalRequestViewModel()
        {
            TasksDetails = new TasksDetailsViewModel();
        }

        [ReadOnly(true)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.Employee), typeof(ProjectTimeReportApprovalRequestResources))]
        public long EmployeeId { get; set; }

        [ReadOnly(true)]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.Project), typeof(ProjectTimeReportApprovalRequestResources))]
        public long ProjectId { get; set; }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.TotalNrOfBillableHours), typeof(ProjectTimeReportApprovalRequestResources))]
        public decimal BillableHours
        {
            get { return TasksDetails.Tasks.Where(t => t.IsBillable).Sum(d => d.Hours); }
        }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.TotalNrOfNotBillableHours), typeof(ProjectTimeReportApprovalRequestResources))]
        public decimal NonBillableHours
        {
            get { return TasksDetails.Tasks.Where(t => !t.IsBillable).Sum(d => d.Hours); }
        }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.TotalNrOfHours), typeof(ProjectTimeReportApprovalRequestResources))]
        public decimal TotalNumberOfHours
        {
            get { return TasksDetails.Tasks.Sum(d => d.Hours); }
        }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.From), typeof(ProjectTimeReportApprovalRequestResources))]
        public DateTime From { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.To), typeof(ProjectTimeReportApprovalRequestResources))]
        public DateTime To { get; set; }

        [CustomControl(TemplatePath = "Areas.TimeTracking.Views.TimeReport._TasksDetails")]
        public TasksDetailsViewModel TasksDetails { get; set; }
    }
}