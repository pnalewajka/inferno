﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceViewModelToVacationBalanceDtoMapping : ClassMapping<VacationBalanceViewModel, VacationBalanceDto>
    {
        public VacationBalanceViewModelToVacationBalanceDtoMapping()
        {
            Mapping = v => new VacationBalanceDto
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                EffectiveOn = v.EffectiveOn,
                Operation = v.Operation,
                Comment = v.Comment,
                HourChange = v.HourChange,
                Timestamp = v.Timestamp
            };
        }
    }
}
