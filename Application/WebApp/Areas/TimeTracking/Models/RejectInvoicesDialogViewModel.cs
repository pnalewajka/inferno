﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class RejectInvoicesDialogViewModel
    {
        public List<long> TimeReportIds { get; set; }

        [AllowHtml]
        [HtmlSanitize]
        public string Message { get; set; }
    }
}
