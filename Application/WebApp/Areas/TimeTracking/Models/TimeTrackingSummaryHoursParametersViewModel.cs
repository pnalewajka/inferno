﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.TimeTrackingSummaryHoursParameters")]
    [TabDescription(0, "basic", "BasicTabText", typeof(ReportResources))]
    [TabDescription(1, "employees", "EmployeesTabText", typeof(ReportResources))]
    public class TimeTrackingSummaryHoursParametersViewModel
    {
        [Tab("basic")]
        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.FromYearText), typeof(ReportResources))]
        public int FromYear { get; set; }

        [Tab("basic")]
        [Order(2)]
        [Required]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(MonthsListItemProvider))]
        [DisplayNameLocalized(nameof(ReportResources.FromMonthText), typeof(ReportResources))]
        public int FromMonth { get; set; }

        [Tab("basic")]
        [Order(3)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ToYearText), typeof(ReportResources))]
        public int ToYear { get; set; }

        [Tab("basic")]
        [Order(4)]
        [Required]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ItemProvider = typeof(MonthsListItemProvider))]
        [DisplayNameLocalized(nameof(ReportResources.ToMonthText), typeof(ReportResources))]
        public int ToMonth { get; set; }

        [Tab("basic")]
        [Order(5)]
        [Render(Size = Size.Small)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "EmptyStatusText",
            ResourceType = typeof(ReportResources), ItemProvider = typeof(EnumBasedListItemProvider<TimeReportStatus>))]
        [DisplayNameLocalized(nameof(ReportResources.StatusText), typeof(ReportResources))]
        public TimeReportStatus? Status { get; set; }

        [Tab("basic")]
        [Order(6)]
        [Render(Size = Size.Small)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "EmptyStatusText",
            ResourceType = typeof(ReportResources), ItemProvider = typeof(EnumBasedListItemProvider<EmploymentType>))]
        [DisplayNameLocalized(nameof(ReportResources.EmploymentTypeText), typeof(ReportResources))]
        public EmploymentType? EmploymentType { get; set; }

        [Tab("employees")]
        [Order(7)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeesText), typeof(ReportResources))]
        public long[] EmployeeIds { get; set; }

        [Tab("employees")]
        [Order(8)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long[] EmployeeOrgUnitIds { get; set; }

        [Tab("employees")]
        [Order(9)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.LocationText), typeof(ReportResources))]
        public long[] LocationIds { get; set; }

        [Tab("employees")]
        [Order(10)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.ContractTypeText), typeof(ReportResources))]
        public long[] ContractTypeIds { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResult = new List<ValidationResult>();

            if (FromYear * 12 + FromMonth >= ToYear * 12 + ToMonth)
            {
                validationResult.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate, Enumerable.Empty<string>()));
            }

            return validationResult;
        }
    }
}
