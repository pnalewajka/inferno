﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class CalendarAbsenceViewModelToCalendarAbsenceDtoMapping : ClassMapping<CalendarAbsenceViewModel, CalendarAbsenceDto>
    {
        public CalendarAbsenceViewModelToCalendarAbsenceDtoMapping()
        {
            Mapping = v => new CalendarAbsenceDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                OfficeGroupEmail = v.OfficeGroupEmail,
                Timestamp = v.Timestamp,
                EmployeesIds = v.EmployeesIds != null && v.EmployeesIds.Length > 0 ? v.EmployeesIds : new long[] { }
            };
        }
    }
}
