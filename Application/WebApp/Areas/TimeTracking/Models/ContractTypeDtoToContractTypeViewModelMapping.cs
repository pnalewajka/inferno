﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ContractTypeDtoToContractTypeViewModelMapping : ClassMapping<ContractTypeDto, ContractTypeViewModel>
    {
        public ContractTypeDtoToContractTypeViewModelMapping()
        {
            Mapping = d => new ContractTypeViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                EmploymentType = d.EmploymentType,
                IsActive = d.IsActive,
                ActiveDirectoryCode = d.ActiveDirectoryCode,
                TimeTrackingCompensationCalculator = d.TimeTrackingCompensationCalculator,
                BusinessTripCompensationCalculator = d.BusinessTripCompensationCalculator,
                AllowedOverTimeTypes = d.AllowedOverTimeTypes,
                HasPaidVacation = d.HasPaidVacation,
                AbsenceTypeIds = d.AbsenceTypeIds,
                DailyWorkingLimit = d.DailyWorkingLimit,
                MinimunOvertimeBankBalance = d.MinimunOvertimeBankBalance,
                UseAutomaticOvertimeBankCalculation = d.UseAutomaticOvertimeBankCalculation,
                ShouldNotifyOnAbsenceRemoval = d.ShouldNotifyOnAbsenceRemoval,
                SettlementSectionsFeatures = d.SettlementSectionsFeatures,
                NotifyInvoiceAmount = d.NotifyInvoiceAmount,
                NotifyMissingBusinessTripSettlement = d.NotifyMissingBusinessTripSettlement,
                UseDailySettlementMeals = d.UseDailySettlementMeals,
                HasToSpecifyDestinationType = d.HasToSpecifyDestinationType,
                IsTaxDeductibleCostEnabled = d.IsTaxDeductibleCostEnabled,
                Timestamp = d.Timestamp,
            };
        }
    }
}
