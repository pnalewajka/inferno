﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class EmployeesExceedingWorkingLimitParametersViewModelToEmployeesExceedingWorkingLimitParametersDtoMapping : ClassMapping<EmployeesExceedingWorkingLimitParametersViewModel, EmployeesExceedingWorkingLimitParametersDto>
    {
        public EmployeesExceedingWorkingLimitParametersViewModelToEmployeesExceedingWorkingLimitParametersDtoMapping()
        {
            Mapping = v => new EmployeesExceedingWorkingLimitParametersDto
            {
                Year = v.Year,
                Month = v.Month,
                OrgUnitIds = v.OrgUnitIds,
                LocationIds = v.LocationIds,
                ContractTypeIds = v.ContractTypeIds
            };
        }
    }
}
