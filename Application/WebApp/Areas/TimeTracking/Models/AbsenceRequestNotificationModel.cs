﻿using System;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceRequestNotificationModel
    {
        public string ReceiverName { get; set; }

        public string EmployeeFullName { get; set; }

        public string Description { get; set; }

        public Uri RequestsWatchedByMeLink { get; set; }
    }

}