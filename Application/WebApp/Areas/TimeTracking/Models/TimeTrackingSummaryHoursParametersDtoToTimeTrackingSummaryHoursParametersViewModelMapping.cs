﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingSummaryHoursParametersDtoToTimeTrackingSummaryHoursParametersViewModelMapping : ClassMapping<TimeTrackingSummaryHoursParametersDto, TimeTrackingSummaryHoursParametersViewModel>
    {
        public TimeTrackingSummaryHoursParametersDtoToTimeTrackingSummaryHoursParametersViewModelMapping()
        {
            Mapping = d => new TimeTrackingSummaryHoursParametersViewModel
            {
                FromYear = d.FromYear,
                FromMonth = d.FromMonth,
                ToYear = d.ToYear,
                ToMonth = d.ToMonth,
                Status = d.Status,
                EmploymentType = d.EmploymentType,
                EmployeeIds = d.EmployeeIds,
                EmployeeOrgUnitIds = d.EmployeeOrgUnitIds,
                LocationIds = d.LocationIds,
                ContractTypeIds = d.ContractTypeIds
            };
        }
    }
}
