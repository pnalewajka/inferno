﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceParametersViewModelToVacationBalanceParametersDtoMapping : ClassMapping<VacationBalanceParametersViewModel, VacationBalanceParametersDto>
    {
        public VacationBalanceParametersViewModelToVacationBalanceParametersDtoMapping()
        {
            Mapping = v => new VacationBalanceParametersDto
            {
                EffectiveOn = v.EffectiveOn,
                EmployeeScope = v.EmployeeScope,
                ContractTypeIds = v.ContractTypeIds
            };
        }
    }
}
