﻿using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceDtoToVacationBalanceImportViewModelMapping :
        ClassMapping<VacationBalanceDto, VacationBalanceImportViewModel>
    {
        public VacationBalanceDtoToVacationBalanceImportViewModelMapping(
            IAllocationRequestImportLookupService lookupService)
        {
            Mapping = d => new VacationBalanceImportViewModel
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                Login = d.EmployeeLogin,
                FirstName = d.EmployeeFirstName,
                LastName = d.EmployeeLastName,
                EffectiveOn = d.EffectiveOn,
                Operation = d.Operation,
                Comment = d.Comment,
                HourChange = d.HourChange,
                TotalHourBalance = d.TotalHourBalance,
                EmployeeEmail = lookupService.GetEmployeeEmailById(d.EmployeeId),
                Timestamp = d.Timestamp
            };
        }
    }
}