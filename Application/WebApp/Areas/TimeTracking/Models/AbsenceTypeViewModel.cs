﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceTypeViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(2)]
        [StringLength(32)]
        [Required]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.Code), typeof(AbsenceTypeResources))]
        public string Code { get; set; }

        [Order(1)]
        [LocalizedStringLength(255)]
        [LocalizedStringRequired]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.Name), typeof(AbsenceTypeResources))]
        public LocalizedString Name { get; set; }

        [Order(3)]
        [NonSortable]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.Description), typeof(AbsenceTypeResources))]
        public LocalizedString Description { get; set; }

        [Order(4)]
        [NonSortable]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.EmployeeInstruction), typeof(AbsenceTypeResources))]
        public LocalizedString EmployeeInstruction { get; set; }

        [Order(5)]
        [StringLength(255)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.KbrCode), typeof(AbsenceTypeResources))]
        public string KbrCode { get; set; }

        [Order(6)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.IsVacation), typeof(AbsenceTypeResources))]
        public bool IsVacation { get; set; }

        [Order(7)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.IsTimeOffForOvertime), typeof(AbsenceTypeResources))]
        public bool IsTimeOffForOvertime { get; set; }

        [Order(8)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.RequiresHRApproval), typeof(AbsenceTypeResources))]
        public bool RequiresHRApproval { get; set; }

        [Order(9)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.ReportToProject), typeof(AbsenceTypeResources))]
        [Visibility(VisibilityScope.Form)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "NotReportedToProject",
            ResourceType = typeof(AbsenceTypeResources), ItemProvider = typeof(AbsenceProjectListItemProvider))]
        public long? AbsenceProjectId { get; set; }

        [Order(10)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.Order), typeof(AbsenceTypeResources))]
        public int Order { get; set; }

        [Order(11)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AbsenceTypeResources.AllowHourlyReporting), typeof(AbsenceTypeResources))]
        public bool AllowHourlyReporting { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
