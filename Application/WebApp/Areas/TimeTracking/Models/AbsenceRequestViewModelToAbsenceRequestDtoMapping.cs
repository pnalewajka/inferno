﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceRequestViewModelToAbsenceRequestDtoMapping : ClassMapping<AbsenceRequestViewModel, AbsenceRequestDto>
    {
        public AbsenceRequestViewModelToAbsenceRequestDtoMapping()
        {
            Mapping = viewModel =>
            new AbsenceRequestDto
            {
                AffectedUserId = viewModel.AffectedUserId,
                Comment = viewModel.Comment,
                From = viewModel.From,
                To = viewModel.To,
                SubstituteUserId = viewModel.SubstituteUserId,
                AbsenceTypeId = viewModel.AbsenceTypeId,
                Hours = viewModel.Hours
            };
        }
    }
}