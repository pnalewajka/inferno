﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AllocatedAndReportedHoursParametersViewModelToAllocatedAndReportedHoursParametersDtoMapping : ClassMapping<AllocatedAndReportedHoursParametersViewModel, AllocatedAndReportedHoursParametersDto>
    {
        public AllocatedAndReportedHoursParametersViewModelToAllocatedAndReportedHoursParametersDtoMapping()
        {
            Mapping = v => new AllocatedAndReportedHoursParametersDto
            {
                ProfileIds = v.ProfileIds,
                EmployeeOrgUnitIds = v.EmployeeOrgUnitIds,
                LocationIds = v.LocationIds,
                LineManagerIds = v.LineManagerIds,
                JobMatrixIds = v.JobMatrixIds,
                PlaceOfWork = v.PlaceOfWork,
                UtilizationCategoryIds = v.UtilizationCategoryIds,
                BillabilityFiltering = v.BillabilityFiltering
            };
        }
    }
}
