﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectAttributeValueViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        public long AttributeId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<AttributeValueFeature>))]
        public AttributeValueFeature Features { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
