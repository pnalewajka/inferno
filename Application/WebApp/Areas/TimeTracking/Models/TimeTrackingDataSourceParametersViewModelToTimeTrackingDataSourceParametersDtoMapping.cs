﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingDataSourceParametersViewModelToTimeTrackingDataSourceParametersDtoMapping : ClassMapping<TimeTrackingDataSourceParametersViewModel, TimeTrackingDataSourceParametersDto>
    {
        public TimeTrackingDataSourceParametersViewModelToTimeTrackingDataSourceParametersDtoMapping()
        {
            Mapping = v => new TimeTrackingDataSourceParametersDto
            {
                From = v.From,
                To = v.To,
                Status = v.Status,
                EmploymentType = v.EmploymentType,
                ProjectType = v.ProjectType,
                ProjectIds = v.ProjectIds,
                EmployeeIds = v.EmployeeIds,
                ProjectOrgUnitIds = v.ProjectOrgUnitIds,
                ProjectTagsIds = v.ProjectTagsIds,
                EmployeeOrgUnitIds = v.EmployeeOrgUnitIds,
                HourlyRateTypes = v.HourlyRateTypes,
                ProjectClientIds = v.ProjectClientIds,
                LocationIds = v.LocationIds,
                ContractTypeIds = v.ContractTypeIds,
                MyEmployees = v.MyEmployees,
            };
        }
    }
}
