﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectDetailsDtoToTimeReportProjectDetailsViewModelMapping : ClassMapping<TimeReportProjectDetailsDto, TimeReportProjectDetailsViewModel>
    {
        public TimeReportProjectDetailsDtoToTimeReportProjectDetailsViewModelMapping()
        {
            Mapping = d => new TimeReportProjectDetailsViewModel
            {
                EmployeeId = d.EmployeeId,
                EmployeeLastName = d.EmployeeLastName,
                EmployeeFirstName = d.EmployeeFirstName,
                OrgUnitId = d.OrgUnitId,
                ProjectId = d.ProjectId,
                TimeReportStatus = d.TimeReportStatus,
                OvertimeTotals = d.OvertimeTotals,
                OvertimeNormalHours = d.OvertimeNormalHours,
                OvertimeLateNightHours = d.OvertimeLateNightHours,
                OvertimeWeekendHours = d.OvertimeWeekendHours,
                OvertimeHolidayHours = d.OvertimeHolidayHours,
                TotalHours = d.TotalHours,
                TimeReportId = d.TimeReportId,
                RequestId = d.RequestId,
                RequestStatus = d.RequestStatus,
                IsRequestAvailable = d.IsRequestAvailable
            };
        }
    }
}
