﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class CalendarAbsenceViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(CalendarAbsenceResources.CalendarNameLabel), typeof(CalendarAbsenceResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(255)]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(CalendarAbsenceResources.CalendarDescriptionLabel), typeof(CalendarAbsenceResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [StringLength(255)]
        public string Description { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(CalendarAbsenceResources.OfficeGroupEmailLabel), typeof(CalendarAbsenceResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [EmailAddress]
        [StringLength(255)]
        [Hint("More info: https://wiki.intive.com/confluence/display/CS/Configuring+calendars+for+Dante")]
        public string OfficeGroupEmail { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ImpersonatedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ModifiedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Required]
        [CannotBeEmpty]
        [DisplayNameLocalized(nameof(CalendarAbsenceResources.EmployeesLabel), typeof(CalendarAbsenceResources))]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long[] EmployeesIds { get; set; }

        public override string ToString()
        {
            return Name + ": " + Description;
        }
    }
}
