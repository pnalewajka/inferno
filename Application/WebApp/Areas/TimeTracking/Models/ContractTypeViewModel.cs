﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ContractTypeViewModel
    {
        public long Id { get; set; }

        [LocalizedStringLength(255)]
        [LocalizedStringRequired]
        [DisplayNameLocalized(nameof(ContractTypeResource.Name), typeof(ContractTypeResource))]
        public LocalizedString Name { get; set; }

        [LocalizedStringLength(512)]
        [DisplayNameLocalized(nameof(ContractTypeResource.Description), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-big")]
        public LocalizedString Description { get; set; }

        [DisplayNameLocalized(nameof(ContractTypeResource.EmploymentType), nameof(ContractTypeResource.EmploymentTypeShort), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-small")]
        public EmploymentType? EmploymentType { get; set; }

        [DisplayNameLocalized(nameof(ContractTypeResource.IsActive), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-normal")]
        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayNameLocalized(nameof(ContractTypeResource.ActiveDirectoryCode), nameof(ContractTypeResource.ActiveDirectoryCodeShort), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-normal")]
        public string ActiveDirectoryCode { get; set; }

        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<TimeTrackingCompensationCalculators>))]
        [DisplayNameLocalized(nameof(ContractTypeResource.TimeTrackingCompensationCalculator), typeof(ContractTypeResource))]
        [Visibility(VisibilityScope.Form)]
        public TimeTrackingCompensationCalculators? TimeTrackingCompensationCalculator { get; set; }

        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<BusinessTripCompensationCalculators>))]
        [DisplayNameLocalized(nameof(ContractTypeResource.BusinessTripCompensationCalculator), typeof(ContractTypeResource))]
        [Visibility(VisibilityScope.Form)]
        public BusinessTripCompensationCalculators? BusinessTripCompensationCalculator { get; set; }

        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<OverTimeTypes>))]
        [DisplayNameLocalized(nameof(ContractTypeResource.OverTimeTypes), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-large")]
        public OverTimeTypes AllowedOverTimeTypes { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ContractTypeResource.HasPaidVacation), nameof(ContractTypeResource.HasPaidVacationShort), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-normal")]
        public bool HasPaidVacation { get; set; }

        [ValuePicker(Type = typeof(AbsenceTypePickerController), DialogWidth = "900px")]
        [DisplayNameLocalized(nameof(ContractTypeResource.AbsenceTypes), typeof(ContractTypeResource))]
        [Render(GridCssClass = "column-width-large")]
        [NonSortable]
        public long[] AbsenceTypeIds { get; set; }

        [DisplayNameLocalized(nameof(ContractTypeResource.DailyWorkingLimit), typeof(ContractTypeResource))]
        [Range(0, 24)]
        [Visibility(VisibilityScope.Form)]
        public decimal? DailyWorkingLimit { get; set; }

        [DisplayNameLocalized(nameof(ContractTypeResource.AutomaticOvertimeBankCalculation), typeof(ContractTypeResource))]
        [Visibility(VisibilityScope.Form)]
        public bool UseAutomaticOvertimeBankCalculation { get; set; }

        [DisplayNameLocalized(nameof(ContractTypeResource.MinimunOvertimeBankBalance), typeof(ContractTypeResource))]
        [Visibility(VisibilityScope.Form)]
        public decimal MinimunOvertimeBankBalance { get; set; }

        [DisplayNameLocalized(nameof(ContractTypeResource.ShouldNotifyOnAbsenceRemoval), typeof(ContractTypeResource))]
        [Visibility(VisibilityScope.Form)]
        public bool ShouldNotifyOnAbsenceRemoval { get; set; }

        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<SettlementSectionsFeatureType>))]
        [DisplayNameLocalized(nameof(ContractTypeResource.SettlementsConfiguration), typeof(ContractTypeResource))]
        public SettlementSectionsFeatureType SettlementSectionsFeatures { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContractTypeResource.NotifyInvoiceAmount), typeof(ContractTypeResource))]
        public bool NotifyInvoiceAmount { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContractTypeResource.NotifyMissingBusinessTripSettlement), typeof(ContractTypeResource))]
        public bool NotifyMissingBusinessTripSettlement { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContractTypeResource.UseDailySettlementMeals), typeof(ContractTypeResource))]
        public bool UseDailySettlementMeals { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContractTypeResource.HasToSpecifyDestinationType), typeof(ContractTypeResource))]
        public bool HasToSpecifyDestinationType { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContractTypeResource.IsTaxDeductibleCostEnabled), typeof(ContractTypeResource))]
        public bool IsTaxDeductibleCostEnabled { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}