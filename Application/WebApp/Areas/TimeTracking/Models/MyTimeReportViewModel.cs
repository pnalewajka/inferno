﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportViewModel : IValidatableObject
    {
        public long TimeReportId { get; set; }

        public TimeReportStatus Status { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFullName { get; set; }

        public bool IsMyTimeReport { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }

        public IList<MyTimeReportDayDescriptionViewModel> Days { get; set; }

        public IList<MyTimeReportWeekDescriptionViewModel> Weeks { get; set; }

        public IList<MyTimeReportProjectViewModel> Projects { get; set; }

        public IList<ClockCardDailyEntryViewModel> ClockCardDailyEntries { get; set; }

        public bool IsTimeReportAvailablePreviousMonth { get; set; }

        public bool IsReadOnly { get; set; }

        public bool IsClockCardRequired { get; set; }

        public bool IsSubmitRequest { get; set; }

        public IList<TimeTrackingImportSourceViewModel> ImportSources { get; set; }

        public IList<OvertimeApprovalViewModel> OvertimeApprovals { get; set; }

        public CommandButtonsViewModel ToolbarButtons { get; set; }

        public bool CanBeReopened { get; set; }

        public decimal ContractedHours { get; set; }

        public string TimeZoneDisplayName { get; set; }

        public int TimeZoneOffsetMinutes { get; set; }

        public OverTimeTypes AllowedOvertime { get; set; }

        public MyTimeReportViewModel()
        {
            Days = new List<MyTimeReportDayDescriptionViewModel>();
            Weeks = new List<MyTimeReportWeekDescriptionViewModel>();
            Projects = new List<MyTimeReportProjectViewModel>();
            ImportSources = new List<TimeTrackingImportSourceViewModel>();
            OvertimeApprovals = new List<OvertimeApprovalViewModel>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Projects.Any(p => p.Tasks.Any(t => t.Name == null || t.Name.Trim() == string.Empty)))
            {
                yield return new ValidationResult(TimeReportResources.TaskNameCantBeEmpty);
            }
        }
    }
}