﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceParametersDtoToVacationBalanceParametersViewModelMapping : ClassMapping<VacationBalanceParametersDto, VacationBalanceParametersViewModel>
    {
        public VacationBalanceParametersDtoToVacationBalanceParametersViewModelMapping()
        {
            Mapping = d => new VacationBalanceParametersViewModel
            {
                EffectiveOn = d.EffectiveOn,
                EmployeeScope = d.EmployeeScope,
                ContractTypeIds = d.ContractTypeIds
            };
        }
    }
}
