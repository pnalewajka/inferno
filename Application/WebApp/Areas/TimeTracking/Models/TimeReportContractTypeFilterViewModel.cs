﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Filters.TimeReportContractTypeFilter")]
    [DescriptionLocalized("EmployeeContractTypeFilterDialogTitle", typeof(EmployeeResources))]
    public class TimeReportContractTypeFilterViewModel
    {
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeContractTypeLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [Render(Size = Size.Large)]
        public long[] ContractTypeIds { get; set; }

        public override string ToString()
        {
            return EmployeeResources.EmployeeContractTypeLabel;
        }
    }
}