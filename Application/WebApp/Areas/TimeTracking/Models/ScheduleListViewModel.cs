﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ScheduleListViewModel
    {
        public ScheduleListViewModel()
        {
            Employees = new List<EmployeeScheduleListViewModel>();
        }

        [DisplayFormat(DataFormatString = "{0:dd/MMMM/yyyy)", ApplyFormatInEditMode = true)]
        public DateTime PeriodStartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MMMM/yyyy)", ApplyFormatInEditMode = true)]
        public DateTime PeriodEndDate { get; set; }

        public DateTime Date { get; set; }

        public bool IsHoliday { get; set; }

        public DateTime CurrentDate { get; set; }

        public bool IsWeekend
        {
            get
            {
                var dayOfWeek = Date.DayOfWeek;

                return dayOfWeek == DayOfWeek.Sunday || dayOfWeek == DayOfWeek.Saturday;
            }
        }

        public bool IsLastDayOfWeek => Date.DayOfWeek == DayOfWeek.Sunday;

        public IList<EmployeeScheduleListViewModel> Employees { get; set; }
    }
}
