using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrakingProjectSummaryParametersDtoToTimeTrakingProjectSummaryParametersViewModelMapping : ClassMapping<TimeTrakingProjectSummaryParametersDto, TimeTrakingProjectSummaryParametersViewModel>
    {
        public TimeTrakingProjectSummaryParametersDtoToTimeTrakingProjectSummaryParametersViewModelMapping()
        {
            Mapping = dto => new TimeTrakingProjectSummaryParametersViewModel
            {
                ProjectId = dto.ProjectId,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate
            };
        }
    }
}