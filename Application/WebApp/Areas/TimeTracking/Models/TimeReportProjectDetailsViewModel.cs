﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectDetailsViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long EmployeeId { get; set; }

        [DisplayNameLocalized(nameof(ProjectDashboardResources.LastNameHeaderText), typeof(ProjectDashboardResources))]
        public string EmployeeLastName { get; set; }

        [DisplayNameLocalized(nameof(ProjectDashboardResources.FirstNameHeaderText), typeof(ProjectDashboardResources))]
        public string EmployeeFirstName { get; set; }

        [DisplayNameLocalized(nameof(ProjectDashboardResources.OrgUnitHeaderText), typeof(ProjectDashboardResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long OrgUnitId { get; set; }

        [DisplayNameLocalized(nameof(ProjectDashboardResources.ProjectHeaderText), typeof(ProjectDashboardResources))]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public long ProjectId { get; set; }

        [DisplayNameLocalized(nameof(ProjectDashboardResources.StatusHeaderText), typeof(ProjectDashboardResources))]
        public TimeReportStatus TimeReportStatus { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectDashboardResources.OvertimeHoursHeaderText), typeof(ProjectDashboardResources))]
        public decimal OvertimeTotals { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectDashboardResources.OvertimeNormalHoursHeaderText), typeof(ProjectDashboardResources))]
        public decimal OvertimeNormalHours { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectDashboardResources.OvertimeLateNightHoursHeaderText), typeof(ProjectDashboardResources))]
        public decimal OvertimeLateNightHours { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectDashboardResources.OvertimeWeekendHoursHeaderText), typeof(ProjectDashboardResources))]
        public decimal OvertimeWeekendHours { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectDashboardResources.OvertimeHolidayHoursHeaderText), typeof(ProjectDashboardResources))]
        public decimal OvertimeHolidayHours { get; set; }

        [NonSortable]
        [DisplayNameLocalized(nameof(ProjectDashboardResources.TotalHoursHeaderText), typeof(ProjectDashboardResources))]
        public decimal TotalHours { get; set; }

        [Visibility(VisibilityScope.None)]
        public long TimeReportId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? RequestId { get; set; }

        [Visibility(VisibilityScope.None)]
        public RequestStatus? RequestStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsRequestAvailable { get; set; }
    }
}
