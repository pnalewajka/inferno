﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class EmployeeScheduleListViewModel
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<AbsenceDayDto> AbsenceDays { get; set; }

        public ICollection<DateTime> HomeOfficeDays { get; set; }

        public ICollection<DateTime> HolidayDays { get; set; }

        public ICollection<DateTime> NotContractedDays { get; set; }

        public IList<DailyContractedHoursDto> ContractedWorkingHours { get; set; }
    }
}
