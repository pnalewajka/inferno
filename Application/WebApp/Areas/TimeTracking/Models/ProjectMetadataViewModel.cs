﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ProjectMetadataViewModel
    {
        public long ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string Apn { get; set; }

        public OverTimeTypes AllowedProjectOvertime { get; set; }

        public string KupferwerkProjectId { get; set; }

        public DateTime? ReportingStartsOn { get; set; }

        public IEnumerable<OvertimeApprovalViewModel> OvertimeApprovals { get; set; }

        public IEnumerable<MyTimeReportProjectAttributeViewModel> ProjectAttributes { get; set; }
    }
}