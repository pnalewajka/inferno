﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeRequestViewModel
    {
        [DisplayNameLocalized(nameof(OvertimeRequestResource.Project), typeof(OvertimeRequestResource))]
        [Required]
        [Order(0)]
        [ValuePicker(Type = typeof(TimeTrackingProjectPickerController), OnResolveUrlJavaScript = "url=url + '&mode=2'")]
        [UpdatesApprovers]
        public long ProjectId { get; set; }

        [DisplayNameLocalized(nameof(OvertimeRequestResource.DateFrom), typeof(OvertimeRequestResource))]
        [Order(2)]
        [Required]
        [UpdatesApprovers]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "date-column")]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(OvertimeRequestResource.DateTo), typeof(OvertimeRequestResource))]
        [Order(3)]
        [Required]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "date-column")]
        [UpdatesApprovers]
        public DateTime To { get; set; }

        [DisplayNameLocalized(nameof(OvertimeRequestResource.HourLimit), typeof(OvertimeRequestResource))]
        [Order(5)]
        [Required]
        [Range(0d, 365d, ErrorMessageResourceType = typeof(OvertimeRequestResource),
            ErrorMessageResourceName = "HourLimitMustBeGreaterThanZero_ErrorMessage")]
        public decimal HourLimit { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(OvertimeRequestResource.Comment), typeof(OvertimeRequestResource))]
        [Order(6)]
        public string Comment { get; set; }
    }
}
