﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectAttributeViewModel
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [ValuePicker(Type = typeof(TimeReportProjectAttributeValuePickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name = " + nameof(Id) + "]').val()")]
        public long? DefaultValueId { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
