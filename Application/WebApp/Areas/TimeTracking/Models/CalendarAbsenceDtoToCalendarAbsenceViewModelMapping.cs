﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class CalendarAbsenceDtoToCalendarAbsenceViewModelMapping : ClassMapping<CalendarAbsenceDto, CalendarAbsenceViewModel>
    {
        public CalendarAbsenceDtoToCalendarAbsenceViewModelMapping()
        {
            Mapping = d => new CalendarAbsenceViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                OfficeGroupEmail = d.OfficeGroupEmail,
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                EmployeesIds = d.EmployeesIds
            };
        }
    }
}
