﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsencesKbrDataSourceParametersDtoToAbsencesKbrDataSourceParametersViewModelMapping : ClassMapping<AbsencesKbrDataSourceParametersDto, AbsencesKbrDataSourceParametersViewModel>
    {
        public AbsencesKbrDataSourceParametersDtoToAbsencesKbrDataSourceParametersViewModelMapping()
        {
            Mapping = d => new AbsencesKbrDataSourceParametersViewModel
            {
                AbsencesFrom = d.AbsencesFrom,
                AbsencesTo = d.AbsencesTo,
                ApprovedFrom = d.ApprovedTo,
                ApprovedTo = d.ApprovedTo,
                IsVacation = d.IsVacation,
                AbsenceTypeIds = d.AbsenceTypeIds,
                EmploymentType = d.EmploymentType,
                ContractTypeIds = d.ContractTypeIds,
                CompanyIds = d.CompanyIds,
                OrgUnitIds = d.OrgUnitIds,
                LocationIds = d.LocationIds
            };
        }
    }
}
