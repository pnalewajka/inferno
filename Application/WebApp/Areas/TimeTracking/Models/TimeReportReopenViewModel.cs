﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.TimeReportReopen")]
    public class TimeReportReopenViewModel
    {
        [HiddenInput]
        public long EmployeeId { get; set; }

        [HiddenInput]
        public int Year { get; set; }

        [HiddenInput]
        [Range(1, 12)]
        public int Month { get; set; }

        [ValuePicker(Type = typeof(TimeTrackingProjectPickerController), OnResolveUrlJavaScript = "url=url +'&mode=1&employee-id=' + $('[name = " + nameof(EmployeeId) + "]').val() + '&year=' + $('[name = " + nameof(Year) + "]').val() + '&month=' + $('[name = " + nameof(Month) + "]').val()")]
        [DisplayNameLocalized(nameof(TimeReportResources.ProjectsToReopen), typeof(TimeReportResources))]
        public long[] ProjectsToReopen { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ReopenReason), typeof(TimeReportResources))]
        [StringLength(255)]
        [Multiline(5)]
        public string Reason { get; set; }

        public TimeReportReopenViewModel()
        {
            ProjectsToReopen = new long[0];
        }
    }
}