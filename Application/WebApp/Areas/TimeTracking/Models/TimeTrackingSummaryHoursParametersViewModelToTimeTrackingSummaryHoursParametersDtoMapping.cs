﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingSummaryHoursParametersViewModelToTimeTrackingSummaryHoursParametersDtoMapping : ClassMapping<TimeTrackingSummaryHoursParametersViewModel, TimeTrackingSummaryHoursParametersDto>
    {
        public TimeTrackingSummaryHoursParametersViewModelToTimeTrackingSummaryHoursParametersDtoMapping()
        {
            Mapping = v => new TimeTrackingSummaryHoursParametersDto
            {
                FromYear = v.FromYear,
                FromMonth = v.FromMonth,
                ToYear = v.ToYear,
                ToMonth = v.ToMonth,
                Status = v.Status,
                EmploymentType = v.EmploymentType,
                EmployeeIds = v.EmployeeIds,
                EmployeeOrgUnitIds = v.EmployeeOrgUnitIds,
                LocationIds = v.LocationIds,
                ContractTypeIds = v.ContractTypeIds
            };
        }
    }
}
