﻿using System;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ClockCardDailyEntryDtoToClockCardDailyEntryViewModelMapping : ClassMapping<ClockCardDailyEntryDto, ClockCardDailyEntryViewModel>
    {
        public ClockCardDailyEntryDtoToClockCardDailyEntryViewModelMapping()
        {
            Mapping = d => new ClockCardDailyEntryViewModel
            {
                Id = d.Id,
                TimeReportId = d.TimeReportId,
                Day = DateTime.SpecifyKind(d.Day, DateTimeKind.Utc),
                InTime = d.InTime.HasValue ? d.InTime.Value.ToLocalTime() : (DateTime?)null,
                OutTime = d.OutTime.HasValue ? d.OutTime.Value.ToLocalTime() : (DateTime?)null,
                BreakDuration = d.BreakDuration
            };
        }
    }
}
