﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Accounts.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceRequestViewModel : IRequestAdditionalInformation
    {
        [Required]
        [Order(1)]
        [ValuePicker(Type = typeof(FilteredUserPickerController), OnResolveUrlJavaScript = "url=url+'&filter-key=" + nameof(FilteredUserPickerFilterKeys.UsersICanAdministrateAsHr) + "'")]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.AffectedUser), typeof(AbsenceRequestResources))]
        public long AffectedUserId { get; set; }

        [Required]
        [Order(3)]
        [UpdatesApprovers(ServerResultClientHandler = "AbsenceRequest.handleUpdateApprovers")]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.AbsenceType), typeof(AbsenceRequestResources))]

        [ValuePicker(Type = typeof(AbsenceTypePickerController), OnResolveUrlJavaScript = "AbsenceRequest.resolveAbsenceTypeUrl", DialogWidth ="900px")]
        public long AbsenceTypeId { get; set; }

        [Order(4)]
        [CustomControl(TemplatePath = "Areas.TimeTracking.Views.AbsenceRequest._AdditionalInformation")]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.AdditionalInformation), typeof(AbsenceRequestResources))]
        public string AdditionalInformation { get; set; }

        [Order(5)]
        [Required]
        [UpdatesApprovers(ServerResultClientHandler = "AbsenceRequest.handleUpdateApprovers")]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.From), typeof(AbsenceRequestResources))]
        public DateTime From { get; set; }

        [Order(6)]
        [Required]
        [UpdatesApprovers(ServerResultClientHandler = "AbsenceRequest.handleUpdateApprovers")]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.To), typeof(AbsenceRequestResources))]
        public DateTime To { get; set; }

        [Order(7)]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.ReportedHours), typeof(AbsenceRequestResources))]
        public decimal Hours { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal MaxAllowedHoursToReport { get; set; }

        [Order(7)]
        [ValuePicker(Type = typeof(UserPickerController), OnResolveUrlJavaScript = "url=url+'&is-actively-working=true'")]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.Substitute), typeof(AbsenceRequestResources))]
        public long? SubstituteUserId { get; set; }

        [Order(8)]
        [StringLength(255)]
        [Multiline(4)]
        [DisplayNameLocalized(nameof(AbsenceRequestResources.Comment), typeof(AbsenceRequestResources))]
        [Hint("CommentRequiredForAbsenceInPast", typeof(AbsenceRequestResources))]
        public string Comment { get; set; }
    }
}