using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportViewModelToMyTimeReportDtoMapping : ClassMapping<MyTimeReportViewModel, MyTimeReportDto>
    {
        public MyTimeReportViewModelToMyTimeReportDtoMapping(
            IClassMapping<ClockCardDailyEntryViewModel, ClockCardDailyEntryDto> clockCardDailyEntryViewModelToClockCardDailyEntryDtoMapping)
        {
            Mapping = v => new MyTimeReportDto
            {
                TimeReportId = v.TimeReportId,
                EmployeeId = v.EmployeeId,
                Year = v.Year,
                Month = v.Month,
                TimeZoneOffsetMinutes = v.TimeZoneOffsetMinutes,
                TimeZoneDisplayName = v.TimeZoneDisplayName,
                Rows = v.Projects.SelectMany(p => p.Tasks, (timeReportProjectViewModel, timeReportTaskViewModel) => new MyTimeReportRowDto
                {
                    Id = timeReportTaskViewModel.Id.GetValueOrDefault(),
                    ProjectId = timeReportProjectViewModel.ProjectId,
                    ProjectName = timeReportProjectViewModel.ProjectName,
                    TaskName = TimeReportRowHelper.RemoveNotAllowedCharacters(timeReportTaskViewModel.Name),
                    OvertimeVariant = timeReportTaskViewModel.OvertimeVariant,
                    ImportSourceId = timeReportTaskViewModel.ImportSourceId,
                    ImportedTaskCode = timeReportTaskViewModel.ImportedTaskCode,
                    ImportedTaskType = timeReportTaskViewModel.ImportedTaskType,
                    AbsenceTypeId = timeReportTaskViewModel.AbsenceTypeId,
                    Days = timeReportTaskViewModel.Days.Select(d => new MyTimeReportDailyEntryDto
                    {
                        Day = DateTime.SpecifyKind(d.Day.Date, DateTimeKind.Utc),
                        Hours = d.Hours
                    }),
                    SelectedAttributesValueIds = timeReportTaskViewModel.SelectedAttributesValueIds.Where(a => a.HasValue).Select(a => a.Value).ToArray(),
                }).ToList(),
                ClockCardDailyEntries = v.ClockCardDailyEntries != null
                    ? v.ClockCardDailyEntries.Select(clockCardDailyEntryViewModelToClockCardDailyEntryDtoMapping.CreateFromSource).ToList()
                    : new List<ClockCardDailyEntryDto>()
            };
        }
    }
}
