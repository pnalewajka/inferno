﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.EmployeesExceedingWorkingLimitParametres")]
    public class EmployeesExceedingWorkingLimitParametersViewModel
    {
        [Required]
        [Range(2000, 3000)]
        [DisplayNameLocalized(nameof(ReportResources.Year), typeof(ReportResources))]
        public int Year { get; set; }

        [Required]
        [Range(1, 12)]
        [DisplayNameLocalized(nameof(ReportResources.Month), typeof(ReportResources))]
        public int Month { get; set; }

        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long[] OrgUnitIds { get; set; }

        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.LocationText), typeof(ReportResources))]
        public long[] LocationIds { get; set; }

        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.ContractTypeText), typeof(ReportResources))]
        public long[] ContractTypeIds { get; set; }
    }
}
