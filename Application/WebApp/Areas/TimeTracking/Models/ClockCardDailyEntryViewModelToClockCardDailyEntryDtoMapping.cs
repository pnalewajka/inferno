﻿using System;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ClockCardDailyEntryViewModelToClockCardDailyEntryDtoMapping : ClassMapping<ClockCardDailyEntryViewModel, ClockCardDailyEntryDto>
    {
        public ClockCardDailyEntryViewModelToClockCardDailyEntryDtoMapping()
        {
            Mapping = v => new ClockCardDailyEntryDto
            {
                Id = v.Id,
                TimeReportId = v.TimeReportId,
                Day = v.Day.Date,
                InTime = v.InTime.HasValue ? new DateTime(v.Day.Date.Year, v.Day.Date.Month, v.Day.Date.Day, v.InTime.Value.Hour, v.InTime.Value.Minute, 0) : (DateTime?)null,
                OutTime = v.OutTime.HasValue ? new DateTime(v.Day.Date.Year, v.Day.Date.Month, v.Day.Date.Day, v.OutTime.Value.Hour, v.OutTime.Value.Minute, 0) : (DateTime?)null,
                BreakDuration = v.BreakDuration
            };
        }
    }
}
