﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ProjectStatusViewModel
    {
        public long  Id { get; set; }

        public string Name { get; set; }

        public TimeReportStatus Status { get; set; }
    }
}