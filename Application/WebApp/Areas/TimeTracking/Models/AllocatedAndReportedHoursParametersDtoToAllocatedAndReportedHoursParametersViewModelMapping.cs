﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AllocatedAndReportedHoursParametersDtoToAllocatedAndReportedHoursParametersViewModelMapping : ClassMapping<AllocatedAndReportedHoursParametersDto, AllocatedAndReportedHoursParametersViewModel>
    {
        public AllocatedAndReportedHoursParametersDtoToAllocatedAndReportedHoursParametersViewModelMapping()
        {
            Mapping = d => new AllocatedAndReportedHoursParametersViewModel
            {
                ProfileIds = d.ProfileIds,
                EmployeeOrgUnitIds = d.EmployeeOrgUnitIds,
                LocationIds = d.LocationIds,
                LineManagerIds = d.LineManagerIds,
                JobMatrixIds = d.JobMatrixIds,
                PlaceOfWork = d.PlaceOfWork,
                UtilizationCategoryIds = d.UtilizationCategoryIds,
                BillabilityFiltering = d.BillabilityFiltering
            };
        }
    }
}
