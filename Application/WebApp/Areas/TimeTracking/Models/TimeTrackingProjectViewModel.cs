﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingProjectViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ProjectName), typeof(TimeReportResources))]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ClientName), typeof(TimeReportResources))]
        public string ClientName { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ProjectManagerName), typeof(TimeReportResources))]
        public string ProjectManagerName { get; set; }

        [Ellipsis(MaxStringLength = 13)]
        [DisplayNameLocalized(nameof(TimeReportResources.PetsCode), typeof(TimeReportResources))]
        public string PetsCode { get; set; }

        [Visibility(VisibilityScope.Form)]
        public string Approvers { get; set; }

        [Visibility(VisibilityScope.None)]
        public string JiraCode { get; set; }

        [Visibility(VisibilityScope.None)]
        public MyTimeReportProjectAttributeViewModel[] Attributes { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
