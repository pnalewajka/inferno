﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel : ClassMapping<MyTimeReportDayDescriptionDto, MyTimeReportDayDescriptionViewModel>
    {
        public MyTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel()
        {
            Mapping = dto => new MyTimeReportDayDescriptionViewModel
            {
                Date = dto.Date,
                ContractedHours = dto.ContractedHours,
                DailyWorkingLimit = dto.DailyWorkingLimit,
                HolidayName = dto.HolidayName,
                IsHoliday = dto.IsHoliday,
            };
        }
    }
}