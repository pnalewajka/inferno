﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ReopenTimeTrackingReportRequestDtoToReopenTimeTrackingReportRequestViewModelMapping : ClassMapping<ReopenTimeTrackingReportRequestDto, ReopenTimeTrackingReportRequestViewModel>
    {
        public ReopenTimeTrackingReportRequestDtoToReopenTimeTrackingReportRequestViewModelMapping()
        {
            Mapping = d => new ReopenTimeTrackingReportRequestViewModel
            {
                Month = d.Month,
                Year = d.Year,
                ProjectIds = d.ProjectIds,
                Reason = d.Reason
            };
        }
    }
}
