﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportTaskViewModel
    {
        public MyTimeReportTaskViewModel()
        {
            Days = new List<MyTimeReportDailyEntryViewModel>();
            SelectedAttributesValueIds = new List<long?>();
        }

        public long? Id { get; set; }
        public string Name { get; set; }
        public bool IsReadOnly { get; set; }
        public TimeReportStatus Status { get; set; }
        public HourlyRateType OvertimeVariant { get; set; }
        public long? AbsenceTypeId { get; set; }
        public long? ImportSourceId { get; set; }
        public string ImportedTaskCode { get; set; }

        [MaxLength(128)]
        public string ImportedTaskType { get; set; }

        public IList<long?> SelectedAttributesValueIds { get; set; }

        public IList<MyTimeReportDailyEntryViewModel> Days { get; set; }
    }
}