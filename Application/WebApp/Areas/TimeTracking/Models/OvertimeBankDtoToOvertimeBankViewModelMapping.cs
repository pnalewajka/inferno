﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeBankDtoToOvertimeBankViewModelMapping : ClassMapping<OvertimeBankDto, OvertimeBankViewModel>
    {
        public OvertimeBankDtoToOvertimeBankViewModelMapping()
        {
            Mapping = d => new OvertimeBankViewModel
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                EmployeeFirstName = d.EmployeeFirstName,
                EmployeeLastName = d.EmployeeLastName,
                RequestId = d.RequestId,
                OvertimeChange = d.OvertimeChange,
                EffectiveOn = d.EffectiveOn,
                OvertimeBalance = d.OvertimeBalance,
                Comment = d.Comment,
                IsAutomatic = d.IsAutomatic,
                Timestamp = d.Timestamp
            };
        }
    }
}
