﻿using System;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ClockCardDailyEntryViewModel
    {
        public long Id { get; set; }

        public long TimeReportId { get; set; }

        public DateTime Day { get; set; }

        public DateTime? InTime { get; set; }

        public DateTime? OutTime { get; set; }

        public TimeSpan? BreakDuration { get; set; }
    }
}
