﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.ReportDataSources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.VacationBalances")]
    public class VacationBalanceParametersViewModel
    {
        [DisplayNameLocalized(nameof(VacationBalanceResources.date), typeof(VacationBalanceResources))]
        [Required]
        [Order(0)]
        public DateTime EffectiveOn { get; set; }

        [DisplayNameLocalized(nameof(VacationBalanceResources.ContractType), typeof(VacationBalanceResources))]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [Order(1)]
        public long[] ContractTypeIds { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(VacationBalanceResources.EmployeeScope), typeof(VacationBalanceResources))]
        [Render(Size = Size.Small)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.NotAvailable, ResourceType = typeof(VacationBalanceResources), ItemProvider = typeof(VacationBalanceReportScopeItemProvider))]
        [Order(2)]
        public ReportScopeEnum EmployeeScope { get; set; }
    }
}
