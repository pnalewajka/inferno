﻿using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportDailyEntryViewModel
    {        
        public DateTime Day { get; set; }

        [Range(0, 24, ErrorMessageResourceName = "TaskHoursInvalidValueMessage", ErrorMessageResourceType = typeof(TimeReportResources))]
        public decimal Hours { get; set; }

        public bool IsReadOnly { get; set; }
    }
}