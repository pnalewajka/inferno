﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingImportSourceDtoToTimeTrackingImportSourceViewModelMapping : ClassMapping<TimeTrackingImportSourceDto, TimeTrackingImportSourceViewModel>
    {
        public TimeTrackingImportSourceDtoToTimeTrackingImportSourceViewModelMapping()
        {
            Mapping =
                d =>
                    new TimeTrackingImportSourceViewModel
                    {
                        Id = d.Id,
                        Code = d.Code,
                        Name = d.Name,
                        ImportStrategyIdentifier = d.ImportStrategyIdentifier,
                        Configuration = d.Configuration
                    };
        }
    }
}
