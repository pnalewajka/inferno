﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceImportViewModel : VacationBalanceViewModel
    {
        [DisplayNameLocalized(nameof(VacationBalanceResources.EmployeeEmail), typeof(VacationBalanceResources))]
        [Required]
        [Order(0)]
        public string EmployeeEmail { get; set; }
    }
}