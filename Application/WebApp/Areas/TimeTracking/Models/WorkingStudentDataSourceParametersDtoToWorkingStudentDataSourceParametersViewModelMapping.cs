﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class WorkingStudentDataSourceParametersDtoToWorkingStudentDataSourceParametersViewModelMapping : ClassMapping<WorkingStudentDataSourceParametersDto, WorkingStudentDataSourceParametersViewModel>
    {
        public WorkingStudentDataSourceParametersDtoToWorkingStudentDataSourceParametersViewModelMapping()
        {
            Mapping = d => new WorkingStudentDataSourceParametersViewModel
            {
                From = d.From,
                To = d.To,
                Status = d.Status,
                EmployeeIds = d.EmployeeIds,
                EmployeeOrgUnitIds = d.OrgUnitIds,
                LocationIds = d.LocationIds,
                ContractTypeIds = d.ContractTypeIds,
                MyEmployees = d.IsMyEmployees
            };
        }
    }
}
