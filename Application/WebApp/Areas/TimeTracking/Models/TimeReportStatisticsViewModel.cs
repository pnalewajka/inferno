﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.TimeReportStatistics")]
    public class TimeReportStatisticsViewModel
    {
        [DisplayNameLocalized(nameof(TimeReportResources.EmployeeFullNameLabel), typeof(TimeReportResources))]
        public string EmployeeFullName { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.YearLabel), typeof(TimeReportResources))]
        public int Year { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.MonthLabel), typeof(TimeReportResources))]
        public int Month { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ContractedHoursLabel), typeof(TimeReportResources))]
        public decimal ContractedHours { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ReportedHoursLabel), typeof(TimeReportResources))]
        public decimal ReportedHours { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.BillableHoursLabel), typeof(TimeReportResources))]
        public decimal BillableHours { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.NonBillableHoursLabel), typeof(TimeReportResources))]
        public decimal NonBillableHours { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.OvertimeHoursLabel), typeof(TimeReportResources))]
        public decimal OvertimeHours { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.TaxDeductHoursLabel), typeof(TimeReportResources))]
        public decimal TaxDeductHours { get; set; }

        [Visibility(VisibilityScope.None)]
        public Dictionary<HourlyRateType, decimal> HoursPerOvertimeType { get; set; }
    }
}