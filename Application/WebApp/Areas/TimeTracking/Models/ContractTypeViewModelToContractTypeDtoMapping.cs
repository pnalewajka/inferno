﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ContractTypeViewModelToContractTypeDtoMapping : ClassMapping<ContractTypeViewModel, ContractTypeDto>
    {
        public ContractTypeViewModelToContractTypeDtoMapping()
        {
            Mapping = v => new ContractTypeDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                EmploymentType = v.EmploymentType,
                IsActive = v.IsActive,
                DailyWorkingLimit = v.DailyWorkingLimit,
                ActiveDirectoryCode = v.ActiveDirectoryCode,
                TimeTrackingCompensationCalculator = v.TimeTrackingCompensationCalculator,
                BusinessTripCompensationCalculator = v.BusinessTripCompensationCalculator,
                AllowedOverTimeTypes = v.AllowedOverTimeTypes,
                HasPaidVacation = v.HasPaidVacation,
                UseAutomaticOvertimeBankCalculation = v.UseAutomaticOvertimeBankCalculation,
                MinimunOvertimeBankBalance = v.MinimunOvertimeBankBalance,
                AbsenceTypeIds = v.AbsenceTypeIds,
                ShouldNotifyOnAbsenceRemoval = v.ShouldNotifyOnAbsenceRemoval,
                SettlementSectionsFeatures = v.SettlementSectionsFeatures,
                NotifyInvoiceAmount = v.NotifyInvoiceAmount,
                NotifyMissingBusinessTripSettlement = v.NotifyMissingBusinessTripSettlement,
                UseDailySettlementMeals = v.UseDailySettlementMeals,
                HasToSpecifyDestinationType = v.HasToSpecifyDestinationType,
                IsTaxDeductibleCostEnabled = v.IsTaxDeductibleCostEnabled,
                Timestamp = v.Timestamp,
            };
        }
    }
}
