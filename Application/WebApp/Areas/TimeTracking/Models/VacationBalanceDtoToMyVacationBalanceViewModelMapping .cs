﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceDtoToMyVacationBalanceViewModelMapping : ClassMapping<VacationBalanceDto, MyVacationBalanceViewModel>
    {
        public VacationBalanceDtoToMyVacationBalanceViewModelMapping()
        {
            Mapping = d => new MyVacationBalanceViewModel
            {
                Id = d.Id,
                EffectiveOn = d.EffectiveOn,
                Operation = d.Operation,
                Comment = d.Comment,
                HourChange = d.HourChange,
                TotalHourBalance = d.TotalHourBalance
            };
        }
    }
}
