﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.TimeTrakingProjectSummaryDataSource")]
    public class TimeTrakingProjectSummaryParametersViewModel
    {
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.ProjectsText), typeof(ReportResources))]
        public long ProjectId { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime StartDate { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime EndDate { get; set; }

    }
}