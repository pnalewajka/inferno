﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsencesDataSourceParametersDtoToAbsencesDataSourceParametersViewModelMapping : ClassMapping<AbsencesDataSourceParametersDto, AbsencesDataSourceParametersViewModel>
    {
        public AbsencesDataSourceParametersDtoToAbsencesDataSourceParametersViewModelMapping()
        {
            Mapping = d => new AbsencesDataSourceParametersViewModel
            {
                ApprovedFrom = d.ApprovedFrom,
                ApprovedTo = d.ApprovedTo,
                IsVacation = d.IsVacation,
                AbsenceTypeIds = d.AbsenceTypeIds,
                EmploymentType = d.EmploymentType,
                ContractTypeIds = d.ContractTypeIds,
                CompanyIds = d.CompanyIds,
                OrgUnitIds = d.OrgUnitIds,
                LocationIds = d.LocationIds
            };
        }
    }
}
