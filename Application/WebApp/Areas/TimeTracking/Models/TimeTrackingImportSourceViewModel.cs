﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeTrackingImportSourceViewModel
    {
        public long Id { get; set; }

        [StringLength(255)]
        [Required]
        public string Code { get; set; }

        [Required]
        public LocalizedString Name { get; set; }

        [StringLength(255)]
        [Required]
        public string ImportStrategyIdentifier { get; set; }

        public string Configuration { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
