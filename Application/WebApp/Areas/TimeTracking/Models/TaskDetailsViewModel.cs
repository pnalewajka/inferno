﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TaskDetailsViewModel
    {
        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.TaskName), typeof(ProjectTimeReportApprovalRequestResources))]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.HoursType), typeof(ProjectTimeReportApprovalRequestResources))]
        public HourlyRateType OvertimeVariant { get; set; }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.Hours), typeof(ProjectTimeReportApprovalRequestResources))]
        public decimal Hours { get; set; }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.From), typeof(ProjectTimeReportApprovalRequestResources))]
        public DateTime From { get; set; }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.To), typeof(ProjectTimeReportApprovalRequestResources))]
        public DateTime To { get; set; }

        public bool IsBillable { get; set; }

        public string Attributes { get; set; }
    }
}