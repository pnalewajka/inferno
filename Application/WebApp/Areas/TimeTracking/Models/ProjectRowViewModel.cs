﻿using System.Collections.Generic;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ProjectRowViewModel
    {
        public MyTimeReportProjectViewModel Project { get; set; }
        public IList<MyTimeReportDayDescriptionViewModel> Days { get; set; }
    }
}