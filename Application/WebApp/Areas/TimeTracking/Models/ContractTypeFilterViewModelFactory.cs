﻿using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ContractTypeFilterViewModelFactory : IViewModelFactory<ContractTypeFilterViewModel>
    {
        private readonly ITimeService _timeService;

        public ContractTypeFilterViewModelFactory(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public ContractTypeFilterViewModel Create()
        {
            return new ContractTypeFilterViewModel
            {
                Date = _timeService.GetCurrentDate()
            };
        }
    }
}
