﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceDtoToVacationBalanceViewModelMapping : ClassMapping<VacationBalanceDto, VacationBalanceViewModel>
    {
        public VacationBalanceDtoToVacationBalanceViewModelMapping()
        {
            Mapping = d => new VacationBalanceViewModel
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                Login = d.EmployeeLogin,
                FirstName = d.EmployeeFirstName,
                LastName = d.EmployeeLastName,
                EffectiveOn = d.EffectiveOn,
                Operation = d.Operation,
                Comment = d.Comment,
                HourChange = d.HourChange,
                TotalHourBalance = d.TotalHourBalance,
                CanBeDeleted = d.RequestId == null,
                Timestamp = d.Timestamp
            };
        }

        public static string GetEmployeeFullName(VacationBalanceDto vacationBalance)
        {
            var result = $"{vacationBalance.EmployeeFirstName} {vacationBalance.EmployeeLastName}";

            if (!string.IsNullOrEmpty(vacationBalance.EmployeeLogin))
            {
                result += $" - {vacationBalance.EmployeeLogin}";
            }

            return result;
        }
    }
}
