﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class JiraIssueToTimeReportRowMapperDtoToJiraIssueToTimeReportRowMapperViewModelMapping : ClassMapping<JiraIssueToTimeReportRowMapperDto, JiraIssueToTimeReportRowMapperViewModel>
    {
        public JiraIssueToTimeReportRowMapperDtoToJiraIssueToTimeReportRowMapperViewModelMapping()
        {
            Mapping = d => new JiraIssueToTimeReportRowMapperViewModel
            {
                Id = d.Id,
                Name = d.Name,
            };
        }
    }
}
