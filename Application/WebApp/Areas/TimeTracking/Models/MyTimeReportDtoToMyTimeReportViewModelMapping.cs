﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportDtoToMyTimeReportViewModelMapping : ClassMapping<MyTimeReportDto, MyTimeReportViewModel>
    {
        public MyTimeReportDtoToMyTimeReportViewModelMapping(
            IClassMapping<ClockCardDailyEntryDto, ClockCardDailyEntryViewModel> clockCardDailyEntryDtoToClockCardDailyEntryViewModelMapping,
            IClassMapping<TimeReportProjectAttributeDto, MyTimeReportProjectAttributeViewModel> attributeDtoToAttributeViewModelMapping,
            IClassMapping<MyTimeReportDayDescriptionDto, MyTimeReportDayDescriptionViewModel> myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel,
            IEmploymentPeriodService employmentPeriodService)
        {
            Mapping = dto => new MyTimeReportViewModel
            {
                TimeReportId = dto.TimeReportId,
                Status = dto.Status,
                EmployeeId = dto.EmployeeId,
                EmployeeFullName = dto.EmployeeFullName,
                Year = dto.Year,
                Month = dto.Month,
                IsReadOnly = dto.IsReadOnly,
                IsTimeReportAvailablePreviousMonth = dto.IsTimeReportAvailablePreviousMonth,
                TimeZoneOffsetMinutes = dto.TimeZoneOffsetMinutes,
                TimeZoneDisplayName = dto.TimeZoneDisplayName,
                AllowedOvertime = dto.AllowedOvertime,
                Projects = dto.Rows.GroupBy(r => r.ProjectId).Select(g => new MyTimeReportProjectViewModel
                {
                    ProjectId = g.Key,
                    ProjectName = g.First().ProjectName,
                    ReportingStartsOn = g.First().ReportingStartsOn,
                    Apn = g.First().Apn,
                    KupferwerkProjectId = g.First().KupferwerkProjectId,
                    PetsCode = g.First().PetsCode,
                    IsReadOnly = g.All(t => t.IsReadOnly),
                    AllowedProjectOvertime = g.First().AllowedProjectOvertime,
                    Tasks = g.Select(r => new MyTimeReportTaskViewModel
                    {
                        Id = r.Id,
                        Name = r.TaskName,
                        IsReadOnly = r.IsReadOnly,
                        Status = r.Status,
                        OvertimeVariant = r.OvertimeVariant,
                        ImportSourceId = r.ImportSourceId,
                        ImportedTaskCode = r.ImportedTaskCode,
                        ImportedTaskType = r.ImportedTaskType,
                        AbsenceTypeId = r.AbsenceTypeId,
                        Days = r.Days.Select(d => new MyTimeReportDailyEntryViewModel
                        {
                            Day = DateTime.SpecifyKind(d.Day, DateTimeKind.Utc),
                            Hours = d.Hours,
                            IsReadOnly = d.IsReadOnly,
                        }).ToList(),
                        SelectedAttributesValueIds = g.First().Attributes
                            .Select(pa => !pa.PossibleValues.Any(a => r.SelectedAttributesValueIds.Contains(a.Id)) 
                            ? null 
                            : (long?)pa.PossibleValues.First(a => r.SelectedAttributesValueIds.Contains(a.Id)).Id).ToArray()
                    }).ToList(),
                    Attributes = g.First().Attributes.Select(attributeDtoToAttributeViewModelMapping.CreateFromSource).ToList(),
                }).ToList(),
                ClockCardDailyEntries = dto.ClockCardDailyEntries
                    .Select(clockCardDailyEntryDtoToClockCardDailyEntryViewModelMapping.CreateFromSource).ToList(),
                Days = dto.Days.Select(myTimeReportDayDescriptionDtoToMyTimeReportDayDescriptionViewModel.CreateFromSource).ToList(),
                Weeks = dto.Weeks.Select(w => new MyTimeReportWeekDescriptionViewModel
                {
                    WeekDays = w.WeekDays,
                    WeekNumber = w.WeekNumber
                }).ToList(),
                ContractedHours = employmentPeriodService.GetContractedHours(dto.EmployeeId, dto.Year, dto.Month, null)
                .Sum(d => d.Hours)
            };
        }
    }
}