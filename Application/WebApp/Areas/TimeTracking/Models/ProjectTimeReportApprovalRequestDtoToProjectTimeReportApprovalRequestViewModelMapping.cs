﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.TimeTracking.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class ProjectTimeReportApprovalRequestDtoToProjectTimeReportApprovalRequestViewModelMapping : ClassMapping<ProjectTimeReportApprovalRequestDto, ProjectTimeReportApprovalRequestViewModel>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;

        public ProjectTimeReportApprovalRequestDtoToProjectTimeReportApprovalRequestViewModelMapping(IPrincipalProvider principalProvider, ISystemParameterService systemParameterService)
        {
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;

            Mapping = dto =>
                new ProjectTimeReportApprovalRequestViewModel
                {
                    EmployeeId = dto.EmployeeId,
                    ProjectId = dto.ProjectId,
                    From = dto.From,
                    To = dto.To,
                    TasksDetails = new TasksDetailsViewModel { 
                        Tasks = dto.Tasks.Select(t => new TaskDetailsViewModel
                        {
                            Name = t.TaskName,
                            OvertimeVariant = t.OvertimeVariant,
                            Hours = t.Hours,
                            From = t.From,
                            To = t.To,
                            IsBillable = t.IsBillable,
                            Attributes = string.Join(", ", t.Attributes),
                        }).ToList(),
                        OriginalReportUrl = GetTimeReportUrl(dto)
                    }
                };
        }

        private string GetTimeReportUrl(ProjectTimeReportApprovalRequestDto dto)
        {
            var areaName = RoutingHelper.GetAreaName(typeof(MyTimeReportController));

            if (dto.EmployeeId == _principalProvider.Current.EmployeeId)
            {
                var controllerName = RoutingHelper.GetControllerName<MyTimeReportController>();

                var webUrl = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
                var action =
                    $"{webUrl}{areaName}/{controllerName}/{nameof(MyTimeReportController.Index)}?year={dto.From.Year}&month={dto.From.Month}";

                return action;
            }
            else
            {
                var controllerName = RoutingHelper.GetControllerName<EmployeeTimeReportController>();
                var actionName = nameof(EmployeeTimeReportController.ReadOnlyTimeReport);

                var webUrl = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
                var action =
                    $"{webUrl}{areaName}/{controllerName}/{actionName}?employeeId={dto.EmployeeId}&year={dto.From.Year}&month={dto.From.Month}";

                return action;
            }
        }
    }
}