﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportProjectAttributeViewModelToTimeReportProjectAttributeDtoMapping : ClassMapping<TimeReportProjectAttributeViewModel, TimeReportProjectAttributeDto>
    {
        public TimeReportProjectAttributeViewModelToTimeReportProjectAttributeDtoMapping()
        {
            Mapping = v => new TimeReportProjectAttributeDto
            {
                Id = v.Id,
                Name = v.Name,
                Description = v.Description,
                DefaultValueId = v.DefaultValueId,
            };
        }
    }
}
