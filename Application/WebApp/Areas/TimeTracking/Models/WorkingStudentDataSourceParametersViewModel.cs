﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("Models.WorkingStudentDataSourceParameters")]
    [TabDescription(0, "basic", "BasicTabText", typeof(ReportResources))]
    [TabDescription(1, "employees", "EmployeesTabText", typeof(ReportResources))]
    public class WorkingStudentDataSourceParametersViewModel
    {
        [Tab("basic")]
        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime From { get; set; }

        [Tab("basic")]
        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime To { get; set; }

        [Tab("basic")]
        [Order(3)]
        [Render(Size = Size.Medium)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "EmptyStatusText",
            ResourceType = typeof(ReportResources), ItemProvider = typeof(EnumBasedListItemProvider<TimeReportStatus>))]
        [DisplayNameLocalized(nameof(ReportResources.StatusText), typeof(ReportResources))]
        public TimeReportStatus? Status { get; set; }

        [Tab("employees")]
        [Order(4)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeesText), typeof(ReportResources))]
        public long[] EmployeeIds { get; set; }

        [Tab("employees")]
        [Order(5)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long[] EmployeeOrgUnitIds { get; set; }

        [Tab("employees")]
        [Order(6)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.LocationText), typeof(ReportResources))]
        public long[] LocationIds { get; set; }

        [Tab("employees")]
        [Order(7)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.ContractTypeText), typeof(ReportResources))]
        public long[] ContractTypeIds { get; set; }

        [Tab("employees")]
        [Order(8)]
        [Render(Size = Size.Medium)]
        [DisplayNameLocalized(nameof(ReportResources.MyEmployeesText), typeof(ReportResources))]
        public bool MyEmployees { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResult = new List<ValidationResult>();

            if (From >= To)
            {
                validationResult.Add(new ValidationResult(CommonResources.StartDateGreaterThanEndDate, new List<string> { nameof(From) }));
            }

            return validationResult;
        }
    }
}
