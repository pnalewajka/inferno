﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using System.ComponentModel;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using System.Collections.Generic;
using System.Web.Services.Description;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceViewModel : IValidatableObject
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(VacationBalanceResources.Employee), typeof(VacationBalanceResources))]
        [ImportIgnore]
        [NonSortable]
        [Required]
        [Order(0)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Visibility(VisibilityScope.Form)]
        public long EmployeeId { get; set; }

        [Order(0)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(VacationBalanceResources.LoginHeaderText), typeof(VacationBalanceResources))]
        public string Login { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(VacationBalanceResources.FirstNameHeaderText), typeof(VacationBalanceResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "column-width-big")]
        public string FirstName { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(VacationBalanceResources.LastNameHeaderText), typeof(VacationBalanceResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "column-width-big")]
        public string LastName { get; set; }

        [DisplayNameLocalized(nameof(VacationBalanceResources.EffectiveOn), typeof(VacationBalanceResources))]
        [Order(3)]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "column-width-large date-column")]
        public DateTime EffectiveOn { get; set; }

        [DisplayNameLocalized(nameof(VacationBalanceResources.Operation), typeof(VacationBalanceResources))]
        [Order(4)]
        [NonSortable]
        public VacationOperation Operation { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(VacationBalanceResources.Comment), typeof(VacationBalanceResources))]
        [Order(5)]
        [NonSortable]
        public string Comment { get; set; }

        [DisplayNameLocalized(nameof(VacationBalanceResources.RegularVacationHourChange), typeof(VacationBalanceResources))]
        [Order(6)]
        [NonSortable]
        [Render(GridCssClass = "column-width-large")]
        public decimal HourChange { get; set; }

        [DisplayNameLocalized(nameof(VacationBalanceResources.HourBalanceAfterChange), typeof(VacationBalanceResources))]
        [Order(6)]
        [ImportIgnore]
        [ReadOnly(true)]
        [NonSortable]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "column-width-large")]
        public decimal TotalHourBalance { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeDeleted { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var isOperationValueOutOfScope = HourChange == 0;

            if (isOperationValueOutOfScope)
            {
                yield return new PropertyValidationResult(VacationBalanceResources.RegularVacationHourOutOfScopeErrorMessage, () => HourChange);
            }

            // Force vacation request hours to be negative
            var isVacationOperationValueOutOfScope = Operation == VacationOperation.Vacation && HourChange > 0;

            if (isVacationOperationValueOutOfScope)
            {
                yield return new PropertyValidationResult(VacationBalanceResources.VacationOperationValueOutOfScopeErrorMessage, () => HourChange);
            }

            // Force yearly calculation to be positive
            var isYearlyCalculationOutOfScope = Operation == VacationOperation.YearlyCalculation && HourChange < 0;

            if (isYearlyCalculationOutOfScope)
            {
                yield return new PropertyValidationResult(VacationBalanceResources.YearlyCalculationHoursOutOfScope, () => HourChange);
            }
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}{(string.IsNullOrEmpty(Login) ? string.Empty : $" - {Login}")} - {Operation.GetDescription()} {EffectiveOn:d}";
        }
    }
}
