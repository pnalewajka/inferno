﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public abstract class AbsencesDataSourceParametersBaseViewModel
    {
        [Order(10)]
        [Render(Size = Size.Small)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "AllStatusText",
            ResourceType = typeof(ReportResources), ItemProvider = typeof(BooleanBasedListItemProvider<VacationBooleanResources>))]
        [DisplayNameLocalized(nameof(ReportResources.AbsencesText), typeof(ReportResources))]
        public bool? IsVacation { get; set; }

        [Order(11)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(AbsenceTypePickerController))]
        [DisplayNameLocalized(nameof(ReportResources.AbsenceTypesText), typeof(ReportResources))]
        public long[] AbsenceTypeIds { get; set; }

        [Order(12)]
        [Render(Size = Size.Small)]
        [Dropdown(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, EmptyItemDisplayName = "EmptyStatusText",
            ResourceType = typeof(ReportResources), ItemProvider = typeof(EnumBasedListItemProvider<EmploymentType>))]
        [DisplayNameLocalized(nameof(ReportResources.EmploymentTypeText), typeof(ReportResources))]
        public EmploymentType? EmploymentType { get; set; }

        [Order(13)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(ContractTypePickerController), DialogWidth = "1000px")]
        [DisplayNameLocalized(nameof(ReportResources.ContractTypesText), typeof(ReportResources))]
        public long[] ContractTypeIds { get; set; }

        [Order(14)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.CompaniesText), typeof(ReportResources))]
        public long[] CompanyIds { get; set; }

        [Order(15)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.EmployeeOrgUnitsText), typeof(ReportResources))]
        public long[] OrgUnitIds { get; set; }

        [Order(16)]
        [Render(Size = Size.Medium)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [DisplayNameLocalized(nameof(ReportResources.LocationText), typeof(ReportResources))]
        public long[] LocationIds { get; set; }
    }
}
