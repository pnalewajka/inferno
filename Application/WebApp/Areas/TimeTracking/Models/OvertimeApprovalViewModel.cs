﻿using System;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class OvertimeApprovalViewModel
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public long ProjectId { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public decimal HourLimit { get; set; }

        public long RequestId { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
