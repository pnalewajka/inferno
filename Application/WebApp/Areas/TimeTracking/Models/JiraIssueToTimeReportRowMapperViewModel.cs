﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class JiraIssueToTimeReportRowMapperViewModel
    {
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
