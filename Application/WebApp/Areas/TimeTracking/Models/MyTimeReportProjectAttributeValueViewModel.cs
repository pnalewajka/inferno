﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class MyTimeReportProjectAttributeValueViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ClassName { get; set; }

        public string Description { get; set; }

        public AttributeValueFeature Features { get; set; }
    }
}
