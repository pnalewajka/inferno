﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.BusinessTrips.DocumentMappings;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.TimeTracking.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Attributes.CellContent;
using Smt.Atomic.Business.Compensation.DocumentMappings;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TimeReportViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long EmployeeId { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(TimeReportResources.AcronymLabel), typeof(TimeReportResources))]
        public string EmployeeAcronym { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [Hub(nameof(EmployeeId))]
        public string EmployeeFullName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmployeeEmail { get; set; }

        [Order(3)]
        [NonSortable]
        [DisplayNameLocalized(nameof(TimeReportResources.YearLabel), typeof(TimeReportResources))]
        public int Year { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte Month { get; set; }

        [Order(4)]
        [NonSortable]
        [DisplayNameLocalized(nameof(TimeReportResources.MonthLabel), typeof(TimeReportResources))]
        public string DisplayMonth => CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Month);

        [Visibility(VisibilityScope.None)]
        public decimal ContractedHours { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal ReportedNormalHours { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal NoServiceHours { get; set; }

        [Order(5)]
        [ProgressBar(BarCssClass = "progress-bar-success")]
        [DisplayNameLocalized(nameof(TimeReportResources.ProgressLabel), typeof(TimeReportResources))]
        public decimal Progress => ContractedHours != 0M ? Math.Min((ReportedNormalHours + NoServiceHours) * 100M / ContractedHours, 100M) : 0M;

        [Order(6)]
        [DisplayNameLocalized(nameof(TimeReportResources.StatusLabel), typeof(TimeReportResources))]
        [IconizedTimeReportStatus]
        public TimeReportStatus Status { get; set; }

        [Order(7)]
        [NonSortable]
        [ProjectStatusCell]
        [DisplayNameLocalized(nameof(TimeReportResources.ProjectsLabel), typeof(TimeReportResources))]
        public ProjectStatusViewModel[] ProjectStatuses { get; set; }

        [Order(8)]
        [NonSortable]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeOrgUnitShort), typeof(EmployeeResources))]
        public string OrgUnitName { get; set; }

        [Visibility(VisibilityScope.None)]
        [ValuePicker(Type = typeof(ManagerPickerController))]
        public long? LineManagerId { get; set; }

        [Order(9)]
        [NonSortable]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLineManagerShort), typeof(EmployeeResources))]
        [Hub(nameof(LineManagerId))]
        public string LineManagerName { get; set; }

        [Order(10)]
        [NonSortable]
        [DisplayNameLocalized(nameof(TimeReportResources.TotalHoursLabel), typeof(TimeReportResources))]
        public decimal TotalHours { get; set; }

        [Order(11)]
        [NonSortable]
        [DisplayNameLocalized(nameof(TimeReportResources.RegularHoursLabel), typeof(TimeReportResources))]
        public decimal RegularHours { get; set; }

        [Order(12)]
        [NonSortable]
        [DisplayNameLocalized(nameof(TimeReportResources.OvertimeHoursLabel), typeof(TimeReportResources))]
        public decimal OvertimeHours { get; set; }


        [DisplayNameLocalized(nameof(TimeReportResources.TaxDeductHoursLabel), typeof(TimeReportResources))]
        public decimal TaxDeductHours { get; set; }

        [Order(13)]
        [NonSortable]
        [DisplayNameLocalized(nameof(TimeReportResources.TaxDeductHoursLabel), typeof(TimeReportResources))]
        public string TaxDeductionPercent => TaxDeductHours == 0 ? string.Format(TimeReportResources.TaxDeductHoursPercent, 0,0) :
        string.Format(TimeReportResources.TaxDeductHoursPercent, TaxDeductHours, string.Format("{0:0.0}", (ReportedNormalHours != 0M ? Math.Min(TaxDeductHours * 100M / ReportedNormalHours, 100M) : 0M)));

        [DisplayNameLocalized(nameof(TimeReportResources.ControllingStatus), typeof(TimeReportResources))]
        [Order(14)]
        [SortBy(nameof(ControllingStatus))]
        public TooltipText GetControllingStatus(UrlHelper helper)
        {
            return new TooltipText
            {
                Label = ControllingStatus.GetDescription(),
                Tooltip = ControllingErrorMessage
            };
        }

        [Visibility(VisibilityScope.None)]
        public ControllingStatus ControllingStatus { get; set; }

        [DisplayNameLocalized(nameof(TimeReportResources.ControllingErrorMessage), typeof(TimeReportResources))]
        [Order(15)]
        [Visibility(VisibilityScope.Grid)]
        public string ControllingErrorMessage { get; set; }

        [Order(16)]
        [SortBy(nameof(InvoiceNotificationStatus))]
        [DisplayNameLocalized(nameof(TimeReportResources.InvoiceNotificationStatus), typeof(TimeReportResources))]
        public TooltipText GetInvoiceNotificationStatus(UrlHelper helper)
        {
            return new TooltipText
            {
                Label = InvoiceNotificationStatus.GetDescription(),
                Tooltip = InvoiceNotificationErrorMessage
            };
        }

        [Visibility(VisibilityScope.None)]
        public InvoiceNotificationStatus InvoiceNotificationStatus { get; set; }

        [Order(17)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(TimeReportResources.InvoiceNotificationErrorMessage), typeof(TimeReportResources))]
        public string InvoiceNotificationErrorMessage { get; set; }

        [Order(18)]
        [SortBy(nameof(InvoiceStatus))]
        [DisplayNameLocalized(nameof(TimeReportResources.InvoiceStatus), typeof(TimeReportResources))]
        public TooltipText GetInvoiceStatus(UrlHelper helper)
        {
            return new TooltipText
            {
                Label = InvoiceStatus.GetDescription(),
                Tooltip = InvoiceErrorMessage
            };
        }

        [Visibility(VisibilityScope.None)]
        public InvoiceStatus InvoiceStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public InvoiceIssues? InvoiceIssues { get; set; }

        [Visibility(VisibilityScope.None)]
        public string InvoiceRejectionReason { get; set; }

        [Order(19)]
        [DisplayNameLocalized(nameof(TimeReportResources.InvoiceErrorMessage), typeof(TimeReportResources))]
        public string InvoiceErrorMessage => InvoiceStatus == InvoiceStatus.InvoiceRejected ? InvoiceRejectionReason
            : InvoiceStatus == InvoiceStatus.InvoiceSentWithIssues ? "" : null;

        [Order(20)]
        [NonSortable]
        [ReadOnly(true)]
        [Visibility(VisibilityScope.Grid)]
        [RoleRequired(SecurityRoleType.CanViewContractorInvoices)]
        [DisplayNameLocalized(nameof(TimeReportResources.InvoiceAmount), typeof(TimeReportResources))]
        public string InvoiceAmount { get; set; }

        [Order(21)]
        [NonSortable]
        [DocumentList]
        [Visibility(VisibilityScope.Grid)]
        [DocumentUpload(typeof(InvoiceDocumentMapping))]
        [RoleRequired(SecurityRoleType.CanViewContractorInvoices)]
        [DisplayNameLocalized(nameof(TimeReportResources.InvoiceAttachment), typeof(TimeReportResources))]
        public List<DocumentViewModel> InvoiceDocuments { get; set; }

        [Order(22)]
        [DisplayNameLocalized(nameof(TimeReportResources.LastChangedLabel), typeof(TimeReportResources))]
        [Render(GridCssClass = "single-line-date")]
        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public string Url { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EditOnBehalfUrl { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeReopened { get; set; }

        public override string ToString()
        {
            return EmployeeFullName;
        }

        [DisplayNameLocalized(nameof(TimeReportResources.EmploymentPeriodContractType), nameof(TimeReportResources.EmploymentPeriodContractTypeShort), typeof(TimeReportResources))]
        [NonSortable]
        [Render(GridCssClass = "no-break-column")]
        public string ContractTypeName { get; set; }
    }
}
