﻿using System;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Business.TimeTracking.Workflows.Dto;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class AbsenceRequestNotificationDtoToAbsenceRequestNotificationModelMapping : ClassMapping<AbsenceRequestNotificationDto, AbsenceRequestNotificationModel>
    {
        public AbsenceRequestNotificationDtoToAbsenceRequestNotificationModelMapping(ISystemParameterService systemParameters)
        {
            Uri baseUri = new Uri(systemParameters.GetParameter<string>(ParameterKeys.ServerAddress));
            Mapping =
                d =>
                    new AbsenceRequestNotificationModel
                    {
                        ReceiverName = d.ReceiverName,
                        EmployeeFullName = d.EmployeeFullName,
                        Description = d.Description,
                        RequestsWatchedByMeLink = new Uri(baseUri, "/Workflows/Request/List?filter=" + FilterCodes.WatchedByMe)
                    };
        }
    }
}
