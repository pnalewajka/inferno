﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class HourlyRateDtoToHourlyRateViewModelMapping : ClassMapping<HourlyRateDto, HourlyRateViewModel>
    {
        public HourlyRateDtoToHourlyRateViewModelMapping()
        {
            Mapping = d => new HourlyRateViewModel
            {
                Id = d.Id,
                Description = d.Description
            };
        }
    }
}
