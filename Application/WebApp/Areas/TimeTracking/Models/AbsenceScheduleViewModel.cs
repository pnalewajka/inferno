﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    [Identifier("ViewModel.AbsenceSchedule")]
    public class AbsenceScheduleViewModel
    {
        [DisplayNameLocalized(nameof(AbsenceScheduleResources.From), typeof(AbsenceScheduleResources))]
        public DateTime PeriodStartDate { get; set; } = DateTime.Now.AddMonths(-1).GetFirstDayInMonth();

        [DisplayNameLocalized(nameof(AbsenceScheduleResources.To), typeof(AbsenceScheduleResources))]
        public DateTime PeriodEndDate { get; set; } = DateTime.Now.AddMonths(2).GetLastDayInMonth();

        [DisplayNameLocalized(nameof(AbsenceScheduleResources.EmployeeIdsLabel), typeof(AbsenceScheduleResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=not-terminated-employees'")]
        public long[] EmployeeIds { get; set; }

        [DisplayNameLocalized(nameof(AbsenceScheduleResources.ProjectIdsLabel), typeof(AbsenceScheduleResources))]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public long[] ProjectIds { get; set; }
    }
}