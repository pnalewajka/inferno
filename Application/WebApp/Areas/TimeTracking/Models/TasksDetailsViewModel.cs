using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class TasksDetailsViewModel
    {
        public TasksDetailsViewModel()
        {
            Tasks = new List<TaskDetailsViewModel>();
        }

        [DisplayNameLocalized(nameof(ProjectTimeReportApprovalRequestResources.TasksDetails), typeof(ProjectTimeReportApprovalRequestResources))]
        public IList<TaskDetailsViewModel> Tasks { get; set; }

        public string OriginalReportUrl { get; set; }
    }
}