﻿using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Models
{
    public class VacationBalanceImportViewModelToVacationBalanceDtoMapping : ClassMapping<VacationBalanceImportViewModel, VacationBalanceDto>
    {
        public VacationBalanceImportViewModelToVacationBalanceDtoMapping(IAllocationRequestImportLookupService lookupService)
        {
            Mapping = v => new VacationBalanceDto
            {
                Id = v.Id,
                EmployeeId = lookupService.GetEmployeeIdByEmail(v.EmployeeEmail),
                EmployeeLogin = v.Login,
                EmployeeFirstName = v.FirstName,
                EmployeeLastName = v.LastName,
                EffectiveOn = v.EffectiveOn,
                Operation = v.Operation,
                Comment = v.Comment,
                HourChange = v.HourChange,
                Timestamp = v.Timestamp
            };
        }
    }
}