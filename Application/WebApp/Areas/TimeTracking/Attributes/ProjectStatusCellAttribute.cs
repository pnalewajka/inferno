﻿using System.Reflection;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.TimeTracking.Models;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Attributes
{
    public class ProjectStatusCellAttribute: CellContentAttribute
    {
        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            var projectStatuses = (ProjectStatusViewModel[])cellContentConverterContext.PropertyValue;

            return new DisplayProjectStatus
            {
                Statuses = projectStatuses
            };
        }

        protected override string TemplateCode => "ProjectStatus";
    }
}