﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.TimeTracking
{
    public class TimeTrackingAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "TimeTracking";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TimeTracking_default",
                "TimeTracking/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}