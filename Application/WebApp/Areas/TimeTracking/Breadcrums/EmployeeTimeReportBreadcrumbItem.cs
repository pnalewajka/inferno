﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Breadcrums
{
    public class EmployeeTimeReportBreadcrumbItem : TimeReportBaseBreadcrumbItem<EmployeeTimeReportController>
    {
        public EmployeeTimeReportBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
        }

        public override string ActionName { get { return "TimeReport"; } }
    }
}