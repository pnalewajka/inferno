﻿using System.Linq;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Breadcrums
{
    public class AbsenceScheduleListBreadcrumbItem : BreadcrumbItem
    {
        public AbsenceScheduleListBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            var items = Context.SiteMap.GetPath(
                    RoutingHelper.GetControllerName<AbsenceScheduleController>(),
                    nameof(AbsenceScheduleController.Configure))
                .Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText()))
                .ToList();

            items.Add(new BreadcrumbItemViewModel(string.Empty, AbsenceScheduleResources.AbsenceScheduleList));

            SetItems(items);
        }
    }
}
