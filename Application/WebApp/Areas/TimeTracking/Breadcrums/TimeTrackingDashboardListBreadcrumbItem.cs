﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Breadcrums
{
    public class TimeTrackingDashboardListBreadcrumbItem : CardIndexBreadcrumbItem
    {
        private readonly ITimeService _timeService;

        public TimeTrackingDashboardListBreadcrumbItem(
            IBreadcrumbContextProvider breadcrumbContextProvider,
            IBreadcrumbService breadcrumbService, ITimeService timeService)
            : base(breadcrumbContextProvider, breadcrumbService)
        {
            _timeService = timeService;
        }

        protected override void SetDisplayName(string displayName)
        {
            var context = (TimeTrackingDashboardContext)((ICardIndexController)Context.Controller).Context;

            if (!context.IsSet)
            {
                var currentDate = _timeService.GetCurrentDate();
                context.Month = currentDate.Month;
                context.Year = currentDate.Year;
            }

            base.SetDisplayName(string.Format(
                TimeReportResources.TimeReportBreacrumbItemSuffix,
                displayName,
                DateHelper.BeginningOfMonth(context.Year.Value, (byte)context.Month.Value)));
        }
    }
}