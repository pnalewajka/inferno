﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Accounts.Breadcrumbs
{
    public class TimeReportProjectAttributeValueBreadcrumbItem : BreadcrumbItem
    {
        public TimeReportProjectAttributeValueBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider)
            : base(breadcrumbContextProvider)
        {
            DisplayName = TimeReportProjectAttributeResources.ProjectAttributeValueBreadcrumbItem;
        }
    }
}