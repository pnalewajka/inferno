﻿using System.Linq;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Resources;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Breadcrums
{
    public class TimeReportListBreadcrumbItem : CardIndexBreadcrumbItem
    {
        public TimeReportListBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider, IBreadcrumbService breadcrumbService)
            : base(breadcrumbContextProvider, breadcrumbService)
        {
        }

        protected override void SetDisplayName(string displayName)
        {
            var context = (TimeReportContext)((ICardIndexController)Context.Controller).Context;

            if (IsMyTimeReportFilterSelected())
            {
                base.SetDisplayName(
                    string.Format(
                        TimeReportResources.TimeReportBreacrumbItemSuffix,
                        displayName,
                        DateHelper.BeginningOfMonth(context.TimeReportYear, context.TimeReportMonth)));
            }
        }

        private bool IsMyTimeReportFilterSelected()
        {
            var filters = ((GridViewModel)Context.ViewModel)?.Filters;

            return !(filters != null && filters.SelectMany(x => x.Filters)
                         .Any(x => x.IsSelected && x.Code == FilterCodes.MyTimeReports));
        }
    }
}