﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;
using Smt.Atomic.WebApp.Breadcrumbs;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.TimeTracking.Breadcrums
{
    public abstract class TimeReportBaseBreadcrumbItem<T> : BreadcrumbItem
        where T : BaseEmployeeTimeReportController
    {
        public TimeReportBaseBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            var controllerTypes = GetControllerTypes();
            var controllerName = RoutingHelper.GetControllerName(controllerTypes.First());

            var references = Context.SiteMap.GetPath(controllerName, ActionName).ToList();

            var items = references
                .Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText()))
                .ToList();

            SetItems(items);

            var atomicController = (T)breadcrumbContextProvider.Context.Controller;

            DisplayName = atomicController.Layout.PageTitle;
        }

        public abstract string ActionName { get; }

        private List<Type> GetControllerTypes()
        {
            var result = new List<Type>();

            var controllerType = Context.Controller.GetType();
            result.Add(controllerType);

            while (TypeHelper.IsSubclassOfRawGeneric(controllerType, typeof(SubCardIndexController<,,,>)))
            {
                controllerType = TypeHelper.GetRawGenericArguments(controllerType, typeof(SubCardIndexController<,,,>))[2];
                result.Insert(0, controllerType);
            }

            return result;
        }
    }
}