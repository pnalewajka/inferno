﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers.Buttons
{
    public static class CandidateButtonsHelper
    {
        private static readonly string JobApplicationsGroup = "job-applications";
        private static readonly string ContactRecordsGroup = "contact-records";
        private static readonly string NotesGroup = "notes";
        private static readonly string RecruitmentProcessGroup = "recruitment-process";
        private static readonly string FilesGroup = "files";
        private static readonly string DataConsentGroup = "data-consent";
        private static readonly string DataActivityGroup = "data-activity";
        private static readonly string TechnicalReviewButtonsGroup = "technical-review-buttons";

        public static IEnumerable<CommandButton> GetToolbatButtons(UrlHelper url)
        {
            return GetQuickAddRecruitmentProcessCommandButtons(url)
                .Union(GetContactRecordsCommandButtons(url))
                .Union(GetNotesCommandButtons(url))
                .Union(GetFileCommandButtons(url))
                .Union(GetJobApplicationsCommandButtons(url))
                .Union(GetDataConsentsCommandButtons(url))
                .Union(GetDataActivitiesCommandButtons(url))
                .Union(GetAnonymizeCommandButton(url))
                .Union(GetCandidateTechnicalReviewButton(url));
        }

        public static IEnumerable<RowButton> GetRowButtons(UrlHelper url)
        {
            return GetWatchButtons(url)
                .Union(GetFavoriteButtons(url));
        }

        public static List<FooterButton> GetFooterButtons(UrlHelper url, long candidateId)
        {
            return new List<FooterButton>()
            {
                GetAddJobApplicationsFooterButton(url, candidateId),
                GetAddProcessFooterButton(url, candidateId),
                GetAddFileFooterButton(url, candidateId),
                GetAddContactFooterButton(url, candidateId),
                GetAddDataConsentsFooterButton(url, candidateId)
            };
        }

        private static FooterButton GetAddFileFooterButton(UrlHelper url, long candidateId)
        {
            var addButton = new FooterButton("addFile", CandidateResources.FileButtonText)
            {
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile,
                OnClickAction = new DropdownAction
                {
                    Items = new List<ICommandButton>
                    {
                        CreateFileFromDiskFooterButton(url, candidateId),
                        CreateFileFromInboundEmailCommandButton(url, candidateId)
                    }
                },
                ButtonStyleType = CommandButtonStyle.Default,
                Icon = FontAwesome.Plus
            };

            return addButton;
        }

        internal static CommandButton CreateFileFromInboundEmailCommandButton(UrlHelper urlHelper, long candidateId)
        {
            var addFileFromInboundEmailUriAction = urlHelper.UriActionPost(
                nameof(CandidateInboundEmailFileController.GetAddDialog), RoutingHelper.GetControllerName<CandidateInboundEmailFileController>(),
                KnownParameter.None, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

            return new CommandButton
            {
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile,
                Text = CandidateFileResources.FromInboundEmail,
                OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{addFileFromInboundEmailUriAction.Url}\")")
            };
        }

        private static CommandButton CreateFileFromDiskFooterButton(UrlHelper urlHelper, long candidateId)
        {
            var addFileFromDiskUriAction = urlHelper.UriActionPost(
                nameof(CandidateFileController.GetAddDialog), RoutingHelper.GetControllerName<CandidateFileController>(),
                KnownParameter.None, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

            return new CommandButton
            {
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile,
                Text = CandidateFileResources.FromDisk,
                OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{addFileFromDiskUriAction.Url}\")")
            };
        }

        private static FooterButton GetAddProcessFooterButton(UrlHelper url, long candidateId)
        {
            var addProcessUriAction = url.UriActionPost(
                nameof(RecruitmentProcessController.GetAddDialog), RoutingHelper.GetControllerName<RecruitmentProcessController>(),
                KnownParameter.None, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

            return new FooterButton("addProcess", CandidateResources.RecruitmentProcessButton)
            {
                RequiredRole = SecurityRoleType.CanAddRecruitmentProcess,
                OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{addProcessUriAction.Url}\")"),
                ButtonStyleType = CommandButtonStyle.Default,
                Icon = FontAwesome.Plus
            };
        }

        private static FooterButton GetAddContactFooterButton(UrlHelper url, long candidateId)
        {
            var addContactUriAction = url.UriActionPost(
                nameof(CandidateContactRecordController.GetAddDialog), RoutingHelper.GetControllerName<CandidateContactRecordController>(),
                KnownParameter.None, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

            return new FooterButton("addContact", CandidateResources.ContactRecordButtonText)
            {
                OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{addContactUriAction.Url}\")"),
                RequiredRole = SecurityRoleType.CanAddRecruitmentContactRecord,
                ButtonStyleType = CommandButtonStyle.Default,
                Icon = FontAwesome.Plus
            };
        }

        private static FooterButton GetAddDataConsentsFooterButton(UrlHelper url, long candidateId)
        {
            var addConsentUriAction = url.UriActionPost(
                nameof(CandidateDataConsentController.GetAddDialog), RoutingHelper.GetControllerName<CandidateDataConsentController>(),
                KnownParameter.None, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

            var addButton = new FooterButton("addDataConsent", CandidateResources.DataConsentButtonText)
            {
                OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{addConsentUriAction.Url}\")"),
                RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                ButtonStyleType = CommandButtonStyle.Default,
                Icon = FontAwesome.Plus
            };

            SetAddDataConsentFooterDropdownItems(addButton, url, candidateId);

            return addButton;
        }

        private static void SetAddDataConsentFooterDropdownItems(ICommandButton addButton, UrlHelper urlHelper, long candidateId)
        {
            var consentTypes = EnumHelper.GetEnumValues<DataConsentType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var consentType in consentTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(consentType.ToString());
                var uriAction = urlHelper.UriActionPost(
                    nameof(CandidateDataConsentController.GetAddDialog), RoutingHelper.GetControllerName<CandidateDataConsentController>(),
                    KnownParameter.None, DataConsentController.ConsentTypeParameterName, typeValueName, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

                var addItemButton = new CommandButton
                {
                    Group = DataConsentResources.ConsentTypeLabel,
                    RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                    Text = consentType.GetDescriptionOrValue(),
                    OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{uriAction.Url}\")")
                };

                addDropdownItems.Add(addItemButton);
            }

            addButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        private static FooterButton GetAddJobApplicationsFooterButton(UrlHelper url, long candidateId)
        {
            var addButton = new FooterButton("addApplication", CandidateResources.JobApplicationButtonText)
            {
                RequiredRole = SecurityRoleType.CanAddJobApplications,
                ButtonStyleType = CommandButtonStyle.Default,
                Icon = FontAwesome.Plus
            };

            SetAddJobApplicationFooterDropdownItems(addButton, url, candidateId);

            return addButton;
        }

        private static void SetAddJobApplicationFooterDropdownItems(ICommandButton addButton, UrlHelper urlHelper, long candidateId)
        {
            var originTypes = EnumHelper.GetEnumValues<ApplicationOriginType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var originType in originTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(originType.ToString());

                var uriAction = urlHelper.UriActionPost(
                    nameof(CandidateJobApplicationsController.GetAddDialog), RoutingHelper.GetControllerName<CandidateJobApplicationsController>(),
                    KnownParameter.None, JobApplicationController.OriginTypeParameterName, typeValueName, RoutingHelper.ParentIdParameterName, candidateId.ToInvariantString());

                var addItemButton = new CommandButton
                {
                    Group = JobApplicationResources.OriginTypeLabel,
                    RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpening,
                    Text = originType.GetDescriptionOrValue(),
                    OnClickAction = new JavaScriptCallAction($"Candidate.ShowDialog(\"{uriAction.Url}\")")
                };

                addDropdownItems.Add(addItemButton);
            }

            addButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        private static IEnumerable<ToolbarButton> GetJobApplicationsCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.JobApplicationsButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateJobApplicationsController.List),
                    RoutingHelper.GetControllerName<CandidateJobApplicationsController>(), KnownParameter.ParentId),
                Icon = FontAwesome.FileTextO,
                RequiredRole = SecurityRoleType.CanViewJobApplications,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = JobApplicationsGroup
            };

            var addButton = new ToolbarButton
            {
                Text = CandidateResources.JobApplicationsAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(CandidateJobApplicationsController.GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateJobApplicationsController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddJobApplications,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = JobApplicationsGroup
            };

            JobApplicationController.SetAddJobApplicationDropdownItems(
                addButton, url, RoutingHelper.GetControllerName<CandidateJobApplicationsController>(), KnownParameter.ParentId);

            yield return addButton;
        }

        private static IEnumerable<ToolbarButton> GetContactRecordsCommandButtons(UrlHelper url)
        {

            yield return new ToolbarButton
            {
                Text = CandidateResources.ContactRecordsButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateContactRecordController.List),
                    RoutingHelper.GetControllerName<CandidateContactRecordController>(), KnownParameter.ParentId),
                Icon = FontAwesome.CalendarO,
                RequiredRole = SecurityRoleType.CanViewRecruitmentContactRecord,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = ContactRecordsGroup
            };

            yield return new ToolbarButton
            {
                Text = CandidateResources.ContactRecordsAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(CandidateContactRecordController.GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateContactRecordController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentContactRecord,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = ContactRecordsGroup
            };
        }

        private static IEnumerable<CommandButton> GetNotesCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.NotesButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateNoteController.List),
                       RoutingHelper.GetControllerName<CandidateNoteController>(), KnownParameter.ParentId),
                Icon = FontAwesome.StickyNoteO,
                RequiredRole = SecurityRoleType.CanViewRecruitmentCandidateNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = NotesGroup
            };

            yield return new ToolbarButton
            {
                Text = CandidateResources.NotesAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(CandidateNoteController.GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateNoteController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = NotesGroup
            };
        }

        private static IEnumerable<CommandButton> GetFileCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.FilesButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateFileController.List),
                      RoutingHelper.GetControllerName<CandidateFileController>(), KnownParameter.ParentId),
                Icon = FontAwesome.Paperclip,
                RequiredRole = SecurityRoleType.CanViewRecruitmentCandidateFile,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = FilesGroup
            };

            var addButton = new ToolbarButton
            {
                Text = CandidateResources.FileAddButtonText,
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = FilesGroup,
                OnClickAction = new DropdownAction
                {
                    Items = new List<ICommandButton>
                    {
                        CandidateFileController.CreateFileFromDiskCommandButton(url, KnownParameter.ParentId),
                        CandidateInboundEmailFileController.CreateFileFromInboundEmailCommandButton(url, KnownParameter.ParentId)
                    }
                }
            };

            yield return addButton;
        }

        private static IEnumerable<CommandButton> GetDataConsentsCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.DataConsentsButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateDataConsentController.List),
                   RoutingHelper.GetControllerName<CandidateDataConsentController>(), KnownParameter.ParentId),
                Icon = FontAwesome.BalanceScale,
                RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataConsentGroup
            };

            var addButton = new ToolbarButton
            {
                Text = CandidateResources.DataConsentsAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(CandidateDataConsentController.GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateDataConsentController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataConsentGroup
            };

            DataConsentController.SetAddDataConsentDropdownItems(
                addButton, url, RoutingHelper.GetControllerName<CandidateDataConsentController>(),
                nameof(CandidateDataConsentController.GetAddDialog), KnownParameter.ParentId);

            yield return addButton;
        }

        private static IEnumerable<CommandButton> GetDataActivitiesCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.DataActivitiesButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateDataActivityController.List),
                    RoutingHelper.GetControllerName<CandidateDataActivityController>(), KnownParameter.ParentId),
                Icon = FontAwesome.History,
                RequiredRole = SecurityRoleType.CanViewDataActivities,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataActivityGroup
            };

            yield return new ToolbarButton
            {
                Text = CandidateResources.DataActivitiesAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(CandidateDataActivityController.GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateDataActivityController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddDataActivities,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = DataActivityGroup
            };
        }

        private static IEnumerable<CommandButton> GetQuickAddRecruitmentProcessCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.RecruitmentProcessesButton,
                OnClickAction = url.UriActionGet(nameof(CandidateRecruitmentProcessController.List),
                    RoutingHelper.GetControllerName<CandidateRecruitmentProcessController>(), KnownParameter.ParentId),
                Icon = FontAwesome.UserPlus,
                RequiredRole = SecurityRoleType.CanViewRecruitmentProcess,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = RecruitmentProcessGroup
            };

            yield return new ToolbarButton
            {
                Text = CandidateResources.RecruitmentProcessButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(RecruitmentProcessQucikAddController.GetAddDialog),
                    RoutingHelper.GetControllerName<RecruitmentProcessQucikAddController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentProcess,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = RecruitmentProcessGroup
            };
        }

        private static IEnumerable<RowButton> GetWatchButtons(UrlHelper url)
        {
            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewRecruitmentCandidate,
                Icon = FontAwesome.Eye,
                OnClickAction = url.UriActionPost(nameof(CandidateController.WatchCandidats), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<CandidateViewModel>
                {
                    Predicate = row => row.HasValidConsent && !row.IsWatchedByCurrentUser
                },
                Tooltip = CandidateResources.WatchButtonLabel,
                CssClass = "toogle-off"
            };

            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewRecruitmentCandidate,
                Icon = FontAwesome.Eye,
                OnClickAction = url.UriActionPost(nameof(CandidateController.UnwatchCandidats), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<CandidateViewModel>
                {
                    Predicate = row => row.HasValidConsent && row.IsWatchedByCurrentUser
                },
                Tooltip = CandidateResources.UnwatchButtonLabel,
                CssClass = "toogle-on"
            };
        }

        private static IEnumerable<CommandButton> GetAnonymizeCommandButton(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.AnonymizeButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateController.Anonymize), KnownParameter.SelectedId),
                RequiredRole = SecurityRoleType.CanAnonymizeCandidates,
                Confirmation = new Confirmation
                {
                    AcceptButtonText = CardIndexResources.OkButtonText,
                    IsRequired = true,
                    Message = CandidateResources.AnonymizeConfirmationMessage,
                    RejectButtonText = CardIndexResources.CancelButtonText
                },
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
            };
        }

        private static IEnumerable<CommandButton> GetCandidateTechnicalReviewButton(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.TechnicalReviewButtonText,
                OnClickAction = url.UriActionGet(nameof(CandidateTechnicalReviewController.List),
                    RoutingHelper.GetControllerName<CandidateTechnicalReviewController>(), KnownParameter.ParentId),
                Icon = FontAwesome.UserPlus,
                RequiredRole = SecurityRoleType.CanViewRecruitmentTechnicalReviews,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = TechnicalReviewButtonsGroup
            };
        }

        private static IEnumerable<RowButton> GetFavoriteButtons(UrlHelper url)
        {
            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanFavorCandidate,
                Tooltip = CandidateResources.FavoriteButtonLabel,
                OnClickAction = url.UriActionPost(nameof(CandidateController.AddToFavorite), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Icon = FontAwesome.Heart,
                Availability = new RowButtonAvailability<CandidateViewModel>
                {
                    Predicate = c => c.HasValidConsent && !c.IsFavoriteCandidate
                },
                CssClass = "toogle-off"
            };

            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanFavorCandidate,
                Tooltip = CandidateResources.DislikedButtonLabel,
                OnClickAction = url.UriActionPost(nameof(CandidateController.RemoveFromFavorite), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<CandidateViewModel>
                {
                    Predicate = c => c.HasValidConsent && c.IsFavoriteCandidate
                },
                Icon = FontAwesome.Heart,
                CssClass = "toogle-on"
            };
        }
    }
}