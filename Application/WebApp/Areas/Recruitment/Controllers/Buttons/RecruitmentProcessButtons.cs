﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers.Buttons
{
    public static class RecruitmentProcessButtons
    {
        private const string RecruitmentProcessStepsGroup = "recruitment-process-steps";
        private const string NotesGroup = "notes";
        private const string WatchButtonsGroup = "watch-buttons";
        private const string CloseGroup = "close-button";
        private const string HoldGroup = "hold-buttons";
        private const string ReopenButtonGroup = "reopen";

        public static IEnumerable<CommandButton> GetToolbarButtons(UrlHelper url, string parentIdParamName = null)
        {
            return GetRecruitmentProcessStepsCommandButtons(url, parentIdParamName)
                .Union(GetNotesCommandButtons(url))
                .Union(GetCloseCommandButton())
                .Union(GetCloneButton(url))
                .Union(GetReopenProcessCommandButton());
        }

        public static IEnumerable<CommandButton> GetRowButtons(UrlHelper url)
        {
            return GetWatchCommandButton(url);
        }

        private static IEnumerable<CommandButton> GetRecruitmentProcessStepsCommandButtons(UrlHelper url, string parentIdParamName)
        {
            var recruitmentProcessStepOnClickAction = string.IsNullOrEmpty(parentIdParamName)
                ? url.UriActionGet(nameof(RecruitmentProcessStepController.List),
                    RoutingHelper.GetControllerName<RecruitmentProcessStepController>(), KnownParameter.ParentId)
                : url.UriActionGet(nameof(RecruitmentProcessStepController.List),
                    RoutingHelper.GetControllerName<RecruitmentProcessStepController>(), KnownParameter.ParentId,
                    $"{NamingConventionHelper.ConvertPascalCaseToHyphenated(parentIdParamName)}", "~url.parentId");

            yield return new ToolbarButton
            {
                Text = RecruitmentProcessResources.RecruitmentProcessStepsButtonText,
                OnClickAction = recruitmentProcessStepOnClickAction,
                Icon = FontAwesome.List,
                RequiredRole = SecurityRoleType.CanViewRecruitmentProcessSteps,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.status !== {(int)RecruitmentProcessStatus.Draft}"
                },
                Group = RecruitmentProcessStepsGroup,
                IsDefault = true
            };

            var addStepButton = new ToolbarButton
            {
                Group = RecruitmentProcessStepsGroup,
                RequiredRole = SecurityRoleType.CanAddRecruitmentStepViaDropdown,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.status === {(int)RecruitmentProcessStatus.Active}"
                },
                Text = RecruitmentProcessResources.RecruitmentProcessStepsAddButtonText
            };

            RecruitmentProcessStepController.SetAddRecruitmentProcessStepDropdownItems(addStepButton, url,
                RoutingHelper.GetControllerName(typeof(RecruitmentProcessStepController)),
                KnownParameter.ParentId);

            yield return addStepButton;
        }

        private static IEnumerable<CommandButton> GetNotesCommandButtons(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = RecruitmentProcessResources.NotesButtonText,
                OnClickAction = url.UriActionGet(nameof(RecruitmentProcessNoteController.List),
                    RoutingHelper.GetControllerName<RecruitmentProcessNoteController>(), KnownParameter.ParentId),
                Icon = FontAwesome.StickyNoteO,
                RequiredRole = SecurityRoleType.CanViewRecruitmentProcessNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = NotesGroup
            };

            yield return new ToolbarButton
            {
                Text = RecruitmentProcessResources.NotesAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(url.UriActionPost(nameof(RecruitmentProcessController.GetAddDialog),
                    RoutingHelper.GetControllerName<RecruitmentProcessNoteController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentProcessNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = NotesGroup
            };
        }

        private static IEnumerable<CommandButton> GetWatchCommandButton(UrlHelper url)
        {
            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewRecruitmentProcess,
                Icon = FontAwesome.Eye,
                OnClickAction = url.UriActionPost(nameof(RecruitmentProcessController.WatchRecruitmentProcess), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<RecruitmentProcessViewModel>
                {
                    Predicate = row => !row.IsWatchedByCurrentUser
                },
                Tooltip = RecruitmentProcessResources.WatchButtonLabel,
                CssClass = "toogle-off"
            };

            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewRecruitmentProcess,
                Icon = FontAwesome.Eye,
                OnClickAction = url.UriActionPost(nameof(RecruitmentProcessController.UnwatchRecruitmentProcess), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<RecruitmentProcessViewModel>
                {
                    Predicate = row => row.IsWatchedByCurrentUser
                },
                Tooltip = RecruitmentProcessResources.UnwatchButtonLabel,
                CssClass = "toogle-on"
            };
        }

        private static IEnumerable<CommandButton> GetCloseCommandButton()
        {
            yield return new ToolbarButton
            {
                Text = RecruitmentProcessResources.CloseButtonLabel,
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.CloseFromGrid(grid, 'Models.{ nameof(RecruitmentProcessCloseViewModel) }');"),
                RequiredRole = SecurityRoleType.CanCloseRecruitmentProcesses,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(RecruitmentProcessViewModel.IsNotClosed))}"
                },
                Group = CloseGroup
            };
        }

        private static IEnumerable<RowButton> GetOnOffHoldRowButtons(UrlHelper url)
        {
            yield return new RowButton
            {
                Icon = FontAwesome.Pause,
                OnClickAction = url.UriActionPost(nameof(RecruitmentProcessController.PutOnHoldRecruitmentProcess), KnownParameter.SelectedId),
                RequiredRole = SecurityRoleType.CanPutRecruitmentProcessOnHold,
                Availability = new RowButtonAvailability<RecruitmentProcessViewModel>
                {
                    Predicate = row => row.Status != RecruitmentProcessStatus.Closed && row.Status != RecruitmentProcessStatus.OnHold
                },
                Tooltip = RecruitmentProcessResources.PutOnHoldLabel
            };

            yield return new RowButton
            {
                Icon = FontAwesome.Play,
                OnClickAction = url.UriActionPost(nameof(RecruitmentProcessController.TakeOffHoldRecruitmentProcess), KnownParameter.SelectedId),
                RequiredRole = SecurityRoleType.CanTakeRecruitmentProcessOffHold,
                Availability = new RowButtonAvailability<RecruitmentProcessViewModel>
                {
                    Predicate = row => row.Status == RecruitmentProcessStatus.OnHold
                },
                Tooltip = RecruitmentProcessResources.TakeOffHoldLabel
            };
        }

        private static IEnumerable<CommandButton> GetCloneButton(UrlHelper url)
        {
            yield return new ToolbarButton
            {
                Text = RecruitmentProcessResources.Clone,
                RequiredRole = SecurityRoleType.CanCloneRecruitmentProcess,
                Icon = FontAwesome.Clone,
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.Clone(grid, 'Models.{ nameof(JobOpeningPickerViewModel) }');"),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"(row.status === {(int)RecruitmentProcessStatus.Active} || row.status === {(int)JobOpeningStatus.Closed})",
                    Mode = AvailabilityMode.SingleRowSelected,
                },
            };
        }


        private static IEnumerable<CommandButton> GetReopenProcessCommandButton()
        {
            yield return new ToolbarButton
            {
                Text = RecruitmentProcessResources.ReOpenCommandButtonText,
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.ReopenProcessFromGrid(grid, 'Models.{ nameof(RecruitmentProcessReopenViewModel) }');"),
                Icon = FontAwesome.Refresh,
                RequiredRole = SecurityRoleType.CanReopenRecruitmentProcesses,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.canBeReopened",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = ReopenButtonGroup
            };
        }

    }
}