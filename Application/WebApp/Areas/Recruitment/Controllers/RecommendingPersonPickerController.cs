﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.RecommendingPerson")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecommendingPersons)]
    public class RecommendingPersonPickerController : ReadOnlyCardIndexController<RecommendingPersonViewModel, RecommendingPersonDto, RecommendingPersonPickerContext>
    {
        public RecommendingPersonPickerController(IRecommendingPersonPickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}