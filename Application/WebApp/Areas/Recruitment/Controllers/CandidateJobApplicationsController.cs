﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewJobApplications)]
    public class CandidateJobApplicationsController : SubCardIndexController<JobApplicationViewModel, JobApplicationDto, CandidateController, ParentIdContext>
    {
        private IJobApplicationCardIndexDataService _cardIndexDataService;

        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IJobApplicationService _jobApplicationService;

        public CandidateJobApplicationsController(
            IJobApplicationCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService,
            IJobApplicationService jobApplicationService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;
            _jobApplicationService = jobApplicationService;
            CardIndex.Settings.Title = JobApplicationResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobApplications;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditJobApplications;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobApplications;
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            JobApplicationController.SetAddJobApplicationDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this));
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult Edit(long id)
        {
            LogJobApplicationViewDataActivity(id);

            return base.Edit(id);
        }

        public override ActionResult View(long id)
        {
            LogJobApplicationViewDataActivity(id);

            return base.View(id);
        }

        private void LogJobApplicationViewDataActivity(long candidateId)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.JobApplication, candidateId, GetType().Name);

            var jobApplication = _jobApplicationService.GetJobApplicationById(candidateId);

            if (jobApplication.OriginType == ApplicationOriginType.Recommendation)
            {
                _dataActivityLogService.LogRecommendingPersonDataActivity(jobApplication.RecommendingPersonId.Value, ActivityType.ViewedRecord, GetType().Name);
            }
        }

        private object GetDefaultNewRecord()
        {
            var contentTypeName = Request.QueryString[JobApplicationController.OriginTypeParameterName];
            var contentType = EnumHelper.GetEnumValueOrDefault<ApplicationOriginType>(contentTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (contentType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            var dto = _cardIndexDataService.GetDefaultNewRecord(contentType.Value);

            dto.CandidateId = Context.ParentId;

            return dto;
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();
            var configuration = new GridViewConfiguration<JobApplicationViewModel>(JobApplicationResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(c => c.CandidateId);
            configurations.Add(configuration);

            return configurations;
        }
    }
}