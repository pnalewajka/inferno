﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize]
    public class CandidateInboundEmailFileController : SubCardIndexController<CandidateFileFromInboundEmailViewModel, CandidateFileDto, CandidateController, ParentIdContext>
    {
        private ICandidateFileCardIndexDataService _cardIndexDataService;

        public CandidateInboundEmailFileController(ICandidateFileCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = CandidateFileResources.File;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentCandidateFile;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentCandidateFile;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentCandidateFile;
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.CandidateId = Context.ParentId;

            return dto;
        }

        internal static CommandButton CreateFileFromInboundEmailCommandButton(UrlHelper urlHelper, KnownParameter knownParameters)
        {
            return new CommandButton
            {
                Group = CandidateFileResources.Title,
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile,
                Text = CandidateFileResources.FromInboundEmail,
                OnClickAction = JavaScriptCallAction.ShowDialog(urlHelper.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateInboundEmailFileController>(), KnownParameter.ParentId))
            };
        }
    }
}