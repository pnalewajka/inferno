﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.CandidateSearch")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidate)]
    public class CandidateSearchController : CardIndexController<CandidateSearchResultViewModel, CandidateDto, CandidateSearchContext>
    {
        public CandidateSearchController(ICandidatePickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.ShouldHideSearchBox = true;
        }
    }
}