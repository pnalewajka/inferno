﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentRecommendingPersonContactRecords)]
    public class RecommendingPersonContactRecordsController : SubCardIndexController<RecommendingPersonContactRecordViewModel, RecommendingPersonContactRecordDto, RecommendingPersonController, ParentIdContext>
    {
        private IRecommendingPersonContactRecordCardIndexDataService _cardIndexDataService;

        private readonly IDataActivityLogService _dataActivityLogService;

        public RecommendingPersonContactRecordsController(
            IRecommendingPersonContactRecordCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.Title = RecommendingPersonContactRecordResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentRecommendingPersonContactRecords;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentRecommendingPersonContactRecords;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentRecommendingPersonContactRecords;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentRecommendingPersonContactRecords;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogRecommendingPersonDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.RecommendingPersonContact, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.RecommendingPersonContact, id, GetType().Name);

            return base.Edit(id);
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.RecommendingPersonId = Context.ParentId;

            return dto;
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();
            var configuration = new GridViewConfiguration<RecommendingPersonContactRecordViewModel>(RecommendingPersonContactRecordResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(c => c.RecommendingPersonId);
            configurations.Add(configuration);

            return configurations;
        }
    }
}