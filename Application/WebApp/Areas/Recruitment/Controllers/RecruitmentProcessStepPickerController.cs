﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.RecruitmentProcessStep")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcessSteps)]
    public class RecruitmentProcessStepPickerController : CardIndexController<RecruitmentProcessStepViewModel, RecruitmentProcessStepDto, RecruitmentProcessPickerContext>
    {
        public RecruitmentProcessStepPickerController(IRecruitmentProcessStepCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configuration = new GridViewConfiguration<RecruitmentProcessStepViewModel>(RecruitmentProcessStepResources.Title, string.Empty) { IsDefault = true };
            configuration.ExcludeAllColumns();

            configuration.IncludeColumn(c => c.RecruitmentProcessId);
            configuration.IncludeColumn(c => c.Type);
            configuration.IncludeColumn(c => c.PlannedOn);
            configuration.IncludeColumn(c => c.Status);
            configuration.IncludeColumn(c => c.AssignedEmployeeId);

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}