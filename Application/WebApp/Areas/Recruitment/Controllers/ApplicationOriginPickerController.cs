﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.ApplicationOrigin")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataOrigin)]
    public class ApplicationOriginPickerController : CardIndexController<ApplicationOriginViewModel, ApplicationOriginDto, ApplicationOriginPickerContext>
    {
        public ApplicationOriginPickerController(IApplicationOriginPickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = ApplicationOriginResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentDataOrigin;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentDataOrigin;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentDataOrigin;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentDataOrigin;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        public override ActionResult List(GridParameters parameters)
        {
            if (Context?.OriginType == null)
            {
                CardIndex.Settings.OnBeforeListViewRender += model =>
                {
                    model.RowGrouping = rows => rows
                        .Cast<ApplicationOriginViewModel>()
                        .GroupBy(o => o.GetGroupName());
                };
            }

            return base.List(parameters);
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<ApplicationOriginViewModel>(ApplicationOriginResources.Title, string.Empty) { IsDefault = true };
            configuration.IncludeColumn(c => c.Name);
            configurations.Add(configuration);

            return configurations;
        }
    }
}