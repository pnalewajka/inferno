﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataActivities)]
    public class CandidateDataActivityController : SubCardIndexController<DataActivityViewModel, DataActivityDto, CandidateController, ParentIdContext>
    {
        private ICandidateDataActivityCardIndexDataService _cardIndexDataService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public CandidateDataActivityController(
            ICandidateDataActivityCardIndexDataService cardIndexDataService,
            IDataActivityLogService dataActivityLogService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;
            CardIndex.Settings.Title = DataActivityResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDataActivities;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewDataActivities;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDataActivities;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDataActivities;

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CardIndex.Settings.AddFormCustomization += EditAddFormCustomization;
            CardIndex.Settings.EditFormCustomization += EditAddFormCustomization;
        }

        private Action<FormRenderingModel> EditAddFormCustomization = form =>
        {
            form.GetControlByName<DataActivityViewModel>(m => m.DataOwnerId).IsReadOnly = true;
            form.GetControlByName<DataActivityViewModel>(m => m.ReferenceId).IsReadOnly = true;
            form.GetControlByName<DataActivityViewModel>(m => m.ReferenceType).IsReadOnly = true;
        };

        private object GetDefaultNewRecord()
        {
            return _cardIndexDataService.GetDefaultNewRecord(Context.ParentId);
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.DataActivity, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.DataActivity, id, GetType().Name);

            return base.Edit(id);
        }

    }
}