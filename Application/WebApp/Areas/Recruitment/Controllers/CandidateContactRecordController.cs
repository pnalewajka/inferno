﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentContactRecord)]
    public class CandidateContactRecordController : SubCardIndexController<CandidateContactRecordViewModel, CandidateContactRecordDto, CandidateController, ParentIdContext>
    {
        private ICandidateContactRecordCardIndexDataService _cardIndexDataService;

        private readonly IDataActivityLogService _dataActivityLogService;

        public CandidateContactRecordController(
            ICandidateContactRecordCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.Title = ContactRecordResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentContactRecord;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentContactRecord;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentContactRecord;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentContactRecord;

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.CandidateContact, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.CandidateContact, id, GetType().Name);

            return base.Edit(id);
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.CandidateId = Context.ParentId;

            return dto;
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configuration = new GridViewConfiguration<CandidateContactRecordViewModel>(ContactRecordResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(c => c.CandidateId);

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}