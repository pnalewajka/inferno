﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataOrigin)]
    public class ApplicationOriginController : CardIndexController<ApplicationOriginViewModel, ApplicationOriginDto>
    {
        public ApplicationOriginController(IApplicationOriginCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = ApplicationOriginResources.Title;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentDataOrigin;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentDataOrigin;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentDataOrigin;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentDataOrigin;

            CardIndex.Settings.OnBeforeListViewRender += model =>
            {
                model.RowGrouping = rows => rows
                .Cast<ApplicationOriginViewModel>()
                .GroupBy(o => o.GetGroupName());
            };

            CardIndex.Settings.AllowMultipleRowSelection = false;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }
    }
}