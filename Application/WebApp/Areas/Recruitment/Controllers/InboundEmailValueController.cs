﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize]
    public class InboundEmailValueController : SubCardIndexController<InboundEmailValueViewModel, InboundEmailValueDto,
        InboundEmailController, ParentIdContext>
    {
        private readonly IInboundEmailValueCardIndexDataService _cardIndexDataService;

        public InboundEmailValueController(
            IInboundEmailValueCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

        }
    }
}