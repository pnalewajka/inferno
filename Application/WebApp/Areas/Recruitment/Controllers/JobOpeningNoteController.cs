﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentJobOpeningNotes)]
    public class JobOpeningNoteController : SubCardIndexController<JobOpeningNoteViewModel, JobOpeningNoteDto, JobOpeningController, ParentIdContext>
    {
        private readonly IJobOpeningNoteCardIndexDataService _cardIndexDataService;

        public JobOpeningNoteController(
            IJobOpeningNoteCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = JobOpeningNoteResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpeningNotes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentJobOpeningNotes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentJobOpeningNotes;

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<JobOpeningNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<JobOpeningNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.ViewFormCustomization = form =>
            {
                form.GetControlByName<JobOpeningNoteViewModel>(m => m.RichContent).Visibility = ControlVisibility.Hide;
            };
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();

            dto.JobOpeningId = Context.ParentId;
            dto.NoteType = NoteType.UserNote;
            dto.NoteVisibility = NoteVisibility.Public;

            return dto;
        }
    }
}