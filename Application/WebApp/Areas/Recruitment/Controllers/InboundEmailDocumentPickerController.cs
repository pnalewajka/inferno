﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.InboundEmailDocument")]
    [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
    public class InboundEmailDocumentPickerController :
        ReadOnlyCardIndexController<InboundEmailDocumentViewModel, InboundEmailDocumentDto, InboundEmailPickerContext>
    {
        public InboundEmailDocumentPickerController(IInboundEmailDocumentPickerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.OnBeforeListViewRender += model =>
            {
                model.RowGrouping = rows => rows
                .Cast<InboundEmailDocumentViewModel>()
                .GroupBy(o => o.InboundEmailLabel);
            };

            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configuration = new GridViewConfiguration<InboundEmailDocumentViewModel>(InboundEmailResources.InboundEmailsName, "")
            {
                IsDefault = true
            };

            configuration.IncludeColumns(new List<Expression<Func<InboundEmailDocumentViewModel, object>>>
            {
                c => c.ContentType,
                c => c.DocumentName
            });

            return new List<IGridViewConfiguration>
            {
                configuration
            };
        }
    }
}