﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecommendingPersonNotes)]
    public class RecommendingPersonNoteController : SubCardIndexController<RecommendingPersonNoteViewModel, RecommendingPersonNoteDto, RecommendingPersonController, ParentIdContext>
    {
        private readonly IRecommendingPersonNoteCardIndexDataService _cardIndexDataService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public RecommendingPersonNoteController(
            IRecommendingPersonNoteCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.Title = RecommendingPersonNoteResources.Title;

            CardIndex.Settings.DefaultNewRecord = DefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecommendingPersonNotes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecommendingPersonNotes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecommendingPersonNotes;

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<RecommendingPersonNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<RecommendingPersonNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.ViewFormCustomization = form =>
            {
                form.GetControlByName<RecommendingPersonNoteViewModel>(m => m.RichContent).Visibility = ControlVisibility.Hide;
            };

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogRecommendingPersonDataActivity(Context.ParentId, ActivityType.ViewedRecord,
                GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(Context.ParentId, ActivityType.ViewedRecord,
                ReferenceType.RecommendingPersonNote, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(Context.ParentId, ActivityType.ViewedRecord,
                ReferenceType.RecommendingPersonNote, id, GetType().Name);

            return base.Edit(id);
        }

        private object DefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.RecommendingPersonId = Context.ParentId;
            dto.NoteType = NoteType.UserNote;
            dto.NoteVisibility = NoteVisibility.Public;

            return dto;
        }
    }
}