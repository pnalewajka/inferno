﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
    public class CandidateDataConsentController : SubCardIndexController<DataConsentViewModel, DataConsentDto, CandidateController, ParentIdContext>
    {
        private ICandidateDataConsentCardIndexDataService _cardIndexDataService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public CandidateDataConsentController(
            ICandidateDataConsentCardIndexDataService cardIndexDataService,
            IDataActivityLogService dataActivityLogService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;
            CardIndex.Settings.Title = DataConsentResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/DocumentViewer.js");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentDataConsent;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentDataConsent;

            DataConsentController.ApplyFormConfiguration(CardIndex.Settings);
            DataConsentController.SetAddDataConsentDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this), nameof(GetAddDialog));
        }

        private object GetDefaultNewRecord()
        {
            var consentTypeName = Request.QueryString[DataConsentController.ConsentTypeParameterName];
            var consentType = EnumHelper.GetEnumValueOrDefault<DataConsentType>(consentTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (consentType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            return _cardIndexDataService.GetDefaultNewRecord(Context.ParentId, consentType.Value);
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.DataConsent, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.DataConsent, id, GetType().Name);

            return base.Edit(id);
        }
    }
}