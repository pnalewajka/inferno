﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateNotes)]
    public class CandidateNoteController : SubCardIndexController<CandidateNoteViewModel, CandidateNoteDto, CandidateController, ParentIdContext>
    {
        private readonly ICandidateNoteCardIndexDataService _cardIndexDataService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public CandidateNoteController(
            ICandidateNoteCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.Title = CandidateNoteResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CustomizeToolbarButtons();

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<CandidateNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<CandidateNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.ViewFormCustomization = form =>
            {
                form.GetControlByName<CandidateNoteViewModel>(m => m.RichContent).Visibility = ControlVisibility.Hide;
            };
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.CandidateNote, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.CandidateNote, id, GetType().Name);

            return base.Edit(id);
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();

            dto.CandidateId = Context.ParentId;
            dto.NoteType = NoteType.UserNote;
            dto.NoteVisibility = NoteVisibility.Public;

            return dto;
        }

        private void CustomizeToolbarButtons()
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateNotes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentCandidateNotes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentCandidateNotes;

            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = $"row.noteType!=={(int)NoteType.SystemNote}"
            };
        }
    }
}