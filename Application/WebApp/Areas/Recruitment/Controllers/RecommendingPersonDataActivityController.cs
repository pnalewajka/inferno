﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewDataActivities)]
    public class RecommendingPersonDataActivityController : SubCardIndexController<DataActivityViewModel, DataActivityDto, RecommendingPersonController, ParentIdContext>
    {
        private IRecommendingPersonDataActivityCardIndexDataService _cardIndexDataService;

        public RecommendingPersonDataActivityController(
            IRecommendingPersonDataActivityCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = DataActivityResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddDataActivities;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewDataActivities;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditDataActivities;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteDataActivities;

            CardIndex.Settings.AddFormCustomization += EditAddFormCustomization;
            CardIndex.Settings.EditFormCustomization += EditAddFormCustomization;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private Action<FormRenderingModel> EditAddFormCustomization = form =>
        {
            form.GetControlByName<DataActivityViewModel>(m => m.DataOwnerId).IsReadOnly = true;
            form.GetControlByName<DataActivityViewModel>(m => m.ReferenceId).IsReadOnly = true;
            form.GetControlByName<DataActivityViewModel>(m => m.ReferenceType).IsReadOnly = true;
        };

        private object GetDefaultNewRecord()
        {
            return _cardIndexDataService.GetDefaultNewRecord(Context.ParentId);
        }
    }
}