﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateFile)]
    public class CandidateFileController : SubCardIndexController<CandidateFileViewModel, CandidateFileDto, CandidateController, ParentIdContext>
    {
        private ICandidateFileCardIndexDataService _cardIndexDataService;
        private readonly ICandidateFileService _candidateFileService;
        private readonly IDocumentDownloadService _documentDownloadService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICustomerCardIndexDataService _customerService;

        public CandidateFileController(
            ICandidateFileCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            ICandidateFileService candidateFileService,
            IDocumentDownloadService documentDownloadService,
            IDataActivityLogService dataActivityLogService,
            ICustomerCardIndexDataService customerService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _candidateFileService = candidateFileService;
            _documentDownloadService = documentDownloadService;
            _dataActivityLogService = dataActivityLogService;
            _customerService = customerService;

            CardIndex.Settings.Title = CandidateFileResources.File;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile;

            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentCandidateFile;
            CardIndex.Settings.EditButton.Availability = GetDataConsentsButtonAvailability();

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentCandidateFile;
            CardIndex.Settings.ViewButton.Availability = GetDataConsentsButtonAvailability();

            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentCandidateFile;
            CardIndex.Settings.DeleteButton.Availability = GetDataConsentsButtonAvailability();

            CardIndex.Settings.ToolbarButtons.AddRange(GetViewFileCommandButton());

            Layout.Css.Add("~/Areas/Recruitment/Content/DocumentViewer.css");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/DocumentViewer.js");
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.CandidateFile, id, GetType().Name);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.CandidateFile, id, GetType().Name);

            return base.Edit(id);
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.CandidateId = Context.ParentId;

            return dto;
        }

        internal static CommandButton CreateFileFromDiskCommandButton(UrlHelper urlHelper, KnownParameter knownParameters)
        {
            return new CommandButton
            {
                Group = CandidateFileResources.Title,
                RequiredRole = SecurityRoleType.CanAddRecruitmentCandidateFile,
                Text = CandidateFileResources.FromDisk,
                OnClickAction = JavaScriptCallAction.ShowDialog(urlHelper.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<CandidateFileController>(), KnownParameter.ParentId))
            };
        }

        private IEnumerable<CommandButton> GetViewFileCommandButton()
        {
            var gdprFileAction = new JavascriptPredicatedAction(Url.UriActionGet(nameof(ViewFile), "DataConsent", nameof(GDPR), KnownParameter.SelectedId), "row.isDataConsent");
            var candidateFile = new JavascriptPredicatedAction(Url.UriActionGet(nameof(ViewFile), KnownParameter.SelectedId));

            var viewFileAction = new JavascriptPredicatedActions(new[] { gdprFileAction, candidateFile });

            yield return new ToolbarButton
            {
                Text = CandidateFileResources.ViewFile,
                OnClickAction = viewFileAction.ToJavascriptDialogAction(),
                RequiredRole = SecurityRoleType.CanViewRecruitmentCandidateFile,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        public ActionResult ViewDocument(long id)
        {
            TempData[GetTempDataKey(id)] = DownloadJustification.CreateViewedDocumentDownloadJustification();

            Layout.MasterName = null;

            var viewModel = new DocumentViewModel
            {
                DocumentId = id,
                DocumentName = _candidateFileService.GetDocumentNameByDocumentId(id)
            };

            var dialogModel = new DocumentViewerViewModel(viewModel);

            return PartialView("CandidateDocumentViewer", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        public ActionResult ViewFile(long id)
        {
            var documentId = _candidateFileService.GetCandidateFileDocumentIdByFileId(id);

            return ViewDocument(documentId);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        public ActionResult Download(long id)
        {
            return Download(new DownloadJustificationViewModel(id));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        public ActionResult PerformDownload(DownloadJustificationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Download(model);
            }

            var customer = _customerService.GetRecordById(model.CustomerId);

            TempData[GetTempDataKey(model.DocumentId)] = DownloadJustification.CreateDocumentSharedWithCustomerDownloadJustification(customer, model.Comment);

            return DialogHelper.Redirect(this, Url.Action(nameof(PerformDownload), new { Id = model.DocumentId }));
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        public ActionResult PerformDownload(long id)
        {
            var candidateId = _candidateFileService.GetCandidateIdByDocumentId(id);
            var tempDataKey = GetTempDataKey(id);
            var downloadJustification = TempData[tempDataKey] as DownloadJustification;

            if (Request?.UrlReferrer?.LocalPath == "/Scripts/ViewerJS/")
            {
                TempData.Keep(tempDataKey);
            }

            if (downloadJustification == null)
            {
                AddAlert(AlertType.Error, CandidateFileResources.NoDownloadJustificationError);

                _dataActivityLogService.LogCandidateDataActivity(
                    candidateId,
                    ActivityType.UnjustifiedDownloadAttempt,
                    GetType().Name);

                return RedirectToAction(nameof(View), new RouteValueDictionary { { "id", id }, { RoutingHelper.ParentIdParameterName, candidateId } });
            }

            var documentDownloadDto = _documentDownloadService.GetDocument("Documents.CandidateFileDocumentMapping", id);

            _dataActivityLogService.LogCandidateDataActivity(
                candidateId,
                downloadJustification.ActivityType,
                GetType().Name,
                documentDownloadDto.DocumentName,
                downloadJustification.Text);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }

        private ActionResult Download(DownloadJustificationViewModel model)
        {
            var documentName = _candidateFileService.GetDocumentNameByDocumentId(model.DocumentId);

            var dialogViewModel = new ModelBasedDialogViewModel<DownloadJustificationViewModel>(model)
            {
                Title = string.Format(CandidateFileResources.DocumentShareDialogTitle, documentName),
                LabelWidth = LabelWidth.Large
            };

            return PartialView(
                    "CandidateDownloadJustification",
                    dialogViewModel
                    );
        }

        private string GetTempDataKey(long id) => $"document-download-justification:{id}";

        private ToolbarButtonAvailability GetDataConsentsButtonAvailability() =>
            new ToolbarButtonAvailability
            {
                Predicate = "!row.isDataConsent"
            };
    }
}