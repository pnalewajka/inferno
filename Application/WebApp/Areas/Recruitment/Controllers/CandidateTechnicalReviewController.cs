﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.TechnicalReview;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentTechnicalReviews)]
    public class CandidateTechnicalReviewController : SubCardIndexController<TechnicalReviewViewModel, TechnicalReviewDto, CandidateController, ParentIdContext>
    {
        private readonly IDataActivityLogService _dataActivityLogService;

        public CandidateTechnicalReviewController(
            ICandidateTechnicalReviewCardIndexDataService cardIndexDataService,
            IDataActivityLogService dataActivityLogService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = TechnicalReviewResources.Title;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentTechnicalReviews;

            _dataActivityLogService = dataActivityLogService;

            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.TechnicalReview, id, GetType().Name);

            return base.View(id);
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configurations = new List<IGridViewConfiguration>();
            var configuration = new GridViewConfiguration<TechnicalReviewViewModel>(TechnicalReviewResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            configuration.ExcludeColumn(c => c.CandidateFullName);

            configurations.Add(configuration);

            return configurations;
        }
    }
}