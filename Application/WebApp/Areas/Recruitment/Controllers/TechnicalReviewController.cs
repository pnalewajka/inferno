﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.TechnicalReview;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentTechnicalReviews)]
    public class TechnicalReviewController : CardIndexController<TechnicalReviewViewModel, TechnicalReviewDto>
    {
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ITechnicalReviewCardIndexDataService _technicalReviewCardIndexDataService;

        public TechnicalReviewController(ITechnicalReviewCardIndexDataService cardIndexDataService,
            IDataActivityLogService dataActivityLogService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = TechnicalReviewResources.Title;

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentTechnicalReviews;

            CardIndex.Settings.AllowMultipleRowSelection = false;

            _dataActivityLogService = dataActivityLogService;
            _technicalReviewCardIndexDataService = cardIndexDataService;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            var candidateId = _technicalReviewCardIndexDataService.GetCandidateId(id);

            _dataActivityLogService.LogCandidateDataActivity(candidateId, ActivityType.ViewedRecord, ReferenceType.TechnicalReview, id, GetType().Name);

            return base.View(id);
        }
    }
}