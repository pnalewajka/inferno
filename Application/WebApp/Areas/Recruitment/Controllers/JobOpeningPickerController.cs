﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("JobOpeningPickers.JobOpening")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentJobOpening)]
    public class JobOpeningPickerController : CardIndexController<JobOpeningViewModel, JobOpeningDto, JobOpeningPickerContext>
    {
        private const string MinimalViewId = "minimal";

        public JobOpeningPickerController(
            IJobOpeningPickerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<JobOpeningViewModel>(JobOpeningResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<JobOpeningViewModel, object>>>
            {
                o => o.PositionName,
                o => o.JobProfileIds,
                o => o.CityIds,
                o=> o.DecisionMakerId
            });
            configurations.Add(configuration);

            return configurations;
        }

    }
}