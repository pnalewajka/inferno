﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcess)]
    public class RecruitmentProcessQucikAddController : CardIndexController<RecruitmentProcessQuickAddViewModel, RecruitmentProcessDto, ParentIdContext>
    {
        private IRecruitmentProcessCardIndexDataService _cardIndexDataService;

        public RecruitmentProcessQucikAddController(IRecruitmentProcessCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            CardIndex.Settings.Title = RecruitmentProcessResources.Title;
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
        }

        public object GetDefaultNewRecord()
        {
            var defaultNewRecord = _cardIndexDataService.GetDefaultNewRecord();
            defaultNewRecord.CandidateId = Context.ParentId;
            defaultNewRecord.RecruiterId = GetCurrentPrincipal().EmployeeId;
            return defaultNewRecord;
        }
    }
}
