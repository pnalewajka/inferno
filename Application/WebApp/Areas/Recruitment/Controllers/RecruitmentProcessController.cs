﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Layout;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers.Buttons;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;


namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcess)]
    [Identifier("RecruitmentController.RecruitmentProcessController")]
    [PerformanceTest(nameof(RecruitmentProcessController.List))]
    public class RecruitmentProcessController : CardIndexController<RecruitmentProcessViewModel, RecruitmentProcessDto>
    {
        private const string RecruitmentProcessStepsGroup = "recruitment-process-steps";
        private const string NotesGroup = "notes";
        private const string WatchButtonsGroup = "watch-buttons";
        private const string CloseGroup = "close-button";
        private const string HoldGroup = "hold-buttons";
        private const string ReopenButtonGroup = "reopen";

        private readonly IRecruitmentProcessService _recruitmentProcessService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IRecruitmentProcessCardIndexDataService _cardIndexDataService;

        public RecruitmentProcessController(
            IRecruitmentProcessCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IRecruitmentProcessService recruitmentProcessService,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;
            _recruitmentProcessService = recruitmentProcessService;
            _dataActivityLogService = dataActivityLogService;
            _cardIndexDataService = cardIndexDataService;
            ConfigureRecruitmentProcessController(CardIndex, Url, Layout, GetCurrentPrincipal());

            CardIndex.Settings.ToolbarButtons.AddRange(RecruitmentProcessButtons.GetToolbarButtons(Url));
            CardIndex.Settings.RowButtons.AddRange(RecruitmentProcessButtons.GetRowButtons(Url));
            CardIndex.Settings.EditFormCustomization += SetReadOnlyFieldIfNecessary;

            Layout.Resources.AddFrom<RecruitmentProcessResources>(nameof(RecruitmentProcessResources.NoteDialogTitle));
            Layout.Resources.AddFrom<RecruitmentProcessResources>(nameof(RecruitmentProcessResources.ReopenProcessDialogTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ToolbarButtonConfirmationDialogTitle));

            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.FilterGroups.Add(GetSimpleFilterGroup());
            CardIndex.Settings.FilterGroups.Add(GetMaturedProcessesFilterGroup());

            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(id),
                ActivityType.ViewedRecord, ReferenceType.RecruitmentProcess, id, GetType().Name);
            ConfigureViewButtons(CardIndex, GetCurrentPrincipal(), id);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(id),
                ActivityType.ViewedRecord, ReferenceType.RecruitmentProcess, id, GetType().Name);
            ConfiguteEditButtons(CardIndex, GetCurrentPrincipal(), id);

            return base.Edit(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcess)]
        public ActionResult WatchRecruitmentProcess(long[] ids)
        {
            _recruitmentProcessService.ToggleWatching(ids, GetCurrentPrincipal().EmployeeId, WatchingStatus.Watch);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcess)]
        public ActionResult UnwatchRecruitmentProcess(long[] ids)
        {
            _recruitmentProcessService.ToggleWatching(ids, GetCurrentPrincipal().EmployeeId, WatchingStatus.Unwatch);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanCloseRecruitmentProcesses)]
        public ActionResult CloseRecruitmentProcess(RecruitmentProcessCloseViewModel viewModel)
        {
            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(viewModel.Id),
               ActivityType.UpdatedRecord, ReferenceType.RecruitmentProcess, viewModel.Id, GetType().Name);

            var recruitmentProcessDto = _classMappingFactory.CreateMapping<RecruitmentProcessCloseViewModel, RecruitmentProcessCloseDto>().CreateFromSource(viewModel);

            _recruitmentProcessService.CloseRecruitmentProcess(recruitmentProcessDto);

            var listActionUrl = CardIndexHelper.GetListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        public static void ConfigureViewButtons(CardIndex<RecruitmentProcessViewModel, RecruitmentProcessDto> cardIndex, IAtomicPrincipal principal, long id)
        {
            var recruitmentProcess = cardIndex.GetRecordById(id);

            if (recruitmentProcess.WatcherIds.Any(w => w == principal.EmployeeId))
            {
                cardIndex.ConfigureViewButtons += CreateButtonAction(
                    nameof(RecruitmentProcessWorkflowAction.Unwatch),
                    RecruitmentProcessResources.UnwatchButtonLabel,
                    $"RecruitmentProcess.InvokeActionFromViewForm('{nameof(RecruitmentProcessController.UnwatchRecruitmentProcess)}', {id.ToString()});",
                    SecurityRoleType.CanBeRecruitmentDataWatcher);
            }
            else
            {
                cardIndex.ConfigureViewButtons += CreateButtonAction(
                    nameof(RecruitmentProcessWorkflowAction.Watch),
                    RecruitmentProcessResources.WatchButtonLabel,
                    $"RecruitmentProcess.InvokeActionFromViewForm('{nameof(RecruitmentProcessController.WatchRecruitmentProcess)}', {id.ToString()});",
                    SecurityRoleType.CanBeRecruitmentDataWatcher);
            }

            if (recruitmentProcess.Status != RecruitmentProcessStatus.Closed && principal.IsInRole(SecurityRoleType.CanCloseRecruitmentProcesses))
            {
                cardIndex.ConfigureViewButtons += CreateButtonAction(
                    nameof(RecruitmentProcessCloseViewModel),
                    RecruitmentProcessResources.CloseButtonLabel,
                    $"RecruitmentProcess.CloseFromView(event.currentTarget, {id}, 'Models.{ nameof(RecruitmentProcessCloseViewModel) }');",
                    SecurityRoleType.CanCloseRecruitmentProcesses);
            }

            cardIndex.ConfigureViewButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id, principal, recruitmentProcess));
        }

        public static void ConfiguteEditButtons(CardIndex<RecruitmentProcessViewModel, RecruitmentProcessDto> cardIndex, IAtomicPrincipal principal, long id)
        {
            var recruitmentProcess = cardIndex.GetRecordById(id);

            if (recruitmentProcess.WatcherIds.Any(w => w == principal.EmployeeId))
            {
                cardIndex.ConfigureEditButtons += CreateEditWorkflowAction(
                    nameof(RecruitmentProcessWorkflowAction.Unwatch),
                    RecruitmentProcessResources.UnwatchButtonLabel,
                    RecruitmentProcessWorkflowAction.Unwatch);
            }
            else
            {
                cardIndex.ConfigureEditButtons += CreateEditWorkflowAction(
                    nameof(RecruitmentProcessWorkflowAction.Watch),
                    RecruitmentProcessResources.WatchButtonLabel,
                    RecruitmentProcessWorkflowAction.Watch);
            }

            if (recruitmentProcess.Status != RecruitmentProcessStatus.Closed && principal.IsInRole(SecurityRoleType.CanCloseRecruitmentProcesses))
            {
                cardIndex.ConfigureEditButtons += CreateButtonAction(
                    nameof(RecruitmentProcessCloseViewModel),
                    RecruitmentProcessResources.CloseButtonLabel,
                    $"RecruitmentProcess.CloseFromEditForm(event.currentTarget, {id}, 'Models.{ nameof(RecruitmentProcessCloseViewModel) }', {(int)RecruitmentProcessWorkflowAction.Close});",
                    SecurityRoleType.CanCloseRecruitmentProcesses);
            }

            cardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id, principal, recruitmentProcess));
        }

        public static void ConfigureRecruitmentProcessController(
            CardIndex<RecruitmentProcessViewModel, RecruitmentProcessDto> cardIndex,
            UrlHelper urlHelper,
            LayoutViewModel layout,
            IAtomicPrincipal principal)
        {
            cardIndex.Settings.Title = RecruitmentProcessResources.Title;

            cardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentProcess;
            cardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentProcess;
            cardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentProcess;
            cardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentProcess;

            layout.Scripts.Add("~/Areas/Recruitment/Scripts/RecruitmentProcess.js");

            layout.Resources.AddFrom<RecruitmentProcessResources>(nameof(RecruitmentProcessResources.CloseDialogTitle));
            layout.Resources.AddFrom<RecruitmentProcessResources>(nameof(RecruitmentProcessResources.NoteDialogTitle));
            layout.Resources.AddFrom<RecruitmentProcessResources>(nameof(RecruitmentProcessResources.JobOpeningPickerTitle));

            cardIndex.Settings.ViewButton.IsDefault = false;
            cardIndex.Settings.EditButton.IsDefault = false;

            cardIndex.Settings.AddFormCustomization += AddFormCustomization;
            cardIndex.Settings.ViewFormCustomization += EditViewFormCustomization;
            cardIndex.Settings.EditFormCustomization += EditViewFormCustomization;

            cardIndex.ConfigureEditButtons += (CardIndexRecordViewModel<RecruitmentProcessViewModel> model) =>
            {
                if (model.ViewModelRecord.Status == RecruitmentProcessStatus.Closed)
                {
                    model.Buttons.Remove(model.Buttons.GetEditButton());
                }
            };
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var process = (RecruitmentProcessViewModel)base.GetViewModelFromContext(childContext);

            var processUrl = Url.UriActionGet<RecruitmentProcessController>(c => c.Details(process.Id), KnownParameter.None, RoutingHelper.IdParameterName, process.Id.ToString()).Url;

            var panelTitleText = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewRecruitmentProcess)
                ? $"<a href=\"{processUrl}\" target=\"_blank\">{process}</a>"
                : $"{process}";

            var panel = new PanelViewModel(panelTitleText);

            panel.AddProperty(RecruitmentProcessResources.Status, process.Status.GetDescription());

            if (!string.IsNullOrWhiteSpace(process.RecruiterName))
            {
                panel.AddProperty(RecruitmentProcessResources.RecruiterId, process.RecruiterName);
            }

            if (!string.IsNullOrWhiteSpace(process.DecisionMakerName))
            {
                panel.AddProperty(JobOpeningResources.DecisionMaker, process.DecisionMakerName);
            }

            if (process.Status == RecruitmentProcessStatus.Closed)
            {
                panel.AddProperty(RecruitmentProcessResources.ClosedOn, $"{process.ClosedOn:d}");

                if (process.ClosedReason.HasValue)
                {
                    panel.AddProperty(RecruitmentProcessResources.ClosedReason, process.ClosedReason.GetDescription());
                }
            }

            return panel;
        }


        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configuration = new GridViewConfiguration<RecruitmentProcessViewModel>(RecruitmentProcessResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();

            var currentPrincipal = GetCurrentPrincipal();

            if (currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentCandidate))
            {
                configuration.ExcludeColumn(m => m.CandidateId);
            }
            else
            {
                configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetCandidateLink));
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentJobOpening))
            {
                configuration.ExcludeColumn(m => m.JobOpeningId);
            }
            else
            {
                configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetJobOpeningLink));
            }

            return new List<IGridViewConfiguration> { configuration };
        }

        private static void AddFormCustomization(FormRenderingModel form)
        {
            var model = form.GetViewModel<RecruitmentProcessViewModel>();

            form.HideControlsByTabId(RecruitmentProcessViewModel.ProcessTab);
            form.HideControlsByTabId(RecruitmentProcessViewModel.ScreeningTab);
            form.HideControlsByTabId(RecruitmentProcessViewModel.OfferTab);
            form.HideControlsByTabId(RecruitmentProcessViewModel.FinalTab);
            form.HideControlsByTabId(RecruitmentProcessViewModel.NoteTab);
            form.HideControlsByTabId(RecruitmentProcessViewModel.ContributorTab);
            form.HideControlsByTabId(RecruitmentProcessViewModel.StepsTab);

            form.Controls.ToList().ForEach(c => c.TabId = null);

            form.GetControlByName<RecruitmentProcessViewModel>(p => p.StepInfos).Visibility = ControlVisibility.Remove;

            AdjustStatusControlsVisibility(model, form);
        }

        private static void EditViewFormCustomization(FormRenderingModel form)
        {
            form.GetControlByName<RecruitmentProcessViewModel>(p => p.CandidateId).IsReadOnly = true;
            form.GetControlByName<RecruitmentProcessViewModel>(p => p.JobOpeningId).IsReadOnly = true;

            var model = form.GetViewModel<RecruitmentProcessViewModel>();

            if (!model.IsReadyToShowScreeningData)
            {
                form.HideControlsByTabId(RecruitmentProcessViewModel.ScreeningTab);
            }

            if (!model.IsReadyToShowOfferData)
            {
                form.HideControlsByTabId(RecruitmentProcessViewModel.OfferTab);
            }

            if (!model.IsReadyToShowFinalData)
            {
                form.HideControlsByTabId(RecruitmentProcessViewModel.FinalTab);
            }

            if (model.Status == RecruitmentProcessStatus.Closed)
            {
                form.Controls.ToList().ForEach(c => c.IsReadOnly = true);
            }

            AdjustStatusControlsVisibility(model, form);
        }

        private static void AdjustStatusControlsVisibility(RecruitmentProcessViewModel model, FormRenderingModel form)
        {
            if (model.ClosedReason == null)
            {
                form.GetControlByName<RecruitmentProcessViewModel>(p => p.ClosedReason).Visibility = ControlVisibility.Hide;
                form.GetControlByName<RecruitmentProcessViewModel>(p => p.ClosedOn).Visibility = ControlVisibility.Hide;
                form.GetControlByName<RecruitmentProcessViewModel>(p => p.ClosedComment).Visibility = ControlVisibility.Hide;
            }
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanPutRecruitmentProcessOnHold)]
        public ActionResult PutOnHoldRecruitmentProcess(long id)
        {
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(id),
                ActivityType.UpdatedRecord, ReferenceType.RecruitmentProcess, id, GetType().Name);

            _recruitmentProcessService.PutRecruitmentProcessOnHold(id, null);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanTakeRecruitmentProcessOffHold)]
        public ActionResult TakeOffHoldRecruitmentProcess(long id)
        {
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(id),
                ActivityType.UpdatedRecord, ReferenceType.RecruitmentProcess, id, GetType().Name);

            _recruitmentProcessService.TakeRecruitmentProcessOffHold(id);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanCloneRecruitmentProcess)]
        public void Clone(long id, long jobOpeningId)
        {
            var listActionUrl = CardIndexHelper.GetListUrl(this, Context);

            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(id),
                ActivityType.CreatedRecord, ReferenceType.RecruitmentProcess, id, GetType().Name);

            var cloneResult = _recruitmentProcessService.CloneRecruitmentProcess(id, jobOpeningId);

            AddAlerts(cloneResult.Alerts);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddRecruitmentProcessNotes)]
        public void AddNote(AddNoteViewModel viewModel)
        {
            var dialogNoteDto = _classMappingFactory.CreateMapping<AddNoteViewModel, AddNoteDto>().CreateFromSource(viewModel);

            _recruitmentProcessService.AddNoteToRecruitmentProcess(dialogNoteDto);
        }

        [AtomicAuthorize(SecurityRoleType.CanReopenRecruitmentProcesses)]
        public ActionResult ReopenProcess(RecruitmentProcessReopenViewModel viewModel)
        {
            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(viewModel.Id),
               ActivityType.UpdatedRecord, ReferenceType.RecruitmentProcess, viewModel.Id, GetType().Name);

            var recruitmentProcessReopenDto = _classMappingFactory.CreateMapping<RecruitmentProcessReopenViewModel, RecruitmentProcessReopenDto>().CreateFromSource(viewModel);

            var reopenProcessResult = _recruitmentProcessService.ReopenProcess(recruitmentProcessReopenDto);
            AddAlerts(reopenProcessResult.Alerts);

            return RedirectToAction(nameof(List));
        }



        private static IList<IFooterButton> GetAvaliableButtons(long recruitmentProcessId, IAtomicPrincipal principal, RecruitmentProcessViewModel recruitmentProcess)
        {
            var avaliableButtons = new List<IFooterButton>();

            if (principal.IsInRole(SecurityRoleType.CanAddRecruitmentProcessNotes))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(AddNoteViewModel), RecruitmentProcessResources.NewNoteButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.addNoteFromViewForm(this, {recruitmentProcessId}, 'Models.{ nameof(AddNoteViewModel) }');"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Plus
                    }
                );
            }

            if (recruitmentProcess.Status == RecruitmentProcessStatus.Closed && principal.IsInRole(SecurityRoleType.CanReopenRecruitmentProcesses))
            {
                avaliableButtons.Add
                (
                    new FooterButton(ReopenButtonGroup, RecruitmentProcessResources.ReOpenCommandButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.ReopenProcess({recruitmentProcessId}, 'Models.{ nameof(RecruitmentProcessReopenViewModel) }');"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Refresh
                    }
                );
            }

            return avaliableButtons;
        }

        private static Action<CardIndexRecordViewModel<RecruitmentProcessViewModel>> CreateButtonAction(string name, string text, string onClickJavaScript, SecurityRoleType roleType)
        {
            return model =>
            {
                var button = new FooterButton(name, text)
                {
                    OnClickAction = new JavaScriptCallAction(onClickJavaScript),
                    Attributes = FooterButtonBuilder.SubmitFormButtonAttribures(),
                    RequiredRole = roleType
                };

                model.Buttons.Add(button);
            };
        }

        private static Action<CardIndexRecordViewModel<RecruitmentProcessViewModel>> CreateEditWorkflowAction(string name, string displayName, RecruitmentProcessWorkflowAction action, string confirmationTitle = null, string confirmationContent = null)
        {
            return CreateWorkflowAction(FooterButtonBuilder.EditButton(), name, displayName, action, confirmationTitle, confirmationContent);
        }

        private static Action<CardIndexRecordViewModel<RecruitmentProcessViewModel>> CreateWorkflowAction(IFooterButton button, string name, string displayName, RecruitmentProcessWorkflowAction action, string confirmationTitle = null, string confirmationContent = null)
        {
            return model =>
            {
                button.Id = name;
                button
                    .SetDisplayText(displayName)
                    .SetSubmitParameter<RecruitmentProcessViewModel>((int)action, a => a.WorkflowAction);

                if (!string.IsNullOrEmpty(confirmationTitle) && !string.IsNullOrEmpty(confirmationContent))
                {
                    button.SetRequireConfirmation(confirmationTitle, confirmationContent);
                }

                model.Buttons.Add(button);
            };
        }

        private void SetReadOnlyFieldIfNecessary(FormRenderingModel form)
        {
            var currentUser = GetCurrentPrincipal();

            if (!currentUser.IsInRole(SecurityRoleType.CanEditRecruitmentProcessCoordinators))
            {
                form.GetControlByName<RecruitmentProcessViewModel>(j => j.RecruiterId).IsReadOnly = true;
            }

            if (!currentUser.IsInRole(SecurityRoleType.CanEditRecruitmentProcessOfferDetails))
            {
                MakeReadonlyPropertiesInTab(form, "offer");
            }

            if (!currentUser.IsInRole(SecurityRoleType.CanEditRecruitmentProcessFinalDetails))
            {
                MakeReadonlyPropertiesInTab(form, "final");
            }
        }

        private void MakeReadonlyPropertiesInTab(FormRenderingModel form, string tabName)
        {
            foreach (var control in form.Controls.Where(c => c.TabId == tabName))
            {
                control.IsReadOnly = true;
            }
        }

        protected FilterGroup GetSimpleFilterGroup()
        {
            return new FilterGroup
            {
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.Grid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.MyRecords, DisplayName = RecruitmentProcessResources.MyRecordsFilterLabel},
                    new Filter {Code = FilterCodes.AnyOwner, DisplayName = RecruitmentProcessResources.AnyOwnerFilterLabel}
                }
            };
        }

        protected FilterGroup GetMaturedProcessesFilterGroup()
        {
            return new FilterGroup
            {
                Type = FilterGroupType.RadioGroup,
                Visibility = CommandButtonVisibility.Grid,
                Filters = new List<Filter>
                {
                    new Filter {Code = FilterCodes.AnyMaturityWillDo, DisplayName = RecruitmentProcessResources.AnyMaturityFilterLabel},
                    new Filter {Code = FilterCodes.MaturedProcessesOnly, DisplayName = RecruitmentProcessResources.MaturedProcessesOnlyFilterLabel},
                }
            };
        }
    }
}