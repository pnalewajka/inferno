﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.Candidate")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidate)]
    public class CandidatePickerController : ReadOnlyCardIndexController<CandidateViewModel, CandidateDto, CandidatePickerContext>
    {
        private const string MinimalViewId = "minimal";

        public CandidatePickerController(ICandidatePickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<CandidateViewModel>(CandidateResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<CandidateViewModel, object>>>
            {
                c => c.FirstName,
                c => c.LastName
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}