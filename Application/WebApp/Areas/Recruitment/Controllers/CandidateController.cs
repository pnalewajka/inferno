﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers.Buttons;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidate)]
    [Identifier("RecruitmentController.CandidateController")]
    [PerformanceTest(nameof(CandidateController.List))]
    public class CandidateController : CardIndexController<CandidateViewModel, CandidateDto>
    {
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICandidateService _candidateService;
        private readonly ICandidateCardIndexDataService _cardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMappingFactory _mappingFactory;

        public CandidateController(
            ICandidateCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService,
            IPrincipalProvider principalProvider,
            ICandidateService candidateService,
            IClassMappingFactory mappingFactory)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _candidateService = candidateService;
            _cardIndexDataService = cardIndexDataService;
            _principalProvider = principalProvider;
            _mappingFactory = mappingFactory;

            CardIndex.Settings.Title = CandidateResources.Title;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
            Layout.Css.Add("~/Areas/Recruitment/Content/DocumentViewer.css");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentCandidate;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentCandidate;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentCandidate;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentCandidate;

            CardIndex.Settings.ToolbarButtons.AddRange(CandidateButtonsHelper.GetToolbatButtons(Url));
            CardIndex.Settings.RowButtons.AddRange(CandidateButtonsHelper.GetRowButtons(Url));

            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/ContactRecord.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/Candidate.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/DocumentViewer.js");

            CardIndex.Settings.ViewFormCustomization += EditViewFormCustomization;
            CardIndex.Settings.EditFormCustomization += EditViewFormCustomization;
            CardIndex.Settings.AddFormCustomization += AddFormCustomization;

            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Name,  CandidateResources.SearchAreaName),
                new SearchArea(SearchAreaCodes.Email,  CandidateResources.SearchAreaEmail),
                new SearchArea(SearchAreaCodes.Phone, CandidateResources.SearchAreaPhone),
                new SearchArea(SearchAreaCodes.JobProfiles, CandidateResources.SearchAreaJobProfiles, false),
                new SearchArea(SearchAreaCodes.Cities, CandidateResources.SearchAreaCities, false),
                new SearchArea(SearchAreaCodes.Resume, CandidateResources.SearchAreaResume, false),
            };

            CardIndex.Settings.FilterGroups = InitializeFilterGroups();
            CardIndex.Settings.GridViewConfigurations = GetViewConfiguration().ToList();
            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;
            AddClientSideResources();
        }

        private void AddClientSideResources()
        {
            Layout.Resources.AddFrom<CandidateResources>(nameof(CandidateResources.ProcessRecruiter));
            Layout.Resources.AddFrom<CandidateResources>(nameof(CandidateResources.ProcessDecisionMaker));
            Layout.Resources.AddFrom<CandidateResources>(nameof(CandidateResources.ProcessStatus));
            Layout.Resources.AddFrom<CandidateResources>(nameof(CandidateResources.ProcessStepsToDo));
            Layout.Resources.AddFrom<CandidateResources>(nameof(CandidateResources.ProcessStepsDone));
            Layout.Resources.AddFrom<CandidateResources>(nameof(CandidateResources.ProcessJobOpening));
        }

        private IEnumerable<IGridViewConfiguration> GetViewConfiguration()
        {
            var defaultView = new GridViewConfiguration<CandidateViewModel>(CandidateResources.DefaultView, "default");
            defaultView.IncludeAllColumns();
            defaultView.ExcludeColumn(m => m.LastActivitytBy);
            defaultView.ExcludeColumn(m => m.LastActivitytOn);
            defaultView.IsDefault = true;

            yield return defaultView;

            var extendedConfiguration = new GridViewConfiguration<CandidateViewModel>(CandidateResources.ExtendedView, "extended");
            extendedConfiguration.IncludeAllColumns();

            yield return extendedConfiguration;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private IList<FilterGroup> InitializeFilterGroups()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = (new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.CandidateAllCandidates,
                            DisplayName = CandidateResources.AllCandidatesLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.CandidateMyProcessCandidates,
                            DisplayName = CandidateResources.MyProcessCandidatesLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.CandidateMyCandidates,
                            DisplayName = CandidateResources.MyCandidatesLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.CandidateProcessedByMeCandidates,
                            DisplayName = CandidateResources.ProcessedByMeCandidatesLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.CandidateMyFavorites,
                            DisplayName = CandidateResources.MyFavoriteCandidatesLabel
                        },
                    })
                },
                new FilterGroup
                {
                    DisplayName = CandidateResources.AdvancedFilterGroupName,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<CandidateAdvancedFiltersViewModel, CandidateAdvancedFiltersDto>(
                            FilterCodes.CandidatePropertiesFilters,
                            CandidateResources.CandidatePropertiesFilter)
                    }
                }
            };
        }

        public override ActionResult View(long id)
        {
            Layout.Scripts.Add("~/scripts/react");
            Layout.Scripts.Add("~/scripts/react-ui");
            Layout.Scripts.Add("~/scripts/recruitment/controls");

            _dataActivityLogService.LogCandidateDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            CardIndex.ConfigureViewButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id));

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            Layout.Scripts.Add("~/scripts/react");
            Layout.Scripts.Add("~/scripts/react-ui");
            Layout.Scripts.Add("~/scripts/recruitment/controls");

            _dataActivityLogService.LogCandidateDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id));

            return base.Edit(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanAnonymizeCandidates)]
        public ActionResult Anonymize(long id)
        {
            _dataActivityLogService.LogCandidateDataActivity(id, ActivityType.AnonymizeRecord, GetType().Name);
            _candidateService.AnonymizeCandidateById(id);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidate)]
        public ActionResult WatchCandidats(List<long> Ids)
        {
            _candidateService.ToggleWatching(Ids, _principalProvider.Current.EmployeeId, WatchingStatus.Watch);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentCandidate)]
        public ActionResult UnwatchCandidats(List<long> Ids)
        {
            _candidateService.ToggleWatching(Ids, _principalProvider.Current.EmployeeId, WatchingStatus.Unwatch);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddRecruitmentProcessNotes)]
        public JsonNetResult AddNote(AddNoteViewModel viewModel)
        {
            var dialogNoteDto = _mappingFactory.CreateMapping<AddNoteViewModel, AddNoteDto>().CreateFromSource(viewModel);

            var success = _candidateService.AddNote(dialogNoteDto);

            return new JsonNetResult(success);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanFavorCandidate)]
        public ActionResult AddToFavorite(ICollection<long> ids)
        {
            var result = _candidateService.ToogleFavorite(ids, FavoriteStatus.Favorite);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);
            AddAlerts(result.Alerts);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanFavorCandidate)]
        public ActionResult RemoveFromFavorite(ICollection<long> ids)
        {
            var result = _candidateService.ToogleFavorite(ids, FavoriteStatus.Disliked);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);
            AddAlerts(result.Alerts);

            return new RedirectResult(listActionUrl);
        }

        private IList<IFooterButton> GetAvaliableButtons(long candidateId)
        {
            var avaliableButtons = new List<IFooterButton>();

            if (_candidateService.IsWatchedByCurrentUser(candidateId))
            {
                avaliableButtons.Add(CreateWorkflowActionButton(
                    nameof(CandidateWorkflowAction.Unwatch),
                    CandidateResources.UnwatchButtonLabel,
                    CandidateWorkflowAction.Unwatch));
            }
            else
            {
                avaliableButtons.Add(CreateWorkflowActionButton(
                    nameof(CandidateWorkflowAction.Watch),
                    CandidateResources.WatchButtonLabel,
                    CandidateWorkflowAction.Watch));
            }

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanAddRecruitmentCandidateNotes))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(AddNoteViewModel), CandidateResources.NewNoteButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"Candidate.addNoteFromViewForm(this, {candidateId}, 'Models.{ nameof(AddNoteViewModel) }');"),
                        ButtonStyleType = CommandButtonStyle.Default,
                        Icon = FontAwesome.Plus
                    }
                );
            }

            CandidateButtonsHelper.GetFooterButtons(Url, candidateId).ForEach(
                b => avaliableButtons.Add(b));

            return avaliableButtons;
        }

        private void AddFormCustomization(FormRenderingModel form)
        {
            form.GetControlByName<CandidateViewModel>(c => c.LastCompanyResumeDocument).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.LastOriginalResumeDocument).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.CurrentConsents).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.RecentActivity).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(j => j.IsAnonymized).Visibility = ControlVisibility.Hide;
            form.GetControlByName<CandidateViewModel>(j => j.RelocateDetails).Visibility = ControlVisibility.Hide;
        }

        private void EditViewFormCustomization(FormRenderingModel form)
        {
            var model = form.GetViewModel<CandidateViewModel>();

            form.GetControlByName<CandidateViewModel>(j => j.IsAnonymized).Visibility = model.IsAnonymized ? ControlVisibility.Show : ControlVisibility.Hide;
            form.GetControlByName<CandidateViewModel>(j => j.ShouldAddRelatedDataConsent).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.ConsentScope).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.ConsentType).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.InboundEmailDocumentId).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.ConsentFiles).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(c => c.RecentActivity).Visibility = ControlVisibility.Remove;
            form.GetControlByName<CandidateViewModel>(j => j.RelocateDetails).Visibility = model.CanRelocate ? ControlVisibility.Hide : ControlVisibility.Hide;

            var currentPrincipal = GetCurrentPrincipal();

            if (!currentPrincipal.IsInRole(SecurityRoleType.CanEditCandidateCoordinators))
            {
                form.GetControlByName<CandidateViewModel>(j => j.ConsentCoordinatorId).IsReadOnly = true;
                form.GetControlByName<CandidateViewModel>(j => j.ContactCoordinatorId).IsReadOnly = true;
            }
        }

        private IFooterButton CreateWorkflowActionButton(string name, string displayName, CandidateWorkflowAction action, string confirmTitle = null, string confirmContent = null)
        {
            var button = FooterButtonBuilder.EditButton();

            button.Id = name;
            button.SetDisplayText(displayName);
            button.SetSubmitParameter<CandidateViewModel>((int)action, a => a.WorkflowAction);

            if (!string.IsNullOrEmpty(confirmTitle) && !string.IsNullOrEmpty(confirmContent))
            {
                button.SetRequireConfirmation(confirmTitle, confirmContent);
            }

            return button;
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var candidate = (CandidateViewModel)base.GetViewModelFromContext(childContext);

            var candidateUrl = Url.UriActionGet<CandidateController>(c => c.Details(candidate.Id), KnownParameter.None, RoutingHelper.IdParameterName, candidate.Id.ToString()).Url;

            var panelTitleText = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewRecruitmentCandidate)
                ? $"<a href=\"{candidateUrl}\" target=\"_blank\">{candidate.FullName}</a>"
                : candidate.FullName;

            var panel = new PanelViewModel(panelTitleText);

            if (!string.IsNullOrWhiteSpace(candidate.EmailAddress))
            {
                panel.AddProperty(CandidateResources.EmailAddress, candidate.EmailAddress);
            }

            if (!string.IsNullOrWhiteSpace(candidate.OtherEmailAddress))
            {
                panel.AddProperty(CandidateResources.EmailAddress, candidate.OtherEmailAddress);
            }

            if (!string.IsNullOrWhiteSpace(candidate.MobilePhone))
            {
                panel.AddProperty(CandidateResources.MobilePhone, candidate.MobilePhone);
            }

            if (!string.IsNullOrWhiteSpace(candidate.OtherPhone))
            {
                panel.AddProperty(CandidateResources.OtherPhone, candidate.OtherPhone);
            }

            if (!string.IsNullOrWhiteSpace(candidate.SkypeLogin))
            {
                panel.AddProperty(CandidateResources.SkypeLogin, candidate.SkypeLogin);
            }

            if (!string.IsNullOrWhiteSpace(candidate.SocialLink))
            {
                panel.AddProperty(CandidateResources.SocialLink, candidate.SocialLink);
            }

            return panel;
        }
    }
}