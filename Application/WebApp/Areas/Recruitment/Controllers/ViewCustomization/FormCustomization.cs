﻿using System.Linq;
using System.Web;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers.ViewCustomization
{
    public abstract class FormCustomization : IFormCustomization
    {
        public abstract void CustomizeAddForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null);
        public abstract void CustomizeEditForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null);
        public abstract void CustomizeViewForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null);

        protected void MakeReadonlyPropertiesInTab(FormRenderingModel form, string tabName)
        {
            foreach (var control in form.Controls.Where(c => c.TabId == tabName))
            {
                control.IsReadOnly = true;
            }
        }

        protected void HideControlsInTab(FormRenderingModel form, string tabId, params string[] visibleControls)
        {
            var tabControls = form.Controls.Where(c => c.TabId == tabId);

            foreach (var control in tabControls)
            {
                if (visibleControls != null && visibleControls.Contains(control.Name))
                {
                    continue;
                }

                control.Visibility = ControlVisibility.Remove;
            }
        }
    }
}