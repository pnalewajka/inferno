﻿using System.Web;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers.ViewCustomization
{
    public interface IFormCustomization
    {
        void CustomizeAddForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null);

        void CustomizeEditForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null);

        void CustomizeViewForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null);
    }
}
