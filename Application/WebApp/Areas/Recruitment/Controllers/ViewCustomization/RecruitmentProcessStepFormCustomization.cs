﻿using System.Linq;
using System.Web;
using Castle.Core.Internal;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers.ViewCustomization
{
    public class RecruitmentProcessStepFormCustomization : FormCustomization
    {
        internal const string AllowCloseDecisionParameterName = "allow-close-decision";

        public override void CustomizeAddForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null)
        {
            var model = form.GetViewModel<RecruitmentProcessStepViewModel>();

            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.Decision).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.DecidedOn).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.DecisionComment).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.Status).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.CloseCurrentStep).Visibility = ControlVisibility.Hide;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.Details).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.CreatedByFullName).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.CreatedOn).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.ModifiedByFullName).Visibility = ControlVisibility.Remove;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.ModifiedOn).Visibility = ControlVisibility.Remove;

            HideControlsInTab(form, RecruitmentProcessStepViewModel.ScreeningTab);

            if (!currentUser.IsInRole(SecurityRoleType.CanChangeRecruitmentProcessStepDecisionMaker))
            {
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.AssignedEmployeeId).IsReadOnly = true;
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.PlannedOn).Visibility = ControlVisibility.Remove;
            }

            switch (model.Type)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    break;

                case RecruitmentProcessStepType.TechnicalReview:
                    HideControlsInTab(form, RecruitmentProcessStepViewModel.TechnicalReviewTab,
                        nameof(TechnicalReviewRecruitmentProcessStepViewModel.JobProfileIds));
                    break;

                case RecruitmentProcessStepType.HiringManagerInterview:
                case RecruitmentProcessStepType.ClientInterview:
                case RecruitmentProcessStepType.HiringDecision:
                case RecruitmentProcessStepType.ContractNegotiations:
                    form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.PlannedOn).Visibility = ControlVisibility.Hide;
                    HideControlsInTab(form, RecruitmentProcessStepViewModel.ContractNegotiationFinalTab);
                    break;
            }

            AddPreviousStepRelatedCustomization(form, request);

            foreach (var control in form.Controls)
            {
                control.TabId = null;
            }
        }

        public override void CustomizeEditForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null)
        {
            var model = form.GetViewModel<RecruitmentProcessStepViewModel>();

            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.RecruitmentProcessId).IsReadOnly = true;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.CloseCurrentStep).Visibility = ControlVisibility.Hide;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.RecruitmentProcessId).Visibility = ControlVisibility.Hide;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.Status).Visibility = ControlVisibility.Hide;
            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.Type).Visibility = ControlVisibility.Hide;

            if (model.Status == RecruitmentProcessStepStatus.Draft || model.Decision == RecruitmentProcessStepDecision.None)
            {
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.Decision).Visibility = ControlVisibility.Hide;
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.DecidedOn).Visibility = ControlVisibility.Hide;
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.DecisionComment).Visibility = ControlVisibility.Hide;
            }

            if (model.CreatedById != currentUser.EmployeeId)
            {
                form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.DecidedOn).IsReadOnly = true;
            };

            if (currentUser.EmployeeId != model.AssignedEmployeeId)
            {
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.ScreeningTab);
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.CandidateTechnicalReviewTab);
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.ContractNegotiationFinalTab);
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.ContractNegotiationOfferTab);
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.TechnicalReviewTab);

                if (!currentUser.IsInRole(SecurityRoleType.CanChangeRecruitmentProcessStepDecisionMaker))
                {
                    form.Controls.ForEach(c => c.IsReadOnly = true);
                }
            }

            SetReadOnlyBasedOnSecurityRoles(form, currentUser);

            if (model.Status == RecruitmentProcessStepStatus.Done || model.Status == RecruitmentProcessStepStatus.Cancelled)
            {
                form.Controls.ToList().ForEach(c => c.IsReadOnly = true);
            }

            switch (model.Type)
            {
                case RecruitmentProcessStepType.ClientResumeApproval:
                    form.GetControlByName<ClientResumeApprovalRecruitmentProcessStepViewModel>(s => s.ResumeShownToClientOn).IsReadOnly = true;

                    break;
            }

            ApplyDetailsViewCustomization(form, currentUser);
        }

        public override void CustomizeViewForm(FormRenderingModel form, IAtomicPrincipal currentUser, HttpRequestBase request = null)
        {
            var model = form.GetViewModel<RecruitmentProcessStepViewModel>();

            form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.CloseCurrentStep).Visibility = ControlVisibility.Hide;
            ApplyDetailsViewCustomization(form, currentUser);
        }

        private void AddPreviousStepRelatedCustomization(FormRenderingModel form, HttpRequestBase request)
        {
            const string PreviousStepTypeParameterName = "source-step-type";

            var previousStepType = request.QueryString[PreviousStepTypeParameterName];

            if (previousStepType.IsNullOrEmpty())
            {
                return;
            }

            var closePreviousStepControl = form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.CloseCurrentStep);

            closePreviousStepControl.Visibility = ControlVisibility.Show;
            closePreviousStepControl.IsReadOnly = request.QueryString[AllowCloseDecisionParameterName] != "true";
            closePreviousStepControl.Label = string.Format(RecruitmentProcessStepResources.CloseStep, previousStepType);

            form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.RecruitmentProcessId).Visibility = ControlVisibility.Hide;
            form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.Details).Visibility = ControlVisibility.Remove;
        }

        private void SetReadOnlyBasedOnSecurityRoles(FormRenderingModel form, IAtomicPrincipal currentUser)
        {
            if (!currentUser.IsInRole(SecurityRoleType.CanChangeRecruitmentProcessStepDecisionMaker))
            {
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.AssignedEmployeeId).IsReadOnly = true;
                form.GetControlByName<RecruitmentProcessStepViewModel>(s => s.PlannedOn).IsReadOnly = true;
            }

            if (!currentUser.IsInRole(SecurityRoleType.CanEditRecruitmentProcessOfferDetails))
            {
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.ContractNegotiationOfferTab);
            }

            if (!currentUser.IsInRole(SecurityRoleType.CanEditRecruitmentProcessFinalDetails))
            {
                MakeReadonlyPropertiesInTab(form, RecruitmentProcessStepViewModel.ContractNegotiationFinalTab);
            }
        }

        private void ApplyDetailsViewCustomization(FormRenderingModel form, IAtomicPrincipal currentUser)
        {
            var control = form.GetControlByName<RecruitmentProcessStepViewModel>(c => c.Details);

            if (control == null || control.Visibility == ControlVisibility.Remove)
            {
                return;
            }

            if (currentUser.IsInRole(SecurityRoleType.CanViewAllCandidateData))
            {
                ((CustomControlRenderingModel)control)
                    .TemplatePath = "Smt.Atomic.WebApp.Areas.Recruitment.Views.Shared.RecruitmentProcessStepAllDetails";
            }
            else if (currentUser.IsInRole(SecurityRoleType.CanViewManagerialCandidateData))
            {
                ((CustomControlRenderingModel)control)
                    .TemplatePath = "Smt.Atomic.WebApp.Areas.Recruitment.Views.Shared.RecruitmentProcessStepAllManagerialDetails";
            }
        }
    }
}