﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentContactRecord)]
    public class CandidateContactRecordsController : CardIndexController<CandidateContactRecordViewModel, CandidateContactRecordDto>
    {
        public CandidateContactRecordsController(ICandidateContactRecordCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.Title = ContactRecordResources.Title;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentContactRecord;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentContactRecord;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentContactRecord;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentContactRecord;

            CardIndex.Settings.FilterGroups = GetFilterGroups();
            CardIndex.Settings.AllowMultipleRowSelection = false;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private IList<FilterGroup> GetFilterGroups()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.MyRecords,
                            DisplayName = ContactRecordResources.MyContactRecordsLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.AnyOwner,
                            DisplayName = ContactRecordResources.AllContactRecordsLabel
                        },
                    }
                }
            };
        }
    }
}