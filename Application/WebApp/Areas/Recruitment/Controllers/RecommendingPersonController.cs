﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecommendingPersons)]
    public class RecommendingPersonController : CardIndexController<RecommendingPersonViewModel, RecommendingPersonDto>
    {
        private const string NotesGroup = "notes";
        private const string ContactRecordsGroup = "contact-records";
        private const string DataConsentGroup = "data-consent";
        private const string DataActivityGroup = "data-activity";

        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IRecommendingPersonService _recommendingPersonService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IEmployeeService _employeeService;
        private readonly IClassMappingFactory _mappingFactory;

        public RecommendingPersonController(
            IRecommendingPersonCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDataActivityLogService dataActivityLogService,
            IRecommendingPersonService recommendingPersonService,
            IPrincipalProvider principalProvider,
            IEmployeeService employeeService,
            IClassMappingFactory mappingFactory)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _recommendingPersonService = recommendingPersonService;
            _principalProvider = principalProvider;
            _employeeService = employeeService;
            _mappingFactory = mappingFactory;

            CardIndex.Settings.Title = RecommendingPersonResources.Title;
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecommendingPersons;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecommendingPersons;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecommendingPersons;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecommendingPersons;

            CardIndex.Settings.ToolbarButtons.AddRange(GetContactRecordsCommandButtons());
            CardIndex.Settings.ToolbarButtons.AddRange(GetNotesCommandButtons());
            CardIndex.Settings.ToolbarButtons.AddRange(GetDataConsentCommandButtons());
            CardIndex.Settings.ToolbarButtons.AddRange(GetDataActivityCommandButtons());
            CardIndex.Settings.ToolbarButtons.AddRange(GetAnonymizeCommandButton());

            CardIndex.Settings.AddFormCustomization += AddFormCustomization;
            CardIndex.Settings.EditFormCustomization += EditFormCustomization;
            CardIndex.Settings.ViewFormCustomization += EditFormCustomization;

            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;

            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/ContactRecord.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/RecommendingPerson.js");
            Layout.Resources.AddFrom<RecommendingPersonResources>(nameof(RecommendingPersonResources.NoteDialogTitle));
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            CardIndex.ConfigureViewButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id));

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id));

            return base.Edit(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanAnonymizeRecommendingPersons)]
        public ActionResult Anonymize(long id)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(id, ActivityType.AnonymizeRecord, GetType().Name);

            var currentEmployeeFullName = _employeeService.GetEmployeeOrDefaultById(_principalProvider.Current.EmployeeId).FullName;
            _recommendingPersonService.AddNote(new RecommendingPersonNote
            {
                RecommendingPersonId = id,
                NoteVisibility = NoteVisibility.Public,
                Content = string.Format(RecommendingPersonResources.AnonymizationNote, currentEmployeeFullName)
            });

            _recommendingPersonService.AnonymizeRecommendingPersonById(id);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        private void EditFormCustomization(FormRenderingModel form)
        {
            var model = form.GetViewModel<RecommendingPersonViewModel>();

            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanEditRecommendingPersonCoordinators))
            {
                form.GetControlByName<RecommendingPersonViewModel>(j => j.CoordinatorId).IsReadOnly = true;
            }

            form.GetControlByName<RecommendingPersonViewModel>(j => j.IsAnonymized).Visibility = model.IsAnonymized ? ControlVisibility.Show : ControlVisibility.Hide;

            HideShouldAddRelatedDataConsent(form);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddRecommendingPersonNotes)]
        public JsonNetResult AddNote(AddNoteViewModel viewModel)
        {
            var dialogNoteDto = _mappingFactory.CreateMapping<AddNoteViewModel, AddNoteDto>().CreateFromSource(viewModel);

            var success = _recommendingPersonService.AddNote(dialogNoteDto);

            return new JsonNetResult(success);
        }

        private void HideShouldAddRelatedDataConsent(FormRenderingModel formRenderingModel)
        {
            formRenderingModel.GetControlByName<RecommendingPersonViewModel>(a => a.ShouldAddRelatedDataConsent).Visibility = ControlVisibility.Hide;
        }

        private void AddFormCustomization(FormRenderingModel formRenderingModel)
        {
            formRenderingModel.GetControlByName<RecommendingPersonViewModel>(a => a.CreatedByFullName).Visibility = ControlVisibility.Hide;
            formRenderingModel.GetControlByName<RecommendingPersonViewModel>(a => a.CreatedOn).Visibility = ControlVisibility.Hide;
            formRenderingModel.GetControlByName<RecommendingPersonViewModel>(a => a.CoordinatorId).Visibility = ControlVisibility.Hide;
            formRenderingModel.GetControlByName<RecommendingPersonViewModel>(a => a.IsAnonymized).Visibility = ControlVisibility.Hide;

            if (!GetCurrentPrincipal().IsInRole(SecurityRoleType.CanAddRecruitmentDataConsent))
            {
                HideShouldAddRelatedDataConsent(formRenderingModel);
            }
        }

        private IEnumerable<CommandButton> GetNotesCommandButtons()
        {
            yield return new ToolbarButton
            {
                Text = RecommendingPersonNoteResources.NotesButtonText,
                OnClickAction = Url.UriActionGet(nameof(RecommendingPersonNoteController.List),
                                RoutingHelper.GetControllerName<RecommendingPersonNoteController>(), KnownParameter.ParentId),
                Icon = FontAwesome.List,
                RequiredRole = SecurityRoleType.CanViewRecommendingPersonNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = NotesGroup
            };

            yield return new ToolbarButton
            {
                Text = RecommendingPersonNoteResources.NotesAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<RecommendingPersonNoteController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecommendingPersonNotes,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = NotesGroup
            };
        }

        private IEnumerable<CommandButton> GetDataConsentCommandButtons()
        {
            yield return new ToolbarButton
            {
                Text = RecommendingPersonResources.DataConsentsButtonText,
                OnClickAction = Url.UriActionGet(nameof(RecommendingPersonDataConsentController.List),
                                RoutingHelper.GetControllerName<RecommendingPersonDataConsentController>(), KnownParameter.ParentId),
                Icon = FontAwesome.BalanceScale,
                RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataConsentGroup
            };

            var addButton = new ToolbarButton
            {
                Text = RecommendingPersonResources.DataConsentAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<RecommendingPersonDataConsentController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataConsentGroup
            };

            DataConsentController.SetAddDataConsentDropdownItems(addButton, Url,
                RoutingHelper.GetControllerName<RecommendingPersonDataConsentController>(), nameof(GetAddDialog), KnownParameter.ParentId);

            yield return addButton;
        }

        private IEnumerable<CommandButton> GetDataActivityCommandButtons()
        {
            yield return new ToolbarButton
            {
                Text = RecommendingPersonResources.DataActivityButtonText,
                OnClickAction = Url.UriActionGet(nameof(RecommendingPersonDataActivityController.List),
                                RoutingHelper.GetControllerName<RecommendingPersonDataActivityController>(), KnownParameter.ParentId),
                Icon = FontAwesome.History,
                RequiredRole = SecurityRoleType.CanViewDataActivities,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataActivityGroup
            };

            yield return new ToolbarButton
            {
                Text = RecommendingPersonResources.DataActivityAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<RecommendingPersonDataActivityController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddDataActivities,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = DataActivityGroup
            };
        }

        private IEnumerable<CommandButton> GetContactRecordsCommandButtons()
        {
            yield return new ToolbarButton
            {
                Text = CandidateResources.ContactRecordsButtonText,
                OnClickAction = Url.UriActionGet(nameof(RecommendingPersonContactRecordsController.List),
                    RoutingHelper.GetControllerName<RecommendingPersonContactRecordsController>(), KnownParameter.ParentId),
                Icon = FontAwesome.CalendarO,
                RequiredRole = SecurityRoleType.CanViewRecruitmentRecommendingPersonContactRecords,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = ContactRecordsGroup
            };

            yield return new ToolbarButton
            {
                Text = CandidateResources.ContactRecordsAddButtonText,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<RecommendingPersonContactRecordsController>(), KnownParameter.ParentId)),
                RequiredRole = SecurityRoleType.CanAddRecruitmentRecommendingPersonContactRecords,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"(row.hasValidConsent)"
                },
                Group = ContactRecordsGroup
            };
        }

        private IEnumerable<CommandButton> GetAnonymizeCommandButton()
        {
            yield return new ToolbarButton
            {
                Text = RecommendingPersonResources.AnonymizeButtonText,
                OnClickAction = Url.UriActionGet(nameof(Anonymize), KnownParameter.SelectedId),
                RequiredRole = SecurityRoleType.CanAnonymizeRecommendingPersons,
                Confirmation = new Confirmation
                {
                    AcceptButtonText = CardIndexResources.OkButtonText,
                    IsRequired = true,
                    Message = RecommendingPersonResources.AnonymizeConfirmationMessage,
                    RejectButtonText = CardIndexResources.CancelButtonText
                },
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
            };
        }

        private IList<IFooterButton> GetAvaliableButtons(long recommendingPersonId)
        {
            var avaliableButtons = new List<IFooterButton>();

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanAddRecommendingPersonNotes))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(AddNoteViewModel), RecommendingPersonResources.NewNoteButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"RecommendingPerson.addNoteFromViewForm(this, {recommendingPersonId}, 'Models.{ nameof(AddNoteViewModel) }');"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Plus
                    }
                );
            }

            return avaliableButtons;
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var recommendingPerson = (RecommendingPersonViewModel)base.GetViewModelFromContext(childContext);

            var recommendingPersonUrl = Url.UriActionGet<CandidateController>(c => c.Details(recommendingPerson.Id), KnownParameter.None, RoutingHelper.IdParameterName, recommendingPerson.Id.ToString()).Url;

            var panelTitleText = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewRecommendingPersons)
                ? $"<a href=\"{recommendingPersonUrl}\" target=\"_blank\">{recommendingPerson.FullName}</a>"
                : recommendingPerson.FullName;

            var panel = new PanelViewModel(panelTitleText);

            if (!string.IsNullOrWhiteSpace(recommendingPerson.EmailAddress))
            {
                panel.AddProperty(RecommendingPersonResources.EmailAddress, recommendingPerson.EmailAddress);
            }

            if (!string.IsNullOrWhiteSpace(recommendingPerson.PhoneNumber))
            {
                panel.AddProperty(RecommendingPersonResources.PhoneNumber, recommendingPerson.PhoneNumber);
            }

            return panel;
        }
    }
}