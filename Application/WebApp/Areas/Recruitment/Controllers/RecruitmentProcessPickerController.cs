﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.RecruitmentProcess")]
    [AtomicAuthorize(SecurityRoleType.CanViewMyRecruitmentProcesses)]

    public class RecruitmentProcessPickerController : ReadOnlyCardIndexController<RecruitmentProcessViewModel, RecruitmentProcessDto, RecruitmentProcessPickerContext>
    {
        public RecruitmentProcessPickerController(
            IRecruitmentProcessPickerCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GridViewConfigurations;
        }

        private IList<IGridViewConfiguration> GridViewConfigurations
        {
            get
            {
                var configuration = new GridViewConfiguration<RecruitmentProcessViewModel>(RecruitmentProcessResources.Title, "List") { IsDefault = true };
                configuration.IncludeAllColumns();
                configuration.ExcludeColumn(p => p.Progress);
                configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetStepsToDo));
                configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetStepsDone));

                return new List<IGridViewConfiguration> { configuration };
            }
        }
    }
}