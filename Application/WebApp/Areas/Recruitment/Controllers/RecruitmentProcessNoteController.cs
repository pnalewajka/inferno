﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcessNotes)]
    public class RecruitmentProcessNoteController : SubCardIndexController<RecruitmentProcessNoteViewModel, RecruitmentProcessNoteDto, RecruitmentProcessController, ParentIdContext>
    {
        private readonly IRecruitmentProcessNoteCardIndexDataService _cardIndexDataService;

        public RecruitmentProcessNoteController(
            IRecruitmentProcessNoteCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = RecruitmentProcessNoteResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentProcessNotes;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentProcessNotes;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentProcessNotes;

            CardIndex.Settings.AddFormCustomization = form =>
            {
                form.GetControlByName<RecruitmentProcessNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.EditFormCustomization = form =>
            {
                form.GetControlByName<RecruitmentProcessNoteViewModel>(m => m.Content).Visibility = ControlVisibility.Remove;
            };

            CardIndex.Settings.ViewFormCustomization = form =>
            {
                form.GetControlByName<RecruitmentProcessNoteViewModel>(m => m.RichContent).Visibility = ControlVisibility.Hide;
            };
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();

            dto.RecruitmentProcessId = Context.ParentId;
            dto.NoteType = NoteType.UserNote;
            dto.NoteVisibility = NoteVisibility.Public;

            return dto;
        }
    }
}