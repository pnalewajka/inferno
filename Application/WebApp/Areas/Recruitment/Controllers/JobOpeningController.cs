﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpeningFilters;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentJobOpening)]
    [Identifier("RecruitmentController.JobOpeningController")]
    [PerformanceTest(nameof(JobOpeningController.List))]
    public class JobOpeningController : CardIndexController<JobOpeningViewModel, JobOpeningDto>
    {
        private readonly IJobOpeningCardIndexDataService _cardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IJobOpeningService _jobOpeningService;
        private readonly IClassMappingFactory _mappingFactory;

        private const string HiringModeParameterName = "hiring-mode";
        private const string NotesButtonsGroup = "notes-buttons";
        private const string WatchButtonsGroup = "watch-buttons";
        private const string HandleButtonsGroup = "handle-buttons";
        private const string HoldGroup = "hold-buttons";

        public JobOpeningController(
            IJobOpeningCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IPrincipalProvider principalProvider,
            IJobOpeningService jobOpeningService,
            IClassMappingFactory mappingFactory)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _principalProvider = principalProvider;
            _jobOpeningService = jobOpeningService;
            _mappingFactory = mappingFactory;

            CardIndex.Settings.Title = JobOpeningResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpening;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentJobOpening;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentJobOpening;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentJobOpening;

            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportJobOpenings);
            ;

            CardIndex.Settings.FilterGroups = InitializeFilterGroups();

            SetAddJobOpeningDropdownItems();

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.ConfigureAddButtons += CreateAddWorkflowAction(
                "sendForApproval",
                JobOpeningResources.SendForApprovalButtonLabel,
                JobOpeningWorkflowAction.SendForApproval,
                JobOpeningResources.JobOpeninigApproveConfirmationTitle,
                JobOpeningResources.JobOpeninigSendForApprovalConfirmationMessage,
                additionalCssClass: "pull-left");

            CardIndex.Settings.ToolbarButtons.AddRange(AddNotesButtons());
            CardIndex.Settings.ToolbarButtons.AddRange(AddWorkflowButtons());
            CardIndex.Settings.ToolbarButtons.Add(GetRecruitmentProcessButton());
            CardIndex.Settings.ToolbarButtons.Add(GetCloneButton());
            CardIndex.Settings.ToolbarButtons.Add(GetReopenButton());

            CardIndex.Settings.RowButtons.AddRange(AddWatchButtons());
            CardIndex.Settings.RowButtons.AddRange(GetOnOffHoldCommandButtons(Url));

            CardIndex.Settings.AddFormCustomization += AddFormCustomization;
            CardIndex.Settings.ViewFormCustomization += EditViewFormCustomization;
            CardIndex.Settings.EditFormCustomization += EditViewFormCustomization;

            CardIndex.ConfigureEditButtons += (CardIndexRecordViewModel<JobOpeningViewModel> model) =>
            {
                if (model.ViewModelRecord.Status == JobOpeningStatus.Closed)
                {
                    model.Buttons.Remove(model.Buttons.GetEditButton());
                }
            };

            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;

            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/JobOpening.js");
            Layout.Scripts.Add("~/scripts/react");
            Layout.Scripts.Add("~/scripts/react-ui");
            Layout.Scripts.Add("~/scripts/recruitment/controls");

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            AddClientSideResources();
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private void AddClientSideResources()
        {
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.ProcessRecruiter));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.ProcessDecisionMaker));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.ProcessStatus));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.ProcessStepsToDo));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.ProcessStepsDone));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.ProcessCandidate));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.JobOpeninigRejectReasonMessage));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.JobOpeninigRejectReasonTitle));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.JobOpeninigRejectReasonLabel));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.JobOpeninigsRejectReasonMessage));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.CloseDialogTitle));
            Layout.Resources.AddFrom<JobOpeningResources>(nameof(JobOpeningResources.NoteDialogTitle));
        }

        public override ActionResult Edit(long id)
        {
            var jobOpening = _cardIndexDataService.GetRecordById(id);

            if (jobOpening.Status == JobOpeningStatus.Draft || jobOpening.Status == JobOpeningStatus.Rejected || jobOpening.Status == JobOpeningStatus.OnHold)
            {
                CardIndex.ConfigureEditButtons += CreateEditWorkflowAction(
                    "sendForApproval",
                    JobOpeningResources.SendForApprovalButtonLabel,
                    JobOpeningWorkflowAction.SendForApproval,
                    JobOpeningResources.JobOpeninigApproveConfirmationTitle,
                    JobOpeningResources.JobOpeninigSendForApprovalConfirmationMessage,
                    additionalCssClass: "pull-left");
            }

            if (jobOpening.CanBeApprovedByCurrentUser)
            {
                CardIndex.ConfigureEditButtons += CreateEditWorkflowAction(
                    "approve",
                    JobOpeningResources.ApproveButtonLabel,
                    JobOpeningWorkflowAction.Approve,
                    JobOpeningResources.JobOpeninigApproveConfirmationTitle,
                    JobOpeningResources.JobOpeninigApproveConfirmationMessage,
                    additionalCssClass: "pull-left");

                CardIndex.ConfigureEditButtons += CreateButtonAction(
                    "reject", JobOpeningResources.RejectButtonLabel,
                    $"JobOpening.RejectFromEditForm({(int)JobOpeningWorkflowAction.Reject}, '{ IdentifierHelper.GetTypeIdentifier(typeof(JobOpeningRejectViewModel))  }');");
            }

            if (jobOpening.Status != JobOpeningStatus.Closed && _principalProvider.Current.IsInRole(SecurityRoleType.CanCloseJobOpenings))
            {
                CardIndex.ConfigureEditButtons += CreateButtonAction(
                    "close",
                    JobOpeningResources.CloseButtonLabel,
                    $"JobOpening.CloseFromViewForm({id}, '{ IdentifierHelper.GetTypeIdentifier(typeof(JobOpeningCloseViewModel))  }', {(int)JobOpeningWorkflowAction.Close});");
            }

            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id, Url));

            return base.Edit(id);
        }

        public override ActionResult View(long id)
        {
            var jobOpening = _cardIndexDataService.GetRecordById(id);

            if (jobOpening.CanBeApprovedByCurrentUser)
            {
                var approveConfirmMessage = JobOpeningResources.JobOpeninigApproveConfirmationMessage;
                var approveConfirmTitle = JobOpeningResources.JobOpeninigApproveConfirmationTitle;
                var approveAction = nameof(JobOpeningController.ApproveJobOpenings);

                CardIndex.ConfigureViewButtons += CreateButtonAction(
                    "approve", JobOpeningResources.ApproveButtonLabel,
                    $"JobOpening.InvokeActionFromViewFormWithConfirmation('{approveAction}',{id.ToString()}, '{approveConfirmTitle}', '{approveConfirmMessage}');");

                CardIndex.ConfigureViewButtons += CreateButtonAction(
                    "reject", JobOpeningResources.RejectButtonLabel,
                    $"JobOpening.RejectFromViewForm({id.ToString()}, '{ IdentifierHelper.GetTypeIdentifier(typeof(JobOpeningRejectViewModel)) }');");
            }

            CardIndex.ConfigureViewButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id, Url));

            return base.View(id);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentJobOpening)]
        public ActionResult WatchJobOpenings(List<long> ids)
        {
            _jobOpeningService.ToggleWatching(ids, _principalProvider.Current.EmployeeId, WatchingStatus.Watch);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentJobOpening)]
        public ActionResult UnwatchJobOpenings(List<long> ids)
        {
            _jobOpeningService.ToggleWatching(ids, _principalProvider.Current.EmployeeId, WatchingStatus.Unwatch);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanApproveJobOpenings)]
        public ActionResult ApproveJobOpenings(List<long> ids)
        {
            _jobOpeningService.ApproveJobOpenings(ids);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanCloseJobOpenings)]
        public ActionResult CloseJobOpenings(JobOpeningCloseViewModel jobOpeningClose = null)
        {
            var jobOpeningCloseDto = _mappingFactory.CreateMapping<JobOpeningCloseViewModel, JobOpeningCloseDto>().CreateFromSource(jobOpeningClose);

            _jobOpeningService.CloseJobOpening(jobOpeningCloseDto);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [AtomicAuthorize(SecurityRoleType.CanCloneJobOpenings)]
        public ActionResult Clone(long id)
        {
            long clonedJobOpeningId = _jobOpeningService.CloneJobOpening(id);

            return RedirectToAction(nameof(Edit), new { Id = clonedJobOpeningId });
        }

        [AtomicAuthorize(SecurityRoleType.CanReOpenJobOpenings)]
        public ActionResult ReOpen(long id)
        {
            _jobOpeningService.ReopenJobOpening(id);

            return RedirectToAction(nameof(Edit), new { Id = id });
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanApproveJobOpenings)]
        public ActionResult RejectJobOpenings(List<long> ids, string reason)
        {
            _jobOpeningService.RejectJobOpenings(ids, reason);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddRecruitmentProcessNotes)]
        public JsonNetResult AddNote(AddNoteViewModel viewModel)
        {
            var dialogNoteDto = _mappingFactory.CreateMapping<AddNoteViewModel, AddNoteDto>().CreateFromSource(viewModel);

            var success = _jobOpeningService.AddNote(dialogNoteDto);

            return new JsonNetResult(success);
        }

        private IList<FilterGroup> InitializeFilterGroups()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = JobOpeningResources.Status,
                    Type = FilterGroupType.RadioGroup,
                    Filters = (new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.StatusAll,
                            DisplayName = JobOpeningResources.StatusAllLabel,
                        }
                    })
                    .Union(CardIndexHelper.BuildEnumBasedFilters<JobOpeningStatus>()).ToList()
                },
                new FilterGroup
                {
                    DisplayName = JobOpeningResources.Owner,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.MyRecords,
                            DisplayName = JobOpeningResources.MyJobOpeningRecordsLabel,
                        },
                        new Filter
                        {
                            Code = FilterCodes.AnyOwner,
                            DisplayName = JobOpeningResources.AllJobApplicationRecordsLabel,
                        }
                    }
                },
                new FilterGroup
                {
                    DisplayName = JobOpeningResources.JobOpeningFilters,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<JobOpeningFiltersViewModel, JobOpeningFiltersDto>(FilterCodes.JobOpeningFilters, JobOpeningResources.JobOpeningFilters)
                    }
                }
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanPutJobOpeningOnHold)]
        public ActionResult PutJobOpeningOnHold(long id)
        {
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            _jobOpeningService.PutJobOpeningOnHold(id);

            return new RedirectResult(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanTakeJobOpeningOffHold)]
        public ActionResult TakeJobOpeningOffHold(long id)
        {
            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            _jobOpeningService.TakeJobOpeningOffHold(id);

            return new RedirectResult(listActionUrl);
        }

        private static IEnumerable<RowButton> GetOnOffHoldCommandButtons(UrlHelper url)
        {
            yield return new RowButton
            {
                Icon = FontAwesome.Pause,
                OnClickAction = url.UriActionPost(nameof(PutJobOpeningOnHold), KnownParameter.SelectedId),
                RequiredRole = SecurityRoleType.CanPutJobOpeningOnHold,
                Availability = new RowButtonAvailability<JobOpeningViewModel>
                {
                    Predicate = row => row.Status == JobOpeningStatus.Active
                },
                Tooltip = JobOpeningResources.PutOnHoldLabel
            };

            yield return new RowButton
            {
                Icon = FontAwesome.Play,
                OnClickAction = url.UriActionPost(nameof(TakeJobOpeningOffHold), KnownParameter.SelectedId),
                RequiredRole = SecurityRoleType.CanTakeJobOpeningOffHold,
                Availability = new RowButtonAvailability<JobOpeningViewModel>
                {
                    Predicate = row => row.Status == JobOpeningStatus.OnHold
                },
                Tooltip = JobOpeningResources.TakeOffHoldLabel
            };
        }

        private Action<CardIndexRecordViewModel<JobOpeningViewModel>> CreateEditWorkflowAction(string name, string displayName,
            JobOpeningWorkflowAction action, string confirmTitle = null, string confirmContent = null, string additionalCssClass = null)
        {
            return CreateWorkflowAction(FooterButtonBuilder.EditButton(), name, displayName, action, confirmTitle, confirmContent, additionalCssClass);
        }

        private Action<CardIndexRecordViewModel<JobOpeningViewModel>> CreateAddWorkflowAction(string name, string displayName,
            JobOpeningWorkflowAction action, string confirmTitle = null, string confirmContent = null, string additionalCssClass = null)
        {
            return CreateWorkflowAction(FooterButtonBuilder.AddButton(), name, displayName, action, confirmTitle, confirmContent, additionalCssClass);
        }

        private Action<CardIndexRecordViewModel<JobOpeningViewModel>> CreateWorkflowAction(IFooterButton button, string name,
            string displayName, JobOpeningWorkflowAction action, string confirmTitle = null, string confirmContent = null, string additionalCssClass = null)
        {
            return (CardIndexRecordViewModel<JobOpeningViewModel> model) =>
            {
                button.Id = name;
                button
                    .SetDisplayText(displayName)
                    .SetSubmitParameter<JobOpeningViewModel>((int)action, a => a.WorkflowAction);

                button.CssClass = $"{button.CssClass} {additionalCssClass}";

                if (!string.IsNullOrEmpty(confirmTitle) && !string.IsNullOrEmpty(confirmContent))
                {
                    button.SetRequireConfirmation(confirmTitle, confirmContent);
                }

                model.Buttons.Add(button);
            };
        }

        private Action<CardIndexRecordViewModel<JobOpeningViewModel>> CreateButtonAction(string name, string text,
            string onClickJavaScript)
        {
            return (CardIndexRecordViewModel<JobOpeningViewModel> model) =>
            {
                model.Buttons.Add(new FooterButton(name, text)
                {
                    OnClickAction = new JavaScriptCallAction(onClickJavaScript),
                    Attributes = FooterButtonBuilder.SubmitFormButtonAttribures()
                });
            };
        }

        private void SetAddJobOpeningDropdownItems()
        {
            var hiringModes = EnumHelper.GetEnumValues<HiringMode>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var hiringMode in hiringModes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(hiringMode.ToString());

                var addButton = new CommandButton
                {
                    Group = JobOpeningResources.HireForLabel,
                    RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpening,
                    Text = hiringMode.GetDescriptionOrValue(),
                    OnClickAction = Url.UriActionGet(nameof(Add), KnownParameter.Context, HiringModeParameterName, typeValueName)
                };

                addDropdownItems.Add(addButton);
            }

            CardIndex.Settings.AddButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        private object GetDefaultNewRecord()
        {
            var hiringModeName = Request.QueryString[HiringModeParameterName];
            var hiringMode = EnumHelper.GetEnumValueOrDefault<HiringMode>(hiringModeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (hiringMode == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            var dto = _cardIndexDataService.GetDefaultNewRecord(hiringMode.Value);
            var model = _mappingFactory.CreateMapping<JobOpeningDto, JobOpeningViewModel>().CreateFromSource(dto);

            return model;
        }

        private IList<IFooterButton> GetAvaliableButtons(long jobOpeningId, UrlHelper urlHelper)
        {
            var jobOpening = _cardIndexDataService.GetRecordById(jobOpeningId);

            var avaliableButtons = new List<IFooterButton>();

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanAddRecruitmentJobOpeningNotes))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(AddNoteViewModel), JobOpeningResources.NewNoteButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"JobOpening.addNoteFromViewForm(this, {jobOpeningId}, '{ IdentifierHelper.GetTypeIdentifier(typeof(AddNoteViewModel)) }');"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Plus
                    }
                );
            }

            if (jobOpening.IsWatchedByCurrentUser)
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(JobOpeningController.UnwatchJobOpenings), JobOpeningResources.UnwatchButtonLabel)
                    {
                        OnClickAction = new JavaScriptCallAction($"JobOpening.InvokeActionFromViewForm('{nameof(JobOpeningController.UnwatchJobOpenings)}', {jobOpeningId.ToString()});"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Eye
                    }
                );
            }
            else
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(JobOpeningController.WatchJobOpenings), JobOpeningResources.WatchButtonLabel)
                    {
                        OnClickAction = new JavaScriptCallAction($"JobOpening.InvokeActionFromViewForm('{nameof(JobOpeningController.WatchJobOpenings)}', {jobOpeningId.ToString()});"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.EyeSlash
                    }
                );
            }

            if (jobOpening.Status == JobOpeningStatus.Active)
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(JobOpeningController.PutJobOpeningOnHold), JobOpeningResources.PutOnHoldLabel)
                    {
                        OnClickAction = urlHelper.UriActionPost(nameof(JobOpeningController.PutJobOpeningOnHold),
                            RoutingHelper.GetControllerName(this), KnownParameter.SelectedId),
                        ButtonStyleType = CommandButtonStyle.Primary,
                        Icon = FontAwesome.Pause,
                        Text = JobOpeningResources.PutOnHoldLabel
                    }
                );
            }

            if (jobOpening.Status == JobOpeningStatus.OnHold)
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(JobOpeningController.TakeJobOpeningOffHold), JobOpeningResources.TakeOffHoldLabel)
                    {
                        OnClickAction = urlHelper.UriActionPost(nameof(JobOpeningController.TakeJobOpeningOffHold),
                            RoutingHelper.GetControllerName(this), KnownParameter.SelectedId),
                        ButtonStyleType = CommandButtonStyle.Primary,
                        Icon = FontAwesome.Play,
                        Text = JobOpeningResources.TakeOffHoldLabel
                    }
                );
            }

            return avaliableButtons;
        }

        private IEnumerable<CommandButton> AddNotesButtons()
        {
            yield return new ToolbarButton
            {
                Text = JobOpeningNoteResources.Title,
                RequiredRole = SecurityRoleType.CanViewRecruitmentJobOpeningNotes,
                OnClickAction = Url.UriActionGet(nameof(JobOpeningNoteController.List),
                    RoutingHelper.GetControllerName<JobOpeningNoteController>(), KnownParameter.ParentId),
                Icon = FontAwesome.StickyNoteO,
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                Group = NotesButtonsGroup
            };

            yield return new ToolbarButton
            {
                Text = JobOpeningNoteResources.NotesAddButtonText,
                RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpeningNotes,
                OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog),
                    RoutingHelper.GetControllerName<JobOpeningNoteController>(), KnownParameter.ParentId)),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                Group = NotesButtonsGroup
            };
        }

        private IEnumerable<CommandButton> AddWatchButtons()
        {
            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewRecruitmentJobOpening,
                Icon = FontAwesome.Eye,
                OnClickAction = Url.UriActionPost(nameof(WatchJobOpenings), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<JobOpeningViewModel>
                {
                    Predicate = row => !row.IsWatchedByCurrentUser
                },
                Tooltip = JobOpeningResources.WatchButtonLabel,
                CssClass = "toogle-off"
            };

            yield return new RowButton
            {
                RequiredRole = SecurityRoleType.CanViewRecruitmentJobOpening,
                Icon = FontAwesome.EyeSlash,
                OnClickAction = Url.UriActionPost(nameof(UnwatchJobOpenings), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new RowButtonAvailability<JobOpeningViewModel>
                {
                    Predicate = row => row.IsWatchedByCurrentUser
                },
                Tooltip = JobOpeningResources.UnwatchButtonLabel,
                CssClass = "toogle-on"
            };
        }

        private IEnumerable<CommandButton> AddWorkflowButtons()
        {
            yield return new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanApproveJobOpenings,
                Text = JobOpeningResources.ApproveButtonLabel,
                OnClickAction = Url.UriActionPost(nameof(ApproveJobOpenings), KnownParameter.SelectedIds),
                Visibility = CommandButtonVisibility.Grid,
                Confirmation = new Confirmation
                {
                    AcceptButtonText = CardIndexResources.OkButtonText,
                    IsRequired = true,
                    Message = JobOpeningResources.JobOpeninigApproveConfirmationMessage,
                    RejectButtonText = CardIndexResources.CancelButtonText
                },
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(JobOpeningViewModel.CanBeApprovedByCurrentUser))}"
                },
                Group = HandleButtonsGroup
            };

            yield return new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanApproveJobOpenings,
                Text = JobOpeningResources.RejectButtonLabel,
                OnClickAction = new JavaScriptCallAction($"JobOpening.RejectFromGrid(grid, '{ IdentifierHelper.GetTypeIdentifier(typeof(JobOpeningRejectViewModel)) }');"),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(JobOpeningViewModel.CanBeApprovedByCurrentUser))}"
                },
                Group = HandleButtonsGroup
            };

            yield return new ToolbarButton
            {
                RequiredRole = SecurityRoleType.CanCloseJobOpenings,
                Text = JobOpeningResources.CloseButtonLabel,
                OnClickAction = new JavaScriptCallAction($"JobOpening.CloseFromGrid(grid, '{ IdentifierHelper.GetTypeIdentifier(typeof(JobOpeningCloseViewModel)) }');"),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(JobOpeningViewModel.Status))} != {(int)JobOpeningStatus.Closed}"
                },
                Group = HandleButtonsGroup
            };
        }

        private CommandButton GetRecruitmentProcessButton()
        {
            return new ToolbarButton
            {
                Text = JobOpeningResources.Processes,
                RequiredRole = SecurityRoleType.CanViewRecruitmentProcess,
                Icon = FontAwesome.UserPlus,
                OnClickAction = Url.UriActionGet(nameof(JobOpeningRecruitmentProcessController.List),
                    RoutingHelper.GetControllerName<JobOpeningRecruitmentProcessController>(), KnownParameter.ParentId),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = $"row.{NamingConventionHelper.ConvertPascalCaseToCamelCase(nameof(JobOpeningViewModel.Status))} == {(int)JobOpeningStatus.Active}"
                },
            };
        }

        private CommandButton GetCloneButton()
        {
            return new ToolbarButton
            {
                Text = JobOpeningResources.Clone,
                RequiredRole = SecurityRoleType.CanCloneJobOpenings,
                Icon = FontAwesome.Clone,
                OnClickAction = Url.UriActionGet(nameof(JobOpeningController.Clone),
                    RoutingHelper.GetControllerName<JobOpeningController>(), KnownParameter.SelectedId),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"(row.status ==={(int)JobOpeningStatus.Active} || row.status ==={(int)JobOpeningStatus.Closed})",
                    Mode = AvailabilityMode.SingleRowSelected,
                },
            };
        }

        private CommandButton GetReopenButton()
        {
            return new ToolbarButton
            {
                Text = JobOpeningResources.Reopen,
                RequiredRole = SecurityRoleType.CanReOpenJobOpenings,
                Icon = FontAwesome.Refresh,
                OnClickAction = Url.UriActionGet(nameof(JobOpeningController.ReOpen),
                    RoutingHelper.GetControllerName<JobOpeningController>(), KnownParameter.SelectedId),
                Visibility = CommandButtonVisibility.Grid,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"(row.status ==={(int)JobOpeningStatus.Closed})",
                    Mode = AvailabilityMode.SingleRowSelected,
                },
            };
        }

        private void AddFormCustomization(FormRenderingModel form)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanEditJobOpeningCoordinators))
            {
                form.GetControlByName<JobOpeningViewModel>(j => j.RecruiterIds).IsReadOnly = true;
                form.GetControlByName<JobOpeningViewModel>(j => j.RecrutmentOwnerIds).IsReadOnly = true;
            }

            form.GetControlByName<JobOpeningViewModel>(j => j.RejectionReason).Visibility = ControlVisibility.Hide;
            form.GetControlByName<JobOpeningViewModel>(j => j.ClosedComment).Visibility = ControlVisibility.Hide;
            form.GetControlByName<JobOpeningViewModel>(j => j.ClosedReason).Visibility = ControlVisibility.Hide;
        }

        private void EditViewFormCustomization(FormRenderingModel form)
        {
            var model = form.GetViewModel<JobOpeningViewModel>();

            var rejectionReasonControl = form.GetControlByName<JobOpeningViewModel>(j => j.RejectionReason);

            if (model.Status != JobOpeningStatus.Rejected)
            {
                rejectionReasonControl.Visibility = ControlVisibility.Hide;
            }

            if (model.Status == JobOpeningStatus.PendingApproval && !string.IsNullOrEmpty(model.RejectionReason))
            {
                rejectionReasonControl.Label = JobOpeningResources.RejectionReasonPreviousLabel;
                rejectionReasonControl.Visibility = ControlVisibility.Show;
            }

            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanEditJobOpeningCoordinators))
            {
                form.GetControlByName<JobOpeningViewModel>(j => j.RecruiterIds).IsReadOnly = true;
                form.GetControlByName<JobOpeningViewModel>(j => j.RecrutmentOwnerIds).IsReadOnly = true;
            }

            if (model.Status == JobOpeningStatus.Closed)
            {
                form.Controls.ToList().ForEach(c => c.IsReadOnly = true);
            }
            else
            {
                form.GetControlByName<JobOpeningViewModel>(j => j.ClosedComment).Visibility = ControlVisibility.Hide;
                form.GetControlByName<JobOpeningViewModel>(j => j.ClosedReason).Visibility = ControlVisibility.Hide;
            }
        }

        public override object GetViewModelFromContext(object childContext)
        {
            var jobOpening = (JobOpeningViewModel)base.GetViewModelFromContext(childContext);

            var jobOpeningUrl = Url.UriActionGet<JobOpeningController>(c => c.Details(jobOpening.Id), KnownParameter.None, RoutingHelper.IdParameterName, jobOpening.Id.ToString()).Url;

            var panelTitleText = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewRecruitmentJobOpening)
                ? $"<a href=\"{jobOpeningUrl}\" target=\"_blank\">{jobOpening}</a>"
                : $"{jobOpening}";

            var panel = new PanelViewModel(panelTitleText);

            panel.AddProperty(JobOpeningResources.CreatedBy, jobOpening.CreatedByFullName);

            if (!string.IsNullOrWhiteSpace(jobOpening.AcceptingEmployeeFullName))
            {
                panel.AddProperty(JobOpeningResources.AcceptingEmployeeId, jobOpening.AcceptingEmployeeFullName);
            }

            panel.AddProperty(JobOpeningResources.Status, jobOpening.Status.GetDescription());

            panel.StartNewGroup();

            panel.AddProperty(JobOpeningResources.HiringModeLabel, jobOpening.HiringMode.GetDescription());

            if (!string.IsNullOrWhiteSpace(jobOpening.CustomerName))
            {
                panel.AddProperty(JobOpeningResources.AccountId, jobOpening.CustomerName);
            }

            panel.StartNewGroup();

            panel.AddProperty(JobOpeningResources.CityIds, string.Join(", ", jobOpening.CityNames));

            return panel;
        }
    }
}