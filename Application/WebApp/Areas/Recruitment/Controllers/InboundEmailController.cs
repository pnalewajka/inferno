﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;
using LayoutClass = Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts.Layout;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
    public class InboundEmailController : CardIndexController<InboundEmailViewModel, InboundEmailDto>
    {
        private readonly IDocumentDownloadService _documentDownloadService;
        private IInboundEmailCardIndexDataService _inboundEmailCardIndexDataService;
        private readonly ICustomerCardIndexDataService _customerService;
        private readonly IInboundEmailService _inboundEmailService;
        private const string AssignButtonGroup = "assign";
        private const string EmailColorParameter = "emailColor";

        public InboundEmailController(
            IInboundEmailCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IDocumentDownloadService documentDownloadService,
            IApplicationOriginService applicationOriginService,
            ICustomerCardIndexDataService customerCardIndexDataService,
            IInboundEmailService inboundEmailService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _inboundEmailCardIndexDataService = cardIndexDataService;
            _documentDownloadService = documentDownloadService;
            _customerService = customerCardIndexDataService;
            _inboundEmailService = inboundEmailService;
            Layout.Parameters.AddParameter("RecommendationApplicationOriginId", applicationOriginService.GetApplicationOriginId(ApplicationOriginType.Recommendation));

            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/InboundEmail.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/Candidate.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/DocumentViewer.js");

            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.AssignMessageConfirmationMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.UnassignMessageConfirmationMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.ChangeCategoryConfirmationMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.SendForResearchConfirmationMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.CloseDialogTitle));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.SelectedCandidateMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.SelectedRecommendingPersonMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.NotSelectedOriginMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.NotSelectedCandidateMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.NotSelectedRecommendingPersonMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.SelectedJobApplicationMessage));
            Layout.Resources.AddFrom<InboundEmailResources>(nameof(InboundEmailResources.NotSelectedConsentTypeMessage));

            CardIndex.Settings.Title = InboundEmailResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddInboundEmail;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewInboundEmail;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditInboundEmail;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteInboundEmail;
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = $"(row.status !== {(int)InboundEmailStatus.Closed})"
            };

            CardIndex.Settings.ToolbarButtons.Add(GetAssignCommandButton());
            CardIndex.Settings.ToolbarButtons.Add(GetUnassignCommandButton());
            CardIndex.Settings.ToolbarButtons.Add(GetSendForResearchCommandButton());
            CardIndex.Settings.ToolbarButtons.Add(GetColorCategoryCommandButton());

            CardIndex.Settings.FilterGroups = GetInboundEmailStatusFilter();

            CardIndex.Settings.AddFormCustomization = form =>
                {
                    form.GetControlByName<InboundEmailViewModel>(m => m.Body).IsRequired = true;
                    form.GetControlByName<InboundEmailViewModel>(m => m.BodyDisplay).Visibility = ControlVisibility.Remove;
                };

            CardIndex.Settings.ViewFormCustomization = CardIndex.Settings.EditFormCustomization = form =>
                    {
                        var model = form.GetViewModel<InboundEmailViewModel>();

                        form.GetControlByName<InboundEmailViewModel>(m => m.To).IsReadOnly = true;
                        form.GetControlByName<InboundEmailViewModel>(m => m.FromEmailAddress).IsReadOnly = true;
                        form.GetControlByName<InboundEmailViewModel>(m => m.FromFullName).IsReadOnly = true;
                        form.GetControlByName<InboundEmailViewModel>(m => m.Subject).IsReadOnly = true;
                        form.GetControlByName<InboundEmailViewModel>(m => m.SentOn).IsReadOnly = true;
                        form.GetControlByName<InboundEmailViewModel>(m => m.ReceivedOn).IsReadOnly = true;
                        form.GetControlByName<InboundEmailViewModel>(m => m.Documents).IsReadOnly = true;

                        form.GetControlByName<InboundEmailViewModel>(m => m.Body).Visibility = ControlVisibility.Hide;
                        form.GetControlByName<InboundEmailViewModel>(m => m.BodyType).Visibility = ControlVisibility.Hide;

                        if (model.Status == InboundEmailStatus.Assigned && model.Category == InboundEmailCategory.Recommendation)
                        {
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.IsEligibleForReferralProgram).IsReadOnly = true;
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.RecommendingPersonId).IsReadOnly = true;
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.RecommendingPersonFirstName).IsReadOnly = true;
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.RecommendingPersonLastName).IsReadOnly = true;
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.RecommendingPersonEmailAddress).IsReadOnly = true;
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.RecommendingPersonPhoneNumber).IsReadOnly = true;
                        }

                        if (model.Status != InboundEmailStatus.New)
                        {
                            var categoryField = form.GetControlByName<InboundEmailViewModel>(a => a.Category);

                            categoryField.ControlWidth = LayoutClass.MediumControlWidth;
                            categoryField.IsReadOnly = true;
                        }

                        if (model.Status != InboundEmailStatus.SentForResearch)
                        {
                            form.GetControlByName<RecommendationInboundEmailViewModel>(m => m.ResearchingComment).Visibility = ControlVisibility.Hide;
                        }
                    };

            CardIndex.Settings.ValidationCustomization += ValidationCustomization;

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
            Layout.Css.Add("~/Areas/Recruitment/Content/DocumentViewer.css");

            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;
            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(SearchAreaCodes.Candidate,  InboundEmailResources.SearchAreaCandidate),
                new SearchArea(SearchAreaCodes.RecommendigPerson,  InboundEmailResources.SearchAreaRecommendingPerson)
            };
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            _inboundEmailCardIndexDataService.LogDataActivity(id, ActivityType.ViewedRecord, GetType().Name);

            return base.View(id);
        }

        private void ValidationCustomization(object model, ModelStateDictionary modelState)
        {
            var inboundEmailViewModel = (InboundEmailViewModel)model;

            if (!modelState.IsValid || inboundEmailViewModel.WorkflowAction == InboundEmailWorkflowAction.Refresh)
            {
                inboundEmailViewModel.WorkflowAction = null;
                modelState.Invalidate();
            }
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
        public ActionResult ViewFile(long id)
        {
            TempData[GetTempDataKey(id)] = DownloadJustification.CreateViewedDocumentDownloadJustification();

            Layout.MasterName = null;

            var viewModel = new DocumentViewModel
            {
                DocumentId = id,
                DocumentName = _inboundEmailCardIndexDataService.GetDocumentNameById(id)
            };

            var dialogModel = new DocumentViewerViewModel(viewModel);

            return PartialView("InboundEmailDocumentViewer", dialogModel);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
        public ActionResult Download(long id)
        {
            return Download(new DownloadJustificationViewModel(id));
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
        public ActionResult PerformDownload(DownloadJustificationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Download(model);
            }

            var customer = _customerService.GetRecordById(model.CustomerId);

            TempData[GetTempDataKey(model.DocumentId)] = DownloadJustification.CreateDocumentSharedWithCustomerDownloadJustification(customer, model.Comment);

            return DialogHelper.Redirect(this, Url.Action(nameof(PerformDownload), new { Id = model.DocumentId }));
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
        public ActionResult PerformDownload(long id)
        {
            var inboundEmailId = _inboundEmailCardIndexDataService.GetInboundEmailIdByDocumentId(id);
            var tempDataKey = GetTempDataKey(id);
            var downloadJustification = TempData[tempDataKey] as DownloadJustification;

            if (Request?.UrlReferrer?.LocalPath == "/Scripts/ViewerJS/")
            {
                TempData.Keep(tempDataKey);
            }

            if (downloadJustification == null)
            {
                AddAlert(AlertType.Error, InboundEmailResources.NoDownloadJustificationError);

                return RedirectToAction(nameof(View), new RouteValueDictionary { { "id", inboundEmailId } });
            }

            var documentDownloadDto = _documentDownloadService.GetDocument("Documents.InboundEmailDocumentMapping", id);

            _inboundEmailCardIndexDataService.LogDataActivity(
                inboundEmailId,
                downloadJustification.ActivityType,
                GetType().Name,
                documentDownloadDto.DocumentName,
                downloadJustification.Text);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAssignRecruitmentInboundEmail)]
        public ActionResult AssignMessage(long inboundEmailId, long employeeId)
        {
            var result = _inboundEmailCardIndexDataService.AssignMessage(inboundEmailId, employeeId);
            AddAlerts(result);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return Redirect(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanUnassignRecruitmentInboundEmail)]
        public ActionResult UnassignMessage(long inboundEmailId)
        {
            var result = _inboundEmailCardIndexDataService.UnassignMessage(inboundEmailId);
            AddAlerts(result);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return Redirect(listActionUrl);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAssignRecruitmentInboundEmail)]
        public ActionResult SendForResearch(long inboundEmailId)
        {
            var result = _inboundEmailCardIndexDataService.SendForResearch(inboundEmailId);
            AddAlerts(result);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return Redirect(listActionUrl);
        }

        public override ActionResult Edit(InboundEmailViewModel viewModelRecord)
        {
            var response = base.Edit(viewModelRecord);

            if ((response is RedirectResult) && viewModelRecord.WorkflowAction == InboundEmailWorkflowAction.ChangeCategory)
            {
                return RedirectToAction("Edit", viewModelRecord.Id);
            }

            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableWokflowButtons(viewModelRecord.Id));

            return response;
        }

        public override ActionResult Edit(long id)
        {
            _inboundEmailCardIndexDataService.LogDataActivity(id, ActivityType.ViewedRecord, GetType().Name);
            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableWokflowButtons(id));

            return base.Edit(id);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanSetColorCategoryToInboundEmails)]
        public ActionResult SetEmailColorCategory(long id, InboundEmailColorCategory? emailColor)
        {
            _inboundEmailService.SetEmailColorCategory(id, emailColor);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

            return new RedirectResult(listActionUrl);
        }

        private CommandButton GetAssignCommandButton()
        {
            return new ToolbarButton
            {
                Text = InboundEmailResources.AssignMessageButtonText,
                OnClickAction = new JavaScriptCallAction($"InboundEmail.assignMessage(grid);"),
                Icon = FontAwesome.User,
                RequiredRole = SecurityRoleType.CanAssignRecruitmentInboundEmail,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.canBeAssigned",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = AssignButtonGroup
            };
        }

        private CommandButton GetUnassignCommandButton()
        {
            return new ToolbarButton
            {
                Text = InboundEmailResources.UnassignMessageButtonText,
                OnClickAction = new JavaScriptCallAction($"InboundEmail.unassignMessage(grid);"),
                Icon = FontAwesome.Recycle,
                RequiredRole = SecurityRoleType.CanUnassignRecruitmentInboundEmail,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.canBeUnassigned",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = AssignButtonGroup
            };
        }

        private CommandButton GetSendForResearchCommandButton()
        {
            return new ToolbarButton
            {
                Text = InboundEmailResources.SendForResearchMessageButtonText,
                OnClickAction = new JavaScriptCallAction($"InboundEmail.sendForResearch(grid);"),
                Icon = FontAwesome.ArrowRight,
                RequiredRole = SecurityRoleType.CanSendInboundEmailForResearch,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"row.status === {(int)InboundEmailStatus.Assigned}"
                        + $"&& row.category !== {(int)InboundEmailCategory.Unknown}"
                        + $"&& row.category !== {(int)InboundEmailCategory.Spam}",
                    Mode = AvailabilityMode.SingleRowSelected
                }
            };
        }

        private CommandButton GetColorCategoryCommandButton()
        {
            var addButton = new ToolbarButton
            {
                Text = InboundEmailResources.EmailColorCategoryButtonLabel,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                },
                RequiredRole = SecurityRoleType.CanSetColorCategoryToInboundEmails
            };

            var emailColors = EnumHelper.GetEnumValues<InboundEmailColorCategory>()
                .Select(c =>
                    new CommandButton
                    {
                        RequiredRole = SecurityRoleType.CanSetColorCategoryToInboundEmails,
                        Text = c.GetDescriptionOrValue(),
                        AutomationIdentifier = InboundEmailHelper.GetEmailColorCategoryCssClass(c),
                        OnClickAction = Url.UriActionPost(nameof(InboundEmailController.SetEmailColorCategory),
                            RoutingHelper.GetControllerName<InboundEmailController>(),
                            KnownParameter.SelectedId | KnownParameter.Context, EmailColorParameter, c.ToString()),
                    }
                ).ToList<ICommandButton>();

            var noColorCategory = new CommandButton
            {
                RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpening,
                Text = InboundEmailResources.NoColorCategoryLabel,
                OnClickAction = Url.UriActionPost(nameof(InboundEmailController.SetEmailColorCategory),
                        RoutingHelper.GetControllerName<InboundEmailController>(),
                        KnownParameter.SelectedId),
            };

            emailColors.Add(noColorCategory);

            addButton.OnClickAction = new DropdownAction
            {
                Items = emailColors
            };

            return addButton;
        }

        private Action<CardIndexRecordViewModel<InboundEmailViewModel>> CreateButtonAction(string name, string text, string onClickJavaScript)
        {
            return (CardIndexRecordViewModel<InboundEmailViewModel> model) =>
            {
                model.Buttons.Add(new FooterButton(name, text)
                {
                    OnClickAction = new JavaScriptCallAction(onClickJavaScript),
                    Attributes = FooterButtonBuilder.SubmitFormButtonAttribures()
                });
            };
        }

        private IList<IFooterButton> GetAvaliableWokflowButtons(long id)
        {
            var avaliableButtons = new List<IFooterButton>();

            var inboundEmail = _inboundEmailCardIndexDataService.GetRecordById(id);

            var category = inboundEmail.Category;
            var currentPrincipal = GetCurrentPrincipal();

            if ((inboundEmail.Status == InboundEmailStatus.New || inboundEmail.Status == InboundEmailStatus.SentForResearch)
                && currentPrincipal.IsInRole(SecurityRoleType.CanAssignRecruitmentInboundEmail)
                && (category != InboundEmailCategory.Unknown)
                && (category != InboundEmailCategory.Spam))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(InboundEmailWorkflowAction.Assign), InboundEmailResources.AssignMessageButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.assignMessageInForm()"),
                        Icon = FontAwesome.User,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );
            }

            if (inboundEmail.Status == InboundEmailStatus.Assigned
                && currentPrincipal.IsInRole(SecurityRoleType.CanUnassignRecruitmentInboundEmail))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(InboundEmailWorkflowAction.Unassign), InboundEmailResources.UnassignMessageButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.unassignMessageInForm()"),
                        Icon = FontAwesome.Recycle,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanCloseRecruitmentInboundEmail) && category != InboundEmailCategory.Unknown)
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(InboundEmailWorkflowAction.Close), InboundEmailResources.CloseInboundEmailButtonLabel)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.closeFromViewForm({id}, 'Models.{nameof(InboundEmailCloseViewModel)}');"),
                        Icon = FontAwesome.Times,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanAssignRecruitmentInboundEmail)
                && currentPrincipal.IsInRole(SecurityRoleType.CanSendInboundEmailForResearch)
                && category != InboundEmailCategory.Spam
                && category != InboundEmailCategory.Unknown
                && inboundEmail.Status != InboundEmailStatus.SentForResearch)
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(InboundEmailWorkflowAction.SendForResearch), InboundEmailResources.SendForResearchMessageButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.sendForResearchMessageInForm()"),
                        Icon = FontAwesome.ArrowRight,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );
            }

            if (!(category == InboundEmailCategory.JobApplication
                || category == InboundEmailCategory.Recommendation))
            {
                return avaliableButtons;
            }

            var editAndStayButton = FooterButtonBuilder.EditAndStayButton();
            editAndStayButton.CssClass = "hidden";

            avaliableButtons.Add(editAndStayButton);

            if (currentPrincipal.IsInRole(SecurityRoleType.CanAddJobApplications))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(JobApplicationController), InboundEmailResources.AddJobApplicationButtonLabel)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.addJobApplication()"),
                        Icon = FontAwesome.Plus,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );

            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanAddRecruitmentCandidate))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(CandidateController), InboundEmailResources.AddRecruitmentCandidateButtonLabel)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.addCandidate()"),
                        Icon = FontAwesome.Plus,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );
            }

            if (category == InboundEmailCategory.Recommendation
                && inboundEmail.Status == InboundEmailStatus.New
                && currentPrincipal.IsInRole(SecurityRoleType.CanAddRecommendingPersons))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(RecommendingPersonController), InboundEmailResources.AddRecommendingPersonButtonLabel)
                    {
                        OnClickAction = new JavaScriptCallAction($"InboundEmail.addRecommendingPerson()"),
                        Icon = FontAwesome.Plus,
                        ButtonStyleType = CommandButtonStyle.Default
                    }
                );

            }

            return avaliableButtons;
        }

        private List<FilterGroup> GetInboundEmailStatusFilter()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = String.Empty,
                    Type = FilterGroupType.RadioGroup,
                    Filters = (new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.StatusDefault,
                            DisplayName = InboundEmailResources.StatusNonClosedLabel,
                        },
                        new Filter
                        {
                            Code = FilterCodes.InboundEmailsUnread,
                            DisplayName = InboundEmailResources.UnreadLabel,
                        },
                        new Filter
                        {
                            Code = FilterCodes.StatusAll,
                            DisplayName = InboundEmailResources.StatusAllLabel,
                        }
                    })
                },
                new FilterGroup
                {
                    DisplayName = InboundEmailResources.Status,
                    Type = FilterGroupType.RadioGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<InboundEmailStatus>()
                },
                new FilterGroup
                {
                    DisplayName = InboundEmailResources.Owner,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.MyRecords,
                            DisplayName = InboundEmailResources.MyRecordsFilterLabel,
                        },
                        new Filter
                        {
                            Code = FilterCodes.AnyOwner,
                            DisplayName = InboundEmailResources.AnyOwnerFilterLabel,
                        }
                    }
                },
                new FilterGroup
                {
                    DisplayName = InboundEmailResources.EmailColorCategoriesFilterLabel,
                    Type = FilterGroupType.RadioGroup,
                    Filters = ColorFilters
                },
            };
        }

        private IList<Filter> ColorFilters
        {
            get
            {
                var filters = CardIndexHelper.BuildEnumBasedFilters<InboundEmailColorCategory>(null, colorCategory => InboundEmailHelper.GetEmailColorCategoryCssClass(colorCategory));
                filters.Add(new Filter
                {
                    Code = FilterCodes.InboundEmailAllColors,
                    DisplayName = InboundEmailResources.AllColorCategoriesLabel,
                });

                return filters;
            }
        }

        private Action<CardIndexRecordViewModel<InboundEmailViewModel>> CreateEditWorkflowAction(string name, string displayName, InboundEmailWorkflowAction action, string confirmTitle = null, string confirmContent = null)
        {
            return CreateWorkflowAction(FooterButtonBuilder.EditButton(), name, displayName, action, confirmTitle, confirmContent);
        }

        private Action<CardIndexRecordViewModel<InboundEmailViewModel>> CreateWorkflowAction(IFooterButton button, string name, string displayName, InboundEmailWorkflowAction action, string confirmTitle = null, string confirmContent = null)
        {
            return (CardIndexRecordViewModel<InboundEmailViewModel> model) =>
            {
                button.Id = name;
                button
                    .SetDisplayText(displayName)
                    .SetSubmitParameter<InboundEmailViewModel>((int)action, a => a.WorkflowAction);

                if (!string.IsNullOrEmpty(confirmTitle) && !string.IsNullOrEmpty(confirmContent))
                {
                    button.SetRequireConfirmation(confirmTitle, confirmContent);
                }

                model.Buttons.Add(button);
            };
        }

        private ActionResult Download(DownloadJustificationViewModel model)
        {
            var documentName = _inboundEmailCardIndexDataService.GetDocumentNameById(model.DocumentId);
            var dialogViewModel = new ModelBasedDialogViewModel<DownloadJustificationViewModel>(model)
            {
                Title = string.Format(InboundEmailResources.DocumentShareDialogTitle, documentName),
                LabelWidth = LabelWidth.Large
            };

            return PartialView(
                    "InboundEmailDownloadJustification",
                    dialogViewModel
                    );
        }

        private string GetTempDataKey(long id)
        {
            return $"document-download-justification:{id}";
        }
    }
}