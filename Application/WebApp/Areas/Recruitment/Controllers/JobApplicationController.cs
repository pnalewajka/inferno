﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.JobApplicationController")]
    [AtomicAuthorize(SecurityRoleType.CanViewJobApplications)]
    public class JobApplicationController : CardIndexController<JobApplicationViewModel, JobApplicationDto>
    {
        internal const string OriginTypeParameterName = "origin-type";
        private IJobApplicationCardIndexDataService _cardIndexDataService;

        public JobApplicationController(IJobApplicationCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = JobApplicationResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddJobApplications;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewJobApplications;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditJobApplications;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteJobApplications;

            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;
            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.FilterGroups = GetFilterGroups();

            SetAddJobApplicationDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this));
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        internal static void SetAddJobApplicationDropdownItems(ICommandButton addButton, UrlHelper urlHelper, string controllerName, KnownParameter knownParameters = KnownParameter.None)
        {
            var originTypes = EnumHelper.GetEnumValues<ApplicationOriginType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var originType in originTypes)
            {
                var typeValueName = NamingConventionHelper.ConvertPascalCaseToHyphenated(originType.ToString());

                var addItemButton = new CommandButton
                {
                    Group = JobApplicationResources.OriginTypeLabel,
                    RequiredRole = SecurityRoleType.CanAddRecruitmentJobOpening,
                    Text = originType.GetDescriptionOrValue(),
                    OnClickAction = JavaScriptCallAction.ShowDialog(urlHelper.UriActionPost(nameof(GetAddDialog), controllerName, KnownParameter.Context | knownParameters, OriginTypeParameterName, typeValueName))
                };

                addDropdownItems.Add(addItemButton);
            }

            addButton.OnClickAction = new DropdownAction
            {
                Items = addDropdownItems
            };
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddRecruitmentDataConsent)]
        public string GetOriginType(long applicationOriginId)
        {
            return NamingConventionHelper.ConvertPascalCaseToHyphenated(_cardIndexDataService.GetOriginType(applicationOriginId).ToString());
        }

        private object GetDefaultNewRecord()
        {
            var contentTypeName = Request.QueryString[OriginTypeParameterName];
            var contentType = EnumHelper.GetEnumValueOrDefault<ApplicationOriginType>(contentTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (contentType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            var dto = _cardIndexDataService.GetDefaultNewRecord(contentType.Value);

            return dto;
        }

        private IList<FilterGroup> GetFilterGroups()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Code = FilterCodes.MyRecords,
                            DisplayName = JobApplicationResources.MyJobApplicationRecordsLabel
                        },
                        new Filter
                        {
                            Code = FilterCodes.AnyOwner,
                            DisplayName = JobApplicationResources.AllJobApplicationRecordsLabel
                        },
                    }
                }
            };
        }
    }
}