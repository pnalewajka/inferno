﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers.Buttons;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcess)]
    public class JobOpeningRecruitmentProcessController : SubCardIndexController<RecruitmentProcessViewModel, RecruitmentProcessDto, JobOpeningController, RecruitmentProcessContext>
    {
        private readonly IJobOpeningRecruitmentProcessCardIndexDataService _cardIndexDataService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IAnonymizationService _anonymizationService;

        public JobOpeningRecruitmentProcessController(
            IJobOpeningRecruitmentProcessCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;
            _anonymizationService = baseControllerDependencies.AnonymizationService;

            RecruitmentProcessController.ConfigureRecruitmentProcessController(CardIndex, Url, Layout, GetCurrentPrincipal());
            CardIndex.Settings.ToolbarButtons.AddRange(RecruitmentProcessButtons.GetToolbarButtons(Url, nameof(RecruitmentProcessContext.JobOpeningId)));
            CardIndex.Settings.RowButtons.AddRange(RecruitmentProcessButtons.GetRowButtons(Url));
            CardIndex.Settings.EditFormCustomization += SetReadOnlyFieldIfNecessary;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.GridViewConfigurations = GetGridConfiguration();
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            RecruitmentProcessController.ConfigureViewButtons(CardIndex, GetCurrentPrincipal(), id);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            RecruitmentProcessController.ConfiguteEditButtons(CardIndex, GetCurrentPrincipal(), id);

            return base.Edit(id);
        }

        [HttpGet]
        public JsonNetResult GetJson(GridParameters parameters)
        {
            var classMappingDto = _classMappingFactory.CreateMapping<RecruitmentProcessDto, RecruitmentProcessViewModel>();
            var classMappingViewModel = _classMappingFactory.CreateMapping<RecruitmentProcessViewModel, RecruitmentProcessDto>();

            var search = parameters.ToCriteria(
                classMappingViewModel,
                _anonymizationService,
                Enumerable.Empty<string>(),
                CardIndex.Settings.FilterGroups);

            var candidateProcesses = _cardIndexDataService.GetRecords(search);
            var candidateProcessesAsViewModels = candidateProcesses.Rows.Select(classMappingDto.CreateFromSource);

            return new JsonNetResult(candidateProcessesAsViewModels)
            {
                SerializerSettings = JsonHelper.DefaultAjaxSettings
            };
        }

        private void SetReadOnlyFieldIfNecessary(FormRenderingModel form)
        {
            if (!GetCurrentPrincipal().IsInRole(SecurityRoleType.CanEditRecruitmentProcessCoordinators))
            {
                form.GetControlByName<RecruitmentProcessViewModel>(j => j.RecruiterId).IsReadOnly = true;
            }
        }

        private IList<IGridViewConfiguration> GetGridConfiguration()
        {
            var configuration = new GridViewConfiguration<RecruitmentProcessViewModel>(RecruitmentProcessResources.Title, string.Empty) { IsDefault = true };
            configuration.IncludeAllColumns();

            var currentPrincipal = GetCurrentPrincipal();

            if (currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentCandidate))
            {
                configuration.ExcludeColumn(m => m.CandidateId);
            }
            else
            {
                configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetCandidateLink));
            }

            configuration.ExcludeColumn(x => x.JobOpeningId);

            return new List<IGridViewConfiguration> { configuration };
        }

        private object GetDefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.JobOpeningId = Context.ParentId;

            return dto;
        }
    }
}