﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Models;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentDataConsent)]
    public class RecommendingPersonDataConsentController : SubCardIndexController<DataConsentViewModel, DataConsentDto, RecommendingPersonController, ParentIdContext>
    {
        private IRecommendingPersonDataConsentCardIndexDataService _cardIndexDataService;

        public RecommendingPersonDataConsentController(
            IRecommendingPersonDataConsentCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;

            CardIndex.Settings.Title = DataConsentResources.Title;

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentDataConsent;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentDataConsent;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentDataConsent;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentDataConsent;

            DataConsentController.ApplyFormConfiguration(CardIndex.Settings);
            DataConsentController.SetAddDataConsentDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this), nameof(GetAddDialog));
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        private object GetDefaultNewRecord()
        {
            var consentTypeName = Request.QueryString[DataConsentController.ConsentTypeParameterName];
            var consentType = EnumHelper.GetEnumValueOrDefault<DataConsentType>(consentTypeName, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (consentType == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            return _cardIndexDataService.GetDefaultNewRecord(Context.ParentId, consentType.Value);
        }
    }
}