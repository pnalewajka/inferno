﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Extensions;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers.ViewCustomization;
using Smt.Atomic.WebApp.Areas.Recruitment.Helpers;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcessSteps)]
    public class RecruitmentProcessStepController : SubCardIndexController<RecruitmentProcessStepViewModel, RecruitmentProcessStepDto, RecruitmentProcessController, RecruitmentProcessContext>
    {
        private readonly IRecruitmentProcessStepCardIndexDataService _cardIndexDataService;
        private readonly IRecruitmentProcessStepService _recruitmentProcessStepService;
        private readonly IFormCustomization _formCustomization;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IRecruitmentProcessService _recruitmentProcessService;

        internal const string TypeParameterName = "type";
        internal const string AllowCloseDecisionParameterName = "allow-close-decision";
        private const string ReopenButtonGroup = "reopen";

        public RecruitmentProcessStepController(
           IRecruitmentProcessStepCardIndexDataService cardIndexDataService,
           IBaseControllerDependencies baseControllerDependencies,
           IRecruitmentProcessStepService recruitmentProcessStepService,
           IRecruitmentProcessService recruitmentProcessService,
           IPrincipalProvider principalProvider,
           IClassMappingFactory classMappingFactory,
           IDataActivityLogService dataActivityLogService)
           : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _recruitmentProcessStepService = recruitmentProcessStepService;
            _principalProvider = principalProvider;
            _recruitmentProcessService = recruitmentProcessService;
            _classMappingFactory = classMappingFactory;
            _dataActivityLogService = dataActivityLogService;

            _formCustomization = new RecruitmentProcessStepFormCustomization();
            CardIndex.Settings.Title = RecruitmentProcessStepResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddRecruitmentProcessSteps;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewRecruitmentProcessSteps;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditRecruitmentProcessSteps;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteRecruitmentProcessSteps;

            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/RecruitmentProcessStep.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/RecruitmentProcess.js");
            Layout.Scripts.Add("~/Areas/Recruitment/Scripts/DocumentViewer.js");
            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
            Layout.Css.Add("~/Areas/Recruitment/Content/DocumentViewer.css");

            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ToolbarButtonConfirmationDialogTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ReopenStepConfirmationMessage));

            CardIndex.Settings.DefaultNewRecord = GetDefaultNewRecord;
            CardIndex.Settings.GridViewConfigurations = GetGridSettings();
            CardIndex.Settings.AllowMultipleRowSelection = false;
            CardIndex.Settings.ModificationTracking = FormModificationTracking.MarkEditedField;

            SetAddRecruitmentProcessStepDropdownItems(CardIndex.Settings.AddButton, Url, RoutingHelper.GetControllerName(this));

            ApplyFormCustomizations(GetCurrentPrincipal());
            ApplyButtonCustomization();

            CardIndex.Settings.ValidationCustomization += ValidationCustomization;

            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.RejectDialogTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.CloseRecruitmentProcessDialogTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.NextStepAddedSuccessfullyText));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.NextStepAddedSuccessfullyDialogTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ConfirmationQuestion));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ConfirmationTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ErrorMessageTitle));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.ContractNegotiationsFinalErrorMessage));
            Layout.Resources.AddFrom<RecruitmentProcessStepResources>(nameof(RecruitmentProcessStepResources.NoteDialogTitle));

            if (Context == null || Context.ParentId == BusinessLogicHelper.NonExistingId)
            {
                var stepOwnerFilters = new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    Filters = new List<Filter>
                    {
                        new Filter {Code = FilterCodes.MyProcessRecords, DisplayName = RecruitmentProcessStepResources.MyProcessRecordsFilterLabel},
                        new Filter {Code = FilterCodes.MyRecords, DisplayName = RecruitmentProcessStepResources.MyRecordsFilterLabel},
                        new Filter {Code = FilterCodes.AnyOwner, DisplayName = RecruitmentProcessStepResources.AnyOwnerFilterLabel}
                    }
                };

                CardIndex.Settings.FilterGroups.Add(stepOwnerFilters);
            }

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewStepDetailsWhenDone) && (Context == null || Context.ParentId == BusinessLogicHelper.NonExistingId))
            {
                var stepStatusFilters = new FilterGroup
                {
                    DisplayName = RecruitmentProcessStepResources.Status,
                    Type = FilterGroupType.RadioGroup,
                    Filters = (new List<Filter>
                            {
                                new Filter
                                {
                                    Code = FilterCodes.RecruitmentProcessStepStatusDefault,
                                    DisplayName = RecruitmentProcessResources.StepFilterDefault,
                                },
                                new Filter
                                {
                                    Code = FilterCodes.RecruitmentProcessStepStatusAll,
                                    DisplayName = RecruitmentProcessResources.StepFilterAll,
                                }
                            })
                        .Union(CardIndexHelper.BuildEnumBasedFilters<RecruitmentProcessStepStatus>()).ToList()
                };

                CardIndex.Settings.FilterGroups.Add(stepStatusFilters);
            }

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanFilterRecruitmentProcessStepsByType) && (Context == null || Context.ParentId == BusinessLogicHelper.NonExistingId))
            {
                var stepTypeFilters = new FilterGroup
                {
                    DisplayName = RecruitmentProcessStepResources.Type,
                    Type = FilterGroupType.RadioGroup,
                    Filters = CardIndexHelper.BuildEnumBasedFilters<RecruitmentProcessStepType>()
                };

                CardIndex.Settings.FilterGroups.Add(stepTypeFilters);
            }

            SetupRowButtons();
        }

        internal static void SetAddRecruitmentProcessStepDropdownItems(ICommandButton addButton, UrlHelper urlHelper, string controllerName, KnownParameter knownParameter = KnownParameter.None)
        {
            var processSteps = EnumHelper.GetEnumValues<RecruitmentProcessStepType>();
            var addDropdownItems = new List<ICommandButton>();

            foreach (var processStep in processSteps)
            {
                var stepName = NamingConventionHelper.ConvertPascalCaseToHyphenated(processStep.ToString());

                var addStepButton = new CommandButton
                {
                    Text = processStep.GetDescriptionOrValue(),
                    RequiredRole = GetRequiredSecurityRole(processStep),
                    OnClickAction = JavaScriptCallAction.ShowDialog(urlHelper.UriActionPost(nameof(GetAddDialog), controllerName, KnownParameter.Context | knownParameter, TypeParameterName, stepName))
                };

                addDropdownItems.Add(addStepButton);
            }

            addButton.OnClickAction = new DropdownAction { Items = addDropdownItems };
            addButton.RequiredRole = SecurityRoleType.CanAddRecruitmentProcessSteps;
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            if (Context.ParentId == BusinessLogicHelper.NonExistingId)
            {
                CardIndex.Settings.AddButton.Availability = new DisabledCommandButtonAvailability();
            }

            return base.List(parameters);
        }

        private void AddPreviousStepRelatedCustomization(FormRenderingModel form)
        {
            const string PreviousStepTypeParameterName = "source-step-type";

            var previousStepType = Request.QueryString[PreviousStepTypeParameterName];

            if (previousStepType.IsNullOrEmpty())
            {
                return;
            }

            var closePreviousStepControl = form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.CloseCurrentStep);

            closePreviousStepControl.Visibility = ControlVisibility.Show;
            closePreviousStepControl.IsReadOnly = Request.QueryString[AllowCloseDecisionParameterName] != "true";
            closePreviousStepControl.Label = string.Format(RecruitmentProcessStepResources.CloseStep, previousStepType);

            form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.RecruitmentProcessId).Visibility = ControlVisibility.Hide;
            form.GetControlByName<RecruitmentProcessStepViewModel>(vm => vm.Details).Visibility = ControlVisibility.Remove;
        }

        private static SecurityRoleType? GetRequiredSecurityRole(RecruitmentProcessStepType processStep)
        {
            switch (processStep)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                case RecruitmentProcessStepType.TechnicalReview:
                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                case RecruitmentProcessStepType.HiringManagerInterview:
                case RecruitmentProcessStepType.ClientInterview:
                    return null;

                case RecruitmentProcessStepType.ClientResumeApproval:
                case RecruitmentProcessStepType.HiringDecision:
                case RecruitmentProcessStepType.ContractNegotiations:
                default:
                    return SecurityRoleType.CanAddAnyRecruitmentStep;
            }
        }

        private void ApplyFormCustomizations(IAtomicPrincipal currentUser)
        {
            CardIndex.Settings.AddFormCustomization = form => _formCustomization.CustomizeAddForm(form, currentUser, Request);
            CardIndex.Settings.EditFormCustomization = form => _formCustomization.CustomizeEditForm(form, currentUser, Request);
            CardIndex.Settings.ViewFormCustomization = form => _formCustomization.CustomizeViewForm(form, currentUser, Request);

            CardIndex.ConfigureEditButtons += (CardIndexRecordViewModel<RecruitmentProcessStepViewModel> model) =>
            {
                if (model.ViewModelRecord.Status == RecruitmentProcessStepStatus.Done
                    || model.ViewModelRecord.Status == RecruitmentProcessStepStatus.Cancelled)
                {
                    model.Buttons.Remove(model.Buttons.GetEditButton());
                }
            };
        }

        public override object GetContextViewModel()
        {
            if (Context.ParentId == BusinessLogicHelper.NonExistingId)
            {
                return null;
            }

            return base.GetContextViewModel();
        }

        private void ApplyButtonCustomization()
        {
            if (Context.ParentId != BusinessLogicHelper.NonExistingId
                && _recruitmentProcessService.GetStatus(Context.ParentId) == RecruitmentProcessStatus.Closed)
            {
                if (_recruitmentProcessService.CanProcessBeReopened(Context.ParentId))
                {
                    CardIndex.Settings.ToolbarButtons.Add(GetReopenProcessCommandButton(Context.ParentId));
                }
            }
            else
            {
                CardIndex.Settings.ToolbarButtons.Add(ReopenStepCommandButton());
            }

            if (!GetCurrentPrincipal().IsInRole(SecurityRoleType.CanAddRecruitmentStepViaDropdown) || !CanAddSteps())
            {
                CardIndex.Settings.AddButton.Availability = new DisabledCommandButtonAvailability();
            }
        }

        private void ValidationCustomization(object model, ModelStateDictionary modelState)
        {
            var stepViewModel = (RecruitmentProcessStepViewModel)model;

            if (!modelState.IsValid)
            {
                stepViewModel.WorkflowAction = null;

                return;
            }

            switch (stepViewModel.WorkflowAction)
            {
                case RecruitmentProcessStepWorkflowActionType.RefreshAndRunScript:
                    modelState.Invalidate();

                    Layout.Scripts.OnDocumentReady.Add(stepViewModel.WorkflowScript);

                    stepViewModel.WorkflowAction = null;
                    stepViewModel.WorkflowScript = null;

                    break;
            }
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanReopenProcessSteps)]
        public ActionResult ReopenProcessStep(long processStepId)
        {
            try
            {
                _cardIndexDataService.ReOpenProcessStep(processStepId);
            }
            catch (Exception exception)
            {
                AddAlert(AlertType.Error, exception.Message);
            }

            if (Context.ParentId != BusinessLogicHelper.NonExistingId)
            {
                var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

                return new RedirectResult(listActionUrl);
            }

            return RedirectToAction(nameof(List));
        }

        private static CommandButton GetReopenProcessCommandButton(long recruitmentProcessId)
        {
            return new ToolbarButton
            {
                Text = RecruitmentProcessStepResources.ReOpenProcessCommandButtonText,
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.ReopenProcess({recruitmentProcessId}, 'Models.{ nameof(RecruitmentProcessReopenViewModel) }');"),
                Icon = FontAwesome.Refresh,
                RequiredRole = SecurityRoleType.CanReopenRecruitmentProcesses,
                Group = ReopenButtonGroup
            };
        }

        private CommandButton ReopenStepCommandButton()
        {
            return new ToolbarButton
            {
                Text = RecruitmentProcessStepResources.ReOpenStepCommandButtonText,
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.reopenProcessStepFromGrid(grid, {Context.ParentId});"),
                Icon = FontAwesome.Refresh,
                RequiredRole = SecurityRoleType.CanReopenProcessSteps,
                Availability = new ToolbarButtonAvailability
                {
                    Predicate = $"(row.status === {(int)RecruitmentProcessStepStatus.Done} || row.status === {(int)RecruitmentProcessStepStatus.Cancelled}) && row.recruitmentProcessStatus !== {(int)RecruitmentProcessStatus.Closed}",
                    Mode = AvailabilityMode.SingleRowSelected
                },
                Group = ReopenButtonGroup
            };
        }

        public override ActionResult Edit(long id)
        {
            var candidateId = _recruitmentProcessStepService.GetCandidateId(id);

            _dataActivityLogService.LogCandidateDataActivity(candidateId, ActivityType.ViewedRecord, ReferenceType.RecruitmentProcessStep, id, GetType().Name);

            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id));

            return RedirectToListOnLimitedAccess(id) ?? base.Edit(id);
        }

        public override ActionResult Edit(RecruitmentProcessStepViewModel viewModelRecord)
        {
            CardIndex.ConfigureEditButtons += e => e.Buttons.AddRange(GetAvaliableButtons(viewModelRecord.Id));

            var response = base.Edit(viewModelRecord);

            if (response.GetType() != typeof(RedirectResult))
            {
                return response;
            }

            if (viewModelRecord.Type == RecruitmentProcessStepType.ContractNegotiations
               && viewModelRecord.WorkflowAction == RecruitmentProcessStepWorkflowActionType.ApproveCandidate)
            {
                var onboardingRequestId = _recruitmentProcessService.GetOnboardingRequestId(viewModelRecord.RecruitmentProcessId);

                return onboardingRequestId == null
                    ? RedirectToParentList(viewModelRecord)
                    : RedirectToAction("Edit", "Request", new { area = "Workflows", id = onboardingRequestId });
            }

            return viewModelRecord.WorkflowAction != null && !viewModelRecord.CloseCurrentStep
                ? RedirectToAction(nameof(Edit), new { viewModelRecord.Id })
                : RedirectToParentList(viewModelRecord);
        }

        public override ActionResult View(long id)
        {
            var candidateId = _recruitmentProcessStepService.GetCandidateId(id);

            _dataActivityLogService.LogCandidateDataActivity(candidateId, ActivityType.ViewedRecord, ReferenceType.RecruitmentProcessStep, id, GetType().Name);

            CardIndex.ConfigureViewButtons += e => e.Buttons.AddRange(GetAvaliableButtons(id));

            return RedirectToListOnLimitedAccess(id) ?? base.View(id);
        }

        [HttpPost]
        [AtomicAuthorize(SecurityRoleType.CanAddRecruitmentProcessNotes)]
        public void AddRecruitmentProcessNote(AddNoteViewModel viewModel)
        {
            var dialogNoteDto = _classMappingFactory.CreateMapping<AddNoteViewModel, AddNoteDto>().CreateFromSource(viewModel);

            _recruitmentProcessStepService.AddNoteToRecruitmentProcess(dialogNoteDto);
        }

        protected override void AddGoToParentButton(object context)
        {
            if (Context.ParentId == BusinessLogicHelper.NonExistingId)
            {
                return;
            }

            base.AddGoToParentButton(context);
        }

        private void SetupRowButtons()
        {
            var canViewRecruitmentTechnicalReviewSummary = GetCurrentPrincipal().IsInRole(SecurityRoleType.CanGenerateTechnicalReviewSummaryReport);

            if (canViewRecruitmentTechnicalReviewSummary)
            {
                CardIndex.Settings.RowButtons.Add(new RowButton
                {
                    Text = string.Empty,
                    OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.GenerateTechnicalReviewSummaryReport(this.rowData[0].id)"),
                    Icon = FontAwesome.Download,
                    Visibility = CommandButtonVisibility.Grid,
                    Availability = new RowButtonAvailability<RecruitmentProcessStepViewModel>
                    {
                        Predicate = r => r.Type == RecruitmentProcessStepType.TechnicalReview
                    }
                });
            }
        }

        private ActionResult RedirectToListOnLimitedAccess(long id)
        {
            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewStepDetailsWhenDone))
            {
                return null;
            }

            if (_cardIndexDataService.GetStatus(id) == RecruitmentProcessStepStatus.Done)
            {
                AddAlert(AlertType.Warning, RecruitmentProcessStepResources.NoPermissionToViewDoneRecruitmentProcessStepWarning);

                var listActionUrl = CardIndexHelper.GetReturnOrListUrl(this, Context);

                return new RedirectResult(listActionUrl);
            }

            return null;
        }

        private bool CanAddSteps()
        {
            if (Context.ParentId == BusinessLogicHelper.NonExistingId)
            {
                return false;
            }

            var processStatus = _recruitmentProcessService.GetStatus(Context.ParentId);

            return processStatus == RecruitmentProcessStatus.Active;
        }

        private IList<IGridViewConfiguration> GetGridSettings()
        {
            var configuration = new GridViewConfiguration<RecruitmentProcessStepViewModel>(TechnicalReviewResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();

            if (Context.ParentId != BusinessLogicHelper.NonExistingId)
            {
                configuration.ExcludeColumn(c => c.RecruitmentProcessId);
                configuration.ExcludeColumn(c => c.JobOpeningDecisionMakerId);
            }

            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllRecruitmentProcessSteps)
                && !_principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllRecruitmentProcessStepsFromMyRecruitmentProcesses))
            {
                configuration.ExcludeColumn(c => c.AssignedEmployeeId);
            }

            return new List<IGridViewConfiguration> { configuration };
        }

        private object GetDefaultNewRecord()
        {
            const string TypeParameterName = "type";

            var contentType = Request.GetQueryParameter<RecruitmentProcessStepType>(TypeParameterName);

            var dto = _cardIndexDataService.GetDefaultNewRecord(contentType, Context.ParentId);
            dto.CloseCurrentStep = Request.QueryString[AllowCloseDecisionParameterName] != "true";
            long fromStepId;

            if (long.TryParse(Request.QueryString["FromStepId"], out fromStepId))
            {
                dto.FromStepId = fromStepId;
            }

            return dto;
        }

        private IList<IFooterButton> GetAvaliableButtons(long processStepId)
        {
            var avaliableButtons = GetWorkflowButtons(processStepId);
            var currentStep = _cardIndexDataService.GetRecordById(processStepId);
            var currentPrincipal = GetCurrentPrincipal();
            const string reopenProcessButtonName = "reopwenProcessButton";

            if (currentStep.RecruitmentProcessStatus == RecruitmentProcessStatus.Closed)
            {
                if (_recruitmentProcessService.CanProcessBeReopened(currentStep.RecruitmentProcessId))
                {
                    avaliableButtons.Add
                    (
                        new FooterButton(ReopenButtonGroup, RecruitmentProcessStepResources.ReOpenProcessCommandButtonText)
                        {
                            OnClickAction = new JavaScriptCallAction($"RecruitmentProcess.ReopenProcess({currentStep.RecruitmentProcessId}, 'Models.{ nameof(RecruitmentProcessReopenViewModel) }');"),
                            CssClass = "btn btn-default",
                            Icon = FontAwesome.Refresh
                        }
                    );
                }

                return avaliableButtons;
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanCloseRecruitmentProcesses))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(AddNoteViewModel), RecruitmentProcessStepResources.CloseRecruitmentProcessDialogTitle)
                    {
                        OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.CloseProcess('{ IdentifierHelper.GetTypeIdentifier(typeof(RecruitmentProcessCloseViewModel)) }');"),
                        CssClass = "btn btn-danger"
                    }
                );
            }

            if (currentStep.Status == RecruitmentProcessStepStatus.Done && currentPrincipal.IsInRole(SecurityRoleType.CanReopenProcessSteps))
            {
                avaliableButtons.Add
                (
                    new FooterButton(reopenProcessButtonName, RecruitmentProcessStepResources.ReOpenStepCommandButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.reopenProcessStep({processStepId}, {Context.ParentId});"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Refresh
                    }
                );
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanAddRecruitmentProcessNotes))
            {
                avaliableButtons.Add
                (
                    new FooterButton(nameof(AddNoteViewModel), RecruitmentProcessStepResources.NewNoteButtonText)
                    {
                        OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.addNoteFromViewForm(this, {processStepId}, '{ IdentifierHelper.GetTypeIdentifier(typeof(AddNoteViewModel)) }');"),
                        CssClass = "btn btn-default",
                        Icon = FontAwesome.Plus
                    }
                );
            }

            return avaliableButtons;
        }

        private IList<IFooterButton> GetWorkflowButtons(long processStepId)
        {
            var avaliableActions = _recruitmentProcessStepService.GetWorkflowActions(new RecruitmentProcessStepActionContext
            {
                EmployeeId = GetCurrentPrincipal().EmployeeId,
                RecruitmentProcessStepId = processStepId
            });

            return RecruitmentProcessStepButtonHelper.CreateButtons(avaliableActions);
        }

        private ActionResult RedirectToParentList(RecruitmentProcessStepViewModel viewModel)
        {
            var parentIdName = NamingConventionHelper.ConvertPascalCaseToHyphenated(nameof(ParentIdContext.ParentId));
            var parentId = Context == null ? viewModel.RecruitmentProcessId : Context.ParentId;

            if (!GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewRecruitmentProcess))
            {
                parentId = BusinessLogicHelper.NonExistingId;
            }

            return RedirectToAction(nameof(List),
                new RouteValueDictionary(Context.AsDictionary()));
        }

        protected override string GetParentUrl(object context)
        {
            var recruitmentProcessContext = context as RecruitmentProcessContext;

            if (recruitmentProcessContext.JobOpeningId.HasValue)
            {
                return Url.UriActionGet(nameof(List), RoutingHelper.GetControllerName<JobOpeningRecruitmentProcessController>(),
                    KnownParameter.None, RoutingHelper.ParentIdParameterName, recruitmentProcessContext.JobOpeningId.Value.ToString()).Url;
            }

            if (recruitmentProcessContext.CandidateId.HasValue)
            {
                return Url.UriActionGet(nameof(List), RoutingHelper.GetControllerName<CandidateRecruitmentProcessController>(),
                    KnownParameter.None, RoutingHelper.ParentIdParameterName, recruitmentProcessContext.CandidateId.Value.ToString()).Url;
            }

            return base.GetParentUrl(context);
        }
    }
}