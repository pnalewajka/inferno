﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("RecruitmentValuePickers.InboundEmail")]
    [AtomicAuthorize(SecurityRoleType.CanViewInboundEmail)]
    public class InboundEmailPickerController : CardIndexController<InboundEmailViewModel, InboundEmailDto, InboundEmailPickerContext>
    {
        private const string MinimalViewId = "minimal";

        public InboundEmailPickerController(IInboundEmailPickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configuration = new GridViewConfiguration<InboundEmailViewModel>(InboundEmailResources.InboundEmailsName, "")
            {
                IsDefault = true
            };

            configuration.IncludeColumns(new List<Expression<Func<InboundEmailViewModel, object>>>
            {
                c => c.To,
                c => c.FromEmailAddress,
                c => c.Subject
            });

            return new List<IGridViewConfiguration>
            {
                configuration
            };
        }
    }
}