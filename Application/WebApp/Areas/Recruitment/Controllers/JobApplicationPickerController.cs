﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [Identifier("JobApplicationPicker.JobApplication")]
    [AtomicAuthorize(SecurityRoleType.CanViewJobApplications)]
    public class JobApplicationPickerController : ReadOnlyCardIndexController<JobApplicationViewModel, JobApplicationDto>
    {
        private const string MinimalViewId = "minimal";

        public JobApplicationPickerController(IJobApplicationPickerCardIndexDataService cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) 
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.GridViewConfigurations = GetGridCustomConfigurations();
        }

        private IList<IGridViewConfiguration> GetGridCustomConfigurations()
        {
            var configurations = new List<IGridViewConfiguration>();

            var configuration = new GridViewConfiguration<JobApplicationViewModel>(JobApplicationResources.MinimalViewText, MinimalViewId) { IsDefault = true };
            configuration.IncludeColumns(new List<Expression<Func<JobApplicationViewModel, object>>>
            {
                c => c.CandidateId,
                c => c.ApplicationOriginId,
                c => c.CityIds,
                c => c.JobProfileIds
            });
            configurations.Add(configuration);

            return configurations;
        }
    }
}