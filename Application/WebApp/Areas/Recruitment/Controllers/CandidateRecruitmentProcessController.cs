﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers.Buttons;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewRecruitmentProcess)]
    public class CandidateRecruitmentProcessController : SubCardIndexController<RecruitmentProcessViewModel, RecruitmentProcessDto, CandidateController, RecruitmentProcessContext>
    {
        private readonly ICandidateRecruitmentProcessCardIndexDataService _cardIndexDataService;
        private readonly IAnonymizationService _anonymizationService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IClassMappingFactory _classMappingFactory;

        public CandidateRecruitmentProcessController(
            ICandidateRecruitmentProcessCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            IAnonymizationService anonymizationService,
            IDataActivityLogService dataActivityLogService)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _cardIndexDataService = cardIndexDataService;
            _anonymizationService = anonymizationService;
            _dataActivityLogService = dataActivityLogService;
            _classMappingFactory = baseControllerDependencies.ClassMappingFactory;

            RecruitmentProcessController.ConfigureRecruitmentProcessController(CardIndex, Url, Layout, GetCurrentPrincipal());
            CardIndex.Settings.ToolbarButtons.AddRange(RecruitmentProcessButtons.GetToolbarButtons(Url, nameof(RecruitmentProcessContext.CandidateId)));
            CardIndex.Settings.RowButtons.AddRange(RecruitmentProcessButtons.GetRowButtons(Url));
            CardIndex.Settings.EditFormCustomization += SetReadOnlyFieldIfNecessary;
            CardIndex.Settings.AllowMultipleRowSelection = false;

            CardIndex.Settings.DefaultNewRecord = DefaultNewRecord;
            CardIndex.Settings.GridViewConfigurations = GetGridConfiguration();

            Layout.Css.Add("~/Areas/Recruitment/Content/recruitment.css");
        }

        public override ActionResult List(GridParameters parameters)
        {
            SwitchAddAndEditToDetails();

            return base.List(parameters);
        }

        public override ActionResult View(long id)
        {
            RecruitmentProcessController.ConfigureViewButtons(CardIndex, GetCurrentPrincipal(), id);

            return base.View(id);
        }

        public override ActionResult Edit(long id)
        {
            RecruitmentProcessController.ConfiguteEditButtons(CardIndex, GetCurrentPrincipal(), id);

            return base.Edit(id);
        }

        [HttpGet]
        public JsonNetResult GetJson(GridParameters parameters)
        {
            var classMappingDto = _classMappingFactory.CreateMapping<RecruitmentProcessDto, RecruitmentProcessViewModel>();
            var classMappingViewModel = _classMappingFactory.CreateMapping<RecruitmentProcessViewModel, RecruitmentProcessDto>();

            var search = parameters.ToCriteria(
                classMappingViewModel,
                _anonymizationService,
                Enumerable.Empty<string>(),
                CardIndex.Settings.FilterGroups);

            var candidateProcesses = _cardIndexDataService.GetRecords(search);
            var candidateProcessesAsViewModels = candidateProcesses.Rows.Select(classMappingDto.CreateFromSource);

            return new JsonNetResult(candidateProcessesAsViewModels)
            {
                SerializerSettings = JsonHelper.DefaultAjaxSettings
            };
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var id = filterContext.RouteData.Values["id"];

            if (id == null)
            {
                _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, GetType().Name);
            }
            else
            {
                _dataActivityLogService.LogCandidateDataActivity(Context.ParentId, ActivityType.ViewedRecord, ReferenceType.RecruitmentProcess, long.Parse(id.ToString()), GetType().Name);
            }
        }

        private void SetReadOnlyFieldIfNecessary(FormRenderingModel form)
        {
            if (!GetCurrentPrincipal().IsInRole(SecurityRoleType.CanEditRecruitmentProcessCoordinators))
            {
                form.GetControlByName<RecruitmentProcessViewModel>(j => j.RecruiterId).IsReadOnly = true;
            }
        }

        private object DefaultNewRecord()
        {
            var dto = _cardIndexDataService.GetDefaultNewRecord();
            dto.CandidateId = Context.ParentId;

            return dto;
        }

        private IList<IGridViewConfiguration> GetGridConfiguration()
        {
            var configuration = new GridViewConfiguration<RecruitmentProcessViewModel>(RecruitmentProcessResources.Title, string.Empty) { IsDefault = true };

            configuration.IncludeAllColumns();
            var currentPrincipal = GetCurrentPrincipal();

            configuration.ExcludeColumn(m => m.CandidateId);
            configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetCandidateLink));

            if (currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentJobOpening))
            {
                configuration.ExcludeColumn(m => m.JobOpeningId);
            }
            else
            {
                configuration.ExcludeMethodColumn(nameof(RecruitmentProcessViewModel.GetJobOpeningLink));
            }

            return new List<IGridViewConfiguration> { configuration };
        }
    }
}