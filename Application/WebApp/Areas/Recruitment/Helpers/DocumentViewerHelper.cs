﻿using System;
using System.IO;
using System.Linq;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Helpers
{
    public static class DocumentViewerHelper
    {
        private static readonly string[] ViewerJsSupportedExtensions = new[] { ".pdf", ".odt", ".odp", ".ods" };
        private static readonly string[] ImageSupportedExtensions = new[] { ".jpg", ".jpeg", ".png", ".apng", ".gif", ".svg", ".bmp" };
        private static readonly string[] AllSupportedExtension = ViewerJsSupportedExtensions.Union(ImageSupportedExtensions).ToArray();

        public static bool IsSupportedFile(string fileName)
        {
            return IsSupported(fileName, AllSupportedExtension);
        }

        public static bool IsViewerJsSupportedFormat(string fileName)
        {
            return IsSupported(fileName, ViewerJsSupportedExtensions);
        }

        public static bool IsImageSupportedFormat(string fileName)
        {
            return IsSupported(fileName, ImageSupportedExtensions);
        }

        private static bool IsSupported(string fileName, string[] supportedFormats)
        {
            var fileExtension = Path.GetExtension(fileName);

            return supportedFormats.Any(format => format.Equals(fileExtension, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}