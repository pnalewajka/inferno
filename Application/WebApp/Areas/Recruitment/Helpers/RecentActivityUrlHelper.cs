﻿using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Helpers
{
    public static class RecentActivityUrlHelper
    {
        public static DisplayUrl GetDisplayUrl(UrlHelper helper, long candidateId, RecentActivityViewModel activity, string recentRecruitmentProcess)
        {
            if (activity == null)
            {
                return new DisplayUrl
                {
                    Label = CandidateResources.NoRecentActivity
                };
            }

            switch (activity.ReferenceType)
            {
                case ReferenceType.Candidate:
                    return new DisplayUrl
                    {
                        Label = string.Format(CandidateResources.EditedCandidateActivity, activity.By, activity.On.ToShortDateString()),
                        Action = helper.UriActionGet<CandidateController>(c => c.Details(default(long)), KnownParameter.None,
                            RoutingHelper.IdParameterName, activity.ReferenceId.ToString())
                    };

                case ReferenceType.CandidateContact:
                    return new DisplayUrl
                    {
                        Label = string.Format(CandidateResources.ContactActivity, activity.By, activity.On.ToShortDateString()),
                        Action = helper.UriActionGet<CandidateContactRecordController>(c => c.Details(default(long)), KnownParameter.None, RoutingHelper.IdParameterName,
                            activity.ReferenceId.ToString(), RoutingHelper.ParentIdParameterName, candidateId.ToString())
                    };

                case ReferenceType.CandidateNote:
                    return new DisplayUrl
                    {
                        Label = string.Format(CandidateResources.NoteActivity, activity.By, activity.On.ToShortDateString()),
                        Action = helper.UriActionGet<CandidateNoteController>(c => c.Details(default(long)), KnownParameter.None, RoutingHelper.IdParameterName, activity.ReferenceId.ToString(),
                            RoutingHelper.ParentIdParameterName, candidateId.ToString())
                    };

                case ReferenceType.RecruitmentProcess:
                    return new DisplayUrl
                    {
                        Label = string.Format(CandidateResources.RecruitmentProcessActivity, recentRecruitmentProcess),
                        Action = helper.UriActionGet<RecruitmentProcessController>(c => c.Details(default(long)), KnownParameter.None, RoutingHelper.IdParameterName, activity.ReferenceId.ToString())
                    };

                case ReferenceType.RecruitmentProcessStep:
                    return new DisplayUrl
                    {
                        Label = string.Format(CandidateResources.RecruitmentProcessActivity, recentRecruitmentProcess),
                        Action = helper.UriActionGet<RecruitmentProcessStepController>(c => c.Details(default(long)), KnownParameter.None, RoutingHelper.IdParameterName, activity.ReferenceId.ToString())
                    };

                case ReferenceType.CandidateFile:
                    return new DisplayUrl
                    {
                        Label = string.Format(CandidateResources.CandidateFileActivity, activity.By, activity.On.ToShortDateString()),
                        Action = helper.UriActionGet<CandidateFileController>(c => c.Details(default(long)), KnownParameter.None, RoutingHelper.IdParameterName, activity.ReferenceId.ToString(),
                            RoutingHelper.ParentIdParameterName, candidateId.ToString())
                    };
            }

            return new DisplayUrl
            {
                Label = "No Recent Activity"
            };
        }
    }
}