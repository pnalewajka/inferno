﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Helpers
{
    public static class RecruitmentProcessStepButtonHelper
    {
        public static IList<IFooterButton> CreateButtons(RecruitmentProcessStepWorkflowAction[] nextButtons)
        {
            var actionButtons = nextButtons.Where(b => b.Rule.AddedStepType == null).Select(CreateButton).ToList();
            var nextStepButtons = nextButtons.Where(b => b.Rule.AddedStepType != null).ToArray();

            if (nextStepButtons.Any())
            {
                actionButtons.Add(InitializeNextStepButton(nextStepButtons));
            }

            return actionButtons.Where(b => b != null).ToList();
        }

        private static IFooterButton CreateButton(RecruitmentProcessStepWorkflowAction nextButton)
        {
            var workflowAction = nextButton.Rule.WorkflowAction;

            switch (nextButton.Rule.WorkflowAction)
            {
                case RecruitmentProcessStepWorkflowActionType.RejectCandidate:
                    return InitializeRejectButton(GetButtonLabel(nextButton.StepType, RecruitmentProcessStepWorkflowActionType.RejectCandidate));

                case RecruitmentProcessStepWorkflowActionType.ApproveCandidate:
                    return InitializeWorkflowActionButton(
                        RecruitmentProcessStepWorkflowActionType.ApproveCandidate,
                        GetButtonLabel(nextButton.StepType, RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                        CommandButtonStyle.Success);

                case RecruitmentProcessStepWorkflowActionType.PutProcessOnHold:
                    return InitializeWorkflowActionButton(
                        RecruitmentProcessStepWorkflowActionType.PutProcessOnHold,
                        RecruitmentProcessStepResources.ProcessOnHoldButtonLabel,
                        CommandButtonStyle.Warning);

                case RecruitmentProcessStepWorkflowActionType.CancelProcessStep:
                    return InitializeWorkflowActionButton(
                        RecruitmentProcessStepWorkflowActionType.CancelProcessStep,
                        RecruitmentProcessStepResources.CancelStepButtonLabel,
                        CommandButtonStyle.Warning);
            }

            throw new NotImplementedException("Can't add button for action RefreshAndRunScript");
        }

        private static IFooterButton InitializeRejectButton(string buttonLabel) =>
            new FooterButton(nameof(RecruitmentProcessStepWorkflowActionType.RejectCandidate), buttonLabel)
            {
                ButtonStyleType = CommandButtonStyle.Danger,
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.rejectCandidate(event.currentTarget, "
                        + $"'Models.{ nameof(RecrutimentProcessStepRejectionViewModel) }');"),
                Attributes = FooterButtonBuilder.SubmitFormButtonAttribures()
            };

        private static IFooterButton InitializeNextStepButton(RecruitmentProcessStepWorkflowAction[] workflowActions)
        {
            if (workflowActions.Length == 1)
            {
                var action = workflowActions.Single();

                var nextStepButton = InitializeNextStepButton(action.Rule, action.SameStepCount);
                nextStepButton.ButtonStyleType = CommandButtonStyle.Info;

                return nextStepButton;
            }

            return new FooterButton("next-step", RecruitmentProcessStepResources.NextStepButtonLabel)
            {
                ButtonStyleType = CommandButtonStyle.Info,
                OnClickAction = new DropdownAction
                {
                    Items = workflowActions.Select(s => InitializeNextStepButton(s.Rule, s.SameStepCount)).ToList<ICommandButton>()
                }
            };
        }

        private static IFooterButton InitializeNextStepButton(RecruitmentProcessStepRule nextButtonRule, long sameStepCount)
        {
            var nextStepType = nextButtonRule.AddedStepType.Value;
            var allowCloseDecision = nextButtonRule.AllowCloseStepDecision;

            string buttonLabel = GetButtonLabel(nextStepType);

            var processAwareButtonLabel = sameStepCount > 0 ? $"{buttonLabel} #{sameStepCount + 1}" : buttonLabel;
            var areConditionsTypical = nextButtonRule.AreConditionsTypical;

            return new FooterButton(nameof(nextStepType), processAwareButtonLabel)
            {
                CssClass = areConditionsTypical ? string.Empty : " btn-not-typical-action",
                OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.refreshAndRunScript(event.currentTarget, "
                    + $"'RecruitmentProcessStep.addNextStep(\"{NamingConventionHelper.ConvertPascalCaseToHyphenated(nextStepType.ToString())}\""
                    + $", {allowCloseDecision.ToString().ToLower()}, {(int)nextButtonRule.WorkflowAction})')"),
                Attributes = FooterButtonBuilder.SubmitFormButtonAttribures(),
                ButtonStyleType = CommandButtonStyle.None
            };
        }

        private static string GetButtonLabel(RecruitmentProcessStepType nextStepType)
        {
            return ResourceHelper.GetString(typeof(RecruitmentProcessStepResources), $"{nextStepType}ButtonLabel");
        }

        private static string GetButtonLabel(RecruitmentProcessStepType currentStepType, RecruitmentProcessStepWorkflowActionType actionType)
        {
            return ResourceHelper.GetString(typeof(RecruitmentProcessStepResources), $"{actionType}{currentStepType}ButtonLabel")
                ?? ResourceHelper.GetString(typeof(RecruitmentProcessStepResources), $"{actionType}ButtonLabel");
        }

        private static IFooterButton InitializeWorkflowActionButton(RecruitmentProcessStepWorkflowActionType processStepWorkflowAction, string buttonLabel, CommandButtonStyle styleType) =>
          new FooterButton(nameof(processStepWorkflowAction), buttonLabel)
          {
              ButtonStyleType = styleType,
              OnClickAction = new JavaScriptCallAction($"RecruitmentProcessStep.executeWorkflowAction(event.currentTarget, {(int)processStepWorkflowAction});"),
              Attributes = FooterButtonBuilder.SubmitFormButtonAttribures()
          };
    }
}