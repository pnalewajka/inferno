﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.WebApp.Areas.Recruitment.Models;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Helpers
{
    internal class InboundEmailHelper
    {
        internal static bool GetBool(List<InboundEmailValueViewModel> values, InboundEmailValueType key, bool defaultValue = false)
        {
            var item = GetFirstOrDefault(values, key);

            if (item == null || item.ValueLong == null)
            {
                return defaultValue;
            }

            return item.ValueLong.Value != 0;
        }

        internal static void SetBool(List<InboundEmailValueViewModel> values, InboundEmailValueType key, bool value)
        {
            var item = GetFirstOrDefault(values, key, true);

            item.ValueLong = value ? 1 : 0;
        }

        internal static long GetLong(List<InboundEmailValueViewModel> values, InboundEmailValueType key, long defaultValue = 0)
        {
            var item = GetFirstOrDefault(values, key);

            if (item == null || item.ValueLong == null)
            {
                return defaultValue;
            }

            return item.ValueLong.Value;
        }

        internal static void SetLong(List<InboundEmailValueViewModel> values, InboundEmailValueType key, long value)
        {
            var item = GetFirstOrDefault(values, key, true);

            item.ValueLong = value;
        }

        internal static long? GetNullableLong(List<InboundEmailValueViewModel> values, InboundEmailValueType key, long? defaultValue = 0)
        {
            var item = GetFirstOrDefault(values, key);

            if (item == null)
            {
                return defaultValue;
            }

            return item.ValueLong;
        }

        internal static void SetNullableLong(List<InboundEmailValueViewModel> values, InboundEmailValueType key, long? value)
        {
            var item = GetFirstOrDefault(values, key, true);

            item.ValueLong = value;
        }

        internal static string GetString(List<InboundEmailValueViewModel> values, InboundEmailValueType key, string defaultValue = null)
        {
            var item = GetFirstOrDefault(values, key);

            if (item == null || item.ValueString == null)
            {
                return defaultValue;
            }

            return item.ValueString;
        }

        internal static void SetString(List<InboundEmailValueViewModel> values, InboundEmailValueType key, string value)
        {
            var item = GetFirstOrDefault(values, key, true);

            item.ValueString = value;
        }

        internal static T GetEnum<T>(List<InboundEmailValueViewModel> values, InboundEmailValueType key, T defaultValue = default(T))
            where T : struct
        {
            var enumType = typeof(T);

            if (!enumType.IsEnum)
            {
                throw new InvalidCastException();
            }

            var item = GetFirstOrDefault(values, key);

            if (item == null || item.ValueLong == null)
            {
                return defaultValue;
            }

            return (T)Enum.Parse(enumType, Enum.GetName(enumType, item.ValueLong));
        }

        internal static void SetEnum<T>(List<InboundEmailValueViewModel> values, InboundEmailValueType key, T value)
            where T : struct
        {
            if (!typeof(T).IsEnum)
            {
                throw new InvalidCastException();
            }

            var item = GetFirstOrDefault(values, key, true);

            item.ValueLong = (int)(object)value;
        }

        internal static T? GetNullableEnum<T>(List<InboundEmailValueViewModel> values, InboundEmailValueType key, T? defaultValue = null)
            where T : struct
        {
            var enumType = typeof(T);

            if (!enumType.IsEnum)
            {
                throw new InvalidCastException();
            }

            var item = GetFirstOrDefault(values, key);

            if (item == null)
            {
                return defaultValue;
            }

            var value = item.ValueLong;

            if (value == null)
            {
                return null;
            }

            return (T)Enum.Parse(enumType, Enum.GetName(enumType, item.ValueLong));
        }

        internal static void SetNullableEnum<T>(List<InboundEmailValueViewModel> values, InboundEmailValueType key, T? value)
            where T : struct
        {
            if (!typeof(T).IsEnum)
            {
                throw new InvalidCastException();
            }

            var item = GetFirstOrDefault(values, key, true);

            if (value == null)
            {
                item.ValueLong = null;
            }
            else
            {
                item.ValueLong = (int)(object)value.Value;
            }
        }

        internal static InboundEmailValueViewModel GetFirstOrDefault(List<InboundEmailValueViewModel> values, InboundEmailValueType key, bool addEmptyIfNotFound = false)
        {
            var item = values.FirstOrDefault(v => v.Key == key);

            if (item == null && addEmptyIfNotFound)
            {
                item = new InboundEmailValueViewModel { Key = key };
                values.Add(item);
            }

            return item;
        }
    }
}