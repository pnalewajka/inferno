﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Recruitment
{
    public class RecruitmentAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Recruitment";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Recruitment",
                "Recruitment/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );


            BundleConfig.RegisterRecruitmentBundles(BundleTable.Bundles);
        }
    }
}