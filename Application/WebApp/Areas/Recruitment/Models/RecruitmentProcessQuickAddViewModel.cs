﻿using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RecruitmentProcessQuickAddViewModel")]
    public class RecruitmentProcessQuickAddViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(JobOpeningPickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.JobOpeningId), typeof(RecruitmentProcessResources))]
        [Render(Size = Size.Large)]
        public long JobOpeningId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=true'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CandidateId), typeof(RecruitmentProcessResources))]
        [Render(Size = Size.Large)]
        public long CandidateId { get; set; }

        [Visibility(VisibilityScope.None)]
        public long RecruiterId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.InitialSteps), typeof(RecruitmentProcessResources))]
        [CheckboxGroup(ItemProvider = typeof(InitialRecruitmentProcessStepTypeItemProvider))]
        [Render(ControlCssClass = "align-options-200")]
        public RecruitmentProcessStepType[] InitialSteps { get; set; }

        [HiddenInput]
        public RecruitmentProcessStatus Status { get; set; }
    }
}