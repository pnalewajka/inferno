﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class JobOpeningStepFunnelReportParametersDtoToJobOpeningStepFunnelReportParametersViewModelMapping
         : ClassMapping<JobOpeningStepFunnelReportParametersDto, JobOpeningStepFunnelReportParametersViewModel>
    {
        public JobOpeningStepFunnelReportParametersDtoToJobOpeningStepFunnelReportParametersViewModelMapping()
        {
            Mapping = v => new JobOpeningStepFunnelReportParametersViewModel
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                DecisionMakerIds = v.DecisionMakerIds
            };
        }
    }
}