﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class JobOpeningStepFunnelReportParametersViewModelToJobOpeningStepFunnelReportParametersDtoMapping
         : ClassMapping<JobOpeningStepFunnelReportParametersViewModel, JobOpeningStepFunnelReportParametersDto>
    {
        public JobOpeningStepFunnelReportParametersViewModelToJobOpeningStepFunnelReportParametersDtoMapping()
        {
            Mapping = v => new JobOpeningStepFunnelReportParametersDto
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                DecisionMakerIds = v.DecisionMakerIds
            };
        }
    }
}