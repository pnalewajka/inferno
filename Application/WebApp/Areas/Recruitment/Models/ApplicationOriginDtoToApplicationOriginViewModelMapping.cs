﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ApplicationOriginDtoToApplicationOriginViewModelMapping : ClassMapping<ApplicationOriginDto, ApplicationOriginViewModel>
    {
        public ApplicationOriginDtoToApplicationOriginViewModelMapping()
        {
            Mapping = d => new ApplicationOriginViewModel
            {
                Id = d.Id,
                Name = d.Name,
                ParserIdentifier = d.ParserIdentifier,
                EmailAddress = d.EmailAddress,
                OriginType = d.OriginType
            };
        }
    }
}
