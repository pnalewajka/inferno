﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class HistoryEntryDtoToHistoryEntryViewModelMapping : ClassMapping<HistoryEntryDto, HistoryEntryViewModel>
    {
        private readonly IPrincipalProvider _principalProvider;

        public HistoryEntryDtoToHistoryEntryViewModelMapping(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;

            Mapping = d => new HistoryEntryViewModel
            {
                ActivityDescription = d.ActivityDescription,
                PerformedByUserFullName = d.PerformedByUserFullName,
                PerformedOn = d.PerformedOn,
                SnapshotChanges = FilterSnapshotChanges(d.SnapshotChanges)
            };
        }

        private IEnumerable<KeyValuePair<string, ValueChange>> FilterSnapshotChanges(Dictionary<string, ValueChange> snapshotChanges)
        {
            if (snapshotChanges != null && snapshotChanges.Any())
            {
                return snapshotChanges.Where(s => !s.Value.SecurityRole.HasValue || _principalProvider.Current.IsInRole(s.Value.SecurityRole.Value))
                    .Select(s => new KeyValuePair<string, ValueChange>(s.Key, s.Value));
            }

            return Enumerable.Empty<KeyValuePair<string, ValueChange>>();
        }
    }
}
