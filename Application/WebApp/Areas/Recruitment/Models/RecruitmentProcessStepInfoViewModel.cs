﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessStepInfoViewModel
    {
        public long Id { get; set; }

        public RecruitmentProcessStepType Type { get; set; }

        public RecruitmentProcessStepStatus Status { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public DateTime? PlannedOn { get; set; }

        public DateTime? DueOn { get; set; }

        public DateTime? DecidedOn { get; set; }

        public bool CanBeEdited { get; set; }

        public bool CanBeViewed { get; set; }

        public bool CanViewDoneStep { get; set; }

        public string Hint { get; set; }

        public string Comment { get; set; }

        public bool IsPending { get; set; }

        public string AssigneeFullName { get; set; }

        public string StepInfoDescription => GetStepInfoDescription();

        public string StepInfoTitle => GetStepInfoTitle();

        public string TypeDescription => Type.GetLocalizedDescription().ToString();

        private string GetStepInfoDescription()
        {
            var title = GetStepInfoTitle();
            var type = TypeDescription;

            return string.IsNullOrWhiteSpace(title)
                ? type.ToString()
                : $"{type} ({title})";
        }

        private string GetStepInfoTitle()
        {
            var plannedSegment = PlannedOn == null
                ? null
                : $"{RecruitmentProcessStepResources.PlannedOn}: {PlannedOn:g}";

            var deadlineSegment = Status == RecruitmentProcessStepStatus.Done || DueOn == null
                ? null
                : $"{RecruitmentProcessStepResources.DueOn}: {DueOn:d}";

            var title = string.Join(" · ", new[] { plannedSegment, deadlineSegment, AssigneeFullName }.Where(s => s != null));

            return title;
        }
    }
}