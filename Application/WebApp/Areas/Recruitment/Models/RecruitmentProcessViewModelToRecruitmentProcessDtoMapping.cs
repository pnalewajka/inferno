﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessViewModelToRecruitmentProcessDtoMapping : ClassMapping<RecruitmentProcessViewModel, RecruitmentProcessDto>
    {
        public RecruitmentProcessViewModelToRecruitmentProcessDtoMapping()
        {
            Mapping = v => new RecruitmentProcessDto
            {
                Id = v.Id,
                CandidateId = v.CandidateId,
                JobOpeningId = v.JobOpeningId,
                RecruiterId = v.RecruiterId,
                CityId = v.CityId,
                JobApplicationId = v.JobApplicationId,
                Status = v.Status,
                ResumeShownToClientOn = v.ResumeShownToClientOn,
                ClosedOn = v.ClosedOn,
                ClosedReason = v.ClosedReason,
                ClosedComment = v.ClosedComment,
                WatcherIds = v.WatcherIds.ToArray(),
                OfferPositionId = v.OfferPositionId,
                OfferComment = v.OfferComment,
                OfferContractType = v.OfferContractType,
                OfferLocationId = v.OfferLocationId,
                OfferSalary = v.OfferSalary,
                OfferStartDate = v.OfferStartDate,
                FinalComment = v.FinalComment,
                FinalLocationId = v.FinalLocationId,
                FinalLongTermContractTypeId = v.FinalLongTermContractTypeId,
                FinalLongTermSalary = v.FinalLongTermSalary,
                FinalPositionId = v.FinalPositionId,
                FinalTrialContractTypeId = v.FinalTrialContractTypeId,
                FinalTrialSalary = v.FinalTrialSalary,
                FinalStartDate = v.FinalStartDate,
                FinalSignedDate = v.FinalSignedDate,
                FinalEmploymentSourceId = v.FinalEmploymentSourceId,
                WorkflowAction = v.WorkflowAction,
                WorkflowClosedComment = v.WorkflowClosedComment,
                WorkflowClosedReason = v.WorkflowClosedReason,
                ScreeningGeneralOpinion = v.ScreeningGeneralOpinion,
                ScreeningMotivation = v.ScreeningMotivation,
                ScreeningWorkplaceExpectations = v.ScreeningWorkplaceExpectations,
                ScreeningSkills = v.ScreeningSkills,
                ScreeningLanguageEnglish = v.ScreeningLanguageEnglish,
                ScreeningLanguageGerman = v.ScreeningLanguageGerman,
                ScreeningLanguageOther = v.ScreeningLanguageOther,
                ScreeningContractExpectations = v.ScreeningContractExpectations,
                ScreeningAvailability = v.ScreeningAvailability,
                ScreeningOtherProcesses = v.ScreeningOtherProcesses,
                ScreeningCounterOfferCriteria = v.ScreeningCounterOfferCriteria,
                ScreeningRelocationCanDo = v.ScreeningRelocationCanDo,
                ScreeningRelocationComment = v.ScreeningRelocationComment,
                ScreeningBusinessTripsCanDo = v.ScreeningBusinessTripsCanDo,
                ScreeningBusinessTripsComment = v.ScreeningBusinessTripsComment,
                ScreeningEveningWorkCanDo = v.ScreeningEveningWorkCanDo,
                ScreeningEveningWorkComment = v.ScreeningEveningWorkComment,
                ScreeningWorkPermitNeeded = v.ScreeningWorkPermitNeeded,
                ScreeningWorkPermitComment = v.ScreeningWorkPermitComment,
                CrmId = v.CrmId,
                IsMatureEnoughForHiringManager = v.IsMatureEnoughForHiringManager,
                Timestamp = v.Timestamp,
            };
        }
    }
}
