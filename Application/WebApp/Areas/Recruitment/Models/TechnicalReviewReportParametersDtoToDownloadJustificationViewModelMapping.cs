﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class TechnicalReviewReportParametersDtoToDownloadJustificationViewModelMapping : ClassMapping<TechnicalReviewSummaryReportParametersDto, DownloadJustificationViewModel>
    {
        public TechnicalReviewReportParametersDtoToDownloadJustificationViewModelMapping()
        {
            Mapping = v => new DownloadJustificationViewModel
            {
                Comment = v.Comment,
                CustomerId = v.CustomerId,
                RecruitmentProcessStepId = v.RecruitmentProcessStepId
            };
        }
    }
}