﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.Sales.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.DownloadJustification")]
    public class DownloadJustificationViewModel
    {
        [HiddenInput]
        public long DocumentId { get; set; }

        [HiddenInput]
        public long RecruitmentProcessStepId { get; set; }

        [DisplayNameLocalized(nameof(CandidateFileResources.CustomerToShareDocumentWith), typeof(CandidateFileResources))]
        [ValuePicker(Type = typeof(CustomerPickerController))]
        [Render(Size = Size.Large)]
        [Required]
        public virtual long CustomerId { get; set; }

        [DisplayNameLocalized(nameof(CandidateFileResources.Comment), typeof(CandidateFileResources))]
        [Multiline(2)]
        [MaxLength(200)]
        [Required]
        public string Comment { get; set; }

        public DownloadJustificationViewModel(long documentId)
        {
            DocumentId = documentId;
        }

        public DownloadJustificationViewModel()
        {
        }
    }
}