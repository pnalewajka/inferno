﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.Reports.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.HiredCandidateReportParameters")]
    public class HiredCandidateReportParametersViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [DisplayNameLocalized(nameof(TechnicalReviewResources.JobProfilesShort), typeof(TechnicalReviewResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public List<long> JobProfileIds { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.CityIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(CityPickerController))]
        public List<long> CityIds { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.RecruiterIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=reporting-recruiters'")]
        public List<long> RecruiterIds { get; set; }


    }
}