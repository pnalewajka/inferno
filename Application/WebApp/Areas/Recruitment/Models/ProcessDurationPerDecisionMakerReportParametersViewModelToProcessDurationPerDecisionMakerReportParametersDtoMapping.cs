﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessDurationPerDecisionMakerReportParametersViewModelToProcessDurationPerDecisionMakerReportParametersDtoMapping
        : ClassMapping<ProcessDurationPerDecisionMakerReportParametersViewModel, ProcessDurationPerDecisionMakerReportParametersDto>
    {
        public ProcessDurationPerDecisionMakerReportParametersViewModelToProcessDurationPerDecisionMakerReportParametersDtoMapping()
        {
            Mapping = v => new ProcessDurationPerDecisionMakerReportParametersDto
            {
                DateFrom = v.DateFrom.StartOfDay(),
                DateTo = v.DateTo.EndOfDay(),
                DecisionMakerEmployeeIds = v.DecisionMakerEmployeeIds,
                ProcessResult = v.ProcessResult
            };
        }
    }
}