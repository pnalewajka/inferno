﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class InboundEmailValueViewModelToInboundEmailValueDtoMapping : ClassMapping<InboundEmailValueViewModel, InboundEmailValueDto>
    {
        public InboundEmailValueViewModelToInboundEmailValueDtoMapping()
        {
            Mapping = v => new InboundEmailValueDto
            {
                Id = v.Id,
                Key = v.Key,
                ValueString = v.ValueString,
                ValueLong = v.ValueLong,
                Timestamp = v.Timestamp
            };
        }
    }
}
