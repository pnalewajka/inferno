﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class AddNoteViewModelToAddNoteDtoMapping : ClassMapping<AddNoteViewModel, AddNoteDto>
    {
        public AddNoteViewModelToAddNoteDtoMapping()
        {
            Mapping = v => new AddNoteDto
            {
                ParentId = v.ParentId,
                NoteVisibility = v.NoteVisibility,
                Content = v.Content
            };
        }
    }
}
