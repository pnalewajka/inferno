﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.ProcessProgressReportParameters")]
    public class ProcessProgressReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(JobOpeningResources.RecruiterIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public List<long> RecruiterIds { get; set; }

        [DisplayNameLocalized(nameof(TechnicalReviewResources.JobProfilesShort), typeof(TechnicalReviewResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public List<long> JobProfileIds { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.CityIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(CityPickerController))]
        public List<long> CityIds { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.Type), typeof(RecruitmentProcessStepResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<RecruitmentProcessStepType>))]
        [Render(ControlCssClass = "align-options-300")]
        public RecruitmentProcessStepType[] RecruitmentProcessStepTypes { get; set; }

        [ValuePicker(Type = typeof(JobOpeningPickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.JobOpeningIds), typeof(RecruitmentProcessResources))]
        public List<long> JobOpeningIds { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.AssignedEmployeeIds), typeof(RecruitmentProcessStepResources))]
        public List<long> AssignedEmployeeIds { get; set; }

        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(JobOpeningResources.OrgUnitId), typeof(JobOpeningResources))]
        public List<long> OrgUnitIds { get; set; }
    }
}