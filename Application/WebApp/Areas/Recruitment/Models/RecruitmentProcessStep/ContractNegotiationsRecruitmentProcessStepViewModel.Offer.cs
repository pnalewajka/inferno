﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    public partial class ContractNegotiationsRecruitmentProcessStepViewModel
    {
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferPositionId), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [Tab(ContractNegotiationOfferTab)]
        [Required]
        public long? OfferPositionId { get; set; }

        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferContractType), typeof(RecruitmentProcessResources))]
        [Tab(ContractNegotiationOfferTab)]
        [StringLength(50)]
        [Required]
        public string OfferContractType { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferSalary), typeof(RecruitmentProcessResources))]
        [Tab(ContractNegotiationOfferTab)]
        [StringLength(150)]
        [Required]
        [RoleRequired(SecurityRoleType.CanViewRecruitmentProcessOfferedSalary)]
        public string OfferSalary { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferStartDate), typeof(RecruitmentProcessResources))]
        [Tab(ContractNegotiationOfferTab)]
        [StringLength(130)]
        [Required]
        public string OfferStartDate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferLocationId), typeof(RecruitmentProcessResources))]
        [Tab(ContractNegotiationOfferTab)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [Required]
        public long? OfferLocationId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferComment), typeof(RecruitmentProcessResources))]
        [Tab(ContractNegotiationOfferTab)]
        [Multiline(3)]
        [StringLength(500)]
        public string OfferComment { get; set; }
    }
}