﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    public class RecruitmentProcessStepViewModelToRecruitmentProcessStepDtoMapping : ClassMapping<RecruitmentProcessStepViewModel, RecruitmentProcessStepDto>
    {
        public RecruitmentProcessStepViewModelToRecruitmentProcessStepDtoMapping()
        {
            Mapping = v => GetDto(v);
        }

        private RecruitmentProcessStepDto GetDto(RecruitmentProcessStepViewModel vm)
        {
            var dto = new RecruitmentProcessStepDto();

            SetCommonValuesFromVm(dto, vm);
            SetDetailsFromVm(dto, vm);

            dto.FromStepId = vm.FromStepId;

            return dto;
        }

        private void SetDetailsFromVm(RecruitmentProcessStepDto dto, RecruitmentProcessStepViewModel vm)
        {
            switch (vm.Type)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    dto.StepDetails = MapViewModelToScreeningStepDetailsDto((ScreeningProcessStepViewModel)vm);
                    break;

                case RecruitmentProcessStepType.TechnicalReview:
                    dto.StepDetails = MapViewModelToTechnicalReviewStepDetailsDto((TechnicalReviewRecruitmentProcessStepViewModel)vm);
                    break;

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    break;

                case RecruitmentProcessStepType.ClientResumeApproval:
                    dto.StepDetails = MapViewModelToClientResumeApprovalDto((ClientResumeApprovalRecruitmentProcessStepViewModel)vm);
                    break;

                case RecruitmentProcessStepType.HiringManagerInterview:
                    break;

                case RecruitmentProcessStepType.ClientInterview:
                    break;

                case RecruitmentProcessStepType.HiringDecision:
                    break;

                case RecruitmentProcessStepType.ContractNegotiations:
                    dto.StepDetails = MapViewModelToContractNegotiationsDetailsDto((ContractNegotiationsRecruitmentProcessStepViewModel)vm);
                    break;

                default:
                    break;
            }
        }

        private ClientResumeApprovalDto MapViewModelToClientResumeApprovalDto(ClientResumeApprovalRecruitmentProcessStepViewModel vm)
        {
            return new ClientResumeApprovalDto
            {
                ResumeShownToClientOn = vm.ResumeShownToClientOn
            };
        }

        private ContractNegotiationsDetailsDto MapViewModelToContractNegotiationsDetailsDto(ContractNegotiationsRecruitmentProcessStepViewModel vm)
        {
            return new ContractNegotiationsDetailsDto
            {
                FinalComment = vm.FinalComment,
                FinalLocationId = vm.FinalLocationId,
                FinalLongTermContractTypeId = vm.FinalLongTermContractTypeId,
                FinalLongTermSalary = vm.FinalLongTermSalary,
                FinalPositionId = vm.FinalPositionId,
                FinalStartDate = vm.FinalStartDate,
                FinalSignedDate = vm.FinalSignedDate,
                FinalTrialContractTypeId = vm.FinalTrialContractTypeId,
                FinalTrialSalary = vm.FinalTrialSalary,
                FinalEmploymentSourceId = vm.FinalEmploymentSourceId,
                OfferComment = vm.OfferComment,
                OfferContractType = vm.OfferContractType,
                OfferLocationId = vm.OfferLocationId,
                OfferPositionId = vm.OfferPositionId,
                OfferSalary = vm.OfferSalary,
                OfferStartDate = vm.OfferStartDate
            };
        }

        private void SetCommonValuesFromVm(RecruitmentProcessStepDto dto, RecruitmentProcessStepViewModel vm)
        {
            dto.Id = vm.Id;
            dto.RecruitmentProcessId = vm.RecruitmentProcessId;
            dto.Type = vm.Type;
            dto.PlannedOn = vm.PlannedOn;
            dto.AssignedEmployeeId = vm.AssignedEmployeeId;
            dto.OtherAssignedEmployeeIds = (vm.OtherAttendingEmployeeIds ?? new List<long>()).ToArray();
            dto.Decision = vm.Decision;
            dto.Status = vm.Status;
            dto.DecisionComment = vm.DecisionComment;
            dto.CloseCurrentStep = vm.CloseCurrentStep;
            dto.RecruitmentHint = vm.RecruitmentHint;
            dto.WorkflowAction = vm.WorkflowAction;
            dto.NextStepId = vm.NextStepId;
            dto.Timestamp = vm.Timestamp;
        }

        private ScreeningStepDetailsDto MapViewModelToScreeningStepDetailsDto(ScreeningProcessStepViewModel vm)
        {
            return new ScreeningStepDetailsDto
            {
                ScreeningAvailability = vm.ScreeningAvailability,
                ScreeningBusinessTripsCanDo = vm.ScreeningBusinessTripsCanDo,
                ScreeningBusinessTripsComment = vm.ScreeningBusinessTripsComment,
                ScreeningContractExpectations = vm.ScreeningContractExpectations,
                ScreeningCounterOfferCriteria = vm.ScreeningCounterOfferCriteria,
                ScreeningGeneralOpinion = vm.ScreeningGeneralOpinion,
                ScreeningLanguageEnglish = vm.ScreeningLanguageEnglish,
                ScreeningLanguageGerman = vm.ScreeningLanguageGerman,
                ScreeningLanguageOther = vm.ScreeningLanguageOther,
                ScreeningMotivation = vm.ScreeningMotivation,
                ScreeningOtherProcesses = vm.ScreeningOtherProcesses,
                ScreeningRelocationCanDo = vm.ScreeningRelocationCanDo,
                ScreeningRelocationComment = vm.ScreeningRelocationComment,
                ScreeningEveningWorkCanDo = vm.ScreeningEveningWorkCanDo,
                ScreeningEveningWorkComment = vm.ScreeningEveningWorkComment,
                ScreeningSkills = vm.ScreeningSkills,
                ScreeningWorkPermitComment = vm.ScreeningWorkPermitComment,
                ScreeningWorkPermitNeeded = vm.ScreeningWorkPermitNeeded,
                ScreeningWorkplaceExpectations = vm.ScreeningWorkplaceExpectations
            };
        }

        private TechnicalReviewStepDetailsDto MapViewModelToTechnicalReviewStepDetailsDto(TechnicalReviewRecruitmentProcessStepViewModel vm)
        {
            return new TechnicalReviewStepDetailsDto
            {
                JobProfileIds = vm.JobProfileIds,
                EnglishUsageAssessment = vm.EnglishUsageAssessment,
                TeamProjectSuitabilityAssessment = vm.TeamProjectSuitabilityAssessment,
                OtherRemarks = vm.OtherRemarks,
                RiskAssessment = vm.RiskAssessment,
                SeniorityLevelAssessment = vm.SeniorityLevelAssessment,
                TechnicalKnowledgeAssessment = vm.TechnicalKnowledgeAssessment,
                StrengthAssessment = vm.StrengthAssessment,
                TechnicalAssignmentAssessment = vm.TechnicalAssignmentAssessment,
            };
        }
    }
}
