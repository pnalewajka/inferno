﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.RecruitmentProcessStepViewModel")]
    [ModelBinder(typeof(RecruitmentProcessStepViewModelBinder))]
    [TabDescription(1, StepDetailsTab, nameof(RecruitmentProcessStepResources.RecruitmentProcessStepDetails), typeof(RecruitmentProcessStepResources))]
    [TabDescription(2, ScreeningTab, nameof(RecruitmentProcessStepResources.ScreeningTabText), typeof(RecruitmentProcessStepResources))]
    [TabDescription(3, TechnicalReviewTab, nameof(RecruitmentProcessStepResources.TechnicalReviewTabText), typeof(RecruitmentProcessStepResources))]
    [TabDescription(4, ContractNegotiationOfferTab, nameof(RecruitmentProcessStepResources.ContractNegotiationOfferTabText), typeof(RecruitmentProcessStepResources))]
    [TabDescription(5, ContractNegotiationFinalTab, nameof(RecruitmentProcessStepResources.ContractNegotiationFinalTabText), typeof(RecruitmentProcessStepResources))]
    [TabDescription(6, NoteTab, nameof(NotesTabLabel), SecurityRoleType.CanViewRecruitmentProcessNotes)]
    [TabDescription(7, CandidateTechnicalReviewTab, nameof(TechnicalReviewsTabLabel), SecurityRoleType.CanViewTechnicalVerificationDetails)]
    [TabDescription(8, ContributorTab, nameof(RecruitmentProcessResources.ContributorTabText), typeof(RecruitmentProcessResources))]
    public class RecruitmentProcessStepViewModel
    {
        public const string StepDetailsTab = "step-details";
        public const string ScreeningTab = "screening";
        public const string TechnicalReviewTab = "technical-review";
        public const string ContractNegotiationOfferTab = "contract-negotiation-offer";
        public const string ContractNegotiationFinalTab = "contract-negotiation-final";
        public const string NoteTab = "notes";
        public const string CandidateTechnicalReviewTab = "technical-verification-tab";
        protected const string ContributorTab = "contributor";

        public long Id { get; set; }

        [Order(1)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(RecruitmentProcessPickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.RecruitmentProcess), typeof(RecruitmentProcessStepResources))]
        [Render(Size = Size.Large)]
        [Tab(StepDetailsTab)]
        public long RecruitmentProcessId { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public RecruitmentProcessStatus RecruitmentProcessStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public long CreatedById { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public long? FromStepId { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.Type), typeof(RecruitmentProcessStepResources))]
        [Required]
        [ReadOnly(true)]
        [Tab(StepDetailsTab)]
        public virtual RecruitmentProcessStepType Type { get; set; }

        [Order(3)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.Status), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        public RecruitmentProcessStepStatus Status { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.PlannedOn), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        [StringFormat("{0:g}")]
        public virtual DateTime? PlannedOn { get; set; }

        [Order(5)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.AssignedEmployeeId), typeof(RecruitmentProcessStepResources))]
        [Render(Size = Size.Large)]
        [Tab(StepDetailsTab)]
        public virtual long? AssignedEmployeeId { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.JobOpeningDecisionMakerId), typeof(RecruitmentProcessStepResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Render(Size = Size.Large)]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        public long? JobOpeningDecisionMakerId { get; set; }

        [Order(7)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.OtherAttendingEmployees), typeof(RecruitmentProcessStepResources))]
        [Render(Size = Size.Large)]
        [Tab(StepDetailsTab)]
        [Visibility(VisibilityScope.Form)]
        public virtual List<long> OtherAttendingEmployeeIds { get; set; }

        [Order(8)]
        [Multiline(2)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.RecruitmentHint), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        [StringLength(1024)]
        public string RecruitmentHint { get; set; }

        [Order(9)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.Decision), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        public RecruitmentProcessStepDecision Decision { get; set; }

        [Order(10)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.DecidedOn), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        [StringFormat("{0:g}")]
        public DateTime? DecidedOn { get; set; }

        [Order(11)]
        [Multiline(2)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.Comment), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        [StringLength(1024)]
        public string DecisionComment { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public RecruitmentProcessStepWorkflowActionType? WorkflowAction { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(NoteTab)]
        [ReadOnly(true)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RecruitmentProcessNotes")]
        public List<RecruitmentProcessNoteViewModel> Notes { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CreatedOn), typeof(RecruitmentProcessResources))]
        [Tab(ContributorTab)]
        [ReadOnly(true)]
        [Order(1)]
        public DateTime? CreatedOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CreatedByFullName), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [Tab(ContributorTab)]
        [ReadOnly(true)]
        [Order(2)]
        public string CreatedByFullName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.LastModifiedOn), typeof(RecruitmentProcessStepResources))]
        [Tab(ContributorTab)]
        [ReadOnly(true)]
        [Order(3)]
        public DateTime? ModifiedOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.LastModifiedByFullName), typeof(RecruitmentProcessStepResources))]
        [Tab(ContributorTab)]
        [ReadOnly(true)]
        [Order(4)]
        public string ModifiedByFullName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(CandidateTechnicalReviewTab)]
        [ReadOnly(true)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.TechnicalVerificationDetails")]
        public List<TechnicalReviewDetailViewModel> TechnicalReviews { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public string WorkflowScript { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RecruitmentProcessStepBaseDetails")]
        [RoleRequired(SecurityRoleType.CanViewBasicCandidateData)]
        public RecruitmentProcessStepDetailsViewModel Details { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.StepTimeline")]
        public List<RecruitmentProcessStepTimelineViewModel> StepTimeline { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.CloseStep), typeof(RecruitmentProcessStepResources))]
        public bool CloseCurrentStep { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.None)]
        public DateTime? DueOn { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long? NextStepId { get; set; }

        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public string NotesTabLabel => $"{RecruitmentProcessStepResources.NoteTabText} ({Notes?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string TechnicalReviewsTabLabel =>
            $"{RecruitmentProcessStepResources.CandidateTechnicalReviewTabText} ({TechnicalReviews?.Count ?? 0})";

        public override string ToString()
        {
            return $"{Type.GetDescriptionOrValue()} {PlannedOn?.ToShortDateString()}";
        }

        public RecruitmentProcessStepViewModel()
        {
            CloseCurrentStep = true;
        }
    }
}
