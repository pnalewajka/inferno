﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.TechnicalReviewRecruitmentProcessStepViewModel")]
    public class TechnicalReviewRecruitmentProcessStepViewModel : RecruitmentProcessStepViewModel
    {
        [TimePicker]
        public override DateTime? PlannedOn { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=technical-interviewers'")]
        public override long? AssignedEmployeeId { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified-or-technical-interviewers'")]
        public override List<long> OtherAttendingEmployeeIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.JobProfilesShort), typeof(TechnicalReviewResources))]
        [NonSortable]
        [Tab(TechnicalReviewTab)]
        public List<long> JobProfileIds { get; set; }

        [Tab(TechnicalReviewTab)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.VacancyLevels")]
        public VacanciesDto[] Vacancies { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.SeniorityLevelAssessment), typeof(TechnicalReviewResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<SeniorityLevelEnum>))]
        [Tab(TechnicalReviewTab)]
        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.SoftwareEngineeringAssessment), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(3)]
        public string TechnicalKnowledgeAssessment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.TechnicalAssignmentAssessment), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(3)]
        public string TechnicalAssignmentAssessment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.EnglishUsageAssessment), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(2)]
        public string EnglishUsageAssessment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.StrengthAssessment), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(3)]
        public string StrengthAssessment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.RiskAssessment), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(3)]
        public string RiskAssessment { get; set; }
        
        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.ExperienceAssessment), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(3)]
        public string TeamProjectSuitabilityAssessment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [MaxLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.OtherRemarks), nameof(TechnicalReviewResources.OtherRemarksLabelShort), typeof(TechnicalReviewResources))]
        [Tab(TechnicalReviewTab)]
        [Multiline(3)]
        public string OtherRemarks { get; set; }

        public TechnicalReviewRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.TechnicalReview;
            Vacancies = new VacanciesDto[0];
        }
    }
}