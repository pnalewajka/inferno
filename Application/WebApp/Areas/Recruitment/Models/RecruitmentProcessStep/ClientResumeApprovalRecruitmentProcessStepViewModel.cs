﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.ClientResumeApprovalRecruitmentProcessStepViewModel")]
    public class ClientResumeApprovalRecruitmentProcessStepViewModel : RecruitmentProcessStepViewModel
    {
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.ResumeShownToClientOn), typeof(RecruitmentProcessStepResources))]
        [Tab(StepDetailsTab)]
        [TimePicker]
        public DateTime? ResumeShownToClientOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public override DateTime? PlannedOn { get; set; }

        public ClientResumeApprovalRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.ClientResumeApproval;
        }
    }
}