﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.HiringManagerResumeApprovalRecruitmentProcessStepViewModel")]
    public class HiringManagerResumeApprovalRecruitmentProcessStepViewModel : HiringManagerProcessStepViewModel
    {
        [Visibility(VisibilityScope.None)]
        public override DateTime? PlannedOn { get; set; }

        public HiringManagerResumeApprovalRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.HiringManagerResumeApproval;
        }
    }
}