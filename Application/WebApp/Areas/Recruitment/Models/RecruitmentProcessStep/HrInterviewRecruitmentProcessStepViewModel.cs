﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.HrInterviewRecruitmentProcessStepViewModel")]
    public class HrInterviewRecruitmentProcessStepViewModel : ScreeningProcessStepViewModel
    {
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public override long? AssignedEmployeeId { get; set; }

        public HrInterviewRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.HrInterview;
        }
    }
}