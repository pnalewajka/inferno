﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.ClientInterviewRecruitmentProcessStepViewModel")]
    public class ClientInterviewRecruitmentProcessStepViewModel : RecruitmentProcessStepViewModel
    {
        [TimePicker]
        public override DateTime? PlannedOn { get; set; }

        public ClientInterviewRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.ClientInterview;
        }
    }
}