﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    public class RecruitmentProcessStepViewModelBinder : AtomicModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var hiringModePropertyName = PropertyHelper.GetPropertyName<RecruitmentProcessStepViewModel>(s => s.Type);
            var hiringModeParameterName = NamingConventionHelper.ConvertPascalCaseToHyphenated(hiringModePropertyName);
            var value = bindingContext.ValueProvider.GetValue(hiringModeParameterName)
                        ?? bindingContext.ValueProvider.GetValue(hiringModePropertyName);

            if (value != null)
            {
                var pascalCaseValue = NamingConventionHelper.ConvertHyphenatedToPascalCase(value.AttemptedValue);
                var stepType = EnumHelper.GetEnumValue<RecruitmentProcessStepType>(pascalCaseValue);
                var viewModelType = RecruitmentProcessStepDtoToRecruitmentProcessStepViewModelMapping.CreateViewModelType(stepType);

                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, viewModelType);
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}