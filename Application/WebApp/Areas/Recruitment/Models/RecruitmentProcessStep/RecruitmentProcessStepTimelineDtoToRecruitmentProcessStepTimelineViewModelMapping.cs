﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    public class RecruitmentProcessStepTimelineDtoToRecruitmentProcessStepTimelineViewModelMapping : ClassMapping<RecruitmentProcessStepTimelineDto, RecruitmentProcessStepTimelineViewModel>
    {
        public RecruitmentProcessStepTimelineDtoToRecruitmentProcessStepTimelineViewModelMapping()
        {
            Mapping = s => new RecruitmentProcessStepTimelineViewModel
            {
                Id = s.Id,
                CurrentId = s.CurrentId,
                Decision = s.Decision,
                AssignedEmployeeName = s.AssignedEmployeeName,
                PlannedOn = s.PlannedOn,
                Status = s.Status,
                Type = s.Type,
                CanBeViewedByCurrentUser = s.CanBeViewedByCurrentUser
            };
        }
    }
}