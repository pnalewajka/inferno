﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    public class RecruitmentProcessStepDtoToRecruitmentProcessStepViewModelMapping : ClassMapping<RecruitmentProcessStepDto, RecruitmentProcessStepViewModel>
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;

        public RecruitmentProcessStepDtoToRecruitmentProcessStepViewModelMapping(
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
            _classMappingFactory = classMappingFactory;
            Mapping = d => GetViewModel(d);
        }

        private RecruitmentProcessStepViewModel GetViewModel(RecruitmentProcessStepDto dto)
        {
            var viewModel = CreateViewModel(dto.Type);

            SetCommonValuesFromDto(dto, viewModel);

            SetDetailsFromDto(dto, viewModel);

            viewModel.FromStepId = dto.FromStepId;

            return viewModel;
        }

        private void SetCommonValuesFromDto(
            RecruitmentProcessStepDto dto,
            RecruitmentProcessStepViewModel model)
        {
            var currentPrincipal = _principalProvider.Current;
            var timelineMapping = _classMappingFactory.CreateMapping<RecruitmentProcessStepTimelineDto, RecruitmentProcessStepTimelineViewModel>();

            model.Id = dto.Id;
            model.RecruitmentProcessId = dto.RecruitmentProcessId;
            model.RecruitmentProcessStatus = dto.RecruitmentProcessStatus;
            model.Type = dto.Type;
            model.PlannedOn = dto.PlannedOn;
            model.DueOn = dto.DueOn;
            model.AssignedEmployeeId = dto.AssignedEmployeeId;
            model.OtherAttendingEmployeeIds = new List<long>(dto.OtherAssignedEmployeeIds ?? new long[0]);
            model.Decision = dto.Decision;
            model.CreatedById = dto.CreatedById;
            model.CreatedByFullName = dto.CreatedByFullName;
            model.CreatedOn = dto.CreatedOn;
            model.ModifiedByFullName = dto.ModifiedByFullName;
            model.ModifiedOn = dto.ModifiedOn;
            model.DecidedOn = dto.DecidedOn;
            model.Status = dto.Status;
            model.DecisionComment = dto.DecisionComment;
            model.CloseCurrentStep = dto.CloseCurrentStep;
            model.RecruitmentHint = dto.RecruitmentHint;
            model.JobOpeningDecisionMakerId = dto.JobOpeningDecisionMakerId;
            model.NextStepId = dto.NextStepId;
            model.StepTimeline = dto.StepTimeline.Select(timelineMapping.CreateFromSource).ToList();
            model.Details = new RecruitmentProcessStepDetailsViewModel
            {
                Candidate = dto.Candidate != null
                    ? _classMappingFactory.CreateMapping<CandidateDto, CandidateViewModel>().CreateFromSource(dto.Candidate)
                    : null,
                JobOpening = dto.JobOpening != null
                    ? _classMappingFactory.CreateMapping<JobOpeningDto, JobOpeningViewModel>().CreateFromSource(dto.JobOpening)
                    : null,
                RecruitmentProcess = dto.RecruitmentProcess != null
                    ? _classMappingFactory.CreateMapping<RecruitmentProcessDto, RecruitmentProcessViewModel>().CreateFromSource(dto.RecruitmentProcess)
                    : null,
                TechnicalReviews = dto.TechnicalReviews != null
                    ? dto.TechnicalReviews.Select(s => _classMappingFactory.CreateMapping<TechnicalReviewDto, TechnicalReviewDetailViewModel>().CreateFromSource(s)).ToList()
                    : null,
                CanViewCandidates = currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentCandidate),
                CanViewJobOpenings = currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentJobOpening),
                CanViewProcesses = currentPrincipal.IsInRole(SecurityRoleType.CanViewRecruitmentProcess),
            };
            model.Notes = model.Details.RecruitmentProcess?.Notes ?? new List<RecruitmentProcessNoteViewModel>();
            model.TechnicalReviews = model.Details.TechnicalReviews ?? new List<TechnicalReviewDetailViewModel>();
            model.Timestamp = dto.Timestamp;
        }

        private RecruitmentProcessStepViewModel CreateViewModel(RecruitmentProcessStepType dtoType)
        {
            switch (dtoType)
            {
                case RecruitmentProcessStepType.HrCall:
                    return new HrCallRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.HrInterview:
                    return new HrInterviewRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.TechnicalReview:
                    return new TechnicalReviewRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    return new HiringManagerResumeApprovalRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.ClientResumeApproval:
                    return new ClientResumeApprovalRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.HiringManagerInterview:
                    return new HiringManagerInterviewRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.ClientInterview:
                    return new ClientInterviewRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.HiringDecision:
                    return new HiringDecisionRecruitmentProcessStepViewModel();

                case RecruitmentProcessStepType.ContractNegotiations:
                    return new ContractNegotiationsRecruitmentProcessStepViewModel();

                default:
                    throw new ArgumentOutOfRangeException(nameof(dtoType));
            }
        }

        public static Type CreateViewModelType(RecruitmentProcessStepType dtoType)
        {
            switch (dtoType)
            {
                case RecruitmentProcessStepType.HrCall:
                    return typeof(HrCallRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.HrInterview:
                    return typeof(HrInterviewRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.TechnicalReview:
                    return typeof(TechnicalReviewRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    return typeof(HiringManagerResumeApprovalRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.ClientResumeApproval:
                    return typeof(ClientResumeApprovalRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.HiringManagerInterview:
                    return typeof(HiringManagerInterviewRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.ClientInterview:
                    return typeof(ClientInterviewRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.HiringDecision:
                    return typeof(HiringDecisionRecruitmentProcessStepViewModel);

                case RecruitmentProcessStepType.ContractNegotiations:
                    return typeof(ContractNegotiationsRecruitmentProcessStepViewModel);

                default:
                    throw new ArgumentOutOfRangeException(nameof(dtoType));
            }
        }

        private void SetDetailsFromDto(RecruitmentProcessStepDto dto, RecruitmentProcessStepViewModel model)
        {
            if (dto.StepDetails == null)
            {
                return;
            }

            switch (dto.Type)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    SetScreeningData((ScreeningStepDetailsDto)dto.StepDetails, (ScreeningProcessStepViewModel)model);
                    break;

                case RecruitmentProcessStepType.TechnicalReview:
                    SetTechnicalReviewDetailsFromDto((TechnicalReviewStepDetailsDto)dto.StepDetails, (TechnicalReviewRecruitmentProcessStepViewModel)model);
                    break;

                case RecruitmentProcessStepType.ClientResumeApproval:
                    ((ClientResumeApprovalRecruitmentProcessStepViewModel)model).ResumeShownToClientOn = ((ClientResumeApprovalDto)dto.StepDetails).ResumeShownToClientOn;
                    break;

                case RecruitmentProcessStepType.HiringManagerInterview:
                    break;

                case RecruitmentProcessStepType.ClientInterview:
                    break;

                case RecruitmentProcessStepType.HiringDecision:
                    break;

                case RecruitmentProcessStepType.ContractNegotiations:
                    SetContractNegotiations((ContractNegotiationsDetailsDto)dto.StepDetails, (ContractNegotiationsRecruitmentProcessStepViewModel)model);
                    break;

                default:
                    break;
            }
        }

        private static void SetContractNegotiations(ContractNegotiationsDetailsDto stepDetails, ContractNegotiationsRecruitmentProcessStepViewModel model)
        {
            model.FinalStartDate = stepDetails.FinalStartDate;
            model.FinalSignedDate = stepDetails.FinalSignedDate;
            model.FinalPositionId = stepDetails.FinalPositionId;
            model.FinalTrialContractTypeId = stepDetails.FinalTrialContractTypeId;
            model.FinalLongTermContractTypeId = stepDetails.FinalLongTermContractTypeId;
            model.FinalTrialSalary = stepDetails.FinalTrialSalary;
            model.FinalLongTermSalary = stepDetails.FinalLongTermSalary;
            model.FinalLocationId = stepDetails.FinalLocationId;
            model.FinalEmploymentSourceId = stepDetails.FinalEmploymentSourceId;
            model.FinalComment = stepDetails.FinalComment;
            model.OfferPositionId = stepDetails.OfferPositionId;
            model.OfferContractType = stepDetails.OfferContractType;
            model.OfferSalary = stepDetails.OfferSalary;
            model.OfferStartDate = stepDetails.OfferStartDate;
            model.OfferLocationId = stepDetails.OfferLocationId;
            model.OfferComment = stepDetails.OfferComment;
        }

        private static void SetScreeningData(ScreeningStepDetailsDto stepDetails, ScreeningProcessStepViewModel model)
        {
            model.ScreeningAvailability = stepDetails.ScreeningAvailability;
            model.ScreeningBusinessTripsCanDo = stepDetails.ScreeningBusinessTripsCanDo;
            model.ScreeningBusinessTripsComment = stepDetails.ScreeningBusinessTripsComment;
            model.ScreeningContractExpectations = stepDetails.ScreeningContractExpectations;
            model.ScreeningCounterOfferCriteria = stepDetails.ScreeningCounterOfferCriteria;
            model.ScreeningGeneralOpinion = stepDetails.ScreeningGeneralOpinion;
            model.ScreeningLanguageEnglish = stepDetails.ScreeningLanguageEnglish;
            model.ScreeningLanguageGerman = stepDetails.ScreeningLanguageGerman;
            model.ScreeningLanguageOther = stepDetails.ScreeningLanguageOther;
            model.ScreeningMotivation = stepDetails.ScreeningMotivation;
            model.ScreeningOtherProcesses = stepDetails.ScreeningOtherProcesses;
            model.ScreeningRelocationCanDo = stepDetails.ScreeningRelocationCanDo;
            model.ScreeningRelocationComment = stepDetails.ScreeningRelocationComment;
            model.ScreeningSkills = stepDetails.ScreeningSkills;
            model.ScreeningWorkPermitComment = stepDetails.ScreeningWorkPermitComment;
            model.ScreeningWorkPermitNeeded = stepDetails.ScreeningWorkPermitNeeded;
            model.ScreeningWorkplaceExpectations = stepDetails.ScreeningWorkplaceExpectations;
            model.ScreeningEveningWorkCanDo = stepDetails.ScreeningEveningWorkCanDo;
            model.ScreeningEveningWorkComment = stepDetails.ScreeningEveningWorkComment;
        }

        private static void SetTechnicalReviewDetailsFromDto(TechnicalReviewStepDetailsDto stepDetails, TechnicalReviewRecruitmentProcessStepViewModel model)
        {
            model.JobProfileIds = stepDetails.JobProfileIds;
            model.EnglishUsageAssessment = stepDetails.EnglishUsageAssessment;
            model.TeamProjectSuitabilityAssessment = stepDetails.TeamProjectSuitabilityAssessment;
            model.OtherRemarks = stepDetails.OtherRemarks;
            model.RiskAssessment = stepDetails.RiskAssessment;
            model.SeniorityLevelAssessment = stepDetails.SeniorityLevelAssessment;
            model.Vacancies = stepDetails.Vacancies;
            model.TechnicalKnowledgeAssessment = stepDetails.TechnicalKnowledgeAssessment;
            model.StrengthAssessment = stepDetails.StrengthAssessment;
            model.TechnicalAssignmentAssessment = stepDetails.TechnicalAssignmentAssessment;
        }
    }
}