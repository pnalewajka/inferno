﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.ScreeningProcessStepViewModel")]
    public class ScreeningProcessStepViewModel : RecruitmentProcessStepViewModel
    {
        [TimePicker]
        public override DateTime? PlannedOn { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningGeneralOpinion), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningGeneralOpinion { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningMotivation), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningMotivation { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningWorkplaceExpectations), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningWorkplaceExpectations { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningSkills), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningSkills { get; set; }

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningLanguageEnglish), typeof(RecruitmentProcessResources))]
        [Multiline(2)]
        [Tab(ScreeningTab)]
        public string ScreeningLanguageEnglish { get; set; }

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningLanguageGerman), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(2)]
        public string ScreeningLanguageGerman { get; set; }

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningLanguageOther), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(2)]
        public string ScreeningLanguageOther { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningContractExpectations), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningContractExpectations { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningAvailability), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(2)]
        public string ScreeningAvailability { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningOtherProcesses), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(2)]
        public string ScreeningOtherProcesses { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningCounterOfferCriteria), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(2)]
        public string ScreeningCounterOfferCriteria { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningRelocationCanDo), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        public bool? ScreeningRelocationCanDo { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningRelocationComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningRelocationComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningBusinessTripsCanDo), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        public bool? ScreeningBusinessTripsCanDo { get; set; }

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningBusinessTripsComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningBusinessTripsComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningWorkPermitNeeded), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        public bool? ScreeningWorkPermitNeeded { get; set; }

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningWorkPermitComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningWorkPermitComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningEveningWorkCanDo), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        public bool? ScreeningEveningWorkCanDo { get; set; }

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningEveningWorkComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        public string ScreeningEveningWorkComment { get; set; }
    }
}