﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.RecrutimentProcessStepRejectionViewModel")]
    public class RecrutimentProcessStepRejectionViewModel
    {
        [Required]
        [Multiline(3)]
        [MaxLength(1024)]
        [DisplayNameLocalized(nameof(RecruitmentProcessStepResources.RejectionComment), typeof(RecruitmentProcessStepResources))]
        public string RejectionComment { get; set; }
    }
}