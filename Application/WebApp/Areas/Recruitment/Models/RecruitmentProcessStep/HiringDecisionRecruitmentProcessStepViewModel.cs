﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.HiringDecisionRecruitmentProcessStepViewModel")]
    public class HiringDecisionRecruitmentProcessStepViewModel : HiringManagerProcessStepViewModel
    {
        [Visibility(VisibilityScope.None)]
        public override DateTime? PlannedOn { get; set; }

        public HiringDecisionRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.HiringDecision;
        }
    }
}