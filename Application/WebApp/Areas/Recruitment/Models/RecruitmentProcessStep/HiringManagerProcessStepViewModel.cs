﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.HiringManagerRecruitmentProcessStepViewModel")]
    public class HiringManagerProcessStepViewModel : RecruitmentProcessStepViewModel
    {
        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningGeneralOpinion), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        [ReadOnly(true)]
        public string ScreeningGeneralOpinion => Details?.RecruitmentProcess?.ScreeningGeneralOpinion;


        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningMotivation), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        [ReadOnly(true)]
        public string ScreeningMotivation => Details?.RecruitmentProcess?.ScreeningMotivation;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningWorkplaceExpectations), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        [ReadOnly(true)]
        public string ScreeningWorkplaceExpectations => Details?.RecruitmentProcess?.ScreeningWorkplaceExpectations;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningSkills), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        [ReadOnly(true)]
        public string ScreeningSkills => Details?.RecruitmentProcess?.ScreeningSkills;

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningLanguageEnglish), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(2)]
        [ReadOnly(true)]
        public string ScreeningLanguageEnglish => Details?.RecruitmentProcess?.ScreeningLanguageEnglish;

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningLanguageGerman), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningLanguageGerman => Details?.RecruitmentProcess?.ScreeningLanguageGerman;

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningLanguageOther), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningLanguageOther => Details?.RecruitmentProcess?.ScreeningLanguageOther;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningContractExpectations), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(3)]
        [ReadOnly(true)]
        public string ScreeningContractExpectations => Details?.RecruitmentProcess?.ScreeningContractExpectations;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningAvailability), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningAvailability => Details?.RecruitmentProcess?.ScreeningAvailability;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningOtherProcesses), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningOtherProcesses => Details?.RecruitmentProcess?.ScreeningOtherProcesses;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningCounterOfferCriteria), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningCounterOfferCriteria => Details?.RecruitmentProcess?.ScreeningCounterOfferCriteria;

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningRelocationCanDo), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        [ReadOnly(true)]
        public bool? ScreeningRelocationCanDo => Details?.RecruitmentProcess?.ScreeningRelocationCanDo;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningRelocationComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningRelocationComment => Details?.RecruitmentProcess?.ScreeningRelocationComment;

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningBusinessTripsCanDo), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        [ReadOnly(true)]
        public bool? ScreeningBusinessTripsCanDo => Details?.RecruitmentProcess?.ScreeningBusinessTripsCanDo;

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningBusinessTripsComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningBusinessTripsComment => Details?.RecruitmentProcess?.ScreeningBusinessTripsComment;

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningEveningWorkCanDo), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        [ReadOnly(true)]
        public bool? ScreeningEveningWorkCanDo => Details?.RecruitmentProcess?.ScreeningEveningWorkCanDo;

        [StringLength(3000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningEveningWorkComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningEveningWorkComment => Details?.RecruitmentProcess?.ScreeningEveningWorkComment;

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningWorkPermitNeeded), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(YesNoItemProvider))]
        [ReadOnly(true)]
        public bool? ScreeningWorkPermitNeeded => Details?.RecruitmentProcess?.ScreeningWorkPermitNeeded;

        [StringLength(1000)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ScreeningWorkPermitComment), typeof(RecruitmentProcessResources))]
        [Tab(ScreeningTab)]
        [Multiline(1)]
        [ReadOnly(true)]
        public string ScreeningWorkPermitComment => Details?.RecruitmentProcess?.ScreeningWorkPermitComment;
    }
}