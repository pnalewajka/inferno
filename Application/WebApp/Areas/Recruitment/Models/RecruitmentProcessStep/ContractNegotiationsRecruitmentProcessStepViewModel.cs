﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    [Identifier("Models.ContractNegotiationsRecruitmentProcessStepViewModel")]
    public partial class ContractNegotiationsRecruitmentProcessStepViewModel : RecruitmentProcessStepViewModel
    {
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public override long? AssignedEmployeeId { get; set; }

        [TimePicker]
        public override DateTime? PlannedOn { get; set; }

        public ContractNegotiationsRecruitmentProcessStepViewModel()
        {
            Type = RecruitmentProcessStepType.ContractNegotiations;
        }
    }
}