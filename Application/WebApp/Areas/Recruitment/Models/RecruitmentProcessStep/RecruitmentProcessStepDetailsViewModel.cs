﻿using System.Collections.Generic;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecruitmentProcessStep
{
    public class RecruitmentProcessStepDetailsViewModel
    {
        public CandidateViewModel Candidate { get; set; }

        public JobOpeningViewModel JobOpening { get; set; }

        public RecruitmentProcessViewModel RecruitmentProcess { get; set; }

        public List<TechnicalReviewDetailViewModel> TechnicalReviews { get; set; }

        public bool CanViewJobOpenings { get; set; }

        public bool CanViewCandidates { get; set; }

        public bool CanViewProcesses { get; set; }
    }
}