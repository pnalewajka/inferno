﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessDurationPerRecruiterReportParametersDtoToProcessDurationPerRecruiterReportParametersViewModelMapping
        : ClassMapping<ProcessDurationPerRecruiterReportParametersDto, ProcessDurationPerRecruiterReportParametersViewModel>
    {
        public ProcessDurationPerRecruiterReportParametersDtoToProcessDurationPerRecruiterReportParametersViewModelMapping()
        {
            Mapping = v => new ProcessDurationPerRecruiterReportParametersViewModel
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                RecruiterIds = v.RecruiterIds,
                ProcessResult = v.ProcessResult
            };
        }
    }
}