﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessReopenDtoToRecruitmentProcessReopenViewModelMapping : ClassMapping<RecruitmentProcessReopenViewModel, RecruitmentProcessReopenDto>
    {
        public RecruitmentProcessReopenDtoToRecruitmentProcessReopenViewModelMapping()
        {
            Mapping = d => new RecruitmentProcessReopenDto
            {
                Id = d.Id,
                Comment = d.Comment
            };
        }
    }
}