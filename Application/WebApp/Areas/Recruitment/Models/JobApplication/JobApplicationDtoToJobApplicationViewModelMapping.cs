﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    public class JobApplicationDtoToJobApplicationViewModelMapping : ClassMapping<JobApplicationDto, JobApplicationViewModel>
    {
        public JobApplicationDtoToJobApplicationViewModelMapping()
        {
            Mapping = d => GetViewModel(d);
        }

        public static Type GetViewModelType(ApplicationOriginType originType)
        {
            switch (originType)
            {
                case ApplicationOriginType.Agency:
                    return typeof(AgencyJobApplicationViewModel);

                case ApplicationOriginType.DropOff:
                    return typeof(DropOffJobApplicationViewModel);

                case ApplicationOriginType.JobAdvertisement:
                    return typeof(JobAdvertisementJobApplicationViewModel);

                case ApplicationOriginType.Event:
                    return typeof(JobFairJobApplicationViewModel);

                case ApplicationOriginType.Other:
                    return typeof(OtherJobApplicationViewModel);

                case ApplicationOriginType.Recommendation:
                    return typeof(RecommendationJobApplicationViewModel);

                case ApplicationOriginType.SocialNetwork:
                    return typeof(SocialNetworkJobApplicationViewModel);

                case ApplicationOriginType.Researching:
                    return typeof(ResearchJobApplicationViewModel);

                case ApplicationOriginType.DirectSearch:
                    return typeof(DirectSearchJobApplicationViewModel);

                default:
                    throw new ArgumentOutOfRangeException(nameof(originType));
            }
        }

        public static JobApplicationViewModel CreateViewModel(ApplicationOriginType originType)
        {
            switch (originType)
            {
                case ApplicationOriginType.Agency:
                    return new AgencyJobApplicationViewModel();

                case ApplicationOriginType.DropOff:
                    return new DropOffJobApplicationViewModel();

                case ApplicationOriginType.JobAdvertisement:
                    return new JobAdvertisementJobApplicationViewModel();

                case ApplicationOriginType.Event:
                    return new JobFairJobApplicationViewModel();

                case ApplicationOriginType.Other:
                    return new OtherJobApplicationViewModel();

                case ApplicationOriginType.Recommendation:
                    return new RecommendationJobApplicationViewModel();

                case ApplicationOriginType.SocialNetwork:
                    return new SocialNetworkJobApplicationViewModel();

                case ApplicationOriginType.Researching:
                    return new ResearchJobApplicationViewModel();

                case ApplicationOriginType.DirectSearch:
                    return new DirectSearchJobApplicationViewModel();

                default:
                    throw new ArgumentOutOfRangeException(nameof(originType));
            }
        }

        public static JobApplicationViewModel GetViewModel(JobApplicationDto dto)
        {
            var viewModel = CreateViewModel(dto.OriginType);

            SetCommonValuesFromDto(dto, viewModel);

            return viewModel;
        }

        private static void SetCommonValuesFromDto(JobApplicationDto dto, JobApplicationViewModel model)
        {
            model.Id = dto.Id;
            model.SourceEmailId = dto.SourceEmailId;
            model.CandidateId = dto.CandidateId;
            model.OriginType = dto.OriginType;
            model.ApplicationOriginId = dto.ApplicationOriginId;
            model.RecommendingPersonId = dto.RecommendingPersonId;
            model.OriginComment = dto.OriginComment;
            model.JobProfileIds = dto.JobProfileIds.ToList();
            model.CityIds = dto.CityIds.ToList();
            model.CandidateComment = dto.CandidateComment;
            model.CreatedOn = dto.CreatedOn;
            model.CreatedByFullName = dto.CreatedByFullName;
            model.IsEligibleForReferralProgram = dto.IsEligibleForReferralProgram;
            model.Timestamp = dto.Timestamp;
        }
    }
}
