﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    [Identifier("Models.ResearchJobApplicationViewModel")]
    public class ResearchJobApplicationViewModel : JobApplicationViewModel
    {
        [Visibility(VisibilityScope.Form)]
        public override ApplicationOriginType OriginType { get; set; }

        [HiddenInput]
        public override long ApplicationOriginId { get; set; }

        public ResearchJobApplicationViewModel()
        {
            OriginType = ApplicationOriginType.Researching;
        }
    }
}