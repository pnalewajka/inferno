﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    [Identifier("Models.AgencyJobApplicationViewModel")]
    public class AgencyJobApplicationViewModel : JobApplicationViewModel
    {
        [ValuePicker(Type = typeof(ApplicationOriginPickerController), OnResolveUrlJavaScript = "url += '&origin-type=agency'")]
        [DisplayNameLocalized(nameof(JobApplicationResources.Agency), typeof(JobApplicationResources))]
        public override long ApplicationOriginId { get; set; }

        public AgencyJobApplicationViewModel()
        {
            OriginType = ApplicationOriginType.Agency;
        }
    }
}