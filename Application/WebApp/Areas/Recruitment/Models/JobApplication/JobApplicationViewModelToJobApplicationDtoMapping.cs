﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    public class JobApplicationViewModelToJobApplicationDtoMapping : ClassMapping<JobApplicationViewModel, JobApplicationDto>
    {
        public JobApplicationViewModelToJobApplicationDtoMapping()
        {
            Mapping = v => new JobApplicationDto
            {
                Id = v.Id,
                SourceEmailId = v.SourceEmailId,
                CandidateId = v.CandidateId,
                OriginType = v.OriginType,
                ApplicationOriginId = v.ApplicationOriginId,
                RecommendingPersonId = v.RecommendingPersonId,
                OriginComment = v.OriginComment,
                JobProfileIds = v.JobProfileIds.ToArray(),
                CityIds = v.CityIds.ToArray(),
                CandidateComment = v.CandidateComment,
                IsEligibleForReferralProgram = v.IsEligibleForReferralProgram,
                Timestamp = v.Timestamp
            };
        }
    }
}