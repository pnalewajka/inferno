﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    [Identifier("Models.SocialNetworkJobApplicationViewModel")]
    public class SocialNetworkJobApplicationViewModel : JobApplicationViewModel
    {
        [ValuePicker(Type = typeof(ApplicationOriginPickerController), OnResolveUrlJavaScript = "url += '&origin-type=social-network'")]
        [DisplayNameLocalized(nameof(JobApplicationResources.SocialNetwork), typeof(JobApplicationResources))]
        public override long ApplicationOriginId { get; set; }
        
        public SocialNetworkJobApplicationViewModel()
        {
            OriginType = ApplicationOriginType.SocialNetwork;
        }
    }
}