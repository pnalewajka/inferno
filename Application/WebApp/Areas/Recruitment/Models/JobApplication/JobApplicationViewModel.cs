﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Http.ModelBinding;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    [Identifier("Models.JobApplicationViewModel")]
    [ModelBinder(typeof(JobApplicationViewModelBinder))]
    public class JobApplicationViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobApplicationResources.CandidateID), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=true'")]
        [Render(Size = Size.Large)]
        [Order(1)]
        public long CandidateId { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(JobApplicationResources.OriginType), typeof(JobApplicationResources))]
        [ReadOnly(true)]
        [Render(Size = Size.Medium)]
        [Order(2)]
        public virtual ApplicationOriginType OriginType { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobApplicationResources.ApplicationOrigin), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(ApplicationOriginPickerController))]
        [Render(Size = Size.Large)]
        [Order(3)]
        public virtual long ApplicationOriginId { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(JobApplicationResources.RecommendingPersonId), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(RecommendingPersonPickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=true'")]
        [Render(Size = Size.Large)]
        [Order(4)]
        public virtual long? RecommendingPersonId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobApplicationResources.OriginComment), typeof(JobApplicationResources))]
        [Multiline(3)]
        [Order(5)]
        [StringLength(400)]
        public string OriginComment { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobApplicationResources.CityIds), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(CityPickerController))]
        [Order(6)]
        public List<long> CityIds { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobApplicationResources.JobProfileIds), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [Order(7)]
        public List<long> JobProfileIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobApplicationResources.CandidateComment), typeof(JobApplicationResources))]
        [Multiline(3)]
        [Order(8)]
        [StringLength(250)]
        public string CandidateComment { get; set; }

        [DisplayNameLocalized(nameof(JobApplicationResources.CreatedOn), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [Order(9)]
        public DateTime CreatedOn { get; set; }

        [DisplayNameLocalized(nameof(JobApplicationResources.CreatedBy), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        [Order(10)]
        public string CreatedByFullName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobApplicationResources.SourceEmail), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(InboundEmailPickerController), OnResolveUrlJavaScript = "url += '&assigned-only=true'")]
        [Order(11)]
        [Render(Size = Size.Large)]
        public long? SourceEmailId { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(JobApplicationResources.IsEligibleForReferralProgram), typeof(JobApplicationResources))]
        public virtual bool IsEligibleForReferralProgram { get; set; }

        public byte[] Timestamp { get; set; }

        public JobApplicationViewModel()
        {
            JobProfileIds = new List<long>();
            CityIds = new List<long>();
        }

        public override string ToString()
        {
            return $"{JobApplicationResources.JobApplication} #{Id} ({OriginType}, {CreatedOn.ToString("d")})";
        }
    }
}
