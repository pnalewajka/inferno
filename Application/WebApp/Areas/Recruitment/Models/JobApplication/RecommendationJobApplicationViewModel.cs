﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    [Identifier("Models.RecommendationJobApplicationViewModel")]
    public class RecommendationJobApplicationViewModel : JobApplicationViewModel
    {
        [Visibility(VisibilityScope.Form)]
        public override long? RecommendingPersonId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobApplicationResources.IsEligibleForReferralProgram), typeof(JobApplicationResources))]
        public override bool IsEligibleForReferralProgram { get; set; }

        [HiddenInput]
        public override long ApplicationOriginId { get; set; }

        public RecommendationJobApplicationViewModel()
        {
            OriginType = ApplicationOriginType.Recommendation;
        }
    }
}