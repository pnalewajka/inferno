﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobApplication
{
    [Identifier("Models.JobAdvertisementJobApplicationViewModel")]
    public class JobAdvertisementJobApplicationViewModel : JobApplicationViewModel
    {
        [ValuePicker(Type = typeof(ApplicationOriginPickerController), OnResolveUrlJavaScript = "url += '&origin-type=job-advertisement'")]
        [DisplayNameLocalized(nameof(JobApplicationResources.JobAdvertisment), typeof(JobApplicationResources))]
        public override long ApplicationOriginId { get; set; }
        
        public JobAdvertisementJobApplicationViewModel()
        {
            OriginType = ApplicationOriginType.JobAdvertisement;
        }
    }
}