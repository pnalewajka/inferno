﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessFunnelReportParametersViewModelToProcessFunnelReportParametersDtoMapping : ClassMapping<ProcessFunnelReportParametersViewModel, ProcessFunnelReportParametersDto>
    {
        public ProcessFunnelReportParametersViewModelToProcessFunnelReportParametersDtoMapping()
        {
            Mapping = v => new ProcessFunnelReportParametersDto
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                RecruiterIds = v.RecruiterIds
            };
        }
    }
}