﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.DocumentMappings;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [TabDescription(0, MessageTab, nameof(InboundEmailResources.MessageTabName), typeof(InboundEmailResources))]
    [TabDescription(1, ProcessTab, nameof(InboundEmailResources.ProcessTabName), typeof(InboundEmailResources))]
    [TabDescription(2, ValuesTab, nameof(InboundEmailResources.ValuesTabName), typeof(InboundEmailResources))]
    [TabDescription(3, RecommendationTab, nameof(InboundEmailResources.RecommendationTabName), typeof(InboundEmailResources))]
    [TabDescription(4, UnknownTab, nameof(InboundEmailResources.UnknownTabName), typeof(InboundEmailResources))]
    [TabDescription(5, JobApplicationTab, nameof(InboundEmailResources.JobApplicationTabName), typeof(InboundEmailResources))]
    [TabDescription(6, DataOwnerTab, nameof(InboundEmailResources.DataOwnerTabName), typeof(InboundEmailResources))]
    [TabDescription(9, ContributorsTab, nameof(InboundEmailResources.ContributorsTabName), typeof(InboundEmailResources))]
    [Identifier("Models.InboundEmailViewModel")]
    [ModelBinder(typeof(InboundEmailViewModelBinder))]
    public class InboundEmailViewModel : IGridViewRow, IValidatableObject
    {
        protected const string MessageTab = "message";
        protected const string ValuesTab = "values";
        protected const string ProcessTab = "process";
        protected const string ContributorsTab = "contributors";
        protected const string RecommendationTab = "recommendation";
        protected const string UnknownTab = "unknown";
        protected const string JobApplicationTab = "jobapplication";
        protected const string DataOwnerTab = "dataowner";

        public long Id { get; set; }

        [DisplayNameLocalized(nameof(InboundEmailResources.Body), typeof(InboundEmailResources))]
        [Multiline(4)]
        [Tab(MessageTab)]
        [Required]
        [Visibility(VisibilityScope.Form)]
        [AllowHtml]
        [HtmlSanitize]
        [Trim]
        public string Body { get; set; }

        [DisplayNameLocalized(nameof(InboundEmailResources.Body), typeof(InboundEmailResources))]
        [Tab(MessageTab)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.InboundEmailBody")]
        public string BodyDisplay => Body;

        [Tab(MessageTab)]
        [DisplayNameLocalized(nameof(InboundEmailResources.BodyType), typeof(InboundEmailResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<EmailBodyType>))]
        [RequiredEnum(typeof(EmailBodyType))]
        [Visibility(VisibilityScope.Form)]
        public EmailBodyType BodyType { get; set; }

        [Tab(MessageTab)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(InboundEmailResources.Documents), typeof(InboundEmailResources))]
        [DocumentUpload(typeof(InboundEmailDocumentMapping), DownloadUrlTemplate = "/Recruitment/InboundEmail/ViewFile/{1}", ShouldUseDownloadDialog = true)]
        public List<DocumentViewModel> Documents { get; set; }

        [Tab(MessageTab)]
        [DisplayNameLocalized(nameof(InboundEmailResources.To), typeof(InboundEmailResources))]
        [Required]
        [Trim]
        public string To { get; set; }

        [Tab(MessageTab)]
        [DisplayNameLocalized(nameof(InboundEmailResources.FromEmailAddress), typeof(InboundEmailResources))]
        [Required]
        [Trim]
        public string FromEmailAddress { get; set; }

        [Visibility(VisibilityScope.None)]
        public string FromFullName { get; set; }

        [Tab(MessageTab)]
        [DisplayNameLocalized(nameof(InboundEmailResources.Subject), typeof(InboundEmailResources))]
        [Required]
        [Trim]
        public string Subject { get; set; }

        [Tab(MessageTab)]
        [DisplayNameLocalized(nameof(InboundEmailResources.SentOn), typeof(InboundEmailResources))]
        [TimePicker(15)]
        [Render(GridCssClass = "date-time-column")]
        [StringFormat("{0:g}")]
        [Required]
        public DateTime SentOn { get; set; }

        [Tab(MessageTab)]
        [DisplayNameLocalized(nameof(InboundEmailResources.ReceivedOn), typeof(InboundEmailResources))]
        [Visibility(VisibilityScope.Form)]
        [TimePicker(15)]
        [Required]
        public DateTime ReceivedOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(ValuesTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.InboundEmailValue")]
        public List<InboundEmailValueViewModel> Values { get; set; }

        [OnValueChange(ClientHandler = "InboundEmail.assignWorkflowActionAndCallSubmit(5)")] //numer 5refers to InboundEmailWorkflowAction.ChangeCategory
        [DisplayNameLocalized(nameof(InboundEmailResources.InboundEmailCategory), typeof(InboundEmailResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<InboundEmailCategory>))]
        [RequiredEnum(typeof(InboundEmailCategory))]
        [Tab(ProcessTab)]
        public InboundEmailCategory Category { get; set; }

        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(InboundEmailResources.Status), typeof(InboundEmailResources))]
        [Render(Size = Size.Medium)]
        [Tab(ProcessTab)]
        public InboundEmailStatus Status { get; set; }

        [Tab(ContributorsTab)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(InboundEmailResources.ProcessedBy), typeof(InboundEmailResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified'")]
        [Render(Size = Size.Medium)]
        public long? ProcessedById { get; set; }

        [Visibility(VisibilityScope.None)]
        public long[] ReaderIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public InboundEmailColorCategory? EmailColorCategory { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasBeenReadByCurrentUser { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasBeenReadByOthers { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public InboundEmailWorkflowAction? WorkflowAction { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long? WorkflowProcessedById { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public string ClosedComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [StringLength(300)]
        [Multiline(3)]
        [Tab(ProcessTab)]
        [Trim]
        [DisplayNameLocalized(nameof(InboundEmailResources.ResearchingComment), typeof(InboundEmailResources))]
        public string ResearchingComment { get; set; }

        [Visibility(VisibilityScope.None)]
        [HiddenInput]
        public InboundEmailClosedReason? ClosedReason { get; set; }

        [Visibility(VisibilityScope.None)]
        public CandidateInfoViewModel MatchingCandidate { get; set; }

        [DisplayNameLocalized(nameof(InboundEmailResources.MatchingCandidate), typeof(InboundEmailResources))]
        public DisplayUrl GetMatchingCandidate(UrlHelper helper)
        {
            return MatchingCandidate == null ? null : new DisplayUrl
            {
                Label = MatchingCandidate.FullName,
                Action = helper.UriActionGet<CandidateController>(c => c.View(default(long)), KnownParameter.None, nameof(Id), MatchingCandidate.Id.ToString())
            };
        }

        [DisplayNameLocalized(nameof(InboundEmailResources.MostRecentRecruitmentProcess), typeof(InboundEmailResources))]
        public DisplayUrl GetMostRecentRecruitmentProcess(UrlHelper helper)
        {
            return MatchingCandidate?.MostRecentProcess == null ? null : new DisplayUrl
            {
                Label = MatchingCandidate.MostRecentProcess.ToString(),
                Action = helper.UriActionGet<RecruitmentProcessController>(c => c.View(default(long)), KnownParameter.None, nameof(Id), MatchingCandidate.MostRecentProcess?.Id.ToString())
            };
        }

        [Visibility(VisibilityScope.None)]
        public bool CanBeAssigned => (Status == InboundEmailStatus.New || Status == InboundEmailStatus.SentForResearch)
                    && CanBeAssignedBecauseOfCategory;

        [Visibility(VisibilityScope.None)]
        public bool CanBeUnassigned => Status == InboundEmailStatus.Assigned;

        protected virtual bool CanBeAssignedBecauseOfCategory => Category == InboundEmailCategory.JobApplication || Category == InboundEmailCategory.DataOwnerInquiry;

        public byte[] Timestamp { get; set; }

        public InboundEmailViewModel()
        {
            Documents = new List<DocumentViewModel>();
            Values = new List<InboundEmailValueViewModel>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (WorkflowAction != null)
            {
                if (Category == InboundEmailCategory.Unknown)
                {
                    yield return new PropertyValidationResult(InboundEmailResources.NeedToSpecifiyCategory, () => Category);
                }
            }
        }

        public override string ToString()
        {
            var from = string.IsNullOrWhiteSpace(FromFullName)
                ? FromEmailAddress
                : $"{FromFullName} [{FromEmailAddress}]";

            return $"{from} ({Subject})";
        }

        private IEnumerable<string> GetInboundEmailRowClass()
        {
            if (!HasBeenReadByCurrentUser)
            {
                yield return RecruitmentStyles.RowClassInboundEmailUnreadByMe;
            }

            if (HasBeenReadByOthers)
            {
                yield return RecruitmentStyles.RowClassInboundEmailReadByOthers;
            }

            if (EmailColorCategory.HasValue)
            {
                yield return InboundEmailHelper.GetEmailColorCategoryCssClass(EmailColorCategory.Value);
            }
        }

        private IEnumerable<string> GetInboundEmailRowTitle()
        {
            if (!HasBeenReadByCurrentUser)
            {
                yield return InboundEmailResources.NewMessage;
            }

            if (HasBeenReadByOthers)
            {
                var otherReadersLength = HasBeenReadByCurrentUser ? ReaderIds.Length - 1 : ReaderIds.Length;
                var otherReadersTitle = otherReadersLength > 1
                    ? string.Format(InboundEmailResources.OtherPeopleReadMessage, otherReadersLength)
                    : InboundEmailResources.OneOtherPersonReadMessage;

                yield return HasBeenReadByCurrentUser ? otherReadersTitle : $"({otherReadersTitle})";
            }
        }

        string IGridViewRow.RowCssClass => string.Join(" ", GetInboundEmailRowClass());

        string IGridViewRow.RowTitle => string.Join(" ", GetInboundEmailRowTitle());
    }
}