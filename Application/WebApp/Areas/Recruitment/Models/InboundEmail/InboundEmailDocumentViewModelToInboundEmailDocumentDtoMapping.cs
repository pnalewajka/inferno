﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    public class InboundEmailDocumentViewModelToInboundEmailDocumentDtoMapping :
        ClassMapping<InboundEmailDocumentViewModel, InboundEmailDocumentDto>
    {
        public InboundEmailDocumentViewModelToInboundEmailDocumentDtoMapping()
        {
            Mapping = d => new InboundEmailDocumentDto
            {
                DocumentId = d.Id,
                ContentType = d.ContentType,
                DocumentName = d.DocumentName,
                FromEmailAddress = d.FromEmailAddress,
                FromFullName = d.FromFullName,
                Subject = d.Subject,
                To = d.To
            };
        }
    }
}