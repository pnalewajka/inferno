﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    public class InboundEmailDtoToInboundEmailViewModelMapping : ClassMapping<InboundEmailDto, InboundEmailViewModel>
    {
        private readonly IClassMapping<InboundEmailValueDto, InboundEmailValueViewModel> _inboundEmailValueDtoToInboundEmailValueViewModelMapping;
        private readonly IClassMapping<CandidateDto, CandidateInfoViewModel> _candidateDtoClassMapping;

        public InboundEmailDtoToInboundEmailViewModelMapping(
            IClassMapping<InboundEmailValueDto, InboundEmailValueViewModel> inboundEmailValueDtoToInboundEmailValueViewModelMapping,
            IClassMapping<CandidateDto, CandidateInfoViewModel> candidateDtoClassMapping)
        {
            _inboundEmailValueDtoToInboundEmailValueViewModelMapping = inboundEmailValueDtoToInboundEmailValueViewModelMapping;
            _candidateDtoClassMapping = candidateDtoClassMapping;
            Mapping = d => GetViewModel(d);
        }

        private InboundEmailViewModel GetViewModel(InboundEmailDto inboundEmailDto)
        {
            var viewModel = CreateViewModel(inboundEmailDto.Category, inboundEmailDto);

            SetCommonValuesFromDto(inboundEmailDto, viewModel);

            return viewModel;
        }

        private void SetCommonValuesFromDto(InboundEmailDto d, InboundEmailViewModel model)
        {
            model.Id = d.Id;
            model.To = d.To;
            model.FromEmailAddress = d.FromEmailAddress;
            model.FromFullName = d.FromFullName;
            model.Subject = d.Subject;
            model.Body = d.Body;
            model.BodyType = d.BodyType;
            model.SentOn = d.SentOn;
            model.ReceivedOn = d.ReceivedOn;
            model.Status = d.Status;

            model.ProcessedById = d.ProcessedById;
            model.Category = d.Category;
            model.HasBeenReadByCurrentUser = d.HasBeenReadByCurrentUser;
            model.HasBeenReadByOthers = d.HasBeenReadByOthers;
            model.ReaderIds = d.ReaderIds;
            model.Documents = (d.Documents ?? new DocumentDto[0])
                .Select(p => new DocumentViewModel
                {
                    ContentType = p.ContentType,
                    DocumentName = p.DocumentName,
                    DocumentId = p.DocumentId,
                    TemporaryDocumentId = p.TemporaryDocumentId,
                })
                .ToList();
            model.Values = d.Values.Select(_inboundEmailValueDtoToInboundEmailValueViewModelMapping.CreateFromSource).ToList();
            model.WorkflowAction = d.WorkflowAction;
            model.WorkflowProcessedById = d.WorkflowProcessedById;
            model.ClosedComment = d.ClosedComment;
            model.ClosedReason = d.ClosedReason;
            model.ResearchingComment = d.ResearchingComment;
            model.Timestamp = d.Timestamp;
            model.MatchingCandidate = d.MatchingCandidateDto == null
                ? null
                : _candidateDtoClassMapping.CreateFromSource(d.MatchingCandidateDto);
            model.EmailColorCategory = d.EmailColorCategory;
        }

        private static InboundEmailViewModel CreateViewModel(InboundEmailCategory category, InboundEmailDto inboundEmailDto)
        {
            switch (category)
            {
                case InboundEmailCategory.Unknown:
                    return new UnknownInboundEmailViewModel();

                case InboundEmailCategory.Spam:
                    return new SpamInboundEmailViewModel();

                case InboundEmailCategory.JobApplication:
                    return new JobApplicationInboundEmailViewModel();

                case InboundEmailCategory.Recommendation:
                    return new RecommendationInboundEmailViewModel();

                case InboundEmailCategory.DataOwnerInquiry:
                    return new DataOwnerInquiryInboundEmailViewModel();

                default:
                    throw new ArgumentOutOfRangeException(nameof(category));
            }
        }

        public static Type CreateViewModelType(InboundEmailCategory category)
        {
            switch (category)
            {
                case InboundEmailCategory.Unknown:
                    return typeof(UnknownInboundEmailViewModel);

                case InboundEmailCategory.Spam:
                    return typeof(SpamInboundEmailViewModel);

                case InboundEmailCategory.JobApplication:
                    return typeof(JobApplicationInboundEmailViewModel);

                case InboundEmailCategory.Recommendation:
                    return typeof(RecommendationInboundEmailViewModel);

                case InboundEmailCategory.DataOwnerInquiry:
                    return typeof(DataOwnerInquiryInboundEmailViewModel);

                default:
                    throw new ArgumentOutOfRangeException(nameof(category));
            }
        }
    }
}