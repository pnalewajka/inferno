﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [Identifier("Models.DataOwnerInquiryInboundEmailViewModel")]
    public class DataOwnerInquiryInboundEmailViewModel : InboundEmailViewModel
    {
        public DataOwnerInquiryInboundEmailViewModel()
        {
            Category = InboundEmailCategory.DataOwnerInquiry;
        }
    }
}