﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [Identifier("Models.UnknownInboundEmailViewModel")]
    public class UnknownInboundEmailViewModel : InboundEmailViewModel
    {
        public UnknownInboundEmailViewModel()
        {
            Category = InboundEmailCategory.Unknown;
        }
    }
}