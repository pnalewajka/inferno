﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class InboundEmailCloseViewModelToInboundEmailCloseDtoMapping : ClassMapping<InboundEmailCloseViewModel, InboundEmailCloseDto>
    {
        public InboundEmailCloseViewModelToInboundEmailCloseDtoMapping()
        {
            Mapping = v => new InboundEmailCloseDto
            {
                Id = v.Id,
                Reason = v.Reason,
                Comment = v.Comment
            };
        }
    }
}
