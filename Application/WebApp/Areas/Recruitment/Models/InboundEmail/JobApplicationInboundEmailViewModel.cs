﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Helpers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [Identifier("Models.JobApplicationInboundEmailViewModel")]
    [FieldSetDescription(1, ApplicationFieldset, nameof(JobApplicationResources.ApplicationFieldsetLabel), typeof(JobApplicationResources))]
    [FieldSetDescription(2, CandidateFieldset, nameof(JobApplicationResources.CandidateFieldsetLabel), typeof(JobApplicationResources))]
    [FieldSetDescription(3, ConsentFieldset, nameof(JobApplicationResources.ConsentFieldsetLabel), typeof(JobApplicationResources))]
    public class JobApplicationInboundEmailViewModel : InboundEmailViewModel
    {
        private const string CandidateFieldset = "candidate";
        private const string ApplicationFieldset = "application";
        private const string ConsentFieldset = "consent";

        public JobApplicationInboundEmailViewModel()
        {
            Category = InboundEmailCategory.JobApplication;
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldset)]
        [Tab(JobApplicationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.FirstName), typeof(CandidateResources))]
        [Trim]
        public string CandidateFirstName
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidateFirstName); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidateFirstName, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldset)]
        [Tab(JobApplicationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.LastName), typeof(CandidateResources))]
        [Trim]
        public string CandidateLastName
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidateLastName); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidateLastName, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldset)]
        [Tab(JobApplicationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.EmailAddress), typeof(CandidateResources))]
        [Trim]
        public string CandidateEmailAddress
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidateEmailAddress); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidateEmailAddress, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldset)]
        [Tab(JobApplicationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.MobilePhone), typeof(CandidateResources))]
        [Trim]
        public string CandidatePhoneNumber
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidatePhoneNumber); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidatePhoneNumber, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldset)]
        [Tab(JobApplicationTab)]
        [ValuePicker(Type = typeof(CandidateSearchController), OnResolveUrlJavaScript = "url += InboundEmail.getCandidateSearchParams()")]
        [DisplayNameLocalized(nameof(JobApplicationResources.MatchedCandidate), typeof(JobApplicationResources))]
        public long? CandidateId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.MatchingCandidateId); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.MatchingCandidateId, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(ApplicationFieldset)]
        [Tab(JobApplicationTab)]
        [ValuePicker(Type = typeof(ApplicationOriginPickerController), OnResolveUrlJavaScript = "url += '&skip-recommendation=true'")]
        [DisplayNameLocalized(nameof(JobApplicationResources.ApplicationOrigin), typeof(JobApplicationResources))]
        public long? ApplicationOriginId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.ApplicationOriginId); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.ApplicationOriginId, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(ApplicationFieldset)]
        [Tab(JobApplicationTab)]
        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(JobApplicationResources.CityIds), typeof(JobApplicationResources))]
        public long? CityId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.CityId); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.CityId, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(ApplicationFieldset)]
        [Tab(JobApplicationTab)]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [DisplayNameLocalized(nameof(JobApplicationResources.JobProfile), typeof(JobApplicationResources))]
        public long? JobProfileId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.JobProfileId); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.JobProfileId, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(ApplicationFieldset)]
        [Tab(JobApplicationTab)]
        [DisplayNameLocalized(nameof(JobApplicationResources.ReferenceNumber), typeof(JobApplicationResources))]
        [Trim]
        public string ReferenceNumber
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.ReferenceNumber); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.ReferenceNumber, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(ApplicationFieldset)]
        [Tab(JobApplicationTab)]
        [DisplayNameLocalized(nameof(JobApplicationResources.JobApplication), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(JobApplicationPickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name = " + nameof(CandidateId) + "]').val()")]
        [HiddenInput]
        public long? JobApplicationId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.JobApplicationId, null); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.JobApplicationId, value); }
        }
    }
}