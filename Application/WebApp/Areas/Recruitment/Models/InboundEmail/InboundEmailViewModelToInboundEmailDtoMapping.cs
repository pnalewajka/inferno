﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    public class InboundEmailViewModelToInboundEmailDtoMapping : ClassMapping<InboundEmailViewModel, InboundEmailDto>
    {
        public InboundEmailViewModelToInboundEmailDtoMapping(IClassMapping<InboundEmailValueViewModel, InboundEmailValueDto> inboundEmailValueViewModelToInboundEmailValueDtoMapping)
        {
            Mapping = v => new InboundEmailDto
            {
                Id = v.Id,
                To = v.To,
                FromEmailAddress = v.FromEmailAddress,
                FromFullName = v.FromFullName,
                Subject = v.Subject,
                Body = v.Body,
                BodyType = v.BodyType,
                SentOn = v.SentOn,
                ReceivedOn = v.ReceivedOn,
                Status = v.Status,
                ProcessedById = v.ProcessedById,
                Category = v.Category,
                Documents = v.Documents
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    })
                    .ToArray(),
                Values = v.Values.Select(inboundEmailValueViewModelToInboundEmailValueDtoMapping.CreateFromSource).ToList(),
                WorkflowAction = v.WorkflowAction,
                WorkflowProcessedById = v.WorkflowProcessedById,
                ClosedReason = v.ClosedReason,
                ClosedComment = v.ClosedComment,
                ResearchingComment = v.ResearchingComment,
                Timestamp = v.Timestamp
            };
        }
    }
}
