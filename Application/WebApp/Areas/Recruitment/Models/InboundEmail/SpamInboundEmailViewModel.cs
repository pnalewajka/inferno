﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [Identifier("Models.SpamInboundEmailViewModel")]
    public class SpamInboundEmailViewModel : InboundEmailViewModel
    {
        public SpamInboundEmailViewModel()
        {
            Category = InboundEmailCategory.Spam;
        }
    }
}