﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    public class InboundEmailDocumentDtoToInboundEmailDocumentViewModelMapping :
        ClassMapping<InboundEmailDocumentDto, InboundEmailDocumentViewModel>
    {
        public InboundEmailDocumentDtoToInboundEmailDocumentViewModelMapping()
        {
            Mapping = d => new InboundEmailDocumentViewModel
            {
                Id = d.DocumentId.Value,
                ContentType = d.ContentType,
                DocumentName = d.DocumentName,
                FromEmailAddress = d.FromEmailAddress,
                FromFullName = d.FromFullName,
                Subject = d.Subject,
                To = d.To
            };
        }
    }
}