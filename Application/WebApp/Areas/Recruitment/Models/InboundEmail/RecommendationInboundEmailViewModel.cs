﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Helpers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [Identifier("Models.RecommendationInboundEmailViewModel")]
    [FieldSetDescription(1, RecommendationFieldSet, nameof(RecommendationResources.RecommendationFieldsetLabel), typeof(RecommendationResources))]
    [FieldSetDescription(2, CandidateFieldSet, nameof(JobApplicationResources.CandidateFieldsetLabel), typeof(JobApplicationResources))]
    [FieldSetDescription(3, ConsentFieldSet, nameof(JobApplicationResources.ConsentFieldsetLabel), typeof(JobApplicationResources))]
    public class RecommendationInboundEmailViewModel : InboundEmailViewModel
    {
        private const string RecommendationFieldSet = "recommendation";
        private const string CandidateFieldSet = "candidate";
        private const string ConsentFieldSet = "consent";

        public RecommendationInboundEmailViewModel()
        {
            Category = InboundEmailCategory.Recommendation;
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(RecommendationFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(RecommendationResources.FirstName), typeof(RecommendationResources))]
        public string RecommendingPersonFirstName
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.RecommendingPersonFirstName); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.RecommendingPersonFirstName, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(RecommendationFieldSet)]
        [Trim]
        [Tab(RecommendationTab)]
        [DisplayNameLocalized(nameof(RecommendationResources.LastName), typeof(RecommendationResources))]
        public string RecommendingPersonLastName
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.RecommendingPersonLastName); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.RecommendingPersonLastName, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(RecommendationFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(RecommendationResources.EmailAddress), typeof(RecommendationResources))]
        public string RecommendingPersonEmailAddress
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.RecommendingPersonEmailAddress); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.RecommendingPersonEmailAddress, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(RecommendationFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(RecommendationResources.MobilePhone), typeof(RecommendationResources))]
        public string RecommendingPersonPhoneNumber
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.RecommendingPersonPhoneNumber); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.RecommendingPersonPhoneNumber, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(RecommendationFieldSet)]
        [Tab(RecommendationTab)]
        [ValuePicker(Type = typeof(RecommendingPersonSearchController), OnResolveUrlJavaScript = "url += InboundEmail.getRecommendingPersonSearchParams()")]
        [DisplayNameLocalized(nameof(RecommendationResources.MatchedRecommendingPerson), typeof(RecommendationResources))]
        public long? RecommendingPersonId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.MatchingRecommendingPersonId); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.MatchingRecommendingPersonId, value); }
        }

        [Tab(RecommendationTab)]
        [FieldSet(RecommendationFieldSet)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(InboundEmailResources.IsEligibleForReferralProgram), typeof(InboundEmailResources))]
        public bool IsEligibleForReferralProgram
        {
            get { return InboundEmailHelper.GetBool(Values, InboundEmailValueType.IsEligibleForReferralProgram); }
            set { InboundEmailHelper.SetBool(Values, InboundEmailValueType.IsEligibleForReferralProgram, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(CandidateResources.FirstName), typeof(CandidateResources))]
        public string CandidateFirstName
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidateFirstName); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidateFirstName, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(CandidateResources.LastName), typeof(CandidateResources))]
        public string CandidateLastName
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidateLastName); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidateLastName, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(CandidateResources.EmailAddress), typeof(CandidateResources))]
        public string CandidateEmailAddress
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidateEmailAddress); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidateEmailAddress, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldSet)]
        [Tab(RecommendationTab)]
        [Trim]
        [DisplayNameLocalized(nameof(CandidateResources.MobilePhone), typeof(CandidateResources))]
        public string CandidatePhoneNumber
        {
            get { return InboundEmailHelper.GetString(Values, InboundEmailValueType.CandidatePhoneNumber); }
            set { InboundEmailHelper.SetString(Values, InboundEmailValueType.CandidatePhoneNumber, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldSet)]
        [Tab(RecommendationTab)]
        [ValuePicker(Type = typeof(CandidateSearchController), OnResolveUrlJavaScript = "url += InboundEmail.getCandidateSearchParams()")]
        [DisplayNameLocalized(nameof(JobApplicationResources.MatchedCandidate), typeof(JobApplicationResources))]
        public long? CandidateId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.MatchingCandidateId); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.MatchingCandidateId, value); }
        }

        [Visibility(VisibilityScope.Form)]
        [FieldSet(CandidateFieldSet)]
        [Tab(RecommendationTab)]
        [DisplayNameLocalized(nameof(JobApplicationResources.JobApplication), typeof(JobApplicationResources))]
        [ValuePicker(Type = typeof(JobApplicationPickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name = " + nameof(CandidateId) + "]').val()")]
        [HiddenInput]
        public long? JobApplicationId
        {
            get { return InboundEmailHelper.GetNullableLong(Values, InboundEmailValueType.JobApplicationId, null); }
            set { InboundEmailHelper.SetNullableLong(Values, InboundEmailValueType.JobApplicationId, value); }
        }

        protected override bool CanBeAssignedBecauseOfCategory
        {
            get
            {
                return RecommendingPersonId != null;
            }
        }
    }
}