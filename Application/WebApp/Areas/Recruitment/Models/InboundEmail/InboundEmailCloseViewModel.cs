﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.InboundEmailCloseViewModel")]
    public class InboundEmailCloseViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(InboundEmailResources.ClosedReason), typeof(InboundEmailResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<InboundEmailClosedReason>))]
        [Render(ControlCssClass = "align-options-200")]
        public InboundEmailClosedReason? Reason { get; set; }

        [DisplayNameLocalized(nameof(InboundEmailResources.ClosedComment), typeof(InboundEmailResources))]
        [StringLength(255)]
        [Multiline(3)]
        [Trim]
        public string Comment { get; set; }
    }
}
