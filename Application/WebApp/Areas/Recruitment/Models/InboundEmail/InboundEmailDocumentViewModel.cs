﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.InboundEmail
{
    [Identifier("Models.InboundEmailDocumentViewModel")]
    public class InboundEmailDocumentViewModel
    {
        public long Id { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(InboundEmailResources.DocumentName), typeof(InboundEmailResources))]
        [Trim]
        public string DocumentName { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(InboundEmailResources.ContentType), typeof(InboundEmailResources))]
        public string ContentType { get; set; }

        public string To { get; set; }

        public string FromEmailAddress { get; set; }

        public string FromFullName { get; set; }

        public string Subject { get; set; }
        
        public string InboundEmailLabel => string.IsNullOrWhiteSpace(FromFullName)
                ? $"{FromEmailAddress} ({Subject})"
                : $"{FromFullName} [{FromEmailAddress}] ({Subject})";

        public override string ToString()
        {
            return $"{DocumentName}-{InboundEmailLabel}";
        }
    }
}