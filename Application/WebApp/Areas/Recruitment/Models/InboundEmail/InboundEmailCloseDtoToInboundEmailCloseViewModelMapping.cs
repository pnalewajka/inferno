﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class InboundEmailCloseDtoToInboundEmailCloseViewModelMapping : ClassMapping<InboundEmailCloseDto, InboundEmailCloseViewModel>
    {
        public InboundEmailCloseDtoToInboundEmailCloseViewModelMapping()
        {
            Mapping = d => new InboundEmailCloseViewModel
            {
                Id = d.Id,
                Reason = d.Reason,
                Comment = d.Comment
            };
        }
    }
}
