﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RejectedCandidateReportViewModelToRejectedCandidateReportDtoMapping : ClassMapping<RejectedCandidateReportParametersViewModel, RejectedCandidateReportParametersDto>
    {
        public RejectedCandidateReportViewModelToRejectedCandidateReportDtoMapping()
        {
            Mapping = v => new RejectedCandidateReportParametersDto
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                OrgUnitId = v.OrgUnitId,
                RecruiterIds = v.RecruiterIds,
                RejectingEmployeeIds = v.RejectingEmployeeIds,
                RejectionReason = v.RejectionReason
            };
        }
    }
}