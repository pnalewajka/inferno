﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class HiredCandidateReportParametersViewModelToHiredCandidateReportParametersDtoMapping : ClassMapping<HiredCandidateReportParametersViewModel, HiredCandidateReportParametersDto>
    {
        public HiredCandidateReportParametersViewModelToHiredCandidateReportParametersDtoMapping()
        {
            Mapping = v => new HiredCandidateReportParametersDto
            {
                CityIds = v.CityIds,
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                JobProfileIds = v.JobProfileIds,
                RecruiterIds = v.RecruiterIds
            };
        }
    }
}