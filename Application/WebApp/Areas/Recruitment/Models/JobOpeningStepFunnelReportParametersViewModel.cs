﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.JobOpeningStepFunnelReportParameters")]
    public class JobOpeningStepFunnelReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.DecisionMakerIds), typeof(ReportResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=reporting-hiring-managers'")]
        public List<long> DecisionMakerIds { get; set; }
    }
}