﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessInfoDtoToRecruitmentProcessInfoViewModelMapping : ClassMapping<RecruitmentProcessInfoDto, RecruitmentProcessInfoViewModel>
    {
        public RecruitmentProcessInfoDtoToRecruitmentProcessInfoViewModelMapping()
        {
            Mapping = d => new RecruitmentProcessInfoViewModel
            {
                Id = d.Id,
                RecruiterFullName = d.RecruiterFullName,
                PositionName = d.PositionName,
                RecruiterId = d.RecruiterId,
                LastActivityOn = d.LastActivityOn,
                LastActivityBy = d.LastActivityBy
            };
        }
    }
}