﻿using System;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.JobOpeningStatusReportParameters")]
    public class JobOpeningStatusReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.JobOpeningStatuses), typeof(ReportResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<JobOpeningStatus>))]
        public JobOpeningStatus[] JobOpeningStatuses { get; set; }
    }
}