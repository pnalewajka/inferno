﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class HiredCandidateReportParametersDtoToHiredCandidateReportParametersViewModelMapping : ClassMapping<HiredCandidateReportParametersDto, HiredCandidateReportParametersViewModel>
    {
        public HiredCandidateReportParametersDtoToHiredCandidateReportParametersViewModelMapping()
        {
            Mapping = v => new HiredCandidateReportParametersViewModel
            {
                CityIds = v.CityIds,
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                JobProfileIds = v.JobProfileIds,
                RecruiterIds = v.RecruiterIds
            };
        }
    }
}