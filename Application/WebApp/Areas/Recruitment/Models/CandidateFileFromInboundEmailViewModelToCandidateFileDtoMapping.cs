﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;


namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class CandidateFileFromInboundEmailViewModelToCandidateFileDtoMapping : ClassMapping<CandidateFileFromInboundEmailViewModel, CandidateFileDto>
    {
        public CandidateFileFromInboundEmailViewModelToCandidateFileDtoMapping()
        {
            Mapping = v => new CandidateFileDto
            {
                Id = v.Id,
                CandidateId = v.CandidateId,
                DocumentType = v.DocumentType,
                Comment = v.Comment,
                File = new[]
                {
                    new DocumentDto
                    {
                        DocumentId = v.InboundEmailDocumentId
                    }
                },
                FileSource = FileSource.InboundEmailAttachment,
                Timestamp = v.Timestamp
            };
        }
    }
}