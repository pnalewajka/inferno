﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessDtoToRecruitmentProcessQuickAddViewModelMapping : ClassMapping<RecruitmentProcessDto, RecruitmentProcessQuickAddViewModel>
    {
        public RecruitmentProcessDtoToRecruitmentProcessQuickAddViewModelMapping()
        {
            Mapping = d => new RecruitmentProcessQuickAddViewModel
            {
                CandidateId = d.CandidateId,
                JobOpeningId = d.JobOpeningId,
                RecruiterId = d.RecruiterId,
                InitialSteps = d.InitialSteps,
                Status = d.Status
            };
        }
    }
}