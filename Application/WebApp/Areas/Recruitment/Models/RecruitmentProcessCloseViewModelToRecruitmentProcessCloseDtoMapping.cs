﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessCloseViewModelToRecruitmentProcessCloseDtoMapping : ClassMapping<RecruitmentProcessCloseViewModel, RecruitmentProcessCloseDto>
    {
        public RecruitmentProcessCloseViewModelToRecruitmentProcessCloseDtoMapping()
        {
            Mapping = v => new RecruitmentProcessCloseDto
            {
                Id = v.Id,
                Reason = v.Reason,
                Comment = v.Comment
            };
        }
    }
}
