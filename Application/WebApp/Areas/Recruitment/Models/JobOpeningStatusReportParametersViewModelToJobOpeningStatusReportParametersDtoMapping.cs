﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class JobOpeningStatusReportParametersViewModelToJobOpeningStatusReportParametersDtoMapping
         : ClassMapping<JobOpeningStatusReportParametersViewModel, JobOpeningStatusReportParametersDto>
    {
        public JobOpeningStatusReportParametersViewModelToJobOpeningStatusReportParametersDtoMapping()
        {
            Mapping = v => new JobOpeningStatusReportParametersDto
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                JobOpeningStatuses = v.JobOpeningStatuses
            };
        }
    }
}