﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessCloseDtoToRecruitmentProcessCloseViewModelMapping : ClassMapping<RecruitmentProcessCloseDto, RecruitmentProcessCloseViewModel>
    {
        public RecruitmentProcessCloseDtoToRecruitmentProcessCloseViewModelMapping()
        {
            Mapping = d => new RecruitmentProcessCloseViewModel
            {
                Id = d.Id,
                Reason = d.Reason,
                Comment = d.Comment
            };
        }
    }
}
