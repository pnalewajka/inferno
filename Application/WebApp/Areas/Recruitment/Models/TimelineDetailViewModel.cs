﻿using System;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class TimelineDetailViewModel
    {
        public string Header { get; set; }

        public string Details { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime HappenedOn { get; set; }

        public bool RenderAsHtml { get; set; }
    }
}