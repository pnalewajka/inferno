﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecommendingPersonContactRecordViewModelToRecommendingPersonContactRecordDtoMapping : ClassMapping<RecommendingPersonContactRecordViewModel, RecommendingPersonContactRecordDto>
    {
        public RecommendingPersonContactRecordViewModelToRecommendingPersonContactRecordDtoMapping()
        {
            Mapping = v => new RecommendingPersonContactRecordDto
            {
                Id = v.Id,
                RecommendingPersonId = v.RecommendingPersonId,
                ContactType = v.ContactType,
                ContactedOn = v.ContactedOn,
                ContactedByEmployeeId = v.ContactedById,
                Comment = v.Comment,
                Timestamp = v.Timestamp
            };
        }
    }
}
