﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecommendingPersonNoteDtoToRecommendingPersonNoteViewModelMapping : ClassMapping<RecommendingPersonNoteDto, RecommendingPersonNoteViewModel>
    {
        public RecommendingPersonNoteDtoToRecommendingPersonNoteViewModelMapping()
        {
            Mapping = d => new RecommendingPersonNoteViewModel
            {
                Id = d.Id,
                RecommendingPersonId = d.RecommendingPersonId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content != null ? MentionItemHelper.ReplaceTokens(d.Content, i => i.Label) : string.Empty,
                RichContent = d.Content,
                CreatedOn = d.CreatedOn,
                CreatedByFullName = d.CreatedByFullName,
                Timestamp = d.Timestamp
            };
        }
    }
}
