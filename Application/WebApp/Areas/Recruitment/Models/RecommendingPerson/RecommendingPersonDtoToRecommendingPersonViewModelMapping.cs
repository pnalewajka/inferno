﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecommendingPersonDtoToRecommendingPersonViewModelMapping : ClassMapping<RecommendingPersonDto, RecommendingPersonViewModel>
    {
        public RecommendingPersonDtoToRecommendingPersonViewModelMapping(IClassMappingFactory mappingFactory)
        {
            Mapping = d => new RecommendingPersonViewModel
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                EmailAddress = d.EmailAddress,
                PhoneNumber = d.PhoneNumber,
                IsAnonymized = d.IsAnonymized,
                CoordinatorId = d.CoordinatorId,
                ModifiedOn = d.ModifiedOn,
                HasValidConsent = d.HasValidConsent,
                Notes = d.Notes != null ? d.Notes.OrderByDescending(n => n.CreatedOn).Select(mappingFactory.CreateMapping<RecommendingPersonNoteDto, RecommendingPersonNoteViewModel>().CreateFromSource).ToList() : null,
                CreatedByFullName = d.CreatedByFullName,
                CreatedOn = d.CreatedOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
