﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecommendingPerson
{
    public class RecommendingPersonDtoToRecommendingPersonSearchViewModelMapping : ClassMapping<RecommendingPersonDto, RecommendingPersonSearchViewModel>
    {
        public RecommendingPersonDtoToRecommendingPersonSearchViewModelMapping()
        {
            Mapping = d => new RecommendingPersonSearchViewModel
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                EmailAddress = d.EmailAddress,
                PhoneNumber = d.PhoneNumber,
                CoordinatorId = d.CoordinatorId,
                ModifiedOn = d.ModifiedOn,
                HasValidConsent = d.HasValidConsent,
                Timestamp = d.Timestamp
            };
        }
    }
}