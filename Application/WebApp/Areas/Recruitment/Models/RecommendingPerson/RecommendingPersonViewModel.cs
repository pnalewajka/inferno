﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [TabDescription(0, InformationTab, nameof(RecruitmentProcessResources.InformationTabText), typeof(RecruitmentProcessResources))]
    [TabDescription(1, NoteTab, nameof(RecruitmentProcessResources.NoteTabText), typeof(RecruitmentProcessResources))]
    [Identifier("Models.RecommendingPersonViewModel")]
    public class RecommendingPersonViewModel : IGridViewRow
    {
        private const string InformationTab = "information";
        private const string NoteTab = "notes";

        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Order(1)]
        [Required]
        [MaxLength(255)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.FirstName), typeof(RecommendingPersonResources))]
        public string FirstName { get; set; }

        [Order(2)]
        [Required]
        [MaxLength(255)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.LastName), typeof(RecommendingPersonResources))]
        public string LastName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string FullName { get { return $"{FirstName} {LastName}".Trim(); } }

        [Order(3)]
        [Required]
        [MaxLength(255)]
        [EmailAddress]
        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.EmailAddress), typeof(RecommendingPersonResources))]
        public string EmailAddress { get; set; }

        [Order(4)]
        [SortBy(nameof(EmailAddress))]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.EmailAddress), typeof(RecommendingPersonResources))]
        public DisplayUrl GetEmailAddress(UrlHelper helper) => CellContentHelper.GetEmailLink(EmailAddress, "recommending-person-record-contact-email");

        [Order(5)]
        [MaxLength(20)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.PhoneNumber), typeof(RecommendingPersonResources))]
        public string PhoneNumber { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.CoordinatorId), typeof(RecommendingPersonResources))]
        [Required]
        [Tab(InformationTab)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public long CoordinatorId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.CreatedByFullName), typeof(RecommendingPersonResources))]
        [ReadOnly(true)]
        public string CreatedByFullName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.CreatedOn), typeof(RecommendingPersonResources))]
        [ReadOnly(true)]
        public DateTime CreatedOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.ShouldAddRelatedDataConsent), typeof(RecommendingPersonResources))]
        public bool ShouldAddRelatedDataConsent { get; set; }

        [Visibility(VisibilityScope.None)]
        [ReadOnly(true)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.IsAnonymized), typeof(RecommendingPersonResources))]
        public bool IsAnonymized { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(NoteTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RecommendingPersonNote")]
        public List<RecommendingPersonNoteViewModel> Notes { get; set; }

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasValidConsent { get; set; }

        public byte[] Timestamp { get; set; }

        public RecommendingPersonViewModel()
        {
            ShouldAddRelatedDataConsent = true;
        }

        public override string ToString()
        {
            return $"{FullName}";
        }

        string IGridViewRow.RowCssClass => HasValidConsent ? null : CardIndexStyles.RowClassDanger;

        string IGridViewRow.RowTitle => HasValidConsent ? null : RecommendingPersonResources.HasNoValidConsents;
    }
}
