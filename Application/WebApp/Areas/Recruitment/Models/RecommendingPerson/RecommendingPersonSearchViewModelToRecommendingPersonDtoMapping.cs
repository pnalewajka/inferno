﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecommendingPerson
{
    public class RecommendingPersonSearchViewModelToRecommendingPersonDtoMapping : ClassMapping<RecommendingPersonSearchViewModel, RecommendingPersonDto>
    {
        public RecommendingPersonSearchViewModelToRecommendingPersonDtoMapping()
        {
            Mapping = v => new RecommendingPersonDto
            {
                Id = v.Id,
                FirstName = v.FirstName,
                LastName = v.LastName,
                EmailAddress = v.EmailAddress,
                PhoneNumber = v.PhoneNumber,
                CoordinatorId = v.CoordinatorId,
                ShouldAddRelatedDataConsent = v.ShouldAddRelatedDataConsent,
                Timestamp = v.Timestamp
            };
        }
    }
}