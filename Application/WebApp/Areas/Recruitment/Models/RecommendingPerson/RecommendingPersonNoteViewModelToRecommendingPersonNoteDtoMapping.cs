﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecommendingPersonNoteViewModelToRecommendingPersonNoteDtoMapping : ClassMapping<RecommendingPersonNoteViewModel, RecommendingPersonNoteDto>
    {
        public RecommendingPersonNoteViewModelToRecommendingPersonNoteDtoMapping()
        {
            Mapping = v => new RecommendingPersonNoteDto
            {
                Id = v.Id,
                RecommendingPersonId = v.RecommendingPersonId,
                NoteType = v.NoteType,
                NoteVisibility = v.NoteVisibility,
                Content = v.RichContent,
                Timestamp = v.Timestamp
            };
        }
    }
}
