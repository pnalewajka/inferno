﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecommendingPersonContactRecordDtoToRecommendingPersonContactRecordViewModelMapping : ClassMapping<RecommendingPersonContactRecordDto, RecommendingPersonContactRecordViewModel>
    {
        public RecommendingPersonContactRecordDtoToRecommendingPersonContactRecordViewModelMapping()
        {
            Mapping = d => new RecommendingPersonContactRecordViewModel
            {
                Id = d.Id,
                RecommendingPersonId = d.RecommendingPersonId,
                ContactType = d.ContactType,
                ContactedOn = d.ContactedOn,
                ContactedById = d.ContactedByEmployeeId,
                Comment = d.Comment,
                Timestamp = d.Timestamp
            };
        }
    }
}
