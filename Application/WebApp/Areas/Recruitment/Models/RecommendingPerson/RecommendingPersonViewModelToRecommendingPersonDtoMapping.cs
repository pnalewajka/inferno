﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecommendingPersonViewModelToRecommendingPersonDtoMapping : ClassMapping<RecommendingPersonViewModel, RecommendingPersonDto>
    {
        public RecommendingPersonViewModelToRecommendingPersonDtoMapping()
        {
            Mapping = v => new RecommendingPersonDto
            {
                Id = v.Id,
                FirstName = v.FirstName,
                LastName = v.LastName,
                EmailAddress = v.EmailAddress,
                PhoneNumber = v.PhoneNumber,
                IsAnonymized = v.IsAnonymized,
                CoordinatorId = v.CoordinatorId,
                ShouldAddRelatedDataConsent = v.ShouldAddRelatedDataConsent,
                Timestamp = v.Timestamp
            };
        }
    }
}
