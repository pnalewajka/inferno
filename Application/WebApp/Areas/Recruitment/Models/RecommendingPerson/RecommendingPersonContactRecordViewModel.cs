﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RecommendingPersonContactRecordViewModel")]
    public class RecommendingPersonContactRecordViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecommendingPersonContactRecordResources.RecommendingPersonId), typeof(RecommendingPersonContactRecordResources))]
        [ValuePicker(Type = typeof(RecommendingPersonPickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=true'")]
        [Render(GridCssClass = "no-break-column", Size = Size.Medium)]
        [Required]
        [Order(1)]
        public long RecommendingPersonId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecommendingPersonContactRecordResources.Comment), typeof(RecommendingPersonContactRecordResources))]
        [Multiline(4)]
        [Render(GridCssClass = "remaining-space-column")]
        [Order(2)]
        [NonSortable]
        public string Comment { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecommendingPersonContactRecordResources.ContactType), typeof(RecommendingPersonContactRecordResources))]
        [Render(GridCssClass = "no-break-column", Size = Size.Medium)]
        [RequiredEnum(typeof(ContactType))]
        [Order(3)]
        public ContactType ContactType { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecommendingPersonContactRecordResources.ContactedOn), typeof(RecommendingPersonContactRecordResources))]
        [TimePicker(15)]
        [Render(GridCssClass = "no-break-column")]
        [Order(4)]
        public DateTime ContactedOn { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecommendingPersonContactRecordResources.ContactedByEmployeeId), typeof(RecommendingPersonContactRecordResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified'")]
        [Required]
        [Render(GridCssClass = "no-break-column")]
        [Order(5)]
        public long ContactedById { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
