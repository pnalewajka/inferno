﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RecommendingPersonNoteViewModel")]
    public class RecommendingPersonNoteViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(RecommendingPersonPickerController))]
        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.RecommendingPerson), typeof(RecommendingPersonNoteResources))]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public long RecommendingPersonId { get; set; }

        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.Content), typeof(RecommendingPersonNoteResources))]
        [StringLength(5000)]
        [Render(GridCssClass = "remaining-space-column")]
        [Multiline(3)]
        [AllowHtml]
        [RichText]
        [NonSortable]
        public string Content { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.Content), typeof(RecommendingPersonNoteResources))]
        [StringLength(5000)]
        [Required]
        [NonSortable]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RichNoteControl")]
        [AllowHtml]
        [HtmlSanitize]
        public string RichContent { get; set; }

        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.NoteType), typeof(RecommendingPersonNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteType NoteType { get; set; }

        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.NoteVisibility), typeof(RecommendingPersonNoteResources))]
        [RequiredEnum(typeof(NoteVisibility))]
        [RadioGroup(ItemProvider = typeof(BasicNoteVisibilityItemProvider))]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteVisibility NoteVisibility { get; set; }

        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.CreatedOn), typeof(RecommendingPersonNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public DateTime CreatedOn { get; set; }

        [DisplayNameLocalized(nameof(RecommendingPersonNoteResources.CreatedBy), typeof(RecommendingPersonNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public string CreatedByFullName { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString() => Content.Length > 50 ? $"{Content.Substring(0, 50)}…" : Content;
    }
}
