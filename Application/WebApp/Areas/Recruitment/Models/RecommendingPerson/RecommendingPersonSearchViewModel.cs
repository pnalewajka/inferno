﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.RecommendingPerson
{
    public class RecommendingPersonSearchViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [SortBy(nameof(FullName))]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.FullName), typeof(RecommendingPersonResources))]
        public DisplayUrl GetFullName(UrlHelper helper)
        {
            return new DisplayUrl
            {
                Label = FullName,
                Action = helper.UriActionGet<CandidateController>(c => c.View(default(long)), KnownParameter.None, nameof(Id), Id.ToString())
            };
        }

        [Order(1)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.FirstName), typeof(RecommendingPersonResources))]
        [MaxLength(255)]
        public string FirstName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.LastName), typeof(RecommendingPersonResources))]
        [MaxLength(255)]
        public string LastName { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.PhoneNumber), typeof(RecommendingPersonResources))]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.EmailAddress), typeof(RecommendingPersonResources))]
        [MaxLength(255)]
        public string EmailAddress { get; set; }

        [Order(5)]
        [SortBy(nameof(EmailAddress))]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.EmailAddress), typeof(RecommendingPersonResources))]
        public DisplayUrl GetEmailAddress(UrlHelper helper) => CellContentHelper.GetEmailLink(EmailAddress, "recommending-person-record-contact-email");

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecommendingPersonResources.CoordinatorId), typeof(RecommendingPersonResources))]
        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public long CoordinatorId { get; set; }

        [Visibility(VisibilityScope.Form)]
        public bool ShouldAddRelatedDataConsent { get; set; }

        [Visibility(VisibilityScope.None)]
        public string FullName => $"{FirstName} {LastName}";

        [Visibility(VisibilityScope.None)]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasValidConsent { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString() => FullName;
    }
}