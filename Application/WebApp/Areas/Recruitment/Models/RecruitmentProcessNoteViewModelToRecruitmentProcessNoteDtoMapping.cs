﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessNoteViewModelToRecruitmentProcessNoteDtoMapping : ClassMapping<RecruitmentProcessNoteViewModel, RecruitmentProcessNoteDto>
    {
        public RecruitmentProcessNoteViewModelToRecruitmentProcessNoteDtoMapping()
        {
            Mapping = v => new RecruitmentProcessNoteDto
            {
                Id = v.Id,
                RecruitmentProcessId = v.RecruitmentProcessId,
                NoteType = v.NoteType,
                NoteVisibility = v.NoteVisibility,
                Content = v.RichContent,
                RecruitmentProcessStepId = v.RecruitmentProcessStepId,
                Timestamp = v.Timestamp
            };
        }
    }
}
