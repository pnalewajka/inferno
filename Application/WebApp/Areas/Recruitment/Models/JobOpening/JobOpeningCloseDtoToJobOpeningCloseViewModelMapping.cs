﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningCloseDtoToJobOpeningCloseViewModelMapping : ClassMapping<JobOpeningCloseDto, JobOpeningCloseViewModel>
    {
        public JobOpeningCloseDtoToJobOpeningCloseViewModelMapping()
        {
            Mapping = v => new JobOpeningCloseViewModel
            {
                Id = v.JobOpeningId,
                ClosedComment = v.ClosedComment,
                ClosedReason = v.ClosedReason
            };
        }
    }
}