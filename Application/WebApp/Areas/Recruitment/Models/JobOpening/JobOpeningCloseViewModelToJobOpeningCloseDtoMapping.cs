﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningCloseViewModelToJobOpeningCloseDtoMapping : ClassMapping<JobOpeningCloseViewModel, JobOpeningCloseDto>
    {
        public JobOpeningCloseViewModelToJobOpeningCloseDtoMapping()
        {
            Mapping = v => new JobOpeningCloseDto
            {
                JobOpeningId = v.Id,
                ClosedComment = v.ClosedComment,
                ClosedReason = v.ClosedReason
            };
        }
    }
}