﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningNoteViewModelToJobOpeningNoteDtoMapping : ClassMapping<JobOpeningNoteViewModel, JobOpeningNoteDto>
    {
        public JobOpeningNoteViewModelToJobOpeningNoteDtoMapping()
        {
            Mapping = v => new JobOpeningNoteDto
            {
                Id = v.Id,
                JobOpeningId = v.JobOpeningId,
                NoteType = v.NoteType,
                NoteVisibility = v.NoteVisibility,
                Content = v.RichContent,
                Timestamp = v.Timestamp
            };
        }
    }
}
