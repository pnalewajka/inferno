﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.ModelBinders;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningViewModelBinder : AtomicModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var hiringModePropertyName = PropertyHelper.GetPropertyName<JobOpeningViewModel>(t => t.HiringMode);
            var hiringModeParameterName = NamingConventionHelper.ConvertPascalCaseToHyphenated(hiringModePropertyName);
            var value = bindingContext.ValueProvider.GetValue(hiringModeParameterName) ?? bindingContext.ValueProvider.GetValue(hiringModePropertyName);

            if (value != null)
            {
                var pascalCaseValue = NamingConventionHelper.ConvertHyphenatedToPascalCase(value.AttemptedValue);
                var hiringMode = EnumHelper.GetEnumValue<HiringMode>(pascalCaseValue);
                var viewModelType = JobOpeningDtoToJobOpeningViewModelMapping.GetViewModelType(hiringMode);

                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, viewModelType);
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}