﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.Sales.Controllers;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    [ModelBinder(typeof(JobOpeningViewModelBinder))]
    public class JobOpeningViewModel : IGridViewRow, IValidatableObject
    {
        protected const string GeneralTab = "General";
        protected const string DemandTab = "Demand";
        protected const string PositionTab = "Position";
        protected const string ProcessParametersTab = "ProcessParameters";
        protected const string ProjectDescriptionTab = "ProjectDescription";
        protected const string ContributorsTab = "Contributors";
        protected const string NoteTab = "Notes";
        protected const string TimelineTab = "timeline";
        protected const string RecruitmentProcessesTab = "RecruitmentProcesses";

        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.HiringModeLabel), nameof(JobOpeningResources.HiringModeShortLabel), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [ReadOnly(true)]
        [Render(Size = Size.Small)]
        public virtual HiringMode HiringMode { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.PositionNameLabel), nameof(JobOpeningResources.PositionNameShortLabel), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [Required]
        [StringLength(400)]
        public string PositionName { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.JobProfileIds), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public List<long> JobProfileIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public List<string> JobProfileNames { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.CityIds), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [ValuePicker(Type = typeof(CityPickerController))]
        public List<long> CityIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public List<string> CityNames { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=hiring-managers'")]
        [DisplayNameLocalized(nameof(JobOpeningResources.DecisionMaker), typeof(JobOpeningResources))]
        [Render(Size = Size.Large)]
        [Tab(GeneralTab)]
        public long DecisionMakerId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [Tab(GeneralTab)]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(JobOpeningResources.OrgUnitId), typeof(JobOpeningResources))]
        public long OrgUnitId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(GeneralTab)]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        [DisplayNameLocalized(nameof(JobOpeningResources.Company), typeof(JobOpeningResources))]
        public long CompanyId { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.AccountId), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [ValuePicker(Type = typeof(CustomerPickerController))]
        public virtual long? CustomerId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string CustomerName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyTotal), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Render(GridCssClass = "numeric-column")]
        public int VacancyTotal { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.Priority), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [Range(0, 2)]
        [Render(GridCssClass = "numeric-column")]
        public int Priority { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.Status), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [Render(Size = Size.Medium)]
        public JobOpeningStatus Status { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.RoleOpenedOn), typeof(JobOpeningResources))]
        [Tab(GeneralTab)]
        [StringFormat("{0:d}")]
        [ReadOnly(true)]
        [Render(GridCssClass = "date-column no-break-column")]
        public DateTime? RoleOpenedOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyLevel1), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Range(0, 50)]
        public int VacancyLevel1 { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyLevel2), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Range(0, 50)]
        public int VacancyLevel2 { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyLevel3), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Range(0, 50)]
        public int VacancyLevel3 { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyLevel4), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Range(0, 50)]
        public int VacancyLevel4 { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyLevel5), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Range(0, 50)]
        public int VacancyLevel5 { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.VacancyLevelNotApplicable), typeof(JobOpeningResources))]
        [Tab(DemandTab)]
        [Range(0, 50)]
        public int VacancyLevelNotApplicable { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.NoticePeriod), typeof(JobOpeningResources))]
        [Tab(PositionTab)]
        [StringLength(120)]
        public string NoticePeriod { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.SalaryRate), typeof(JobOpeningResources))]
        [Tab(PositionTab)]
        [StringLength(130)]
        public string SalaryRate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.IsPolishSpeakerRequired), typeof(JobOpeningResources))]
        [Tab(PositionTab)]
        public bool IsPolishSpeakerRequired { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.EnglishLevel), typeof(JobOpeningResources))]
        [Tab(PositionTab)]
        public LanguageReferenceLevel EnglishLevel { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.OtherLanguages), typeof(JobOpeningResources))]
        [Tab(PositionTab)]
        [StringLength(100)]
        public string OtherLanguages { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.IsHighlyConfidential), typeof(JobOpeningResources))]
        [Tab(ProcessParametersTab)]
        public bool IsHighlyConfidential { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.IsTechnicalVerificationRequired), typeof(JobOpeningResources))]
        [Tab(ProcessParametersTab)]
        public bool IsTechnicalVerificationRequired { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.TechnicalReviewComment), typeof(JobOpeningResources))]
        [Tab(ProcessParametersTab)]
        [StringLength(300)]
        [Multiline(3)]
        public string TechnicalReviewComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.IsIntiveResumeRequired), typeof(JobOpeningResources))]
        [Tab(ProcessParametersTab)]
        public bool IsIntiveResumeRequired { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.AgencyEmploymentMode), typeof(JobOpeningResources))]
        [Tab(ProcessParametersTab)]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent, ItemProvider = typeof(EnumBasedListItemProvider<AgencyEmploymentMode>))]
        public AgencyEmploymentMode? AgencyEmploymentMode { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(ProjectDescriptionTab)]
        [DisplayNameLocalized(nameof(JobOpeningResources.ProjectDescriptionList), typeof(JobOpeningResources))]
        [Repeater(nameof(JobOpeningResources.ProjectDescriptionListItem), typeof(JobOpeningResources),
            CanAddRecord = true,
            AddButtonText = nameof(JobOpeningResources.ProjectDescriptionAddButtonText),
            CanDeleteRecord = true,
            DeleteButtonText = nameof(JobOpeningResources.ProjectDescriptionDeleteButtonText),
            CanReorder = true,
            MinCount = 0,
            MaxCount = 5)]
        public virtual List<ProjectDescriptionViewModel> ProjectDescriptions { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.RecruiterIds), typeof(JobOpeningResources))]
        [Tab(ContributorsTab)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public List<long> RecruiterIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.RecruitmentOwnerIds), typeof(JobOpeningResources))]
        [Tab(ContributorsTab)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        public List<long> RecrutmentOwnerIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(ContributorsTab)]
        [DisplayNameLocalized(nameof(JobOpeningResources.WatcherIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-watcher'")]
        public List<long> WatcherIds { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.CreatedBy), typeof(JobOpeningResources))]
        [Tab(ContributorsTab)]
        public string CreatedByFullName { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.CreatedOn), typeof(JobOpeningResources))]
        [Tab(ContributorsTab)]
        public DateTime CreatedOn { get; set; }

        [ReadOnly(true)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningResources.AcceptingEmployeeId), typeof(JobOpeningResources))]
        [Tab(ContributorsTab)]
        public string AcceptingEmployeeFullName { get; set; }

        [Tab(RecruitmentProcessesTab)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateRecruitmentProcesses")]
        public object Processes { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public JobOpeningWorkflowAction? WorkflowAction { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public string WorkflowClosedComment { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public JobOpeningClosedReason? WorkflowClosedReason { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(GeneralTab)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(JobOpeningResources.RejectionReasonLabel), typeof(JobOpeningResources))]
        public string RejectionReason { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(NoteTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.JobOpeningNotes")]
        public List<JobOpeningNoteViewModel> Notes { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(TimelineTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.HistoryEntry")]
        public List<HistoryEntryViewModel> HistoryEntries { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public string WorkflowRejectionReason { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeApprovedByCurrentUser { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsWatchedByCurrentUser { get; set; }

        [ReadOnly(true)]
        [Tab(GeneralTab)]
        [Visibility(VisibilityScope.Form)]
        public string ClosedComment { get; set; }

        [ReadOnly(true)]
        [Tab(GeneralTab)]
        [Visibility(VisibilityScope.Form)]
        public JobOpeningClosedReason? ClosedReason { get; set; }

        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid? CrmId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ProjectDescriptionTabLabel => $"{JobOpeningResources.ProjectDescriptionsTabText} ({ProjectDescriptions?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string NoteTabLabel => $"{JobOpeningResources.NoteTabText} ({Notes?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string TimelineTabLabel => $"{JobOpeningResources.TimelineTabText} ({HistoryEntries?.Count ?? 0})";

        public JobOpeningViewModel()
        {
            ProjectDescriptions = new List<ProjectDescriptionViewModel>();
            ProjectDescriptions.Add(new ProjectDescriptionViewModel());
            RecrutmentOwnerIds = new List<long>();
            RecruiterIds = new List<long>();
            WatcherIds = new List<long>();
            Notes = new List<JobOpeningNoteViewModel>();
            HistoryEntries = new List<HistoryEntryViewModel>();
        }

        public override string ToString()
        {
            return $"{PositionName}";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!JobProfileIds.Any())
            {
                yield return new ValidationResult(JobOpeningResources.InvalidJobProfiles);
            }
        }

        string IGridViewRow.RowTitle
        {
            get
            {
                switch (Status)
                {
                    case JobOpeningStatus.Draft:
                        return JobOpeningResources.RowTitleDraft;

                    case JobOpeningStatus.PendingApproval:
                        return JobOpeningResources.RowTitlePendingApproval;

                    case JobOpeningStatus.Rejected:
                        return JobOpeningResources.RowTitleRejected;

                    default:
                        return null;
                }
            }
        }

        string IGridViewRow.RowCssClass
        {
            get
            {
                switch (Status)
                {
                    case JobOpeningStatus.Draft:
                        return CardIndexStyles.RowClassWarning;

                    case JobOpeningStatus.PendingApproval:
                        return CardIndexStyles.RowClassWarning;

                    case JobOpeningStatus.Rejected:
                        return CardIndexStyles.RowClassDanger;

                    default:
                        return null;
                }
            }
        }
    }
}
