﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    [TabDescription(0, GeneralTab, nameof(JobOpeningResources.GeneralTabText), typeof(JobOpeningResources))]
    [TabDescription(1, DemandTab, nameof(JobOpeningResources.DemandTabText), typeof(JobOpeningResources))]
    [TabDescription(2, PositionTab, nameof(JobOpeningResources.PositionTabText), typeof(JobOpeningResources))]
    [TabDescription(3, ProjectDescriptionTab, nameof(ProjectDescriptionTabLabel))]
    [TabDescription(4, ProcessParametersTab, nameof(JobOpeningResources.ProcessParametersTabText), typeof(JobOpeningResources))]
    [TabDescription(5, NoteTab, nameof(NoteTabLabel), SecurityRoleType.CanViewRecruitmentJobOpeningNotes)]
    [TabDescription(6, TimelineTab, nameof(TimelineTabLabel), SecurityRoleType.CanViewJobOpeningHistory)]
    [TabDescription(7, ContributorsTab, nameof(JobOpeningResources.ContributorsTabText), typeof(JobOpeningResources))]
    [TabDescription(8, RecruitmentProcessesTab, nameof(JobOpeningResources.RecruitmentProcessTabLabel), typeof(JobOpeningResources))]
    public sealed class ProjectJobOpeningViewModel : JobOpeningViewModel
    {
        [Visibility(VisibilityScope.Form)]
        [Required]
        public override long? CustomerId
        {
            get { return base.CustomerId; }
            set { base.CustomerId = value; }
        }

        public ProjectJobOpeningViewModel()
            : base()
        {
            HiringMode = HiringMode.Project;
        }
    }
}
