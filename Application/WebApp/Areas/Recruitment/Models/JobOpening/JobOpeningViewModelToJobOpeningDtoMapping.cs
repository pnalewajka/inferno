﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningViewModelToJobOpeningDtoMapping : ClassMapping<JobOpeningViewModel, JobOpeningDto>
    {
        private readonly IClassMapping<ProjectDescriptionViewModel, ProjectDescriptionDto> _projectDescriptionViewModelToProjectDescriptionDtoMapping;

        public JobOpeningViewModelToJobOpeningDtoMapping(
            IClassMappingFactory mappingFactory)
        {
            _projectDescriptionViewModelToProjectDescriptionDtoMapping = mappingFactory.CreateMapping<ProjectDescriptionViewModel, ProjectDescriptionDto>();
            Mapping = v => GetDto(v);
        }

        private JobOpeningDto GetDto(JobOpeningViewModel viewModel)
        {
            var dto = new JobOpeningDto();
            var viewModelType = JobOpeningDtoToJobOpeningViewModelMapping.GetViewModelType(viewModel.HiringMode);

            SetCommonValuesFromModel(viewModel, dto);

            return dto;
        }

        private void SetCommonValuesFromModel(JobOpeningViewModel model, JobOpeningDto dto)
        {
            dto.Id = model.Id;
            dto.CustomerId = model.CustomerId;
            dto.CityIds = model.CityIds.ToArray();
            dto.JobProfileIds = model.JobProfileIds.ToArray();
            dto.PositionName = model.PositionName;
            dto.OrgUnitId = model.OrgUnitId;
            dto.CompanyId = model.CompanyId;
            dto.Priority = model.Priority;
            dto.HiringMode = model.HiringMode;
            dto.VacancyLevel1 = model.VacancyLevel1;
            dto.VacancyLevel2 = model.VacancyLevel2;
            dto.VacancyLevel3 = model.VacancyLevel3;
            dto.VacancyLevel4 = model.VacancyLevel4;
            dto.VacancyLevel5 = model.VacancyLevel5;
            dto.VacancyLevelNotApplicable = model.VacancyLevelNotApplicable;
            dto.IsTechnicalVerificationRequired = model.IsTechnicalVerificationRequired;
            dto.IsIntiveResumeRequired = model.IsIntiveResumeRequired;
            dto.IsPolishSpeakerRequired = model.IsPolishSpeakerRequired;
            dto.NoticePeriod = model.NoticePeriod;
            dto.SalaryRate = model.SalaryRate;
            dto.EnglishLevel = model.EnglishLevel;
            dto.OtherLanguages = model.OtherLanguages;
            dto.AgencyEmploymentMode = model.AgencyEmploymentMode;
            dto.Status = model.Status;
            dto.RecruitersIds = model.RecruiterIds.ToArray();
            dto.WatcherIds = model.WatcherIds.ToArray();
            dto.RecruitmentOwnersIds = model.RecrutmentOwnerIds.ToArray();
            dto.WorkflowAction = model.WorkflowAction;
            dto.WorkflowRejectionReason = model.WorkflowRejectionReason;
            dto.WorkflowClosedReason = model.WorkflowClosedReason;
            dto.WorkflowClosedComment = model.WorkflowClosedComment;
            dto.IsHighlyConfidential = model.IsHighlyConfidential;
            dto.DecisionMakerId = model.DecisionMakerId;
            dto.TechnicalReviewComment = model.TechnicalReviewComment;
            dto.CrmId = model.CrmId;
            dto.Timestamp = model.Timestamp;
            dto.ProjectDescriptions = model.ProjectDescriptions?
                .Select(_projectDescriptionViewModelToProjectDescriptionDtoMapping.CreateFromSource)
                .ToArray();
        }
    }
}
