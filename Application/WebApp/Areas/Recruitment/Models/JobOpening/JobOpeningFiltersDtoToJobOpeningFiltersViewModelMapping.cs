﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpeningFilters;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningFiltersDtoToJobOpeningFiltersViewModelMapping : ClassMapping<JobOpeningFiltersDto, JobOpeningFiltersViewModel>
    {
        public JobOpeningFiltersDtoToJobOpeningFiltersViewModelMapping()
        {
            Mapping = d => new JobOpeningFiltersViewModel
            {
                CityId = d.CityIds,
                DecisionMakerId = d.DecisionMakerId,
                JobProfileIds = d.JobProfileIds,
                OrgUnitId = d.OrgUnitId,
                HiringMode = d.HiringMode,
                OpenedOnFrom = d.OpenedOnFrom,
                OpenedOnTo = d.OpenedOnTo,
                Priority = d.Priority,
                Status = d.Status,
                AgencyEmploymentMode = d.AgencyEmploymentMode
            };
        }
    }
}