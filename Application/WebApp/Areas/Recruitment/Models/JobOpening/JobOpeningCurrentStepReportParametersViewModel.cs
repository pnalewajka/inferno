﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.JobOpeningCurrentStepReportParameters")]
    public class JobOpeningCurrentStepReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.JobOpeningId), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(JobOpeningPickerController), OnResolveUrlJavaScript = "url += '&mode=reporting-hiring-managers'")]
        public List<long> JobOpeningIds { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.HiringManagerIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=reporting-hiring-managers'")]
        public List<long> HiringManagerIds { get; set; }

        [DisplayNameLocalized(nameof(TechnicalReviewResources.JobProfilesShort), typeof(TechnicalReviewResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        public List<long> JobProfileIds { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.CityIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(CityPickerController))]
        public List<long> CityIds { get; set; }
    }
}