﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    [Identifier("Models.JobOpeningNoteViewModel")]
    public class JobOpeningNoteViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(JobOpeningPickerController))]
        [DisplayNameLocalized(nameof(JobOpeningNoteResources.JobOpening), typeof(JobOpeningNoteResources))]
        [Required]
        [NonSortable]
        public long JobOpeningId { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningNoteResources.Content), typeof(JobOpeningNoteResources))]
        [StringLength(5000)]
        [Render(GridCssClass = "remaining-space-column")]
        [Multiline(3)]
        [NonSortable]
        [AllowHtml]
        [RichText]
        public string Content { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(JobOpeningNoteResources.Content), typeof(JobOpeningNoteResources))]
        [StringLength(5000)]
        [Required]
        [NonSortable]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RichNoteControl")]
        [AllowHtml]
        [HtmlSanitize]
        public string RichContent { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.CreatedOn), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public DateTime CreatedOn { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.CreatedBy), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public string CreatedByFullName { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningNoteResources.NoteType), typeof(JobOpeningNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteType NoteType { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningNoteResources.NoteVisibility), typeof(JobOpeningNoteResources))]
        [RequiredEnum(typeof(NoteVisibility))]
        [RadioGroup(ItemProvider = typeof(BasicNoteVisibilityItemProvider))]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteVisibility NoteVisibility { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
