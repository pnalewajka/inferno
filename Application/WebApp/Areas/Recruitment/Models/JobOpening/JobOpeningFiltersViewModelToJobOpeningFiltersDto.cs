﻿using System;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpeningFilters;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningFiltersViewModelToJobOpeningFiltersDtoMapping : ClassMapping<JobOpeningFiltersViewModel, JobOpeningFiltersDto>
    {
        public JobOpeningFiltersViewModelToJobOpeningFiltersDtoMapping()
        {
            Mapping = a => new JobOpeningFiltersDto
            {
                CityIds = a.CityId,
                DecisionMakerId = a.DecisionMakerId,
                JobProfileIds = a.JobProfileIds,
                OrgUnitId = a.OrgUnitId,
                HiringMode = a.HiringMode,
                OpenedOnFrom = a.OpenedOnFrom.HasValue ? a.OpenedOnFrom.Value.StartOfDay() : (DateTime?)null,
                OpenedOnTo = a.OpenedOnTo.HasValue ? a.OpenedOnTo.Value.EndOfDay() : (DateTime?)null,
                Priority = a.Priority,
                Status = a.Status,
                AgencyEmploymentMode = a.AgencyEmploymentMode
            };
        }
    }
}