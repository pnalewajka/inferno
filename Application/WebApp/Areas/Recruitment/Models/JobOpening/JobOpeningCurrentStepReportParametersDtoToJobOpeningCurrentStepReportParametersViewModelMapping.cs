﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class JobOpeningCurrentStepReportParametersDtoToJobOpeningCurrentStepReportParametersViewModelMapping
         : ClassMapping<JobOpeningCurrentStepReportParametersDto, JobOpeningCurrentStepReportParametersViewModel>
    {
        public JobOpeningCurrentStepReportParametersDtoToJobOpeningCurrentStepReportParametersViewModelMapping()
        {
            Mapping = v => new JobOpeningCurrentStepReportParametersViewModel
            {
                JobOpeningIds = v.JobOpeningIds,
                CityIds = v.CityIds,
                JobProfileIds = v.JobProfileIds,
                HiringManagerIds = v.HiringManagersIds
            };
        }
    }
}