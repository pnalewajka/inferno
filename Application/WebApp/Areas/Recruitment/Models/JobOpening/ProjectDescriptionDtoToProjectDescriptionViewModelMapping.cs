﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class ProjectDescriptionDtoToProjectDescriptionViewModelMapping : ClassMapping<ProjectDescriptionDto, ProjectDescriptionViewModel>
    {
        public ProjectDescriptionDtoToProjectDescriptionViewModelMapping()
        {
            Mapping = d => new ProjectDescriptionViewModel
            {
                Id = d.Id,
                AboutClientAndProject = d.AboutClientAndProject,
                SpecificDuties = d.SpecificDuties,
                ToolsAndEnvironment = d.ToolsAndEnvironment,
                AboutTeam = d.AboutTeam,
                ProjectTimeline = d.ProjectTimeline,
                Methodology = d.Methodology,
                RequiredSkills = d.RequiredSkills,
                AdditionalSkills = d.AdditionalSkills,
                RecruitmentDetails = d.RecruitmentDetails,
                IsTravelRequired = d.IsTravelRequired,
                TravelAndAccomodation = d.TravelAndAccomodation,
                CanWorkRemotely = d.CanWorkRemotely,
                RemoteWorkArrangements = d.RemoteWorkArrangements,
                Comments = d.Comments,
                Timestamp = d.Timestamp
            };
        }
    }
}
