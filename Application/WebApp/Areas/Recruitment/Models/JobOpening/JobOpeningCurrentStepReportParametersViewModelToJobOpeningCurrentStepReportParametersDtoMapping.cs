﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class JobOpeningCurrentStepReportParametersViewModelToJobOpeningCurrentStepReportParametersDtoMapping
         : ClassMapping<JobOpeningCurrentStepReportParametersViewModel, JobOpeningCurrentStepReportParametersDto>
    {
        public JobOpeningCurrentStepReportParametersViewModelToJobOpeningCurrentStepReportParametersDtoMapping()
        {
            Mapping = v => new JobOpeningCurrentStepReportParametersDto
            {
                JobOpeningIds = v.JobOpeningIds,
                CityIds = v.CityIds,
                JobProfileIds = v.JobProfileIds,
                HiringManagersIds = v.HiringManagerIds
            };
        }
    }
}