﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class ProjectDescriptionViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.AboutClient), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(2048)]
        public string AboutClientAndProject { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.SpecificDuties), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(8000)]
        public string SpecificDuties { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.ToolsAndEnvironment), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(1024)]
        public string ToolsAndEnvironment { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.AboutTeam), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(2048)]
        public string AboutTeam { get; set; }

        [StringLength(2048)]
        [DisplayNameLocalized(nameof(ProjectDescriptionResources.ProjectTimeline), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        public string ProjectTimeline { get; set; }

        [StringLength(512)]
        [DisplayNameLocalized(nameof(ProjectDescriptionResources.Methodology), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        public string Methodology { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.RequiredSkills), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(8000)]
        public string RequiredSkills { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.AdditionalSkills), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(4096)]
        public string AdditionalSkills { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.RecruitmentDetails), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(2048)]
        public string RecruitmentDetails { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.IsTravelRequired), typeof(ProjectDescriptionResources))]
        public bool IsTravelRequired { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.TravelAndAccomodation), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(8000)]
        public string TravelAndAccomodation { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.CanWorkRemotely), typeof(ProjectDescriptionResources))]
        public bool CanWorkRemotely { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.RemoteWorkArrangements), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(2048)]
        public string RemoteWorkArrangements { get; set; }

        [DisplayNameLocalized(nameof(ProjectDescriptionResources.Comments), typeof(ProjectDescriptionResources))]
        [Multiline(3)]
        [StringLength(3072)]
        public string Comments { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
