﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    [Identifier("Models.JobOpeningRejectViewModel")]
    public class JobOpeningRejectViewModel
    {
        [StringLength(255)]
        [Multiline(3)]
        [DisplayNameLocalized(nameof(JobOpeningResources.RejectionReasonLabel), nameof(JobOpeningResources.RejectionReasonLabel), typeof(JobOpeningResources))]
        public string RejectionReason { get; set; }
    }
}