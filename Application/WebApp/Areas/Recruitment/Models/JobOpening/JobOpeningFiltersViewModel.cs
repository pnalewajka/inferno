﻿
using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpeningFilters
{
    [Identifier("Filters.JobOpeningProfileFilter")]
    [DescriptionLocalized(nameof(JobOpeningResources.JobOpeningFilters), typeof(JobOpeningResources))]
    [FieldSetDescription(1, GeneralFieldSet)]
    [FieldSetDescription(2, OpenedOnFieldSet, nameof(JobOpeningResources.RoleOpenedOn), typeof(JobOpeningResources))]
    public class JobOpeningFiltersViewModel
    {
        private const string GeneralFieldSet = "General";
        private const string OpenedOnFieldSet = "OpenedOn";

        [ValuePicker(Type = typeof(JobProfilePickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [DisplayNameLocalized(nameof(JobOpeningResources.JobProfileIdsShort), typeof(JobOpeningResources))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long[] JobProfileIds { get; set; }

        [ValuePicker(Type = typeof(OrgUnitPickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [DisplayNameLocalized(nameof(JobOpeningResources.OrgUnitId), typeof(JobOpeningResources))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long? OrgUnitId { get; set; }

        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [DisplayNameLocalized(nameof(JobOpeningResources.DecisionMaker), typeof(JobOpeningResources))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long? DecisionMakerId { get; set; }

        [ValuePicker(Type = typeof(CityPickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [DisplayNameLocalized(nameof(JobOpeningResources.CityIds), typeof(JobOpeningResources))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long[] CityId { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.Priority), typeof(JobOpeningResources))]
        [Range(0, 2)]
        [Render(GridCssClass = "numeric-column")]
        [FieldSet(GeneralFieldSet)]
        public int? Priority { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.Status), typeof(JobOpeningResources))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public JobOpeningStatus? Status { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.HiringModeShortLabel), typeof(JobOpeningResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<HiringMode>))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [FieldSet(GeneralFieldSet)]
        public int[] HiringMode { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.From), typeof(JobOpeningResources))]
        [FieldSet(OpenedOnFieldSet)]
        public DateTime? OpenedOnFrom { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.To), typeof(JobOpeningResources))]
        [FieldSet(OpenedOnFieldSet)]
        public DateTime? OpenedOnTo { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.AgencyEmploymentMode), typeof(JobOpeningResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<AgencyEmploymentMode>))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public AgencyEmploymentMode? AgencyEmploymentMode { get; set; }

        public override string ToString() => JobOpeningResources.JobOpeningFilters;
    }
}