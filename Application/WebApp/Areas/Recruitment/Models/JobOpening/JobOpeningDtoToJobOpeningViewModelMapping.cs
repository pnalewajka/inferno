﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class JobOpeningDtoToJobOpeningViewModelMapping : ClassMapping<JobOpeningDto, JobOpeningViewModel>
    {
        private readonly IClassMapping<JobOpeningNoteDto, JobOpeningNoteViewModel> _jobOpeningNoteDtoToJobOpeningNoteViewModelMapping;
        private readonly IClassMapping<HistoryEntryDto, HistoryEntryViewModel> _activityHistoryDtoToActivityHistoryViewModelMapping;
        private readonly IClassMapping<ProjectDescriptionDto, ProjectDescriptionViewModel> _projectDescriptionDtoToProjectDescriptionViewModelMapping;

        public JobOpeningDtoToJobOpeningViewModelMapping(
            IClassMappingFactory mappingFactory)
        {
            _jobOpeningNoteDtoToJobOpeningNoteViewModelMapping = mappingFactory.CreateMapping<JobOpeningNoteDto, JobOpeningNoteViewModel>();
            _activityHistoryDtoToActivityHistoryViewModelMapping = mappingFactory.CreateMapping<HistoryEntryDto, HistoryEntryViewModel>();
            _projectDescriptionDtoToProjectDescriptionViewModelMapping = mappingFactory.CreateMapping<ProjectDescriptionDto, ProjectDescriptionViewModel>();
            Mapping = d => GetViewModel(d);
        }

        public static Type GetViewModelType(HiringMode hiringMode)
        {
            switch (hiringMode)
            {
                case HiringMode.Intive:
                    return typeof(IntiveJobOpeningViewModel);

                case HiringMode.Project:
                    return typeof(ProjectJobOpeningViewModel);

                default:
                    throw new ArgumentOutOfRangeException(nameof(hiringMode));
            }
        }

        public static JobOpeningViewModel CreateViewModel(HiringMode hiringMode)
        {
            switch (hiringMode)
            {
                case HiringMode.Intive:
                    return new IntiveJobOpeningViewModel();

                case HiringMode.Project:
                    return new ProjectJobOpeningViewModel();

                default:
                    throw new ArgumentOutOfRangeException(nameof(hiringMode));
            }
        }

        public JobOpeningViewModel GetViewModel(JobOpeningDto dto)
        {
            var viewModel = CreateViewModel(dto.HiringMode);

            SetCommonValuesFromDto(dto, viewModel);

            return viewModel;
        }

        private void SetCommonValuesFromDto(JobOpeningDto dto, JobOpeningViewModel model)
        {
            model.Id = dto.Id;
            model.CustomerId = dto.CustomerId;
            model.CustomerName = dto.CustomerName;
            model.CityIds = dto.CityIds.ToList();
            model.CityNames = dto.CityNames.ToList();
            model.JobProfileIds = dto.JobProfileIds != null ? dto.JobProfileIds.ToList() : null;
            model.JobProfileNames = dto.JobProfileNames != null ? dto.JobProfileNames.ToList() : null;
            model.PositionName = dto.PositionName;
            model.OrgUnitId = dto.OrgUnitId;
            model.CompanyId = dto.CompanyId;
            model.AcceptingEmployeeFullName = dto.AcceptingEmployeeFullName;
            model.Priority = dto.Priority;
            model.HiringMode = dto.HiringMode;
            model.RoleOpenedOn = dto.RoleOpenedOn;
            model.VacancyLevel1 = dto.VacancyLevel1;
            model.VacancyLevel2 = dto.VacancyLevel2;
            model.VacancyLevel3 = dto.VacancyLevel3;
            model.VacancyLevel4 = dto.VacancyLevel4;
            model.VacancyLevel5 = dto.VacancyLevel5;
            model.VacancyLevelNotApplicable = dto.VacancyLevelNotApplicable;
            model.VacancyTotal = dto.VacancyTotal;
            model.IsTechnicalVerificationRequired = dto.IsTechnicalVerificationRequired;
            model.IsIntiveResumeRequired = dto.IsIntiveResumeRequired;
            model.IsPolishSpeakerRequired = dto.IsPolishSpeakerRequired;
            model.NoticePeriod = dto.NoticePeriod;
            model.SalaryRate = dto.SalaryRate;
            model.EnglishLevel = dto.EnglishLevel;
            model.OtherLanguages = dto.OtherLanguages;
            model.AgencyEmploymentMode = dto.AgencyEmploymentMode;
            model.Status = dto.Status;
            model.RecruiterIds = dto.RecruitersIds.ToList();
            model.WatcherIds = dto.WatcherIds.ToList();
            model.RecrutmentOwnerIds = dto.RecruitmentOwnersIds.ToList();
            model.CanBeApprovedByCurrentUser = dto.CanBeApprovedByCurrentUser;
            model.IsHighlyConfidential = dto.IsHighlyConfidential;
            model.CreatedByFullName = dto.CreatedByFullName;
            model.CreatedOn = dto.CreatedOn;
            model.IsWatchedByCurrentUser = dto.IsWatchedByCurrentUser;
            model.RejectionReason = dto.RejectionReason;
            model.Timestamp = dto.Timestamp;
            model.ClosedComment = dto.ClosedComment;
            model.ClosedReason = dto.ClosedReason;
            model.DecisionMakerId = dto.DecisionMakerId;
            model.TechnicalReviewComment = dto.TechnicalReviewComment;
            model.Notes = dto.Notes?.OrderByDescending(n => n.CreatedOn).Select(_jobOpeningNoteDtoToJobOpeningNoteViewModelMapping.CreateFromSource).ToList() ?? new List<JobOpeningNoteViewModel>();
            model.HistoryEntries = dto.HistoryEntries?.Select(_activityHistoryDtoToActivityHistoryViewModelMapping.CreateFromSource).ToList() ?? new List<HistoryEntryViewModel>();
            model.CrmId = dto.CrmId;
            model.ProjectDescriptions = dto.ProjectDescriptions
                .Select(_projectDescriptionDtoToProjectDescriptionViewModelMapping.CreateFromSource)
                .ToList();
        }
    }
}