﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    [Identifier("Models.JobOpeningCloseViewModel")]
    public class JobOpeningCloseViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<JobOpeningClosedReason>))]
        [DisplayNameLocalized(nameof(JobOpeningResources.ClosedReason), nameof(JobOpeningResources.ClosedReason), typeof(JobOpeningResources))]
        public JobOpeningClosedReason ClosedReason { get; set; }

        [StringLength(255)]
        [Multiline(3)]
        [DisplayNameLocalized(nameof(JobOpeningResources.ClosedComment), nameof(JobOpeningResources.ClosedComment), typeof(JobOpeningResources))]
        public string ClosedComment { get; set; }
    }
}