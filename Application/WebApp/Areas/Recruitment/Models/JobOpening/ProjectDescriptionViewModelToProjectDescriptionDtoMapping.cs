﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    public class ProjectDescriptionViewModelToProjectDescriptionDtoMapping : ClassMapping<ProjectDescriptionViewModel, ProjectDescriptionDto>
    {
        public ProjectDescriptionViewModelToProjectDescriptionDtoMapping()
        {
            Mapping = v => new ProjectDescriptionDto
            {
                Id = v.Id,
                AboutClientAndProject = v.AboutClientAndProject,
                SpecificDuties = v.SpecificDuties,
                ToolsAndEnvironment = v.ToolsAndEnvironment,
                AboutTeam = v.AboutTeam,
                ProjectTimeline = v.ProjectTimeline,
                Methodology = v.Methodology,
                RequiredSkills = v.RequiredSkills,
                AdditionalSkills = v.AdditionalSkills,
                RecruitmentDetails = v.RecruitmentDetails,
                IsTravelRequired = v.IsTravelRequired,
                TravelAndAccomodation = v.TravelAndAccomodation,
                CanWorkRemotely = v.CanWorkRemotely,
                RemoteWorkArrangements = v.RemoteWorkArrangements,
                Comments = v.Comments,
                Timestamp = v.Timestamp
            };
        }
    }
}
