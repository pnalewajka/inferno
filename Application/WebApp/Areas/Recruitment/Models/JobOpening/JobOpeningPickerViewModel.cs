﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.JobOpening
{
    [Identifier("Models.JobOpeningPickerViewModel")]
    public class JobOpeningPickerViewModel
    {
        [ValuePicker(Type = typeof(JobOpeningPickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        public long JobOpeningId { get; set; }
    }
}