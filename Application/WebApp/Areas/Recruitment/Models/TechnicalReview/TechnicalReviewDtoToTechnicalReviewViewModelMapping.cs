﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.TechnicalReview
{
    public class TechnicalReviewDtoToTechnicalReviewViewModelMapping : ClassMapping<TechnicalReviewDto, TechnicalReviewViewModel>
    {
        public TechnicalReviewDtoToTechnicalReviewViewModelMapping()
        {
            Mapping = d => new TechnicalReviewViewModel
            {
                Id = d.Id,
                JobProfileIds = d.JobProfileIds,
                Timestamp = d.Timestamp,
                SeniorityLevelAssessment = d.SeniorityLevelAssessment,
                TechnicalAssignmentAssessment = d.TechnicalAssignmentAssessment,
                TechnicalKnowledgeAssessment = d.TechnicalKnowledgeAssessment,
                TeamProjectSuitabilityAssessment = d.TeamProjectSuitabilityAssessment,
                EnglishUsageAssessment = d.EnglishUsageAssessment,
                RiskAssessment = d.RiskAssessment,
                StrengthAssessment = d.StrengthAssessment,
                OtherRemarks = d.OtherRemarks,
                PositionName = d.PositionName,
                AssigneeFullName = d.AssigneeFullName,
                PlannedOn = d.PlannedOn,
                CandidateFullName = d.CandidateFullName,
                Decision = d.Decision
            };
        }
    }
}
