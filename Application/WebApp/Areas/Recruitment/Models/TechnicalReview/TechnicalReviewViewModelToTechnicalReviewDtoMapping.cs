﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.TechnicalReview
{
    public class TechnicalReviewViewModelToTechnicalReviewDtoMapping : ClassMapping<TechnicalReviewViewModel, TechnicalReviewDto>
    {
        public TechnicalReviewViewModelToTechnicalReviewDtoMapping()
        {
            Mapping = v => new TechnicalReviewDto
            {
                Id = v.Id,
                JobProfileIds = v.JobProfileIds,
                Timestamp = v.Timestamp,
                SeniorityLevelAssessment = v.SeniorityLevelAssessment,
                TechnicalAssignmentAssessment = v.TechnicalAssignmentAssessment,
                TechnicalKnowledgeAssessment = v.TechnicalKnowledgeAssessment,
                TeamProjectSuitabilityAssessment = v.TeamProjectSuitabilityAssessment,
                EnglishUsageAssessment = v.EnglishUsageAssessment,
                RiskAssessment = v.RiskAssessment,
                StrengthAssessment = v.StrengthAssessment,
                OtherRemarks = v.OtherRemarks
            };
        }
    }
}
