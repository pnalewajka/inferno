﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class TechnicalReviewDetailViewModel
    {
        public long StepId { get; set; }

        public virtual IList<string> JobProfilesNames { get; set; }

        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        public string TechnicalKnowledgeAssessment { get; set; }

        public string TechnicalAssignmentAssessment { get; set; }

        public string EnglishUsageAssessment { get; set; }

        public string StrengthAssessment { get; set; }

        public string RiskAssessment { get; set; }

        public string TeamProjectSuitabilityAssessment { get; set; }

        public string OtherRemarks { get; set; }

        public string AssigneeFullName { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public DateTime HappenedOn { get; set; }
    }
}