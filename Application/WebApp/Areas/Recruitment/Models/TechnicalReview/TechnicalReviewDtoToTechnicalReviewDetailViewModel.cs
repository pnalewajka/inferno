﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.TechnicalReview
{
    public class TechnicalReviewDtoToTechnicalReviewDetailViewModel
        : ClassMapping<TechnicalReviewDto, TechnicalReviewDetailViewModel>
    {
        public TechnicalReviewDtoToTechnicalReviewDetailViewModel()
        {
            Mapping = tr => new TechnicalReviewDetailViewModel
            {
                StepId = tr.RecruitmentProcessStepId,
                EnglishUsageAssessment = tr.EnglishUsageAssessment,
                SeniorityLevelAssessment = tr.SeniorityLevelAssessment,
                TechnicalKnowledgeAssessment = tr.TechnicalKnowledgeAssessment,
                TechnicalAssignmentAssessment = tr.TechnicalAssignmentAssessment,
                StrengthAssessment = tr.StrengthAssessment,
                RiskAssessment = tr.RiskAssessment,
                TeamProjectSuitabilityAssessment = tr.TeamProjectSuitabilityAssessment,
                OtherRemarks = tr.OtherRemarks,
                JobProfilesNames = tr.JobProfileNames,
                AssigneeFullName = tr.AssigneeFullName,
                Decision = tr.Decision,
                HappenedOn = tr.ModifiedOn
            };
        }
    }
}