﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.TechnicalReview
{
    public class TechnicalReviewViewModel
    {
        public long Id { get; set; }

        [Order(1)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Render(GridCssClass = "no-break-column")]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.CandidateFullName), typeof(TechnicalReviewResources))]
        public string CandidateFullName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Render(GridCssClass = "no-break-column")]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.PositionName), typeof(TechnicalReviewResources))]
        public string PositionName { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.AssigneeFullName), typeof(TechnicalReviewResources))]
        public string AssigneeFullName { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.PlannedOn), typeof(TechnicalReviewResources))]
        [StringFormat("{0:d}")]
        public DateTime? PlannedOn { get; set; }

        [Order(5)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.Decision), typeof(TechnicalReviewResources))]
        public RecruitmentProcessStepDecision Decision { get; set; }

        [Order(6)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Render(GridCssClass = "no-break-column")]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.JobProfileIds), typeof(TechnicalReviewResources))]
        [NonSortable]
        public List<long> JobProfileIds { get; set; }

        [Order(7)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.SeniorityLevelAssessment), nameof(TechnicalReviewResources.SeniorityLevelAssessmentShort), typeof(TechnicalReviewResources))]
        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        [Order(8)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.SoftwareEngineeringAssessment), typeof(TechnicalReviewResources))]
        [Multiline(3)]
        [StringLength(5000)]
        public string TechnicalKnowledgeAssessment { get; set; }

        [Order(9)]
        [Visibility(VisibilityScope.Form)]
        [StringLength(5000)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.TechnicalAssignmentAssessment), typeof(TechnicalReviewResources))]
        [Multiline(3)]
        public string TechnicalAssignmentAssessment { get; set; }

        [Order(10)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.EnglishUsageAssessment), typeof(TechnicalReviewResources))]
        [Multiline(2)]
        [StringLength(5000)]
        public string EnglishUsageAssessment { get; set; }

        [Order(11)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.StrengthAssessment), typeof(TechnicalReviewResources))]
        [Multiline(3)]
        [StringLength(5000)]
        public string StrengthAssessment { get; set; }

        [Order(12)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.RiskAssessment), typeof(TechnicalReviewResources))]
        [Multiline(3)]
        [StringLength(5000)]
        public string RiskAssessment { get; set; }

        [Order(14)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.ExperienceAssessment), typeof(TechnicalReviewResources))]
        [Multiline(3)]
        [StringLength(5000)]
        public string TeamProjectSuitabilityAssessment { get; set; }

        [Order(15)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [Render(GridCssClass = "remaining-space-column")]
        [DisplayNameLocalized(nameof(TechnicalReviewResources.OtherRemarks), nameof(TechnicalReviewResources.OtherRemarksLabelShort), typeof(TechnicalReviewResources))]
        [Multiline(3)]
        [StringLength(5000)]
        public string OtherRemarks { get; set; }

        public byte[] Timestamp { get; set; }

        public TechnicalReviewViewModel()
        {
            JobProfileIds = new List<long>();
        }
    }
}