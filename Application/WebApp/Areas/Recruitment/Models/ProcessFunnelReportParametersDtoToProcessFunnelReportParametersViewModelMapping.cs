﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessFunnelReportParametersDtoToProcessFunnelReportParametersViewModelMapping : ClassMapping<ProcessFunnelReportParametersDto, ProcessFunnelReportParametersViewModel>
    {
        public ProcessFunnelReportParametersDtoToProcessFunnelReportParametersViewModelMapping()
        {
            Mapping = v => new ProcessFunnelReportParametersViewModel
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                RecruiterIds = v.RecruiterIds
            };
        }
    }
}