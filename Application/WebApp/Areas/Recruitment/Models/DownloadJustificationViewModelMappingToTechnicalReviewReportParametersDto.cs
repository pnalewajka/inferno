﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class DownloadJustificationViewModelMappingToTechnicalReviewReportParametersDto : ClassMapping<DownloadJustificationViewModel, TechnicalReviewSummaryReportParametersDto>
    {
        public DownloadJustificationViewModelMappingToTechnicalReviewReportParametersDto()
        {
            Mapping = v => new TechnicalReviewSummaryReportParametersDto
            {
                Comment = v.Comment,
                CustomerId = v.CustomerId,
                RecruitmentProcessStepId = v.RecruitmentProcessStepId
            };
        }
    }
}