﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class CandidateFileDtoToCandidateFileFromInboundEmailViewModelMapping
        : ClassMapping<CandidateFileDto, CandidateFileFromInboundEmailViewModel>
    {
        public CandidateFileDtoToCandidateFileFromInboundEmailViewModelMapping()
        {
            Mapping = d => new CandidateFileFromInboundEmailViewModel
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                DocumentType = d.DocumentType,
                Comment = d.Comment,
                Timestamp = d.Timestamp
            };
        }
    }
}