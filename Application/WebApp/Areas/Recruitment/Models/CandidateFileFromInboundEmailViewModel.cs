﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.CandidateFileFromInboundEmailViewModel")]
    public class CandidateFileFromInboundEmailViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=false'")]
        [DisplayNameLocalized(nameof(CandidateFileResources.Candidate), typeof(CandidateFileResources))]
        [Required]
        [Render(Size = Size.Large)]
        public long CandidateId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(InboundEmailDocumentPickerController), OnResolveUrlJavaScript = "url += '&assigned-only=true'")]
        [DisplayNameLocalized(nameof(InboundEmailResources.InboundEmailDocument), typeof(InboundEmailResources))]
        [Required]
        [Render(Size = Size.Large)]
        public long InboundEmailDocumentId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateFileResources.DocumentType), typeof(CandidateFileResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<RecruitmentDocumentType>))]
        [Render(ControlCssClass = "align-options-200")]
        [Required]
        public RecruitmentDocumentType DocumentType { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [Multiline(3)]
        [DataType(DataType.MultilineText)]
        [DisplayNameLocalized(nameof(CandidateFileResources.Comment), typeof(CandidateFileResources))]
        [StringLength(100)]
        public string Comment { get; set; }

        public byte[] Timestamp { get; set; }

        public CandidateFileFromInboundEmailViewModel()
        {
        }
    }
}