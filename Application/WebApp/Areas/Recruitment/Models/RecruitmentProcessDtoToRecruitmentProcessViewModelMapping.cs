﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessDtoToRecruitmentProcessViewModelMapping : ClassMapping<RecruitmentProcessDto, RecruitmentProcessViewModel>
    {
        public RecruitmentProcessDtoToRecruitmentProcessViewModelMapping(IPrincipalProvider principalProvider,
            IClassMapping<RecruitmentProcessNoteDto, RecruitmentProcessNoteViewModel> recruitmentProcessNoteClassMapping,
            IClassMapping<RecruitmentProcessStepInfoDto, RecruitmentProcessStepInfoViewModel> stepInfoClassMapping,
            IClassMapping<HistoryEntryDto, HistoryEntryViewModel> activityHistoryMapping)
        {
            Mapping = d => new RecruitmentProcessViewModel
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                JobOpeningId = d.JobOpeningId,
                PositionName = d.PositionName,
                RecruiterId = d.RecruiterId,
                RecruiterName = d.RecruiterName,
                DecisionMakerName = d.DecisionMakerName,
                CityId = d.CityId,
                Status = d.Status,
                ResumeShownToClientOn = d.ResumeShownToClientOn,
                ClosedOn = d.ClosedOn,
                ClosedReason = d.ClosedReason,
                ClosedComment = d.ClosedComment,
                WatcherIds = d.WatcherIds.ToList(),
                CandidateName = d.CandidateName,
                OfferPositionId = d.OfferPositionId,
                OfferComment = d.OfferComment,
                OfferContractType = d.OfferContractType,
                OfferLocationId = d.OfferLocationId,
                OfferSalary = d.OfferSalary,
                OfferStartDate = d.OfferStartDate,
                FinalComment = d.FinalComment,
                FinalLocationId = d.FinalLocationId,
                JobApplicationId = d.JobApplicationId,
                FinalLongTermContractTypeId = d.FinalLongTermContractTypeId,
                FinalLongTermSalary = d.FinalLongTermSalary,
                FinalPositionId = d.FinalPositionId,
                FinalTrialContractTypeId = d.FinalTrialContractTypeId,
                FinalTrialSalary = d.FinalTrialSalary,
                FinalStartDate = d.FinalStartDate,
                FinalEmploymentSourceId = d.FinalEmploymentSourceId,
                FinalSignedDate = d.FinalSignedDate,
                IsWatchedByCurrentUser = d.WatcherIds.Any(w => w == principalProvider.Current.EmployeeId),
                IsNotClosed = d.Status != RecruitmentProcessStatus.Closed,
                ScreeningGeneralOpinion = d.ScreeningGeneralOpinion,
                ScreeningMotivation = d.ScreeningMotivation,
                ScreeningWorkplaceExpectations = d.ScreeningWorkplaceExpectations,
                ScreeningSkills = d.ScreeningSkills,
                ScreeningLanguageEnglish = d.ScreeningLanguageEnglish,
                ScreeningLanguageGerman = d.ScreeningLanguageGerman,
                ScreeningLanguageOther = d.ScreeningLanguageOther,
                ScreeningContractExpectations = d.ScreeningContractExpectations,
                ScreeningAvailability = d.ScreeningAvailability,
                ScreeningOtherProcesses = d.ScreeningOtherProcesses,
                ScreeningCounterOfferCriteria = d.ScreeningCounterOfferCriteria,
                ScreeningRelocationCanDo = d.ScreeningRelocationCanDo,
                ScreeningRelocationComment = d.ScreeningRelocationComment,
                ScreeningBusinessTripsCanDo = d.ScreeningBusinessTripsCanDo,
                ScreeningBusinessTripsComment = d.ScreeningBusinessTripsComment,
                ScreeningEveningWorkCanDo = d.ScreeningBusinessTripsCanDo,
                ScreeningEveningWorkComment = d.ScreeningEveningWorkComment,
                ScreeningWorkPermitNeeded = d.ScreeningWorkPermitNeeded,
                ScreeningWorkPermitComment = d.ScreeningWorkPermitComment,
                StepInfos = d.StepInfos.Select(stepInfoClassMapping.CreateFromSource).ToList(),
                Notes = d.Notes != null
                    ? d.Notes.OrderByDescending(n => n.CreatedOn).Select(recruitmentProcessNoteClassMapping.CreateFromSource).ToList()
                    : new List<RecruitmentProcessNoteViewModel>(),
                HistoryEntries = d.HistoryEntries != null
                    ? d.HistoryEntries.Select(activityHistoryMapping.CreateFromSource).ToList()
                    : new List<HistoryEntryViewModel>(),
                IsReadyToShowScreeningData = d.IsReadyToShowScreeningData,
                IsReadyToShowTechnicalReviewData = d.IsReadyToShowTechnicalReviewData,
                IsReadyToShowOfferData = d.IsReadyToShowOfferData,
                IsReadyToShowFinalData = d.IsReadyToShowFinalData,
                CanBeReopened = d.CanBeReopened,
                OnboardingRequestStatus = d.OnboardingRequestStatus,
                DisplayName = d.DisplayName,
                CrmId = d.CrmId,
                IsMatureEnoughForHiringManager = d.IsMatureEnoughForHiringManager,
                CreatedOn = d.CreatedOn,
                CreatedByFullName = d.CreatedByFullName,
                Timestamp = d.Timestamp,
            };
        }
    }
}