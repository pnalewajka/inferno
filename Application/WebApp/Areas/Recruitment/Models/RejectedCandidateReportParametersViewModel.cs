﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Recruitment.ItemProviders;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RejectedCandidateReportParameters")]
    public class RejectedCandidateReportParametersViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.RecruiterIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=reporting-recruiters'")]
        public List<long> RecruiterIds { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ClosedReason), typeof(RecruitmentProcessResources))]
        [Dropdown(ItemProvider = typeof(RecruitmentProcessRejectionReasonItemProvider), EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent)]
        [Render(ControlCssClass = "align-options-200")]
        public RecruitmentProcessClosedReason? RejectionReason { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.RejectedByEmployee), typeof(ReportResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified'")]
        public List<long> RejectingEmployeeIds { get; set; }

        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        [DisplayNameLocalized(nameof(JobOpeningResources.OrgUnitId), typeof(JobOpeningResources))]
        public long? OrgUnitId { get; set; }
    }
}