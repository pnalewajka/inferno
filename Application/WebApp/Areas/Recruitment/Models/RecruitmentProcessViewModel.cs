﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [TabDescription(0, InformationTab, nameof(RecruitmentProcessResources.InformationTabText), typeof(RecruitmentProcessResources))]
    [TabDescription(1, ProcessTab, nameof(RecruitmentProcessResources.ProcessTabText), typeof(RecruitmentProcessResources))]
    [TabDescription(2, ScreeningTab, nameof(RecruitmentProcessResources.ScreeningTabText), typeof(RecruitmentProcessResources))]
    [TabDescription(3, OfferTab, nameof(RecruitmentProcessResources.OfferTabText), typeof(RecruitmentProcessResources))]
    [TabDescription(4, FinalTab, nameof(RecruitmentProcessResources.FinalTabText), typeof(RecruitmentProcessResources))]
    [TabDescription(5, NoteTab, nameof(NoteTabLabel), SecurityRoleType.CanViewRecruitmentProcessNotes)]
    [TabDescription(6, StepsTab, nameof(StepsTabLabel))]
    [TabDescription(7, HistoryTab, nameof(HistoryTabLabel), SecurityRoleType.CanViewRecruitmentProcessHistory)]
    [TabDescription(8, ContributorTab, nameof(RecruitmentProcessResources.ContributorTabText), typeof(RecruitmentProcessResources))]
    [Identifier("Models.RecruitmentProcessViewModel")]
    public partial class RecruitmentProcessViewModel
    {
        internal const string InformationTab = "information";
        internal const string ContributorTab = "contributor";
        internal const string ProcessTab = "process";
        internal const string OfferTab = "offer";
        internal const string FinalTab = "final";
        internal const string ScreeningTab = "screening";
        internal const string NoteTab = "notes";
        internal const string StepsTab = "steps";
        internal const string HistoryTab = "history";

        public long Id { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=true'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CandidateId), typeof(RecruitmentProcessResources))]
        [Tab(InformationTab)]
        [Order(1)]
        public long CandidateId { get; set; }

        [SortBy(nameof(CandidateId))]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CandidateId), typeof(RecruitmentProcessResources))]
        [Order(1)]
        public DisplayUrl GetCandidateLink(UrlHelper helper)
        {
            return CellContentHelper.GetPageLink(helper.UriActionGet<CandidateController>(c => c.Details(CandidateId), KnownParameter.None, "id", CandidateId.ToString()).Url, CandidateName, skipIcon: true);
        }

        [Visibility(VisibilityScope.None)]
        public string CandidateName { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(JobOpeningPickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.JobOpeningId), typeof(RecruitmentProcessResources))]
        [Tab(InformationTab)]
        [Order(2)]
        public long JobOpeningId { get; set; }

        [SortBy(nameof(JobOpeningId))]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.JobOpeningId), typeof(RecruitmentProcessResources))]
        [Order(2)]
        public DisplayUrl GetJobOpeningLink(UrlHelper helper)
            => CellContentHelper.GetPageLink(helper.UriActionGet<JobOpeningController>(c => c.Details(JobOpeningId), KnownParameter.None, "id", JobOpeningId.ToString()).Url, PositionName, skipIcon: true);

        [Visibility(VisibilityScope.None)]
        public string PositionName { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(JobOpeningResources.DecisionMaker), typeof(JobOpeningResources))]
        [Order(3)]
        public string DecisionMakerName { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.RecruiterId), typeof(RecruitmentProcessResources))]
        [Tab(ContributorTab)]
        [Order(4)]
        public long RecruiterId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string RecruiterName { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CityPickerController))]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CityId), typeof(RecruitmentProcessResources))]
        [Tab(InformationTab)]
        [Order(5)]
        public long? CityId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(JobApplicationPickerController), OnResolveUrlJavaScript = "url += '&parent-id=' + $('[name = " + nameof(CandidateId) + "]').val()")]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.JobApplicationId), typeof(RecruitmentProcessResources))]
        [Tab(InformationTab)]
        [Order(6)]
        public long? JobApplicationId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.Status), typeof(RecruitmentProcessResources))]
        [Tab(ProcessTab)]
        [ReadOnly(true)]
        [Order(7)]
        public RecruitmentProcessStatus Status { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ResumeShownToClientOn), typeof(RecruitmentProcessResources))]
        [Tab(ProcessTab)]
        [Order(8)]
        public DateTime? ResumeShownToClientOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ClosedOn), typeof(RecruitmentProcessResources))]
        [Tab(ProcessTab)]
        [ReadOnly(true)]
        [Order(9)]
        public DateTime? ClosedOn { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ClosedReason), typeof(RecruitmentProcessResources))]
        [Tab(ProcessTab)]
        [ReadOnly(true)]
        [Order(10)]
        [Tooltip(nameof(ClosedTooltip))]
        public RecruitmentProcessClosedReason? ClosedReason { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ClosedComment), typeof(RecruitmentProcessResources))]
        [Tab(ProcessTab)]
        [ReadOnly(true)]
        [Order(11)]
        public string ClosedComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.Watchers), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-watcher'")]
        [Tab(ContributorTab)]
        [Order(12)]
        public List<long> WatcherIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CreatedOn), typeof(RecruitmentProcessResources))]
        [Tab(ContributorTab)]
        [ReadOnly(true)]
        [Order(13)]
        public DateTime CreatedOn { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.CreatedByFullName), typeof(RecruitmentProcessResources))]
        [Tab(ContributorTab)]
        [ReadOnly(true)]
        [Order(14)]
        public string CreatedByFullName { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsWatchedByCurrentUser { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public RecruitmentProcessWorkflowAction? WorkflowAction { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public RecruitmentProcessClosedReason? WorkflowClosedReason { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public string WorkflowClosedComment { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsNotClosed { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.Progress), typeof(RecruitmentProcessResources))]
        [Visibility(VisibilityScope.Grid)]
        [NonSortable]
        [Order(15)]
        public string Progress => GetProgress();

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.StepsToDo), typeof(RecruitmentProcessResources))]
        [Order(16)]
        [Render(GridCssClass = "separate-lines")]
        public IEnumerable<DisplayUrl> GetStepsToDo(UrlHelper urlHelper) => GetOrderedStepInfos(RecruitmentProcessStepStatus.ToDo)
                                            .Select(s => GetDisplayUrl(s, urlHelper)).ToList();

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.StepsDone), typeof(RecruitmentProcessResources))]
        [Order(17)]
        [Render(GridCssClass = "separate-lines")]
        public IEnumerable<DisplayUrl> GetStepsDone(UrlHelper urlHelper) => GetOrderedStepInfos(RecruitmentProcessStepStatus.Done)
                                            .Select(s => GetDisplayUrl(s, urlHelper)).ToList();

        [Visibility(VisibilityScope.None)]
        public IList<string> StepsToDo => GetOrderedStepInfos(RecruitmentProcessStepStatus.ToDo)
                                             .Select(s => s.StepInfoDescription).ToList();

        [Visibility(VisibilityScope.None)]
        public IList<string> StepsDone => GetOrderedStepInfos(RecruitmentProcessStepStatus.Done)
                                             .Select(s => s.StepInfoDescription).ToList();

        [Visibility(VisibilityScope.Form)]
        [Tab(StepsTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RecruitmentProcessStepTab")]
        [ReadOnly(true)]
        public List<RecruitmentProcessStepInfoViewModel> StepInfos { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(NoteTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RecruitmentProcessNotes")]
        [Order(18)]
        [ReadOnly(true)]
        public List<RecruitmentProcessNoteViewModel> Notes { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(HistoryTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.HistoryEntry")]
        [ReadOnly(true)]
        public List<HistoryEntryViewModel> HistoryEntries { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsReadyToShowFinalData { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsReadyToShowScreeningData { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsReadyToShowTechnicalReviewData { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsMatureEnoughForHiringManager { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsReadyToShowOfferData { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool CanBeReopened { get; set; }

        [Visibility(VisibilityScope.None)]
        public RequestStatus? OnboardingRequestStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public string StatusName => Status.GetLocalizedDescription()?.ToString();

        [Visibility(VisibilityScope.None)]
        public string DisplayName { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid? CrmId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string ClosedTooltip => Status == RecruitmentProcessStatus.Closed ? $"{ClosedOn?.ToShortDateString()} · {ClosedComment}" : null;

        public byte[] Timestamp { get; set; }

        [Visibility(VisibilityScope.None)]
        public string NoteTabLabel =>
            $"{RecruitmentProcessResources.NoteTabText} ({Notes?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string StepsTabLabel =>
            $"{RecruitmentProcessResources.RecruitmentProcessStepsTab} ({StepInfos?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string HistoryTabLabel =>
            $"{RecruitmentProcessResources.RecruitmentProcessHistoryTab} ({HistoryEntries?.Count ?? 0})";

        public override string ToString()
        {
            return DisplayName;
        }

        private static DisplayUrl GetDisplayUrl(RecruitmentProcessStepInfoViewModel stepInfo, UrlHelper urlHelper)
        {
            var iconCssClass = stepInfo.Status == RecruitmentProcessStepStatus.ToDo
                ? (stepInfo.IsPending ? FontAwesome.Warning : FontAwesome.HourglassO)
                : (stepInfo.Decision == RecruitmentProcessStepDecision.Positive ? FontAwesome.ThumbsOUp : FontAwesome.ThumbsODown);

            string title = stepInfo.StepInfoTitle;

            return new DisplayUrl
            {
                Label = stepInfo.Type.GetLocalizedDescription().ToString(),
                IconCssClass = IconHelper.GetCssClass(iconCssClass),
                Title = title,
                Action = urlHelper.UriActionGet<RecruitmentProcessStepController>(c => c.Edit(null), KnownParameter.None, nameof(Id), stepInfo.Id.ToString())
            };
        }

        private string GetProgress()
        {
            if (Status == RecruitmentProcessStatus.Draft)
            {
                return RecruitmentProcessResources.NotStarted;
            }

            if (Status == RecruitmentProcessStatus.Closed)
            {
                return OnboardingRequestStatus != null
                    ? $"{RecruitmentProcessResources.Done} ({OnboardingRequestStatus?.GetLocalizedDescription()})"
                    : RecruitmentProcessResources.Done;
            }

            var lastToDo = GetOrderedStepInfos(RecruitmentProcessStepStatus.ToDo)
                               .LastOrDefault();

            if (lastToDo == null)
            {
                return RecruitmentProcessResources.Preparation;
            }

            var typeDescription = lastToDo.Type.GetLocalizedDescription();

            return lastToDo.IsPending
                ? $"{RecruitmentProcessResources.WaitingFor} {typeDescription}"
                : typeDescription.ToString();
        }

        private IEnumerable<RecruitmentProcessStepInfoViewModel> GetOrderedStepInfos(RecruitmentProcessStepStatus ofStatus)
        {
            return StepInfos?.Where(s => s.Status == ofStatus).OrderBy(s => s.Type).ThenBy(s => s.PlannedOn ?? DateTime.MaxValue)
                       ?? Enumerable.Empty<RecruitmentProcessStepInfoViewModel>();
        }
    }
}
