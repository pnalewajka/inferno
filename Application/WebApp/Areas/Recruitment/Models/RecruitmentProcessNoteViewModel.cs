﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RecruitmentProcessNoteViewModel")]
    public class RecruitmentProcessNoteViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(RecruitmentProcessPickerController))]
        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.RecruitmentProcess), typeof(RecruitmentProcessNoteResources))]
        [NonSortable]
        [ReadOnly(true)]
        [Required]
        [Render(Size = Size.Large)]
        public long RecruitmentProcessId { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.Content), typeof(RecruitmentProcessNoteResources))]
        [StringLength(5000)]
        [Multiline(3)]
        [NonSortable]
        [AllowHtml]
        [RichText]
        [Render(GridCssClass = "remaining-space-column")]
        public string Content { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.Content), typeof(RecruitmentProcessNoteResources))]
        [StringLength(5000)]
        [Required]
        [NonSortable]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RichNoteControl")]
        [AllowHtml]
        [HtmlSanitize]
        public string RichContent { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.CreatedOn), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public DateTime CreatedOn { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.CreatedBy), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column", Size = Size.Large)]
        [NonSortable]
        public string CreatedByFullName { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.NoteType), typeof(RecruitmentProcessNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteType NoteType { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.NoteVisibility), typeof(RecruitmentProcessNoteResources))]
        [RequiredEnum(typeof(NoteVisibility))]
        [RadioGroup(ItemProvider = typeof(BasicNoteVisibilityItemProvider))]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteVisibility NoteVisibility { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public long? RecruitmentProcessStepId { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
