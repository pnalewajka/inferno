﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessProgressReportParametersViewModelToProcessProgressReportParametersDto : ClassMapping<ProcessProgressReportParametersViewModel, ProcessProgressReportParametersDto>
    {
        public ProcessProgressReportParametersViewModelToProcessProgressReportParametersDto()
        {
            Mapping = v => new ProcessProgressReportParametersDto
            {
                CityIds = v.CityIds,
                JobProfileIds = v.JobProfileIds,
                RecruiterIds = v.RecruiterIds,
                RecruitmentProcessStepTypes = v.RecruitmentProcessStepTypes,
                JobOpeningIds = v.JobOpeningIds,
                AssignedEmployeeIds = v.AssignedEmployeeIds,
                OrgUnitIds = v.OrgUnitIds
            };
        }
    }
}