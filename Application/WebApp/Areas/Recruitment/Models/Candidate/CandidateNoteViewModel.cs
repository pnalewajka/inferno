﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    [Identifier("Models.CandidateNoteViewModel")]
    public class CandidateNoteViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=false'")]
        [DisplayNameLocalized(nameof(CandidateNoteResources.Candidate), typeof(CandidateNoteResources))]
        [Required]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public long CandidateId { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.Content), typeof(CandidateNoteResources))]
        [StringLength(5000)]
        [Render(GridCssClass = "remaining-space-column")]
        [Multiline(3)]
        [NonSortable]
        [AllowHtml]
        [RichText]
        public string Content { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateNoteResources.Content), typeof(CandidateNoteResources))]
        [StringLength(5000)]
        [Required]
        [NonSortable]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RichNoteControl")]
        [AllowHtml]
        [HtmlSanitize]
        public string RichContent { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.NoteType), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteType NoteType { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.NoteVisibility), typeof(CandidateNoteResources))]
        [RequiredEnum(typeof(NoteVisibility))]
        [RadioGroup(ItemProvider = typeof(BasicNoteVisibilityItemProvider))]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public NoteVisibility NoteVisibility { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.CreatedOn), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public DateTime CreatedOn { get; set; }

        [DisplayNameLocalized(nameof(CandidateNoteResources.CreatedBy), typeof(CandidateNoteResources))]
        [Visibility(VisibilityScope.Grid)]
        [ReadOnly(true)]
        [Render(GridCssClass = "no-break-column")]
        [NonSortable]
        public string CreatedByFullName { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
