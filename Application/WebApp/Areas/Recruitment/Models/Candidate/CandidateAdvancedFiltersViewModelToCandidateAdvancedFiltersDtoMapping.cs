﻿using System;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateAdvancedFiltersViewModelToCandidateAdvancedFiltersDtoMapping
         : ClassMapping<CandidateAdvancedFiltersViewModel, CandidateAdvancedFiltersDto>
    {
        public CandidateAdvancedFiltersViewModelToCandidateAdvancedFiltersDtoMapping()
        {
            Mapping = v => new CandidateAdvancedFiltersDto
            {
                CityIds = v.CityIds,
                JobProfileIds = v.JobProfileIds,
                CoordinatorIds = v.CoordinatorIds,
                EmployeeStatuses = v.EmployeeStatuses,
                LanguageId = v.LanguageWithLevelId.HasValue
                    ? LanguageWithLevelBusinessLogic.GetLanguageId(v.LanguageWithLevelId.Value)
                    : (long?)null,
                LanguageReferenceLevelId = v.LanguageWithLevelId.HasValue
                    ? LanguageWithLevelBusinessLogic.GetLevel(v.LanguageWithLevelId.Value)
                    : (LanguageReferenceLevel?)null,
                LastContactFrom = v.LastContactFrom.HasValue
                    ? v.LastContactFrom.Value.StartOfDay()
                    : (DateTime?)null,
                LastContactTo = v.LastContactTo.HasValue
                    ? v.LastContactTo.Value.EndOfDay()
                    : (DateTime?)null,
            };
        }
    }
}