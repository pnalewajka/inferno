﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateViewModelToCandidateDtoMapping : ClassMapping<CandidateViewModel, CandidateDto>
    {
        public CandidateViewModelToCandidateDtoMapping()
        {
            Mapping = v => new CandidateDto
            {
                Id = v.Id,
                FirstName = v.FirstName,
                LastName = v.LastName,
                ContactRestriction = v.ContactRestriction,
                MobilePhone = v.MobilePhone,
                OtherPhone = v.OtherPhone,
                EmailAddress = v.EmailAddress,
                OtherEmailAddress = v.OtherEmailAddress,
                SkypeLogin = v.SkypeLogin,
                SocialLink = v.SocialLink,
                NextFollowUpDate = v.NextFollowUpDate,
                EmployeeStatus = v.EmployeeStatus,
                CanRelocate = v.CanRelocate,
                RelocateDetails = v.RelocateDetails,
                CityIds = v.CityIds.ToArray(),
                JobProfileIds = v.JobProfileIds.ToArray(),
                Languages = v.LanguageWithLevelIds.Select(l => new CandidateLanguageDto
                {
                    LanguageId = LanguageWithLevelBusinessLogic.GetLanguageId(l),
                    Level = LanguageWithLevelBusinessLogic.GetLevel(l)
                }).ToList(),
                ResumeTextContent = v.ResumeTextContent,
                WatcherIds = v.WatcherIds.ToArray(),
                ConsentCoordinatorId = v.ConsentCoordinatorId,
                ContactCoordinatorId = v.ContactCoordinatorId,
                WorkflowAction = v.WorkflowAction,
                IsHighlyConfidential = v.IsHighlyConfidential,
                ShouldAddRelatedDataConsent = v.ShouldAddRelatedDataConsent,
                ConsentType = v.ConsentType,
                ConsentScope = v.ConsentScope,
                OriginalSourceId = v.OriginalSourceId,
                OriginalSourceComment =  v.OriginalSourceComment,
                CrmId = v.CrmId,
                Documents = v.ConsentFiles.Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    })
                    .Union(v.InboundEmailDocumentId == null ? new DocumentDto[0] : new[] { new DocumentDto { DocumentId = v.InboundEmailDocumentId } })
                    .ToArray(),
                Timestamp = v.Timestamp
            };
        }
    }
}
