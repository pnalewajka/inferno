﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    [Identifier("Models.CandidateFileDetailsViewModel")]
    public class CandidateFileDetailsViewModel
    {
        public long DocumentId { get; set; }

        public string Comment { get; set; }

        public string DocumentUrl { get; set; }

        public string DocumentName { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public RecruitmentDocumentType DocumentType { get; set; }
    }
}