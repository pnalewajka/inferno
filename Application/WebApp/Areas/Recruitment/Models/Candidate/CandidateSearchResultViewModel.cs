﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateSearchResultViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [SortBy(nameof(FullName))]
        [DisplayNameLocalized(nameof(CandidateResources.FullName), typeof(CandidateResources))]
        public DisplayUrl GetFullName(UrlHelper helper)
        {
            return new DisplayUrl
            {
                Label = FullName,
                Action = helper.UriActionGet<CandidateController>(c => c.View(default(long)), KnownParameter.None, nameof(Id), Id.ToString())
            };
        }

        [Order(1)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.FirstName), typeof(CandidateResources))]
        [MaxLength(255)]
        public string FirstName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.LastName), typeof(CandidateResources))]
        [MaxLength(255)]
        public string LastName { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateResources.MobilePhone), typeof(CandidateResources))]
        [MaxLength(20)]
        public string MobilePhone { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.EmailAddress), typeof(CandidateResources))]
        [MaxLength(255)]
        public string EmailAddress { get; set; }

        [Order(5)]
        [SortBy(nameof(EmailAddress))]
        [DisplayNameLocalized(nameof(CandidateResources.EmailAddress), typeof(CandidateResources))]
        public DisplayUrl GetEmailAddress(UrlHelper helper) => CellContentHelper.GetEmailLink(EmailAddress, "candidate-record-contact-email");

        [Order(6)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.SkypeLogin), typeof(CandidateResources))]
        [MaxLength(255)]
        public string SkypeLogin { get; set; }

        [Order(7)]
        [SortBy(nameof(SkypeLogin))]
        [DisplayNameLocalized(nameof(CandidateResources.SkypeLogin), typeof(CandidateResources))]
        public DisplayUrl GetSkypeLogin(UrlHelper helper) => CellContentHelper.GetSkypeLink(SkypeLogin, SkypeLogin, "candidate-record-contact-skype");

        [Order(8)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.SocialLink), typeof(CandidateResources))]
        [MaxLength(255)]
        public string SocialLink { get; set; }

        [Visibility(VisibilityScope.None)]
        public string FullName { get; set; }

        public byte[] Timestamp { get; set; }

        public CandidateSearchResultViewModel()
        {
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
