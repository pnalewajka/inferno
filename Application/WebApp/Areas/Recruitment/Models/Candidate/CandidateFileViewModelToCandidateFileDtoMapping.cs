﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateFileViewModelToCandidateFileDtoMapping : ClassMapping<CandidateFileViewModel, CandidateFileDto>
    {
        public CandidateFileViewModelToCandidateFileDtoMapping()
        {
            Mapping = v => new CandidateFileDto
            {
                Id = v.Id,
                CandidateId = v.CandidateId,
                DocumentType = v.DocumentType.Value,
                Comment = v.Comment,
                File = new DocumentDto[]
                {
                    new DocumentDto
                    {
                        ContentType = v.File.ContentType,
                        DocumentName = v.File.DocumentName,
                        DocumentId = v.File.DocumentId,
                        TemporaryDocumentId = v.File.TemporaryDocumentId
                    }
                },
                FileSource = FileSource.Disk,
                IsDataConsent = v.IsDataConsent,
                Timestamp = v.Timestamp
            };
        }
    }
}
