﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    [Identifier("Filters.CandidatePropertiesFilter")]
    [FieldSetDescription(1, GeneralFieldSet)]
    [FieldSetDescription(2, LastContactFieldSet, nameof(CandidateResources.LastContactFieldSetText), typeof(CandidateResources))]
    [DescriptionLocalized(nameof(CandidateResources.CandidatePropertiesFilter), typeof(CandidateResources))]

    public class CandidateAdvancedFiltersViewModel
    {
        private const string GeneralFieldSet = "General";
        private const string LastContactFieldSet = "LastContact";

        [Order(1)]
        [DisplayNameLocalized(nameof(CandidateResources.EmployeeStatusShort), typeof(CandidateResources))]
        [CheckboxGroup(ItemProvider = typeof(EnumBasedListItemProvider<CandidateEmploymentStatus>))]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long[] EmployeeStatuses { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(CandidateResources.City), typeof(CandidateResources))]
        [ValuePicker(Type = typeof(CityPickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [NonSortable]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long[] CityIds { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(CandidateResources.Profile), typeof(CandidateResources))]
        [ValuePicker(Type = typeof(JobProfilePickerController), OnResolveUrlJavaScript = "url=url +'&should-exclude=true'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [NonSortable]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long[] JobProfileIds { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(CandidateResources.Language), typeof(CandidateResources))]
        [ValuePicker(Type = typeof(LanguageWithLevelPickerController))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long? LanguageWithLevelId { get; set; }

        [Order(5)]
        [DisplayNameLocalized(nameof(CandidateResources.Recruiter), typeof(CandidateResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters&should-exclude=true'")]
        [JsonConverter(typeof(ArrayToUrlConverter))]
        [FieldSet(GeneralFieldSet)]
        [Render(Size = Size.Large)]
        public long[] CoordinatorIds { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(CandidateResources.From), typeof(CandidateResources))]
        [FieldSet(LastContactFieldSet)]
        public DateTime? LastContactFrom { get; set; }

        [Order(7)]
        [DisplayNameLocalized(nameof(CandidateResources.To), typeof(CandidateResources))]
        [FieldSet(LastContactFieldSet)]
        public DateTime? LastContactTo { get; set; }

        public override string ToString()
        {
            return CandidateResources.CandidatePropertiesFilter;
        }
    }
}