﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateDtoToCandidateViewModelMapping : ClassMapping<CandidateDto, CandidateViewModel>
    {
        public CandidateDtoToCandidateViewModelMapping(IClassMappingFactory mappingFactory, ITimeService timeService)
        {
            Mapping = d => new CandidateViewModel
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                ContactRestriction = d.ContactRestriction,
                MobilePhone = d.MobilePhone,
                OtherPhone = d.OtherPhone,
                EmailAddress = d.EmailAddress,
                OtherEmailAddress = d.OtherEmailAddress,
                SkypeLogin = d.SkypeLogin,
                SocialLink = d.SocialLink,
                NextFollowUpDate = d.NextFollowUpDate,
                EmployeeStatus = d.EmployeeStatus,
                CanRelocate = d.CanRelocate,
                RelocateDetails = d.RelocateDetails,
                CityIds = d.CityIds.ToList(),
                CityNames = d.CityNames.ToList(),
                JobProfileIds = d.JobProfileIds.ToList(),
                JobProfileNames = d.JobProfileNames.ToList(),
                LanguageWithLevelIds = d.Languages.Select(l => LanguageWithLevelBusinessLogic.GetId(l.LanguageId, l.Level)).ToList(),
                LanguageNames = d.Languages.Select(l => $"{l.Language.Name} {l.Level}").ToList(),
                WatcherIds = d.WatcherIds.ToList(),
                ResumeTextContent = d.ResumeTextContent,
                ContactCoordinatorId = d.ContactCoordinatorId,
                ConsentCoordinatorId = d.ConsentCoordinatorId,
                HasValidConsent = d.HasValidConsent,
                LastContactBy = d.LastContactBy,
                LastContactOn = d.LastContactOn,
                LastContactType = d.LastContactType,
                IsAnonymized = d.IsAnonymized,
                TechnicalReviewStepInfos = d.TechnicalReviewStepInfo
                    .Select(mappingFactory.CreateMapping<RecruitmentProcessStepInfoDto, RecruitmentProcessStepInfoViewModel>().CreateFromSource)
                    .ToList(),
                TimelineDetails = d.FileDetails.Select(de => new TimelineDetailViewModel
                {
                    Details = string.Format(CandidateResources.NewFileDescription, de.DocumentName, de.Comment),
                    Header = TimelineDetailType.File.GetDescription(null),
                    HappenedOn = de.CreatedOn,
                    CreatedByFullName = de.CreatedByFullName
                }).Concat(
                    d.Notes == null
                    ? new List<TimelineDetailViewModel>()
                    : d.Notes.Select(de => new TimelineDetailViewModel
                    {
                        Details = de.Content != null ? MentionItemHelper.ReplaceTokens(de.Content, i => i.Label) : string.Empty,
                        Header = TimelineDetailType.CandidateNote.GetDescription(null),
                        HappenedOn = de.CreatedOn,
                        CreatedByFullName = de.CreatedByFullName,
                        RenderAsHtml = true
                    })).Concat(
                 d.ContactRecordDetails.Select(de => new TimelineDetailViewModel
                 {
                     Details = de.Comment,
                     Header = TimelineDetailType.ContactRecord.GetDescription(null),
                     HappenedOn = de.ContactedOn,
                     CreatedByFullName = de.CreatedByFullName
                 })).Concat(
                 d.JobApplicationDetails.Select(de => new TimelineDetailViewModel
                 {
                     Details = string.IsNullOrEmpty(de.CandidateComment)
                        ? string.Format(CandidateResources.JobApplicationDescription, de.OriginName, de.OriginComment, string.Empty)
                        : string.Format(CandidateResources.JobApplicationDescription, de.OriginName, de.OriginComment, string.Format(CandidateResources.CandidateRemarks, de.CandidateComment)),
                     Header = TimelineDetailType.JobApplication.GetDescription(null),
                     HappenedOn = de.CreatedOn,
                     CreatedByFullName = de.CreatedByFullName
                 })).Concat(
                 d.RecruitmentProcessStepDetails.Select(de => new TimelineDetailViewModel
                 {
                     Header = de.Type.GetDescription(null),
                     Details = string.IsNullOrEmpty(de.Comment)
                        ? string.Format(CandidateResources.ProcessStepDescription, de.Decision, string.Empty)
                        : string.Format(CandidateResources.ProcessStepDescription, de.Decision, string.Format(CandidateResources.ProcessStepComment, de.Comment)),
                     HappenedOn = de.CreatedOn,
                     CreatedByFullName = de.CreatedByFullName
                 })).Concat(
                 d.TechnicalReviewDetails.Select(de => new
                 {
                     DetailList = new Dictionary<string, string>
                     {
                         {CandidateResources.SeniorityLevelAssessment, de.SeniorityLevelAssessment.ToString() },
                         {CandidateResources.SeniorityLevelExpectation, de.SeniorityLevelExpectation.ToString() },
                         {CandidateResources.JobProfileNames, string.Join(", ", de.JobProfileNames) },
                         {CandidateResources.Decision, de.Decision.ToString() },
                         {CandidateResources.TechnicalAssignmentAssessment, de.TechnicalAssignmentAssessment },
                         {CandidateResources.SolutionDesignAssessment, de.SolutionDesignAssessment },
                         {CandidateResources.SoftwareEngineeringAssessment, de.TechnicalKnowledgeAssessment },
                         {CandidateResources.ExperienceAssessment, de.TeamProjectSuitabilityAssessment },
                         {CandidateResources.EnglishUsageAssessment, de.EnglishUsageAssessment },
                         {CandidateResources.RiskAssessment, de.RiskAssessment },
                         {CandidateResources.StrengthAssessment, de.StrengthAssessment },
                         {CandidateResources.OtherRemarks, de.OtherRemarks }
                     },
                     HappenedOn = de.CreatedOn,
                     de.CreatedByFullName
                 }).Select(ga => new TimelineDetailViewModel
                 {
                     Details = string.Join(Environment.NewLine, ga.DetailList.Where(details => !string.IsNullOrEmpty(details.Value)).Select(details => $"{details.Key}: {details.Value}")),
                     Header = TimelineDetailType.TechnicalReview.GetDescription(null),
                     CreatedByFullName = ga.CreatedByFullName,
                     HappenedOn = ga.HappenedOn
                 }))
                 .Concat(new[]
                 {
                     new TimelineDetailViewModel
                     {
                         CreatedByFullName = d.CreatedByFullName,
                         HappenedOn = d.CreatedOn,
                         Details = CandidateResources.RecordCreated,
                     },
                     new TimelineDetailViewModel
                     {
                         CreatedByFullName = d.ModifiedByFullName,
                         HappenedOn = d.ModifiedOn,
                         Details = CandidateResources.RecordModified
                     }
                 })
                .OrderByDescending(de => de.HappenedOn)
                .ToList(),
                IsHighlyConfidential = d.IsHighlyConfidential,
                LastCompanyResumeDocument = d.LastCompanyResumeDocumentId == null ? null : new DocumentViewModel { DocumentId = d.LastCompanyResumeDocumentId, DocumentName = EnumResources.CompanyResume },
                LastOriginalResumeDocument = d.LastOriginalResumeDocumentId == null ? null : new DocumentViewModel { DocumentId = d.LastOriginalResumeDocumentId, DocumentName = EnumResources.OriginalResume },
                CurrentConsents = d.Consents.IsNullOrEmpty() ? null : mappingFactory.CreateMapping<CandidateDataConsentDto, CandidateDataConsentViewModel>().CreateFromSource(d.Consents.Where(c =>
                    DataConsentBusinessLogic.IsNotExpired.Call(c.ExpiresOn, timeService.GetCurrentDate()))).ToList(),
                Timestamp = d.Timestamp,
                IsWatchedByCurrentUser = d.IsWatchedByCurrentUser,
                MostRecentProcess = d.LastProcessInfo != null ? mappingFactory.CreateMapping<RecruitmentProcessInfoDto, RecruitmentProcessInfoViewModel>().CreateFromSource(d.LastProcessInfo) : null,
                Notes = d.Notes != null ? d.Notes.Select(mappingFactory.CreateMapping<CandidateNoteDto, CandidateNoteViewModel>().CreateFromSource).ToList() : null,
                OriginalSourceId = d.OriginalSourceId,
                OriginalSourceComment = d.OriginalSourceComment,
                CrmId = d.CrmId,
                ConsentFiles = (d.Documents ?? new DocumentDto[0])
                    .Select(p => new DocumentViewModel
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.DocumentName,
                        DocumentId = p.DocumentId,
                        TemporaryDocumentId = p.TemporaryDocumentId,
                    })
                    .ToList(),
                CandidateFileDetails = d.FileDetails.IsNullOrEmpty()
                    ? new List<CandidateFileDetailsViewModel>()
                    : mappingFactory.CreateMapping<CandidateFileDetailsDto, CandidateFileDetailsViewModel>()
                        .CreateFromSource(d.FileDetails)
                        .ToList(),
                IsFavoriteCandidate = d.IsFavoriteCandidate,
                LastActivitytBy = d.RecentActivity == null ? null : d.RecentActivity.UserFullName,
                LastActivitytOn = d.RecentActivity == null ? (DateTime?)null : d.RecentActivity.OccuredOn,
                RecentActivity = d.RecentActivity != null ? mappingFactory.CreateMapping<RecentActivityDto, RecentActivityViewModel>().CreateFromSource(d.RecentActivity) : null,
                ReferenceType = d.RecentActivity == null ? (ReferenceType?)null : d.RecentActivity.ReferenceType
            };
        }
    }
}
