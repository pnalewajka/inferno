﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateDataConsentViewModel
    {
        public string DataAdministrator { get; set; }

        public DataConsentType ConsentType { get; set; }

        public string Scope { get; set; }

        public DateTime? ExpiresOn { get; set; }
    }
}