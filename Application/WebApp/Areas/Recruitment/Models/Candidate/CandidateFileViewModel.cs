﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    [Identifier("Models.CandidateFileViewModel")]
    public class CandidateFileViewModel
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=false'")]
        [DisplayNameLocalized(nameof(CandidateFileResources.Candidate), typeof(CandidateFileResources))]
        [Required]
        [Render(Size = Size.Large)]
        public long CandidateId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateFileResources.File), typeof(CandidateFileResources))]
        [DocumentUpload(typeof(CandidateFileDocumentMapping), DownloadUrlTemplate = "/Recruitment/CandidateFile/ViewDocument/{1}", ShouldUseDownloadDialog = true)]
        [Required]
        [Render(Size = Size.Large)]
        public DocumentViewModel File { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateFileResources.DocumentType), typeof(CandidateFileResources))]
        [RadioGroup(EmptyItemBehavior = EmptyItemBehavior.PresentOnlyOnInit, ItemProvider = typeof(EnumBasedListItemProvider<RecruitmentDocumentType>))]
        [Render(ControlCssClass = "align-options-200")]
        [Required]
        public RecruitmentDocumentType? DocumentType { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateFileResources.Comment), typeof(CandidateFileResources))]
        [Multiline(3)]
        [StringLength(100)]
        public string Comment { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsFileSupportedByViewerJS { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(CandidateFileResources.ModifiedOn), typeof(CandidateFileResources))]
        [StringFormat("{0:d}")]
        [Render(GridCssClass = "date-column")]
        public DateTime ModifiedOn { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsDataConsent { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return File.DocumentName;
        }

        public CandidateFileViewModel()
        {
        }
    }
}
