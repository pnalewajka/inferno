﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateNoteDtoToCandidateNoteViewModelMapping : ClassMapping<CandidateNoteDto, CandidateNoteViewModel>
    {
        public CandidateNoteDtoToCandidateNoteViewModelMapping()
        {
            Mapping = d => new CandidateNoteViewModel
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content != null ? MentionItemHelper.ReplaceTokens(d.Content, i => i.Label) : string.Empty,
                RichContent = d.Content,
                CreatedOn = d.CreatedOn,
                CreatedByFullName = d.CreatedByFullName,
                Timestamp = d.Timestamp
            };
        }
    }
}
