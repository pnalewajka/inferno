﻿using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.WebApp.Areas.GDPR.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateFileDetailsDtoToCandidateFileDetailsViewModel : ClassMapping<CandidateFileDetailsDto, CandidateFileDetailsViewModel>
    {
        public CandidateFileDetailsDtoToCandidateFileDetailsViewModel()
        {
            Mapping = d => new CandidateFileDetailsViewModel
            {
                DocumentId = d.Id,
                DocumentName = d.DocumentName,
                DocumentType = d.DocumentType,
                ModifiedOn = d.ModifiedOn,
                DocumentUrl = GetDocumentUrl(d.CandidateFileOrigin, d.Id)
            };
        }

        private string GetDocumentUrl(CandidataFileOrigin candidataFileOrigin, long documentId)
        {
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            var uriAction = candidataFileOrigin == CandidataFileOrigin.DataConsentFile
                    ? urlHelper.UriActionGet(nameof(DataConsentController.ViewFile), RoutingHelper.GetControllerName<DataConsentController>(), nameof(GDPR),
                        KnownParameter.None, "id", documentId.ToString())
                    : urlHelper.UriActionGet(nameof(CandidateFileController.ViewFile), RoutingHelper.GetControllerName<CandidateFileController>(), nameof(Recruitment),
                        KnownParameter.None, "id", documentId.ToString());

            return uriAction.Url;
        }
    }
}