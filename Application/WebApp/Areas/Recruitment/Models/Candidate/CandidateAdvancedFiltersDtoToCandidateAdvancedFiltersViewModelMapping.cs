﻿using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateAdvancedFiltersDtoToCandidateAdvancedFiltersViewModelMapping
         : ClassMapping<CandidateAdvancedFiltersDto, CandidateAdvancedFiltersViewModel>
    {
        public CandidateAdvancedFiltersDtoToCandidateAdvancedFiltersViewModelMapping()
        {
            Mapping = v => new CandidateAdvancedFiltersViewModel
            {
                CityIds = v.CityIds,
                JobProfileIds = v.JobProfileIds,
                CoordinatorIds = v.CoordinatorIds,
                EmployeeStatuses = v.EmployeeStatuses,
                LanguageWithLevelId = v.LanguageId.HasValue && v.LanguageReferenceLevelId.HasValue
                    ? LanguageWithLevelBusinessLogic.GetId(v.LanguageId.Value, v.LanguageReferenceLevelId.Value)
                    : (long?)null,
                LastContactFrom = v.LastContactFrom,
                LastContactTo = v.LastContactTo
            };
        }
    }
}