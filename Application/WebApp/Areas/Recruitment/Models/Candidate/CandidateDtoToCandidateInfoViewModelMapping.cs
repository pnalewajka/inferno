﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateDtoToCandidateInfoViewModelMapping : ClassMapping<CandidateDto, CandidateInfoViewModel>
    {
        public CandidateDtoToCandidateInfoViewModelMapping(IClassMapping<RecruitmentProcessInfoDto, RecruitmentProcessInfoViewModel> recruitmentProcessInfoDtoClassMapping)
        {
            Mapping = d => new CandidateInfoViewModel
            {
                Id = d.Id,
                FullName = d.FullName,
                MostRecentProcess = d.LastProcessInfo != null ? recruitmentProcessInfoDtoClassMapping.CreateFromSource(d.LastProcessInfo) : null,
            };
        }
    }
}