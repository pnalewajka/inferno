﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.GDPR.DocumentMappings;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Helpers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    [Identifier("Models.CandidateViewModel")]
    [TabDescription(1, InformationTab, nameof(CandidateResources.InformationTabText), typeof(CandidateResources))]
    [TabDescription(2, ResumeTab, nameof(CandidateResources.ResumeTabText), typeof(CandidateResources))]
    [TabDescription(3, NotesTab, nameof(NotesTabLabel), SecurityRoleType.CanViewRecruitmentCandidateNotes)]
    [TabDescription(4, TimelineTab, nameof(TimelineTabLabel))]
    [TabDescription(5, FilesTab, nameof(FilesTabLabel), SecurityRoleType.CanViewRecruitmentCandidateFile)]
    [TabDescription(6, TechnicalReviewStepsTab, nameof(TechnicalReviewStepsTabLabel))]
    [TabDescription(8, RecruitmentProcessesTab, nameof(CandidateResources.RecruitmentProcessTab), typeof(CandidateResources))]
    [FieldSetDescription(1, GeneralFieldSet)]
    [FieldSetDescription(2, DetailsFieldSet, nameof(CandidateResources.DetailsFieldSetText), typeof(CandidateResources))]
    [FieldSetDescription(3, ContactFieldSet, nameof(CandidateResources.ContactFieldSetText), typeof(CandidateResources))]
    [FieldSetDescription(4, SourceFieldSet, nameof(CandidateResources.SourceFieldSetText), typeof(CandidateResources))]
    public class CandidateViewModel : IGridViewRow, IValidatableObject
    {
        private const string InformationTab = "Information";
        private const string ResumeTab = "Resume";
        private const string ContactActivitiesTab = "ContactActivities";
        private const string NotesTab = "Notes";
        private const string TimelineTab = "Timeline";
        private const string FilesTab = "Files";
        private const string GeneralFieldSet = "General";
        private const string DetailsFieldSet = "Details";
        private const string ContactFieldSet = "Contact";
        private const string SourceFieldSet = "Source";
        private const string TechnicalReviewStepsTab = "steps";
        private const string RecruitmentProcessesTab = "processes";

        public long Id { get; set; }

        [Order(1)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateResources.FirstName), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [Required]
        [MaxLength(255)]
        [Trim]
        [FieldSet(GeneralFieldSet)]
        public string FirstName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateResources.LastName), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [Required]
        [MaxLength(255)]
        [Trim]
        [FieldSet(GeneralFieldSet)]
        public string LastName { get; set; }

        [Order(3)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.ContactRestriction), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [FieldSet(GeneralFieldSet)]
        public ContactRestriction ContactRestriction { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateResources.MobilePhone), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [MaxLength(50)]
        [FieldSet(GeneralFieldSet)]
        public string MobilePhone { get; set; }

        [Order(5)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.OtherPhone), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [MaxLength(50)]
        [FieldSet(GeneralFieldSet)]
        public string OtherPhone { get; set; }

        [Order(6)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.EmailAddress), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [Required]
        [MaxLength(255)]
        [FieldSet(GeneralFieldSet)]
        public string EmailAddress { get; set; }

        [Order(7)]
        [SortBy(nameof(EmailAddress))]
        [DisplayNameLocalized(nameof(CandidateResources.ContactLabel), typeof(CandidateResources))]
        [Render(GridCssClass = "dot-separated")]
        public IEnumerable<DisplayUrl> GetContactLinks(UrlHelper helper)
        {
            return GetContactSources();
        }

        [Order(8)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.OtherEmailAddress), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [EmailAddress]
        [MaxLength(255)]
        [FieldSet(GeneralFieldSet)]
        public string OtherEmailAddress { get; set; }

        [Order(9)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.SkypeLogin), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [RegularExpression(@"^[a-zA-Z0-9\.,\-_\:]*$", ErrorMessageResourceName = "SkypeErrorMessage", ErrorMessageResourceType = typeof(CandidateResources))]
        [MinLength(5)]
        [MaxLength(255)]
        [FieldSet(GeneralFieldSet)]
        public string SkypeLogin { get; set; }

        [Order(11)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.SocialLink), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [MaxLength(255)]
        [FieldSet(GeneralFieldSet)]
        public string SocialLink { get; set; }

        [Order(12)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.NextFollowUpDate), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        public DateTime? NextFollowUpDate { get; set; }

        [Order(13)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.EmployeeStatus), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [FieldSet(DetailsFieldSet)]
        public CandidateEmploymentStatus EmployeeStatus { get; set; }

        [Order(14)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateResources.CityIds), nameof(CandidateResources.CityIdsShort), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [ValuePicker(Type = typeof(CityPickerController))]
        [NonSortable]
        [FieldSet(DetailsFieldSet)]
        public List<long> CityIds { get; set; }

        [Order(15)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.CanRelocate), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [OnValueChange(ClientHandler = "Candidate.OnCanRelocateChanged", ExecuteOnLoad = true)]
        [FieldSet(DetailsFieldSet)]
        public bool CanRelocate { get; set; }

        [Order(16)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.RelocateDetails), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [MaxLength(1023)]
        [FieldSet(DetailsFieldSet)]
        public string RelocateDetails { get; set; }

        [Order(17)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(CandidateResources.JobProfileIds), nameof(CandidateResources.JobProfileIdsShort), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [ValuePicker(Type = typeof(JobProfilePickerController))]
        [NonSortable]
        [FieldSet(DetailsFieldSet)]
        public List<long> JobProfileIds { get; set; }

        [Order(18)]
        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.LanguageWithLevelIds), typeof(CandidateResources))]
        [ValuePicker(Type = typeof(LanguageWithLevelPickerController))]
        [FieldSet(DetailsFieldSet)]
        public List<long> LanguageWithLevelIds { get; set; }

        [Order(19)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.CvFullText), typeof(CandidateResources))]
        [Tab(ResumeTab)]
        [Multiline(8)]
        public string ResumeTextContent { get; set; }

        [Order(20)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.ContactCoordinatorId), typeof(CandidateResources))]
        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        public long ContactCoordinatorId { get; set; }

        [Order(21)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.ConsentCoordinatorId), typeof(CandidateResources))]
        [Required]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruiters'")]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        public long ConsentCoordinatorId { get; set; }

        [Order(22)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.WatcherIds), typeof(CandidateResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-watcher'")]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        public List<long> WatcherIds { get; set; }

        [Order(23)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.Timeline")]
        [Tab(TimelineTab)]
        public List<TimelineDetailViewModel> TimelineDetails { get; set; }

        [Order(24)]
        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        [DisplayNameLocalized(nameof(CandidateResources.LastContactBy), typeof(CandidateResources))]
        [ReadOnly(true)]
        [Render(Size = Size.Medium)]
        public string LastContactBy { get; set; }

        [Order(25)]
        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        [DisplayNameLocalized(nameof(CandidateResources.LastContactOn), nameof(CandidateResources.LastContactOnGrid), typeof(CandidateResources))]
        [ReadOnly(true)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        public DateTime? LastContactOn { get; set; }

        [Order(26)]
        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [FieldSet(ContactFieldSet)]
        [DisplayNameLocalized(nameof(CandidateResources.LastContactVia), typeof(CandidateResources))]
        [ReadOnly(true)]
        [Render(Size = Size.Medium)]
        public ContactType? LastContactType { get; set; }

        [Order(27)]
        [Tab(InformationTab)]
        [Visibility(VisibilityScope.Form)]
        [ReadOnly(true)]
        [DisplayNameLocalized(nameof(CandidateResources.IsAnonymized), typeof(CandidateResources))]
        public bool IsAnonymized { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(TechnicalReviewStepsTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RecruitmentProcessStepTab")]
        [ReadOnly(true)]
        public List<RecruitmentProcessStepInfoViewModel> TechnicalReviewStepInfos { get; set; }

        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.IsHighlyConfidential), typeof(CandidateResources))]
        [Visibility(VisibilityScope.Form)]
        [FieldSet(DetailsFieldSet)]
        public bool IsHighlyConfidential { get; set; }

        [HiddenInput]
        [Visibility(VisibilityScope.Form)]
        public CandidateWorkflowAction? WorkflowAction { get; set; }

        [SortBy(nameof(RecentActivity))]
        [DisplayNameLocalized(nameof(CandidateResources.MostRecentActivity), typeof(CandidateResources))]
        public DisplayUrl GetMostRecentActivity(UrlHelper helper)
        {
            return RecentActivityUrlHelper.GetDisplayUrl(helper, Id, RecentActivity, MostRecentProcess?.ToString());
        }

        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateDocumentUrl")]
        [Tab(InformationTab)]
        public DocumentViewModel LastOriginalResumeDocument { get; set; }

        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewRecruitmentCandidateFile)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateDocumentUrl")]
        [Tab(InformationTab)]
        public DocumentViewModel LastCompanyResumeDocument { get; set; }

        [Visibility(VisibilityScope.Form)]
        [ValuePicker(Type = typeof(ApplicationOriginPickerController))]
        [Required]
        [DisplayNameLocalized(nameof(CandidateResources.OriginalSource), typeof(CandidateResources))]
        [Tab(InformationTab)]
        [FieldSet(SourceFieldSet)]
        public long OriginalSourceId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(CandidateResources.OriginalSourceComment), typeof(CandidateResources))]
        [StringLength(400)]
        [Tab(InformationTab)]
        [FieldSet(SourceFieldSet)]
        public string OriginalSourceComment { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(NotesTab)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateNotes")]
        [ReadOnly(true)]
        public List<CandidateNoteViewModel> Notes { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.ShouldAddRelatedDataConsent), typeof(RecommendingPersonResources))]
        [OnValueChange(ClientHandler = "Candidate.OnShouldAddRelatedDataConsentChanged", ExecuteOnLoad = true)]
        public bool ShouldAddRelatedDataConsent { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(DataConsentResources.DataConsentType), typeof(DataConsentResources))]
        [RadioGroup(ItemProvider = typeof(CandidateConsentTypeItemProvider), EmptyItemBehavior = EmptyItemBehavior.NotAvailable)]
        [OnValueChange(ClientHandler = "Candidate.OnConsentTypeChanged")]
        public DataConsentType? ConsentType { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(CandidateResources.ConsentFiles), typeof(CandidateResources))]
        [DocumentUpload(typeof(DataConsentDocumentMapping))]
        public List<DocumentViewModel> ConsentFiles { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [ValuePicker(Type = typeof(InboundEmailDocumentPickerController), OnResolveUrlJavaScript = "url += ($('#inbound-email-id').val() ? '&inbound-email-id=' + $('#inbound-email-id').val() : '&assigned-only=true')")]
        [DisplayNameLocalized(nameof(CandidateResources.InboundEmailDocument), typeof(CandidateResources))]
        [Render(Size = Size.Large)]
        public long? InboundEmailDocumentId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [Tab(InformationTab)]
        [DisplayNameLocalized(nameof(DataConsentResources.Scope), typeof(DataConsentResources))]
        public string ConsentScope { get; set; }

        [HiddenInput]
        [Tab(InformationTab)]
        [Visibility(VisibilityScope.Form)]
        public long? InboundEmailId { get; set; }

        [Tab(InformationTab)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateCurrentConsents")]
        [FieldSet(SourceFieldSet)]
        [ReadOnly(true)]
        public List<CandidateDataConsentViewModel> CurrentConsents { get; set; }

        [Tab(FilesTab)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateFiles")]
        public IList<CandidateFileDetailsViewModel> CandidateFileDetails { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(CandidateResources.LastActivityBy), typeof(CandidateResources))]
        public string LastActivitytBy { get; set; }

        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(CandidateResources.LastActivityOn), typeof(CandidateResources))]
        [Render(GridCssClass = "date-time-column")]
        public DateTime? LastActivitytOn { get; set; }

        [Tab(RecruitmentProcessesTab)]
        [Visibility(VisibilityScope.Form)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.CandidateRecruitmentProcesses")]
        public object Processes { get; set; }

        [Visibility(VisibilityScope.None)]
        public RecentActivityViewModel RecentActivity { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsFavoriteCandidate { get; set; }

        [Visibility(VisibilityScope.None)]
        public Guid? CrmId { get; set; }

        [Visibility(VisibilityScope.None)]
        public ReferenceType? ReferenceType { get; set; }

        [Visibility(VisibilityScope.None)]
        public long? ReferenceId { get; set; }

        [Visibility(VisibilityScope.None)]
        public string FullName => $"{FirstName} {LastName}".Trim();

        [Visibility(VisibilityScope.None)]
        public RecruitmentProcessInfoViewModel MostRecentProcess { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool IsWatchedByCurrentUser { get; set; }

        [Visibility(VisibilityScope.None)]
        public bool HasValidConsent { get; set; }

        [Visibility(VisibilityScope.None)]
        public List<string> JobProfileNames { get; set; }

        [Visibility(VisibilityScope.None)]
        public List<string> CityNames { get; set; }

        [Visibility(VisibilityScope.None)]
        public List<string> LanguageNames { get; set; }

        public byte[] Timestamp { get; set; }

        public CandidateViewModel()
        {
            CityIds = new List<long>();
            JobProfileIds = new List<long>();
            LanguageWithLevelIds = new List<long>();
            WatcherIds = new List<long>();
            CurrentConsents = new List<CandidateDataConsentViewModel>();
            TimelineDetails = new List<TimelineDetailViewModel>();
            ConsentFiles = new List<DocumentViewModel>();
            ShouldAddRelatedDataConsent = false;
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!Uri.IsWellFormedUriString(EmailAddress, UriKind.RelativeOrAbsolute))
            {
                yield return new ValidationResult(CandidateResources.InvalidEmailAddressValidationMessage, new[] { nameof(EmailAddress) });
            }

            if (OriginalSourceId == default(long))
            {
                yield return new ValidationResult(CandidateResources.OriginalSourceValidationMessage, new[] { nameof(OriginalSourceId) });
            }

            if (!(Uri.IsWellFormedUriString(EmailAddress, UriKind.Absolute) || ValidationRuleHelper.IsEmail(EmailAddress)))
            {
                yield return new ValidationResult(CandidateResources.EmailAddressErrorMessage, new[] { nameof(EmailAddress) });
            }

            if (!(string.IsNullOrEmpty(SocialLink) || Uri.IsWellFormedUriString(SocialLink, UriKind.Absolute)))
            {
                yield return new ValidationResult(CandidateResources.SocialLinkErrorMessage, new[] { nameof(SocialLink) });
            }

            if (!ShouldAddRelatedDataConsent)
            {
                yield break;
            }

            if (DataConsentBusinessLogic.DoesConsentTypeRequireAttachment.Call(ConsentType))
            {
                if (ConsentFiles.Count == 0 && InboundEmailDocumentId == null)
                {
                    yield return new ValidationResult(DataConsentResources.CandidateConsentAttachmentError, new[] { nameof(ConsentFiles), nameof(InboundEmailDocumentId) });
                }
            }

            if (!ConsentType.HasValue)
            {
                yield return new ValidationResult(DataConsentResources.ConsentTypeRequiredError, new[] { nameof(ConsentType) });
            }

            if (DataConsentBusinessLogic.DoesConsentTypeRequireScope.Call(ConsentType)
                && string.IsNullOrWhiteSpace(ConsentScope))
            {
                yield return new ValidationResult(
                    string.Format(DataConsentResources.ValidationEmptyError,
                        DataConsentResources.Scope,
                        DataConsentResources.DataConsentType,
                        ConsentType.Value.GetDescriptionOrValue()),
                    new[] { nameof(ConsentScope) });
            }
        }

        [Visibility(VisibilityScope.None)]
        public string NotesTabLabel =>
            $"{CandidateResources.NotesTabText} ({Notes?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string TimelineTabLabel =>
            $"{CandidateResources.TimelineTabText} ({TimelineDetails?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string FilesTabLabel =>
            $"{CandidateResources.FilesTabText} ({CandidateFileDetails?.Count ?? 0})";

        [Visibility(VisibilityScope.None)]
        public string TechnicalReviewStepsTabLabel =>
            $"{CandidateResources.TechnicalReviewStepsTab} ({TechnicalReviewStepInfos?.Count ?? 0})";

        string IGridViewRow.RowTitle => HasValidConsent ? null : CandidateResources.HasNoValidConsents;

        string IGridViewRow.RowCssClass
        {
            get
            {
                var cssClasses = string.Empty;

                if (!HasValidConsent)
                {
                    cssClasses = $"{cssClasses} {CardIndexStyles.RowClassDanger}";
                }

                if (IsFavoriteCandidate)
                {
                    cssClasses = $"{cssClasses} {RecruitmentStyles.RowClassCandidateFavorite}";
                }

                return cssClasses;
            }
        }

        private IEnumerable<DisplayUrl> GetContactSources()
        {
            yield return CellContentHelper.GetEmailOrPageLink(EmailAddress, string.Empty, "candidate-record-contact-email", "candidate-record-contact-social");

            if (SkypeLogin != null)
            {
                yield return CellContentHelper.GetSkypeLink(SkypeLogin, string.Empty, "candidate-record-contact-skype");
            }
        }
    }
}