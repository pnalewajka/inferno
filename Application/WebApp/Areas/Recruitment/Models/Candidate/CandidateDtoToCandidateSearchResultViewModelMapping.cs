﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateDtoToCandidateSearchResultViewModelMapping : ClassMapping<CandidateDto, CandidateSearchResultViewModel>
    {
        public CandidateDtoToCandidateSearchResultViewModelMapping()
        {
            Mapping = d => new CandidateSearchResultViewModel
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                MobilePhone = d.MobilePhone,
                EmailAddress = d.EmailAddress,
                SkypeLogin = d.SkypeLogin,
                SocialLink = d.SocialLink,
                FullName = d.FullName,
                Timestamp = d.Timestamp,
            };
        }
    }
}
