﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class RecentActivityDtoToRecentActivityViewModelMapping
         : ClassMapping<RecentActivityDto, RecentActivityViewModel>
    {
        public RecentActivityDtoToRecentActivityViewModelMapping()
        {
            Mapping = ra => new RecentActivityViewModel
            {
                By = ra.UserFullName,
                On = ra.OccuredOn,
                ReferenceId = ra.ReferenceId,
                ReferenceType = ra.ReferenceType
            };
        }
    }
}