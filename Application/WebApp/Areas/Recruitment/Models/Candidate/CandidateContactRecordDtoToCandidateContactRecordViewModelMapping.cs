﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateContactRecordDtoToCandidateContactRecordViewModelMapping : ClassMapping<CandidateContactRecordDto, CandidateContactRecordViewModel>
    {
        public CandidateContactRecordDtoToCandidateContactRecordViewModelMapping()
        {
            Mapping = d => new CandidateContactRecordViewModel
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                ContactType = d.ContactType,
                ContactedOn = d.ContactedOn,
                ContactedByEmployeeId = d.ContactedByEmployeeId,
                CandidateReaction = d.CandidateReaction,
                RelevantJobOpeningIds = d.RelevantJobOpeningIds.ToList(),
                Comment = d.Comment,
                Timestamp = d.Timestamp
            };
        }
    }
}
