﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.WebApp.Areas.Recruitment.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateFileDtoToCandidateFileViewModelMapping : ClassMapping<CandidateFileDto, CandidateFileViewModel>
    {
        public CandidateFileDtoToCandidateFileViewModelMapping()
        {
            Mapping = d => new CandidateFileViewModel
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                DocumentType = d.DocumentType,
                Comment = d.Comment,
                File = d.File.Select(f => new DocumentViewModel
                    {
                        ContentType = f.ContentType,
                        DocumentName = f.DocumentName,
                        DocumentId = f.DocumentId,
                        TemporaryDocumentId = f.TemporaryDocumentId,
                    }).FirstOrDefault(),
                IsFileSupportedByViewerJS = d.File.Any(f => DocumentViewerHelper.IsSupportedFile(f.DocumentName)),
                ModifiedOn = d.ModifiedOn,
                IsDataConsent = d.IsDataConsent,
                Timestamp = d.Timestamp
            };
        }
    }
}
