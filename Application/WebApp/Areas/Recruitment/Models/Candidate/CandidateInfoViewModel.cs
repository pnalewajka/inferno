﻿namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateInfoViewModel
    {
        public long Id { get; set; }

        public string FullName { get; set; }

        public RecruitmentProcessInfoViewModel MostRecentProcess { get; set; }
    }
}