﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class RecentActivityViewModel
    {
        public ReferenceType ReferenceType { get; set; }

        public long ReferenceId { get; set; }

        public string By { get; set; }

        public DateTime On { get; set; }
    }
}