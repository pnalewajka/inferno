﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateNoteViewModelToCandidateNoteDtoMapping : ClassMapping<CandidateNoteViewModel, CandidateNoteDto>
    {
        public CandidateNoteViewModelToCandidateNoteDtoMapping()
        {
            Mapping = v => new CandidateNoteDto
            {
                Id = v.Id,
                CandidateId = v.CandidateId,
                NoteType = v.NoteType,
                NoteVisibility = v.NoteVisibility,
                Content = v.RichContent,
                Timestamp = v.Timestamp
            };
        }
    }
}
