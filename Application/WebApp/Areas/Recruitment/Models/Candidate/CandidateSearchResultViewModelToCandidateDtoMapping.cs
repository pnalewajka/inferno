﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateSearchResultViewModelToCandidateDtoMapping : ClassMapping<CandidateSearchResultViewModel, CandidateDto>
    {
        public CandidateSearchResultViewModelToCandidateDtoMapping()
        {
            Mapping = v => new CandidateDto
            {
                Id = v.Id,
                FirstName = v.FirstName,
                LastName = v.LastName,
                MobilePhone = v.MobilePhone,
                EmailAddress = v.EmailAddress,
                SkypeLogin = v.SkypeLogin,
                SocialLink = v.SocialLink,
                Timestamp = v.Timestamp
            };
        }
    }
}
