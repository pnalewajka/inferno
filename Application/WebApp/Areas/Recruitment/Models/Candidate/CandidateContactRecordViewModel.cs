﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Validation;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    [Identifier("Models.ContactRecordViewModel")]
    public class CandidateContactRecordViewModel : IValidatableObject
    {
        public long Id { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ContactRecordResources.CandidateId), typeof(ContactRecordResources))]
        [ValuePicker(Type = typeof(CandidatePickerController), OnResolveUrlJavaScript = "url += '&only-valid-consent=true'")]
        [Render(GridCssClass = "no-break-column", Size = Size.Medium)]
        [Required]
        [Order(1)]
        public long CandidateId { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ContactRecordResources.Comment), typeof(ContactRecordResources))]
        [Multiline(4)]
        [Render(GridCssClass = "remaining-space-column")]
        [Order(2)]
        [NonSortable]
        [StringLength(5000)]
        public string Comment { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ContactRecordResources.ContactType), typeof(ContactRecordResources))]
        [Render(GridCssClass = "no-break-column", Size = Size.Medium)]
        [RequiredEnum(typeof(ContactType))]
        [Order(3)]
        public ContactType ContactType { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ContactRecordResources.CandidateReaction), typeof(ContactRecordResources))]
        [RadioGroup(ItemProvider = typeof(EnumBasedListItemProvider<CandidateReaction>))]
        [RequiredEnum(typeof(CandidateReaction))]
        [Render(GridCssClass = "no-break-column")]
        [Order(4)]
        public CandidateReaction CandidateReaction { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ContactRecordResources.ContactedOn), typeof(ContactRecordResources))]
        [TimePicker(15)]
        [Render(GridCssClass = "no-break-column")]
        [Order(5)]
        public DateTime ContactedOn { get; set; }

        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(ContactRecordResources.ContactedByEmployeeId), typeof(ContactRecordResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=recruitment-data-certified'")]
        [Required]
        [Render(GridCssClass = "no-break-column")]
        [Order(6)]
        public long ContactedByEmployeeId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContactRecordResources.RelevantJobOpeningIds), typeof(ContactRecordResources))]
        [ValuePicker(Type = typeof(JobOpeningPickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [Order(7)]
        public List<long> RelevantJobOpeningIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(ContactRecordResources.NextContactOn), typeof(ContactRecordResources))]
        [TimePicker(15)]
        [Order(8)]
        public DateTime? NextContactdOn { get; set; }

        public byte[] Timestamp { get; set; }

        public CandidateContactRecordViewModel()
        {
            RelevantJobOpeningIds = new List<long>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CandidateReaction == CandidateReaction.ContactLater && NextContactdOn == null)
            {
                yield return new PropertyValidationResult(ContactRecordResources.NextContactOnRequired, () => NextContactdOn);
            }
        }

        public override string ToString() => StringHelper.TruncateAtWords(Comment, 50);
    }
}