﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateDataConsentDtoToCandidateDataConsentViewModelMapping : ClassMapping<CandidateDataConsentDto, CandidateDataConsentViewModel>
    {
        public CandidateDataConsentDtoToCandidateDataConsentViewModelMapping()
        {
            Mapping = d => new CandidateDataConsentViewModel
            {
                DataAdministrator = d.DataAdministrator,
                ExpiresOn = d.ExpiresOn,
                Scope = d.Scope,
                ConsentType = d.ConsentType
            };
        }
    }
}