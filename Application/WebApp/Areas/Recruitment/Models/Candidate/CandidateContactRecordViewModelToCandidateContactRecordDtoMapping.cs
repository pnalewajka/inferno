﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models.Candidate
{
    public class CandidateContactRecordViewModelToCandidateContactRecordDtoMapping : ClassMapping<CandidateContactRecordViewModel, CandidateContactRecordDto>
    {
        public CandidateContactRecordViewModelToCandidateContactRecordDtoMapping()
        {
            Mapping = v => new CandidateContactRecordDto
            {
                Id = v.Id,
                CandidateId = v.CandidateId,
                ContactType = v.ContactType,
                ContactedOn = v.ContactedOn,
                ContactedByEmployeeId = v.ContactedByEmployeeId,
                CandidateReaction = v.CandidateReaction,
                RelevantJobOpeningIds = v.RelevantJobOpeningIds.ToArray(),
                Comment = v.Comment,
                NextContactdOn = v.NextContactdOn,
                Timestamp = v.Timestamp
            };
        }
    }
}
