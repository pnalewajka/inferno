﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ApplicationOriginViewModelToApplicationOriginDtoMapping : ClassMapping<ApplicationOriginViewModel, ApplicationOriginDto>
    {
        public ApplicationOriginViewModelToApplicationOriginDtoMapping()
        {
            Mapping = v => new ApplicationOriginDto
            {
                Id = v.Id,
                Name = v.Name,
                OriginType = v.OriginType,
                ParserIdentifier = v.ParserIdentifier,
                EmailAddress = v.EmailAddress,
                Timestamp = v.Timestamp
            };
        }
    }
}
