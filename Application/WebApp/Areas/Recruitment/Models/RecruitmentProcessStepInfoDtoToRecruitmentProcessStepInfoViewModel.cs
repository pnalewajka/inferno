﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessStepInfoDtoToRecruitmentProcessStepInfoViewModel : ClassMapping<RecruitmentProcessStepInfoDto, RecruitmentProcessStepInfoViewModel>
    {
        private readonly ITimeService _timeService;

        public RecruitmentProcessStepInfoDtoToRecruitmentProcessStepInfoViewModel(ITimeService timeService)
        {
            _timeService = timeService;

            Mapping = d => new RecruitmentProcessStepInfoViewModel
            {
                Id = d.Id,
                IsPending = d.Status == RecruitmentProcessStepStatus.ToDo && d.DueOn.HasValue && d.DueOn < _timeService.GetCurrentDate(),
                Comment = d.Comment,
                DecidedOn = d.DecidedOn,
                Hint = d.Hint,
                PlannedOn = d.PlannedOn,
                DueOn = d.DueOn,
                Decision = d.Decision,
                Status = d.Status,
                Type = d.Type,
                AssigneeFullName = d.AssigneeFullName,
                CanBeEdited = d.CanBeEdited,
                CanBeViewed = d.CanBeViewed,
                CanViewDoneStep = d.CanViewDoneStep
            };
        }
    }
}