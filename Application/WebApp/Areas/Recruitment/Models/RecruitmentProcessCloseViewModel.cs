﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Recruitment.ItemProviders;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RecruitmentProcessCloseViewModel")]
    public class RecruitmentProcessCloseViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ClosedReason), typeof(RecruitmentProcessResources))]
        [RadioGroup(ItemProvider = typeof(ManualRecruitmentProcessClosedReasonItemProvider))]
        [Render(ControlCssClass = "align-options-200")]
        public RecruitmentProcessClosedReason? Reason { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ClosedComment), typeof(RecruitmentProcessResources))]
        [StringLength(1024)]
        [Multiline(3)]
        public string Comment { get; set; }
    }
}