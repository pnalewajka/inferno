﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RejectedCandidateReportDtoToRejectedCandidateReportViewModelMapping : ClassMapping<RejectedCandidateReportParametersDto, RejectedCandidateReportParametersViewModel>
    {
        public RejectedCandidateReportDtoToRejectedCandidateReportViewModelMapping()
        {
            Mapping = v => new RejectedCandidateReportParametersViewModel
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                OrgUnitId = v.OrgUnitId,
                RecruiterIds = v.RecruiterIds,
                RejectingEmployeeIds = v.RejectingEmployeeIds,
                RejectionReason = v.RejectionReason
            };
        }
    }
}