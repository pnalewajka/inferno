﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessQuickAddViewModelToRecruitmentProcessDtoMapping : ClassMapping<RecruitmentProcessQuickAddViewModel, RecruitmentProcessDto>
    {
        public RecruitmentProcessQuickAddViewModelToRecruitmentProcessDtoMapping()
        {
            Mapping = v => new RecruitmentProcessDto
            {
                CandidateId = v.CandidateId,
                JobOpeningId = v.JobOpeningId,
                RecruiterId = v.RecruiterId,
                InitialSteps = v.InitialSteps,
                WatcherIds = new long[0],
                Status = v.Status
            };
        }
    }
}