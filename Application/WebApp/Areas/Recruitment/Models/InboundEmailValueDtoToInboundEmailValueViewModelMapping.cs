﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class InboundEmailValueDtoToInboundEmailValueViewModelMapping : ClassMapping<InboundEmailValueDto, InboundEmailValueViewModel>
    {
        public InboundEmailValueDtoToInboundEmailValueViewModelMapping()
        {
            Mapping = d => new InboundEmailValueViewModel
            {
                Id = d.Id,
                Key = d.Key,
                ValueString = d.ValueString,
                ValueLong = d.ValueLong,
                Timestamp = d.Timestamp
            };
        }
    }
}
