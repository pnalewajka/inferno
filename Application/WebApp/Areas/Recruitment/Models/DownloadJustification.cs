﻿using System;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Serializable]
    public class DownloadJustification
    {
        public static DownloadJustification CreateViewedDocumentDownloadJustification() =>
            new DownloadJustification(ActivityType.ViewedDocument);

        public static DownloadJustification CreateDocumentSharedWithCustomerDownloadJustification(CustomerDto customer, string comment) =>
            new DownloadJustification(ActivityType.SharedDocumentWithCustomer, $"{customer.LegalName} ({customer.Id}) [{comment}]");

        private DownloadJustification(ActivityType activityType, string text = null)
        {
            ActivityType = activityType;
            Text = text;
        }

        public ActivityType ActivityType { get; }

        public string Text { get; }
    }
}