﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.RecruitmentProcessReopenViewModel")]
    public class RecruitmentProcessReopenViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ReopenReason), typeof(RecruitmentProcessResources))]
        [StringLength(1024)]
        [Multiline(3)]
        [Required]
        public string Comment { get; set; }
    }
}