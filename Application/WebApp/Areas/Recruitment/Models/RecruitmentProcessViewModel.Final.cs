﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public partial class RecruitmentProcessViewModel
    {
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalSignedDate), typeof(RecruitmentProcessResources))]
        [Tab(FinalTab)]
        public DateTime? FinalSignedDate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalStartDate), typeof(RecruitmentProcessResources))]
        [Tab(FinalTab)]
        public DateTime? FinalStartDate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalOnboardingDate), typeof(RecruitmentProcessResources))]
        [Tab(FinalTab)]
        public DateTime? FinalOnboardingDate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalPosition), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [Tab(FinalTab)]
        public long? FinalPositionId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalTrialContractType), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [Tab(FinalTab)]
        public long? FinalTrialContractTypeId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalLongTermContractType), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [Tab(FinalTab)]
        public long? FinalLongTermContractTypeId { get; set; }

        [Tab(FinalTab)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewRecruitmentProcessFinalSalary)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalTrialSalary), typeof(RecruitmentProcessResources))]
        public string FinalTrialSalary { get; set; }

        [Tab(FinalTab)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewRecruitmentProcessFinalSalary)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalLongTermSalary), typeof(RecruitmentProcessResources))]
        public string FinalLongTermSalary { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalLocation), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        [Tab(FinalTab)]
        public long? FinalLocationId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalEmploymentSource), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(ApplicationOriginPickerController))]
        [Tab(FinalTab)]
        public long? FinalEmploymentSourceId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.FinalComment), typeof(RecruitmentProcessResources))]
        [Tab(FinalTab)]
        [Multiline(3)]
        public string FinalComment { get; set; }
    }
}