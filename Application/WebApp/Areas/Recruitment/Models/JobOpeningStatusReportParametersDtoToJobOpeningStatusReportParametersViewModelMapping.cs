﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class JobOpeningStatusReportParametersDtoToJobOpeningStatusReportParametersViewModelMapping
         : ClassMapping<JobOpeningStatusReportParametersDto, JobOpeningStatusReportParametersViewModel>
    {
        public JobOpeningStatusReportParametersDtoToJobOpeningStatusReportParametersViewModelMapping()
        {
            Mapping = v => new JobOpeningStatusReportParametersViewModel
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                JobOpeningStatuses = v.JobOpeningStatuses
            };
        }
    }
}