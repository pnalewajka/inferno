﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ApplicationOriginViewModel
    {
        public long Id { get; set; }

        [DisplayNameLocalized(nameof(ApplicationOriginResources.Name), typeof(ApplicationOriginResources))]
        [NonSortable]
        public string Name { get; set; }

        [DisplayNameLocalized(nameof(ApplicationOriginResources.OriginType), typeof(ApplicationOriginResources))]
        [NonSortable]
        public ApplicationOriginType OriginType { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ApplicationOriginResources.ParserIdentifier), typeof(ApplicationOriginResources))]
        [Visibility(VisibilityScope.Form)]
        public string ParserIdentifier { get; set; }

        [StringLength(255)]
        [DisplayNameLocalized(nameof(ApplicationOriginResources.EmailAddress), typeof(ApplicationOriginResources))]
        public string EmailAddress { get; set; }

        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            switch (OriginType)
            {
                case ApplicationOriginType.Agency:
                case ApplicationOriginType.JobAdvertisement:
                case ApplicationOriginType.SocialNetwork:
                    return $"{Name} ({OriginType.GetDescription()})";

                default:
                    return Name;
            }
        }

        internal string GetGroupName()
        {
            switch (OriginType)
            {
                case ApplicationOriginType.Agency:
                case ApplicationOriginType.JobAdvertisement:
                case ApplicationOriginType.SocialNetwork:
                    return OriginType.GetDescription();

                default:
                    return ApplicationOriginResources.Specific;
            }
        }
    }
}
