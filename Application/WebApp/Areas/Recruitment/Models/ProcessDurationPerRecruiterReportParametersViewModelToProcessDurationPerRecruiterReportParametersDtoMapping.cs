﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessDurationPerRecruiterReportParametersViewModelToProcessDurationPerRecruiterReportParametersDtoMapping
        : ClassMapping<ProcessDurationPerRecruiterReportParametersViewModel, ProcessDurationPerRecruiterReportParametersDto>
    {
        public ProcessDurationPerRecruiterReportParametersViewModelToProcessDurationPerRecruiterReportParametersDtoMapping()
        {
            Mapping = v => new ProcessDurationPerRecruiterReportParametersDto
            {
                DateFrom = v.DateFrom.StartOfDay(),
                DateTo = v.DateTo.EndOfDay(),
                RecruiterIds = v.RecruiterIds,
                ProcessResult = v.ProcessResult
            };
        }
    }
}