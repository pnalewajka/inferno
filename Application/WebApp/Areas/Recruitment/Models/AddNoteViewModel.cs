﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.AddNoteViewModel")]
    public class AddNoteViewModel
    {
        [Visibility(VisibilityScope.None)]
        public long ParentId { get; set; }

        [Required]
        [AllowHtml]
        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.Content), typeof(RecruitmentProcessNoteResources))]
        [StringLength(4000)]
        [CustomControl(TemplatePath = "Areas.Recruitment.Views.Shared.RichNoteControl")]
        [HtmlSanitize]
        public string Content { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(RecruitmentProcessNoteResources.NoteVisibility), typeof(RecruitmentProcessNoteResources))]
        [Render(ControlCssClass = "align-options-200")]
        [RadioGroup(ItemProvider = typeof(BasicNoteVisibilityItemProvider))]
        public NoteVisibility NoteVisibility { get; set; }

        public AddNoteViewModel()
        {
            NoteVisibility = NoteVisibility.Public;
        }
    }
}