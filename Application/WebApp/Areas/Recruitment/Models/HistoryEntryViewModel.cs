﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.ActivityHistoryViewModel")]
    public class HistoryEntryViewModel
    {
        public DateTime PerformedOn { get; set; }

        public string PerformedByUserFullName { get; set; }

        public string ActivityDescription { get; set; }

        public IEnumerable<KeyValuePair<string, ValueChange>> SnapshotChanges { get; set; }
    }
}
