﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class AddNoteDtoToAddNoteViewModelMapping : ClassMapping<AddNoteDto, AddNoteViewModel>
    {
        public AddNoteDtoToAddNoteViewModelMapping()
        {
            Mapping = d => new AddNoteViewModel
            {
                ParentId = d.ParentId,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content
            };
        }
    }
}
