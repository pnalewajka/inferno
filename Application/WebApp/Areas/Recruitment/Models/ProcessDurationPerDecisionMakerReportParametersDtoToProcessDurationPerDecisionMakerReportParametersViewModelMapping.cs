﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class ProcessDurationPerDecisionMakerReportParametersDtoToProcessDurationPerDecisionMakerReportParametersViewModelMapping
        : ClassMapping<ProcessDurationPerDecisionMakerReportParametersDto, ProcessDurationPerDecisionMakerReportParametersViewModel>
    {
        public ProcessDurationPerDecisionMakerReportParametersDtoToProcessDurationPerDecisionMakerReportParametersViewModelMapping()
        {
            Mapping = v => new ProcessDurationPerDecisionMakerReportParametersViewModel
            {
                DateFrom = v.DateFrom,
                DateTo = v.DateTo,
                DecisionMakerEmployeeIds = v.DecisionMakerEmployeeIds,
                ProcessResult = v.ProcessResult
            };
        }
    }
}