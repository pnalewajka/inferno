﻿using System;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessInfoViewModel
    {
        public long Id { get; set; }

        public string PositionName { get; set; }

        public long RecruiterId { get; set; }

        public string RecruiterFullName { get; set; }

        public DateTime? LastActivityOn { get; set; }

        public string LastActivityBy { get; set; }

        public override string ToString()
        {
            string baseInfo = $"{PositionName} · {RecruiterFullName}";

            if (LastActivityOn.HasValue)
            {
                if (RecruiterFullName == LastActivityBy)
                {
                    return $"{baseInfo} ({LastActivityOn.Value.ToString("d")})";
                }

                return $"{baseInfo} ({LastActivityOn.Value.ToString("d")} {LastActivityBy})";
            }

            return baseInfo;
        }
    }
}