﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class RecruitmentProcessNoteDtoToRecruitmentProcessNoteViewModelMapping : ClassMapping<RecruitmentProcessNoteDto, RecruitmentProcessNoteViewModel>
    {
        public RecruitmentProcessNoteDtoToRecruitmentProcessNoteViewModelMapping()
        {
            Mapping = d => new RecruitmentProcessNoteViewModel
            {
                Id = d.Id,
                RecruitmentProcessId = d.RecruitmentProcessId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content != null ? MentionItemHelper.ReplaceTokens(d.Content, i => i.Label) : string.Empty,
                RichContent = d.Content,
                CreatedOn = d.CreatedOn,
                CreatedByFullName = d.CreatedByFullName,
                RecruitmentProcessStepId = d.RecruitmentProcessStepId,
                Timestamp = d.Timestamp
            };
        }
    }
}
