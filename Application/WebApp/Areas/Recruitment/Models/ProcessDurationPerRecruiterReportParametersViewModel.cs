﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.Reports.Resources;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    [Identifier("Models.ProcessDurationPerRecruiterReportParameters")]
    public class ProcessDurationPerRecruiterReportParametersViewModel
    {
        [DisplayNameLocalized(nameof(ReportResources.FromDateText), typeof(ReportResources))]
        public DateTime DateFrom { get; set; }

        [DisplayNameLocalized(nameof(ReportResources.ToDateText), typeof(ReportResources))]
        public DateTime DateTo { get; set; }

        [DisplayNameLocalized(nameof(JobOpeningResources.RecruiterIds), typeof(JobOpeningResources))]
        [ValuePicker(Type = typeof(EmployeePickerController), OnResolveUrlJavaScript = "url += '&mode=reporting-recruiters'")]
        public List<long> RecruiterIds { get; set; }

        [DisplayNameLocalized(nameof(RecruitmentProcessResources.ProcessResultText), typeof(RecruitmentProcessResources))]
        [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<OverallRecruitmentProcessResult>), EmptyItemBehavior = EmptyItemBehavior.AlwaysPresent)]
        public OverallRecruitmentProcessResult? ProcessResult { get; set; }
    }
}