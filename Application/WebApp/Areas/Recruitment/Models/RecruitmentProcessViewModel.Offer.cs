﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;
using Smt.Atomic.WebApp.Areas.TimeTracking.Controllers;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public partial class RecruitmentProcessViewModel
    {
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferPositionId), typeof(RecruitmentProcessResources))]
        [ValuePicker(Type = typeof(JobTitlePickerController), OnResolveUrlJavaScript = "url += '&only-active=true'")]
        [Tab(OfferTab)]
        public long? OfferPositionId { get; set; }

        [ValuePicker(Type = typeof(ContractTypePickerController))]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferContractType), typeof(RecruitmentProcessResources))]
        [Tab(OfferTab)]
        public string OfferContractType { get; set; }

        [Tab(OfferTab)]
        [Visibility(VisibilityScope.Form)]
        [RoleRequired(SecurityRoleType.CanViewRecruitmentProcessOfferedSalary)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferSalary), typeof(RecruitmentProcessResources))]
        public string OfferSalary { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferStartDate), typeof(RecruitmentProcessResources))]
        [Tab(OfferTab)]
        public string OfferStartDate { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferLocationId), typeof(RecruitmentProcessResources))]
        [Tab(OfferTab)]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? OfferLocationId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(RecruitmentProcessResources.OfferComment), typeof(RecruitmentProcessResources))]
        [Tab(OfferTab)]
        [Multiline(3)]
        public string OfferComment { get; set; }
    }
}