﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Models
{
    public class InboundEmailValueViewModel
    {
        public long Id { get; set; }

        public InboundEmailValueType Key { get; set; }

        public string ValueString { get; set; }

        public long? ValueLong { get; set; }

        public byte[] Timestamp { get; set; }

        public string GetDisplayValue()
        {
            return ValueLong.HasValue ? $"{ValueLong}" : ValueString;
        }
    }
}
