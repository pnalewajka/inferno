﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Recruitment.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class JobOpeningResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal JobOpeningResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Recruitment.Resources.JobOpeningResources", typeof(JobOpeningResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approved By.
        /// </summary>
        public static string AcceptingEmployeeId {
            get {
                return ResourceManager.GetString("AcceptingEmployeeId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer.
        /// </summary>
        public static string AccountId {
            get {
                return ResourceManager.GetString("AccountId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Agency Mode.
        /// </summary>
        public static string AgencyEmploymentMode {
            get {
                return ResourceManager.GetString("AgencyEmploymentMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        public static string AllJobApplicationRecordsLabel {
            get {
                return ResourceManager.GetString("AllJobApplicationRecordsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approve.
        /// </summary>
        public static string ApproveButtonLabel {
            get {
                return ResourceManager.GetString("ApproveButtonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cities.
        /// </summary>
        public static string CityIds {
            get {
                return ResourceManager.GetString("CityIds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clone.
        /// </summary>
        public static string Clone {
            get {
                return ResourceManager.GetString("Clone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string CloseButtonLabel {
            get {
                return ResourceManager.GetString("CloseButtonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment.
        /// </summary>
        public static string ClosedComment {
            get {
                return ResourceManager.GetString("ClosedComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Closing Job Opening.
        /// </summary>
        public static string CloseDialogTitle {
            get {
                return ResourceManager.GetString("CloseDialogTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reason.
        /// </summary>
        public static string ClosedReason {
            get {
                return ResourceManager.GetString("ClosedReason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        public static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consents.
        /// </summary>
        public static string ConsentsTabText {
            get {
                return ResourceManager.GetString("ConsentsTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Activifies.
        /// </summary>
        public static string ContactActivitiesTabText {
            get {
                return ResourceManager.GetString("ContactActivitiesTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contributors.
        /// </summary>
        public static string ContributorsTabText {
            get {
                return ResourceManager.GetString("ContributorsTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Author.
        /// </summary>
        public static string CreatedBy {
            get {
                return ResourceManager.GetString("CreatedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created On.
        /// </summary>
        public static string CreatedOn {
            get {
                return ResourceManager.GetString("CreatedOn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decision Maker.
        /// </summary>
        public static string DecisionMaker {
            get {
                return ResourceManager.GetString("DecisionMaker", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Demand.
        /// </summary>
        public static string DemandTabText {
            get {
                return ResourceManager.GetString("DemandTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employment.
        /// </summary>
        public static string EmploymentTabText {
            get {
                return ResourceManager.GetString("EmploymentTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to English Level.
        /// </summary>
        public static string EnglishLevel {
            get {
                return ResourceManager.GetString("EnglishLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to From.
        /// </summary>
        public static string From {
            get {
                return ResourceManager.GetString("From", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General.
        /// </summary>
        public static string GeneralTabText {
            get {
                return ResourceManager.GetString("GeneralTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hire For.
        /// </summary>
        public static string HireForLabel {
            get {
                return ResourceManager.GetString("HireForLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hiring managers.
        /// </summary>
        public static string HiringManagerIds {
            get {
                return ResourceManager.GetString("HiringManagerIds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hiring Mode.
        /// </summary>
        public static string HiringModeLabel {
            get {
                return ResourceManager.GetString("HiringModeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mode.
        /// </summary>
        public static string HiringModeShortLabel {
            get {
                return ResourceManager.GetString("HiringModeShortLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job Opening has to have at least one Job Profile.
        /// </summary>
        public static string InvalidJobProfiles {
            get {
                return ResourceManager.GetString("InvalidJobProfiles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Highly Confidential.
        /// </summary>
        public static string IsHighlyConfidential {
            get {
                return ResourceManager.GetString("IsHighlyConfidential", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Intive Resume Required.
        /// </summary>
        public static string IsIntiveResumeRequired {
            get {
                return ResourceManager.GetString("IsIntiveResumeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Polish Speaker Required.
        /// </summary>
        public static string IsPolishSpeakerRequired {
            get {
                return ResourceManager.GetString("IsPolishSpeakerRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Technical Verification Required.
        /// </summary>
        public static string IsTechnicalVerificationRequired {
            get {
                return ResourceManager.GetString("IsTechnicalVerificationRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job Opening Filters.
        /// </summary>
        public static string JobOpeningFilters {
            get {
                return ResourceManager.GetString("JobOpeningFilters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to List.
        /// </summary>
        public static string JobOpeningListViewName {
            get {
                return ResourceManager.GetString("JobOpeningListViewName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to approve this job opening?.
        /// </summary>
        public static string JobOpeninigApproveConfirmationMessage {
            get {
                return ResourceManager.GetString("JobOpeninigApproveConfirmationMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warning.
        /// </summary>
        public static string JobOpeninigApproveConfirmationTitle {
            get {
                return ResourceManager.GetString("JobOpeninigApproveConfirmationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reason:.
        /// </summary>
        public static string JobOpeninigRejectReasonLabel {
            get {
                return ResourceManager.GetString("JobOpeninigRejectReasonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide the reason for job opening rejection..
        /// </summary>
        public static string JobOpeninigRejectReasonMessage {
            get {
                return ResourceManager.GetString("JobOpeninigRejectReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejection reason.
        /// </summary>
        public static string JobOpeninigRejectReasonTitle {
            get {
                return ResourceManager.GetString("JobOpeninigRejectReasonTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to send this job opening for approval?.
        /// </summary>
        public static string JobOpeninigSendForApprovalConfirmationMessage {
            get {
                return ResourceManager.GetString("JobOpeninigSendForApprovalConfirmationMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide the reason for rejection of selected job opening(s).
        /// </summary>
        public static string JobOpeninigsRejectReasonMessage {
            get {
                return ResourceManager.GetString("JobOpeninigsRejectReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job Profiles.
        /// </summary>
        public static string JobProfileIds {
            get {
                return ResourceManager.GetString("JobProfileIds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profiles.
        /// </summary>
        public static string JobProfileIdsShort {
            get {
                return ResourceManager.GetString("JobProfileIdsShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minimal.
        /// </summary>
        public static string MinimalViewText {
            get {
                return ResourceManager.GetString("MinimalViewText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Records.
        /// </summary>
        public static string MyJobOpeningRecordsLabel {
            get {
                return ResourceManager.GetString("MyJobOpeningRecordsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Note.
        /// </summary>
        public static string NewNoteButtonText {
            get {
                return ResourceManager.GetString("NewNoteButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adding Note.
        /// </summary>
        public static string NoteDialogTitle {
            get {
                return ResourceManager.GetString("NoteDialogTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notes.
        /// </summary>
        public static string NoteTabText {
            get {
                return ResourceManager.GetString("NoteTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notice Period.
        /// </summary>
        public static string NoticePeriod {
            get {
                return ResourceManager.GetString("NoticePeriod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Org Unit.
        /// </summary>
        public static string OrgUnitId {
            get {
                return ResourceManager.GetString("OrgUnitId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other Languages.
        /// </summary>
        public static string OtherLanguages {
            get {
                return ResourceManager.GetString("OtherLanguages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Owner.
        /// </summary>
        public static string Owner {
            get {
                return ResourceManager.GetString("Owner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Position Name.
        /// </summary>
        public static string PositionNameLabel {
            get {
                return ResourceManager.GetString("PositionNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Position.
        /// </summary>
        public static string PositionNameShortLabel {
            get {
                return ResourceManager.GetString("PositionNameShortLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Position Details.
        /// </summary>
        public static string PositionTabText {
            get {
                return ResourceManager.GetString("PositionTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Priority.
        /// </summary>
        public static string Priority {
            get {
                return ResourceManager.GetString("Priority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Candidate.
        /// </summary>
        public static string ProcessCandidate {
            get {
                return ResourceManager.GetString("ProcessCandidate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decision Maker.
        /// </summary>
        public static string ProcessDecisionMaker {
            get {
                return ResourceManager.GetString("ProcessDecisionMaker", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Processed By Me.
        /// </summary>
        public static string ProcessedByMeCandidatesLabel {
            get {
                return ResourceManager.GetString("ProcessedByMeCandidatesLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Processes.
        /// </summary>
        public static string Processes {
            get {
                return ResourceManager.GetString("Processes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Process Parameters.
        /// </summary>
        public static string ProcessParametersTabText {
            get {
                return ResourceManager.GetString("ProcessParametersTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recruiter.
        /// </summary>
        public static string ProcessRecruiter {
            get {
                return ResourceManager.GetString("ProcessRecruiter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string ProcessStatus {
            get {
                return ResourceManager.GetString("ProcessStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        public static string ProcessStepsDone {
            get {
                return ResourceManager.GetString("ProcessStepsDone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Do.
        /// </summary>
        public static string ProcessStepsToDo {
            get {
                return ResourceManager.GetString("ProcessStepsToDo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project Description.
        /// </summary>
        public static string ProjectDescription {
            get {
                return ResourceManager.GetString("ProjectDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Project Description.
        /// </summary>
        public static string ProjectDescriptionAddButtonText {
            get {
                return ResourceManager.GetString("ProjectDescriptionAddButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string ProjectDescriptionDeleteButtonText {
            get {
                return ResourceManager.GetString("ProjectDescriptionDeleteButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project Descriptions.
        /// </summary>
        public static string ProjectDescriptionList {
            get {
                return ResourceManager.GetString("ProjectDescriptionList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project.
        /// </summary>
        public static string ProjectDescriptionListItem {
            get {
                return ResourceManager.GetString("ProjectDescriptionListItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project Descriptions.
        /// </summary>
        public static string ProjectDescriptionsTabText {
            get {
                return ResourceManager.GetString("ProjectDescriptionsTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project Description.
        /// </summary>
        public static string ProjectDescriptionTabText {
            get {
                return ResourceManager.GetString("ProjectDescriptionTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project.
        /// </summary>
        public static string ProjectLabel {
            get {
                return ResourceManager.GetString("ProjectLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Projects.
        /// </summary>
        public static string ProjectsLabel {
            get {
                return ResourceManager.GetString("ProjectsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to On Hold.
        /// </summary>
        public static string PutOnHoldLabel {
            get {
                return ResourceManager.GetString("PutOnHoldLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recruiters.
        /// </summary>
        public static string RecruiterIds {
            get {
                return ResourceManager.GetString("RecruiterIds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Process Owners.
        /// </summary>
        public static string RecruitmentOwnerIds {
            get {
                return ResourceManager.GetString("RecruitmentOwnerIds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Processes.
        /// </summary>
        public static string RecruitmentProcessTabLabel {
            get {
                return ResourceManager.GetString("RecruitmentProcessTabLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reject.
        /// </summary>
        public static string RejectButtonLabel {
            get {
                return ResourceManager.GetString("RejectButtonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejection Reason.
        /// </summary>
        public static string RejectionReasonLabel {
            get {
                return ResourceManager.GetString("RejectionReasonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Previous Rejection Reason.
        /// </summary>
        public static string RejectionReasonPreviousLabel {
            get {
                return ResourceManager.GetString("RejectionReasonPreviousLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reopen.
        /// </summary>
        public static string Reopen {
            get {
                return ResourceManager.GetString("Reopen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opened.
        /// </summary>
        public static string RoleOpenedOn {
            get {
                return ResourceManager.GetString("RoleOpenedOn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This Job Opening is still a draft. Remember to send it for approval..
        /// </summary>
        public static string RowTitleDraft {
            get {
                return ResourceManager.GetString("RowTitleDraft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This Job Opening is waiting for approval. Stay tuned..
        /// </summary>
        public static string RowTitlePendingApproval {
            get {
                return ResourceManager.GetString("RowTitlePendingApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This Job Opening has been rejected. Adjust values and re-send it for approval..
        /// </summary>
        public static string RowTitleRejected {
            get {
                return ResourceManager.GetString("RowTitleRejected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Salary Rate.
        /// </summary>
        public static string SalaryRate {
            get {
                return ResourceManager.GetString("SalaryRate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send for approval.
        /// </summary>
        public static string SendForApprovalButtonLabel {
            get {
                return ResourceManager.GetString("SendForApprovalButtonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        public static string StatusActiveLabel {
            get {
                return ResourceManager.GetString("StatusActiveLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        public static string StatusAllLabel {
            get {
                return ResourceManager.GetString("StatusAllLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Default.
        /// </summary>
        public static string StatusNonClosedLabel {
            get {
                return ResourceManager.GetString("StatusNonClosedLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Off Hold.
        /// </summary>
        public static string TakeOffHoldLabel {
            get {
                return ResourceManager.GetString("TakeOffHoldLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Technical Review Comment.
        /// </summary>
        public static string TechnicalReviewComment {
            get {
                return ResourceManager.GetString("TechnicalReviewComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Timeline.
        /// </summary>
        public static string TimelineTabText {
            get {
                return ResourceManager.GetString("TimelineTabText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job Openings.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To.
        /// </summary>
        public static string To {
            get {
                return ResourceManager.GetString("To", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unwatch.
        /// </summary>
        public static string UnwatchButtonLabel {
            get {
                return ResourceManager.GetString("UnwatchButtonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy (L1).
        /// </summary>
        public static string VacancyLevel1 {
            get {
                return ResourceManager.GetString("VacancyLevel1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy (L2).
        /// </summary>
        public static string VacancyLevel2 {
            get {
                return ResourceManager.GetString("VacancyLevel2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy (L3).
        /// </summary>
        public static string VacancyLevel3 {
            get {
                return ResourceManager.GetString("VacancyLevel3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy (L4).
        /// </summary>
        public static string VacancyLevel4 {
            get {
                return ResourceManager.GetString("VacancyLevel4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy (L5).
        /// </summary>
        public static string VacancyLevel5 {
            get {
                return ResourceManager.GetString("VacancyLevel5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy (N/A).
        /// </summary>
        public static string VacancyLevelNotApplicable {
            get {
                return ResourceManager.GetString("VacancyLevelNotApplicable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vacancy.
        /// </summary>
        public static string VacancyTotal {
            get {
                return ResourceManager.GetString("VacancyTotal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Watch.
        /// </summary>
        public static string WatchButtonLabel {
            get {
                return ResourceManager.GetString("WatchButtonLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Watchers.
        /// </summary>
        public static string WatcherIds {
            get {
                return ResourceManager.GetString("WatcherIds", resourceCulture);
            }
        }
    }
}
