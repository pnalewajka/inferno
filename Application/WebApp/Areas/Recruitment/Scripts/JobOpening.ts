﻿namespace JobOpening {
    interface ICloseJobOpening {
        Comment: string;
        Reason: number;
    }

    import StandardButtons = Dialogs.StandardButtons;

    export function RejectFromEditForm(workflowAction: number, modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.JobOpeninigRejectReasonTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    $("#workflow-action").val(workflowAction);
                    $("#workflow-rejection-reason").val(eventArgs.model.RejectionReason);
                    Forms.submitRecordForm($("button[type='submit'][name='edit']")[0]);
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function CloseFromGrid(grid: Grid.GridComponent, modelId: string) {
        const selectedId = grid.getSelectedRowData().map(r => r.id.toString())[0];
        close(selectedId, modelId, e => {
            const url = `~/Recruitment/JobOpening/CloseJobOpenings?id=${selectedId}&closedReason=${e.Reason}&closedComment=${e.Comment}`;

            Forms.submit(Globals.resolveUrl(url), "POST");
        });
    }

    export function CloseFromViewForm(id: number, modelId: string, workflowAction: number) {
        close(id, modelId, (e => {
            $("#workflow-action").val(workflowAction);
            $("#workflow-closed-comment").val(e.Comment);
            $("#workflow-closed-reason").val(e.Reason);

            Forms.submitRecordForm($("button[type='submit'][name='edit']")[0]);
        }));
    }

    function close(openJobId: number, modelId: string, action: (e: ICloseJobOpening) => void) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.CloseDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const closedComment = eventArgs.model.ClosedComment != null ? eventArgs.model.ClosedComment : "";
                    const closedReason = eventArgs.model.ClosedReason;
                    action.call(null, { Comment: closedComment, Reason: closedReason });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function RejectFromGrid(grid: Grid.GridComponent, modelId: string) {
        const selectedIds = grid.getSelectedRowData().map(r => r.id.toString());
        reject(selectedIds, modelId);
    }

    export function RejectFromViewForm(id: number, modelId: string) {
        const selectedIds = [id.toString()];
        reject(selectedIds, modelId);
    }

    export function InvokeActionFromViewForm(action: string, id: number) {
        const url = `~/Recruitment/JobOpening/${action}?Ids=${id}`;
        Forms.submit(Globals.resolveUrl(url), "POST");
    }

    export function InvokeActionFromViewFormWithConfirmation(action: string, id: number, confirmationTitle: string, confirmationMessage: string) {
        CommonDialogs.confirm(
            confirmationMessage,
            confirmationTitle,
            (eventArgs) => {
                if (eventArgs.isOk) {
                    InvokeActionFromViewForm(action, id);
                }
            }, "", "", "");
    }

    export function addNoteFromViewForm(button: Element, id: number, modelId: string) {
        Dialogs.showModel({
            modelId: modelId, 
            title: Globals.resources.NoteDialogTitle, 
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = `~/Recruitment/JobOpening/AddNote`;
                    const model = {
                        parentId: id,
                        noteVisibility: eventArgs.model.NoteVisibility,
                        content: eventArgs.model.Content
                    };
                    $.post(Globals.resolveUrl(url), model, () => {
                        window.location.reload(true);
                    });
                }
            },
            iconName: "", 
            standardButtons: StandardButtons.OkCancel
        });
    }

    function reject(jobOpeningIds: string[], modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.JobOpeninigRejectReasonTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = `~/Recruitment/JobOpening/RejectJobOpenings?Ids=${jobOpeningIds}&reason=${encodeURI(eventArgs.model.RejectionReason)}`;
                    Forms.submit(Globals.resolveUrl(url), "GET");
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }
}

$(() => {
    const jobOpeningId = Number($("#id").val());

    Dante.RecruitmentProcess.Grid.Initialize(jobOpeningId, Dante.RecruitmentProcess.Grid.ProcessGridMode.JobOpening);
});
