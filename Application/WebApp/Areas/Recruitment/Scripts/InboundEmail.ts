﻿﻿namespace InboundEmail {
    interface ICloseInboundEmail {
        Comment: string;
        Reason: number;
    }

    enum WorkflowAction {
        Assign = 1,
        Close = 2,
        Refresh = 3,
        SendForResearch = 4,
        ChangeCategory = 5,
        Unassign = 6,
    }

    const windowWidth = "1000px";
    const recommendationOriginType = "recommendation";

    import StandardButtons = Dialogs.StandardButtons;

    const employeeValuePickerIdentifier = "EmployeeValuePickers.Employee";
    const recruitersValuePickerUrlResolveCallback = ValuePicker.getContextResolveUrlCallback({ mode: "recruiters" });

    export function assignMessage(grid: Grid.GridComponent) {
        const selectedEmailId = grid.getSelectedRowData<Grid.IRow>().map((r) => r.id);

        ValuePicker.selectSingle(employeeValuePickerIdentifier, (selectedEmployee: Grid.IRow) => {
            if (selectedEmployee === null) {
                return;
            }

            const confirmationMessageBody = Strings.format(Globals.resources.AssignMessageConfirmationMessage, selectedEmployee.toString);

            CommonDialogs.confirm(confirmationMessageBody,
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                    if (eventArgs.isOk) {
                        const employeeId = selectedEmployee.id.toString();
                        const url = Globals.resolveUrl(`~/Recruitment/InboundEmail/AssignMessage?inboundEmailId=${selectedEmailId}&employeeId=${employeeId}`);
                        Forms.submit(url, "POST", false, true);
                    }
                });
        }, recruitersValuePickerUrlResolveCallback);
    }

    export function unassignMessage(grid: Grid.GridComponent) {
        const selectedEmailId = grid.getSelectedRowData<Grid.IRow>().map((r) => r.id);

        const confirmationMessageBody = Globals.resources.UnassignMessageConfirmationMessage;

        CommonDialogs.confirm(confirmationMessageBody,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = Globals.resolveUrl(`~/Recruitment/InboundEmail/UnassignMessage?inboundEmailId=${selectedEmailId}`);
                    Forms.submit(url, "POST", false, true);
                }
            });
    }

    export function sendForResearch(grid: Grid.GridComponent) {
        const selectedEmailId = grid.getSelectedRowData<Grid.IRow>().map((r) => r.id);

        CommonDialogs.confirm(Globals.resources.SendForResearchConfirmationMessage,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = Globals.resolveUrl(`~/Recruitment/InboundEmail/SendForResearch?inboundEmailId=${selectedEmailId}`);
                    Forms.submit(url, "POST", false, true);
                }
            });
    }

    export function getCandidateSearchParams() {
        let searchParams = "";
        const candidateFirstName = basicEncode($("#candidate-first-name").val());
        const candidateLastName = basicEncode($("#candidate-last-name").val());
        const candidatePhoneNumber = basicEncode($("#candidate-phone-number").val());
        const candidateEmailAddress = basicEncode($("#candidate-email-address").val());

        if (!Strings.isNullOrEmpty(candidateFirstName)) {
            searchParams += "&first-name=" + candidateFirstName;
        }

        if (!Strings.isNullOrEmpty(candidateLastName)) {
            searchParams += "&last-name=" + candidateLastName;
        }

        if (!Strings.isNullOrEmpty(candidatePhoneNumber)) {
            searchParams += "&phone-number=" + candidatePhoneNumber;
        }

        if (!Strings.isNullOrEmpty(candidateEmailAddress)) {
            searchParams += "&email-address=" + candidateEmailAddress;
        }

        return searchParams;
    }

    export function addCandidate() {
        const candidateFirstName = basicEncode($("#candidate-first-name").val());
        const candidateLastName = basicEncode($("#candidate-last-name").val());
        const candidatePhoneNumber = basicEncode($("#candidate-phone-number").val());
        const candidateEmailAddress = basicEncode($("#candidate-email-address").val());
        const candidatePicker = basicEncode($("[name=CandidateId]").val());
        const referenceNumber = $("#reference-number").val() || "-";
        const jobProfile = $("#job-profile-id").val() || "-";
        const applicationOriginId = $("[name='ApplicationOriginId']").val() || Globals.getParameterAsNumber("RecommendationApplicationOriginId");
        const documents = $("[name=TrackedDocuments]");
        const documentId = documents.length === 1 ? $("[name=TrackedDocuments]").attr("documentid") : "";
        const inboundEmailId = $("#id").val();

        if (!Strings.isNullOrEmpty(candidatePicker)) {
            CommonDialogs.alert({ 
                htmlMessage: Globals.resources.SelectedCandidateMessage, 
                title: "", 
                eventHandler: undefined, 
                iconName: "", 
                okButtonText: "" 
                });

            return;
        }

        const newCandidateUrl = `/Recruitment/Candidate/GetAddDialog?firstname=${candidateFirstName}&lastname=${candidateLastName}&emailaddress=${candidateEmailAddress}&mobilephone=${candidatePhoneNumber}&inboundemaildocumentid=${documentId}&inboundemailid=${inboundEmailId}&originalsourceid=${applicationOriginId}`;

        const onBeforeShow =
            (dialog: JQuery) => {
                Dialogs.setWidth(dialog, windowWidth);
            };

        Dialogs.showDialog({
            actionUrl: newCandidateUrl,
            eventHandler: result => {
                if (result.isOk) {
                    const newCandidateId = result.model.Id;
                    $("input[name=CandidateId]").val(newCandidateId);

                    addNoteForCandidateFromResearchingComment(newCandidateId);

                    result.dialog.remove();

                    saveAndStay();
                }
            },
            onBeforeShow
        });
    }

    export function addRecommendingPerson() {
        const recommendingPersonFirstName = basicEncode($("#recommending-person-first-name").val());
        const recommendingPersonLastName = basicEncode($("#recommending-person-last-name").val());
        const recommendingPersonEmailAdress = basicEncode($("#recommending-person-email-address").val());
        const recommendingPersonPhoneNumber = basicEncode($("#recommending-person-phone-number").val());
        const recommendingPersonPicker = basicEncode($("[name=RecommendingPersonId]").val());

        if (Strings.isNullOrEmpty(recommendingPersonPicker)) {
            const newRecommendingPersonUrl = `/Recruitment/RecommendingPerson/GetAddDialog?firstname=${recommendingPersonFirstName}&lastname=${recommendingPersonLastName}&emailaddress=${recommendingPersonEmailAdress}&phonenumber=${recommendingPersonPhoneNumber}`;
            const onBeforeShow =
                (dialog: JQuery) => {
                    Dialogs.setWidth(dialog, windowWidth);
                };

            Dialogs.showDialog({
                actionUrl: newRecommendingPersonUrl,
                eventHandler: result => {
                    if (result.isOk) {
                        const newRecommendingPersonId = result.model.Id;
                        $("input[name=RecommendingPersonId]").val(newRecommendingPersonId);

                        result.dialog.remove();

                        saveAndStay();
                    }
                },
                onBeforeShow
            });
        } else {
            CommonDialogs.alert({ htmlMessage: Globals.resources.SelectedRecommendingPersonMessage, title: "", eventHandler: undefined, iconName: "", okButtonText: ""});
        }
    }

    export function addJobApplication() {
        const jobApplicationId = $("input[name=JobApplicationId]").val();
        const originId = $("input[name=ApplicationOriginId]").val();
        const cityIds = $("input[name=CityId]").val();
        const jobProfileId = $("input[name=JobProfileId]").val();
        const candidateId = $("input[name=CandidateId]").val();
        const recommendingPersonId = $("input[name=RecommendingPersonId]").val();
        const inboundEmailId = $("#id").val();
        const referenceNumber = basicEncode($("#reference-number").val());

        const getOriginTypeUrl = `/Recruitment/JobApplication/GetOriginType?applicationOriginId=${originId}`;

        if (!Strings.isNullOrEmpty(jobApplicationId)) {
            CommonDialogs.alert({ 
                htmlMessage: Globals.resources.SelectedJobApplicationMessage, 
                title: "", 
                eventHandler: undefined, 
                iconName: "", 
                okButtonText: "" 
                });

            return;
        }

        if (Strings.isNullOrEmpty(candidateId)) {
            CommonDialogs.alert({
                htmlMessage: Globals.resources.NotSelectedCandidateMessage, 
                title: "", 
                eventHandler: undefined, 
                iconName: "", 
                okButtonText: ""
            });

            return;
        }

        if (recommendingPersonId !== undefined) {
            if (Strings.isNullOrEmpty(recommendingPersonId)) {
                CommonDialogs.alert({ 
                    htmlMessage: Globals.resources.NotSelectedRecommendingPersonMessage, 
                    title: "", 
                    eventHandler: undefined, 
                    iconName: "", 
                    okButtonText: "" 
                    });

                return;
            }

            openNewRecommendationWindow(recommendationOriginType, candidateId, inboundEmailId, recommendingPersonId);

            return;
        }

        if (Strings.isNullOrEmpty(originId)) {
            CommonDialogs.alert({
                htmlMessage: Globals.resources.NotSelectedOriginMessage,
                title: "",
                eventHandler: undefined,
                iconName: "",
                okButtonText: ""
            });

            return;
        }

        $.ajax({
            type: "POST",
            url: getOriginTypeUrl,
            success: (response, statusText, jqXhr) => {
                openNewJobApplicationWindow(response, candidateId, jobProfileId, cityIds, inboundEmailId, originId, referenceNumber);
            }
        });
    }

    function openNewJobApplicationWindow(originType: string, candidateId: number, jobProfileId: number, cityIds: number, inboundEmailId: number, originId: number, referenceNumber: string | null) {
        const JobApplicationIdField = $("input[name=JobApplicationId]");

        let addJobApplicationUrl = `/Recruitment/JobApplication/GetAddDialog?origin-type=${originType}&candidateid=${candidateId}&jobprofileids=${jobProfileId}&cityids=${cityIds}&sourceemailid=${inboundEmailId}&applicationoriginid=${originId}`;

        if (!Strings.isNullOrEmpty(referenceNumber)) {
            addJobApplicationUrl += `&origincomment=Reference Number: ${referenceNumber}`;
        }

        const onBeforeShow =
            (dialog: JQuery) => {
                Dialogs.setWidth(dialog, windowWidth);
            };

        Dialogs.showDialog({
            actionUrl: addJobApplicationUrl, 
            eventHandler: result => {
                if (result.isOk) {
                    const newJobApplication = result.model.Id;
                    JobApplicationIdField.val(newJobApplication);

                    result.dialog.remove();

                    saveAndStay();
                }
            }, 
            onBeforeShow
        });
    }

    function openNewRecommendationWindow(originType: string, candidateId: number, inboundEmailId: number, recommendingPersonId: number) {
        const JobApplicationIdField = $("input[name=JobApplicationId]");

        const addJobApplicationUrl = `/Recruitment/JobApplication/GetAddDialog?origin-type=${originType}&candidateid=${candidateId}&sourceemailid=${inboundEmailId}&recommendingpersonid=${recommendingPersonId}`;
        const onBeforeShow =
            (dialog: JQuery) => {
                Dialogs.setWidth(dialog, windowWidth);
            };

        Dialogs.showDialog({
            actionUrl: addJobApplicationUrl, 
            eventHandler: result => {
                if (result.isOk) {
                    const newJobApplication = result.model.Id;
                    JobApplicationIdField.val(newJobApplication);

                    result.dialog.remove();

                    saveAndStay();
                }
            }, 
            onBeforeShow
        });
    }

    function basicEncode(value: string) {
        if (!Strings.isNullOrEmpty(value)) {
            return value.replace(/</g, "(").replace(/>/g, ")");
        }

        return "";
    }

    function saveAndStay() {
        $("button[name=edit-and-stay]").click();
    }

    export function getRecommendingPersonSearchParams() {
        let searchParams = "";
        const recommendingPersonFirstName = basicEncode($("#recommending-person-first-name").val());
        const recommendingPersonLastName = basicEncode($("#recommending-person-last-name").val());
        const recommendingPersonPhoneNumber = basicEncode($("#recommending-person-phone-number").val());
        const recommendingPersonEmailAddress = basicEncode($("#recommending-person-email-address").val());

        if (!Strings.isNullOrEmpty(recommendingPersonFirstName)) {
            searchParams += "&first-name=" + recommendingPersonFirstName;
        }

        if (!Strings.isNullOrEmpty(recommendingPersonLastName)) {
            searchParams += "&last-name=" + recommendingPersonLastName;
        }

        if (!Strings.isNullOrEmpty(recommendingPersonPhoneNumber)) {
            searchParams += "&phone-number=" + recommendingPersonPhoneNumber;
        }

        if (!Strings.isNullOrEmpty(recommendingPersonEmailAddress)) {
            searchParams += "&email-address=" + recommendingPersonEmailAddress;
        }

        return searchParams;
    }

    export function assignMessageInForm() {
        const category = $("[name='Category']:checked").val();
        const recommendingPersonId = basicEncode($("[name=RecommendingPersonId").val());

        if (category === "3" && Strings.isNullOrEmpty(recommendingPersonId)) { // category == recommendation
            CommonDialogs.alert({ 
                htmlMessage: Globals.resources.NotSelectedRecommendingPersonMessage, 
                title: "", 
                eventHandler: undefined, 
                iconName: "", 
                okButtonText: "" 
                });

            return;
        }

        ValuePicker.selectSingle(employeeValuePickerIdentifier, (selectedEmployee: Grid.IRow) => {
            if (selectedEmployee === null) {
                return;
            }

            const confirmationMessageBody = Strings.format(Globals.resources.AssignMessageConfirmationMessage, selectedEmployee.toString);

            CommonDialogs.confirm(confirmationMessageBody,
                Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                    if (eventArgs.isOk) {
                        $("#workflow-action").val(WorkflowAction.Assign);
                        $("#workflow-processed-by-id").val(selectedEmployee.id.toString());
                        submitViaEditButton();
                    }
                });
        }, recruitersValuePickerUrlResolveCallback);
    }

    export function unassignMessageInForm() {
        const confirmationMessageBody = Globals.resources.UnassignMessageConfirmationMessage;

        CommonDialogs.confirm(confirmationMessageBody,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    $("#workflow-action").val(WorkflowAction.Unassign);
                    submitViaEditButton();
                }
            });
    }

    export function sendForResearchMessageInForm() {
        CommonDialogs.confirm(Globals.resources.SendForResearchConfirmationMessage,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    $("#workflow-action").val(WorkflowAction.SendForResearch);
                    submitViaEditButton();
                }
            });
    }

    let initiallySelectedCategoryControl: HTMLInputElement;

    export function assignWorkflowActionAndCallSubmit(workflowAction: WorkflowAction) {
        const confirmationMessageBody = Globals.resources.ChangeCategoryConfirmationMessage;

        CommonDialogs.confirm(confirmationMessageBody,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    $("#workflow-action").val(workflowAction);
                    submitViaEditButton();
                } else {
                    initiallySelectedCategoryControl.checked = true;
                }
            });
    }

    export function closeFromViewForm(id: number, modelId: string) {
        close(id, modelId, (e => {
            $("#workflow-action").val(WorkflowAction.Close);
            $("#closed-comment").val(e.Comment);
            $("#closed-reason").val(e.Reason);

            submitViaEditButton();
        }));
    }

    function submitViaEditButton() {
        const editButton = $("button[name='edit']").get(0);
        Forms.submitRecordForm(editButton);
    }

    function close(openJobId: number, modelId: string, action: (e: ICloseInboundEmail) => void) {
        Dialogs.showModel({
            modelId: modelId, 
            title: Globals.resources.CloseDialogTitle, 
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const comment = eventArgs.model.Comment != null ? eventArgs.model.Comment : "";
                    const reason = eventArgs.model.Reason;
                    action.call(null, { Comment: comment, Reason: reason });
                }
            }, 
            iconName: "", 
            standardButtons: StandardButtons.OkCancel
        });
    }

    function addNoteForCandidateFromResearchingComment(candidateId: number) {
        const resarchingComment = $("#researching-comment").val();

        if (resarchingComment.length === 0 || resarchingComment === "") {
            return;
        }

        $.post(`/Recruitment/Candidate/AddNote`, {
            Content: resarchingComment,
            ParentId: candidateId
        });
    }

    function initalizeIFrameForEdgeAndIe() {
        const iframe = $(".email-body-frame");

        iframe.ready(() => {
            const browserType = BrowserSurvey.detectBrowserType();

            if (browserType === BrowserSurvey.browserType.Edge
                || browserType === BrowserSurvey.browserType.Ie) {
                iframe.contents().find("body").html(
                    iframe.attr("srcdoc")
                );
            }
        });
    }

    $(document).ready(() => {
        initiallySelectedCategoryControl = $("[name='Category']:checked").get(0) as HTMLInputElement;
        initalizeIFrameForEdgeAndIe();
    });
}
