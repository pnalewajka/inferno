﻿﻿﻿namespace RecommendingPerson {
    import StandardButtons = Dialogs.StandardButtons;

    export function addNoteFromViewForm(button: Element, id: number, modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.NoteDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = `~/Recruitment/RecommendingPerson/AddNote`;
                    const model = {
                        parentId: id,
                        noteVisibility: eventArgs.model.NoteVisibility,
                        content: eventArgs.model.Content
                    };
                    $.post(Globals.resolveUrl(url), model, () => {
                        window.location.reload(true);
                    });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }
}
