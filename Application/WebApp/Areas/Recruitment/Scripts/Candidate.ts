﻿﻿namespace Candidate {
    enum DataConsentType {
        CandidateConsentGeneral = 1,
        CandidateConsentSpecificPosition = 2,
        RecommendingPersonConsentRecommendation = 3,
        CandidateConsentLegacy = 4,
        CandidateConsentSocialNetwork = 5,
        CandidateConsentInitialContact = 6,
    }

    import StandardButtons = Dialogs.StandardButtons;

    export function InvokeActionFromViewForm(action: string, id: number) {
        const url = `~/Recruitment/Candidate/${action}?Ids=${id}`;
        Forms.submit(Globals.resolveUrl(url), "POST");
    }

    export function addNoteFromViewForm(button: Element, id: number, modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.NoteDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = `~/Recruitment/Candidate/AddNote`;
                    const model = {
                        parentId: id,
                        noteVisibility: eventArgs.model.NoteVisibility,
                        content: eventArgs.model.Content
                    };
                    $.post(Globals.resolveUrl(url), model).then(() => {
                        window.location.reload(true);
                    });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function OnShouldAddRelatedDataConsentChanged(): void {
        const shouldShow = $("[name='ShouldAddRelatedDataConsent']").is(":checked");
        $("[name='ConsentType']").parents("div.form-group").toggleClass("collapse", !shouldShow);
        $("[data-field-name='ConsentFiles']").toggleClass("collapse", !shouldShow);
        $("[data-field-name='InboundEmailDocumentId']").parents("div.form-group").toggleClass("collapse", !shouldShow);
        OnConsentTypeChanged();
    }

    export function OnConsentTypeChanged(): void {
        const shouldShow = $("[name='ConsentType']:checked").val() === DataConsentType.CandidateConsentSpecificPosition.toString() && !$("[name='ConsentType']").parents("div.form-group").hasClass("collapse");
        $("[name='ConsentScope']").parents("div.form-group").toggleClass("collapse", !shouldShow);
    }

    export function OnCanRelocateChanged(): void {
        const shouldShow = $("[name='CanRelocate']").is(":checked");
        $("[name='RelocateDetails']").parents("div.form-group").toggleClass("collapse", !shouldShow);
    }

    export function ShowDialog(url: string): void {
        const dialogWidth = "1000px";
        const onBeforeShow = (dialog: JQuery) => { Dialogs.setWidth(dialog, dialogWidth); };

        Dialogs.showDialog({
            actionUrl: url, onBeforeShow: onBeforeShow, eventHandler: (result) => {
                if (result.isOk) {
                    Utils.reloadPage(); // needed to prevent calling HandleAddDialog on main form after dialog submit action
                }
            }
        });
    }
}

$(() => {
    const candidateId = Number($("#id").val());

    Dante.RecruitmentProcess.Grid.Initialize(candidateId, Dante.RecruitmentProcess.Grid.ProcessGridMode.Candidate);
});
