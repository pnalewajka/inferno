﻿namespace Dante.RecruitmentProcess.Grid {
    class RecruitmentProcessStepGridControl extends Atomic.Ui.Grid<IRecruitmentProcessStepViewModel> {
        protected getCssClassName(): string {
            return "table react-grid";
        }
    }

    export class RecruitmentProcessStepGrid extends Dante.Components.StateComponent<IRecruitmentProcessStepGridProps, IRecruitmentProcessStepGridState> {
        public constructor(props: IRecruitmentProcessStepGridProps) {
            super({
                Items: props.Items
            });
        }

        public render(): JSX.Element {
            return <div className="recruitment-process-step-grid" >
                {this.state.Items.length > 0 && <RecruitmentProcessStepGridControl columns={this.columns} rows={this.state.Items} />}
            </div>;
        }

        private columns: Array<Atomic.Ui.IGridColumnDefinition<IRecruitmentProcessStepViewModel>> = [
            {
                code: "step-name",
                columnCssClass: "recruitment-process-step",
                resolveComponent: (cellData, model) => <a href={`/Recruitment/RecruitmentProcessStep/Details/${model.id}`}>{model.typeDescription}</a>
            },
            {
                code: "planned-on",
                columnCssClass: "recruitment-process-step",
                resolveComponent: (cellData, model) => {
                    const date = model.plannedOn || model.dueOn;

                    const iconCssClass = model.status === RecruitmentProcessStepStatus.ToDo
                        ? (model.isPending ? "fa fa-warning" : "fa fa-hourglass-o")
                        : (model.decision === RecruitmentProcessStepDecision.Positive ? "fa fa-thumbs-o-up" : "fa fa-thumbs-o-down");

                    if (date !== null) {
                        return (
                            <span> <i className={iconCssClass} /> {new Date(date).toLocaleDateString()}</span>
                            );
                    }

                    return null;
                }
            },
            {
                code: "assigned-employee",
                columnCssClass: "recruitment-process-step",
                resolveComponent: (cellData, model) => <span>{model.assigneeFullName}</span>
            }];
    }

    export interface IRecruitmentProcessStepViewModel {
        typeDescription: string;
        assigneeFullName: string;
        status: RecruitmentProcessStepStatus;
        plannedOn: string | null;
        dueOn: string | null;
        id: number;
        isPending: boolean;
        decision: RecruitmentProcessStepDecision;
    }

    export enum RecruitmentProcessStepStatus {
        ToDo = 1,
        Done = 2
    }

    export enum RecruitmentProcessStepDecision {
        Positive = 1,
        Negative = 2
    }

    export interface IRecruitmentProcessStepGridProps {
        Items: IRecruitmentProcessStepViewModel[];
    }

    export interface IRecruitmentProcessStepGridState {
        Items: IRecruitmentProcessStepViewModel[];
    }
}
