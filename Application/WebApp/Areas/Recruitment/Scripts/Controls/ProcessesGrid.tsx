﻿namespace Dante.RecruitmentProcess.Grid {
    export function Initialize(parentId: number, mode: ProcessGridMode): void {
        const renderElement = $("#recruitment-processes-grid");

        if (renderElement !== null) {
            ReactDOM.render(<div><ProcessGrid ParentId={parentId} Mode={mode} /></div>, renderElement[0]);
        }
    }

    class ProcessGrid extends Dante.Components.StateComponent<IRecruitmentProcessGridProps, IRecruitmentProcessGridState> {
        public constructor(props: IRecruitmentProcessGridProps) {
            super({
                $isLoading: true,
                Items: []
            });
        }

        public componentWillMount(): void {
            const endpoint = this.getEndpoint(this.props.Mode);

            $.getJSON(`/Recruitment/${endpoint}/GetJson?parent-id=${this.props.ParentId}&order=last-activity-on-desc`)
                .done((data: IRecruitmentProcessViewModel[]) => this.updateState({ Items: data, $isLoading: false }));
        }

        public render(): JSX.Element {
            return (
                <div>
                    {this.state.$isLoading && <Dante.Ui.ProgressIndicator />}
                    {!this.state.$isLoading && <RecruitmentProcessGrid Items={this.state.Items} Mode={this.props.Mode} />}
                </div>
            );
        }

        private getEndpoint(mode: ProcessGridMode): string {
            switch (mode) {
                case ProcessGridMode.Candidate:
                    return "CandidateRecruitmentProcess";
                    
                case ProcessGridMode.JobOpening:
                    return "JobOpeningRecruitmentProcess";
                    
                default:
                    throw new Error("not implemented");
            }
        }
    }
       
    export interface IRecruitmentProcessGridProps {
        ParentId: number;
        Mode: ProcessGridMode;
    }

    export interface IRecruitmentProcessGridState {
        $isLoading: boolean;
        Items: IRecruitmentProcessViewModel[];
    }

    export enum ProcessGridMode {
        Candidate = 1,
        JobOpening = 2
    }
}
