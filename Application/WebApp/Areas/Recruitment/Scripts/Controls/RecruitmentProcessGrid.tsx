﻿namespace Dante.RecruitmentProcess.Grid {
    class RecruitmentProcessGridControl extends Atomic.Ui.Grid<IRecruitmentProcessViewModel> { }

    interface IRecruitmentProcessGridColumnDefinition extends Atomic.Ui.IGridColumnDefinition<IRecruitmentProcessViewModel> {
        mode?: ProcessGridMode;
    }

    export class RecruitmentProcessGrid extends Dante.Components.StateComponent<IRecruitmentProcessControlGridProps, IRecruitmentProcessControlGridState> {
        public constructor(props: IRecruitmentProcessControlGridProps) {
            super({
                Items: props.Items
            });
        }

        public render(): JSX.Element {
            return <RecruitmentProcessGridControl
                columns={this.columns.filter((el: IRecruitmentProcessGridColumnDefinition) => el.mode === undefined || el.mode === this.props.Mode)}
                rows={this.state.Items} />;
        }

        private columns: IRecruitmentProcessGridColumnDefinition[] = [
            {
                code: "job-opening",
                label: Globals.resources.ProcessJobOpening,
                mode: ProcessGridMode.Candidate,
                resolveComponent: (cellData, model) => <a href={`/Recruitment/JobOpening/Details/${model.jobOpeningId}`}>{model.positionName}</a>
            },
            {
                code: "candidate",
                label: Globals.resources.ProcessCandidate,
                mode: ProcessGridMode.JobOpening,
                resolveComponent: (cellData, model) => <a href={`/Recruitment/Candidate/Details/${model.candidateId}`}>{model.candidateName}</a>
            },
            {
                code: "decision-maker",
                label: Globals.resources.ProcessDecisionMaker,
                resolveComponent: (cellData, model) => <span>{model.decisionMakerName}</span>
            },
            {
                code: "recruiter",
                label: Globals.resources.ProcessRecruiter,
                resolveComponent: (cellData, model) => <span onClick={() => Hub.open(model.recruiterId, model.recruiterName)}>{model.recruiterName}</span>
            },
            {
                code: "status",
                label: Globals.resources.ProcessStatus,
                resolveComponent: (cellData, model) => <span>{model.statusName}</span>
            },
            {
                code: "planned-step",
                label: Globals.resources.ProcessStepsToDo,
                resolveComponent: (cellData, model) => <RecruitmentProcessStepGrid
                    Items={model.stepInfos.filter((el: IRecruitmentProcessStepViewModel) => el.status === RecruitmentProcessStepStatus.ToDo)} />    
            },
            {
                code: "done-step",
                label: Globals.resources.ProcessStepsDone,
                resolveComponent: (cellData, model) => <RecruitmentProcessStepGrid
                    Items={model.stepInfos.filter((el: IRecruitmentProcessStepViewModel) => el.status === RecruitmentProcessStepStatus.Done)} />      
            }];
    }
    
    export interface IRecruitmentProcessViewModel {
        jobOpeningId: number;
        candidateId: number;
        candidateName: string;
        positionName: string;
        decisionMakerName: string;
        recruiterName: string;
        recruiterId: number;
        statusName: string;
        stepInfos: IRecruitmentProcessStepViewModel[];
    }

    export interface IRecruitmentProcessControlGridProps {
        Items: IRecruitmentProcessViewModel[];
        Mode: ProcessGridMode;
    }

    export interface IRecruitmentProcessControlGridState {
        Items: IRecruitmentProcessViewModel[];
    }
}
