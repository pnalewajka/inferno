﻿namespace RecruitmentProcess {
    interface ICloseRecruimentProcessOpening {
        Comment: string;
        Reason: number;
    }

    interface IReopenRecruimentProcessOpening {
        Comment: string;
    }

    interface IRecruitmentProcessViewModel extends Grid.IRow {
        isReopenedForAdjustments: boolean;
    }

    import StandardButtons = Dialogs.StandardButtons;

    export function InvokeActionFromViewForm(action: string, id: number) {
        const url = `~/Recruitment/RecruitmentProcess/${action}?ids=${id}`;
        Forms.submit(Globals.resolveUrl(url), "POST");
    }

    export function CloseFromGrid(grid: Grid.GridComponent, modelId: string) {
        const recruitmentProcessViewModel = grid.getSelectedRowData<IRecruitmentProcessViewModel>()[0];

        close(recruitmentProcessViewModel.id, modelId, e => closeRequest(recruitmentProcessViewModel.id, e));
    }

    export function CloseFromView(button: Element, id: number, modelId: string) {
        close(id, modelId, e => closeRequest(id, e));
    }

    export function CloseFromEditForm(button: Element, id: number, modelId: string, workflowAction: number) {
        close(id, modelId, (e => {
            $("#workflow-action").val(workflowAction);
            $("#workflow-closed-comment").val(e.Comment);
            $("#workflow-closed-reason").val(e.Reason);

            Forms.submitRecordForm(button);
        }));
    }

    export function addNoteFromViewForm(button: Element, id: number, modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.NoteDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    $.post(`/Recruitment/RecruitmentProcess/AddNote`,
                        { parentId: id, noteVisibility: eventArgs.model.NoteVisibility, content: eventArgs.model.Content },
                        (response, statusText, jqXhr) => {
                            window.location.reload(true);
                        });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function Clone(grid: Grid.GridComponent, modelId: string) {
        const selectedId = grid.getSelectedRowData().map(r => r.id.toString())[0];
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.JobOpeningPickerTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    $.post(`/Recruitment/RecruitmentProcess/Clone`,
                        { id: selectedId, jobOpeningId: eventArgs.model.JobOpeningId },
                        (response, statusText, jqXhr) => {
                            window.location.reload(true);
                        });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    function close(openJobId: number, modelId: string, action: (e: ICloseRecruimentProcessOpening) => void) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.CloseDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const closedComment = eventArgs.model.Comment != null ? eventArgs.model.Comment : "";
                    const closedReason = eventArgs.model.Reason;
                    action.call(null, { Comment: closedComment, Reason: closedReason });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    function closeRequest(id: number, e: ICloseRecruimentProcessOpening) {
        $.post(
            `/Recruitment/RecruitmentProcess/CloseRecruitmentProcess`,
            { id, reason: e.Reason, comment: e.Comment },
            (response, statusText, jqXhr) => {
                window.location.reload(true);
            });
    }

    export function ReopenProcessFromGrid(grid: Grid.GridComponent, modelId: string) {
        const [selectedId] = grid.getSelectedRowData().map(r => +r.id);

        ReopenProcess(selectedId, modelId);
    }

    export function ReopenProcess(recruitmentProcessId: number, modelId: string) {
        Dialogs.showModel({
            modelId,
            title: Globals.resources.ReopenProcessDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const comment = eventArgs.model.Comment != null ? eventArgs.model.Comment : "";

                    reopenRequest(recruitmentProcessId, { Comment: comment });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
            }
        );
    }

    function reopenRequest(id: number, e: IReopenRecruimentProcessOpening) {
        $.post(
            `/Recruitment/RecruitmentProcess/ReopenProcess`,
            { id, comment: e.Comment },
            (response, statusText, jqXhr) => {
                window.location.reload(true);
            });
    }
}
