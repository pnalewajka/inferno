﻿namespace RecruitmentProcessStep {
    import StandardButtons = Dialogs.StandardButtons;

    enum WorkflowAction {
        ApproveCandidate = 1,
        RejectCandidate = 2,
        PutProcessOnHold = 3,
        Cancel = 4,
        AddHrInterview = 5,
        AddTechnicalReview = 6,
        AddClientInterview = 7,
        AddHiringManagerInterview = 8,
        AddHiringManagerDecision = 9,
        SendResumeToHiringManager = 10,
        SendResumeToClient = 11,
        StartContractNegotations = 12,
        RefreshAndRunScript = 128
    }

    export function refreshAndRunScript(button: Element, script: string) {
        $("#workflow-action").val(WorkflowAction.RefreshAndRunScript);
        $("#workflow-script").val(script);

        Forms.submitRecordForm(button);
    }

    export function reopenProcessStepFromGrid(grid: Grid.GridComponent, parentId: number) {
        const selectedId = grid.getSelectedRowData().map(r => r.id.toString())[0];
        reopenProcessStep(selectedId, parentId);
    }

    export function reopenProcessStep(processStepId: number, parentId: number) {
        const confirmationMessageBody = Strings.format(Globals.resources.ReopenStepConfirmationMessage);

        CommonDialogs.confirm(confirmationMessageBody,
            Globals.resources.ToolbarButtonConfirmationDialogTitle, (eventArgs) => {
                if (eventArgs.isOk) {
                    const url = `~/Recruitment/RecruitmentProcessStep/ReopenProcessStep?processStepId=${processStepId}&parent-id=${parentId}`;
                    Forms.submit(Globals.resolveUrl(url), "GET");
                }
            });
    }

    export function rejectCandidate(button: Element, modelId: string) {
        if (!validateForm()) {
            return;
        }

        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.RejectDialogTitle,
            eventHandler: (eventArgs) => {
                const isValid = $("form", eventArgs.dialog).valid();

                if (!eventArgs.isOk && !isValid) {
                    return;
                }

                const rejectComment = eventArgs.model.RejectionComment;
                $("#close-current-step").prop("checked", true);
                $("#decision-comment").val(rejectComment);
                executeWorkflowAction(button, WorkflowAction.RejectCandidate);
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function executeWorkflowAction(button: Element, workflowAction: WorkflowAction) {
        if (!validateForm()) {
            return;
        }

        $("#workflow-action").val("");

        if (!canProceedWithWorkflowAction(workflowAction)) {
            CommonDialogs.alert({ 
                htmlMessage: Globals.resources.ContractNegotiationsFinalErrorMessage, 
                title: Globals.resources.ErrorMessageTitle, 
                eventHandler: undefined, 
                iconName: "", 
                okButtonText: "" 
                });

            return;
        }

        CommonDialogs.confirm(Globals.resources.ConfirmationQuestion, Globals.resources.ConfirmationTitle, eventArgs => {
            if (!eventArgs.isOk) {
                return;
            }
            $("#close-current-step").prop("checked", true);
            $("#workflow-action").val(workflowAction);

            Forms.submitRecordForm(button);
        }, "", "");
    }

    function canProceedWithWorkflowAction(workflowAction: WorkflowAction): boolean {
        const contractNegotiationStep = 900;

        if (workflowAction !== WorkflowAction.ApproveCandidate || Number($("#type").val()) !== contractNegotiationStep) {
            return true;
        }

        let isDataValid = true;
        $("#contract-negotiation-final :input[name!=FinalPlaceOfWork][name!=FinalComment]").each((i, val: Element) =>
            isDataValid = isDataValid && !Strings.isNullOrEmpty((val as HTMLInputElement).value));

        return isDataValid;
    }

    export function addNextStep(nextStepType: string, isCloseDecisionAllowed: boolean, workflowAction: WorkflowAction) {
        const processId = $("input[name=RecruitmentProcessId]").val();
        const currentType = $("input[id=type-display-value]").val();
        const dialogWidth = "1000px";
        const onBeforeShow = (dialog: JQuery) => { Dialogs.setWidth(dialog, dialogWidth); };
        const stepId = $("#id").val();

        const url = `/Recruitment/RecruitmentProcessStep/GetAddDialog?type=${nextStepType}&parent-id=${processId}&source-step-type=${currentType}&allow-close-decision=${isCloseDecisionAllowed}&FromStepId=${stepId}`;

        $("#workflow-action").val("");

        Dialogs.showDialog({
            actionUrl: url,
            eventHandler: result => {
                if (result.isOk) {
                    const nextStepId = result.model.Id;
                    const closePreviousStep = (result.dialog.find("#close-current-step")[0] as HTMLInputElement).checked;
                    result.dialog.remove();

                    CommonDialogs.alert({
                        htmlMessage: Globals.resources.NextStepAddedSuccessfullyText,
                        title: Globals.resources.NextStepAddedSuccessfullyDialogTitle,
                        eventHandler: r => {
                            $("#close-current-step").prop("checked", closePreviousStep);
                            $("#next-step-id").val(nextStepId);
                            $("#workflow-action").val(closePreviousStep ? WorkflowAction.ApproveCandidate : workflowAction);

                            $(".form-footer button[name=edit]").click();
                        },
                        iconName: "",
                        okButtonText: ""
                    });
                }
            },
            onBeforeShow
        });
    }

    export function addNoteFromViewForm(button: Element, id: number, modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.NoteDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    $.ajax({
                        type: "POST",
                        data: { parentId: id, noteVisibility: eventArgs.model.NoteVisibility, content: eventArgs.model.Content },
                        url: `/Recruitment/RecruitmentProcessStep/AddRecruitmentProcessNote`,
                        success: (response, statusText, jqXhr) => {
                            window.location.reload(true);
                        }
                    });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    export function GenerateTechnicalReviewSummaryReport(id: number) {
        const dialogWidth: string = "1000px";
        const url = `~/Reporting/ReportExecution/ConfigureReportDialog?reportCode=TechnicalReviewSummaryReport&RecruitmentProcessStepId=${id}`;

        Dialogs.showDialog({
            actionUrl: url,
            onBeforeInit:
            (dialog) => {
                Dialogs.setWidth(dialog, dialogWidth);
            }
        });
    }

    export function CloseProcess(modelId: string) {
        Dialogs.showModel({
            modelId: modelId,
            title: Globals.resources.CloseRecruitmentProcessDialogTitle,
            eventHandler: (eventArgs) => {
                if (eventArgs.isOk) {
                    const closedComment = eventArgs.model.Comment != null ? eventArgs.model.Comment : "";
                    const closedReason = eventArgs.model.Reason;
                    const processId = $("input[name=RecruitmentProcessId]").val();

                    $.post(
                        `/Recruitment/RecruitmentProcess/CloseRecruitmentProcess`,
                        { id: processId, reason: closedReason, comment: closedComment },
                        (response, statusText, jqXhr) => {
                            $(".form-footer button[name=cancel]").click();
                        });
                }
            },
            iconName: "",
            standardButtons: StandardButtons.OkCancel
        });
    }

    function validateForm(): boolean {
        const validationResult = $("#main-form").valid();

        if (!validationResult) {
            $("#workflow-action").val("");
        }

        return validationResult;
    }
}
