﻿namespace ContactRecord {
    enum ContactType {
        Email = 1,
        Telephone = 2,
        Skype = 3,
        SocialNetwork = 4,
        Meeting = 5,
        Other = 6,
    }

    function ContactVia(event: Event, contactType: ContactType, contactController: string): void {
        const element = Utils.getEventTarget(event);
        const grid = Controls.getInstance(Grid.GridComponent, element);
        const rowData = grid.getRowDataFor(element);
        const rowId = rowData.id;

        const url = grid.resolveUrlParameters(`/Recruitment/${contactController}/GetAddDialog?parent-id=${rowId}&ContactType=${contactType}`);

        Grid.onHandlePageReloadingDialog(url, event);
    }

    export function ContactCandidateViaEmail(event: Event): void {
        ContactVia(event, ContactType.Email, "CandidateContactRecord");
    }

    export function ContactCandidateViaSkype(event: Event): void {
        ContactVia(event, ContactType.Skype, "CandidateContactRecord");
    }

    export function ContactRecommendingPersonViaEmail(event: Event): void {
        ContactVia(event, ContactType.Email, "RecommendingPersonContactRecords");
    }

    export function ContactCandidateViaSocial(event: Event): void {
        ContactVia(event, ContactType.SocialNetwork, "CandidateContactRecord");
    }
}

$(document).ready(() => {
    $(".candidate-record-contact-skype a").click(($event) => {
        ContactRecord.ContactCandidateViaSkype($event.originalEvent);
    });

    $(".candidate-record-contact-email a").click(($event) => {
        ContactRecord.ContactCandidateViaEmail($event.originalEvent);
    });

    $(".recommending-person-record-contact-email a").click(($event) => {
        ContactRecord.ContactRecommendingPersonViaEmail($event.originalEvent);
    });

    $(".candidate-record-contact-social a").click(($event) => {
        ContactRecord.ContactCandidateViaSocial($event.originalEvent);
    });
});
