﻿﻿namespace DocumentViewer {
    export function showDownloadDialog(url: string) {
        $(".modal").modal("hide");
        Dialogs.showDialog({ actionUrl: url });
    }
}
