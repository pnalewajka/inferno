﻿using System;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp.Areas.Recruitment
{
    public class BundleConfig
    {
        internal static void RegisterRecruitmentBundles(BundleCollection bundles)
        {
            var recruitmentBundle = new ScriptBundle("~/scripts/recruitment/controls")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder(false)
            }.IncludeDirectory("~/Areas/Recruitment/Scripts/Controls", "*.js", true);

            bundles.Add(recruitmentBundle);
        }
    }
}