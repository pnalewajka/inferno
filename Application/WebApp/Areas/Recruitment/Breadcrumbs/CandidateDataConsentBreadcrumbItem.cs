﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.GDPR.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Breadcrumbs
{
    public class CandidateDataConsentBreadcrumbItem : BreadcrumbItem
    {
        public CandidateDataConsentBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = DataConsentResources.Title;
        }
    }
}