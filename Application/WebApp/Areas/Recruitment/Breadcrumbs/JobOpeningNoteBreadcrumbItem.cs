﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Recruitment.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Recruitment.Breadcrumbs
{
    public class JobOpeningNoteBreadcrumbItem : BreadcrumbItem
    {
        public JobOpeningNoteBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = JobOpeningNoteResources.Title;
        }
    }
}