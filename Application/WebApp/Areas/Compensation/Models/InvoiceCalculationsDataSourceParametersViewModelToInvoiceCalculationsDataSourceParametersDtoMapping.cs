﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class InvoiceCalculationsDataSourceParametersViewModelToInvoiceCalculationsDataSourceParametersDtoMapping : ClassMapping<InvoiceCalculationsDataSourceParametersViewModel, InvoiceCalculationsDataSourceParametersDto>
    {
        public InvoiceCalculationsDataSourceParametersViewModelToInvoiceCalculationsDataSourceParametersDtoMapping()
        {
            Mapping = v => new InvoiceCalculationsDataSourceParametersDto
            {
                Year = v.Year,
                Month = v.Month
            };
        }
    }
}
