﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class BonusDtoToBonusViewModelMapping : ClassMapping<BonusDto, BonusViewModel>
    {
        public BonusDtoToBonusViewModelMapping()
        {
            Mapping = d => new BonusViewModel
            {
                Id = d.Id,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                EmployeeId = d.EmployeeId,
                EmployeeFullName = d.EmployeeFullName,
                ProjectId = d.ProjectId,
                BonusAmount = d.BonusAmount,
                CurrencyId = d.CurrencyId,
                Justification = d.Justification,
                Type = d.Type,
                Reinvoice = d.Reinvoice
            };
        }
    }
}
