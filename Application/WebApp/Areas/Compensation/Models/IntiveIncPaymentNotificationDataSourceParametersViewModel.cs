﻿using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    [Identifier("Models.IntiveIncPaymentNotificationDataSourceParameters")]
    public class IntiveIncPaymentNotificationDataSourceParametersViewModel
    {
        public long? BusinessTripSettlementRequestId { get; set; }

        public long? ExpenseRequestId { get; set; }
    }
}
