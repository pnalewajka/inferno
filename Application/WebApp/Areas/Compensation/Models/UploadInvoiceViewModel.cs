﻿using System.Web.Mvc;
using Smt.Atomic.Business.Compensation.DocumentMappings;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Compensation.Resources;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class UploadInvoiceViewModel
    {
        [DisplayNameLocalized(nameof(InvoiceResources.InvoiceLabel), typeof(InvoiceResources))]
        [DocumentUpload(typeof(InvoiceDocumentMapping))]
        public DocumentViewModel Invoice { get; set; }

        [HiddenInput]
        public long EmployeeId { get; set; }

        [HiddenInput]
        public int Year { get; set; }

        [HiddenInput]
        public byte Month { get; set; }
    }
}
