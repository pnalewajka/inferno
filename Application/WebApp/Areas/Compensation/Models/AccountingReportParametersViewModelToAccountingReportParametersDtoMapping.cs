﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class AccountingReportParametersViewModelToAccountingReportParametersDtoMapping : ClassMapping<AccountingReportParametersViewModel, AccountingReportParametersDto>
    {
        public AccountingReportParametersViewModelToAccountingReportParametersDtoMapping()
        {
            Mapping = v => new AccountingReportParametersDto
            {
                Year = v.Year,
                Month = v.Month,
                EmployeeIds = v.EmployeeIds
            };
        }
    }
}
