﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Compensation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Workflows.Resources;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class BonusViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "no-break-column")]
        [DisplayNameLocalized(nameof(BonusResources.StartDate), typeof(BonusResources))]
        public DateTime StartDate { get; set; }

        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "no-break-column")]
        [DisplayNameLocalized(nameof(BonusResources.EndDate), typeof(BonusResources))]
        public DateTime? EndDate { get; set; }

        [Required]
        [Render(GridCssClass = "no-break-column")]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(BonusResources.Employee), typeof(BonusResources))]
        public long EmployeeId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public bool ShouldRecalculateInvoicesAfterChange { get; set; }

        [Visibility(VisibilityScope.None)]
        public string EmployeeFullName { get; set; }

        [Required]
        [Render(GridCssClass = "no-break-column")]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        [DisplayNameLocalized(nameof(BonusResources.Project), typeof(BonusResources))]
        public long ProjectId { get; set; }

        [Required]
        [MinValue(1)]
        [DisplayNameLocalized(nameof(BonusResources.BonusAmount), typeof(BonusResources))]
        public decimal BonusAmount { get; set; }

        [Required]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        [DisplayNameLocalized(nameof(BonusResources.Currency), typeof(BonusResources))]
        public long CurrencyId { get; set; }

        [Required]
        [MaxLength(255)]
        [Multiline(3)]
        [Render(GridCssClass = "remaining-space-column")]
        [DisplayNameLocalized(nameof(BonusResources.Justification), typeof(BonusResources))]
        public string Justification { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BonusResources.Type), typeof(BonusResources))]
        public BonusType Type { get; set; }

        [Required]
        [DisplayNameLocalized(nameof(BonusRequestResource.Reinvoice), typeof(BonusRequestResource))]
        [RadioGroup(ItemProvider = typeof(YesNoItemProvider))]
        public bool Reinvoice { get; set; }

        public override string ToString()
        {
            return string.Format(BonusResources.BonusDescription, EmployeeFullName, StartDate, EndDate);
        }
    }
}
