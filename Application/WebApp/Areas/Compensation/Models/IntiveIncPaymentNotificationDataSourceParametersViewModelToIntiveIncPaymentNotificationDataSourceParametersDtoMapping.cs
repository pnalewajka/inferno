﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class IntiveIncPaymentNotificationDataSourceParametersViewModelToIntiveIncPaymentNotificationDataSourceParametersDtoMapping : ClassMapping<IntiveIncPaymentNotificationDataSourceParametersViewModel, IntiveIncPaymentNotificationDataSourceParametersDto>
    {
        public IntiveIncPaymentNotificationDataSourceParametersViewModelToIntiveIncPaymentNotificationDataSourceParametersDtoMapping()
        {
            Mapping = v => new IntiveIncPaymentNotificationDataSourceParametersDto
            {
                BusinessTripSettlementRequestId = v.BusinessTripSettlementRequestId,
                ExpenseRequestId = v.ExpenseRequestId
            };
        }
    }
}
