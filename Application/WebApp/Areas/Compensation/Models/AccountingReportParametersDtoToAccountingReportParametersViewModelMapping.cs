﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class AccountingReportParametersDtoToAccountingReportParametersViewModelMapping : ClassMapping<AccountingReportParametersDto, AccountingReportParametersViewModel>
    {
        public AccountingReportParametersDtoToAccountingReportParametersViewModelMapping()
        {
            Mapping = d => new AccountingReportParametersViewModel
            {
                Year = d.Year,
                Month = d.Month,
                EmployeeIds = d.EmployeeIds
            };
        }
    }
}
