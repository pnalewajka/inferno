﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class InvoiceDetailsDtoToInvoiceDetailsViewModelMapping : ClassMapping<InvoiceDetailsDto, InvoiceDetailsViewModel>
    {
        public InvoiceDetailsDtoToInvoiceDetailsViewModelMapping()
        {
            Mapping = d => new InvoiceDetailsViewModel
            {
                Month = d.Month,
                Year = d.Year,
                EmployeeId = d.EmployeeId,
                ContractorCompanyName = d.ContractorCompanyName,
                ContractorCompanyStreetAddress = d.ContractorCompanyStreetAddress,
                ContractorCompanyZipCode = d.ContractorCompanyZipCode,
                ContractorCompanyCity = d.ContractorCompanyCity,
                ContractorCompanyTaxIdentificationNumber = d.ContractorCompanyTaxIdentificationNumber,
                InvoiceNotificationStatus = d.InvoiceNotificationStatus,
                InvoiceNotificationErrorMessage = d.InvoiceNotificationErrorMessage,
                ServicesTotal = d.ServicesTotal,
                ExpensesTotal = d.ExpensesTotal,
                CompensationTotal = d.CompensationTotal,
                InvoiceStatus = d.InvoiceStatus,
                InvoiceIssues = d.InvoiceIssues,
                InvoiceRejectionReason = d.InvoiceRejectionReason,
                Invoice = d.Invoice == null ? null : new DocumentViewModel
                {
                    ContentType = d.Invoice.ContentType,
                    DocumentName = d.Invoice.DocumentName,
                    DocumentId = d.Invoice.DocumentId,
                }
            };
        }
    }
}
