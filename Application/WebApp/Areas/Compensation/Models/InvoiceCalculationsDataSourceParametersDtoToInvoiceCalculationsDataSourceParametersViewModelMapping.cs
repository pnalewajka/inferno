﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class InvoiceCalculationsDataSourceParametersDtoToInvoiceCalculationsDataSourceParametersViewModelMapping : ClassMapping<InvoiceCalculationsDataSourceParametersDto, InvoiceCalculationsDataSourceParametersViewModel>
    {
        public InvoiceCalculationsDataSourceParametersDtoToInvoiceCalculationsDataSourceParametersViewModelMapping()
        {
            Mapping = d => new InvoiceCalculationsDataSourceParametersViewModel
            {
                Year = d.Year,
                Month = d.Month
            };
        }
    }
}
