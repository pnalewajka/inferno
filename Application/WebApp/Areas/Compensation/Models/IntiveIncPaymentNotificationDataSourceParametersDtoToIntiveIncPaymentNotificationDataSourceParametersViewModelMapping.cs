﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class IntiveIncPaymentNotificationDataSourceParametersDtoToIntiveIncPaymentNotificationDataSourceParametersViewModelMapping : ClassMapping<IntiveIncPaymentNotificationDataSourceParametersDto, IntiveIncPaymentNotificationDataSourceParametersViewModel>
    {
        public IntiveIncPaymentNotificationDataSourceParametersDtoToIntiveIncPaymentNotificationDataSourceParametersViewModelMapping()
        {
            Mapping = d => new IntiveIncPaymentNotificationDataSourceParametersViewModel
            {
                BusinessTripSettlementRequestId = d.BusinessTripSettlementRequestId,
                ExpenseRequestId = d.ExpenseRequestId
            };
        }
    }
}
