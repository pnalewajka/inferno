﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    [Identifier("Models.AccountingReportParameters")]
    public class AccountingReportParametersViewModel : IValidatableObject
    {
        public int? Year { get; set; }

        public int? Month { get; set; }

        [DisplayNameLocalized(nameof(EmployeeResources.Employee), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        public long[] EmployeeIds { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Month.HasValue && (Month < 1 || Month > 12))
            {
                yield return new ValidationResult(nameof(Month));
            }
        }
    }
}
