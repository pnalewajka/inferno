﻿using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    public class BonusViewModelToBonusDtoMapping : ClassMapping<BonusViewModel, BonusDto>
    {
        public BonusViewModelToBonusDtoMapping()
        {
            Mapping = v => new BonusDto
            {
                Id = v.Id,
                StartDate = v.StartDate,
                EndDate = v.EndDate,
                EmployeeId = v.EmployeeId,
                ShouldRecalculateInvoicesAfterChange = v.ShouldRecalculateInvoicesAfterChange,
                ProjectId = v.ProjectId,
                BonusAmount = v.BonusAmount,
                CurrencyId = v.CurrencyId,
                Justification = v.Justification,
                Type = v.Type,
                Reinvoice = v.Reinvoice
            };
        }
    }
}
