﻿using System;
using System.Linq;
using Smt.Atomic.Business.Compensation.DocumentMappings;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Compensation.Resources;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    [FieldSetDescription(1, ContractorFieldSet, nameof(InvoiceResources.ContractorFieldSetLabel), typeof(InvoiceResources))]
    [FieldSetDescription(2, InvoiceNotificationFieldSet, nameof(InvoiceResources.InvoiceNotificationFieldSetLabel), typeof(InvoiceResources))]
    [FieldSetDescription(3, InvoiceFieldSet, nameof(InvoiceResources.InvoiceFieldSetLabel), typeof(InvoiceResources))]
    public class InvoiceDetailsViewModel
    {
        private const string ContractorFieldSet = "Contractor";
        private const string InvoiceNotificationFieldSet = "InvoiceNotification";
        private const string InvoiceFieldSet = "Invoice";

        [FieldSet(ContractorFieldSet)]
        [ValuePicker(Type = typeof(EmployeePickerController))]
        [DisplayNameLocalized(nameof(InvoiceResources.EmployeeLabel), typeof(InvoiceResources))]
        public long EmployeeId { get; set; }

        [FieldSet(ContractorFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ContractorCompanyNameLabel), typeof(InvoiceResources))]
        public string ContractorCompanyName { get; set; }

        [FieldSet(ContractorFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ContractorCompanyStreetAddressLabel), typeof(InvoiceResources))]
        public string ContractorCompanyStreetAddress { get; set; }

        [FieldSet(ContractorFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ContractorCompanyZipCodeLabel), typeof(InvoiceResources))]
        public string ContractorCompanyZipCode { get; set; }

        [FieldSet(ContractorFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ContractorCompanyCityLabel), typeof(InvoiceResources))]
        public string ContractorCompanyCity { get; set; }

        [FieldSet(ContractorFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ContractorCompanyTaxIdentificationNumberLabel), typeof(InvoiceResources))]
        public string ContractorCompanyTaxIdentificationNumber { get; set; }

        [FieldSet(InvoiceNotificationFieldSet)]
        [Tooltip(nameof(InvoiceNotificationStatusField))]
        [DisplayNameLocalized(nameof(InvoiceResources.InvoiceNotificationStatusLabel), typeof(InvoiceResources))]
        public string InvoiceNotificationStatusField => string.IsNullOrEmpty(InvoiceNotificationErrorMessage)
            ? InvoiceNotificationStatus.GetDescription()
            : $"{InvoiceNotificationStatus.GetDescription()} ({InvoiceNotificationErrorMessage})";

        [FieldSet(InvoiceNotificationFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ServicesTotalLabel), typeof(InvoiceResources))]
        public string ServicesTotalField => ServicesTotal?.ToString();

        [FieldSet(InvoiceNotificationFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.ExpensesTotalLabel), typeof(InvoiceResources))]
        public string ExpensesTotalField => ExpensesTotal?.ToString();

        [FieldSet(InvoiceNotificationFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.CompensationTotalLabel), typeof(InvoiceResources))]
        public string CompensationTotalField => CompensationTotal?.ToString();

        [FieldSet(InvoiceFieldSet)]
        [Tooltip(nameof(InvoiceStatusField))]
        [DisplayNameLocalized(nameof(InvoiceResources.InvoiceStatusLabel), typeof(InvoiceResources))]
        public string InvoiceStatusField => InvoiceStatus == InvoiceStatus.InvoiceRejected
           ? $"{InvoiceStatus.GetDescription()} ({InvoiceRejectionReason})"
           : InvoiceStatus.GetDescription();

        [FieldSet(InvoiceFieldSet)]
        [Tooltip(nameof(InvoiceIssuesField))]
        [DisplayNameLocalized(nameof(InvoiceResources.InvoiceIssuesLabel), typeof(InvoiceResources))]
        public string InvoiceIssuesField => InvoiceIssues.HasValue
            ? string.Join("; ", EnumHelper.GetFlags(InvoiceIssues.Value).Select(f => f.GetDescription()))
            : null;

        [FieldSet(InvoiceFieldSet)]
        [DisplayNameLocalized(nameof(InvoiceResources.InvoiceLabel), typeof(InvoiceResources))]
        [DocumentUpload(typeof(InvoiceDocumentMapping))]
        public DocumentViewModel Invoice { get; set; }

        [Visibility(VisibilityScope.None)]
        public int Year { get; set; }

        [Visibility(VisibilityScope.None)]
        public byte Month { get; set; }

        [Visibility(VisibilityScope.None)]
        public InvoiceNotificationStatus InvoiceNotificationStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public string InvoiceNotificationErrorMessage { get; set; }

        [Visibility(VisibilityScope.None)]
        public CurrencyAmount ServicesTotal { get; set; }

        [Visibility(VisibilityScope.None)]
        public CurrencyAmount ExpensesTotal { get; set; }

        [Visibility(VisibilityScope.None)]
        public CurrencyAmount CompensationTotal { get; set; }

        [Visibility(VisibilityScope.None)]
        public InvoiceStatus InvoiceStatus { get; set; }

        [Visibility(VisibilityScope.None)]
        public string InvoiceRejectionReason { get; set; }

        [Visibility(VisibilityScope.None)]
        public InvoiceIssues? InvoiceIssues { get; set; }

        [Visibility(VisibilityScope.None)]
        public Exception Exception { get; set; }
    }
}
