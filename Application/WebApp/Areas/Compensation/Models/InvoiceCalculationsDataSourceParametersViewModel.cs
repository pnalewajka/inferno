﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.WebApp.Areas.Compensation.Models
{
    [Identifier("Models.InvoiceCalculationsDataSourceParameters")]
    public class InvoiceCalculationsDataSourceParametersViewModel : IValidatableObject
    {
        public int? Year { get; set; }

        public int? Month { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Month.HasValue && (Month < 1 || Month > 12))
            {
                yield return new ValidationResult(nameof(Month));
            }
        }
    }
}
