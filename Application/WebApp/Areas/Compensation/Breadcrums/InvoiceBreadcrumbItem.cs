﻿using System.Linq;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Compensation.Controllers;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Compensation.Breadcrums
{
    public class InvoiceBreadcrumbItem : BreadcrumbItem
    {
        public InvoiceBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            var controllerName = RoutingHelper.GetControllerName(typeof(InvoiceController));
            var references = Context.SiteMap.GetPath(controllerName, nameof(InvoiceController.Invoice)).ToList();
            var items = references.Select(r => new BreadcrumbItemViewModel(r.GetUrl(UrlHelper), r.GetPlainText())).ToList();

            SetItems(items);

            var atomicController = (AtomicController)breadcrumbContextProvider.Context.Controller;

            DisplayName = atomicController.Layout.PageTitle;
        }
    }
}
