﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.Compensation
{
    public class CompensationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Compensation";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Compensation_default",
                "Compensation/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
