﻿namespace Invoice {
    export function setDate(year: number, month: number): void {
        window.location.search = UrlHelper.updateUrlParameters(window.location.search, `year=${year}&month=${month}`);
    }

    $(() => {
        $("#compensation-total-field, #services-total-field, #expenses-total-field")
            .addClass("sensitive-data")
            .append("<div class='show-sensitive fa'></div>")
            .on("click", ".show-sensitive", function(this: JQuery) {
                const $this = $(this);

                $this.parents(".sensitive-data").toggleClass("sensitive-data-show");
            });

        const element = $("[data-field-id=\"invoice\"]")[0];
        const documentPicker = Controls.getExistingInstance(DocumentUpload.DocumentPicker, element);

        documentPicker.onUploadFinished.subscribe(isSuccessful => {
            if (isSuccessful) {
                $("#upload-form").first().submit();
            }
        });
    });
}
