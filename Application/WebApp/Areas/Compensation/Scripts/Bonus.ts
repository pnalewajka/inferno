﻿namespace Bonus {
    export function confirmInvoiceChanges(): Promise<void> {

        const viewModel = getFormData();

        return new Promise<void>((resolve, reject) => {

            CommonDialogs.pleaseWait.show();

            $.post("/Compensation/Bonus/CheckAffectedInvoices", viewModel).then(modalData => {
                CommonDialogs.pleaseWait.hide();

                if (!modalData.showDialog) {
                    return resolve();
                }

                CommonDialogs.confirm(
                    modalData.content,
                    modalData.title,
                    (eventArgs) => {
                        const recalculateInvoicesElement = $("input[name='ShouldRecalculateInvoicesAfterChange']");
                        recalculateInvoicesElement.val(eventArgs.isOk ? "true" : "false");

                        return resolve();
                    },
                    undefined,
                    modalData.ok,
                    modalData.cancel
                );
            });
        });
    }

    function getFormData()
    {
        const form = $("#main-form");

        if (form.length === 0) {
            return {};
        }

        const data = form.serialize();

        return data;
    }
}
