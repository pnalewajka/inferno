﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Compensation.Consts;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Compensation.Models;
using Smt.Atomic.WebApp.Areas.Compensation.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Compensation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewBonuses)]
    [PerformanceTest(nameof(BonusController.List))]
    public class BonusController : CardIndexController<BonusViewModel, BonusDto>
    {
        private readonly IBonusService _bonusService;

        public BonusController(
            IBonusService bonusService,
            IBonusCardIndexDataService cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            _bonusService = bonusService;

            CardIndex.Settings.Title = BonusResources.Title;

            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddBonuses;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditBonuses;
            CardIndex.Settings.ViewButton.RequiredRole = SecurityRoleType.CanViewBonuses;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteBonuses;
            CardIndex.Settings.AddExportButtons(SecurityRoleType.CanExportBonus);

            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    DisplayName = BonusResources.Employee,
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new Filter{Code = FilterCodes.BonusMyEmployees, DisplayName = BonusResources.MyEmployeesFilterName},
                        new Filter{Code = FilterCodes.BonusAllEmployees, DisplayName = BonusResources.AllEmployeesFilterName}
                    }
                },
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.BonusOrgUnit, BonusResources.OrgUnitFilterName)
                    }
                }
            };

            CardIndex.ConfigureAddButtons += (CardIndexRecordViewModel<BonusViewModel> model) =>
            {
                model.Buttons.GetAddButton().SetBeforeAction("Bonus.confirmInvoiceChanges");
            };

            CardIndex.ConfigureEditButtons += (CardIndexRecordViewModel<BonusViewModel> model) =>
            {
                model.Buttons.GetEditButton().SetBeforeAction("Bonus.confirmInvoiceChanges");
            };

            Layout.Scripts.Add("~/Areas/Compensation/Scripts/Bonus.js");
        }

        [HttpPost]
        public JsonNetResult CheckAffectedInvoices(BonusViewModel bonus)
        {
            var affectedInvoices = _bonusService.GetAffectedInvoicesCount(
                bonus.EmployeeId, bonus.StartDate, bonus.EndDate);

            return new JsonNetResult(
                new
                {
                    showDialog = affectedInvoices > 0,
                    title = BonusResources.AffectedInvoicesDialogTitle,
                    content = string.Format(BonusResources.AffectedInvoicesDialogContent, affectedInvoices),
                    ok = BonusResources.AffectedInvoicesDialogOk,
                    cancel = BonusResources.AffectedInvoicesDialogCancel,
                });
        }
    }
}