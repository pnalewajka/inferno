using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Compensation.Models;
using Smt.Atomic.WebApp.Areas.Compensation.Resources;

namespace Smt.Atomic.WebApp.Areas.Compensation.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanUploadInvoice)]
    public class InvoiceController : AtomicController
    {
        private readonly ITimeService _timeService;
        private readonly ITimeReportInvoiceService _timeReportInvoiceService;
        private readonly IClassMapping<InvoiceDetailsDto, InvoiceDetailsViewModel> _invoiceDtoToViewModelMapping;

        public InvoiceController(
            IBaseControllerDependencies baseDependencies,
            ITimeService timeService,
            ITimeReportInvoiceService timeReportInvoiceService,
            IClassMapping<InvoiceDetailsDto, InvoiceDetailsViewModel> invoiceDtoToViewModelMapping)
            : base(baseDependencies)
        {
            _timeService = timeService;
            _timeReportInvoiceService = timeReportInvoiceService;
            _invoiceDtoToViewModelMapping = invoiceDtoToViewModelMapping;

            Layout.Css.Add("~/Areas/Compensation/Content/Invoice.css");
            Layout.Scripts.Add("~/Areas/Compensation/Scripts/Invoice.js");
            Layout.Parameters.AddParameter<int>(ParameterKeys.TemporaryDocumentMaxUploadChunkSize);
        }

        [HttpGet]
        [BreadcrumbBar("Invoice")]
        public ActionResult Invoice(int? year, byte? month, long? employeeId)
        {
            EnsureParametersHaveValues(ref year, ref month, ref employeeId);

            try
            {
                var dto = _timeReportInvoiceService.GetInvoiceDetails(year.Value, month.Value, employeeId.Value);
                var viewModel = _invoiceDtoToViewModelMapping.CreateFromSource(dto);

                Layout.PageTitle = string.Format(InvoiceResources.PageTitleFormat, new DateTime(year.Value, month.Value, 1));

                return View("~/Areas/Compensation/Views/Invoice/Invoice.cshtml", viewModel);

            }
            catch (Exception exception)
            {
                return Exception(year.Value, month.Value, exception);
            }
        }

        [HttpPost]
        [BreadcrumbBar("Invoice")]
        public ActionResult Invoice(UploadInvoiceViewModel viewModel)
        {
            try
            {
                var invoiceDto = new DocumentDto
                {
                    ContentType = viewModel.Invoice.ContentType,
                    DocumentId = viewModel.Invoice.DocumentId,
                    DocumentName = viewModel.Invoice.DocumentName,
                    TemporaryDocumentId = viewModel.Invoice.TemporaryDocumentId
                };

                _timeReportInvoiceService.UploadInvoice(viewModel.Year, viewModel.Month, viewModel.EmployeeId, invoiceDto);

                return Invoice(viewModel.Year, viewModel.Month, viewModel.EmployeeId);

            }
            catch (Exception exception)
            {
                return Exception(viewModel.Year, viewModel.Month, exception);
            }
        }

        private ActionResult Exception(int year, byte month, Exception exception)
        {
            Layout.PageTitle = string.Format(InvoiceResources.PageTitleFormat, new DateTime(year, month, 1));

            var viewModel = new InvoiceDetailsViewModel
            {
                Year = year,
                Month = month,
                Exception = exception
            };

            return View("~/Areas/Compensation/Views/Invoice/Invoice.cshtml", viewModel);
        }

        private void EnsureParametersHaveValues(ref int? year, ref byte? month, ref long? employeeId)
        {
            var principal = GetCurrentPrincipal();

            if (!employeeId.HasValue)
            {
                employeeId = principal.EmployeeId;
            }
            else if (employeeId.Value != principal.EmployeeId && !principal.IsInRole(SecurityRoleType.CanUploadInvoiceOnBehalf))
            {
                throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanUploadInvoiceOnBehalf.ToString() });
            }

            if (!year.HasValue || !month.HasValue)
            {
                var currentDate = _timeService.GetCurrentDate();

                year = currentDate.Year;
                month = (byte)currentDate.Month;
            }
        }
    }
}
