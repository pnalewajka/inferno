﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Areas.Finance.Controllers
{
    public abstract class TransactionContextReadOnlyCardIndexController<TViewModel, TDto, TTransactionContext> : ReadOnlyCardIndexController<TViewModel, TDto, TTransactionContext>
        where TViewModel : class, new()
        where TDto : class, new()
        where TTransactionContext : TransactionContext
    {
        private readonly ITimeService _timeService;

        protected TransactionContextReadOnlyCardIndexController(
            ICardIndexDataService<TDto> cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies,
            ITimeService timeService)
          : base(cardIndexDataService, baseControllerDependencies)
        {
            _timeService = timeService;

            CardIndex.Settings.AddExportButtons();
            CardIndex.Settings.PageSize = 100;

            Layout.Scripts.Add("~/Areas/Finance/Scripts/Transaction.js");

            SetDefaultContext();
        }

        [BreadcrumbBar("Transaction")]
        public override ActionResult List(GridParameters parameters)
        {
            CardIndex.Settings.Title = ListTitle();

            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretLeft, LayoutResources.PreviousMonth, -1));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.MapMarker, LayoutResources.CurrentMonth, null));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretRight, LayoutResources.NextMonth, 1));

            return base.List(parameters);
        }

        protected abstract string ListTitle();

        private CommandButton CreateMonthChangeButton(string icon, string text, int? monthsToAdd)
        {
            DateTime selectedMonthDate;

            if (monthsToAdd.HasValue)
            {
                if (Context.Year.HasValue && Context.Month.HasValue)
                {
                    selectedMonthDate = new DateTime(Context.Year.Value, Context.Month.Value, 1);
                }
                else
                {
                    selectedMonthDate = _timeService.GetCurrentDate();
                }

                selectedMonthDate = selectedMonthDate.AddMonths(monthsToAdd.Value);
            }
            else
            {
                selectedMonthDate = _timeService.GetCurrentDate();
            }

            return new ToolbarButton
            {
                Group = "date-navigation",
                Icon = icon,
                Text = text,
                OnClickAction = new JavaScriptCallAction($"Transaction.changeFilterDate({selectedMonthDate.Year}, {selectedMonthDate.Month})")
            };
        }

        private void SetDefaultContext()
        {
            if (!Context.IsSet())
            {
                var selectedMonthDate = _timeService.GetCurrentDate();

                Context.Year = selectedMonthDate.Year;
                Context.Month = selectedMonthDate.Month;
            }
        }
    }
}