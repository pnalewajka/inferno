﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Finance.Helpers;
using Smt.Atomic.WebApp.Areas.Finance.Models;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.WebApp.Controllers;
using Smt.Atomic.WebApp.Resources;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.Finance.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewProjectTransactions)]
    public class ProjectTransactionController : SubCardIndexController<TransactionViewModel, TransactionDto, ProjectController, ProjectTransactionContext>
    {
        private readonly ITimeService _timeService;

        public ProjectTransactionController(
            IProjectTransactionCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies,
            ITimeService timeService) 
          : base(cardIndexDataService, baseControllerDependencies)
        {
            _timeService = timeService;

            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations();
            CardIndex.Settings.PageSize = 100;
            CardIndex.Settings.Title = TransactionResources.TransactionsTitle;
            CardIndex.Settings.EmptyListMessage = TransactionResources.ThereAreNoTransactionsRelatedToTheSelectedProject;
            CardIndex.Settings.FilterGroups = GetFilters();
            CardIndex.Settings.IsContextViewCollapsed = true;
            ConfigureButtons();
            SetDefaultContext();

            Layout.Scripts.Add("~/Areas/Finance/Scripts/Transaction.js");
        }

        private void ConfigureButtons()
        {
            CardIndex.Settings.AddExportButtons();

            CardIndex.Settings.AddButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.DeleteButton.Visibility = CommandButtonVisibility.None;

            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretLeft, LayoutResources.PreviousMonth, -1));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.MapMarker, LayoutResources.CurrentMonth, null));
            CardIndex.Settings.ToolbarButtons.Add(CreateMonthChangeButton(FontAwesome.CaretRight, LayoutResources.NextMonth, 1));
        }

        public override ActionResult List(GridParameters parameters)
        {
            CardIndex.Settings.Title = string.Format(TransactionResources.PageTitleFormat, Context.Year, Context.Month);

            return base.List(parameters);
        }

        protected override string GetParentUrl(object context)
        {
            return Url.Action(
                nameof(ProjectController.List), RoutingHelper.GetControllerName<ProjectController>(), new {area = RoutingHelper.GetAreaName(typeof(ProjectController)) });
        }

        private CommandButton CreateMonthChangeButton(string icon, string text, int? monthsToAdd)
        {
            DateTime selectedMonthDate;

            if (monthsToAdd.HasValue)
            {
                if (Context.Year.HasValue && Context.Month.HasValue)
                {
                    selectedMonthDate = new DateTime(Context.Year.Value, Context.Month.Value, 1);
                }
                else
                {
                    selectedMonthDate = _timeService.GetCurrentDate();
                }

                selectedMonthDate = selectedMonthDate.AddMonths(monthsToAdd.Value);
            }
            else
            {
                selectedMonthDate = _timeService.GetCurrentDate();
            }

            return new ToolbarButton
            {
                Group = "date-navigation",
                Icon = icon,
                Text = text,
                OnClickAction = new JavaScriptCallAction($"Transaction.changeFilterDate({selectedMonthDate.Year}, {selectedMonthDate.Month})")
            };
        }

        private IList<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            return TransactionGridViewConfigurationHelper.CreateGridViewConfiguration(excludeProjectColumn: true);
        }

        private void SetDefaultContext()
        {
            if (!Context.IsSet())
            {
                var selectedMonthDate = _timeService.GetCurrentDate();

                Context.Year = selectedMonthDate.Year;
                Context.Month = selectedMonthDate.Month;
            }
        }

        protected IList<FilterGroup> GetFilters()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    Filters = new List<Filter>
                    {
                        new Filter {Code = FilterCodes.MyTransactions, DisplayName = TransactionResources.MyTransactions},
                        new Filter {Code = FilterCodes.AllTransactions, DisplayName = TransactionResources.AllTransactions}
                    }
                }
            };
        }
    }
}