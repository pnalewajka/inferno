﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Allocation.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Finance.Models;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;

namespace Smt.Atomic.WebApp.Areas.Finance.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewAggregatedProjectTransactions)]
    public class AggregatedProjectTransactionsController : TransactionContextReadOnlyCardIndexController<AggregatedProjectTransactionsViewModel, AggregatedProjectTransactionsDto, TransactionContext>
    {
        public AggregatedProjectTransactionsController(
            IAggregatedProjectTransactionsCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies,
            ITimeService timeService) 
          : base(cardIndexDataService, baseControllerDependencies, timeService)
        {
            CardIndex.Settings.Title = AggregatedProjectTransactionsResources.CardIndexTitle;
            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.FilterGroups = GetFilters();
            CardIndex.Settings.SearchAreas = new List<SearchArea>
            {
                new SearchArea(FilterCodes.ClientName, AggregatedProjectTransactionsResources.ClientShortName),
                new SearchArea(FilterCodes.ProjectName, AggregatedProjectTransactionsResources.ProjectShortName),
                new SearchArea(FilterCodes.Region, AggregatedProjectTransactionsResources.Region)
            };

            AdjustGridViewConfiguration();
        }

        protected override string ListTitle()
        {
            return string.Format(AggregatedProjectTransactionsResources.PageTitleFormat, Context.Year, Context.Month);
        }

        protected IList<FilterGroup> GetFilters()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.AllProjects, DisplayName = TransactionResources.AllProjects },
                        new Filter { Code = FilterCodes.MyProjects, DisplayName = TransactionResources.MyProjects }
                    }
                },
                new FilterGroup
                {
                    Type = FilterGroupType.OrCheckboxGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    Filters = new List<Filter>
                    {
                        new Filter { Code = FilterCodes.WithTransactions, DisplayName = TransactionResources.WithTransactions },
                        new Filter { Code = FilterCodes.WithoutTrasanctions, DisplayName = TransactionResources.WithoutTransactions }
                    }
                },
                new FilterGroup
                {
                    DisplayName = EmployeeResources.OrganizationTitle,
                    Type = FilterGroupType.OrCheckboxGroup,
                    Filters = new List<Filter>
                    {
                        new ParametricFilter<OrgUnitFilterViewModel>(FilterCodes.OrgUnits, EmployeeResources.EmployeeOrgUnitFilterName)
                    }
                }
            };
        }

        private void AdjustGridViewConfiguration()
        {
            var configuration = GridViewConfiguration<AggregatedProjectTransactionsViewModel>.DefaultGridViewConfiguration;
            configuration.IsDefault = true;

            if (GetCurrentPrincipal().IsInRole(SecurityRoleType.CanViewProjectTransactions))
            {
                configuration.ExcludeColumn(m => m.ProjectShortName);
            }
            else
            {
                configuration.ExcludeMethodColumn(nameof(AggregatedProjectTransactionsViewModel.GetProjectUrl));
            }

            CardIndex.Settings.GridViewConfigurations = new IGridViewConfiguration[]{ configuration };
        }
    }
}