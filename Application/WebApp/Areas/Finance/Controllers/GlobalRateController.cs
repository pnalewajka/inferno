﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Finance.Models;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.WebApp.Controllers;

namespace Smt.Atomic.WebApp.Areas.Finance.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewGlobalRate)]
    public class GlobalRateController : CardIndexController<GlobalRateViewModel, GlobalRateDto>
    {
        public GlobalRateController(
            IGlobalRateCardIndexDataService globalRateCardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
           : base(globalRateCardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddButton.RequiredRole = SecurityRoleType.CanAddGlobalRate;
            CardIndex.Settings.EditButton.RequiredRole = SecurityRoleType.CanEditGlobalRate;
            CardIndex.Settings.DeleteButton.RequiredRole = SecurityRoleType.CanDeleteGlobalRate;

            CardIndex.Settings.Title = GlobalRateResources.GlobalRateTitle;
        }
    }
}