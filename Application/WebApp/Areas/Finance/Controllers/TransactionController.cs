﻿using System.Collections.Generic;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.Finance.Helpers;
using Smt.Atomic.WebApp.Areas.Finance.Models;
using Smt.Atomic.WebApp.Areas.Finance.Resources;

namespace Smt.Atomic.WebApp.Areas.Finance.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewTransactions)]
    public class TransactionController : TransactionContextReadOnlyCardIndexController<TransactionViewModel, TransactionDto, TransactionContext>
    {
        public TransactionController(
            ITransactionCardIndexDataService cardIndexDataService, 
            IBaseControllerDependencies baseControllerDependencies,
            ITimeService timeService) 
          : base(cardIndexDataService, baseControllerDependencies, timeService)
        {
            CardIndex.Settings.GridViewConfigurations = CreateGridViewConfigurations();
            CardIndex.Settings.Title = TransactionResources.TransactionsTitle;
            CardIndex.Settings.FilterGroups = GetFilters();
        }

        protected override string ListTitle()
        {
            return string.Format(TransactionResources.PageTitleFormat, Context.Year, Context.Month);
        }      

        private IList<IGridViewConfiguration> CreateGridViewConfigurations()
        {
            return TransactionGridViewConfigurationHelper.CreateGridViewConfiguration();
        }

        protected IList<FilterGroup> GetFilters()
        {
            return new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Visibility = CommandButtonVisibility.Grid,
                    Filters = new List<Filter>
                    {
                        new Filter {Code = FilterCodes.AllTransactions, DisplayName = TransactionResources.AllTransactions},
                        new Filter {Code = FilterCodes.MyTransactions, DisplayName = TransactionResources.MyTransactions},                        
                    }
                }
            };
        }
    }
}