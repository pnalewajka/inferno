﻿using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.WebApp.Breadcrumbs;

namespace Smt.Atomic.WebApp.Areas.Finance.Breadcrumbs
{
    public class ProjectTransactionBreadcrumbItem : BreadcrumbItem
    {
        public ProjectTransactionBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider) : base(breadcrumbContextProvider)
        {
            DisplayName = TransactionResources.TransactionsTitle;
        }
    }
}