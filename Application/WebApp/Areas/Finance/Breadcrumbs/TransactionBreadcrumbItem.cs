﻿using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Breadcrumbs.Items;

namespace Smt.Atomic.WebApp.Areas.Finance.Breadcrumbs
{
    public class TransactionBreadcrumbItem : CardIndexBreadcrumbItem
    {
        public TransactionBreadcrumbItem(IBreadcrumbContextProvider breadcrumbContextProvider, IBreadcrumbService breadcrumbService) : base(breadcrumbContextProvider, breadcrumbService)
        {
            DisplayName = ((AtomicController)breadcrumbContextProvider.Context.Controller).Layout.PageTitle;
        }
    }
}