﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.WebApp.Areas.Finance.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class TransactionResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TransactionResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.WebApp.Areas.Finance.Resources.TransactionResources", typeof(TransactionResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account.
        /// </summary>
        internal static string Account {
            get {
                return ResourceManager.GetString("Account", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account type.
        /// </summary>
        internal static string AccountType {
            get {
                return ResourceManager.GetString("AccountType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Projects.
        /// </summary>
        internal static string AllProjects {
            get {
                return ResourceManager.GetString("AllProjects", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All transactions.
        /// </summary>
        internal static string AllTransactions {
            get {
                return ResourceManager.GetString("AllTransactions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Amount (EUR).
        /// </summary>
        internal static string AmountEUR {
            get {
                return ResourceManager.GetString("AmountEUR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Amount (PLN).
        /// </summary>
        internal static string AmountPLN {
            get {
                return ResourceManager.GetString("AmountPLN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Budget position.
        /// </summary>
        internal static string BudgetPosition {
            get {
                return ResourceManager.GetString("BudgetPosition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        internal static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        internal static string CompanyTabName {
            get {
                return ResourceManager.GetString("CompanyTabName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cost center.
        /// </summary>
        internal static string CostCenter {
            get {
                return ResourceManager.GetString("CostCenter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created by.
        /// </summary>
        internal static string CreatedBy {
            get {
                return ResourceManager.GetString("CreatedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        internal static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Doc. date.
        /// </summary>
        internal static string DocumentDateGridName {
            get {
                return ResourceManager.GetString("DocumentDateGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document date.
        /// </summary>
        internal static string DocumentDateLabelName {
            get {
                return ResourceManager.GetString("DocumentDateLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Doc. no..
        /// </summary>
        internal static string DocumentNoGridName {
            get {
                return ResourceManager.GetString("DocumentNoGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number.
        /// </summary>
        internal static string DocumentNoLabelName {
            get {
                return ResourceManager.GetString("DocumentNoLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document.
        /// </summary>
        internal static string DocumentTabName {
            get {
                return ResourceManager.GetString("DocumentTabName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Doc. type.
        /// </summary>
        internal static string DocumentTypeGridName {
            get {
                return ResourceManager.GetString("DocumentTypeGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        internal static string DocumentTypeLabelName {
            get {
                return ResourceManager.GetString("DocumentTypeLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entry no..
        /// </summary>
        internal static string EntryNo {
            get {
                return ResourceManager.GetString("EntryNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to External document no..
        /// </summary>
        internal static string ExternalDocumentNoGridName {
            get {
                return ResourceManager.GetString("ExternalDocumentNoGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to External document no..
        /// </summary>
        internal static string ExternalDocumentNoLabelName {
            get {
                return ResourceManager.GetString("ExternalDocumentNoLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General ledger account no..
        /// </summary>
        internal static string GeneralLedgerAccountNo {
            get {
                return ResourceManager.GetString("GeneralLedgerAccountNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General manager.
        /// </summary>
        internal static string GeneralManager {
            get {
                return ResourceManager.GetString("GeneralManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ifrs.
        /// </summary>
        internal static string Ifrs {
            get {
                return ResourceManager.GetString("Ifrs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location.
        /// </summary>
        internal static string Location {
            get {
                return ResourceManager.GetString("Location", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Projects.
        /// </summary>
        internal static string MyProjects {
            get {
                return ResourceManager.GetString("MyProjects", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My transactions.
        /// </summary>
        internal static string MyTransactions {
            get {
                return ResourceManager.GetString("MyTransactions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No tax.
        /// </summary>
        internal static string NoTax {
            get {
                return ResourceManager.GetString("NoTax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operational director.
        /// </summary>
        internal static string OperationalDirector {
            get {
                return ResourceManager.GetString("OperationalDirector", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operational manager.
        /// </summary>
        internal static string OperationalManager {
            get {
                return ResourceManager.GetString("OperationalManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Org. unit.
        /// </summary>
        internal static string OrgUnitGridName {
            get {
                return ResourceManager.GetString("OrgUnitGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Organizational unit.
        /// </summary>
        internal static string OrgUnitLabelName {
            get {
                return ResourceManager.GetString("OrgUnitLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other details.
        /// </summary>
        internal static string OtherTabName {
            get {
                return ResourceManager.GetString("OtherTabName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transactions for {0}-{1:D2}.
        /// </summary>
        internal static string PageTitleFormat {
            get {
                return ResourceManager.GetString("PageTitleFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posting date.
        /// </summary>
        internal static string PostingDateGridName {
            get {
                return ResourceManager.GetString("PostingDateGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posting date.
        /// </summary>
        internal static string PostingDateLabelName {
            get {
                return ResourceManager.GetString("PostingDateLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project.
        /// </summary>
        internal static string Project {
            get {
                return ResourceManager.GetString("Project", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project group.
        /// </summary>
        internal static string ProjectGroup {
            get {
                return ResourceManager.GetString("ProjectGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project manager.
        /// </summary>
        internal static string ProjectManager {
            get {
                return ResourceManager.GetString("ProjectManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project type.
        /// </summary>
        internal static string ProjectType {
            get {
                return ResourceManager.GetString("ProjectType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project utilization.
        /// </summary>
        internal static string ProjectUtilization {
            get {
                return ResourceManager.GetString("ProjectUtilization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Region.
        /// </summary>
        internal static string Region {
            get {
                return ResourceManager.GetString("Region", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales person.
        /// </summary>
        internal static string SalesPerson {
            get {
                return ResourceManager.GetString("SalesPerson", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Src. code.
        /// </summary>
        internal static string SourceCodeGridName {
            get {
                return ResourceManager.GetString("SourceCodeGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code.
        /// </summary>
        internal static string SourceCodeLabelName {
            get {
                return ResourceManager.GetString("SourceCodeLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Src. company name.
        /// </summary>
        internal static string SourceCompanyNameGridName {
            get {
                return ResourceManager.GetString("SourceCompanyNameGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company name.
        /// </summary>
        internal static string SourceCompanyNameLabelName {
            get {
                return ResourceManager.GetString("SourceCompanyNameLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Src. name.
        /// </summary>
        internal static string SourceNameGridName {
            get {
                return ResourceManager.GetString("SourceNameGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        internal static string SourceNameLabelName {
            get {
                return ResourceManager.GetString("SourceNameLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Src. no..
        /// </summary>
        internal static string SourceNoGridName {
            get {
                return ResourceManager.GetString("SourceNoGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number.
        /// </summary>
        internal static string SourceNoLabelName {
            get {
                return ResourceManager.GetString("SourceNoLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source.
        /// </summary>
        internal static string SourceTabName {
            get {
                return ResourceManager.GetString("SourceTabName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Src. type.
        /// </summary>
        internal static string SourceTypeGridName {
            get {
                return ResourceManager.GetString("SourceTypeGridName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        internal static string SourceTypeLabelName {
            get {
                return ResourceManager.GetString("SourceTypeLabelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are no transactions related to the selected project.
        /// </summary>
        internal static string ThereAreNoTransactionsRelatedToTheSelectedProject {
            get {
                return ResourceManager.GetString("ThereAreNoTransactionsRelatedToTheSelectedProject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transactions.
        /// </summary>
        internal static string TransactionsTitle {
            get {
                return ResourceManager.GetString("TransactionsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vertical.
        /// </summary>
        internal static string Vertical {
            get {
                return ResourceManager.GetString("Vertical", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Without transactions.
        /// </summary>
        internal static string WithoutTransactions {
            get {
                return ResourceManager.GetString("WithoutTransactions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to With transactions.
        /// </summary>
        internal static string WithTransactions {
            get {
                return ResourceManager.GetString("WithTransactions", resourceCulture);
            }
        }
    }
}
