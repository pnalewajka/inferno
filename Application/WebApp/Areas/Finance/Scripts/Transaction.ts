﻿module Transaction {
   
    export function changeFilterDate(year: number, month: number) {
        let urlSearch = window.location.search;
        urlSearch = UrlHelper.updateUrlParameter(urlSearch, "year", year.toString());
        urlSearch = UrlHelper.updateUrlParameter(urlSearch, "month", month.toString());
        window.location.search = urlSearch;
    }
}