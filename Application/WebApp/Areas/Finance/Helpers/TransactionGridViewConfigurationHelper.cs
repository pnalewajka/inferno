﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Areas.Finance.Models;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Areas.Finance.Helpers
{
    public static class TransactionGridViewConfigurationHelper
    {
        public static IList<IGridViewConfiguration> CreateGridViewConfiguration(bool excludeProjectColumn = false)
        {
            var minimalView =
                new GridViewConfiguration<TransactionViewModel>(LayoutResources.MinimalViewText, "minimal");

            minimalView.IncludeColumn(r => r.PostingDate);
            minimalView.IncludeColumn(r => r.DocumentType);
            minimalView.IncludeColumn(r => r.DocumentNo);
            minimalView.IncludeColumn(r => r.Description);
            minimalView.IncludeColumn(r => r.SourceName);
            minimalView.IncludeColumn(r => r.ProjectId);
            minimalView.IncludeMethodColumn(nameof(TransactionViewModel.GetAmountEur));
            minimalView.IncludeMethodColumn(nameof(TransactionViewModel.GetOrgUnit));
            minimalView.IncludeMethodColumn(nameof(TransactionViewModel.GetCompany));
            minimalView.IncludeMethodColumn(nameof(TransactionViewModel.GetLocation));

            minimalView.IsDefault = true;

            var extendedView =
                new GridViewConfiguration<TransactionViewModel>(LayoutResources.ExtendedViewText, "extended");

            extendedView.IncludeColumn(r => r.PostingDate);
            extendedView.IncludeColumn(r => r.DocumentType);
            extendedView.IncludeColumn(r => r.DocumentNo);
            extendedView.IncludeColumn(r => r.Description);
            extendedView.IncludeColumn(r => r.SourceName);
            extendedView.IncludeColumn(r => r.SourceNo);
            extendedView.IncludeColumn(r => r.SourceCode);
            extendedView.IncludeColumn(r => r.SourceType);
            extendedView.IncludeColumn(r => r.ProjectId);
            extendedView.IncludeMethodColumn(nameof(TransactionViewModel.GetAmountEur));
            extendedView.IncludeMethodColumn(nameof(TransactionViewModel.GetAmountPln));            
            extendedView.IncludeMethodColumn(nameof(TransactionViewModel.GetOrgUnit));
            extendedView.IncludeMethodColumn(nameof(TransactionViewModel.GetCompany));
            extendedView.IncludeMethodColumn(nameof(TransactionViewModel.GetLocation));

            var fullView =
                new GridViewConfiguration<TransactionViewModel>(LayoutResources.FullViewText, "full");

            fullView.IncludeAllColumns();
            fullView.IncludeAllMethodColumns();

            if (excludeProjectColumn)
            {
                minimalView.ExcludeColumn(r => r.ProjectId);
                extendedView.ExcludeColumn(r => r.ProjectId);
                fullView.ExcludeColumn(r => r.ProjectId);
            }

            return new IGridViewConfiguration[]
            {
                minimalView,
                extendedView,
                fullView
            };
        }
    }
}