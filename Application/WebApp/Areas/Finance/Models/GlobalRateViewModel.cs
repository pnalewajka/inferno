﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Resources;
using Smt.Atomic.WebApp.Areas.Dictionaries.Controllers;
using Smt.Atomic.WebApp.Areas.Dictionaries.Resources;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.WebApp.Areas.SkillManagement.Controllers;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class GlobalRateViewModel
    {
        public long Id { get; set; }

        [Order(0)]
        [Required]
        [StringLength(200)]
        [DisplayNameLocalized(nameof(DictionaryResources.NameLabel), typeof(DictionaryResources))]
        public string Name { get; set; }

        [Order(1)]
        [Required]
        [DisplayNameLocalized(nameof(GlobalRateResources.ValueLabel), typeof(GlobalRateResources))]
        public decimal Value { get; set; }

        [Order(2)]
        [Required]
        [DisplayNameLocalized(nameof(GlobalRateResources.CurrencyLabel), typeof(GlobalRateResources))]
        [ValuePicker(Type = typeof(CurrencyPickerController))]
        public long CurrencyId { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(DictionaryResources.DescriptionLabel), typeof(DictionaryResources))]
        public string Description { get; set; }

        [Order(4)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeJobMatrixLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(JobMatrixLevelPickerController))]
        public long? JobMatrixLevelId { get; set; }

        [Order(5)]
        [DisplayNameLocalized(nameof(EmployeeResources.EmployeeLocationLabel), typeof(EmployeeResources))]
        [ValuePicker(Type = typeof(EmployeeLocationPickerController))]
        public long? LocationId { get; set; }

        [Order(6)]
        [DisplayNameLocalized(nameof(DictionaryResources.CompanyLabel), typeof(DictionaryResources))]
        [ValuePicker(Type = typeof(CompanyPickerController))]
        public long? CompanyId { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
