﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Areas.Finance.Controllers;
using Smt.Atomic.WebApp.Areas.Finance.Resources;
using Smt.Atomic.WebApp.Areas.Organization.Controllers;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class AggregatedProjectTransactionsViewModel
    {
        public long Id { get; set; }

        [Order(1)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ClientShortName), typeof(AggregatedProjectTransactionsResources))]
        [Visibility(VisibilityScope.Grid)]
        public string ClientShortName { get; set; }

        [Order(2)]
        [Visibility(VisibilityScope.Grid)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ProjectShortName), typeof(AggregatedProjectTransactionsResources))]
        public string ProjectShortName { get; set; }

        [Order(2)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ProjectShortName), typeof(AggregatedProjectTransactionsResources))]
        [SortBy(nameof(ProjectShortName))]
        public DisplayUrl GetProjectUrl(UrlHelper helper)
        {
            return new DisplayUrl
            {
                Label = ProjectShortName,
                Action = GetUriActionToListOfProjectTransaction(helper)
            };
        }

        [Visibility(VisibilityScope.None)]
        public long ProjectId { get; set; }

        [Order(3)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.OrgUnit), typeof(AggregatedProjectTransactionsResources))]
        [ValuePicker(Type = typeof(OrgUnitPickerController))]
        public long OrgUnitId { get; set; }

        [Visibility(VisibilityScope.None)]        
        public decimal? Revenue { get; set; }

        [Order(4)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.Region), typeof(AggregatedProjectTransactionsResources))]
        public string Region { get; set; }

        [Order(5)]
        [SortBy(nameof(Revenue))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.Revenue), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetRevenue(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(Revenue);
        }

        [Order(6)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.RevenuePerHours), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetRevenuePerHours(UrlHelper helper)
        {
            if (Hours == 0)
            {
                return GetValueOrNoDataTooltipText(null);
            }

            return GetValueOrNoDataTooltipText(Revenue / Hours);
        }
        
        [Visibility(VisibilityScope.None)]
        public decimal? EstimatedRevenue { get; set; }

        [Order(7)]
        [SortBy(nameof(EstimatedRevenue))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.EstimatedRevenue),typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetEstimatedRevenue(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(EstimatedRevenue);
        }

        [Order(7)]
        [Visibility(VisibilityScope.FormAndGrid)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.Hours), typeof(AggregatedProjectTransactionsResources))]
        public decimal Hours { get; set; }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ProjectCost), typeof(AggregatedProjectTransactionsResources))]
        public decimal? ProjectCost { get; set; }

        [Order(8)]
        [SortBy(nameof(ProjectCost))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ProjectCost), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetProjectCost(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(ProjectCost);
        }

        [Visibility(VisibilityScope.None)]
        public decimal? ProjectMargin { get; set; }

        [Order(9)]
        [SortBy(nameof(ProjectMargin))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ProjectMargin), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetProjectMargin(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(ProjectMargin);
        }

        [Visibility(VisibilityScope.None)]
        public decimal? ProjectMarginPercent { get; set; }

        [Order(10)]
        [SortBy(nameof(ProjectMarginPercent))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ProjectMarginPercent), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetProjectMarginPercent(UrlHelper helper)
        {
            return GetPercentValueOrNoDataTooltipText(ProjectMarginPercent);
        }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.OperationCost), typeof(AggregatedProjectTransactionsResources))]
        public decimal? OperationCost { get; set; }

        [Order(11)]
        [SortBy(nameof(OperationCost))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.OperationCost), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetOperationCost(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(OperationCost);
        }

        [Visibility(VisibilityScope.None)]
        public decimal? OperationMargin { get; set; }

        [Order(12)]
        [SortBy(nameof(OperationMargin))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.OperationMargin), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetOperationMargin(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(OperationMargin);
        }

        [Visibility(VisibilityScope.None)]
        public decimal? OperationMarginPercent { get; set; }

        [Order(13)]
        [SortBy(nameof(OperationMarginPercent))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.OperationMarginPercent), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetOperationMarginPercent(UrlHelper helper)
        {
            return GetPercentValueOrNoDataTooltipText(OperationMarginPercent);
        }

        [Visibility(VisibilityScope.None)]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ServiceCost), typeof(AggregatedProjectTransactionsResources))]
        public decimal? ServiceCost { get; set; }

        [Order(14)]
        [SortBy(nameof(ServiceCost))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ServiceCost), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetServiceCost(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(ServiceCost);
        }

        [Visibility(VisibilityScope.None)]
        public decimal? ServiceMargin { get; set; }

        [Order(15)]
        [SortBy(nameof(ServiceMargin))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ServiceMargin), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetServiceMargin(UrlHelper helper)
        {
            return GetValueOrNoDataTooltipText(ServiceMargin);
        }

        [Visibility(VisibilityScope.None)]
        public decimal? ServiceMarginPercent { get; set; }

        [Order(16)]
        [SortBy(nameof(ServiceMarginPercent))]
        [DisplayNameLocalized(nameof(AggregatedProjectTransactionsResources.ServiceMarginPercent), typeof(AggregatedProjectTransactionsResources))]
        public TooltipText GetServiceMarginPercent(UrlHelper helper)
        {
            return GetPercentValueOrNoDataTooltipText(ServiceMarginPercent);
        }

        private TooltipText GetValueOrNoDataTooltipText(decimal? value)
        {
            return new TooltipText
            {
                Tooltip = $"{value:n} EUR",
                Label = value?.ToString("n") ?? NoData,
                CssClass = "numeric-column",
                ExportValue = value?.ToInvariantString("F") ?? ""
            };
        }

        private TooltipText GetPercentValueOrNoDataTooltipText(decimal? value)
        {
            return new TooltipText
            {
                Label = value?.ToString("P").Replace(" ","") ?? NoData,
                CssClass = "numeric-column",
                ExportValue = value.HasValue ? (value * 100).ToInvariantString("F") : ""
            };
        }

        private UriAction GetUriActionToListOfProjectTransaction(UrlHelper helper)
        {
            var parameters = new List<string>();

            parameters.AddRange(new[] { RoutingHelper.ParentIdParameterName, ProjectId.ToString() });

            var context =
                UrlFieldsHelper.CreateObjectFromQueryParameters<TransactionContext>(
                    System.Web.HttpContext.Current.Request.QueryString.ToDictionary(), true);

            if (context.IsSet())
            {
                parameters.AddRange(new[] { nameof(context.Year).ToLower(), context.Year.ToString() });
                parameters.AddRange(new[] { nameof(context.Month).ToLower(), context.Month.ToString() });
            }

            return
                helper.UriActionGet(
                    nameof(ProjectTransactionController.List), RoutingHelper.GetControllerName<ProjectTransactionController>(), KnownParameter.None, parameters.ToArray());
        }

        private static string NoData => "-";
    }
}