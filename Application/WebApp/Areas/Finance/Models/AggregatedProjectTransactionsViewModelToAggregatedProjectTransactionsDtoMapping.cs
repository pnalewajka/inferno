﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class AggregatedProjectTransactionsViewModelToAggregatedProjectTransactionsDtoMapping : ClassMapping<AggregatedProjectTransactionsViewModel, AggregatedProjectTransactionsDto>
    {
        public AggregatedProjectTransactionsViewModelToAggregatedProjectTransactionsDtoMapping()
        {
            Mapping = v => new AggregatedProjectTransactionsDto
            {
                Id = v.Id,
                ProjectId = v.ProjectId,
                OrgUnitId = v.OrgUnitId,
                ProjectShortName = v.ProjectShortName,
                ClientShortName = v.ClientShortName,
                Revenue = v.Revenue,
                ProjectCost = v.ProjectCost,
                ProjectMargin = v.ProjectMargin,
                ProjectMarginPercent = v.ProjectMarginPercent,
                OperationCost = v.OperationCost,
                OperationMargin = v.OperationMargin,
                OperationMarginPercent = v.OperationMarginPercent,
                ServiceCost = v.ServiceCost,
                ServiceMargin = v.ServiceMargin,
                ServiceMarginPercent = v.ServiceMarginPercent,
                Region = v.Region,
                Hours = v.Hours
            };
        }
    }
}
