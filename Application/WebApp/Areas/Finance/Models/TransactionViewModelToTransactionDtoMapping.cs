﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class TransactionViewModelToTransactionDtoMapping : ClassMapping<TransactionViewModel, TransactionDto>
    {
        public TransactionViewModelToTransactionDtoMapping()
        {
            Mapping = v => new TransactionDto
            {
                Id = v.Id,
                Description = v.Description,
                AmountPln = v.AmountPln,
                PostingDate = v.PostingDate,
                AmountEur = v.AmountEur,
                DocumentDate = v.DocumentDate,
                DocumentNo = v.DocumentNo,
                DocumentType = v.DocumentType,
                EntryNo = v.EntryNo,
                ExternalDocumentNo = v.ExternalDocumentNo,
                GeneralLedgerAccountNo = v.GeneralLedgerAccountNo,
                SourceNo = v.SourceNo,
                SourceName = v.SourceName,
                SourceCode = v.SourceCode,
                SourceType = v.SourceType,
                SourceCompanyName = v.SourceCompanyName,
                CreatedByUserId = v.CreatedByUserId,
                BudgetPositionCode = v.BudgetPositionCode,
                BudgetPositionName = v.BudgetPositionName,
                CompanyCode = v.CompanyCode,
                CompanyName = v.CompanyName,
                Ifrs = v.Ifrs,
                LocationCode = v.LocationCode,
                LocationName = v.LocationName,
                NoTax = v.NoTax,
                OrgUnitCode = v.OrgUnitCode,
                OrgUnitName = v.OrgUnitName,
                ProjectId = v.ProjectId,
                Account = v.Account,
                CostCenterCode = v.CostCenterCode,
                CostCenterName = v.CostCenterName,
                ProjectManagerUserId = v.ProjectManagerUserId,
                ProjectGroup = v.ProjectGroup,
                ProjectType = v.ProjectType,
                ProjectUtilization = v.ProjectUtilization,
                AccountType = v.AccountType,
                OperationalManagerUserId = v.OperationalManagerUserId,
                Region = v.Region,
                SalesPersonUserId = v.SalesPersonUserId,
                Vertical = v.Vertical,
                GeneralManagerUserId = v.GeneralManagerUserId,
                OperationalDirectorUserId = v.OperationalDirectorUserId
            };
        }
    }
}
