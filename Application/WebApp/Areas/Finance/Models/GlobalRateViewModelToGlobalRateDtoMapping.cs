﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class GlobalRateViewModelToGlobalRateDtoMapping : ClassMapping<GlobalRateViewModel, GlobalRateDto>
    {
        public GlobalRateViewModelToGlobalRateDtoMapping()
        {
            Mapping = v => new GlobalRateDto
            {
                Id = v.Id,
                Name = v.Name,
                Value = v.Value,
                CurrencyId = v.CurrencyId,
                Description = v.Description,
                JobMatrixLevelId = v.JobMatrixLevelId,
                LocationId = v.LocationId,
                CompanyId = v.CompanyId
            };
        }
    }
}
