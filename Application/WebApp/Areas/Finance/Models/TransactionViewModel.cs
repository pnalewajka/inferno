﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.Accounts.Controllers;
using Smt.Atomic.WebApp.Areas.Allocation.Controllers;
using Smt.Atomic.WebApp.Areas.Finance.Resources;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    [TabDescription(0, "document", "DocumentTabName", typeof(TransactionResources))]
    [TabDescription(1, "source", "SourceTabName", typeof(TransactionResources))]
    [TabDescription(2, "company", "CompanyTabName", typeof(TransactionResources))]
    [TabDescription(3, "other", "OtherTabName", typeof(TransactionResources))]
    public class TransactionViewModel
    {
        public long Id { get; set; }

        [Tab("document")]
        [Order(1)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(TransactionResources.DocumentDateLabelName), nameof(TransactionResources.DocumentDateGridName), typeof(TransactionResources))]
        public DateTime? DocumentDate { get; set; }

        [Tab("document")]
        [Order(2)]
        [StringFormat("{0:d}")]
        [DataType(DataType.Date)]
        [Render(GridCssClass = "date-column")]
        [DisplayNameLocalized(nameof(TransactionResources.PostingDateLabelName), nameof(TransactionResources.PostingDateGridName), typeof(TransactionResources))]
        public DateTime? PostingDate { get; set; }

        [Tab("document")]
        [Order(3)]
        [DisplayNameLocalized(nameof(TransactionResources.DocumentTypeLabelName), nameof(TransactionResources.DocumentTypeGridName), typeof(TransactionResources))]
        public string DocumentType { get; set; }

        [Tab("document")]
        [Order(4)]
        [DisplayNameLocalized(nameof(TransactionResources.DocumentNoLabelName), nameof(TransactionResources.DocumentNoGridName), typeof(TransactionResources))]
        public string DocumentNo { get; set; }

        [Tab("document")]
        [Order(5)]
        [DisplayNameLocalized(nameof(TransactionResources.Description), typeof(TransactionResources))]
        [Render(GridCssClass = "up-to-30-percent", Size = Size.Large)]
        public string Description { get; set; }

        [Visibility(VisibilityScope.None)]
        public decimal? AmountEur { get; set; }

        [Tab("document")]
        [Order(6)]
        [DisplayNameLocalized(nameof(TransactionResources.AmountEUR), typeof(TransactionResources))]
        [Visibility(VisibilityScope.Form)]
        public string AmountEurForm => $"{AmountEur:n} EUR";

        [Order(6)]
        [DisplayNameLocalized(nameof(TransactionResources.AmountEUR), typeof(TransactionResources))]
        public TooltipText GetAmountEur(UrlHelper helper)
        {
            return new TooltipText
            {
                CssClass = "numeric-column",
                Label = $"{AmountEur:n}",
                Tooltip = $"{AmountEur:n} EUR",
                ExportValue = AmountEur?.ToInvariantString("F") ?? ""
            };
        }

        [Visibility(VisibilityScope.None)]
        public decimal? AmountPln { get; set; }

        [Tab("document")]
        [Order(7)]
        [DisplayNameLocalized(nameof(TransactionResources.AmountPLN), typeof(TransactionResources))]
        [Visibility(VisibilityScope.Form)]
        public string AmountPlnForm => $"{AmountPln:n} PLN";

        [Order(7)]
        [DisplayNameLocalized(nameof(TransactionResources.AmountPLN), typeof(TransactionResources))]
        public TooltipText GetAmountPln(UrlHelper helper)
        {
            return new TooltipText
            {
                CssClass = "numeric-column",
                Label = $"{AmountPln:n}",
                Tooltip = $"{AmountPln:n} PLN",
                ExportValue = AmountPln?.ToInvariantString() ?? ""
            };
        }

        [Tab("document")]
        [Order(8)]
        [DisplayNameLocalized(nameof(TransactionResources.EntryNo), typeof(TransactionResources))]
        public int EntryNo { get; set; }

        [Tab("document")]
        [Order(9)]
        [DisplayNameLocalized(nameof(TransactionResources.ExternalDocumentNoLabelName), nameof(TransactionResources.ExternalDocumentNoGridName), typeof(TransactionResources))]
        public string ExternalDocumentNo { get; set; }


        [Tab("source")]
        [Order(10)]
        [DisplayNameLocalized(nameof(TransactionResources.SourceNameLabelName), nameof(TransactionResources.SourceNameGridName), typeof(TransactionResources))]
        public string SourceName { get; set; }

        [Tab("source")]
        [Order(11)]
        [DisplayNameLocalized(nameof(TransactionResources.SourceNoLabelName), nameof(TransactionResources.SourceNoGridName), typeof(TransactionResources))]
        public string SourceNo { get; set; }

        [Tab("source")]
        [Order(12)]
        [DisplayNameLocalized(nameof(TransactionResources.SourceCodeLabelName), nameof(TransactionResources.SourceCodeGridName), typeof(TransactionResources))]
        public string SourceCode { get; set; }

        [Tab("source")]
        [Order(13)]
        [DisplayNameLocalized(nameof(TransactionResources.SourceTypeLabelName), nameof(TransactionResources.SourceTypeGridName), typeof(TransactionResources))]
        public string SourceType { get; set; }

        [Tab("source")]
        [Order(14)]
        [DisplayNameLocalized(nameof(TransactionResources.SourceCompanyNameLabelName), nameof(TransactionResources.SourceCompanyNameGridName), typeof(TransactionResources))]
        public string SourceCompanyName { get; set; }


        [Tab("company")]
        [Order(15)]
        [DisplayNameLocalized(nameof(TransactionResources.Project), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(ProjectPickerController))]
        public long? ProjectId { get; set; }

        [Tab("company")]
        [Order(16)]
        [DisplayNameLocalized(nameof(TransactionResources.ProjectManager), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? ProjectManagerUserId { get; set; }

        [Tab("company")]
        [Order(17)]
        [DisplayNameLocalized(nameof(TransactionResources.ProjectGroup), typeof(TransactionResources))]
        public string ProjectGroup { get; set; }

        [Tab("company")]
        [Order(18)]
        [DisplayNameLocalized(nameof(TransactionResources.ProjectType), typeof(TransactionResources))]
        public string ProjectType { get; set; }

        [Tab("company")]
        [Order(19)]
        [DisplayNameLocalized(nameof(TransactionResources.ProjectUtilization), typeof(TransactionResources))]
        public string ProjectUtilization { get; set; }

        [Visibility(VisibilityScope.None)]
        public string OrgUnitCode { get; set; }

        [Tab("company")]
        [Order(20)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TransactionResources.OrgUnitLabelName), typeof(TransactionResources))]
        public string OrgUnitName { get; set; }

        [Order(20)]
        [SortBy(nameof(OrgUnitCode))]
        [DisplayNameLocalized(nameof(TransactionResources.OrgUnitGridName), typeof(TransactionResources))]
        public TooltipText GetOrgUnit(UrlHelper urlHelper)
        {
            return new TooltipText
            {
                Tooltip = OrgUnitName,
                Label = OrgUnitCode
            };
        }

        [Visibility(VisibilityScope.None)]
        public string CompanyCode { get; set; }

        [Tab("company")]
        [Order(21)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TransactionResources.Company), typeof(TransactionResources))]
        public string CompanyName { get; set; }

        [Order(21)]
        [SortBy(nameof(CompanyCode))]
        [DisplayNameLocalized(nameof(TransactionResources.Company), typeof(TransactionResources))]
        public TooltipText GetCompany(UrlHelper urlHelper)
        {
            return new TooltipText
            {
                Tooltip = CompanyName,
                Label = CompanyCode
            };
        }

        [Visibility(VisibilityScope.None)]
        public string LocationCode { get; set; }

        [Tab("company")]
        [Order(22)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TransactionResources.Location), typeof(TransactionResources))]
        public string LocationName { get; set; }

        [Order(22)]
        [SortBy(nameof(LocationCode))]
        [DisplayNameLocalized(nameof(TransactionResources.Location), typeof(TransactionResources))]
        public TooltipText GetLocation(UrlHelper urlHelper)
        {
            return new TooltipText
            {
                Tooltip = LocationName,
                Label = LocationCode
            };
        }

        [Tab("company")]
        [Order(23)]
        [DisplayNameLocalized(nameof(TransactionResources.CreatedBy), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? CreatedByUserId { get; set; }
        
        [Visibility(VisibilityScope.None)]
        public string BudgetPositionCode { get; set; }

        [Tab("company")]
        [Order(24)]
        [DisplayNameLocalized(nameof(TransactionResources.OperationalManager), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? OperationalManagerUserId { get; set; }

        [Tab("company")]
        [Order(25)]
        [DisplayNameLocalized(nameof(TransactionResources.SalesPerson), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? SalesPersonUserId { get; set; }

        [Tab("company")]
        [Order(26)]
        [DisplayNameLocalized(nameof(TransactionResources.GeneralManager), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? GeneralManagerUserId { get; set; }

        [Tab("company")]
        [Order(27)]
        [DisplayNameLocalized(nameof(TransactionResources.OperationalDirector), typeof(TransactionResources))]
        [ValuePicker(Type = typeof(UserPickerController))]
        public long? OperationalDirectorUserId { get; set; }


        [Tab("other")]
        [Order(28)]
        [DisplayNameLocalized(nameof(TransactionResources.Region), typeof(TransactionResources))]
        public string Region { get; set; }

        [Tab("other")]
        [Order(29)]
        [DisplayNameLocalized(nameof(TransactionResources.Account), typeof(TransactionResources))]
        public string Account { get; set; }

        [Tab("other")]
        [Order(30)]
        [DisplayNameLocalized(nameof(TransactionResources.AccountType), typeof(TransactionResources))]
        public string AccountType { get; set; }

        [Tab("other")]
        [Order(31)]
        [DisplayNameLocalized(nameof(TransactionResources.GeneralLedgerAccountNo), typeof(TransactionResources))]
        public string GeneralLedgerAccountNo { get; set; }

        [Tab("other")]
        [Order(32)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TransactionResources.BudgetPosition), typeof(TransactionResources))]
        public string BudgetPositionName { get; set; }

        [Order(32)]
        [SortBy(nameof(BudgetPositionCode))]
        [DisplayNameLocalized(nameof(TransactionResources.BudgetPosition), typeof(TransactionResources))]
        public TooltipText GetBudgetPosition(UrlHelper urlHelper)
        {
            return new TooltipText
            {
                Tooltip = BudgetPositionName,
                Label = BudgetPositionCode
            };
        }

        [Visibility(VisibilityScope.None)]
        public string CostCenterCode { get; set; }

        [Tab("other")]
        [Order(33)]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(TransactionResources.CostCenter), typeof(TransactionResources))]
        public string CostCenterName { get; set; }

        [Order(33)]
        [SortBy(nameof(CostCenterCode))]
        [DisplayNameLocalized(nameof(TransactionResources.CostCenter), typeof(TransactionResources))]
        public TooltipText GetCostCenter(UrlHelper urlHelper)
        {
            return new TooltipText
            {
                Tooltip = CostCenterName,
                Label = CostCenterCode
            };
        }

        [Tab("other")]
        [Order(34)]
        [DisplayNameLocalized(nameof(TransactionResources.NoTax), typeof(TransactionResources))]
        public bool NoTax { get; set; }

        [Tab("other")]
        [Order(35)]
        [DisplayNameLocalized(nameof(TransactionResources.Ifrs), typeof(TransactionResources))]
        public string Ifrs { get; set; }

        [Tab("other")]
        [Order(36)]
        [DisplayNameLocalized(nameof(TransactionResources.Vertical), typeof(TransactionResources))]
        public string Vertical { get; set; }
    }
}
