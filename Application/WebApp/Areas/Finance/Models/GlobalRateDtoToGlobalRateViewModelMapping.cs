﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class GlobalRateDtoToGlobalRateViewModelMapping : ClassMapping<GlobalRateDto, GlobalRateViewModel>
    {
        public GlobalRateDtoToGlobalRateViewModelMapping()
        {
            Mapping = d => new GlobalRateViewModel
            {
                Id = d.Id,
                Name = d.Name,
                Value = d.Value,
                CurrencyId = d.CurrencyId,
                Description = d.Description,
                JobMatrixLevelId = d.JobMatrixLevelId,
                LocationId = d.LocationId,
                CompanyId = d.CompanyId
            };
        }
    }
}
