﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class AggregatedProjectTransactionsDtoToAggregatedProjectTransactionsViewModelMapping :
        ClassMapping<AggregatedProjectTransactionsDto, AggregatedProjectTransactionsViewModel>
    {
        public AggregatedProjectTransactionsDtoToAggregatedProjectTransactionsViewModelMapping()
        {
            Mapping = d => new AggregatedProjectTransactionsViewModel
            {
                Id = d.Id,
                ProjectId = d.ProjectId,
                OrgUnitId = d.OrgUnitId,
                ProjectShortName = d.ProjectShortName,
                ClientShortName = d.ClientShortName,
                Revenue = d.Revenue,
                ProjectCost = d.ProjectCost,
                ProjectMargin = d.ProjectMargin,
                ProjectMarginPercent = d.ProjectMarginPercent,
                OperationCost = d.OperationCost,
                OperationMargin = d.OperationMargin,
                OperationMarginPercent = d.OperationMarginPercent,
                ServiceCost = d.ServiceCost,
                ServiceMargin = d.ServiceMargin,
                ServiceMarginPercent = d.ServiceMarginPercent,
                Region = d.Region,
                Hours = d.Hours,
                EstimatedRevenue = d.EstimatedRevenue
            };
        }
    }
}