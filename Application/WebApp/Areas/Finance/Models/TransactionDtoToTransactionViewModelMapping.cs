﻿using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.Finance.Models
{
    public class TransactionDtoToTransactionViewModelMapping : ClassMapping<TransactionDto, TransactionViewModel>
    {
        public TransactionDtoToTransactionViewModelMapping()
        {
            Mapping = d => new TransactionViewModel
            {
                Id = d.Id,
                Description = d.Description,
                AmountPln = d.AmountPln,
                PostingDate = d.PostingDate,
                AmountEur = d.AmountEur,
                DocumentDate = d.DocumentDate,
                DocumentNo = d.DocumentNo,
                DocumentType = d.DocumentType,
                EntryNo = d.EntryNo,
                ExternalDocumentNo = d.ExternalDocumentNo,
                GeneralLedgerAccountNo = d.GeneralLedgerAccountNo,
                SourceNo = d.SourceNo,
                SourceName = d.SourceName,
                SourceCode = d.SourceCode,
                SourceType = d.SourceType,
                SourceCompanyName = d.SourceCompanyName,
                CreatedByUserId = d.CreatedByUserId,
                BudgetPositionCode = d.BudgetPositionCode,
                BudgetPositionName = d.BudgetPositionName,
                CompanyCode = d.CompanyCode,
                CompanyName = d.CompanyName,
                Ifrs = d.Ifrs,
                LocationCode = d.LocationCode,
                LocationName = d.LocationName,
                NoTax = d.NoTax,
                OrgUnitCode = d.OrgUnitCode,
                OrgUnitName = d.OrgUnitName,
                ProjectId = d.ProjectId,
                Account = d.Account,
                CostCenterCode = d.CostCenterCode,
                CostCenterName = d.CostCenterName,
                ProjectManagerUserId = d.ProjectManagerUserId,
                ProjectGroup = d.ProjectGroup,
                ProjectType = d.ProjectType,
                ProjectUtilization = d.ProjectUtilization,
                AccountType = d.AccountType,
                OperationalManagerUserId = d.OperationalManagerUserId,
                Region = d.Region,
                SalesPersonUserId = d.SalesPersonUserId,
                Vertical = d.Vertical,
                GeneralManagerUserId = d.GeneralManagerUserId,
                OperationalDirectorUserId = d.OperationalDirectorUserId
            };
        }
    }
}
