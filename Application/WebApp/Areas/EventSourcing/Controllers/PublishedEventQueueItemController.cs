﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.WebApp.Areas.EventSourcing.Models;
using Smt.Atomic.WebApp.Areas.EventSourcing.Resources;
using Smt.Atomic.WebApp.Controllers;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.WebApp.Areas.EventSourcing.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanViewPublishedEventQueueItem)]
    [PerformanceTest(nameof(PublishedEventQueueItemController.List))]
    public class PublishedEventQueueItemController : ReadOnlyCardIndexController<PublishedEventQueueItemViewModel, PublishedEventQueueItemDto>
    {
        private readonly IPublishedEventQueueItemCardIndexDataService _publishedEventQueueItemDataService;

        public PublishedEventQueueItemController(IPublishedEventQueueItemCardIndexDataService publishedEventQueueItemDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(publishedEventQueueItemDataService, baseControllerDependencies)
        {
            _publishedEventQueueItemDataService = publishedEventQueueItemDataService;
            CardIndex.Settings.AllowMultipleRowSelection = false;
            InitializeFilter();
            InitializeButton();
        }

        [HttpGet]
        [AtomicAuthorize(SecurityRoleType.CanReExecutePublishedEventQueueItem)]
        public virtual ActionResult ReExecute(long id)
        {
            var result = _publishedEventQueueItemDataService.ReExecute(id);
            AddAlerts(result);

            return new RedirectResult(CardIndexHelper.GetReturnOrListUrl(this, Context));
        }

        private void InitializeButton()
        {
            var reExecuteButton = new ToolbarButton
            {
                Text = PublishedEventQueueItemResources.ReExecuteButtonText,
                OnClickAction = Url.UriActionGet(nameof(ReExecute), KnownParameter.SelectedId),
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected,
                    Predicate = "row.exception!=null"
                },
            };

            CardIndex.Settings.ToolbarButtons.Add(reExecuteButton);
        }

        private void InitializeFilter()
        {
            CardIndex.Settings.FilterGroups = new List<FilterGroup>
            {
                new FilterGroup
                {
                    Type = FilterGroupType.RadioGroup,
                    Filters = GetFilters()
                },
            };
        }

        private IList<Filter> GetFilters()
        {
            return new List<Filter>
                       {
                            new Filter
                            {
                                Code = PublishedEventQueueItemFilterCodes.Active,
                                DisplayName = PublishedEventQueueItemResources.ActiveFilterLabel
                            },
                            new Filter
                            {
                                Code = PublishedEventQueueItemFilterCodes.Failed,
                                DisplayName = PublishedEventQueueItemResources.FailedFilterLabel
                            },
                       };
        }
    }
}
