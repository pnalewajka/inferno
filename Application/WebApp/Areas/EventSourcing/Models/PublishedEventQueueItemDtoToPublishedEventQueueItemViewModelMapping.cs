﻿using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.EventSourcing.Models
{
    public class PublishedEventQueueItemDtoToPublishedEventQueueItemViewModelMapping : ClassMapping<PublishedEventQueueItemDto, PublishedEventQueueItemViewModel>
    {
        public PublishedEventQueueItemDtoToPublishedEventQueueItemViewModelMapping()
        {
            Mapping = d => new PublishedEventQueueItemViewModel
            {
                Id = d.Id,
                Timestamp = d.Timestamp,
                BusinessEventId = d.BusinessEventId.ToString(),
                PublishedOn = d.PublishedOn,
                LeasedOn = d.LeasedOn,
                BusinessEvent = d.BusinessEvent,
                Exception = d.Exception
            };
        }
    }
}
