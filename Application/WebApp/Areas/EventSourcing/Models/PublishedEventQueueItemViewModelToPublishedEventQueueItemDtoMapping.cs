﻿using System;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Areas.EventSourcing.Models
{
    public class PublishedEventQueueItemViewModelToPublishedEventQueueItemDtoMapping : ClassMapping<PublishedEventQueueItemViewModel, PublishedEventQueueItemDto>
    {
        public PublishedEventQueueItemViewModelToPublishedEventQueueItemDtoMapping()
        {
            Mapping = v => new PublishedEventQueueItemDto
            {
                Id = v.Id,
                Timestamp = v.Timestamp,
                BusinessEventId = GetBusinessEventId(v.BusinessEventId),
                PublishedOn = v.PublishedOn,
                LeasedOn = v.LeasedOn,
                BusinessEvent = v.BusinessEvent,
                Exception = v.Exception
            };
        }

        private Guid GetBusinessEventId(string businessEventId)
        {
            return string.IsNullOrWhiteSpace(businessEventId) ? Guid.Empty : new Guid(businessEventId);
        }
    }
}
