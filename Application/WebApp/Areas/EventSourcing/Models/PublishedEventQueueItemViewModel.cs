﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.WebApp.Areas.EventSourcing.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApp.Areas.EventSourcing.Models
{
    public class PublishedEventQueueItemViewModel
    {
        public long Id { get; set; }

        public byte[] Timestamp { get; set; }

        [DisplayNameLocalized(nameof(PublishedEventQueueItemResources.BusinessEventIdLabel), typeof(PublishedEventQueueItemResources))]
        public string BusinessEventId { get; set; }

        [DisplayNameLocalized(nameof(PublishedEventQueueItemResources.PublishedOnLabel), typeof(PublishedEventQueueItemResources))]
        public DateTime PublishedOn { get; set; }

        [DisplayNameLocalized(nameof(PublishedEventQueueItemResources.LeasedOnLabel), typeof(PublishedEventQueueItemResources))]
        public DateTime? LeasedOn { get; set; }

        [DisplayNameLocalized(nameof(PublishedEventQueueItemResources.BusinessEventLabel), typeof(PublishedEventQueueItemResources))]
        public string BusinessEvent { get; set; }

        [DisplayNameLocalized(nameof(PublishedEventQueueItemResources.ExceptionLabel), typeof(PublishedEventQueueItemResources))]
        [StringLength(2048)]
        public string Exception { get; set; }
    }
}
