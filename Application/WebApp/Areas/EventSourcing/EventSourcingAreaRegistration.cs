﻿using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Areas.EventSourcing
{
    public class EventSourcingAreaRegistration : AreaRegistration
    {
        public override string AreaName => "EventSourcing";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EventSourcing_default",
                "EventSourcing/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}