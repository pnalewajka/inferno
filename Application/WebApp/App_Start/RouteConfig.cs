﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Smt.Atomic.WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "AtomicDialogs",
                "Dialog/{action}",
                new
                {
                    controller = "Dialog"
                }
            );

            routes.MapRoute(
                "AtomicCache",
                "CacheBackgroundProcessing/{action}",
                new
                {
                    controller = "CacheBackgroundProcessing"
                }
            );

            routes.MapRoute(
                "AtomicErrors",
                "Error/{action}",
                new
                {
                    controller = "Error"
                }
            );

            routes.MapRoute(
                "ValuePicker",
                "ValuePicker/{action}",
                new
                {
                    controller = "ValuePicker"
                }
            );

            routes.MapRoute(
                "TemporaryDocumentUpload",
                "DocumentUpload/{action}",
                new
                {
                    controller = "DocumentUpload"
                }
            );

            routes.MapRoute(
                "TemporaryDocumentDownload",
                "DocumentDownload/DownloadTemporary/{temporaryDocumentId}",
                new
                {
                    controller = "DocumentDownload",
                    action = "DownloadTemporary",
                }
            );

            routes.MapRoute(
                "DocumentDownload",
                "DocumentDownload/Download/{mappingIdentifier}/{documentId}",
                new
                {
                    controller = "DocumentDownload",
                    action = "Download",
                }
            );

            routes.MapRoute(
                "UserPictureDownload",
                "Accounts/UserPicture/Download/{userId}/{userPictureSize}",
                new
                {
                    area = "Accounts",
                    controller = "UserPicture",
                    action = "Download",
                }
            );

            routes.MapRoute(
                "Hub",
                "Hub/{action}",
                new
                {
                    controller = "Hub"
                }
            );

            routes.MapRoute(
                "Default",
                "{area}/{controller}/{action}/{id}",
                new
                {
                    area = "WebSite",
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                }
            );
        }
    }
}