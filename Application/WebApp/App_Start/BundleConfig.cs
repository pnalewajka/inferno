﻿using System;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterStyleSheets(bundles);
            RegisterScripts(bundles);
        }

        private static void RegisterStyleSheets(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/styles/libs")
                            .Include
                            (
                                "~/Content/bootstrap.css",
                                "~/Content/datepicker.css",
                                "~/Content/jquery.timepicker.css",
                                "~/Content/font-awesome.css",
                                "~/Content/bootstrap-select.css",
                                "~/Content/bootstrap-colorpicker.css",
                                "~/Content/jquery.JCrop.css",
                                "~/Content/select2.css",
                                "~/Scripts/atwho/jquery.atwho.css"
                            ).IncludeDirectory("~/Content/Dante", "*.css", true));

            bundles.Add(new StyleBundle("~/styles/libs-atomic-print")
                            .Include
                            (
                                "~/Content/Bootstrap.css",
                                "~/Content/font-awesome.css"
                             ));

            bundles.Add(new StyleBundle("~/styles/atomic-print")
                            .Include
                            (
                                "~/Content/Atomic-print.css"
                             ));

            bundles.Add(new StyleBundle("~/styles/atomic")
                            .Include
                            (
                                "~/Content/Atomic.css"
                            ));

            bundles.Add(new StyleBundle("~/styles/site")
                            .Include
                            (
                                "~/Content/site.css"
                            ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                "~/Content/themes/base/jquery.ui.core.css",
                "~/Content/themes/base/jquery.ui.resizable.css",
                "~/Content/themes/base/jquery.ui.selectable.css",
                "~/Content/themes/base/jquery.ui.accordion.css",
                "~/Content/themes/base/jquery.ui.autocomplete.css",
                "~/Content/themes/base/jquery.ui.button.css",
                "~/Content/themes/base/jquery.ui.dialog.css",
                "~/Content/themes/base/jquery.ui.slider.css",
                "~/Content/themes/base/jquery.ui.tabs.css",
                "~/Content/themes/base/jquery.ui.datepicker.css",
                "~/Content/themes/base/jquery.ui.progressbar.css",
                "~/Content/themes/base/jquery.ui.theme.css"));
        }

        private static void RegisterScripts(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/scripts/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/scripts/libs")
                            .Include(
                                "~/Scripts/importHelpers.js",
                                "~/Scripts/promise.js",
                                "~/Scripts/jquery-2.1.3.js",
                                "~/Scripts/jquery-ui*",
                                "~/Scripts/jquery.validate.js",
                                "~/Scripts/jquery.validate.unobtrusive.js",
                                "~/Scripts/jquery.validate.unobtrusive.bootstrap.js",
                                "~/Scripts/bootstrap.js",
                                "~/Scripts/bootstrap-datepicker.js",
                                "~/Scripts/bootstrap-select.js",
                                "~/Scripts/bootstrap-contextmenu.js",
                                "~/Scripts/jquery.stickytableheaders.js",
                                "~/Scripts/handlebars-v4.0.5.js",
                                "~/Scripts/jquery.stickytableheaders.js",
                                "~/Scripts/jquery.hotkeys.js",
                                "~/Scripts/lodash.underscore.js",
                                "~/Scripts/fileupload/jquery.fileupload.js",
                                "~/Scripts/fileupload/jquery.iframe-transport.js",
                                "~/Scripts/fileupload/jquery.ui.widget.js",
                                "~/Scripts/jquery.JCrop.js",
                                "~/Scripts/select2.js",
                                "~/Scripts/protovis-d3.2.js",
                                "~/Scripts/tableHeadFixer.js",
                                "~/Scripts/jquery.timepicker.js",
                                "~/Scripts/jquery.mobile-events.js"
                            )
                            .IncludeDirectory("~/Scripts/atwho", "*.js", true)
                            .IncludeDirectory("~/Scripts/Atomic/ckeditor", "*.js", true)
                            .IncludeDirectory("~/Scripts/Dante/Mentions", "*.js", true));

            bundles.Add(new ScriptBundle("~/scripts/libs-atomic-print")
                            .Include(
                                "~/Scripts/jquery-{version}.js",
                                "~/Scripts/bootstrap.js"
                            ));

            bundles.Add(new Bundle("~/scripts/atomic-bundle")
                            .Include
                            (
                                "~/Scripts/Atomic/Globals.js",
                                "~/Scripts/Atomic/WebVpnHooks.js",
                                "~/Scripts/Atomic/Strings.js",
                                "~/Scripts/Atomic/UrlHelper.js",
                                "~/Scripts/Atomic/Dialogs.js",
                                "~/Scripts/Atomic/CommonDialogs.js",
                                "~/Scripts/Atomic/HamburgerButtons.js",
                                "~/Scripts/Atomic/Help.js",
                                "~/Scripts/Atomic/Forms.js",
                                "~/Scripts/Atomic/Validation.js",
                                "~/Scripts/Atomic/Utils.js",
                                "~/Scripts/Atomic/Controls.js",
                                "~/Scripts/Atomic/Grid.js",
                                "~/Scripts/Atomic/ValuePicker.js",
                                "~/Scripts/Atomic/Optional.js",
                                "~/Scripts/Atomic/NumericControls.js",
                                "~/Scripts/Atomic/DocumentUpload.js",
                                "~/Scripts/Atomic/GlobalErrorHandling.js",
                                "~/Scripts/Atomic/CustomValidations.js",
                                "~/Scripts/Atomic/Repeater.js",
                                "~/Scripts/Atomic/BrowserSurvey.js",
                                "~/Scripts/Atomic/ClientStorage.js",
                                "~/Scripts/Atomic/AlertMessages.js",
                                "~/Scripts/Atomic/RestorePoints.js",
                                "~/Scripts/Atomic/Tiles.js",
                                "~/Scripts/Atomic/CropImageUpload.js",
                                "~/Scripts/Atomic/CropImage.js",
                                "~/Scripts/Atomic/JQueryExtensions.js",
                                "~/Scripts/Atomic/MapPicker.js",
                                "~/Scripts/Atomic/TypeAhead.js",
                                "~/Scripts/Atomic/DateHelper.js",
                                "~/Scripts/Atomic/ClientSideOperations.js",
                                "~/Scripts/Atomic/OnValueChange.js",
                                "~/Scripts/Atomic/PivotTable.js",
                                "~/Scripts/Atomic/Enum.js",
                                "~/Scripts/Atomic/Sudo.js",
                                "~/Scripts/Atomic/Hub.js",
                                "~/Scripts/Atomic/EventDispatcher.js",
                                "~/Scripts/Atomic/Debouncer.js",
                                "~/Scripts/OneDrive/OneDrive-v7.2.js"
                           ));

            bundles.Add(new Bundle("~/scripts/inferno-bundle")
                            .Include
                            (
                                "~/Scripts/Dante/UserEmployeeHub.js"
                            ));

            bundles.Add(new Bundle("~/scripts/org-chart")
                            .Include
                            (
                                "~/Scripts/jquery.validate.js",
                                "~/Scripts/jquery.validate.unobtrusive.js",
                                "~/Scripts/jquery.orgchart.js",
                                "~/Scripts/es6-promise.auto.js",
                                "~/Scripts/html2canvas.js"
                            ));

            bundles.Add(new Bundle("~/scripts/react")
                            .Include
                            (
                                "~/Scripts/redux.js",
                                "~/Scripts/react.js",
                                "~/Scripts/react-dom.js",
                                "~/Scripts/date-fns/date-fns.js",
                                "~/Scripts/Dante/Utils.js",
                                "~/Scripts/Dante/DetachedScroll.js",
                                "~/Scripts/Dante/ReactComponents.js"
                            ));

            bundles
                .Add(new Bundle("~/scripts/pdf-reader")
                .Include("~/Scripts/pdf/pdf.js", "~/Scripts/pdf/pdf.worker.js", "~/Scripts/pdf/pdfHelper.js"));

            bundles.Add(new ScriptBundle("~/scripts/react-ui")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder()
            }
            .IncludeDirectory("~/Scripts/Dante/Ui", "*.js", true));

            bundles.Add(new ScriptBundle("~/scripts/process-board")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder()
            }
            .IncludeDirectory("~/Scripts/ProcessBoard/", "*.js", true));

            bundles.Add(new StyleBundle("~/styles/react-ui"));

            bundles.Add(new ScriptBundle("~/scripts/timereport")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder()
            }
            .IncludeDirectory("~/Areas/TimeTracking/Scripts/ReactVersion", "*.js", true)
            .Include("~/Scripts/Dante/Ui/ProgressIndicator.js"));

            bundles.Add(new ScriptBundle("~/scripts/utilization-report")
            {
                ConcatenationToken = Environment.NewLine
            }
            .IncludeDirectory("~/Areas/Reports/Scripts/UtilizationReport", "*.js", true));

            bundles.Add(new ScriptBundle("~/scripts/dashboard")
            {
                ConcatenationToken = Environment.NewLine,
                Orderer = new FileNameLengthBundleOrder(false)
            }
            .IncludeDirectory("~/Areas/WebSite/Scripts", "*.js", true));
        }
    }
}