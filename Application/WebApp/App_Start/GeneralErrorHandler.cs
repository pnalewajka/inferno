﻿using System.Net;
using System.Web.Mvc;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.WebApp.Extensions;
using Smt.Atomic.WebApp.Models.Errors;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp
{
    public class GeneralErrorHandler : IExceptionFilter
    {
        private readonly ILogger _logger;
        private readonly ISettingsProvider _settingsProvider;

        public GeneralErrorHandler(ILogger logger, ISettingsProvider settingsProvider)
        {
            _logger = logger;
            _settingsProvider = settingsProvider;
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                HandleAjaxException(filterContext);
            }
            else
            {
                HandleHttpException(filterContext);
            }

            _logger.Error(filterContext.Exception.Message, filterContext.Exception);
        }

        private void HandleHttpException(ExceptionContext filterContext)
        {
            var model = new ErrorViewModel(filterContext, _settingsProvider);

            var viewData = filterContext.Controller.ViewData;
            viewData.Model = model;

            var layout = viewData.AssureLayoutModel();
            layout.PageTitle = AtomicResources.ErrorViewExceptionOccurredMessage;

            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Error/Error.cshtml",
                ViewData = viewData,
                TempData = filterContext.Controller.TempData,
            };

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        private void HandleAjaxException(ExceptionContext filterContext)
        {
            filterContext.Result = new JsonNetResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new
                {
                    error = true,
                    model = new AjaxErrorViewModel(filterContext, _settingsProvider),
                    message = filterContext.Exception.Message
                }
            };

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }
    }
}