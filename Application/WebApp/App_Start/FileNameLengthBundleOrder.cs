﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Optimization;

namespace Smt.Atomic.WebApp
{
    public class FileNameLengthBundleOrder : IBundleOrderer
    {
        private bool Ascending { get; set; }

        public FileNameLengthBundleOrder(bool ascending = true)
        {
            Ascending = ascending;
        }

        public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            if (Ascending)
            {
                return files.OrderBy(f => f.DirectoryName.Length).ThenBy(f => f.Name.Length);
            }
            else
            {
                return files.OrderByDescending(f => f.DirectoryName.Length).ThenByDescending(f => f.Name.Length);
            }
        }
    }
}