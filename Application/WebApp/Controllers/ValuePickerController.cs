using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.WebApp.Controllers
{
    public class ValuePickerController : Controller
    {
        public ActionResult List(string listId, GridParameters gridParameters)
        {
            Type pickerType = IdentifierHelper.GetTypeByIdentifier(listId);

            var queryString = RoutingHelper.ReplaceKnownTokens(Request.QueryString.ToString());
            var parameters = RoutingHelper.GetValueDictionary(queryString);

            parameters[GridParameterModelBinder.ValuePickerModeParameterUrlName] = true;

            var area = RoutingHelper.GetAreaName(pickerType);
            parameters[RoutingHelper.AreaParameterName] = area;

            return RedirectToAction("List", RoutingHelper.GetControllerName(pickerType), parameters);
        }

        [HttpGet]
        public ActionResult JsonList(string listId, string formatterId, GridParameters gridParameters)
        {
            Type pickerType = IdentifierHelper.GetTypeByIdentifier(listId);

            var queryString = RoutingHelper.ReplaceKnownTokens(Request.QueryString.ToString());
            var parameters = RoutingHelper.GetValueDictionary(queryString);

            parameters[GridParameterModelBinder.ValuePickerModeParameterUrlName] = true;

            var area = RoutingHelper.GetAreaName(pickerType);
            parameters[RoutingHelper.AreaParameterName] = area;

            return RedirectToAction("JsonList", RoutingHelper.GetControllerName(pickerType), parameters);
        }
    }
}