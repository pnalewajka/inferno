﻿using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Controllers
{
    public abstract class ReadOnlySubCardIndexController<TViewModel, TDto, TParentController, TContext>
        : ReadOnlyCardIndexController<TViewModel, TDto, TContext>
        where TDto : class, new() where TViewModel : class, new()
        where TParentController : AtomicController, ICardIndexController
    {
        public ReadOnlySubCardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            Layout.Scripts.Add("~/Scripts/Atomic/SubCardIndex.js");
        }

        public override ActionResult List(GridParameters parameters)
        {
            AddGoToParentButton(parameters.Context);

            return base.List(parameters);
        }

        public override object GetContextViewModel()
        {
            var parentController = ReflectionHelper.CreateInstanceWithIocDependencies<TParentController>();
            var contextModel = parentController.GetViewModelFromContext(Context);

            return contextModel;
        }

        protected virtual string GetParentUrl(object context)
        {
            return Url.Action(nameof(List), RoutingHelper.GetControllerName<TParentController>());
        }

        protected virtual void AddGoToParentButton(object context)
        {
            var goToParentButton = new ToolbarButton
            {
                Availability = new ToolbarButtonAvailability { Mode = AvailabilityMode.Always },
                Icon = FontAwesome.ArrowUp,
                Text = AtomicResources.SubCardIndexControllerAddGoToParentButton,
                OnClickAction = new JavaScriptCallAction(GetGoToParentButtonJavaScriptCode(context)),
                Hotkey = HotkeyHelper.GetHotkey(Key.Alt, Key.Up)
            };

            CardIndex.Settings.ToolbarButtons.Add(goToParentButton);
        }

        private string GetGoToParentButtonJavaScriptCode(object context)
        {
            var fallbackUrl = GetParentUrl(context);
            var parentControllerName = RoutingHelper.GetControllerName<TParentController>();

            return $"Forms.redirectToRestorePoint('{parentControllerName}', '{fallbackUrl}')";
        }
    }

    public abstract class ReadOnlySubCardIndexController<TViewModel, TDto, TParentController>
        : ReadOnlySubCardIndexController<TViewModel, TDto, TParentController, ParentIdContext>
        where TDto : class, new()
        where TViewModel : class, new()
        where TParentController : AtomicController, ICardIndexController
    {
        public ReadOnlySubCardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}