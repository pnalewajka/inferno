﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanDownloadDocuments)]
    public class DocumentDownloadController : AtomicController
    {
        private readonly ITemporaryDocumentPromotionService _temporaryDocumentService;
        private readonly IDocumentDownloadService _documentDownloadService;

        public DocumentDownloadController(
            IBaseControllerDependencies baseDependencies,
            ITemporaryDocumentPromotionService temporaryDocumentService,
            IDocumentDownloadService documentDownloadService)
            : base(baseDependencies)
        {
            _temporaryDocumentService = temporaryDocumentService;
            _documentDownloadService = documentDownloadService;
        }

        [HttpGet]
        public ActionResult DownloadTemporary(Guid temporaryDocumentId)
        {
            var temporaryDocumentMetaData = _temporaryDocumentService.GetDocumentMetadata(temporaryDocumentId);

            if (temporaryDocumentMetaData.State != TemporaryDocumentState.Completed)
            {
                throw new IncompleteDocumentException(temporaryDocumentMetaData.Name);
            }

            return new FileStreamResult(_temporaryDocumentService.GetContentStream(temporaryDocumentId),
                temporaryDocumentMetaData.ContentType) { FileDownloadName = temporaryDocumentMetaData.Name };
        }

        [HttpGet]
        public ActionResult Download(string mappingIdentifier, long documentId)
        {
            var documentDownloadDto = _documentDownloadService.GetDocument(mappingIdentifier, documentId);

            return new FileContentResult(documentDownloadDto.Content, documentDownloadDto.ContentType)
            {
                FileDownloadName = documentDownloadDto.DocumentName
            };
        }
    }
}