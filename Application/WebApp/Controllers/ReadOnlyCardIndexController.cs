using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.TreeView;

namespace Smt.Atomic.WebApp.Controllers
{
    public abstract class ReadOnlyCardIndexController<TViewModel, TDto> : ReadOnlyCardIndexController<TViewModel, TDto, ParentIdContext>
        where TViewModel : class, new()
        where TDto : class
    {
        public ReadOnlyCardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = false;
        }
    }

    public abstract class ReadOnlyCardIndexController<TViewModel, TDto, TContext> : AtomicController, IValuePicker, ICardIndexController, IExpandingGridController
        where TViewModel : class, new()
        where TDto : class
    {
        private readonly IExportService _exportService;

        private bool _contextCreated;
        private TContext _context;

        private const int ValuePickerPageSize = 8;
        private const int ValuePickerPaginationButtonMaxCount = 7;
        private const int CopyToClipboardPageSize = 10000;

        protected CardIndex<TViewModel, TDto> CardIndex { get; set; }

        protected TContext Context
        {
            get
            {
                if (!_contextCreated)
                {
                    _context = UrlFieldsHelper.CreateObjectFromQueryParameters<TContext>(System.Web.HttpContext.Current.Request.QueryString.ToDictionary(), CardIndex.Settings.IsContextRequired);
                    _contextCreated = true;
                }

                return _context;
            }
        }

        object ICardIndexController.Context => Context;

        public ReadOnlyCardIndexController(
            ICardIndexDataService<TDto> cardIndexDataService,
            IBaseControllerDependencies baseControllerDependencies)
            : base(baseControllerDependencies)
        {
            _exportService = baseControllerDependencies.ExportService;

            var classMappingFactory = baseControllerDependencies.ClassMappingFactory;
            var alertService = baseControllerDependencies.AlertService;

            CardIndex = new CardIndex<TViewModel, TDto>(Url,
                cardIndexDataService,
                classMappingFactory,
                alertService,
                baseControllerDependencies.PrincipalProvider,
                baseControllerDependencies.AnonymizationService,
                baseControllerDependencies.FieldNameService);

            CardIndex.Settings.AddReadOnlyCardIndexButtons();

            CardIndex.Settings.ViewButton.OnClickAction = Url.UriActionGet(nameof(View), KnownParameter.SelectedId | KnownParameter.Context);

            Layout.Parameters.AddParameter<string>(ParameterKeys.MapPickerDefaultLocation);
            Layout.Parameters.AddParameter<int>(ParameterKeys.MapPickerDefaultRadiusInMeters);
            Layout.Resources.AddFrom<CardIndexResources>("RecordCountFormat");
            Layout.Resources.AddFrom<CardIndexResources>("DeleteRecordConfirmMessage");
            Layout.Resources.AddFrom<CardIndexResources>("DeleteRecordConfirmTitle");
            Layout.Resources.AddFrom<CardIndexResources>("PressOkToContinue");
            Layout.Resources.AddFrom<CardIndexResources>("TextHasBeenCopiedToClipboard");
            Layout.Scripts.OnDocumentReady.Add($"RestorePoints.saveControllerPoint('{Layout.Scripts.ControllerAreaName}/{Layout.Scripts.ControllerName}')");
        }

        public virtual void PrepareForValuePicker(bool allowMultipleRowSelection)
        {
            CardIndex.Settings.Condensed = true;
            CardIndex.Settings.ListViewName = "~/Views/CardIndex/ValuePicker.cshtml";

            CardIndex.Settings.OnSelectionChangedJavaScript = "ValuePicker.onGridSelectionChanged";
            CardIndex.Settings.OnRenderedJavaScript = "ValuePicker.onGridRendered";
            CardIndex.Settings.OnRowDoubleClickedJavaScript = "ValuePicker.onRowDoubleClicked";

            CardIndex.Settings.AllowMultipleRowSelection = allowMultipleRowSelection;
            CardIndex.Settings.ShouldEnterInvokeDefaultButton = false;
            CardIndex.Settings.ShouldDoubleClickToggleSelection = true;

            CardIndex.Settings.PageSize = ValuePickerPageSize;
            CardIndex.Settings.BindingPrefix += "ValuePicker";

            CardIndex.Settings.Pagination.ButtonMaxCount = ValuePickerPaginationButtonMaxCount;
            CardIndex.Settings.Pagination.ShouldIncludePageHint = false;

            CardIndex.Settings.RowMenuMode = RowMenuMode.HamburgerMenu;
            CardIndex.Settings.DisplayMode = GridDisplayMode.Rows;

            Layout.PageTitle = allowMultipleRowSelection
                                   ? DialogResources.ValuePickerDialogPickOneOrMoreTitle
                                   : DialogResources.ValuePickerDialogPickOneTitle;
        }

        public virtual void PrepareForGridExpanding()
        {
            CardIndex.Settings.ListViewName = "~/Views/CardIndex/ExpandList.cshtml";

            CardIndex.Settings.DisplayMode = GridDisplayMode.Rows;
            CardIndex.Settings.TreeMode = TreeDisplayMode.Collapsible;
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex")]
        public virtual ActionResult List(GridParameters parameters)
        {
            return CardIndex.GetListView(this, parameters);
        }

        [HttpGet]
        public virtual ActionResult JsonList(string formatterId, GridParameters parameters)
        {
            Type formatterType = null;

            if (!string.IsNullOrEmpty(formatterId))
            {
                formatterType = IdentifierHelper.GetTypeByIdentifier(formatterId);
            }

            return new JsonNetResult(CardIndex.GetListItems(parameters.SearchPhrase, formatterType, Context));
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/CardIndexView")]
        public virtual ActionResult View(long id)
        {
            return CardIndex.GetViewView(this, id, Context);
        }

        public IList<ListItem> GetListItems(long[] ids, Type formatterType, bool filtered = true)
        {
            return CardIndex.GetListItems(ids, formatterType, filtered);
        }

        public IList<ListItem> GetListItemsBySearchPhrase(string searchPhrase, Type formatterType)
        {
            return CardIndex.GetListItems(searchPhrase, formatterType, Context);
        }

        public virtual object GetViewModelFromContext(object childContext)
        {
            var parentIdContext = childContext as IParentIdContext;

            if (parentIdContext == null)
            {
                throw new InvalidCastException("For context that is not a subclass of ParentIdContext this method must be overridden.");
            }

            return GetViewModelFromContext(parentIdContext.ParentId);
        }

        public object GetViewModelFromContext(long id)
        {
            return CardIndex.GetAnonymizedRecordById(id);
        }

        public virtual object GetContextViewModel()
        {
            return null;
        }

        [DeleteFile]
        public virtual FilePathResult ExportToCsv(GridParameters parameters)
        {
            return _exportService.ExportToCsv(CardIndex, parameters, Url);
        }

        [DeleteFile]
        public virtual FilePathResult ExportToExcel(GridParameters parameters)
        {
            return _exportService.ExportToExcel(CardIndex, parameters, Url);
        }

        public virtual string CopyToClipboard(GridParameters parameters)
        {
            CardIndex.Settings.PageSize = CopyToClipboardPageSize;

            return _exportService.ExportToClipboardFormat(CardIndex, parameters, Url);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (string.IsNullOrWhiteSpace(CardIndex.Settings.ExportToCsvUrl))
            {
                CardIndex.Settings.ExportToCsvUrl = Url.Action("ExportToCsv");
            }

            if (string.IsNullOrWhiteSpace(CardIndex.Settings.ExportToExcelUrl))
            {
                CardIndex.Settings.ExportToExcelUrl = Url.Action("ExportToExcel");
            }

            if (string.IsNullOrWhiteSpace(CardIndex.Settings.CopyToClipboardUrl))
            {
                CardIndex.Settings.CopyToClipboardUrl = Url.Action("CopyToClipboard");
            }

            base.OnActionExecuting(filterContext);
        }
    }
}