﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Models;
using Smt.Atomic.WebApp.Resources;
using ImportViewModel = Smt.Atomic.Presentation.Renderers.Models.ImportViewModel;

namespace Smt.Atomic.WebApp.Controllers
{
    public abstract class CardIndexController<TViewModel, TDto> : CardIndexController<TViewModel, TDto, ParentIdContext>
        where TViewModel : class, new()
        where TDto : class, new()
    {
        public CardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.IsContextRequired = false;
        }
    }

    public abstract class CardIndexController<TViewModel, TDto, TContext> : ReadOnlyCardIndexController<TViewModel, TDto, TContext>
        where TViewModel : class, new()
        where TDto : class, new()
    {
        private readonly IImportService _importService;

        public CardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies)
            : base(cardIndexDataService, baseControllerDependencies)
        {
            CardIndex.Settings.AddStandardCardIndexButtons();

            CardIndex.Settings.AddButton.OnClickAction = Url.UriActionGet(nameof(Add), KnownParameter.Context);
            CardIndex.Settings.EditButton.OnClickAction = Url.UriActionGet(nameof(Edit), KnownParameter.SelectedId | KnownParameter.Context);
            CardIndex.Settings.DeleteButton.OnClickAction = Url.UriActionPost(nameof(Delete), KnownParameter.SelectedIds | KnownParameter.Context);
            CardIndex.Settings.ViewButton.OnClickAction = Url.UriActionGet(nameof(View), KnownParameter.SelectedId | KnownParameter.Context);

            Layout.Parameters.AddParameter<int>(ParameterKeys.TemporaryDocumentMaxUploadChunkSize);
            Layout.Parameters.AddParameter<int>(ParameterKeys.UploadPreviewImageMaxDimension);
            Layout.Parameters.AddParameter<int>(ParameterKeys.UploadCropImageMaxDimension);
            Layout.Parameters.AddParameter<string>(ParameterKeys.MapPickerDefaultLocation);
            Layout.Parameters.AddParameter<int>(ParameterKeys.MapPickerDefaultRadiusInMeters);
            
            Layout.Resources.AddFrom<AtomicResources>(nameof(AtomicResources.CurrentRecord));

            Layout.Resources.AddFrom<AtomicResources>(nameof(AtomicResources.CurrentRecord));

            _importService = baseControllerDependencies.ImportService;
        }

        protected void SwitchAddAndEditToDialogs()
        {
            CardIndex.Settings.AddButton.OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetAddDialog), KnownParameter.Context));
            CardIndex.Settings.EditButton.OnClickAction = JavaScriptCallAction.ShowDialog(Url.UriActionPost(nameof(GetEditDialog), KnownParameter.SelectedId | KnownParameter.Context));
        }

        /// <summary>
        /// Please use this in override of the List method.
        /// </summary>
        protected void SwitchAddAndEditToDetails()
        {
            if (!CardIndex.Settings.EditButton.IsAccessibleToCurrentUser())
            {
                CardIndex.Settings.ViewButton.Text = CardIndexResources.DetailsButtonText;
                CardIndex.Settings.EditButton.Visibility = CommandButtonVisibility.None;

                return;
            }

            var viewAvailability = CardIndex.Settings.ViewButton.Availability as ToolbarButtonAvailability;
            var editAvailability = CardIndex.Settings.EditButton.Availability as ToolbarButtonAvailability;

            if (viewAvailability != null && editAvailability != null)
            {
                var viewUriAction = CardIndex.Settings.ViewButton.OnClickAction as UriAction;
                var editUriAction = CardIndex.Settings.EditButton.OnClickAction as UriAction;

                var predicates = new JavascriptPredicatedAction[] {
                    new JavascriptPredicatedAction(editUriAction, editAvailability.Predicate),
                    new JavascriptPredicatedAction(viewUriAction, viewAvailability.Predicate)
                };

                var predicateAction = new JavascriptPredicatedActions(predicates);
                var onClickAction = predicateAction.ToJavascriptSubmitAction();

                CardIndex.Settings.EditButton.OnClickAction = onClickAction;
            }

            CardIndex.Settings.ViewButton.Visibility = CommandButtonVisibility.None;
            CardIndex.Settings.EditButton.Text = CardIndexResources.DetailsButtonText;
            CardIndex.Settings.EditButton.Availability = new ToolbarButtonAvailability
            {
                Mode = AvailabilityMode.SingleRowSelected,
                Predicate = viewAvailability.Predicate
            };
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/CardIndexAdd")]
        public virtual ActionResult Add()
        {
            return CardIndex.GetAddView(this, Context);
        }

        [HttpPost]
        public virtual ActionResult GetAddDialog()
        {
            return CardIndex.GetAddDialogView(this, Context);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult HandleAddDialog(TViewModel viewModelRecord)
        {
            return CardIndex.HandleAddDialog(this, viewModelRecord, Context);
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/CardIndexEdit")]
        public virtual ActionResult Edit(long id)
        {
            return CardIndex.GetEditView(this, id, Context);
        }

        [HttpPost]
        public virtual ActionResult GetEditDialog(long id)
        {
            return CardIndex.GetEditDialogView(this, id, Context);
        }

        [HttpPost]
        public virtual ActionResult GetViewDialog(long id)
        {
            return CardIndex.GetViewDialogView(this, id, Context);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult HandleEditDialog(TViewModel viewModelRecord)
        {
            return CardIndex.HandleEditDialog(this, viewModelRecord, Context);
        }

        [HttpGet]
        public virtual ActionResult Details(long id)
        {
            return CardIndex.RedirectToDetails(this, id, Context);
        }

        [HttpGet]
        [BreadcrumbBar("CardIndex/CardIndexBulkEdit")]
        public virtual ActionResult EditBulk(long[] ids)
        {
            return CardIndex.GetBulkEditFieldLookupStepView(this, new BulkEditPropertyLookupViewModel(ids), Context);
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/CardIndexAdd")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Add(TViewModel viewModelRecord)
        {
            return CardIndex.HandleAdd(this, viewModelRecord, Context);
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/CardIndexEdit")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(TViewModel viewModelRecord)
        {
            return CardIndex.HandleEdit(this, viewModelRecord, Context);
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/CardIndexBulkEdit")]
        public virtual ActionResult EditBulk(BulkEditPropertyLookupViewModel viewModelRecord)
        {
            return CardIndex.GetBulkEditFieldValueStepView(this, viewModelRecord, Context);
        }

        [HttpPost]
        [BreadcrumbBar("CardIndex/CardIndexBulkEdit")]
        public virtual ActionResult PerformBulkChange(BulkEditViewModel viewModelRecord)
        {
            return CardIndex.HandleBulkEditEntities(this, viewModelRecord, Context);
        }

        [HttpPost]
        public virtual ActionResult Delete([ModelBinder(typeof(CommaSeparatedListModelBinder))]IList<long> ids)
        {
            return CardIndex.HandleDelete(this, ids, Context);
        }

        [HttpGet]
        [DeleteFile]
        public virtual FilePathResult ImportTemplate(int importIndex, ImportTemplateType templateType)
        {
            return CardIndex.GetImportTemplate(importIndex, _importService, templateType);
        }

        [HttpPost]
        public virtual ActionResult ImportDialog(int importIndex)
        {
            return CardIndex.GetImportDialog(this, importIndex);
        }

        [HttpPost]
        public virtual ActionResult Import(int importIndex, ImportViewModel importViewModel)
        {
            return CardIndex.HandleImport(this, importIndex, importViewModel, _importService);
        }

        protected void ConfigureBulkEdit<TCardIndexService>(SecurityRoleType? requiredRole)
        {
            CardIndex.Settings.ConfigureBulkEdit<TCardIndexService>(
                button =>
                {
                    button.OnClickAction =
                    Url.UriActionFormatGet(nameof(EditBulk), KnownParameter.SelectedIds | KnownParameter.Context);
                    button.RequiredRole = requiredRole;
                });
        }
    }
}