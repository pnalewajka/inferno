﻿using System;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ViewModels;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.WebApp.Controllers
{
    [AllowAnonymous]
    public class DialogController : AtomicController
    {
        public DialogController(IBaseControllerDependencies baseDependencies) 
            : base(baseDependencies)
        {
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Alert(string htmlMessage, string title = null, string iconName = null,
                                  string okButtonText = null, string okButtonTag = null)
        {
            var dialogModel = new AlertDialogViewModel
            {
                HtmlMessage = htmlMessage,
                IconName = iconName,
                Title = title ?? DialogClientResources.AlertDialogTitle,
                OkButtonText = okButtonText ?? DialogClientResources.AlertDialogOkButtonText,
                OkButtonTag = okButtonTag ?? DialogHelper.OkButtonTag
            };

            return PartialView("~/Views/Dialog/Alert.cshtml", dialogModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Confirm(string htmlMessage, string title = null, string iconName = null,
                                    string okButtonText = null, string cancelButtonText = null)
        {
            var dialogModel = new ConfirmDialogViewModel
            {
                HtmlMessage = htmlMessage,
                IconName = iconName,
                Title = title ?? DialogClientResources.ConfirmDialogTitle,
            };

            dialogModel.OkButton.Text = okButtonText ?? DialogClientResources.ConfirmDialogOkButtonText;
            dialogModel.CancelButton.Text = cancelButtonText ?? DialogClientResources.ConfirmDialogCancelButtonText;

            return PartialView(dialogModel);
        }

        [HttpPost]
        public ActionResult ShowModel(string modelId, string title = null, string iconName = null, StandardButtons standardButtons = StandardButtons.OkCancel)
        {
            var modelType = IdentifierHelper.GetTypeByIdentifier(modelId, false);

            if (modelType == null)
            {
                var encodedType = Server.HtmlEncode(modelId);
                var htmlMessage = string.Format(DialogResources.ShowModelIdNotFoundHtmlMessageFormat, encodedType);

                return Error(encodedType, htmlMessage);
            }

            var model = CreateModel(modelType);

            if (Request.QueryString.Count > 0)
            {
                ViewModelHelper.TryUpdateModel(this, model, clearValidationErrors: true);
            }

            var dialogModel = GetModelBasedDialogViewModel(title, iconName, standardButtons, model);

            return PartialView(dialogModel);
        }

        private static object CreateModel(Type modelType)
        {
            return typeof(DialogController).GetMethod(nameof(CreateTypedModel), BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(modelType).Invoke(null, new object[0]);
        }

        private static TViewModel CreateTypedModel<TViewModel>()
        {
            var factory = ReflectionHelper.TryResolveInterface<IViewModelFactory<TViewModel>>();
            return factory != null ? factory.Create() : Activator.CreateInstance<TViewModel>();
        }

        private static ModelBasedDialogViewModel<object> GetModelBasedDialogViewModel(string title, string iconName,
                                                                    StandardButtons standardButtons,
                                                                    object model)
        {
            var dialogModel = new ModelBasedDialogViewModel<object>(model, standardButtons)
            {
                IconName = iconName,
                Title = title ?? DialogResources.ShowModelDialogTitle,
            };

            return dialogModel;
        }

        private ActionResult Error(string title, string htmlMessage)
        {
            var dialogModel = new ErrorDialogViewModel
            {
                HtmlMessage = htmlMessage,
                Title = title
            };

            return PartialView("~/Views/Dialog/Error.cshtml", dialogModel); 
        }

        [HttpPost]
        public ActionResult ValidateModel(string modelId, string title = null, string iconName = null,
                                          StandardButtons standardButtons = StandardButtons.OkCancel)
        {
            var modelType = IdentifierHelper.GetTypeByIdentifier(modelId, false);

            if (modelType == null)
            {
                var encodedType = Server.HtmlEncode(modelId);
                var htmlMessage = string.Format(DialogResources.ShowModelIdNotFoundHtmlMessageFormat, encodedType);

                return Error(encodedType, htmlMessage);
            }

            var model = CreateModel(modelType);

            var dialogModel = GetModelBasedDialogViewModel(title, iconName, standardButtons, model);

            if (ViewModelHelper.TryUpdateModel(this, model))
            {
                return DialogHelper.HandleEvent(dialogModel.DialogId, DialogHelper.OkButtonTag, model);
            }

            return PartialView("~/Views/Dialog/ShowModel.cshtml", dialogModel);
        }
    }
}