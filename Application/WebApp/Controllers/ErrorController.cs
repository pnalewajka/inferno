﻿using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.WebApp.Models.Errors;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Controllers
{
    [AllowAnonymous]
    public class ErrorController : AtomicController
    {
        public const string ExceptionKey = "exception";
        private readonly ISettingsProvider _settingsProvider;

        public ErrorController(IBaseControllerDependencies baseDependencies) : base(baseDependencies)
        {
            _settingsProvider = baseDependencies.SettingsProvider;
        }

        [PasswordIssueRedirect(false)]
        public ActionResult PageNotFound()
        {
            var model = new PageNotFoundViewModel(_settingsProvider);

            Layout.PageTitle = Layout.PageHeader = AtomicResources.PageNotFoundPageTitle;

            return View("~/Views/Error/PageNotFound.cshtml", model);
        }

        [PasswordIssueRedirect(false)]
        public ActionResult OtherError()
        {
            var exception = RouteData.Values[ExceptionKey] as Exception;
            var model = new ErrorViewModel(exception, _settingsProvider);

            Layout.PageTitle = Layout.PageHeader = AtomicResources.ErrorViewExceptionOccurredMessage;

            return View("~/Views/Error/Error.cshtml", model);
        }
    }
}