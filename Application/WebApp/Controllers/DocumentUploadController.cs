﻿using System;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Logging;
using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.MediaProcessing.Interfaces;
using Smt.Atomic.CrossCutting.MediaProcessing.Models;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.HttpHeaders;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.WebApp.Models.DocumentUpload;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Controllers
{
    [AtomicAuthorize(SecurityRoleType.CanUploadDocuments)]
    public class DocumentUploadController : AtomicController
    {
        private const string ImageMimeMatch = "image/";

        private readonly ILogger _logger;
        private readonly IImageProcessingService _imageProcessingService;
        private readonly ITemporaryDocumentPromotionService _temporaryDocumentService;

        public DocumentUploadController(
            IBaseControllerDependencies baseDependencies,
            ITemporaryDocumentPromotionService temporaryDocumentService,
            ILogger logger,
            IImageProcessingService imageProcessingService)
            : base(baseDependencies)
        {
            _temporaryDocumentService = temporaryDocumentService;
            _logger = logger;
            _imageProcessingService = imageProcessingService;
        }

        [HttpPost]
        public ActionResult BeginUpload()
        {
            return new JsonNetResult(new TemporaryDocumentIdentifier());
        }

        [HttpPost]
        public ActionResult UploadDocument(Guid temporaryDocumentId)
        {
            //As for now only DocumentUpload file by file upload method is supported, no simultanious upload is possible
            if (Request.Files == null || Request.Files.Count != 1)
            {
                return new JsonNetResult(DocumentDescriptor.UploadFailedResult);
            }

            var documentDescriptor = UploadFile(Request.Files[0], temporaryDocumentId);

            return new JsonNetResult(documentDescriptor);
        }

        [HttpPost]
        public ActionResult UploadImage(Guid temporaryDocumentId)
        {
            //As for now only DocumentUpload file by file upload method is supported, no simultanious upload is possible
            if (Request.Files == null || Request.Files.Count != 1)
            {
                return new JsonNetResult(DocumentDescriptor.UploadFailedResult);
            }

            var uploadBlock = Request.Files[0];

            if (uploadBlock == null || uploadBlock.ContentType == null || !uploadBlock.ContentType.StartsWith(ImageMimeMatch))
            {
                return new JsonNetResult(DocumentDescriptor.UploadFailedResult);
            }

            var uploadedDocumentDescriptor = UploadFile(uploadBlock, temporaryDocumentId);
            var uploadedImageDescriptor = new ImageDescriptor(uploadedDocumentDescriptor);

            if (!uploadedDocumentDescriptor.UploadFailed && uploadedDocumentDescriptor.WasDocumentCompleted)
            {
                using (var imageDataStream = _temporaryDocumentService.GetContentStream(temporaryDocumentId))
                {
                    try
                    {
                        var imageSize = _imageProcessingService.GetImageSize(imageDataStream);
                        uploadedImageDescriptor.Width = imageSize.Width;
                        uploadedImageDescriptor.Height = imageSize.Height;
                    }
                    catch (Exception e)
                    {
                        _logger.ErrorFormat(e, "Error while processing image");

                        return new JsonNetResult(DocumentDescriptor.UploadFailedResult);
                    }
                }
            }

            return new JsonNetResult(uploadedImageDescriptor);
        }

        [HttpPost]
        public ActionResult ShowCropImageDialog(string temporaryDocumentId, string viewModelIdentifier, string propertyName)
        {
            var model = new CropImageModel
            {
                TemporaryDocumentId = temporaryDocumentId,
                Settings = new CropImageSettings(CropImageAttributeHelper.GetCropAttribute(viewModelIdentifier, propertyName))
            };

            var dialogModel = new ModelBasedDialogViewModel<CropImageModel>(model)
            {
                Title = ComponentResources.CropImagePopupTitle
            };

            return PartialView("~/Views/DocumentUpload/CropImage.cshtml", dialogModel);
        }

        [HttpPost]
        public ActionResult CropImage(CropImageModel model)
        {
            var dialogModel = new ModelBasedDialogViewModel<CropImageModel>(model)
            {
                Title = ComponentResources.CropImagePopupTitle
            };

            model.Settings =
                new CropImageSettings(CropImageAttributeHelper.GetCropAttribute(model.ViewModelIdentifier,
                    model.PropertyName));

            using (var imageDataStream = _temporaryDocumentService.GetContentStream(new Guid(model.TemporaryDocumentId)))
            {
                try
                {
                    using (
                        var croppedImage = _imageProcessingService.CropImage(imageDataStream,
                            new ImageCropOptions(model.CropPositionLeft, model.CropPositionTop, model.CropSizeWidth,
                                model.CropSizeHeight)))
                    {
                        var documentMetaData = _temporaryDocumentService.GetDocumentMetadata(new Guid(model.TemporaryDocumentId));
                        var newTemporaryDocumentId = _temporaryDocumentService.CreateTemporaryDocument(documentMetaData.Name, documentMetaData.ContentType, croppedImage.ContentLength, null);
                        _temporaryDocumentService.AddChunk(newTemporaryDocumentId, croppedImage.Content);
                        _temporaryDocumentService.CompleteTemporaryDocument(newTemporaryDocumentId);

                        var documentDescriptor = new DocumentDescriptor
                        {
                            ContentType = documentMetaData.ContentType,
                            DocumentName = documentMetaData.Name,
                            TemporaryDocumentId = newTemporaryDocumentId,
                            UploadFailed = false,
                            WasDocumentCompleted = true
                        };

                        return DialogHelper.HandleEvent(dialogModel.DialogId, DialogHelper.OkButtonTag,
                            new ImageDescriptor(documentDescriptor, croppedImage.Size.Width,
                                croppedImage.Size.Height));
                    }
                }
                catch (Exception e)
                {
                    _logger.ErrorFormat(e, "Error while processing image");

                    return new JsonNetResult(DocumentDescriptor.UploadFailedResult);
                }
            }
        }

        private DocumentDescriptor UploadFile(HttpPostedFileBase uploadedBlock, Guid temporaryDocumentId)
        {
            var contentRangeHeaderString = Request.Headers.Get(ContentRangeHeader.HeaderName);

            try
            {
                if (string.IsNullOrWhiteSpace(contentRangeHeaderString))
                {
                    return ProcessWholeFile(uploadedBlock, temporaryDocumentId);
                }

                return ProcessChunkedFile(uploadedBlock, temporaryDocumentId, contentRangeHeaderString);
            }
            catch (Exception e)
            {
                _logger.Error("Document upload failed", e);
            }

            return DocumentDescriptor.UploadFailedResult;
        }

        private DocumentDescriptor ProcessWholeFile(HttpPostedFileBase file, Guid temporaryDocumentId)
        {
            _temporaryDocumentService.CreateTemporaryDocument(file.FileName, file.ContentType,
                file.ContentLength, temporaryDocumentId);
            _temporaryDocumentService.AddChunk(temporaryDocumentId, file.InputStream);
            var documentCompleted = _temporaryDocumentService.CompleteTemporaryDocument(temporaryDocumentId);

            return
                new DocumentDescriptor
                {
                    DocumentName = file.FileName,
                    TemporaryDocumentId = temporaryDocumentId,
                    ContentType = file.ContentType,
                    WasDocumentCompleted = documentCompleted
                };
        }

        private DocumentDescriptor ProcessChunkedFile(HttpPostedFileBase fileChunk, Guid temporaryDocumentId,
            string contentRangeHeaderString)
        {
            var documentCompleted = false;
            ContentRangeHeader contentRangeHeader;

            if (!ContentRangeHeader.TryParse(contentRangeHeaderString, out contentRangeHeader))
            {
                return DocumentDescriptor.UploadFailedResult;
            }

            if (contentRangeHeader.IsLengthUnknown || contentRangeHeader.IsUnSatisfiedRange)
            {
                return DocumentDescriptor.UploadFailedResult;
            }

            if (contentRangeHeader.FirstBytePos == 0)
            {
                _temporaryDocumentService.CreateTemporaryDocument(fileChunk.FileName, fileChunk.ContentType,
                    contentRangeHeader.CompleteLength, temporaryDocumentId);
            }

            _temporaryDocumentService.AddChunk(temporaryDocumentId, fileChunk.InputStream);

            if (contentRangeHeader.LastBytePos == contentRangeHeader.CompleteLength - 1)
            {
                documentCompleted = _temporaryDocumentService.CompleteTemporaryDocument(temporaryDocumentId);
            }

            return
                new DocumentDescriptor
                {
                    DocumentName = fileChunk.FileName,
                    TemporaryDocumentId = temporaryDocumentId,
                    ContentType = fileChunk.ContentType,
                    WasDocumentCompleted = documentCompleted
                };
        }

        private class DocumentDescriptor
        {
            [JsonProperty("documentName")]
            public string DocumentName { get; set; }

            [JsonProperty("temporaryDocumentId")]
            public Guid TemporaryDocumentId { get; set; }

            [JsonProperty("contentType")]
            public string ContentType { get; set; }

            [JsonProperty("uploadFailed")]
            public bool UploadFailed { get; set; }

            [JsonProperty("wasDocumentCompleted")]
            public bool WasDocumentCompleted { get; set; }

            public static readonly DocumentDescriptor UploadFailedResult = new DocumentDescriptor { UploadFailed = true };
        }

        private class ImageDescriptor : DocumentDescriptor
        {
            public ImageDescriptor(DocumentDescriptor documentDescriptor)
            {
                DocumentName = documentDescriptor.DocumentName;
                TemporaryDocumentId = documentDescriptor.TemporaryDocumentId;
                ContentType = documentDescriptor.ContentType;
                UploadFailed = documentDescriptor.UploadFailed;
                WasDocumentCompleted = documentDescriptor.WasDocumentCompleted;
            }

            public ImageDescriptor(DocumentDescriptor documentDescriptor, int width, int height) : this(documentDescriptor)
            {
                Width = width;
                Height = height;
            }

            [JsonProperty("width")]
            public int Width { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }
        }

        private class TemporaryDocumentIdentifier
        {
            public TemporaryDocumentIdentifier()
            {
                Identifier = Guid.NewGuid();
            }

            [JsonProperty("temporaryDocumentId")]
            public Guid Identifier { get; private set; }
        }
    }
}