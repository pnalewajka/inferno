using System;
using System.Web.Mvc;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Controllers
{
    public class CacheBackgroundProcessingController : AtomicController
    {
        private readonly ICacheBackroundProcessingService _cacheBackroundProcessingService;
        private readonly ILogger _logger;

        public CacheBackgroundProcessingController(
            ICacheBackroundProcessingService cacheBackroundProcessingService, 
            IBaseControllerDependencies baseControllerDependencies,
            ILogger logger)
            :base(baseControllerDependencies)
        {
            _cacheBackroundProcessingService = cacheBackroundProcessingService;
            _logger = logger;
        }

        [AllowAnonymous]
        public ActionResult StartProcessing(Guid token)
        {
            try
            {
                _logger.Info($"Background processing for token {token} started");
                _cacheBackroundProcessingService.StartProcessing(token);

                return new EmptyResult();

            }
            catch (Exception exception)
            {
                _logger.Fatal($"Cache background processing for token {token} failed", exception);
                throw;
            }
        }
    }
}