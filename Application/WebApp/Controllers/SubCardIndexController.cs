﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.WebApp.Helpers;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Controllers
{
    public abstract class SubCardIndexController<TViewModel, TDto, TParentController, TContext>
        : CardIndexController<TViewModel, TDto, TContext>
        where TDto : class, new() where TViewModel : class, new()
        where TParentController : Controller, ICardIndexController
    {
        public SubCardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
            Layout.Scripts.Add("~/Scripts/Atomic/SubCardIndex.js");
        }

        public override ActionResult List(GridParameters parameters)
        {
            AddGoToParentButton(parameters.Context);

            return base.List(parameters);
        }

        public override object GetContextViewModel()
        {
            var parentController = ReflectionHelper.CreateInstanceWithIocDependencies<TParentController>();

            if (!IsParentAcccessibleForCurrentUser())
            {
                return null;
            }

            var contextModel = parentController.GetViewModelFromContext(Context);

            return contextModel;
        }

        protected virtual string GetParentUrl(object context)
        {
            return Url.Action("List", RoutingHelper.GetControllerName<TParentController>());
        }

        protected virtual void AddGoToParentButton(object context)
        {
            var goToParentButton = new ToolbarButton
            {
                Availability = new ToolbarButtonAvailability { Mode = AvailabilityMode.Always },
                Icon = FontAwesome.ArrowUp,
                Text = AtomicResources.SubCardIndexControllerAddGoToParentButton,
                OnClickAction = new JavaScriptCallAction(GetGoToParentButtonJavaScriptCode(context)),
                Hotkey = HotkeyHelper.GetHotkey(Key.Alt, Key.Up)
            };

            CardIndex.Settings.ToolbarButtons.Add(goToParentButton);
        }

        private string GetGoToParentButtonJavaScriptCode(object context)
        {
            var fallbackUrl = GetParentUrl(context);
            var parentControllerName = RoutingHelper.GetControllerName<TParentController>();

            return $"Forms.redirectToRestorePoint('{parentControllerName}', '{fallbackUrl}')";
        }

        private bool IsParentAcccessibleForCurrentUser()
        {
            var requiredRoles = CardIndexControllerAuthorizationHelper.GetRequiredSecurityRolesForController<TParentController>();

            var currentPrincipal = GetCurrentPrincipal();

            return requiredRoles.All(r => currentPrincipal.IsInRole(r));
        }
    }

    public abstract class SubCardIndexController<TViewModel, TDto, TParentController>
        : SubCardIndexController<TViewModel, TDto, TParentController, ParentIdContext>
        where TDto : class, new()
        where TViewModel : class, new()
        where TParentController : Controller, ICardIndexController
    {
        public SubCardIndexController(ICardIndexDataService<TDto> cardIndexDataService, IBaseControllerDependencies baseControllerDependencies) : base(cardIndexDataService, baseControllerDependencies)
        {
        }
    }
}