﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Controllers
{
    [AtomicAuthorize]
    public class HubController
        : Controller
    {
        public ActionResult RenderHub(string identifier, long id)
        {
            var pickerControllerType = IdentifierHelper.GetTypeByIdentifier(identifier);

            return new TransferActionResult(
                pickerControllerType,
                nameof(IHubController.Hub));
        }
    }
}