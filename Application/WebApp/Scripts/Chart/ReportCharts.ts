﻿namespace InfernoChart {
    export interface IChartEntry {
        Date: Date;
        Value: number;
    }

    export interface IChartGroup {
        GroupName: string;
        GroupClassName: string;
        Elements: IChartEntry[];
    }

    export type ChartPartGenerator = (selector: d3.Selection<any>, xScale: d3.time.Scale<number, number>, yScale: d3.scale.Linear<number, number>, data: IChartEntry[], generatorClassName: string) => void;

    export class ChartGenerator {
        private svg: d3.Selection<any>;
        private height = 200;
        ValueRange: number[];
        DateRange: Date[];

        constructor(private dataProviders: InfernoChart.IChartGroup[], private width = 1200, container = "#chart") {
            this.svg = d3.select(container)
                .append("svg")
                .attr("width", $('table').width())
                .attr("height", this.height)
                .attr("style", "padding-left:" + ($("thead th:first", $('table')).outerWidth() + $("thead th:last", $('table')).outerWidth() / 2 - 5) + "px");

            this.calculateDateRanges(dataProviders);
        }

        private calculateDateRanges(dataProviders: InfernoChart.IChartGroup[]): void {
            const dateResults: Date[] = [];
            const valueResults: number[] = [];
            for (let group of dataProviders) {
                for (let element of group.Elements) {
                    dateResults.push(element.Date);
                    valueResults.push(element.Value);
                }
            }

            this.ValueRange = d3.extent<number>(valueResults, d => d);
            this.DateRange = d3.extent<Date>(dateResults, d => d);
        }

        private verticalAxis: InfernoChart.ChartPartGenerator = (selector, xScale) => {
            selector.append("g")
                .attr("class", "x axis")
                .attr("transform", `translate(0, ${this.height})`)
                .call(d3.svg.axis().scale(xScale)
                    .orient("bottom").ticks(0));
        }

        private horizontalAxis: InfernoChart.ChartPartGenerator = (selector, xScale, yScale) => {
            selector.append("g")
                .attr("class", "y axis")
                .call(d3.svg.axis().scale(yScale)
                    .orient("left").ticks(5));
        }
        private defaultRenderers: InfernoChart.ChartPartGenerator[] = [
            this.verticalAxis,
            this.horizontalAxis
        ];

        render(renderers: InfernoChart.ChartPartGenerator[]): void {

            const xScale = d3.time.scale().range([0, this.width]).domain(this.DateRange);
            const yScale = d3.scale.linear().range([this.height, 0]).domain(this.ValueRange);

            for (let renderer of this.defaultRenderers) {
                renderer(this.svg, xScale, yScale, [], "");
            }
            for (let group of this.dataProviders) {
                for (let renderer of renderers) {
                    renderer(this.svg, xScale, yScale, group.Elements, group.GroupClassName);
                }
            }
        }
    }
}

$(() => {
    $(".toggle-print").click(function (this: JQuery) {
        const $button = $(this);
        const $container = $button.parents(".single-chart-container");

        $button.toggleClass("fa-caret-up");
        $container.toggleClass("not-for-printing");
    });

    const $pageContainer = $("#report-chart-page");

    const parseDate = (value: string): Date => {
        const dateFormat = Globals.getDateFormat();
        return $.fn.datepicker.DPGlobal.parseDate(value, dateFormat);
    };

    $(".selectpicker").change(function (this: JQuery) {
        if (this.form != null) {
            this.form.submit();
            return;
        }

        const managerId = $(this).val();
        if (!!managerId) {
            $pageContainer.removeClass("done");
            window.location.search = `id=${managerId}`;
        }
    });

    let lineCounter = 0;
    let dotCounter = 0;
    let textCounter = 0;
    const currentweek = parseDate($("#CurrentWeek").val());

    const chartGenerators: InfernoChart.ChartPartGenerator[] = [
        (selector: any, xScale: any, yScale: any, data: any, groupClassName: string) => {
            var valueline = d3.svg.line<InfernoChart.IChartEntry>()
                .x(d => xScale(d.Date))
                .y(d => yScale(d.Value))
                .interpolate("linear");

            selector.append("path")
                .attr("class", Dante.Utils.CombineClassNames(["line", `line-${lineCounter++ + 1}`, groupClassName]))
                .attr("d", valueline(data));
        },
        (selector: any, xScale: any, yScale: any, data: any, groupClassName: string) => {
            let c = 0;
            for (let item of data) {
                selector.append("circle")
                    .attr("id", `circle_${c}`)
                    .attr("data-index", c)
                    .attr("data-set", dotCounter + 1)
                    .attr("cy", (d: any) => yScale(item.Value))
                    .attr("cx", (d: any) => xScale(item.Date) + 2.5)
                    .attr("r", (d: any) => 14)
                    .attr("class", Dante.Utils.CombineClassNames(["dot", `dot-${dotCounter + 1}`, groupClassName, currentweek.getTime() === item.Date.getTime() ? "selected-cell" : undefined]));
                c++;
            }
            dotCounter++;
        },
        (selector: any, xScale: any, yScale: any, data: any, groupClassName: string) => {
            let c = 0;
            for (let item of data) {
                selector.append("text")
                    .attr("id", `text_${c}`)
                    .attr("data-index", c)
                    .attr("data-set", textCounter + 1)
                    .attr("y", (d: any) => yScale(item.Value) + 2.5)
                    .attr("x", (d: any) => xScale(item.Date) + 2.5)
                    .attr("text-anchor", "middle")
                    .attr("class", Dante.Utils.CombineClassNames(["text", `text-${textCounter + 1}`, groupClassName]))
                    .attr("fill", "#fff")
                    .text(item.Value);
                c++;
            }

            textCounter++;
        }
    ];

    $(".chart-container[data-for]").each((index, element) => {
        const $container = $(element);
        const chartId = $container.data("for");
        const $table = $(".table", $container);

        const chartGroups: InfernoChart.IChartGroup[] = [];
        const columns: Date[] = [];

        $("thead tr:last th", $table).slice(1).map((i, e) => {
            columns.push(parseDate($(e).data("time")));
        });

        $("tbody tr.chart_row", $table).each((row_index, row) => {
            var textValues: string[] = [];
            $("td", row)
                .map((i, val) => textValues.push($(val).data("value")));

            const [groupName] = textValues;
            const values = textValues.slice(1).map(i => parseFloat(i));

            chartGroups.push({
                GroupName: groupName,
                GroupClassName: row.className,
                Elements: columns.map((date, i) => {
                    return <InfernoChart.IChartEntry>{ Date: date, Value: values[i] };
                })
            });
        });

        const $thead = $("thead", $table);
        const $tbody = $("tbody", $table);

        const $firstColumn = $("th:first", $thead);
        const $lastColumn = $("td:last", $tbody);

        var width = $thead.outerWidth() - $firstColumn.outerWidth() - $lastColumn.outerWidth();
        var generator = new InfernoChart.ChartGenerator(chartGroups, width, chartId);

        generator.render(chartGenerators);
        $('svg').hide().find('text').fadeOut().fadeIn().end().fadeIn(500);
    });

    let $thisWeekColumn = $(".chart-container[data-for]:first .table td.selected-cell");

    if (!!$thisWeekColumn) {
        $(".content.container-fluid").scrollLeft($thisWeekColumn.position().left);
    }


    $("svg text.text[data-index][data-set]").each((index, element) => {
        const $textElement = $(element);
        const indexCounter = $textElement.data("index");
        const setValue = $textElement.data("set");
        const textWidth = $textElement[0].getBoundingClientRect().width * 0.55;
        $(`#circle_${indexCounter}[data-set="${setValue}"]`).attr("r", Math.max(textWidth, 14) + "px");
    });

    $(".x.axis .tick line").attr("y2", "10");
    $(".x.axis .tick text").attr("y", "18");
    $(".y.axis .tick line").attr("x", "-10").attr("x2", "-10");
    $(".y.axis .tick text").attr("x", "-18").attr("x2", "-18");

    $pageContainer.addClass("done");
});