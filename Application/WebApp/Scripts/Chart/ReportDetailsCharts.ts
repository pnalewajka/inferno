﻿﻿module ReportDetailsCharts {
    export function setFixedColumns(containerSelector: string): void {
        const $fixedColumns = $(".fix-horizontal", containerSelector);
        const $fixedHeaders = $(".fix-vertical", containerSelector);

        $(containerSelector)
            .scroll(function (this: JQuery) {
                const scrollLeft = $(this).scrollLeft();
                const scrollTop = $(this).scrollTop();

                $fixedColumns.css("left", scrollLeft);
                $fixedHeaders.css("top", scrollTop);
            });
    }

    export function collapseExpand($container: JQuery): void {
        $(".fa", $container).click(function (this: JQuery) {
            const $item = $(this);
            const $parent = $item.parents("tbody:first");

            $item.toggleClass("fa-chevron-down").toggleClass("fa-chevron-up");
            $parent.toggleClass("collapsed");
        });
    }

    export function attachBenchAnchors(baseUrl: string, $container: JQuery, period: Date, orgUnitId: number): void {
        $("tr[data-profile] td a[href]", $container).hover(function (this: JQuery) {
            const $this = $(this);
            if ($this.is("[href='#']")) {
                const $row = $this.parents("tr:first");
                const level = $row.data("level");
                const profile = $row.data("profile");
                const location = $this.data("location");

                $this.attr("href", navigationUrl(baseUrl, period, orgUnitId, location, profile, level));
            }
        }, () => { });
    }


    function navigationUrl(baseUrl: string, period: Date, orgUnitId: number, locationId: number, jobProfile: number, level: number): string {
        baseUrl += `?week-start.week=${period}&org-units-filter.org-unit-ids=${orgUnitId}`;
        let filterValue = "org-units-filter,week-start,skills-and-knowledge*projectcontributor";

        if (!!locationId) {
            filterValue += ",location";
            baseUrl += `&location.location-ids=${locationId}`;
        }

        if (!!jobProfile) {
            baseUrl += `&skills-and-knowledge.job-profile-ids=${jobProfile}`;
        }

        if (!!level) {
            baseUrl += `&skills-and-knowledge.job-matrix-levels=${level}`;
        }

        baseUrl += `&filter=${filterValue}`;

        return baseUrl;
    }

    export function onReportFilterChange(eventArgs: Dialogs.EventArgs): void {
        if (eventArgs.isOk) {
            const model = eventArgs.model as { OrgUnitIds: number[], CountryIds: number[], LocationIds: number[], DateFrom: string, DateTo: string };

            model.DateFrom = model.DateFrom === null ? "" : model.DateFrom;
            model.DateTo = model.DateTo === null ? "" : model.DateTo;

            window.location.search = [
                `DateFrom=${model.DateFrom.substr(0, 10)}`,
                `DateTo=${model.DateTo.substr(0, 10)}`,
                `OrgUnitIds=${model.OrgUnitIds.join(",")}`,
                `CountryIds=${model.CountryIds.join(",")}`,
                `LocationIds=${model.LocationIds.join(",")}`,
            ].join("&");
        }
    }
}

$(() => {
    const $container = $("#report-chart-page");
    $container.addClass("with_details");

    ReportDetailsCharts.setFixedColumns(".content.container-fluid");

    $(".text", $container).click(function (this: JQuery) {
        const $item = $(this);
        const dialogUrl = $("#report-chart-page[data-chart-details]").data("chart-details") + window.location.search;

        if (!!dialogUrl) {

            const orgUnitId = $("#ChartDropDown").val();
            const period = $($item.parents(".chart-container").find("th[data-time]")[$item.data("index")]).data("time");

            Dialogs.showDialog({
                actionUrl: dialogUrl,
                formData: { period },
                eventHandler: () => { }
            });
        }
    });
});