namespace Dante.ProcessBoard {
    export class ProcessBoardGroup<T extends IProcessItem> extends React.Component<{ group: IProcessBoardGroup<T> }, {}> {
        public render(): JSX.Element {
            const { group, children } = this.props;

            return (
                <div className="process-board-group">
                    <div className="process-board-group-header process-board-group-container">
                        <div className="process-board-group-header-title ellipsis">
                            {group.name}
                        </div>
                    </div>
                    <div className="process-board-group-body process-board-group-container">
                        {children}
                    </div>
                </div>
            );
        }
    }
}
