namespace Dante.ProcessBoard {
    export class ProcessBoardSwimlane<T extends IProcessItem> extends React.Component<{ swimlane: IProcessBoardSwimlane<T> }, {}> {
        public render(): JSX.Element {
            const { swimlane, children } = this.props;

            return (
                <div className="process-board-swimlane">
                    {
                        swimlane.name.length > 0
                            ? <div className="process-board-swimlane-title">{swimlane.name}</div>
                            : null
                    }
                    <div className="process-board-swimlane-body">
                        {children}
                    </div>
                </div>
            );
        }
    }
}
