namespace Dante.ProcessBoard {
    export interface IProcessAction<T> {
        optimisticUpdate(): Promise<T>;
        serverUpdate(item?: T): Promise<T> | T;
        canExecuteAction: () => boolean;
    }

    export interface IItemProcessAction<T> extends IProcessAction<T> {
        displayName: string;
        className?: string;
        iconClassName?: string;
    }

    export interface IProcessBoardSwimlane<T extends IProcessItem> {
        name: string;
        isSwimlaneItem: (item: T) => boolean;
        isSwimlaneGroupItem: (item: IProcessBoardGroup<T>) => boolean;
    }

    export interface IProcessBoardGroup<T extends IProcessItem> {
        groupType: number;
        name: string;
        isGroupItem?: (item: T) => boolean;
        processAction?: (item: T) => IProcessAction<T>;

        availableProcessActions?: (item: T) => Array<IItemProcessAction<T>>;
    }

    export interface IProcessItem {
        id: number;
        $isReloading?: boolean;
        $loadingError?: string;
        $deleted?: boolean;
    }

    export interface IProcessBoardProps<T extends IProcessItem> {
        Groups: Array<IProcessBoardGroup<T>>;
        Swimlanes?: Array<IProcessBoardSwimlane<T>>;
        Items: T[];
    }

    export interface IProcessBoardState<T> {
        Items: T[];
    }
}
