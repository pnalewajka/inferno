namespace Dante.ProcessBoard {
    export const archivumItems: Dante.Utils.IIndexDictionary<IProcessItem> = {};
    export const actionsTriggered: Dante.Utils.IIndexDictionary<IProcessAction<IProcessItem>> = {};

    function toggleReloading<T extends IProcessItem>(item: T, $isReloading: boolean): T {
        return {
            ...item as object, ...{ $isReloading }
        } as T;
    }

    export function RetryProcessAction<T extends IProcessItem>(item: T, updateState: (newState: T) => void): void {
        const action = actionsTriggered[item.id];

        if (action === undefined) {
            return;
        }

        HandleProcessAction(item, action, updateState);
    }

    export function HandleProcessAction<T extends IProcessItem>(item: T, processAction: IProcessAction<T>, updateState: (newState: T) => void): Promise<T> {
        if (!processAction.canExecuteAction()) {
            return Promise.reject("Can't execute action");
        }

        if (archivumItems[item.id] === undefined) {
            archivumItems[item.id] = item;
        }

        actionsTriggered[item.id] = processAction;

        return new Promise<T>((globalResolve, globalReject) => {
            processAction.optimisticUpdate().then((optimisticItem) => {
                updateState(toggleReloading(optimisticItem, true));

                Promise.resolve(processAction.serverUpdate(optimisticItem))
                    .then(serverItem => toggleReloading(serverItem, false))
                    .then(serverItem => {
                        if (!serverItem.$loadingError) {
                            delete actionsTriggered[item.id];
                            delete archivumItems[item.id];
                        }

                        if (serverItem !== undefined) {
                            updateState(serverItem);
                            globalResolve(serverItem);
                        } else {
                            updateState(optimisticItem);
                            globalResolve(optimisticItem);
                        }
                    })
                    .catch(($loadingError: string) => {
                        const errorItem = {
                            ...optimisticItem as object,
                            ...{ $loadingError }
                        } as T;

                        updateState(errorItem);
                        globalReject(errorItem);
                    });
            }).catch(error => { globalReject(error); });
        });
    }
}
