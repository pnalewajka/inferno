﻿namespace Dante.ProcessBoard {
    export abstract class ProcessBoardBase<T extends IProcessItem> extends Dante.Components.StateComponent<IProcessBoardProps<T>, IProcessBoardState<T>> {
        public constructor() {
            super({ Items: [] });
        }

        protected receiveProps(newProps: IProcessBoardProps<T>): void {
            this.updateState({ Items: [...newProps.Items] });
        }

        private currentlyDragging: EventTarget | null = null;

        private onItemDroped(itemId: number, group: IProcessBoardGroup<T>): void {
            const [item] = this.state.Items.filter(i => i.id === itemId);
            if (item === undefined || group.processAction === undefined || !group.processAction(item).canExecuteAction()) {
                return;
            }

            if (group.processAction !== undefined) {
                if (archivumItems[item.id] === undefined) {
                    archivumItems[item.id] = item;
                }

                this.handleProcessAction(item, group.processAction(item));
            }
        }

        protected updateProcessItem(item: T): void {
            this.updateState({
                Items: [item, ...this.state.Items.filter(i => i.id !== item.id)]
            });
        }

        protected handleProcessAction(item: T, processAction: IProcessAction<T>): Promise<T> {
            return HandleProcessAction<T>(item, processAction, this.updateProcessItem.bind(this));
        }

        protected optymisticUpdate(item: T, group: IProcessBoardGroup<T>): Promise<T> {
            if (group.processAction === undefined) {
                return Promise.resolve(item);
            }

            return group.processAction(item).optimisticUpdate();
        }

        private toggleReloading(item: T, $isReloading: boolean): T {
            return {
                ...item as object, ...{ $isReloading }
            } as T;
        }

        protected serverUpdate(item: T, group: IProcessBoardGroup<T>): Promise<T> | T {
            if (group.processAction === undefined) {
                return this.toggleReloading(item, true);
            }

            return Promise.resolve(group.processAction(item).serverUpdate()).then(r => this.toggleReloading(r, false));
        }

        protected abstract renderItemContent(item: T, group: IProcessBoardGroup<T>): JSX.Element;

        protected renderError(item: T, group: IProcessBoardGroup<T>): JSX.Element {
            return (
                <div className="process-board-group-body-item" title={item.$loadingError}>
                    <div className="process-board-group-body-item-body process-board-group-body-item-body-error">
                        {this.renderItemContent(item, group)}
                        <div className="process-board-group-body-item-buttons">
                            <button
                                className="btn btn-sm btn-link fa fa-repeat"
                                onClick={() => Dante.ProcessBoard.RetryProcessAction(item, this.updateProcessItem.bind(this))}
                                title={Globals.resources.TryAgainButtonText}></button>
                            {
                                archivumItems[item.id] !== undefined
                                    ? <button
                                        className="btn btn-sm btn-link fa fa-times-circle-o"
                                        onClick={() => this.cancelAction(item.id)}
                                        title={Globals.resources.ConfirmDialogCancelButtonText}></button>
                                    : null
                            }
                        </div>
                    </div>
                </div>
            );
        }

        protected renderItem(item: T, group: IProcessBoardGroup<T>): JSX.Element {
            const itemActions = group.availableProcessActions === undefined || !!item.$isReloading
                ? []
                : group.availableProcessActions(item)
                    .filter(a => a.canExecuteAction())
                    .map(a => ({ display: a.displayName, onClick: () => { this.handleProcessAction(item, a); } }));

            return (
                <Dante.Ui.Dragable key={`${item.id}_${!item.$isReloading}`} dragItemId={() => item.id} disabled={!this.canDrag(item, group)}>
                    <div className="process-board-group-body-item">
                        {
                            this.renderItemTitle(item, group).length > 0
                                ? (
                                    <div className="process-board-group-body-item-header">
                                        {this.renderItemTitle(item, group)}
                                    </div>
                                )
                                : null
                        }
                        <div className="process-board-group-body-item-body">
                            {this.renderItemContent(item, group)}
                            {itemActions.length === 0
                                ? null
                                : (
                                    <Dante.Ui.OptionPicker options={itemActions} />
                                )
                            }
                        </div>
                    </div>
                </Dante.Ui.Dragable>
            );
        }

        public render(): JSX.Element {
            const swimlanes = this.props.Swimlanes || [
                { name: "", isSwimlaneGroupItem: item => true, isSwimlaneItem: item => true },
            ];

            return (
                <div className="process-board">
                    {
                        swimlanes.map(s => (
                            <ProcessBoardSwimlane key={s.name} swimlane={s}>
                                {
                                    this.props.Groups.filter(s.isSwimlaneGroupItem).map(g => (
                                        <div className="process-board-container" style={{ width: `${100 / this.props.Groups.length}%` }} key={g.groupType}>
                                            <Dante.Ui.Dropable drop={id => this.onItemDroped(id, g)} canDrop={(itemId: number) => this.canDrop(this.draggedItem(itemId), g, s)}>
                                                <ProcessBoardGroup group={g}>
                                                    {
                                                        this.state.Items
                                                            .filter(item => !item.$deleted && this.isItemForGroup(item, g) && s.isSwimlaneItem(item))
                                                            .map(item => (
                                                                item.$loadingError && item.$loadingError.length > 0
                                                                    ? this.renderError(item, g)
                                                                    : this.renderItem(item, g)
                                                            ))}
                                                </ProcessBoardGroup>
                                            </Dante.Ui.Dropable>
                                        </div>
                                    ))
                                }
                            </ProcessBoardSwimlane>
                        ))
                    }
                </div>
            );
        }

        protected cancelAction(itemId: number): void {
            const archiveItem = archivumItems[itemId];

            this.updateState({
                Items: [archiveItem as T, ...this.state.Items.filter(i => i.id !== archiveItem.id)]
            }, () => {
                delete archivumItems[itemId];
            });
        }

        protected renderItemTitle(item: T, group: IProcessBoardGroup<T>): string { return ""; }
        protected isItemForGroup(item: T, group: IProcessBoardGroup<T>): boolean {
            return group.isGroupItem !== undefined && group.isGroupItem(item);
        }

        protected draggedItem(itemId: number): T {
            const items = this.state.Items.filter(a => a.id === itemId);
            const [item] = items;

            return item;
        }

        protected canDrag(item: T, group: Dante.ProcessBoard.IProcessBoardGroup<T>): boolean {
            return this.props.Groups.some(g => g.processAction !== undefined && g.processAction(item).canExecuteAction());
        }

        protected canDrop(item: T, group: IProcessBoardGroup<T>, swimlane: IProcessBoardSwimlane<T>): boolean {
            return group.processAction !== undefined && group.processAction(item).canExecuteAction() && swimlane.isSwimlaneItem(item);
        }
    }
}
