﻿namespace Dante.Ui
{
    export interface IDropdownOption
    {
        value: string;
        displayValue: string;
    }

    export interface IDropdownProps extends IDisableable
    {
        value?: number | string;
        placeholder?: string;
        options: IDropdownOption[];
        onChange: (value: string) => void;
        selectProps?: React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement>;
    }

    export class Dropdown extends React.Component<IDropdownProps, {}>
    {
        private readonly PLACEHOLDER_KEY: string = "reactdropdown:placeholder";

        public constructor(props: IDropdownProps)
        {
            super(props);
        }

        public setValue(newValue: string)
        {
            this.props.onChange(newValue);
        }

        public render(): JSX.Element
        {
            const options: JSX.Element[] = [];

            if (this.props.placeholder !== undefined)
            {
                options.push(
                    <option key={this.PLACEHOLDER_KEY} value={this.PLACEHOLDER_KEY} disabled={true}>
                        {this.props.placeholder}
                    </option>);
            }

            for (const option of this.props.options)
            {
                options.push(
                    <option key={option.value} value={option.value}>
                        {option.displayValue}
                    </option>);
            }

            let propsClassName = "";
            if (this.props.selectProps !== undefined && this.props.selectProps.className !== undefined)
            {
                propsClassName = this.props.selectProps.className;
            }

            return (
                <select
                    autoFocus={true}
                    {...this.props.selectProps}
                    value={this.props.value !== undefined ? this.props.value.toString() : this.PLACEHOLDER_KEY}
                    onChange={e => this.setValue(e.target.value)}
                    onBlur={e => {
                        this.setValue(this.props.value === undefined ? this.PLACEHOLDER_KEY : this.props.value.toString());
                    }}
                    className={"form-control " + propsClassName}
                    children={options}></select>
            );
        }
    }

    export interface IEnumDropdownProps
    {
        enum: any;
        value?: number;
        placeholder?: string;
        enumResources: Utils.IDictionary<string>;
        onChange: (value: number) => void;
        selectProps?: React.HTMLProps<HTMLSelectElement>;
        className?: string;
        disabled?: boolean;
    }

    export class EnumDropdown extends React.Component<IEnumDropdownProps, {}>
    {
        private getOptions(): IDropdownOption[]
        {
            const options: IDropdownOption[] = [];

            for (const key in this.props.enum)
            {
                if (!isNaN(Number(key)))
                {
                    options.push({
                        value: key,
                        displayValue: this.props.enumResources[key] !== undefined
                            ? this.props.enumResources[key]
                            : this.props.enum[key],
                    });
                }
            }

            return options;
        }

        public render(): JSX.Element
        {
            return <Dropdown
                placeholder={this.props.placeholder}
                selectProps={this.props.selectProps}
                value={this.props.value}
                options={this.getOptions()}
                onChange={(value: string) => { this.props.onChange(parseInt(value)); }} />;
        }
    }
}
