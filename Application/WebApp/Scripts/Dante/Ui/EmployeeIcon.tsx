﻿namespace Dante.Ui {
    export function EmployeeIcon(props: { employeeId: number, employeeName: string } & React.Props<HTMLDivElement>): JSX.Element {
        const backgroundImage = `url(/Accounts/UserPicture/DownloadByEmployee?employeeId=${props.employeeId}&userPictureSize=Normal)`;

        return (
            <div className="employee-icon-container" >
                <div className="employee-icon hub-trigger" onClick={() => Hub.open(props.employeeId, "EmployeeValuePickers.Employee")} title={props.employeeName} style={{ backgroundImage }}>
                    {props.children}
                </div>
            </div>
        );
    }
}
