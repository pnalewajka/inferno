﻿namespace Dante.Ui {
    export interface IDatePickerProps extends IDisableable {
        value?: Date | null;
        onChange: (value: Date | null) => void;
        className?: string;
        autofocus: boolean;
    }

    export class DatePicker extends React.Component<IDatePickerProps, {}>
    {
        public constructor(props: IDatePickerProps) {
            super(props);
        }

        public setValue(newValue: Date) {
            this.props.onChange(newValue);
        }

        private $inputItem?: JQuery;
        private quietValue: boolean = false;

        public shouldComponentUpdate(): boolean {
            return this.$inputItem === undefined;
        }

        private registerDatePicker(input: HTMLElement): void {
            const datePickerLocale = Globals.getDatePickerLocale();
            const dateFormat = Globals.getDateFormat();

            const datePickerOptions = {
                startView: 0,
                language: datePickerLocale,
                autoclose: true,
                format: dateFormat,
            };

            if (input !== null) {
                this.$inputItem = $(input)
                    .datepicker(datePickerOptions)
                    .datepicker("setUTCDate", this.props.value)
                    .on("changeDate", () => {
                        if (this.$inputItem !== undefined && !this.quietValue) {
                            const newDate = this.$inputItem.datepicker("getUTCDate");
                            this.props.onChange(newDate);
                        }
                    })
                    .on("hide", () => {
                        if (this.props.value !== undefined && this.props.value !== null) {
                            this.props.onChange(this.props.value);
                        }
                    });

                if (!!this.props.autofocus && this.$inputItem !== undefined) {
                    this.$inputItem.focus();
                }
            } else if (this.$inputItem !== undefined) {
                this.$inputItem.datepicker("destroy");
            }
        }

        public componentWillReceiveProps(nextProps: IDatePickerProps): void {
            if (this.$inputItem !== undefined && this.props.value !== nextProps.value) {
                this.quietValue = true;

                if (nextProps.value === null || nextProps.value === undefined) {
                    this.$inputItem.datepicker("setDate", null);
                } else {
                    this.$inputItem.datepicker("setUTCDate", nextProps.value);
                }

                this.quietValue = false;
            }
        }

        public render(): JSX.Element {
            const valueAsString = this.props.value !== null && this.props.value !== undefined ? DateHelper.getDateAsInputValueString(this.props.value) : "";

            return (
                <input key={(this.props.value === null || this.props.value === undefined) ? "empty" : this.props.value.toDateString()} disabled={this.props.disabled} className={Dante.Utils.CombineClassNames(["form-datepicker", this.props.className])} ref={this.registerDatePicker.bind(this)} />
            );
        }
    }
}
