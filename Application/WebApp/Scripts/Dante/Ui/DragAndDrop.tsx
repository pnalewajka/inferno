﻿namespace Dante.Ui {
    export class Dropable extends React.Component<{ drop: (id: number) => void, canDrop?: (id: number) => boolean }, {}> {
        private dropElement: HTMLDivElement | null = null;
        private activClassName = "can-drop";
        private notActivClassName = "can-not-drop";

        public render(): JSX.Element {
            return (
                <div
                    ref={divElement => {
                        this.dropElement = divElement;
                        if (this.dropElement !== null) {
                            $(this.dropElement).droppable({
                                addClasses: false,
                                accept: ".drag",
                                hoverClass: "hovering",
                                activate: (event: dynamic, ui: dynamic) => {
                                    if (this.props.canDrop !== undefined) {
                                        const canContainerHandleDrop = this.props.canDrop(this.draggableElementId(ui));
                                        this.handleElement(element => {
                                            element.classList.add(canContainerHandleDrop ? this.activClassName : this.notActivClassName);
                                        });
                                    }
                                },
                                deactivate: () => {
                                    this.handleElement(element => {
                                        element.classList.remove(this.activClassName);
                                        element.classList.remove(this.notActivClassName);
                                    });
                                },
                                drop: (ev: dynamic, ui: dynamic) => {
                                    this.props.drop(this.draggableElementId(ui));
                                }
                            });
                        }
                    }}
                    className="dropable">
                    {this.props.children}
                </div>
            );
        }

        private handleElement(action: (element: HTMLElement) => void): void {
            if (this.dropElement !== null) {
                action(this.dropElement);
            }
        }

        private draggableElementId(eventUi: dynamic): number {
            return eventUi.draggable.data().id;
        }

        private forElement(action: (element: HTMLDivElement) => void) {
            if (this.dropElement !== null) {
                action(this.dropElement);
            }
        }
    }

    export class Dragable extends React.Component<{ dragItemId: () => number, disabled?: boolean }, {}> {
        private dragElement: HTMLDivElement | null = null;

        private get isEnabled(): boolean {
            return !this.props.disabled;
        }

        public render(): JSX.Element {
            return (
                <div
                    data-id={+this.props.dragItemId()}
                    className={Dante.Utils.ClassNames({ drag: this.isEnabled })}
                    ref={element => {
                        this.dragElement = element;
                        if (this.dragElement !== null) {
                            $(this.dragElement).draggable({
                                start: (e: Event, a: dynamic) => {
                                    const contextWidth = $(a.helper.context).width();
                                    if (contextWidth > 0) {
                                        $(a.helper[0]).width(contextWidth);
                                    }

                                    return this.isEnabled;
                                },
                                helper: "clone",
                                opacity: 0.95,
                                zIndex: 100
                            });
                        }
                    }} >
                    {this.props.children}
                </div>
            );
        }
    }
}
