﻿namespace Atomic.Ui {
    export interface IProgressBarProps {
        value: number;
    }

    export class ProgressBar extends React.Component<IProgressBarProps, {}> {
        public render(): JSX.Element {
            return (
                <div className="progress">
                    <div className="progress-bar progress-bar-striped" style={{ width: `${this.props.value}%` }}>
                        {this.props.children}
                    </div>
                </div>
            );
        }
    }
}
