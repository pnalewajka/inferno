﻿namespace Dante.Ui {
    interface IProgressIndicatorProps {
        repetitions?: number;
        startStep?: number;
        steps?: number;
        duration?: number;
    }

    export class ProgressIndicator extends React.Component<IProgressIndicatorProps, { cx: number, cy: number }>
    {
        private intervalHandler: number;
        private totalPathLength: number;
        private step: number;
        private repetitions: number;
        private motionPath: SVGPathElement;

        public static defaultProps: Partial<IProgressIndicatorProps> = {
            repetitions: 40,
            startStep: 55,
            steps: 220,
            duration: 2000,
        };

        constructor(props: IProgressIndicatorProps) {
            super(props);
            this.state = {
                cx: 50,
                cy: 13
            };
        }

        public render(): JSX.Element {
            return <div className="progress-indicator">
                <svg width="100" height="100">
                    <path id="motionPath" d="M 5 5 L 30 5 Q 55 5 55 30 Q 55 55 30 55 L 5 55 L 5 5z" fill="none" strokeWidth="1.5" stroke="#C00000"></path>
                    <path d="M 20 20 L 30 20 Q 40 20 40 30 Q 40 40 30 40 L 20 40 L 20 20" fill="#C00000"></path>
                    <circle cx={this.state.cx} cy={this.state.cy} r="3.5" fill="#C00000"></circle>
                </svg>
            </div>;
        }

        private generateNextFrame(): void {
            const point = this.motionPath!.getPointAtLength(this.totalPathLength * this.step / this.props.steps!);
            this.setState({ cx: point.x, cy: point.y });

            if (this.step++ > this.props.steps!) {
                this.step = 0;
            }

            if (this.step === this.props.startStep) {
                this.repetitions--;
            }

            if (this.repetitions === 0) {
                window.clearInterval(this.intervalHandler);
            }
        }

        public componentDidMount(): void {
            const motionPath = document.getElementById("motionPath") as SVGPathElement | null;

            if (motionPath === null) {
                throw new Error("Animation element: (motionPaht) not found");
            }

            this.motionPath = motionPath!;

            this.totalPathLength = this.motionPath.getTotalLength();
            this.step = this.props.startStep!;
            this.repetitions = this.props.repetitions!;

            this.intervalHandler = window.setInterval(() => this.generateNextFrame(), this.props.duration! / this.props.steps!);
        }

        public componentWillUnmount(): void {
            window.clearInterval(this.intervalHandler);
        }
    }
}
