﻿namespace Dante.Ui {
    export interface IOptionPickerOption {
        display: string;
        onClick: () => void;
    }

    export interface IOptionPickerProps extends IDisableable {
        options: IOptionPickerOption[];
        text?: string;
        tooltip?: string;
    }

    export class OptionPicker extends React.Component<IOptionPickerProps, {}>
    {
        private dropdownElement: HTMLDivElement | null = null;

        private checkIsDialogInViewPort = (event: React.MouseEvent<HTMLElement>) => {
            if (this.dropdownElement === null) {
                return;
            }

            const { pageX } = event;
            const element = $(this.dropdownElement);
            const dropdownWidth = element.width();
            const elementNotFullyInViewPort = pageX < dropdownWidth;

            if (elementNotFullyInViewPort) {
                element.addClass("pull-right");
            }
        }

        public render(): JSX.Element {
            return (
                <div className="dropdown" onClick={this.checkIsDialogInViewPort}>
                    <button
                        disabled={this.props.disabled}
                        title={this.props.tooltip}
                        className="btn dropdown-toggle btn-link btn-sm fa fa-bars"
                        type="button"
                        id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                        {
                            this.props.text === undefined
                                ? null
                                : <span>{this.props.text}</span>
                        }
                    </button>
                    <div ref={element => (this.dropdownElement = element)} className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        {
                            this.props.options.map((a, i) => (
                                <a
                                    key={i}
                                    className="dropdown-item"
                                    href="#"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        e.stopPropagation();
                                        a.onClick();
                                    }}>
                                {a.display}
                                </a>
                            ))
                        }
                    </div>
                </div>
            );
        }
    }
}
