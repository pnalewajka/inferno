﻿namespace Atomic.Ui {

    export type FileAddedFunction = (fileName: string, fileId: string, fileContent: File) => void;
    export type FileProgressFunction = (fileId: string, progress: number) => void;
    export type FileUploadedFunction = (fileId: string) => void;

    export interface IFileProps {
        saveUrl: string;
        dropMessage?: string;

        onFileAdded?: FileAddedFunction;
        onFileProgress?: FileProgressFunction;
        onFileUploaded?: FileUploadedFunction;
    }

    interface IFile extends File {
        name: string;
        size: number;
    }

    interface IFileFormData {
        temporaryDocumentId: string;
    }

    interface IFileEventData {
        submit(): void;
        files: IFile[];
        formData: IFileFormData;
        jqXHR: any;

        loaded: number;
        total: number;
    }

    var dropZoneId = "drop-file-zone";

    abstract class BaseFileComponent extends React.Component<IFileProps, {}> {

        protected getFileUploadSettings(): JQueryFileInputOptions {
            return {
                dataType: 'json',
                maxChunkSize: 2 * 1024,
                autoUpload: true,
                url: this.props.saveUrl,
                add: (e: Event, d: IFileEventData) => { this.onSubmit(e, d); },
                progress: (e: Event, d: IFileEventData) => { this.onProgress(d); },
                always: (e: Event, d: IFileEventData) => { this.onFileUploaded(d); }
            }
        }

        protected onSubmit(e: any, data: IFileEventData): boolean {
            var thisInstance = this;
            var [file] = data.files;

            $.post(Globals.resolveUrl("~/DocumentUpload/BeginUpload"), (result: IFileFormData) => {
                data.formData = result;
                if (this.props.onFileAdded !== undefined) {
                    this.props.onFileAdded(file.name, result.temporaryDocumentId, file);
                }

                this.upload(data);
            });

            return false;
        }

        protected triggerUpload($item: JQuery, data: IFileEventData): void {
            $item.fileupload('send', data);
        }

        protected abstract upload(data: IFileEventData): void;

        protected onProgress(data: IFileEventData): void {
            var progress = Math.round(data.loaded / data.total * 100);
            if (!!this.props.onFileProgress) {
                this.props.onFileProgress(data.formData.temporaryDocumentId, progress);
            }
        }

        protected onFileUploaded(data: IFileEventData): void {
            if (!!this.props.onFileUploaded) {
                this.props.onFileUploaded(data.formData.temporaryDocumentId);
            }
        }

        protected abstract attachFileZone(item: HTMLElement | null): void;
    }

    export class FileButton extends BaseFileComponent {
        private $inputContainer: JQuery | null;

        protected upload(data: IFileEventData): void {
            if (this.$inputContainer != null) {
                this.triggerUpload(this.$inputContainer, data);
            }
        }

        protected onSubmit(e: Event, data: IFileEventData): boolean {
            return super.onSubmit(e, data);
        }

        render() {
            return (
                <button className="btn btn-default file-input-button" onClick={this.handleButtonClick.bind(this)}>
                    <i className="fa fa-files-o"></i>
                    <span>{this.props.dropMessage}</span>
                    <input id="fileupload" type="file" name="files[]" multiple={true} ref={this.attachFileZone.bind(this)} />
                </button>
            );
        }

        handleButtonClick(): void {
            if (this.$inputContainer != null) {
                this.$inputContainer.trigger('click');
            }
        }

        attachFileZone(item: HTMLElement | null): void {
            if (item !== null) {
                var settings = super.getFileUploadSettings();
                settings.fileInput = item;
                var dropElement = document.getElementById(dropZoneId);

                if (dropElement !== null) {
                    settings.dropZone = dropElement;
                }

                this.$inputContainer = $(item).fileupload(settings);
            }
        }
    }

    export class FileDropZone extends React.Component<{ dropMessage: string }, {}> {
        private $dropZoneContainer: JQuery;

        private debounceHover = Dante.Utils.Debounce(() => {
            if (this.$dropZoneContainer !== null) {
                this.$dropZoneContainer.removeClass("file-over");
            }
        }, 100);

        private dropOver(e: React.DragEvent<HTMLDivElement>): void {
            if (this.$dropZoneContainer !== undefined) {
                if (!this.$dropZoneContainer.hasClass("file-over")) {
                    this.$dropZoneContainer.addClass("file-over")
                }

                this.debounceHover();
            }

            e.preventDefault();
        }

        protected attachFileZone(item: HTMLElement | null): void {
            if (item !== null && this.$dropZoneContainer === undefined) {
                this.$dropZoneContainer = $(item);
            }
        }

        public render(): JSX.Element {
            return (
                <div
                    id={dropZoneId}
                    className="file file-drop"
                    ref={this.attachFileZone.bind(this)}
                    onDragOver={this.dropOver.bind(this)}
                    onDrop={(e) => e.preventDefault()}>
                    <div className="file-drop-box">
                        <div className={"file-drop-box-input"}>
                            <label htmlFor="file"><strong>{this.props.dropMessage}</strong></label>
                        </div>
                    </div>
                    <div className="file-drop-content">{this.props.children}</div>
                </div>
            );
        }
    }
}