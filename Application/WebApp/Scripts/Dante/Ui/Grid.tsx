﻿namespace Atomic.Ui {

    export type ComponentResolver<TModel> = (cellContext: ICellContext<TModel>, model: TModel) => JSX.Element | string | null;
    export type CellRenderer<TModel> = (cellContext: ICellContext<TModel>, model: TModel) => JSX.Element;

    export enum GridRowMode {
        None = 0,
        Selected = 1 << 0,
        Hover = 1 << 1,
    }

    export interface ICellContext<TModel> {
        rowIndex: number;
        columnIndex: number;
        columnDefinition: IGridColumnDefinition<TModel>;
        rowState: GridRowMode;
    }

    export interface IRowData {
        rowIndex: number;
        rowState: GridRowMode;
    }

    export interface IGridCellDefinition<TModel> {
    }

    export interface IGridColumnDefinition<TModel> {
        label?: string | null;
        code: string;
        columnCssClass?: string | null;
        resolveComponent: string | ComponentResolver<TModel>;
    }

    interface IGridRowState {
        mode: GridRowMode;
    }

    export interface IGridRowProps<TModel> {
        row: TModel;
        rowIndex: number;
        renderCell: CellRenderer<TModel>;
        columnDefinitions: IGridColumnDefinition<TModel>[];
    }

    export interface IGridProps<TModel> {
        columns: IGridColumnDefinition<TModel>[];
        rows: TModel[];
        extraProps?: any;
    }

    export interface IGridState<TModel> {
        rows: TModel[];
    }

    export class GridRow extends React.Component<IGridRowProps<any>, IGridRowState> {
        constructor() {
            super();
            this.state = { mode: GridRowMode.None };
        }

        toggleFlag(flag: GridRowMode): void {
            if (this.state.mode & flag) {
                this.removeModeFlag(flag);
            } else {
                this.addModeFlag(flag);
            }
        }

        addModeFlag(flag: GridRowMode): void {
            this.setState({ mode: this.state.mode | flag });
        }

        removeModeFlag(flag: GridRowMode): void {
            var newMode = this.state.mode;
            newMode &= ~flag;
            this.setState({ mode: newMode });
        }

        public render(): JSX.Element {
            const rowData: IRowData = {
                rowIndex: this.props.rowIndex,
                rowState: this.state.mode
            };

            return (
                <tr
                    onClick={() => { this.toggleFlag(GridRowMode.Selected); }}
                    onMouseEnter={() => { this.addModeFlag(GridRowMode.Hover); }}
                    onMouseLeave={() => { this.removeModeFlag(GridRowMode.Hover | GridRowMode.Selected); }}>
                    {this.props.columnDefinitions.map((columnDefinition, cellIndex) => this.props.renderCell({ ...rowData, columnIndex: 1, columnDefinition: columnDefinition }, this.props.row))}
                </tr>
            );
        }
    }

    export class Grid<TModel> extends React.Component<IGridProps<TModel>, IGridState<TModel>> {
        protected renderHeaderCell(columnDefinition: IGridColumnDefinition<TModel>): JSX.Element {
            return <th key={columnDefinition.code} className="grid-column-header">{columnDefinition.label}</th>;
        }

        protected renderBodyCell: CellRenderer<TModel> = (context: ICellContext<TModel>, row: TModel): JSX.Element => {
            const resolveComponent = context.columnDefinition.resolveComponent;

            const cellClass = Dante.Utils.CombineClassNames([
                "grid-column",
                `grid-column-${context.columnDefinition.code}`,
                context.columnDefinition.columnCssClass,
            ]);

            if (typeof resolveComponent === "string") {
                var result = Utils.executeHandler(resolveComponent, { context, model: row });
                return <td key={context.columnDefinition.code} className={cellClass}>{result}</td>;
            } else if (typeof resolveComponent === "function") {
                var component = context.columnDefinition.resolveComponent as ComponentResolver<TModel>;

                return <td key={context.columnDefinition.code} className={cellClass}>{component(context, row)}</td>;
            }

            return this.emptyCell();
        }

        protected emptyCell = () => <span>&nbsp;</span>;

        protected renderHeader(columnDefinitions: IGridColumnDefinition<TModel>[]): JSX.Element {
            const hasEmptyHeader = columnDefinitions.every(c => c.label == null);

            return (
                hasEmptyHeader
                    ? <thead></thead>
                    : <thead>
                        <tr>
                            {columnDefinitions.map(this.renderHeaderCell)}
                        </tr>
                      </thead>
            );
        }

        protected renderRows(columnDefinitions: IGridColumnDefinition<TModel>[], rows: TModel[]): JSX.Element[] {
            return rows.map((row, index) => this.renderRow(columnDefinitions, row, index));
        }

        protected renderBody(columnDefinitions: IGridColumnDefinition<TModel>[], rows: TModel[]): JSX.Element {
            return (
                <tbody>
                    {this.renderRows(columnDefinitions, rows)}
                </tbody>
            );
        }

        protected renderRow(columnDefinitions: IGridColumnDefinition<TModel>[], row: TModel, rowIndex: number): JSX.Element {
            return (
                <GridRow key={rowIndex}
                    {...{
                        row, rowIndex,
                        columnDefinitions,
                        renderCell: this.renderBodyCell.bind(this)
                    }}
                />
            );
        }

        protected getCssClassName(): string {
            return "table table-striped table-bordered table-hover react-grid";
        }

        render() {
            return (
                <table className={this.getCssClassName()}>
                    {this.renderHeader(this.props.columns)}
                    {this.renderBody(this.props.columns, this.props.rows)}
                </table>
            );
        }
    }
}
