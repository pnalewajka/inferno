﻿namespace Dante.Ui {
    export interface IDisableable {
        disabled?: boolean;
    }

    export interface IEditableInputProps extends IDisableable {
        value: string;
        onChange: (newValue: string) => void;
        className?: string;
    }

    export interface IEditableInputNumberProps extends IDisableable {
        value: number | null;
        onChange: (newValue: number | null) => void;
    }

    export interface IEditableInputState {
        originalValue: string | null;
        value: string | null;
    }

    interface IBaseEditableValuePickerProps extends IDisableable {
        defaultText?: string;
        valuePickerId: string;
        // tslint:disable-next-line:no-any
        onChange: (model: any) => void;
        resolveUrl?: (url: string) => string;
        className?: string;
    }

    export interface IEditableSingleValuePickerProps extends IBaseEditableValuePickerProps {
        value: Utils.IKeyItem | null;
    }

    export interface IEditableMultipleValuePickerProps extends IBaseEditableValuePickerProps {
        values: Utils.IKeyItem[] | null;
    }

    export interface IEditableComponentProps extends IDisableable {
        renderDisplay: (setEditMode: (value: boolean) => void) => JSX.Element;
        renderEdit: (setEditMode: (value: boolean) => void) => JSX.Element;
    }

    export interface IEditableComponentState {
        isEditMode: boolean;
    }

    function storeParentWidth(element: EventTarget): void {
        const $parent = $(element).parent();
        const width = $parent.outerWidth();
        $parent.css({ maxWidth: width, minWidth: width, width });
    }

    export class EditableComponent
        extends React.Component<IEditableComponentProps, IEditableComponentState>
    {
        public constructor() {
            super();
            this.state = { isEditMode: false };
        }

        public render(): JSX.Element {
            if (this.state.isEditMode) {
                return this.props.renderEdit(this.setEditMode.bind(this));
            }

            return this.props.renderDisplay(this.setEditMode.bind(this));
        }

        private setEditMode(value: boolean): void {
            if (!this.props.disabled) {
                this.setState({ isEditMode: value });
            }
        }
    }

    abstract class BaseEditableValuePicker<P extends IBaseEditableValuePickerProps> extends React.Component<P, {}> {

        protected abstract openValuePicker(): void;

        protected abstract displayText(): string;

        public render(): JSX.Element {
            return (
                <div className={Dante.Utils.CombineClassNames(["row-line editable-valuepicker", this.props.className])} onClick={() => this.openValuePicker()}>
                    <span title={this.displayText()}>{this.displayText()}</span>
                    < i className="fa fa-search"></i>
                </div>
            );
        }
    }

    export class EditableMultipleValuePicker extends BaseEditableValuePicker<IEditableMultipleValuePickerProps> {
        protected openValuePicker(): void {
            if (!this.props.disabled) {

                const selectedIds = this.props.values !== null ? this.props.values.map(m => m.key) : [];

                const selectionManager = {
                    selection: new ValuePicker.SelectionManager(ValuePicker.SelectionMode.Multiple, selectedIds)
                };

                ValuePicker.selectMultiple(this.props.valuePickerId, (response: {}) => {
                    this.props.onChange(response);
                }, this.props.resolveUrl);
            }
        }

        protected displayText(): string {
            return !!this.props.values && this.props.values.length > 0 ? this.props.values.map(v => v.name).join(", ") : this.props.defaultText || "";
        }
    }

    export class EditableValuePicker extends BaseEditableValuePicker<IEditableSingleValuePickerProps> {

        protected openValuePicker(): void {
            if (!this.props.disabled) {
                const selectedIds = this.props.value !== null ? [this.props.value.key] : [];

                const selectionManager = {
                    selection: new ValuePicker.SelectionManager(ValuePicker.SelectionMode.SingleOrNone, selectedIds)
                };

                ValuePicker.selectSingle(this.props.valuePickerId, (response: {}) => {
                    this.props.onChange(response);
                }, this.props.resolveUrl, selectionManager);
            }
        }

        protected displayText(): string {
            return !!this.props.value ? this.props.value.name : this.props.defaultText || "";
        }
    }

    export class EditableInputComponent extends Dante.Components.StateComponent<IEditableInputProps, IEditableInputState> {
        private inputElement: HTMLInputElement | null = null;
        private setSelectionDebouncer = Dante.Utils.Debounce(() => {
            if (!!this.inputElement) {
                this.inputElement.setSelectionRange(0, this.inputElement.value.length);
            }
        });
        private renderDisplay(changeEditMode: (value: boolean) => void): JSX.Element {
            return (
                <div
                    title={this.props.value}
                    className={Dante.Utils.CombineClassNames(["editable-input-display", this.props.className])}
                    onClick={(e) => {
                        storeParentWidth(e.target);
                        changeEditMode(true);
                    }}>
                    {this.props.value}
                </div>);
        }

        private updateLocalValue(value: string): void {
            this.updateState({ value });
        }

        private closeEditMode(newValue: string, changeEditMode: (value: boolean) => void): void {
            if (newValue !== this.state.originalValue) {
                this.props.onChange(newValue);
                this.updateState({ value: newValue, originalValue: newValue });
            }

            changeEditMode(false);
        }

        private renderEdit(changeEditMode: (value: boolean) => void): JSX.Element {
            const newValue = this.state.value == null ? "" : this.state.value;

            return (
                <input
                    className="form-control editable-input-edit"
                    autoFocus={true}
                    type="text"
                    value={newValue}
                    ref={(item) => this.inputElement = item}
                    onFocus={this.setSelectionDebouncer}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.updateLocalValue(e.target.value);
                    }}
                    onKeyDown={(e) => { if (e.keyCode === 13) { this.closeEditMode(newValue, changeEditMode); } }}
                    onBlur={() => {
                        this.closeEditMode(newValue, changeEditMode);
                    }}
                />
            );
        }

        constructor() {
            super();
            this.state = { value: null, originalValue: null };
        }

        protected receiveProps(nextProps: IEditableInputProps): void {
            if (this.state.value !== null) {
                this.updateState({ originalValue: nextProps.value });
            } else {
                this.updateState({ value: nextProps.value, originalValue: nextProps.value });
            }
        }

        public render(): JSX.Element {
            return (<Dante.Ui.EditableComponent {...{
                disabled: this.props.disabled,
                renderDisplay: this.renderDisplay.bind(this),
                renderEdit: this.renderEdit.bind(this)
            }} />);
        }
    }

    export class EditableInputNumberComponent extends Dante.Components.StateComponent<IEditableInputNumberProps, {}> {
        public render(): JSX.Element {
            return (<EditableInputComponent disabled={this.props.disabled} value={this.parseValueToString(this.props.value)} onChange={(value) => { this.triggerChange(value); }} />);
        }

        private parseValueToString(value: number | null): string {
            return value === null ? "" : Dante.Utils.DisplayNumber(value);
        }

        private parseNumber(stringNumber: string): number | null {
            const options: (Array<(stringToParse: string) => number>) = [
                stringToParse => Number(stringToParse),
                stringToParse => Number(stringToParse.replace(/,/g, ".")),
                stringToParse => Number(stringToParse.replace(/./g, ","))
            ];

            let parsedHours: number | null = null;

            options.forEach(option => {
                const h = option(stringNumber);

                if (!isNaN(h)) {
                    parsedHours = h;
                }
            });

            return parsedHours;
        }

        private triggerChange(newValue: string): void {
            const numberValue = this.parseNumber(newValue.replace(/ /g, ""));

            if (numberValue !== null) {
                this.props.onChange(numberValue);
            } else {
                this.props.onChange(null);
            }
        }
    }

    export class EditableEnumDropdown extends React.Component<Dante.Ui.IEnumDropdownProps, {}> {

        protected renderDisplayText(): string | JSX.Element {
            const props: Dante.Ui.IEnumDropdownProps = this.props;

            return props.enumResources[props.value || 0];
        }

        private renderDisplay(changeEditMode: (value: boolean) => void): JSX.Element {
            return (
                <span
                    className={Dante.Utils.CombineClassNames(["editable-input-display", this.props.className])}
                    onClick={(e) => {
                        storeParentWidth(e.target);
                        changeEditMode(true);
                    }}>
                    {this.renderDisplayText()}
                </span>
            );
        }

        private renderEdit(changeEditMode: (value: boolean) => void): JSX.Element {
            return (
                <Dante.Ui.EnumDropdown {...this.props}
                    selectProps={{ className: Dante.Utils.CombineClassNames(["editable-input-edit", this.props.className]) }}
                    onChange={(value: number): void => {
                        if (this.props.value !== value) {
                            this.props.onChange(value);
                        }

                        changeEditMode(false);
                    }} />);
        }

        public render(): JSX.Element {
            return (
                <EditableComponent
                    disabled={this.props.disabled}
                    renderDisplay={this.renderDisplay.bind(this)}
                    renderEdit={this.renderEdit.bind(this)}
                />
            );
        }
    }

    export class EditableDropdown extends React.Component<Dante.Ui.IDropdownProps> {
        private renderDisplay(changeEditMode: (value: boolean) => void): JSX.Element {
            return (<span>{this.props.options[this.props.value as number || 0]}</span>);
        }

        private renderEdit(changeEditMode: (value: boolean) => void): JSX.Element {
            return (<Dante.Ui.Dropdown {...this.props} />);
        }

        public render(): JSX.Element {
            return (
                <EditableComponent
                    disabled={this.props.disabled}
                    renderDisplay={this.renderDisplay.bind(this)}
                    renderEdit={this.renderEdit.bind(this)}
                />
            );
        }
    }

    export class EditableDatePicker extends React.Component<Dante.Ui.IDatePickerProps, {}> {
        private getDisplayValue(): string {
            if (this.props.value === undefined) {
                return "";
            }

            const dateFormat = Globals.getDateFormat();
            const locale = Globals.getDatePickerLocale();

            return $.fn.datepicker.DPGlobal.formatDate(this.props.value, dateFormat, locale);
        }


        private renderDisplay(changeEditMode: (value: boolean) => void): JSX.Element {
            return (
                <div
                    className={Dante.Utils.CombineClassNames(["editable-input-display", this.props.className])}
                    onClick={(e) => { changeEditMode(true); }}>
                    {this.getDisplayValue()}
                </div>);
        }

        private renderEdit(changeEditMode: (value: boolean) => void): JSX.Element {
            return (<Dante.Ui.DatePicker {...this.props}
                onChange={(value: Date): void => {
                    if (this.props.value !== value) {
                        this.props.onChange(value);
                    }

                    changeEditMode(false);
                }}
            />);
        }

        public render(): JSX.Element {
            return (
                <Dante.Components.DatePickerLocaleInitialized>
                    <EditableComponent
                        disabled={this.props.disabled}
                        renderDisplay={this.renderDisplay.bind(this)}
                        renderEdit={this.renderEdit.bind(this)}
                    />
                </Dante.Components.DatePickerLocaleInitialized>
            );
        }
    }
}
