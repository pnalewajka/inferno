﻿namespace Dante.Ui {
    export interface IDialogAction {
        text: string;
        actionClass?: string;
        action: (dialog: DialogComponent) => Utils.NotObligatoryPromise<void>;
    }

    export class DialogComponent extends Dante.Components.StateComponent<{ title?: string, actions?: IDialogAction[] }, { isOpen: boolean }> {
        public constructor() {
            super({ isOpen: false });
        }

        private modalDiv: HTMLDivElement | null = null;

        public show(): Promise<void> {
            if (this.modalDiv != null) {
                const modal = $(this.modalDiv).modal();
                this.updateState({ isOpen: true });

                return new Promise<void>(resolve => {
                    modal
                        .on("hide.bs.modal", () => {
                            this.updateState({ isOpen: false });
                            resolve();
                        });
                });
            }

            return Promise.resolve<void>();
        }

        protected propsChanged(): boolean {
            return false;
        }

        public hide(): Promise<void> {
            this.updateState({ isOpen: false }, () => {
                if (this.modalDiv != null) {
                    $(this.modalDiv).modal("hide");
                }
            });

            return Promise.resolve<void>();
        }

        public render(): JSX.Element | null {
            const modalActions: IDialogAction[] = [
                ...this.props.actions || [DialogComponent.CancelButton()]
            ];

            return (
                <div className="modal common-dialog" ref={row => { if (row != null) { this.modalDiv = row; } }} onClick={($event) => { $event.stopPropagation(); }}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            {!!this.props.title
                                ? (
                                    <div className="modal-header">
                                        <h4 className="modal-title">{this.props.title}</h4>
                                    </div>
                                )
                                : null
                            }
                            <div className="modal-body">{this.state.isOpen ? this.props.children : null}</div>
                            {
                                modalActions.length > 0
                                    ? (
                                        <div className="modal-footer">
                                            {modalActions.map((a, index) => (
                                                <button
                                                    key={index}
                                                    type="button"
                                                    className={Dante.Utils.CombineClassNames(["btn", a.actionClass])}
                                                    onClick={() => { a.action(this); }}>
                                                    {a.text}
                                                </button>
                                            ))}
                                        </div>
                                    )
                                    : null
                            }
                        </div>
                    </div>
                </div>
            );
        }

        public static CancelButton(action?: (dialog: DialogComponent) => Utils.NotObligatoryPromise<void>): IDialogAction {
            return {
                text: Globals.resources.PromptDialogCancelButtonText,
                actionClass: "btn-default",
                action: (dialog: DialogComponent) => {
                    return Promise.resolve(action === undefined ? undefined : action(dialog)).then(() => {
                        return dialog.hide();
                    });
                }
            };
        }

        public static OkButton(action?: (dialog: DialogComponent) => Utils.NotObligatoryPromise<void>): IDialogAction {
            return {
                text: Globals.resources.PromptDialogOkButtonText,
                actionClass: "btn-primary",
                action: (dialog: DialogComponent) => {
                    return Promise.resolve(action === undefined ? undefined : action(dialog)).then(() => {
                        return dialog.hide();
                    });
                }
            };
        }

        public static SubmitButton(action?: (dialog: DialogComponent) => Utils.NotObligatoryPromise<void>): IDialogAction {
            return DialogComponent.OkButton((dialog: DialogComponent) => {
                return Promise.resolve(action === undefined ? undefined : action(dialog)).then(() => {
                    if (dialog.modalDiv === null) {
                        return Promise.reject("No modal dialog");
                    }

                    const $modelForm = $("form", dialog.modalDiv);

                    if ($modelForm.length !== 1) {
                        return Promise.reject("No modal dialog form to submit");
                    }

                    CommonDialogs.pleaseWait.show();
                    $modelForm.submit();

                    return Promise.resolve();
                });
            });
        }
    }
}
