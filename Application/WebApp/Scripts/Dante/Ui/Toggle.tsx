﻿namespace Atomic.Ui {

    export var ToggleButton: React.StatelessComponent<{ checked: boolean, onChanged: (value: boolean) => void, disabled?: boolean }> = function (props): JSX.Element {
        return (
            <label className="toggle-button">
                <input type="checkbox" disabled={props.disabled} checked={props.checked} onChange={(e) => { props.onChanged(e.target.value !== "on"); }} />
                <span className="slider round">
                </span>
            </label>
        );
    }
}