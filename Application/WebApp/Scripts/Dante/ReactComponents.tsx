﻿namespace Dante.Components {

    export abstract class StateComponent<P, S> extends React.Component<P, S> {
        protected constructor(defaultState?: S) {
            super();

            if (defaultState !== undefined) {
                this.state = defaultState;
            }
        }

        protected receiveProps(newProps: P): void {
            /* no action by default */
        }

        protected updateState(nextStatePartial: Partial<S>, callback?: () => void): void {
            const newState = Dante.Utils.Assign(this.state, nextStatePartial);

            if (this.stateChanged(newState)) {
                this.setState(newState, callback);
            } else if (callback !== undefined) {
                callback();
            }
        }

        protected propsChanged(nextProps: P): boolean {
            return JSON.stringify(nextProps) !== JSON.stringify(this.props);
        }

        protected stateChanged(nextState: S): boolean {
            return JSON.stringify(nextState) !== JSON.stringify(this.state);
        }

        public shouldComponentUpdate(nextProps: P, nextState: S): boolean {
            return this.propsChanged(nextProps) || this.stateChanged(nextState);
        }

        public componentWillMount(): void {
            this.receiveProps(this.props);
        }

        public componentWillReceiveProps(nextProps: P): void {
            this.receiveProps(nextProps);
        }
    }

    export abstract class StoreComponent<R, P, S> extends StateComponent<P, S> {
        private storeUnsubscriber: Redux.Unsubscribe | null = null;
        protected store: Redux.Store<R>;

        constructor(store: Redux.Store<R>) {
            super();
            this.store = store;
            this.state = {} as S;
        }

        public componentWillMount(): void {
            super.componentWillMount();
            this.storeUnsubscriber = this.store.subscribe(this.reciveStateFromStore.bind(this));
            this.reciveStateFromStore();
        }

        public componentWillUnmount(): void {
            if (this.storeUnsubscriber != null) {
                this.storeUnsubscriber();
            }

            this.storeUnsubscriber = null;
        }

        private reciveStateFromStore(): void {
            if (!!this.storeUnsubscriber) {
                this.updateFromStore(this.store.getState());
            }
        }

        protected abstract updateFromStore(state: R): void;

        protected dispatchToStore(action: Redux.Action): void {
            this.store.dispatch(action);
        }
    }

    export class DatePickerLocaleInitialized extends StateComponent<{}, { isInitialized: boolean }> {
        public constructor() {
            super({ isInitialized: false });
        }

        protected propsChanged(nextProps: {}): boolean {
            return false;
        }

        protected stateChanged(nextState: {}): boolean {
            return true;
        }

        public componentWillMount(): void {
            Forms.initLocalizedDatePicker().then(() => {
                this.updateState({ isInitialized: true });
            });
        }

        public render(): JSX.Element | null {
            if (!this.state.isInitialized) {
                return null;
            }

            return this.props.children as JSX.Element;
        }
    }
}
