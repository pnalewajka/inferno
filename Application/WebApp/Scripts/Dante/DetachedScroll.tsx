﻿namespace Dante.DetachedScroll {
    export type ScrollPoint = { horizontal: number, vertical: number };
    export const ScrollEvent: Atomic.Events.EventDispatcher<ScrollPoint> = new Atomic.Events.EventDispatcher<ScrollPoint>();

    interface IState {
        containerHeight: number;
        containerWidth: number;
        headerHeight: number;
        columnWidth: number;
        dataWidth: number;
        dataHeight: number;
    }

    export interface IProps {
        dispacher?: Atomic.Events.EventDispatcher<any>;
    }

    export abstract class Component extends React.Component<IProps, IState>{

        protected contentElement: HTMLDivElement | null;
        protected handlerElement: HTMLDivElement | null;

        debounceSetState = Utils.Debounce(() => {
            const $globalContainer = $(".content.container-fluid");

            const containerHeight = $globalContainer.height();
            const containerWidth = $globalContainer.width();

            const headerHeight = this.globalHeaderHeight();
            const columnWidth = this.globalColumnWidth();
            const dataWidth = this.dataWidth();
            const dataHeight = this.dataHeight();

            this.setState({
                containerHeight,
                containerWidth,
                headerHeight,
                columnWidth,
                dataWidth,
                dataHeight,
            });
        }, 1);

        debounceScroll = Utils.Debounce(() => {

            if (this.handlerElement == null || this.contentElement == null) {
                return;
            }

            const { scrollLeft, scrollTop } = this.handlerElement;

            this.contentElement.scrollLeft = scrollLeft;
            this.contentElement.scrollTop = scrollTop;

            const verticalScrollClassName = "vertical-scroll";
            const shadowMargin = 5;
            const $contentElement = $(this.contentElement);


            if (scrollLeft > shadowMargin && !$contentElement.hasClass(verticalScrollClassName)) {
                $contentElement.addClass(verticalScrollClassName);
            } else if (scrollLeft < shadowMargin && $contentElement.hasClass(verticalScrollClassName)) {
                $contentElement.removeClass(verticalScrollClassName);
            }


            Dante.Utils.StaticCss(this.verticalElementsToScroll(), { transform: `translate(0, ${scrollTop}px)` });
            Dante.Utils.StaticCss(this.horizontalElementsToScroll(), { transform: `translate(${scrollLeft - 1}px, 0)` });
            ScrollEvent.dispatch({
                horizontal: scrollLeft,
                vertical: scrollTop,
            });
        }, 5);

        constructor() {
            super();
            this.state = {
                containerHeight: 0,
                containerWidth: 0,
                headerHeight: 0,
                columnWidth: 0,
                dataWidth: 0,
                dataHeight: 0
            };
        }

        protected globalContainerHeight(): number {
            return $(".content.container-fluid").height();
        }

        protected globalContainerWidth(): number {
            return $(".content.container-fluid").width();
        }

        protected abstract globalHeaderHeight(): number;
        protected abstract globalColumnWidth(): number;
        protected abstract dataWidth(): number;
        protected abstract dataHeight(): number;

        protected abstract horizontalElementsToScroll(): string;
        protected abstract verticalElementsToScroll(): string;

        public componentDidMount(): void {
            this.debounceSetState();
            window.addEventListener("resize", this.debounceSetState);
            if (this.props.dispacher !== undefined) {
                this.props.dispacher.subscribe(() => { this.debounceSetState(); });
            }
        }

        public componentWillUnmount(): void {
            if (this.props.dispacher !== undefined) {
                this.props.dispacher.unsubscribe(this.debounceSetState);
            }
        }

        attachWheelScroll(container: HTMLDivElement | null) {
            if (container !== null) {
                this.contentElement = container;

                $(this.contentElement).bind("mousewheel DOMMouseScroll", (event) => {
                    if (this.handlerElement !== null) {
                        const scrollStep = 30;
                        const { scrollTop } = this.handlerElement;
                        const direction = ((event.originalEvent as any).wheelDelta || (event.originalEvent as any).detail * -1) < 0 ? scrollStep : -scrollStep;
                        this.handlerElement.scrollTop = scrollTop + direction;
                    }
                });
            }
        }

        render() {

            const contentWidth = this.state.containerWidth - 20;
            const contentHeight = this.state.containerHeight - 17;

            const scrollWidth = this.state.containerWidth - this.state.columnWidth;
            const scrollHeight = this.state.containerHeight - this.state.headerHeight;

            const horizontalScroll = this.state.dataWidth - this.state.containerWidth + scrollWidth;
            const verticalScroll = this.state.dataHeight - this.state.containerHeight + scrollHeight + this.state.headerHeight;

            const contentCss: React.CSSProperties = { width: contentWidth, height: contentHeight };
            const handlerCss: React.CSSProperties = { height: scrollHeight, width: scrollWidth };

            const dataCss: React.CSSProperties = {
                width: horizontalScroll,
                height: verticalScroll
            };

            const shadowCss: React.CSSProperties = {
                height: this.state.dataHeight,
                top: -this.state.dataHeight,
                left: this.state.columnWidth - 22
            };

            return (
                <div className="detached-scroll">
                    <div className="detached-scroll-content" ref={this.attachWheelScroll.bind(this)} style={contentCss}>
                        {this.props.children}
                        <div style={shadowCss} className="detached-scroll-content-shadow"></div>
                    </div>
                    <div className="detached-scroll-handler" ref={item => this.handlerElement = item} onScroll={this.debounceScroll} style={handlerCss}>
                        <div style={dataCss}></div>
                    </div>
                </div>
            );
        }
    }
}
