﻿namespace UserEmployeeHub {
    export function expandProfileDetails() {
        $(".profile-details").toggleClass("hide");
        $(".show-profile-details i").toggleClass("fa-angle-up fa-angle-down");
    }
}
