﻿namespace Dante.Mentions {
    interface IValuePickerResult {
        Id: number;
        DisplayName: string;
    }

    export function CustomMentionValuePicker(listId: string, name: string, iconClass: string, urlParams?: UrlHelper.UrlParameter[] | null): Atomic.Ckeditor.MentionPlugin.IMentionPluginParameters {
        const urlParamsAsString = urlParams ? `&${UrlHelper.joinUrlParams(urlParams)}` : ``;

        const params: Atomic.Ckeditor.MentionPlugin.IMentionPluginParameters = {
            symbol: "@",
            name: "custom-mention",
            label: name,
            iconClass,
            getElement: (selectedId?: number): Promise<Atomic.Ckeditor.MentionPlugin.IMentionItem> => {
                return new Promise<Atomic.Ckeditor.MentionPlugin.IMentionItem>(resolve => {
                    ValuePicker.selectSingle(listId, (row: Grid.IRow) => {
                        resolve({ Id: row.id, Label: row.toString, SourceName: listId });
                    }, (url) => url += urlParamsAsString);
                });
            },
            searchElements: (searchKey: string): Promise<Atomic.Ckeditor.MentionPlugin.IMentionItem[]> => {
                const urlParamsAsJson = urlParams ? urlParams.reduce((obj: Dante.Utils.IDictionary<string>, item) => {
                    obj[item.name] = item.value;

                    return obj;
                }, {}) : {} as Dante.Utils.IDictionary<string>;

                urlParamsAsJson[`listId`] = listId;
                urlParamsAsJson[`search`] = searchKey;

                return new Promise<Atomic.Ckeditor.MentionPlugin.IMentionItem[]>(resolve => {
                    $.get(Globals.resolveUrl(`~/ValuePicker/JsonList`), urlParamsAsJson)
                        .then((rows: IValuePickerResult[]) => {
                            resolve(rows.map(row => {
                                const item: Atomic.Ckeditor.MentionPlugin.IMentionItem = {
                                    Id: row.Id,
                                    Label: row.DisplayName,
                                    SourceName: listId
                                };

                                return item;
                            }));
                        });
                });
            }
        };

        return params;
    }

    export function EmployeeMentionValuePicker(args?: { name?: string, mode?: string }): Atomic.Ckeditor.MentionPlugin.IMentionPluginParameters {
        const params: UrlHelper.UrlParameter[] | null = args && args.mode ? [new UrlHelper.UrlParameter("mode", args.mode)] : null;
        const customMention = CustomMentionValuePicker("EmployeeValuePickers.Employee", args && args.name ? args.name : "", "employees_icon", params);
        customMention.name = "employee-mention";

        customMention.resolveListTemplate = (item) => {
            return `<div class="at-mention employee-mention"><span class="mention-icon" style="background-image: url('/Accounts/UserPicture/DownloadByEmployee?employeeId=${item.Id}&userPictureSize=Small')">&nbsp;</span><span>${item.Label}</span></div>`;
        };

        return customMention;
    }
}
