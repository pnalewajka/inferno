﻿namespace Dante.ReactHelper
{
    interface JsxClass<P, S> extends React.Component<P, S>
    {
        render(): React.ReactElement<P>
    }

    export interface IReactConstructor<P, S>
    {
        new (props: P): JsxClass<P, S>;
    }
}
