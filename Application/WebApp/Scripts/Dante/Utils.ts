﻿namespace Dante.Utils {
    export type NotObligatoryPromise<T> = Promise<T> | undefined;

    export interface IDictionary<T> {
        [key: string]: T;
    }

    export interface IKeyItem {
        key: number;
        name: string;
    }

    export interface IIndexDictionary<T> {
        [key: number]: T;
    }

    export function Clone<T>(obj: T): T {

        if (obj == null
            || typeof obj === "boolean"
            || typeof obj === "number"
            || typeof obj === "string"
            || obj instanceof Date) {
            return obj;
        }

        // Ivan did this :)
        var copy = (obj instanceof Array ? [] : {}) as T;
        (copy as any).__proto__ = (obj as any).__proto__;

        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = Clone(obj[attr]);
            }
        }

        return copy;
    }

    export function Assign<T>(source: T, options: Partial<T>, ...extraOptions: Array<Partial<T>>): T {
        return $.extend(Clone(source), options, ...extraOptions) as T;
    }

    export function SelectMany<T, R>(collection: T[], mapper: (item: T) => R[]): R[] {
        let result: R[] = [];
        for (let index = 0; index < collection.length; index++) {
            result = [...result, ...mapper(collection[index])]
        }

        return result;
    }

    export function Range(size: number): number[] {
        const range: number[] = [];

        for (let i = 0; i < size; i++) {
            range.push(i);
        }

        return range;
    }

    export function Enumerate<T>(source: IDictionary<T>): T[] {
        let outputArray: T[] = [];

        for (var item in source) {
            const arrayItem = source[item];
            if (!!arrayItem) {
                outputArray.push(arrayItem);
            }
        }

        return outputArray;
    }

    export function CombineClassNames(classNames: Array<string | null | undefined>): string {
        return classNames.filter(c => !!c).join(' ');
    }

    export function ClassNames(input: { [name: string]: boolean }, defaultClasses?: string | string[]): string {
        let outputClass: string[] = !defaultClasses
            ? []
            : defaultClasses instanceof Array
                ? [...defaultClasses]
                : [defaultClasses];

        for (var item in input) {
            if (input[item]) {
                outputClass.push(item);
            }
        }

        return outputClass.join(' ');
    }

    export function AreEqual<T>(obj1: T, obj2: T): boolean {
        return JSON.stringify(obj1) === JSON.stringify(obj2);
    }

    export function Debounce(action: () => void, delay = 0): () => void {
        return Atomic.Debouncer.basic(action, delay);
    }

    export function GroupBy<T>(collection: T[], resolve: (item: T) => number): Array<T[]> {
        var group = collection.reduce<any>((groups, item) => {
            const value = resolve(item);

            groups[value] = [...(groups[value] || []), item];

            return groups;
        }, {});

        return Enumerate<T[]>(group);
    }

    export function Keys(object: any): string[] {
        let result: string[] = [];
        for (var name in object) {
            if (object.hasOwnProperty(name)) {
                result.push(name);
            }
        }

        return result;
    }

    export function Distinct<T>(collection: T[], resolve?: (a: T) => any): Array<T> {
        return collection.filter((item, index, array) => {
            return array.map(i => resolve === undefined ? i : resolve(i)).indexOf(resolve === undefined ? item : resolve(item)) === index;
        });
    }

    export function DisplayNumber(value: number): string {
        if (value === 0) {
            return "0";
        }

        return (+(Math.round(value * 100) / 100).toFixed(2)).toString();
    }

    export function Lazy<T>(factory: () => T): () => T {
        let instance: T;

        return () => instance !== undefined
            ? instance
            : (instance = factory());
    }

    export function StaticCss(cssSelector: string, cssProperties: React.CSSProperties): string {
        const dataKey = "stored-css";
        const selectorId = cssSelector.replace(/\./g, '').replace(/\,/g, '').split(' ').join('');

        const storedCss = $(`[id="${selectorId}"]`).data(dataKey) || {};
        const toStoreCss = Assign(storedCss, cssProperties);

        let styles: string[] = [];
        for (var key in toStoreCss) {
            const keyValue = toStoreCss[key];
            const keyName = key.replace(/\.?([A-Z])/g, function (x, y) { return "-" + y.toLowerCase() }).replace(/^-/, "");

            styles.push(`${keyName}: ${keyValue}${isNaN(keyValue) ? '' : 'px'}`);
        }

        const css = `${cssSelector} { ${styles.join('; ')} }`;

        let $style = setStaticCss(selectorId, css);
        $style.data(dataKey, toStoreCss);

        return css;
    }

    function setStaticCss(id: string, css: string): JQuery {
        let $style = $(`style[id="${id}"]`);

        if ($style.length > 0) {
            $style.text(css);
        } else {
            $style = $(`<style type="text/css" id="${id}"></style>`).append(document.createTextNode(css));
            $('head').append($style);
        }

        return $style;
    }

    function camelCaseReviver(key: string, value: { [key: string]: object }) {
        if (value && typeof value === 'object') {
            for (var k in value) {
                if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
                    value[k.charAt(0).toLowerCase() + k.substring(1)] = value[k];
                    delete value[k];
                }
            }
        }

        return value;
    }

    export function parseJson<T>(jsonString: string): T {
        return JSON.parse(jsonString, camelCaseReviver) as T;
    }

    export function ItemAsArray<T>(item: T | T[]): T[] {
        return Array.isArray(item)
            ? [...item as T[]]
            : [item as T];
    }

    export function ElementToHtml(element: JSX.Element): string {
        const $container = $("<div></div>");
        ReactDOM.render(element, $container[0]);

        return $container.html();
    }

    export const swapElements = <T>(arr: Array<T>, fromIndex: number, toIndex: number) => {
        let copy = arr.concat([]);
        const temp = copy[fromIndex];
        copy[fromIndex] = copy[toIndex];
        copy[toIndex] = temp;

        return copy;
    }

    export function findIndex<T>(array: T[], predicate: (e: T) => boolean): number | undefined {
        for (let i = 0; i < array.length; ++i) {
            if (predicate(array[i])) {
                return i;
            }
        }

        return undefined;
    }

    export function ToPascalSearchUrl(form: JQuery): string {
        return $(".form-group input[name][value]", form).get().reduce((previousValue, currentValue) => {
            const $item = $(currentValue);

            return `${previousValue}&${$item.attr("name")}=${$item.val()}`;
        }, "").substr(1);
    }

    export function getEndOfMonth(date: Date = new Date()): Date {
        return toUTCDate(new Date(date.getFullYear(), date.getMonth() + 1, 0));
    }

    export function getEndOfWeek(date: Date = new Date()): Date {
        const difference = date.getDate() - date.getDay() + 5;
        
        const newDate = new Date(date.setDate(difference));

        return toUTCDate(newDate);
    }

    export function toUTCDate(date: Date): Date {
        return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(),
            date.getHours(), date.getMinutes(), date.getSeconds()));
    }

    export function daysBetween(dateA: Date, dateB: Date): number {
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

        return Math.round((dateA.getTime() - dateB.getTime()) / (oneDay));
    }
}
