﻿/// <reference path="ClientSideOperations.ts"/>
namespace OnValueChange
{
    type ServerResultData = dynamic;

    class ServerResponse
    {
        public operations: ClientSideOperations.IClientSideOperation[];
        public serverResultData: ServerResultData;
    }

    class OnValueChangeDescriptor
    {
        public enabled: boolean;
        public clientHandler?: string;
        public serverHandler?: string;
        public serverResultClientHandler?: string;
        public executeOnLoad: boolean;

        public hasEventsAttached: boolean;
    }

    function getDescriptor(jElement: JQuery): OnValueChangeDescriptor
    {
        const descriptor = new OnValueChangeDescriptor();

        descriptor.enabled = jElement.data("onvaluechange-enabled") || undefined;
        descriptor.clientHandler = jElement.data("onvaluechange-clienthandler") || undefined;
        descriptor.serverHandler = jElement.data("onvaluechange-serverhandler") || undefined;
        descriptor.serverResultClientHandler = jElement.data("onvaluechange-serverresultclienthandler") || undefined;
        descriptor.executeOnLoad = jElement.data("onvaluechange-executeonload") || false;

        descriptor.hasEventsAttached = jElement.data("onvaluechange-events-attached") || false;

        return descriptor;
    }

    function getFormData(jElement: JQuery)
    {
        const formId = jElement.attr("form");
        const form = formId ? $(`form[id='${formId}']`) : jElement.closest("form");

        if (form.length === 0)
        {
            return { };
        }

        const data = form.serialize();

        return data;
    }

    export function attachOnChangedEvent(): void
    {
        $("[data-onvaluechange-enabled]").each((i, element) =>
        {
            const jElement = $(element);
            const descriptor = getDescriptor(jElement);

            if (!descriptor.hasEventsAttached)
            {
                jElement.data("onvaluechange-events-attached", true);

                // ReSharper disable once CreateSpecializedOverload
                jElement.on("change paste keyup", (eventObject) =>
                {
                    // Avoid firing event if value haven't changed
                    if (!jElement.is(":radio") && !jElement.is(":checkbox") && !jElement.is(".repeater"))
                    {
                        if (jElement.data("last-value") === jElement.val())
                        {
                            return;
                        }

                        jElement.data("last-value", jElement.val());
                    }

                    // Call handlers
                    const currentDescriptor = getDescriptor(jElement);

                    if (!currentDescriptor.enabled)
                    {
                        return;
                    }

                    if (currentDescriptor.clientHandler !== undefined)
                    {
                        Utils.executeHandler(currentDescriptor.clientHandler, { eventObject, jElement });
                    }

                    if (currentDescriptor.serverHandler !== undefined)
                    {
                        const dataToSend = getFormData(jElement);

                        $.post(currentDescriptor.serverHandler, dataToSend, (response: ServerResponse, status: string) =>
                        {
                            if (status === "success")
                            {
                                for (const operation of response.operations)
                                {
                                    ClientSideOperations.fromJson(operation).apply();
                                }

                                if (currentDescriptor.serverResultClientHandler !== undefined)
                                {
                                    const data = response.serverResultData;
                                    Utils.executeHandler(currentDescriptor.serverResultClientHandler, { data, jElement });
                                }
                            }
                        });
                    }
                });
            }

            if (descriptor.executeOnLoad)
            {
                jElement.trigger("change");
            }
        });
    }
}
