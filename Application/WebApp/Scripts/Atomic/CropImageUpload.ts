﻿module DocumentUpload {
    export class CropImageUpload extends DocumentPicker {
        private maxPreviewImageDimension: number;

        private propertyName: string;
        private viewModelIdentifier: string;
        private documentPickerItem: JQuery;

        constructor(elem: Element) {
            super(elem, '~/DocumentUpload/UploadImage');
            this.propertyName = $(elem).attr('data-property-name');
            this.viewModelIdentifier = $(elem).attr('data-viewmodel-identifier');

            this.maxPreviewImageDimension = Globals.getParameterAsNumber("System.DocumentUpload.MaxPreviewImageDimension");

            this.processImagePreviews();
        }

        private processImagePreviews(): void {
            var imagePreviewItems = $(this.container).find('img[data-link]');

            if (imagePreviewItems.length === 0) {
                return;
            }

            //we handle only one preview of picture
            var imagePreview = imagePreviewItems.first();

            imagePreview.load(() => {
                var htmlImage: any = imagePreviewItems[0];
                var imageSize = new Controls.ImageSize(htmlImage.width, htmlImage.height);
                var scaledImageSize = imageSize.getScaledSize(this.maxPreviewImageDimension);

                imagePreview.width(scaledImageSize.width);
                imagePreview.height(scaledImageSize.height);

                var parentDiv = imagePreview.closest('div');
                parentDiv.width(scaledImageSize.width);
                parentDiv.height(scaledImageSize.height);

                imagePreview.removeClass('collapse');
            });

            //load image into hidden img element, so that we can work on its size without breaking layout
            imagePreview.attr('src', this.documentDownloadPath + imagePreview.attr('data-link'));
        }

        public onUploadCompleted(documentPickerItem: JQuery, uploadedImage: DocumentDescriptor): void {
            this.documentPickerItem = documentPickerItem;
            super.onUploadCompleted(documentPickerItem, uploadedImage);
            this.addImagePreview(documentPickerItem, <ImageDescriptor>uploadedImage);
            this.attachCropCommand(documentPickerItem);
        }

        private attachCropCommand(documentPickerItem: JQuery): void {
            var thisComponent = this;
            documentPickerItem.find('i.document-picker-crop-button').off('click');
            documentPickerItem.find('i.document-picker-crop-button').click(() => {
                var temporaryDocumentHiddenInput = documentPickerItem.find("input[name$='.TemporaryDocumentId']").first();
                var temporaryDocumentId = temporaryDocumentHiddenInput.val();

                var postData = {
                    temporaryDocumentId: temporaryDocumentId,
                    viewModelIdentifier: thisComponent.viewModelIdentifier,
                    propertyName: thisComponent.propertyName
                };

                Dialogs.showDialog({
                    actionUrl: "~/DocumentUpload/ShowCropImageDialog", 
                    formData: postData,
                    eventHandler: eventArgs => {
                        if (eventArgs.isOk) {
                            var imageDescriptor = <ImageDescriptor>eventArgs.model;
                            this.updateTemporaryDocumentIdReferences(imageDescriptor.temporaryDocumentId);
                            this.addImagePreview(this.documentPickerItem, imageDescriptor);
                            this.attachCropCommand(this.documentPickerItem);
                            this.updateTemporaryDocumentUploadLink(this.documentPickerItem, imageDescriptor);
                        }
                        CommonDialogs.pleaseWait.hide();
                    }, 
                    onBeforeSubmit: (dialog) => {
                        var cropImageControl = <CropImage>Controls.getAllInstances($(dialog)).filter((v) => {
                            return (v instanceof CropImage);
                        })[0];
                        var values = cropImageControl.getCropInfo();
                        $(dialog).setInputValue('CropPositionLeft', Math.round(values.cropPositionLeft));
                        $(dialog).setInputValue('CropPositionTop', Math.round(values.cropPositionTop));
                        $(dialog).setInputValue('CropSizeWidth', Math.round(values.cropWidth));
                        $(dialog).setInputValue('CropSizeHeight', Math.round(values.cropHeight));
                        $(dialog).setInputValue('PropertyName', this.propertyName);
                        $(dialog).setInputValue('ViewModelIdentifier', this.viewModelIdentifier);
                    }
                });
            });            
        }

        private updateTemporaryDocumentUploadLink(documentPickerItem: JQuery, file: DocumentDescriptor): void {
            var documentDownloadLink = this.temporaryDocumentDownloadPath + file.temporaryDocumentId;
            var fileNameElement = documentPickerItem.find('a.document-picker-file-name').first();

            if (fileNameElement.length > 0) {
                fileNameElement.attr('href', documentDownloadLink);
            }
        }

        public getDocumentDescriptor(jsonObject: any): DocumentDescriptor {
            var imageDescriptor = new ImageDescriptor();
            Utils.populateFrom(imageDescriptor, jsonObject);

            return imageDescriptor;
        }

        private addImagePreview(documentPickerItem: JQuery, uploadedImage: ImageDescriptor): void {
            var fileNameElement = documentPickerItem.find(".document-picker-file-name").first();
            var downloadPath = this.temporaryDocumentDownloadPath + uploadedImage.temporaryDocumentId;
            var imageSize = new Controls.ImageSize(uploadedImage.width, uploadedImage.height);
            var scaledImageSize = imageSize.getScaledSize(this.maxPreviewImageDimension);

            fileNameElement.replaceWith(
                '<div class="preview-image preview-image-center document-picker-file-name" style= "width: '
                + scaledImageSize.width + 'px; height: '
                + scaledImageSize.height + 'px;" ><img src="'
                + downloadPath + '" width="'
                + scaledImageSize.width + '" height="'
                + scaledImageSize.height + '" /> <i class="'
                + Utils.getIconClass('glyphicon-edit') + ' document-picker-crop-button image-crop-edit"></i></div>' + "<div style='text-align: center' class='document-picker-file-name'>"
                + fileNameElement.text() + "</div>");
        }

        private updateTemporaryDocumentIdReferences(temporaryDocumentId: string): void {
            var oldTemporaryDocumentId = this.documentPickerItem.attr('data-id');

            this.container.find("input[name$='.TemporaryDocumentId']").each((index, element) => {
                if ($(element).val() === oldTemporaryDocumentId) {
                    $(element).val(temporaryDocumentId);
                }
            });

            this.documentPickerItem.attr('data-id', temporaryDocumentId);
        }
    }

    export class ImageDescriptor extends DocumentDescriptor{
        public width: number;
        public height: number;
    }
}
