﻿module JQueryExtensions {
    // Find specific input in current context (jquery scope), then set it's value to chosen one
    export function setInputValue(context: JQuery, inputName: string, inputValue: any) {
        context.find('input[name="' + inputName + '"]').val(inputValue);
    }

    export function isVisibleOnScreen($element: JQuery): boolean {
        const $window = $(window);

        var viewport = { top: 0, bottom: 0 };
        viewport.top = $window.scrollTop();
        viewport.bottom = viewport.top + $window.height();

        var bounds = { top: 0, bottom: 0 };
        bounds.top = $element.offset().top;
        bounds.bottom = bounds.top + $element.outerHeight();

        return bounds.top <= viewport.bottom && bounds.bottom >= viewport.top;
    };
}

 (function($) {
     $.fn.setInputValue = function (inputName: string, inputValue: any) {
         JQueryExtensions.setInputValue(this, inputName, inputValue);
     };

     $.fn.isVisibleOnScreen = function(): boolean {
         return JQueryExtensions.isVisibleOnScreen($(this));
     };
 })(jQuery);
 