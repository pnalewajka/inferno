﻿module ClientStorage {
    var clientStorageKey = "AtomicClientStorage";

    export enum PersistenceLevel {
        Session,
        Page
    }

    function getClientStorage(): any {
        return window.hasOwnProperty(clientStorageKey)
            ? (window as any)[clientStorageKey]
            : ((window as any)[clientStorageKey] = [])
    }

    function setPageValue(key: string, value: any) {
        getClientStorage()[key] = value;
    }

    function getPageValue(key: string) {
        return getClientStorage()[key];
    }

    export function setObject(key: string, value: any, persistence: PersistenceLevel = PersistenceLevel.Session) {
        if (persistence === PersistenceLevel.Page) {
            setPageValue(key, value);
            return;
        }

        if (BrowserSurvey.isSessionStorageSupported()) {
            var serializedValue = JSON.stringify(value);
            sessionStorage.setItem(key, serializedValue);
        } else {
            var serializedStorage = window.name;
            var storage = serializedStorage ? JSON.parse(serializedStorage) : {};

            storage[key] = value;

            serializedStorage = JSON.stringify(storage);
            window.name = serializedStorage;
        }
    }

    export function getObject<T>(key: string, persistence: PersistenceLevel = PersistenceLevel.Session): T {
        if (persistence === PersistenceLevel.Page) {
            return getPageValue(key);
        }

        if (BrowserSurvey.isSessionStorageSupported()) {
            var serializedValue = sessionStorage.getItem(key);

            return serializedValue
                ? JSON.parse(serializedValue)
                : undefined;
        }
        else {
            var serializedStorage = window.name;
            var storage = serializedStorage ? JSON.parse(serializedStorage) : {};

            return <T>storage[key];
        }
    }

    export function setString(key: string, value: string, persistence: PersistenceLevel = PersistenceLevel.Session) {
        if (persistence === PersistenceLevel.Page) {
            setPageValue(key, value);
            return;
        }

        if (BrowserSurvey.isSessionStorageSupported()) {
            sessionStorage.setItem(key, value);
        } else {
            var serializedStorage = window.name;
            var storage = serializedStorage ? JSON.parse(serializedStorage) : {};

            storage[key] = value;

            serializedStorage = JSON.stringify(storage);
            window.name = serializedStorage;
        }
    }

    export function getString(key: string, persistence: PersistenceLevel = PersistenceLevel.Session): string | null {
        if (persistence === PersistenceLevel.Page) {
            return getPageValue(key);
        }

        if (BrowserSurvey.isSessionStorageSupported()) {
            return sessionStorage.getItem(key);
        }
        else {
            var serializedStorage = window.name;
            var storage = serializedStorage ? JSON.parse(serializedStorage) : {};

            return storage[key];
        }
    }
} 
