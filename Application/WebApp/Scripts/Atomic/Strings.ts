﻿module Strings {
    export function format(formatString: string, ...argArray: any[]): string {
        if (argArray == null || argArray.length === 0) {
            return formatString;
        }

        for (var i = 0; i < argArray.length; i++) {
            formatString = formatString.replace("{" + i + "}", argArray[i]);
        }

        return formatString;
    }

    export function escapeAttributeValue(value: string) {
        // As mentioned on http://api.jquery.com/category/selectors/
        return value.replace(/([!"#$%&'()*+,./:;<=>?@\[\\\]^`{|}~])/g, "\\$1");
    }

    export function trimEnd(s: string, sufix: string): string {
        while (s.substr(s.length - sufix.length) === sufix) {
            s = s.substring(0, s.length - sufix.length);
        }

        return s;
    }

    export function areLowerCaseEqual(a: string | null, b: string | null): boolean {
        if (a == null) {
            if (b == null) {
                return true;
            } else {
                return false;
            }
        }

        if (b == null) {
            return false;
        }

        return a.toLowerCase() === b.toLowerCase();
    }

    export function startsWithLowerCase(a: string, b: string): boolean {
        if (a == null) {
            if (b == null) {
                return true;
            } else {
                return false;
            }
        }

        if (b == null) {
            return false;
        }

        return a.toLowerCase().indexOf(b.toLowerCase()) === 0;
    }

    export function isNullOrEmpty(s?: string | null): boolean {
        return s === undefined || s === null || s === "";
    }
    export function isNullOrWhiteSpace(s?: string | null): boolean {
        return s === undefined || s === null || s.trim() === "";
    }

    export function startsWith(s: string, phrase: string): boolean {
        if (isNullOrEmpty(s)) {
            return false;
        }

        return s.indexOf(phrase) === 0;
    }

    export function endsWith(s: string, phrase: string): boolean {
        if (isNullOrEmpty(s)) {
            return false;
        }

        var index = s.lastIndexOf(phrase);

        return index >= 0 && index === s.length - phrase.length;
    }
}