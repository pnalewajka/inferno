﻿module DateHelper {

    export function createDateAsUTC(date: Date) {
        return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
    }

    export function toDateString(date: Date) {
        return date.toISOString().slice(0, 10);
    }

    export function toTimeString(date: Date) {
        return date.toISOString().slice(11, 16);
    }

    export function parseDateTime(date: Date | string, time: string) {
        if (date instanceof Date) {
            date = toDateString(<Date>date);
        }

        return new Date(`${date}T${time}Z`);
    }

    export function parseBackendDate(date: string) {
        return new Date(`${date}Z`);
    }

    export function getThisWeekMonday(date: Date | string) {
        date = new Date(date);

        var day = date.getDay(),
            diff = date.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday

        return new Date(date.setDate(diff));
    }

    function addZeros(n: number): string {
        return (n < 10 ? '0' : '') + n;
    }

    export function getDateAsInputValueString(d: Date) {
        return d.getFullYear() + '-' +
            addZeros(d.getMonth() + 1) + '-' +
            addZeros(d.getDate());
    } 
}