﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/bootstrap/bootstrap.d.ts"/>
/// <reference path="./Forms.ts"/>

module Dialogs {
    export interface IDialogEventHandlers {
        eventHandler?: IButtonClickedEventHandler;
        onBeforeInit?: DialogHandler;
        onBeforeSubmit?: DialogHandler;
        onBeforeShow?: DialogHandler;
        onBeforeHide?: OnBeforeHideHandler;
        onSubmit?: OnSubmitHandler;
        formData?: dynamic;
        clientData?: dynamic;
    }

    export interface IDialogSettings extends IDialogEventHandlers {
        actionUrl: string,
    }

    export interface IDialogHtmlSettings extends IDialogEventHandlers {
        html: string,
    }

    export interface IDialogModelSettings {
        modelId: string;
        title: string;
        eventHandler?: IButtonClickedEventHandler;
        iconName: string;
        standardButtons: StandardButtons;
        modelUrlParameters?: string;
    }

    export class EventArgs {
        public dialog: JQuery;
        public buttonTag: string;
        public model: any;
        public clientData: any;

        constructor(dialog: JQuery, buttonTag: string, model: any, clientData: any) {
            this.dialog = dialog;
            this.buttonTag = buttonTag;
            this.model = model;
            this.clientData = clientData;
        }

        private isEmptyProperty(value: any): boolean {
            return typeof jQuery.type(value) === "string"
                && Strings.isNullOrEmpty(value)
                || jQuery.type(value) === "undefined"
                || value === null;
        }

        public get isOk(): boolean {
            return this.buttonTag === "ok";
        }

        public get isModelEmpty(): boolean {
            var model = this.model;

            if (model != null) {
                if (jQuery.isEmptyObject(model)) {
                    return true;
                }

                for (var prop in model) {
                    if (model.hasOwnProperty(prop)) {
                        if (!this.isEmptyProperty(model[prop])) {
                            return false;
                        }
                    }
                }
            };

            return true;
        }
    }

    export class PromptEventArgs extends EventArgs {
        public enteredText: string;

        constructor(dialog: JQuery, buttonTag: string, enteredText: string, model: any, clientData: any) {
            super(dialog, buttonTag, model, clientData);
            this.enteredText = enteredText;
        }
    }

    export interface IButtonClickedEventHandler {
        (eventArgs: EventArgs): void;
    }

    export interface IPromptButtonClickedEventHandler {
        (promptEventArgs: PromptEventArgs): void;
    }

    export enum StandardButtons {
        None = 0,

        Ok = 1,
        OkCancel = 2,
        YesNo = 3,
        AddCancel = 4,
        SaveCancel = 5,
    }

    interface IButtonClickedEventHandlerLookup {
        [index: string]: IButtonClickedEventHandler;
    }

    type DialogHandler = (dialog: JQuery) => void;
    type OnBeforeHideHandler = (e: JQueryEventObject) => boolean;
    type OnSubmitHandlerData = { action: string, formData: string, dialog: JQuery };
    type OnSubmitHandler = (data: OnSubmitHandlerData) => void;

    export class DialogEventHandler {
        private original?: IButtonClickedEventHandler;
        private extras: IButtonClickedEventHandlerLookup;
        private onBeforeShowDialog?: DialogHandler;
        private onBeforeInitDialog?: DialogHandler;
        private onBeforeSubmitDialog?: DialogHandler;
        private onBeforeHideDialog?: OnBeforeHideHandler;
        public onSubmit?: OnSubmitHandler;
        public clientData: any;

        constructor(options: IDialogEventHandlers) {
            const { eventHandler, clientData, onBeforeShow, onBeforeSubmit, onBeforeInit, onBeforeHide, onSubmit } = options;
            this.extras = {};
            this.original = eventHandler;
            this.clientData = clientData;
            this.onBeforeShowDialog = onBeforeShow;
            this.onBeforeSubmitDialog = onBeforeSubmit;
            this.onBeforeHideDialog = onBeforeHide;
            this.onBeforeInitDialog = onBeforeInit;
            this.onSubmit = onSubmit;
        }

        private onAfterShowDialog: (dialog: JQuery) => void = (dialog) => {
            var verticalPaddingValue = 30;
            var windowHeight = $(window).height();
            var headerHeight = $(".modal-header", dialog).height();
            var footerHeight = $(".modal-footer", dialog).height();

            var maxHeight = windowHeight * 0.9 - 2 * verticalPaddingValue - headerHeight - footerHeight;
            $(".modal-dialog .modal-body", dialog).css("max-height", `${maxHeight}px`);
            $(".modal-backdrop", dialog).css("height", `${windowHeight}px`);
        }

        public addExtra(handler: IButtonClickedEventHandler, key: string): void {
            this.extras[key] = handler;
        }

        public removeExtra(key: string): void {
            delete this.extras[key];
        }

        public handle(eventArgs: EventArgs) {
            var extraHandler = this.extras[eventArgs.buttonTag];
            if (extraHandler) {
                extraHandler(eventArgs);
            } else {
                if (this.original != null) {
                    this.original(eventArgs);
                }
            }
        }

        public handleBeforeInitDialog(dialog: JQuery) {
            if (this.onBeforeInitDialog != null) {
                this.onBeforeInitDialog(dialog);
            }
        }

        public handleBeforeShowDialog(dialog: JQuery) {
            if (this.onBeforeShowDialog != null) {
                this.onBeforeShowDialog(dialog);
            }
        }

        public handleBeforeSubmitDialog(dialog: JQuery) {
            if (this.onBeforeSubmitDialog != null) {
                this.onBeforeSubmitDialog(dialog);
            }
        }

        public handleBeforeHideDialog(e: JQueryEventObject) {
            if (this.onBeforeHideDialog != null) {
                return this.onBeforeHideDialog(e);
            }

            return true;
        }

        public handleAfterShowDialog(dialog: JQuery) {
            if (this.onAfterShowDialog != null) {
                this.onAfterShowDialog(dialog);
            }
        }
    }

    export class DialogButton {
        public text: string;
        public tag: string;
        public isDismissButton: boolean;

        constructor(text: string, tag: string, isDismissButton = true) {
            this.text = text;
            this.tag = tag;
            this.isDismissButton = isDismissButton;
        }
    }

    export class DialogInput {
        public label: string;
        public tag: string;
        public value: string;

        constructor(label: string, tag: string) {
            this.tag = tag;
            this.label = label;
        }
    }

    export class DialogBuilder {
        public eventHandler: DialogEventHandler | null;
        private dialogId: string;
        private htmlMessage: string;
        private title: string;
        private iconName: string;
        private buttons: DialogButton[];
        private inputs: DialogInput[];
        private hideCloseButton: boolean;
        private hasForm: boolean;
        public stopProcessing: boolean;

        public init(dialogId: string, htmlMessage: string, title: string, eventHandler: DialogEventHandler | null, iconName: string, buttons: DialogButton[], inputs: DialogInput[], hideCloseButton: boolean = false, hasForm: boolean = false) {
            this.dialogId = dialogId;
            this.htmlMessage = htmlMessage;
            this.title = title;
            this.iconName = iconName;
            this.eventHandler = eventHandler;
            this.buttons = buttons || [];
            this.inputs = inputs || [];
            this.hideCloseButton = hideCloseButton;
            this.stopProcessing = false;
            this.hasForm = hasForm;
        }

        public getHtml(): string {
            var html = "";
            var dialogClassNameInsert = this.iconName ? " has-icon" : "";

            html += `<div class="modal common-dialog${dialogClassNameInsert}" id="${this.dialogId}">`;
            html += "<div class=\"modal-dialog\">";
            html += "<div class=\"modal-content\">";

            if (this.title) {
                html += "<div class=\"modal-header\">";
                if (!this.hideCloseButton) {
                    html += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>";
                }
                html += `<h4 class="modal-title">${this.title}</h4>`;
                html += "</div>";
            }

            html += "<div class=\"modal-body\">";

            if (this.hasForm) {
                html += "<form method='POST' role='form'>";
            }

            if (this.iconName) {
                html += "<div class=\"dialog-icon\">";
                html += `<i class="${Utils.getIconClass(this.iconName)} aria-hidden="true"></i>`;
                html += "</div>";
            }

            html += "<div class=\"html-message\">";

            html += this.htmlMessage;

            html += "</div>";

            if (this.hasForm) {
                html += "</form>";
            }

            var dialogInputs = this.inputs;

            if (dialogInputs && dialogInputs.length > 0) {
                html += "<div class=\"modal-inputs\">";

                for (let i = 0; i < dialogInputs.length; i++) {
                    var input = dialogInputs[i];

                    html += "<div class=\"dialog-input\">";
                    html += `<div class="input-label col-sm-3 control-label required">${input.label}</div>`;

                    html += "<div class=\"col-sm-9\">";
                    html += `<input name="${input.tag}" type='text' class="form-control" data-input="${input.tag}"`;
                    html += " />";
                    html += `<span class="field-validation-valid"">${Strings.format(Globals.resources.PromptDialogValidationMessage, input.label)}`;
                    html += "</span>";
                    html += "</div>";

                    html += "</div>";
                }

                html += "</div>";
            }

            html += "</div>";

            var dialogButtons = this.buttons.filter(b => b.text ? true : false);

            if (dialogButtons.length) {
                html += "<div class=\"modal-footer\">";

                for (let i = 0; i < dialogButtons.length; i++) {
                    var button = dialogButtons[i];

                    var className = i === 0 ? "btn-primary" : "btn-default";
                    var dismiss = button.isDismissButton ? " data-dismiss=\"modal\"" : "";

                    html += `<button type="button" class="btn ${className}"${dismiss} data-button="${button.tag}">${button.text}</button>`;
                }

                html += "</div>";
            }

            html += "</div>";
            html += "</div>";
            html += "</div>";

            return html;
        }

        public getModalOptions(): any {
            return {};
        }

        public buildAndShow(): void {
            var html = this.getHtml();
            var options = this.getModalOptions();
            showHtmlDialog(html, this.eventHandler, options);
        }
    }

    class DialogReloader {
        private url: string;
        private formData: any;
        private dialogEventHandler: DialogEventHandler;
        private modalWidth: number;

        constructor(url: string, formData: any, dialogEventHandler: DialogEventHandler) {
            this.url = url;
            this.formData = formData;
            this.dialogEventHandler = dialogEventHandler;
            this.modalWidth = 0;
        }

        public getCurrentUrl(): string {
            return this.url;
        }

        public adjustDialogInitialSize(dialog: JQuery): void {
            dialog
                .css("visibility", "hidden")
                .css("display", "block");

            var modal = dialog.find(".modal-dialog");

            var htmlMessage = dialog.find(".html-message");
            var messageElement = htmlMessage[0];

            var scrollWidth = messageElement.scrollWidth;
            var clientWidth = messageElement.clientWidth;

            var modalWidth = modal.width();

            this.adjustDialogHeight(dialog);

            dialog
                .css("visibility", "")
                .css("display", "");

            if (scrollWidth > clientWidth) {
                var widthDelta = Math.ceil((scrollWidth - clientWidth) / 100) * 100;
                modalWidth += widthDelta;
            }

            this.modalWidth = Math.max(modalWidth, this.modalWidth);
            modal.width(this.modalWidth);
        }

        public adjustDialogHeight(dialog: JQuery): void {
            var dialogHeightSafetyMargin: number = 2;
            var dialogHeightMinValue: number = 50;

            var clientHeight = $(window).height();

            var dialogHeader = dialog.find(".modal-header");
            var dialogBody = dialog.find(".modal-body");
            var dialogFooter = dialog.find(".modal-footer");

            var marginTop = dialogHeader.offset().top;
            var otherHeight = dialogHeader.outerHeight(true) + dialogFooter.outerHeight(true) + dialogHeightSafetyMargin;
            var remainingHeight = Math.max(dialogHeightMinValue, clientHeight - otherHeight - marginTop * 2);

            dialogBody.css("max-height", remainingHeight + "px");
        }

        public reloadDialog(url: string) {
            this.url = url;

            $.ajax({
                type: "POST",
                url: this.url,
                data: this.formData,
                success: (response, statusText, jqXhr) => {
                    this.handleResponse(response, statusText, jqXhr);
                }
            });
        }

        public reload(): void {
            this.reloadDialog(this.getCurrentUrl());
        }

        private attachToDialog(newDom: JQuery) {
            newDom.data("reloader", this);
            Controls.getAllInstances(newDom, "IPageReloadingControl")
                .forEach((c) => {
                    var pageReloadingControl: Controls.IPageReloadingControl = <Controls.IPageReloadingControl>(<any>c);
                    pageReloadingControl.onExistingUrlRequested = () => this.getCurrentUrl();
                    pageReloadingControl.onPageReload = (url) => this.reloadDialog(url);
                });
        }

        private hideAndRemoveAllModals() {
            $(".modal").modal("hide");
        }

        private handleResponse(response: any, statusText: string, jqXhr: JQueryXHR) {
            if ($.type(response) === "string") {
                var newDom = showHtmlDialog(<string>(response), this.dialogEventHandler);
                this.attachToDialog(newDom);

                return;
            }

            if (response.action === "redirect") {
                if (response.data.repost === true) {
                    $(".modal").hide().find("form").first().attr("action", response.data.url).submit();
                } else {
                    window.location.href = response.data.url;
                }

                CommonDialogs.pleaseWait.hide();
                this.hideAndRemoveAllModals();
                CommonDialogs.pleaseWait.hide();

                return;
            }

            if (response.action === "redirect-to-list") {
                Forms.redirectToList(response.data.url);
            }

            if (response.dialogId) {
                $(`#${response.dialogId}`).hide();
            }

            switch (response.action) {
                case "alert":
                    alert(response.data);
                    break;

                case "reload":
                    window.location.reload(true);
                    break;

                case "event":
                    var $dialog = $(`#${response.dialogId}`);
                    var eventArgs = new EventArgs($dialog, response.data.buttonTag, response.model, this.dialogEventHandler.clientData);

                    this.dialogEventHandler.handle(eventArgs);
                    break;
            }
        }
    }

    function defaultSubmitDialogAction(eventTarget: JQuery, dialogEventHandler: DialogEventHandler) {
        var dialog = eventTarget.closest(".modal");
        var form = dialog.find("form").first();

        if (!form.length) {
            return;
        }

        CommonDialogs.pleaseWait.show();

        dialogEventHandler.handleBeforeSubmitDialog(dialog);

        var action = form.attr("action");
        var formData = form.serialize();

        if (dialogEventHandler.onSubmit === undefined) {
            var dialogReloader = new DialogReloader(action, formData, dialogEventHandler);
            dialogReloader.reload();
        } else {
            dialogEventHandler.onSubmit({ action, formData, dialog });
        }
    }

    function handleDialogButtonClick(e: JQueryEventObject, eventHandler: DialogEventHandler | null) {
        var eventTarget = $(e.target);
        var buttonTag = eventTarget.attr("data-button") || "other";

        if (buttonTag === "submit" && eventHandler != null) {
            var handler = () => {
                defaultSubmitDialogAction(eventTarget, eventHandler);
                eventHandler.removeExtra("submit");
            };

            eventHandler.addExtra(handler, "submit");
        }

        var $dialog = eventTarget.closest(".modal");
        var dialogId = $dialog.attr("id");

        if (eventHandler != null) {
            var eventArgs;

            if (dialogId === CommonDialogs.PromptDialog.id) {
                var enteredText = eventTarget.find(`input[data-input='${CommonDialogs.PromptDialog.inputTag}']`).val();
                eventArgs = new PromptEventArgs($dialog, buttonTag, enteredText, null, eventHandler.clientData);
            }
            else {
                eventArgs = new EventArgs($dialog, buttonTag, null, eventHandler.clientData);
            }

            eventHandler.handle(eventArgs);
        }
    }

    function assureFullMenuVisibility(dialogSelector: string) {
        $(dialogSelector + " .dropdown-toggle").on("click", function (this: HTMLElement) {
            var button = $(this);
            var btnOffset = button.offset();
            var modalOffset = $(".modal-content").offset();
            var dropdown = button.next("ul");
            var dropDownTop = btnOffset.top - modalOffset.top + button.outerHeight();
            var dropDownLeft = btnOffset.left - modalOffset.left;

            dropdown.css("position", "fixed");
            dropdown.css("top", dropDownTop + "px");
            dropdown.css("left", dropDownLeft + "px");
        });
    }

    export function showHtmlDialog(html: string, eventHandler: DialogEventHandler | null, modalOptions: ModalOptionsBackdropString | null = null): JQuery {
        var newDom = $(html.replace(/^\s+|\s+$/gm, ""));

        if (newDom.length !== 1) {
            //when accessing via VPN we have to filter additional <script> tags added by FortiClient VPN and leave only the root node
            newDom = $(newDom.filter("div"));
        }

        var dialogId = newDom.attr("id");
        var isErrorMessage = newDom.attr("data-is-error") === "1";
        var dialogSelector = `#${dialogId}`;

        var previousCopy = $(dialogSelector);

        if (previousCopy.length) {
            previousCopy.modal("resetScrollbar");
            previousCopy.replaceWith(newDom);
        } else {
            $("body").append(newDom);
        }

        if (!isErrorMessage) {
            if (eventHandler != null) {
                newDom.data("client-data", eventHandler.clientData);
                eventHandler.handleBeforeInitDialog(newDom);
            }

            Forms.init(newDom, true);

            assureFullMenuVisibility(dialogSelector);

            $(dialogSelector + " .modal-footer *[data-button]").on("click.atomic", function (this: HTMLElement, e) {
                var button = $(this);
                button.closest(".modal").attr("data-button", button.attr("data-button"));
                button.closest(".modal").attr("data-dismiss", button.attr("data-dismiss"));

                if (button.attr("data-dismiss") !== "modal") {
                    handleDialogButtonClick(e, eventHandler);
                }
            });

            $(dialogSelector).on("shown.bs.modal", () => {
                $(dialogSelector).find("input[autofocus]").focus();
            });

            $(dialogSelector).on("hidden.bs.modal", e => {
                $(dialogSelector).remove();

                if ($(e.target).attr("data-dismiss") === "modal") {
                    handleDialogButtonClick(e, eventHandler);
                }
            });

            $(dialogSelector).on("hide.bs.modal", e => {
                return eventHandler != null && eventHandler.handleBeforeHideDialog(e);
            });

            if (eventHandler != null) {
                eventHandler.handleBeforeShowDialog(newDom);
            }
        }

        if (dialogId !== CommonDialogs.PleaseWaitDialog.dialogId) {
            CommonDialogs.pleaseWait.hide();
        }
        const defaultModalOptions: ModalOptionsBackdropString = {
            backdrop: "static"
        };

        var modalInstance = $(dialogSelector).modal({ ...defaultModalOptions, ...(modalOptions || {}) as ModalOptionsBackdropString });

        if (eventHandler != null && !!modalInstance) {
            $(".modal-content", $(dialogSelector)).draggable({ handle: ".modal-header" });
            eventHandler.handleAfterShowDialog(modalInstance);
        }
        return newDom;
    }

    export function showDialog(options: IDialogSettings): void {
        const { actionUrl, formData } = options;

        if (typeof actionUrl === "undefined" || actionUrl == null) {
            return;
        }

        CommonDialogs.pleaseWait.show();

        var url = Globals.resolveUrl(options.actionUrl);
        var dialogEventHandler = new DialogEventHandler(options);
        var dialogReloader = new DialogReloader(url, formData, dialogEventHandler);

        dialogReloader.reload();
    }

    export function showModel(settings: IDialogModelSettings) {
        const { modelId, title, iconName, standardButtons, modelUrlParameters, eventHandler } = settings;

        if (typeof modelId === "undefined" || modelId == null) {
            return;
        }

        var formData = {
            modelId: modelId,
            title: title,
            iconName: iconName,
            standardButtons: standardButtons
        };

        CommonDialogs.pleaseWait.show();

        var url = Strings.isNullOrWhiteSpace(modelUrlParameters)
            ? Globals.resolveUrl("~/Dialog/ShowModel")
            : Globals.resolveUrl(`~/Dialog/ShowModel?${modelUrlParameters}`);

        var dialogEventHandler = new DialogEventHandler({ eventHandler });
        var dialogReloader = new DialogReloader(url, formData, dialogEventHandler);
        dialogReloader.reload();
    }

    export function setWidth(dialog: JQuery, width: string) {
        dialog.find(".modal-dialog").css("width", width);
    }
}
