﻿module Enum {
    export function hasFlag(myEnum: number, flag: number): boolean
    {
        return (myEnum & flag) === flag;
    }
}
