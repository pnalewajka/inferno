﻿module Optional {
    export function initControls(context: JQuery): void {
        context.find('.optional-value-control').each((index: number, elem: Element) => {
            $(elem).find('.optional-value-control-inner input').each((i: number, input: Element) => {
                const jElement = $(input);
                const hasEventsAttached = jElement.data("optional-events-attached") || false;

                if (!hasEventsAttached) {
                    jElement.data("optional-events-attached", true);

                    jElement.on("change paste keyup", () => {
                        $(elem).find('.optional-value-control-radio input[value=1]:radio').prop("checked", true);
                    });
                }
            });
        });
    }
}