﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/select2/select2.d.ts"/>

module TypeAhead {

    function initTypeAheadControl(typeAhead: Element): void {
        var $typeAhead = $(typeAhead);
        var name = $typeAhead.data('field-name');
        var fieldId = $typeAhead.data('field-id');
        var listId = $typeAhead.data('list-id');
        var formatterId = $typeAhead.data('formatter-id');
        var onResolveUrlFuncOrBody = $typeAhead.data('on-resolve-url');
        var onSelectionChangedFuncOrBody = $typeAhead.data('on-selection-changed');
        var minimumInputLength = $typeAhead.data('minimum-input-length');
        var prompt = $typeAhead.data('prompt');
        var isMultiValue = $typeAhead.data('is-multivalue');
        var typeAheadInput = $typeAhead.find('#' + fieldId);
        var idInput = $typeAhead.find('input[name="' + name + '"]');
        var itemContainer = $typeAhead.find(".item-container");
        var itemContainerInput = itemContainer.find(".type-ahead-input-item");
        var clearButton = $typeAhead.find('.type-ahead-remove-button');

        var onResolveUrl = (url: string) => {
            if (onResolveUrlFuncOrBody == null || onResolveUrlFuncOrBody === "") {
                return url;
            }
            
            var params = { control: typeAhead, url: url };

            return Utils.executeHandler(onResolveUrlFuncOrBody, params);
        };

        var onSelectionChanged = () => {
            if (onSelectionChangedFuncOrBody != null && onSelectionChangedFuncOrBody !== "") {
                var params = { control: typeAhead };
                Utils.executeHandler(onSelectionChangedFuncOrBody, params);
            }

            idInput.valid();
        };

        var getDataUrl = () => {
            var url = Globals.resolveUrl('~/ValuePicker/JsonList?listId=' + listId);

            if (formatterId != null)
                url = url + '&formatterId=' + formatterId;

            if (onResolveUrl != null) {
                url = onResolveUrl(url);
            }

            return url;
        }

        var onSelectMultipleEvent = (event: any) => {
            var select2Event = event;

            if (select2Event.params != null && select2Event.params.data != null && select2Event.params.data.id != null) {
                var recordId = select2Event.params.data.id;
                var recordText = select2Event.params.data.text;
                var previousValue = idInput.val();

                Forms.appendToInput(idInput, ',', recordId.toString());
                itemContainerInput.before('<div data-id="' + recordId + '" class="type-ahead-item">' + recordText +
                    '<i class="' + Utils.getIconClass('glyphicon-remove') + ' type-ahead-remove-item-button"></i></div>');

                if (idInput.val() !== previousValue) {
                    onSelectionChanged();
                }

                typeAheadInput.val("").trigger("change");
                $typeAhead.find("input[placeholder]").each(function (this: JQuery) {
                    $(this).attr('size', $(this).attr('placeholder').length);
                    $(this).css('width', 'auto');
                });
            }
        };

        var onSelectSingleEvent = (event: any) => {
            var select2Event = event;

            if (select2Event.params != null && select2Event.params.data != null && select2Event.params.data.id != null) {
                var recordId = select2Event.params.data.id;
                var previousValue = idInput.val();

                idInput.val(recordId.toString());

                if (idInput.val() !== previousValue) {
                    onSelectionChanged();
                }
            }
        };

        typeAheadInput.select2({
            ajax: {
                url: getDataUrl(),
                dataType: 'json',
                delay: 250,
                data(params) {
                    return {
                        search: params.term
                    }
                },

                processResults(data, params) {
                    var displayNames = new Array<any>();
                    var idValue = idInput.val();
                    var currentlySelected = Strings.isNullOrEmpty(idValue) ? new Array<any>() : idValue.split(',');

                    $.each(data, (i, item) => {
                        if ($.inArray(item.Id.toString(), currentlySelected) < 0) {
                            displayNames.push({
                                id: item.Id.toString(),
                                text: item.DisplayName
                            });
                        }
                    });

                    return {
                        results: displayNames
                    }
                },

                cache: false
            },

            minimumInputLength: minimumInputLength,
            placeholder: prompt,

            dropdownAutoWidth: true,
            width: '100%'
        });

        $typeAhead.find("input[placeholder]").each(function (this: JQuery) {
            $(this).attr('size', $(this).attr('placeholder').length);
            $(this).css('width', 'auto');
        });

        typeAheadInput.on("select2:select", isMultiValue
            ? event => onSelectMultipleEvent(event)
            : event => onSelectSingleEvent(event));

        clearButton.click(() => {
            var previousValue = idInput.val();

            idInput.val("");
            typeAheadInput.val("").trigger("change");

            if (isMultiValue) {
                $typeAhead.find("input[placeholder]").each(function (this: JQuery) {
                    $(this).attr('size', $(this).attr('placeholder').length);
                    $(this).css('width', 'auto');
                });

                itemContainer.find(".type-ahead-item").remove();
            }

            if (previousValue !== "") {
                onSelectionChanged();
            }
        });

        itemContainer.click((event) => {
            var $target = $(event.target);
            var $parent = $target.parent();
            var id = $parent.data("id");

            if (id != null) {
                $parent.remove();
                Forms.removeFromInput(idInput, ',', id);

                typeAheadInput.val("").trigger("change");
                $typeAhead.find("input[placeholder]").each(function (this: JQuery) {
                    $(this).attr('size', $(this).attr('placeholder').length);
                    $(this).css('width', 'auto');
                });

                onSelectionChanged();
            } else {
                typeAheadInput.focus();
                typeAheadInput.select2("open");
            }
        });
    }

    export function initControls(context: JQuery): void {
        context.find('.type-ahead ').each((index: number, elem: Element) => {
            initTypeAheadControl(elem);
        });
    }
} 