module Reporting {
    export function displayDate(date: string | null | undefined): string;
    export function displayDate(date: Date | null | undefined): string;
    export function displayDate(date: string | Date | null | undefined): string {
        if (date == null) {
            return "";
        }

        if (!(date instanceof Date)) {
            date = new Date(date);
        }

        type DynamicDate = string | number;

        let year: DynamicDate = date.getUTCFullYear();

        let month: DynamicDate = date.getUTCMonth() + 1;
        if (month < 10) month = "0" + month;

        let day: DynamicDate = date.getUTCDate();
        if (day < 10) day = "0" + day;

        return `${year}/${month}/${day}`;
    }

    function flatModel(model: any): any {
        for (var propertyName in model) {
            if (Array.isArray(model[propertyName])) {
                model[propertyName] = model[propertyName].join(",");
            }
        }

        return model;
    }

    export function showParametersModal(dialogUrl: string, reportCode: string, parameters: any): void {
        const dialogWidth: string = "1000px";
        const url = UrlHelper.updateUrlParameter(dialogUrl, "reportCode", reportCode);
        parameters.Data = flatModel(parameters.Data);

        Dialogs.showDialog({
            actionUrl: url,
            formData: parameters,
            eventHandler: () => { },
            onBeforeInit: dialog => {
                Dialogs.setWidth(dialog, dialogWidth);
            }
        });
    }
    export function exportConfig(pivotTable: PivotTable): void {
        const currentConfig = pivotTable.getCurrentConfig();
        prompt("Copy configuration from here:", JSON.stringify(currentConfig));
    }

    export function importConfig(pivotTable: PivotTable): void {
        var config = prompt("Paste configuration here:");

        if (config != null && config.length > 0) {
            pivotTable.applyConfig(JSON.parse(config));
        }
    }

    export interface IPivotTableConfig {
        rows: string[];
        cols: string[];
        vals: string[];
        aggregatorName?: string;
        rowOrder?: string;
        colOrder?: string;
        localeStrings?: any;
    }

    export interface IPivotMenuItem<TConfig> {
        name: string;
        code: string;
        config: TConfig;
    }

    export interface IPivotTablePredefinedConfig extends IPivotMenuItem<IPivotTableConfig> {
    }

    export interface IPivotTableBaseConfig {
        dataConverter?: (data: any) => any[];
        configurations?: IPivotTablePredefinedConfig[];
    }

    export class PivotTable {
        private _jElement: JQuery;
        private _configurations: IPivotTablePredefinedConfig[];
        private _data: dynamic[];
        private _isUiEnabled: boolean;

        private static defaultDataConverter(data: dynamic): dynamic[] {
            return data[Object.getOwnPropertyNames(data)[0]];
        }

        private static replaceNullOrUndefinedWithEmptyString(data: dynamic[]): dynamic[] {
            const keys = data.map(e => Object.getOwnPropertyNames(e)) // get property names
                .reduce((a, b) => a.concat(b), []) // selectmany
                .filter((v, i, s) => s.indexOf(v) === i); // unique

            return data.map(e => {
                for (let i = 0; i < keys.length; ++i) {
                    const key = keys[i];
                    if (e[key] == null) {
                        e[key] = "";
                    }
                }

                return e;
            });
        }

        public constructor(jElement: JQuery, data: any, config: IPivotTableBaseConfig) {
            this._jElement = jElement;
            this._isUiEnabled = false;

            this._data = config.dataConverter !== undefined
                ? config.dataConverter(data)
                : PivotTable.defaultDataConverter(data);
            this._data = PivotTable.replaceNullOrUndefinedWithEmptyString(this._data);

            this._configurations = config.configurations !== undefined
                ? config.configurations
                : this.createDefaultConfigurations();

            this.applyDefaultConfig();
        }

        private createDefaultConfigurations(): IPivotTablePredefinedConfig[] {
            if (this._data.length == 0) {
                return [];
            }

            return [{
                name: "Default",
                code: "default",
                config: {
                    rows: Object.getOwnPropertyNames(this._data[0]),
                    cols: [],
                    vals: []
                }
            }];
        }

        public applyDefaultConfig(): void {
            const definitions = this.getConfigDefinitions();

            if (definitions.length === 0) {
                this._isUiEnabled = true;
                this.render();
            } else {
                this.applyConfigByCode(definitions[0].code as string);
            }
        }

        public getCurrentConfig(): IPivotTableConfig {
            const config = this._jElement.data("pivotUIOptions");

            if (config !== undefined) {
                delete config["aggregators"];
                delete config["renderers"];
                delete config["onRefresh"];

                delete config["rendererOptions"];
                delete config["localeStrings"];

                return config;
            }

            return this.getBaseConfig();
        }

        private render(config: Partial<IPivotTableConfig> = this.getCurrentConfig()): void {
            this._jElement.toggleClass("ui-disabled", !this._isUiEnabled);
            this._jElement.empty().pivotUI(this._data, $.extend(this.getBaseConfig(), config), true);
        }

        private getBaseConfig(): IPivotTableConfig {
            const locales = ($ as any).pivotUtilities.locales.en.localeStrings;

            return {
                rows: [],
                cols: [],
                vals: [],
                localeStrings: $.extend({}, locales, { renderError: "Configure report is too complex to render. Please reduce number of rows or columns." })
            };
        }

        public toggleUI(): void {
            this._isUiEnabled = !this._isUiEnabled;
            this.render();
        }

        public getConfigDefinitions(): ReadonlyArray<IPivotTablePredefinedConfig> {
            return Object.freeze(this._configurations);
        }

        public applyConfig(newConfig: Partial<IPivotTableConfig>): void {
            this.render(newConfig);
        }

        public applyConfigByCode(configCode: string): void {
            let predefined = this._configurations.filter(c => c.code == configCode)[0];

            if (predefined === undefined || predefined.config === undefined) {
                return this.applyConfig({});
            }

            return this.applyConfig(predefined.config);
        }

        public copyToClipboard(): void {
            var range = new Range();
            var selection = window.getSelection();

            // Remove all posible selections before click Copy To Clipboard
            selection.removeAllRanges();

            range.selectNodeContents($(".pvtTable")[0]);
            selection.addRange(range);

            document.execCommand("copy");
            selection.removeAllRanges();
        }

        public downloadExcel(xlnt: xlnt, fileName: string): void {

            const skip: boolean[][] = [];

            const shouldSkip = (col: number, row: number) => {
                skip[col] = skip[col] || [];
                return skip[col][row] || false;
            };

            const setSkip = (col: number, row: number, skipCols: number, skipRows: number) => {
                for (let c = col; c <= col + skipCols; ++c) {
                    for (let r = row; r <= row + skipRows; ++r) {
                        if (c === col && r === row) {
                            continue;
                        }

                        skip[c] = skip[c] || [];
                        skip[c][r] = true;
                    }
                }
            };

            const workbook = new xlnt.workbook();
            const sheet = workbook.active_sheet();

            const offset = [0, 0];
            const table = $(".pvtTable")[0];

            const rows = table.querySelectorAll("tr");
            let currentRowIndex = offset[0];

            for (let rowIndex = 0;
                rowIndex < rows.length;
                ++rowIndex)
            {
                ++currentRowIndex;
                const row = rows[rowIndex];
                const columns = row.querySelectorAll("th,td") as NodeListOf<HTMLTableCellElement>;
                let currentColumnIndex = offset[1];

                for (let columnIndex = 0;
                    columnIndex < columns.length;
                    ++columnIndex)
                {
                    const column = columns[columnIndex];
                    ++currentColumnIndex;

                    while (shouldSkip(currentColumnIndex, currentRowIndex)) {
                        ++currentColumnIndex;
                    }

                    const value = column.innerText;
                    const numericValue = parseFloat(value);
                    const backgroundColor = getComputedStyle(column).backgroundColor;

                    if (value !== "") {
                        sheet.using_cell(
                            currentColumnIndex, currentRowIndex,
                            c => c.set_value(value == numericValue.toString()
                                ? numericValue
                                : value));

                        // TODO: autoadjust cell width // https://github.com/tfussell/xlnt/issues/347
                        // TODO: set cell background color // https://github.com/tfussell/xlnt/issues/348
                    }

                    const colspan = Math.max((column.colSpan || 0) - 1, 0);
                    const rowspan = Math.max((column.rowSpan || 0) - 1, 0);

                    sheet.merge_cells(
                        currentColumnIndex, currentRowIndex,
                        currentColumnIndex + colspan, currentRowIndex + rowspan);

                    setSkip(
                        currentColumnIndex, currentRowIndex,
                        colspan, rowspan);
                }
            };

            workbook.download(fileName);

            workbook.delete();
            sheet.delete();
        }
    }
}