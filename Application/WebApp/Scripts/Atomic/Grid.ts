﻿/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>

module Grid {
    var highlightClassName = "selected";
    var importDialogWidth = "750px";

    export interface IRow {
        id: any;
        toString: string;
    };

    export enum GridDisplayMode {
        Rows,
        Tiles,
        Custom
    }

    export function getComponent(element: Element): GridComponent {
        return Controls.getInstance(GridComponent, element);
    }

    function onSortByClicked(this: JQuery, event: JQueryEventObject) {
        var element = Utils.getEventTarget(event);
        var columnName = $(this).attr("data-column-name");
        var grid = Controls.getInstance(GridComponent, element);
        grid.setOrderBy(columnName, event.ctrlKey);
        grid.reloadPage();

        return grid;
    }

    function onSearchButtonClicked(event: Event) {
        var grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
        grid.reloadPage();
    }

    function onSearchButtonKeyDown(event: JQueryEventObject) {
        if (event.keyCode === Forms.KeyCode.Enter) {
            var grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
            grid.reloadPage();
        }
    }

    export function onImportClicked(importDialogUrl: string) {
        Dialogs.showDialog({
            actionUrl: importDialogUrl,
            eventHandler: eventArgs => {
                if (eventArgs.isOk) {
                    Utils.reloadPage();
                }
            },
            onBeforeInit: (dialog) => {
                Dialogs.setWidth(dialog, importDialogWidth);
            }
        });
    }

    export function onHandlePageReloadingDialog(url: string, event: Event, width?: number): void {
        const grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
        const valuePickerDialog = $('.common-dialog-value-picker');

        const onBeforeShow = width !== undefined
            ? (dialog: JQuery) => { Dialogs.setWidth(dialog, `${width}px`); }
            : undefined;

        Dialogs.showDialog({
            actionUrl: grid.resolveUrlParameters(url),
            eventHandler: result => {
                if (result.isOk) {
                    if (valuePickerDialog.length > 0) {
                        result.dialog.remove();
                        valuePickerDialog.last().data('reloader').reload();
                    } else {
                        window.location.reload();
                    }
                }
            }, onBeforeShow
        });
    }

    export function resolveUrlParameters(url: string, selectedRows: IRow[], contextValue: string): string {
        if (url.indexOf("context=~context") !== -1) {
            url = UrlHelper.removeUrlParameterAssignment(url, "context=~context");
            url = UrlHelper.updateUrlParameters(url, contextValue, true);
        }

        var urlTokens = url.match(/~url\.(\S)+?\b/g);

        if (urlTokens != null) {
            var paramsString = UrlHelper.getUrlParameters(window.location.href);
            var params = UrlHelper.splitUrlParams(paramsString);

            for (var index in urlTokens) {
                var urlToken = urlTokens[index];

                var urlParameterFromToken = urlToken.replace("~url.", "");
                var urlParam = Utils.pascalCaseToDelimitierSepareted(urlParameterFromToken)

                var parentIdParam = params.filter(s => s.name === urlParam);

                if (parentIdParam.length > 0) {
                    url = url.replace(new RegExp(urlToken, "g"), parentIdParam[0].value);
                }
            }
        }

        if (selectedRows.length > 0) {
            // special case values
            var id = selectedRows[0].id;
            var ids = selectedRows.map((v) => v.id).join(",");

            url = url
                .replace(/~this.ids/g, ids)
                .replace(/~this.id/g, id.toString());

        } else {
            url = url
                .replace(/~this.ids/g, "")
                .replace(/~this.id/g, "");
        }

        // general approach
        var tokens = url.match(/~selected\.(\S)+?\b/g);

        if (tokens != null) {
            for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];
                var propertyName = token.substr(10);

                var values = selectedRows.map((v: any) => v[propertyName]).join(",");
                url = url.replace(token, encodeURIComponent(values));
            }
        }

        url = url.split("&").filter((v) => !Strings.isNullOrEmpty(v)).join("&");
        url = url.replace("?&", "?");

        url = Strings.trimEnd(url, "?");

        return url;
    }

    function shouldHandleMouseEvent(event: MouseEvent | JQueryEventObject, allowedButtons: Array<number> = [1]): boolean {
        return allowedButtons.indexOf(event.which) !== -1;
    }

    function shouldHandleKeyboardEvent(event: KeyboardEvent | JQueryEventObject, allowedKeyCodes: Array<number>): boolean {
        return allowedKeyCodes.indexOf((<KeyboardEvent>event).keyCode) !== -1;
    }

    function onPageClicked(this: JQuery, event: JQueryEventObject): void {
        if (!shouldHandleMouseEvent(event)) {
            return;
        }

        var page = parseInt($(this).attr("data-target-page"));

        if (page === NaN) {
            return;
        }

        var element = Utils.getEventTarget(event);
        var grid = Controls.getInstance(GridComponent, element);
        grid.goToPage(page);
    }

    function onPageKeyDown(this: JQuery, event: JQueryEventObject): void {
        if (!shouldHandleKeyboardEvent(event, [Forms.KeyCode.Space, Forms.KeyCode.Enter])) {
            return;
        }

        var page = parseInt($(this).attr("data-target-page"));

        if (page === NaN) {
            return;
        }

        var element = Utils.getEventTarget(event);
        var grid = Controls.getInstance(GridComponent, element);
        grid.goToPage(page);
    }

    var previousClickTime = Date.now();
    var previousRowIndex = -1;
    var doubleClickDelayMilliseconds = 500;

    function getRowIndex(row: JQuery): number {
        return row.index() - row.prevAll(".row-label").length;
    }

    function shouldSkipRowClick(row: JQuery): boolean {
        var currentRowIndex = getRowIndex(row);
        var currentClickTime = Date.now();

        var timeDelta = currentClickTime - previousClickTime;
        var indexDelta = currentRowIndex - previousRowIndex;

        previousClickTime = currentClickTime;
        previousRowIndex = currentRowIndex;

        return timeDelta < doubleClickDelayMilliseconds && indexDelta === 0;
    }

    function isRowButton(clickedElement: JQuery): boolean {
        return clickedElement.closest(".row-buttons").length > 0 || clickedElement.closest(".row-commands").length > 0;
    }

    function onRowClicked(event: JQueryEventObject) {
        if (!shouldHandleMouseEvent(event)) {
            return;
        }

        var element = Utils.getEventTarget(event);
        var clickedElement = $(element);

        if (isRowButton(clickedElement)) {
            return;
        }

        var grid = Controls.getInstance(GridComponent, element);

        if (!grid.hasRows) {
            return;
        }

        var row = clickedElement.closest(grid.itemElementSelector);

        if (shouldSkipRowClick(row)) {
            return;
        }

        grid.onRowClicked(row);
    }

    function onRowKeyDown(event: JQueryEventObject) {
        var element = Utils.getEventTarget(event);
        var clickedElement = $(element);

        var grid = Controls.getInstance(GridComponent, element);

        if (!grid.hasRows) {
            Forms.cancelEvent(event);

            return;
        }

        var row = clickedElement.closest(grid.itemElementSelector);

        if (shouldHandleKeyboardEvent(event, [Forms.KeyCode.Enter]) && grid.shouldEnterInvokeDefaultButton) {
            grid.onRowEnterPressed(row);
            Forms.cancelEvent(event);

            return;
        }

        if (event.keyCode === Forms.KeyCode.Down) {
            row.next().focus();
            return;
        }


        if (event.keyCode === Forms.KeyCode.Up) {
            row.prev().focus();
            return;
        }

        if (!shouldHandleKeyboardEvent(event, [Forms.KeyCode.Space])) {
            return;
        }

        grid.onRowClicked(row);
    }

    function onRowDoubleClicked(event: JQueryEventObject) {
        var element = Utils.getEventTarget(event);
        var clickedElement = $(element);

        var grid = Controls.getInstance(GridComponent, element);
        var row = clickedElement.closest(grid.itemElementSelector);

        grid.onRowDoubleClicked(row, event);
    }

    export function onExportToExcelClicked(event: Event) {
        var grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
        var params = UrlHelper.getUrlParameters(grid.generateReloadUrl());
        var url = grid.excelExportUrl + "?" + params;
        Forms.redirect(url);
    }

    export function onExportToCsvClicked(event: Event) {
        var grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
        var params = UrlHelper.getUrlParameters(grid.generateReloadUrl());
        var url = grid.csvExportUrl + "?" + params;
        Forms.redirect(url);
    }

    export function onCopyToClipboardClicked(event: Event) {
        var grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
        var params = UrlHelper.getUrlParameters(grid.generateReloadUrl());
        var url = grid.copyToClipboardUrl + "?" + params;

        var $temp = $("<textarea>").css({
            position: "absolute",
            left: "-1000px"
        });

        $("body").append($temp);

        CommonDialogs.pleaseWait.show();

        $.get(url, (data) => {

            CommonDialogs.pleaseWait.hide();

            CommonDialogs.confirm(Globals.resources.PressOkToContinue, Globals.resources.TextHasBeenCopiedToClipboard, () => {
                $temp.val(data).select();
                document.execCommand("copy");
                $temp.remove();
            });
        });
    }

    export function onViewConfigClicked(this: Element, event: Event) {
        var element = this;
        var grid = Controls.getInstance(GridComponent, element);
        grid.setViewConfig(element.getAttribute("data-view-config") as string);
        var url = UrlHelper.updateUrlParameter(grid.generateReloadUrl(), "view", grid.getViewConfig());
        Forms.redirect(url);
    }

    export function onFilterTagMenuItemClicked(event: JQueryEventObject) {
        var element = Utils.getEventTarget(event);
        var filterCode = $(element).closest("a").first().data("filter-code");
        $(`.filter-menu li[data-filter-code="${filterCode}"] a`)[0].click();
    }

    export function onFilterMenuItemClicked(event: JQueryEventObject) {
        var element = Utils.getEventTarget(event);
        var grid = Controls.getInstance(GridComponent, element);

        var menuItem = $($(element).closest("li")[0]);
        grid.toggleMenuItems(menuItem.data("group-index"), menuItem.data("filter-code"));

        var dropdownMenu = $(element).closest("ul.dropdown-menu");

        var applyFilterButton = dropdownMenu.find(".filter-apply-button");
        var isHintDisplayed = applyFilterButton.length !== 0;

        if (event.ctrlKey && isHintDisplayed) {
            event.stopPropagation();
            applyFilterButton.show();
            dropdownMenu.find(".filter-hint").hide();
        } else {
            grid.reloadPage();
        }
    }

    function onFilterApplyButtonClicked(event: JQueryEventObject) {
        var grid = Controls.getInstance(GridComponent, Utils.getEventTarget(event));
        grid.reloadPage();
    }

    export function onLinkClicked(event: Event) {
        var element = Utils.getEventTarget(event);
        var link = $(element);
        var href = link.attr("href");

        var grid = Controls.getInstance(GridComponent, element);
        var row = link.closest(grid.itemElementSelector);
        var rowData = grid.getRowDataFor<IRow>(row[0]);

        var url = grid.resolveUrlParameters(href, [rowData]);

        Forms.redirect(url);
        Forms.cancelEvent(event);
    }

    function performConfirmedAction(action: () => void, actionElement: JQuery, grid: GridComponent | null): void {
        var isConfirmationRequired = actionElement.data("confirm");

        if (!isConfirmationRequired) {
            action();
        }
        else {
            var messageFormat = actionElement.data("confirm-message");

            var selectedRowsHtml = grid == null
                ? Globals.resources.CurrentRecord
                : grid.getSelectedRowsHtml();

            var message = Strings.format(messageFormat, selectedRowsHtml);
            var title = Globals.resources.ConfirmDialogTitle;
            var acceptButtonText = actionElement.data("confirm-accept-button");
            var rejectButtonText = actionElement.data("confirm-reject-button");

            var eventHandler: Dialogs.IButtonClickedEventHandler = (eventArgs) => {
                if (eventArgs.isOk) {
                    action();
                }
            };

            CommonDialogs.confirm(message, title, eventHandler, undefined, acceptButtonText, rejectButtonText);
        }
    }

    function executeAction(event: MouseEvent | JQueryEventObject, grid: GridComponent | null, actionElement: JQuery, rowData: IRow[] | null) {
        var handler = actionElement.data("handler");

        var action = () => {
            Utils.executeHandler(handler, { grid: grid, event: event, rowData: rowData });
        };

        performConfirmedAction(action, actionElement, grid);
    }

    function navigateAction(event: MouseEvent | JQueryEventObject, grid: GridComponent | null, actionElement: JQuery, rowData: IRow[]) {
        var contextValue = grid == null
            ? $("input[name=context]").val()
            : grid.gridContext;

        if (rowData.length === 0) {
            rowData = grid == null ? rowData : grid.getSelectedRowData<IRow>();
        }

        var url = Grid.resolveUrlParameters(actionElement.data("url"), rowData, contextValue);
        var method = actionElement.data("method");
        var isNewTab = actionElement.data("new-tab") || event.ctrlKey;

        var action = () => {
            Forms.submit(url, method, isNewTab, true);
        };

        performConfirmedAction(action, actionElement, grid);
    }

    export function onInternalButtonClicked(event: MouseEvent | JQueryEventObject) {
        Forms.cancelEvent(event);

        var element = Utils.getEventTarget(event);

        if (Forms.isDisabled(element)) {
            return;
        }

        var clickedElement = $(element);
        var actionElement = clickedElement.closest("[data-action]");

        if (actionElement.length === 0) {
            return;
        }

        var recordId = $("#main-form input[name=\"Id\"]").val();
        var rowData: IRow[] = [];

        if (recordId) {
            rowData = [{
                id: recordId,
                toString: recordId
            }];
        }

        switch (actionElement.data("action")) {
            case "execute":
                executeAction(event, null, actionElement, rowData);
                break;

            case "navigate":
                navigateAction(event, null, actionElement, rowData);
                break;
        }
    }

    function onButtonClicked(event: MouseEvent | JQueryEventObject) {
        Forms.cancelEvent(event);

        var element = Utils.getEventTarget(event);

        if (Forms.isDisabled(element)) {
            return;
        }

        var clickedElement = $(element);
        var actionElement = clickedElement.closest("[data-action]");

        if (actionElement.length === 0) {
            return;
        }

        var grid = Grid.getComponent(actionElement[0]);

        var rowData: IRow[] = [];

        if (grid.isRowDataAvaliable(element)) {
            rowData = [grid.getRowDataFor(element)]
        }

        switch (actionElement.data("action")) {
            case "execute":
                executeAction(event, grid, actionElement, rowData);
                break;

            case "navigate":
                navigateAction(event, grid, actionElement, rowData);
                break;
        }
    }

    function initializeGridActions(context: JQuery) {
        $(".grid", context)
            .find("thead th.grid-column-header[data-column-name]")
            .on("click", onSortByClicked)
            .end()
            .find("tbody tr, .tile")
            .not(".no-click")
            .on("click", onRowClicked)
            .on("keydown", onRowKeyDown)
            .on("dblclick", onRowDoubleClicked)
            .on("doubletap", onRowDoubleClicked);

        $(".pagination [data-target-page]", context)
            .on("click", onPageClicked)
            .on("keydown", onPageKeyDown);

        $(".search-box", context)
            .find("input")
            .on("keydown", onSearchButtonKeyDown)
            .end()
            .find(".search-button-addon")
            .on("click", onSearchButtonClicked);

        $(".filter-menu", context)
            .find(".filter-menu-item")
            .on("click", onFilterMenuItemClicked)
            .end()
            .find(".filter-apply-button")
            .on("click", onFilterApplyButtonClicked);

        $(".grid-toolbar", context)
            .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
            .not("[onclick]")
            .on("click", onButtonClicked);

        $(".row-buttons", context)
            .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
            .not("[onclick]")
            .on("click", onButtonClicked);

        $(".row-commands", context)
            .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
            .not("[onclick]")
            .on("click", onButtonClicked);

        $(".grid-view-menu li[role=\"presentation\"] a", context)
            .on("click", onViewConfigClicked);

        $(".grid", context)
            .find(".tree-item-expander")
            .on("click", onTreeItemExpanding);

        $(".areas-search-addon")
            .on("click", onAreasSearchTriggerClick)
            .find("a")
            .on("click", onAreasSearchOptionClick);
    }

    export class GridComponent extends Controls.Component implements Controls.IPageReloadingControl, Controls.INotifyOnPageReadyControl {

        private filterExpressions: { [filterCode: string]: string };

        private viewconfig: string;

        private get order(): string {
            return this.getInputValue("order");
        }

        private set order(value: string) {
            this.setInputValue("order", value);
        }

        private get page(): number {
            return parseInt(this.getInputValue("page"));
        }

        private set page(value: number) {
            this.setInputValue("page", value);
        }

        public get gridContext(): string {
            return this.getInputValue("context");
        }

        public get excelExportUrl(): string {
            return this.container.data("excel-export-url");
        }

        public get csvExportUrl(): string {
            return this.container.data("csv-export-url");
        }

        public get copyToClipboardUrl(): string {
            return this.container.data("copy-to-clipboard-url");
        }

        public get shouldEnterInvokeDefaultButton(): boolean {
            return this.container.data("enter-invokes-default-button");
        }

        get shouldDoubleClickToggleSelection(): boolean {
            return this.container.data("doubleclick-toggles-selection");
        }

        public get hasRows(): string {
            return this.container.data("has-rows");
        }

        public get displayMode(): GridDisplayMode {
            const mode = this.container.data("display-mode") as keyof typeof GridDisplayMode;

            return GridDisplayMode[mode];
        }

        public get initializeActionName(): string {
            return this.container.data("initialize-action");
        }

        public get itemElementSelector(): string {
            switch (this.displayMode) {
                case GridDisplayMode.Rows:
                case GridDisplayMode.Custom:
                    return "tbody tr:not(.row-label)";

                case GridDisplayMode.Tiles:
                    return "div.tile";

                default:
                    throw "Invalid display mode";
            }
        }

        private get bindingPrefix(): string {
            var prefix = this.container.data("binding-prefix");
            return prefix == null ? "" : prefix;
        }

        private get hasContextMenu(): boolean {
            return this.container.data("has-contextmenu");
        }

        private get allowMultipleRowSelection(): boolean {
            return this.container.data("allow-multiple-row-selection");
        }

        private getHandlerCode(name: string): string | null {
            var handlerCode = this.container.data(name);

            if (Strings.isNullOrEmpty(handlerCode)) {
                return null;
            }

            return handlerCode;
        }

        private get onSelectionChangedHandler(): string | null {
            return this.getHandlerCode("on-selection-changed");
        }

        private get onRowDoubleClickedHandler(): string | null {
            return this.getHandlerCode("on-row-doubleclicked");
        }

        private get onRendered(): string | null {
            return this.getHandlerCode("on-rendered");
        }

        public onExistingUrlRequested: () => string = () => document.URL;

        public onPageReload: (url: string) => void = (url) => window.location.href = url;

        public notifyOnPageReady: (context: JQuery) => void = (context) => {
            this.initFixedHeaderTables(context);
            this.initMenus(context);
            this.initViewActions(context);
            this.initParametricFilters(context);
            this.initFilterTags(context);
            this.initParametricFilterTags(context);
            this.initSearchBox(context);
            context.find(".grid .hide-content").removeClass("hide-content");

            var onRendered = this.onRendered;

            if (onRendered != null) {
                var params = { component: this };
                Utils.executeHandler(onRendered, params);
            }

            initializeGridActions(context);
            this.initDropdownMenus(context);
            this.initSubDropdownMenus(context);
        };

        constructor(rootContainer: JQuery) {
            super(rootContainer);

            this.filterExpressions = {};

            this.addImplementation("IPageReloadingControl");
            this.addImplementation("INotifyOnPageReadyControl");
        }

        public goToPage(page: number): void {
            this.page = page;
            this.reloadPage(false);
        }

        public generateReloadUrl(): string {
            var url: string = this.onExistingUrlRequested();

            this.find("input[data-page-parameter]").each((i, e) => {
                var name = $(e).attr("name");
                var value = $(e).val();
                url = UrlHelper.updateUrlParameter(url, name, value);
            });

            if (this.areFiltersRendered()) {
                var filterExpression = this.buildFilterExpression();
                url = this.removeOldFilterParameters(url, filterExpression);
                url = UrlHelper.updateUrlParameter(url, "filter", filterExpression);
                var parametricFilterExpression = this.buildParametricFilterExpression();
                url = UrlHelper.updateUrlParameters(url, parametricFilterExpression, false);
            }

            var searchAreas: string[] = [];
            this.find("input[data-search-area]:checked").each((i, e) => {
                var name = $(e).attr("name");
                searchAreas.push(name);
            });

            var urlParameters = UrlHelper.getUrlParameters(url);

            if (Strings.isNullOrEmpty(UrlHelper.getQueryParameter(urlParameters, "search"))) {
                url = UrlHelper.removeUrlParametersStartingWith(url, "search-in");
            } else {
                url = UrlHelper.updateUrlParameter(url, "search-in", searchAreas.join());
            }

            url = UrlHelper.removeEmptyUrlParameters(url);
            url = UrlHelper.removeUrlParameterAssignment(url, "page=0");
            url = UrlHelper.removeUrlParameterAssignment(url, "order=id");

            return url;
        }

        public generateExpandUrl(parentid: string): string {
            var url: string = this.onExistingUrlRequested();

            this.find("input[data-page-parameter]").each((i, e) => {
                var name = $(e).attr("name");
                var value = $(e).val();
                url = UrlHelper.updateUrlParameter(url, name, value);
            });

            if (this.areFiltersRendered()) {
                var filterExpression = this.buildFilterExpression();
                url = this.removeOldFilterParameters(url, filterExpression);
                url = UrlHelper.updateUrlParameter(url, "filter", filterExpression);
                var parametricFilterExpression = this.buildParametricFilterExpression();
                url = UrlHelper.updateUrlParameters(url, parametricFilterExpression, false);
            }

            url = UrlHelper.removeEmptyUrlParameters(url);
            url = UrlHelper.removeUrlParametersStartingWith(url, "page");
            url = UrlHelper.removeUrlParameterAssignment(url, "order=id");

            url = UrlHelper.updateUrlParameter(url, "gridexpand", "true");
            url = UrlHelper.updateUrlParameter(url, "parent-id", parentid);

            return url;
        }

        private removeOldFilterParameters(url: string, newFilterExpression: string): string {
            var urlParameters: string = UrlHelper.getUrlParameters(url);
            var oldFilterExpression = UrlHelper.getQueryParameter(urlParameters, "filter") as string;

            var newFilterCodes = this.getFilterCodesFromExpression(newFilterExpression);
            var oldFilterCodes = this.getFilterCodesFromExpression(oldFilterExpression);
            var removedFilterCodes = oldFilterCodes.filter(f => newFilterCodes.indexOf(f) === -1);

            for (var i = 0; i < removedFilterCodes.length; ++i) {
                var prefixToRemove = removedFilterCodes[i] + ".";
                url = UrlHelper.removeUrlParametersStartingWith(url, prefixToRemove);
            }

            return url;
        }

        private getFilterCodesFromExpression(filterExpression: string): string[] {
            var separators = [" ", "\\*", ","];

            return filterExpression
                ? filterExpression.split(new RegExp(separators.join("|")))
                : [];
        }

        private initFixedHeaderTables(context: JQuery): void {
            if (!context.is("body")) {
                return;
            }
            var tables = context.find(".grid .grid-table table");
            const $fixedHeaders = $(".grid-column-header", tables);
            const $tableElement = context.find(".grid .grid-table table");

            $(".grid .grid-table")
                .scroll(() => {
                    const position = $tableElement.position();
                    $fixedHeaders.css("transform", `translate(0, ${position.top * -1}px)`);
                });

            let timeOutId: number;
            $(window).on("resize.sticky", () => {
                if (timeOutId != null) {
                    clearTimeout(timeOutId);
                }
                timeOutId = window.setTimeout(() => {
                    tables.each((index: number, elem: Element) => {
                        $(elem).parent().trigger("resize.stickyTableHeaders");
                    });
                }, 30);
            });
        }

        private initMenus(context: JQuery): void {
            context.find(".row-buttons ul.dropdown-menu").addClass("dropdown-menu-right");

            if (this.hasContextMenu) {
                context.find(this.itemElementSelector).contextmenu({
                    target: `#contextmenu-${this.bindingPrefix}`
                });
            }
        }

        private initDropdownMenus(context: JQuery): void {
            $(context).on("shown.bs.dropdown", function CalculateDropDownVerticalDirection(this: JQuery, jQueryEvent, relatedTargetObject) {
                const $this = $(relatedTargetObject.relatedTarget);
                const $parent = $this.parent();
                const $ul = $parent.children(".dropdown-menu");
                const $button = $parent.children(".dropdown-toggle");
                const ulOffset = $ul.offset();
                const $window = $(window);

                // how much space would be left on the top if the dropdown opened that direction
                var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $window.scrollTop();

                // how much space is left at the bottom
                var spaceDown = $window.scrollTop() + $window.height() - (ulOffset.top + $ul.height());

                // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
                if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown)) {
                    $parent.addClass("dropup");
                }

                const diff = window.innerWidth - $ul.offset().left - $ul.width();

                if (diff < 0 && !$ul.hasClass("pull-right")) {
                    $ul.addClass("pull-right");
                }

            }).on("hidden.bs.dropdown", function (this: JQuery, a, b) {

                // always reset after close
                $(this).removeClass("dropup");
                $(b.relatedTarget).parent().children(".dropdown-menu").removeClass("pull-right");
            });
        }

        private initSubDropdownMenus(context: JQuery): void {
            $(".dropdown-submenu", context).hover(function (this: HTMLElement) {
                const $this = $(this);
                const $dropdownMenu = $this.children(".dropdown-menu");
                const diff = window.innerWidth - $dropdownMenu.offset().left - $dropdownMenu.width();

                if (diff < 0 && !$dropdownMenu.hasClass("dropdown-menu-pull-right")) {
                    $dropdownMenu.addClass("dropdown-menu-pull-right");
                }
            });

            $(".dropdown-menu-pull-right", context).mouseout(function (this: JQuery) {
                $(this).removeClass("dropdown-menu-pull-right");
            });
        }

        private initViewActions(context: JQuery): void {
            const initializeActionName = this.initializeActionName;

            if (!initializeActionName) {
                return;
            }

            const initFunction = Function(`return ${initializeActionName}`)();

            initFunction(this.container, this.getRowData());
        }

        private areFiltersRendered(): boolean {
            return this.find(".filter-menu").length > 0;
        }

        private buildParametricFilterExpression(): string {
            var filterExpression = "";

            this.find(".filter-menu:noparent([data-skip]) li[data-filter-identifier]").each((i, e) => {
                var element = $(e);

                if (!this.isFilterItemSelected(e)) {
                    return;
                }

                var filterCode = element.attr("data-filter-code");
                var expression = this.filterExpressions[filterCode];

                if (expression != null) {
                    if (filterExpression.length > 0) {
                        filterExpression += "&";
                    }

                    filterExpression += expression;
                }
            });

            return filterExpression;
        }

        private buildFilterExpression(): string {
            var groups: string[] = [];
            this.find(".filter-menu:noparent([data-skip]) li").each((i, e) => {
                var element = $(e);
                var groupIndex = element.data("group-index");
                var groupType = element.data("group-type");
                var filterCode = element.data("filter-code");
                var aElement = element.find("a");
                var isChecked = aElement.hasClass("radio") || aElement.hasClass("check");

                if (isChecked) {
                    if (groups[groupIndex] == null) {
                        groups[groupIndex] = filterCode;
                    } else {
                        var separator;

                        if (groupType === "AndCheckboxGroup") {
                            separator = "*";
                        } else if (groupType === "OrCheckboxGroup") {
                            separator = " ";
                        } else {
                            separator = ",";
                        }

                        groups[groupIndex] += separator + filterCode;
                    }
                }
            });

            return groups.filter(x => !Strings.isNullOrWhiteSpace(x)).join(",");
        }

        public reloadPage(resetPage: boolean = true): void {
            if (resetPage) {
                this.page = 0;
            }

            var url = this.generateReloadUrl();
            this.onPageReload(url);
        }

        public setOrderBy(columnName: string, keepExisting: boolean): void {
            var descendingSufix = "-desc";
            if (!keepExisting) {
                var descending = this.order.indexOf(columnName) !== -1
                    && this.order.indexOf(columnName + descendingSufix) === -1;
                var orderMarker: string = (descending ? descendingSufix : "");
                this.order = columnName + orderMarker;
            } else {
                if (this.order.indexOf(columnName) !== -1) {
                    if (this.order.indexOf(columnName + descendingSufix) !== -1) {
                        this.order = this.order.replace(columnName + descendingSufix, columnName);
                    } else {
                        this.order = this.order.replace(columnName, columnName + descendingSufix);
                    }
                } else {
                    if (this.order == null || this.order === "") {
                        this.order = columnName;
                    } else {
                        this.order += "," + columnName;
                    }
                }
            }
        }

        public setViewConfig(config: string) {
            this.viewconfig = config;
        }

        public getViewConfig() {
            return this.viewconfig;
        }

        public onRowClicked(row: JQuery) {
            this.toggleRowSelection(row);
            this.refreshButtonAvailability();
            this.handleSelectionChanged(row);
        }

        public onRowExpanding(row: JQuery) {
            var treeItemId = row.data("tree-item-id");
            var expander = row.find(".tree-item-expander");

            if (expander.hasClass("expander-state-to-load")) {
                expander
                    .removeClass("expander-state-to-load")
                    .removeClass("fa-plus-square-o")
                    .removeClass("fa-minus-square-o")
                    .addClass("fa-refresh")
                    .addClass("fa-spinning");

                var thisRow = row;

                var url = this.generateExpandUrl(treeItemId);
                url = UrlHelper.updateUrlParameter(url, "view", this.getViewConfig());

                $.get(url, (result) => {
                    var updateHtml = $(result).find("#grid-update");
                    var updateScript = $(result).find("#grid-data");
                    var updateChildren = updateHtml.find("tr");

                    updateChildren.hide();

                    thisRow.after(updateChildren);
                    thisRow.append(updateScript.children());

                    //initialize events
                    updateChildren
                        .on("click", onRowClicked)
                        .on("keydown", onRowKeyDown)
                        .on("dblclick", onRowDoubleClicked)
                        .on("doubletap", onRowDoubleClicked);

                    updateChildren
                        .find(".row-buttons")
                        .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
                        .not("[onclick]")
                        .on("click", onButtonClicked);

                    updateChildren
                        .find(".row-commands")
                        .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
                        .not("[onclick]")
                        .on("click", onButtonClicked);

                    updateChildren
                        .find(".tree-item-expander")
                        .on("click", onTreeItemExpanding);

                    var thisRowTwig = thisRow.data("twig") || "";
                    var $thisRowTwigs = thisRow.find(".tree-twig-insert").children();

                    updateChildren
                        .each((i, e) => {
                            var $row = $(e);
                            $row.data("twig", thisRowTwig + $row.data("twig"));

                            var $rowTwigInsert = $row.find(".tree-twig-insert");
                            $rowTwigInsert.append($thisRowTwigs.clone());

                            if (thisRowTwig.length > 0) {
                                var twigCode = thisRowTwig[thisRowTwig.length - 1];
                                $rowTwigInsert.append(`<div class="twig-${twigCode}"></div>`);
                            }
                        });

                    this.refreshRowsCount();
                    this.triggerTableHeaderResize();
                });
            }
            else if (expander.hasClass("expander-state-plus")) {
                this.onRowExpanded(row);
            }
            else if (expander.hasClass("expander-state-minus")) {
                this.onRowCollapsed(row);
            }
        }

        public onRowExpanded(row: JQuery) {
            var treeItemId = row.data("tree-item-id");
            var children = this.find("tr[data-tree-parent-id=\"" + treeItemId + "\"]");

            if (children.length) {
                children.each((i, e) => {
                    this.onRowCollapsed($(e));
                });

                children.show();
            }

            row.find(".tree-item-expander")
                .removeClass("expander-state-plus")
                .addClass("expander-state-minus")
                .removeClass("fa-refresh")
                .removeClass("fa-spinning")
                .removeClass("fa-plus-square-o")
                .addClass("fa-minus-square-o");

            this.refreshRowsCount();
            this.triggerTableHeaderResize();
        }

        public onRowCollapsed(row: JQuery) {
            var treeItemId = row.data("tree-item-id");
            var children = this.find("tr[data-tree-parent-id=\"" + treeItemId + "\"]");

            if (children.length) {
                children.each((i, e) => {
                    this.onRowCollapsed($(e));
                });

                children.hide();
            }

            row.find(".tree-item-expander")
                .removeClass("expander-state-minus")
                .addClass("expander-state-plus")
                .removeClass("fa-refresh")
                .removeClass("fa-spinning")
                .removeClass("fa-minus-square-o")
                .addClass("fa-plus-square-o");

            this.refreshRowsCount();
            this.triggerTableHeaderResize();
        }

        public onRowEnterPressed(row: JQuery) {
            var isChanged = this.setRowSelection(row);
            this.refreshButtonAvailability();

            if (isChanged) {
                this.handleSelectionChanged(row);
            }

            this.handleRowDoubleClick(row);
            this.clickDefaultButton();
        }

        public onRowDoubleClicked(row: JQuery, event: JQueryEventObject) {
            if (this.shouldDoubleClickToggleSelection) {
                this.handleRowDoubleClick(row);

                return;
            }

            var isChanged = this.setRowSelection(row);

            if (isChanged) {
                this.refreshButtonAvailability();
                this.handleSelectionChanged(row);
            }

            if (!this.handleRowDoubleClick(row)) {
                this.clickDefaultButton(event);
            }
        }

        public resolveUrlBasedOnPredicates(urlPredicates: { Predicate: any, RedirectAction: string }[], defaultUrl: string): string | null {
            var selectedRows = this.getSelectedRowData<IRow>();
            var url = this.executeUrlPredicates(urlPredicates, selectedRows);

            if (url == null) {
                return null;
            }

            return this.resolveUrlParameters(url, selectedRows);
        }

        public resolveUrlParameters(url: string, selectedRows: IRow[] | null = null): string {
            var contextValue = this.gridContext;
            selectedRows = selectedRows || this.getSelectedRowData<IRow>();

            return Grid.resolveUrlParameters(url, selectedRows, contextValue);
        }

        public getSelectedRowData<T extends IRow>(): T[] {
            var allRows = this.getRowData<T>();
            var selectedRowIndices = this.getRowSelectionTable();

            return allRows.filter((v, i) => selectedRowIndices[i]);
        }

        public getSelectedRowDisplayNames(): string[] {
            var allRows = this.getRowData<IRow>();
            var selectedRowIndices = this.getRowSelectionTable();

            return allRows
                .filter((v, i) => selectedRowIndices[i])
                .map(r => r.toString);
        }

        public getSelectedRowIds(): string[] {
            var allRows = this.getRowData<IRow>();
            var selectedRowIndices = this.getRowSelectionTable();

            return allRows
                .filter((v, i) => selectedRowIndices[i])
                .map(r => r.id);
        }

        public getSelectedRowsHtml(): string {
            var displayNames = this.getSelectedRowDisplayNames();

            var html = "<ul class=\"selected-items\">";
            html += displayNames.map(n => `<li>${n}</li>`).join("");
            html += "</ul>";

            return html;
        }

        public highlightSelectedItems(selectedIds: any[]) {
            if (!this.hasRows) {
                return;
            }

            var allRows = this.getRowData<IRow>();
            selectedIds = selectedIds.map((id) => id.toString());

            if (selectedIds.length > 0) {
                this.container.find(this.itemElementSelector)
                    .each((i, e) => {
                        var rowData = allRows[i];
                        if (selectedIds.indexOf(rowData.id.toString()) !== -1) {
                            $(e).addClass(highlightClassName);
                        }
                    });
            }
        }

        public getRowData<T extends IRow>(): T[] {
            var data = (window as any)[this.bindingPrefix + "GridData"];

            return <T[]>data;
        }

        public isRowDataAvaliable(element: Element): boolean {
            var row = $(element).closest(this.itemElementSelector);
            var rowIndex = getRowIndex(row);

            return rowIndex !== -1;
        }

        public getRowDataFor<T extends IRow>(element: Element): T {
            var row = $(element).closest(this.itemElementSelector);
            var rowIndex = getRowIndex(row);

            return this.getRowData<T>()[rowIndex];
        }

        private getRowSelectionTable(): boolean[] {
            return this.container.find(this.itemElementSelector)
                .map((i, e) => $(e).hasClass(highlightClassName))
                .toArray();
        }

        private isButtonAvailable(button: Element, data: IRow[]): boolean {
            var mode = $(button).data("availability-mode");

            if (mode === "single-row-selected" && data.length !== 1) {
                return false;
            }

            if (mode === "at-least-one-row-selected" && data.length === 0) {
                return false;
            }

            var availabilityHandler = $(button).data("availability-predicate");

            return this.executePredicate(availabilityHandler, data);
        }

        private executePredicate(predicate: any, data: IRow[]) {
            if (predicate != null) {
                for (var i = 0; i < data.length; i++) {
                    var rowData = data[i];
                    var res = <boolean>Utils.executeHandler(predicate, { row: rowData });
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        }

        private executeUrlPredicates(urlPredicates: { Predicate: any, RedirectAction: string }[], selectedRows: IRow[]): string | null {
            for (var urlPredicate of urlPredicates) {
                var predicate = urlPredicate.Predicate;
                var predicateResult = this.executePredicate(predicate, selectedRows);

                if (predicateResult) {
                    return urlPredicate.RedirectAction;
                }
            }

            return null;
        }

        private toggleRowSelection(row: JQuery) {
            var isSelected = row.hasClass(highlightClassName);

            if (!isSelected) {
                row.addClass(highlightClassName);
            } else {
                row.removeClass(highlightClassName);
            }

            if (!this.allowMultipleRowSelection) {
                this.container.find(this.itemElementSelector + "." + highlightClassName).each((i, e) => {
                    if (row[0] !== e) {
                        $(e).removeClass(highlightClassName);
                    }
                });
            }
        }

        private setRowSelection(row: JQuery): boolean {
            var isSelected = row.hasClass(highlightClassName);
            var isChanged = false;

            if (!isSelected) {
                row.addClass(highlightClassName);
                isChanged = true;
            }

            this.container.find(this.itemElementSelector + "." + highlightClassName).each((i, e) => {
                if (row[0] !== e) {
                    $(e).removeClass(highlightClassName);
                    isChanged = true;
                }
            });

            return isChanged;
        }

        private handleSelectionChanged(row: JQuery) {
            var eventHandler = this.onSelectionChangedHandler;

            if (eventHandler == null) {
                return;
            }

            var isRowSelected = row.hasClass(highlightClassName);
            var rowIndex = getRowIndex(row);
            var rowData = this.getRowData<IRow>()[rowIndex];

            var params = {
                grid: this,
                row: row[0],
                rowData: rowData,
                isSelected: isRowSelected
            };

            Utils.executeHandler(eventHandler, params);
        }

        private handleRowDoubleClick(row: JQuery) {
            var eventHandler = this.onRowDoubleClickedHandler;

            if (eventHandler == null) {
                return false;
            }

            var isRowSelected = row.hasClass(highlightClassName);
            var rowIndex = getRowIndex(row);
            var rowData = this.getRowData<IRow>()[rowIndex];

            var params = {
                grid: this,
                row: row[0],
                rowData: rowData,
                isSelected: isRowSelected
            };

            Utils.executeHandler(eventHandler, params);

            return true;
        }

        private refreshButtonAvailability() {
            var data = this.getSelectedRowData<IRow>();
            this.find("*[data-availability-mode]").each((i, e) => {
                var elementWithModifier = $(e).find("[data-layout-modifier]");
                if (elementWithModifier.length > 0) {
                    var layoutModifiedHandler = elementWithModifier.data("layout-modifier");
                    Utils.executeHandler(layoutModifiedHandler, { rows: data, e: e });
                }

                var result = this.isButtonAvailable(e, data);
                if (!result) {
                    $(e).addClass("disabled");
                    $(e).attr("disabled", "disabled");
                } else {
                    $(e).removeClass("disabled");
                    $(e).removeAttr("disabled");
                }
            });
        }

        private refreshRowsCount() {
            var visibleRowsCount = this.find("tbody tr:visible").length;
            var recordCountElement = $(".record-count-footer");

            if (recordCountElement != null && recordCountElement.length > 0) {
                recordCountElement.text(Strings.format(Globals.resources.RecordCountFormat, visibleRowsCount));
            }
        }

        private triggerTableHeaderResize() {
            $(window).trigger("resize.sticky");
        }

        private clickDefaultButton(event?: JQueryEventObject) {
            var buttons = this.find("[data-default-button='true']")
                .not(".disabled")
                .filter((index, element) => {
                    return !Forms.isDisabled(element);
                });

            var e = Forms.propagateClickEvent(event);
            buttons.eq(0).trigger(e);
        }

        private initParametricFilters(context: JQuery) {
            var menuItems = context.find(".filter-menu:noparent([data-skip]) li[data-filter-identifier]");

            menuItems.each((i, m) => {
                if (this.isFilterItemSelected(m) && !this.isFilterAlwaysOn(m)) {
                    $(m).find("a").click(Grid.onFilterMenuItemClicked);
                } else {
                    $(m).find("a").click(() => this.handleDialogForParametricFilter(m, true));
                }
            });
        }

        private initFilterTags(context: JQuery) {
            var tags = context.find("#filter-tags i");
            tags.each((i, m) => {
                $(m)
                    .click(() => {
                        var menuItems = context.find(".filter-menu:noparent([data-skip]) li[data-filter-code]");
                        menuItems.each((j, g) => {
                            if ($(g).attr("data-filter-code") === $(m).attr("data-filter-code")) {
                                $(g).find("a").click();
                            }
                        });
                    });
            });

            $(".filter-tag-group")
                .find(".filter-tag-menu-item")
                .on("click", onFilterTagMenuItemClicked);
        }

        private initParametricFilterTags(context: JQuery) {
            var tags = context.find("#filter-tags .filter-tag");

            tags.each((i, m) => {
                $(m)
                    .click(() => {
                        var menuItems = context.find(".filter-menu:noparent([data-skip]) li[data-filter-identifier]");
                        menuItems.each((j, g) => {

                            if (this.isFilterItemSelected(g) || this.isFilterAlwaysOn(g)) {

                                if ($(g).attr("data-filter-code") === $(m).closest("[data-filter-code]").attr("data-filter-code")) {
                                    this.handleDialogForParametricFilter(g, false);
                                }
                            }
                        });
                    });

            });
        }

        private initSearchBox(context: JQuery) {
            let $input = $(".search-box input", context);

            if ($input.length && document.location.search) {
                $input.select();
            }
        }

        private isFilterItemSelected(filterItem: Element): boolean {
            var anchor = $(filterItem).find("a")[0];

            return $(anchor).hasClass("radio") || $(anchor).hasClass("check");
        }

        private isFilterAlwaysOn(filterItem: Element): boolean {
            var anchor = $(filterItem).find("a")[0];

            return $(anchor).hasClass("always-on");
        }

        private handleDialogForParametricFilter(filterMenuItem: Element, withToogle: boolean) {
            var filterViewModelIdentifier = $(filterMenuItem).attr("data-filter-identifier");
            var filterViewModelDialogTitle = $(filterMenuItem).attr("data-filter-dialog-title");

            var mappings = this.getFilterParameterMapping(filterMenuItem);
            var parameters: any = {};

            var urlParameters = UrlHelper.getUrlParameters(Forms.getCurrentUrl());

            for (var key in mappings.toViewModelPropertyMapping) {
                if (mappings.toViewModelPropertyMapping.hasOwnProperty(key)) {
                    var paramValue = UrlHelper.getQueryParameter(urlParameters, key);

                    if (paramValue !== null) {
                        var mapping = mappings.toViewModelPropertyMapping[key];
                        parameters[mapping] = paramValue;
                    }
                }
            }

            var modelUrlParams = jQuery.param(parameters);

            Dialogs.showModel({
                modelId: filterViewModelIdentifier,
                title: filterViewModelDialogTitle,
                eventHandler: (eventArgs) => { this.handleFilterDialogEvent(filterMenuItem, eventArgs, withToogle); },
                iconName: "",
                standardButtons: Dialogs.StandardButtons.OkCancel,
                modelUrlParameters: modelUrlParams
            });
        }

        private handleFilterDialogEvent(filterMenuItem: Element, eventArgs: Dialogs.EventArgs, withToogle: boolean) {
            CommonDialogs.pleaseWait.hide();
            var allowNoParameterValues: boolean = $(filterMenuItem).data("allow-no-parameter-values");

            if (eventArgs.isOk && (!eventArgs.isModelEmpty || allowNoParameterValues)) {
                var mappings = this.getFilterParameterMapping(filterMenuItem);
                var filterCode = $(filterMenuItem).attr("data-filter-code");
                var parameters: any = {};

                for (var key in mappings.toUrlFieldNameMapping) {
                    if (mappings.toUrlFieldNameMapping.hasOwnProperty(key)) {
                        var mapping = mappings.toUrlFieldNameMapping[key];
                        parameters[mapping] = eventArgs.model[key];
                    }
                }

                this.filterExpressions[filterCode] = jQuery.param(parameters);
                if (withToogle) {
                    this.toggleMenuItem(filterMenuItem);
                }

                this.reloadPage();
            }
        }

        private getFilterParameterMapping(filterMenuItem: Element): FilterParameterMappings {

            var mappings = new FilterParameterMappings();

            for (var i = 0; i < filterMenuItem.attributes.length; i++) {
                var attribute = filterMenuItem.attributes[i];
                mappings.addFromAttribute(attribute);
            }

            return mappings;
        }

        public toggleMenuItems(groupIndex: String, filterCode: String) {
            $(`li[data-group-index="${groupIndex}"][data-filter-code="${filterCode}"]`).each((i, e) => {
                this.toggleMenuItem(e);
            })
        }

        public toggleMenuItem(menuItem: Element) {
            var jMenuItem = $(menuItem);
            var aElement = jMenuItem.find("a");

            var groupIndex = jMenuItem.data("group-index");
            var groupType = jMenuItem.data("group-type");

            if (groupType === "RadioGroup") {
                var aElementParent = aElement.parents('.dropdown-menu');

                if (aElementParent.find(`li[data-group-index="${groupIndex}"] a.always-on`).length === 0 || aElement.hasClass("always-on")) {
                    aElementParent.find(`li[data-group-index="${groupIndex}"] a`).removeClass("radio");
                    aElement.addClass("radio");
                }
            } else {
                if (!aElement.hasClass("always-on") || !aElement.hasClass("check")) {
                    aElement.toggleClass("check");
                }
            }
        }
    }

    class FilterParameterMapping {
        public urlFieldName: string;
        public viewModelPropertyName: string;

        constructor(urlFieldName: string, viewModelPropertyName: string) {
            this.urlFieldName = urlFieldName;
            this.viewModelPropertyName = viewModelPropertyName;
        }
    }

    class FilterParameterMappings {
        private filterParameterAttributePrefix = "data-filter-param-";
        private filterParameterMappings: FilterParameterMapping[];
        public toUrlFieldNameMapping: { [viewModelName: string]: string }
        public toViewModelPropertyMapping: { [urlFieldName: string]: string }

        constructor() {
            this.filterParameterMappings = new Array<FilterParameterMapping>();
            this.toUrlFieldNameMapping = {};
            this.toViewModelPropertyMapping = {};
        }

        public addFromAttribute(attribute: Attr) {
            var prefixMatch = new RegExp("^" + this.filterParameterAttributePrefix);

            if (!attribute.name.match(prefixMatch)) {
                return;
            }

            var parameterUrlFieldName = attribute.name.replace(prefixMatch, "");
            var parameterViewModelName = attribute.value;

            this.filterParameterMappings.push(new FilterParameterMapping(parameterUrlFieldName, parameterViewModelName));
            this.toUrlFieldNameMapping[parameterViewModelName] = parameterUrlFieldName;
            this.toViewModelPropertyMapping[parameterUrlFieldName] = parameterViewModelName;
        }
    }

    export function onTreeItemExpanding(event: JQueryEventObject) {

        event.stopPropagation();

        if (!shouldHandleMouseEvent(event)) {
            return;
        }

        var element = Utils.getEventTarget(event);
        var clickedElement = $(element);

        var grid = Controls.getInstance(GridComponent, element);
        var row = clickedElement.closest(grid.itemElementSelector);

        grid.onRowExpanding(row);
        $(window).trigger("resize.sticky");
    }

    export function onAreasSearchOptionClick(event: JQueryEventObject) {
        var $target = $(event.currentTarget);
        var $input = $target.find("input");

        setTimeout(() => {
            $input.prop("checked", !$input.prop("checked"));
            $input.closest("a").toggleClass("check");
        });
        $(event.target).blur();
        event.preventDefault();
    }

    export function onAreasSearchTriggerClick(event: JQueryEventObject) {
        var searchTriggerElement = $(event.currentTarget);
        var isOpen = searchTriggerElement.hasClass("open");
        var clickedElement = $(event.target);
        var isBubbling = !clickedElement.hasClass("areas-search-addon");

        if (isBubbling || isOpen) {
            return;
        }

        setTimeout(() => {
            $(".areas-search-addon .dropdown-toggle")
                .dropdown("toggle");
        });
    }

    export function mergeGridData(parentIds: any, originalData: any, dataToMerge: any) {
        parentIds.forEach((parentid: string) => {
            $(`tr[data-tree-item-id="${parentid}"]`).each((i, row) => {
                var grid = Controls.getInstance(GridComponent, row);
                var mergedData = new Array<any>();

                $.each(dataToMerge, (i, item) => {
                    if (item.parentId === parentid) {
                        mergedData.push(item);
                    }
                });

                var rowIndex = getRowIndex($(row));

                for (var j = 0; j < mergedData.length; j++) {
                    originalData.splice(rowIndex + 1 + j, 0, mergedData[j]);
                }

                grid.onRowExpanded($(row));
                $(row).find("script").remove();
            });
        });
    }
}
