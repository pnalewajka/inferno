﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>

module HamburgerButtons {
    function resizeBurgerBox(box: JQuery) {
        var burgerButtonWidth = 50;

        var slices = box.find('.burger-slices');
        var allowedWidth = slices.width() - burgerButtonWidth;

        var menuButton = box.find('.burger-button');
        var burgerMenu = box.find('.burger-button ul.dropdown-menu');
        var buttonGroups = slices.children('.btn-group');
        var sliceWidth = 0;
        var hasHiddenGroups = false;

        buttonGroups.finish();

        buttonGroups.each((index, groupElement) => {
            var buttonGroup = $(groupElement);

            if (buttonGroup.css("display") === "hidden") {
                buttonGroup
                    .css('visibility', 'hidden')
                    .css('display', 'inline-block');
            }

            sliceWidth += buttonGroup.outerWidth();

            if (sliceWidth > allowedWidth) {
                buttonGroup
                    .hide('slow')
                    .css('visibility', '')
                    .attr('data-skip', 'yes');

                burgerMenu.find('li[data-group-index=' + index + ']')
                    .css('display', 'list-item')
                    .removeAttr('data-skip');

                hasHiddenGroups = true;
            } else {
                buttonGroup
                    .css('display', 'inline-block')
                    .css('visibility', 'visible')
                    .removeAttr('data-skip');

                burgerMenu.find('li[data-group-index=' + index + ']')
                    .css('display', 'none')
                    .attr('data-skip', 'yes');
            }
        });

        if (hasHiddenGroups) {
            menuButton.show('slow');
        }
        else {
            menuButton.css('display', 'none');
        }
    }

    function resizeBurgerBoxes(context: JQuery) {
        context.find(".burger-box").each((index, box) => {
            resizeBurgerBox($(box));
        });
    }

    function initializeWindowResizeHandlers() {
        $(window).on("resize.atomic.burgers", () => {
            resizeBurgerBoxes($("body"));
        });
    }

    function copyButtonAttributes(button: JQuery, li: JQuery) {
        $.each(button[0].attributes, (i, attribute) => {
            if (attribute.specified) {
                var name = attribute.name;
                var value = attribute.value;

                if (Strings.startsWith(name, "data-") || Strings.startsWith(name, "on")) {
                    li.attr(name, value);
                }
            }
        });

        if (button.is(":disabled")) {
            li.attr("disabled", "disabled");
            li.addClass("disabled");
        }
    }

    function addBurgerMenuItem(burgerMenu: JQuery, button: JQuery, groupIndex: number) {
        var li = $('<li data-group-index="' + groupIndex + '"></li>');
        var a = $('<a href="#"></a>');

        a.appendTo(li);
        a.html(button.html());

        copyButtonAttributes(button, li);

        li.appendTo(burgerMenu);
    }

    function addBurgerSubMenu(burgerMenu: JQuery, button: JQuery, menuList: JQuery, groupIndex: number) {
        var li = $('<li class="dropdown-submenu" data-group-index="' + groupIndex + '"></li>');
        var a = $('<a href="#"></a>');

        a.appendTo(li);
        a.html(button.html());
        a.find(".caret").remove();

        copyButtonAttributes(button, li);
        li.attr("data-toggle", "collapse");
        li.attr("data-parent", "#toolbar-burger-menu");

        li.append($(menuList[0].outerHTML));

        let id = `${button.attr("id")}-submenu`;
        li.find('ul').attr("id", id);
        li.attr("data-target", `#${id}`);

        li.find('.dropdown-menu a').click(function (this: JQuery, event: Event) {
            burgerMenu.dropdown('toggle');
        });

        li.appendTo(burgerMenu);

        a.click(function (this: JQuery, event: Event) {
            event.stopPropagation();
        });
    }

    function initializeBurgerBox(box: JQuery) {
        var slices = box.find('.burger-slices');
        slices.on('show.bs.dropdown', () => {
            slices.css('overflow', 'visible');
        });
        slices.on('hidden.bs.dropdown', () => {
            slices.css('overflow', 'hidden');
        });

        $('.burger-button').on('show.bs.dropdown', function (this: JQuery, event: Event) {
            $(this).find('#toolbar-burger-menu .dropdown-menu.in').collapse('hide');
        });

        var burgerMenu = box.find('.burger-button ul.dropdown-menu');
        slices.children('.btn-group').each((groupIndex, element) => {

            if (groupIndex > 0) {
                burgerMenu.append($('<li class="divider" data-group-index="' + (groupIndex - 1) + '"></li>'));
            }

            var buttonGroup = $(element);
            buttonGroup.data('group-index', groupIndex);

            var groupItems = buttonGroup.children();

            if (groupItems.length === 2 && groupItems.eq(1).hasClass('dropdown-menu')) {
                // un-nested dropdown button
                addBurgerSubMenu(burgerMenu, groupItems.eq(0), groupItems.eq(1), groupIndex);
            } else {
                // 'normal' button group
                groupItems.each((subIndex, subElement) => {
                    var item = $(subElement);
                    if (item.hasClass('btn-group') || item.hasClass('dropdown')) {
                        var subItems = item.children();
                        addBurgerSubMenu(burgerMenu, subItems.eq(0), subItems.eq(1), groupIndex);
                    }
                    else {
                        addBurgerMenuItem(burgerMenu, item, groupIndex);
                    }
                });
            }
        });

        $("#toolbar-burger-menu li.dropdown-submenu > a").click((ev) => {
            ev.stopPropagation();
        });
    }

    function initializeBurgerBoxes(context: JQuery) {
        context.find(".burger-box").each((index, box) => {
            initializeBurgerBox($(box));
        });
    }

    export function initMenus(context: JQuery, isFromDialog: boolean = false): void {
        var initialBurgerBoxResizeDelay = 500;

        if (!isFromDialog) {
            initializeWindowResizeHandlers();
        }

        initializeBurgerBoxes(context);

        window.setTimeout(() => {
            resizeBurgerBoxes(context);
        }, initialBurgerBoxResizeDelay);
    }
} 
