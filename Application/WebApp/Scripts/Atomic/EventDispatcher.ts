﻿module Atomic.Events {
    type Callback<TEvent> = (event: TEvent) => void;

    export class EventDispatcher<TEvent>
    {
        protected listeners: Callback<TEvent>[];

        public constructor() {
            this.listeners = [];
        }

        public subscribe(listener: Callback<TEvent>): void {
            this.listeners.push(listener);
        }

        public unsubscribe(listener: Callback<TEvent>): void {
            this.listeners = this.listeners.filter(c => c !== listener);
        }

        public dispatch(event: TEvent): void {
            for (let listener of this.listeners) {
                listener(event);
            }
        }
    }

    export class AsyncEventDispatcher<TEvent> extends EventDispatcher<TEvent>
    {
        public dispatch(event: TEvent): void {
            for (let listener of this.listeners) {
                Utils.pushBackBrowserTask(() => listener(event));
            }
        }
    }
}
