/// <reference path="../typings/jquery/jquery.d.ts"/>

module Repeater {
    export class RepeaterManager extends Controls.Component {
        private itemTitle: string;
        private minCount: number;
        private maxCount: number;
        private canDeleteRecord: boolean;
        private canReorder: boolean;
        private items: JQuery;
        private template: JQuery;
        private addButton: JQuery;
        private static positionBoundAttributes: string[] = [
            'id',
            'for',
            'name',
            'data-valmsg-for',
            'data-field-name',
            'data-field-id',
            'aria-describedby'
        ];

        constructor(element: Element) {
            super($(element));
            this.itemTitle = this.container.data('item-title');
            this.minCount = parseInt(this.container.data('min-count'));
            this.maxCount = parseInt(this.container.data('max-count'));
            this.canDeleteRecord = this.container.data('can-delete-record');
            this.canReorder = this.container.data('can-reorder');
            this.template = this.container.find('> .repeater-items fieldset.hidden');

            this.addButton = this.container.find('> .repeater-toolbar .add');
            this.addButton.click(() => { this.onAddRecordClicked(); });

            this.adjustItemButtons();
        }

        private itemCount(): number {
            return this.items.length;
        }

        private adjustAddButtonVisibility(): boolean {
            if (this.maxCount > 0 && this.itemCount() >= this.maxCount) {
                this.addButton.hide();

                return false;
            } else {
                this.addButton.show();

                return true;
            }
        }

        private adjustItemButtons(): void {
            this.items = this.container.find('> .repeater-items fieldset:not(.hidden).repeater-item');

            var itemCount = this.itemCount();

            this.items.each((index, item) => {
                var element = $(item);
                var deleteButton = element.find('> .item-header .delete');
                var upButton = element.find('> .item-header .up');
                var downButton = element.find('> .item-header .down');

                deleteButton.off().click(b => { this.onDeleteRecordClicked($(b.target).closest('fieldset')); });
                upButton.off().click(b => { this.onMoveRecordUpClicked($(b.target).closest('fieldset')); });
                downButton.off().click(b => { this.onMoveRecordDownClicked($(b.target).closest('fieldset')); });

                this.adjustButtonVisibility(upButton, index === 0);
                this.adjustButtonVisibility(downButton, index === (itemCount - 1));
            });

            this.adjustAddButtonVisibility();
        }

        private adjustButtonVisibility(actionButton: JQuery, shouldHide: boolean): void {
            if (shouldHide) {
                actionButton.addClass('hidden');
            } else {
                actionButton.removeClass('hidden');
            }
        }

        private onAddRecordClicked(): void {

            if (this.maxCount == 0 || (this.maxCount > 0 && this.itemCount() < this.maxCount)) {
                var item = this.template.clone();

                RepeaterManager.positionBoundAttributes.forEach((value) => {
                    this.replaceAttributeValues(item, value, 'template.', '');
                });

                this.adjustPositionAttribute(item, this.itemCount());

                item.removeClass('hidden');
                this.container.find("> .repeater-items").append(item);

                this.adjustItemButtons();

                this.container.trigger("change");

                Forms.init($(item), false, true);
            }

            this.adjustAddButtonVisibility();
        }

        private adjustPositionAttribute(container: JQuery, newPosition: number, oldPosition?: number) {
            var oldSearchValue = oldPosition != undefined
                ? '[' + oldPosition + ']'
                : /\[[0-9]*\]/;

            var newPositionString = '[' + newPosition + ']';

            RepeaterManager.positionBoundAttributes.forEach((value) => {
                this.replaceAttributeValues(container, value, oldSearchValue, newPositionString);
            });

            container.data('position', newPosition);
        }

        private replaceAttributeValues(container: JQuery, attributeName: string, searchValue: string | RegExp, newValue: string): void {
            container.find('[' + attributeName + ']').each((index, element) => {
                var jQueryElement = $(element);
                var value = jQueryElement.attr(attributeName);

                value = value.replace(searchValue, newValue);

                jQueryElement.attr(attributeName, value);
            });
        }

        private repositionItems(): void {

            this.adjustItemButtons();

            this.items.each((index, item) => {
                var container = $(item);
                var position = container.data('position');

                this.adjustPositionAttribute($(container), index, position);
            });

            this.adjustItemButtons();
        }

        private onDeleteRecordClicked(element: JQuery): void {
            element.hide(300, () => {
                element.remove();

                this.repositionItems();

                this.container.trigger("change");

                this.adjustAddButtonVisibility();
            });
        }

        private onMoveRecordUpClicked(element: JQuery): void {
            element.prev('fieldset').before(element);

            this.repositionItems();
        }

        private onMoveRecordDownClicked(element: JQuery): void {
            element.next('fieldset').after(element);

            this.repositionItems();
        }

        private validateMin(): boolean {
            var itemCount = this.itemCount();

            if (this.minCount === 0) {
                return true;
            }

            if (itemCount === 0 || itemCount < this.minCount) {

                return false;
            }

            return true;
        }

        private validateMax(): boolean {
            var itemCount = this.itemCount();

            if (this.maxCount > 0 && itemCount > this.maxCount) {
                return false;
            }

            return true;
        }


        public static registerCustomValidation(): void {
            $.validator.addMethod('repeatercontainermin', (value, element) => {
                var repeaterManager = Controls.getExistingInstance(RepeaterManager, element);

                return repeaterManager.validateMin();
            });

            $.validator.unobtrusive.adapters.addBool('repeatercontainermin');

            $.validator.addMethod('repeatercontainermax', (value, element) => {
                var repeaterManager = Controls.getExistingInstance(RepeaterManager, element);

                return repeaterManager.validateMin();
            });

            $.validator.unobtrusive.adapters.addBool('repeatercontainermax');
        }
    }
}