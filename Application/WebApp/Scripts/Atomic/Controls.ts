﻿module Controls {
    function getRootHtmlContainer(innerElement: any): JQuery {
        var rootElement = $(innerElement).closest("*[data-component]");

        if (rootElement.length === 1) {
            return rootElement;
        }

        var delegationElement = $(innerElement).closest("*[data-component-ref]");

        if (delegationElement.length !== 1) {
            throw "Root element not found for given component";
        }

        var componentId = delegationElement.data("component-ref");
        rootElement = $("*[data-component][data-component-id='" + componentId + "']");

        return rootElement;
    }

    export function getInstance<T extends Component>(type: { new (rootContainer: JQuery): T; }, innerElement: Element): T {
        var instanceDataAtrribute = "component-instance";
        var rootContainer = getRootHtmlContainer(innerElement);
        var instance: T = <T>rootContainer.data(instanceDataAtrribute);

        if (instance == null) {
            instance = new type(rootContainer);
            rootContainer.data(instanceDataAtrribute, instance);
        }

        return instance;
    }

    export function getExistingInstance<T extends Component>(type: { new (element: Element): T; }, innerElement: Element): T {
        var instanceDataAtrribute = "component-instance";
        var rootContainer = getRootHtmlContainer(innerElement);
        var instance: T = <T>rootContainer.data(instanceDataAtrribute);

        return instance;
    }


    export function getInstanceByName(name: string, rootContainer: JQuery): any {
        var instanceDataAtrribute = "component-instance";
        var instance = rootContainer.data(instanceDataAtrribute);

        if (instance == null) {
            var ctor = Utils.getFunctionByName(name);
            instance = new ctor(rootContainer); 
            rootContainer.data(instanceDataAtrribute, instance);
        }

        return instance;
    }

    export function getAllInstances(root: JQuery, interfaceName: string | null = null): Component[] {
        if (interfaceName == null) {
            var allComponents = $(root).find("*[data-component]").map((i, e) => getInstanceByName($(e).data("component"), $(e)));
            return <Component[]>allComponents.toArray();
        } else {
            return getAllInstances(root).filter((i) => i.isImplementationOf(interfaceName));
        }
    }

    export class Component {
        public container: JQuery;
        private implementedInterfaces: string;

        constructor(rootContainer: any) {
            this.container = rootContainer;
            this.implementedInterfaces = "";
        }

        public getInputValue(name: string): string {
            return this.find("input[name='" + name+ "']").val();
        }

        public setInputValue(name: string, value: any): void {
            this.find("input[name='" + name + "']").val(value.toString());
        }

        public find(selector: string): JQuery {
            var inner = this.container.find(selector);

            var componentId = this.container.data('component-id');
            var outer = $("*[data-component-ref='" + componentId + "']").find(selector);

            return inner.add(outer);
        }

        public addImplementation(interfaceName: string) {
            this.implementedInterfaces += interfaceName + ";";
        }

        public isImplementationOf(interfaceName: string): boolean {
            if (this.implementedInterfaces == null) {
                return false;
            }

            return this.implementedInterfaces.indexOf(interfaceName + ";") !== -1;
        }
    }

    export interface IPageReloadingControl {
        onExistingUrlRequested: () => string;
        onPageReload: (url: string) => void;
    }

    export interface INotifyOnPageReadyControl {
        notifyOnPageReady: (context: JQuery) => void;
    }

    export class ImageSize {
        public width: number;
        public height: number;
        constructor(width: number, height: number) {
            this.width = width;
            this.height = height;
        }

        public getScaledSize(maxDimension: number): ImageSize {
            var largerDimension = Math.max(this.width, this.height);

            if (largerDimension > maxDimension) {
                var scale = maxDimension / largerDimension;

                return new ImageSize(Math.round(this.width * scale), Math.round(this.height * scale));
            }

            return new ImageSize(this.width, this.height);
        }
    }
}
