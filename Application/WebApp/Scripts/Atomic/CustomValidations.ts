﻿/// <reference path="../typings/jquery.validation/jquery.validation.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>

module CustomValidations {
    function registerCustomSelectors(): void {
        // register ':noparent(some-selector)' selector - to assure that an element is not included in an unwanted parent
        $.expr[':'].noparent = (element: Element, index: number, metaInformation: any[]) => {
            // meta information contains: full selector, only selector, quotes used, parameters
            // here, the parameter is the 'predicate selector' we want to use for parent filtering
            return $(element).parents(metaInformation[3]).length < 1;
        };
    }

    function registerCannotBeEmpty() {
        $.validator.addMethod('hasSelectedValue', (value) => (value != null && value !== ''), '');

        // and an unobtrusive adapter
        $.validator.unobtrusive.adapters.add('cannotbeempty', {}, (options: any) => {
            options.rules['hasSelectedValue'] = true;
            options.messages['hasSelectedValue'] = options.message;
        });
    }

    function registerModuleValidations(): void {
        DocumentUpload.DocumentPicker.registerCustomValidation();
        Repeater.RepeaterManager.registerCustomValidation();
        MapPicker.registerCustomValidation();
    }

    function registerCustomPresentation(): void {
        MapPicker.registerCustomPresentation();
    }

    export function init(): void {
        $.validator.setDefaults({ ignore: ":hidden:not(.hidden-but-validate)" });

        registerCannotBeEmpty();
        registerModuleValidations();
        registerCustomSelectors();
        registerCustomPresentation();
    }
};