﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>
/// <reference path="../Atomic/Globals.ts"/>

module Help {
    export class HelpResource {
        public path: string;
        public link: string;
    }

    export class HelpResourcePage {
        public url: string;
        public resources: Array<HelpResource>;
    }

    export class HelpResources {
        public pages: Array<HelpResourcePage>;
    }

    declare var helpResources: HelpResources;

    var isResourceScriptLoaded: boolean = false;

    function getHelpBoxContainer(context: JQuery, resource: HelpResource, isDialog: boolean): JQuery {
        var path: string;
        var parentCount = 0;
        var parentSelector = "!parent";

        if (Strings.isNullOrEmpty(resource.path)) {
            if (isDialog) {
                path = ".modal-header h4";
            } else {
                path = ".breadcrumb>li:last-child(),.breadcrumb-container>li:last-child()";
            }
        } else {
            path = resource.path;

            while (Strings.endsWith(path, parentSelector)) {
                parentCount++;
                path = path.substr(0, path.length - parentSelector.length);
            }
        }

        var container = context.find(path);

        while (parentCount > 0) {
            parentCount--;
            container = container.parent();
        }

        return container;
    }

    function getHelpBoxClassInsert(resource: HelpResource, isDialog: boolean) {
        return !isDialog && resource.path === ""
            ? " page-level"
            : "";
    }

    function initHelpControl(context: JQuery, resource: HelpResource, isDialog: boolean) {
        var container = getHelpBoxContainer(context, resource, isDialog);

        container.remove(".help-box");

        if (Strings.isNullOrEmpty(resource.link)) {
            return;
        }

        var classInsert = getHelpBoxClassInsert(resource, isDialog);

        $(`<a class='help-box${classInsert}' href='${resource.link}' target='_blank'><i class='fa fa-info-circle'></i></a>`)
            .appendTo(container).click(e => e.stopPropagation());
    }

    function isMatchingPage(page: HelpResourcePage, currentPageUrl: string) {
        return Strings.startsWithLowerCase(currentPageUrl, page.url);
    }

    function findPageResources(helpResources: HelpResources, currentPageUrl: string): Array<HelpResource> {
        const matchingPages = helpResources.pages.filter(page => isMatchingPage(page, currentPageUrl));

        return matchingPages.reduce((previousValue, currentValue) => previousValue.concat(currentValue.resources), [] as Array<HelpResource>);
    }

    function initHelpControls(context: JQuery, isDialog: boolean): void {
        const referenceUrl = isDialog
            ? context.find("form").attr("action")
            : UrlHelper.getAbsolutePath(Forms.getCurrentUrl());

        const resources = findPageResources(helpResources, referenceUrl);

        for (var i = 0; i < resources.length; i++) {
            initHelpControl(context, resources[i], isDialog);
        }
    }

    export function initResources(context: JQuery, isDialog: boolean) {
        if (!isResourceScriptLoaded) {
            var scriptUrl = Globals.resolveUrl("~/Content/help/resources.js");

            $.getScript(scriptUrl, () => {
                isResourceScriptLoaded = true;
                initHelpControls(context, isDialog);
            });
        } else {
            initHelpControls(context, isDialog);
        }
    }

    export function openPageLevelLink() {
        var pageLevelHelpBox = $(".help-box.page-level");

        if (pageLevelHelpBox.length === 0) {
            return;
        }

        pageLevelHelpBox.click();
    }
} 
