﻿module Print {
    export function init(): void {
        window.print();
    }
}

$(document).ready(() => {
    Print.init();
});