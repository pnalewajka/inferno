﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>
/// <reference path="../Atomic/Globals.ts"/>

module Forms {
    declare var ace: any;
    declare var CKEDITOR: any;
    declare var color: any;

    var isAceEditorScriptLoaded: boolean = false;
    var isCkEditorScriptLoaded: boolean = false;
    export var onCkEditorInit: () => void = () => { };
    var datePickerLocaleLoad: Promise<void> | null = null;
    var areDocumentHotkeyReady: boolean = false;
    var returnUrlFieldName = "Atomic.ReturnUrl";
    var isDirty: boolean = false;

    export const returnPointQueryParamName = "return-point";

    function initComponents(context: JQuery): void {
        context.find("*[data-component]").map((i, e) => Controls.getInstanceByName($(e).data("component"), $(e)));
        // we depend on the fact that all instances will be assured by this call
        // not only of the specified interface
        var onPageReadys = Controls.getAllInstances(context, "INotifyOnPageReadyControl");

        onPageReadys.forEach((c) => {
            var component = <Controls.INotifyOnPageReadyControl>(<any>c);
            component.notifyOnPageReady(context);
        });
    }

    function initMainMenu(context: JQuery, isFromDialog: boolean) {
        if (isFromDialog) {
            return;
        }

        $("#main-menu li.dropdown-submenu a").click((ev) => {
            ev.stopPropagation();
        });
    }

    function scrollToFirstValidationMessage(context: JQuery): void {
        const $container = $('.content.container-fluid');

        if ($container.length > 0) {
            const containerOffset = $container.offset().top;
            const containerHeight = $container.height();
            const [validationElementsOffset] = $.map($("span.field-validation-error", context), (domElement) => $(domElement).parents(".form-group:first").offset().top);

            if (validationElementsOffset !== undefined) {
                const scrollOffset = validationElementsOffset - containerOffset;
                $container.scrollTop(validationElementsOffset - containerOffset);
            }
        }
    }

    function initAlertNavigation(context: JQuery): void {
        scrollToFirstValidationMessage(context);
    }

    function initTabControls(context: JQuery): void {
        context.find("div[role=\"tabpanel\"] a[role=\"tab\"]").click(function (this: JQuery, event) {
            Forms.cancelEvent(event);
            $(this).tab("show");
        });

        openTabBasedOnUrl(context);
    }

    function getTabNameFromHashUrl(hashUrl: string): string {
        return hashUrl.replace("tab-", "");
    }

    function isTabSelectedInHashUrl(hashUrl: string): boolean {
        return hashUrl !== "" && hashUrl.indexOf("tab-") > -1;
    }

    function openTabBasedOnUrl(context: JQuery): void {
        var hashUrl = window.location.hash;
        var tabId = getTabNameFromHashUrl(hashUrl);

        if (!isTabSelectedInHashUrl(hashUrl) || context.find(tabId).length <= 0) {
            return;
        }

        if (context.find("li a[href][role=\"tab\"].has-error:first").length > 0) {
            return;
        }

        context.find(`[href='${tabId}']`).click();
    }

    function initSelectControls(context: JQuery, isFromDialog: boolean) {
        if (!isFromDialog) {
            context.find('.bootstrap-select').remove();
        }

        context.find(".selectpicker").each((i, el) => {
            $(el).selectpicker();
        });
    }

    function initTextAreas(context: JQuery): void {
        const autoresizeingTextareas = context.find("textarea[data-allow-autoresize='true']");

        autoresizeingTextareas.each((index: number, textarea: HTMLTextAreaElement) => {
            initAutoresize(textarea);
        });

        $(window).resize(function () {
            autoresizeingTextareas.each((index: number, textarea: HTMLTextAreaElement) => {
                textarea.style.maxHeight = '';
                initAutoresize(textarea);
            });
        });

        $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            autoresizeingTextareas.each((index: number, textarea: HTMLTextAreaElement) => {
                initAutoresize(textarea);
            });
        })

        $("textarea[data-allow-autoresize='true']", context).on("keydown paste cut change", function (this: HTMLTextAreaElement) {
            autoresize(this);
            keepInViewport(this);
        });

        $("textarea[data-val-length-max]", context).on("keyup paste cut change focus", function (this: HTMLTextAreaElement) {
            const max = this.dataset.valLengthMax;

            if (max !== undefined) {
                handleValidationHint(this, parseInt(max));
            }
        });

        function initAutoresize(textarea: HTMLTextAreaElement) {
            const minHeight = 60;
            initMinHeight(textarea, minHeight);
            initMaxHeight(textarea);
            autoresize(textarea);
        }

        function initMinHeight(textarea: HTMLTextAreaElement, minHeight: number) {
            const currentMinHeight = textarea.style.minHeight;

            if (!currentMinHeight) {
                textarea.style.minHeight = minHeight + "px";
            }
        }

        function initMaxHeight(textarea: HTMLTextAreaElement) {
            const maxHeight = textarea.style.maxHeight;
            const maxHeightRatio = 0.5;

            if (!maxHeight) {
                textarea.style.maxHeight = getRightViewportHeight(textarea, maxHeightRatio);
            }
        }

        function getRightViewportHeight(textarea: HTMLTextAreaElement, maxHeightRatio: number): string {
            const $parentNode = $(textarea).closest('.modal-body, .content, body');

            if ($parentNode !== null) {
                const availableHeight = getAvailableHeight($parentNode.get(0));
                const maxHeight = valBetween(availableHeight, (window.innerHeight * maxHeightRatio), window.innerHeight)

                return maxHeight + "px";
            }

            return (window.innerHeight * maxHeightRatio) + "px"
        }

        function getAvailableHeight(node: Element, heightRatio = 0.8): number {
            return Math.floor(node.clientHeight * heightRatio);
        }

        function valBetween(value: number, min: number, max: number) {
            return (Math.min(max, Math.max(min, value)));
        }

        function autoresize(textarea: HTMLTextAreaElement) {
            textarea.style.height = 'auto';
            textarea.style.height = (textarea.scrollHeight + 2) + 'px';
            textarea.scrollTop = textarea.scrollHeight;
        }

        function keepInViewport(textarea: HTMLTextAreaElement): void {
            const textareaNode = textarea.getBoundingClientRect();
            const $textarea = $(textarea);
            const $parent = $(textarea).closest('.modal-body, .content, body');

            if ($parent !== null) {
                const parent = $parent.get(0);
                const parentRect = parent.getBoundingClientRect();
                const diff = parentRect.top + parentRect.height - textareaNode.top - textareaNode.height;

                if (diff < 0) {
                    $parent.scrollTop($parent.scrollTop() + Math.abs(diff) + 2);
                }
            } else {
                textarea.scrollIntoView(false);
            }
        }

        function handleValidationHint(textArea: HTMLTextAreaElement, maxInputLenght: number): void {
            const $validationHint = $(textArea).siblings(".validation-hint");
            const input = textArea.value.replace(/\r?\n/g, "\r\n");
            $validationHint.text(formatText(input.length, maxInputLenght));

            if (input.length > maxInputLenght) {
                $validationHint.addClass("validation-hint-error text-danger");
            } else {
                $validationHint.removeClass("validation-hint-error text-danger");
            }

            function formatText(inputLength: number, maxInputLenght: number): string {
                return inputLength ? `${inputLength}/${maxInputLenght}` : '';
            }
        }
    }

    function extractRealControlHeight(control: JQuery): Promise<number> {
        const tabPanel = control.parents(".tab-pane").first();

        // not inside a tab
        if (tabPanel.length === 0) {
            return Promise.resolve<number>(control.height());
        }

        return new Promise<number>(r => {

            const isActive = tabPanel.hasClass("active");

            // force browser to render the tab content in the background
            if (!isActive) {
                tabPanel.addClass("hidden-but-rendered");
                tabPanel.addClass("active");
            }

            Utils.pushBackBrowserTask(() => {
                const height = control.height();

                // restore original status
                tabPanel.removeClass("hidden-but-rendered");
                tabPanel.toggleClass("active", isActive);

                // return the extracted height
                Utils.pushBackBrowserTask(() => r(height));
            });
        });
    }

    function initAceEditorControl(control: JQuery) {
        var editorMode = control.data("ace-editor-mode");
        var parent = control.parent();

        // when having ace editor inside hidden tab, height needs to be calculated like this
        extractRealControlHeight(control).then(height => {
            control.hide();
            control.removeAttr("data-ace-editor");

            var editorWrapper = $("<div class=\"form-control ace-editor-wrapper\"></div>").appendTo(parent);
            var editor = $("<div class=\"ace-editor\"></div>").appendTo(editorWrapper);
            editor.text(control.val());
            editor.height(height);

            var aceEditor = ace.edit(editor[0]);
            var aceSession = aceEditor.getSession();

            aceSession.setMode(editorMode);

            aceEditor.on("blur", () => {
                control.val(aceEditor.getValue());
            });

            if (control.is("[readonly]")) {
                aceEditor.setReadOnly(true);
            }
        });
    }

    function initAceEditorControls(aceEditors: JQuery) {
        aceEditors.each((index, element) => {
            initAceEditorControl($(element));
        });
    }

    function initAceEditor(context: JQuery) {
        var aceEditors = context.find("*[data-ace-editor='true']");

        if (aceEditors.length === 0) {
            return;
        }

        if (!isAceEditorScriptLoaded) {
            isAceEditorScriptLoaded = true;
            var scriptBaseUrl = Globals.resolveUrl("~/Scripts/ace/src-min");
            var scriptUrl = scriptBaseUrl + "/ace.js";
            $.getScript(scriptUrl, () => {
                ace.config.set("basePath", scriptBaseUrl);
                initAceEditorControls(aceEditors);
            });
        } else {
            initAceEditorControls(aceEditors);
        }
    }

    function initCkEditorControl(control: JQuery) {
        const scriptBaseUrl = Globals.resolveUrl("~/Scripts/CKEditor/");
        CKEDITOR.plugins.addExternal('confighelper', scriptBaseUrl, 'CKEditorPlaceholderPlugin.js');
        CKEDITOR.plugins.addExternal('clipboard', scriptBaseUrl, 'clipboardPlugin.js');
        CKEDITOR.plugins.addExternal('widgetselection', scriptBaseUrl, 'widgetselectionPlugin.js');
        CKEDITOR.plugins.addExternal('lineutils', scriptBaseUrl, 'lineutilsPlugin.js');
        CKEDITOR.plugins.addExternal('widget', scriptBaseUrl, 'widgetPlugin.js');
        CKEDITOR.plugins.addExternal('htmlwriter', scriptBaseUrl, 'htmlWriterPlugin.js');
        CKEDITOR.plugins.addExternal('notification', scriptBaseUrl, 'notificationPlugin.js');
        CKEDITOR.plugins.addExternal('wordcount', scriptBaseUrl, 'wordCountPlugin.js');

        let pluginList = "confighelper,clipboard,widgetselection,lineutils,widget,htmlwriter,notification,wordcount";

        var mentionPluginConfigAttribute = eval(control.attr("data-ck-editor-mention"));

        if (mentionPluginConfigAttribute !== undefined) {
            pluginList += `,${Atomic.Ckeditor.MentionPlugin.registerPlugin(mentionPluginConfigAttribute)}`;
        }

        const maxLength = control.attr('maxlength');

        CKEDITOR.replace(control[0], {
            extraPlugins: pluginList,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: true,
                maxCharCount: maxLength === undefined ? -1 : maxLength,
            }
        });

        const ckEditorName = Object.keys(CKEDITOR.instances)[0];
        const ckEditorInstance = CKEDITOR.instances[ckEditorName];

        ckEditorInstance.on('change', (e: any) => {
            control.val(e.editor.getData());
        });
    }

    function initCkEditorControls(ckEditors: JQuery) {
        ckEditors.each((index, element) => {
            initCkEditorControl($(element));
        });
    }

    function initCkEditor(context: JQuery) {
        var ckEditors = context.find("*[data-ck-editor='true']");
        if (ckEditors.length === 0) {
            return;
        }

        if (!isCkEditorScriptLoaded) {
            isCkEditorScriptLoaded = true;
            var preset = ckEditors.data("ck-editor-presets");

            var scriptUrl = Strings.format("//cdn.ckeditor.com/4.5.3/{0}/ckeditor.js", preset);
            $.getScript(scriptUrl, () => {
                initCkEditorControls(ckEditors);
                onCkEditorInit();
            });
        } else {
            initCkEditorControls(ckEditors);
            onCkEditorInit();
        }
    }

    function initNoClickItems(context: JQuery): void {
        context.find(".no-click").click((event) => {
            event.stopPropagation();
        });
    }

    function initTimeControl(elem: Element): void {
        const timePickerZIndex = 1050;

        var $timeInput = $(elem).find("input.form-control");
        var $timeInputValue = $timeInput.siblings("[type='hidden']");
        var $timeButton = $(elem).find(".input-group-addon");

        var isReadonly = $timeInput.is("[readonly]");

        if (!isReadonly) {
            $timeInput
                .timepicker({
                    defaultTime: $timeInput.data("value"),
                    interval: $timeInput.data("interval"),
                    timeFormat: $timeInput.data("timeformat").replace("tt", "p"),
                    zindex: timePickerZIndex,
                    change: (time: Date) => {
                        $timeInputValue.val(time.getHours() * 60 + time.getMinutes());
                    }
                });

            $timeButton.click(($event) => { $event.preventDefault(); $event.stopPropagation(); $timeInput.focus(); });
        }
    }

    function initDateControl(elem: Element, locale: string, dateFormat: string): void {
        var dateInput = $(elem).find("input");
        var isReadonly = dateInput.attr("data-readonly");

        if (!isReadonly) {
            var minDate = dateInput.attr("data-min-value");
            var maxDate = dateInput.attr("data-max-value");
            var dateConfig = {
                startDate: minDate,
                endDate: maxDate,
                startView: 0 /*start with days*/,
                language: locale,
                autoclose: true,
                format: dateFormat
            };
            $(elem).datepicker(dateConfig);
        }
    }

    function initDateControls(context: JQuery, locale: string, dateFormat: string): void {
        context
            .find(".date")
            .each((index: number, elem: Element) => {
                initDateControl(elem, locale, dateFormat);
            });

        context
            .find(".time")
            .each((index: number, elem: Element) => {
                initTimeControl(elem);
            });
    }

    export function initLocalizedDatePicker(): Promise<void> {
        var datePickerLocale = Globals.getDatePickerLocale();

        if (datePickerLocaleLoad == null) {
            datePickerLocaleLoad = new Promise<void>((resolve) => {
                var localeUrl = `~/Scripts/locales/bootstrap-datepicker.${datePickerLocale}.js`;
                var scriptUrl = Globals.resolveUrl(localeUrl);

                $.getScript(scriptUrl, () => {
                    resolve();
                });
            });
        }

        return datePickerLocaleLoad;
    }

    function initDatePickers(context: JQuery): void {
        var dateFormat = Globals.getDateFormat();
        var datePickerLocale = Globals.getDatePickerLocale();

        initLocalizedDatePicker().then(() => {
            initDateControls(context, datePickerLocale, dateFormat);
        });
    }

    function initColorControl(elem: Element): void {
        var colorInput = $(elem).find("input");
        var isReadonly = colorInput.attr("data-readonly");
        var format = colorInput.attr("data-format").toLowerCase();

        if (!isReadonly) {
            var dateConfig = {
                format: format
            };
            $(elem).colorpicker(dateConfig);
        }
    }

    function initColorControls(context: JQuery): void {
        context.find(".color-picker").each((index: number, elem: Element) => {
            initColorControl(elem);
        });
    }

    function initColorPickers(context: JQuery): void {
        var scriptUrl = Globals.resolveUrl("~/Scripts/bootstrap-colorpicker.min.js");

        $.getScript(scriptUrl, () => {
            initColorControls(context);
        });
    }

    function initFormValidation(elem: Element, isFromDialog: boolean): void {
        var form = $(elem);

        if (!isFromDialog) {
            $(() => {
                form.validateBootstrap(true);
            });
        }
        else {
            $.validator.unobtrusive.parse(form);
        }
    }

    function initFormValidations(context: JQuery, isFromDialog: boolean): void {
        context.find("form").each((index: number, elem: Element) => {
            initFormValidation(elem, isFromDialog);
        });
    }

    function initDocumentHotkeys() {
        if (areDocumentHotkeyReady) {
            return;
        }

        areDocumentHotkeyReady = true;

        $(document).on("keydown", "", "F1", (event) => {
            Help.openPageLevelLink();
            Forms.cancelEvent(event);
        });
    }

    function initHotkeyPassthroughForControl(context: JQuery, control: JQuery) {
        var hotkeys = control.attr("data-hotkey-passthrough");
        control.removeAttr("data-hotkey-passthrough");

        if (Strings.isNullOrEmpty(hotkeys)) {
            return;
        }

        control.on("keydown", "", hotkeys, (event) => {
            event.target = context as any;
            $(context).trigger(event);
        });
    }

    function initHotkeysForControl(context: JQuery, control: JQuery) {
        var hotkeys = control.attr("data-hotkey");
        control.removeAttr("data-hotkey");

        if (Strings.isNullOrEmpty(hotkeys)) {
            return;
        }

        context.on("keydown", "", hotkeys, (event) => {
            control[0].click();
            Forms.cancelEvent(event);
        });
    }

    function initHotkeys(context: JQuery) {
        context.find("[data-hotkey]").each((index, element) => {
            initHotkeysForControl(context, $(element));
        });

        context.find("[data-hotkey-passthrough]").each((index, element) => {
            initHotkeyPassthroughForControl(context, $(element));
        });

        initDocumentHotkeys();
    }

    function getAllAvailableAccelerators(): Array<string> {
        const availableAccelerators = getLetters();
        const blackList = getBlackList(BrowserSurvey.detectBrowserType());

        return availableAccelerators.filter(v => blackList.indexOf(v) === -1);

        function getLetters(): Array<string> {
            const letters = new Array<string>();

            for (let i = 65; i <= 90; i++) {
                letters.push(String.fromCharCode(i));
            }

            return letters;
        }

        function getBlackList(browserType: BrowserSurvey.browserType): Array<string> {
            switch (browserType) {
                case (BrowserSurvey.browserType.Firefox):
                    return ["C", "D", "E", "H", "N", "P", "W", "Z"];
                case (BrowserSurvey.browserType.Chrome):
                    return ["D", "E", "F"];
                case (BrowserSurvey.browserType.Safari):
                    return ["D", "E", "F", "M"];
                case (BrowserSurvey.browserType.Edge):
                    return ["D"];
                case (BrowserSurvey.browserType.Ie):
                    return ["D"];
                default:
                    return [];
            }
        }
    }

    function getTargetElement(element: JQuery): JQuery | null {
        const forValue: HTMLElement | null = document.getElementById(element.prop('for'));

        return forValue === null ? null : $(forValue);
    }

    function getMultiValuePicker(element: JQuery): JQuery {
        return element.closest('.value-picker.is-multivalue');
    }

    function getValuePicker(element: JQuery): JQuery {
        return element.closest('.value-picker');
    }

    function getValuePickerSelectButton(element: JQuery): JQuery {
        return getValuePicker(element).find('.value-picker-add-button');
    }

    function isValuePicker(element: JQuery): boolean {
        return getValuePicker(element).length > 0;
    }

    function isMultiValuePicker(element: JQuery): boolean {
        return getMultiValuePicker(element).length > 0;
    }

    function applyAccelerator(element: JQuery, accessKey: string) {
        var textContent = element.text();

        var targetElement = getTargetElement(element);

        if (targetElement !== null) {
            var accessKeyElement = element;

            if (targetElement.length && isValuePicker(targetElement)) {
                var selectButton = getValuePickerSelectButton(targetElement);
                accessKeyElement = selectButton.length ? selectButton : targetElement;
            }

            accessKeyElement.attr("accesskey", accessKey);
        }

        var upperIndex = textContent.indexOf(accessKey.toUpperCase());
        var lowerIndex = textContent.indexOf(accessKey.toLowerCase());

        var index = upperIndex === -1 ? lowerIndex : upperIndex;

        if (index === -1) {
            return;
        }

        var newTextContent = `<span>${textContent.substr(0, index)}`
            + `<span class="accelerator">${textContent.substr(index, 1)}</span>`
            + textContent.substr(index + 1) + "</span>";

        if (element.find("input[type='checkbox']").length || element.find("input[type='radio']").length) {
            element.html(element.html().replace(`>${textContent}`, `>${newTextContent}`));
        } else {
            element.html(newTextContent);
        }
    }

    function applyAccelerators(context: JQuery, selector: string, availableAccelerators: Array<string>) {
        var items = context.find(selector).not("[accesskey]");

        items.each((index, element) => {
            var $element = $(element);

            var targetElement = getTargetElement($element);

            if (targetElement !== null && targetElement.length) {
                if (targetElement.first().is('[readonly]')) {
                    return;
                }
            }

            var text = $element.text();
            var keys = text.split("");

            for (var i = 0; i < keys.length; i++) {
                var key = keys[i].toUpperCase();

                if (availableAccelerators.indexOf(key) !== -1) {
                    applyAccelerator($element, key);
                    Utils.removeElement(availableAccelerators, key);
                    break;
                }
            }
        });
    }

    function initAccelerators(context: JQuery) {
        var availableAccelerators = getAllAvailableAccelerators();

        var existingAccelerators = $(document).find("[accesskey]")
            .map((index, element) => $(element).attr("accesskey").toString().toUpperCase())
            .toArray();

        availableAccelerators = availableAccelerators.filter(v => existingAccelerators.indexOf(v) === -1);

        applyAccelerators(context, "ul.nav-tabs > li > a", availableAccelerators);
        applyAccelerators(context, "div.form-group label", availableAccelerators);
    }

    function oneTimeMapPickerInitialization(context: JQuery): void {
        if (context.find("*[data-component='MapPicker.GoogleMaps']").length > 0) {
            MapPicker.oneTimeInitialization();
        }
    }

    function markEditedField(element: Element): void {
        const emptyId = element.id === "";
        const name = element.getAttribute("name");

        const labelId: string = emptyId && name != null ? Utils.pascalCaseToDelimitierSepareted(name) : element.id;

        if (!$(`[for='${labelId}']`).hasClass("modified")) {
            $(`[for='${labelId}']`).addClass("modified");
        }
    }

    function onInputChange(): void {
        if (isDirty) {
            return;
        }

        isDirty = true;
        window.onbeforeunload = () => isDirty ? "" : undefined;

        if (!$(".breadcrumb").hasClass("modified-breadcrumb")) {
            $(".breadcrumb").addClass("modified-breadcrumb");
        }

        if (document.title.indexOf("* ") === -1) {
            document.title = "* " + document.title;
        }
    }

    function initTrackChanges(context: JQuery): void {
        const form = context.find("form")[0];

        if (form === undefined) {
            return;
        }

        const formTracking = form.getAttribute("data-tracking");

        if (formTracking === null) {
            return;
        }

        const trackingStrategy: FormModificationTracking = parseInt(formTracking) as FormModificationTracking;

        if (trackingStrategy === FormModificationTracking.None) {
            return;
        }

        context.find('input:not(:button,:submit),textarea,select').change((el) => {
            onInputChange();

            if (trackingStrategy === FormModificationTracking.MarkEditedField) {
                markEditedField(el.target);
            }
        });
    }

    export function init(context: JQuery, isFromDialog: boolean = false, reInitialize: boolean = false): void {
        initNoClickItems(context);
        initDatePickers(context);
        initColorPickers(context);
        initSelectControls(context, isFromDialog);
        initTabControls(context);
        initAlertNavigation(context);
        initTextAreas(context);
        initFormActions(context);
        initTrackChanges(context);

        CustomValidations.init();

        if (reInitialize) {
            context.closest("form").validateBootstrap(true);
        } else {
            initFormValidations(context, isFromDialog);
        }

        HamburgerButtons.initMenus(context, isFromDialog);

        initComponents(context);
        initMainMenu(context, isFromDialog);

        ValuePicker.initControls(context);
        TypeAhead.initControls(context);
        Optional.initControls(context);
        Help.initResources(context, isFromDialog);

        initAceEditor(context);
        initCkEditor(context);

        initHotkeys(context);
        initAccelerators(context);

        oneTimeMapPickerInitialization(context);

        Hub.init(context);

        OnValueChange.attachOnChangedEvent();
    }

    function initFormActions(context: JQuery) {
        $(".form-buttons", context)
            .not('button[data-toggle="dropdown"]')
            .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
            .not("[onclick]")
            .click((e) => Grid.onInternalButtonClicked(e));

        $(".form-buttons", context).not('button[data-toggle="dropdown"]')
            .click((e) => Grid.onInternalButtonClicked(e));

        $(".dropdown-button", ".footer-section")
            .not('button[data-toggle="dropdown"]')
            .find("[data-action=\"execute\"], [data-action=\"navigate\"]")
            .not("[onclick]")
            .click((e) => Grid.onInternalButtonClicked(e));

        $(".dropdown-button", ".footer-section")
            .not('button[data-toggle="dropdown"]')
            .click((e) => Grid.onInternalButtonClicked(e));
    }

    function onButtonClicked(event: MouseEvent | JQueryEventObject) {
        Forms.cancelEvent(event);

        var element = Utils.getEventTarget(event);

        if (Forms.isDisabled(element)) {
            return;
        }

        var clickedElement = $(element);
        var actionElement = clickedElement.closest("[data-action]");

        if (actionElement.length === 0) {
            return;
        }

        switch (actionElement.data("action")) {
            case "execute":
                executeAction(event, clickedElement);
                break;

            case "navigate":
                navigateAction(event, clickedElement);
                break;
        }
    }

    function navigateAction(event: MouseEvent | JQueryEventObject, actionElement: JQuery) {
        var url = actionElement.data("url");
        var method = actionElement.data("method");
        var isNewTab = actionElement.data("new-tab") || event.ctrlKey;

        var action = () => {
            Forms.submit(url, method, isNewTab, true);
        };

        performConfirmedAction(action, actionElement);
    }

    export function appendToInput(input: JQuery, separator: string, value: string) {
        if (Strings.isNullOrWhiteSpace(value)) {
            return;
        }

        var currentValue = input.val();

        if (currentValue === "") {
            input.val(value);
        } else {
            input.val(currentValue + separator + value);
        }
    }

    export function removeFromInput(input: JQuery, separator: string, value: string) {
        if (Strings.isNullOrEmpty(value)) {
            return;
        }

        var currentValues = input.val().split(separator);
        Utils.removeElement(currentValues, value.toString());
        input.val(currentValues.join(separator));
    }

    export function isDisabled(element: Element) {
        var disabled = $(element).closest(":disabled, [disabled], .disabled");

        return disabled.length > 0;
    }

    export function cancelEvent(event: Event) {
        event.stopImmediatePropagation();
        event.stopPropagation();
        event.preventDefault();
    }

    export function submit(url: string, method: string = "POST", shouldOpenInNewTab: boolean = false, redirectToReturnPoint: boolean = false) {
        var target = shouldOpenInNewTab ? "_blank" : "_self";

        if (method === "GET") {
            open(url, shouldOpenInNewTab);
        }
        else {
            var returnUrl: string;

            if (redirectToReturnPoint) {
                returnUrl = RestorePoints.getReturnPointUrl(getReturnPoint()) as string;
            }
            else {
                returnUrl = encodeURI(Forms.getCurrentUrl());
            }

            var referrerInput = Strings.format("<input type='hidden' name='{0}' value='{1}' />", returnUrlFieldName, returnUrl);
            var form = Strings.format("<form action='{0}' method='{1}' target='{2}'>{3}</form>", url, method, target, referrerInput);

            $(form).appendTo("body").submit();
        }
    }

    function getRecordId() {
        return $("#main-form input[name=\"Id\"]").val();
    }

    function executeAction(event: MouseEvent | JQueryEventObject, actionElement: JQuery) {
        var handler = actionElement.data("handler");

        var action = () => {
            Utils.executeHandler(handler, { event: event });
        };

        performConfirmedAction(action, actionElement);
    }

    function performConfirmedAction(action: () => void, actionElement: JQuery) {
        var isConfirmationRequired = actionElement.data("confirm");

        if (!isConfirmationRequired) {
            action();
        }
        else {
            var messageFormat = actionElement.data("confirm-message");

            var title = Globals.resources.ConfirmDialogTitle;
            var acceptButtonText = actionElement.data("confirm-accept-button");
            var rejectButtonText = actionElement.data("confirm-reject-button");

            var eventHandler: Dialogs.IButtonClickedEventHandler = (eventArgs) => {
                if (eventArgs.isOk) {
                    action();
                }
            };

            CommonDialogs.confirm(messageFormat, title, eventHandler, undefined, acceptButtonText, rejectButtonText);
        }
    }

    export function handleButtonAction($button: JQuery): Promise<void> {
        const buttonConfirmTitle = $button.attr("data-confirm-title");
        const buttonConfirmContent = $button.attr("data-confirm-content");
        const buttonBeforeAction = $button.attr("data-before-action");

        const promises: Array<() => Promise<void>> = [];

        if (buttonBeforeAction !== undefined) {
            promises.push(() => Utils.executeHandler(buttonBeforeAction, {}));
        }

        if (buttonConfirmTitle !== undefined && buttonConfirmContent !== undefined) {
            promises.push(() => new Promise<void>((resolve, reject) => {
                CommonDialogs.confirm(
                    buttonConfirmContent,
                    buttonConfirmTitle,
                    (eventArgs) => {
                        if (eventArgs.isOk) {
                            resolve();
                        } else {
                            reject();
                        }
                    });
            }));
        }

        const sequencedPromises = promises.reduce(
            (promise, task) => promise.then(task), Promise.resolve<void>());

        return sequencedPromises;
    }

    export function submitRecordForm(button: Element, redirect?: string) {
        var $button = $(button);
        var formId = $button.attr("form");
        var form = formId ? $(`form#${formId}`) : $button.closest("form");

        if (form.length === 0) {
            throw "Form not found. Could not set restore point url";
        }

        if (form.valid()) {
            $button.attr("disabled", "disabled");
        }

        isDirty = false;

        handleButtonAction($button).then(() => {
            var urlInput = form.find(`input[name='${returnUrlFieldName}']`);

            if (urlInput.length === 0) {
                urlInput = $(`<input type="hidden" name="${returnUrlFieldName}" />`).appendTo(form);
            }

            const returnPoint = getReturnPoint();
            const returnPointUrl = redirect === undefined
                ? RestorePoints.getReturnPointUrl(returnPoint) as string
                : redirect;

            urlInput.val(returnPointUrl);

            const buttonActionName = $button.attr("data-action-name");
            const buttonActionValue = $button.attr("data-action-value");

            if (buttonActionName !== undefined && buttonActionValue !== undefined) {
                $(`input[type='hidden'][name='${buttonActionName}']`, form).val(buttonActionValue);
            }

            form.submit();
        }).catch(() => {
            $button.removeAttr("disabled");
        });
    }

    export function redirect(url: string) {
        window.location.href = url;
    }

    export function open(url: string, shouldOpenInNewTab: boolean) {
        if (shouldOpenInNewTab) {
            window.open(url, "_blank");
        } else {
            redirect(url);
        }
    }

    export function redirectToRestorePoint(pointName: string, fallbackUrl: string): void {
        if (!RestorePoints.returnToPoint(pointName)) {
            redirect(fallbackUrl);
        }
    }

    export function getReturnPoint(): string {
        const searchUrl = window.location.search.slice(1);
        let returnPoint = UrlHelper.getQueryParameter(searchUrl, returnPointQueryParamName, RestorePoints.getCurrentControllerName() + "/List");

        if (!!returnPoint) {
            returnPoint = returnPoint.replace(/\./g, "/");
        }

        return returnPoint as string;
    }

    export function redirectToList(fallbackUrl: string): void {
        redirectToRestorePoint(getReturnPoint(), fallbackUrl);
    }

    export function getCurrentUrl(): string {
        var url = window.location.href;

        if (url.substr(url.length - 1, 1) === "#") {
            url = url.substr(0, url.length - 1);
        }

        return url;
    }

    export function propagateClickEvent(event: JQueryEventObject | null = null): JQueryEventObject {
        var e = jQuery.Event("click");

        if (event != null) {
            e.ctrlKey = event.ctrlKey;
        }

        return e;
    }

    export enum KeyCode {
        Enter = 13,
        Space = 32,
        Up = 38,
        Down = 40
    }

    export enum FormModificationTracking {
        None = 0,
        WarnOnExit = 1 << 0,
        MarkEditedField = 1 << 1,
    }
}

$(document).ready(() => {
    var body = $("body");
    Forms.init(body, false);
});