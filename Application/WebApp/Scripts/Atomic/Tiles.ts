﻿module Tiles {
    export function maxSizeForAll(container: JQuery): void {
        var width = 0;
        var height = 0;
        const tiles = $(".tile", container);

        tiles.each((index, element) => {
            var tile = $(element);

            width = Math.max(width, tile.width());
            height = Math.max(height, tile.height());
        });

        tiles.width(width).height(height);
    }
}
