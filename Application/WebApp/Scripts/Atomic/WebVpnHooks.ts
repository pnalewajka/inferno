declare const fgt_sslvpn: dynamic;

if ((window as dynamic).fgt_sslvpn !== undefined)
{
    const original_url_rewrite = fgt_sslvpn.url_rewrite;

    fgt_sslvpn.url_rewrite = (url: string) =>
    {
        if (url.indexOf("cdn.ckeditor.com") !== -1)
        {
            return url;
        }

        return original_url_rewrite(url);
    };
}
