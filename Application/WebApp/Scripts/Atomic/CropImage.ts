﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>
/// <reference path="../typings/jquery.validation/jquery.validation.d.ts"/>
/// <reference path="../typings/jquery.fileupload/jquery.fileupload.d.ts"/>
/// <reference path="../typings/jquery.jcrop/jquery.jcrop.d.ts"/>

module DocumentUpload {
    export class CropImage extends Controls.Component {
        private temporaryDocumentDownloadPath: string;
        private minWidth: number;
        private maxWidth: number;
        private minHeight: number;
        private maxHeight: number;
        private shouldPreserveAspectRatio: boolean;
        private maxCropImageDimension: number;
        private selectionCoords: ImageCroppingCoordinates;

        public temporaryDocumentId: string;

        constructor(elem: Element) {
            super($(elem));
            this.minWidth = parseInt($(elem).attr('data-min-width'));
            this.maxWidth = parseInt($(elem).attr('data-max-width'));
            this.minHeight = parseInt($(elem).attr('data-min-height'));
            this.maxHeight = parseInt($(elem).attr('data-max-height'));
            this.shouldPreserveAspectRatio = Utils.parseBoolean($(elem).attr('data-should-preserve-aspect-ratio'));
            this.maxCropImageDimension = Globals.getParameterAsNumber("System.DocumentUpload.MaxCropImageDimension");

            this.temporaryDocumentId = $(elem).attr('data-id');
            this.temporaryDocumentDownloadPath = Globals.resolveUrl("~/DocumentDownload/DownloadTemporary/");

            this.attachImage($(elem));
        }

        private attachImage(mainElement: JQuery): void {
            var imageUrl = this.temporaryDocumentDownloadPath + this.temporaryDocumentId;

            mainElement.find('.preview-image').first().append("<img class='collapse' />");
            var newImage = $(mainElement.find('img')[0]);

            newImage.load(() => {
                this.appendJCrop(newImage);

                var parentItem = newImage.closest('div.preview-image');
                var jcropItem = parentItem.find('div.jcrop-holder');
                var left = (parentItem.width() / 2) - (jcropItem.width() / 2);
                jcropItem.css('left', left);
            });

            newImage.attr('src', imageUrl);
        }

        private appendJCrop(imageElement: JQuery): void {
            var imageSize = new Controls.ImageSize(imageElement.width(), imageElement.height());
            var scaledImageSize = imageSize.getScaledSize(this.maxCropImageDimension);

            imageElement.Jcrop({
                keySupport: false,
                minSize: [this.minWidth, this.minHeight],
                maxSize: [this.maxWidth, this.maxHeight],
                boxWidth: scaledImageSize.width,
                boxHeight: scaledImageSize.height,
                onSelect: (coords: ImageCroppingCoordinates) => { this.getSelectionCoords(coords); },
                onChange: (coords: ImageCroppingCoordinates) => { this.getSelectionCoords(coords); },
                aspectRatio: this.shouldPreserveAspectRatio ? imageElement.width() / imageElement.height() : null
            });
        }

        private getSelectionCoords(coords: ImageCroppingCoordinates) {
            this.selectionCoords = coords;
        }

        public getCropInfo(): CropInfo {
            var cropInfo = new CropInfo();
            cropInfo.cropHeight = this.selectionCoords.h;
            cropInfo.cropWidth = this.selectionCoords.w;
            cropInfo.cropPositionLeft = this.selectionCoords.x;
            cropInfo.cropPositionTop = this.selectionCoords.y;

            return cropInfo;
        }
    }

    export class CropInfo {
        public cropPositionLeft: number;
        public cropPositionTop: number;
        public cropWidth: number;
        public cropHeight: number;
    }

    class ImageCroppingCoordinates {
        public x: number;
        public y: number;
        public x2: number;
        public y2: number;
        public w: number;
        public h: number;
    }
}