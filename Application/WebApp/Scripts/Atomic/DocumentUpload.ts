﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>
/// <reference path="../typings/jquery.validation/jquery.validation.d.ts"/>
/// <reference path="../typings/jquery.fileupload/jquery.fileupload.d.ts"/>

module DocumentUpload {
    export class DocumentPicker extends Controls.Component {
        private itemContainer: JQuery;
        private isMultiValue: boolean;
        private itemMaxPos: number;
        private controlName: string;
        public  temporaryDocumentDownloadPath: string;
        public documentDownloadPath: string;
        private numberOfUploadsInProgress: number;
        private hiddenInputForValidation: JQuery;
        public onUploadFinished = new Atomic.Events.EventDispatcher<boolean>();
        public static uploadServiceUrl: string = '~/DocumentUpload/UploadDocument';

        public get canDownload():boolean {
            return this.container.data("can-download") === "true";
        }

        public get canUpload(): boolean {
            return this.container.data("can-upload") === "true";
        }

        constructor(elem: Element, uploadServiceUrl: string = '~/DocumentUpload/UploadDocument') {
            super($(elem));
            this.isMultiValue = $(elem).attr('data-is-multivalue') === 'true';
            this.controlName = $(elem).attr('data-field-name');
            this.temporaryDocumentDownloadPath = Globals.resolveUrl("~/DocumentDownload/DownloadTemporary/");
            this.documentDownloadPath = Globals.resolveUrl("~/DocumentDownload/Download/");
            this.numberOfUploadsInProgress = 0;
            this.hiddenInputForValidation = $(elem).find('input.hidden-but-validate');

            var picker = $(elem).find('.document-picker');
            picker.find('.document-upload').fileupload({
                dataType: 'json',
                maxChunkSize: Globals.getParameterAsNumber("System.DocumentUpload.MaxUploadChunkSize"),
                url: Globals.resolveUrl(uploadServiceUrl),
                add: (e: Event, d: any) => { this.onAdd(e, d); },
                submit: (e: Event, d: any) => { return this.onSubmit(e, d); },
                progress: (e: Event, d: any) => { this.onProgress(e, d); },
                always: (e: Event, d: any) => { this.onAlways(e, d); }
            });

            $(elem).find("span.document-picker-add-button").click(() => { this.uploadClick(); });
            $(elem).find("span.document-picker-cloud-button").click(() => { this.uploadOneDriveClick(); });
            this.itemContainer = $(elem).find(".item-container");

            var thisInstance = this;
            this.itemMaxPos = -1;
            var documentPickerItems = this.itemContainer.find('[data-id]');

            $(elem).find(".document-picker-remove-button").click(()=> {
                this.itemContainer
                    .find('[data-id]')
                    .each((index, item) => this.removeItem($(item)));
            });

            $.each(documentPickerItems, (index, item) => {
                $(item).find('i.glyphicon-remove').click(() => {
                    thisInstance.removeItem($(item));
                });

                var actualPos = parseInt($(item).find('input[name$=".Index"]').val());

                if (index === 0) {
                    thisInstance.itemMaxPos = actualPos;
                } else {
                    if (thisInstance.itemMaxPos < actualPos) {
                        thisInstance.itemMaxPos = actualPos;
                    }
                }
            });

            return;
        }

        private downloadFromUrlAndAttach(url: string, fileName: string) {
            let fileInput = this.container.find(".document-upload");
            let blob = null;
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.onload = () => {
                blob = xhr.response;
                blob.name = fileName;
                fileInput.fileupload('add', { files: [blob] })
            }
            xhr.send();                        
        }

        private uploadOneDriveClick(): void
        {
            let redirectUri = window.location.origin + '/Allocation/Employee/MyEmployeeProfile';
            let odOptions = {
                clientId: "2d6f432b-1c10-490c-9490-da7956893e5b", //client id generated at https://apps.dev.microsoft.com/ for Dante & Atomic
                action: "download",
                multiSelect: true,
                success: (files:dynamic) => {
                    for(var file of files.value){
                        this.downloadFromUrlAndAttach(file["@microsoft.graph.downloadUrl"], file.name);
                    }

                    return;
                },
                cancel: () => { /*no action needed on cancel*/},
                error: () => {
                    CommonDialogs.alert({
                        htmlMessage: "Error when opening OneDrive dialog",
                        title: "One Drive Error",
                        eventHandler: undefined,
                        iconName: "",
                        okButtonText: ""
                    });
                },
                advanced: {
                    redirectUri: redirectUri
                }
            }
            window.OneDrive.open(odOptions);
        }

        private uploadClick(): void {
            this.container.find(".document-upload").trigger('click');
        }

        private incrementUploadCount():void {
            this.numberOfUploadsInProgress++;
            this.hiddenInputForValidation.valid();
        }

        private decrementUploadCount(isSuccessful: boolean): void {
            this.numberOfUploadsInProgress--;
            this.hiddenInputForValidation.valid();
            this.onUploadFinished.dispatch(isSuccessful);
        }

        private onAdd(event: any, data: any): void {
            var itemContainer = this.itemContainer;
            if (!this.isMultiValue) {
                itemContainer.find(".document-picker-item").remove();
            }
            $.each(data.files, (index, file) => {
                this.incrementUploadCount();
                itemContainer.append('<div class="document-picker-item" data-id="' + file.name + '"><span class="document-picker-file-name">' + file.name + '</span><i class="' + Utils.getIconClass('glyphicon-remove') + ' document-picker-failed-remove-button pull-right"></i><div class="document-picker-upload-progressbar clearfix" style="width: 0%"></div></div>');
            });
            data.submit();
        }

        private onProgress(event: any, data: any): void {
            var file = data.files[0];
            var itemContainer = this.itemContainer;
            var node = itemContainer.find('[data-id="' + file.name + '"]');
            var progress = Math.round(data.loaded / data.total * 100);
            node.find('.document-picker-upload-progressbar').text(progress + '%').css('width', progress + '%');
        }

        private onSubmit(event: any, data: any): boolean {
            var thisInstance = this;
            var documentUploadCtrl = this.container.find(".document-upload");
            var itemContainer = this.itemContainer;
            var file = data.files[0];
            var documentPickerItem = itemContainer.find('[data-id="' + file.name + '"]');

            $.post(Globals.resolveUrl("~/DocumentUpload/BeginUpload"),(result) => {
                data.formData = result;
                documentPickerItem.find('i.glyphicon-remove').click(() => {
                    thisInstance.abortItem(documentPickerItem, data);
                });
                data.jqXHR = $(documentUploadCtrl).fileupload('send', data);
            });

            return false;
        }

        private onAlways(event: any, data: any): void {
            var thisInstance = this;
            var itemContainer = this.itemContainer;
            var file = data.files[0];
            var documentPickerItem = itemContainer.find('[data-id="' + file.name + '"]');
            documentPickerItem.removeAttr("data-id");
            documentPickerItem.find(".document-picker-upload-progressbar").remove();
            documentPickerItem.find('i.glyphicon-remove').off('click');
            documentPickerItem.find('i.glyphicon-remove').click(() => {
                thisInstance.removeItem(documentPickerItem);
            });

            if (file.error) {
                documentPickerItem.addClass("document-picker-upload-error");
                this.decrementUploadCount(false);

                return;
            }

            if (data.result != null) {
                var uploadedDocument = this.getDocumentDescriptor(data.result);

                if (uploadedDocument.uploadFailed) {
                    documentPickerItem.addClass("document-picker-upload-error");
                } else {
                    this.onUploadCompleted(documentPickerItem, uploadedDocument);
                }

                this.decrementUploadCount(!uploadedDocument.uploadFailed);

                return;
            }

            this.decrementUploadCount(false);
        }

        public getDocumentDescriptor(jsonObject: any): DocumentDescriptor {
            var documentDescriptor = new DocumentDescriptor();
            Utils.populateFrom(documentDescriptor, jsonObject);

            return documentDescriptor;
        }

        public onUploadCompleted(documentPickerItem: JQuery, uploadedDocument: DocumentDescriptor) : void {
            documentPickerItem.attr("data-id", uploadedDocument.temporaryDocumentId);
            this.addInputs(documentPickerItem, uploadedDocument);
            this.setTemporaryDocumentDownloadLink(documentPickerItem, uploadedDocument);
        }

        private setTemporaryDocumentDownloadLink(documentPickerItem: JQuery, file: DocumentDescriptor): void {
            var documentDownloadLink = this.temporaryDocumentDownloadPath + file.temporaryDocumentId;
            var fileNameElement = documentPickerItem.find('.document-picker-file-name').first();
            var newContent: string;

            if (this.canDownload) {
                newContent = '<a href="' + documentDownloadLink + '" class="document-picker-file-name">' + fileNameElement.text() + '</a>';
            } else {
                newContent = '<span class="document-picker-file-name">' + fileNameElement.text() + '</span>';
            }

            fileNameElement.replaceWith(newContent);
        }

        private addInputs(documentPickerItem: JQuery, file: DocumentDescriptor): void {
            this.itemMaxPos++;
            if (this.isMultiValue) {
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '.Index" value="' + this.itemMaxPos + '" />');
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '[' + this.itemMaxPos + '].TemporaryDocumentId" value="' + file.temporaryDocumentId + '" />');
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '[' + this.itemMaxPos + '].ContentType" value="' + file.contentType + '" />');
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '[' + this.itemMaxPos + '].DocumentName" value="' + file.documentName + '" />');
            } else {
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '.TemporaryDocumentId" value="' + file.temporaryDocumentId + '" />');
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '.ContentType" value="' + file.contentType + '" />');
                documentPickerItem.append('<input type="hidden" name="' + this.controlName + '.DocumentName" value="' + file.documentName + '" />');                
            }
        }

        private abortItem(documentPickerItem: JQuery, data: any): void {
            data.abort();

            documentPickerItem.removeAttr("data-id");
            documentPickerItem.find('span').addClass("aborted");
        }

        private removeItem(documentPickerItem: JQuery): void {
            documentPickerItem.remove();
        }

        public isUploadInProgress(): boolean {
            return this.numberOfUploadsInProgress !== 0;
        }

        public static registerCustomValidation(): void {
            $.validator.addMethod("documentuploadinprogress", (value, element) => {
                var documentPicker = Controls.getExistingInstance(DocumentPicker, element);

                return !documentPicker.isUploadInProgress();
            });

            $.validator.unobtrusive.adapters.addBool("documentuploadinprogress");
        }
    }

    export class DocumentDescriptor {
        public documentName: string;
        public temporaryDocumentId: string;
        public contentType: string;
        public uploadFailed: boolean;
        public wasDocumentCompleted: boolean;
    }
}
