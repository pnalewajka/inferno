﻿module UrlHelper {
    export function updateUrlParameter(url: string, param: string, val: string): string {
        var tempArray = url.split("?");
        var baseUrl = tempArray[0];
        var parameters = tempArray[1];

        var existingParams = "";
        var temp = "";

        if (parameters) {
            tempArray = parameters.split("&");
            for (var i = 0; i < tempArray.length; i++) {
                if (tempArray[i].split("=")[0] !== param) {
                    existingParams += temp + tempArray[i];
                    temp = "&";
                }
            }
        }

        var assignment = temp + "" + param + "=" + encodeParameter(val);
        return baseUrl + "?" + existingParams + assignment;
    }

    export class UrlParameter {
        public name: string;
        public value: string;

        constructor(name: string, value: string) {
            this.name = name;
            this.value = value;
        }
    }

    export function updateUrlParameters(url: string, params: string, skipExisting: boolean = false): string {

        url = removeEmptyUrlParameters(url);

        if (params === "" || params == null) {
            return url;
        }

        var urlArray = url.split("?");
        var baseUrl = urlArray[0];
        var parameters = urlArray[1] || "";

        var existingParams = parameters !== "" ? splitUrlParams(parameters) : [];
        var newParams = splitUrlParams(params);

        var existingNames = existingParams.map(p => p.name);
        var appendParams = new Array<UrlParameter>();

        for (var i = 0; i < newParams.length; i++) {
            var name = newParams[i].name;
            var value = newParams[i].value;
            var index = existingNames.indexOf(name);

            if (index !== -1) {
                if (!skipExisting) {
                    existingParams[index].value = value;
                }
            } else {
                appendParams.push(new UrlParameter(name, value));
            }
        }

        var separator = existingParams.length * appendParams.length > 0 ? "&" : "";
        return baseUrl + "?" + joinUrlParams(existingParams) + separator + joinUrlParams(appendParams);
    }

    export function splitUrlParams(params: string): UrlParameter[] {
        return params.split("&").map(p => p.split("=")).map(p => new UrlParameter(p[0], decodeParameter(p[1])));
    }

    export function joinUrlParams(params: UrlParameter[]): string {
        return params.map(p => p.name + "=" + encodeParameter(p.value)).join("&");
    }

    export function removeEmptyUrlParameters(url: string): string {
        var tempArray = url.split("?");

        if (tempArray.length === 1) {
            return url;
        }

        var baseUrl = tempArray[0];
        var parameters = tempArray[1];

        parameters = parameters.replace(/[^=&]+=(&|$)/g, "").replace(/&$/, "");

        if (parameters === "") {
            return baseUrl;
        }

        return baseUrl + "?" + parameters;
    }

    export function removeUrlParameterAssignment(url: string, parameterAssignmentToRemove: string): string {
        return removeUrlParameterWhere(url, p => p === parameterAssignmentToRemove);
    }

    export function removeUrlParametersStartingWith(url: string, prefixToRemove: string): string {
        return removeUrlParameterWhere(url, p => p.substring(0, prefixToRemove.length) === prefixToRemove);
    }

    export function removeUrlParameterWhere(url: string, predicate: (p: string) => boolean): string {
        var tempArray = url.split("?");
        var baseUrl = tempArray[0];
        var parameters = tempArray[1];

        if (parameters == null) {
            return url;
        }

        tempArray = parameters.split("&");
        parameters = "";

        for (var i = 0; i < tempArray.length; i++) {
            if (!predicate(tempArray[i])) {
                if (parameters === "") {
                    parameters += tempArray[i];
                } else {
                    parameters += `&${tempArray[i]}`;
                }
            }
        }

        if (parameters === "") {
            return baseUrl;
        }

        return baseUrl + "?" + parameters;
    }

    export function getUrlParameters(url: string): string {
        var tempArray = url.split("?");
        var parameters = tempArray[1];

        if (parameters) {
            return parameters;
        }

        return "";
    }

    export function getQueryParameter(queryParameters: string, name: string, defaultValue: string | null = null): string | null {
        var parameters = queryParameters.split("&");

        for (var i = 0; i < parameters.length; i++) {
            var pair = parameters[i].split("=");
            if (pair[0] === name) {
                return decodeParameter(pair[1]);
            }
        }
        
        return defaultValue;
    }

    export function decodeParameter(value: string): string {
        var decoded = decodeURIComponent(value)
            .replace(/\+/ig, " ");

        return decoded;
    }

    export function encodeParameter(value: string): string {
        var encoded = encodeURIComponent(value)
            .replace(/%2C/ig, ",")
            .replace(/%20/ig, "+");

        return encoded;
    }

    export function getAbsolutePath(url: string): string {
        var withoutProtocol = url.split("://")[1];
        var slashIndex = withoutProtocol.indexOf("/");

        return slashIndex === -1
            ? "/"
            : withoutProtocol.substr(slashIndex);
    }

    export function dateUrlParameter(value?: Date): string {
        return value !== undefined
            ? value.toISOString().slice(0, 10)
            : "";
    }
}
