﻿namespace SubCardIndex {
    export function InitSubCardIndexHeader(): void {
        if ($("#summary-collapse-button").length) {

            $("#summary-collapse-button").click(() => {
                if ($("#summary-collapse-button").hasClass("collapsed")) {
                    $("#summary-content-short").hide();
                } else {
                    $("#summary-content-short").delay(100).fadeIn();
                }
            });
        }
    }
}

$(() => {
    SubCardIndex.InitSubCardIndexHeader();
});
