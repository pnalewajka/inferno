﻿module ClientSideOperations {
    type BaseCustomAction<T> = ((listId: string, data: T) => any) | string;
    export type CustomAction = BaseCustomAction<any>;
    export type ComboBoxCustomAction = BaseCustomAction<Grid.IRow>;


    // Must be identical to enum in ClientSideOperationType.cs
    export enum ClientSideOperationType {
        ValueChange = 1,
        Visibility = 2,
        Editable = 3,
        ValuePicker = 4,
        CustomAction = 5,
        MultiValuePicker = 6,
    }

    export interface IClientSideOperation {
        type: ClientSideOperationType;
        targetElementId: string;

        apply(): void;
    }

    export interface IClientSideValuePickerOperation extends IClientSideOperation {
        data: Grid.IRow;
    }

    export interface IClientSideCustomOperation extends IClientSideOperation {
        data: any;
        action: CustomAction;
    }

    export function fromJson(operation: IClientSideOperation): IClientSideOperation {
        switch (operation.type) {
            case ClientSideOperationType.Visibility:
                return ClientSideVisibilityOperation.fromJson(operation);

            case ClientSideOperationType.ValueChange:
                return ClientSideValueChangeOperation.fromJson(operation);

            case ClientSideOperationType.Editable:
                return ClientSideEditableOperation.fromJson(operation);

            case ClientSideOperationType.ValuePicker:
                return ClientSideValuePickerOperation.fromJson(operation as IClientSideCustomOperation);

            case ClientSideOperationType.CustomAction:
                return ClientSideCustomOperation.fromJson(operation as IClientSideCustomOperation);

            case ClientSideOperationType.MultiValuePicker:
                return ClientSideMultiValuePickerOperation.fromJson(operation as IClientSideCustomOperation);

            default:
                throw `ClientSideOperation type: '${operation.type}' not implemented`;
        }
    }

    export class ClientSideVisibilityOperation implements IClientSideOperation {
        public type = ClientSideOperationType.Visibility;
        public targetElementId: string;
        public isVisible: boolean;

        public static fromJson(jsonObject: any): ClientSideVisibilityOperation {
            var operation = new ClientSideVisibilityOperation();
            operation.targetElementId = jsonObject.targetElementId;
            operation.isVisible = jsonObject.isVisible;

            return operation;
        }

        public apply(): void {
            var element = $(document.getElementById(this.targetElementId) as HTMLElement)
                .closest(".repeater, .form-group");

            if (this.isVisible) {
                element
                    .show()
                    .filter(".collapse")
                    .removeClass("collapse");
            }
            else {
                element
                    .hide()
                    .not(".collapse")
                    .addClass("collapse");
            }
        }
    }

    export class ClientSideValueChangeOperation implements IClientSideOperation {
        public type = ClientSideOperationType.ValueChange;
        public targetElementId: string;
        public value: string;

        public static fromJson(jsonObject: any): ClientSideValueChangeOperation {
            var operation = new ClientSideValueChangeOperation();
            operation.targetElementId = jsonObject.targetElementId;
            operation.value = jsonObject.value;

            return operation;
        }

        public apply(): void {
            var element = $(document.getElementById(this.targetElementId) as HTMLElement);
            var dateObject = element.parents('.date:first');
            var isDatePicker = dateObject.data('datepicker');

            if (isDatePicker) {
                dateObject.datepicker('setDate', this.value);
            }

            else {
                element.val(this.value);
                element.data("value", this.value);
                element.trigger("change");
            }
        }
    }

    class ClientSideCustomOperation implements IClientSideCustomOperation {
        type = ClientSideOperationType.CustomAction;
        targetElementId: string;
        data: any;
        action: CustomAction;

        apply(): void {
            eval(this.action as string).call(this, this.targetElementId, this.data);
        }

        static fromJson(jsonObject: IClientSideCustomOperation): IClientSideOperation {
            var operation = new ClientSideCustomOperation();
            operation.targetElementId = jsonObject.targetElementId;
            operation.action = jsonObject.action;
            operation.data = jsonObject.data;

            return operation;
        }
    }

    class ClientSideValuePickerOperation extends ClientSideCustomOperation {
        type = ClientSideOperationType.ValuePicker;

        static fromJson(jsonObject: IClientSideValuePickerOperation): IClientSideOperation {
            var operation = new ClientSideCustomOperation();
            operation.targetElementId = jsonObject.targetElementId;
            operation.action = ValuePicker.setValue;
            operation.data = jsonObject.data;

            return operation;
        }
    }

    class ClientSideMultiValuePickerOperation extends ClientSideCustomOperation {
        type = ClientSideOperationType.ValuePicker;

        static fromJson(jsonObject: IClientSideValuePickerOperation): IClientSideOperation {
            var operation = new ClientSideCustomOperation();
            operation.targetElementId = jsonObject.targetElementId;
            operation.action = ValuePicker.setMultiValue;
            operation.data = jsonObject.data;

            return operation;
        }
    }

    export class ClientSideEditableOperation implements IClientSideOperation {
        public type = ClientSideOperationType.Editable;
        public targetElementId: string;
        public isEditable: boolean;

        public static fromJson(jsonObject: any): ClientSideEditableOperation {
            var operation = new ClientSideEditableOperation();
            operation.targetElementId = jsonObject.targetElementId;
            operation.isEditable = jsonObject.isEditable;

            return operation;
        }

        public apply(): void {
            var element = $(document.getElementById(this.targetElementId) as HTMLElement);

            element.prop('readonly', !this.isEditable);
        }
    }
}