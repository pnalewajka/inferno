/// <reference path="../Atomic/Globals.ts"/>

namespace Hub {
    export const hubClass = "hub-trigger";

    export function init($context: JQuery) {
        $context
            .find("*[data-hub-identifier][data-hub-recordid]")
            .each((index, element) => {
                if (!$(element).hasClass(hubClass)) {
                    $(element)
                        .addClass(hubClass)
                        .click(onClick);
                }
            });
    }

    export function open(recordId: number, identifier: string): void {
        const url = Globals.resolveUrl(`~/Hub/RenderHub?identifier=${identifier}&id=${recordId}`);

        Dialogs.showDialog({ actionUrl: url });
    }

    function onClick(this: HTMLElement, e: JQueryMouseEventObject) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        const target = $(this);
        const identifier = target.data("hub-identifier");
        const recordId = target.data("hub-recordid");

        open(recordId, identifier);
    }
}
