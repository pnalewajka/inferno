﻿module CommonDialogs {

    export interface IAlertSettings {
        htmlMessage: string,
        title: string,
        eventHandler: Dialogs.IButtonClickedEventHandler | undefined,
        iconName: string,
        okButtonText: string
    }

    class AlertDialogBuilder extends Dialogs.DialogBuilder {
        constructor(htmlMessage: string, title: string, eventHandler: Dialogs.DialogEventHandler, iconName: string, okButtonText: string) {
            title = title ? title : Globals.resources.AlertDialogTitle;
            okButtonText = okButtonText ? okButtonText : Globals.resources.AlertDialogOkButtonText;

            var buttons = [
                new Dialogs.DialogButton(okButtonText, "ok")
            ];

            super();
            this.init("common-dialog-alert", htmlMessage, title, eventHandler, iconName, buttons, []);
        }
    }

    export function alert(alertSettings: IAlertSettings) {
        const { htmlMessage, title, eventHandler, iconName, okButtonText } = alertSettings
        if (Strings.isNullOrEmpty(htmlMessage)) {
            return;
        }

        var dialogBuilder = new AlertDialogBuilder(htmlMessage, title, new Dialogs.DialogEventHandler({ eventHandler }),
            iconName,
            okButtonText);
        dialogBuilder.buildAndShow();
    }

    class ConfirmDialogBuilder extends Dialogs.DialogBuilder {
        constructor(htmlMessage: string, title: string, eventHandler: Dialogs.DialogEventHandler, iconName?: string, okButtonText?: string, cancelButtonText?: string) {
            title = (!Strings.isNullOrEmpty(title) ? title : Globals.resources.ConfirmDialogTitle) as string;
            okButtonText = (!Strings.isNullOrEmpty(okButtonText) ? okButtonText : Globals.resources.ConfirmDialogOkButtonText) as string;
            cancelButtonText = (!Strings.isNullOrEmpty(cancelButtonText) ? cancelButtonText : Globals.resources.ConfirmDialogCancelButtonText) as string;
            iconName = (!Strings.isNullOrEmpty(iconName) ? iconName : "") as string;

            var buttons = [
                new Dialogs.DialogButton(okButtonText, "ok"),
                new Dialogs.DialogButton(cancelButtonText, "cancel")
            ];

            super();
            this.init("common-dialog-confirm", htmlMessage, title, eventHandler, iconName, buttons, []);
        }
    }

    export function confirm(htmlMessage: string, title: string, eventHandler?: Dialogs.IButtonClickedEventHandler, iconName?: string, okButtonText?: string, cancelButtonText?: string) {
        if (Strings.isNullOrEmpty(htmlMessage)) {
            return;
        }

        var dialogBuilder = new ConfirmDialogBuilder(htmlMessage, title, new Dialogs.DialogEventHandler({ eventHandler }), iconName, okButtonText, cancelButtonText);
        dialogBuilder.buildAndShow();
    }

    class PromptDialogBuilder extends Dialogs.DialogBuilder {
        public static dialogId = "common-dialog-prompt";
        public static inputTag = "prompt-input";

        constructor(htmlMessage: string, title: string, inputLabel: string, eventHandler: Dialogs.DialogEventHandler, iconName: string, okButtonText: string, cancelButtonText: string) {
            title = title ? title : Globals.resources.PromptDialogTitle;
            okButtonText = okButtonText ? okButtonText : Globals.resources.PromptDialogOkButtonText;
            cancelButtonText = cancelButtonText ? cancelButtonText : Globals.resources.PromptDialogCancelButtonText;
            inputLabel = inputLabel ? inputLabel : Globals.resources.PromptDialogInputLabel;

            var buttons = [
                new Dialogs.DialogButton(okButtonText, "ok"),
                new Dialogs.DialogButton(cancelButtonText, "cancel")
            ];

            var inputs = [
                new Dialogs.DialogInput(inputLabel, PromptDialogBuilder.inputTag)
            ];

            super();
            this.init(PromptDialogBuilder.dialogId, htmlMessage, title, eventHandler, iconName, buttons, inputs);
        }
    }

    export function prompt(htmlMessage: string, title: string, inputLabel: string, eventHandler: Dialogs.IPromptButtonClickedEventHandler, iconName: string, okButtonText: string, cancelButtonText: string) {
        if (Strings.isNullOrEmpty(htmlMessage)) {
            return;
        }

        var dialogBuilder = new PromptDialogBuilder(htmlMessage, title, inputLabel, new Dialogs.DialogEventHandler({
            eventHandler,
            onBeforeHide: (e) => {
                var clickedButton = $(e.delegateTarget).attr("data-button");

                if (clickedButton === "ok") {
                    var inputElement = $(e.target).find("input[data-input='" + PromptDialog.inputTag + "']");
                    var inputValidationLabel = inputElement.next();

                    if (inputElement.val() === "") {
                        inputElement.addClass("input-validation-error");
                        inputValidationLabel.removeClass("field-validation-valid");
                        inputValidationLabel.addClass("field-validation-error");

                        return false;
                    }
                    else {
                        inputElement.removeClass("input-validation-error");
                        inputValidationLabel.addClass("field-validation-valid");
                        inputValidationLabel.removeClass("field-validation-error");

                        return true;
                    }
                }

                return true;
            }
        }), iconName, okButtonText, cancelButtonText);
        dialogBuilder.buildAndShow();
    }

    class PleaseWaitDialogBuilder extends Dialogs.DialogBuilder {
        constructor() {
            super();
            this.init(PleaseWaitDialog.dialogId, Globals.resources.PleaseWaitMessage, "", null, "glyphicon-time", [], []);
        }

        public getModalOptions(): any {
            return { backdrop: "static" };
        }
    }

    export class PleaseWaitDialog {
        private timeOutId: number = 0;
        private isShown: boolean = false;

        public static dialogId = "common-dialog-please-wait";

        public show(delayInMilliseconds: number = 100) {
            this.timeOutId = window.setTimeout(() => { this.showPleaseWait(); }, delayInMilliseconds);
        }

        public hide() {

            if (this.timeOutId) {
                var timeOutId = this.timeOutId;
                this.timeOutId = 0;

                window.clearTimeout(timeOutId);
            }

            if (this.isShown) {
                $("#" + PleaseWaitDialog.dialogId).remove();
            }
        }

        private showPleaseWait() {
            if (this.timeOutId === 0) {
                return;
            }

            this.timeOutId = 0;
            this.isShown = true;

            var dialogBuilder = new PleaseWaitDialogBuilder();
            dialogBuilder.buildAndShow();
        }
    }

    export class PromptDialog {
        public static id = PromptDialogBuilder.dialogId;
        public static inputTag = PromptDialogBuilder.inputTag;
    }

    export var pleaseWait: PleaseWaitDialog = new PleaseWaitDialog();
}
