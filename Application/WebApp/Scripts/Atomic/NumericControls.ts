﻿module NumericControls {
    export class NumberControl extends Controls.Component {
        private isNullable: boolean;
        private numberInput: JQuery;
        private precision: number;

        constructor(elem: Element) {
            super($(elem));
            this.numberInput = $(elem).find("input");
            this.precision = parseInt(this.numberInput.attr('data-number-precision'));
            this.isNullable = Utils.parseBoolean(this.numberInput.attr('data-is-nullable'));

            var increment = $(elem).find(".number-increment");
            if (increment !== null) {
                $(increment).click(() => {
                    this.numberControlSetValue(1);
                });
            }

            var decrement = $(elem).find(".number-decrement");
            if (decrement !== null) {
                $(decrement).click(() => {
                    this.numberControlSetValue(-1);
                });
            }

            var setNull = $(elem).find(".number-set-null");
            if (setNull !== null) {
                $(setNull).click(() => {
                    this.numberControlSetValue(null);
                });
            }

            this.numberInput.on('change', () => {
                this.numberControlSetValue(0);
            });
        }

        isDisabled(): boolean {
            return this.numberInput.is('.disabled')
                || this.numberInput.attr('disabled') !== undefined
                || this.numberInput.attr('readonly') !== undefined;
        }

        parseValue(value: string): number {
            if (this.precision === 0) {
                return parseInt(value);
            }

            if (value.length == 0) {
                return NaN;
            }

            value = value.replace('.', Globals.getDecimalSeparator());

            var regexExpression = "^(\\-|\\+)?([0-9]+)?(\\" + Globals.getDecimalSeparator() + ')?([0-9]+)?$';
            var regex = new RegExp(regexExpression);

            if (regex.test(value)) {
                var parsedValue = Number(value.replace(Globals.getDecimalSeparator(), '.'));
                var matches = regex.exec(value);

                if (value.indexOf(Globals.getDecimalSeparator()) > 0 && matches != null && matches.length >= 2) {
                    var precision = matches[matches.length - 1].length - 1;
                    if (precision > this.precision) {
                        parsedValue = this.round(parsedValue);
                    }
                }

                return parsedValue;
            }

            return NaN;
        }

        round(value: number): number {
            var factor = Math.pow(10, this.precision);
            var addValue = 0.000001;
            var roundedValue = Math.round(value * factor + addValue) / factor;

            return roundedValue;
        }

        formatValue(value: number): string {
            return (value.toFixed(this.precision).toString()).replace('.', Globals.getDecimalSeparator());
        }

        numberControlSetValue(delta: number | null): void {
            if (this.isDisabled()) {
                return;
            }

            if (this.isNullable && delta == null) {
                this.numberInput.val('');
                return;
            }

            var actualValue = this.parseValue(this.numberInput.val());

            if (this.isNullable && delta === 0 && isNaN(actualValue)) {
                this.numberInput.val('');
                return;
            }

            if (isNaN(actualValue)) {
                actualValue = 0;
            }

            var minValue = this.parseValue(this.numberInput.attr('data-min-value'));
            var maxValue = this.parseValue(this.numberInput.attr('data-max-value'));

            var value = actualValue + (delta != null ? delta : 0);

            if (minValue !== NaN) {
                if (value < minValue) {
                    value = minValue;
                }
            }

            if (maxValue !== NaN) {
                if (value > maxValue) {
                    value = maxValue;
                }
            }

            if (value !== actualValue) {
                this.numberInput.val(this.formatValue(value));
                this.numberInput.trigger("change");
            }
        }
    }
} 