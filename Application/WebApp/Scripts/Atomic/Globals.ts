﻿module Globals {
    export var resources: any;
    export var enumResources: any;

    export interface IFormattingData {
        dateFormat: string;
        decimalSeparator: string;
        datePickerLocale: string;
    }

    export interface IApplicationSettings {
        googleMapsJavaScriptApiKey: string;
        googleMapsIsSearchEnabled: boolean;
    }

    export interface ICurrentUser {
        Id: number
        CanChangeOwnPassword: boolean;
        CanShowNtlmBypassLogin: boolean;
        ChoreCount: number;
        CompanyName: string;
        FirstName: string;
        FullName: string;
        HasCorporateDetails: boolean;
        IsAuthenticated: boolean;
        JobTitle: string;
        LastName: string;
        Login: string;
        Roles: string[];
        ShortName: string;
    }

    export var formatting: IFormattingData;

    export var settings: IApplicationSettings;

    export var parameters: { [key: string]: any };

    export function getDateFormat(): string {
        return formatting.dateFormat;
    }

    export function getDatePickerLocale(): string {
        return formatting.datePickerLocale;
    }

    export function getDecimalSeparator(): string {
        return formatting.decimalSeparator;
    }

    export interface INavigationData {
        baseUrl: string;
        currentUser: ICurrentUser;
    }

    export var navigation: INavigationData;

    export function resolveUrl(virtualPath: string): string {
        if (virtualPath.substr(0, 2) === "~/") {
            return navigation.baseUrl + virtualPath.substr(2);
        }

        return virtualPath;
    }

    export function getParameterAsString(parameterKey: string): string {
        return <string>Globals.getParameterAsObject(parameterKey);
    }

    export function getParameterAsNumber(parameterKey: string): number {
        return Number(Globals.getParameterAsObject(parameterKey));
    }

    export function getParameterAsObject(parameterKey: string): any {
        if (!parameters[parameterKey]) {
            throw new Error("Parameter key [" + parameterKey + "] is not defined");
        }

        return parameters[parameterKey];
    }

    export function setClickHandlerOnEnvironmentLabel() {
        $(".environment-label.stage.can-switch, .environment-label.prod.can-switch")
            .css('cursor', 'pointer')
            .click((e: JQueryEventObject) => {
                if (e.target.innerHTML === 'STAGE') {
                    Forms.redirect(window.location.href.replace('dante-stage.intive.org', 'dante.intive.org'));
                } else if (e.target.innerHTML === 'PROD') {
                    Forms.redirect(window.location.href.replace('dante.intive.org', 'dante-stage.intive.org'));
                }
            });
    }
}