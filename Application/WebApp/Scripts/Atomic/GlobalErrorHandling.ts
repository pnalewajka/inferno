﻿/// <reference path="../typings/jquery/jquery.d.ts"/>

module GlobalErrorHandling {
    export function init(): void {
        $.ajaxSetup({
            error: (xhr: XMLHttpRequest) => {
                if (xhr.status !== 200) {
                    handleAjaxErrorInDialog(xhr.responseText);
                }
            }
        });
    }

    export function handleAjaxErrorInBody(ajaxResponseText: any, element?: JQuery) {
        if (typeof ajaxResponseText === "undefined" || ajaxResponseText == null) {
            return;
        }

        try {
            var resultModel = <IErrorResultModel>JSON.parse(ajaxResponseText);

            if (typeof element === "undefined" || ajaxResponseText == null) {
                element = $("body");
            }

            var html: string = '';

            html += '<div class="content-wrapper"><div class="content container-fluid">';
            html += `\t<p class="text-muted error-message">${resultModel.message}</p>`
            html += '</div><div>';

            element.html(html);
        }
        catch (exception) {
            $("body").html(ajaxResponseText);
        }
    };

    export function handleAjaxErrorInDialog(ajaxResponseText: any) {
        if (typeof ajaxResponseText === "undefined" || ajaxResponseText == null) {
            return;
        }

        var dialogBuilder = new ErrorDialogBuilder(ajaxResponseText);

        if (!dialogBuilder.stopProcessing) {
            dialogBuilder.buildAndShow();
        }
    };

    interface IAjaxException {
        message: string;
        exception: string;
        innerExceptions: string[];
    }

    interface IErrorViewModel {
        isBusinessException: boolean;
        exception: IAjaxException;
        actionName: string;
        controllerName: string;
        form: any;
        formMethod: string;
        systemVersion: string;
        reportNewIssueUrl: string;
        isRoutingDataAvailable: string;
        areDetailsAvailable: boolean;
    }

    interface IErrorResultModel {
        error: boolean;
        model: IErrorViewModel;
        message: string;
    }

    class ErrorDialogBuilder extends Dialogs.DialogBuilder {
        constructor(responseText: string) {
            var title: string = Globals.resources.AjaxErrorDialogTitle;

            var buttons = [
                new Dialogs.DialogButton('<i class="fa fa-refresh"></i> ' + Globals.resources.AjaxErrorDialogRefreshButtonText, 'refresh', false),
                new Dialogs.DialogButton('<i class="fa fa-bullhorn"></i> ' + Globals.resources.AjaxErrorDialogReportIssueButtonText, 'report-issue', false)
            ];

            super();

            var dialogBody: string;
            var reportIssueUrl: string;
            var eventHandler: Dialogs.DialogEventHandler;

            try {
                var resultModel = <IErrorResultModel>JSON.parse(responseText);
                dialogBody = this.jsonResponseGetHtml(resultModel.model);
                reportIssueUrl = resultModel.model.reportNewIssueUrl;
                eventHandler = new Dialogs.DialogEventHandler({
                    onBeforeShow: (dialog) => {

                        $(dialog).find('div.modal-dialog').addClass(resultModel.model.areDetailsAvailable ? 'modal-lg' : 'modal-md');

                        $(dialog).find('button[data-button="refresh"], button[data-dismiss="modal"]').click((e: JQueryEventObject) => {
                            e.stopPropagation();
                            location.reload();
                        });

                        var reportIssueButton = $(dialog).find('button[data-button="report-issue"]');

                        if (!reportIssueUrl) {
                            reportIssueButton.addClass('collapse');
                        } else {
                            reportIssueButton.click(() => {
                                window.open(reportIssueUrl);
                            });
                        }
                    }
                });
            } catch (exception) {
                this.nonJsonResponse(responseText);
                this.stopProcessing = true;

                return;
            }

            this.init("common-dialog-alert", dialogBody, title, eventHandler, "", buttons, []);
        }

        private nonJsonResponse(responseText: string): void {
            $("body").html(responseText);
        }

        private jsonResponseGetHtml(errorViewModel: IErrorViewModel): string {
            var html: string = '';

            if (!errorViewModel.areDetailsAvailable) {
                html += `<div><p class="text-muted error-message">${errorViewModel.exception.message}</p></div>`;
                return html;
            }

            html += '<div>\n\r';
            html += '\t<p class="text-muted error-message">' + errorViewModel.exception.message + '</p>\n\r';
            html += '\t<div class="error-details-section">\n\r';
            html += '\t\t<h4 data-toggle="collapse" href="#error-details" ' + (errorViewModel.isBusinessException ? 'class="collapsed"' : '') + '>' + Globals.resources.AjaxErrorDialogShowDetailsHeaderText + '</h4>\n\r';
            html += '\t\t<div id="error-details" class="collapse' + (errorViewModel.isBusinessException ? '' : ' in') + '">\n\r';
            html += '\t\t\t<h5>' + Globals.resources.AjaxErrorDialogExceptionLabel + '</h5>\n\r';
            html += '\t\t\t<div class="well">\n\r';
            html += '\t\t\t\t<div class="exception-message">' + errorViewModel.exception.exception + '</div>\n\r';
            html += '\t\t\t\t</div>\n\r';

            $.each(errorViewModel.exception.innerExceptions, (index, innerException) => {
                html += '\t\t\t<h5>' + Globals.resources.AjaxErrorDialogInnerExceptionLabel + '</h5>\n\r';
                html += '\t\t\t<div class="well">\n\r';
                html += '\t\t\t\t<div class="exception-message">' + innerException + '</div>\n\r';
                html += '\t\t\t\t</div>\n\r';
            });

            html += '\t\t\t<h5>' + Globals.resources.AjaxErrorDialogErrorSourceLabel + '</h5>\n\r';
            html += '\t\t\t<table class="table table-striped table-condensed">\n\r';

            if (errorViewModel.isRoutingDataAvailable) {
                html += '\t\t\t<tr>\n\r';
                html += '\t\t\t<td>' + Globals.resources.AjaxErrorDialogActionLabel + '</td>\n\r';
                html += '\t\t\t<td>' + errorViewModel.actionName + '</td>\n\r';
                html += '\t\t\t</tr>\n\r';
                html += '\t\t\t<tr>\n\r';
                html += '\t\t\t<td>' + Globals.resources.AjaxErrorDialogControllerLabel + '</td>\n\r';
                html += '\t\t\t<td>' + errorViewModel.controllerName + '</td>\n\r';
                html += '\t\t\t</tr>\n\r';
            }

            html += '\t\t\t<tr>\n\r';
            html += '\t\t\t<td>' + Globals.resources.AjaxErrorDialogSystemVersionLabel + '</td>\n\r';
            html += '\t\t\t<td>' + errorViewModel.systemVersion + '</td>\n\r';
            html += '\t\t\t</tr>\n\r';
            html += '\t\t\t</table>\n\r';

            if (errorViewModel.form) {
                html += '\t\t\t<h5>' + Globals.resources.AjaxErrorDialogFormVariablesLabel + '</h5>\n\r';
                html += '\t\t\t<table class="table table-striped table-condensed">\n\r';
                $.each(errorViewModel.form, (index, keyValue) => {
                    html += '\t\t\t\t<tr>\n\r';
                    html += '\t\t\t\t<td>' + keyValue.Key + '</td>\n\r';
                    html += '\t\t\t\t<td>' + keyValue.Value + '</td>\n\r';
                    html += '\t\t\t\t</tr>\n\r';
                });
                html += '\t\t\t</table>\n\r';
            }

            html += '\t\t</div>\n\r';
            html += '\t</div>\n\r';
            html += '</div>\n\r';

            return html;
        }

        public getModalOptions(): any {
            return { backdrop: 'static', keyboard: false };
        }
    }
}

$(document).ready(() => {
    GlobalErrorHandling.init();
});
