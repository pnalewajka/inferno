﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/jquery.plugins/jquery.plugins.d.ts"/>
/// <reference path="../typings/googlemaps/google.maps.d.ts"/>

module MapPicker {
    import ErrorDictionary = JQueryValidation.ErrorDictionary;
    import ErrorListItem = JQueryValidation.ErrorListItem;

    export class GoogleMaps extends Controls.Component {
        private static cultureIndependantDecimalSeparator: string= ".";
        private defaultLocation: google.maps.LatLng;
        private isInPlaceEditor: boolean;
        private dialogString: string;
        private defaultRadius: number;
        private dialogId: string;

        private isLatLngHidden: boolean;
        private isLocationHidden: boolean;
        private isRadiusHidden: boolean;
        private isSearchHidden: boolean;
        private isNullable: boolean;
        private bindableInputContext: JQuery;
        private changing: boolean;

        private marker: google.maps.Marker | null;
        private map: google.maps.Map | null;
        private circle: google.maps.Circle | null;
        private searchBox: google.maps.places.SearchBox | null;

        constructor(elem: Element) {
            super($(elem));

            this.dialogId = "map-picker-dialog-" + $(elem).attr("data-property-name");
            this.defaultRadius = Globals.getParameterAsNumber("System.MapPicker.DefaultRadiusInMeters");

            this.isInPlaceEditor = Utils.parseBoolean($(elem).attr('data-in-place-editor'));
            this.isLatLngHidden = Utils.parseBoolean($(elem).attr('data-option-latlng-hidden'));
            this.isLocationHidden = Utils.parseBoolean($(elem).attr('data-option-location-hidden'));
            this.isRadiusHidden = Utils.parseBoolean($(elem).attr('data-option-radius-hidden'));
            this.isSearchHidden = Utils.parseBoolean($(elem).attr('data-option-search-hidden'));
            this.isNullable = Utils.parseBoolean($(elem).attr('data-option-is-nullable'));

            if (!this.isInPlaceEditor) {
                var dialog = $(elem).find('div.map-picker-popup.hidden');

                if (dialog != null) {
                    this.dialogString = dialog.html();
                    dialog.remove();
                }
            } else {
                this.bindableInputContext = this.container;
            }
        }

        public initialize(): void {
            this.defaultLocation = this.parseLatLng(Globals.getParameterAsString("System.MapPicker.DefaultLocation"));

            this.container.find("span.map-picker-popup-button").click(() => {
                this.popupButtonClicked();
            });

            this.container.find("span.map-picker-remove-button").click(() => {
                this.clearState();
            });

            if (this.isInPlaceEditor) {
                this.initEditorMap();
            }
        }

        private popupButtonClicked(): void {
            this.marker = null;
            this.map = null;

            this.showMapPickerDialog();
        }

        private getLocation(context: JQuery): google.maps.LatLng | null {
            var latitude = Utils.parseDecimal(context.find('input[data-input-name="latitude"]').val());
            var longitude = Utils.parseDecimal(context.find('input[data-input-name="longitude"]').val());

            if (isNaN(latitude) || isNaN(longitude)) {
                return null;
            }

            return new google.maps.LatLng(latitude, longitude);
        }

        private getRadius(context: JQuery): number | null {
            var radius = Utils.parseDecimal(context.find('input[data-input-name="radius"]').val());

            if (isNaN(radius)) {
                return null;
            }

            return radius;
        }

        private setLocation(newLocation: string): void {
            this.container.find("input[type='hidden']").val(newLocation);
        }
        
        private fixGoogleMapFontProblem(): void {
            // hack for all browsers except chrome.
            // Google Maps API, fetches additional font for search box, but the font doesn't support latin-ext (regional characters)
            // We change <link> node with font again, after Google's script does it.
            // This hack should be here until ticket on google tracker is resolved. 
            // Ticket: https://code.google.com/p/gmaps-api-issues/issues/detail?id=10136

            var head = document.getElementsByTagName('head')[0];

            var insertBefore = head.insertBefore;

            head.insertBefore = <T extends Node>(newElement: T, referenceElement: Element | null) => {
                insertBefore.call(head, newElement, referenceElement);

                var fontLinkItem = newElement.attributes.getNamedItem("href");

                if (fontLinkItem != null && fontLinkItem.nodeValue != null && fontLinkItem.nodeValue.indexOf('https://fonts.googleapis.com/css?family=Roboto') === 0) {
                    fontLinkItem.nodeValue = 'https://fonts.googleapis.com/css?family=Roboto:300&subset=latin-ext,latin';
                    newElement.attributes.setNamedItem(fontLinkItem);
                }

                return newElement;
            };
        }

        private initEditorMap(): void {
            var mapElement = this.container.find("div.map-picker-editor-map").get(0);
            var actualLocation = this.getLocation(this.container);
            var defaultLocationUsed = false;
            this.bindInputs();

            if (actualLocation == null) {
                actualLocation = this.defaultLocation;
                defaultLocationUsed = true;
            }

            var options = {
                zoom: 14,
                center: actualLocation,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: false,
                zoomControl: true,
                draggable: true,
                keyboardShortcuts: true,
                panControl: true,
                scaleControl: true
            };

            this.fixGoogleMapFontProblem();

            this.map = new google.maps.Map(mapElement, options);

            google.maps.event.addListener(this.map, 'click', (event: google.maps.MouseEvent) => {
                this.addMarker(event.latLng);
            });

            this.initSearchBox();
            this.initClearButton();

            if (!defaultLocationUsed) {
                this.addMarker(actualLocation);
            }

            this.fixMapDisplay();
        }

        private getMap(): google.maps.Map {
            if (this.map == null)
                throw new Error("Map is null");

            return this.map as google.maps.Map;
        }

        private getMarker(): google.maps.Marker {
            if (this.marker == null)
                throw new Error("Marker is null");

            return this.marker as google.maps.Marker;
        }

        private getSearchBox(): google.maps.places.SearchBox {
            if (this.searchBox == null)
                throw new Error("SearchBox is null");

            return this.searchBox as google.maps.places.SearchBox;
        }

        private initSearchBox(): void {
            if (this.isSearchHidden) {
                return;
            }

            var searchBoxInput = this.bindableInputContext.find('input.pac-input');

            if (searchBoxInput.length === 0) {
                return;
            }

            var input = <HTMLInputElement>searchBoxInput.get(0);
            this.searchBox = new google.maps.places.SearchBox(input);
            this.getMap().controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            this.getMap().addListener('bounds_changed', () => {
                this.getSearchBox().setBounds(this.getMap().getBounds());
            });

            searchBoxInput.on('keydown', (e) => {
                if (e.which === 13) {
                    return false;
                }

                return true;
            });

            this.searchBox.addListener('places_changed', () => {
                var places = this.getSearchBox().getPlaces();

                if (places.length === 0) {
                    return;
                }

                if (this.marker != null) {
                    this.marker = null;
                }

                this.addMarker(places[0].geometry.location);
                this.updateMarker();

                this.getMap().panTo(this.getMarker().getPosition());
            });
        }

        private initClearButton(): void {
            if (!this.isNullable) {
                return;
            }

            var buttonDiv = this.bindableInputContext.find('div.pac-button');

            if (buttonDiv.length === 0) {
                return;
            }

            var button = buttonDiv.get(0);
            this.getMap().controls[google.maps.ControlPosition.TOP_RIGHT].push(button);
            buttonDiv.on('click', () => {
                this.clearState();
            });
        }

        private clearState(): void {
            var dataInputNames = ['latitude', 'longitude', "radius", "location"];

            if (this.marker != null) {
                if (this.circle != null) {
                    this.circle = null;
                }
                
                this.marker = null;
            }

            this.changing = true;

            for (var i = 0; i < dataInputNames.length; i++) {
                var dataInputName = dataInputNames[i];
                var targetInput = this.container.find('input[data-input-name="' + dataInputName + '"]');

                if (targetInput.length === 1) {
                    targetInput.val('');
                }
            }

            this.changing = false;
        }

        private initPopupMap(dialog: JQuery): void {
            this.bindableInputContext = dialog;
            var actualLocation = this.getLocation(this.container);
            var defaultLocationUsed = false;

            if (actualLocation == null) {
                actualLocation = this.defaultLocation;
                defaultLocationUsed = true;
            }

            var options = {
                zoom: 14,
                center: actualLocation,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: false,
                zoomControl: true,
                draggable: true,
                keyboardShortcuts: true,
                panControl: true,
                scaleControl: true
            };

            this.copyValues(this.container, this.bindableInputContext);

            var mapElement = dialog.find("div.map-picker-popup-map").get(0);
            this.map = new google.maps.Map(mapElement, options);

            google.maps.event.addListener(this.map, 'click', (event: google.maps.MouseEvent) =>{
                this.addMarker(event.latLng);
            });

            this.initSearchBox();

            if (!defaultLocationUsed) {
                this.addMarker(actualLocation);
            }
        }

        private getLocalizedDecimal(value: number): string {
            return value.toString().replace(GoogleMaps.cultureIndependantDecimalSeparator, Globals.getDecimalSeparator());
        }

        private updateInputs(latLng: google.maps.LatLng): void {
            this.changing = true;
            this.bindableInputContext.find('input[data-input-name="latitude"]').val(this.getLocalizedDecimal(latLng.lat()));
            this.bindableInputContext.find('input[data-input-name="longitude"]').val(this.getLocalizedDecimal(latLng.lng()));
            this.changing = false;
        }

        private bindInputs() {
            this.bindableInputContext.find('input[data-input-name="latitude"]').on('change', () => {
                this.setMarkerPosition();
            });

            this.bindableInputContext.find('input[data-input-name="longitude"]').on('change', () => {
                this.setMarkerPosition();
            });

            this.bindableInputContext.find('input[data-input-name="radius"]').on('change', () => {
                this.updateRadius();
            });
        }

        private setMarkerPosition(): void {
            if (this.changing) {
                return;
            }

            var inputLongitude = this.bindableInputContext.find('input[data-input-name="longitude"]');

            if (!inputLongitude.valid()) {
                return;
            }

            var inputLatitude = this.bindableInputContext.find('input[data-input-name="latitude"]');

            if (!inputLatitude.valid()) {
                return;
            }

            var latitude = Utils.parseDecimal(inputLatitude.val());
            var longitude = Utils.parseDecimal(inputLongitude.val());

            if (this.marker) {
                this.marker.setPosition(new google.maps.LatLng(latitude, longitude));
                this.getMap().panTo(this.marker.getPosition());
            }
        }

        private updateMarker(): void {
            this.geocodePosition(this.getMarker().getPosition());
            this.updateInputs(this.getMarker().getPosition());
        }

        private addMarker(latLng: google.maps.LatLng): void {
            if (this.marker != null) {
                return;
            }

            this.marker = new google.maps.Marker({
                position: latLng,
                map: this.getMap(),
                animation: google.maps.Animation.DROP,
                draggable: true
            });

            this.addRadius(this.container, latLng);

            this.geocodePosition(latLng);
            this.updateInputs(latLng);

            google.maps.event.addListener(this.marker, 'dragend', () => {
                this.geocodePosition(this.getMarker().getPosition());
                this.updateInputs(this.getMarker().getPosition());
            });
        }

        private addRadius(context: JQuery, latLng: google.maps.LatLng): void {
            var radiusInput = this.bindableInputContext.find('input[data-input-name="radius"]');

            if (this.isRadiusHidden) {
                radiusInput.val(this.defaultRadius);

                return;
            }

            var radius = parseInt(radiusInput.val());

            if (isNaN(radius)) {
                radius = this.defaultRadius;
                this.changing = true;
                radiusInput.val(radius);
                this.changing = false;
            }

            var options = {
                strokeColor: "#0000FF",
                strokeOpacity: 0.35,
                strokeWeight: 2,
                fillColor: "#0000FF",
                fillOpacity: 0.20,
                radius: radius,
                center: latLng,
                map: this.getMap()
            };

            this.circle = new google.maps.Circle(options);
            this.getMarker().bindTo("position", this.circle, "center");
        }

        private updateRadius(): void {
            if (this.changing) {
                return;
            }

            var input = this.bindableInputContext.find('input[data-input-name="radius"]');

            if (!input.valid()) {
                return;
            }

            if (this.circle == null) {
                return;
            }

            var newRadius = parseInt(input.val());
            this.circle.setRadius(newRadius);
        }

        private geocodePosition(latLng: google.maps.LatLng): void {
            if (this.isLocationHidden) {
                return;
            }

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                    location: latLng
            }, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                    this.bindableInputContext.find('input[data-input-name="location"]').val(results[0].formatted_address);
                } 
            });
        }

        private parseLatLng(value: string): google.maps.LatLng {
            var parts = value.split(',');

            if (parts.length !== 2) {
                return new google.maps.LatLng(0,0);
            }

            var latitude = parseFloat(parts[0]);
            var longitude = parseFloat(parts[1]);

            return new google.maps.LatLng(latitude, longitude);
        }

        private showMapPickerDialog() {
            var title = this.container.data('dialog-title');

            var eventHandler: Dialogs.IButtonClickedEventHandler = (eventArgs) => {
                if (eventArgs.isOk) {
                    this.copyValues(this.bindableInputContext, this.container);
                }
            };

            var onBeforeInit = (dialog: JQuery) => {
                this.initPopupMap(dialog);
            };

            var onBeforeShow = () => {
                this.bindInputs();
            };

            var onBeforeHide = (e: JQueryEventObject): boolean => {
                var clickedButton = $(e.delegateTarget).attr("data-button");

                if (clickedButton === "ok") {
                    var form = $(e.target).closest('.modal').find('form');
                    form.validateBootstrap(true);

                    return form.valid();
                }

                return true;
            };
                                                                                                  
            var dialogBuilder = new MapPickerDialogBuilder(this.dialogId, title, this.dialogString, new Dialogs.DialogEventHandler({ eventHandler, onBeforeInit, onBeforeHide, onBeforeShow }));
            dialogBuilder.buildAndShow();
            this.fixMapDisplay();
        }

        private copyValues(sourceContext: JQuery, targetContext: JQuery): void {
            var dataInputNames = ['latitude', 'longitude', "radius", "location"];

            for (var i = 0; i < dataInputNames.length; i++) {
                var dataInputName = dataInputNames[i];
                var sourceInput = sourceContext.find('input[data-input-name="' + dataInputName + '"]');
                var targetInput = targetContext.find('input[data-input-name="' + dataInputName + '"]');

                if (sourceInput.length === 1 && targetInput.length === 1) {
                    targetInput.val(sourceInput.val());
                }
            }
        }

        private fixMapDisplay(): void {
            var center = this.getMap().getCenter();
            google.maps.event.trigger(this.map, 'resize');
            this.getMap().setCenter(center);
        }
    }

    export function mapsLoadedCallBack(): void {
        var mapPickers = Controls.getAllInstances($(document)).filter((v) => {
            return (v instanceof GoogleMaps);
        });

        mapPickers.forEach((value) => {
            (<GoogleMaps>value).initialize();
        });
    }

    export function oneTimeInitialization(): void {
        $.getScript("https://maps.googleapis.com/maps/api/js?key=" + Globals.settings.googleMapsJavaScriptApiKey + "&callback=MapPicker.mapsLoadedCallBack&libraries=places");
    }

    class MapPickerDialogBuilder extends Dialogs.DialogBuilder {
        constructor(dialogId: string, title: string, body: string, eventHandler: Dialogs.DialogEventHandler) {
            var okButtonText = Globals.resources.ConfirmDialogOkButtonText;
            var cancelButtonText = Globals.resources.ConfirmDialogCancelButtonText;
            super();

            var buttons = [
                new Dialogs.DialogButton(okButtonText, "ok"),
                new Dialogs.DialogButton(cancelButtonText, "cancel")
            ];

            this.init(dialogId, body, title, eventHandler, "", buttons, [], undefined, true);
        }

        public buildAndShow(): void {
            var html = this.getHtml();
            var options = this.getModalOptions();

            Dialogs.showHtmlDialog(html, this.eventHandler, options);
        }
    }

    export function registerCustomValidation(): void {
        $.validator.addMethod("rangeint", (value, element, param) => {
            if (value === '') {
                return true;
            }

            var actualValue = parseInt(value);

            if (isNaN(actualValue)) {
                return false;
            }

            var min = parseInt(param.min);
            var max = parseInt(param.max);

            return actualValue >= min && actualValue <= max;
        });

        $.validator.addMethod("rangedecimal", (value, element, param) => {
            if (value === '') {
                return true;
            }

            var actualValue = Utils.parseDecimal(value);

            if (isNaN(actualValue)) {
                return false;
            }

            var min = Utils.parseDecimal(param.min);
            var max = Utils.parseDecimal(param.max);

            return actualValue >= min && actualValue <= max;
        });

        $.validator.unobtrusive.adapters.add("rangedecimal", ['min', 'max'], (options: any) => {
            options.rules['rangedecimal'] = options.params;
            options.messages['rangedecimal'] = options.message;            
        });

        $.validator.unobtrusive.adapters.add("rangeint", ['min', 'max'], (options: any) => {
            options.rules['rangeint'] = options.params;
            options.messages['rangeint'] = options.message;            
        });
    }

    export function registerCustomPresentation(): void {
        $.validator.setDefaults({
            showErrors: function(this: any, errorMap: ErrorDictionary, errorList: ErrorListItem[]): void {
                var mapPicker = $(this.currentElements[0]).closest('form').find('.map-picker');

                this.defaultShowErrors(errorMap, errorList);

                if (mapPicker.length === 0) {
                    return;
                }

                $.each(this.validElements(), (index, element) => {
                    var input = $(element);

                    input.data("title", "")
                        .tooltip("destroy");
                });

                $.each(errorList, (index, error) => {
                    var input = $(error.element);

                    input
                        .tooltip("destroy")
                        .data("title", error.message)
                        .tooltip();
                });
            }
        });
    }
}