﻿module AlertMessages{
    export enum AlertType {
        Success,
        Information,
        Warning,
        Error
    }

    function getAlertClass(alertType: AlertType) {
        switch (alertType) {
            case AlertType.Success:
                return "alert-success";

            case AlertType.Information:
                return "alert-info";

            case AlertType.Warning:
                return "alert-warning";

            case AlertType.Error:
                return "alert-danger";

            default:
                throw "ArgumentOutOfRangeException";
        }
    }

    export function addAlert(alertType: AlertType, message: string) {
        if ($('.alert-container').length === 0) {
            $('.content').prepend('<div class="alert-container"></div>');
        };
        $('.alert-container').append(
            '<div class="alert ' + getAlertClass(alertType) + '">' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '    <span aria-hidden="true">&times;</span>' +
            '  </button>' +
            '  <div class="alert-message">' + message + '</div>' +
            '</div>');        
    }

    export function addError(message: string) {
        addAlert(AlertType.Error, message);
    }

    export function addInformation(message: string) {
        addAlert(AlertType.Information, message);
    }

    export function addSuccess(message: string) {
        addAlert(AlertType.Success, message);
    }

    export function addWarning(message: string) {
        addAlert(AlertType.Warning, message);
    }
}