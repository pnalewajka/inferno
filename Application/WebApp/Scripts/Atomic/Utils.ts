﻿module Utils {

    // push_back a new task (ensure it executes after all previous events are processed)
    export const pushBackBrowserTask = (task: () => void) => setTimeout(() => task(), 0);

    export function getFunctionByName(functionName: string, context: any | null = null) {
        if (context == null) {
            context = window;
        }

        if (/[\(\)=!]/.test(functionName)) {
            return null;
        }

        var namespaces = functionName.split(".");
        var func = namespaces.pop() as string;

        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }

        if (context == null) {
            return null;
        }

        var member = context[func];

        return $.isFunction(member) ? member : null;
    }

    export function executeHandler(handlerCode: string, parameters: any): any {
        var existingFunction = getFunctionByName(handlerCode);

        if (existingFunction != null) {
            parameters.handler = () => {
                var paramArray = new Array();

                for (var prop in parameters) {
                    if (parameters.hasOwnProperty(prop)) {
                        paramArray.push(parameters[prop]);
                    }
                }

                return existingFunction.apply(parameters, paramArray);
            };
        }
        else {
            parameters.handler = () => {
                for (var prop in parameters) {
                    if (parameters.hasOwnProperty(prop)) {
                        (window as any)[prop] = parameters[prop];
                    }
                }

                return eval(handlerCode);
            };
        }

        return parameters.handler();
    }

    export function parseBoolean(value: string): boolean {
        if (!value) {
            return false;
        }

        var intValue = parseInt(value);

        if (!isNaN(intValue)) {
            return !!intValue;
        }

        return value.toLowerCase() === 'true';
    }

    export function parseDecimal(value: string): number {
        if (!value) {
            return NaN;
        }

        value = value.replace(Globals.getDecimalSeparator(), ".");
        var parsedValue = parseFloat(value);

        return parsedValue;
    }

    export function removeElement(array: any[], element: any): void {
        var index: number;

        while ((index = array.indexOf(element)) !== -1) {
            array.splice(index, 1);
        }
    }

    export function getIconClass(shortIconName: string): string | null {
        if (Strings.isNullOrEmpty(shortIconName)) {
            return null;
        }

        if (shortIconName.indexOf("glyphicon-") === 0) {
            return "glyphicon " + shortIconName;
        }

        if (shortIconName.indexOf("fa-") === 0) {
            return "fa " + shortIconName;
        }

        return null;
    }

    export function getEventTarget(event: Event): Element {
        return event.srcElement || <Element>event.target;
    }

    export function getButtonEventTarget(event: Event): Element {
        const item = getEventTarget(event);

        if (item.tagName !== "BUTTON") {
            return item.parentElement as Element;
        }

        return item;
    }

    export function getMaxValue(items: any[]): any {
        return Math.max.apply(Math, items);
    }

    export function reloadPage() {
        Forms.redirect(Forms.getCurrentUrl());
    }

    export function populateFrom<T>(obj: T, jsonObject: any): T {
        for (var propName in jsonObject) {
            (obj as any)[propName] = jsonObject[propName];
        }

        return obj;
    }

    export function getHtmlList(rows: Grid.IRow[]): string {
        if (rows == null || rows.length === 0) {
            return "";
        }

        var html = "<ul>";

        for (var i in rows) {
            if (rows.hasOwnProperty(i)) {
                html += `<li>${rows[i].toString}</li>`;
            }
        }

        html += "</ul>";

        return html;
    }

    export function pascalCaseToDelimitierSepareted(text: string): string {
        return text.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
    }
} 