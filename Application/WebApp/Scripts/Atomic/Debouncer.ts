﻿namespace Atomic {
    export class Debouncer {
        private _action: () => void;
        private _delay: number;
        private _timeout?: number;

        public static basic(action: () => void, delay: number): () => void {
            const debouncer = new Debouncer(action, delay);

            return debouncer.execute.bind(debouncer);
        }

        private forceExecute(): void {
            this.clearExistingTimeout();
            this._action();
        }

        public constructor(action: () => void, delay: number) {
            this._action = action;
            this._delay = delay;
        }

        public clearExistingTimeout(): void {
            if (this._timeout !== undefined) {
                window.clearTimeout(this._timeout);
                this._timeout = undefined;
            }
        }

        public subscribeWindowClose(): void {
            // unsubscribe if we are already subscribed
            this.unsubscribeWindowClose();

            $(window).on('beforeunload', () => {
                if (this._timeout !== undefined) {
                    return false;
                }

                return;
            });
        }

        public unsubscribeWindowClose(): void {
            $(window).unbind('beforeunload', this._action);
        }

        public execute(): void {
            this.clearExistingTimeout();
            this._timeout = window.setTimeout(() => this.forceExecute(), this._delay);
        }
    }
}
