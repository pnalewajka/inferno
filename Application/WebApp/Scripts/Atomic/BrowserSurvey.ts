﻿/**
 * Contains checks for different feature support in the current browser
 * @module BrowserSurvey
 */
module BrowserSurvey {
    /**
     * Detects if the session storage is supported by the current browser
     * @method isSessionStorageSupported
     */
    export function isSessionStorageSupported(): boolean {
        return typeof window.sessionStorage != "undefined";
    }


    /**
     * Detects if cookies are enabled in the current browser
     * @method areCookiesEnabled
     */
    export function areCookiesEnabled(): boolean {
        if (typeof navigator.cookieEnabled != "undefined" && !navigator.cookieEnabled) {
            return false;
        }

        if (window.document.cookie !== "") {
            return true;
        }

        var testCookie = "test=test";

        window.document.cookie = testCookie;

        if (window.document.cookie === testCookie) {
            window.document.cookie = "";
            return true;
        }

        return false;
    }

    export enum browserType {
        Chrome,
        Firefox,
        Safari,
        Edge,
        Ie
    }

    /**
     * Detects the current browser type
     * @method detectBrowserType
     */
    export function detectBrowserType(): browserType {
        const userAgent = navigator.userAgent.toLowerCase();
        const vendor = navigator.vendor.toLowerCase();

        if ((userAgent.indexOf('chrome') > -1) && (vendor.indexOf("google") > -1)) { return browserType.Chrome; }
        if (userAgent.indexOf('firefox') > -1) { return browserType.Firefox; }
        if (userAgent.indexOf('edge') > -1) { return browserType.Edge; }
        if (userAgent.indexOf('safari') > -1) { return browserType.Safari; }

        return browserType.Ie; // if everything fails it's an internet explorer, eh...
    }
}