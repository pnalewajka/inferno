module ValuePicker {
    export enum SelectionMode { SingleOrNone = -1, Single = 0, Multiple = 1 };

    export class SelectionManager {
        private gotChanges: boolean;
        private selectionMode: SelectionMode;
        private selectedItems: Grid.IRow[];
        private selectedIds: any[];

        constructor(selectionMode: SelectionMode, previousIds: any[] = []) {
            this.selectedIds = previousIds.map((id) => id.toString());
            this.selectionMode = selectionMode;
            this.selectedItems = new Array<Grid.IRow>();

            this.selectedIds.forEach((id) => {
                this.selectedItems[id] = { id: id, toString: "object" + id }
            });
        }

        public select(item: Grid.IRow): void {
            var id = item.id.toString();
            this.selectedItems[id] = item;
            if (this.selectedIds.indexOf(id) === -1) {
                if (this.selectionMode === SelectionMode.Multiple) {
                    this.selectedIds.push(id);
                } else {
                    this.selectedIds[0] = id;
                }
                this.gotChanges = true;
            }
        }

        public unselect(id: any): void {
            var index = this.selectedIds.indexOf(id.toString());
            if (index !== -1) {
                this.selectedIds.splice(index, 1);
                delete this.selectedItems[id];
                this.gotChanges = true;
            }
        }

        public get isOkButtonClickAllowed(): boolean {
            return this.isSelectionValid;
        }

        public get isSelectionValid(): boolean {
            return this.getSelectedIds().length > 0
                || this.selectionMode === SelectionMode.SingleOrNone
                || this.selectionMode === SelectionMode.Multiple;
        }

        public get shouldDisplaySelectionHint(): boolean {
            return this.selectionMode === SelectionMode.Multiple;
        }

        public getSelectedItems(): Grid.IRow[] {
            return this.selectedIds.map((id) => this.selectedItems[id]);
        }

        public getSelectedIds(): any[] {
            return this.selectedIds;
        }

        public getSelectionMode(): SelectionMode {
            return this.selectionMode;
        }

        public unselectAll(): void {
            for (let id of this.selectedIds) {
                this.unselect(id);
            }
        }
    }

    export interface IDialogSettings {
        selection: SelectionManager;
        dialogWidth?: string;
        onClose?: () => void;
        storageKey?: string;
    }

    export interface IResolveUrlCallback {
        (url: string): string;
    }

    export interface ISelectedRowCallback {
        (record: Grid.IRow): void;
    }

    export interface ISelectedRowsCallback {
        (record: Grid.IRow[]): void;
    }

    function getDefaultDialogSettings(selectionMode: SelectionMode): IDialogSettings {
        return {
            selection: new SelectionManager(selectionMode),
        };
    }

    function setSelectionHint(modal: JQuery, selection: SelectionManager) {
        if (!selection.shouldDisplaySelectionHint) {
            return;
        }

        var header = modal.find('.modal-header');
        if (header.length === 0) {
            return;
        }

        var hint = header.find('.selection-hint');
        if (hint.length === 0) {
            hint = $('<span class="selection-hint"></span>')
                .appendTo(header);
        }

        var count = selection.getSelectedIds().length;
        var format: string;

        switch (count) {
            case 0:
                format = Globals.resources.ValuePickerSelectionHintZeroFormat;
                break;

            case 1:
                format = Globals.resources.ValuePickerSelectionHintOneFormat;
                break;

            default:
                format = Globals.resources.ValuePickerSelectionHintManyFormat;
                break;
        }

        var hintText = Strings.format(format, count);
        hint.text(hintText);
    }

    function renderRow(valuePicker: JQuery, item: Grid.IRow): string {
        const hasHub = valuePicker.data("has-hub");
        const identifier = valuePicker.data('list-id');

        return hasHub
            ? `<span data-hub-identifier="${identifier}" data-hub-recordid="${item.id}">${item.toString}</span>`
            : `<span>${item.toString}</span>`;
    }

    export function setValue(valuePickerId: string, value: Grid.IRow): void {
        const $valuePicker = $(`div.value-picker[data-field-name][data-field-id='${valuePickerId}']`);
        const $displayField = $valuePicker.find(`[id='${$valuePicker.data('field-id')}']`);
        const $valueField = $valuePicker.find(`[name='${$valuePicker.data('field-name')}']`);

        if (!!value) {
            const rowHtml = renderRow($valuePicker, value);

            $displayField.html(rowHtml);
            $valueField.val(value.id);
        } else {
            $displayField.empty();
            $valueField.val("");
        }

        Hub.init($displayField);
    }

    export function getPickerById(id: string): JQuery {
        return $(document.getElementById(id) as HTMLElement);
    }

    export function setMultiValue(valuePickerId: string, values: Grid.IRow[]): void {
        var $valuePickerInput = getPickerById(valuePickerId);
        var $valuePickerContainer = $valuePickerInput.siblings(".item-container");

        $valuePickerContainer.empty();

        if (values.length > 0) {
            values.forEach((item: Grid.IRow) => addValuePickerItem(item, $valuePickerContainer));
        }

        $valuePickerInput.closest(".input-group").siblings(".hidden-but-validate").val((values.map(v => v.id)).join(","));
        $valuePickerInput.val((values.map(v => v.toString)).join(","));

        Hub.init($valuePickerContainer);
    }

    function addValuePickerItem(value: Grid.IRow, container: JQuery) {
        container.append(`<div class="value-picker-item" data-id="${value.id}">${value.toString}
        <i class="glyphicon glyphicon-remove value-picker-remove-item-button"></i></div>`);
    }

    function onSelectionChanged(modal: JQuery): void {
        var dialogOkButton = modal.find('button[data-button="ok"]');

        var clientData = modal.data("client-data");
        var selection = <SelectionManager>clientData.selection;

        if (selection.isOkButtonClickAllowed) {
            dialogOkButton.removeClass('disabled');
            dialogOkButton.removeAttr('disabled');
        } else {
            dialogOkButton.addClass('disabled');
            dialogOkButton.attr('disabled', 'disabled');
        }

        setSelectionHint(modal, selection);
    }

    export function pickItemAndClose(modal: JQuery, item: Grid.IRow): void {
        const dialogOkButton = modal.find("button[data-button=\"ok\"]");

        const clientData = modal.data("client-data");
        const selection = <SelectionManager>clientData.selection;

        selection.unselectAll();

        selection.select(item);

        onSelectionChanged(modal);

        dialogOkButton.click();
    }

    function stashValuePickerParameters(dialog: JQuery, storageKey: string): void {
        var parameters = new ValuePickerParameters();

        parameters.search = dialog.find("input[name='search']").val();
        parameters.filter = dialog.find("input[name='filter']").val();
        parameters.order = dialog.find("input[name='order']").val();
        parameters.page = dialog.find("input[name='page']").val();

        ClientStorage.setObject(storageKey, parameters, ClientStorage.PersistenceLevel.Page);
    }

    function selectValues(listId: string,
        onSelected: ISelectedRowsCallback,
        onResolveUrl: IResolveUrlCallback | null,
        dialogSettings: IDialogSettings) {

        var allowMultiple = dialogSettings.selection.getSelectionMode() === SelectionMode.Multiple;
        var storageKey = dialogSettings.storageKey;

        var url = Globals.resolveUrl('~/ValuePicker/List?listId=' + listId + '&multiselect=' + allowMultiple);

        if (storageKey) {
            var parameters =
                ClientStorage.getObject<ValuePickerParameters>(storageKey, ClientStorage.PersistenceLevel.Page);
            if (parameters) {
                var parameterString = $.param(parameters);
                url = UrlHelper.updateUrlParameters(url, parameterString, true);
            }
        }

        if (onResolveUrl != null) {
            url = onResolveUrl(url);
        }

        var selection = dialogSettings.selection;
        var clientData: any = { selection: selection };

        var onClose = dialogSettings.onClose;

        var onDialogButtonClicked: Dialogs.IButtonClickedEventHandler = (eventArgs: Dialogs.EventArgs) => {
            if (onClose) {
                onClose();
            }

            if (eventArgs.isOk) {
                var records = selection.getSelectedItems();

                if (selection.isSelectionValid) {
                    onSelected(records);
                }
            }
        }

        var onBeforeInitDialog: (dialog: JQuery) => void = (dialog) => {
            if (dialogSettings.dialogWidth) {
                Dialogs.setWidth(dialog, dialogSettings.dialogWidth);
            }

            if (storageKey) {
                stashValuePickerParameters(dialog, storageKey);
            }
        };

        var onBeforeShowDialog: (Dialogs: JQuery) => void = (dialog) => {

            dialog.addClass('common-dialog-value-picker');
        };
        Dialogs.showDialog({
            actionUrl: url,
            eventHandler: onDialogButtonClicked,
            clientData: clientData,
            onBeforeInit: onBeforeInitDialog,
            onBeforeShow: onBeforeShowDialog
        });
    }

    export function selectSingle(listId: string, onSelected: ISelectedRowCallback, onResolveUrl: IResolveUrlCallback | null = null, dialogSettings: IDialogSettings | null = null): void {
        var settings = $.extend({}, getDefaultDialogSettings(SelectionMode.Single), dialogSettings);

        if (settings.selection === null || settings.selection.getSelectionMode() === SelectionMode.Multiple) {
            throw "Invalid selection configuration. Cannot display value picker dialog.";
        }

        var selectedCallback: ISelectedRowsCallback;

        switch (settings.selection.getSelectionMode()) {

            default:
            case SelectionMode.Single:
                selectedCallback = (items: Grid.IRow[]) => onSelected(items[0]);
                break;

            case SelectionMode.SingleOrNone:
                selectedCallback = (items: Grid.IRow[]) => {
                    if (items.length > 0) {
                        onSelected(items[0]);
                    }
                }
                break;
        }

        selectValues(listId, selectedCallback, onResolveUrl, settings);
    }

    export function selectMultiple(listId: string, onSelected: ISelectedRowsCallback, onResolveUrl: IResolveUrlCallback | null = null, dialogSettings: IDialogSettings | null = null): void {
        var settings = $.extend({}, getDefaultDialogSettings(SelectionMode.Multiple), dialogSettings);


        if (settings.selection === null || settings.selection.getSelectionMode() !== SelectionMode.Multiple) {
            throw "Invalid selection configuration. Cannot display value picker dialog.";
        }

        selectValues(listId, onSelected, onResolveUrl, settings);
    }

    export function getContextResolveUrlCallback(context: any): IResolveUrlCallback {
        var contextParams = $.param(context);
        return (url: string) => {
            return UrlHelper.updateUrlParameters(url, contextParams);
        };
    }

    export function onGridSelectionChanged(grid: Grid.GridComponent, row: Element, rowData: Grid.IRow, isSelected: boolean) {
        var modal = grid.container.closest('.modal');

        var clientData = modal.data("client-data");
        var selection = <SelectionManager>clientData.selection;

        if (isSelected) {
            selection.select(rowData);
        } else {
            selection.unselect(rowData.id);
        }

        onSelectionChanged(modal);
    }

    export function onRowDoubleClicked(grid: Grid.GridComponent, row: Element, rowData: Grid.IRow, isSelected: boolean) {
        var modal = grid.container.closest('.modal');
        var dialogOkButton = modal.find('button[data-button="ok"]');

        if (!dialogOkButton.is(".disabled")) {
            dialogOkButton.click();
        }
    }

    export function onGridRendered(component: Grid.GridComponent) {
        var modal = $(component.container).closest(".modal");
        var clientData = modal.data("client-data");
        var selection = <SelectionManager>clientData.selection;

        var dialogOkButton = modal.find('button[data-button="ok"]');
        if (selection.isOkButtonClickAllowed) {
            dialogOkButton.removeClass('disabled');
            dialogOkButton.removeAttr('disabled');
        }

        component.highlightSelectedItems(selection.getSelectedIds());
        setSelectionHint(modal, selection);
    }

    function getDisplayText(itemContainer: JQuery) {
        return itemContainer.find("div.value-picker-item").map((i, e) => e.textContent).get().join(", ");
    }

    function initValuePickerControl(valuePicker: Element): void {
        var $valuePicker = $(valuePicker);
        var name = $valuePicker.data('field-name');
        var fieldId = $valuePicker.data('field-id');
        var dialogWidth = $valuePicker.data('dialog-width') || null;
        var selectButton = $valuePicker.find('.value-picker-add-button');
        var isMultiValue = $valuePicker.data('is-multivalue');
        var isNoValueAllowed = $valuePicker.data('is-no-value-allowed');
        var listId = $valuePicker.data('list-id');
        var storageKey = $valuePicker.data('storage-key');
        var displayNameInput = $valuePicker.find(`[id="${fieldId}"]`);
        var idInput = $valuePicker.find(`input[name="${name}"]`);
        var itemContainer = $valuePicker.find(".item-container");
        var keyDownRoot = isMultiValue ? itemContainer : displayNameInput;
        const hasHub = $valuePicker.data("has-hub");

        var selectionMode = isMultiValue
            ? SelectionMode.Multiple
            : (isNoValueAllowed ? SelectionMode.SingleOrNone : SelectionMode.Single);

        var clearButton = $valuePicker.find('.value-picker-remove-button');

        var onResolveUrlFuncOrBody = $valuePicker.data('on-resolve-url');
        var onSelectionChangedFuncOrBody = $valuePicker.data('on-selection-changed');

        var onSelectionChanged = () => {
            if (onSelectionChangedFuncOrBody !== null && onSelectionChangedFuncOrBody !== "") {
                var params = { control: valuePicker };
                Utils.executeHandler(onSelectionChangedFuncOrBody, params);
            }

            idInput.trigger("change");
            idInput.valid();
        };

        var onResolveUrl = (url: string) => {
            if (onResolveUrlFuncOrBody === null || onResolveUrlFuncOrBody === "") {
                return url;
            }
            var params = { control: valuePicker, url: url };
            return Utils.executeHandler(onResolveUrlFuncOrBody, params);
        };

        var triggerSelectButton = () => {
            if (selectButton[0] !== undefined) {
                selectButton[0].click();
            }
        };

        clearButton.click(() => {
            var previousValue = idInput.val();
            idInput.val("");
            displayNameInput.empty();
            itemContainer.empty();

            if (previousValue !== "") {
                onSelectionChanged();
            }
        });

        itemContainer.click((event) => {
            var $target = $(event.target);
            var $parent = $target.parent();
            var id = $parent.data("id");

            if (!Strings.isNullOrEmpty(id)) {
                $parent.remove();
                Forms.removeFromInput(idInput, ",", id);
                var displayText = getDisplayText(itemContainer);
                displayNameInput.val(displayText);
                onSelectionChanged();
            } else {
                triggerSelectButton();
            }
        });

        displayNameInput.click(triggerSelectButton);

        selectButton.click(() => {
            var idValue = idInput.val() as string;
            var currentlySelected = idValue === "" ? new Array<any>() : idValue.split(',');
            var selection = new SelectionManager(selectionMode, currentlySelected);

            var dialogSettings: IDialogSettings = {
                selection: selection,
                dialogWidth: dialogWidth,
                storageKey: storageKey,
                onClose: () => {
                    keyDownRoot.blur();
                }
            };

            if (isMultiValue) {
                ValuePicker.selectMultiple(listId, (records) => {
                    var previousValue = idInput.val();

                    var recordIds = records.map((r) => r.id.toString());
                    var newlyAdded = records.filter((v) => currentlySelected.indexOf(v.id.toString()) === -1);
                    var toBeRemovedIds = currentlySelected.filter((id) => recordIds.indexOf(id) === -1);

                    newlyAdded.forEach((rec) => {
                        const recordHtml = renderRow($valuePicker, rec);

                        $(itemContainer).append('<div data-id="' + rec.id + '" class="value-picker-item">' + recordHtml + '<i class="' + Utils.getIconClass('glyphicon-remove') + ' value-picker-remove-button"></i></div>');
                    });

                    toBeRemovedIds.forEach((id) => {
                        $(itemContainer).find("div[data-id='" + id + "']").remove();
                    });

                    idInput.val(recordIds.join(','));
                    displayNameInput.val(getDisplayText(itemContainer));

                    if (idInput.val() !== previousValue) {
                        onSelectionChanged();
                    }

                    Hub.init(itemContainer);
                    Hub.init(displayNameInput);

                }, onResolveUrl, dialogSettings);
            } else {
                ValuePicker.selectSingle(listId, (record) => {
                    var previousValue = idInput.val();

                    if (record == null) {
                        idInput.val('');
                        displayNameInput.empty();
                    } else if (record.id !== previousValue) {
                        const recordHtml = renderRow($valuePicker, record);

                        displayNameInput.html(recordHtml);
                        idInput.val(record.id);
                        onSelectionChanged();
                    }

                    Hub.init(displayNameInput);

                }, onResolveUrl, dialogSettings);
            }
        });

        keyDownRoot.on("keydown", "", "Alt+down", () => {
            triggerSelectButton();
        });

        keyDownRoot.on("keydown", "", "Ctrl+del", () => {
            clearButton[0].click();
        });
    }

    export function initControls(context: JQuery): void {
        context.find('.value-picker').each((index: number, elem: Element) => {
            initValuePickerControl(elem);
        });
    }

    class ValuePickerParameters {
        public filter: string;
        public search: string;
        public order: string;
        public page: number;
    }
} 