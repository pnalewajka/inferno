﻿module Validation {

    function toggleTabButtonErrors(hasErrors: boolean, tabButton: JQuery): void {
        if (hasErrors && !tabButton.is(".has-error")) {
            tabButton.addClass("has-error bg-danger text-danger");
        } else if (!hasErrors && tabButton.is(".has-error")) {
            tabButton.removeClass("has-error bg-danger text-danger");
        }
    }

    function refreshTabValidation(tabContainer: Element) {
        const tabContainerElements = $(".has-error, .field-validation-error", tabContainer);
        const tabButton = $(`a[href="#${tabContainer.id}"][role="tab"]`);

        toggleTabButtonErrors(tabContainerElements.length > 0, tabButton);
    }

    function refreshFieldTabValidation(element: JQuery): void {
        const tabContainer = $(element).parents("[id][role='tabpanel']")[0];
        if (!!tabContainer) {
            refreshTabValidation(tabContainer);
        }
    }

    function refreshTabsValidation(): void {
        $("[id][role='tabpanel']").each((index: number, element: Element) => {
            refreshTabValidation(element);
        });

        $("li a[href][role=\"tab\"].has-error:first").click();
    }

    function addErrorClass(element: JQuery) {
        var group = element.closest('.form-group');
        if (group && group.length > 0) {
            group.addClass('has-error');
        }
        refreshFieldTabValidation(element);
    }

    function removeErrorClass(element: JQuery) {
        var group = element.closest('.form-group');
        if (group && group.length > 0) {
            group.removeClass('has-error');
        }
        refreshFieldTabValidation(element);
    }

    function onError(
        formElement: JQuery,
        errorPlacementBase: (error: JQuery, element: JQuery) => void,
        error: JQuery,
        inputElement: JQuery) {
        errorPlacementBase(error, inputElement);

        if ($(inputElement).hasClass('input-validation-error')) {
            addErrorClass(inputElement);
        }
    }

    function onSuccess(successBase: (error: JQuery) => void, error: JQuery) {
        var container = error.data("unobtrusiveContainer");

        successBase(error);

        if (container) {
            removeErrorClass(container);
        }
    }

    $.fn.validateBootstrap = function (refresh: boolean) {
        const dateFormat = Globals.getDateFormat();
        $.validator.addMethod("date", function (this: JQuery, value: string, element: Element) {
            return this.optional(element) || !/Invalid|NaN/.test($.fn.datepicker.DPGlobal.parseDate(value, dateFormat).toString());
        });

        return this.each(function (this: JQuery) {

            var $this = $(this);

            if (refresh) {
                $this.removeData('validator');
                $this.removeData('unobtrusiveValidation');
                $.validator.unobtrusive.parse($this);
                refreshTabsValidation();
            }

            var validator = $this.data('validator');

            if (validator) {
                validator.settings.errorClass += ' text-danger';
                var errorPlacementBase = validator.settings.errorPlacement;
                var successBase = validator.settings.success;

                validator.settings.errorPlacement = (error: JQuery, inputElement: JQuery) => {
                    onError($this, errorPlacementBase, error, inputElement);
                };

                validator.settings.success = (error: JQuery) => {
                    onSuccess(successBase, error);
                }

                $this.find('.input-validation-error').each(function (this: JQuery) {
                    var currentElement = $(this)[0];
                    var currentElementName = $(currentElement).attr('name');
                    var errorElement = $this.find("[data-valmsg-for='" + Strings.escapeAttributeValue(currentElementName) + "']");
                    var newElement = $(document.createElement(validator.settings.errorElement))
                        .addClass('text-danger')
                        .attr('for', Strings.escapeAttributeValue(currentElementName))
                        .text(errorElement.text());
                    onError($this, errorPlacementBase, newElement, $(this));
                });
            }

            // if validation isn't enabled, but the form has the validation error message element, add error class to container
            else {
                $this.find('.input-validation-error').each(function (this: JQuery) {
                    addErrorClass($(this));
                });
            }
        });
    };
};
