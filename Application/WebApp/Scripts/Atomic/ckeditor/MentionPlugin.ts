﻿namespace Atomic.Ckeditor.MentionPlugin {
    declare var CKEDITOR: any;

    export interface IMentionItem {
        Id: number;
        Label: string;
        SourceName: string;
    }

    export interface IMentionPluginParameters {
        symbol: string;
        name: string;
        label: string;
        getElement(selectedId?: number): Promise<IMentionItem>;
        searchElements(searchKey: string): Promise<IMentionItem[]>;
        iconClass: string;
        resolveListTemplate?: (item: IMentionItem) => string;
    }

    function escapeRegExp(value: string) {
        return value.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }

    function atwhoJqueryItem(editor: any): JQuery {
        if (editor.mode != 'source') {

            editor.document.getBody().$.contentEditable = true;

            return $(editor.document.getBody().$);
        } else {
            return $(editor.container.$).find(".cke_source");
        }
    }

    function load_atwho(editor: any, at_config: any) {
        atwhoJqueryItem(editor)
            .atwho(at_config)
            .on("inserted.atwho", ($event: any, $li: JQuery) => {
                const rowItem: IMentionItem = {
                    Id: +$li.attr("data-id"),
                    Label: $li.attr("data-label"),
                    SourceName: $li.attr("data-source")
                };

                editor.insertHtml(`${at_config.at}${JSON.stringify(rowItem)}`);
                editor.editable().findOne(".atwho-inserted").remove();
            });
    }

    function getTokenName(params: IMentionPluginParameters): string {
        return `token-${params.name}`;
    }

    function registerAutocomplete(params: IMentionPluginParameters): void {
        CKEDITOR.on('instanceReady', function (event: any) {

            var at_config = {
                at: params.symbol,
                show_the_at: true,
                spaceSelectsMatch: false,
                acceptSpaceBar: true,
                searchKey: 'Label',
                limit: 8,
                callbacks: {
                    matcher: (flag: string, subtext: string) => {
                        const pattern = `${flag}([A-Za-z${decodeURI("%C3%80")}-${decodeURI("%C3%BF")}0-9_\u0104\u0106\u0118\u0141\u0143\u00D3\u015A\u0179\u017B\u0105\u0107\u0119\u0142\u0144\u00F3\u015B\u017A\u017C\ \'\.\+\-]*)$|${flag}([^\\x00-\\xff]*)$`;

                        const regexp = new RegExp(pattern, 'gi');
                        const match = regexp.exec(subtext);

                        if (match) {
                            return match[2] || match[1];
                        } else {
                            return null;
                        }
                    },
                    sorter: (query: string, items: IMentionItem[], searchKey: string): IMentionItem[] => {
                        /* take as it is, server handle sorting */
                        return items;
                    },
                    remoteFilter: (search: string, callback: (rows: IMentionItem[]) => void): void => {
                        params.searchElements(search).then(data => {
                            callback(data);
                        });
                    }
                },
                displayTpl: (item: IMentionItem) => `<li data-id='${item.Id}' data-label='${item.Label}' data-source='${item.SourceName}'>${params.resolveListTemplate === undefined ? item.Label : params.resolveListTemplate(item)}</li>`
            }

            var editor = event.editor;
            editor.on('mode', function () {
                load_atwho(editor, at_config);
            });

            // First load
            load_atwho(editor, at_config);
        });
    }

    function registerWidget(editor: any, params: IMentionPluginParameters): void {
        const tokenName = getTokenName(params);

        editor.widgets.add(tokenName, {
            template: `<span class="cke_token ${params.name}"></span>`,
            draggable: false,
            getLabel: function (): string { return params.name; },
            downcast: function () {
                var item = this.data.row as IMentionItem;

                return new CKEDITOR.htmlParser.text(`${params.symbol}${JSON.stringify(item)}`);
            },
            init: function () {
                const $item = $(this.element.$);
                const rowItem: IMentionItem = {
                    Id: +$item.attr("data-id"),
                    Label: $item.attr("data-label"),
                    SourceName: $item.attr("data-source")
                };

                this.setData('row', rowItem);
            },
            data: function () {
                this.element.setText(this.data.row.Label);
            }
        });
    }

    function registerIconButton(params: IMentionPluginParameters): string {
        const pluginName = `mention-${params.name}`;
        const tokenName = getTokenName(params);
        const commandName = `command-${params.name}`;

        if (CKEDITOR.plugins.registered[pluginName] !== undefined) {
            return pluginName;
        }

        CKEDITOR.plugins.add(pluginName, {
            icons: pluginName,
            lang: 'en,pl',
            hidpi: true,
            onLoad: function () {
                CKEDITOR.addCss(`
                    .cke_token.${params.name}::before {
                        content: '${params.symbol}';
                        color: #333;
                        font-weight: bold;
                    }
                `);
            },
            init: function (editor: any) {
                editor.on('doubleclick', function ($event: any) {
                    var focusedWidget = $event.editor.widgets.focused;

                    if (focusedWidget != null && focusedWidget.name === tokenName) {
                        const data = focusedWidget.data.row as IMentionItem;
                        params.getElement(data.Id).then(selectedItem => {
                            editor.insertHtml(`${params.symbol}${JSON.stringify(selectedItem)}`);
                        });
                    }
                });

                editor.addCommand(commandName, {
                    exec: (editor: any) => {
                        params.getElement().then(selectedItem => {
                            editor.insertHtml(`${params.symbol}${JSON.stringify(selectedItem)}`);
                        });
                    }
                });

                editor.ui.addButton(params.label, {
                    label: params.label,
                    command: commandName,
                    toolbar: 'insert',
                    icon: commandName,
                });

                registerWidget(editor, params);
            },
            afterInit: function (editor: any) {
                editor.dataProcessor.dataFilter.addRules({
                    text: function (text: string, node: any) {
                        var dtd = node.parent && CKEDITOR.dtd[node.parent.name];
                        if (dtd && !dtd.span) {
                            return;
                        }

                        return replaceTokens(text, (item) => {
                            var widgetWrapper = null,
                                innerElement = new CKEDITOR.htmlParser.element('span', {
                                    'class': `cke_token ${params.name}`,
                                    'data-id': item.Id,
                                    'data-label': item.Label,
                                    'data-source': item.SourceName,
                                });

                            innerElement.add(new CKEDITOR.htmlParser.text(item.toString));
                            widgetWrapper = editor.widgets.wrapElement(innerElement, tokenName);

                            return widgetWrapper.getOuterHtml();
                        }, params.symbol);
                    }
                });
            }
        });

        return pluginName;
    }

    export function registerPlugin(params: IMentionPluginParameters): string {
        registerAutocomplete(params);
        const pluginName = registerIconButton(params);

        return pluginName;
    }

    function parseToken(match: string, symbol: string): IMentionItem {
        return JSON.parse(match.substr(symbol.length)) as IMentionItem;
    }

    function tokenRegex(symbol: string = "@"): RegExp {
        var tokenStart = `${symbol}{`;
        var tokenEnd = '}';
        var tokenStartRegex = escapeRegExp(tokenStart);
        var tokenEndRegex = escapeRegExp(tokenEnd);

        return new RegExp(tokenStartRegex + '([^' + tokenStartRegex + tokenEndRegex + '])+' + tokenEndRegex, 'g');
    }

    export function replaceTokens(input: string, expression: (item: IMentionItem) => string, symbol: string = "@"): string {
        return input.replace(tokenRegex(symbol), (match) => {
            var item = parseToken(match, symbol);

            return expression(item);
        });
    }

    export function tokenMentions(input: string, symbol: string = "@"): IMentionItem[] {
        const matches = input.match(tokenRegex(symbol));

        if (matches == null) {
            return [];
        }

        return (matches as string[]).map(match => parseToken(match, symbol) as IMentionItem);
    }

    export function tokensIds(input: string, symbol: string = "@"): number[] {
        return tokenMentions(input, symbol).map(mention => mention.Id);
    }
}
