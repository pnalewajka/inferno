﻿module RestorePoints {
    var currentControllerName: string;
    var restorePointsStorageKey = "restorePoints";

    export function saveControllerPoint(name: string) {
        currentControllerName = name;

        savePoint(name);
    }

    export function getCurrentControllerName() {
        return currentControllerName;
    }

    function loadRestorePoints(): RestorePointsModel {
        return ClientStorage.getObject<RestorePointsModel>(restorePointsStorageKey) || new RestorePointsModel();
    }

    function saveRestorePoints(points: RestorePointsModel) {
        ClientStorage.setObject(restorePointsStorageKey, points);
    }

    export function savePoint(name: string, url: string = Forms.getCurrentUrl()) {
        var points = loadRestorePoints();
        points.urls[name] = url;
        saveRestorePoints(points);
    }

    export function returnToPoint(name: string): boolean {
        const url = getReturnPointUrl(name);

        if (url) {
            Forms.redirect(url);
            return true;
        }

        return false;
    }

    export function getReturnPointUrl(name: string): string | null {
        const points = loadRestorePoints();

        for (let url in points.urls) {
            if (points.urls.hasOwnProperty(url)) {
                if (url.toLowerCase() === name.toLowerCase()) {
                    return points.urls[url];
                }
            }
        }

        return "";
    }

    class RestorePointsModel {
        public urls: { [name: string]: string; };

        constructor() {
            this.urls = {};
        }
    }
} 