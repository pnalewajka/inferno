﻿declare const PDFJS: PDF.PDFJSStatic;

namespace PdfHelper {
    export function getBase64(file: File): Promise<Uint8Array> {
        return new Promise<Uint8Array>((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                resolve(reader.result);
            };
            reader.onerror = (error) => {
                reject();
            };
        });
    }

    export function getPageText(pageNum: number, pdf: PDF.PDFDocumentProxy): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            pdf.getPage(pageNum).then((pdfPage) => {
                pdfPage.getTextContent().then(((textContent) => {
                    const finalString = textContent
                        .items
                        .map((item) => item.str)
                        .join(" ");
                    resolve(finalString);
                }));
            });
        });
    }

    export function readPdf(file: File): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            getBase64(file)
                .then((data) => {
                    PDFJS.getDocument(data)
                        .then((pdf: PDF.PDFDocumentProxy) => {
                            const pdfDocument = pdf;
                            const pagesPromises: Array<Promise<string>> = [];

                            for (let i = 0; i < pdf.numPages; i++) {
                                ((pageNumber) => {
                                    pagesPromises.push(getPageText(pageNumber, pdfDocument));
                                })(i + 1);
                            }

                            return Promise.all(pagesPromises).then((pageText: string[]) => {
                                resolve(pageText);
                            });
                        }, reject);
                }).catch(reject);
        });
    }
}
