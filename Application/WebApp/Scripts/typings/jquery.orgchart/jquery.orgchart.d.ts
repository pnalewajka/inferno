﻿interface JQuery {
    orgchart(data?: any, ...options: any[]): JQuery[];
}

interface JQueryStatic {
    orgchart(data?: any, ...options: any[]): JQuery[];
}