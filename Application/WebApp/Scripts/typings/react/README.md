# Installation
> `npm install --save @types/react`

# Summary
This package contains type definitions for React (http://facebook.github.io/react/).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react

Additional Details
 * Last updated: Fri, 31 Mar 2017 16:47:32 GMT
 * Dependencies: none
 * Global values: React

# Credits
These definitions were written by Asana <https://asana.com>, AssureSign <http://www.assuresign.com>, Microsoft <https://microsoft.com>, John Reilly <https://github.com/johnnyreilly/>, Benoit Benezech <https://github.com/bbenezech>, Patricio Zavolinsky <https://github.com/pzavolinsky>, Digiguru <https://github.com/digiguru>.
