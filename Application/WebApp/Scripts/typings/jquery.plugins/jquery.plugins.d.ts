﻿interface JQuery {
    datepicker(config: any): any;
    datepicker(method: string, config: any): any;
    colorpicker(config: any): any;
    validate(): void;
    tab(show: string): void;
    validateBootstrap(refresh: boolean): any;
    stickyTableHeaders(options: any): any;
    fixedTableColumns(options: { fixedCols? : number, width : string, height? : number, colModal : any }): any;
    selectpicker(options?: any): any;
    contextmenu(options?: any): any;
    replaceWith(func: () => string): JQuery;
    setInputValue(inputName: string, inputValue: any): any;
}

declare module JQueryValidation {
    interface ValidatorStatic {
        /* temporary workaround to limit odd-looking casts */
        unobtrusive: any;

        setDefaults(options: any): any;
        addMethod(methodName: string, func: (value: any, element: any, params?: any) => void, message?: string): void;
    }
}