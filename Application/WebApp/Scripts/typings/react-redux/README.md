# Installation
> `npm install --save @types/react-redux`

# Summary
This package contains type definitions for react-redux (https://github.com/rackt/react-redux).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/react-redux

Additional Details
 * Last updated: Wed, 22 Mar 2017 00:26:29 GMT
 * Dependencies: react, redux
 * Global values: none

# Credits
These definitions were written by Qubo <https://github.com/tkqubo>, Sean Kelley <https://github.com/seansfkelley>.
