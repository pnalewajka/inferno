﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Areas.WebSite.Controllers;
using Smt.Atomic.WebApp.Resources;

namespace Smt.Atomic.WebApp.Models
{
    public class BulkEditPropertyLookupViewModel : IBulkEditPropertyLookupViewModel, ICardIndexRecordWithButtonsViewModel
    {
        public BulkEditPropertyLookupViewModel()
        {
        }

        public BulkEditPropertyLookupViewModel(long[] recordIds)
        {
            RecordIds = recordIds;
        }

        [Required]
        [Visibility(VisibilityScope.Form)]
        [DisplayNameLocalized(nameof(AtomicResources.BulkEditPropertyLookupViewModelDtoPropertyId), typeof(AtomicResources))]
        [ValuePicker(Type = typeof(DtoPropertyPickerController), OnResolveUrlJavaScript = "url = UrlHelper.updateUrlParameters(url, 'view-model-identifier='+$('#view-model-identifier').val())", OnSelectionChangedJavaScript = "$('input[type=\"submit\"]').click()")]
        public long? DtoPropertyId { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public string ViewModelIdentifier { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public long[] RecordIds { get; set; }

        [Visibility(VisibilityScope.None)]
        public string CancelButtonUrl { get; set; }

        [Visibility(VisibilityScope.None)]
        public IList<IFooterButton> Buttons
        {
            get
            {
                return new List<IFooterButton> {
                    FooterButtonBuilder.EditButton(),
                    FooterButtonBuilder.CancelButton(CancelButtonUrl)
                };
            }
        }
    }
}