﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.WebApp.Models
{
    public class ModelFieldNameViewModelToModelFieldNameDtoMapping : ClassMapping<ModelFieldNameViewModel, PropertyNameDto>
    {
        public ModelFieldNameViewModelToModelFieldNameDtoMapping()
        {
            Mapping = v => new PropertyNameDto
            {
                Id = v.Id,
                PropertyName = v.FieldName,
                ViewModelType = v.ViewModelType
            };
        }
    }
}
