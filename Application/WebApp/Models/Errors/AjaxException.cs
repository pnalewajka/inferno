﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.WebApp.Models.Errors
{
    public class AjaxException
    {
        [JsonProperty("message")]
        public string Message { get; private set; }

        [JsonProperty("exception")]
        public string Exception { get; private set; }

        [JsonProperty("innerExceptions")]
        public List<string> InnerExceptions { get; private set; }

        public AjaxException([NotNull] Exception exception)
        {
            if (exception == null)
            {
                throw new ArgumentNullException(nameof(exception));
            }

            Exception = exception.ToString();
            Message = exception.Message;
            InnerExceptions = new List<string>();

            var innerException = exception.InnerException;

            while (innerException != null)
            {
                InnerExceptions.Add(innerException.ToString());

                innerException = innerException.InnerException;
            }
        }
    }
}