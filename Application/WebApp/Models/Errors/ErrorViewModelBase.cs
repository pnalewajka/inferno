using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.WebApp.Models.Errors
{
    public abstract class ErrorViewModelBase
    {
        [JsonProperty("actionName")]
        public string ActionName { get; protected set; }

        [JsonProperty("controllerName")]
        public string ControllerName { get; protected set; }

        [JsonProperty("form")]
        public List<KeyValuePair<string, string>> Form { get; set; }

        [JsonProperty("formMethod")]
        public FormMethod FormMethod { get; protected set; }

        [JsonProperty("systemVersion")]
        public string SystemVersion { get; protected set; }

        [JsonProperty("reportNewIssueUrl")]
        public string ReportNewIssueUrl { get; protected set; }

        [JsonProperty("isRoutingDataAvailable")]
        public bool IsRoutingDataAvailable { get; protected set; }

        [JsonProperty("areDetailsAvailable")]
        public bool AreDetailsAvailable { get; set; }

        [JsonProperty("areFormDetailsAvailable")]
        public bool AreFormDetailsAvailable { get; set; }

        protected ErrorViewModelBase(ExceptionContext filterContext, ISettingsProvider settingsProvider)
        {
            ControllerName = (string)filterContext.RouteData.Values["controller"];
            ActionName = (string)filterContext.RouteData.Values["action"];
            IsRoutingDataAvailable = true;

            SystemVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ReportNewIssueUrl = settingsProvider.ReportNewIssueUrl;
            AreDetailsAvailable = settingsProvider.AreExceptionDetailsAvailable;
            AreFormDetailsAvailable = settingsProvider.AreExceptionFormDetailsAvailable;

            var httpRequest = filterContext.HttpContext.Request;
            FormMethod = httpRequest.HttpMethod == "GET" ? FormMethod.Get : FormMethod.Post;

            Form = new List<KeyValuePair<string, string>>();

            if (AreFormDetailsAvailable)
            {
                foreach (var key in httpRequest.Form.AllKeys)
                {
                    var value = httpRequest.Unvalidated().Form[key];
                    Form.Add(new KeyValuePair<string, string>(key, HttpUtility.HtmlDecode(value)));
                }
            }
        }

        protected ErrorViewModelBase(ISettingsProvider settingsProvider)
        {
            SystemVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ReportNewIssueUrl = settingsProvider.ReportNewIssueUrl;
            AreDetailsAvailable = settingsProvider.AreExceptionDetailsAvailable;
        }
    }
}