﻿using System;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.WebApp.Models.Errors
{
    public class ErrorViewModel : ErrorViewModelBase
    {
        public bool IsBusinessException { get; private set; }
        public Exception Exception { get; private set; }

        public ErrorViewModel(ExceptionContext filterContext, ISettingsProvider settingsProvider) : base(filterContext, settingsProvider)
        {
            Exception = filterContext.Exception;
            IsBusinessException = Exception as BusinessException != null;
        }

        public ErrorViewModel(Exception exception, ISettingsProvider settingsProvider) : base(settingsProvider)
        {
            Exception = exception;
            IsBusinessException = Exception as BusinessException != null;
        }
    }
}