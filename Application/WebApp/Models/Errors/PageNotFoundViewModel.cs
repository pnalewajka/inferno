﻿using System.Reflection;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.WebApp.Models.Errors
{
    public class PageNotFoundViewModel
    {
        public string SystemVersion { get; private set; }
        public string ReportNewIssueUrl { get; private set; }

        public PageNotFoundViewModel(ISettingsProvider settingsProvider)
        {
            SystemVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ReportNewIssueUrl = settingsProvider.ReportNewIssueUrl;
        }
    }
}