using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.WebApp.Models.Errors
{
    public class AjaxErrorViewModel : ErrorViewModelBase
    {
        [JsonProperty("isBusinessException")]
        public bool IsBusinessException { get; private set; }

        [JsonProperty("exception")]
        public AjaxException Exception { get; private set; }

        public AjaxErrorViewModel(ExceptionContext filterContext, ISettingsProvider settingsProvider) : base(filterContext, settingsProvider)
        {
            Exception = new AjaxException(filterContext.Exception);
            IsBusinessException = filterContext.Exception as BusinessException != null;
        }

        public AjaxErrorViewModel(Exception exception, ISettingsProvider settingsProvider)
            : base(settingsProvider)
        {
            Exception = new AjaxException(exception);
            IsBusinessException = exception as BusinessException != null;
        }
    }
}