﻿using System;
using System.Threading;
using System.Web.Mvc;

namespace Smt.Atomic.WebApp.Models
{
    public class DateTimeModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            return DateTime.Parse(value.AttemptedValue, Thread.CurrentThread.CurrentCulture);
        }
    }
}