﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Resources;
using Smt.Atomic.Presentation.Common.Converters;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.WebApp.Models
{
    [Identifier("Filters.DateRange")]
    [DescriptionLocalized("DateRange", typeof (GeneralResources))]
    public class DateRangeFilterViewModel
    {
        [UrlFieldName("from")]
        [DisplayNameLocalized(nameof(GeneralResources.From), typeof(GeneralResources))]
        [Required]
        [JsonConverter(typeof (DateOnlyDateTimeConverter))]
        public DateTime? From { get; set; }

        [UrlFieldName("to")]
        [DisplayNameLocalized(nameof(GeneralResources.To), typeof(GeneralResources))]
        [Required]
        [JsonConverter(typeof (DateOnlyDateTimeConverter))]
        public DateTime? To { get; set; }

        public override string ToString()
        {
            return string.Format(GeneralResources.DateRangeFilterDescription, From?.ToShortDateString(), To?.ToShortDateString());
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (From.HasValue && To.HasValue && To.Value <= From.Value)
            {
                yield return new ValidationResult(GeneralResources.ToDateShouldBeGreaterThanFromDate);
            }
        }
    }
}