﻿using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApp.Models.DocumentUpload
{
    public class CropImageSettings
    {
        public long MinWidth { get; set; }
        public long MaxWidth { get; set; }
        public long MinHeight { get; set; }
        public long MaxHeight { get; set; }
        public bool ShouldPreserveAspectRatio { get; set; }

        public CropImageSettings()
        {
        }

        public CropImageSettings(CropImageUploadAttribute cropImageUploadAttribute)
        {
            MinHeight = cropImageUploadAttribute.MinHeight;
            MaxHeight = cropImageUploadAttribute.MaxHeight;
            MinWidth = cropImageUploadAttribute.MinWidth;
            MaxWidth = cropImageUploadAttribute.MaxWidth;
            ShouldPreserveAspectRatio = cropImageUploadAttribute.ShouldPreserveAspectRatio;
        }
    }
}