﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.WebApp.Models.DocumentUpload
{
    [Identifier("Models.CropImageModel")]
    public class CropImageModel : IValidatableObject
    {
        [Visibility(VisibilityScope.None)]
        public CropImageSettings Settings { get; set; }

        public string TemporaryDocumentId { get; set; }

        public int CropPositionLeft { get; set; }
        public int CropPositionTop { get; set; }
        public int CropSizeWidth { get; set; }
        public int CropSizeHeight { get; set; }

        public string PropertyName { get; set; }
        public string ViewModelIdentifier { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Enumerable.Empty<ValidationResult>();
        }
    }
}