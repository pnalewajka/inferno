﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Allocation.Models;

namespace Smt.Atomic.WebApp.Models
{
    [Identifier("Models.UserEmployeeHub")]
    public class UserEmployeeHubViewModel
    {
        public UserViewModel User { get; set; }

        public EmployeeViewModel Employee { get; set; }

        public bool CanEditEmployeesResume { get; protected set; }

        public bool CanEditUsers { get; protected set; }

        public bool CanViewEmploymentPeriods { get; protected set; }

        public UserEmployeeHubViewModel(
            IAtomicPrincipal principal)
        {
            CanEditEmployeesResume = principal.IsInRole(SecurityRoleType.CanEditEmployeesResume);
            CanEditUsers = principal.IsInRole(SecurityRoleType.CanEditUsers);
            CanViewEmploymentPeriods = principal.IsInRole(SecurityRoleType.CanViewEmploymentPeriods);
        }
    }
}