﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers;

namespace Smt.Atomic.WebApp.Models
{
    public class ModelFieldNameDtoToModelFieldNameViewModelMapping : ClassMapping<PropertyNameDto, ModelFieldNameViewModel>
    {
        public ModelFieldNameDtoToModelFieldNameViewModelMapping()
        {
            Mapping = d => new ModelFieldNameViewModel
            {
                Id = d.Id,
                DisplayFieldName = NameLocalizationHelper.GetPropertyLabel(d.ViewModelType, d.PropertyName),
                FieldName = d.PropertyName,
                ViewModelType = d.ViewModelType
            };
        }
    }
}
