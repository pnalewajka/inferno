﻿using System.ComponentModel;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.WebApp.Models.GridViewConfiguration
{
    public class JavaScriptRenderViewConfiguration<TViewModel> : GridViewConfiguration<TViewModel>, IInitializableViewConfiguration, ICustomViewConfiguration
    {
        public JavaScriptRenderViewConfiguration([Localizable(true)] string name, string identifier, string initializeJavaScriptAction) : base(name, identifier)
        {
            InitializeJavaScriptAction = initializeJavaScriptAction;
        }

        public string InitializeJavaScriptAction { get; set; }

        public string CustomViewName => "~/Views/Shared/_JavaScriptRenderView.cshtml";

        public GridDisplayMode DisplayMode => GridDisplayMode.Custom;
    }
}