﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.WebApp.Areas.Allocation.Models;

namespace Smt.Atomic.WebApp.Models
{
    [Identifier("Models.OrgUnitHub")]
    public class OrgUnitHubViewModel
    {
        public string Name { get; set; }

        public string FlatName { get; set; }

        public long? EmployeeId { get; set; }

        public EmployeeViewModel Employee { get; set; }
    }
}
