﻿using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.WebApp.Resources;
using System;

namespace Smt.Atomic.WebApp.Models
{
    public class ModelFieldNameViewModel
    {
        public int Id { get; set; }

        [DisplayNameLocalized(nameof(AtomicResources.ModelFieldNameViewModelFieldName), typeof(AtomicResources))]
        public string DisplayFieldName { get; set; }

        [Visibility(VisibilityScope.None)]
        public string FieldName { get; set; }

        [Visibility(VisibilityScope.None)]
        public Type ViewModelType { get; set; }

        public override string ToString()
        {
            return DisplayFieldName;
        }
    }
}
