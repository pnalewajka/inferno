using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IBreadcrumbService
    {
        BreadcrumbContext Context { get; set; }

        /// <summary>
        /// Builds breadcrumb bar based on the path
        /// </summary>
        IList<BreadcrumbItemViewModel> BuildBreadcrumbBar(string breadcrumbPath);

        /// <summary>
        /// Builds breadcrumb bar based on the path
        /// </summary>
        IList<BreadcrumbItemViewModel> BuildBreadcrumbBar(params Type[] breadcrumbPath);

        /// <summary>
        /// Builds default breadcrumb bar based on the current controller, action and sitemap
        /// </summary>
        IEnumerable<BreadcrumbItemViewModel> BuildDefaultBreadcrumbBar();
    }
}