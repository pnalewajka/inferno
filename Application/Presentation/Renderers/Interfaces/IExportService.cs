﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IExportService
    {
        FilePathResult ExportToExcel<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, UrlHelper urlHelper)
            where TViewModel : class, new() where TDto : class;

        FilePathResult ExportToCsv<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, UrlHelper urlHelper)
            where TViewModel : class, new()
            where TDto : class;

        string ExportToClipboardFormat<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, UrlHelper urlHelper)
            where TViewModel : class, new()
            where TDto : class;
    }
}