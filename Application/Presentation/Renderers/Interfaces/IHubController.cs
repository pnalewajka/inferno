﻿using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IHubController
    {
        PartialViewResult Hub(long id);
    }
}
