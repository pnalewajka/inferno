﻿namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IInitializableViewConfiguration
    {
        string InitializeJavaScriptAction { get; }
    }
}
