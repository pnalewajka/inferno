﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IImportService
    {
        FilePathResult GetCsvImportTemplate(CardIndexSettings cardIndexSettings, int importIndex, ImportTemplateType templateType);
        FilePathResult GetExcelImportTemplate(CardIndexSettings cardIndexSettings, int importIndex, ImportTemplateType templateType);
        ImportRecordResult ImportFromExcel(Guid fileId, DataImportSettings importSettings);
        ImportRecordResult ImportFromExcel(string fileName, DataImportSettings importSettings);
        ImportRecordResult ImportFromCsv(Guid fileId, DataImportSettings importSettings);
        ImportRecordResult ImportFromCsv(string fileName, DataImportSettings importSettings);
    }
}