﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IBaseControllerDependencies
    {
        ISiteMapService SiteMapService { get; }
        IPrincipalProvider PrincipalProvider { get; }
        IAlertService AlertService { get; }
        ISystemParameterService SystemParameterService { get; set; }
        IExportService ExportService { get; }
        ISettingsProvider SettingsProvider { get; }
        IBreadcrumbService BreadcrumbService { get; }
        IClassMappingFactory ClassMappingFactory { get; }
        IWebApplicationSettings WebApplicationSettings { get; }
        IImportService ImportService { get; set; }
        IAnonymizationService AnonymizationService { get; }
        IDtoPropertyNameCardIndexDataService FieldNameService { get; }
        IUserChoreProvider UserChoresProvider { get; }
    }
}