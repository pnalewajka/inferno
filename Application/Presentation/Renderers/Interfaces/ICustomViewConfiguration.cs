﻿using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface ICustomViewConfiguration
    {
        string CustomViewName { get; }

        GridDisplayMode DisplayMode { get; }
    }
}
