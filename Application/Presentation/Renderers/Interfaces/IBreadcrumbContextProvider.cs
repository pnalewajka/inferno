﻿using Smt.Atomic.Presentation.Renderers.Breadcrumbs;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IBreadcrumbContextProvider
    {
        BreadcrumbContext Context { get; }
    }
}