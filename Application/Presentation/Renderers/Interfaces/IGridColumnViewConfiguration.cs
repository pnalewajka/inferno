using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IGridColumnViewConfiguration
    {
        string MemberPath { get; }
        MemberInfo MemberInfo { get; }
        IEnumerable<MemberInfo> MemberChain { get; }
        bool IsVisible { get; set; }
        bool IsSortable { get; set; }
        SortingDirection SortingDirection { get; set; }
    }
}