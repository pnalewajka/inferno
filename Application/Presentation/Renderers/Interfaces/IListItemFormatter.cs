﻿namespace Smt.Atomic.Presentation.Renderers.Interfaces
{   
    public interface IListItemFormatter<TViewModel> where TViewModel : class, new()
    {
        string Format(TViewModel viewModel);
    }
}
