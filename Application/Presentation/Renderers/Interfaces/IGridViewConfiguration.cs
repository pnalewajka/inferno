using System.Collections.Generic;
using System.ComponentModel;

namespace Smt.Atomic.Presentation.Renderers.Interfaces
{
    public interface IGridViewConfiguration
    {
        /// <summary>
        /// User friendly name.
        /// </summary>
        [Localizable(true)]
        string Name { get; }

        /// <summary>
        /// Identifier that shows up in URL parameter.
        /// </summary>
        string Identifier { get; }

        /// <summary>
        /// List of columns with their configurations
        /// </summary>
        IList<IGridColumnViewConfiguration> ColumnConfiguration { get; }

        /// <summary>
        /// Determines if this configuration is default
        /// </summary>
        bool IsDefault { get; }

        /// <summary>
        /// Determines if a configuration is hidden from list of available views
        /// </summary>
        bool IsHidden { get; }
    }
}