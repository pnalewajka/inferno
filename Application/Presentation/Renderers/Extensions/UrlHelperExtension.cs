﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

namespace Smt.Atomic.Presentation.Renderers.Extensions
{
    /// <summary>
    /// UriAction specific branch of UrlHelper extensions
    /// </summary>
    public static class UrlHelperExtension
    {
        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionType">What http verb to use here</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static UriAction UriActionFormat(this UrlHelper urlHelper,
                                          ActionType actionType,
                                          [NotNull, AspMvcAction] string actionName,
                                          [NotNull, AspMvcController] string controllerName,
                                          KnownParameter knownParameters,
                                          string parameters = null,
                                          params object[] args)
        {
            var recognizedParameters = GetKnownParameters(knownParameters);
            var allParameters = StringHelper.Join("&", recognizedParameters, parameters);

            var url = urlHelper.ActionWithParams(actionName, controllerName, allParameters, args ?? new object[0]);
            var action = new UriAction(url, actionType);

            return action;
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionType">What http verb to use here</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriAction(this UrlHelper urlHelper,
                                          ActionType actionType,
                                          [NotNull, AspMvcAction] string actionName,
                                          [NotNull, AspMvcController] string controllerName,
                                          KnownParameter knownParameters,
                                          params string[] parameters)
        {
            var recognizedParameters = GetKnownParameterKeysAndValues(knownParameters);
            var allParameters = recognizedParameters.Concat(parameters ?? new string[0]).ToArray();

            var url = urlHelper.ActionWithParams(actionName, controllerName, allParameters);
            var action = new UriAction(url, actionType);

            return action;
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionType">What http verb to use here</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static UriAction UriActionFormat(this UrlHelper urlHelper,
                                          ActionType actionType,
                                          [NotNull, AspMvcAction] string actionName,
                                          KnownParameter knownParameters,
                                          string parameters = null,
                                          params object[] args)
        {
            var recognizedParameters = GetKnownParameters(knownParameters);
            var allParameters = StringHelper.Join("&", recognizedParameters, parameters);

            var url = urlHelper.ActionWithParams(actionName, allParameters, args ?? new object[0]);
            var action = new UriAction(url, actionType);

            return action;
        }


        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionType">What http verb to use here</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriAction(this UrlHelper urlHelper,
                                          ActionType actionType,
                                          [NotNull, AspMvcAction] string actionName,
                                          KnownParameter knownParameters,
                                          params string[] parameters)
        {
            var recognizedParameters = GetKnownParameterKeysAndValues(knownParameters);
            var allParameters = recognizedParameters.Concat(parameters ?? new string[0]).ToArray();

            var url = urlHelper.ActionWithParams(actionName, allParameters);
            var action = new UriAction(url, actionType);

            return action;
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static UriAction UriActionFormatGet(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          [NotNull, AspMvcController] string controllerName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          string parameters = null,
                                          params object[] args)
        {
            return UriActionFormat(urlHelper, ActionType.Get, actionName, controllerName, knownParameters, parameters, args);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriActionGet(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          [NotNull, AspMvcController] string controllerName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          params string[] parameters)
        {
            return UriAction(urlHelper, ActionType.Get, actionName, controllerName, knownParameters, parameters);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriActionGet(this UrlHelper urlHelper,
                                             [NotNull, AspMvcAction] string actionName,
                                             [NotNull, AspMvcController] string controllerName,
                                             [NotNull, AspMvcController] string areaName,
                                             KnownParameter knownParameters = KnownParameter.None,
                                             params string[] parameters)
        {
            parameters = (parameters ?? new string[0])
                .Union(new[] { RoutingHelper.AreaParameterName, areaName })
                .ToArray();

            return UriAction(urlHelper, ActionType.Get, actionName, controllerName, knownParameters, parameters);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="methodNameExpression">Expression to retrieve the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        /// <typeparam name="TController">Controller type to examine for controller name and area.</typeparam>
        public static UriAction UriActionGet<TController>(this UrlHelper urlHelper,
                                          [NotNull] Expression<Func<TController, object>> methodNameExpression,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          params string[] parameters) where TController : Controller
        {
            var controllerName = RoutingHelper.GetControllerName<TController>();
            var actionName = MethodHelper.GetMethodName(methodNameExpression);
            var areaName = RoutingHelper.GetAreaName<TController>();

            parameters = (parameters ?? new string[0])
                .Union(new[] { RoutingHelper.AreaParameterName, areaName })
                .ToArray();

            return UriAction(urlHelper, ActionType.Get, actionName, controllerName, knownParameters, parameters);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static UriAction UriActionFormatGet(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          string parameters = null,
                                          params object[] args)
        {
            return UriActionFormat(urlHelper, ActionType.Get, actionName, knownParameters, parameters, args);
        }


        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriActionGet(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          params string[] parameters)
        {
            return UriAction(urlHelper, ActionType.Get, actionName, knownParameters, parameters);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static UriAction UriActionFormatPost(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          [NotNull, AspMvcController] string controllerName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          string parameters = null,
                                          params object[] args)
        {
            return UriActionFormat(urlHelper, ActionType.Post, actionName, controllerName, knownParameters, parameters, args);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriActionPost(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          [NotNull, AspMvcController] string controllerName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          params string[] parameters)
        {
            return UriAction(urlHelper, ActionType.Post, actionName, controllerName, knownParameters, parameters);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="methodNameExpression">Expression to retrieve the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        /// <typeparam name="TController">Controller type to examine for controller name and area.</typeparam>
        public static UriAction UriActionPost<TController>(this UrlHelper urlHelper,
                                          [NotNull] Expression<Func<TController, object>> methodNameExpression,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          params string[] parameters) where TController : Controller
        {
            var controllerName = RoutingHelper.GetControllerName<TController>();
            var actionName = MethodHelper.GetMethodName(methodNameExpression);
            var areaName = RoutingHelper.GetAreaName<TController>();

            parameters = (parameters ?? new string[0])
                .Union(new[] { RoutingHelper.AreaParameterName, areaName })
                .ToArray();

            return UriAction(urlHelper, ActionType.Get, actionName, controllerName, knownParameters, parameters);
        }

        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static UriAction UriActionFormatPost(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          string parameters = null,
                                          params object[] args)
        {
            return UriActionFormat(urlHelper, ActionType.Post, actionName, knownParameters, parameters, args);
        }


        /// <summary>
        /// Generates a UriAction object with a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The UriAction object with a fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="knownParameters">Specify typical, supported parameters, such as context or selected id</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        public static UriAction UriActionPost(this UrlHelper urlHelper,
                                          [NotNull, AspMvcAction] string actionName,
                                          KnownParameter knownParameters = KnownParameter.None,
                                          params string[] parameters)
        {
            return UriAction(urlHelper, ActionType.Post, actionName, knownParameters, parameters);
        }


        private static string GetKnownParameters(KnownParameter knownParameters)
        {
            var parameters = GetParameterArray(knownParameters);

            return string.Join("&", parameters);
        }

        private static IEnumerable<string> GetKnownParameterKeysAndValues(KnownParameter knownParameters)
        {
            var parameters = GetParameterArray(knownParameters);

            return parameters.SelectMany(p => p.Split('=')).ToArray();
        }

        private static IEnumerable<string> GetParameterArray(KnownParameter knownParameters)
        {
            var parameters = new List<string>();

            foreach (var parameter in EnumHelper.GetEnumValues<KnownParameter>())
            {
                var descriptionAttribute = AttributeHelper.GetEnumFieldAttribute<DescriptionAttribute>(parameter);

                if (descriptionAttribute != null && knownParameters.HasFlag(parameter))
                {
                    parameters.Add(descriptionAttribute.Description);
                }
            }

            return parameters;
        }
    }
}
