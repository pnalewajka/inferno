﻿using System;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        /// <summary>
        /// Add errors from model state to alert service
        /// </summary>
        /// <param name="modelStateDictionary"></param>
        /// <param name="alertService"></param>
        public static void AddErrorsToAlertService([NotNull] this ModelStateDictionary modelStateDictionary,
            [NotNull] IAlertService alertService)
        {
            if (modelStateDictionary == null)
            {
                throw new ArgumentNullException(nameof(alertService));
            }

            if (alertService == null)
            {
                throw new ArgumentNullException(nameof(alertService));
            }

            modelStateDictionary.Values.SelectMany(v => v.Errors)
                .ToList()
                .ForEach(x => alertService.AddError(x.ErrorMessage.ToInvariantString()));
        }
    }
}
