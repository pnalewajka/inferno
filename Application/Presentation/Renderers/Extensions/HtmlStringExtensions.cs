﻿using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace Smt.Atomic.Presentation.Renderers.Extensions
{
    public static class HtmlStringExtensions
    {
        public static string ToPlainText(this HtmlString htmlString)
        {
            var lineBreakRegex = new Regex(@"<(br|BR)\s{0,1}\/{0,1}>", RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(@"<[^>]*(>|$)", RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(@"(>|$)(\W|\n|\r)+<", RegexOptions.Multiline);
            var spaceRegex = new Regex("[\u0020]{2,}", RegexOptions.Multiline);
            var nbspRegex = new Regex("[\u00A0]", RegexOptions.Multiline);

            var text = htmlString.ToHtmlString();
            text = WebUtility.HtmlDecode(text);
            text = tagWhiteSpaceRegex.Replace(text, "><");
            text = spaceRegex.Replace(text, " ");
            text = nbspRegex.Replace(text, " ");
            text = lineBreakRegex.Replace(text, "\n");
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
    }
}
