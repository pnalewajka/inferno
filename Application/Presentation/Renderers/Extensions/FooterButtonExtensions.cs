﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Extensions
{
    public static class FooterButtonExtensions
    {
        public const string EditButtonName = "edit";
        public const string EditAndStayButtonName = "edit-and-stay";
        public const string AddButtonName = "add";
        public const string AddAndEditButtonName = "add-and-edit";
        public const string CancelButtonName = "cancel";
        public const string DeleteButtonName = "delete";

        /// <summary>
        /// Find button by name
        /// </summary>
        /// <param name="buttons">Buttons collection</param>
        /// <param name="name">Name of button</param>
        /// <returns>Single or default ICardIndexButtonViewModel</returns>
        public static IFooterButton GetButtonByName(this IEnumerable<IFooterButton> buttons, string name)
        {
            return buttons.SingleOrDefault(b => b.Id == name);
        }

        /// <summary>
        /// Find edit button
        /// </summary>
        /// <param name="buttons">Buttons collection</param>
        /// <returns>Single or default ICardIndexButtonViewModel</returns>
        public static IFooterButton GetEditButton(this IList<IFooterButton> buttons)
        {
            return buttons.GetButtonByName(EditButtonName);
        }

        /// <summary>
        /// Find add button
        /// </summary>
        /// <param name="buttons">Buttons collection</param>
        /// <returns>Single or default ICardIndexButtonViewModel</returns>
        public static IFooterButton GetAddButton(this IList<IFooterButton> buttons)
        {
            return buttons.GetButtonByName(AddButtonName);
        }

        /// <summary>
        /// Change display text for button
        /// </summary>
        /// <param name="button">Button</param>
        /// <param name="displayText">New text to display</param>
        /// <returns>ICardIndexButtonViewModel</returns>
        public static IFooterButton SetDisplayText(this IFooterButton button, string displayText)
        {
            button.Text = displayText;

            return button;
        }

        /// <summary>
        /// Set submit form parameter for button from button name
        /// </summary>
        /// <typeparam name="TModel">View model</typeparam>
        /// <param name="button">Button</param>
        /// <param name="expression">View model expression</param>
        /// <returns>ICardIndexButtonViewModel</returns>
        public static IFooterButton SetSubmitParameter<TModel>(this IFooterButton button, Expression<Func<TModel, object>> expression)
        {
            return button.SetSubmitParameter(PropertyHelper.GetPropertyName(expression), button.Id);
        }

        /// <summary>
        /// Set submit form parameter for button
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="button">Button</param>
        /// <param name="buttonValue">Value</param>
        /// <param name="expression">View model expression</param>
        /// <returns>ICardIndexButtonViewModel</returns>
        public static IFooterButton SetSubmitParameter<TModel>(this IFooterButton button, object buttonValue, Expression<Func<TModel, object>> expression)
        {
            return button.SetSubmitParameter(PropertyHelper.GetPropertyName(expression), buttonValue);
        }

        /// <summary>
        /// Set submit form parameter for button
        /// </summary>
        /// <param name="button">Button</param>
        /// <param name="propertyName">View model property name</param>
        /// <param name="value">Value</param>
        /// <returns>ICardIndexButtonViewModel</returns>
        public static IFooterButton SetSubmitParameter(this IFooterButton button, string propertyName, object value)
        {
            button.Attributes.Add("data-action-name", propertyName, TagAttributeDuplicateBehavior.Replace);
            button.Attributes.Add("data-action-value", value, TagAttributeDuplicateBehavior.Replace);

            return button;
        }

        /// <summary>
        /// Display confirm dialog on button click
        /// </summary>
        /// <param name="button">Button</param>
        /// <param name="confirmTitle">Dialog title</param>
        /// <param name="confirmContent">Dialog content</param>
        /// <returns></returns>
        public static IFooterButton SetRequireConfirmation(this IFooterButton button, string confirmTitle, string confirmContent)
        {
            button.Attributes.Add("data-confirm-title", confirmTitle, TagAttributeDuplicateBehavior.Replace);
            button.Attributes.Add("data-confirm-content", confirmContent, TagAttributeDuplicateBehavior.Replace);

            return button;
        }

        public static IFooterButton SetBeforeAction(this IFooterButton button, string jsAction)
        {
            button.Attributes.Add("data-before-action", jsAction, TagAttributeDuplicateBehavior.Replace);

            return button;
        }
    }
}
