﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class ViewModelPropertyHelper
    {
        private const string TimestampPropertyName = "Timestamp";

        /// <summary>
        /// Determines if the given property should be used in form/grid column generation
        /// </summary>
        /// <param name="memberInfo">Property to be determined</param>
        /// <param name="propertyInfos">All properties of a given type</param>
        /// <returns>true, if the property controls readonly attribute of other property and should be skipped in form rendering</returns>
        public static bool IsReadOnlyControlProperty(MemberInfo memberInfo, MemberInfo[] propertyInfos)
        {
            const string prefix = "Is";
            const string sufix = "ReadOnly";

            var readOnlyControlProperty =
                memberInfo.Name.StartsWith(prefix)
                && memberInfo.Name.EndsWith(sufix)
                &&
                propertyInfos.Any(
                    i =>
                        i.Name ==
                        memberInfo.Name.Substring(prefix.Length,
                            memberInfo.Name.Length - prefix.Length - sufix.Length));

            return readOnlyControlProperty;
        }

        /// <summary>
        /// Determines how the given property should be displayed in a given visibility context
        /// </summary>
        /// <param name="memberInfo">property info of a given property</param>
        /// <param name="visibilityScope">visibility context for which the check should be performed</param>
        /// <returns></returns>
        public static ControlVisibility ShouldBeVisible(MemberInfo memberInfo, VisibilityScope visibilityScope)
        {
            var shouldBeVisible = true;

            var displayAttribute = AttributeHelper.GetMemberAttribute(memberInfo, (DisplayAttribute)null);
            var visibilityAttribute = AttributeHelper.GetMemberAttribute(memberInfo, (VisibilityAttribute)null);

            var propertiesHiddenByDefault = new[] { ViewModelHelper.IdPropertyName };
            var propertiesHiddenInGridByDefault = new[] { TimestampPropertyName };

            if (visibilityAttribute == null)
            {
                if (propertiesHiddenByDefault.Contains(memberInfo.Name))
                {
                    if (visibilityScope.HasFlag(VisibilityScope.Grid))
                    {
                        return ControlVisibility.Remove;
                    }

                    return ControlVisibility.Hide;
                }

                if (propertiesHiddenInGridByDefault.Contains(memberInfo.Name)
                    && visibilityScope.HasFlag(VisibilityScope.Grid))
                {
                    return ControlVisibility.Remove;
                }

                visibilityAttribute = new VisibilityAttribute(VisibilityScope.Form | VisibilityScope.Grid);
            }

            if (displayAttribute != null)
            {
                var autoGenerateField = displayAttribute.GetAutoGenerateField();

                if (autoGenerateField.HasValue)
                {
                    shouldBeVisible &= autoGenerateField.Value;
                }
            }

            shouldBeVisible &= (visibilityScope & visibilityAttribute.VisibilityScope) != VisibilityScope.None;

            return shouldBeVisible ? ControlVisibility.Show : ControlVisibility.Remove;
        }

        /// <summary>
        /// Gets the value that will be used to display a watermark in the UI.
        /// </summary>
        /// <param name="memberInfo">Property info of the member to get a watermark for</param>
        /// <returns>A value that will be used to display a watermark in the UI.</returns>
        public static string GetPrompt(MemberInfo memberInfo)
        {
            var displayAttribute = AttributeHelper.GetMemberAttribute(memberInfo, (DisplayAttribute)null);

            if (displayAttribute != null && !string.IsNullOrEmpty(displayAttribute.Prompt))
            {
                return displayAttribute.Prompt;
            }

            return AttributeHelper.GetMemberAttribute(memberInfo, () => new PromptAttribute(null)).Text;
        }

        /// <summary>
        /// Returns all the property info in the order which they should be used in the UI
        /// </summary>
        /// <param name="propertyInfos"></param>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> GetSortedProperties(PropertyInfo[] propertyInfos)
        {
            return
                propertyInfos.Select(i => new
                {
                    PropertyInfo = i,
                    Order = ViewModelMemberHelper.GetOrder(i)
                })
                    .OrderBy(p => p.Order)
                    .Select(p => p.PropertyInfo);
        }
    }
}
