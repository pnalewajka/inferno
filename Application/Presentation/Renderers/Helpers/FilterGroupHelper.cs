﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public class FilterGroupHelper
    {
        /// <summary>
        /// Get list of restricted filters
        /// </summary>
        /// <param name="filterGroups"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>
        public static IList<string> GetReadRestrictedFilterCodes(IEnumerable<FilterGroup> filterGroups, IEnumerable<string> userRoles)
        {
            var userRolesHash = userRoles.ToHashSet();

            return filterGroups.SelectMany(g => g.Filters)
                .Where(f => f.RequiredRole.HasValue && !userRolesHash.Contains(f.RequiredRole.Value.ToString()))
                .Select(f => f.Code)
                .ToList();
        }
    }
}
