namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    /// <summary>
    /// Handles common icon routines
    /// </summary>
    public static class IconHelper
    {
        /// <summary>
        /// Returns a full icon class name for supported naming conventions (glyphicons and font awesome)
        /// </summary>
        /// <param name="shortIconName">Short icon name</param>
        /// <returns>Full class name to be used in html</returns>
        public static string GetCssClass(string shortIconName)
        {
            if (string.IsNullOrWhiteSpace(shortIconName))
            {
                return null;
            }

            if (shortIconName.StartsWith("glyphicon-"))
            {
                return "glyphicon " + shortIconName;
            }

            if (shortIconName.StartsWith("fa-"))
            {
                return "fa " + shortIconName;
            }

            return null;
        }
    }
}