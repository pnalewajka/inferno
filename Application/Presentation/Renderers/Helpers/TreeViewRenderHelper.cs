﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RazorEngine.Text;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class TreeViewRenderHelper
    {
        public static RawString GetRowDataAttributes(object model, long rowIndex, long rowCount)
        {
            var treeNodeItemRow = model as ITreeNodeItemViewModel;

            if (treeNodeItemRow == null)
            {
                return new RawString(string.Empty);
            }

            var attributes = GetRowTagAttributes(treeNodeItemRow, rowIndex, rowCount);

            return new RawString(attributes.Render());
        }

        public static TagAttributes GetRowDataTagAttributes(object model, long rowIndex, long rowCount)
        {
            var treeNodeItemRow = model as ITreeNodeItemViewModel;

            if (treeNodeItemRow == null)
            {
                return new TagAttributes();
            }

            var attributes = GetRowTagAttributes(treeNodeItemRow, rowIndex, rowCount);

            return attributes;
        }

        public static string GetRowCss(object model, long rowIndex, long rowCount)
        {
            var treeNodeItemRow = model as ITreeNodeItemViewModel;

            if (treeNodeItemRow == null)
            {
                return null;
            }

            return treeNodeItemRow.GetRowCssClass() + (rowIndex == rowCount - 1 ? " last-item" : string.Empty);
        }

        public static MvcHtmlString ParentItemsIds(IList<object> rows)
        {
            var parentIds = rows
                .Select(r => (r as ITreeNodeItemViewModel)?.ParentId)
                .Where(r => r.HasValue)
                .Select(r => r.Value)
                .ToList()
                .Distinct();

            return new MvcHtmlString(JsonHelper.Serialize(parentIds, true));
        }

        private static TagAttributes GetRowTagAttributes(ITreeNodeItemViewModel treeNodeItemRow, long rowIndex, long rowCount)
        {
            var attributes = new TagAttributes();
            attributes.Add("data-tree-item-id", treeNodeItemRow.Id);

            if (treeNodeItemRow.Depth.HasValue)
            {
                attributes.Add("data-tree-depth", treeNodeItemRow.Depth);

                if (treeNodeItemRow.Depth > 0)
                {
                    attributes.Add("data-twig", rowIndex == rowCount - 1 ? "l" : "i");
                }
            }

            if (treeNodeItemRow.ParentId.HasValue)
            {
                attributes.Add("data-tree-parent-id", treeNodeItemRow.ParentId);
            }

            return attributes;
        }
    }
}
