﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class ViewModelMemberHelper
    {
        /// <summary>
        /// Gets the label from given member info to be used in forms, grid, etc.
        /// </summary>
        /// <param name="viewModelType">View model type</param>
        /// <param name="memberName">Name of the property to process</param>
        /// <returns>string which should be used as label or column name</returns>
        public static string GetLabel(Type viewModelType, string memberName)
        {
            var propertyInfo = viewModelType.GetProperty(memberName);

            return GetLabel(propertyInfo);
        }

        /// <summary>
        /// Gets the label from given member info to be used in forms, grid, etc.
        /// </summary>
        /// <param name="memberInfo">Member info of a given member</param>
        /// <returns>string which should be used as label or column name</returns>
        public static string GetLabel([NotNull] MemberInfo memberInfo)
        {
            return PropertyHelper.GetPropertyFriendlyName(memberInfo);
        }

        /// <summary>
        /// Gets the grid label from given member info to be used in forms, grid, etc.
        /// </summary>
        /// <param name="memberInfo">Member info of a given member</param>
        /// <returns>string which should be used as column name</returns>
        public static string GetGridLabel([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException("memberInfo");
            }

            var displayAttribute = AttributeHelper.GetMemberAttribute<DisplayNameLocalizedAttribute>(memberInfo);
            
            if (displayAttribute != null)
            {
                return displayAttribute.DisplayShortName;
            }

            return GetLabel(memberInfo);
        }

        /// <summary>
        /// Checks if the member has a label set via attribute
        /// </summary>
        /// <param name="memberInfo">Member info of a given member</param>
        /// <returns>If label attribute is present</returns>
        public static bool HasLabel([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            var displayAttribute = AttributeHelper.GetMemberAttribute<DisplayAttribute>(memberInfo);

            if (displayAttribute != null)
            {
                return true;
            }

            var displayNameAttribute = AttributeHelper.GetMemberAttribute<DisplayNameAttribute>(memberInfo);

            if (displayNameAttribute != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the order in which the member should be used in UI
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public static long GetOrder([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            var orderAttribute = AttributeHelper.GetMemberAttribute<OrderAttribute>(memberInfo);

            if (orderAttribute == null)
            {
                return long.MaxValue;
            }

            return orderAttribute.Order;
        }

        /// <summary>
        /// Gets the css class to be used for given value when displayed in grid
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public static string GetGridCssClass([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            var renderAttribute = AttributeHelper.GetMemberAttribute<RenderAttribute>(memberInfo);

            return renderAttribute?.GridCssClass;
        }

        /// <summary>
        /// Gets the css class to be used for given header when displayed in grid
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public static string GetGridHeaderCssClass([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            return AttributeHelper.GetMemberAttribute(memberInfo, () => new RenderAttribute()).GridHeaderCssClass;
        }

        /// <summary>
        /// Checks if method should be treated as sortable via different property, returns property name
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns>Property name to be used during sorting</returns>
        public static string GetSortableByPropertyName([NotNull] MethodInfo methodInfo)
        {
            if (methodInfo == null)
            {
                throw new ArgumentNullException(nameof(methodInfo));
            }

            var sortableByAttribute = AttributeHelper.GetMemberAttribute<SortByAttribute>(methodInfo);

            if (sortableByAttribute != null && !string.IsNullOrWhiteSpace(sortableByAttribute.PropertyName))
            {
                return sortableByAttribute.PropertyName;
            }

            return null;
        }

        /// <summary>
        /// Gets the hint from given member info to be used in forms
        /// </summary>
        /// <param name="memberInfo">Member info of a given member</param>
        /// <returns>string which should be used as hint</returns>
        public static string GetHint([NotNull] MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            var hintAttribute = AttributeHelper.GetMemberAttribute<HintAttribute>(memberInfo);

            return hintAttribute?.Text;
        }

        /// <summary>
        /// Gets the tooltip for given member info to be used in forms
        /// </summary>
        /// <param name="memberInfo">Member info of a given member</param>
        /// <param name="model">Model from which retrieve the tooltip value </param>
        /// <returns>String which should be used as a tooltip</returns>
        public static string GetTooltip([NotNull] MemberInfo memberInfo, [NotNull] object model)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            var tooltipAttribute = AttributeHelper.GetMemberAttribute<TooltipAttribute>(memberInfo);

            return tooltipAttribute != null 
                ? PropertyHelper.GetPropertyValue<string>(model, tooltipAttribute.TooltipValuePropertyName) 
                : null;
        }
    }
}