﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class SearchAreaHelper
    {
        private const string QueryStringFieldName = "search-in";

        public static ISet<string> GetSelectedSearchAreasCodes(IDictionary<string, object> queryStringFieldValues)
        {
            if (queryStringFieldValues.TryGetValue(QueryStringFieldName, out object areas))
            {
                return areas
                    .ToString()
                    .Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .ToHashSet();
            }

            return new HashSet<string>();
        }
    }
}
