using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Filter = Smt.Atomic.Presentation.Renderers.CardIndex.Filters.Filter;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    /// <summary>
    /// Methods dealing with common Card Index configuration elements, such as context and filters
    /// </summary>
    public static class CardIndexHelper
    {
        private const string ReturnUrlFieldName = "Atomic.ReturnUrl";
        private const string DeleteActionName = "Delete";
        private const string ListActionName = "List";

        /// <summary>
        /// Returns a list of filters based on the given enum. Enum values can be filtered, too.
        /// </summary>
        /// <typeparam name="TEnum">Enum type to be used for filter item retrieval</typeparam>
        /// <param name="valuePredicate">Optional predicate to filter down the values</param>
        /// <param name="getCssClass">Optional func to set css class for given TEnum</param>
        /// <returns>List of filters</returns>
        public static IList<Filter> BuildEnumBasedFilters<TEnum>(
            Func<TEnum, bool> valuePredicate = null,
            Func<TEnum, string> getCssClass = null)
        {
            if (valuePredicate == null)
            {
                valuePredicate = v => true;
            }

            var enumValues = EnumHelper.GetEnumValues<TEnum>()
                .Where(valuePredicate);

            var filters = enumValues
                .Select(v => new Filter
                {
                    Code = v.ToString(),
                    DisplayName = ((Enum)(object)v).GetDescriptionOrValue(),
                    CssClass = getCssClass?.Invoke(v)
                })
                .ToList();

            return filters;
        }

        /// <summary>
        /// Builds a group of filters that are used when user can access both soft-deleted and existing records
        /// </summary>
        /// <param name="displayName"></param>
        /// <returns>Filter group used to filter soft-deleted items</returns>
        public static FilterGroup BuildSoftDeletableFilterGroup(string displayName)
        {
            return new FilterGroup
            {
                DisplayName = displayName,
                Type = FilterGroupType.RadioGroup,
                Filters = BuildEnumBasedFilters<SoftDeletableStateFilter>()
            };
        }

        /// <summary>
        /// Returns most suitable url for given's controller List action.
        /// </summary>
        /// <param name="controller">Controller that contains the card index component</param>
        /// <param name="context">Data context to be used in url formulation</param>
        /// <returns>List action url</returns>
        public static string GetListUrl(AtomicController controller, object context)
        {
            var parameters = UrlFieldsHelper.CreateQueryParametersFromObject(context);
            var listActionUrl = controller.Url.Action(ListActionName, parameters == null ? null : new RouteValueDictionary(parameters));

            return listActionUrl;
        }

        /// <summary>
        /// Returns most suitable url for given's controller form action (e.g. Edit, View)
        /// </summary>
        /// <param name="controller">Controller that contains the card index component</param>
        /// <param name="context">Data context to be used in url formulation</param>
        /// <returns>Form action url</returns>
        public static string GetRecordActionUrl(AtomicController controller, string actionName, long id, object context)
        {
            var parameters = UrlFieldsHelper.CreateQueryParametersFromObject(context);
            var routeDictionary = parameters == null
                ? new RouteValueDictionary()
                : new RouteValueDictionary(parameters);

            routeDictionary.Add(RoutingHelper.IdParameterName, id);

            var actionUrl = controller.Url.Action(actionName, new RouteValueDictionary(routeDictionary));

            return actionUrl;
        }

        /// <summary>
        /// Returns most suitable url for given's controller Delete action.
        /// </summary>
        /// <param name="controller">Controller that contains the card index component</param>
        /// <param name="context">Data context to be used in url formulation</param>
        /// <returns>Delete action url</returns>
        public static string GetDeleteUrl(AtomicController controller, object context)
        {
            var parameters = UrlFieldsHelper.CreateQueryParametersFromObject(context);

            if (controller is ICardIndexController && parameters == null)
            {
                parameters = HttpUtility.ParseQueryString(KnownParameter.SelectedIds.GetDescription()).ToDictionary();
            }

            var listActionUrl = controller.Url.Action(DeleteActionName, parameters == null ? null : new RouteValueDictionary(parameters));

            return listActionUrl;
        }

        /// <summary>
        /// Attempts to retrieve the return url specified by the form. 
        /// If not found, returns list action url (using GetListUrl)
        /// </summary>
        /// <param name="controller">Controller that contains the card index component</param>
        /// <param name="context">Data context to be used in fallback url formulation</param>
        /// <returns>Referrer or list action url</returns>
        public static string GetReturnOrListUrl(AtomicController controller, object context)
        {
            var referrerUrl = controller.Request.Form[ReturnUrlFieldName];

            if (!string.IsNullOrWhiteSpace(referrerUrl))
            {
                return referrerUrl;
            }

            return GetListUrl(controller, context);
        }
    }
}