﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RazorEngine;
using RazorEngine.Templating;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class RazorTemplateHelper
    {
        private static readonly Dictionary<string, string> Templates = new Dictionary<string, string>();

        /// <summary>
        /// Applies specified Razor template to the control model given and returns the transformation result as a string
        /// </summary>
        /// <param name="controlRenderingModel">Control model for a given partial view</param>
        /// <param name="templateName">Template to use to render given control</param>
        /// <returns>Html output of the template applied to control model</returns>
        public static string ParseControlTemplate([NotNull] ControlRenderingModel controlRenderingModel,
                                                  [NotNull] string templateName,
                                                  string templateVersion)
        {
            if (controlRenderingModel == null)
            {
                throw new ArgumentNullException(nameof(controlRenderingModel));
            }

            if (templateName == null)
            {
                throw new ArgumentNullException(nameof(templateName));
            }

            var modelType = controlRenderingModel.GetType();
            var template = GetTemplateResource(templateName, modelType);

            var customRenderingModel = controlRenderingModel as CustomControlRenderingModel;

            if (customRenderingModel != null)
            {
                var templatePath = customRenderingModel.GetTemplatePathByLayout(templateVersion);
                var assembly = customRenderingModel.Assembly;

                var customTemplate = ResourceHelper.GetResourceAsString(templatePath + ".cshtml", assembly);
                Engine.Razor.AddTemplate(templatePath, customTemplate);
            }

            Engine.Razor.AddTemplate(templateName, template);
            var controlHtml = Engine.Razor.RunCompile(templateName, modelType, controlRenderingModel);

            return controlHtml;
        }

        private static string GetTemplateResource(string templateName, Type modelType)
        {
            if (!Templates.ContainsKey(templateName))
            {
                var templateResource = ResourceHelper.GetResourceAsStringByShortName(templateName, modelType);

                var templateLines = templateResource.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Select(l => l.Trim()).ToList();

                var template = new StringBuilder();

                foreach (var line in templateLines)
                {
                    if (line.StartsWith("@using") || line.StartsWith("@model"))
                    {
                        template.AppendLine(line);
                    }
                    else
                    {
                        if (!line.StartsWith("@") && !line.StartsWith("<") && !line.StartsWith("/>"))
                        {
                            template.Append(" ");
                        }

                        var cleanLine = Regex.Replace(line, "\\s{2,}", " ");
                        cleanLine = cleanLine.Replace("{ <", "{<")
                                             .Replace("> }", ">}");
                        template.Append(cleanLine);
                    }
                }

                Templates[templateName] = template.ToString();
            }

            return Templates[templateName];
        }
    }
}