﻿using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    /// <summary>
    /// Support methods for crop image attribute handling
    /// </summary>
    public static class CropImageAttributeHelper
    {
        /// <summary>
        /// Extract crop image attribute from class given by Identifier
        /// </summary>
        /// <param name="viewModelClassIdentifier"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static CropImageUploadAttribute GetCropAttribute(string viewModelClassIdentifier, string propertyName)
        {
            var viewModelType = IdentifierHelper.GetTypeByIdentifier(viewModelClassIdentifier);
            var propertyInfo = PropertyHelper.GetInstancePublicPropertyByName(viewModelType, propertyName);

            var cropImageAttribute = AttributeHelper.GetPropertyAttribute<CropImageUploadAttribute>(propertyInfo);

            if (cropImageAttribute == null)
            {
                throw new AttributeNotFoundException<CropImageUploadAttribute>();
            }

            return cropImageAttribute;
        }
    }
}
