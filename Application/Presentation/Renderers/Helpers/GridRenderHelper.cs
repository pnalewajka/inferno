﻿using RazorEngine.Text;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class GridRenderHelper
    {
        public static string GetRowCss(object model, string otherClass = null)
        {
            var gridViewRow = model as IGridViewRow;

            if (gridViewRow == null)
            {
                return otherClass;
            }

            var gridClass = gridViewRow.RowCssClass;

            return string.IsNullOrEmpty(otherClass)
                ? gridClass
                : $"{gridClass} {otherClass}";
        }

        public static RawString GetRowDataAttributes(object model, TagAttributes otherAttributes = null)
        {
            var gridViewRow = model as IGridViewRow;

            if (gridViewRow == null)
            {
                return new RawString(otherAttributes?.Render() ?? string.Empty);
            }

            otherAttributes = otherAttributes ?? new TagAttributes();

            if (!string.IsNullOrWhiteSpace(gridViewRow.RowTitle))
            {
                otherAttributes.Add("title", gridViewRow.RowTitle, TagAttributeDuplicateBehavior.Merge);
            }

            return new RawString(otherAttributes.Render());
        }
    }
}
