﻿using System;
using System.Data;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Results;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Helpers
{
    public static class DialogHelper
    {
        public const string OkButtonTag = "ok";
        public const string CancelButtonTag = "cancel";
        public const string SubmitButtonTag = "submit";

        public static JsonNetResult Reload()
        {
            return new JsonNetResult
            {
                Data = new { action = "reload" }
            };
        }

        public static JsonNetResult Redirect(Controller controller, string relativeUrl, bool repost = false)
        {
            const string redirectActionName = "redirect";

            if (controller == null || controller.Request == null || controller.Request.Url == null)
            {
                throw new NoNullAllowedException(DialogResources.RedirectInvalidControllerRequestUrlExceptionMessage);
            }

            if (controller.Url == null)
            {
                throw new NoNullAllowedException(DialogResources.RedirectInvalidControllerUrlHelperExceptionMessage);
            }

            var resolvedUrl = controller.Url.Content(relativeUrl);

            var uri = new Uri(controller.Request.Url, resolvedUrl);
            var absoluteUrl = uri.AbsoluteUri;

           return DialogAction(redirectActionName, new { url = absoluteUrl, repost });
        }

        public static JsonNetResult RedirectToList(Controller controller)
        {
            const string redirectActionName = "redirect-to-list";

            if (controller?.Request?.Url == null)
            {
                throw new NoNullAllowedException(DialogResources.RedirectInvalidControllerRequestUrlExceptionMessage);
            }

            if (controller.Url == null)
            {
                throw new NoNullAllowedException(DialogResources.RedirectInvalidControllerUrlHelperExceptionMessage);
            }

            var listUrl = controller.Url.Content("List");
            var uri = new Uri(controller.Request.Url, listUrl);
            var absoluteUrl = uri.AbsoluteUri;

            return DialogAction(redirectActionName, new { url = absoluteUrl });
        }

        public static JsonNetResult HandleEvent(string dialogId, string buttonTag, object model)
        {
            const string handleEventActionName = "event";

            return DialogAction(handleEventActionName, new { buttonTag }, model, dialogId);
        }

        public static DialogButton GetSingleOkButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsOkButtonText,
                Tag = OkButtonTag,
                IsDismissButton = true,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter, Key.Esc)
            };
        }

        public static DialogButton GetOkButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsOkButtonText,
                Tag = OkButtonTag,
                IsDismissButton = true,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter)
            };
        }

        public static DialogButton GetOkSubmitButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsOkButtonText,
                Tag = SubmitButtonTag,
                IsDismissButton = false,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter)
            };
        }

        public static DialogButton GetCancelButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsCancelButtonText,
                Tag = CancelButtonTag,
                Hotkey = HotkeyHelper.GetHotkey(Key.Esc)
            };
        }

        public static DialogButton GetCloseButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsCloseButtonText,
                Tag = CancelButtonTag,
                Hotkey = HotkeyHelper.GetHotkey(Key.Esc)
            };
        }

        public static DialogButton GetYesButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsYesButtonText,
                Tag = SubmitButtonTag,
                IsDismissButton = false,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter)
            };
        }

        public static DialogButton GetNoButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsNoButtonText,
                Tag = CancelButtonTag,
                Hotkey = HotkeyHelper.GetHotkey(Key.Esc)
            };
        }

        public static DialogButton GetSaveButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsSaveButtonText,
                Tag = SubmitButtonTag,
                IsDismissButton = false,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter)
            };
        }

        public static DialogButton GetAddButton()
        {
            return new DialogButton
            {
                Text = DialogResources.StandardDialogButtonsAddButtonText,
                Tag = SubmitButtonTag,
                IsDismissButton = false,
                Hotkey = HotkeyHelper.GetHotkey(Key.Enter)
            };
        }

        private static JsonNetResult DialogAction(string action, object data, object model = null, string dialogId = null)
        {
            return new JsonNetResult
            {
                Data = new { action, data, model, dialogId }
            };
        }
    }
}
