﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using CsvHelper;
using CsvHelper.Excel;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.Services
{
    internal class ExportService : IExportService
    {
        private const string ExportExcelFileNameFormat = "{0} {1:yyyy-MM-dd}.xlsx";
        private const string ExportCsvFileNameFormat = "{0} {1:yyyy-MM-dd}.csv";
        private const string ExcelExtension = ".xlsx";

        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IAnonymizationService _anonymizationService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public ExportService(
            IClassMappingFactory classMappingFactory,
            IAnonymizationService anonymizationService,
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _classMappingFactory = classMappingFactory;
            _anonymizationService = anonymizationService;
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public FilePathResult ExportToExcel<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, UrlHelper urlHelper)
            where TViewModel : class, new()
            where TDto : class
        {
            var tempFileName = Path.GetTempFileName() + ExcelExtension;

            var currentCulture = Thread.CurrentThread.CurrentCulture;

            using (CultureInfoHelper.SetCurrentCulture(CultureInfo.InvariantCulture))
            {
                using (var excelSerializer = new ExcelSerializer(tempFileName))
                {
                    using (var csvWriter = new CsvWriter(excelSerializer))
                    {
                        using (CultureInfoHelper.SetCurrentCulture(currentCulture))
                        {
                            Export(cardIndex, parameters, BuildCsvContent(csvWriter, urlHelper));
                        }
                    }
                }

                AdjustColumnWidths(tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName =
                        string.Format(ExportExcelFileNameFormat, cardIndex.Settings.Title, _timeService.GetCurrentTime())
                };

                return result;
            }
        }

        private static void AdjustColumnWidths(string tempFileName)
        {
            var xlWorkbook = new XLWorkbook(tempFileName);
            var xlWorksheet = xlWorkbook.Worksheet(1);

            var column = 1;

            while (!string.IsNullOrWhiteSpace(xlWorksheet.Cell(1, column).GetValue<string>()))
            {
                xlWorksheet.Cell(1, column).Style.Font.Bold = true;
                var xlColumn = xlWorksheet.Column(column++);
                xlColumn.AdjustToContents();

                // Don't remove this. Throws exception if Width > 250
                if (xlColumn.Width > 250)
                {
                    xlColumn.Width = 250;
                }
            }

            xlWorksheet.Range(1, 1, 1, column - 1).SetAutoFilter();
            xlWorkbook.Save();
        }

        public FilePathResult ExportToCsv<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, UrlHelper urlHelper)
            where TViewModel : class, new()
            where TDto : class
        {
            var tempFileName = Path.GetTempFileName();

            using (var streamWriter = new StreamWriter(tempFileName, false, Encoding.UTF8))
            {
                using (var csvWriter = new CsvWriter(streamWriter))
                {
                    csvWriter.Configuration.CultureInfo = CultureInfo.GetCultureInfo(CultureCodes.EnglishAmerican);
                    Export(cardIndex, parameters, BuildCsvContent(csvWriter, urlHelper));
                }
            }

            var result = new FilePathResult(tempFileName, MimeHelper.CommaSeparatedValuesFile)
            {
                FileDownloadName = string.Format(ExportCsvFileNameFormat, cardIndex.Settings.Title, _timeService.GetCurrentTime()),
            };

            return result;
        }

        public string ExportToClipboardFormat<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, UrlHelper urlHelper)
            where TViewModel : class, new()
            where TDto : class
        {
            var result = new StringBuilder();

            Export(cardIndex, parameters, BuildTextContent(result, urlHelper));

            return result.ToString();
        }

        private Action<GridViewModel, GridParameters> BuildTextContent(StringBuilder result, UrlHelper urlHelper)
        {
            Action<GridViewModel, GridParameters> writer = (model, parameters) =>
            {
                var tableColumns = model.Columns.GetAll();

                if (parameters.CurrentPageIndex == 0)
                {
                    var tempRow = new List<string>();
                    foreach (var column in tableColumns)
                    {
                        tempRow.Add(column.Label);
                    }

                    result.Append(string.Join("\t", tempRow) + Environment.NewLine);
                }

                foreach (var row in model.Rows)
                {
                    var tempRow = new List<string>();
                    foreach (var column in tableColumns)
                    {
                        var cellValue = RenderCellText(column, row, urlHelper);
                        tempRow.Add(cellValue);
                    }

                    result.Append(string.Join("\t", tempRow) + Environment.NewLine);
                }
            };

            return writer;
        }

        private Action<GridViewModel, GridParameters> BuildCsvContent(CsvWriter csvWriter, UrlHelper urlHelper)
        {
            Action<GridViewModel, GridParameters> writer = (model, parameters) =>
            {
                var tableColumns = model.Columns.GetAll();

                if (parameters.CurrentPageIndex == 0)
                {
                    foreach (var column in tableColumns)
                    {
                        csvWriter.WriteField(column.Label);
                    }

                    csvWriter.NextRecord();
                }

                foreach (var row in model.Rows)
                {
                    foreach (var column in tableColumns)
                    {
                        var cellValue = RenderCellText(column, row, urlHelper);
                        csvWriter.WriteField(cellValue);
                    }

                    csvWriter.NextRecord();
                }
            };

            return writer;
        }

        private void Export<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters, Action<GridViewModel, GridParameters> buildContent)//  CsvWriter csvWriter)
            where TViewModel : class, new() where TDto : class
        {
            parameters.CurrentPageIndex = 0;
            parameters.PageSize = 10000;

            var dtoToViewModelClassMapping = _classMappingFactory.CreateMapping<TDto, TViewModel>();
            var queryCriteria = parameters.ToCriteria(dtoToViewModelClassMapping, _anonymizationService, _principalProvider.Current.Roles, cardIndex.Settings.FilterGroups);
            var gridViewConfiguration = GetGridViewConfiguration(cardIndex, parameters);

            GridViewModel model;

            do
            {
                model = cardIndex.GetGridModel(queryCriteria, GridRenderingControlContext.None, parameters.QueryStringFieldValues, gridViewConfiguration);
                buildContent(model, parameters);

                parameters.CurrentPageIndex++;
            }
            while (model.CurrentPageIndex < model.LastPageIndex);
        }

        private static string RenderCellText(GridColumnViewModel column, object row, UrlHelper urlHelper) 
        {
            var text = (column.RenderForExport(row, urlHelper) ?? string.Empty).ToString();
            text = HttpUtility.HtmlDecode(text); // decode for excel and csv

            return text;
        }

        private static IGridViewConfiguration GetGridViewConfiguration<TViewModel, TDto>(CardIndex<TViewModel, TDto> cardIndex, GridParameters parameters)
            where TViewModel : class, new() where TDto : class
        {
            IGridViewConfiguration configuration = null;

            if (cardIndex.Settings.GridViewConfigurations != null)
            {
                configuration = cardIndex.Settings.GridViewConfigurations.FirstOrDefault(x => x.Identifier == parameters.ViewName)
                                ?? cardIndex.Settings.GridViewConfigurations.FirstOrDefault(x => x.IsDefault);
            }

            return configuration ?? GridViewConfiguration<TViewModel>.DefaultGridViewConfiguration;
        }
    }
}
