using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Castle.Core.Internal;
using ClosedXML.Excel;
using CsvHelper;
using CsvHelper.Excel;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Services
{
    internal class ImportService : IImportService
    {
        private const string ExcelImportTemplateOutputFileNameFormat = "{0} - Import {1:yyyy-MM-dd}.xlsx";
        private const string CsvImportTemplateOutputFileNameFormat = "{0} - Import {1:yyyy-MM-dd}.csv";
        private const string ExcelExtension = ".xlsx";
        private const string IdColumnName = ViewModelHelper.IdPropertyName;

        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITemporaryDocumentService _temporaryDocumentService;
        private readonly IAnonymizationService _anonymizationService;
        private readonly ITimeService _timeService;
        private readonly HashSet<string> _currentUserRoles;

        public ImportService(
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider,
            ITemporaryDocumentService temporaryDocumentService,
            IAnonymizationService anonymizationService,
            ITimeService timeService)
        {
            _classMappingFactory = classMappingFactory;
            _principalProvider = principalProvider;
            _temporaryDocumentService = temporaryDocumentService;
            _anonymizationService = anonymizationService;
            _timeService = timeService;
            _currentUserRoles = new HashSet<string>(principalProvider.Current.Roles ?? new string[] { });
        }

        private static void AdjustColumnWidths(string tempFileName)
        {
            var xlWorkbook = new XLWorkbook(tempFileName);
            var xlWorksheet = xlWorkbook.Worksheet(1);

            var column = 1;

            while (!string.IsNullOrWhiteSpace(xlWorksheet.Cell(1, column).GetValue<string>()))
            {
                xlWorksheet.Cell(1, column++).Style.Font.Bold = true;
            }

            xlWorksheet.Range(1, 1, 1, column - 1).SetAutoFilter();

            for (var i = 1; i < column; i++)
            {
                xlWorksheet.Column(i).AdjustToContents();
            }

            xlWorkbook.Save();
        }

        private IEnumerable<ControlRenderingModel> GetImportableControls(object model)
        {
            var layout = new SimpleLayout();
            var buildRenderingModels = layout.BuildRenderingModels(model, new Models.ModelBuilderContext(layout), _currentUserRoles);

            return buildRenderingModels.Controls.Where(CanFieldBeImported);
        }

        private bool CanFieldBeImported(ControlRenderingModel model)
        {
            return !model.IsReadOnly
                   && model.Visibility == ControlVisibility.Show
                   && model.CanBeImported;
        }

        public FilePathResult GetCsvImportTemplate(CardIndexSettings cardIndexSettings, int importIndex, ImportTemplateType templateType)
        {
            var importSettings = cardIndexSettings.Imports[importIndex];
            var parameters = new GridParameters();
            var tempFileName = $"{Path.GetTempFileName()}.csv";

            var dtoToViewModelMapping = _classMappingFactory.CreateMapping(importSettings.DtoType,
                importSettings.ViewModelType);

            using (var excelSerializer = new CsvSerializer(File.CreateText(tempFileName)))
            {
                using (var csvWriter = new CsvWriter(excelSerializer))
                {
                    var emptyModel = importSettings.ViewModelType.CreateInstance<object>();

                    if (importSettings.UseRecordId)
                    {
                        csvWriter.WriteField(CardIndexResources.ImportServiceIdColumnText);
                    }

                    foreach (var header in GetImportableControls(emptyModel).SelectMany(cl => cl.GetImportTemplateColumnNames()).ToList())
                    {
                        csvWriter.WriteField(header);
                    }

                    csvWriter.NextRecord();

                    if (templateType == ImportTemplateType.Edit)
                    {
                        CardIndexRecords<object> dataRecords;
                        parameters.CurrentPageIndex = 0;

                        do
                        {
                            dataRecords = GetRecords(importSettings, parameters, dtoToViewModelMapping, cardIndexSettings);

                            var viewModelRecords = dataRecords
                                .Rows
                                .Select(dtoToViewModelMapping.CreateFromSource)
                                .ToList();

                            foreach (var row in viewModelRecords)
                            {
                                if (importSettings.UseRecordId)
                                {
                                    var value = importSettings.ViewModelType.GetProperty(IdColumnName).GetValue(row);
                                    csvWriter.WriteField(value ?? string.Empty);
                                }

                                foreach (var control in GetImportableControls(row))
                                {
                                    foreach (var value in control.GetImportTemplateValues())
                                    {
                                        csvWriter.WriteField(value ?? string.Empty);
                                    }
                                }

                                csvWriter.NextRecord();
                            }

                            parameters.CurrentPageIndex++;
                        } while (dataRecords.CurrentPageIndex < dataRecords.LastPageIndex);
                    }
                }
            }

            var result = new FilePathResult(tempFileName, MimeHelper.CommaSeparatedValuesFile)
            {
                FileDownloadName =
                    string.Format(CsvImportTemplateOutputFileNameFormat, cardIndexSettings.Title,
                        _timeService.GetCurrentTime())
            };

            return result;
        }

        public FilePathResult GetExcelImportTemplate(CardIndexSettings cardIndexSettings, int importIndex, ImportTemplateType templateType)
        {
            var importSettings = cardIndexSettings.Imports[importIndex];
            var parameters = new GridParameters();
            var tempFileName = Path.GetTempFileName() + ExcelExtension;

            var dtoToViewModelMapping = _classMappingFactory.CreateMapping(importSettings.DtoType,
                importSettings.ViewModelType);

            var emptyModel = importSettings.ViewModelType.CreateInstance<object>();

            var headerLabels = GetImportableControls(emptyModel).SelectMany(cl => cl.GetImportTemplateColumnNames()).ToList();
            headerLabels.Insert(0, CardIndexResources.ImportServiceIdColumnText);

            using (CultureInfoHelper.SetCurrentCulture(CultureInfo.InvariantCulture))
            {
                using (var excelSerializer = new ExcelSerializer(tempFileName))
                {
                    using (var csvWriter = new CsvWriter(excelSerializer))
                    {
                        foreach (var controlLabel in headerLabels)
                        {
                            csvWriter.WriteField(controlLabel);
                        }

                        csvWriter.NextRecord();

                        if (templateType == ImportTemplateType.Edit)
                        {
                            CardIndexRecords<object> dataRecords;
                            parameters.CurrentPageIndex = 0;

                            do
                            {
                                dataRecords = GetRecords(importSettings, parameters, dtoToViewModelMapping, cardIndexSettings);

                                var viewModelRecords = dataRecords
                                    .Rows
                                    .Select(dtoToViewModelMapping.CreateFromSource)
                                    .ToList();

                                foreach (var row in viewModelRecords)
                                {
                                    if (importSettings.UseRecordId)
                                    {
                                        var value = importSettings.ViewModelType.GetProperty(IdColumnName).GetValue(row);
                                        csvWriter.WriteField(value ?? string.Empty);
                                    }

                                    foreach (var control in GetImportableControls(row))
                                    {
                                        foreach (var value in control.GetImportTemplateValues())
                                        {
                                            csvWriter.WriteField(value ?? string.Empty);
                                        }
                                    }

                                    csvWriter.NextRecord();
                                }

                                parameters.CurrentPageIndex++;
                            } while (dataRecords.CurrentPageIndex < dataRecords.LastPageIndex);
                        }
                    }
                }

                AdjustColumnWidths(tempFileName);

                var result = new FilePathResult(tempFileName, MimeHelper.OfficeDocumentExcel)
                {
                    FileDownloadName =
                        string.Format(ExcelImportTemplateOutputFileNameFormat, cardIndexSettings.Title,
                            _timeService.GetCurrentTime())
                };

                return result;
            }
        }

        public ImportRecordResult ImportFromCsv(Guid fileId, DataImportSettings importSettings)
        {
            var content = _temporaryDocumentService.GetContentStream(fileId);
            var tempFile = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

            using (var fileStream = File.Create(tempFile))
            {
                content.CopyTo(fileStream);
            }

            return ImportFromCsv(tempFile, importSettings);
        }

        public ImportRecordResult ImportFromCsv(string fileName, DataImportSettings importSettings)
        {
            var result = new ImportRecordResult
            {
                IsSuccessful = true,
            };

            var viewModelToDtoMapping = _classMappingFactory.CreateMapping(importSettings.ViewModelType,
                importSettings.DtoType);

            var emptyModel = importSettings.ViewModelType.CreateInstance<object>();
            var controls = GetImportableControls(emptyModel).ToList();

            using (var csv = new CsvReader(File.OpenText(fileName)))
            {
                while (csv.Read())
                {
                    var headers = csv.FieldHeaders;

                    if (!headers.Contains(IdColumnName))
                    {
                        var invalidFileContentAlert = new AlertDto
                        {
                            Message = ImportServiceResources.InvalidFileContent,
                            Type = AlertType.Error
                        };

                        result.IsSuccessful = false;
                        result.Alerts.Add(invalidFileContentAlert);

                        return result;
                    }

                    var record = csv.CurrentRecord;
                    var model = ImportRow(importSettings, result, controls, headers, record);
                    var dto = viewModelToDtoMapping.CreateFromSource(model);
                    var recordImportResult = importSettings.DataImportService.ImportRecord(dto, importSettings.ImportBehavior);

                    if (!recordImportResult.IsSuccessful)
                    {
                        result.IsSuccessful = false;

                        if (recordImportResult.UniqueKeyViolations != null)
                        {
                            result.UniqueKeyViolations =
                                result.UniqueKeyViolations?.Concat(recordImportResult.UniqueKeyViolations)
                                ?? recordImportResult.UniqueKeyViolations;
                        }
                    }

                    result.Alerts.AddRange(recordImportResult.Alerts);
                }
            }

            return result;
        }

        public object ImportRow(DataImportSettings importSettings, ImportRecordResult result, IList<ControlRenderingModel> controls, string[] headers, string[] record)
        {
            var emptyModel = importSettings.ViewModelType.CreateInstance<object>();

            for (var i = 0; i < record.Length; i++)
            {
                var header = headers[i];
                var value = record[i];

                var isIdColumn = header == IdColumnName && importSettings.UseRecordId;
                var control = controls.SingleOrDefault(c => c.Label == header);

                if (isIdColumn || (control != null && !control.IsReadOnly))
                {
                    var propertyName = isIdColumn ? IdColumnName : control.Name;
                    var property = importSettings.ViewModelType.GetProperty(propertyName);
                    property.SetValue(emptyModel, GetValue(value, property.PropertyType));
                }
                else
                {
                    result.IsSuccessful = false;
                    result.AddAlert(AlertType.Error, string.Format(CardIndexResources.ImportServiceMissingOrReadonlyFieldError, header));
                }
            }

            return emptyModel;
        }

        public ImportRecordResult ImportFromExcel(Guid fileId, DataImportSettings importSettings)
        {
            var content = _temporaryDocumentService.GetContentStream(fileId);
            var tempFile = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

            using (var fileStream = File.Create(tempFile))
            {
                content.CopyTo(fileStream);
            }

            return ImportFromExcel(tempFile, importSettings);
        }

        public ImportRecordResult ImportFromExcel(string fileName, DataImportSettings importSettings)
        {
            using (var excelSerializer = new ExcelParser(fileName))
            {
                var xlWorksheet = excelSerializer.Workbook.Worksheet(1);

                using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
                {
                    var result = ImportWorksheet(importSettings, xlWorksheet);

                    if (result.IsSuccessful)
                    {
                        var beforeBulkImportFinishedEventArgs = new BeforeBulkImportFinishedEventArgs(null);
                        importSettings.DataImportService.BeforeBulkImportFinished(beforeBulkImportFinishedEventArgs);

                        result.Alerts.AddRange(beforeBulkImportFinishedEventArgs.Alerts);

                        if (!beforeBulkImportFinishedEventArgs.Canceled)
                        {
                            transactionScope.Complete();
                        }
                    }

                    return result;
                }
            }
        }

        private ImportRecordResult ImportWorksheet(DataImportSettings importSettings, IXLWorksheet xlWorksheet)
        {
            var result = new ImportRecordResult
            {
                IsSuccessful = true,
            };

            var emptyModel = importSettings.ViewModelType.CreateInstance<object>();
            var controls = GetImportableControls(emptyModel).ToList();
            var firstMeaningColumn = importSettings.UseRecordId ? 2 : 1;
            var viewModelToDtoMapping = _classMappingFactory.CreateMapping(importSettings.ViewModelType,
                importSettings.DtoType);

            var row = 2;

            while (!xlWorksheet.Cell(row, firstMeaningColumn).IsEmpty())
            {
                var model = ImportRow(importSettings, xlWorksheet, controls, row, result);

                if (!result.IsSuccessful)
                {
                    break;
                }

                var dto = viewModelToDtoMapping.CreateFromSource(model);
                var recordImportResult = importSettings.DataImportService.ImportRecord(dto,
                    importSettings.ImportBehavior);

                if (!recordImportResult.IsSuccessful)
                {
                    result.IsSuccessful = false;

                    if (recordImportResult.UniqueKeyViolations != null)
                    {
                        result.UniqueKeyViolations =
                            result.UniqueKeyViolations?.Concat(recordImportResult.UniqueKeyViolations)
                            ?? recordImportResult.UniqueKeyViolations;
                    }
                }

                result.Alerts.AddRange(recordImportResult.Alerts);

                row++;
            }

            result.AddAlert(AlertType.Information,
                string.Format(CardIndexResources.ImportServiceProcessedRowsCountMessage, row - 2));

            return result;
        }

        private static object ImportRow(DataImportSettings importSettings, IXLWorksheet xlWorksheet,
            List<ControlRenderingModel> controls, int row, BoolResult result)
        {
            var model = importSettings.ViewModelType.CreateInstance<object>();
            var col = 1;

            while (!xlWorksheet.Cell(1, col).IsEmpty())
            {
                var columnName = xlWorksheet.Cell(1, col).GetString();
                var control = controls.FirstOrDefault(c => c.GetImportTemplateColumnNames().Contains(columnName));
                var isIdColumn = columnName == IdColumnName && importSettings.UseRecordId;

                if (col == 1 && !isIdColumn)
                {
                    var invalidFileContentAlert = new AlertDto
                    {
                        Message = ImportServiceResources.InvalidFileContent,
                        Type = AlertType.Error
                    };

                    result.IsSuccessful = false;
                    result.Alerts.Add(invalidFileContentAlert);

                    break;
                }

                if (isIdColumn || control != null && !control.IsReadOnly)
                {
                    string propertyName = isIdColumn ? IdColumnName : control.Name;
                    var property = importSettings.ViewModelType.GetProperty(propertyName);
                    Type propertyType = isIdColumn ? typeof(long) : property.PropertyType;
                    var value = GetCellValue(xlWorksheet, row, ref col, propertyType);
                    property.SetValue(model, value);
                }
                else
                {
                    result.IsSuccessful = false;
                    result.AddAlert(AlertType.Error,
                        string.Format(CardIndexResources.ImportServiceMissingOrReadonlyFieldError, columnName));
                }

                col++;
            }

            return model;
        }

        private static object GetValue(string value, Type type)
        {
            if (value.IsNullOrEmpty())
            {
                return null;
            }

            if (type.IsEnum)
            {
                if (TypeHelper.IsFlaggedEnum(type))
                {
                    return GetFlaggedEnum(type, value);
                }

                return Enum.Parse(type, value);
            }

            if (type == typeof(string) || typeof(LocalizedStringBase).IsAssignableFrom(type))
            {
                return value;
            }

            if (type == typeof(bool) || type == typeof(bool?))
            {
                return bool.Parse(value);
            }

            if (type == typeof(double) || type == typeof(double?))
            {
                return double.Parse(value);
            }

            if (type == typeof(DateTime) || type == typeof(DateTime?))
            {
                return DateTime.Parse(value);
            }

            if (type == typeof(int) || type == typeof(int?))
            {
                return int.Parse(value);
            }

            if (type == typeof(long) || type == typeof(long?))
            {
                return long.Parse(value);
            }

            if (type == typeof(decimal) || type == typeof(decimal?))
            {
                var pureText = value.Trim('\"');
                return decimal.Parse(pureText);
            }

            if (type == typeof(long[]))
            {
                return value.ParseCommaSeparatedList<long>().ToArray();
            }

            if (type == typeof(int[]))
            {
                return value.ParseCommaSeparatedList<int>().ToArray();
            }

            throw new NotImplementedException("Not implemented import for type: " + type.Name);
        }

        private static object GetFlaggedEnum(Type type, string value)
        {
            try
            {
                var flags = value.ParseCommaSeparatedList<string>().ToList();
                var flaggedEnum = EnumHelper.GetEnumValue(type, flags);

                return flaggedEnum;
            }
            catch (Exception exception)
            {
                throw new InvalidDataException("Could not convert the combination of flags to an enum value.", exception);
            }
        }

        private static object GetCellValue(IXLWorksheet xlWorksheet, int row, ref int col, Type type)
        {
            IXLCell cell;

            if (typeof(LocalizedStringBase).IsAssignableFrom(type))
            {
                var localizedString = type.CreateInstance<object>();

                foreach (var property in TypeHelper.GetPublicInstanceProperties(type))
                {
                    cell = xlWorksheet.Cell(row, col++);

                    property.SetValue(localizedString, GetValue(cell.GetString(), type));
                }

                --col;

                return localizedString;
            }

            cell = xlWorksheet.Cell(row, col);

            return GetValue(cell.GetString(), type);
        }

        private QueryCriteria GetQueryCriteria(DataImportSettings importSettings, GridParameters parameters,
            IClassMapping dtoToViewModelMapping, CardIndexSettings cardIndexSettings)
        {
            const string criteriaMethodName = "ToCriteria";

            var toCriteriaMethod = typeof(GridParameters).GetMethod(criteriaMethodName);

            var genericMethod = toCriteriaMethod.MakeGenericMethod(importSettings.ViewModelType, importSettings.DtoType);

            var queryCriteria = (QueryCriteria)genericMethod
                .Invoke(parameters, new object[]
                {
                    dtoToViewModelMapping,
                    _anonymizationService,
                    _principalProvider.Current.Roles,
                    cardIndexSettings.FilterGroups
                });

            return queryCriteria;
        }

        private CardIndexRecords<object> GetRecords(DataImportSettings importSettings, GridParameters parameters, IClassMapping dtoToViewModelMapping, CardIndexSettings cardIndexSettings)
        {
            var queryCriteria = GetQueryCriteria(importSettings, parameters, dtoToViewModelMapping, cardIndexSettings);

            var dataImportService = importSettings.DataImportService;
            var cardIndexRecords = dataImportService.GetRecordsAsObjects(queryCriteria);

            return cardIndexRecords;
        }
    }
}