﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Presentation.Renderers.TreeView
{
    public interface IExpandingGridController
    {
        void PrepareForGridExpanding();
    }
}
