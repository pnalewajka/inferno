﻿using System;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap
{
    public partial class Angular
    {
        private readonly Bootstrap _bootstrap;

        public Angular(Bootstrap bootstrap)
        {
            _bootstrap = bootstrap;

            InitializeTypeSpecificBindings();
        }

        public MvcHtmlString DisplayForModel(object model, Layout layout = null, Action<FormRenderingModel> formModelCustomization = null)
        {
            var formRenderingModel = _bootstrap.BuildFormRenderingModel(model, layout);

            InitializeDefaultBindings(formRenderingModel);

            return _bootstrap.DisplayForModel(formRenderingModel, layout, formModelCustomization);
        }

        public MvcHtmlString EditorForModel(object model, Layout layout = null, Action<FormRenderingModel> formModelCustomization = null)
        {
            var formRenderingModel = _bootstrap.BuildFormRenderingModel(model, layout);

            InitializeDefaultBindings(formRenderingModel);

            return _bootstrap.EditorForModel(formRenderingModel, layout, formModelCustomization);
        }
    }
}