﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts
{
    public class HiddenLayout : Layout
    {
        public override FormRenderingModel BuildRenderingModels(object formModel, ModelBuilderContext context,
            HashSet<string> currentUserRoles)
        {
            if (formModel == null)
            {
                throw new ArgumentNullException(nameof(formModel));
            }

            var type = formModel.GetType();
            var propertyInfos = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

            var controls = new List<ControlRenderingModel>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var propertyInfo in propertyInfos)
            {
                var readOnlySteeringProperty = ViewModelPropertyHelper.IsReadOnlyControlProperty(propertyInfo, propertyInfos);
                var visibility = ViewModelPropertyHelper.ShouldBeVisible(propertyInfo, VisibilityScope.Form);

                var propertyAccessMode = SecurityHelper.GetPropertyAccessMode(propertyInfo, currentUserRoles);

                // User has no access to field
                if (propertyAccessMode == PropertyAccessMode.NoAccess)
                {
                    visibility = ControlVisibility.Remove;
                }

                if (!readOnlySteeringProperty)
                {
                    if (visibility == ControlVisibility.Show || visibility == ControlVisibility.Hide)
                    {
                        var controlRenderingModel = HiddenInputRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, true);
                        controls.Add(controlRenderingModel);
                    }
                }
            }

            return new FormRenderingModel(controls, formModel);
        }
    }
}
