﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts
{
    public class SimpleLayout : Layout
    {
        public override FormRenderingModel BuildRenderingModels(object formModel, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var formRenderingModel = base.BuildRenderingModels(formModel, context, currentUserRoles);

            return new FormRenderingModel(formRenderingModel.Controls.OrderBy(m => m.Order), formModel, formRenderingModel.Scripts);
        }
    }
}