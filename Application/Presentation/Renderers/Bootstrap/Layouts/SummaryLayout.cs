using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts
{
    public class SummaryLayout : HorizontalFormLayout
    {
        private readonly byte _layoutColumns;
        private readonly byte _layoutColumnWidth;

        /// <summary>
        /// Creates summary layout, suitable for context display
        /// </summary>
        /// <param name="layoutColumns">How many columns should the form be split into?</param>
        /// <param name="labelWidth">How wide should the label be (in Bootstrap columns)?</param>
        public SummaryLayout(byte layoutColumns = 3, byte labelWidth = 2)
            : base(labelWidth)
        {
            _layoutColumns = layoutColumns;
            _layoutColumnWidth = (byte)(BootstrapGridWidth / layoutColumns);

            if (_layoutColumnWidth <= labelWidth)
            {
                throw new ArgumentOutOfRangeException(nameof(labelWidth));
            }
        }

        public override bool IsReadOnly()
        {
            return true;
        }

        protected override string GetTemplateVersion()
        {
            return "Summary";
        }

        protected override byte GetControlWidth(ControlRenderingModel controlRenderingModel)
        {
            return controlRenderingModel.Size == Size.Large
                       ? (byte)(BootstrapGridWidth - LabelWidth)
                       : (byte)(_layoutColumnWidth - LabelWidth);
        }

        protected override byte GetHintWidth(ControlRenderingModel controlRenderingModel)
        {
            return 0;
        }

        protected override void RenderControlBlock(StringBuilder html, IEnumerable<ControlRenderingModel> controls)
        {
            var summaryControls = controls
                .Where(c => c.Visibility != ControlVisibility.Remove
                            && c.CanUseInSummary)
                .ToList();

            var hiddenControls = summaryControls
                .Where(c => c.Visibility == ControlVisibility.Hide)
                .ToList();

            foreach (var hiddenControl in hiddenControls)
            {
                var controlHtml = RenderControl(hiddenControl);
                html.AppendLine(controlHtml);
            }

            var visibleControls = summaryControls
                .Where(c => c.Visibility == ControlVisibility.Show)
                .ToList();

            var rows = CalculateRows(visibleControls);

            foreach (var rowControls in rows)
            {
                html.AppendLine("<div class=\"row\">");

                foreach (var controlRenderingModel in rowControls)
                {
                    html.AppendLine(RenderControl(controlRenderingModel));
                }

                html.AppendLine("</div>");
            }
        }

        public override FormRenderingModel BuildRenderingModels(object formModel, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var formRenderingModel = base.BuildRenderingModels(formModel, context, currentUserRoles);

            foreach (var control in formRenderingModel.Controls)
            {
                control.LabelOffsetClass += " col-vs-offset-3";
                control.LabelWidthClass += " col-vs-3";
                control.ControlWidthClass += " col-vs-9";

                var repeater = control as RepeaterRenderingModel;

                if (repeater != null)
                {
                    repeater.Layout = this;
                }
            }

            return formRenderingModel;
        }

        private IEnumerable<IEnumerable<ControlRenderingModel>> CalculateRows(IEnumerable<ControlRenderingModel> controls)
        {
            var row = new List<ControlRenderingModel>();
            var usedColumns = 0;

            foreach (var control in controls)
            {
                var controlTotalWidth = control.LabelWidth + control.ControlWidth;

                if (usedColumns + controlTotalWidth > BootstrapGridWidth || row.Count == _layoutColumns)
                {
                    yield return row;

                    row = new List<ControlRenderingModel>();
                    usedColumns = 0;
                }

                row.Add(control);
                usedColumns += controlTotalWidth;
            }

            yield return row;
        }
    }
}