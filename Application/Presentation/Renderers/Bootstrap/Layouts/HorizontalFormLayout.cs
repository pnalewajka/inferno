using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts
{
    public class HorizontalFormLayout : SimpleLayout
    {
        protected readonly byte LabelWidth;

        public HorizontalFormLayout(byte labelWidth = (byte)Enums.LabelWidth.Default)
        {
            if (labelWidth >= BootstrapGridWidth)
            {
                throw new ArgumentOutOfRangeException(nameof(labelWidth));
            }

            LabelWidth = labelWidth;
        }

        protected override string GetTemplateVersion()
        {
            return "Horizontal";
        }

        protected override byte GetLabelWidth(ControlRenderingModel controlRenderingModel)
        {
            return LabelWidth;
        }

        protected override byte GetControlWidth(ControlRenderingModel controlRenderingModel)
        {
            return (byte)Math.Min(BootstrapGridWidth - LabelWidth, base.GetControlWidth(controlRenderingModel));
        }

        protected override byte GetHintWidth(ControlRenderingModel controlRenderingModel)
        {
            return (byte)(BootstrapGridWidth - LabelWidth);
        }
    }
}