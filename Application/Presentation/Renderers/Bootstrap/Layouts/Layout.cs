using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Checkbox;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CodeEditor;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DocumentPicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.NumberControl;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RichTextEditor;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textbox;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Timestamp;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts
{
    public abstract class Layout
    {
        public const byte SmallControlWidth = 2;
        public const byte MediumControlWidth = 5;
        public const byte LargeControlWidth = 12;

        public const byte BootstrapGridWidth = 12;
        public const string BootstrapDefaultSizeInfix = "sm";

        public virtual FormRenderingModel BuildRenderingModels(object formModel, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            if (formModel == null)
            {
                throw new ArgumentNullException(nameof(formModel));
            }

            var type = formModel.GetType();
            var propertyInfos = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

            var controls = new List<ControlRenderingModel>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var propertyInfo in propertyInfos)
            {
                var readOnlySteeringProperty = ViewModelPropertyHelper.IsReadOnlyControlProperty(propertyInfo, propertyInfos);
                var visibility = ShouldBeVisible(currentUserRoles, propertyInfo);

                if (!readOnlySteeringProperty)
                {
                    var controlRenderingModel = BuildControlRenderingModel(propertyInfo, formModel, visibility, context, currentUserRoles);

                    if (controlRenderingModel != null)
                    {
                        controls.Add(controlRenderingModel);
                    }
                }
            }

            var scriptAttributes = AttributeHelper.GetClassAttributes<ScriptAttribute>(formModel);

            return new FormRenderingModel(controls, formModel, scriptAttributes.Select(a => a.ScriptPath));
        }

        public virtual bool IsReadOnly()
        {
            return false;
        }

        public ControlRenderingModel BuildControlRenderingModel(PropertyInfo propertyInfo, object formModel, ControlVisibility visibility, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            if (visibility == ControlVisibility.Show)
            {
                var controlRenderingModelBuilder = new ControlRenderingModelBuilder();
                var controlRenderingModel = controlRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, context, currentUserRoles);
                controlRenderingModel.LabelWidth = GetLabelWidth(controlRenderingModel);
                controlRenderingModel.ControlWidth = GetControlWidth(controlRenderingModel);
                controlRenderingModel.HintWidth = GetHintWidth(controlRenderingModel);

                return controlRenderingModel;
            }
            else if (visibility == ControlVisibility.Hide)
            {
                var controlRenderingModel = HiddenInputRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, true);

                return controlRenderingModel;
            }
            else if (visibility == ControlVisibility.Remove)
            {
                var controlRenderingModel = HiddenInputRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, true);
                controlRenderingModel.Visibility = visibility;

                return controlRenderingModel;
            }

            return null;
        }

        protected ControlVisibility ShouldBeVisible(HashSet<string> currentUserRoles, PropertyInfo propertyInfo)
        {
            var visibility = ViewModelPropertyHelper.ShouldBeVisible(propertyInfo, VisibilityScope.Form);

            var propertyAccessMode = SecurityHelper.GetPropertyAccessMode(propertyInfo, currentUserRoles);

            // User has no access to field
            if (propertyAccessMode == PropertyAccessMode.NoAccess)
            {
                visibility = ControlVisibility.Remove;
            }

            return visibility;
        }

        public MvcHtmlString DisplayForModel(FormRenderingModel formRenderingModel)
        {
            var models = formRenderingModel.Controls as ControlRenderingModel[] ?? formRenderingModel.Controls.ToArray();

            foreach (var controlRenderingModel in models)
            {
                controlRenderingModel.IsReadOnly = true;
            }

            return EditorForModel(formRenderingModel);
        }

        public MvcHtmlString EditorForModel(FormRenderingModel formRenderingModel)
        {
            var html = new StringBuilder();
            var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

            foreach (var script in formRenderingModel.Scripts)
            {
                html.AppendFormat("<script src=\"{0}\"></script>", url.Content(script));
            }

            RenderContent(formRenderingModel.Controls, html);

            return new MvcHtmlString(html.ToString());
        }

        protected internal virtual void RenderContent(IEnumerable<ControlRenderingModel> controlRenderingModels, StringBuilder html)
        {
            var tabControlGroups = controlRenderingModels
                .GroupBy(m => m.TabId)
                .OrderBy(g => g.First().TabOrder)
                .ToList();

            var noTabGroup = tabControlGroups.SingleOrDefault(g => g.Key == null);
            var tabGroups = tabControlGroups.Where(g => g.Key != null).ToList();

            if (noTabGroup != null)
            {
                RenderFieldSets(html, noTabGroup);
            }

            if (tabGroups.Count == 1)
            {
                RenderFieldSets(html, tabGroups[0]);
            }
            else if (tabGroups.Count > 1)
            {
                RenderTabHeaders(html, tabGroups);
                RenderTabPanes(html, tabGroups);
            }
        }

        private void RenderTabPanes(StringBuilder html, IList<IGrouping<string, ControlRenderingModel>> tabGroups)
        {
            html.Append("<div class=\"tab-content\">");

            for (int index = 0; index < tabGroups.Count; index++)
            {
                var tabControlGroup = tabGroups[index];
                var tabId = tabControlGroup.Key;
                var classes = index == 0 ? "tab-pane active" : "tab-pane";

                html.AppendFormat("<div role=\"tabpanel\" id=\"{0}\" class=\"{1}\">", tabId, classes);
                RenderFieldSets(html, tabControlGroup);
                html.AppendFormat("</div>");
            }

            html.Append("</div>"); //tab-content
        }

        private static void RenderTabHeaders(StringBuilder html, IList<IGrouping<string, ControlRenderingModel>> tabGroups)
        {
            html.Append("<ul class=\"nav nav-tabs\" role=\"tablist\">");

            for (var index = 0; index < tabGroups.Count; index++)
            {
                var tabGroup = tabGroups[index];

                if (tabGroup.Any(t => t.Visibility != ControlVisibility.Remove))
                {
                    var tabId = tabGroup.Key;
                    var tabName = tabGroup.First().TabName;
                    var classes = index == 0 ? " class=\"active\"" : string.Empty;

                    var tabTextMarkup = HtmlMarkupHelper.HighlightAccelerator(tabName);
                    var accelerator = HtmlMarkupHelper.GetAccelerator(tabName);

                    var acceleratorInsert = string.IsNullOrWhiteSpace(accelerator)
                        ? string.Empty
                        : $" accesskey=\"{accelerator}\"";

                    html.AppendFormat(
                        "<li role=\"presentation\"{2}><a href=\"#{0}\" aria-controls=\"{0}\" role=\"tab\" data-toggle=\"tab\"{3}>{1}</a></li>",
                        tabId, tabTextMarkup, classes, acceleratorInsert);
                }
            }

            html.Append("</ul>");
        }

        protected virtual void RenderFieldSets(StringBuilder html, IEnumerable<ControlRenderingModel> models)
        {
            var fieldSetGroups = models
                .GroupBy(m => m.FieldSetId)
                .OrderBy(m => m.First().FieldSetOrder)
                .ToList();

            var hasFieldSet = fieldSetGroups.Count > 1;

            if (hasFieldSet)
            {
                foreach (var fieldSetGroup in fieldSetGroups)
                {
                    var firstControl = fieldSetGroup.First();
                    var legend = firstControl.Legend;
                    var fieldSetId = firstControl.FieldSetId;

                    if (string.IsNullOrWhiteSpace(fieldSetId))
                    {
                        html.Append("<fieldset>");
                    }
                    else
                    {
                        html.AppendFormat("<fieldset id='{0}'>", fieldSetId);
                    }

                    if (legend != null)
                    {
                        html.AppendFormat("<legend>{0}</legend>", legend);
                    }

                    RenderControlBlock(html, fieldSetGroup);
                    html.Append("</fieldset>");
                }
            }
            else
            {
                var firstGroup = fieldSetGroups[0];
                RenderControlBlock(html, firstGroup);
            }
        }

        protected virtual void RenderControlBlock(StringBuilder html, IEnumerable<ControlRenderingModel> models)
        {
            foreach (var controlRenderingModel in models)
            {
                var controlHtml = RenderControl(controlRenderingModel);
                html.AppendLine(controlHtml);
            }
        }

        protected string RenderControl(ControlRenderingModel model)
        {
            if (model.Visibility == ControlVisibility.Remove)
            {
                return string.Empty;
            }

            var templateName = GetTemplateName(model);
            var controlHtml = RazorTemplateHelper.ParseControlTemplate(model, templateName, GetTemplateVersion());

            return controlHtml;
        }

        protected virtual string GetTemplateVersion()
        {
            return null;
        }

        protected virtual byte GetLabelWidth(ControlRenderingModel controlRenderingModel)
        {
            return BootstrapGridWidth;
        }

        protected virtual byte GetControlWidth(ControlRenderingModel controlRenderingModel)
        {
            switch (controlRenderingModel.Size)
            {
                case Size.Small:
                    return SmallControlWidth;

                case Size.Medium:
                    return MediumControlWidth;

                case Size.Large:
                    return LargeControlWidth;

                case Size.Default:
                    return BootstrapGridWidth;
            }

            throw new InvalidEnumArgumentException(@"Invalid Size specified for rendering model.");
        }

        protected virtual byte GetHintWidth(ControlRenderingModel controlRenderingModel)
        {
            return GetControlWidth(controlRenderingModel);
        }

        private string GetTemplateName(ControlRenderingModel controlRenderingModel)
        {
            var modelType = controlRenderingModel.GetType();

            if (controlRenderingModel == null)
            {
                throw new ArgumentNullException(nameof(controlRenderingModel));
            }

            var controlName = GetMainTemplateName(modelType);
            var templateVersion = GetTemplateVersion();

            var viewName = string.IsNullOrWhiteSpace(templateVersion)
                ? $"{controlName}.cshtml"
                : $"{controlName}.{templateVersion}.cshtml";

            return viewName;
        }

        private static string GetMainTemplateName(Type modelType)
        {
            return modelType.Name.Replace("RenderingModel", string.Empty);
        }

        internal string EditorFor<TViewModel>(TViewModel model, Expression<Func<TViewModel, object>> propertyExpression, ModelBuilderContext context, HashSet<string> currentUserRoles, object inputTagAttributes)
        {
            var propertyInfo = PropertyHelper.GetProperty(propertyExpression);
            var visibility = ShouldBeVisible(currentUserRoles, propertyInfo);
            var controlRenderingModel = BuildControlRenderingModel(propertyInfo, model, visibility, context, currentUserRoles);

            return EditorFor(model, controlRenderingModel, context, currentUserRoles, inputTagAttributes);
        }

        internal string EditorFor<TViewModel>(TViewModel model, string valuePropertyName, ModelBuilderContext context, HashSet<string> currentUserRoles, object inputTagAttributes)
        {
            var propertyInfo = model.GetType().GetProperties()
                    .SingleOrDefault(x => x.Name.Equals(valuePropertyName, StringComparison.InvariantCultureIgnoreCase));
            var visibility = ShouldBeVisible(currentUserRoles, propertyInfo);
            var controlRenderingModel = BuildControlRenderingModel(propertyInfo, model, visibility, context, currentUserRoles);

            return EditorFor(model, controlRenderingModel, context, currentUserRoles, inputTagAttributes);
        }

        private string EditorFor<TViewModel>(TViewModel model, ControlRenderingModel controlRenderingModel, ModelBuilderContext context, HashSet<string> currentUserRoles, object inputTagAttributes)
        {
            if (inputTagAttributes != null)
            {
                var attributes = DictionaryHelper.ToDictionary(inputTagAttributes, NamingConventionHelper.ConvertPascalCaseToHyphenated, v => v);

                foreach (var attribute in attributes)
                {
                    controlRenderingModel.CustomInputTagAttributes.Add(attribute.Key, attribute.Value);
                }
            }

            return RenderControl(controlRenderingModel);
        }

        internal string LabelFor<TViewModel>(TViewModel model, Expression<Func<TViewModel, object>> propertyExpression, ModelBuilderContext context, HashSet<string> currentUserRoles, object labelTagAttributes)
        {
            var controlRenderingModelBuilder = new ControlRenderingModelBuilder();
            var propertyInfo = PropertyHelper.GetProperty(propertyExpression);
            var controlRenderingModel = controlRenderingModelBuilder.GetModelForProperty(
                propertyInfo, model, context, currentUserRoles);

            if (labelTagAttributes != null)
            {
                var attributes = DictionaryHelper.ToDictionary(labelTagAttributes, NamingConventionHelper.ConvertPascalCaseToHyphenated, v => v);

                foreach (var attribute in attributes)
                {
                    controlRenderingModel.CustomInputTagAttributes.Add(attribute.Key, attribute.Value);
                }
            }

            const string labelFormat = "<label class=\"{0}\" for=\"{1}\" {2}>{3}</label>";

            var cssClass = controlRenderingModel.GetRequiredMarkerCss();
            var forId = controlRenderingModel.Id;
            var tagAttributes = controlRenderingModel.GetLabelTagAttributes();
            var innerMarkup = controlRenderingModel.GetLabelTextMarkup();

            var labelMarkup = string.Format(labelFormat, cssClass, forId, tagAttributes, innerMarkup);

            return labelMarkup;
        }

        public static void CompileTemplates()
        {
            var controlRenderingModels = new ControlRenderingModel[]
            {
                new CheckboxRenderingModel(),
                new CodeEditorRenderingModel(),
                new DatePickerRenderingModel(),
                new DocumentPickerRenderingModel(),
                new DropdownRenderingModel(),
                new HiddenInputRenderingModel(),
                new NumberControlRenderingModel(),
                new RichTextEditorRenderingModel(),
                new TextareaRenderingModel(),
                new TextboxRenderingModel(),
                new TimestampRenderingModel(),
                new ValuePickerRenderingModel(),
                new RepeaterRenderingModel
                {
                    Items = new RepeaterItem[]{},
                    TemplateItem = new RepeaterItem(string.Empty, new List<ControlRenderingModel>()),
                    Layout = new SimpleLayout()
                }
            };

            foreach (var model in controlRenderingModels)
            {
                model.Label = string.Empty;
                model.Errors = new ModelErrorCollection();

                ControlRenderingModel renderingModel = model;

                Task.Run(() =>
                {
                    new HorizontalFormLayout().RenderControl(renderingModel);
                    new SummaryLayout().RenderControl(renderingModel);
                    new SimpleLayout().RenderControl(renderingModel);
                    new InlineLayout().RenderControl(renderingModel);
                });
            }
        }
    }
}