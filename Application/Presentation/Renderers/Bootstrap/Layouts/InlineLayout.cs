using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts
{
    public class InlineLayout : SimpleLayout
    {
        protected override string GetTemplateVersion()
        {
            return "Inline";
        }

        protected override byte GetHintWidth(ControlRenderingModel controlRenderingModel)
        {
            return 0;
        }
    }
}