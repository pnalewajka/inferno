using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.Presentation.Renderers.Models;
using Smt.Atomic.Presentation.Renderers.Models.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap
{
    public partial class Bootstrap
    {
        public FormRenderingModel BuildFormRenderingModel(object model, Layout layout = null)
        {
            if (layout == null)
            {
                layout = LayoutAttribute.GetLayoutForModel(model);
            }

            var formRenderingModel = InitializeFormRenderingModel(model, layout);

            return formRenderingModel;
        }

        public MvcHtmlString DisplayForModel(object model, Layout layout = null, Action<FormRenderingModel> formModelCustomization = null)
        {
            if (layout == null)
            {
                layout = LayoutAttribute.GetLayoutForModel(model);
            }

            var formRenderingModel = InitializeFormRenderingModel(model, layout);

            return DisplayForModel(formRenderingModel, layout, formModelCustomization);
        }

        public MvcHtmlString DisplayForModel(FormRenderingModel formRenderingModel, Layout layout, Action<FormRenderingModel> formModelCustomization = null)
        {
            formModelCustomization?.Invoke(formRenderingModel);

            return layout.DisplayForModel(formRenderingModel);
        }

        public MvcHtmlString EditorForModel(object model, Layout layout = null, Action<FormRenderingModel> formModelCustomization = null)
        {
            if (layout == null)
            {
                layout = LayoutAttribute.GetLayoutForModel(model);
            }

            var formRenderingModel = InitializeFormRenderingModel(model, layout);

            return EditorForModel(formRenderingModel, layout, formModelCustomization);
        }

        private FormRenderingModel InitializeFormRenderingModel(object model, Layout layout)
        {
            var dynamicViewModel = model as DynamicViewModel;

            if (dynamicViewModel != null)
            {
                dynamicViewModel.BuildRenderingModels(new ModelBuilderContext(layout, _modelState), _currentUserRoles);

                return dynamicViewModel;
            }
            else
            {
                return layout.BuildRenderingModels(model, new ModelBuilderContext(layout, _modelState), _currentUserRoles);
            }
        }

        public MvcHtmlString HiddenForModel(object model)
        {
            var layout = new HiddenLayout();
            var context = new ModelBuilderContext(layout, _modelState);
            FormRenderingModel formRenderingModel = InitializeFormRenderingModel(model, layout);

            return layout.EditorForModel(formRenderingModel);
        }

        public MvcHtmlString EditorForModel(FormRenderingModel formRenderingModel, Layout layout, Action<FormRenderingModel> formModelCustomization)
        {
            formModelCustomization?.Invoke(formRenderingModel);

            return layout.EditorForModel(formRenderingModel);
        }

        public IEnumerable<string> GetProtectedFields(object model, Action<FormRenderingModel> formModelCustomization = null)
        {
            var layout = LayoutAttribute.GetLayoutForModel(model);
            FormRenderingModel formRenderingModel = InitializeFormRenderingModel(model, layout);

            formModelCustomization?.Invoke(formRenderingModel);

            return formRenderingModel.Controls
                .Where(renderingModel => renderingModel.IsReadOnly || renderingModel.Visibility == ControlVisibility.Remove)
                .Select(renderingModel => renderingModel.Name);
        }

        /// <summary>
        /// User has sufficient rights to read value of specific property
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public bool CanReadProperty<TViewModel>(Expression<Func<TViewModel, object>> expression)
        {
            var propertyInfo = PropertyHelper.GetProperty(expression);
            var accessMode = SecurityHelper.GetPropertyAccessMode(propertyInfo, _currentUserRoles);

            return accessMode == PropertyAccessMode.ReadWrite || accessMode == PropertyAccessMode.ReadOnly;
        }

        /// <summary>
        /// User has sufficient rights to write value of specific property
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public bool CanWriteProperty<TViewModel>(Expression<Func<TViewModel, object>> expression)
        {
            var propertyInfo = PropertyHelper.GetProperty(expression);
            var accessMode = SecurityHelper.GetPropertyAccessMode(propertyInfo, _currentUserRoles);

            return accessMode == PropertyAccessMode.ReadWrite;
        }

    }
}