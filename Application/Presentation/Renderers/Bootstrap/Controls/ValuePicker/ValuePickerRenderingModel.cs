using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker
{
    public class ValuePickerRenderingModel : ControlRenderingModel
    {
        public IList<ListItem> Items { get; set; }

        public bool IsMultiValue { get; set; }
        public bool IsClearButtonAvailable { get; set; }
        public string ValuePickerTypeIdentifier { get; set; }
        public string ClientStorageKey { get; set; }
        public string OnResolveUrlJavaScript { get; set; }
        public string OnSelectionChangedJavaScript { get; set; }
        public string DialogWidth { get; set; }
        public bool HasHub { get; set; }

        public List<long> Ids
        {
            get
            {
                return Items.Select(i => i.Id)
                    .Cast<long>()
                    .ToList();
            }
        }
        
        public List<string> DisplayNames
        {
            get
            {
                return Items
                    .Select(i => i.DisplayName)
                    .ToList();
            }
        }

        public string GetDisplayName()
        {
            return string.Join(", ", DisplayNames);
        }
        
        public string GetMultiValueClass()
        {
            return IsMultiValue ? "is-multivalue" : string.Empty;
        }

        public string PickerButtonClass => IsReadOnly ? "disabled-icon" : string.Empty;

        public bool IsNoValueAllowed => IsMultiValue || IsClearButtonAvailable;

        public ValuePickerRenderingModel()
        {
            Items = new List<ListItem>();
        }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Remove("id");
            attributes.Remove("class");

            attributes.Add("type", "hidden");
            attributes.Add("class", "hidden-but-validate");
            attributes.Add("value", string.Join(",", Ids));
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return string.Join(",", Ids);
        }
    }
}