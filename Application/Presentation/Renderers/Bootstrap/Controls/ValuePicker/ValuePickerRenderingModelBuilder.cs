using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker
{
    public class ValuePickerRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var valuePickerAttribute = AttributeHelper.GetPropertyAttribute<ValuePickerAttribute>(propertyInfo);
            string[] displayNames = null;

            var hubAttribute = AttributeHelper.GetPropertyAttribute<HubAttribute>(propertyInfo);

            if (valuePickerAttribute == null && propertyInfo.PropertyType == typeof(string[]))
            {
                displayNames = (string[])propertyInfo.GetValue(formModel);
                propertyInfo = formModel.GetType().GetProperty(hubAttribute.PickerPropertyName);
                valuePickerAttribute = AttributeHelper.GetPropertyAttribute<ValuePickerAttribute>(propertyInfo);
            }

            var propertyType = propertyInfo.PropertyType;

            if (valuePickerAttribute == null
                || (propertyType != typeof(long)
                && propertyType != typeof(long?)
                && propertyType != typeof(IList<long>)
                && propertyType != typeof(List<long>)
                && propertyType != typeof(long[])))
            {
                return null;
            }

            var ids = GetSelectedIds(propertyInfo, formModel, propertyType);
            var valuePickerType = valuePickerAttribute.Type;
            var listItems = hubAttribute == null
                ? GetListItems(valuePickerAttribute, ids)
                : GetListItems(ids, displayNames);

            var isMultiValue = typeof(System.Collections.IEnumerable).IsAssignableFrom(propertyType);
            var dialogWidth = GetDialogWidth(valuePickerAttribute);

            var renderingModel = new ValuePickerRenderingModel
            {
                Items = listItems,
                IsMultiValue = isMultiValue,
                IsClearButtonAvailable = TypeHelper.IsNullable(propertyType) && !valuePickerAttribute.HideClearButton,
                ValuePickerTypeIdentifier = valuePickerAttribute.TypeIdentifier,
                Size = isMultiValue ? Size.Large : Size.Medium,
                OnResolveUrlJavaScript = valuePickerAttribute.OnResolveUrlJavaScript,
                OnSelectionChangedJavaScript = valuePickerAttribute.OnSelectionChangedJavaScript,
                DialogWidth = dialogWidth,
                ClientStorageKey = valuePickerAttribute.ClientStorageKey,
                HasHub = valuePickerAttribute.HasHub,
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }

        private static string GetDialogWidth(ValuePickerAttribute valuePickerAttribute)
        {
            string dialogWidth = null;
            var defaultsAttribute = AttributeHelper.GetClassAttribute<ValuePickerDefaultsAttribute>(valuePickerAttribute.Type);

            if (defaultsAttribute != null)
            {
                dialogWidth = defaultsAttribute.DialogWidth;
            }

            if (!string.IsNullOrWhiteSpace(valuePickerAttribute.DialogWidth))
            {
                dialogWidth = valuePickerAttribute.DialogWidth;
            }

            return dialogWidth;
        }

        private static IList<ListItem> GetListItems(IEnumerable<long> ids, string[] displayNames)
        {
            return ids.WithIndex().Select(e => new ListItem
            {
                Id = e.Item,
                DisplayName = displayNames[e.Index]
            }).ToList();
        }

        private static IList<ListItem> GetListItems(ValuePickerAttribute valuePickerAttribute, List<long> ids)
        {
            if (ids == null || ids.Count == 0)
            {
                return new List<ListItem>();
            }

            var controller = (IValuePicker)ReflectionHelper.CreateInstanceWithIocDependencies(valuePickerAttribute.Type);
            var listItems = controller.GetListItems(ids.ToArray(), valuePickerAttribute.FormFormatterType);

            return listItems;
        }

        private static List<long> GetSelectedIds(PropertyInfo propertyInfo, object formModel, Type propertyType)
        {
            var ids = new List<long>();

            if (propertyType == typeof(long))
            {
                var value = (long)propertyInfo.GetValue(formModel);

                if (value != 0)
                {
                    ids.Add(value);
                }
            }

            if (propertyType == typeof(long?))
            {
                var value = (long?)propertyInfo.GetValue(formModel);

                if (value.HasValue)
                {
                    ids.Add(value.Value);
                }
            }

            if (propertyType == typeof(long[]))
            {
                var array = (long[])propertyInfo.GetValue(formModel);

                if (array != null)
                {
                    ids.AddRange(array);
                }
            }

            if (propertyType == typeof(IList<long>) || propertyType == typeof(List<long>))
            {
                var list = (IList<long>)propertyInfo.GetValue(formModel);

                if (list != null)
                {
                    ids.AddRange(list);
                }
            }

            return ids;
        }
    }
}