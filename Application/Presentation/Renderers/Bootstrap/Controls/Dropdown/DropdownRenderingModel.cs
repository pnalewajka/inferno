using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown
{
    public class DropdownRenderingModel : ControlRenderingModel
    {
        public long? Value { get; set; }

        public EmptyItemBehavior EmptyItemBehavior  { get; set; }

        public IList<ListItem> Items { get; set; }

        public bool HasSelectedItem
        {
            get
            {
                return OptionItems.Any(i => Value == i.Id);
            }
        }

        public IList<ListItem> OptionItems
        {
            get
            {
                var items = Items;

                if (EmptyItemBehavior == EmptyItemBehavior.PresentOnlyOnInit)
                {
                    items = items.Where(i => i.Id.HasValue).ToList();
                }

                return items;
            }
        }

        public string DisplayValue
        {
            get
            {
                if (Value == null)
                {
                    return string.Empty;
                }

                var selectedItem = Items.SingleOrDefault(i => i.Id == Value.Value);

                return selectedItem == null ? string.Empty : selectedItem.DisplayName;
            }
        }

        public DropdownRenderingModel()
        {
            Items = new ListItem[] { };
        }

        public string GetNothingSelectedText()
        {
            if (!HasSelectedItem && EmptyItemBehavior == EmptyItemBehavior.PresentOnlyOnInit)
            {
                return Items.Single(i => i.Id == null).DisplayName;
            }

            return null;
        }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            if (IsRequired)
            {
                attributes.Add("data-val-required", RequiredValueErrorMessage);
            }

            if (!IsReadOnly)
            {
                attributes.Add("class", "selectpicker", TagAttributeDuplicateBehavior.Merge);
            }
        }

        protected override void AddReadOnlyAttributes(TagAttributes attributes)
        {
            attributes.Add("value", Value);
            attributes.Add("type", "hidden");
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}