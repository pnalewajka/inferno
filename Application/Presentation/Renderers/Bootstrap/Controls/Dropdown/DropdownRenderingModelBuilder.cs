using System;
using System.Reflection;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown
{
    public class DropdownRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var propertyType = propertyInfo.PropertyType;
            
            var isEnumOrNullableEnum = propertyType.IsEnum || (TypeHelper.IsSubclassOfRawGeneric(propertyType, typeof(Nullable<>)) && propertyType.GenericTypeArguments[0].IsEnum);

            var dropDownAttribute = AttributeHelper.GetPropertyAttribute<DropdownAttribute>(propertyInfo);

            if (dropDownAttribute == null && isEnumOrNullableEnum)
            {
                var isNullable = TypeHelper.IsSubclassOfRawGeneric(propertyType, typeof (Nullable<>));
                var enumType = isNullable
                    ? TypeHelper.GetRawGenericArguments(propertyType, typeof (Nullable<>))[0]
                    : propertyType;
                var emptyItemBehavior = isNullable ? EmptyItemBehavior.AlwaysPresent : EmptyItemBehavior.PresentOnlyOnInit;

                var providerType = typeof(EnumBasedListItemProvider<>).MakeGenericType(enumType);

                dropDownAttribute = new DropdownAttribute
                {
                    ItemProvider = providerType,
                    EmptyItemBehavior = emptyItemBehavior
                };
            }

            if (dropDownAttribute == null)
            {
                return null;
            }

            var propertyValue = propertyInfo.GetValue(formModel);

            var renderingModel = new DropdownRenderingModel
            {
                Value = propertyValue == null ? null : (long?)Convert.ToInt64(propertyValue),
                Items = dropDownAttribute.GetItems(),
                EmptyItemBehavior = dropDownAttribute.EmptyItemBehavior
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}