using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.SemanticTypes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput
{
    public class HiddenInputRenderingModel : ControlRenderingModel
    {
        public object Value { get; set; }

        public HiddenInputRenderingModel()
        {
            CanBeImported = false;
            CanUseInSummary = false;
        }

        public string GetHtmlValue()
        {
            if (Value == null)
            {
                return string.Empty;
            }

            if (Value is string)
            {
                return Value as string;
            }

            if (Value is Guid || TypeHelper.IsNumeric(Value.GetType(), false))
            {
                return Value.ToString();
            }

            if (Value is bool)
            {
                return HtmlMarkupHelper.ToInputValue((bool)Value);
            }

            var bytes = Value as Byte[];

            if (bytes != null)
            {
                return Convert.ToBase64String(bytes);
            }

            const string listSeparator = ",";

            var intEnumerable = Value as IEnumerable<int>;

            if (intEnumerable != null)
            {
                return string.Join(listSeparator, intEnumerable);
            }

            var longEnumerable = Value as IEnumerable<long>;

            if (longEnumerable != null)
            {
                return string.Join(listSeparator, longEnumerable);
            }

            if (Value is Enum)
            {
                return Convert.ChangeType(Value, typeof(long)).ToString();
            }

            if (Value is DateTime)
            {
                return ((DateTime)Value).ToString("d", Thread.CurrentThread.CurrentCulture);    
            }

            if (TypeHelper.IsSubclassOfRawGeneric(Value.GetType(), typeof (SemanticType<>)))
            {
                return Value.ToString();
            }

            var message = $"Unexpected type when serializing field to hidden input. Type: {Value.GetType().Name}";
            throw new InvalidDataException(message);            
        }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("value", GetHtmlValue());
            attributes.Add("type", "hidden");

            attributes.Remove("class");
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
} 