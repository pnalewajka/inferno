using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput
{
    public class HiddenInputRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel, bool ignoreMissingAttributes = false)
        {
            var propertyAttribute = AttributeHelper.GetPropertyAttribute<HiddenInputAttribute>(propertyInfo);

            if (propertyAttribute == null && !ignoreMissingAttributes)
            {
                return null;
            }

            var renderingModel = new HiddenInputRenderingModel
            {
                Value = propertyInfo.GetValue(formModel)
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}