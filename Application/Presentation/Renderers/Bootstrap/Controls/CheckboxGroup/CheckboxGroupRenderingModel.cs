using System;
using System.Collections.Generic;
using System.Linq;
using RazorEngine.Text;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CheckboxGroup
{
    public class CheckboxGroupRenderingModel : ControlRenderingModel
    {
        private HashSet<long> _selectedIds = new HashSet<long>();

        public Type PropertyType { get; set; }
        public IList<ListItem> Items { get; set; }
        public IList<long> SelectedIds
        {
            get
            {
                return _selectedIds.ToList();
            }
            set
            {
                _selectedIds = new HashSet<long>(value);
            }
        }

        public bool IsSelected(long id)
        {
            return _selectedIds.Contains<long>(id);
        }

        public bool IsSelected(ListItem item)
        {
            return item.Id != null && IsSelected(item.Id.Value);
        }

        public void Select(long itemId)
        {
            if (!IsSelected(itemId))
            {
                _selectedIds.Add(itemId);
            }
        }

        public void Select(ListItem item)
        {
            if (item.Id == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            Select(item.Id.Value);
        }

        public void UnSelect(long itemId)
        {
            _selectedIds.Remove(itemId);
        }

        public void UnSelect(ListItem item)
        {
            if (item.Id == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            UnSelect(item.Id.Value);
        }

        public List<long> Ids
        {
            get
            {
                return Items.Select(i => i.Id)
                    .Cast<long>()
                    .ToList();
            }
        }

        public List<string> DisplayNames
        {
            get
            {
                return Items
                    .Select(i => i.DisplayName)
                    .ToList();
            }
        }

        public string GetDisplayName()
        {
            var selectedItemNames = Items.Where(IsSelected).Select(i => i.DisplayName);

            return string.Join(", ", selectedItemNames);
        }

        public string GetMultiValueClass()
        {
            return "is-multivalue";
        }

        public bool IsNoValueAllowed => true;

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Remove("id");
            attributes.Remove("class");

            attributes.Add("type", "hidden");
            attributes.Add("class", "hidden-but-validate");
            attributes.Add("value", string.Join(",", Ids));
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            var enumValues = EnumHelper.GetEnumValues(PropertyType);

            if (enumValues.Length == 0)
            {
                yield return string.Empty;
                yield break;
            }

            var typedEnumValues = new Enum[enumValues.Length];
            Array.Copy(enumValues, typedEnumValues, enumValues.Length);

            var enumNames = new List<string>();

            foreach (var enumValue in typedEnumValues)
            {
                var hasValue = SelectedIds.Any(id => Convert.ToInt64(enumValue) == id);

                if (hasValue)
                {
                    enumNames.Add(enumValue.ToString());
                }
            }

            yield return string.Join(",", enumNames);
        }

        public RawString GetClientValidationAttributes()
        {
            var validation = ClientValidationAttributes.Render();

            return new RawString(validation);
        }
    }
}