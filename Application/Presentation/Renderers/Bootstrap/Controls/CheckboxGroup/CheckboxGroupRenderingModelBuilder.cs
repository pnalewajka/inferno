using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CheckboxGroup
{
    public class CheckboxGroupRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var checkboxGroupAttribute = AttributeHelper.GetPropertyAttribute<CheckboxGroupAttribute>(propertyInfo);

            if (checkboxGroupAttribute == null)
            {
                return null;
            }

            var propertyType = propertyInfo.PropertyType;
            var enumType = TypeHelper.GetEnumerationElementType(propertyType);

            if (!TypeHelper.IsFlaggedEnum(propertyType)
                && (enumType == null || !IsEnumOrNumber(enumType)))
            {
                return null;
            }

            var ids = GetSelectedIds(propertyInfo, formModel, propertyType);

            var listItems = checkboxGroupAttribute.GetItems();

            var renderingModel = new CheckboxGroupRenderingModel
            {
                Items = listItems,
                Size = Size.Large,
                SelectedIds = ids,
                PropertyType = propertyType
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }

        private static bool IsEnumOrNumber(Type type)
        {
            return type.IsEnum
                || new[] { typeof(long), typeof(int), typeof(long?), typeof(int?) }.Contains(type);
        }

        private static List<long> GetSelectedIds(PropertyInfo propertyInfo, object formModel, Type propertyType)
        {
            var elementType = TypeHelper.GetEnumerationElementType(propertyType);
            object value = propertyInfo.GetValue(formModel);

            if (value == null)
            {
                return new List<long>();
            }

            // assumming any value will fit long!
            var ids = new List<long>();

            if (TypeHelper.IsIntegral(propertyType))
            {
                var longValue = (long?)value;

                if (longValue != 0)
                {
                    ids.Add(longValue.Value);
                }
            }
            else if (elementType == typeof(long))
            {
                var enumerable = (IEnumerable<long>)value;

                ids.AddRange(enumerable);
            }
            else if (elementType == typeof(int))
            {
                var enumerable = (IEnumerable<int>)value;
                ids.AddRange(enumerable.Select(x => (long)x));
            }
            else if (elementType != null && elementType.IsEnum)
            {
                var enumCollection = (IEnumerable)value;

                foreach (var enumValue in enumCollection)
                {
                    ids.Add((long)Convert.ChangeType(enumValue, typeof(long)));
                }
            }
            else if (propertyType.IsEnum && propertyType.IsDefined(typeof(FlagsAttribute), false))
            {
                var en = (Enum)propertyInfo.GetValue(formModel);

                long enumerationValue;

                if (Enum.GetUnderlyingType(en.GetType()) == typeof(long))
                {
                    enumerationValue = (long)(object)en;
                }
                else if (Enum.GetUnderlyingType(en.GetType()) == typeof(int))
                {
                    enumerationValue = (int)(object)en;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(propertyType), RenderersResources.CheckboxGroupPropertyInvalidTypeExceptionMessage);
                }

                for (var i = 0; i < sizeof(long) * 8; i++)
                {
                    var flag = enumerationValue & (1 << i);

                    if (flag != 0)
                    {
                        ids.Add(flag);
                    }
                }
            }

            return ids.Distinct().ToList();
        }
    }
}