using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Internal;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Checkbox;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CheckboxGroup;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CodeEditor;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ColorPicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CropImageUpload;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DocumentPicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.LocalizedString;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.MapPicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.NumberControl;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Optional;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RadioGroup;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RichTextEditor;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textbox;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Timestamp;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.TypeAhead;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Models;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls
{
    public class ControlRenderingModelBuilder
    {
        public ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var model =
                OptionalRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, context, currentUserRoles)
                ?? CustomControlRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? TimestampRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? HiddenInputRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? CodeEditorRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? ColorPickerRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? RichTextEditorRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? TextareaRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? TextboxRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? DatePickerRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? ValuePickerRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? TypeAheadRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? RadioGroupRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? CheckboxGroupRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? DropdownRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? CheckboxRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? NumberControlRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? CropImageUploadRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, currentUserRoles)
                ?? DocumentPickerRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel, currentUserRoles)
                ?? MapPickerRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? LocalizedStringRenderingModelBuilder.GetModelForProperty(propertyInfo, formModel)
                ?? RepeaterModelBuilder.GetModelForProperty(propertyInfo, formModel, context, currentUserRoles);

            if (model == null)
            {
                var message = $"No builders found for property: {propertyInfo.Name}";
                throw new InvalidDataException(message);
            }

            ApplyAccessRights(model, propertyInfo, currentUserRoles);
            BuildValidationMessages(propertyInfo.Name, model, context);

            return model;
        }

        private static void ApplyAccessRights(ControlRenderingModel model, PropertyInfo propertyInfo, HashSet<string> currentUserRoles)
        {
            var propertyAccessMode = SecurityHelper.GetPropertyAccessMode(propertyInfo, currentUserRoles);

            switch (propertyAccessMode)
            {
                case PropertyAccessMode.NoAccess:
                    model.Visibility = ControlVisibility.Remove;
                    break;

                case PropertyAccessMode.ReadOnly:
                    model.IsReadOnly = true;
                    break;
            }
        }

        internal static void BuildCommonAttributes(PropertyInfo propertyInfo, ControlRenderingModel controlRenderingModel, object formModel)
        {
            controlRenderingModel.Id = NamingConventionHelper.ConvertPascalCaseToHyphenated(propertyInfo.Name);
            controlRenderingModel.Name = propertyInfo.Name;
            controlRenderingModel.Order = ViewModelMemberHelper.GetOrder(propertyInfo);

            if (controlRenderingModel.Label == null)
            {
                controlRenderingModel.Label = ViewModelMemberHelper.GetLabel(propertyInfo);
            }

            if (controlRenderingModel.Hint == null)
            {
                controlRenderingModel.Hint = ViewModelMemberHelper.GetHint(propertyInfo);
            }

            if (controlRenderingModel.Tooltip == null)
            {
                controlRenderingModel.Tooltip = ViewModelMemberHelper.GetTooltip(propertyInfo, formModel);
            }

            controlRenderingModel.Placeholder = ViewModelPropertyHelper.GetPrompt(propertyInfo);

            SetIsReadOnly(propertyInfo, controlRenderingModel, formModel);

            var tabDescriptions = formModel.GetType().GetCustomAttributes<TabDescriptionAttribute>(true);
            SetTabInfo(propertyInfo, controlRenderingModel, tabDescriptions, formModel);

            var fieldSetDescriptions = formModel.GetType().GetCustomAttributes<FieldSetDescriptionAttribute>(true);
            SetFieldSetInfo(propertyInfo, controlRenderingModel, fieldSetDescriptions);
            SetSpecificType(propertyInfo, controlRenderingModel);
            SetIsRequired(propertyInfo, controlRenderingModel, formModel);
            SetMaxLength(propertyInfo, controlRenderingModel);
            SetCanBeImportedAttribute(propertyInfo, controlRenderingModel);
            SetRenderingCustomizations(propertyInfo, controlRenderingModel);
            SetCustomAttributes(propertyInfo, controlRenderingModel);
            SetClientValidationAttributes(propertyInfo, controlRenderingModel, formModel);
            SetOnValueChangeAttribute(propertyInfo, controlRenderingModel);
        }

        private static void SetCanBeImportedAttribute(PropertyInfo propertyInfo, ControlRenderingModel controlRenderingModel)
        {
            var excludeImportAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (ImportIgnoreAttribute)null);

            if (excludeImportAttribute != null && excludeImportAttribute.ImportIgnore)
            {
                controlRenderingModel.CanBeImported = false;
            }
        }

        private static void SetCustomAttributes(PropertyInfo propertyInfo, ControlRenderingModel controlRenderingModel)
        {
            foreach (var attribute in propertyInfo.GetAttributes<HtmlAttributeAttribute>())
            {
                controlRenderingModel.CustomInputTagAttributes.Add(attribute.Name, attribute.Value);
            }
        }

        private static void SetOnValueChangeAttribute(PropertyInfo propertyInfo, ControlRenderingModel controlRenderingModel)
        {
            var attribute = propertyInfo.GetAttribute<OnValueChangeAttribute>();

            if (attribute != null)
            {
                controlRenderingModel.ClientValidationAttributes.Add("data-onvaluechange-enabled", true);
                controlRenderingModel.ClientValidationAttributes.Add("data-onvaluechange-executeonload", attribute.ExecuteOnLoad);

                if (!attribute.ClientHandler.IsNullOrEmpty())
                {
                    controlRenderingModel.ClientValidationAttributes.Add("data-onvaluechange-clienthandler",
                        attribute.ClientHandler);
                }

                if (!attribute.ServerHandler.IsNullOrEmpty())
                {
                    var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                    controlRenderingModel.ClientValidationAttributes.Add("data-onvaluechange-serverhandler", urlHelper.Content(attribute.ServerHandler));
                }

                if (!attribute.ServerResultClientHandler.IsNullOrEmpty())
                {
                    controlRenderingModel.ClientValidationAttributes.Add(
                        "data-onvaluechange-serverresultclienthandler", attribute.ServerResultClientHandler);
                }
            }
        }

        private static void SetClientValidationAttributes(PropertyInfo propertyInfo, ControlRenderingModel controlRenderingModel, object formModel)
        {
            foreach (var attribute in propertyInfo.GetAttributes<ValidationAttribute>().OfType<IAtomicClientValidatable>())
            {
                var conditionalValidationAttribute = attribute as ConditionalValidationAttribute;

                if (conditionalValidationAttribute == null || IsConditionActive(formModel, conditionalValidationAttribute))
                {
                    attribute.SetValidationAttributes(controlRenderingModel.ClientValidationAttributes, controlRenderingModel.Label);
                }
            }
        }

        public static void BuildValidationMessages(string propertyName, ControlRenderingModel controlRenderingModel, ModelBuilderContext context)
        {
            var nestedModelState = context.GetValidationModelState();
            var modelState = nestedModelState[propertyName];
            controlRenderingModel.Errors = modelState != null ? modelState.Errors : new ModelErrorCollection();
        }

        private static void SetMaxLength(PropertyInfo propertyInfo, ControlRenderingModel model)
        {
            var stringLengthAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (StringLengthAttribute)null);

            if (stringLengthAttribute != null)
            {
                model.MinLength = stringLengthAttribute.MinimumLength;
                model.MaxLength = stringLengthAttribute.MaximumLength;
                model.WrongLengthExceededErrorMessage = stringLengthAttribute.FormatErrorMessage(model.GetLabelPlainText());
            }

            var isLocalizedStringAttributeDefined = TrySetMaxLengthBasedOnLocalizedStringLengthAttribute(propertyInfo, model);

            var minLengthAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (MinLengthAttribute)null);

            if (minLengthAttribute != null && stringLengthAttribute != null)
            {
                throw new LengthValidationAttributeMismatchException();
            }

            if (minLengthAttribute != null)
            {
                model.MinLength = minLengthAttribute.Length;
                model.WrongLengthExceededErrorMessage = minLengthAttribute.FormatErrorMessage(model.GetLabelPlainText());
            }

            var maxLengthAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (MaxLengthAttribute)null);
            var isMaxLengthDefinedMoreThanOne = IsMaxLengthDefinedMoreThanOne(maxLengthAttribute != null,
                                                                              stringLengthAttribute != null,
                                                                              isLocalizedStringAttributeDefined);

            if (isMaxLengthDefinedMoreThanOne)
            {
                throw new LengthValidationAttributeMismatchException();
            }

            if (maxLengthAttribute != null)
            {
                model.MaxLength = maxLengthAttribute.Length;
                model.WrongLengthExceededErrorMessage = maxLengthAttribute.FormatErrorMessage(model.GetLabelPlainText());
            }
        }

        private static bool IsMaxLengthDefinedMoreThanOne(params bool[] attributesDefined)
        {
            return attributesDefined.Count(x => x) >= 2;
        }

        private static bool TrySetMaxLengthBasedOnLocalizedStringLengthAttribute(PropertyInfo propertyInfo, ControlRenderingModel model)
        {
            var stringLengthAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (LocalizedStringLengthAttribute)null);
            var attributeExists = stringLengthAttribute != null;

            if (attributeExists)
            {
                model.MaxLength = stringLengthAttribute.MaxLength;
                model.WrongLengthExceededErrorMessage = stringLengthAttribute.FormatErrorMessage(model.GetLabelPlainText());
            }

            return attributeExists;
        }

        private static void SetIsRequired(PropertyInfo propertyInfo, ControlRenderingModel model, object formModel)
        {
            var requiredAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (RequiredAttribute)null);

            var requiredWhenAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (RequiredWhenAttribute)null);
            var conditionalRequiredActive = IsConditionActive(formModel, requiredWhenAttribute);
            var localizedStringRequiredAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (LocalizedStringRequiredAttribute)null);
            var isRequiredDefined = requiredAttribute != null || conditionalRequiredActive || localizedStringRequiredAttribute != null;

            if (isRequiredDefined)
            {
                var validationAttribute = requiredAttribute
                                          ?? (ValidationAttribute)requiredWhenAttribute
                                          ?? localizedStringRequiredAttribute;

                model.IsRequired = true;
                model.RequiredValueErrorMessage = string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, model.GetLabelPlainText());
            }

            var cannotBeEmptyAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (CannotBeEmptyAttribute)null);
            var conditionalCannotbeEmptyActive = cannotBeEmptyAttribute != null && ViewModelHelper.IsSuitableFor(formModel, cannotBeEmptyAttribute.FormType);

            model.RequiredMarker = requiredAttribute != null
                || conditionalCannotbeEmptyActive
                || conditionalRequiredActive;
        }

        private static bool IsConditionActive(object formModel, ConditionalValidationAttribute conditionalValidationAttribute)
        {
            return (conditionalValidationAttribute != null && ViewModelHelper.IsSuitableFor(formModel, conditionalValidationAttribute.FormType));
        }

        private static void SetRenderingCustomizations(PropertyInfo propertyInfo, ControlRenderingModel model)
        {
            var renderAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (RenderAttribute)null);
            var modelSize = GetControlSize(propertyInfo, model);

            if (renderAttribute != null)
            {
                if (renderAttribute.Size != Size.Default)
                {
                    modelSize = renderAttribute.Size;
                }

                model.CssClass = HtmlMarkupHelper.AddCssClass(model.CssClass, renderAttribute.ControlCssClass);
            }

            model.Size = modelSize;
        }

        private static Size GetControlSize(PropertyInfo propertyInfo, ControlRenderingModel model)
        {
            if (model.Size != Size.Default)
            {
                return model.Size;
            }

            var propertyType = propertyInfo.PropertyType;
            if (TypeHelper.IsNumeric(propertyType))
            {
                return Size.Small;
            }

            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                return Size.Small;
            }

            if (propertyType == typeof(string))
            {
                var maxLength = model.MaxLength;

                if (maxLength.HasValue)
                {
                    if (maxLength <= 10)
                    {
                        return Size.Small;
                    }

                    if (maxLength <= 32)
                    {
                        return Size.Medium;
                    }
                }
            }

            return Size.Default;
        }

        private static void SetSpecificType(PropertyInfo propertyInfo, ControlRenderingModel model)
        {
            var dataTypeAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, (DataTypeAttribute)null);
            model.SpecificDataType = dataTypeAttribute != null ? dataTypeAttribute.DataType : (DataType?)null;
        }

        private static void SetTabInfo(PropertyInfo propertyInfo, ControlRenderingModel model, IEnumerable<TabDescriptionAttribute> tabDescriptions, object formModel)
        {
            var tabAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, () => new TabAttribute(null));
            model.TabId = tabAttribute.TabId;
            var tabDescription = tabDescriptions.FirstOrDefault(d => d.Id == model.TabId);

            if (tabDescription != null)
            {
                model.TabName = tabDescription.GetLocalizedTabName(formModel);
                model.TabOrder = tabDescription.Order;
            }
        }

        private static void SetFieldSetInfo(PropertyInfo propertyInfo, ControlRenderingModel model, IEnumerable<FieldSetDescriptionAttribute> fieldSetDescriptions)
        {
            var fieldSetAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, () => new FieldSetAttribute(null));
            model.FieldSetId = fieldSetAttribute.Id;
            var fieldSetDescription = fieldSetDescriptions.FirstOrDefault(d => d.Id == model.FieldSetId);

            if (fieldSetDescription == null && !string.IsNullOrEmpty(model.FieldSetId))
            {
                var message = $"No fieldset with id='{model.FieldSetId}' found";
                throw new InvalidDataException(message);
            }

            model.Legend = fieldSetDescription != null ? fieldSetDescription.GetLocalizedLegend() : null;
            model.FieldSetOrder = fieldSetDescription != null ? fieldSetDescription.Order : long.MaxValue;
        }

        private static void SetIsReadOnly(PropertyInfo propertyInfo, ControlRenderingModel model, object formModel)
        {
            model.IsReadOnly = !propertyInfo.CanWrite
                                || AttributeHelper.GetPropertyAttribute(propertyInfo, () => new ReadOnlyAttribute(false))
                                               .IsReadOnly;

            var formModelType = formModel.GetType();
            var isReadOnlyMethodName = $"Is{propertyInfo.Name}ReadOnly";
            var isReadOnlyMethodInfo = formModelType.GetProperty(isReadOnlyMethodName);

            if (isReadOnlyMethodInfo != null)
            {
                model.IsReadOnly |= (bool)isReadOnlyMethodInfo.GetValue(formModel);
            }
        }
    }
}
