using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RichTextEditor
{
    public class RichTextEditorRenderingModelBuilder
    {
        private const byte DefaultRows = 10;

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof (string))
            {
                return null;
            }

            var richTextEditorAttribute = AttributeHelper.GetPropertyAttribute<RichTextAttribute>(propertyInfo);

            if (richTextEditorAttribute == null)
            {
                return null;
            }

            var multilineAttribute = AttributeHelper.GetPropertyAttribute<MultilineAttribute>(propertyInfo);

            if (multilineAttribute == null)
            {
                multilineAttribute = new MultilineAttribute(DefaultRows);
            }

            var renderingModel = new RichTextEditorRenderingModel
            {
                Value = (string) propertyInfo.GetValue(formModel), 
                Rows = multilineAttribute.Rows ?? DefaultRows,
                EditorPresets = richTextEditorAttribute.EditorPresets,
                MentionJavaScriptResolver = richTextEditorAttribute.MentionJavaScriptResolver
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}