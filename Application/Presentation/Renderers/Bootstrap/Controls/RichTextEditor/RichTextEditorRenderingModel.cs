using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RichTextEditor
{
    public class RichTextEditorRenderingModel : TextareaRenderingModel
    {
        public RichTextEditorPresets EditorPresets { get; set; }
        public string MentionJavaScriptResolver { get; set; }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("data-ck-editor", true);
            attributes.Add("data-ck-editor-presets", GetEditorPresets());

            if (!string.IsNullOrEmpty(MentionJavaScriptResolver))
            {
                attributes.Add("data-ck-editor-mention", MentionJavaScriptResolver);
            }
        }

        private string GetEditorPresets()
        {
            return NamingConventionHelper.ConvertPascalCaseToHyphenated(EditorPresets.ToString());
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}