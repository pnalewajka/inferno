using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.NumberControl
{
    public abstract class NumberControlRenderingModelBase<T> : ControlRenderingModel where T: struct
    {
        public T? MinValue { get; set; }
        public T? MaxValue { get; set; }

        public string Value { get; set; }

        public bool IsNullable { get; set; }
        public byte Precision { get; set; }

        public string SpinnerButtonClass => IsReadOnly ? "disabled-icon" : string.Empty;

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("type", "text");
            attributes.Add("value", Value);
            attributes.Add("placeholder", Placeholder);

            if (IsRequired)
            {
                attributes.Add("data-val-required", RequiredValueErrorMessage);
            }

            if (HasLengthValidation)
            {
                attributes.Add("data-val-length", WrongLengthExceededErrorMessage);
            }

            if (MinLength.HasValue)
            {
                attributes.Add("data-val-length-min", MinLength);
            }

            if (MaxLength.HasValue)
            {
                attributes.Add("data-val-length-max", MaxLength);
                attributes.Add("maxlength", MaxLength);
            }

            attributes.Add("data-min-value", MinValue);
            attributes.Add("data-max-value", MaxValue);
            attributes.Add("data-is-nullable", JavaScriptTypeConversionHelper.ConvertToString(IsNullable));

            if (Precision != 0)
            {
                attributes.Add("data-number-precision", Precision);
            }
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}