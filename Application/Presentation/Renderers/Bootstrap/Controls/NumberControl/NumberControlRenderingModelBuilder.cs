using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.NumberControl
{
    public class NumberControlRenderingModelBuilder
    {
        private const byte DefaultDecimalPrecision = 2;

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var nullablePropertyType = Nullable.GetUnderlyingType(propertyInfo.PropertyType);
            var propertyType = nullablePropertyType ?? propertyInfo.PropertyType;

            if (propertyType != typeof(long)
                && propertyType != typeof(int)
                && propertyType != typeof(decimal)
                && propertyType != typeof(double))
            {
                return null;
            }

            var valueRange = new ValueRange(propertyInfo);
            var value = propertyInfo.GetValue(formModel);

            var renderingModel = new NumberControlRenderingModel
            {
                Value = GetHtmlFormattedValue(value),
                MinValue = valueRange.MinValue,
                MaxValue = valueRange.MaxValue,
                IsNullable = nullablePropertyType != null,
                Precision = GetPrecision(propertyInfo, propertyType)
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }

        private static string GetHtmlFormattedValue(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            if (value is double)
            {
                //http://www.hanselman.com/blog/WhyYouCantDoubleParseDoubleMaxValueToStringOrSystemOverloadExceptionsWhenUsingDoubleParse.aspx
                return ((double) value).ToString("R");
            }

            if (value is Single)
            {
                //http://www.hanselman.com/blog/WhyYouCantDoubleParseDoubleMaxValueToStringOrSystemOverloadExceptionsWhenUsingDoubleParse.aspx
                return ((Single)value).ToString("R");
            }

            return value.ToString();
        }

        private static byte GetPrecision(PropertyInfo propertyInfo, Type propertyType)
        {
            byte precision = 0;

            if (propertyType != typeof(decimal) && propertyType != typeof(double))
            {
                return precision;
            }

            var attribute = AttributeHelper.GetPropertyAttribute<PrecisionAttribute>(propertyInfo);
            precision = attribute != null ? attribute.Precision : DefaultDecimalPrecision;

            return precision;
        }

        private class ValueRange
        {
            public decimal? MinValue { get; private set; }
            public decimal? MaxValue { get; private set; }

            public ValueRange(PropertyInfo propertyInfo)
            {
                var rangeAttribute = AttributeHelper.GetPropertyAttribute<RangeAttribute>(propertyInfo);
                if (rangeAttribute != null)
                {
                    MinValue = Convert.ToDecimal(rangeAttribute.Minimum);
                    MaxValue = Convert.ToDecimal(rangeAttribute.Maximum);
                }

                var minValueAttribute = AttributeHelper.GetPropertyAttribute<MinValueAttribute>(propertyInfo);
                if (minValueAttribute != null)
                {
                    MinValue = minValueAttribute.MinValue;
                }

                var maxValueAttribute = AttributeHelper.GetPropertyAttribute<MaxValueAttribute>(propertyInfo);
                if (maxValueAttribute != null)
                {
                    MaxValue = maxValueAttribute.MaxValue;
                }
            }
        }
    }
}