using System;
using System.Collections.Generic;
using System.Threading;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker
{
    public class DatePickerRenderingModel : ControlRenderingModel
    {
        public DateTime? Value { get; set; }
        public DateTime? MinValue { get; set; }
        public DateTime? MaxValue { get; set; }

        public TimePickerRenderingModel TimePickerRenderingModel { get; set; }

        public string PickerButtonClass => IsReadOnly ? "disabled-icon" : string.Empty;

        public string GetHtmlDisplayValue()
        {
            if (Value.HasValue)
            {
                var dateTimeFormat = TimePickerRenderingModel == null
                    ? "d" // date only eg. 6/15/2008
                    : "g"; // date and time eg. 6/15/2008 9:15 PM

                return Value.Value.ToString(dateTimeFormat, Thread.CurrentThread.CurrentCulture);
            }

            return string.Empty;
        }

        public string GetHtmlValue()
        {
            if (Value.HasValue)
            {
                return Value.Value.ToString("d", Thread.CurrentThread.CurrentCulture);
            }

            return string.Empty;
        }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("type", "text");
            attributes.Add("value", GetHtmlValue());
            attributes.Add("data-val-date", RenderersResources.DatePickerThisMustBeADateMessage);

            if (MinValue.HasValue)
            {
                attributes.Add("data-min-value", GetHtmlMinValue());
            }

            if (MaxValue.HasValue)
            {
                attributes.Add("data-max-value", GetHtmlMaxValue());
            }
        }

        protected override void AddReadOnlyAttributes(TagAttributes attributes)
        {
            base.AddReadOnlyAttributes(attributes);

            attributes.Add("data-readonly", "true");
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }

        private string GetHtmlMinValue()
        {
            if (MinValue.HasValue)
            {
                return MinValue.Value.ToString("d", Thread.CurrentThread.CurrentCulture);
            }

            return null;
        }

        private string GetHtmlMaxValue()
        {
            if (MaxValue.HasValue)
            {
                return MaxValue.Value.ToString("d", Thread.CurrentThread.CurrentCulture);
            }

            return null;
        }
    }
}