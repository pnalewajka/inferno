using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker
{
    public class DatePickerRenderingModelBuilder
    {
        private static readonly DateTime DefaultMinValue = new DateTime(1753, 1, 1);
        private static readonly DateTime DefaultMaxValue = new DateTime(9999, 1, 1);

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (!TypeHelper.IsTypeOrNullableType<DateTime>(propertyInfo.PropertyType))
            {
                return null;
            }

            var range = AttributeHelper.GetPropertyAttribute<RangeAttribute>(propertyInfo);
            var timePicker = AttributeHelper.GetPropertyAttribute<TimePickerAttribute>(propertyInfo);

            var value = GetAdjustedPropertyValue(propertyInfo, formModel);

            var renderingModel = new DatePickerRenderingModel
            {
                Value = value,
                MinValue = range != null ? DateTime.Parse((string)range.Minimum) : DefaultMinValue,
                MaxValue = range != null ? DateTime.Parse((string)range.Maximum) : DefaultMaxValue
            };

            renderingModel.TimePickerRenderingModel = timePicker == null
                ? null
                : new TimePickerRenderingModel(renderingModel.Name)
                {
                    Time = value.HasValue ? (value.Value - value.Value.Date) : TimeSpan.FromMinutes(0),
                    Interval = timePicker.MinuteInterval,
                }; ;

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }

        private static DateTime? GetAdjustedPropertyValue(PropertyInfo propertyInfo, object formModel)
        {
            var value = (DateTime?)propertyInfo.GetValue(formModel);
            var datePicker = AttributeHelper.GetPropertyAttribute<DatePickerAttribute>(propertyInfo);

            if (!datePicker?.EmptyIfDefault ?? false)
            {
                return value;
            }

            return propertyInfo.PropertyType != typeof(DateTime) || value != default(DateTime) ? value : null;
        }
    }
}
