﻿using System;
using System.Globalization;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker
{
    public class TimePickerRenderingModel
    {
        public static string TimePickerControlName(string dateControlName)
        {
            return $"{dateControlName}.Time";
        }

        internal TimePickerRenderingModel(string dateControlName)
        {
            Name = TimePickerControlName(dateControlName);
        }

        public string Name { get; set; }

        public byte Interval { get; set; } = 5;

        public TimeSpan Time { get; set; } = TimeSpan.FromMinutes(0);

        public string TimeFormat { get; set; } = CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern;

        public string DisplayValue
        {
            get
            {
                return $"{Time.Hours.ToString("D2")}:{Time.Minutes.ToString("D2")}";
            }
        }

        public string DisplayText
        {
            get
            {
                return new DateTime().Add(Time).ToShortTimeString();
            }
        }

        public double TotalMinutes
        {
            get
            {
                return Time.TotalMinutes;
            }
        }
    }
}
