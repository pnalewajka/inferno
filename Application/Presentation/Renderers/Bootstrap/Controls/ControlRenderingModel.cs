using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using RazorEngine.Text;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls
{
    public abstract class ControlRenderingModel
    {
        private int _labelWidth;
        private int _controlWidth;
        private int _hintWidth;
        private string _sizeInfix;
        public string Id { get; set; }

        public string Name { get; set; }

        public long Order { get; set; }

        public string Label { get; set; }

        public RawString GetLabelTextMarkup()
        {
            return new RawString(HtmlMarkupHelper.HighlightAccelerator(Label));
        }

        public string GetLabelPlainText()
        {
            return HtmlMarkupHelper.RemoveAccelerator(Label);
        }

        public string Hint { get; set; }

        public bool HasHint => !string.IsNullOrEmpty(Hint);

        public string Tooltip { get; set; }

        public bool RequiredMarker { get; set; }

        public bool IsReadOnly { get; set; }

        public string Placeholder { get; set; }

        public string TabId { get; set; }

        public string TabName { get; set; }

        public long TabOrder { get; set; }

        public long FieldSetOrder { get; set; }

        public string FieldSetId { get; set; }

        public string Legend { get; set; }

        public virtual bool HasValidation => IsRequired || MinLength.HasValue || MaxLength.HasValue || ClientValidationAttributes.Any();

        public virtual bool HasLengthValidation => MinLength.HasValue || MaxLength.HasValue;

        public bool IsRequired { get; set; }

        public string RequiredValueErrorMessage { get; set; }

        public int? MinLength { get; set; }

        public int? MaxLength { get; set; }

        public string WrongLengthExceededErrorMessage { get; set; }

        public Size Size { get; set; }

        public string CssClass { get; set; }

        public int LabelWidth
        {
            get { return _labelWidth; }
            set
            {
                _labelWidth = value;
                LabelWidthClass = GetLabelWidthClass();
                LabelOffsetClass = GetLabelOffsetClass();
            }
        }

        public int ControlWidth
        {
            get { return _controlWidth; }
            set
            {
                _controlWidth = value;
                ControlWidthClass = GetControlWidthClass();
            }
        }

        public int HintWidth
        {
            get { return _hintWidth; }
            set
            {
                _hintWidth = value;
                HintWidthClass = GetHintWidthClass();
            }
        }

        public ModelErrorCollection Errors { get; set; }

        public DataType? SpecificDataType { get; set; }

        public virtual ControlVisibility Visibility { get; set; }

        public TagAttributes CustomInputTagAttributes { get; private set; }

        public TagAttributes ClientValidationAttributes { get; private set; }

        public string SizeInfix
        {
            get { return _sizeInfix; }
            set
            {
                _sizeInfix = value;
                LabelWidthClass = GetLabelWidthClass();
                LabelOffsetClass = GetLabelOffsetClass();
                ControlWidthClass = GetControlWidthClass();
            }
        }

        public string LabelWidthClass { get; set; }

        public string LabelOffsetClass { get; set; }

        public string ControlWidthClass { get; set; }

        public string HintWidthClass { get; set; }
        
        public string GetValidationSpanClass()
        {
            return Errors.Count == 0 ? "field-validation-valid" : "field-validation-error";
        }

        public string GetRequiredMarkerCss()
        {
            return RequiredMarker ? "required" : null;
        }

        public string GetVisibilityClass()
        {
            return Visibility == ControlVisibility.Hide ? "collapse" : null;
        }

        public RawString GetInputTagAttributes()
        {
            var standardAttributes = new TagAttributes();
            AddStandardInputTagAttributes(standardAttributes);

            var standard = standardAttributes.Render();
            var validation = ClientValidationAttributes.Render();
            var custom = CustomInputTagAttributes.Render();

            return new RawString(StringHelper.Join(" ", standard, validation, custom));
        }

        public RawString GetLabelTagAttributes()
        {
            var tagAttributes = new TagAttributes();

            var accessKey = HtmlMarkupHelper.GetAccelerator(Label);

            if (accessKey != null)
            {
                tagAttributes.Add("accesskey", accessKey);
            }

            return new RawString(tagAttributes.Render());
        }

        protected virtual void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            attributes.Add("id", Id);
            attributes.Add("name", Name);
            attributes.Add("class", "form-control");

            if (IsReadOnly)
            {
                AddReadOnlyAttributes(attributes);
            }

            if (HasValidation)
            {
                attributes.Add("data-val", "true");
            }
        }

        protected virtual void AddReadOnlyAttributes(TagAttributes attributes)
        {
            attributes.Add("readonly");
        }

        public bool CanBeImported { get; set; }

        public bool CanUseInSummary { get; set; }


        protected ControlRenderingModel()
        {
            Visibility = ControlVisibility.Show;
            CustomInputTagAttributes = new TagAttributes();
            ClientValidationAttributes = new TagAttributes();

            _sizeInfix = Layout.BootstrapDefaultSizeInfix;

            CanBeImported = true;
            CanUseInSummary = true;
        }

        private string GetLabelWidthClass()
        {
            return $"col-{SizeInfix}-{LabelWidth}";
        }

        private string GetLabelOffsetClass()
        {
            return $"col-{SizeInfix}-offset-{LabelWidth}";
        }

        private string GetControlWidthClass()
        {
            return $"col-{SizeInfix}-{ControlWidth}";
        }

        private string GetHintWidthClass()
        {
            return $"col-{SizeInfix}-{HintWidth}";
        }

        public virtual IEnumerable<string> GetImportTemplateColumnNames()
        {
            yield return Label;
        }

        public abstract IEnumerable<object> GetImportTemplateValues();
    }
}