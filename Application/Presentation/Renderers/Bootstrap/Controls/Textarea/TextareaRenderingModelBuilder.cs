using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea
{
    public class TextareaRenderingModelBuilder
    {
        private const byte DefaultRows = 3;

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof(string))
            {
                return null;
            }

            var multilineAttribute = AttributeHelper.GetPropertyAttribute<MultilineAttribute>(propertyInfo);
            var dataTypeAttribute = AttributeHelper.GetPropertyAttribute<DataTypeAttribute>(propertyInfo);
            var htmlSanitizeAttribute = AttributeHelper.GetPropertyAttribute<HtmlSanitizeAttribute>(propertyInfo);

            if (multilineAttribute == null && dataTypeAttribute != null)
            {
                if (dataTypeAttribute.DataType == DataType.MultilineText)
                {
                    multilineAttribute = new MultilineAttribute();
                }
            }

            if (multilineAttribute == null)
            {
                return null;
            }

            var stringValue = (string)propertyInfo.GetValue(formModel);

            if (htmlSanitizeAttribute != null && htmlSanitizeAttribute.ShouldBeSanitized)
            {
                stringValue = HttpUtility.HtmlDecode(stringValue);
            }

            if (AttributeHelper.HasPropertyAttribute<TrimAttribute>(propertyInfo))
            {
                stringValue = stringValue?.Trim();
            }

            var renderingModel = new TextareaRenderingModel
            {
                Value = stringValue,
                Rows = multilineAttribute.Rows ?? DefaultRows,
                AllowAutoresize = multilineAttribute.AllowAutoresize
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}