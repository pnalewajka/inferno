using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea
{
    public class TextareaRenderingModel : ControlRenderingModel
    {
        public string Value { get; set; }

        public byte Rows { get; set; }

        public bool AllowAutoresize { get; set; }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("rows", Rows);
            attributes.Add("placeholder", Placeholder);

            if (IsRequired)
            {
                attributes.Add("data-val-required", RequiredValueErrorMessage);
            }

            if (HasLengthValidation)
            {
                attributes.Add("data-val-length", WrongLengthExceededErrorMessage);
            }

            if (MinLength.HasValue)
            {
                attributes.Add("data-val-length-min", MinLength);
            }

            if (MaxLength.HasValue)
            {
                attributes.Add("data-val-length-max", MaxLength);
                attributes.Add("maxlength", MaxLength);
            }

            if (AllowAutoresize)
            {
                attributes.Add("data-allow-autoresize", AllowAutoresize);
            }
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}