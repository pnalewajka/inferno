using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater
{
    public class RepeaterModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            Type collectionItemType;
            IEnumerable collection;
            RepeaterAttribute repeaterAttribute;

            var isCollection = typeof(IEnumerable).IsAssignableFrom(propertyInfo.PropertyType);
            var propertyValue = propertyInfo.GetValue(formModel);
            var isEmptyCollection = isCollection && (propertyValue == null || !((IEnumerable)propertyValue).OfType<object>().Any());

            if (isCollection)
            {
                repeaterAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, new RepeaterAttribute(ViewModelMemberHelper.GetLabel(propertyInfo)));
                collectionItemType = TypeHelper.GetCollectionElementType(propertyInfo.PropertyType);
                collection = propertyValue as IEnumerable ?? new List<object>();
            }
            else
            {
                repeaterAttribute = AttributeHelper.GetPropertyAttribute(propertyInfo, new RepeaterAttribute(ViewModelMemberHelper.GetLabel(propertyInfo), null, 1, 1));

                collectionItemType = propertyInfo.PropertyType;
                collection = new List<object> { propertyValue };
            }

            if (collectionItemType.IsEnum)
            {
                return null;
            }

            var renderingModel = new RepeaterRenderingModel
            {
                ItemType = collectionItemType,
                IsCollection = isCollection,
                MinCount = repeaterAttribute.MinCount,
                ItemTitle = repeaterAttribute.GetLocalizedItemTitle(),
                MaxCount = repeaterAttribute.MaxCount,
                CanAddRecord = repeaterAttribute.CanAddRecord,
                AddButtonText = repeaterAttribute.GetLocalizedAddButtonText(),
                DeleteButtonText = repeaterAttribute.GetLocalizedDeleteButtonText(),
                CanDeleteRecord = repeaterAttribute.CanDeleteRecord,
                CanReorder = repeaterAttribute.CanReorder,
                HideInReadOnlyWhenEmpty = repeaterAttribute.HideInReadOnlyWhenEmpty,
                TemplateItem = default(RepeaterItem),
                Items = new List<RepeaterItem>(),
                Layout = LayoutAttribute.GetLayoutForNestedModel(formModel, propertyInfo, context.Layout)
            };

            if (!ViewModelMemberHelper.HasLabel(propertyInfo))
            {
                renderingModel.Label = renderingModel.ItemTitle;
            }

            var templateItemInstance = ReflectionHelper.CreateInstance(collectionItemType);

            var nestedRepeaterAttributeProperty =
                TypeHelper.GetPublicInstancePropertiesWithAttribute<RepeaterAttribute>(templateItemInstance.GetType())
                          .FirstOrDefault();

            if (nestedRepeaterAttributeProperty != null)
            {
                var exceptionMessage =
                    string.Format(Resources.RenderersResources.RepeaterNestingNotSupportedExceptionMessageFormat,
                        nestedRepeaterAttributeProperty.Name);

                throw new InvalidOperationException(exceptionMessage);
            }

            var templateItemFormRenderingModel = renderingModel.Layout.BuildRenderingModels(templateItemInstance, context, currentUserRoles);
            var templateItemName = string.Join(".", new string[] { "template", context.AbsolutePath(), propertyInfo.Name }.Where(s => !string.IsNullOrEmpty(s)));

            renderingModel.TemplateItem = new RepeaterItem(templateItemName + (isCollection ? "[]" : string.Empty) + ".", templateItemFormRenderingModel.Controls);

            if (!isEmptyCollection)
            {
                var index = 0;

                foreach (var nestedElement in collection)
                {
                    if (nestedElement == null) // not a collection
                    {
                        continue;
                    }

                    var namePrefix = propertyInfo.Name + (isCollection ? $"[{index}]" : string.Empty);
                    var nestedContext = new ModelBuilderContext(renderingModel.Layout, namePrefix, context);
                    var formRenderingModel = renderingModel.Layout.BuildRenderingModels(nestedElement, nestedContext, currentUserRoles);
                    var record = new RepeaterItem(nestedContext.AbsolutePath() + '.', formRenderingModel.Controls);

                    renderingModel.Items.Add(record);

                    index++;
                }
            }

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}