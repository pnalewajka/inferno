using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater
{
    public class RepeaterRenderingModel : ControlRenderingModel
    {
        public Type ItemType { get; set; }
        public string ItemTitle { get; set; }
        public string AddButtonText { get; set; }
        public string DeleteButtonText { get; set; }
        public long? MinCount { get; set; }
        public long? MaxCount { get; set; }
        public bool IsCollection { get; set; }
        public bool CanAddRecord { get; set; }
        public bool CanDeleteRecord { get; set; }
        public bool CanReorder { get; set; }
        public bool HideInReadOnlyWhenEmpty { get; set; }
        public RepeaterItem TemplateItem { get; set; }
        public IList<RepeaterItem> Items { get; set; }
        public Layout Layout { get; set; }

        public override ControlVisibility Visibility
        {
            get
            {
                if (HideInReadOnlyWhenEmpty && !Items.Any() && (IsReadOnly || Layout.IsReadOnly()))
                {
                    return ControlVisibility.Remove;
                }

                return base.Visibility;
            }

            set => base.Visibility = value;
        }

        public string GetSummaryContent()
        {
            bool canAddRecord = CanAddRecord;
            bool canDeleteRecord = CanDeleteRecord;
            bool canReorder = CanReorder;

            CanAddRecord = CanDeleteRecord = CanReorder = false;

            var content = GetContent();

            CanAddRecord = canAddRecord;
            CanDeleteRecord = canDeleteRecord;
            CanReorder = canReorder;

            return content;
        }

        public string GetContent()
        {
            return Layout.IsReadOnly()
                ? GetReadOnlyContent()
                : GetHtmlContent();
        }

        private string GetReadOnlyContent()
        {
            bool canAddRecord = CanAddRecord;
            bool canDeleteRecord = CanDeleteRecord;
            bool canReorder = CanReorder;

            CanAddRecord = CanDeleteRecord = CanReorder = false;

            var content = GetHtmlContent();

            CanAddRecord = canAddRecord;
            CanDeleteRecord = canDeleteRecord;
            CanReorder = canReorder;

            return content;
        }

        private string GetHtmlContent()
        {
            var html = new StringBuilder();
            var itemsCount = Items.Count;

            var itemTitle = IsCollection
                                ? HtmlMarkupHelper.RemoveAccelerator(ItemTitle)
                                : string.Empty;

            var mainTitleSource = IsCollection
                                      ? Label
                                      : (Label ?? ItemTitle);

            var mainTitle = HtmlMarkupHelper.HighlightAccelerator(mainTitleSource);

            if (!string.IsNullOrWhiteSpace(mainTitle))
            {
                html.AppendFormat("<legend>{0}</legend>\r\n", mainTitle);
            }

            html.Append("<div class=\"repeater-items\">");

            GetRepeaterItemHtml(TemplateItem.Controls, html, itemTitle, null, itemsCount, true);

            for (var index = 0; index < itemsCount; index++)
            {
                var elementTitle = IsCollection && !string.IsNullOrEmpty(itemTitle)
                    ? $"{itemTitle} {(index + 1)}"
                    : string.Empty;

                GetRepeaterItemHtml(Items[index].Controls, html, elementTitle, index, itemsCount, false);
            }

            html.Append("</div>");

            if (CanAddRecord)
            {
                var buttonText = HtmlMarkupHelper.RemoveAccelerator(AddButtonText ?? CardIndexResources.CardIndexAddRecordButtonText);

                html.Append("<div class=\"repeater-toolbar form-group\">");
                html.AppendFormat("<span class=\"btn btn-link pull-left add\" title=\"{0}\"><span class=\"glyphicon glyphicon-plus\"></span> {0}</span>", buttonText);
                html.Append("</div>");
            }

            return html.ToString();
        }

        private void GetRepeaterItemHtml(IEnumerable<ControlRenderingModel> controls, StringBuilder stringBuilder, string itemTitle, int? index, int itemsCount, bool isTemplate)
        {
            var canMoveDown = CanReorder && (index == null || index + 1 < itemsCount);
            var canMoveUp = CanReorder && (index == null || index > 0);
            var positionData = index == null ? string.Empty : $" data-position=\"{index}\"";

            stringBuilder.AppendFormat("\r\n<fieldset class=\"repeater-item {0}\">\r\n", isTemplate ? "hidden" : string.Empty);
            stringBuilder.Append("<div class=\"item-header\">");
            stringBuilder.Append("<div class=\"pull-right text-right btn-group btn-group-xs\">");
            stringBuilder.Append(CanReorder ? GetButtonHtml(CardIndexResources.CardIndexReorderDownButtonText, "down", "arrow-down", !canMoveDown) : string.Empty);
            stringBuilder.Append(CanReorder ? GetButtonHtml(CardIndexResources.CardIndexReorderUpButtonText, "up", "arrow-up", !canMoveUp) : string.Empty);
            stringBuilder.Append(CanDeleteRecord ? GetButtonHtml(DeleteButtonText ?? CardIndexResources.CardIndexDeleteRecordButtonText, "delete", "remove") : string.Empty);
            stringBuilder.Append("</div>");

            if (!string.IsNullOrWhiteSpace(itemTitle))
            {
                stringBuilder.AppendFormat("<legend{1}>{0}</legend>\r\n", itemTitle, positionData);
            }

            stringBuilder.Append("</div>");

            Layout.RenderContent(controls, stringBuilder);
            stringBuilder.Append("\r\n</fieldset>\r\n");
        }

        private static string GetButtonHtml(string text, string actionName, string actionIcon, bool isHidden = false, string buttonTypeName = "link", bool showText = false)
        {
            text = HtmlMarkupHelper.RemoveAccelerator(text);

            var hiddenClassName = isHidden ? " hidden" : string.Empty;
            var buttonText = showText ? $" {text}" : string.Empty;

            return string.Format("<span class=\"btn btn-{0} {2}{3}\" title=\"{4}\"><span class=\"glyphicon glyphicon-{1}\"></span>{5}</span>", buttonTypeName, actionIcon, actionName, hiddenClassName, text, buttonText);
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            throw new NotImplementedException();
        }
    }
}