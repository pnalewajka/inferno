using System.Collections.Generic;
using System.Linq;
using ClosedXML.Excel;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater
{
    public class RepeaterItem
    {
        public IList<ControlRenderingModel> Controls { get; set; }

        public RepeaterItem(string namePrefix, IEnumerable<ControlRenderingModel> controls)
        {
            Controls = controls.ToList();
            Controls.ForEach(c =>
            {
                c.Id = namePrefix + c.Id;
                c.Name = namePrefix + c.Name;
            });
        }
    }
}