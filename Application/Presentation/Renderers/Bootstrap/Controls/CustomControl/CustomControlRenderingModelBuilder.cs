﻿using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl
{
    public class CustomControlRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var customControlAttribute = AttributeHelper.GetPropertyAttribute<CustomControlAttribute>(propertyInfo);

            if (customControlAttribute == null)
            {
                return null;
            }

            var assemblyNamePrefix = propertyInfo.Module.Assembly.GetName().Name + ".";
            var templatePathPrefix =
                (customControlAttribute.TemplatePath.StartsWith(assemblyNamePrefix)
                    ? string.Empty
                    : assemblyNamePrefix);

            var renderingModel = new CustomControlRenderingModel
            {
                ControlModel = propertyInfo.GetValue(formModel),
                TemplatePath = templatePathPrefix + customControlAttribute.TemplatePath,
                Assembly = propertyInfo.Module.Assembly
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}
