﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CustomControl
{
    public class CustomControlRenderingModel : ControlRenderingModel
    {
        public string TemplatePath { get; set; }

        public Assembly Assembly { get; set; }

        public object ControlModel { get; set; }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return null;
        }

        public string GetTemplatePathByLayout(string layout)
        {
            string templatePathWithLayout = $"{TemplatePath}.{layout}";

            return Assembly.GetManifestResourceNames().Contains($"{templatePathWithLayout}.cshtml")
                ? templatePathWithLayout
                : TemplatePath;
        }

        public override string ToString()
        {
            var enumerable = ControlModel as IEnumerable;

            if (enumerable != null)
            {
                return string.Join(", ", enumerable.Cast<object>().Select(m => m.ToString()));
            }

            return ControlModel.ToString();
        }
    }
}