using System.Reflection;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textbox
{
    public class TextboxRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof(string))
            {
                return null;
            }

            var htmlSanitizeAttribute = AttributeHelper.GetPropertyAttribute<HtmlSanitizeAttribute>(propertyInfo);
            var stringValue = (string)propertyInfo.GetValue(formModel);

            if (htmlSanitizeAttribute != null && htmlSanitizeAttribute.ShouldBeSanitized)
            {
                stringValue = HttpUtility.HtmlDecode(stringValue);
            }

            if (AttributeHelper.HasPropertyAttribute<TrimAttribute>(propertyInfo))
            {
                stringValue = stringValue?.Trim();
            }

            var renderingModel = new TextboxRenderingModel
            {
                Value = stringValue,
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}