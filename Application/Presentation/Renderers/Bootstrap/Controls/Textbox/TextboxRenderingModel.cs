using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textbox
{
    public class TextboxRenderingModel : ControlRenderingModel
    {
        public string InputType => SpecificDataType == DataType.Password ? "password" : "text";

        public string Value { get; set; }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("type", InputType);
            attributes.Add("value", Value);
            attributes.Add("placeholder", Placeholder);

            if (IsRequired)
            {
                attributes.Add("data-val-required", RequiredValueErrorMessage);
            }

            if (HasLengthValidation)
            {
                attributes.Add("data-val-length", WrongLengthExceededErrorMessage);
            }

            if (MinLength.HasValue)
            {
                attributes.Add("data-val-length-min", MinLength);
            }

            if (MaxLength.HasValue)
            {
                attributes.Add("data-val-length-max", MaxLength);
                attributes.Add("maxlength", MaxLength);
            }

            if (SpecificDataType == DataType.Password)
            {
                attributes.Add("autocomplete", "off");
            }
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}