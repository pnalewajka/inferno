using System;
using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DocumentPicker
{
    public class DocumentPickerRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel, HashSet<string> currentUserRoles)
        {
            var propertyType = propertyInfo.PropertyType;

            if (propertyType != typeof(DocumentViewModel)
                && propertyType != typeof(IList<DocumentViewModel>)
                && propertyType != typeof(List<DocumentViewModel>)
                && propertyType != typeof(DocumentViewModel[])
                )
            {
                return null;
            }

            if (propertyType == typeof(DocumentViewModel[]))
            {
                throw new NotSupportedException("DocumentViewModel[] is not supported. Please use List<DocumentViewModel>");
            }

            var isMultiValue = typeof(System.Collections.IEnumerable).IsAssignableFrom(propertyType);
            var propertyValue = propertyInfo.GetValue(formModel);
            var fileList = new List<DocumentViewModel>();
            var fileUploadAttribute = AttributeHelper.GetMemberAttribute<DocumentUploadAttribute>(propertyInfo);

            if (fileUploadAttribute == null)
            {
                throw new AttributeNotFoundException<DocumentUploadAttribute>();
            }

            if (!isMultiValue)
            {
                var file = propertyValue as DocumentViewModel;

                if (file != null)
                {
                    fileList.Add(file);
                }
            }
            else
            {
                var enumerableList = propertyValue as IEnumerable<DocumentViewModel>;

                if (enumerableList != null)
                {
                    fileList.AddRange(enumerableList);
                }
            }

            var mappingIdentifier = fileUploadAttribute.DocumentMappingType != null
                                        ? IdentifierHelper.GetTypeIdentifier(fileUploadAttribute.DocumentMappingType)
                                        : null;

            var downloadUrlTemplate = fileUploadAttribute.DownloadUrlTemplate ?? "/DocumentDownload/Download/{0}/{1}";

            var renderingModel = new DocumentPickerRenderingModel
            {
                CanDownload = currentUserRoles.Contains(SecurityRoleType.CanDownloadDocuments.ToString()),
                CanUpload = currentUserRoles.Contains(SecurityRoleType.CanUploadDocuments.ToString()),
                Items = fileList,
                IsMultiValue = isMultiValue,
                Size = isMultiValue ? Size.Large : Size.Medium,
                DocumentMappingClassIdentifier = mappingIdentifier,
                DownloadDocumentLinkTemplate = downloadUrlTemplate,
                ShouldUseDownloadDialog = fileUploadAttribute.ShouldUseDownloadDialog,
                AcceptableContentTypes = fileUploadAttribute.AcceptableContentTypes
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}