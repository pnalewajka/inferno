using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DocumentPicker
{
    public class DocumentPickerRenderingModel : ControlRenderingModel
    {
        public List<DocumentViewModel> Items { get; set; }

        public bool IsMultiValue { get; set; }

        public string DocumentMappingClassIdentifier { get; set; }

        public string DownloadDocumentLinkTemplate { get; set; }

        public bool ShouldUseDownloadDialog { get; set; }

        public string AcceptableContentTypes { get; set; }
        
        public bool CanDownload { get; set; }
        
        public bool CanUpload { get; set; }

        public DocumentPickerRenderingModel()
        {
            CanBeImported = false;
            CanUseInSummary = true;

            Items = new List<DocumentViewModel>();
        }

        public string GetDownloadLink(DocumentViewModel documentViewModel)
        {
            return string.Format(DownloadDocumentLinkTemplate,
                DocumentMappingClassIdentifier,
                documentViewModel.DocumentId);
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return string.Join(", ", Items.Select(i => i.DocumentId));
        }

        public string GetItemNamePrefix(long index)
        {
            if (IsMultiValue)
            {
                return $"{Name}[{index}]";
            }

            return Name;
        }
    }
}