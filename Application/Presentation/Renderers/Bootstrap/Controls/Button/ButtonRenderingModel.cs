using System;
using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Button
{
    public class ButtonRenderingModel : ControlRenderingModel
    {
        public string Caption { get; set; }

        public ButtonRenderingModel()
        {
            CanBeImported = false;
            CanUseInSummary = false;
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            throw new NotImplementedException();
        }
    }
}