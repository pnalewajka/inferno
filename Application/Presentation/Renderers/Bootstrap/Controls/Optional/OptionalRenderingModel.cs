﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Optional
{
    public partial class OptionalRenderingModel : ControlRenderingModel
    {
        public OptionalRenderingModel(object model, PropertyInfo propertyInfoWithAttributes, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            IsSet = (model as IOption)?.IsSet ?? false;
            InlineHtml = new Lazy<string>(() => GetHtmlForLayout<InlineLayout>(model, propertyInfoWithAttributes, context, currentUserRoles));
            SummaryHtml = new Lazy<string>(() => GetHtmlForLayout<SummaryLayout>(model, propertyInfoWithAttributes, context, currentUserRoles));
        }

        public bool IsSet { get; private set; }

        public Lazy<string> InlineHtml { get; private set; }

        public Lazy<string> SummaryHtml { get; private set; }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            throw new NotImplementedException();
        }

        private string GetHtmlForLayout<TLayout>(object model, PropertyInfo propertyInfoWithAttributes, ModelBuilderContext context, HashSet<string> currentUserRoles)
            where TLayout : Layout
        {
            var layout = (Layout)ReflectionHelper.CreateInstance(typeof(TLayout));
            var modelPropertyInfo = model.GetType().GetProperties().SingleOrDefault(x => x.Name.Equals("Value", StringComparison.InvariantCultureIgnoreCase));
            var propertyInfo = new ReflectingPropertyInfo(modelPropertyInfo, propertyInfoWithAttributes);

            var controlRenderingModel = layout.BuildControlRenderingModel(propertyInfo, model, ControlVisibility.Show, context, currentUserRoles);

            controlRenderingModel.Id = $"{Name}.{controlRenderingModel.Id}";
            controlRenderingModel.Name = $"{Name}.{controlRenderingModel.Name}";
            controlRenderingModel.Errors = Errors;
            controlRenderingModel.Tooltip = Tooltip;

            var builder = new StringBuilder();

            layout.RenderContent(new[] { controlRenderingModel }, builder);

            var result = builder.ToString();

            if (typeof(TLayout) == typeof(SummaryLayout))
            {
                const string summaryLayoutPrefix = "<div class=\"row\">\r\n";
                const string summaryLayoutSuffix = "</div>\r\n";

                result = result.StartsWith(summaryLayoutPrefix) ? result.Substring(summaryLayoutPrefix.Length) : result;
                result = result.EndsWith(summaryLayoutSuffix) ? result.Substring(0, result.Length - summaryLayoutSuffix.Length) : result;
            }

            return result;
        }
    }
}
