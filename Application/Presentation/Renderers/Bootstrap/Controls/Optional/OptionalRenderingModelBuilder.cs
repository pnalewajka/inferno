﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Optional
{
    public class OptionalRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            if (!propertyInfo.PropertyType.IsGenericType || propertyInfo.PropertyType.GetGenericTypeDefinition() != typeof(Optional<>))
            {
                return null;
            }

            var optionalValue = propertyInfo.GetValue(formModel) ?? ReflectionHelper.CreateInstance(propertyInfo.PropertyType);
            var renderingModel = new OptionalRenderingModel(optionalValue, propertyInfo, context, currentUserRoles);

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}
