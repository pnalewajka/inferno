﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Optional
{
    partial class OptionalRenderingModel
    {
        private class ReflectingPropertyInfo : PropertyInfo
        {
            private readonly PropertyInfo _propertyInfo;
            private readonly PropertyInfo _propertyInfoWithAttributes;

            public ReflectingPropertyInfo(PropertyInfo propertyInfo, PropertyInfo propertyInfoWithAttributes)
            {
                _propertyInfo = propertyInfo;
                _propertyInfoWithAttributes = propertyInfoWithAttributes;
            }

            public override PropertyAttributes Attributes
            {
                get { return _propertyInfo.Attributes; }
            }

            public override bool CanRead
            {
                get { return _propertyInfo.CanRead; }
            }

            public override bool CanWrite
            {
                get { return _propertyInfo.CanWrite; }
            }

            public override Type DeclaringType
            {
                get { return _propertyInfo.DeclaringType; }
            }

            public override string Name
            {
                get { return _propertyInfo.Name; }
            }

            public override Type PropertyType
            {
                get { return _propertyInfo.PropertyType; }
            }

            public override Type ReflectedType
            {
                get { return _propertyInfo.ReflectedType; }
            }

            public override MethodInfo[] GetAccessors(bool nonPublic)
            {
                return _propertyInfo.GetAccessors(nonPublic);
            }

            public override IEnumerable<CustomAttributeData> CustomAttributes
            {
                get { return _propertyInfoWithAttributes.CustomAttributes; }
            }

            public override object[] GetCustomAttributes(bool inherit)
            {
                return _propertyInfoWithAttributes.GetCustomAttributes(inherit);
            }

            public override object[] GetCustomAttributes(Type attributeType, bool inherit)
            {
                return _propertyInfoWithAttributes.GetCustomAttributes(attributeType, inherit);
            }

            public override IList<CustomAttributeData> GetCustomAttributesData()
            {
                return _propertyInfoWithAttributes.GetCustomAttributesData();
            }

            public override MethodInfo GetGetMethod(bool nonPublic)
            {
                return _propertyInfo.GetGetMethod(nonPublic);
            }

            public override ParameterInfo[] GetIndexParameters()
            {
                return _propertyInfo.GetIndexParameters();
            }

            public override MethodInfo GetSetMethod(bool nonPublic)
            {
                return _propertyInfo.GetSetMethod(nonPublic);
            }

            public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
            {
                return _propertyInfo.GetValue(obj, invokeAttr, binder, index, culture);
            }

            public override bool IsDefined(Type attributeType, bool inherit)
            {
                return _propertyInfo.IsDefined(attributeType, inherit);
            }

            public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
            {
                _propertyInfo.SetValue(obj, value, invokeAttr, binder, index, culture);
            }

            public override int MetadataToken
            {
                get { return _propertyInfo.MetadataToken; }
            }

            public override Module Module
            {
                get { return _propertyInfo.Module; }
            }
        }
    }
}
