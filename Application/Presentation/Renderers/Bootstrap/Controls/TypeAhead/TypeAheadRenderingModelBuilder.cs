﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.TypeAhead
{
    public class TypeAheadRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var typeAhreadAttribute = AttributeHelper.GetPropertyAttribute<TypeAheadAttribute>(propertyInfo);
            var propertyType = propertyInfo.PropertyType;

            if (typeAhreadAttribute == null
                || (propertyType != typeof(long)
                && propertyType != typeof(long?)
                && propertyType != typeof(IList<long>)
                && propertyType != typeof(List<long>)
                && propertyType != typeof(long[])))
            {
                return null;
            }

            var ids = GetSelectedIds(propertyInfo, formModel, propertyType);
            var cardControllerType = typeAhreadAttribute.Type;
            var controller = (IValuePicker)ReflectionHelper.CreateInstanceWithIocDependencies(cardControllerType);

            var listItems = controller.GetListItems(ids.ToArray(), typeAhreadAttribute.FormFormatterType);
            var isMultiValue = typeof(System.Collections.IEnumerable).IsAssignableFrom(propertyType);

            var renderingModel = new TypeAheadRenderingModel
            {
                Items = listItems,
                IsMultiValue = isMultiValue,
                IsClearButtonAvailable = (isMultiValue || TypeHelper.IsNullable(propertyType))
                                         && !typeAhreadAttribute.HideClearButton,
                ListTypeIdentifier = IdentifierHelper.GetTypeIdentifier(cardControllerType),
                FormFormatterTypeIdentifier = typeAhreadAttribute.FormFormatterType != null
                    ? IdentifierHelper.GetTypeIdentifier(typeAhreadAttribute.FormFormatterType)
                    : string.Empty,
                Size = isMultiValue ? Size.Large : Size.Medium,
                OnResolveUrlJavaScript = typeAhreadAttribute.OnResolveUrlJavaScript,
                OnSelectionChangedJavaScript = typeAhreadAttribute.OnSelectionChangedJavaScript,
                MinimumInputLength = typeAhreadAttribute.MinimumInputLength
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }

        private static List<long> GetSelectedIds(PropertyInfo propertyInfo, object formModel, Type propertyType)
        {
            var ids = new List<long>();

            if (propertyType == typeof(long))
            {
                var value = (long)propertyInfo.GetValue(formModel);

                if (value != 0)
                {
                    ids.Add(value);
                }
            }

            if (propertyType == typeof(long?))
            {
                var value = (long?)propertyInfo.GetValue(formModel);

                if (value.HasValue)
                {
                    ids.Add(value.Value);
                }
            }

            if (propertyType == typeof(long[]))
            {
                var array = (long[])propertyInfo.GetValue(formModel);

                if (array != null)
                {
                    ids.AddRange(array);
                }
            }

            if (propertyType == typeof(IList<long>) || propertyType == typeof(List<long>))
            {
                var list = (IList<long>)propertyInfo.GetValue(formModel);

                if (list != null)
                {
                    ids.AddRange(list);
                }
            }

            return ids;
        }
    }
}
