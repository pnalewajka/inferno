﻿using System.Collections.Generic;
using System.Linq;
using RazorEngine.Text;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.TypeAhead
{
    public class TypeAheadRenderingModel : ControlRenderingModel
    {
        public IList<ListItem> Items { get; set; }

        public bool IsMultiValue { get; set; }
        public bool IsClearButtonAvailable { get; set; }
        public string ListTypeIdentifier { get; set; }
        public string FormFormatterTypeIdentifier { get; set; }
        public string OnResolveUrlJavaScript { get; set; }
        public string OnSelectionChangedJavaScript { get; set; }
        public int MinimumInputLength { get; set; }

        public TypeAheadRenderingModel()
        {
            Items = new List<ListItem>();
        }

        public List<long> Ids
        {
            get
            {
                return Items.Select(i => i.Id)
                    .Cast<long>()
                    .ToList();
            }
        }

        public List<string> DisplayNames
        {
            get
            {
                return Items
                    .Select(i => i.DisplayName)
                    .ToList();
            }
        }

        public string GetDisplayName()
        {
            return string.Join(", ", DisplayNames);
        }

        public string GetMultiValueClass()
        {
            return IsMultiValue ? "is-multivalue" : string.Empty;
        }

        public string PickerButtonClass => IsReadOnly ? "disabled-icon" : string.Empty;

        public bool IsNoValueAllowed => IsMultiValue || IsClearButtonAvailable;

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Remove("id");
            attributes.Remove("class");

            attributes.Add("type", "hidden");
            attributes.Add("class", "hidden-but-validate");
            attributes.Add("value", string.Join(",", Ids));
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return string.Join(",", Ids);
        }

        public string ItemContainerClass
            => IsClearButtonAvailable ? "item-container-with-button" : "item-container-without-button";

        public RawString GetControlDataAttributes()
        {
            TagAttributes attributes = new TagAttributes();

            attributes.Add("data-field-name", Name);
            attributes.Add("data-field-id", Id);
            attributes.Add("data-list-id", ListTypeIdentifier);
            attributes.Add("data-formatter-id", FormFormatterTypeIdentifier);
            attributes.Add("data-on-resolve-url", OnResolveUrlJavaScript);
            attributes.Add("data-on-selection-changed", OnSelectionChangedJavaScript);
            attributes.Add("data-minimum-input-length", MinimumInputLength);
            attributes.Add("data-prompt", Placeholder);
            attributes.Add("data-is-readonl", IsReadOnly ? "true" : "false");
            attributes.Add("data-is-no-value-allowed", IsNoValueAllowed ? "true" : "false");
            attributes.Add("data-is-multivalue", IsMultiValue ? "true" : "false");

            return new RawString(attributes.Render());
        }
    }
}
