using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ColorPicker
{
    public class ColorPickerRenderingModelBuilder
    {
        private const ColorPickerFormat DefaultFormat = ColorPickerFormat.Hex;

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof (string))
            {
                return null;
            }

            var colorPickerAttribute = AttributeHelper.GetPropertyAttribute<ColorPickerAttribute>(propertyInfo);

            if (colorPickerAttribute == null)
            {
                return null;
            }

            var colorPickerModeAttribute = AttributeHelper.GetPropertyAttribute<ColorPickerAttribute>(propertyInfo);

            if (colorPickerModeAttribute == null)
            {
                colorPickerModeAttribute = new ColorPickerAttribute(DefaultFormat);
            }

            var renderingModel = new ColorPickerRenderingModel()
            {
                Value = (string) propertyInfo.GetValue(formModel), 
                ColorPickerFormat = colorPickerModeAttribute.Format,
                Size = Size.Small
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}