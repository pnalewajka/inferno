using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ColorPicker
{
    public class ColorPickerRenderingModel : TextareaRenderingModel
    {
        public ColorPickerFormat ColorPickerFormat { get; set; }

        public string PickerButtonClass => IsReadOnly ? "disabled-icon" : string.Empty;

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);
            attributes.Add("value", Value);
            attributes.Add("data-format", ColorPickerFormat);
            attributes.Add("form-control", true);
        }

        protected override void AddReadOnlyAttributes(TagAttributes attributes)
        {
            base.AddReadOnlyAttributes(attributes);

            attributes.Add("data-readonly", "true");
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}