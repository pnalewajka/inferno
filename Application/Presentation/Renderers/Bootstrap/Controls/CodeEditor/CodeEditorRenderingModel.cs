using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CodeEditor
{
    public class CodeEditorRenderingModel : TextareaRenderingModel
    {
        public SyntaxType SyntaxType { get; set; }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("data-ace-editor", true);
            attributes.Add("data-ace-editor-mode", GetEditorMode());
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }

        private string GetEditorMode()
        {
            return $"ace/mode/{SyntaxType.ToString().ToLowerInvariant()}";
        }
    }
}