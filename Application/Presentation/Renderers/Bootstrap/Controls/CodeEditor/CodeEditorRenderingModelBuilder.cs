using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CodeEditor
{
    public class CodeEditorRenderingModelBuilder
    {
        private const byte DefaultRows = 10;

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof (string))
            {
                return null;
            }

            var codeEditorAttribute = AttributeHelper.GetPropertyAttribute<CodeEditorAttribute>(propertyInfo);

            if (codeEditorAttribute == null)
            {
                return null;
            }

            var multilineAttribute = AttributeHelper.GetPropertyAttribute<MultilineAttribute>(propertyInfo);

            if (multilineAttribute == null)
            {
                multilineAttribute = new MultilineAttribute(DefaultRows);
            }

            var renderingModel = new CodeEditorRenderingModel
            {
                Value = (string) propertyInfo.GetValue(formModel), 
                Rows = multilineAttribute.Rows ?? DefaultRows,
                SyntaxType = codeEditorAttribute.SyntaxType
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}