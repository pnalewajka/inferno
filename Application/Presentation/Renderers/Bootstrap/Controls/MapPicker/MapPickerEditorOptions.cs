namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.MapPicker
{
    public class MapPickerEditorOptions
    {
        public int Height { get; set; }
        public bool IsLocationNameHidden { get; set; }
        public bool IsRadiusHidden { get; set; }
        public bool IsLatLngHidden { get; set; }
        public bool IsSearchHidden { get; set; }
    }
}