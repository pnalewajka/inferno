using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.MapPicker
{
    public class MapPickerRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var propertyType = propertyInfo.PropertyType;

            if (!TypeHelper.IsTypeOrNullableType<GeographicLocationViewModel>(propertyType))
            {
                return null;
            }

            var systemParametersService = ReflectionHelper.ResolveInterface<ISystemParameterService>();

            var mapPickerAttribute = AttributeHelper.GetPropertyAttribute<MapPickerAttribute>(propertyInfo);

            var radiusRangeAttribute =
                AttributeHelper.GetPropertyAttribute<RangeAttribute>(
                    PropertyHelper.GetProperty<GeographicLocationViewModel>(g => g.Radius));

            var renderingModel = new MapPickerRenderingModel
            {
                IsNullable = TypeHelper.IsNullable(propertyType),
                IsInPlaceMapPicker = mapPickerAttribute == null || mapPickerAttribute.IsInPlaceEditor,
                EditorOptions = new MapPickerEditorOptions
                {
                    IsLatLngHidden = mapPickerAttribute!= null && mapPickerAttribute.EditorIsLatLngHidden,
                    IsLocationNameHidden = mapPickerAttribute != null && mapPickerAttribute.EditorIsLocationNameHidden,
                    IsRadiusHidden = mapPickerAttribute != null && mapPickerAttribute.EditorIsRadiusHidden,
                    IsSearchHidden = mapPickerAttribute != null && mapPickerAttribute.EditorIsSearchHidden,
                    Height = mapPickerAttribute != null && mapPickerAttribute.Height > 0 ? mapPickerAttribute.Height : systemParametersService.GetParameter<int>(ParameterKeys.MapPickerDefaultEditorHeight)
                },
                PopupEditorOptions = new MapPickerPopupEditorOptions
                {
                    IsLatLngHidden = mapPickerAttribute!= null && mapPickerAttribute.PopupEditorIsLatLngHidden,
                    IsLocationNameHidden = mapPickerAttribute != null && mapPickerAttribute.PopupEditorIsLocationNameHidden,
                    IsRadiusHidden = mapPickerAttribute != null && mapPickerAttribute.PopupEditorIsRadiusHidden,
                    IsSearchHidden = mapPickerAttribute != null && mapPickerAttribute.PopupEditorIsSearchHidden,
                    Height = mapPickerAttribute != null && mapPickerAttribute.Height > 0 ? mapPickerAttribute.Height : systemParametersService.GetParameter<int>(ParameterKeys.MapPickerDefaultPopupEditorHeight),
                    Title = (mapPickerAttribute != null ? mapPickerAttribute.PopupTitle : null) ?? propertyInfo.Name
                },
                Location = (GeographicLocationViewModel?)propertyInfo.GetValue(formModel),
                RadiusMinValue = (int)radiusRangeAttribute.Minimum,
                RadiusMaxValue = (int)radiusRangeAttribute.Maximum
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}