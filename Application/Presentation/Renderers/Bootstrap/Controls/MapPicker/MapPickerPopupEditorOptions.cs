namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.MapPicker
{
    public class MapPickerPopupEditorOptions : MapPickerEditorOptions
    {
        /// <summary>
        /// Popup title
        /// </summary>
        public string Title { get; set; }
    }
}