using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.MapPicker
{
    public class MapPickerRenderingModel : ControlRenderingModel
    {
        public GeographicLocationViewModel? Location { get; set; }

        public bool IsInPlaceMapPicker { get; set; }
        public bool IsNullable { get; set; }

        public MapPickerEditorOptions EditorOptions { get; set; }
        public MapPickerPopupEditorOptions PopupEditorOptions { get; set; }

        public int RadiusMinValue { get; set; }
        public int RadiusMaxValue { get; set; }

        public MapPickerRenderingModel()
        {
            CanBeImported = false;
            CanUseInSummary = false;

            IsInPlaceMapPicker = true;
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return null;
        }

        public string GetRadiusValidationMessage()
        {
            return string.Format(MapPickerResources.RadiusValueOutOfRangeFormat, RadiusMinValue, RadiusMaxValue);
        }
    }
}