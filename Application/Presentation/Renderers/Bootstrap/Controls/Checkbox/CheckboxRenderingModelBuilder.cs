using System.Reflection;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Checkbox
{
    public class CheckboxRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof(bool))
            {
                return null;
            }

            var renderingModel = new CheckboxRenderingModel
            {
                Value = (bool) propertyInfo.GetValue(formModel)
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}