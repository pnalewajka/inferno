using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Checkbox
{
    public class CheckboxRenderingModel : ControlRenderingModel
    {
        public bool Value { get; set; }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Remove("class");

            attributes.Add("type", "checkbox");
            attributes.Add("value", "true");

            if (Value)
            {
                attributes.Add("checked", "checked");
            }
        }   

        protected override void AddReadOnlyAttributes(TagAttributes attributes)
        {
            attributes.Add("disabled");
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}