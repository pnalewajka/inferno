using System;
using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CropImageUpload
{
    public class CropImageUploadRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel, HashSet<string> currentUserRoles)
        {
            if (formModel == null)
            {
                throw new ArgumentNullException(nameof(formModel));
            }

            var cropImageUploadAttribute = AttributeHelper.GetMemberAttribute<CropImageUploadAttribute>(propertyInfo);

            if (cropImageUploadAttribute == null)
            {
                return null;
            }

            var identifierAttribute = AttributeHelper.GetClassAttribute<IdentifierAttribute>(formModel.GetType());

            if (identifierAttribute == null)
            {
                throw new AttributeNotFoundException<IdentifierAttribute>();
            }

            var renderingModel = new CropImageUploadRenderingModel
            {
                PropertyName = propertyInfo.Name,
                ViewModelIdentifier = identifierAttribute.Value,

                CanDownload = currentUserRoles.Contains(SecurityRoleType.CanDownloadDocuments.ToString()),
                CanUpload = currentUserRoles.Contains(SecurityRoleType.CanUploadDocuments.ToString()),

                AcceptableContentTypes = cropImageUploadAttribute.AcceptableContentTypes,

                DocumentMappingClassIdentifier = cropImageUploadAttribute.DocumentMappingType != null ? IdentifierHelper.GetTypeIdentifier(cropImageUploadAttribute.DocumentMappingType) : null,
                DownloadDocumentLinkTemplate = "{0}/{1}",

                DocumentViewModel = (DocumentViewModel)propertyInfo.GetValue(formModel)
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}