using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.CropImageUpload
{
    public class CropImageUploadRenderingModel : ControlRenderingModel
    {
        public bool CanDownload { get; set; }
        public bool CanUpload { get; set; }

        public string PropertyName { get; set; }
        public string ViewModelIdentifier { get; set; }

        public DocumentViewModel DocumentViewModel { get; set; }

        public string DocumentMappingClassIdentifier { get; set; }
        public string DownloadDocumentLinkTemplate { get; set; }

        public string AcceptableContentTypes { get; set; }

        public CropImageUploadRenderingModel()
        {
            CanBeImported = false;
            CanUseInSummary = false;
        }
        
        public string GetContent()
        {
            return null;
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            throw new NotImplementedException();
        }


        public string GetDownloadLink(DocumentViewModel documentViewModel)
        {
            return string.Format(DownloadDocumentLinkTemplate,
                DocumentMappingClassIdentifier,
                documentViewModel.DocumentId);
        }

        public string GetItemNamePrefix()
        {
            return Name;
        }
    }
}