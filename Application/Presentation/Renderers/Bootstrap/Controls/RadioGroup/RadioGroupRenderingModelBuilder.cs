using System;
using System.Reflection;

using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RadioGroup
{
    public class RadioGroupRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            var radioGroupAttribute = AttributeHelper.GetPropertyAttribute<RadioGroupAttribute>(propertyInfo);

            if (radioGroupAttribute == null)
            {
                return null;
            }

            var propertyValue = propertyInfo.GetValue(formModel);

            var renderingModel = new RadioGroupRenderingModel
            {
                Value = propertyValue == null ? null : (long?)Convert.ToInt64(propertyValue),
                Items = radioGroupAttribute.GetItems(),
                EmptyItemBehavior = radioGroupAttribute.EmptyItemBehavior
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}