using System.Reflection;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Timestamp
{
    public class TimestampRenderingModelBuilder
    {
        private const string TimestampPropertyName = "Timestamp";

        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType != typeof(byte[])
                && propertyInfo.Name != TimestampPropertyName)
            {
                return null;
            }

            var renderingModel = new TimestampRenderingModel
            {
                Value = (byte[])propertyInfo.GetValue(formModel),
            };

            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }
    }
}