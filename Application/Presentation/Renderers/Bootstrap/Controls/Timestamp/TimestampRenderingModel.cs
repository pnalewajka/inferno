using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Timestamp
{
    public class TimestampRenderingModel : ControlRenderingModel
    {
        public byte[] Value { get; set; }

        public TimestampRenderingModel()
        {
            CanBeImported = false;
            CanUseInSummary = false;
        }

        public string GetHtmlValue()
        {
            return Value == null
                       ? string.Empty
                       : Convert.ToBase64String(Value);
        }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            base.AddStandardInputTagAttributes(attributes);

            attributes.Add("value", GetHtmlValue());
            attributes.Add("type", "hidden");

            attributes.Remove("class");
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            yield return Value;
        }
    }
}