﻿using System.Collections.Generic;
using System.Reflection;
using Castle.Core.Internal;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using CommonModels = Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.LocalizedString
{
    public class LocalizedStringRenderingModelBuilder
    {
        public static ControlRenderingModel GetModelForProperty(PropertyInfo propertyInfo, object formModel)
        {
            if (propertyInfo.PropertyType.BaseType != typeof(CommonModels.LocalizedStringBase))
            {
                return null;
            }

            var value = (CommonModels.LocalizedStringBase)propertyInfo.GetValue(formModel);

            if (value == null)
            {
                value = propertyInfo.PropertyType.CreateInstance<CommonModels.LocalizedStringBase>();
            }

            var localizedStringValues = GetLanguageValues(propertyInfo, value);
            var renderingModel = new LocalizedStringRenderingModel(localizedStringValues, value.ToString());
            ControlRenderingModelBuilder.BuildCommonAttributes(propertyInfo, renderingModel, formModel);

            return renderingModel;
        }

        private static IDictionary<string, LocalizedStringItem> GetLanguageValues(PropertyInfo propertyInfo, CommonModels.LocalizedStringBase value)
        {
            var idPrefix = NamingConventionHelper.ConvertPascalCaseToHyphenated(propertyInfo.Name);

            var values = DictionaryHelper.ToDictionary<string, LocalizedStringItem, string>(
                         value,
                         x => x,
                         (p, x) => { return GetLocalizedStringItemFromProperty(p, x, propertyInfo, idPrefix); });

            return values;
        }

        private static LocalizedStringItem GetLocalizedStringItemFromProperty(PropertyInfo localizedStringPropertyInfo, string itemValue, PropertyInfo modelItemPropertyInfo, string idPrefix)
        {
            var descriptionAttribute =
                AttributeHelper.GetPropertyAttribute<DescriptionLocalizedAttribute>(localizedStringPropertyInfo);

            return new LocalizedStringItem(descriptionAttribute.Description,
                                           itemValue,
                                           $"{modelItemPropertyInfo.Name}.{localizedStringPropertyInfo.Name}",
                                           $"{idPrefix}-{localizedStringPropertyInfo.Name}");
        }
    }
}
