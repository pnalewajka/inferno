﻿namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.LocalizedString
{
    public class LocalizedStringItem
    {
        public string Id { get; private set; }

        public string Label { get; private set; }

        public string Name { get; private set; }

        public string Value { get; private set; }

        public LocalizedStringItem(string label, string value, string name, string id)
        {
            Label = label;
            Value = value;
            Name = name;
            Id = id;
        }
    }
}
