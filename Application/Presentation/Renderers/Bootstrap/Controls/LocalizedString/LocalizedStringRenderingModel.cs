﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.LocalizedString
{
    public class LocalizedStringRenderingModel : ControlRenderingModel
    {
        public IDictionary<string, LocalizedStringItem> LocalizedStringValues { get; private set; }

        public string DefaultValue { get; private set; }

        public override IEnumerable<string> GetImportTemplateColumnNames()
        {
            foreach (var item in LocalizedStringValues.Values)
            {
                yield return $"{Label} ({item.Label})";
            }
        }

        public override IEnumerable<object> GetImportTemplateValues()
        {
            return LocalizedStringValues.Values.Select(v => v.Value).ToList();
        }

        public LocalizedStringRenderingModel(IDictionary<string, LocalizedStringItem> localizedStringValues, string defaultValue)
        {
            LocalizedStringValues = localizedStringValues;
            DefaultValue = defaultValue;
        }

        protected override void AddStandardInputTagAttributes(TagAttributes attributes)
        {
            attributes.Add("class", "form-control");

            if (IsReadOnly)
            {
                AddReadOnlyAttributes(attributes);
            }

            if (HasValidation)
            {
                attributes.Add("data-val", "true");
            }

            if (IsRequired)
            {
                attributes.Add("data-val-required", RequiredValueErrorMessage);
            }

            if (HasLengthValidation)
            {
                attributes.Add("data-val-length", WrongLengthExceededErrorMessage);
            }

            if (MinLength.HasValue)
            {
                attributes.Add("data-val-length-min", MinLength);
            }

            if (MaxLength.HasValue)
            {
                attributes.Add("data-val-length-max", MaxLength);
                attributes.Add("maxlength", MaxLength);
            }
        }
    }
}
