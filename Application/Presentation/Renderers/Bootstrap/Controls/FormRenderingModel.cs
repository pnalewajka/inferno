using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Repeater;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Controls
{
    public class FormRenderingModel
    {
        public IEnumerable<string> Scripts { get; protected set; }

        public IEnumerable<ControlRenderingModel> Controls { get; protected set; }

        public object ViewModel { get; set; }

        public FormRenderingModel(IEnumerable<ControlRenderingModel> controls, object viewModel, IEnumerable<string> scripts = null)
        {
            Controls = controls;
            ViewModel = viewModel;
            Scripts = scripts ?? Enumerable.Empty<string>();
        }

        public ControlRenderingModel GetControlByName<TViewModel>(Expression<Func<TViewModel, object>> propertyNameExpression)
        {
            var memberInfos = PropertyHelper.GetPropertyPathMemberInfo(propertyNameExpression).ToList();
            var currentPath = new List<string>();

            ControlRenderingModel control = null;
            var currentControls = Controls;

            for (var i = 0; i < memberInfos.Count; i++)
            {
                var memberInfo = memberInfos[i];
                var isLastPathSegment = memberInfos.Count - 1 == i;

                currentPath.Add(memberInfo.Name);
                control = currentControls.Single(m => m.Name == string.Join(".", currentPath));

                if (!isLastPathSegment && control is RepeaterRenderingModel repeater)
                {
                    currentControls = repeater.Items.SingleOrDefault()?.Controls ?? new ControlRenderingModel[0];
                }
            }

            return control;
        }

        public TViewModel GetViewModel<TViewModel>() where TViewModel : class
        {
            return ViewModel as TViewModel;
        }

        public void HideControlsByTabId(string tabId, ControlVisibility visibility = ControlVisibility.Hide, bool clearTabId = true)
        {
            if (visibility == ControlVisibility.Show)
            {
                throw new ArgumentOutOfRangeException(nameof(visibility));
            }

            Controls.Where(c => c.TabId == tabId).ToList()
                .ForEach(c => {
                    c.Visibility = visibility;

                    if (clearTabId)
                    {
                        c.TabId = null;
                    }
                });
        }
    }
}