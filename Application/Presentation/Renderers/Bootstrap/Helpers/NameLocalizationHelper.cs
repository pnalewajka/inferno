﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using System.Linq;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers
{
    public static class NameLocalizationHelper
    {
        public static Dictionary<string, string> GetPropertyLabels(Type destinationType)
        {
            var dictionary = new Dictionary<string, string>();

            foreach (var property in TypeHelper.GetPublicInstanceProperties(destinationType))
            {
                var attribute = AttributeHelper.GetPropertyAttribute<DisplayNameLocalizedAttribute>(property);
                dictionary.Add(property.Name, attribute == null ? property.Name : attribute.DisplayName);
            }

            return dictionary;
        }

        public static string GetPropertyLabel(Type destinationType, string propertyName)
        {
            var properties = TypeHelper.GetPublicInstanceProperties(destinationType).Where(p => p.Name == propertyName).ToArray();

            if (properties.Length != 1)
            {
                return propertyName;
            }

            var property = properties.Single();
            var attribute = AttributeHelper.GetPropertyAttribute<DisplayNameLocalizedAttribute>(property);

            return attribute == null ? property.Name : attribute.DisplayName;
        }
    }
}
