﻿namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers
{
    public static class JavaScriptTypeConversionHelper
    {
        /// <summary>
        /// Converts boolean to javascript boolean
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToString(bool value)
        {
            return value.ToString().ToLower();
        }
    }
}
