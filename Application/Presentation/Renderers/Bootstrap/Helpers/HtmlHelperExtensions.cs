﻿using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static Bootstrap Bootstrap(this HtmlHelper htmlHelper)
        {
            return new Bootstrap(htmlHelper.ViewData);
        }
    }
}