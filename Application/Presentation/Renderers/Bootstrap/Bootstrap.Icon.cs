﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap
{
    public partial class Bootstrap
    {
        public MvcHtmlString Icon(string shotIconName)
        {
            return new MvcHtmlString(Icon(shotIconName, null));
        }

        public static string Icon(string shotIconName, string additionalCssClass)
        {
            var cssClass = IconHelper.GetCssClass(shotIconName);

            if (cssClass == null)
            {
                return null;
            }

            var additionalCssClassInsert = string.IsNullOrWhiteSpace(additionalCssClass)
                ? null
                : $" {additionalCssClass}";

            return $"<i class=\"{cssClass}{additionalCssClassInsert}\" aria-hidden=\"true\"></i>";
        }
    }
}
