﻿namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Enums
{
    public enum LabelWidth : byte
    {
        Default = 2,
        Large = 5
    }
}