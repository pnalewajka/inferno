﻿namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Enums
{
    public enum SyntaxType
    {
        Html,
        Razor,
        TypeScript,
        JavaScript,
        Xml,
        Sql
    }
}