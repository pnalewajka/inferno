﻿namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Enums
{
    /// <summary>
    /// Indicates how to deal with different ways of hidding controls
    /// </summary>
    public enum ControlVisibility
    {
        /// <summary>
        /// Control isn't modified
        /// </summary>
        Show,
        /// <summary>
        /// Control is hidden via css class collapse
        /// </summary>
        Hide,
        /// <summary>
        /// Control is entirely removed from resulting web page
        /// </summary>
        Remove
    }
}
