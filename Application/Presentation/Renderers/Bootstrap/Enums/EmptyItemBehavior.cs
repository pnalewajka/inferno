namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Enums
{
    /// <summary>
    /// Describes the behavior for the empty item from drop down
    /// </summary>
    public enum EmptyItemBehavior
    {
        /// <summary>
        /// Empty option should not be available
        /// </summary>
        NotAvailable,

        /// <summary>
        /// Empty option should be available for choice
        /// </summary>
        AlwaysPresent,

        /// <summary>
        /// Empty option should be available only as a default value. There should not be possible to set the empty value afterwards
        /// </summary>
        PresentOnlyOnInit
    }
}