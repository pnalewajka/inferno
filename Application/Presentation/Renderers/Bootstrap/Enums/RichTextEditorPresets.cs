﻿namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Enums
{
    /// <summary>
    /// Specifies the desired functionality level
    /// </summary>
    /// <see cref="http://ckeditor.com/presets" />
    public enum RichTextEditorPresets
    {
        /// <summary>
        /// Minimal toolbar, 17 Plugins
        /// </summary>
        Basic,

        /// <summary>
        /// Standard compliant toolbar, 48 Plugins
        /// </summary>
        Standard,

        /// <summary>
        /// Full featured editor, 72 Plugins
        /// </summary>
        Full,
    }
}