﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Enums
{
    public enum ColorPickerFormat
    {
        /// <summary>
        /// rgb (e.g. rgb(51,134,170))
        /// </summary>
        Rgb,

        /// <summary>
        /// Hex (e.g. #452b2b)
        /// </summary>
        Hex,

        /// <summary>
        /// rgba (e.g. rgba(51,134,170, 0.1))
        /// </summary>
        Rgba
    }
}
