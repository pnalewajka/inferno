﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Checkbox;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.NumberControl;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.RadioGroup;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textarea;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Textbox;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap
{
    public partial class Angular
    {
        private IDictionary<Type, Action<ControlRenderingModel>> _typeSpecificBindings;
        
        public void InitializeDefaultBindings(FormRenderingModel formRenderingModel)
        {
            foreach (var control in formRenderingModel.Controls)
            {
                control.CustomInputTagAttributes.Add("ng-model", control.Name);
                InitializeTypeSpecificBindings(control);
            }
        }

        private void InitializeTypeSpecificBindings(ControlRenderingModel renderingModel)
        {
            var controlType = renderingModel.GetType();

            if (_typeSpecificBindings.ContainsKey(controlType))
            {
                _typeSpecificBindings[controlType](renderingModel);
            }
        }

        private void InitializeCheckboxBindings(CheckboxRenderingModel renderingModel)
        {
        }

        private void InitializeDatePickerBindings(DatePickerRenderingModel renderingModel)
        {
        }

        private void InitializeDropdownBindings(DropdownRenderingModel renderingModel)
        {
        }

        private void InitializeRadioGroupBindings(RadioGroupRenderingModel renderingModel)
        {
        }

        private void InitializeHiddenInputBindings(HiddenInputRenderingModel renderingModel)
        {
        }

        private void InitializeNumberControlBindings(NumberControlRenderingModel renderingModel)
        {
        }

        private void InitializeTextareaBindings(TextareaRenderingModel renderingModel)
        {
        }

        private void InitializeTextboxBindings(TextboxRenderingModel renderingModel)
        {
        }

        private void InitializeValuePickerBindings(ValuePickerRenderingModel renderingModel)
        {
        }

        private void InitializeTypeSpecificBindings()
        {
            _typeSpecificBindings = new Dictionary<Type, Action<ControlRenderingModel>>();

            _typeSpecificBindings[typeof(CheckboxRenderingModel)] = (c) => InitializeCheckboxBindings(c as CheckboxRenderingModel);
            _typeSpecificBindings[typeof(DatePickerRenderingModel)] = (c) => InitializeDatePickerBindings(c as DatePickerRenderingModel);
            _typeSpecificBindings[typeof(DropdownRenderingModel)] = (c) => InitializeDropdownBindings(c as DropdownRenderingModel);
            _typeSpecificBindings[typeof(RadioGroupRenderingModel)] = (c) => InitializeRadioGroupBindings(c as RadioGroupRenderingModel);
            _typeSpecificBindings[typeof(HiddenInputRenderingModel)] = (c) => InitializeHiddenInputBindings(c as HiddenInputRenderingModel);
            _typeSpecificBindings[typeof(NumberControlRenderingModel)] = (c) => InitializeNumberControlBindings(c as NumberControlRenderingModel);
            _typeSpecificBindings[typeof(TextareaRenderingModel)] = (c) => InitializeTextareaBindings(c as TextareaRenderingModel);
            _typeSpecificBindings[typeof(TextboxRenderingModel)] = (c) => InitializeTextboxBindings(c as TextboxRenderingModel);
            _typeSpecificBindings[typeof(ValuePickerRenderingModel)] = (c) => InitializeValuePickerBindings(c as ValuePickerRenderingModel);
        }
    }
}