﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TypeAheadAttribute : LookupBaseAttribute
    {
        /// <summary>
        /// Minimum input length for query results
        /// </summary>
        public int MinimumInputLength { get; set; } = 3;
    }
}
