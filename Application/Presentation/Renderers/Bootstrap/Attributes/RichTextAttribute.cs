﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RichTextAttribute : HtmlSanitizeAttribute
    {
        public RichTextAttribute(RichTextEditorPresets editorPresets = RichTextEditorPresets.Basic, string mentionJavaScriptResolver = null)
            : base(true)
        {
            EditorPresets = editorPresets;
            MentionJavaScriptResolver = mentionJavaScriptResolver;
        }

        public RichTextEditorPresets EditorPresets { get; private set; }

        public string MentionJavaScriptResolver { get; private set; }        
    }
}
