﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// How to format cell value
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StringFormatAttribute : Attribute
    {
        public string Format { get; set; }

        public bool ShouldEncodeHtml { get; set; }

        public StringFormatAttribute()
        {
        }

        public StringFormatAttribute(string format)
        {
            Format = format;
        }

        public StringFormatAttribute(string format, bool shouldEncodeHtml)
        {
            Format = format;
            ShouldEncodeHtml = shouldEncodeHtml;
        }
    }
}