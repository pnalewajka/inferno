﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public class OrderAttribute : Attribute
    {
        private readonly long _order;

        public long Order => _order;

        public OrderAttribute(long order)
        {
            _order = order;
        }
    }
}