﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ColorPickerAttribute : Attribute
    {
        public ColorPickerFormat Format { get; private set; }

        public ColorPickerAttribute()
            : this(ColorPickerFormat.Hex)
        {
        }

        public ColorPickerAttribute(ColorPickerFormat colorPickerFormat)
        {
            Format = colorPickerFormat;
        }
    }
}