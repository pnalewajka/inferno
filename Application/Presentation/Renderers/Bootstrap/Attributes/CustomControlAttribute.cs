﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CustomControlAttribute : Attribute
    {
        public string TemplatePath { get; set; }
    }
}