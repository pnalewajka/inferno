﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Marks that the control should be placed inside the fieldset
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldSetAttribute : Attribute
    {
        /// <summary>
        /// Identifier of the field set
        /// </summary>
        public string Id { get; private set; }

        public FieldSetAttribute(string id)
        {
            Id = id;
        }
    }
}