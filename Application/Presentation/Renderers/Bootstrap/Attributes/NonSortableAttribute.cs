﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Do not sort by specific property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NonSortableAttribute : Attribute
    {
    }
}
