﻿using System;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PromptAttribute : Attribute
    {
        private readonly string _text;

        public string Text
        {
            get
            {
                if (ResourceType != null)
                {
                    var resourceManager = ResourceManagerHelper.GetResourceManager(ResourceType);
                    return resourceManager.GetString(_text);
                }

                return _text;
            }
        }

        public Type ResourceType { get; set; }

        public PromptAttribute(string text, Type resourceType = null)
        {
            _text = text;
            ResourceType = resourceType;
        }
    }
}