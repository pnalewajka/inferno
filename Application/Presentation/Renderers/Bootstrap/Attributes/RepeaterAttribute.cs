﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Customizes rendering repeatable details for given view model's collection
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RepeaterAttribute : ValidationAttribute
    {
        private readonly Type _resourceType;

        /// <summary>
        /// Localizable repeater item title
        /// </summary>
        public string ItemTitle { get; set; }

        /// <summary>
        /// Localizable text shown on button for adding items
        /// </summary>
        public string AddButtonText { get; set; }

        /// <summary>
        /// Localizable text shown on button for deleting items
        /// </summary>
        public string DeleteButtonText { get; set; }

        /// <summary>
        /// Get localized add button text
        /// </summary>
        /// <returns></returns>
        public string GetLocalizedAddButtonText()
        {
            return LocalizationHelper.GetLocalizedStringOrDefault(_resourceType, AddButtonText);
        }

        /// <summary>
        /// Get localized delete button text
        /// </summary>
        /// <returns></returns>
        public string GetLocalizedDeleteButtonText()
        {
            return LocalizationHelper.GetLocalizedStringOrDefault(_resourceType, DeleteButtonText);
        }

        /// <summary>
        /// Get localized item title
        /// </summary>
        /// <returns></returns>
        public string GetLocalizedItemTitle()
        {
            return LocalizationHelper.GetLocalizedStringOrDefault(_resourceType, ItemTitle);
        }

        /// <summary>
        /// Validation parameter: mininum number of items
        /// </summary>
        public long MinCount { get; set; }

        /// <summary>
        /// Validation parameter: maximum number of items, zero if unbounded
        /// </summary>
        public long MaxCount { get; set; }

        /// <summary>
        /// Can new records be added in repeater control
        /// </summary>
        public bool CanAddRecord { get; set; }

        /// <summary>
        /// Can records be deleted in repeater control
        /// </summary>
        public bool CanDeleteRecord { get; set; }

        /// <summary>
        /// Can items be reordered in repeater control
        /// </summary>
        public bool CanReorder { get; set; }

        /// <summary>
        /// Can rendering be skipped in read-only view if empty
        /// </summary>
        public bool HideInReadOnlyWhenEmpty { get; set; }

        /// <summary>
        /// Is there any constraint given for the item count
        /// </summary>
        public bool IsCountConstrained => MinCount != 0 || MaxCount > 1;

        /// <summary>
        /// Repeater attribute
        /// </summary>
        /// <param name="itemTitle">Resource key for item title</param>
        /// <param name="resourceType">Type of resource</param>
        /// <param name="minCount">Mininum number of items validation</param>
        /// <param name="maxCount">Maximum number of items validation</param>
        public RepeaterAttribute(string itemTitle = null, Type resourceType = null, long minCount = 0, long maxCount = 0)
        {
            ItemTitle = itemTitle;
            _resourceType = resourceType;
            MinCount = minCount;
            MaxCount = maxCount;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var collection = value as ICollection;

            if (collection == null)
            {
                if (IsCountConstrained)
                {
                    return new ValidationResult(string.Format(RenderersResources.RepeaterNotNullValidationMessage, MinCount), new List<string> { validationContext.DisplayName });
                }

                return ValidationResult.Success;
            }

            if (MinCount > 0 && collection.Count < MinCount)
            {
                return new ValidationResult(string.Format(RenderersResources.RepeaterMinElementsValidationMessageFormat, MinCount), new List<string> { validationContext.DisplayName });
            }

            if (MaxCount > 0 && collection.Count > MaxCount)
            {
                return new ValidationResult(string.Format(RenderersResources.RepeaterMaxElementsValidationMessageFormat, MaxCount), new List<string> { validationContext.DisplayName });
            }

            return ValidationResult.Success;
        }
    }
}