﻿using System;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class LayoutAttribute : Attribute
    {
        private readonly Type _layoutType;

        public Type LayoutType => _layoutType;

        public LayoutAttribute(Type layoutType)
        {
            _layoutType = layoutType;
        }

        public static Layout GetLayoutForModel(object model)
        {
            var defaultLayout = new LayoutAttribute(typeof(SimpleLayout));
            var layoutAttribute = AttributeHelper.GetClassAttribute(model, defaultLayout);

            return (Layout)ReflectionHelper.CreateInstance(layoutAttribute.LayoutType);
        }

        public static Layout GetLayoutForNestedModel(object model, PropertyInfo nestedPropertyInfo, Layout defaultLayout = null)
        {
            var parentModelAttribute = AttributeHelper.GetClassAttribute<LayoutAttribute>(model);
            var layoutAttribute = AttributeHelper.GetPropertyAttribute(nestedPropertyInfo, parentModelAttribute);

            if (layoutAttribute != null)
            {
                return (Layout)ReflectionHelper.CreateInstance(layoutAttribute.LayoutType);
            }

            return defaultLayout;
        }
    }
}