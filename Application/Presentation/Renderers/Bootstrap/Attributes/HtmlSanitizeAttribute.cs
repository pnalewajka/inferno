﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <remarks>
    /// When string is sanitized it sometimes encode special characters as '&' and '+'
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public class HtmlSanitizeAttribute : Attribute
    {
        public bool ShouldBeSanitized { get; private set; }

        public HtmlSanitizeMethod SanitizeHtmlMethod { get; set; } = HtmlSanitizeMethod.Default;

        public HtmlSanitizeAttribute(bool shouldBeSanitized = true)
        {
            ShouldBeSanitized = shouldBeSanitized;
        }
    }
}
