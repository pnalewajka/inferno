﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    public abstract class LookupBaseAttribute : Attribute
    {
        /// <summary>
        /// Type of the card index controller which should be used to populate the list of values
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// Returns Type identifier
        /// </summary>
        public string TypeIdentifier => IdentifierHelper.GetTypeIdentifier(Type);

        /// <summary>
        /// Type of the formatter that will be used, while presenting the selected data in a grid.
        /// </summary>
        public Type GridFormatterType { get; set; }

        /// <summary>
        /// Type of the formatter that will be used, while presenting the selected data on a form.
        /// </summary>
        public Type FormFormatterType { get; set; }

        /// <summary>
        /// JavaScript function name or body which should be called immediatelly before the final value url is determined
        /// Function can use the following parameters: 
        /// url - default url which will be used if no function specified
        /// control - HtmlElement describing the current control
        /// </summary>
        public string OnResolveUrlJavaScript { get; set; }

        /// <summary>
        /// JavaScript function name or body which should be called immediatelly after the value has been selected
        /// </summary>        
        public string OnSelectionChangedJavaScript { get; set; }

        /// <summary>
        /// Determines if clear button should be hidden for a given control
        /// </summary>
        public bool HideClearButton { get; set; }

        /// <summary>
        /// Is Hub available
        /// </summary>
        public bool HasHub => typeof(IHubController).IsAssignableFrom(Type);
    }
}
