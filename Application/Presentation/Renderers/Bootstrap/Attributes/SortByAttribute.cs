﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Method with sortable by attribute supports sorting via given property of view model
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SortByAttribute : Attribute
    {
        private const string PropertyNameFormat = @"Virtual{0}";

        private readonly string _propertyName;
        private readonly bool _isVirtual;

        /// <summary>
        /// Sort by specific property
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="isVirtual">Indicates that column will be sorted using different column, while such different column will be available in resulting grid</param>
        public SortByAttribute(string propertyName, bool isVirtual = true)
        {
            _propertyName = propertyName;
            _isVirtual = isVirtual;
        }

        /// <summary>
        /// Property name used for presentation and sorting
        /// Virtual prefix will be added to distinguish between ordering originators (property or method), if required
        /// </summary>
        public string PropertyName => _isVirtual ? string.Format(PropertyNameFormat, _propertyName) : _propertyName;
    }
}