﻿using System;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Marks that there should be rendered the hint area below the control
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class HintAttribute : Attribute
    {
        private readonly string _text;

        /// <summary>
        /// Content of the hint area
        /// </summary>
        public string Text
        {
            get
            {
                if (ResourceType != null)
                {
                    var resourceManager = ResourceManagerHelper.GetResourceManager(ResourceType);
                    return resourceManager.GetString(_text);
                }

                return _text;
            }
        }

        /// <summary>
        /// Type of the resource which will be used for retrieving localized Text
        /// </summary>
        public Type ResourceType { get; set; }

        /// <summary>
        /// Constructor of HintAttribute
        /// </summary>
        /// <param name="text">Content of the hint area</param>
        /// <param name="resourceType">Type of the resource which will be used for retrieving localized Text</param>
        public HintAttribute(string text, Type resourceType = null)
        {
            _text = text;
            ResourceType = resourceType;
        }
    }
}