﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ValuePickerAttribute : LookupBaseAttribute
    {
        /// <summary>
        /// Optional dialog width to override the default modal width.
        /// This is an initial width that might be adjusted to content.
        /// </summary>
        public string DialogWidth { get; set; }

        /// <summary>
        /// Storage key used to stash value picker settings (search, filter, order) between calls.
        /// Use null (default value) if no stashing should take place.
        /// </summary>
        public string ClientStorageKey { get; set; }
    }
}