﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Provide solution-wide defaults for value picker settings. 
    /// These can be refined per control usage (in view models).
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ValuePickerDefaultsAttribute : Attribute
    {
        /// <summary>
        /// Optional dialog width to override the default modal width.
        /// This is an initial width that might be adjusted to content.
        /// </summary>
        public string DialogWidth { get; set; }
    }
}