﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CheckboxGroupAttribute : Attribute
    {
        public Type ItemProvider { get; set; }

        public IList<ListItem> GetItems()
        {
            var itemProvider = (IListItemProvider) ReflectionHelper.CreateInstanceWithIocDependencies(ItemProvider);
            var items = itemProvider.GetItems().ToList();

            items = items
                .Where(i => i.Id.HasValue)
                .OrderBy(i => i.Id.Value)
                .ToList();

            return items;
        }
    }
}