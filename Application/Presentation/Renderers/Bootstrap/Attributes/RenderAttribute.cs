﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Customizes rendering details for given view model's property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public class RenderAttribute : Attribute
    {
        /// <summary>
        /// Determines how big the control should be (and, as a result, the label, too)
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// Specifies CSS class added to the control's container
        /// </summary>
        public string ControlCssClass { get; set; }

        /// <summary>
        /// Specifies CSS class to be used in a grid cell
        /// </summary>
        public string GridCssClass { get; set; }

        /// <summary>
        /// Specifies CSS class to be used in a grid header
        /// </summary>
        public string GridHeaderCssClass { get; set; }
    }
}