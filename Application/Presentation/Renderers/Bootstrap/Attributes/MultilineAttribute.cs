﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MultilineAttribute : Attribute
    {
        public byte? Rows { get; set; } 

        public bool AllowAutoresize { get; set; }

        public MultilineAttribute()
        {
        }

        public MultilineAttribute(byte rows, bool allowAutoresize = true)
        {
            Rows = rows;
            AllowAutoresize = allowAutoresize;
        }
    }
}