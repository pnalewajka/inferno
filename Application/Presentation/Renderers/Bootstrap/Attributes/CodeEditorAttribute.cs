﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CodeEditorAttribute : Attribute
    {
        public SyntaxType SyntaxType { get; private set; }

        public CodeEditorAttribute(SyntaxType syntaxType)
        {
            SyntaxType = syntaxType;
        }
    }
}