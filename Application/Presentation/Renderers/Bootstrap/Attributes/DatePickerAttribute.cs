﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DatePickerAttribute : Attribute
    {
        /// <summary>
        /// Determines if control should be rendered as empty in case of default value
        /// </summary>
        public bool EmptyIfDefault { get; set; }
    }
}
