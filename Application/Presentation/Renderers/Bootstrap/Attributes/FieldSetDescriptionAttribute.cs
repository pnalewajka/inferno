﻿using System;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Describes the fieldset which will be rendered for a given view model
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class FieldSetDescriptionAttribute : Attribute
    {
        /// <summary>
        /// Describes the order in which the fieldsets should be rendered
        /// </summary>
        public long Order { get; private set; }
        
        /// <summary>
        /// Identifier of the fieldset
        /// </summary>
        public string Id { get; private set; }
        
        /// <summary>
        /// Fieldset legend
        /// </summary>
        public string Legend { get; private set; }
        
        /// <summary>
        /// Type of the resource which will be used for retrieving localized version of the legend
        /// </summary>
        public Type ResourceType { get; private set; }

        public FieldSetDescriptionAttribute(long order, string id)
        {
            Order = order;
            Id = id;
        }

        public FieldSetDescriptionAttribute(long order, string id, string legend, Type resourceType)
        {
            Order = order;
            Id = id;
            Legend = legend;
            ResourceType = resourceType;
        }

        public string GetLocalizedLegend()
        {
            if (ResourceType != null)
            {
                var resourceManager = ResourceManagerHelper.GetResourceManager(ResourceType);
                return resourceManager.GetString(Legend);
            }

            return Legend;
        }
    }
}