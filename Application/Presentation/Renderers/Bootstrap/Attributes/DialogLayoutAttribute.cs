﻿using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    /// <summary>
    /// Customizes rendering details for given view model
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DialogLayoutAttribute : Attribute
    {
        public DialogLayoutAttribute()
        {
            LabelWidth = LabelWidth.Default;
        }

        /// <summary>
        /// Dialog labels width, by default it is 2, which means that bootstrap grid column width will be 2.
        /// For longer labels we can use bigger values. Bootstrap defines values up to 12 (https://getbootstrap.com/css/#grid).
        /// </summary>
        public LabelWidth LabelWidth { get; set; }
    }
}