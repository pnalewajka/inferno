﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RadioGroupAttribute : Attribute
    {
        public Type ItemProvider { get; set; }
        public EmptyItemBehavior EmptyItemBehavior { get; set; }

        public IList<ListItem> GetItems()
        {
            return GetItems(EmptyItemBehavior);
        }

        public IList<ListItem> GetItems(EmptyItemBehavior emptyItemBehavior)
        {
            var itemProvider = (IListItemProvider)ReflectionHelper.CreateInstanceWithIocDependencies(ItemProvider);
            var items = itemProvider.GetItems().ToList();

            if (emptyItemBehavior == EmptyItemBehavior.NotAvailable)
            {
                items = items.Where(i => i.Id != null).ToList();
            }

            if (items.Any(i => i.Id == null)
                || (emptyItemBehavior != EmptyItemBehavior.AlwaysPresent
                    && emptyItemBehavior != EmptyItemBehavior.PresentOnlyOnInit))
            {
                return items.OrderBy(i => i.Id.HasValue).ToList();
            }

            var emptyItem = new ListItem
            {
                Id = null, 
                DisplayName = RenderersResources.RadioGroupEmptyItemDisplayName
            };
            items.Insert(0, emptyItem);

            return items;
        }
    }
}