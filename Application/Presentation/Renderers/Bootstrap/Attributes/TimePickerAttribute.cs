﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TimePickerAttribute : Attribute
    {
        public byte MinuteInterval { get; private set; }

        public TimePickerAttribute(byte minuteInterval = 5)
        {
            MinuteInterval = minuteInterval;
        }
    }
}
