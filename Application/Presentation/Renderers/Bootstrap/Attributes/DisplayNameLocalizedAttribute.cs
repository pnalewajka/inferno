﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using System;
using System.ComponentModel;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public class DisplayNameLocalizedAttribute : DisplayNameAttribute
    {
        private readonly Type _resourceType;
        private readonly string _shortDisplayNameResourceKey;

        /// <summary>
        /// Localized label for property
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return GetResourceString(base.DisplayName);
            }
        }

        /// <summary>
        /// Localized short label for property
        /// </summary>
        public string DisplayShortName
        {
            get
            {
                return GetResourceString(_shortDisplayNameResourceKey) ?? DisplayName;
            }
        }

        /// <summary>
        /// Display label for property
        /// </summary>
        /// <param name="displayName">Resource name for label</param>
        /// <param name="shortDisplayName">Resource name for grid label</param>
        /// <param name="resourceType">Resource type</param>
        public DisplayNameLocalizedAttribute(string displayName, string shortDisplayName, Type resourceType)
            : this(displayName, resourceType)
        {
            _shortDisplayNameResourceKey = shortDisplayName;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="displayName">Resource name for label</param>
        /// <param name="resourceType">Resource type</param>
        public DisplayNameLocalizedAttribute(string displayName, Type resourceType) : base(displayName)
        {
            _resourceType = resourceType;
        }

        private string GetResourceString(string name)
        {
            if (_resourceType != null && !string.IsNullOrEmpty(name))
            {
                return ResourceHelper.GetString(_resourceType, name);
            }

            return name;
        }
    }
}