﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.Bootstrap
{
    public partial class Bootstrap
    {
        private readonly ModelStateDictionary _modelState;
        private readonly HashSet<string> _currentUserRoles;
        private readonly InlineLayout _inlineLayout;

        public Bootstrap(ViewDataDictionary viewDataDictionary)
        {
            _modelState = viewDataDictionary.ModelState;
            _currentUserRoles = (HashSet<string>)viewDataDictionary["CurrentUserRoles"];
            _inlineLayout = new InlineLayout();
        }

        public Angular Angular()
        {
            return new Angular(this);
        }

        /// <summary>
        /// Returns markup with a control for specified property of a viewmodel. Re-uses Atomic controls.
        /// </summary>
        /// <typeparam name="TViewModel">Type of the viewmodel to be processed</typeparam>
        /// <param name="model">Model to be processed</param>
        /// <param name="propertyExpression">Expression to extract the property for which the markup should be rendered</param>
        /// <param name="inputTagAttributes">Optional object to specify additional input tag attributes. Property names get hyphenated when applied to the actual tag.</param>
        /// <param name="layout">Optional layout to be applied for template rendering.</param>
        /// <returns>Markup of a control for a given property of a viewmodel.</returns>
        public MvcHtmlString EditorFor<TViewModel>(TViewModel model, Expression<Func<TViewModel, object>> propertyExpression, object inputTagAttributes = null, Layout layout = null)
        {
            layout = layout ?? LayoutAttribute.GetLayoutForModel(model);

            var content = layout.EditorFor(model, propertyExpression, new Models.ModelBuilderContext(layout, _modelState), _currentUserRoles, inputTagAttributes);

            return new MvcHtmlString(content);
        }

        /// <summary>
        /// Returns markup with a control for specified property of a viewmodel. Re-uses Atomic controls.
        /// </summary>
        /// <typeparam name="TViewModel">Type of the viewmodel to be processed</typeparam>
        /// <param name="model">Model to be processed</param>
        /// <param name="valuePropertyName">Name of property for which the markup should be rendered</param>
        /// <param name="inputTagAttributes">Optional object to specify additional input tag attributes. Property names get hyphenated when applied to the actual tag.</param>
        /// <param name="layout">Optional layout to be applied for template rendering.</param>
        /// <returns>Markup of a control for a given property of a viewmodel.</returns>
        public MvcHtmlString EditorFor<TViewModel>(TViewModel model, string valuePropertyName, object inputTagAttributes = null, Layout layout = null)
        {
            layout = layout ?? LayoutAttribute.GetLayoutForModel(model);

            var content = layout.EditorFor(model, valuePropertyName, new Models.ModelBuilderContext(layout, _modelState), _currentUserRoles, inputTagAttributes);

            return new MvcHtmlString(content);
        }

        /// <summary>
        /// Returns markup with a label for specified property of a viewmodel.
        /// </summary>
        /// <typeparam name="TViewModel">Type of the viewmodel to be processed</typeparam>
        /// <param name="model">Model to be processed</param>
        /// <param name="propertyExpression">Expression to extract the property for which the markup should be rendered</param>
        /// <param name="labelTagAttributes">Optional object to specify additional label tag attributes. Property names get hyphenated when applied to the actual tag.</param>
        /// <param name="layout">Optional layout to be applied for template rendering.</param>
        /// <returns>Markup of a label for a given property of a viewmodel.</returns>
        public MvcHtmlString LabelFor<TViewModel>(TViewModel model, Expression<Func<TViewModel, object>> propertyExpression, object labelTagAttributes = null, Layout layout = null)
        {
            layout = layout ?? LayoutAttribute.GetLayoutForModel(model);

            var content = layout.LabelFor(model, propertyExpression, new Models.ModelBuilderContext(layout, _modelState), _currentUserRoles, labelTagAttributes);

            return new MvcHtmlString(content);
        }

        /// <summary>
        /// Returns markup with a control for specified property of a viewmodel with an inline layout. Re-uses Atomic controls.
        /// </summary>
        /// <typeparam name="TViewModel">Type of the viewmodel to be processed</typeparam>
        /// <param name="model">Model to be processed</param>
        /// <param name="propertyExpression">Expression to extract the property for which the markup should be rendered</param>
        /// <param name="inputTagAttributes">Optional object to specify additional input tag attributes. Property names get hyphenated when applied to the actual tag.</param>
        /// <returns>Markup of a control for a given property of a viewmodel.</returns>
        public MvcHtmlString InlineEditorFor<TViewModel>(TViewModel model, Expression<Func<TViewModel, object>> propertyExpression, object inputTagAttributes = null)
        {
            return EditorFor(model, propertyExpression, inputTagAttributes, _inlineLayout);
        }

        /// <summary>
        /// Returns markup with a label for specified property of a viewmodel with an inline layout. Re-uses Atomic controls.
        /// </summary>
        /// <typeparam name="TViewModel">Type of the viewmodel to be processed</typeparam>
        /// <param name="model">Model to be processed</param>
        /// <param name="propertyExpression">Expression to extract the property for which the markup should be rendered</param>
        /// <param name="labelTagAttributes">Optional object to specify additional label tag attributes. Property names get hyphenated when applied to the actual tag.</param>
        /// <returns>Markup of a label for a given property of a viewmodel.</returns>
        public MvcHtmlString InlineLabelFor<TViewModel>(TViewModel model, Expression<Func<TViewModel, object>> propertyExpression, object labelTagAttributes = null)
        {
            return LabelFor(model, propertyExpression, labelTagAttributes, _inlineLayout);
        }
    }
}
