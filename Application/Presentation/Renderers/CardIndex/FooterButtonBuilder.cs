﻿using System.Web;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public static class FooterButtonBuilder
    {
        public static IFooterButton DeleteButton(string url, string textButton = null)
        {
            if (string.IsNullOrEmpty(textButton))
            {
                textButton = CardIndexResources.CardIndexDeleteRecordButtonText;
            }

            return new FooterButton(FooterButtonExtensions.DeleteButtonName, textButton)
            {
                CssClass = "pull-left",
                Icon = "glyphicon glyphicon-remove",
                ButtonStyleType = CommandButtonStyle.Default,
                ButtonType = CommandButtonType.Submit,
                OnClickAction = new UriAction(url, ActionType.Post),
                Confirmation = new Confirmation
                {
                    Message = CardIndexResources.DeleteRecordConfirmMessage,
                    IsRequired = true
                },
                Hotkey = "ctrl+del",
            };
        }

        public static IFooterButton EditButton(string textButton = null)
        {
            if (string.IsNullOrEmpty(textButton))
            {
                textButton = CardIndexResources.OkButtonText;
            }

            var attributes = SubmitFormButtonAttribures();

            return new FooterButton(FooterButtonExtensions.EditButtonName, textButton)
            {
                CssClass = "pull-right",
                ButtonType = CommandButtonType.Submit,
                OnClickAction = new JavaScriptCallAction("Forms.submitRecordForm(event.currentTarget);"),
                Hotkey = "alt+return",
                Attributes = attributes,
            };
        }

        public static IFooterButton AddButton(string textButton = null)
        {
            if (string.IsNullOrEmpty(textButton))
            {
                textButton = CardIndexResources.AddButtonText;
            }

            var attributes = SubmitFormButtonAttribures();

            return new FooterButton(FooterButtonExtensions.AddButtonName, textButton)
            {
                CssClass = "pull-right",
                ButtonType = CommandButtonType.Submit,
                OnClickAction = new JavaScriptCallAction("Forms.submitRecordForm(event.currentTarget);"),
                Hotkey = "ctrl+return",
                Attributes = attributes,
            };
        }

        public static IFooterButton EditAndStayButton(string textButton = null)
        {
            if (string.IsNullOrEmpty(textButton))
            {
                textButton = CardIndexResources.ApplyEditButtonText;
            }

            var attributes = SubmitFormButtonAttribures();

            return new FooterButton(FooterButtonExtensions.EditAndStayButtonName, textButton)
            {
                CssClass = "pull-right btn-separated",
                ButtonType = CommandButtonType.Button,
                ButtonStyleType = CommandButtonStyle.Default,
                OnClickAction = new JavaScriptCallAction("Forms.submitRecordForm(event.currentTarget, window.location.pathname);"),
                Attributes = attributes,
            };
        }

        public static IFooterButton AddAndOpenEditButton(string editUrlFormat, string textButton = null)
        {
            if (string.IsNullOrEmpty(textButton))
            {
                textButton = CardIndexResources.ApplyAddButtonText;
            }

            var attributes = SubmitFormButtonAttribures();

            return new FooterButton(FooterButtonExtensions.AddAndEditButtonName, textButton)
            {
                CssClass = "pull-right btn-separated",
                ButtonType = CommandButtonType.Button,
                ButtonStyleType = CommandButtonStyle.Default,
                OnClickAction = new JavaScriptCallAction($"Forms.submitRecordForm(event.currentTarget, '{HttpUtility.UrlDecode(editUrlFormat)}');"),
                Attributes = attributes,
            };
        }

        public static IFooterButton CancelButton(string returnUrl, string textButton = null)
        {
            if (string.IsNullOrEmpty(textButton))
            {
                textButton = CardIndexResources.CancelButtonText;
            }

            var attributes = SubmitFormButtonAttribures();

            return new FooterButton(FooterButtonExtensions.CancelButtonName, textButton)
            {
                CssClass = "pull-right",
                ButtonType = CommandButtonType.Button,
                ButtonStyleType = CommandButtonStyle.Default,
                OnClickAction = new UriAction(returnUrl),
                Hotkey = "esc",
                Attributes = attributes,
            };
        }

        public static TagAttributes SubmitFormButtonAttribures(string formName = "main-form")
        {
            var attributes = new TagAttributes();
            attributes.Add("form", formName);

            return attributes;
        }
    }
}
