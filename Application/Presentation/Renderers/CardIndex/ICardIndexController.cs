namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface ICardIndexController
    {
        object Context { get; }

        object GetViewModelFromContext(object childContext);

        object GetContextViewModel();
    }
}