﻿using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface ICardIndexRecordWithButtonsViewModel
    {
        IList<IFooterButton> Buttons { get; }
    }
}
