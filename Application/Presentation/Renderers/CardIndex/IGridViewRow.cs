﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface IGridViewRow
    {
        string RowCssClass { get; }

        string RowTitle { get; }
    }
}
