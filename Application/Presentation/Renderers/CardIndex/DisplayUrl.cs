﻿using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class DisplayUrl : ICellContent
    {
        public string CssClass { get; set; }

        public string IconCssClass { get; set; }

        public string LinkCssClass { get; set; }

        public UriAction Action { get; set; }

        public string Label { get; set; }

        public string ExportValue { get; set; }

        public string Target { get; set; }

        public string Title { get; set; }

        public override string ToString()
        {
            return ExportValue ?? Label;
        }
    }
}