﻿using System.Reflection;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class CellContentConverterContext
    {
        public object PropertyValue { get; set; }

        public string DisplayValue { get; set; }

        public object Model { get; set; }

        public PropertyInfo PropertyInfo { get; set; }
    }
}
