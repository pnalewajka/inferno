﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Toolbar;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class ButtonGroupViewModel
    {
        public string Name { get; private set; }
        public IList<ICommandButton> Buttons { get; private set; }
        public string CssClass { get; set; }

        public ButtonGroupViewModel(string name)
        {
            Name = name;
            Buttons = new List<ICommandButton>();
        }
    }
}