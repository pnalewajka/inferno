﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class BulkEditViewModel : ICardIndexRecordWithButtonsViewModel
    {
        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public long[] EntityIds { get; set; }

        [Visibility(VisibilityScope.Form)]
        [HiddenInput]
        public string EntityFieldName { get; set; }

        [Visibility(VisibilityScope.None)]
        [HiddenInput]
        public object EntityViewModel { get; set; }

        [Visibility(VisibilityScope.None)]
        public string CancelButtonUrl { get; set; }

        [Visibility(VisibilityScope.None)]
        public IList<IFooterButton> Buttons
        {
            get
            {
                return new List<IFooterButton> {
                    FooterButtonBuilder.EditButton(),
                    FooterButtonBuilder.CancelButton(CancelButtonUrl)
                };
            }
        }

        public BulkEditViewModel() : this(string.Empty) { }

        public BulkEditViewModel(string cancelButtonUrl)
        {
            CancelButtonUrl = cancelButtonUrl;
        }
    }
}
