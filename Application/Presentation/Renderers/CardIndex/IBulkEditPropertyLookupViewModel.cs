﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    /// <summary>
    /// Provides information for property lookup controller
    /// </summary>
    public interface IBulkEditPropertyLookupViewModel
    {
        long[] RecordIds { get; set; }

        long? DtoPropertyId { get; set; }

        string ViewModelIdentifier { get; set; }

        string CancelButtonUrl { get; set; }
    }
}
