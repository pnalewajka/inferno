﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.Presentation.Renderers.Controllers;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.TreeView;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class CardIndex<TViewModel, TDto>
        where TViewModel : class, new()
        where TDto : class
    {
        private const string EditActionName = "Edit";
        private const string ViewActionName = "View";

        private readonly IAlertService _alertService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IAnonymizationService _anonymizationService;
        private readonly UrlHelper _urlHelper;
        private readonly ICardIndexDataService<TDto> _cardIndexDataService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IClassMapping<TDto, TViewModel> _dtoToViewModelMapping;
        private readonly IDtoPropertyNameCardIndexDataService _dtoPropertyNameCardIndexDataService;
        private readonly bool _shouldBeAnonymized;
        private readonly Dictionary<string, Func<TViewModel, string>> _rowGroupings = new Dictionary<string, Func<TViewModel, string>>();

        public CardIndexSettings Settings { get; set; }
        public Func<TViewModel, object, bool> CanAddRecordRule { get; set; }
        public Func<TViewModel, object, bool> CanEditRecordRule { get; set; }
        public Func<TViewModel, object, bool> CanViewRecordRule { get; set; }

        public CardIndex(UrlHelper urlHelper,
            ICardIndexDataService<TDto> cardIndexDataService,
            IClassMappingFactory classMappingFactory,
            IAlertService alertService,
            IPrincipalProvider principalProvider,
            IAnonymizationService anonymizationService,
            IDtoPropertyNameCardIndexDataService dtoPropertyNameCardIndexDataService)
        {
            _urlHelper = urlHelper;
            _cardIndexDataService = cardIndexDataService;
            _classMappingFactory = classMappingFactory;
            _alertService = alertService;
            _principalProvider = principalProvider;
            _anonymizationService = anonymizationService;
            _dtoToViewModelMapping = classMappingFactory.CreateMapping<TDto, TViewModel>();
            _dtoPropertyNameCardIndexDataService = dtoPropertyNameCardIndexDataService;

            Settings = new CardIndexSettings(urlHelper)
            {
                ShouldHideSearchBox = !_cardIndexDataService.SupportsSearchByColumns,
                ShouldHideFilters = !_cardIndexDataService.SupportsNamedFilters
            };

            _shouldBeAnonymized = anonymizationService.ShouldBeAnonymized<TViewModel>();
        }

        public GridViewModel GetGridModel(QueryCriteria queryCriteria, GridRenderingControlContext renderingControlContext, IDictionary<string, object> queryStringFieldValues, IGridViewConfiguration gridViewConfiguration)
        {
            if (queryCriteria == null)
            {
                queryCriteria = new QueryCriteria();
            }

            if (queryCriteria.PageSize == null)
            {
                queryCriteria.PageSize = Settings.PageSize;
            }

            if (queryCriteria.OrderBy == null)
            {
                SetDefaultSorting(queryCriteria, gridViewConfiguration);
            }

            var userRoles = _principalProvider.Current.Roles.ToHashSet();
            var gridFilters = new GridViewFilters(Settings, queryCriteria, renderingControlContext, userRoles, queryStringFieldValues);
            queryCriteria.FilterObjects = gridFilters.FilterObjects;
            var cardIndexRecords = _cardIndexDataService.GetRecords(queryCriteria);
            var viewModelRecords = cardIndexRecords.Rows.Select(_dtoToViewModelMapping.CreateFromSource).ToList();

            if (_shouldBeAnonymized)
            {
                foreach (var viewModelRecord in viewModelRecords)
                {
                    _anonymizationService.Anonymize(viewModelRecord);
                }
            }

            var searchAreas = GetSearchAreas(queryStringFieldValues);

            var gridViewModelParameters = new GridViewModelParameters
            {
                Rows = viewModelRecords,
                RowType = typeof(TViewModel),
                QueryCriteria = queryCriteria,
                CurrentPageIndex = cardIndexRecords.CurrentPageIndex,
                LastPageIndex = cardIndexRecords.LastPageIndex,
                RecordCount = cardIndexRecords.RecordCount,
                Settings = Settings,
                RenderingControlContext = renderingControlContext,
                Alerts = cardIndexRecords.Alerts,
                UserRoles = userRoles,
                GridViewFilters = gridFilters,
                GridViewConfiguration = gridViewConfiguration,
                SearchAreas = searchAreas
            };

            return new GridViewModel(gridViewModelParameters);
        }

        private IList<SearchAreaViewModel> GetSearchAreas(IDictionary<string, object> queryStringFieldValues)
        {
            var selectedAreas = SearchAreaHelper.GetSelectedSearchAreasCodes(queryStringFieldValues);

            return Settings.SearchAreas.Select(a =>
                new SearchAreaViewModel(a.Name, a.DisplayName, selectedAreas.Any() ? selectedAreas.Contains(a.Name) : a.IsSelectedByDefault)
            ).ToList();
        }

        private static void SetDefaultSorting(QueryCriteria queryCriteria, IGridViewConfiguration gridViewConfiguration)
        {
            var sortingRules = new List<SortingCriterion>();
            var sortedColumnConfigurations = gridViewConfiguration.ColumnConfiguration
                .Where(x => x.SortingDirection != SortingDirection.Unspecified).ToList();

            foreach (var configuration in sortedColumnConfigurations)
            {
                sortingRules.Add(new SortingCriterion(configuration.MemberPath,
                    configuration.SortingDirection == SortingDirection.Ascending));
            }

            queryCriteria.OrderBy = sortingRules;
        }

        public TViewModel GetDefaultRecord()
        {
            if (Settings.DefaultNewRecord != null)
            {
                var record = Settings.DefaultNewRecord();
                var recordType = record.GetType();

                if (typeof(TDto).IsAssignableFrom(recordType))
                {
                    var dto = (TDto)record;
                    var viewModel = _dtoToViewModelMapping.CreateFromSource(dto);

                    return viewModel;
                }

                return (TViewModel)record;
            }

            var dtoRecord = _cardIndexDataService.GetDefaultNewRecord();
            var viewModelRecord = _dtoToViewModelMapping.CreateFromSource(dtoRecord);

            return viewModelRecord;
        }

        public TViewModel GetRecordById(long id)
        {
            var dtoRecord = _cardIndexDataService.GetRecordById(id);
            var viewModelRecord = _dtoToViewModelMapping.CreateFromSource(dtoRecord);

            return viewModelRecord;
        }

        public TViewModel GetAnonymizedRecordById(long id)
        {
            var viewModelRecord = GetRecordById(id);

            if (_shouldBeAnonymized)
            {
                _anonymizationService.Anonymize(viewModelRecord);
            }

            return viewModelRecord;
        }

        public ActionResult GetListView(AtomicController atomicController, GridParameters parameters)
        {
            CopyFilterRequiredRolesFromDataService();

            atomicController.Layout.PageTitle = Settings.Title;
            atomicController.Layout.PageHeader = Settings.Header;

            AddActionRestorePoint(atomicController, "List");

            if (parameters.RenderingControlContext.HasFlag(GridRenderingControlContext.ValuePicker) && parameters.IsMultiselectAllowed.HasValue)
            {
                var valuePickerController = (IValuePicker)atomicController;
                valuePickerController.PrepareForValuePicker(parameters.IsMultiselectAllowed.Value);
                atomicController.Layout.MasterName = null;
            }

            if (parameters.RenderingControlContext.HasFlag(GridRenderingControlContext.GridExpansion))
            {
                var treeGridExpandController = (IExpandingGridController)atomicController;
                treeGridExpandController.PrepareForGridExpanding();
                atomicController.Layout.MasterName = null;
            }

            if (_cardIndexDataService is ITreeCardIndexDataService treecardIndexDataService)
            {
                treecardIndexDataService.IsCollapsibleTreeMode = Settings.TreeMode == TreeDisplayMode.Collapsible
                                                                 && string.IsNullOrEmpty(parameters.SearchPhrase);
            }

            var queryCriteria = parameters.ToCriteria(
                _dtoToViewModelMapping,
                _anonymizationService,
                _principalProvider.Current.Roles,
                Settings.FilterGroups);

            PopulateSearchAreas(parameters, queryCriteria, Settings.SearchAreas);

            var gridViewConfiguration = GetGridViewConfiguration(parameters);

            var model = GetGridModel(queryCriteria, parameters.RenderingControlContext, parameters.QueryStringFieldValues, gridViewConfiguration);

            if (atomicController is ICardIndexController cardIndexController)
            {
                model.ContextViewModel = cardIndexController.GetContextViewModel();
            }

            AddAlerts(model.Alerts);
            OnBeforeListViewRender(model);

            return atomicController.GetViewResult(Settings.ListViewName, model);
        }

        private static void PopulateSearchAreas(GridParameters parameters, QueryCriteria queryCriteria, IList<SearchArea> searchAreas)
        {
            var selectedCodes = SearchAreaHelper.GetSelectedSearchAreasCodes(parameters.QueryStringFieldValues);
            var codesToPopulate = selectedCodes.Any()
                    ? selectedCodes
                    : searchAreas.Where(a => a.IsSelectedByDefault).Select(a => a.Name);

            foreach (var code in codesToPopulate)
            {
                queryCriteria.SearchCriteria.Codes.Add(code);
            }
        }

        private IGridViewConfiguration GetGridViewConfiguration(GridParameters parameters)
        {
            IGridViewConfiguration viewConfiguration = null;

            if (!string.IsNullOrEmpty(parameters.ViewName) && Settings.GridViewConfigurations != null)
            {
                viewConfiguration = Settings.GridViewConfigurations.SingleOrDefault(c => c.Identifier == parameters.ViewName);
            }

            if (viewConfiguration == null && Settings.GridViewConfigurations != null)
            {
                viewConfiguration = Settings.GridViewConfigurations.SingleOrDefault(c => c.IsDefault);
            }

            if (viewConfiguration == null)
            {
                viewConfiguration = GridViewConfiguration<TViewModel>.DefaultGridViewConfiguration;
            }

            return viewConfiguration;
        }

        public ActionResult GetAddView(AtomicController atomicController, object context)
        {
            var model = GetAddRecordViewModel(atomicController, context);

            atomicController.Layout.PageTitle = Settings.GetAddPageTitle();
            atomicController.Layout.PageHeader = Settings.GetAddPageHeader();
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

            ConfigureAddButtons(model);

            return atomicController.GetViewResult(Settings.AddViewName, model);
        }

        public ActionResult GetAddDialogView(AtomicController atomicController, object context)
        {
            var model = GetAddRecordViewModel(atomicController, context);

            var dialogModel = new AddDialogViewModel<object>(model.ViewModelRecord)
            {
                Title = Settings.GetAddPageTitle(),
                AddFormCustomization = Settings.AddFormCustomization
            };

            return atomicController.GetPartialViewResult(Settings.AddDialogViewName, dialogModel);
        }

        public ActionResult HandleAdd(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            var result = TryHandleAddRecord(atomicController, viewModelRecord, context);

            if (result.IsSuccessful)
            {
                var listActionUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

                return new RedirectResult(string.Format(listActionUrl, result.AddedRecordId));
            }

            atomicController.Layout.PageTitle = Settings.GetAddPageTitle();
            atomicController.Layout.PageHeader = Settings.GetAddPageHeader();
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

            var model = GetRecordViewModel(atomicController, viewModelRecord, context);

            ConfigureAddButtons(model);

            return atomicController.GetViewResult(Settings.AddViewName, model);
        }

        public ActionResult HandleAddDialog(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            var result = TryHandleAddRecord(atomicController, viewModelRecord, context);

            var dialogModel = new AddDialogViewModel<object>(viewModelRecord)
            {
                Title = Settings.GetAddPageTitle(),
                AddFormCustomization = Settings.AddFormCustomization,
                Alerts = _alertService.GetAlerts()
            };

            if (result.IsSuccessful)
            {
                return DialogHelper.HandleEvent(dialogModel.DialogId, DialogHelper.OkButtonTag, viewModelRecord);
            }

            return atomicController.GetPartialViewResult(Settings.AddDialogViewName, dialogModel);
        }

        public Action<CardIndexRecordViewModel<TViewModel>> ConfigureViewButtons = (model) =>
        {
            model.Buttons.Add(FooterButtonBuilder.CancelButton(model.CancelButtonUrl, CardIndexResources.CloseButtonText));
        };

        public Action<CardIndexRecordViewModel<TViewModel>> ConfigureAddButtons = (model) =>
        {
            model.Buttons.Add(FooterButtonBuilder.CancelButton(model.CancelButtonUrl));
            model.Buttons.Add(FooterButtonBuilder.AddButton());
        };

        public Action<CardIndexRecordViewModel<TViewModel>> ConfigureEditButtons = (model) =>
        {
            if (model.ShowDeleteButtonOnEdit && !string.IsNullOrEmpty(model.DeleteButtonUrl))
            {
                model.Buttons.Add(FooterButtonBuilder.DeleteButton(model.DeleteButtonUrl));
            }

            model.Buttons.Add(FooterButtonBuilder.CancelButton(model.CancelButtonUrl));
            model.Buttons.Add(FooterButtonBuilder.EditButton());
        };

        public ActionResult GetEditView(AtomicController atomicController, long id, object context)
        {
            var model = GetEditRecordViewModel(atomicController, id, context);

            atomicController.Layout.PageTitle = Settings.GetEditPageTitle();
            atomicController.Layout.PageHeader = Settings.GetEditPageHeader(model.ViewModelRecord);
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

            ConfigureEditButtons(model);

            return atomicController.GetViewResult(Settings.EditViewName, model);
        }

        public ActionResult GetEditDialogView(AtomicController atomicController, long id, object context)
        {
            var model = GetEditRecordViewModel(atomicController, id, context);

            var dialogModel = new EditDialogViewModel<object>(model.ViewModelRecord)
            {
                Title = Settings.GetEditPageTitle(),
                EditFormCustomization = Settings.EditFormCustomization
            };

            return atomicController.GetPartialViewResult(Settings.EditDialogViewName, dialogModel);
        }

        public ActionResult GetViewDialogView(AtomicController atomicController, long id, object context)
        {
            var model = GetAnonymizedRecordById(id);

            var dialogModel = new ViewDialogViewModel<object>(model)
            {
                Title = Settings.GetViewPageTitle(),
                ViewFormCustomization = Settings.ViewFormCustomization
            };

            dialogModel.Buttons.Clear();
            dialogModel.Buttons.Add(DialogHelper.GetCloseButton());

            return atomicController.GetPartialViewResult(Settings.ViewDialogViewName, dialogModel);
        }

        public ActionResult HandleEdit(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            var result = TryHandleEditRecord(atomicController, viewModelRecord, context);

            if (result.IsSuccessful)
            {
                var listActionUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

                return new RedirectResult(listActionUrl);
            }

            atomicController.Layout.PageTitle = Settings.GetEditPageTitle();
            atomicController.Layout.PageHeader = Settings.GetEditPageHeader(viewModelRecord);
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

            var model = GetRecordViewModel(atomicController, viewModelRecord, context);

            ConfigureEditButtons(model);

            return atomicController.GetViewResult(Settings.EditViewName, model);
        }

        public ActionResult HandleEditDialog(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            var result = TryHandleEditRecord(atomicController, viewModelRecord, context);

            var dialogModel = new EditDialogViewModel<object>(viewModelRecord)
            {
                Title = Settings.GetEditPageTitle(),
                EditFormCustomization = Settings.EditFormCustomization,
                Alerts = _alertService.GetAlerts()
            };

            if (result.IsSuccessful)
            {
                return DialogHelper.HandleEvent(dialogModel.DialogId, DialogHelper.OkButtonTag, viewModelRecord);
            }

            return atomicController.GetPartialViewResult(Settings.EditDialogViewName, dialogModel);
        }

        public ActionResult GetBulkEditFieldLookupStepView(AtomicController atomicController, IBulkEditPropertyLookupViewModel viewModel, object context)
        {
            if (!CanBulkEditRecords())
            {
                return new HttpUnauthorizedResult();
            }

            atomicController.Layout.PageTitle = Settings.GetBulkEditPageTitle(CardIndexResources.BulkEditStepOne);
            atomicController.Layout.PageHeader = Settings.GetBulkEditPageHeader(CardIndexResources.BulkEditStepOne);
            //atomicController.Layout.Css.BodyClass = RecordFormBodyClass;

            viewModel.ViewModelIdentifier = IdentifierHelper.GetTypeIdentifier(typeof(TViewModel));
            viewModel.CancelButtonUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

            return atomicController.GetViewResult("~/Views/CardIndex/BulkEditChooseFieldName.cshtml", viewModel);
        }

        public ActionResult GetBulkEditFieldValueStepView(AtomicController atomicController, IBulkEditPropertyLookupViewModel viewModel, object context)
        {
            if (!CanBulkEditRecords())
            {
                return new HttpUnauthorizedResult();
            }

            if (viewModel.DtoPropertyId.HasValue)
            {
                atomicController.Layout.PageTitle = Settings.GetBulkEditPageTitle(CardIndexResources.BulkEditStepTwo);
                atomicController.Layout.PageHeader = Settings.GetBulkEditPageHeader(CardIndexResources.BulkEditStepTwo);
                atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

                var cancelUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);
                var bulkEditViewModel = new BulkEditViewModel(cancelUrl)
                {
                    EntityFieldName = _dtoPropertyNameCardIndexDataService.GetPropertyNameById(viewModel.DtoPropertyId.Value, typeof(TViewModel))
                };

                var propertyValues = _cardIndexDataService
                    .GetRecordsByIds(viewModel.RecordIds)
                    .Values.Select(_dtoToViewModelMapping.CreateFromSource)
                    .Select(v => PropertyHelper.GetPropertyValue<object>(v, bulkEditViewModel.EntityFieldName))
                    .Distinct()
                    .ToList();

                bulkEditViewModel.EntityViewModel = new TViewModel();

                if (propertyValues.Count == 1)
                {
                    PropertyHelper.SetValue(bulkEditViewModel.EntityViewModel, bulkEditViewModel.EntityFieldName, propertyValues.First());
                }

                bulkEditViewModel.EntityIds = viewModel.RecordIds;

                return atomicController.GetViewResult("~/Views/CardIndex/BulkEditChangeFieldValueField.cshtml", bulkEditViewModel);
            }

            atomicController.Layout.PageTitle = Settings.GetBulkEditPageTitle(CardIndexResources.BulkEditStepOne);
            atomicController.Layout.PageHeader = Settings.GetBulkEditPageHeader(CardIndexResources.BulkEditStepOne);
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;
            viewModel.CancelButtonUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

            return atomicController.GetViewResult("~/Views/CardIndex/BulkEditChooseFieldName.cshtml", viewModel);
        }

        public ActionResult HandleBulkEditEntities(AtomicController atomicController, BulkEditViewModel viewModelRecord, object context)
        {
            if (!CanBulkEditRecords())
            {
                return new HttpUnauthorizedResult();
            }

            var recordViewModel = GetDefaultRecord();
            // Don't validate here as we are interested in validating 1 property, not whole model
            ViewModelHelper.TryUpdateModel(atomicController, recordViewModel);

            if (atomicController.ModelState.IsValidField(viewModelRecord.EntityFieldName))
            {
                var viewModelToDtoMapping = _classMappingFactory.CreateMapping<TViewModel, TDto>();
                var modelDto = viewModelToDtoMapping.CreateFromSource(recordViewModel);
                var fieldsMappings = _dtoToViewModelMapping.GetFieldMappings(typeof(TDto), typeof(TViewModel));
                var fieldMapping = fieldsMappings.SingleOrDefault(f => f.Key == viewModelRecord.EntityFieldName);

                if (fieldMapping.Value.Count == 1)
                {
                    var dtoPropertyName = fieldMapping.Value.Single();
                    var dtoProperty = typeof(TDto).GetProperty(dtoPropertyName);

                    if (dtoProperty != null)
                    {
                        var dtoPropertyValue = dtoProperty.GetValue(modelDto);
                        var bulkEditDto = new BulkEditDto
                        {
                            RecordIds = viewModelRecord.EntityIds,
                            DtoType = typeof(TDto),
                            PropertyName = dtoPropertyName,
                            PropertyValue = dtoPropertyValue,
                            CardIndexDataServiceType = Settings.CardIndexDataServiceType
                        };

                        var result = HandleBulkEditRecords(bulkEditDto, context);
                        AddAlerts(result);

                        if (result.IsSuccessful)
                        {
                            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

                            return new RedirectResult(listActionUrl);
                        }
                    }
                }
            }

            ModelState propertyState;

            if (atomicController.ModelState.TryGetValue(viewModelRecord.EntityFieldName, out propertyState))
            {
                AddAlerts(propertyState.Errors.Select(e => AlertDto.CreateError(e.ErrorMessage)));
            }

            viewModelRecord.EntityViewModel = recordViewModel;
            viewModelRecord.CancelButtonUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

            atomicController.Layout.PageTitle = Settings.GetBulkEditPageTitle(CardIndexResources.BulkEditStepTwo);
            atomicController.Layout.PageHeader = Settings.GetBulkEditPageHeader(CardIndexResources.BulkEditStepTwo);
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

            return atomicController.GetViewResult("~/Views/CardIndex/BulkEditChangeFieldValueField.cshtml", viewModelRecord);
        }

        private bool VerifyValidation(AtomicController atomicController, TViewModel viewModelRecord)
        {
            var readRestrictedViewModelProperties = _anonymizationService.GetReadRestrictedProperties(viewModelRecord.GetType()).ToList();
            var failedNotRestricted = atomicController.ModelState.Keys.Where(p => !readRestrictedViewModelProperties.Contains(p) && !atomicController.ModelState.IsValidField(p));

            if (!failedNotRestricted.Any() && _shouldBeAnonymized && !atomicController.ModelState.IsValid)
            {
                atomicController.RevalidateModel(viewModelRecord);

                var failedValidations = atomicController.ModelState.Count(m => m.Value.Errors.Any());
                var failedProtectedFieldValidationList = readRestrictedViewModelProperties.Where(p => !atomicController.ModelState.IsValidField(p)).ToList();

                if (failedValidations > 0 && failedProtectedFieldValidationList.Count == failedValidations)
                {
                    throw new MissingRequiredRoleException(_anonymizationService.GetMissingRoles<TViewModel>(failedProtectedFieldValidationList));
                }
            }
            else if (Settings.ShowValidationAlerts)
            {
                var errors = atomicController.ModelState
                   .SelectMany(e => e.Value.Errors)
                   .Select(e => e.ErrorMessage)
                   .Distinct()
                   .Select(message => new AlertDto { Message = message, Type = AlertType.Error, IsDismissable = true })
                   .ToList();

                AddAlerts(errors);
            }

            Settings.ValidationCustomization?.Invoke(viewModelRecord, atomicController.ModelState);

            return atomicController.ModelState.IsValid;
        }

        public ActionResult GetViewView(AtomicController atomicController, long id, object context)
        {
            var viewModelRecord = GetAnonymizedRecordById(id);

            if (!CanViewRecord(viewModelRecord, context))
            {
                return new HttpUnauthorizedResult();
            }

            atomicController.Layout.PageTitle = Settings.GetViewPageTitle();
            atomicController.Layout.PageHeader = Settings.GetViewPageHeader(viewModelRecord);
            atomicController.Layout.Css.BodyClass = CardIndexStyles.RecordFormBodyClass;

            var model = GetRecordViewModel(atomicController, viewModelRecord, context);

            ConfigureViewButtons(model);

            return atomicController.GetViewResult(Settings.ViewViewName, model);
        }

        public ActionResult RedirectToDetails(AtomicController atomicController, long id, object context)
        {
            var viewModelRecord = GetAnonymizedRecordById(id);
            string url = null;

            if (CanEditRecord(viewModelRecord, context))
            {
                url = CardIndexHelper.GetRecordActionUrl(atomicController, EditActionName, id, context);
            }
            else if (CanViewRecord(viewModelRecord, context))
            {
                url = CardIndexHelper.GetRecordActionUrl(atomicController, ViewActionName, id, context);
            }

            if (url == null)
            {
                return new HttpUnauthorizedResult();
            }

            return new RedirectResult(url);
        }

        public ActionResult HandleDelete(AtomicController atomicController, IEnumerable<long> ids, object context)
        {
            if (!Settings.DeleteButton.IsAccessibleToCurrentUser())
            {
                return new HttpUnauthorizedResult();
            }

            var result = HandleDeleteRecords(ids, context);
            AddAlerts(result);

            var listActionUrl = CardIndexHelper.GetReturnOrListUrl(atomicController, context);

            return new RedirectResult(listActionUrl);
        }

        public List<ListItem> GetListItems(long[] ids, Type formatterType, bool filtered = true)
        {
            IListItemFormatter<TViewModel> formatter;
            Func<TViewModel, string> displayNameFunc;

            if (formatterType != null)
            {
                formatter = (IListItemFormatter<TViewModel>)ReflectionHelper.CreateInstance(formatterType);
                displayNameFunc = viewModel => formatter.Format(viewModel);
            }
            else
            {
                displayNameFunc = viewModel => viewModel.ToString();
            }

            var dtoDictionary = _cardIndexDataService.GetRecordsByIds(ids, filtered);

            return ids.Where(c => dtoDictionary.Keys.Contains(c)).Select(i => new ListItem
            {
                Id = i,
                DisplayName = displayNameFunc(_dtoToViewModelMapping.CreateFromSource(dtoDictionary[i]))
            })
            .ToList();
        }

        public List<ListItem> GetListItems(string searchPhrase, Type formatterType, object context)
        {
            IListItemFormatter<TViewModel> formatter;
            Func<TViewModel, string> displayNameFunc;

            if (formatterType != null)
            {
                formatter = (IListItemFormatter<TViewModel>)ReflectionHelper.CreateInstance(formatterType);
                displayNameFunc = viewModel => formatter.Format(viewModel);
            }
            else
            {
                displayNameFunc = viewModel => viewModel.ToString();
            }

            var queryCriteria = new QueryCriteria();

            if (!string.IsNullOrEmpty(searchPhrase))
            {
                queryCriteria.SearchPhrase = searchPhrase;
            }

            if (context != null)
            {
                queryCriteria.Context = context;
            }

            var records = _cardIndexDataService.GetRecords(queryCriteria);

            return (
                from row in records.Rows
                let viewRow = _dtoToViewModelMapping.CreateFromSource(row)
                select new ListItem
                {
                    Id = ViewModelHelper.GetModelIdValue(viewRow),
                    DisplayName = displayNameFunc(viewRow)
                }).ToList();
        }

        public long GetRecordId(TViewModel model)
        {
            var viewModelToDtoMapping = _classMappingFactory.CreateMapping<TViewModel, TDto>();
            var dto = viewModelToDtoMapping.CreateFromSource(model);

            return _cardIndexDataService.GetRecordId(dto);
        }

        public void AddUniqueKeyViolationAlerts(RecordOperationResult result, Type viewModelRecordType)
        {
            if (result.UniqueKeyViolations != null && result.UniqueKeyViolations.Any())
            {
                var mapping = _classMappingFactory.CreateMapping(typeof(TDto), viewModelRecordType);
                var violatingViewModelPropertyNames = ClassMappingHelper.TranslateToDestinationPropertyNames(result.UniqueKeyViolations, mapping, typeof(TDto), viewModelRecordType);

                foreach (var violation in violatingViewModelPropertyNames)
                {
                    var propertyNames = viewModelRecordType
                        .GetProperties()
                        .Where(p => violation.PropertyNames.Contains(p.Name))
                        .Where(p => ViewModelPropertyHelper.ShouldBeVisible(p, VisibilityScope.FormAndGrid) == ControlVisibility.Show)
                        .Select(p => p.Name);

                    var propertyLabels = violation
                        .PropertyNames
                        .Where(p => propertyNames.Contains(p))
                        .Select(n => ViewModelMemberHelper.GetLabel(viewModelRecordType, n))
                        .ToList();

                    var message = propertyLabels.Count == 1
                        ? string.Format(AlertResources.FieldIsInUseSingular, propertyLabels.First())
                        : string.Format(AlertResources.FieldIsInUsePlural, string.Join(", ", propertyLabels));

                    result.AddAlert(AlertType.Error, message);
                }
            }
        }

        private void AddAlerts(BusinessResult businessResult)
        {
            foreach (var alertDto in businessResult.Alerts)
            {
                _alertService.AddAlert(alertDto.Type, alertDto.Message, alertDto.IsDismissable);
            }
        }

        private void AddAlerts(IEnumerable<AlertDto> alerts)
        {
            foreach (var alert in alerts)
            {
                _alertService.AddAlert(alert.Type, alert.Message, alert.IsDismissable);
            }
        }

        private void RestoreProtectedFields(TViewModel model, FormType formType, ViewDataDictionary viewData)
        {
            var formModelCustomization = formType == FormType.AddForm
                ? Settings.AddFormCustomization
                : Settings.EditFormCustomization;

            var boostrapHelper = new Bootstrap.Bootstrap(viewData);
            var protectedFields = new HashSet<string>(boostrapHelper.GetProtectedFields(model, formModelCustomization));

            if (!protectedFields.Any())
            {
                return;
            }

            var referenceRecord = formType == FormType.EditForm
                                      ? GetRecordById(GetRecordId(model))
                                      : GetDefaultRecord();

            //restore protected fields from reference record
            var modelType = model.GetType();

            foreach (var protectedField in protectedFields)
            {
                var propertyInfo = modelType.GetProperty(protectedField);

                if (propertyInfo != null && propertyInfo.CanWrite)
                {
                    var existingValue = propertyInfo.GetValue(referenceRecord);
                    propertyInfo.SetValue(model, existingValue);
                }
            }
        }

        public BoolResult TryHandleEditRecord(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            RestoreProtectedFields(viewModelRecord, FormType.EditForm, atomicController.ViewData);

            if (!CanEditRecord(viewModelRecord, context))
            {
                throw new UnauthorizedAccessException();
            }

            if (VerifyValidation(atomicController, viewModelRecord))
            {
                var result = HandleEditRecord(viewModelRecord, context);
                AddAlerts(result);

                return result;
            }

            return new BoolResult(false);
        }

        private AddRecordResult TryHandleAddRecord(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            RestoreProtectedFields(viewModelRecord, FormType.AddForm, atomicController.ViewData);

            if (!CanAddRecord(viewModelRecord, context))
            {
                throw new UnauthorizedAccessException();
            }

            if (VerifyValidation(atomicController, viewModelRecord))
            {
                var result = HandleAddRecord(viewModelRecord, context);
                AddAlerts(result);

                PropertyHelper.SetValue(viewModelRecord, ViewModelHelper.IdPropertyName, result.AddedRecordId);

                return result;
            }

            return new AddRecordResult() { IsSuccessful = false };
        }

        private AddRecordResult HandleAddRecord(TViewModel viewModelRecord, object context)
        {
            return ExecuteWithExceptionHandling(() =>
            {
                var viewModelToDtoMapping = _classMappingFactory.CreateMapping<TViewModel, TDto>();
                var dto = viewModelToDtoMapping.CreateFromSource(viewModelRecord);
                var result = _cardIndexDataService.AddRecord(dto, context);
                AddUniqueKeyViolationAlerts(result, viewModelRecord.GetType());

                return result;
            });
        }

        private BoolResult HandleEditRecord(TViewModel viewModelRecord, object context)
        {
            return ExecuteWithExceptionHandling(() =>
            {
                var viewModelToDtoMapping = _classMappingFactory.CreateMapping<TViewModel, TDto>();
                var dto = viewModelToDtoMapping.CreateFromSource(viewModelRecord);
                var result = _cardIndexDataService.EditRecord(dto, context);
                AddUniqueKeyViolationAlerts(result, viewModelRecord.GetType());

                return new BoolResult(result.IsSuccessful, result.Alerts);
            });
        }

        private BoolResult HandleBulkEditRecords(BulkEditDto dto, object context = null)
        {
            return ExecuteWithExceptionHandling(() =>
            {
                var result = _cardIndexDataService.BulkEditRecords(dto, context);

                return new BoolResult(result.IsSuccessful, result.Alerts);
            });
        }

        private BoolResult HandleDeleteRecords(IEnumerable<long> ids, object context)
        {
            return ExecuteWithExceptionHandling(() =>
            {
                var result = _cardIndexDataService.DeleteRecords(ids, context);

                return new BoolResult(result.IsSuccessful, result.Alerts);
            });
        }

        private CardIndexRecordViewModel<TViewModel> GetRecordViewModel(AtomicController atomicController, TViewModel viewModelRecord, object context)
        {
            var cancelUrl = CardIndexHelper.GetListUrl(atomicController, context);
            var deleteUrl = CardIndexHelper.GetDeleteUrl(atomicController, context);
            var model = new CardIndexRecordViewModel<TViewModel>(viewModelRecord, Settings, cancelUrl, deleteUrl);

            var cardIndexController = atomicController as ICardIndexController;

            if (cardIndexController != null)
            {
                model.ContextViewModel = cardIndexController.GetContextViewModel();
            }

            return model;
        }

        private CardIndexRecordViewModel<TViewModel> GetEditRecordViewModel(AtomicController atomicController, long id, object context)
        {
            var viewModelRecord = GetAnonymizedRecordById(id);

            if (!CanEditRecord(viewModelRecord, context))
            {
                throw new UnauthorizedAccessException();
            }

            ViewModelHelper.TryUpdateModel(atomicController, viewModelRecord, clearValidationErrors: true);

            return GetRecordViewModel(atomicController, viewModelRecord, context);
        }

        private CardIndexRecordViewModel<TViewModel> GetAddRecordViewModel(AtomicController atomicController, object context)
        {
            if (!CanAddRecord(null, context))
            {
                throw new UnauthorizedAccessException();
            }

            var viewModelRecord = GetDefaultRecord();
            ViewModelHelper.TryUpdateModel(atomicController, viewModelRecord, clearValidationErrors: true);

            return GetRecordViewModel(atomicController, viewModelRecord, context);
        }

        private static TResult ExecuteWithExceptionHandling<TResult>(Func<TResult> innerFunction) where TResult : BoolResult, new()
        {
            try
            {
                return innerFunction();
            }
            catch (BusinessException businessException)
            {
                var result = new TResult { IsSuccessful = false };
                result.AddAlert(AlertType.Warning, businessException.Message, true);

                return result;
            }
        }

        private static void AddActionRestorePoint(AtomicController atomicController, string actionName)
        {
            var actionPointName = $"{atomicController.Layout.Scripts.ControllerAreaName}/{atomicController.Layout.Scripts.ControllerName}/{actionName}";
            atomicController.Layout.Scripts.OnDocumentReady.Add("RestorePoints.savePoint('" + actionPointName + "')");
        }

        public FilePathResult GetImportTemplate(int importIndex, IImportService importService, ImportTemplateType templateType)
        {
            return importService.GetExcelImportTemplate(Settings, importIndex, templateType);
        }

        public ActionResult GetImportDialog(AtomicController atomicController, int importIndex)
        {
            var importSettings = Settings.Imports[importIndex];
            var importViewModel = new ImportViewModel();
            var importDialogViewModel = GetImportDialogViewModel(importViewModel, importSettings, null);

            return atomicController.GetPartialViewResult("~/Views/CardIndex/Import.cshtml", importDialogViewModel);
        }

        public ActionResult HandleImport(AtomicController atomicController, int importIndex, ImportViewModel importViewModel, IImportService importService)
        {
            var importSettings = Settings.Imports[importIndex];
            ImportRecordResult validationResult = null;

            if (importViewModel.File != null)
            {
                var temporaryDocument = importViewModel.File.TemporaryDocumentId;

                if (atomicController.ModelState.IsValid && temporaryDocument.HasValue)
                {
                    try
                    {
                        if (importViewModel.File.DocumentName.EndsWith(".csv"))
                        {
                            validationResult = importService.ImportFromCsv(temporaryDocument.Value, importSettings);
                        }
                        else if (importViewModel.File.DocumentName.EndsWith(".xlsx"))
                        {
                            validationResult = importService.ImportFromExcel(temporaryDocument.Value, importSettings);
                        }

                        if (validationResult == null)
                        {
                            throw new InvalidOperationException();
                        }

                        AddUniqueKeyViolationAlerts(validationResult, typeof(TViewModel));

                        if (validationResult.IsSuccessful)
                        {
                            AddAlerts(validationResult);

                            var dialogId = new ImportDialogViewModel(importViewModel).DialogId;

                            return DialogHelper.HandleEvent(dialogId, DialogHelper.OkButtonTag, null);
                        }
                    }
                    catch (FormatException ex)
                    {
                        throw new InvalidExcelFileFormatException(ex);
                    }
                }
            }

            var importDialogViewModel = GetImportDialogViewModel(importViewModel, importSettings, validationResult);

            return atomicController.GetPartialViewResult("~/Views/CardIndex/Import.cshtml", importDialogViewModel);
        }

        public void AddRowGrouping(Expression<Func<TViewModel, object>> propertyFunc, Func<TViewModel, string> rowLabelFunc)
        {
            var columnName = PropertyHelper.GetPropertyName<TViewModel>(propertyFunc);
            var columnCode = NamingConventionHelper.ConvertPascalCaseToHyphenated(columnName);

            _rowGroupings.Add(columnCode, rowLabelFunc);
        }

        private ImportDialogViewModel GetImportDialogViewModel(
            ImportViewModel importViewModel,
            DataImportSettings importSettings,
            BoolResult validationResult)
        {
            var alerts = validationResult != null
                ? validationResult.Alerts.Select(
                a => new AlertViewModel
                {
                    IsDismissable = a.IsDismissable,
                    Message = a.Message,
                    Type = a.Type
                })
                .ToList()
                : new List<AlertViewModel>();

            var importDialogViewModel = new ImportDialogViewModel(importViewModel)
            {
                Title = importSettings.Name,
                SubmitUrl = _urlHelper.Action("Import", new { ImportIndex = importSettings.Index }),
                Issues = alerts
            };

            return importDialogViewModel;
        }

        private void CopyFilterRequiredRolesFromDataService()
        {
            if (!Settings.FilterGroups.Any())
            {
                return;
            }

            var requiredRolesForFilters = _cardIndexDataService.GetFilterRoles();

            foreach (var requiredRoleForFilter in requiredRolesForFilters)
            {
                var filter =
                    Settings.FilterGroups.SelectMany(f => f.Filters)
                        .FirstOrDefault(f => requiredRoleForFilter.Key.ToUpperInvariant() == f.Code.ToUpperInvariant());

                if (filter != null)
                {
                    filter.RequiredRole = requiredRoleForFilter.Value;
                }
            }
        }

        private void OnBeforeListViewRender(GridViewModel model)
        {
            var firstColumn = model.GridParameters.SortingColumnCodes?.FirstOrDefault();

            if (firstColumn != null && _rowGroupings.ContainsKey(firstColumn))
            {
                var propertyFunc = _rowGroupings[firstColumn];

                model.RowGrouping = rows => rows
                .Cast<TViewModel>()
                .GroupBy(propertyFunc);
            }

            Settings.OnBeforeListViewRender?.Invoke(model);
        }

        private bool CanAddRecord(TViewModel viewModel, object context)
        {
            if (CanAddRecordRule != null)
            {
                return CanAddRecordRule(viewModel, context);
            }

            return Settings.AddButton.IsAccessibleToCurrentUser();
        }

        private bool CanEditRecord(TViewModel viewModel, object context)
        {
            if (CanEditRecordRule != null)
            {
                return CanEditRecordRule(viewModel, context);
            }

            return Settings.EditButton.IsAccessibleToCurrentUser();
        }

        private bool CanViewRecord(TViewModel viewModel, object context)
        {
            if (CanViewRecordRule != null)
            {
                return CanViewRecordRule(viewModel, context);
            }

            return Settings.ViewButton.IsAccessibleToCurrentUser();
        }

        private bool CanBulkEditRecords()
        {
            return Settings.BulkEditButton.IsAccessibleToCurrentUser();
        }
    }
}