﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex.Search
{
    public class SearchAreaViewModel
    {
        public string Name { get; private set; }

        public string DisplayName { get; private set; }

        public bool IsSelected { get; private set; }

        public SearchAreaViewModel(string name, string displayName, bool isSelected)
        {
            Name = name;
            DisplayName = displayName;
            IsSelected = isSelected;
        }
    }
}