﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex.Search
{
    public class SearchArea
    {
        public string Name { get; private set; }

        public string DisplayName { get; private set; }

        public bool IsSelectedByDefault { get; private set; }

        public SearchArea(string name, string displayName, bool isSelectedByDefault = true)
        {
            Name = name;
            DisplayName = displayName;
            IsSelectedByDefault = isSelectedByDefault;
        }
    }
}