﻿using Smt.Atomic.Presentation.Renderers.Toolbar;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface IFooterButton : ICommandButton
    {
    }
}
