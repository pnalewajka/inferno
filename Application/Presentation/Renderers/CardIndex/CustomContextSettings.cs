using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class CustomContextSettings : IContextSettings
    {
        private readonly string _viewName;
        private readonly Layout _layout;

        public CustomContextSettings(string viewName, Layout layout = null)
        {
            _viewName = viewName;
            _layout = layout;
        }

        public string ViewName => _viewName;

        public Layout GetLayout()
        {
            return _layout;
        }
    }
}