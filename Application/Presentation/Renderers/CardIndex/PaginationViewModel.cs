﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class PaginationViewModel
    {
        /// <summary>
        /// Max number of buttons to be displayed in the pagination section
        /// </summary>
        public int ButtonMaxCount { get; private set; }

        /// <summary>
        /// Should the pagination include page hint?
        /// </summary>
        public bool ShouldIncludePageHint { get; private set; }

        public PaginationViewModel(PaginationSettings settings)
        {
            ButtonMaxCount = settings.ButtonMaxCount;
            ShouldIncludePageHint = settings.ShouldIncludePageHint;
        }

        public IReadOnlyCollection<PaginationButton> GetButtons(int? currentPageIndex, int? lastPageIndex, long recordCount)
        {
            if (!currentPageIndex.HasValue || !lastPageIndex.HasValue)
            {
                return null;
            }

            if (lastPageIndex.Value == 0)
            {
                return null;
            }

            var buttons = new List<PaginationButton>
            {
                new PaginationButton(currentPageIndex.Value)
                {
                    IsActive = true
                }
            };

            while (buttons.Count < ButtonMaxCount)
            {
                var canAddBefore = buttons[0].TargetPage > 0;
                var canAddAfter = buttons[buttons.Count - 1].TargetPage < lastPageIndex.Value;

                if (!canAddBefore && !canAddAfter)
                {
                    break;
                }

                if (canAddBefore)
                {
                    var pageIndex = buttons[0].TargetPage - 1;
                    buttons.Insert(0, new PaginationButton(pageIndex));
                }

                if (canAddAfter && buttons.Count < ButtonMaxCount)
                {
                    var pageIndex = buttons[buttons.Count - 1].TargetPage + 1;
                    buttons.Add(new PaginationButton(pageIndex));
                }
            }

            var morePagesButton = new PaginationButton
            {
                Caption = CardIndexResources.MorePagesButtonCaption,
                IsDisabled = true
            };

            if (buttons[0].TargetPage != 0)
            {
                var firstPageButton = new PaginationButton(0)
                {
                    Hotkey = HotkeyHelper.GetHotkey(Key.Alt, Key.Shift, Key.Home)
                };

                while (buttons.Count > ButtonMaxCount - 2)
                {
                    buttons.RemoveAt(0);
                }

                buttons.Insert(0, morePagesButton);
                buttons.Insert(0, firstPageButton);
            }

            if (buttons.Last().TargetPage != lastPageIndex)
            {
                var lastPageButton = new PaginationButton(lastPageIndex.Value)
                {
                    Hotkey = HotkeyHelper.GetHotkey(Key.Alt, Key.Shift, Key.End)
                };

                while (buttons.Count > ButtonMaxCount - 2)
                {
                    buttons.RemoveAt(buttons.Count - 1);
                }

                buttons.Add(morePagesButton);
                buttons.Add(lastPageButton);
            }

            var previousPageButton = new PaginationButton(currentPageIndex.Value - 1)
            {
                IsFirst = true,
                Caption = CardIndexResources.PreviousPageButtonCaption,
                Hint = CardIndexResources.PreviousPageButtonHint,
                IsDisabled = currentPageIndex == 0,
                Hotkey = HotkeyHelper.GetHotkey(Key.Alt, Key.Shift, Key.PageDown)
            };
            buttons.Insert(0, previousPageButton);

            var nextPageButton = new PaginationButton(currentPageIndex.Value + 1)
            {
                IsLast = true,
                Caption = CardIndexResources.NextPageButtonCaption,
                Hint = CardIndexResources.NextPageButtonHint,
                IsDisabled = currentPageIndex == lastPageIndex,
                Hotkey = HotkeyHelper.GetHotkey(Key.Alt, Key.Shift, Key.PageUp)
            };
            buttons.Add(nextPageButton);

            if (ShouldIncludePageHint)
            {
                var caption = string.Format(CardIndexResources.RecordCountFormat, recordCount);

                var pageHint = new PaginationButton
                {
                    IsDisabled = true,
                    Caption = caption,
                    CssClass = "page-hint"
                };

                buttons.Insert(0, pageHint);
            }

            return buttons;
        }
    }
}