﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class MethodBasedGridColumnViewModel : GridColumnViewModel
    {
        private const string ViewPathFormat = "~/Views/Grid/Cells/{0}.cshtml";
        private const string ViewPathIEnumerableFormat = "~/Views/Grid/Cells/IEnumerableOf{0}.cshtml";

        private readonly IGridMethodColumnViewConfiguration _column;

        public MethodBasedGridColumnViewModel(IGridMethodColumnViewConfiguration column, SortingDirection sortingDirection)
            : base(column, sortingDirection)
        {
            _column = column;

            SetProperties();
            SetViewPath();
        }

        private void SetProperties()
        {
            var propertyName = ViewModelMemberHelper.GetSortableByPropertyName(_column.MethodInfo);

            if (propertyName != null)
            {
                SortByPropertyName = NamingConventionHelper.ConvertPascalCaseToHyphenated(propertyName);
                IsSortable = _column.IsSortable;
            }
        }

        private void SetViewPath()
        {
            var resultType = _column.MethodInfo.ReturnType;

            if (TypeHelper.IsEnumeration(resultType))
            {
                var elementType = TypeHelper.GetEnumerationElementType(resultType);
                ViewPath = string.Format(ViewPathIEnumerableFormat, elementType.Name);
            }
            else
            {
                ViewPath = string.Format(ViewPathFormat, resultType.Name);
            }
        }

        public override object Render(object row, UrlHelper urlHelper)
        {
            var value = _column.MethodInfo.Invoke(row, new object[] { urlHelper });

            if (typeof(TooltipText).IsAssignableFrom(_column.MethodInfo.ReturnType))
            {
                var hubAttribute = _column.MethodInfo.GetCustomAttributes(typeof(HubAttribute), false).SingleOrDefault() as HubAttribute;
                var tooltipText = value as TooltipText;

                if (hubAttribute != null)
                {
                    var pickerProperty = hubAttribute.ResolveReflectedPickerProperty(row.GetType());
                    var lookupAttribute = hubAttribute.GetLookupAttribute(pickerProperty);

                    if (lookupAttribute.HasHub)
                    {
                        var pickerValue = pickerProperty.GetValue(row);

                        var hubDataAttributes = new Dictionary<string, string>
                        {
                            { "hub-identifier", lookupAttribute.TypeIdentifier },
                            { "hub-recordid", pickerValue.ToString() }
                        };

                        tooltipText.DataAttributes = hubDataAttributes;
                    }
                }
            }

            return value;
        }
    }
}
