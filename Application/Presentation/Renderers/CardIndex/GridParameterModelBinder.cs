using System;
using System.Globalization;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class GridParameterModelBinder : IModelBinder
    {
        public const string SearchParameterUrlName = "search";
        public const string PageParameterUrlName = "page";
        public const string OrderParameterUrlName = "order";
        public const string FilterParameterUrlName = "filter";
        public const string PageSizeParameterUrlName = "page-size";
        public const string ContextParentIdParameterUrlName = RoutingHelper.ParentIdParameterName;
        public const string ValuePickerModeParameterUrlName = "valuepicker";
        public const string MultiselectParameterUrlName = "multiselect";
        public const string GridViewNameParameterUrlName = "view";
        public const string GridExpandParameterUrlName = "gridexpand";

        private const int MinPageSize = 1;
        private const int MaxPageSize = 100;

        private IValueProvider _valueProvider;

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var requestQueryString = controllerContext.HttpContext.Request.Unvalidated.QueryString;
            _valueProvider = new NameValueCollectionValueProvider(requestQueryString, CultureInfo.InvariantCulture);

            var gridParameters = new GridParameters
            {
                SearchPhrase = GetValue<string>(SearchParameterUrlName),
                CurrentPageIndex = GetValue<int?>(PageParameterUrlName),
                OrderBy = GetValue<string>(OrderParameterUrlName),
                Filters = GetValue<string>(FilterParameterUrlName),
                PageSize = GetValue<int?>(PageSizeParameterUrlName),
                RenderingControlContext =
                    (GetValue<bool>(ValuePickerModeParameterUrlName)
                        ? GridRenderingControlContext.ValuePicker
                        : GridRenderingControlContext.None)
                    | (GetValue<bool>(GridExpandParameterUrlName)
                        ? GridRenderingControlContext.GridExpansion
                        : GridRenderingControlContext.None),
                IsMultiselectAllowed = GetValue<bool>(MultiselectParameterUrlName),
                ViewName = GetValue<string>(GridViewNameParameterUrlName)
            };

            gridParameters.QueryStringFieldValues = requestQueryString.ToDictionary();

            var cardIndexController = controllerContext.Controller as ICardIndexController;
            
            if (cardIndexController != null)
            {
                gridParameters.Context = cardIndexController.Context;
            }

            ValidatePageSize(gridParameters);

            return gridParameters;
        }

        private static void ValidatePageSize(GridParameters gridParameters)
        {
            var pageSize = gridParameters.PageSize;

            if (pageSize != null)
            {
                if (pageSize < MinPageSize || pageSize > MaxPageSize)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        private T GetValue<T>(string key)
        {
            var result = _valueProvider.GetValue(key);
            return (result == null) ? default(T) : (T)result.ConvertTo(typeof(T));
        }
    }
}