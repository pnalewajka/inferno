using System;
using System.ComponentModel;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class DataImportSettings
    {
        /// <summary>
        /// Security role which is required for importing data
        /// </summary>
        public SecurityRoleType? RequiredRole { get; set; }

        /// <summary>
        /// Index of the import configuration
        /// </summary>
        public long Index { get; internal set; }

        /// <summary>
        /// Display name of the import configuration which will be used for section in import drop down menu
        /// </summary>
        [Localizable(true)]
        public string Name { get; set; }

        /// <summary>
        /// If true then configuration will be used during import drop down menu creation
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// If true then export template will rely on Id column to determin record identity
        /// </summary>
        public bool UseRecordId { get; set; }

        /// <summary>
        /// Type of the view model which should be used for export
        /// </summary>
        public Type ViewModelType { get; set; }

        /// <summary>
        /// Type of the data transfer object which should be used when passing data to card index data service
        /// </summary>
        public Type DtoType { get; set; }

        /// <summary>
        /// Url of the dialog for downloading empty import template
        /// </summary>
        public string DownloadEmptyTemplateUrl { get; set; }

        /// <summary>
        /// Url of the dialog for downloading edit import template
        /// </summary>
        public string DownloadEditTemplateUrl { get; set; }

        /// <summary>
        /// Url of the dialog for importing data
        /// </summary>
        public string ImportDialogUrl { get; set; }

        /// <summary>
        /// Data import service which should be used for importing data
        /// </summary>
        public IDataImportService DataImportService { get; set; }

        /// <summary>
        /// Import type
        /// </summary>
        public ImportBehavior ImportBehavior { get; set; }
    }
}