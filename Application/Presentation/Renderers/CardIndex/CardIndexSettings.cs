﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Consts;
using Smt.Atomic.Presentation.Renderers.Enums;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.Presentation.Renderers.TreeView;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class CardIndexSettings
    {
        public const string StandardEditOperationGroup = "standard-edit-operations";
        public const string StandardGridOperationGroup = "standard-grid-operations";

        private static readonly string AddButtonId = Guid.NewGuid().ToString();
        private static readonly string EditButtonId = Guid.NewGuid().ToString();
        private static readonly string BulkEditButtonId = Guid.NewGuid().ToString();
        private static readonly string ViewButtonId = Guid.NewGuid().ToString();
        private static readonly string DeleteButtonId = Guid.NewGuid().ToString();
        private static readonly string ExportToExcelButtonId = Guid.NewGuid().ToString();
        private static readonly string ExportToCsvButtonId = Guid.NewGuid().ToString();
        private static readonly string CopyToClipboardButtonId = Guid.NewGuid().ToString();

        internal static readonly string ExportButtonId = Guid.NewGuid().ToString();

        private readonly UrlHelper _urlHelper;
        private string _header;

        /// <summary>
        /// True if the condensed mode for the table should be used
        /// </summary>
        public bool Condensed { get; set; }

        /// <summary>
        /// True if more than on row can be selected
        /// </summary>
        public bool AllowMultipleRowSelection { get; set; }

        /// <summary>
        /// Javascript expression or handler function name
        /// </summary>
        public string OnSelectionChangedJavaScript { get; set; }

        /// <summary>
        /// Javascript expression or handler function name
        /// </summary>
        public string OnRenderedJavaScript { get; set; }

        /// <summary>
        /// Javascript expression or handler function name
        /// </summary>
        public string OnRowDoubleClickedJavaScript { get; set; }

        /// <summary>
        /// If more then one grid is used on the same page this should be set to some value to distinguish between url parameter sets
        /// </summary>
        public string BindingPrefix { get; set; }

        /// <summary>
        /// Core part of the page title (and default header) rendered for each view
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Core part of the page header rendered for each view
        /// </summary>
        public string Header
        {
            get { return _header ?? Title; }
            set { _header = value; }
        }

        /// <summary>
        /// Name of the view which should be used for add a new record form
        /// </summary>
        public string AddViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used for add dialog new record from
        /// </summary>
        public string AddDialogViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used for a record editing 
        /// </summary>
        public string EditViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used for edit dialog record from
        /// </summary>
        public string EditDialogViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used for a record viewing 
        /// </summary>
        public string ViewViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used for viewing record
        /// </summary>
        public string ViewDialogViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used for displaying record list
        /// </summary>
        public string ListViewName { get; set; }

        /// <summary>
        /// Name of the view which should be used to render an empty list
        /// </summary>
        public string EmptyListViewName { get; set; }

        /// <summary>
        /// Model which should be used to render an empty list
        /// </summary>
        public object EmptyListViewModel { get; set; }

        /// <summary>
        /// Page size which will override page size specified in the service
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Toolbar buttons which will be rendered in the grid toolbar
        /// </summary>
        public List<ICommandButton> ToolbarButtons { get; private set; }

        /// <summary>
        /// Context menu options
        /// </summary>
        public List<ICommandButton> ContextMenu { get; private set; }

        /// <summary>
        /// Command buttons which will be rendered for each row
        /// </summary>
        public List<ICommandButton> RowButtons { get; private set; }

        /// <summary>
        /// How should be the row menu options be presented?
        /// </summary>
        public RowMenuMode RowMenuMode { get; set; }

        /// <summary>
        /// Preferred way to display grid items
        /// </summary>
        public GridDisplayMode DisplayMode { get; set; }

        /// <summary>
        /// Preferred way to render tree grid items
        /// </summary>
        public TreeDisplayMode TreeMode { get; set; }

        /// <summary>
        /// What view should be used to render a custom grid display mode
        /// </summary>
        public string CustomGridDisplayModeViewName { get; set; }

        /// Javascript function to use for tile layout adjustments
        /// </summary>
        public string InitializeJavaScriptAction { get; set; }

        /// <summary>
        /// Returns a standard add new record button
        /// </summary>
        public ICommandButton AddButton
        {
            get { return ToolbarButtons.Single(b => b.Id == AddButtonId); }
        }

        /// <summary>
        /// Returns a standard edit record button
        /// </summary>
        public ICommandButton EditButton
        {
            get { return ToolbarButtons.Single(b => b.Id == EditButtonId); }
        }

        /// <summary>
        /// Returns a standard bulk edit record button
        /// </summary>
        public ICommandButton BulkEditButton
        {
            get { return ToolbarButtons.Single(b => b.Id == BulkEditButtonId); }
        }

        /// <summary>
        /// Returns a standard view record button
        /// </summary>
        public ICommandButton ViewButton
        {
            get { return ToolbarButtons.Single(b => b.Id == ViewButtonId); }
        }

        /// <summary>
        /// Returns a standard delete button
        /// </summary>
        public ICommandButton DeleteButton
        {
            get { return ToolbarButtons.Single(b => b.Id == DeleteButtonId); }
        }

        /// <summary>
        /// Expression which will be used to generate default new record when adding
        /// </summary>
        public Func<object> DefaultNewRecord { get; set; }

        /// <summary>
        /// Details on how to render the context section
        /// </summary>
        public IContextSettings ContextSettings { get; set; }

        /// <summary>
        /// Action called before the generated add form is rendered. It allows for minor form changes (readonly, visibility, etc)
        /// </summary>
        public Action<FormRenderingModel> AddFormCustomization { get; set; }

        /// <summary>
        /// Action called before the generated edit form is rendered. It allows for minor form changes (readonly, visibility, etc)
        /// </summary>
        public Action<FormRenderingModel> EditFormCustomization { get; set; }

        /// <summary>
        /// Action called before the generated view form is rendered. It allows for minor form changes (readonly, visibility, etc)
        /// </summary>
        public Action<FormRenderingModel> ViewFormCustomization { get; set; }

        /// <summary>
        /// Allows for some model validation customization
        /// </summary>
        public Action<object, ModelStateDictionary> ValidationCustomization { get; set; }

        /// <summary>
        /// Indicates if search box should be hidden, e.g. due to no search columns 
        /// </summary>
        public bool ShouldHideSearchBox { get; set; }

        /// <summary>
        /// Indicates if filters dropdown should be hidden, e.g. due to no named filters
        /// </summary>
        public bool ShouldHideFilters { get; set; }

        /// <summary>
        /// Indicates if custom views dropdown should be hidden
        /// </summary>
        public bool ShouldHideViewConfigurations { get; set; }

        /// <summary>
        /// Indicates if summary context should be collapsed
        /// </summary>
        public bool IsContextViewCollapsed { get; set; }

        /// <summary>
        /// Text to be displayed when no records are present
        /// </summary>
        public string EmptyListMessage { get; set; }

        /// <summary>
        /// Url which can be used to export grid data to csv
        /// </summary>
        public string ExportToCsvUrl { get; set; }

        /// <summary>
        /// Url which can be used to export grid data to excel
        /// </summary>
        public string ExportToExcelUrl { get; set; }

        /// <summary>
        /// Url which can be used to copy grid data to clipboard
        /// </summary>
        public string CopyToClipboardUrl { get; set; }

        /// <summary>
        /// Filters definition
        /// </summary>
        public IList<FilterGroup> FilterGroups { get; set; }

        /// <summary>
        /// Search areas
        /// </summary>
        public IList<SearchArea> SearchAreas { get; set; }

        /// <summary>
        /// Pagination settings
        /// </summary>
        public PaginationSettings Pagination { get; private set; }

        /// <summary>
        /// Allow context to be null on creation. (Will allow null if ContextType is not defined)
        /// </summary>
        public bool IsContextRequired { get; set; }

        public List<DataImportSettings> Imports { get; private set; }

        /// <summary>
        /// Should the grid react to the enter button
        /// </summary>
        public bool ShouldEnterInvokeDefaultButton { get; set; }

        /// <summary>
        /// Should the grid react to the enter button
        /// </summary>
        public bool ShouldDoubleClickToggleSelection { get; set; }

        /// <summary>
        /// Should edit view have Delete button
        /// </summary>
        public bool ShowDeleteButtonOnEdit { get; set; }

        /// <summary>
        /// Modification tracking
        /// </summary>
        public FormModificationTracking ModificationTracking { get; set; }

        public IList<IGridViewConfiguration> GridViewConfigurations { get; set; }

        public Type CardIndexDataServiceType { get; internal set; }

        public Action<GridViewModel> OnBeforeListViewRender { get; set; }

        public bool ShowValidationAlerts { get; set; }

        public CardIndexSettings(UrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
            Imports = new List<DataImportSettings>();
            FilterGroups = new List<FilterGroup>();
            SearchAreas = new List<SearchArea>();
            AllowMultipleRowSelection = true;

            ShouldEnterInvokeDefaultButton = true;
            ShouldDoubleClickToggleSelection = false;
            ShowDeleteButtonOnEdit = false;

            AddViewName = "~/Views/CardIndex/Add.cshtml";
            EditViewName = "~/Views/CardIndex/Edit.cshtml";
            ViewViewName = "~/Views/CardIndex/View.cshtml";
            ListViewName = "~/Views/CardIndex/List.cshtml";
            EmptyListViewName = "~/Views/Grid/_EmptyListMessage.cshtml";

            AddDialogViewName = "~/Views/CardIndex/AddDialog.cshtml";
            EditDialogViewName = "~/Views/CardIndex/EditDialog.cshtml";
            ViewDialogViewName = "~/Views/CardIndex/ViewDialog.cshtml";

            ToolbarButtons = new List<ICommandButton>();
            ContextMenu = new List<ICommandButton>();
            RowButtons = new List<ICommandButton>();

            EmptyListMessage = CardIndexResources.EmptyListMessage;

            IsContextRequired = true;

            Pagination = new PaginationSettings();

            ModificationTracking = FormModificationTracking.None;
        }

        public void AddReadOnlyCardIndexButtons()
        {
            var viewButton = new ToolbarButton
            {
                Id = ViewButtonId,
                Icon = FontAwesome.FileTextO,
                Group = StandardEditOperationGroup,
                Text = CardIndexResources.CardIndexViewRecordButtonText,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                IsDefault = true,
                AutomationIdentifier = AutomationIdentifiers.CardIndexViewButtonIdentifier,
            };

            ToolbarButtons.Add(viewButton);
        }

        public void AddStandardCardIndexButtons()
        {
            var addButton = new ToolbarButton
            {
                Id = AddButtonId,
                Icon = Glyphicons.Plus,
                Text = CardIndexResources.CardIndexAddRecordButtonText,
                Group = StandardEditOperationGroup,
                AutomationIdentifier = AutomationIdentifiers.CardIndexAddButtonIdentifier,
            };

            ToolbarButtons.Insert(0, addButton);

            var editButton = new ToolbarButton
            {
                Id = EditButtonId,
                Icon = FontAwesome.PencilSquareO,
                Group = StandardEditOperationGroup,
                Text = CardIndexResources.CardIndexEditRecordButtonText,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.SingleRowSelected
                },
                IsDefault = true,
                AutomationIdentifier = AutomationIdentifiers.CardIndexEditButtonIdentifier,
            };

            ToolbarButtons.Insert(1, editButton);

            if (ToolbarButtons.All(b => b.Id != ViewButtonId))
            {
                AddReadOnlyCardIndexButtons();
            }

            var deleteButton = new ToolbarButton
            {
                Id = DeleteButtonId,
                Icon = Glyphicons.Remove,
                Group = StandardEditOperationGroup,
                Text = CardIndexResources.CardIndexDeleteRecordButtonText,
                Confirmation = new Confirmation
                {
                    IsRequired = true,
                    Message = CardIndexResources.CardIndexRemoveRecordsConfirmation
                },
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                },
                AutomationIdentifier = AutomationIdentifiers.CardIndexDeleteButtonIdentifier,
            };

            ToolbarButtons.Add(deleteButton);
        }

        public void AddExportButtons(SecurityRoleType? requiredRole = null)
        {
            var toolbarButton = new ToolbarButton
            {
                Id = ExportButtonId,
                Icon = FontAwesome.Table,
                Group = StandardGridOperationGroup,
                RequiredRole = requiredRole,
                Text = CardIndexResources.ExportDropDownButtonText,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.Always,
                },
                OnClickAction = new DropdownAction
                {
                    Items =
                    {
                        new CommandButton
                        {
                            Id = ExportToCsvButtonId,
                            Text = CardIndexResources.ExportToExcelButtonText,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.Always,
                            },
                            OnClickAction = new JavaScriptCallAction("Grid.onExportToExcelClicked(event);"),
                            AutomationIdentifier = AutomationIdentifiers.CardIndexExportToExcelButtonIdentifier,
                        },
                        new CommandButton
                        {
                            Id = ExportToExcelButtonId,
                            Text = CardIndexResources.ExportToCsvButtonText,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.Always,
                            },
                            OnClickAction = new JavaScriptCallAction("Grid.onExportToCsvClicked(event);"),
                            AutomationIdentifier = AutomationIdentifiers.CardIndexExportToCsvButtonIdentifier,
                        },
                        new CommandButton
                        {
                            Id = CopyToClipboardButtonId,
                            Text = CardIndexResources.CopyToClipboard,
                            Availability = new ToolbarButtonAvailability
                            {
                                Mode = AvailabilityMode.Always,
                            },
                            OnClickAction = new JavaScriptCallAction("Grid.onCopyToClipboardClicked(event);"),
                            AutomationIdentifier = AutomationIdentifiers.CardIndexCopyToClipboardButtonIdentifier,
                        }
                    }
                }
            };

            ToolbarButtons.Add(toolbarButton);
        }

        public string GetEditPageTitle()
        {
            return string.Format(CardIndexResources.CardIndexDetailedPageTitleFormat, Title, GetButtonBasedTitle(EditButton.Text));
        }

        public string GetEditPageHeader<TViewModel>(TViewModel viewModelRecord)
        {

            return TypeHelper.IsMethodOverridden(nameof(ToString), typeof(TViewModel))
                ? string.Format(CardIndexResources.CardIndexDetailedPageExtendedHeaderFormat, Header, viewModelRecord, GetButtonBasedTitle(EditButton.Text))
                : string.Format(CardIndexResources.CardIndexDetailedPageHeaderFormat, Header, GetButtonBasedTitle(EditButton.Text));
        }

        public string GetViewPageTitle()
        {
            return string.Format(CardIndexResources.CardIndexDetailedPageTitleFormat, Title, GetButtonBasedTitle(ViewButton.Text));
        }

        public string GetViewPageHeader<TViewModel>(TViewModel viewModelRecord)
        {
            return TypeHelper.IsMethodOverridden(nameof(ToString), typeof(TViewModel)) ?
                string.Format(CardIndexResources.CardIndexDetailedPageExtendedHeaderFormat, Header, viewModelRecord, GetButtonBasedTitle(ViewButton.Text)) :
                string.Format(CardIndexResources.CardIndexDetailedPageHeaderFormat, Header, GetButtonBasedTitle(ViewButton.Text));
        }

        public string GetAddPageTitle()
        {
            return string.Format(CardIndexResources.CardIndexDetailedPageTitleFormat, Title, GetButtonBasedTitle(AddButton.Text));
        }

        public string GetAddPageHeader()
        {
            return string.Format(CardIndexResources.CardIndexDetailedPageHeaderFormat, Header, GetButtonBasedTitle(AddButton.Text));
        }

        public string GetBulkEditPageHeader(string stepDescription)
        {
            return string.Format(CardIndexResources.CardIndexBulkEditPageHeaderFormat, Title, GetButtonBasedTitle(BulkEditButton.Text), stepDescription);
        }

        public string GetBulkEditPageTitle(string stepDescription)
        {
            return string.Format(CardIndexResources.CardIndexBulkEditPageTitleFormat, Title, GetButtonBasedTitle(BulkEditButton.Text), stepDescription);
        }

        private string GetButtonBasedTitle(string buttonText)
        {
            return HtmlMarkupHelper.RemoveAccelerator(buttonText);
        }

        public DataImportSettings AddImport(Type importViewModelType, Type importDtoType, IDataImportService cardIndexDataService, string name = null, SecurityRoleType? requiredRole = null, ImportBehavior importBehavior = ImportBehavior.InsertOrUpdate)
        {
            var dataImportSettings = new DataImportSettings
            {
                Index = Imports.Count,
                // ReSharper disable Mvc.ActionNotResolved
                DownloadEmptyTemplateUrl = _urlHelper.Action("ImportTemplate", new { importIndex = Imports.Count, templateType = ImportTemplateType.Empty }),
                DownloadEditTemplateUrl = _urlHelper.Action("ImportTemplate", new { importIndex = Imports.Count, templateType = ImportTemplateType.Edit }),
                ImportDialogUrl = _urlHelper.Action("ImportDialog", new { importIndex = Imports.Count }),
                // ReSharper restore Mvc.ActionNotResolved
                IsEnabled = true,
                Name = name,
                UseRecordId = true,
                ViewModelType = importViewModelType,
                DtoType = importDtoType,
                DataImportService = cardIndexDataService,
                RequiredRole = requiredRole,
                ImportBehavior = importBehavior
            };

            Imports.Add(dataImportSettings);

            return dataImportSettings;
        }

        public DataImportSettings AddImport<TImportViewModel, TImportDto>(IDataImportService cardIndexDataService, string name = null,
            SecurityRoleType? requiredRole = null, ImportBehavior importBehavior = ImportBehavior.InsertOrUpdate)
        {
            return AddImport(typeof(TImportViewModel), typeof(TImportDto), cardIndexDataService, name, requiredRole, importBehavior);
        }

        public void ConfigureBulkEdit<TCardIndexService>(Action<ICommandButton> buttonConfiguration)
        {
            var bulkEditButton = new ToolbarButton
            {
                Id = BulkEditButtonId,
                Icon = FontAwesome.Edit,
                Group = StandardGridOperationGroup,
                Text = CardIndexResources.CardIndexBulkEditButtonText,
                Availability = new ToolbarButtonAvailability
                {
                    Mode = AvailabilityMode.AtLeastOneRowSelected,
                }
            };
            buttonConfiguration(bulkEditButton);

            ToolbarButtons.Add(bulkEditButton);
            CardIndexDataServiceType = typeof(TCardIndexService);
        }
    }
}