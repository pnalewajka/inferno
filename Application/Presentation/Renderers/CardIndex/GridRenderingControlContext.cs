﻿using System;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    [Flags]
    public enum GridRenderingControlContext
    {
        None = 0,
        ValuePicker = 1 << 0,
        GridExpansion = 1 << 1
    }
}
