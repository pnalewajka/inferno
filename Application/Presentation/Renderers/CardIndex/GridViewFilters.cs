using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Toolbar;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class GridViewFilters
    {
        private readonly CardIndexSettings _settings;
        private readonly QueryCriteria _queryCriteria;
        private readonly GridRenderingControlContext _renderingControlContext;
        private readonly HashSet<string> _userRoles;
        private readonly IDictionary<string, object> _queryStringFieldValues;

        public IReadOnlyCollection<FilterGroupViewModel> FilterGroupViewModels { get; private set; }
        public Dictionary<string, IFilteringObject> FilterObjects { get; private set; }

        public GridViewFilters(CardIndexSettings settings, QueryCriteria queryCriteria, GridRenderingControlContext renderingControlContext, HashSet<string> userRoles, IDictionary<string, object> queryStringFieldValues)
        {
            _settings = settings;
            _queryCriteria = queryCriteria;
            _renderingControlContext = renderingControlContext;
            _userRoles = userRoles;
            _queryStringFieldValues = queryStringFieldValues;

            CreateFiltersViewModel();
        }

        private bool ShouldElementBeVisible(CommandButtonVisibility visibility)
        {
            return visibility.HasFlag(_renderingControlContext.HasFlag(GridRenderingControlContext.ValuePicker) ? CommandButtonVisibility.ValuePicker : CommandButtonVisibility.Grid);
        }

        private void CreateFiltersViewModel()
        {
            var filtersViewModel = _settings.FilterGroups
                .Where(g => ShouldElementBeVisible(g.Visibility))
                .Select(g => new FilterGroupViewModel(g, _userRoles))
                .Where(v => v.Filters.Any())
                .ToList();

            var selectedFilters = _queryCriteria.Filters
                .SelectMany(g => g.Codes)
                .Select(f => f.ToUpperInvariant())
                .ToHashSet();

            FilterObjects = new Dictionary<string, IFilteringObject>();

            foreach (var filterGroup in filtersViewModel)
            {
                foreach (var filter in filterGroup.Filters)
                {
                    filter.IsSelected = selectedFilters.Contains(filter.Code.ToUpperInvariant());

                    if (!filter.IsSelected)
                    {
                        continue;
                    }

                    if (filter.IsParametric)
                    {
                        filter.InitFilterParameters(_queryStringFieldValues);
                    }
                    
                    FilterObjects.Add(filter.Code, filter);
                }
            }

            FilterGroupViewModels = filtersViewModel;
        }
        
    }
}