﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface ICellContent
    {
        string CssClass { get; }
    }
}
