using System;
using System.Collections.Generic;
using System.Threading;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Enums;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class CardIndexRecordViewModel<TViewModel> : ICardIndexRecordViewModel, ICardIndexRecordWithButtonsViewModel
        where TViewModel : class, new()
    {
        public object ContextViewModel { get; set; }

        public TViewModel ViewModelRecord { get; set; }

        public Action<FormRenderingModel> AddFormCustomization { get; set; }

        public Action<FormRenderingModel> EditFormCustomization { get; set; }

        public Action<FormRenderingModel> ViewFormCustomization { get; set; }

        public string CancelButtonUrl { get; set; }

        public string DeleteButtonUrl { get; }

        public bool ShowDeleteButtonOnEdit { get; }

        object ICardIndexRecordViewModel.ViewModelRecord => ViewModelRecord;

        public IList<IFooterButton> Buttons { get; set; } = new List<IFooterButton>();

        public FormModificationTracking ModificationTracking { get; set; }

        public CardIndexRecordViewModel(TViewModel record, CardIndexSettings cardIndexSettings, string cancelUrl, string deleteUrl)
        {
            ViewModelRecord = record;
            CancelButtonUrl = cancelUrl;
            DeleteButtonUrl = deleteUrl;
            ShowDeleteButtonOnEdit = cardIndexSettings.ShowDeleteButtonOnEdit
                                     && (
                                        !cardIndexSettings.DeleteButton.RequiredRole.HasValue
                                        || Thread.CurrentPrincipal.IsInRole(cardIndexSettings.DeleteButton.RequiredRole.ToString()));
            AddFormCustomization = cardIndexSettings.AddFormCustomization;
            EditFormCustomization = cardIndexSettings.EditFormCustomization;
            ViewFormCustomization = cardIndexSettings.ViewFormCustomization;
            ModificationTracking = cardIndexSettings.ModificationTracking;
        }
    }
}