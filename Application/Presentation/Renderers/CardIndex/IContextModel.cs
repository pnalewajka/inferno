namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface IContextModel
    {
        /// <summary>
        /// Context view model which will be displayed above/next to grid
        /// </summary>
        object ContextViewModel { get; }

        /// <summary>
        /// Details on how to render the context section
        /// </summary>
        IContextSettings ContextSettings { get; }

        /// <summary>
        /// Indicates if summary context should be collapsed
        /// </summary>
        bool IsContextViewCollapsed { get; }
    }
}
