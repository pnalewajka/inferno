using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    /// <summary>
    /// Collection of columns made easy to access also in custom views
    /// </summary>
    public class GridTableColumns
    {
        private readonly IList<GridColumnViewModel> _columns;
        private readonly IDictionary<string, GridColumnViewModel> _columnsByName;

        public GridTableColumns(IList<GridColumnViewModel> columns)
        {
            _columns = columns;
            _columnsByName = _columns.ToDictionary(c => c.MemberInfoName);
        }

        public IList<GridColumnViewModel> GetAll()
        {
            return _columns;
        }

        public GridColumnViewModel GetColumn<TModel>(Expression<Func<TModel, object>> propertyNameExpression)
        {
            var propertyName = PropertyHelper.GetPropertyName(propertyNameExpression);
            
            return _columnsByName[propertyName];
        }

        public object RenderColumn<TModel>(Expression<Func<TModel, object>> propertyNameExpression, object row, UrlHelper urlHelper, string nullOrEmptyText = "")
        {
            var propertyName = PropertyHelper.GetPropertyName(propertyNameExpression);
            var column = _columnsByName[propertyName];

            var renderedValue = $"{column.Render(row, urlHelper)}";

            return string.IsNullOrEmpty(renderedValue)
                       ? nullOrEmptyText
                       : renderedValue;
        }

        public bool TryRemoveColumn(string columnName)
        {
            if (!_columnsByName.ContainsKey(columnName))
            {
                return false;
            }

            var column = _columns.Single(c => c.MemberInfoName == columnName);
            _columns.Remove(column);
            _columnsByName.Remove(columnName);

            return true;
        }
    }
}