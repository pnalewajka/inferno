using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    [ModelBinder(typeof(GridParameterModelBinder))]
    public class GridParameters
    {
        private const char ListSeparator = ',';
        private const string DescSufix = "-desc";

        public string SearchPhrase { get; set; }

        public string OrderBy { get; set; }

        public string Filters { get; set; }

        public int? CurrentPageIndex { get; set; }

        public int? PageSize { get; set; }

        public object Context { get; set; }

        public IDictionary<string, object> QueryStringFieldValues { get; set; }

        public string ViewName { get; set; }

        /// <summary>
        /// Grid rendering context from control  
        /// </summary>
        public GridRenderingControlContext RenderingControlContext { get; set; }

        /// <summary>
        /// Selection mode (when in value picker mode)
        /// </summary>
        public bool? IsMultiselectAllowed { get; set; }

        public string[] SortingColumnCodes { get; private set; }

        public GridParameters()
        {
        }

        public GridParameters(QueryCriteria criteria)
        {
            SearchPhrase = criteria.SearchPhrase;
            CurrentPageIndex = criteria.CurrentPageIndex;
            PageSize = criteria.PageSize;
            QueryStringFieldValues = criteria.QueryStringFieldValues;
            
            SetOrderBy(criteria);
            SetFilterString(criteria);
        }

        public QueryCriteria ToCriteria<TViewModel, TDto>(
            IClassMapping<TDto, TViewModel> dtoToViewModelMapping, 
            IAnonymizationService anonymizationService, 
            IEnumerable<string> userRoles, 
            IList<FilterGroup> filterGroups)
            where TViewModel : class
            where TDto : class
        {
            var readRestrictedViewModelProperties = anonymizationService.GetReadRestrictedProperties(typeof(TViewModel)).ToHashSet();
            var readRestrictedDtoProperties = anonymizationService.GetReadRestrictedPropertiesForRelatedObject(dtoToViewModelMapping, readRestrictedViewModelProperties.ToHashSet());
            var readRestrictedFilterCodes = FilterGroupHelper.GetReadRestrictedFilterCodes(filterGroups, userRoles).ToHashSet();

            var result = new QueryCriteria
            {
                SearchPhrase = SearchPhrase,
                CurrentPageIndex = CurrentPageIndex,
                PageSize = PageSize,
                ReadRestrictedDtoProperties = readRestrictedDtoProperties.ToHashSet(),
                QueryStringFieldValues = QueryStringFieldValues
            };

            SetOrderBy(dtoToViewModelMapping, result);
            SetCriteriaFilters(result, readRestrictedFilterCodes.ToHashSet());

            result.Context = Context;

            SetDefaultFilterForSecuritySensitiveGroups(filterGroups, result, readRestrictedFilterCodes);

            return result;
        }

        private static void SetDefaultFilterForSecuritySensitiveGroups(
            IList<FilterGroup> filterGroups,
            QueryCriteria result, 
            HashSet<string> readRestrictedFilterCodes)
        {
            foreach (var filterGroup in filterGroups)
            {
                var isGroupSecuritySensitive = filterGroup.Filters.Any(f => f.RequiredRole != null) && filterGroup.Type == FilterGroupType.RadioGroup;
                var isGroupSet = result.Filters.SelectMany(g => g.Codes).Any(c => filterGroup.Filters.Any(f => f.Code == c));

                if (isGroupSecuritySensitive && !isGroupSet)
                {
                    var isAnyFilterAvailable = filterGroup.Filters.Any(f => !readRestrictedFilterCodes.Contains(f.Code));

                    if (!isAnyFilterAvailable)
                    {
                        throw new AllFiltersAreRestrictedException();
                    }

                    result.Filters.Add(new FilterCodeGroup
                    {
                        IsConjunction = filterGroup.Type == FilterGroupType.AndCheckboxGroup,
                        Codes = new List<string>
                        {
                            filterGroup.Filters.Select(f => f.Code).First(c => !readRestrictedFilterCodes.Contains(c))
                        }
                    });
                }
            }
        }

        private static SortingCriterion GetSortingCriterion(string viewModelColumnName, IDictionary<string, List<string>> fieldMappings)
        {
            var columnName = NamingConventionHelper.ConvertHyphenatedToPascalCase(viewModelColumnName.TrimEnd(DescSufix));

            var dtoColumnName = GetDtoColumnName(fieldMappings, columnName);

            return new SortingCriterion(dtoColumnName, columnName, !viewModelColumnName.EndsWith(DescSufix));
        }

        private static string GetDtoColumnName(IDictionary<string, List<string>> fieldMappings, string columnName)
        {
            if (fieldMappings.ContainsKey(columnName))
            {
                var fieldMapping = fieldMappings[columnName];
                return fieldMapping[0];
            }

            return columnName;
        }

        private void SetOrderBy(QueryCriteria criteria)
        {
            if (criteria.OrderBy.IsNullOrEmpty())
            {
                return;
            }

            var separator = ListSeparator.ToInvariantString();

            var orderByColumns = criteria
                .OrderBy
                .Select
                (
                    d => new
                    {
                        Code = NamingConventionHelper.ConvertPascalCaseToHyphenated(d.GetPresentationColumnName()),
                        Direction = d.Ascending ? string.Empty : DescSufix
                    }
                ).ToList();

            var columnNames = orderByColumns.Select(c => c.Code + c.Direction);

            OrderBy = string.Join(separator, columnNames);
            SortingColumnCodes = orderByColumns.Select(c => c.Code).ToArray();
        }

        private void SetOrderBy<TViewModel, TDto>(IClassMapping<TDto, TViewModel> dtoToClassMapping, QueryCriteria result)
            where TViewModel : class
            where TDto : class
        {
            if (OrderBy == null)
            {
                return;
            }

            var fieldMappings = dtoToClassMapping.GetFieldMappings(typeof(TDto), typeof(TViewModel));

            result.OrderBy = OrderBy
                .Split(ListSeparator)
                .Select(o => GetSortingCriterion(o, fieldMappings))
                .Where(s => !s.IsReadRestricted(result.ReadRestrictedDtoProperties))
                .ToList();
        }

        private void SetCriteriaFilters(QueryCriteria result, HashSet<string> readRestrictedFilterCodes)
        {
            result.Filters = Filters == null
                ? new List<FilterCodeGroup>()
                : FiltersHelper.ParseFilterString(Filters, readRestrictedFilterCodes);
        }

        private void SetFilterString(QueryCriteria criteria)
        {
            Filters = criteria.Filters.IsNullOrEmpty()
                ? Filters
                : FiltersHelper.CreateFilterString(criteria.Filters);
        }
    }
}