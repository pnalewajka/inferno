﻿using System.Globalization;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class ProgressBar : ICellContent
    {
        private decimal _value;

        public string BarCssClass { get; set; }

        public string CssClass { get; set; }

        public decimal Value
        {
            get { return _value; }
            set { _value = decimal.Truncate(value); }
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:F} %", Value);
        }
    }
}