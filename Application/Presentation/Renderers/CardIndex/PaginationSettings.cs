﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class PaginationSettings
    {
        private const int DefaultButtonMaxCount = 9;

        /// <summary>
        /// Max number of buttons to be displayed in the pagination section
        /// </summary>
        public int ButtonMaxCount { get; set; }

        /// <summary>
        /// Should the pagination include page hint?
        /// </summary>
        public bool ShouldIncludePageHint { get; set; }

        public PaginationSettings()
        {
            ButtonMaxCount = DefaultButtonMaxCount;
            ShouldIncludePageHint = true;
        }        
    }
}