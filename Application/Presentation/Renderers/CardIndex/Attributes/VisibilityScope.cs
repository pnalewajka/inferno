﻿using System;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    [Flags]
    public enum VisibilityScope
    {
        None = 0,

        Form = 1 << 0,
        Grid = 1 << 1,
        FormAndGrid = Form | Grid,
    }
}