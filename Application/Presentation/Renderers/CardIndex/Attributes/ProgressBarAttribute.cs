﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    public class ProgressBarAttribute : CellContentAttribute
    {
        public string BarCssClass { get; set; }

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            return new ProgressBar
            {
                BarCssClass = BarCssClass,
                CssClass = CssClass,
                Value = (decimal)cellContentConverterContext.PropertyValue
            };
        }

        protected override string TemplateCode
        {
            get { return typeof(ProgressBar).Name; }
        }
    }
}