﻿using System;
using System.Linq;
using System.Reflection;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public class HubAttribute
        : CellContentAttribute
    {
        public string PickerPropertyName { get; protected set; }

        public HubAttribute(string pickerPropertyName)
        {
            PickerPropertyName = pickerPropertyName;
        }

        protected override string TemplateCode => "HubCell";

        public PropertyInfo ResolveReflectedPickerProperty(Type viewModelType)
        {
            return viewModelType.GetProperty(PickerPropertyName);
        }

        public LookupBaseAttribute GetLookupAttribute(PropertyInfo property)
        {
            return property
                .GetCustomAttributes(typeof(LookupBaseAttribute), true)
                .Single() as LookupBaseAttribute;
        }

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            var viewModelType = cellContentConverterContext.Model.GetType();
            var pickerProperty = ResolveReflectedPickerProperty(viewModelType);
            var lookupBaseAttribute = GetLookupAttribute(pickerProperty);
            var recordIds = pickerProperty.GetValue(cellContentConverterContext.Model) ?? new long[0];

            return new HubCellContent(
                lookupBaseAttribute,
                cellContentConverterContext.PropertyValue,
                recordIds);
        }
    }
}
