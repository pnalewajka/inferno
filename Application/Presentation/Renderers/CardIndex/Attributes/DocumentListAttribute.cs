﻿using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    public class DocumentListAttribute : CellContentAttribute
    {
        protected override string TemplateCode => nameof(DocumentList);

        public bool Truncate { get; set; } = true;

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            var documents = cellContentConverterContext.PropertyValue as IEnumerable<DocumentViewModel>;
            var documentUploadAttribute = cellContentConverterContext.PropertyInfo.GetCustomAttribute<DocumentUploadAttribute>();
            var documentMappingIdentifier = IdentifierHelper.GetTypeIdentifier(documentUploadAttribute.DocumentMappingType);

            return new DocumentList(documents, documentMappingIdentifier, Truncate);
        }
    }
}