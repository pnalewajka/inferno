﻿using System.Web;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    public class EllipsisAttribute : CellContentAttribute
    {
        public int MaxStringLength { get; set; }

        public bool IsHtml { get; set; }

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            var decodedDisplayValue = HttpUtility.HtmlDecode(cellContentConverterContext.DisplayValue);

            return new TooltipText
            {
                CssClass = CssClass,
                Tooltip = HtmlSanitizeHelper.Sanitize(decodedDisplayValue, HtmlSanitizeMethod.PlainText),
                Label = IsHtml
                    ? HtmlHelper.TruncateAtWords(decodedDisplayValue, MaxStringLength)
                    : StringHelper.TruncateAtWords(decodedDisplayValue, MaxStringLength)
            };
        }

        protected override string TemplateCode
        {
            get { return typeof(TooltipText).Name; }
        }
    }
}