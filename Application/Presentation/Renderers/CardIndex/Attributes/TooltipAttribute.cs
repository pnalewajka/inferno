﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    public class TooltipAttribute : CellContentAttribute
    {
        public string TooltipValuePropertyName { get; private set; }

        public TooltipAttribute(string tooltipValuePropertyName)
        {
            TooltipValuePropertyName = tooltipValuePropertyName;
        }

        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            return new TooltipText
            {
                CssClass = CssClass,
                Tooltip = HtmlSanitizeHelper.Sanitize(
                    PropertyHelper.GetPropertyValue<string>(cellContentConverterContext.Model, TooltipValuePropertyName),
                    HtmlSanitizeMethod.PlainText),
                Label = cellContentConverterContext.DisplayValue,
            };
        }

        protected override string TemplateCode => typeof(TooltipText).Name;
    }
}
