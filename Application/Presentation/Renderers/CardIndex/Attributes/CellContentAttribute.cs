﻿using System;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class CellContentAttribute : Attribute
    {
        public string CssClass { get; set; }

        public virtual string ViewPath
        {
            get { return $"~/Views/Grid/Cells/{TemplateCode}.cshtml"; }
        }

        public abstract ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext);

        protected abstract string TemplateCode { get; }
    }
}