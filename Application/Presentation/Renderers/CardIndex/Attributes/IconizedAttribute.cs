﻿using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    public class IconizedAttribute : CellContentAttribute
    {
        public override ICellContent ConvertToCellContent(CellContentConverterContext cellContentConverterContext)
        {
            return new IconizedContent
            {
                CssClass = GetCssClass(cellContentConverterContext.PropertyValue),
                Value = cellContentConverterContext.DisplayValue,
                IconCssClass = GetIconCssClass(cellContentConverterContext.PropertyValue)
            };
        }

        protected override string TemplateCode
        {
            get { return typeof(IconizedContent).Name; }
        }

        protected virtual string GetIconCssClass(object value)
        {
            return null;
        }

        protected virtual string GetCssClass(object value)
        {
            if (value == null)
            {
                return null;
            }

            var valueType = value.GetType();

            if (valueType.IsEnum)
            {
                return GetEnumCssClass(valueType.Name, value.ToString());
            }
            
            if (valueType == typeof(bool))
            {
                return GetBoolCssClass(valueType.Name, value.ToString());
            }

            return null;
        }

        protected virtual string GetDisplayValue(string displayValue)
        {
            return displayValue;
        }

        private string GetEnumCssClass(string nameType, string value)
        {
            var enumName = GetConvertedValue(nameType);
            var fieldName = GetConvertedValue(value);

            return $"{enumName} {fieldName}";
        }

        private string GetBoolCssClass(string nameType, string value)
        {
            var enumName = GetConvertedValue(nameType);
            var fieldName = GetConvertedValue(value);

            return $"{enumName} {fieldName}";
        }

        private string GetConvertedValue(string value)
        {
            return NamingConventionHelper.ConvertPascalCaseToHyphenated(value);
        }
    }
}