using System;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class VisibilityAttribute : Attribute
    {
        private readonly VisibilityScope _visibilityScope;

        public VisibilityScope VisibilityScope => _visibilityScope;

        public VisibilityAttribute(VisibilityScope visibilityScope)
        {
            _visibilityScope = visibilityScope;
        }
    }
}