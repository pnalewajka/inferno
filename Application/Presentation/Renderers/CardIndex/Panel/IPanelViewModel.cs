﻿using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Panel
{
    public interface IPanelViewModel
    {
        string CssClass { get; }

        string TitleHtml { get; }
        string FooterHtml { get; }

        IEnumerable<PanelViewModelProperty> Properties { get; }
    }
}
