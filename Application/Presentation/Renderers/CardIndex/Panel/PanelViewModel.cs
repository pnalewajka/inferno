﻿using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Panel
{
    public class PanelViewModel : IPanelViewModel
    {
        public string CssClass { get; set; }

        public string TitleHtml { get; set; }
        public string FooterHtml { get; set; }

        public IList<PanelViewModelProperty> Properties { get; set; }
        IEnumerable<PanelViewModelProperty> IPanelViewModel.Properties => Properties;

        private long _groupNumber = 0;

        public PanelViewModel()
        {
            CssClass = "panel-default";
            Properties = new List<PanelViewModelProperty>();
        }

        public PanelViewModel(string panelTitleText)
            : this()
        {
            TitleHtml = $"<h3 class=\"panel-title\">{panelTitleText}</h3>";
        }

        public void StartNewGroup()
        {
            _groupNumber++;
        }

        public void AddProperty(string labelHtml, string valueHtml)
        {
            Properties.Add(new PanelViewModelProperty
            {
                LabelHtml = labelHtml,
                ValueHtml = valueHtml,
                GroupName = GetGroupName()
            });
        }

        public void AddProperty(string labelHtml, string valueHtml, string cssClass)
        {
            Properties.Add(new PanelViewModelProperty
            {
                LabelHtml = labelHtml,
                ValueHtml = valueHtml,
                CssClass = cssClass,
                GroupName = GetGroupName()
            });
        }

        private string GetGroupName()
        {
            return $"group-{_groupNumber}";
        }
    }
}
