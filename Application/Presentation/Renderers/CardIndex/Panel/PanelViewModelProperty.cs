﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex.Panel
{
    public class PanelViewModelProperty
    {
        public string CssClass { get; set; }

        public string LabelHtml { get; set; }
        public string ValueHtml { get; set; }

        public string GroupName { get; set; }
    }
}
