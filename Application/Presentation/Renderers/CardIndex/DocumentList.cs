﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class DocumentList : ICellContent
    {
        public string CssClass => string.Empty;

        public bool Truncate { get; private set; }

        public IEnumerable<DocumentViewModel> Documents { get; private set; }

        public string DocumentMappingIdentifier { get; private set; }

        public DocumentList(IEnumerable<DocumentViewModel> documents, string documentMappingIdentifier, bool truncate)
        {
            Documents = documents;
            DocumentMappingIdentifier = documentMappingIdentifier;
            Truncate = truncate;
        }

        public override string ToString()
        {
            return string.Join(",", Documents.Select(d => d.DocumentName).ToArray());
        }
    }
}