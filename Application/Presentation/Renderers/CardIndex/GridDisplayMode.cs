﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public enum GridDisplayMode
    {
        Rows,
        Tiles,
        Custom
    }
}