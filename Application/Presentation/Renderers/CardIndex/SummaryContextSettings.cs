using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class SummaryContextSettings : IContextSettings
    {
        private readonly byte _layoutColumns;
        private readonly byte _labelWidth;

        public SummaryContextSettings(byte layoutColumns = 3, byte labelWidth = 2)
        {
            _layoutColumns = layoutColumns;
            _labelWidth = labelWidth;
        }

        public string ViewName => "_Context";

        public Layout GetLayout()
        {
            return new SummaryLayout(_layoutColumns, _labelWidth);
        }
    }
}