﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class GridViewModelParameters
    {
        public IEnumerable<object> Rows { get; set; }

        public Type RowType { get; set; }

        public QueryCriteria QueryCriteria { get; set; }

        public int? CurrentPageIndex { get; set; }

        public int? LastPageIndex { get; set; }

        public long RecordCount { get; set; }

        public CardIndexSettings Settings { get; set; }

        public GridRenderingControlContext RenderingControlContext { get; set; }

        public IReadOnlyCollection<AlertDto> Alerts { get; set; }

        public HashSet<string> UserRoles { get; set; }

        public GridViewFilters GridViewFilters { get; set; }

        public IGridViewConfiguration GridViewConfiguration { get; set; }

        public IList<SearchAreaViewModel> SearchAreas { get; set; }
    }
}
