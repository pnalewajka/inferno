﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    public class FilterGroup
    {
        public CommandButtonVisibility Visibility { get; set; }

        [LocalizationRequired]
        public string DisplayName { get; set; }

        [LocalizationRequired]
        public string ButtonText { get; set; }

        public FilterGroupType Type { get; set; }
        
        public IList<Filter> Filters { get; set; }

        public FilterGroup()
        {
            Visibility = CommandButtonVisibility.ValuePickerAndGrid;
            ButtonText = RenderersResources.DefaultFilterGroupButtonText;
        }

    }
}