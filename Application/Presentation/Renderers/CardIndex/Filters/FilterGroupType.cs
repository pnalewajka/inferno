namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    public enum FilterGroupType
    {
        RadioGroup,
        AndCheckboxGroup,
        OrCheckboxGroup
    }
}