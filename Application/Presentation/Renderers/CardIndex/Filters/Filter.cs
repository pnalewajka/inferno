﻿using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    public class Filter
    {
        private string _code;

        [LocalizationRequired]
        public string DisplayName { get; set; }

        /// <summary>
        /// Filter code. Must be the same as the named filter codes defined in CardIndexDataService
        /// </summary>
        public string Code
        {
            get { return _code; }
            set { _code = NamingConventionHelper.ConvertPascalCaseToHyphenated(value); }
        }

        /// <summary>
        /// Security role that must be matched to present filter to user
        /// Null if role check isn't required (default)
        /// </summary>
        internal SecurityRoleType? RequiredRole { get; set; }

        /// <summary>
        /// Css class to be applied to the filter
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Indicates if filter has parameters
        /// </summary>
        public bool IsParametric { get; protected set; }

        /// <summary>
        /// Indicates filter subgroup name
        /// </summary>
        [LocalizationRequired]
        public string SubGroupName { get; set; }

        /// <summary>
        /// Flag describes whether the filter is marked as not switchable
        /// </summary>
        public bool CanBeSwitchedOff { get; set; } = true;
    }
}