﻿using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    public class FilterGroupViewModel
    {
        public string DisplayName { get; set; }

        public string ButtonText { get; set; }

        public FilterGroupType Type { get; set; }

        public List<FilterViewModel> Filters { get; set; }

        public bool IsAnyFilterSelected
        {
            get { return Filters.Any(f => f.IsSelected); }
        }

        public FilterGroupViewModel(FilterGroup filterGroup, HashSet<string> userRoles)
        {
            DisplayName = filterGroup.DisplayName;
            ButtonText = filterGroup.ButtonText;
            Type = filterGroup.Type;
            Filters =
                filterGroup.Filters.Where(
                    f =>
                        !f.RequiredRole.HasValue 
                        || (f.RequiredRole.HasValue && userRoles.Contains(f.RequiredRole.Value.ToString())))
                        .Select(f => f.IsParametric ? new ParametricFilterViewModel(f) : new FilterViewModel(f))
                    .ToList();
        }
    }
}