﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    public class FilterViewModel : IFilteringObject
    {
        private readonly Filter _filter;

        public bool IsSelected { get; set; }

        public string DisplayName
        {
            get { return _filter.DisplayName; }
            set { _filter.DisplayName = value; }
        }

        public string Code
        {
            get { return _filter.Code; }
            set { _filter.Code = value; }
        }

        public string CssClass
        {
            get { return _filter.CssClass; }
            set { _filter.CssClass = value; }
        }

        public bool IsParametric => _filter.IsParametric;

        public string SubGroupName => _filter.SubGroupName;

        public bool CanBeSwitchedOff => _filter.CanBeSwitchedOff;

        public virtual string GetName()
        {
            return DisplayName;
        }

        public virtual void InitFilterParameters([NotNull]IDictionary<string, object> urlFieldValues)
        {
        }

        public virtual FilterParameters GetFilterParameters()
        {
            return null;
        }

        public virtual object GetFilterDto()
        {
            return null;
        }

        public FilterViewModel(Filter filter)
        {
            _filter = filter;
        }

        public string GetCssClass(FilterGroupType groupType)
        {
            var alwaysOnInsert = _filter.CanBeSwitchedOff ? null : " always-on";

            if (IsSelected)
            {
                if (groupType == FilterGroupType.RadioGroup)
                {
                    return $"radio{alwaysOnInsert}";
                }

                return $"check{alwaysOnInsert}";
            }

            return alwaysOnInsert;
        }
    }
}