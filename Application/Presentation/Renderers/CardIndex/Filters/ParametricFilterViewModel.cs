﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    public class ParametricFilterViewModel : FilterViewModel
    {
        private readonly ParametricFilter _parametricFilter;
        private FilterParameters _filterParameters = null;

        public string Identifier => _parametricFilter.ViewModelIdentifier;

        public string DialogTitle => _parametricFilter.DialogTitle;

        public bool CanParametersBeEmpty => _parametricFilter.CanParametersBeEmpty;

        public ParametricFilterViewModel(Filter filter) : base(filter)
        {
            _parametricFilter = filter as ParametricFilter;
        }

        public override FilterParameters GetFilterParameters()
        {
            if (_filterParameters == null)
            {
                _filterParameters = _parametricFilter.GetParameters();
            }

            return _filterParameters;
        }

        public override object GetFilterDto()
        {
            return _parametricFilter.GetDto();
        }

        public override void InitFilterParameters(IDictionary<string, object> urlFieldValues)
        {
            _parametricFilter.InitializeParameters(urlFieldValues);
        }

        public override string GetName()
        {
            return IsSelected ? _parametricFilter.GetName() : base.GetName();
        }
    }
}