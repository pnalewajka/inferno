﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ViewModels;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Filters
{
    /// <summary>
    /// Parametric filter 
    /// </summary>
    public abstract class ParametricFilter : Filter
    {
        protected ParametricFilter(string filterCode, string displayName)
        {
            DisplayName = displayName;
            Code = filterCode;
            IsParametric = true;
        }

        /// <summary>
        /// Get filter name
        /// </summary>
        /// <returns></returns>
        public abstract string GetName();

        /// <summary>
        /// Initialize parameters from url fields
        /// </summary>
        /// <param name="urlFieldValues"></param>
        public abstract void InitializeParameters([NotNull] IDictionary<string, object> urlFieldValues);

        /// <summary>
        /// Get viewmodel properties values as dictionary (property name => property value)
        /// </summary>
        /// <returns></returns>
        public abstract FilterParameters GetParameters();

        /// <summary>
        /// Identifier attribute value
        /// </summary>
        public string ViewModelIdentifier { get; protected set; }
        
        /// <summary>
        /// Title for filter details dialog. Will be extracted from DescriptioLocalizedAttribute, or viewmodel class name will be used if no attribute was found
        /// </summary>
        public string DialogTitle { get; protected set; }

        /// <summary>
        /// Indicated whether parameters can be empty
        /// </summary>
        public bool CanParametersBeEmpty { get; protected set; }

        public abstract object GetDto();
    }

    /// <summary>
    /// Parameteric filter, use this class when simple lambda in card index filtering expression is desired
    /// </summary>
    /// <typeparam name="TFilterViewModel"></typeparam>
    public class ParametricFilter<TFilterViewModel> : ParametricFilter
        where TFilterViewModel : class, new()
    {
        public TFilterViewModel FilterViewModel { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterCode">Filter code as registered in FilterCodes</param>
        /// <param name="displayName">Resource with name displayed when filter isn't selected</param>
        public ParametricFilter(string filterCode, string displayName)
            : base(filterCode, displayName)
        {
            FilterViewModel = ReflectionHelper.TryResolveInterface<IViewModelFactory<TFilterViewModel>>()?.Create() ?? new TFilterViewModel();
            
            ViewModelIdentifier = AttributeHelper.GetClassAttribute<IdentifierAttribute>(typeof (TFilterViewModel)).Value;
            DialogTitle = typeof (TFilterViewModel).Name;

            var dialogTitleAttribute = AttributeHelper.GetClassAttribute<DescriptionLocalizedAttribute>(typeof (TFilterViewModel));

            if (dialogTitleAttribute != null)
            {
                DialogTitle = dialogTitleAttribute.Description;
            }
        }

        public override string GetName()
        {
            return FilterViewModel.ToString();
        }

        public override void InitializeParameters(IDictionary<string, object> urlFieldValues)
        {
            if (urlFieldValues == null)
            {
                throw new ArgumentNullException(nameof(urlFieldValues));
            }

            FilterViewModel = UrlFieldsHelper.CreateObjectFromQueryParameters<TFilterViewModel>(urlFieldValues, true, Code);
        }

        public override FilterParameters GetParameters()
        {
            var filterParameters = new FilterParameters();
            var properties = TypeHelper.GetPublicInstanceProperties(FilterViewModel.GetType());
            var mapping = UrlFieldsHelper.GetObjectPropertyToUrlFieldMapping<TFilterViewModel>(Code);

            foreach (var property in properties)
            {
                filterParameters.Add(
                    mapping[property.Name],
                    new FilterParameter(GetRequiredType(property), property.GetValue(FilterViewModel),
                        property.Name, mapping[property.Name])
                    );
            }

            return filterParameters;
        }

        private Type GetRequiredType(PropertyInfo propertyInfo)
        {
            var type = propertyInfo.PropertyType;

            if (AttributeHelper.HasPropertyAttribute<RequiredAttribute>(propertyInfo))
            {
                return Nullable.GetUnderlyingType(type) ?? type;
            }
            else
            {
                return type;
            }
        }

        public override object GetDto()
        {
            throw new NotSupportedException();
        }
    }

    /// <summary>
    /// Parameteric filter, use this class when lambda with dto in card index filtering expression is desired
    /// Provisioning of viewmodel to dto mapping is required, and it is resolved via convention
    /// </summary>
    /// <typeparam name="TFilterViewModel"></typeparam>
    /// <typeparam name="TFilterDto"></typeparam>
    public class ParametricFilter<TFilterViewModel, TFilterDto> : ParametricFilter<TFilterViewModel> 
        where TFilterViewModel : class, new() 
        where TFilterDto : class
    {
        public ParametricFilter(string filterCode, string displayName)
            : base(filterCode, displayName)
        {

        }

        public override object GetDto()
        {
            var mapping = ReflectionHelper.ResolveInterface<IClassMapping<TFilterViewModel, TFilterDto>>();

            return mapping.CreateFromSource(FilterViewModel);
        }
    }
}