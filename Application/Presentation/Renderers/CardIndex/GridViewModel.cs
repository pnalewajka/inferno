﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Consts;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.CardIndex.Filters;
using Smt.Atomic.Presentation.Renderers.CardIndex.Panel;
using Smt.Atomic.Presentation.Renderers.CardIndex.Search;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Smt.Atomic.Presentation.Renderers.TreeView;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class GridViewModel : GridTableViewModel, IContextModel
    {
        private IContextSettings _contextSettings;

        /// <summary>
        /// If more then one grid is used on the same page this should be set to some value to distinguish between url
        /// parameter sets
        /// </summary>
        public string BindingPrefix { get; set; }

        /// <summary>
        /// Alerts that should be displayed on top of list page
        /// </summary>
        public IReadOnlyCollection<AlertDto> Alerts { get; set; }

        /// <summary>
        /// True if more than on row can be selected
        /// </summary>
        public bool AllowMultipleRowSelection { get; set; }

        /// <summary>
        /// Index of the current page
        /// </summary>
        public int? CurrentPageIndex { get; set; }

        /// <summary>
        /// Index of the last page
        /// </summary>
        public int? LastPageIndex { get; set; }

        /// <summary>
        /// Total record count (without pagination)
        /// </summary>
        public long RecordCount { get; private set; }

        /// <summary>
        /// Pagination settings
        /// </summary>
        public PaginationViewModel Pagination { get; private set; }

        /// <summary>
        /// Javascript expression or handler function name
        /// </summary>
        public string OnSelectionChangedJavaScript { get; set; }

        /// <summary>
        /// Javascript expression or handler function name
        /// </summary>
        public string OnRenderedJavaScript { get; set; }

        /// <summary>
        /// Javascript expression or handler function name
        /// </summary>
        public string OnRowDoubleClickedJavaScript { get; set; }

        /// <summary>
        /// Toolbar buttons which will be rendered in the grid 
        /// </summary>
        public CommandButtonsViewModel ToolbarButtons { get; set; }

        /// <summary>
        /// Context menu options that works similar to toolbar buttons
        /// </summary>
        public CommandButtonsViewModel ContextMenu { get; set; }

        /// <summary>
        /// Text to be used as a search-box placeholder
        /// </summary>
        public string SearchBoxPrompt { get; set; }

        /// <summary>
        /// Context used for subcard index
        /// </summary>
        public string ContextParameters { get; set; }

        /// <summary>
        /// Context view model which will be displayed above/next to grid
        /// </summary>
        public object ContextViewModel { get; set; }

        /// <summary>
        /// Details on how to render the context section
        /// </summary>
        public IContextSettings ContextSettings
        {
            get
            {
                return _contextSettings
                    ?? (_contextSettings = GetContextSettings());
            }

            set => _contextSettings = value;
        }

        /// <summary>
        /// Indicates if search box should be hidden, e.g. due to no search columns 
        /// </summary>
        public bool ShouldHideSearchBox { get; set; }

        /// <summary>
        /// Indicates if filters dropdown should be hidden
        /// </summary>
        public bool ShouldHideFilters { get; set; }

        /// <summary>
        /// Indicates if grid view configuration should be hidden
        /// </summary>
        public bool ShouldHideGridViewConfigurations { get; set; }

        /// <summary>
        /// Indicates if toolbar's button section should be rendered
        /// </summary>
        public bool ShouldRenderToolbarButtons()
        {
            return !ToolbarButtons.Groups.IsNullOrEmpty() || ShouldRenderFilters();
        }

        /// <summary>
        /// Indicates if filters dropdown should be rendered
        /// </summary>
        public bool ShouldRenderFilters()
        {
            return !(ShouldHideFilters || Filters.IsNullOrEmpty());
        }

        /// <summary>
        /// Indicates if view configuration dropdown should be rendered
        /// </summary>
        public bool ShouldRenderViewConfigurationDropdown()
        {
            return !ShouldHideGridViewConfigurations && (GridViewConfigurations != null) && (GridViewConfigurations.Count(c => !c.IsHidden) > 1);
        }

        /// <summary>
        /// Indicates if context menu should be rendered
        /// </summary>
        public bool ShouldRenderContextMenu()
        {
            return !ContextMenu.Groups.IsNullOrEmpty();
        }

        /// <summary>
        /// Indicates if summary context should be collapsed
        /// </summary>
        public bool IsContextViewCollapsed { get; set; }

        /// <summary>
        /// Indicates if toolbar is to be rendered separately
        /// </summary>
        public bool IsToolbarRenderedSeparately { get; set; }

        /// <summary>
        /// Indicates if pagination is to be rendered separately
        /// </summary>
        public bool IsPaginationRenderedSeparately { get; set; }

        /// <summary>
        /// Indicates if the grid is rendered as separate parts
        /// </summary>
        public bool IsRenderedInParts()
        {
            return IsToolbarRenderedSeparately || IsPaginationRenderedSeparately;
        }

        /// <summary>
        /// Url which can be used to export grid data to excel
        /// </summary>
        public string ExportToExcelUrl { get; set; }

        /// <summary>
        /// Url which can be used to copy grid data to clipboard
        /// </summary>
        public string CopyToClipboardUrl { get; set; }

        /// <summary>
        /// Url which can be used to export grid data to csv
        /// </summary>
        public string ExportToCsvUrl { get; set; }

        /// <summary>
        /// Should the grid react to enter button
        /// </summary>
        public bool ShouldEnterInvokeDefaultButton { get; set; }

        /// <summary>
        /// Should the double-click toggle selection
        /// </summary>
        public bool ShouldDoubleClickToggleSelection { get; set; }

        /// <summary>
        /// Grid parameters used to populate current data page
        /// </summary>
        public GridParameters GridParameters { get; set; }

        /// <summary>
        /// List of grid filters
        /// </summary>
        public IReadOnlyCollection<FilterGroupViewModel> Filters { get; private set; }

        public IReadOnlyCollection<IGridViewConfiguration> GridViewConfigurations { get; private set; }

        /// <summary>
        /// Grid rendering context from control  
        /// </summary>
        public GridRenderingControlContext RenderingControlContext { get; set; }

        /// <summary>
        /// When grid is rendered in parts, contains identifier used for component referencing
        /// </summary>
        public string ComponentId { get; private set; }

        /// <summary>
        /// Preferred way to display grid items
        /// </summary>
        public GridDisplayMode DisplayMode { get; private set; }

        /// <summary>
        /// Is a tree renderer in collapsible mode
        /// </summary>
        public bool IsCollapsibleTreeMode { get; set; }

        /// <summary>
        /// What view should be used to render a custom grid display mode
        /// </summary>
        public string CustomGridDisplayModeViewName { get; set; }

        /// Javascript function to use for tile layout adjustments
        /// </summary>
        public string InitializeJavaScriptAction { get; private set; }

        /// <summary>
        /// Name of the view which should be used to render an empty list
        /// </summary>
        public string EmptyListViewName { get; set; }

        /// <summary>
        /// Model which should be used to render an empty list
        /// </summary>
        public object EmptyListViewModel { get; set; }

        /// <summary>
        /// Areas to search using search box
        /// </summary>
        public IList<SearchAreaViewModel> SearchAreas { get; set; }

        /// <summary>
        /// Returns pagination buttons based on the current and last page index
        /// </summary>
        public IReadOnlyCollection<PaginationButton> GetPaginationButtons()
        {
            return Pagination.GetButtons(CurrentPageIndex, LastPageIndex, RecordCount);
        }

        public Func<IList<object>, IEnumerable<IGrouping<string, object>>> RowGrouping { get; set; }

        public bool HasRowGrouping
        {
            get { return RowGrouping != null; }
        }

        public GridViewModel(GridViewModelParameters parameters)
            : base(parameters.Rows, parameters.RowType, parameters.QueryCriteria, parameters.UserRoles, parameters.GridViewConfiguration)
        {
            BindingPrefix = parameters.Settings.BindingPrefix;
            RenderingControlContext = parameters.RenderingControlContext;
            Alerts = parameters.Alerts;
            AllowMultipleRowSelection = parameters.Settings.AllowMultipleRowSelection;
            OnSelectionChangedJavaScript = parameters.Settings.OnSelectionChangedJavaScript;
            OnRowDoubleClickedJavaScript = parameters.Settings.OnRowDoubleClickedJavaScript;
            OnRenderedJavaScript = parameters.Settings.OnRenderedJavaScript;
            CurrentPageIndex = parameters.CurrentPageIndex;
            LastPageIndex = parameters.LastPageIndex;
            RecordCount = parameters.RecordCount;
            Pagination = new PaginationViewModel(parameters.Settings.Pagination);
            AddImportButtons(parameters.Settings);
            ToolbarButtons = new CommandButtonsViewModel(GetAvailableButtons(parameters.Settings.ToolbarButtons, b => ShouldElementBeVisible(b.Visibility)));
            ContextMenu = new CommandButtonsViewModel(GetAvailableButtons(parameters.Settings.ContextMenu, b => ShouldElementBeVisible(b.Visibility)));
            RowButtons.AddRange(GetAvailableButtons(parameters.Settings.RowButtons, b => ShouldElementBeVisible(b.Visibility)));
            RowMenuMode = parameters.Settings.RowMenuMode;
            IsCondensed = parameters.Settings.Condensed;

            DisplayMode = parameters.Settings.DisplayMode;
            IsCollapsibleTreeMode = parameters.Settings.TreeMode == TreeDisplayMode.Collapsible
                                    && string.IsNullOrEmpty(parameters.QueryCriteria.SearchPhrase);
            CustomGridDisplayModeViewName = parameters.Settings.CustomGridDisplayModeViewName;
            InitializeJavaScriptAction = parameters.Settings.InitializeJavaScriptAction;

            SearchBoxPrompt = CardIndexResources.SearchBoxDefaultPrompt;
            ContextParameters = CreateContextParameters(parameters.QueryCriteria.Context);
            ContextSettings = parameters.Settings.ContextSettings;
            ShouldHideSearchBox = parameters.Settings.ShouldHideSearchBox;
            ShouldHideFilters = parameters.Settings.ShouldHideFilters;
            ShouldHideGridViewConfigurations = parameters.Settings.ShouldHideViewConfigurations;
            ShouldEnterInvokeDefaultButton = parameters.Settings.ShouldEnterInvokeDefaultButton;
            ShouldDoubleClickToggleSelection = parameters.Settings.ShouldDoubleClickToggleSelection;
            IsContextViewCollapsed = parameters.Settings.IsContextViewCollapsed;

            if (parameters.Settings.GridViewConfigurations != null)
            {
                GridViewConfigurations = new ReadOnlyCollection<IGridViewConfiguration>(parameters.Settings.GridViewConfigurations);
            }

            Filters = parameters.GridViewFilters.FilterGroupViewModels;
            GridParameters = new GridParameters(parameters.QueryCriteria);
            ApplyGridViewConfiguration(parameters.GridViewConfiguration);

            EmptyListMessage = parameters.Settings.EmptyListMessage ?? EmptyListMessage;
            EmptyListViewName = parameters.Settings.EmptyListViewName;
            EmptyListViewModel = parameters.Settings.EmptyListViewModel;

            ExportToCsvUrl = parameters.Settings.ExportToCsvUrl;
            ExportToExcelUrl = parameters.Settings.ExportToExcelUrl;
            CopyToClipboardUrl = parameters.Settings.CopyToClipboardUrl;

            ComponentId = Guid.NewGuid().ToString();

            SearchAreas = parameters.SearchAreas;
        }

        private void ApplyGridViewConfiguration(IGridViewConfiguration gridViewConfiguration)
        {
            if (gridViewConfiguration == null || gridViewConfiguration.ColumnConfiguration == null)
            {
                return;
            }

            foreach (var propertyConfiguration in gridViewConfiguration.ColumnConfiguration)
            {
                if (!propertyConfiguration.IsVisible)
                {
                    RemoveColumn(propertyConfiguration.MemberPath);
                }
            }

            if (gridViewConfiguration is ICustomViewConfiguration viewConfiguration)
            {
                CustomGridDisplayModeViewName = viewConfiguration.CustomViewName;
                DisplayMode = viewConfiguration.DisplayMode;
            }

            if (gridViewConfiguration is IInitializableViewConfiguration viewInitConfiguration)
            {
                InitializeJavaScriptAction = viewInitConfiguration.InitializeJavaScriptAction;
            }
        }

        private void AddImportButtons(CardIndexSettings settings)
        {
            var importSettingsList = settings.Imports.Where(s => s.IsEnabled && UserHasRequiredRole(s.RequiredRole)).ToList();

            if (importSettingsList.Any())
            {
                var onClickAction = new DropdownAction();

                foreach (var importSettings in importSettingsList)
                {
                    var downloadEmptyTemplateButton = new CommandButton
                    {
                        Text = CardIndexResources.DownloadEmptyImportTemplateButtonText,
                        OnClickAction = new UriAction(importSettings.DownloadEmptyTemplateUrl),
                        Group = importSettings.Name,
                    };

                    var downloadEditTemplateButton = new CommandButton
                    {
                        Text = CardIndexResources.DownloadEditImportTemplateButtonText,
                        OnClickAction = new UriAction(importSettings.DownloadEditTemplateUrl),
                        Group = importSettings.Name,
                    };

                    var performImportButton = new CommandButton
                    {
                        Text = CardIndexResources.ImportDataButtonText,
                        OnClickAction = new JavaScriptCallAction("Grid.onImportClicked('" + importSettings.ImportDialogUrl + "');"),
                        Group = importSettings.Name,
                    };

                    onClickAction.Items.Add(downloadEmptyTemplateButton);
                    onClickAction.Items.Add(downloadEditTemplateButton);
                    onClickAction.Items.Add(performImportButton);
                }

                var importButton = new ToolbarButton
                {
                    Icon = FontAwesome.Table,
                    Group = CardIndexSettings.StandardGridOperationGroup,
                    Text = CardIndexResources.ImportDropdownButtonText,
                    Availability = new ToolbarButtonAvailability
                    {
                        Mode = AvailabilityMode.Always,
                    },
                    OnClickAction = onClickAction
                };

                var index = settings.ToolbarButtons.FindIndex(b => b.Id == CardIndexSettings.ExportButtonId);
                settings.ToolbarButtons.Insert(index + 1, importButton);
            }
        }

        private bool UserHasRequiredRole(SecurityRoleType? requiredRole)
        {
            return (!requiredRole.HasValue || UserRoles.Contains(requiredRole.ToString()));
        }

        private IEnumerable<PropertyInfo> GetRestrictedProperties()
        {
            if (Rows.IsNullOrEmpty())
            {
                yield break;
            }

            var columnsWithRequireRoleAttribute = TypeHelper.GetEnumerationElementType(Rows.GetType()).GetProperties()
                .Where(p => p.GetAttributes<RoleRequiredAttribute>(true).Any());

            foreach (var property in columnsWithRequireRoleAttribute)
            {
                var neededRoles = property.GetAttributes<RoleRequiredAttribute>(true).Select(a => a.ForRead.ToString());

                if (!UserRoles.Intersect(neededRoles).Any())
                {
                    yield return property;
                }
            }
        }

        public MvcHtmlString GetRowsAsJson()
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
            };

            var jsonSerializer = JsonSerializer.Create(jsonSerializerSettings);
            var rows = new List<JObject>();

            foreach (var row in Rows)
            {
                var jObject = JObject.FromObject(row, jsonSerializer);

                foreach (var property in GetRestrictedProperties())
                {
                    var jObjectPropertyName = NamingConventionHelper.ConvertPascalCaseToCamelCase(property.Name);
                    jObject.Remove(jObjectPropertyName);
                }

                jObject.Add("toString", row.ToString());
                rows.Add(jObject);
            }

            return new MvcHtmlString(JsonHelper.Serialize(rows, jsonSerializerSettings));
        }

        public string GetSearchBoxPlainPrompt()
        {
            return HtmlMarkupHelper.RemoveAccelerator(SearchBoxPrompt);
        }

        public string GetSearchBoxAccelerator()
        {
            return HtmlMarkupHelper.GetAccelerator(SearchBoxPrompt);
        }

        private bool ShouldElementBeVisible(CommandButtonVisibility visibility)
        {
            var requiredFlag = RenderingControlContext.HasFlag(GridRenderingControlContext.ValuePicker)
                ? CommandButtonVisibility.ValuePicker
                : CommandButtonVisibility.Grid;

            return visibility.HasFlag(requiredFlag);
        }

        private string CreateContextParameters(object context)
        {
            if (context == null)
            {
                return null;
            }

            var result = new Dictionary<string, string>();

            foreach (var propertyInfo in TypeHelper.GetPublicInstanceProperties(context.GetType()))
            {
                var value = propertyInfo.GetValue(context);

                if (value != null)
                {
                    var valueAsString = value.ToString();
                    var parameterName = NamingConventionHelper.ConvertPascalCaseToHyphenated(propertyInfo.Name);

                    if (propertyInfo.PropertyType.IsEnum
                        || (Nullable.GetUnderlyingType(propertyInfo.PropertyType) != null && Nullable.GetUnderlyingType(propertyInfo.PropertyType).IsEnum))
                    {
                        valueAsString = NamingConventionHelper.ConvertPascalCaseToHyphenated(valueAsString);
                    }

                    result[parameterName] = valueAsString;
                }
            }

            return string.Join("&", result.Select(p => $"{p.Key}={p.Value}"));
        }

        public IEnumerable<IGrouping<string, object>> GetGroupedRows()
        {
            if (RowGrouping == null)
            {
                return Rows.GroupBy(r => (string)null);
            }

            return RowGrouping(Rows);
        }

        private IContextSettings GetContextSettings()
        {
            var contextType = ContextViewModel?.GetType();

            if (contextType != null && typeof(IPanelViewModel).IsAssignableFrom(contextType))
            {
                return new PanelContextSettings();
            }

            return new SummaryContextSettings();
        }
    }
}