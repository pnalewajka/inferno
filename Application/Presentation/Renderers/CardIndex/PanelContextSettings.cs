using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class PanelContextSettings : IContextSettings
    {
        public string ViewName => "_ContextPanel";

        public Layout GetLayout()
        {
            return null;
        }
    }
}