﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public abstract class GridColumnViewModel
    {
        public string SortByPropertyName { get; protected set; }

        public string Label { get; protected set; }

        public string CssClass { get; protected set; }

        public string HeaderCssClass { get; protected set; }

        public string MemberInfoName { get; protected set; }

        public SortingDirection SortingDirection { get; protected set; }

        public bool IsSortable { get; protected set; }

        public long Order { get; set; }

        public string ViewPath { get; set; }

        public abstract object Render(object row, UrlHelper urlHelper);

        public virtual object RenderForExport(object row, UrlHelper urlHelper)
        {
            return Render(row, urlHelper);
        }

        protected GridColumnViewModel(IGridColumnViewConfiguration column, SortingDirection sortingDirection)
        {
            Label = ViewModelMemberHelper.GetGridLabel(column.MemberInfo);
            Order = column.MemberChain.Count() == 1
                ? ViewModelMemberHelper.GetOrder(column.MemberInfo)
                : long.MaxValue;
            CssClass = ViewModelMemberHelper.GetGridCssClass(column.MemberInfo);
            HeaderCssClass = ViewModelMemberHelper.GetGridHeaderCssClass(column.MemberInfo);

            MemberInfoName = column.MemberInfo.Name;
            SortingDirection = sortingDirection;
        }
    }
}