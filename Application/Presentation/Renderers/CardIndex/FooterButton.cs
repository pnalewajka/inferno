﻿using Smt.Atomic.Presentation.Renderers.Toolbar;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class FooterButton : CommandButton, IFooterButton
    {
        public FooterButton(string name, string text)
        {
            Id = name;
            Text = text;
            ButtonType = CommandButtonType.Button;
            ButtonStyleType = CommandButtonStyle.Primary;
        }
    }
}
