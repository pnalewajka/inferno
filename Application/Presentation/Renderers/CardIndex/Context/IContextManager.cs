using System.Linq.Expressions;

namespace Smt.Atomic.Presentation.Renderers.CardIndex.Context
{
    public interface IContextManager
    {
        Expression GetFilteringExpression();
    }
}