using System;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Enums;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface ICardIndexRecordViewModel
    {
        object ViewModelRecord { get; }

        Action<FormRenderingModel> AddFormCustomization { get; }

        Action<FormRenderingModel> EditFormCustomization { get; }

        Action<FormRenderingModel> ViewFormCustomization { get; }

        string CancelButtonUrl { get; }

        string DeleteButtonUrl { get; }

        bool ShowDeleteButtonOnEdit { get; }

        FormModificationTracking ModificationTracking { get; set; }
    }
}