using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.Renderers.Toolbar;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class GridTableViewModel
    {
        /// <summary>
        /// Grid rows to be displayed (aka page)
        /// </summary>
        public IList<object> Rows { get; private set; }

        /// <summary>
        /// Base type for all grid items (aka rows)
        /// </summary>
        public Type RowType { get; private set; }

        /// <summary>
        /// List of columns defined for the grid to display
        /// </summary>
        public GridTableColumns Columns { get; protected set; }

        /// <summary>
        /// Criteria used to populate current data page
        /// </summary>
        public SortingCriteria OrderBy { get; private set; }

        public HashSet<string> UserRoles { get; set; }

        /// <summary>
        /// True if the condensed mode for the table should be used
        /// </summary>
        public bool IsCondensed { get; set; }

        /// <summary>
        /// Text to be displayed when no records are present
        /// </summary>
        public string EmptyListMessage { get; set; }

        /// <summary>
        /// List of per-row buttons to be (possibly) displayed in the action column
        /// </summary>
        public IList<ICommandButton> RowButtons { get; private set; }

        /// <summary>
        /// How should be the row menu options be presented?
        /// </summary>
        public RowMenuMode RowMenuMode { get; set; }

        public GridTableViewModel(IEnumerable rows, Type rowType, SortingCriteria sortingCriteria, HashSet<string> userRoles, IGridViewConfiguration gridViewConfiguration)
        {
            Rows = rows.OfType<object>().ToList();
            RowType = rowType;

            OrderBy = sortingCriteria;
            UserRoles = userRoles;
            Columns = new GridTableColumns(GetColumns(gridViewConfiguration));
            EmptyListMessage = CardIndexResources.EmptyListMessage;

            RowButtons = new List<ICommandButton>();
        }

        public GridTableViewModel(IEnumerable rows, Type rowType, HashSet<string> userRoles, IGridViewConfiguration gridViewConfiguration)
            : this(rows, rowType, new SortingCriteria(), userRoles, gridViewConfiguration)
        {
        }

        public CommandButtonsViewModel GetRowButtons([NotNull] object row)
        {
            if (row == null)
            {
                throw new ArgumentNullException(nameof(row));
            }

            return new CommandButtonsViewModel(GetAvailableButtons(RowButtons, b => IsRowButtonAvailable(b, row)));
        }

        protected IEnumerable<ICommandButton> GetAvailableButtons(IList<ICommandButton> buttons,
            Func<ICommandButton, bool> buttonPredicate)
        {
            var availableButtons = buttons
                .Select(b => b.GetCopy(buttonPredicate))
                .Where(b => b != null);

            return availableButtons;
        }

        protected void RemoveColumn(string columnName)
        {
            Columns.TryRemoveColumn(columnName);
        }

        private static bool IsRowButtonAvailable(ICommandButton button, object row)
        {
            var availability = button.Availability;

            if (availability == null || availability.GetType() == typeof(CommandButtonAvailability))
            {
                return true;
            }

            var buttonAvailability = availability as IRowButtonAvailability;

            if (buttonAvailability == null)
            {
                var message =
                    $"Row button cannot have availability of type {availability.GetType().Name}. Recommended types are: CommandButtonAvailability, or RowButtonAvailability`1, or any type that implements IRowButtonAvailability. Could be a null, too.";
                throw new InvalidCastException(message);
            }

            return buttonAvailability.IsAvailableFor(row);
        }

        private IList<GridColumnViewModel> GetColumns(IGridViewConfiguration gridViewConfiguration)
        {
            var rowType = RowType;
            var gridColumns = new List<GridColumnViewModel>();

            var relevantColumns = gridViewConfiguration.ColumnConfiguration
                .Where(c => c.IsVisible)
                .ToArray();

            var memberInfos = relevantColumns.Select(c => c.MemberInfo).ToArray();

            foreach (var column in relevantColumns)
            {
                var memberInfo = column.MemberInfo;

                var isReadOnlyControlProperty = ViewModelPropertyHelper.IsReadOnlyControlProperty(memberInfo,
                    memberInfos);

                var propertyAccessMode = SecurityHelper.GetPropertyAccessMode(memberInfo, UserRoles);

                if (propertyAccessMode != PropertyAccessMode.NoAccess && !isReadOnlyControlProperty)
                {
                    var orderByStatus = OrderBy.GetSortingDirectionForColumn(column.MemberPath);

                    if (column is IGridPropertyColumnViewConfiguration propertyColumn)
                    {
                        gridColumns.Add(new PropertyBasedGridColumnViewModel(propertyColumn, orderByStatus, Rows));
                    }
                    else if (column is IGridMethodColumnViewConfiguration methodColumn)
                    {
                        gridColumns.Add(new MethodBasedGridColumnViewModel(methodColumn, orderByStatus));
                    }
                    else
                    {
                        throw new Exception("Not supported selector type");
                    }
                }
            }

            return gridColumns.OrderBy(d => d.Order).ToList();
        }
    }
}