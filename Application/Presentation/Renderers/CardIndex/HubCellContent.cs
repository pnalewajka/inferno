using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class HubCellContent : ICellContent
    {
        public bool HasHub { get; set; }

        public string Identifier { get; set; }

        public long[] Ids { get; set; }

        public string[] DisplayNames { get; set; }

        public string CssClass => "";

        public HubCellContent(LookupBaseAttribute lookupAttribute, object displayNames, object ids)
        {
            HasHub = lookupAttribute.HasHub;
            Identifier = lookupAttribute.TypeIdentifier;

            DisplayNames = displayNames is string displayName
                ? new[] { displayName }
                : (((IEnumerable<string>)displayNames)?.ToArray() ?? new[] { string.Empty });

            Ids = ids is long id
                ? new[] { id }
                : ((IEnumerable<long>)ids).ToArray();
        }

        // Used by export mechanism
        public override string ToString()
        {
            return string.Join(", ", DisplayNames);
        }
    }
}
