﻿namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class IconizedContent : ICellContent
    {
        public string IconCssClass { get; set; }

        public string CssClass { get; set; }

        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}