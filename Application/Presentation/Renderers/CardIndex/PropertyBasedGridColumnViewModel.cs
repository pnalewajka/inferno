using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Extensions;
using Smt.Atomic.Presentation.Renderers.GridViews;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class PropertyBasedGridColumnViewModel : GridColumnViewModel
    {
        private readonly IGridPropertyColumnViewConfiguration _column;
        private LookupBaseAttribute _lookupAttribute;
        private IValuePicker _valuePicker;
        private IEnumerable<ListItem> _lookupListItems;
        private IEnumerable<ListItem> _dropdownListItems;
        private string _stringFormat;
        private bool _shouldEncodeHtml;
        private Func<CellContentConverterContext, ICellContent> _cellContentConverter;

        public PropertyBasedGridColumnViewModel(IGridPropertyColumnViewConfiguration column, SortingDirection sortingDirection, IEnumerable<object> rows)
            : base(column, sortingDirection)
        {
            _column = column;

            SortByPropertyName = NamingConventionHelper.ConvertPascalCaseToHyphenated(column.PropertyInfo.Name);

            SetFormatting();
            SetSortable();
            SetCellContentConversion();
            PrepareLookups(rows);
        }

        public override object Render(object row, UrlHelper urlHelper)
        {
            var propertyValue = _column.CompiledMemberSelector.Value.DynamicInvoke(row);
            var displayValue = RenderInternal(propertyValue, urlHelper, false);

            return _cellContentConverter != null
                ? _cellContentConverter(new CellContentConverterContext
                {
                    PropertyValue = propertyValue,
                    DisplayValue = displayValue,
                    Model = row,
                    PropertyInfo = _column.PropertyInfo
                })
                : (object)displayValue;
        }

        public override object RenderForExport(object row, UrlHelper urlHelper)
        {
            var propertyValue = _column.CompiledMemberSelector.Value.DynamicInvoke(row);
            var displayValue = RenderForExportInternal(propertyValue, urlHelper);

            return _cellContentConverter != null
                ? _cellContentConverter(new CellContentConverterContext
                {
                    PropertyValue = propertyValue,
                    DisplayValue = displayValue,
                    Model = row,
                    PropertyInfo = _column.PropertyInfo
                })
                : (object)displayValue;
        }

        private string RenderInternal(object propertyValue, UrlHelper urlHelper, bool isExportFallback)
        {
            if (propertyValue == null)
            {
                return string.Empty;
            }

            if (propertyValue is string valueAsString)
            {
                return GetFormattedValue(valueAsString, isExportFallback);
            }

            var enumerable = propertyValue as IEnumerable<long>;

            if (enumerable != null)
            {
                var ids = enumerable.ToArray();

                if (_lookupListItems != null)
                {
                    return GetListItemString(_lookupListItems, ids, isExportFallback);
                }

                if (_dropdownListItems != null)
                {
                    return GetListItemString(_dropdownListItems, ids, isExportFallback);
                }

                return string.Join(", ", ids.Select(e => e.ToInvariantString()));
            }

            if (_lookupListItems != null)
            {
                var id = Convert.ToInt64(propertyValue);
                var listItem = _lookupListItems.Single(i => i.Id == id);

                return ListItemToString(listItem, isExportFallback);
            }

            if (_dropdownListItems != null)
            {
                var id = Convert.ToInt64(propertyValue);

                return _dropdownListItems.Single(i => i.Id == id).DisplayName;
            }

            if (propertyValue is bool)
            {
                return (bool)propertyValue ? CardIndexResources.BoolValueRenderTrue : CardIndexResources.BoolValueRenderFalse;
            }

            var enumValue = propertyValue as Enum;

            if (enumValue != null)
            {
                return enumValue.GetDescriptionOrValue();
            }

            var stringEnumerable = propertyValue as IEnumerable<string>;

            if (stringEnumerable != null)
            {
                return string.Join(", ", stringEnumerable);
            }

            var documentEnumerable = propertyValue as IEnumerable<DocumentViewModel>;

            if (documentEnumerable != null)
            {
                return string.Join(", ", documentEnumerable.Select(d => d.DocumentName));
            }

            var localizedString = propertyValue as LocalizedStringBase;

            if (localizedString != null)
            {
                return localizedString.ToString();
            }

            if (TypeHelper.IsNumeric(propertyValue.GetType()))
            {
                var formattable = propertyValue as IFormattable;

                return formattable == null
                    ? propertyValue.ToString()
                    : formattable.ToInvariantString();
            }

            return GetFormattedValue(propertyValue, isExportFallback);
        }

        private string RenderForExportInternal(object propertyValue, UrlHelper urlHelper)
        {
            var htmlString = propertyValue as HtmlString;

            if (htmlString != null)
            {
                return htmlString.ToPlainText();
            }

            return RenderInternal(propertyValue, urlHelper, true);
        }

        private string GetFormattedValue(object value, bool isExportFallback)
        {
            var formattedValue = string.IsNullOrWhiteSpace(_stringFormat)
                ? value.ToString()
                : string.Format(_stringFormat, value);

            if (!isExportFallback && _shouldEncodeHtml)
            {
                return WebUtility.HtmlEncode(formattedValue);
            }

            return formattedValue;
        }

        private string GetListItemString(IEnumerable<ListItem> listItems, IEnumerable<long> ids, bool isExportFallback)
        {
            var items = listItems.Where(i => i.Id != null && ids.Contains(i.Id.Value)).ToList();

            return string.Join(", ", items.OrderBy(i => i.DisplayName).Select(e => ListItemToString(e, isExportFallback)));
        }

        private string ListItemToString(ListItem item, bool isExportFallback)
        {
            if (!isExportFallback && (_lookupAttribute?.HasHub ?? false))
            {
                return $"<span data-hub-identifier=\"{_lookupAttribute.TypeIdentifier}\" data-hub-recordid=\"{item.Id}\">{item.DisplayName}</span>";
            }

            return item.DisplayName;
        }

        private void SetFormatting()
        {
            var attribute = AttributeHelper.GetPropertyAttribute<StringFormatAttribute>(_column.PropertyInfo);

            if (attribute == null)
            {
                return;
            }

            _stringFormat = attribute.Format;
            _shouldEncodeHtml = attribute.ShouldEncodeHtml;
        }

        private void SetSortable()
        {
            var attribute = AttributeHelper.GetPropertyAttribute<NonSortableAttribute>(_column.PropertyInfo);
            IsSortable = _column.IsSortable && attribute == null;
        }

        private void SetCellContentConversion()
        {
            var attribute = AttributeHelper.GetPropertyAttribute<CellContentAttribute>(_column.PropertyInfo);

            if (attribute == null)
            {
                return;
            }

            ViewPath = attribute.ViewPath;
            _cellContentConverter = attribute.ConvertToCellContent;
        }

        private void PrepareLookups(IEnumerable<object> rows)
        {
            _lookupAttribute = _column.PropertyInfo.GetCustomAttribute<LookupBaseAttribute>();

            if (_lookupAttribute != null)
            {
                _valuePicker =
                    (IValuePicker)ReflectionHelper.CreateInstanceWithIocDependencies(_lookupAttribute.Type);

                var valuePickerIds = new HashSet<long>();

                foreach (var row in rows)
                {
                    var value = _column.PropertyInfo.GetValue(row);

                    var enumerable = value as IEnumerable<long>;

                    if (enumerable != null)
                    {
                        foreach (var id in enumerable)
                        {
                            valuePickerIds.Add(id);
                        }
                    }
                    else
                    {
                        var id = Convert.ToInt64(value);
                        valuePickerIds.Add(id);
                    }
                }

                _lookupListItems = _valuePicker.GetListItems(valuePickerIds.ToArray(), _lookupAttribute.GridFormatterType, false);
            }

            var dropdownAttribute = _column.PropertyInfo.GetCustomAttribute<DropdownAttribute>();

            if (dropdownAttribute != null)
            {
                _dropdownListItems = dropdownAttribute.GetItems();
            }
        }
    }
}
