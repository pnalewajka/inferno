﻿using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class TooltipText : ICellContent
    {
        public string IconCssClass { get; set; }

        public string CssClass { get; set; }

        public string Tooltip { get; set; }

        public string Label { get; set; }

        public string ExportValue { get; set; }

        public IDictionary<string, string> DataAttributes { get; set; }

        public override string ToString()
        {
            return ExportValue ?? Label;
        }
    }


}
