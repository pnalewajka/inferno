using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public class PaginationButton
    {
        private string _caption;

        public string Caption
        {
            get
            {
                if (_caption == null)
                {
                    return (TargetPage + 1).ToInvariantString();
                }

                return _caption;
            }
            set { _caption = value; }
        }

        public bool IsActive { get; set; }
        public bool IsDisabled { get; set; }
        public int TargetPage { get; set; }
        public bool IsFirst { get; set; }
        public bool IsLast { get; set; }
        public string Hint { get; set; }
        public string CssClass { get; set; }
        public string Hotkey { get; set; }

        public PaginationButton()
        {
        }

        public PaginationButton(int targetPage)
        {
            TargetPage = targetPage;
        }

        public string GetCssClass()
        {
            var classNames = new List<string>();

            if (IsFirst)
            {
                classNames.Add("previous");
            }

            if (IsActive)
            {
                classNames.Add("active");
            }

            if (IsDisabled)
            {
                classNames.Add("disabled");
            }

            if (IsLast)
            {
                classNames.Add("next");
            }

            if (!string.IsNullOrWhiteSpace(CssClass))
            {
                classNames.Add(CssClass);
            }

            return string.Join(" ", classNames);
        }
    }
}