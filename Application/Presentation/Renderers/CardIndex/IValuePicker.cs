using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface IValuePicker
    {
        void PrepareForValuePicker(bool allowMultipleRowSelection);

        ActionResult List(GridParameters parameters);

        ActionResult JsonList(string formatterId, GridParameters parameters);

        IList<ListItem> GetListItems(long[] ids, Type formatterType, bool filtered = true);

        IList<ListItem> GetListItemsBySearchPhrase(string searchPhrase, Type formatterType);
    }
}