using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.CardIndex
{
    public interface IContextSettings
    {
        string ViewName { get; }

        Layout GetLayout();
    }
}