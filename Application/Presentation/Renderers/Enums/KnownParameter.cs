﻿using System;
using System.ComponentModel;

namespace Smt.Atomic.Presentation.Renderers.Enums
{
    [Flags]
    public enum KnownParameter
    {
        None = 0,

        [Description("id=~this.id")]
        SelectedId = 1 << 0,

        [Description("ids=~this.ids")]
        SelectedIds = 1 << 1,

        [Description("context=~context")]
        Context = 1 << 2,

        [Description("parent-id=~this.id")]
        ParentId = 1 << 3
    }
}
