﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Enums
{
    [Flags]
    public enum FormModificationTracking
    {
        None = 0,

        WarnOnExit = 1 << 0,

        MarkEditedField = 1 << 1,
    }
}
