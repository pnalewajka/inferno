﻿using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.Renderers.ViewPages
{
    public abstract class AtomicWebViewPage<TModel> : WebViewPage<TModel>
    {
        private readonly IPrincipalProvider _principalProvider;

        public IAtomicPrincipal CurrentPrincipal
            => _principalProvider.IsSet ? _principalProvider.Current : AtomicPrincipal.Anonymous;

        protected AtomicWebViewPage()
        {
            _principalProvider = ReflectionHelper.ResolveInterface<IPrincipalProvider>();
        }
    }
}