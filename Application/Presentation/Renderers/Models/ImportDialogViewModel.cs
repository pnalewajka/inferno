﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.Renderers.Models.Dialogs;

namespace Smt.Atomic.Presentation.Renderers.Models
{
    public class ImportDialogViewModel : ModelBasedDialogViewModel<ImportViewModel>
    {
        public string SubmitUrl { get; set; }
        public List<AlertViewModel> Issues { get; set; }

        public ImportDialogViewModel(ImportViewModel model, StandardButtons standardButtons = StandardButtons.OkCancel) : base(model, standardButtons)
        {
            Issues = new List<AlertViewModel>();
        }
    }
}