﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class EnumResourcesViewModel
    {
        private readonly IDictionary<string, IDictionary<string, string>> _enumDescriptions = new Dictionary<string, IDictionary<string, string>>();

        public void Add<TEnum>(CultureInfo cultureInfo = null)
        {
            cultureInfo = cultureInfo ?? CultureInfo.CurrentCulture;

            var enumType = typeof(TEnum);
            var descriptionDictionary = new Dictionary<string, string>();
            
            foreach (var value in EnumHelper.GetEnumValues(enumType))
            {
                var numericKey = Convert.ChangeType(value, value.GetTypeCode()).ToString();
                var codeKey = value.ToString();
                var description = value.GetDescription(cultureInfo);

                if (description.IsNullOrEmpty())
                {
                    description = NamingConventionHelper.ConvertPascalCaseToLabel(codeKey); // fallback to enum key
                }

                descriptionDictionary.Add(numericKey, description);
                descriptionDictionary.Add(codeKey, description);
            }

            _enumDescriptions[enumType.Name] = descriptionDictionary;
        }

        public string ToJson()
        {
            var localizationStrings = _enumDescriptions
                .OrderBy(p => p.Key)
                .ToDictionary
                (
                    p => p.Key,
                    p => p.Value
                );

            return JsonHelper.Serialize(localizationStrings);
        }
    }
}