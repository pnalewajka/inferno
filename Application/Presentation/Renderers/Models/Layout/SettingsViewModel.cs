﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class SettingsViewModel
    {
        public string GoogleMapsJavaScriptApiKey { get; private set; }
        public bool GoogleMapsIsSearchEnabled { get; private set; }

        public SettingsViewModel(ISettingsProvider settingsProvider)
        {
            var googleMapsSettings = settingsProvider.GoogleMapsSettings;

            if (googleMapsSettings == null)
            {
                return;
            }

            GoogleMapsJavaScriptApiKey = googleMapsSettings.ApiKey;
            GoogleMapsIsSearchEnabled = googleMapsSettings.IsSearchEnabled;
        }

        public string ToJson()
        {
            return JsonHelper.Serialize(this, true);
        }
    }
}
