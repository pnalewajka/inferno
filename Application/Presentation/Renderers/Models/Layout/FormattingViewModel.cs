﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class FormattingViewModel
    {
        public string ToJson()
        {
            var data = new
            {
                dateFormat = CultureInfoHelper.GetJavaScriptDateFormat(),
                decimalSeparator = CultureInfoHelper.GetDecimalSeparator(),
                datePickerLocale = RenderersResources.DatePickerLocaleCode
            };

            return JsonHelper.Serialize(data);
        }
    }
}