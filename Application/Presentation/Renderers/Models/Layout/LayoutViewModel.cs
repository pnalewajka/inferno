﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class LayoutViewModel
    {
        private const string ScriptOpenTag = "<script type=\"text/javascript\">";
        private const string ScriptCloseTag = "</script>";

        private string _pageTitle;

        public string AutomationIdentifier { get; private set; }
        public string CurrentUICulture { get; private set; }
        public NavigationViewModel Navigation { get; private set; }
        public ScriptsViewModel Scripts { get; private set; }
        public CssViewModel Css { get; private set; }
        public ResourcesViewModel Resources { get; private set; }
        public EnumResourcesViewModel EnumResources { get; private set; }
        public FormattingViewModel Formatting { get; private set; }
        public List<AlertViewModel> Alerts { get; set; }
        public ParametersViewModel Parameters { get; private set; }
        public SettingsViewModel Settings { get; private set; }
        public string AnalyticsScript { get; private set; }

        /// <summary>
        /// Page title is used in the page's title element.
        /// Setting page title also sets the page header (unless aready set).
        /// </summary>
        [LocalizationRequired]
        public string PageTitle
        {
            get { return _pageTitle; }
            set
            {
                _pageTitle = value;

                if (PageHeader == null)
                {
                    PageHeader = value;
                }
            }
        }

        public string PageHeaderViewName { get; set; }

        /// <summary>
        /// Page header is used as a HTML-enabled page title inside the body.
        /// </summary>
        [LocalizationRequired]
        public string PageHeader { get; set; }

        /// <summary>
        /// Logo is displayed in the page's header, usually to the left of the screen.
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// Project name is displayed right after logo, unless the screen is rather small.
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// Name of the layout view to be applied for page rendering.
        /// </summary>
        public string MasterName { get; set; }

        public LayoutViewModel(
            SiteMapModel siteMapModel,
            IAtomicPrincipal principal,
            IUserChoreProvider userChoresProvider,
            ISystemParameterService systemParameterService,
            IWebApplicationSettings webApplicationSettings,
            ISettingsProvider settingsProvider)
        {
            Navigation = new NavigationViewModel(siteMapModel, principal, userChoresProvider, settingsProvider.AuthorizationProviderSettings, webApplicationSettings);
            Scripts = new ScriptsViewModel();
            Css = new CssViewModel();
            Resources = new ResourcesViewModel();
            EnumResources = new EnumResourcesViewModel();
            Formatting = new FormattingViewModel();
            Parameters = new ParametersViewModel(systemParameterService);
            Settings = new SettingsViewModel(settingsProvider);

            Alerts = new List<AlertViewModel>();

            PageTitle = webApplicationSettings.DefaultPageTitle;
            PageHeaderViewName = webApplicationSettings.DefaultPageHeaderViewName;
            PageHeader = webApplicationSettings.DefaultPageHeader;
            LogoUrl = webApplicationSettings.LogoUrl;
            ProjectName = webApplicationSettings.ProjectName;
            MasterName = webApplicationSettings.MasterName;

            CurrentUICulture = CultureInfo.CurrentUICulture.Name;
            AnalyticsScript = CalculateAnaliticsScript(principal, systemParameterService);
        }

        private string CalculateAnaliticsScript(IAtomicPrincipal principal, ISystemParameterService systemParameterService)
        {
            const string UserLoginScriptVariable = "$CURRENT-USER-LOGIN$";
            const string UserEmailScriptVariable = "$CURRENT-USER-EMAIL$";
            const string UserNameScriptVariable = "$CURRENT-USER-NAME$";

            return systemParameterService
                .GetParameter<string>(ParameterKeys.AnalyticsScript)
                .Replace(UserLoginScriptVariable, principal.Login ?? "anonymous")
                .Replace(UserEmailScriptVariable, principal.Email ?? "anonymous@somewhere.com")
                .Replace(UserNameScriptVariable, (principal.FirstName + ' ' + principal.LastName).Trim());
        }

        public string GetFooterWebResources(UrlHelper urlHelper)
        {
            var html = new StringBuilder();

            html.AppendLine(ScriptOpenTag);
            html.AppendLine("$(document).ready(function(){");
            
            foreach (var scriptLine in Scripts.OnDocumentReady)
            {
                html.AppendLine(scriptLine);
            }

            html.AppendLine("});");
            html.AppendLine(ScriptCloseTag);

            return html.ToString();
        }

        public void SetAutomatedIdentifier(ActionExecutingContext context)
        {
            AutomationIdentifier =
                $"auto-{context.ActionDescriptor.ControllerDescriptor.ControllerType.FullName.Replace(".", "-")}-{context.ActionDescriptor.ActionName}"
                    .ToLower();
        }

        public string GetGlobalDataScript(UrlHelper urlHelper)
        {
            var script = new StringBuilder();

            script.AppendLine(ScriptOpenTag);
            script.AppendLine(GetGlobalPropertyScript("resources", Resources.ToJson(), true));
            script.AppendLine(GetGlobalPropertyScript("enumResources", EnumResources.ToJson(), true));
            script.AppendLine(GetGlobalPropertyScript("navigation", Navigation.ToJson(urlHelper)));
            script.AppendLine(GetGlobalPropertyScript("formatting", Formatting.ToJson()));
            script.AppendLine(GetGlobalPropertyScript("parameters", Parameters.ToJson()));
            script.AppendLine(GetGlobalPropertyScript("settings", Settings.ToJson()));
            script.AppendLine(ScriptCloseTag);

            return script.ToString();
        }

        private IEnumerable<string> GetStyleSheetUrls(UrlHelper urlHelper)
        {
            var styleSheetPaths = Css.StyleSheetPaths;
            var styleSheetUrls = styleSheetPaths.Select(urlHelper.Content).ToList();

            return styleSheetUrls;
        }

        private IEnumerable<string> GetFooterScriptUrls(UrlHelper urlHelper)
        {
            var scriptPaths = Scripts.ScriptPaths;
            var scriptUrls = scriptPaths.Select(urlHelper.Content).ToList();

            return scriptUrls;
        }

        private static string GetGlobalPropertyScript(string propertyName, string valueJson, bool assureGlobal = false)
        {
            var js = new StringBuilder();

            if (assureGlobal)
            {
                js.AppendLine("if (!window.Globals) { Globals = { }; }");
            }

            js.Append("Globals." + propertyName + " = ");
            js.Append(valueJson);
            js.Append(";");

            var script = js.ToString();
            return script;
        }
    }
}