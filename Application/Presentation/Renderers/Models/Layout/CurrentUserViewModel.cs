﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Models;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class CurrentUserViewModel
    {
        private Lazy<long> _choreCount;

        public long? Id { get; set; }

        public string Login { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        public string CompanyName { get; set; }

        public bool IsAuthenticated { get; set; }

        public bool CanChangeOwnPassword { get; set; }

        public HashSet<string> Roles { get; set; }

        public bool CanShowNtlmBypassLogin { get; private set; }

        public bool CanViewOwnEmployeeProfile { get; set; }

        public bool HasCorporateDetails => !string.IsNullOrWhiteSpace(CompanyName);


        public long ChoreCount => _choreCount.Value;

        public string FullName
        {
            get
            {
                const string fullNameFormat = "{0} {1}";

                return string.IsNullOrWhiteSpace(FirstName) && string.IsNullOrWhiteSpace(LastName)
                    ? Login
                    : string.Format(fullNameFormat, FirstName, LastName).Trim();
            }
        }

        public string ShortName => string.IsNullOrWhiteSpace(FirstName) ? Login : FirstName;


        public CurrentUserViewModel(
            IAtomicPrincipal principal,
            IUserChoreProvider userChoresProvider,
            AuthorizationProviderSettings authorizationProviderSettings)
        {
            Id = principal.Id;
            Login = principal.Login;
            FirstName = principal.FirstName;
            LastName = principal.LastName;
            CompanyName = principal.CompanyName;
            JobTitle = principal.JobTitle;
            IsAuthenticated = principal.IsAuthenticated;
            CanChangeOwnPassword = principal.IsInRole(SecurityRoleType.CanChangeOwnPassword);
            Roles = new HashSet<string>(principal.Roles ?? new string[0]);
            CanShowNtlmBypassLogin = authorizationProviderSettings.ActiveDirectorySettings.IsPassiveModeEnabled && (authorizationProviderSettings.FacebookSettings.IsEnabled || authorizationProviderSettings.TwitterSettings.IsEnabled || authorizationProviderSettings.GooglePlusSettings.IsEnabled || authorizationProviderSettings.OneTimeTokenSettings.IsEnabled);
            CanViewOwnEmployeeProfile = principal.IsInRole(SecurityRoleType.CanViewOwnEmployeeProfile);

            _choreCount = new Lazy<long>(() => IsAuthenticated ? userChoresProvider.GetActiveChoreCount() : 0);
        }
    }
}