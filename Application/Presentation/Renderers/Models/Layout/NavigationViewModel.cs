﻿using System.Collections.Generic;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class NavigationViewModel
    {
        private readonly SiteMapModel _siteMapModel;
        private readonly IAtomicPrincipal _principal;
        private readonly IWebApplicationSettings _webApplicationSettings;

        [UsedImplicitly] private const string DevEnvironmentBreadcrumb = "DEV";
        [UsedImplicitly] private const string StageEnvironmentBreadcrumb = "STAGE";
        [UsedImplicitly] private const string ProdEnvironmentBreadcrumb = "PROD";

        public IList<MenuItem> Menu => _siteMapModel.Menu;

        public CurrentUserViewModel CurrentUser { get; }
        public BreadcrumbBarViewModel BreadcrumbBar { get; }

        public string EnvironmentName
        {
            get
            {
                #if DEV
                    return DevEnvironmentBreadcrumb;
                #elif STAGE
                    return StageEnvironmentBreadcrumb;
                #else
                    return _principal.IsInRole(SecurityRoleType.CanSwitchBetweenStageAndProd) ? ProdEnvironmentBreadcrumb : string.Empty;;
                #endif
            }
        }

        public NavigationViewModel(
            SiteMapModel siteMapModel,
            IAtomicPrincipal principal,
            IUserChoreProvider userChoresProvider,
            AuthorizationProviderSettings authorizationProviderSettings,
            IWebApplicationSettings webApplicationSettings)
        {
            _siteMapModel = siteMapModel;
            _principal = principal;
            _webApplicationSettings = webApplicationSettings;

            CurrentUser = new CurrentUserViewModel(principal, userChoresProvider, authorizationProviderSettings);
            BreadcrumbBar = new BreadcrumbBarViewModel();
        }

        public string ToJson(UrlHelper urlHelper)
        {
            var data = new
            {
                baseUrl = GetBaseUrl(urlHelper),
                currentUser = CurrentUser
            };

            return JsonHelper.Serialize(data);
        }

        public string GetLandingPageUrl(UrlHelper urlHelper)
        {
            return GetLandingPageUrl(urlHelper, _principal);
        }

        internal string GetLandingPageUrl(UrlHelper urlHelper, IAtomicPrincipal targetPrincipal)
        {
            var landingPage = _siteMapModel.GetLandingPageOrDefault(targetPrincipal);

            if (landingPage != null)
            {
                return landingPage.GetUrl(urlHelper);
            }

            return urlHelper.Content(targetPrincipal.IsAuthenticated 
                ? _webApplicationSettings.DefaultAuthorizedLandingPageUrl
                : _webApplicationSettings.DefaultAnonymousLandingPageUrl);
        }

        private static string GetBaseUrl(UrlHelper urlHelper)
        {
            var baseUrl = urlHelper.Content("~/");

            if (!baseUrl.EndsWith("/"))
            {
                baseUrl += "/";
            }

            return baseUrl;
        }
    }
}