﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class ParametersViewModel
    {
        private readonly ISystemParameterService _systemParameterService;
        public Dictionary<string, object> Parameters { get; private set; }

        public ParametersViewModel(ISystemParameterService systemParameterService)
        {
            _systemParameterService = systemParameterService;
            Parameters = new Dictionary<string, object>();
        }

        /// <summary>
        /// Add simple parameter, to be exposed in javascript globals module, with parameter value
        /// </summary>
        /// <param name="parameterKey"></param>
        /// <param name="value"></param>
        public void AddParameter(string parameterKey, object value)
        {
            if (!Parameters.ContainsKey(parameterKey))
            {
                Parameters.Add(parameterKey, value);
            }
        }

        /// <summary>
        /// Add parameter using system parameter service
        /// </summary>
        /// <typeparam name="TParameterType"></typeparam>
        /// <param name="parameterKey"></param>
        /// <param name="context"></param>
        public void AddParameter<TParameterType>(string parameterKey, object context = null)
        {
            var value = context == null
                ? _systemParameterService.GetParameter<TParameterType>(parameterKey)
                : _systemParameterService.GetParameter<TParameterType>(parameterKey, context);
            
            AddParameter(parameterKey,value);
        }


        public string ToJson()
        {
            return JsonHelper.Serialize(Parameters);
        }
    }
}