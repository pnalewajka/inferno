﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class CssViewModel
    {
        private readonly List<string> _styleSheetPaths = new List<string>();

        public string BodyClass { get; set; }

        public void Add(string scriptPath)
        {
            if (_styleSheetPaths.Contains(scriptPath))
            {
                throw new Exception($"Stylesheet already added: {scriptPath}");
            }

            _styleSheetPaths.Add(scriptPath);
        }

        public IEnumerable<string> StyleSheetPaths
        {
            get
            {
                foreach (var styleSheetPath in _styleSheetPaths)
                {
                    yield return WebResourceHelper.ResolveResourceUrl(styleSheetPath);
                }
            }
        }
    }
}