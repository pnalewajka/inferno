﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class ResourcesViewModel
    {
        private readonly Dictionary<string, string> _localizationStrings = new Dictionary<string, string>();

        public void AddFrom<TResource>(string regex = null, CultureInfo cultureInfo = null)
        {
            cultureInfo = cultureInfo ?? CultureInfo.CurrentCulture;

            var resourceType = typeof (TResource);

            var strings = ResourceManagerHelper.GetAllStrings(resourceType, cultureInfo, regex);
            _localizationStrings.AddRange(strings);
        }

        public void Remove(string regex)
        {
            var keysToRemove = _localizationStrings
                .Keys
                .Where(key => key.Matches(regex))
                .ToList();

            _localizationStrings.Remove(keysToRemove);
        }

        public string ToJson()
        {
            var localizationStrings = _localizationStrings
                .OrderBy(p => p.Key)
                .ToDictionary
                (
                    p => p.Key,
                    p => p.Value
                );

            return JsonHelper.Serialize(localizationStrings);
        }
    }
}