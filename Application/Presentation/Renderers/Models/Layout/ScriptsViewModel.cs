﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Layout
{
    public class ScriptsViewModel
    {
        private readonly List<string> _scriptPaths = new List<string>();

        public void Add(string scriptPath)
        {
            if (_scriptPaths.Contains(scriptPath))
            {
                throw new Exception($"Script already added: {scriptPath}");
            }

            _scriptPaths.Add(scriptPath);
        }

        public IEnumerable<string> ScriptPaths
        {
            get
            {
                foreach (var scriptPath in _scriptPaths)
                {
                    yield return WebResourceHelper.ResolveResourceUrl(scriptPath);
                }
            }
        }

        public IList<string> OnDocumentReady { get; private set; }

        public string ControllerName { get; set; }

        public string ControllerAreaName { get; set; }

        public ScriptsViewModel()
        {
            OnDocumentReady = new List<string>();
        }
    }
}