﻿using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class ErrorDialogViewModel : DialogViewModel
    {
        public string HtmlMessage { get; set; }

        public ErrorDialogViewModel()
            : base("error")
        {
            Buttons.Add(DialogHelper.GetSingleOkButton());

            IconName = DialogResources.CriticalIssueIconName;
            IsErrorMessage = true;
        }
    }
}