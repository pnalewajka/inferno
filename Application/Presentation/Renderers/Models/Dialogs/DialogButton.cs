﻿namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class DialogButton
    {
        public bool IsDisabled { get; set; }
        public string Text { get; set; }
        public string Tag { get; set; }
        public bool IsDismissButton { get; set; }
        public string Hotkey { get; set; }

        public DialogButton()
        {
            IsDismissButton = true;
        }
    }
}