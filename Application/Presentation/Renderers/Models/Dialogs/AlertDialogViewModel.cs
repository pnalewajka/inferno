﻿using Smt.Atomic.Presentation.Renderers.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class AlertDialogViewModel : DialogViewModel
    {
        public string HtmlMessage { get; set; }

        public string OkButtonText
        {
            get { return _okButton.Text; }
            set { _okButton.Text = value; }
        }

        public string OkButtonTag
        {
            get { return _okButton.Tag; }
            set { _okButton.Tag = value; }
        }

        private readonly DialogButton _okButton = DialogHelper.GetSingleOkButton();

        public AlertDialogViewModel()
            : base("alert")
        {
            Buttons.Add(_okButton);
        }
    }
}