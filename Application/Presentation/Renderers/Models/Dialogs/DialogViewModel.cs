﻿using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class DialogViewModel
    {
        public string DialogId { get; private set; }
        public string Title { get; set; }
        public string IconName { get; set; }

        public string ClassName { get; set; }

        public bool IsErrorMessage { get; protected set; }

        public bool HasIcon => !string.IsNullOrEmpty(IconName);

        public List<DialogButton> Buttons { get; protected set; }

        public DialogViewModel(string dialogIdSufix)
        {
            DialogId = "common-dialog-" + dialogIdSufix;
            Buttons = new List<DialogButton>();
        }
    }
}