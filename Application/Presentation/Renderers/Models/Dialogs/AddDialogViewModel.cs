﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class AddDialogViewModel<TModel> : ModelBasedDialogViewModel<TModel>
    {
        public AddDialogViewModel(TModel model, string dialogId = "card-index-add-dialog")
            : base(model, dialogId, StandardButtons.AddCancel)
        {
            Alerts = new List<AlertViewModel>();
        }

        public List<AlertViewModel> Alerts { get; set; }

        public Action<FormRenderingModel> AddFormCustomization { get; set; }
    }
}