﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class EditDialogViewModel<TModel> : ModelBasedDialogViewModel<TModel>
    {
        public EditDialogViewModel(TModel model, string dialogId = "card-index-edit-dialog")
            : base(model, dialogId, StandardButtons.SaveCancel)
        {
            Alerts = new List<AlertViewModel>();
        }

        public List<AlertViewModel> Alerts { get; set; }

        public Action<FormRenderingModel> EditFormCustomization { get; set; }
    }
}
