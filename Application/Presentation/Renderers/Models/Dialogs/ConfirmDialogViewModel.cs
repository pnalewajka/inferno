﻿using Smt.Atomic.Presentation.Renderers.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class ConfirmDialogViewModel : DialogViewModel
    {
        private readonly DialogButton _okButton = DialogHelper.GetOkButton();
        private readonly DialogButton _cancelButton = DialogHelper.GetCancelButton();

        public DialogButton OkButton => _okButton;

        public DialogButton CancelButton => _cancelButton;

        public string HtmlMessage { get; set; }

        public ConfirmDialogViewModel() 
            : this("confirm")
        {
        }

        protected ConfirmDialogViewModel(string dialogIdSufix)
            : base(dialogIdSufix)
        {
            Buttons.Add(_okButton);
            Buttons.Add(_cancelButton);
        }
    }
}