﻿using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class ValuePickerViewModel : ConfirmDialogViewModel
    {
        public ValuePickerViewModel(bool allowMultipleRowSelection, string name)
            : base("value-picker-" + name)
        {
            Title = allowMultipleRowSelection
                        ? DialogResources.ValuePickerDialogPickOneOrMoreTitle
                        : DialogResources.ValuePickerDialogPickOneTitle;

            OkButton.IsDisabled = true;
        }
    }
}