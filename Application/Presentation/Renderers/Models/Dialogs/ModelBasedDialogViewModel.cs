﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public class ModelBasedDialogViewModel<TModel>
        : DialogViewModel
    {
        public TModel Model { get; }
        public string ModelId { get; private set; }
        public LabelWidth LabelWidth { get; set; }
        public StandardButtons StandardButtons { get; private set; }

        public ModelBasedDialogViewModel(TModel model, string dialogId,
            StandardButtons standardButtons = StandardButtons.OkCancel)
            : base(dialogId)
        {
            Model = model;
            ModelId = IdentifierHelper.GetTypeIdentifier(model.GetType());
            InitLabelWidth();
            StandardButtons = standardButtons;

            AddButtons(standardButtons);
        }


        public ModelBasedDialogViewModel(TModel model, StandardButtons standardButtons = StandardButtons.OkCancel)
            : this(model, "show-model-" + model.GetType().Name.ToLowerInvariant(), standardButtons)
        {
        }

        private void InitLabelWidth()
        {
            var dialogLayoutAttribute = AttributeHelper.GetClassAttribute<DialogLayoutAttribute>(Model);

            LabelWidth = dialogLayoutAttribute?.LabelWidth ?? LabelWidth.Default;
        }

        private void AddButtons(StandardButtons standardButtons)
        {
            switch (standardButtons)
            {
                case StandardButtons.Ok:
                    Buttons.Add(DialogHelper.GetOkSubmitButton());
                    break;

                case StandardButtons.OkCancel:
                    Buttons.Add(DialogHelper.GetOkSubmitButton());
                    Buttons.Add(DialogHelper.GetCancelButton());
                    break;

                case StandardButtons.YesNo:
                    Buttons.Add(DialogHelper.GetYesButton());
                    Buttons.Add(DialogHelper.GetNoButton());
                    break;

                case StandardButtons.SaveCancel:
                    Buttons.Add(DialogHelper.GetSaveButton());
                    Buttons.Add(DialogHelper.GetCancelButton());
                    break;

                case StandardButtons.AddCancel:
                    Buttons.Add(DialogHelper.GetAddButton());
                    Buttons.Add(DialogHelper.GetCancelButton());
                    break;

                case StandardButtons.Close:
                    Buttons.Add(DialogHelper.GetCloseButton());
                    break;
            }
        }
    }
}