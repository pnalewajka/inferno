﻿namespace Smt.Atomic.Presentation.Renderers.Models.Dialogs
{
    public enum StandardButtons
    {
        None = 0,

        Ok = 1,
        OkCancel = 2,
        YesNo = 3,
        AddCancel = 4,
        SaveCancel = 5,
        Close = 6,
    }
}