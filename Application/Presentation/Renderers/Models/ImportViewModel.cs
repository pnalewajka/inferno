﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Models
{
    [Identifier("Models.Import")]
    public class ImportViewModel
    {
        [Required]
        [DisplayNameLocalized(nameof(CardIndexResources.FileLabel), typeof(CardIndexResources))]
        [DocumentUpload]
        [Render(Size = Size.Large)]
        public DocumentViewModel File { get; set; }
    }
}