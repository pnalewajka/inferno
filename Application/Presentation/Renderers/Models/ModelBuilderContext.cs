﻿using System.Linq;
using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Renderers.Models
{
    public class ModelBuilderContext
    {
        public string[] Path { get; set; }

        public ModelStateDictionary State { get; private set; }

        public Bootstrap.Layouts.Layout Layout { get; private set; }

        public ModelBuilderContext(Bootstrap.Layouts.Layout layout)
        {
            Layout = layout;
            Path = new string[0];
            State = new ModelStateDictionary();
        }

        public ModelBuilderContext(Bootstrap.Layouts.Layout layout, ModelStateDictionary modelState)
        {
            Layout = layout;
            Path = new string[0];
            State = modelState;
        }

        public ModelBuilderContext(Bootstrap.Layouts.Layout layout, string currentPropertyName, ModelBuilderContext context)
        {
            Layout = layout;
            Path = context.Path.Concat(new[] { currentPropertyName }).ToArray();
            State = context.State;
        }

        public ModelStateDictionary GetValidationModelState()
        {
            var modelPathString = AbsolutePath();
            var nestedModelState = new ModelStateDictionary();

            foreach (var innerState in State.Where(p => p.Key.StartsWith(modelPathString)))
            {
                nestedModelState.Add(innerState.Key.Substring(modelPathString.Length).Trim('.'), innerState.Value);
            }

            return nestedModelState;
        }

        public string AbsolutePath()
        {
            return string.Join(".", Path);
        }
    }
}
