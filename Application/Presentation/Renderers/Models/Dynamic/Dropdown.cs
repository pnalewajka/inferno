﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Dropdown;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public sealed class Dropdown : DynamicModelItem
    {
        private enum DummyEnum
        {
        }

        private class DummyViewModel<T> where T : struct
        {
            [Dropdown(ItemProvider = typeof(EnumBasedListItemProvider<DummyEnum>))]
            public T DummyValue { get; set; }
        }

        [DataMember]
        public string TypeIdentifier { get; set; }

        public override bool IsFieldRequired()
        {
            return true;
        }

        public override object GetValueFromString(string valueAsString)
        {
            return int.Parse(valueAsString);
        }

        public override ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var type = IdentifierHelper.GetTypeByIdentifier(TypeIdentifier);

            var viewModel = typeof(DummyViewModel<>).MakeGenericType(type).GetConstructor(new Type[0]).Invoke(new object[0]);

            var layout = new HorizontalFormLayout();
            var formRenderingModel = layout.BuildRenderingModels(
                viewModel,
                context,
                currentUserRoles);

            var model = (DropdownRenderingModel)formRenderingModel.Controls.Single();
            model.Label = new LocalizedString { English = LabelEn, Polish = LabelPl }.ToString();
            model.Name = $"{nameof(DynamicViewModel.Data)}[{Name}]";

            var listItemProvider = typeof(EnumBasedListItemProvider<>).MakeGenericType(type)
                .GetConstructor(new Type[] { typeof(IPrincipalProvider) })
                .Invoke(new object[] { new ThreadPrincipalProvider() });
            var getItemsMethod = listItemProvider.GetType().GetMethod(nameof(IListItemProvider.GetItems));
            model.Items = ((IEnumerable<ListItem>)getItemsMethod.Invoke(listItemProvider, new object[0])).ToList();

            ControlRenderingModelBuilder.BuildValidationMessages(model.Name, model, context);

            return model;
        }
    }
}