﻿using System.Collections.Generic;
using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Linq;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    [CollectionDataContract(ItemName = "Control")]
    [KnownType(nameof(GetKnownTypes))]
    public class DynamicModelItems : List<DynamicModelItem>
    {
        private static IEnumerable<Type> _knownTypes;

        private static IEnumerable<Type> GetKnownTypes()
        {
            if (_knownTypes == null)
            {
                _knownTypes = Assembly.GetExecutingAssembly()
                                        .GetTypes()
                                        .Where(t => typeof(DynamicModelItem).IsAssignableFrom(t))
                                        .ToList();
            }

            return _knownTypes;
        }
    }
}