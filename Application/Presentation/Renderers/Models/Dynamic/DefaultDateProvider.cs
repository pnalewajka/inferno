﻿using System;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    internal static class DefaultDateProvider
    {
        public static DateTime? GetDefaultValue(DefaultDateProviderParams defaultDate)
        {
            var date = GetReferenceDate(defaultDate.Mode);

            if (date.HasValue)
            {
                date = date.Value
                    .AdjustByMonths(defaultDate.MonthsOffset)
                    .AdjustByMode(defaultDate.Mode)
                    .AdjustByDays(defaultDate.DaysOffset);
            }

            return date;
        }

        private static DateTime? GetReferenceDate(DefaultDateMode mode)
        {
            return mode != DefaultDateMode.NotSet ? DateTime.Today : (DateTime?)null;
        }

        private static DateTime AdjustByMonths(this DateTime date, int? monthsOffset)
        {
            return monthsOffset.HasValue ? date.AddMonths(monthsOffset.Value) : date;
        }

        private static DateTime AdjustByDays(this DateTime date, int? daysOffset)
        {
            return daysOffset.HasValue ? date.AddDays(daysOffset.Value) : date;
        }

        private static DateTime AdjustByMode(this DateTime date, DefaultDateMode mode)
        {
            switch (mode)
            {
                case DefaultDateMode.FirstDayOfMonth:
                    return date.GetFirstDayInMonth();

                case DefaultDateMode.LastDayOfMonth:
                    return date.GetLastDayInMonth();

                case DefaultDateMode.FirstDayOfYear:
                    return date.GetFirstDayOfYear();
            }

            return date;
        }
    }
}
