﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.Checkbox;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public sealed class Checkbox : DynamicModelItem
    {
        private class DummyViewModel
        {
            public bool Checked { get; set; }
        }

        public override bool IsFieldRequired()
        {
            return true;
        }

        public override object GetValueFromString(string valueAsString)
        {
            return bool.Parse(valueAsString);
        }

        public override ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var model = (CheckboxRenderingModel)CreateRenderingModel<DummyViewModel>(context, currentUserRoles);

            return model;
        }
    }
}