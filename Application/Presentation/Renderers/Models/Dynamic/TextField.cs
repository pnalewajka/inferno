﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public sealed class TextField : DynamicModelItem
    {
        private class DummyViewModel
        {
            public string TextBox { get; set; }
        }

        public override bool IsFieldRequired()
        {
            return false;
        }

        public override ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var model = CreateRenderingModel<DummyViewModel>(context, currentUserRoles);

            return model;
        }

        public override object GetValueFromString(string valueAsString)
        {
            return valueAsString;
        }
    }
}