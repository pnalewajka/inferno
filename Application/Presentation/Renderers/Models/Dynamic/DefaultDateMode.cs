﻿using System.Runtime.Serialization;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    [DataContract]
    public enum DefaultDateMode
    {
        NotSet = 0,

        [EnumMember]
        Today,

        [EnumMember]
        FirstDayOfMonth,

        [EnumMember]
        LastDayOfMonth,

        [EnumMember]
        FirstDayOfYear,
    }
}
