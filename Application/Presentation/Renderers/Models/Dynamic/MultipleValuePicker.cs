﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public sealed class MultipleValuePicker : DynamicModelItem
    {
        [Identifier("ValuePickers.DummyMultipleValuePicker")]
        private class DummyValuePicker
        {
        }

        private class DummyViewModel
        {
            [ValuePicker(Type = typeof(DummyValuePicker))]
            public long[] DummyIds { get; set; }
        }

        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string OnResolveUrlJavaScript { get; set; }

        public override bool IsFieldRequired()
        {
            return false;
        }

        public override ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var contextValue = GetContextValue(context);
            var renderModel = CreateRenderingModel<DummyViewModel>(context, currentUserRoles);
            var model = (ValuePickerRenderingModel)renderModel;

            model.ValuePickerTypeIdentifier = Identifier;
            model.OnResolveUrlJavaScript = OnResolveUrlJavaScript;

            var valuePickerType = IdentifierHelper.GetTypeByIdentifier(model.ValuePickerTypeIdentifier);
            var controller = (IValuePicker)ReflectionHelper.CreateInstanceWithIocDependencies(valuePickerType);

            var itemIds = !string.IsNullOrEmpty(contextValue) ? contextValue.Split(',').Select(long.Parse).ToArray() : new long[0];

            var listItems = controller.GetListItems(itemIds, null);
            model.Items = listItems;

            return model;
        }

        public override object GetValueFromString(string valueAsString)
        {
            return valueAsString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(v => long.Parse(v)).ToArray();
        }
    }
}