﻿using System.Runtime.Serialization;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    [DataContract]
    public class DefaultDateProviderParams
    {
        [DataMember]
        public DefaultDateMode Mode { get; set; }

        [DataMember]
        public int? MonthsOffset { get; set; }

        [DataMember]
        public int? DaysOffset { get; set; }
    }
}
