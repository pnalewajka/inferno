﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.HiddenInput;
using Smt.Atomic.Presentation.Renderers.Resources;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    [Identifier("ViewModel.DynamicViewModel")]
    public class DynamicViewModel : FormRenderingModel, IValidatableObject
    {
        [AllowHtml]
        public string Definition { get; set; }

        public IDictionary<string, string> Data { get; set; }

        public DynamicViewModel(string definition) : base(null, null)
        {
            Data = new Dictionary<string, string>();
            Definition = definition;
            ViewModel = this;
        }

        public IDictionary<string, object> GetValues()
        {
            var items = SerializationHelper.DeserializeFromXml<DynamicModelItems>(Definition);
            return items.ToDictionary(i => i.Name, i => i.GetValueFromString(Data[i.Name]));
        }

        private static IEnumerable<ControlRenderingModel> CreateControlModels(string definition, ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var items = SerializationHelper.DeserializeFromXml<DynamicModelItems>(definition);
            var models = items.Select(i => i.ToRenderingModel(context, currentUserRoles)).ToList();

            var definitionHiddenInput = new HiddenInputRenderingModel
            {
                Name = nameof(Definition),
                Value = definition
            };

            models.Add(definitionHiddenInput);

            return models;
        }

        internal void BuildRenderingModels(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            Controls = CreateControlModels(Definition, context, currentUserRoles);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var items = SerializationHelper.DeserializeFromXml<DynamicModelItems>(Definition);

            foreach (var item in items)
            {
                if (item.IsFieldRequired() && (!Data.ContainsKey(item.Name) || item.GetValueFromString(Data[item.Name]) == null))
                {
                    var label = new LocalizedString() { English = item.LabelEn, Polish = item.LabelPl };
                    var message = string.Format(RenderersResources.TheFieldIsRequiredErrorMessage, label.ToString());

                    yield return new ValidationResult(message, new string[] { $"{nameof(Data)}[{item.Name}]" });
                }
            }
        }
    }
}