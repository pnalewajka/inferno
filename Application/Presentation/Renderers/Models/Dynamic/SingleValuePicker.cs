﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.ValuePicker;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public sealed class SingleValuePicker : DynamicModelItem
    {
        [Identifier("ValuePickers.DummySingleValuePicker")]
        private class DummyValuePicker
        {
        }

        private class DummyViewModel
        {
            [ValuePicker(Type = typeof(DummyValuePicker))]
            public long DummyId { get; set; }
        }

        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string OnResolveUrlJavaScript { get; set; }

        public override bool IsFieldRequired()
        {
            return true;
        }

        public override ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var model = (ValuePickerRenderingModel)CreateRenderingModel<DummyViewModel>(context, currentUserRoles);
            model.ValuePickerTypeIdentifier = Identifier;
            model.OnResolveUrlJavaScript = OnResolveUrlJavaScript;

            return model;
        }

        public override object GetValueFromString(string valueAsString)
        {
            if (string.IsNullOrWhiteSpace(valueAsString))
            {
                return null;
            }

            return long.Parse(valueAsString);
        }
    }
}