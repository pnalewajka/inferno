﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public sealed class DatePicker : DynamicModelItem
    {
        private class DummyNotNullableViewModel
        {
            public DateTime SomeDate { get; set; }
        }

        private class DummyNullableViewModel
        {
            public DateTime SomeDate { get; set; }
        }

        [DataMember]
        public DefaultDateProviderParams DefaultDate { get; set; }

        [DataMember]
        public bool IsRequired { get; set; } = true;

        public override bool IsFieldRequired()
        {
            return IsRequired;
        }

        public override ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles)
        {
            var model = IsRequired ? (DatePickerRenderingModel)CreateRenderingModel<DummyNotNullableViewModel>(context, currentUserRoles)
                                   : (DatePickerRenderingModel)CreateRenderingModel<DummyNullableViewModel>(context, currentUserRoles);

            if (DefaultDate != null)
            {
                model.Value = DefaultDateProvider.GetDefaultValue(DefaultDate);
            }
            else
            {
                var contextValue = GetContextValue(context);

                if (DateTime.TryParse(contextValue, out var result))
                {
                    model.Value = result;
                }
            }

            return model;
        }

        public override object GetValueFromString(string valueAsString)
        {
            if (string.IsNullOrWhiteSpace(valueAsString))
            {
                return null;
            }

            return DateTime.Parse(valueAsString);
        }
    }
}