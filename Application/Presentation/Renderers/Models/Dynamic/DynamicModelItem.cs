﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Layouts;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    [DataContract]
    public abstract class DynamicModelItem
    {
        [DataMember]
        public string LabelEn { get; set; }

        [DataMember]
        public string LabelPl { get; set; }

        [DataMember]
        public string Name { get; set; }

        public abstract ControlRenderingModel ToRenderingModel(ModelBuilderContext context, HashSet<string> currentUserRoles);

        protected ControlRenderingModel CreateRenderingModel<TViewModel>(ModelBuilderContext context, HashSet<string> currentUserRoles)
            where TViewModel : new()
        {
            var layout = new HorizontalFormLayout();
            var formRenderingModel = layout.BuildRenderingModels(
                new TViewModel(),
                context,
                currentUserRoles);

            var model = formRenderingModel.Controls.Single();
            model.Label = new LocalizedString { English = LabelEn, Polish = LabelPl }.ToString();
            model.Name = ContextName;

            ControlRenderingModelBuilder.BuildValidationMessages(model.Name, model, context);

            return model;
        }

        protected string GetContextValue(ModelBuilderContext context)
        {
            if (!context.State.ContainsKey(ContextName))
            {
                return string.Empty;
            }

            var item = context.State[ContextName];

            return item?.Value?.AttemptedValue ?? string.Empty;
        }

        protected string ContextName => $"{nameof(DynamicViewModel.Data)}[{Name}]";

        public abstract object GetValueFromString(string valueAsString);

        public abstract bool IsFieldRequired();
    }
}