﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public class DynamicViewModelToDynamicDtoMapping : ClassMapping<DynamicViewModel, DynamicDto>
    {
        public DynamicViewModelToDynamicDtoMapping()
        {
            Mapping = v => new DynamicDto(v.Definition)
            {
                Data = v.GetValues()
            };
        }
    }
}
