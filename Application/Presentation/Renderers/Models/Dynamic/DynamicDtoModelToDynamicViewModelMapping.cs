﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Presentation.Renderers.Models.Dynamic
{
    public class DynamicDtoModelToDynamicViewModelMapping : ClassMapping<DynamicDto, DynamicViewModel>
    {
        public DynamicDtoModelToDynamicViewModelMapping()
        {
            Mapping = d => new DynamicViewModel(d.Definition)
            {
                Data = d.Data.ToDictionary(p => p.Key, p => p.Value.ToString())
            };
        }
    }
}
