namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public interface IRowButtonAvailability
    {
        bool IsAvailableFor(object viewModel);
    }
}