﻿using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class ToolbarButton : CommandButton
    {
        public override string GetButtonCssClass()
        {
            var commonCssClass = $"row-buttons {GetCommonCssClass()} {base.GetButtonCssClass()}";

            if (OnClickAction is DropdownAction)
            {
                return $"{commonCssClass} dropdown-toggle";
            }

            return commonCssClass;
        }
    }
}
