using System;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class RowButtonAvailability<TViewModel> : CommandButtonAvailability, IRowButtonAvailability
    {
        public Func<TViewModel, bool> Predicate { get; set; }

        public RowButtonAvailability()
        {
            Predicate = DefaultPredicate;
        }

        private static bool DefaultPredicate(TViewModel item)
        {
            return true;
        }

        public bool IsAvailableFor(object viewModel)
        {
            var item = (TViewModel) viewModel;

            return Predicate(item);
        }
    }
}