using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class Confirmation
    {
        public bool IsRequired { get; set; }
        
        [LocalizationRequired(true)]
        public string Message { get; set; }

        [LocalizationRequired(true)]
        public string AcceptButtonText { get; set; }

        [LocalizationRequired(true)]
        public string RejectButtonText { get; set; }
    }
}