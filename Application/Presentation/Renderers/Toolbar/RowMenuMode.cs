namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    /// <summary>
    /// Specifies the way the row buttons should be displayed
    /// </summary>
    public enum RowMenuMode
    {
        /// <summary>
        /// Commands should follow toolbar logic, with button groups side-by-side
        /// </summary>
        Toolbar,

        /// <summary>
        /// Commands should be available through a hamburger-like menu
        /// </summary>
        HamburgerMenu
    }
}