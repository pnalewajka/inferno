using System;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    /// <summary>
    /// Indicates how to deal with different ways of hidding toolbar buttons
    /// </summary>
    [Flags]
    public enum CommandButtonVisibility
    {
        /// <summary>
        /// Do not display at all
        /// </summary>
        None = 0,

        /// <summary>
        /// Display only on grid - default
        /// </summary>
        Grid = 1 << 0,

        /// <summary>
        /// Display only on value picker - default
        /// </summary>
        ValuePicker = 1 << 1,

        /// <summary>
        /// Display on grid and on value picker
        /// </summary>
        ValuePickerAndGrid = Grid | ValuePicker
    }
}