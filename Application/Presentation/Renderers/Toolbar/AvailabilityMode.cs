namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public enum AvailabilityMode
    {
        Always,
        SingleRowSelected,
        AtLeastOneRowSelected
    }
}