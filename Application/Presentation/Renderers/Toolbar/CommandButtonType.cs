﻿using System.ComponentModel;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public enum CommandButtonType
    {
        [Description("button")]
        Button,

        [Description("submit")]
        Submit
    }
}
