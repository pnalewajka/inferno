﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Models;
using Action = Smt.Atomic.Presentation.Renderers.Toolbar.Actions.Action;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public interface ICommandButton
    {
        string Id { get; set; }

        string Icon { get; set; }

        string Text { get; set; }

        string CssClass { get; set; }

        string Group { get; set; }

        string CssClass { get; set; }

        string Hotkey { get; set; }

        string AutomationIdentifier { get; set; }

        string LayoutModifierFunctionJavaScript { get; set; }

        bool IsDefault { get; set; }

        TagAttributes Attributes { get; set; }

        bool ShouldOpenInNewTab { set; }

        CommandButtonIconPosition IconPosition { get; set; }

        SecurityRoleType? RequiredRole { get; set; }

        Confirmation Confirmation { get; set; }

        Action OnClickAction { get; set; }

        ICommandButtonAvaliability Availability { get; set; }

        CommandButtonVisibility Visibility { get; set; }

        CommandButtonType ButtonType { get; set; }

        CommandButtonStyle ButtonStyleType { get; set; }

        string Tooltip { get; set; }

        ICommandButton GetCopy(Func<ICommandButton, bool> buttonFilteringPredicate);

        bool IsAccessibleToCurrentUser();

        string GetButtonCssClass();

        bool ShouldBeInitiallyDisabled();

        string GetInnerHtml(bool shouldIncludeCaretForDropdown = true);

        string GetHtmlAttributes();

        string GetListItemCssClass();
    }
}
