namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class CommandButtonAvailability : ICommandButtonAvaliability
    {
        public virtual string GetHtmlAttributes()
        {
            return string.Empty;
        }

        public virtual bool ShouldBeInitiallyDisabled()
        {
            return false;
        }
    }
}