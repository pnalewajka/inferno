﻿using System.ComponentModel;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public enum CommandButtonStyle
    {
        [Description("btn-default")]
        Default = 0,

        [Description("btn-primary")]
        Primary = 1,

        [Description("btn-success")]
        Success = 2,

        [Description("btn-info")]
        Info = 3,

        [Description("btn-warning")]
        Warning = 4,

        [Description("btn-danger")]
        Danger = 5,

        [Description("btn-link")]
        Link = 6,

        [Description("")]
        None = 7
    }
}