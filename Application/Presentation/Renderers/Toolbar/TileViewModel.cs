﻿using System.Linq;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class TileViewModel
    {
        public object Row { get; set; }

        public CommandButtonsViewModel RowButtons { get; set; }
        
        public RowMenuMode RowMenuMode { get; set; }
        
        public GridTableColumns Columns { get; set; }

        public bool HasRowButtons()
        {
            return RowButtons != null
                   && RowButtons.Groups != null
                   && RowButtons.Groups.Any();
        }

        public string GetRowButtonClass()
        {
            if (!HasRowButtons())
            {
                return null;
            }

            return RowMenuMode == RowMenuMode.Toolbar
                       ? "has-toolbar"
                       : "has-hamburger";
        }
    }
}