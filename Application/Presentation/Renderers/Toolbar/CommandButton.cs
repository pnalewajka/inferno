using System;
using System.IO;
using System.Text;
using System.Threading;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;
using Action = Smt.Atomic.Presentation.Renderers.Toolbar.Actions.Action;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class CommandButton : ICommandButton
    {
        public string Id { get; set; }

        public string Icon { get; set; }

        public string CssClass { get; set; }

        public CommandButtonIconPosition IconPosition { get; set; }

        [LocalizationRequired(true)]
        public string Text { get; set; }

        [LocalizationRequired(false)]
        public string Group { get; set; }

        public SecurityRoleType? RequiredRole { get; set; }

        public Confirmation Confirmation { get; set; }

        public Action OnClickAction { get; set; }

        public ICommandButtonAvaliability Availability { get; set; }

        public CommandButtonVisibility Visibility { get; set; }

        public CommandButtonType ButtonType { get; set; }

        public CommandButtonStyle ButtonStyleType { get; set; }

        public bool IsDefault { get; set; }

        public string Hotkey { get; set; }

        public string AutomationIdentifier { get; set; }

        public string LayoutModifierFunctionJavaScript { get; set; }

        public TagAttributes Attributes { get; set; }

        public string Tooltip { get; set; }

        public bool ShouldOpenInNewTab
        {
            set
            {
                if (!(OnClickAction is UriAction uriAction))
                {
                    throw new InvalidOperationException("Cannot automatically set button to open in a new tab");
                }

                uriAction.ShouldOpenInNewTab = value;
            }
        }

        protected string AccessKey => HtmlMarkupHelper.GetAccelerator(Text);

        public CommandButton()
        {
            Visibility = CommandButtonVisibility.Grid;
            Confirmation = new Confirmation();
            Availability = new CommandButtonAvailability();
            Attributes = new TagAttributes();
        }

        public virtual ICommandButton GetCopy(Func<ICommandButton, bool> buttonFilteringPredicate)
        {
            if (!buttonFilteringPredicate(this))
            {
                return null;
            }

            var onClickAction = OnClickAction;

            if (OnClickAction is DropdownAction dropdownAction)
            {
                onClickAction = dropdownAction.GetCopy(buttonFilteringPredicate);

                if (onClickAction == null)
                {
                    return null;
                }
            }

            var copiedButton = (ICommandButton)Activator.CreateInstance(GetType());

            copiedButton.Availability = Availability;
            copiedButton.Confirmation = Confirmation;
            copiedButton.Group = Group;
            copiedButton.Icon = Icon;
            copiedButton.IconPosition = IconPosition;
            copiedButton.Text = Text;
            copiedButton.OnClickAction = onClickAction;
            copiedButton.RequiredRole = RequiredRole;
            copiedButton.Visibility = Visibility;
            copiedButton.IsDefault = IsDefault;
            copiedButton.Hotkey = Hotkey;
            copiedButton.AutomationIdentifier = AutomationIdentifier;
            copiedButton.LayoutModifierFunctionJavaScript = LayoutModifierFunctionJavaScript;
            copiedButton.ButtonStyleType = ButtonStyleType;
            copiedButton.CssClass = CssClass;
            copiedButton.Tooltip = Tooltip;

            return copiedButton;
        }

        public bool ShouldBeInitiallyDisabled()
        {
            return Availability.ShouldBeInitiallyDisabled();
        }

        public bool IsAccessibleToCurrentUser()
        {
            if (Visibility == CommandButtonVisibility.None)
            {
                return false;
            }

            if (!RequiredRole.HasValue)
            {
                return true;
            }

            return Thread.CurrentPrincipal.IsInRole(RequiredRole.Value.ToString());
        }

        public virtual string GetButtonCssClass()
        {
            return $"btn {CssClass}";
        }

        public string GetListItemCssClass()
        {
            var disabledClass = ShouldBeInitiallyDisabled() ? " disabled" : null;

            if (OnClickAction is DropdownAction)
            {
                return $"dropdown-submenu{disabledClass} {AutomationIdentifier}";
            }

            return disabledClass + " " + AutomationIdentifier;
        }

        public string GetHtmlAttributes()
        {
            return GetTagAttributes().Render();
        }

        protected virtual TagAttributes GetTagAttributes()
        {
            if (OnClickAction == null)
            {
                var message = $"Missing onClickedAction in {Text} button definition";
                throw new InvalidDataException(message);
            }

            var tagAttributes = OnClickAction.GetTagAttributes();

            tagAttributes.Merge(Attributes);

            if (Confirmation != null && Confirmation.IsRequired)
            {
                tagAttributes.Add("data-confirm", true);
                tagAttributes.Add("data-confirm-message", Confirmation.Message);

                if (!string.IsNullOrWhiteSpace(Confirmation.AcceptButtonText))
                {
                    tagAttributes.Add("data-confirm-accept-button", Confirmation.AcceptButtonText);
                }

                if (!string.IsNullOrWhiteSpace(Confirmation.RejectButtonText))
                {
                    tagAttributes.Add("data-confirm-reject-button", Confirmation.RejectButtonText);
                }
            }

            if (AccessKey != null)
            {
                tagAttributes.Add("accesskey", AccessKey);
            }

            if (IsDefault)
            {
                tagAttributes.Add("data-default-button", true);
            }

            if (!string.IsNullOrWhiteSpace(Hotkey))
            {
                tagAttributes.Add("data-hotkey", Hotkey);
            }

            if (!string.IsNullOrEmpty(LayoutModifierFunctionJavaScript))
            {
                tagAttributes.Add("data-layout-modifier", LayoutModifierFunctionJavaScript);
            }

            if (!string.IsNullOrEmpty(Tooltip))
            {
                tagAttributes.Add("title", Tooltip);
            }

            return tagAttributes;
        }

        public string GetInnerHtml(bool shouldIncludeCaretForDropdown = true)
        {
            const string space = " ";
            const string beforeTextClassName = "before-text";
            const string afterTextClassName = "after-text";

            var markup = new StringBuilder();

            if (IconPosition == CommandButtonIconPosition.BeforeText)
            {
                markup.Append(Bootstrap.Bootstrap.Icon(Icon, beforeTextClassName));
                markup.Append(space);
            }

            markup.Append(HtmlMarkupHelper.HighlightAccelerator(Text));

            if (IconPosition == CommandButtonIconPosition.AfterText)
            {
                markup.Append(space);
                markup.Append(Bootstrap.Bootstrap.Icon(Icon, afterTextClassName));
            }

            if (shouldIncludeCaretForDropdown && OnClickAction is DropdownAction)
            {
                markup.Append(space);
                markup.Append("<span class=\"caret\"></span>");
            }

            return markup.ToString();
        }

        protected string GetCommonCssClass()
        {
            var disabledClass = ShouldBeInitiallyDisabled() ? " disabled" : string.Empty;
            var btnClass = IsDefault ? CommandButtonStyle.Primary.GetDescription() : ButtonStyleType.GetDescription();

            var commonCssClass = disabledClass + " " + btnClass + " " + AutomationIdentifier;
            return commonCssClass;
        }
    }
}