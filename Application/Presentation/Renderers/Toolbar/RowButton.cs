﻿using System;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class RowButton : CommandButton
    {
        public override string GetButtonCssClass()
        {
            var commonCssClass = $"btn-sm row-buttons {GetCommonCssClass()} {base.GetButtonCssClass()}";

            if (OnClickAction is DropdownAction)
            {
                return $"dropdown-toggle {commonCssClass}";
            }

            return commonCssClass;
        }

        public override ICommandButton GetCopy(Func<ICommandButton, bool> buttonFilteringPredicate)
        {
            var copy = (RowButton)base.GetCopy(buttonFilteringPredicate);

            if (copy == null)
            {
                return null;
            }

            copy.Tooltip = Tooltip;

            return copy;
        }

        protected override TagAttributes GetTagAttributes()
        {
            var attributes = base.GetTagAttributes();

            if (string.IsNullOrWhiteSpace(Tooltip))
            {
                return attributes;
            }

            attributes.Add("title", Tooltip);

            return attributes;
        }
    }
}
