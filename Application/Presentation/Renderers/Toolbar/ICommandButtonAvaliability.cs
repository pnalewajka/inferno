﻿namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public interface ICommandButtonAvaliability
    {
        string GetHtmlAttributes();

        bool ShouldBeInitiallyDisabled();
    }
}