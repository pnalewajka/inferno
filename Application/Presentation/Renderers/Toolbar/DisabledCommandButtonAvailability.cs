﻿namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class DisabledCommandButtonAvailability: CommandButtonAvailability
    {
        public override bool ShouldBeInitiallyDisabled()
        {
            return true;
        }
    }
}
