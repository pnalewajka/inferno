﻿using Smt.Atomic.Presentation.Renderers.Toolbar.Actions;

public class JavascriptPredicatedAction
{
    private const string defaultPredicate = "\"true\"";

    public string Predicate { get; }
    public string RedirectAction { get; }

    public JavascriptPredicatedAction(UriAction redirectAction) : this(redirectAction, defaultPredicate)
    {
    }

    public JavascriptPredicatedAction(UriAction redirectAction, string predicate)
    {
        Predicate = string.IsNullOrEmpty(predicate) ? defaultPredicate : predicate;
        RedirectAction = redirectAction.Url;
    }
}