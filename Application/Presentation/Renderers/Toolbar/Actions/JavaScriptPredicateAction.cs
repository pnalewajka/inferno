﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Smt.Atomic.Presentation.Renderers.Toolbar.Actions
{
    public class JavascriptPredicatedActions
    {
        private IEnumerable<JavascriptPredicatedAction> _predicates { get; }

        public JavascriptPredicatedActions(IEnumerable<JavascriptPredicatedAction> predicates)
        {
            _predicates = predicates;
        }

        public JavaScriptCallAction ToJavascriptSubmitAction()
        {
            var predicateArray = JsonConvert.SerializeObject(_predicates).ToString();
            var onClickCode = $"Forms.submit(grid.resolveUrlBasedOnPredicates({predicateArray}), 'GET', event.ctrlKey, true);";

            return new JavaScriptCallAction(onClickCode);
        }

        public JavaScriptCallAction ToJavascriptDialogAction()
        {
            var predicateArray = JsonConvert.SerializeObject(_predicates).ToString();
            var onClickCode = $"Grid.onHandlePageReloadingDialog(grid.resolveUrlBasedOnPredicates({predicateArray}), event);";

            return new JavaScriptCallAction(onClickCode);
        }
    }
}
