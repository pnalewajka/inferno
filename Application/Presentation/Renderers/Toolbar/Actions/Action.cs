using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Toolbar.Actions
{
    public abstract class Action
    {
        public abstract TagAttributes GetTagAttributes();
    }
}