using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Toolbar.Actions
{
    public class JavaScriptCallAction : Action
    {
        private readonly string _javaScriptCode;

        public JavaScriptCallAction(string javaScriptCode)
        {
            _javaScriptCode = javaScriptCode;
        }

        public override TagAttributes GetTagAttributes()
        {
            var tagAttributes = new TagAttributes();

            tagAttributes.Add("data-action", "execute");

            tagAttributes.Add("data-handler", _javaScriptCode);

            return tagAttributes;
        }

        /// <summary>
        /// Returns javascript code invoking dialog
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static JavaScriptCallAction ShowDialog(UriAction action)
        {
            return new JavaScriptCallAction($"Grid.onHandlePageReloadingDialog('{action.Url}', event);");
        }
    }
}