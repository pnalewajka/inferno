using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Toolbar.Actions
{
    public class DropdownAction : Action
    {
        public List<ICommandButton> Items { get; set; }

        public DropdownAction()
        {
            Items = new List<ICommandButton>();
        }

        public override TagAttributes GetTagAttributes()
        {
            return new TagAttributes();
        }

        public DropdownAction GetCopy(Func<ICommandButton, bool> buttonFilteringPredicate)
        {
            var items = Items
                .Select(b => b.GetCopy(buttonFilteringPredicate))
                .Where(b => b != null)
                .ToList();

            if (!items.Any())
            {
                return null;
            }

            return new DropdownAction
            {
                Items = items
            };
        }
    }
}