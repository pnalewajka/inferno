using System;
using System.Diagnostics;
using System.IO;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Renderers.Toolbar.Actions
{
    /// <summary>
    /// Class representing uri action which is performed by calling the given url
    /// Url can contain the following additional template variables:
    /// ~this.id which will be replaced by the id of the selected row in grid
    /// ~this.ids which will be replaced by the comma separated list of selected ids
    /// ~context which will be replaced by the context filtering expression
    /// ~selected.someProperty which will be replaced by the comma separated list of selected rows' someProperty values
    /// </summary>
    [DebuggerDisplay("{Url}")]
    public class UriAction : Action
    {
        public string Url { get; }

        public bool ShouldOpenInNewTab { get; set; }

        private readonly ActionType _method;

        public UriAction(string url, ActionType method = ActionType.Get, bool skipUriValidation = false)
        {
            ShouldOpenInNewTab = false;

            if (!skipUriValidation && !Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
            {
                throw new InvalidDataException("Wrong URL format: " + url);
            }

            Url = url;
            _method = method;
        }

        public override TagAttributes GetTagAttributes()
        {
            var tagAttributes = new TagAttributes();

            tagAttributes.Add("data-action", "navigate");

            tagAttributes.Add("data-url", Url);
            tagAttributes.Add("data-method", GetMethodName());
            tagAttributes.Add("data-new-tab", HtmlMarkupHelper.ToInputValue(ShouldOpenInNewTab));

            return tagAttributes;
        }

        private string GetMethodName()
        {
            return _method.ToString().ToUpperInvariant();
        }
    }
}