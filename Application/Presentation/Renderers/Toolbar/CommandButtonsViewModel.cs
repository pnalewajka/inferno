﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Presentation.Renderers.CardIndex;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class CommandButtonsViewModel
        : ICellContent
    {
        public IList<ButtonGroupViewModel> Groups { get; private set; }

        public string CssClass => "row-commands";

        public CommandButtonsViewModel()
        {
            Groups = new List<ButtonGroupViewModel>();
        }        
        
        public CommandButtonsViewModel(IEnumerable<ICommandButton> buttons)
        {
            ButtonGroupViewModel currentGroup = null;
            Groups = new List<ButtonGroupViewModel>();

            foreach (var toolbarButton in buttons.Where(b => b.IsAccessibleToCurrentUser()))
            {
                if (currentGroup == null || currentGroup.Name != toolbarButton.Group)
                {
                    currentGroup = new ButtonGroupViewModel(toolbarButton.Group);
                    Groups.Add(currentGroup);
                }

                currentGroup.Buttons.Add(toolbarButton);
            }
        }
    }
}