using System.Text;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public class ToolbarButtonAvailability : CommandButtonAvailability, IToolbarButtonAvaliability
    {
        public AvailabilityMode Mode { get; set; }
        public string Predicate { get; set; }

        public ToolbarButtonAvailability()
        {
            Mode = AvailabilityMode.Always;
        }

        public override string GetHtmlAttributes()
        {
            var attributes = new StringBuilder();
            var hyphenated = NamingConventionHelper.ConvertPascalCaseToHyphenated(Mode.ToString());

            attributes.AppendFormat("data-availability-mode=\"{0}\"", hyphenated);

            if (!string.IsNullOrWhiteSpace(Predicate))
            {
                attributes.AppendFormat(" data-availability-predicate=\"{0}\"", Predicate);
            }

            return attributes.ToString();
        }

        public override bool ShouldBeInitiallyDisabled()
        {
            return Mode != AvailabilityMode.Always;
        }
    }
}