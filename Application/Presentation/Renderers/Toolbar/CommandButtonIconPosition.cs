﻿namespace Smt.Atomic.Presentation.Renderers.Toolbar
{
    public enum CommandButtonIconPosition
    {
        BeforeText = 0,
        AfterText = 1
    }
}