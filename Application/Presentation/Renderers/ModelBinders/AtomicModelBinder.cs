﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ViewModels;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Controls.DatePicker;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Helpers;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public class AtomicModelBinder : DefaultModelBinder
    {
        private Lazy<IAtomicPrincipal> _currentPrincipal = new Lazy<IAtomicPrincipal>(() => new ThreadPrincipalProvider().Current);

        protected override PropertyDescriptorCollection GetModelProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var originalProperties = base.GetModelProperties(controllerContext, bindingContext);
            var additional = GetAliasedProperties(controllerContext, bindingContext);

            return new PropertyDescriptorCollection(originalProperties.OfType<PropertyDescriptor>().Concat(additional).ToArray());
        }

        private IEnumerable<AliasedPropertyDescriptor> GetAliasedProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return GetPropertyDescriptors(controllerContext, bindingContext)
                .SelectMany(property => GetAliasAttributes(property)
                    .Select(attribute =>
                    {
                        ModelMetadata existingProperty;
                        bindingContext.PropertyMetadata.TryGetValue(property.Name, out existingProperty);

                        if (existingProperty != null)
                        {
                            bindingContext.PropertyMetadata.Add(attribute.Alias, existingProperty);
                        }

                        return new AliasedPropertyDescriptor(attribute.Alias, property);
                    })
                );
        }

        private static IEnumerable<AliasAttribute> GetAliasAttributes(PropertyDescriptor property)
        {
            return property
                .ComponentType
                .GetProperty(property.Name)
                .GetCustomAttributes<AliasAttribute>(true);
        }

        private IEnumerable<PropertyDescriptor> GetPropertyDescriptors(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return GetTypeDescriptor(controllerContext, bindingContext)
                .GetProperties()
                .OfType<PropertyDescriptor>();
        }

        private void CheckEnumFieldRequiredRole(Type enumType, object value)
        {
            var attributes = AttributeHelper.GetEnumFieldAttributes<RoleRequiredAttribute>((Enum)value);

            foreach (var attribute in attributes)
            {
                if (!_currentPrincipal.Value.IsInRole(attribute.ForWrite))
                {
                    throw new SecurityException($"Required role is missing: {attribute.ForWrite.ToString()}");
                }
            }
        }

        private void CheckEnumFieldArrayRequiredRole(Type enumType, IEnumerable values)
        {
            foreach (var value in values)
            {
                CheckEnumFieldRequiredRole(enumType, value);
            }
        }

        private void CheckEnumFieldRequiredRole(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
        {
            if (propertyDescriptor.PropertyType.IsEnum)
            {
                CheckEnumFieldRequiredRole(propertyDescriptor.PropertyType, value);
            }

            var collectionEnumType = TypeHelper.GetEnumerationElementType(propertyDescriptor.PropertyType);

            if (collectionEnumType != null && collectionEnumType.IsEnum)
            {
                CheckEnumFieldArrayRequiredRole(collectionEnumType, value as IEnumerable);
            }
        }

        protected override void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
        {
            CheckEnumFieldRequiredRole(controllerContext, bindingContext, propertyDescriptor, value);

            base.SetProperty(controllerContext, bindingContext, propertyDescriptor, value);
        }

        private void SetContextMetadataUsingTypeResolutionAttribute(
            ModelBindingContext bindingContext,
            PropertyDescriptor propertyDescriptor)
        {
            var typeResolutionAttribute = propertyDescriptor.Attributes.OfType<TypeResolutionAttribute>().SingleOrDefault();

            if (typeResolutionAttribute != null)
            {
                var type = typeResolutionAttribute.GetPropertyType(bindingContext.ModelMetadata.Container);
                var model = Activator.CreateInstance(type);
                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
            }
        }

        private object BindEnumCollection(ControllerContext controllerContext,
            ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            var collectionType = propertyDescriptor.PropertyType;
            var enumType = TypeHelper.GetEnumerationElementType(collectionType);

            if (enumType == null || !enumType.IsEnum)
            {
                return null;
            }

            var result = CommaSeparatedBinderHelper.GetList(controllerContext, bindingContext)
                .Select(i => Enum.ToObject(enumType, i))
                .ToCollection(collectionType);

            return result;
        }

        private object BindFlaggedEnum(ControllerContext controllerContext,
            ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            if (!TypeHelper.IsFlaggedEnum(propertyDescriptor.PropertyType))
            {
                return null;
            }

            try
            {
                long flagValue = CommaSeparatedBinderHelper.GetList(controllerContext, bindingContext)
                    .Aggregate(0L, (current, flag) => current | flag);

                return Enum.ToObject(propertyDescriptor.PropertyType, flagValue);
            }
            catch (Exception exception)
            {
                throw new InvalidDataException("Could not convert the combination of flags to an enum value.", exception);
            }
        }

        private object BindBoolean(ControllerContext controllerContext,
            ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            if (!TypeHelper.IsTypeOrNullableType<bool>(propertyDescriptor.PropertyType))
            {
                return null;
            }

            var attemptedValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName)?.AttemptedValue;
            var booleanValue = ConversionHelper.TryConvertZeroOrOneToBool(attemptedValue);

            return booleanValue;
        }

        private object BindDateWithTime(ControllerContext controllerContext,
            ModelBindingContext bindingContext,
            PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            if (!TypeHelper.IsTypeOrNullableType<DateTime>(propertyDescriptor.PropertyType))
            {
                return null;
            }

            var timeSpanPropertyName = TimePickerRenderingModel.TimePickerControlName(bindingContext.ModelName);

            if (!controllerContext.HttpContext.Request.Form.AllKeys.Contains(timeSpanPropertyName))
            {
                return null;
            }

            var value = base.GetPropertyValue(controllerContext, bindingContext, propertyDescriptor, propertyBinder);

            if (value == null)
            {
                return null;
            }

            double spanMinutes;
            var returnValue = (DateTime)value;
            var timeSpanPropertyValue = controllerContext.HttpContext.Request.Form[timeSpanPropertyName];

            if (double.TryParse(timeSpanPropertyValue, out spanMinutes))
            {
                returnValue = returnValue.AddMinutes(spanMinutes);
            }

            return returnValue;
        }

        private object BindRichText(ControllerContext controllerContext,
            ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            var htmlSanitizeAttribute = propertyDescriptor.Attributes.OfType<HtmlSanitizeAttribute>().SingleOrDefault();

            if (htmlSanitizeAttribute == null || !htmlSanitizeAttribute.ShouldBeSanitized)
            {
                return null;
            }

            var value = base.GetPropertyValue(controllerContext, bindingContext, propertyDescriptor, propertyBinder);

            return value != null
                ? HtmlSanitizeHelper.Sanitize(value.ToString(), htmlSanitizeAttribute.SanitizeHtmlMethod)
                : null;
        }

        private object BindHyphenatedEnumValue(ControllerContext controllerContext,
            ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            if (!propertyDescriptor.PropertyType.IsEnum)
            {
                return null;
            }

            var value = bindingContext.ValueProvider.GetValue(propertyDescriptor.Name);

            if (value == null)
            {
                return null;
            }

            var pascalCaseValue = NamingConventionHelper.ConvertHyphenatedToPascalCase(value.AttemptedValue);

            if (EnumHelper.TryParse(propertyDescriptor.PropertyType, pascalCaseValue, out var result))
            {
                return result;
            }

            return null;
        }

        protected override object GetPropertyValue(ControllerContext controllerContext,
            ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor,
            IModelBinder propertyBinder)
        {
            SetContextMetadataUsingTypeResolutionAttribute(bindingContext, propertyDescriptor);

            return
                BindEnumCollection(controllerContext, bindingContext, propertyDescriptor, propertyBinder)
                ?? BindFlaggedEnum(controllerContext, bindingContext, propertyDescriptor, propertyBinder)
                ?? BindHyphenatedEnumValue(controllerContext, bindingContext, propertyDescriptor, propertyBinder)
                ?? BindBoolean(controllerContext, bindingContext, propertyDescriptor, propertyBinder)
                ?? BindRichText(controllerContext, bindingContext, propertyDescriptor, propertyBinder)
                ?? BindDateWithTime(controllerContext, bindingContext, propertyDescriptor, propertyBinder)
                ?? base.GetPropertyValue(controllerContext, bindingContext, propertyDescriptor, propertyBinder);
        }

        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueAdjustingObject = bindingContext.Model as IValueAdjustingObject;
            valueAdjustingObject?.OnModelUpdated();

            base.OnModelUpdated(controllerContext, bindingContext);
        }

        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var factoryType = typeof(IViewModelFactory<>).MakeGenericType(modelType);

            if (ReflectionHelper.HasComponent(factoryType))
            {
                return typeof(AtomicModelBinder)
                    .GetMethod(nameof(GetViewModel), BindingFlags.Instance | BindingFlags.NonPublic)
                    .MakeGenericMethod(modelType)
                    .Invoke(this, new object[0]);
            }

            return base.CreateModel(controllerContext, bindingContext, modelType);
        }

        private TViewModel GetViewModel<TViewModel>()
        {
            var factory = ReflectionHelper.ResolveInterface<IViewModelFactory<TViewModel>>();

            return factory.Create();
        }
    }
}
