﻿namespace Smt.Atomic.Presentation.Renderers.Consts
{
    public static class AutomationIdentifiers
    {
        public const string CardIndexViewButtonIdentifier = "auto-card-index-view";
        public const string CardIndexAddButtonIdentifier = "auto-card-index-add";
        public const string CardIndexEditButtonIdentifier = "auto-card-index-edit";
        public const string CardIndexDeleteButtonIdentifier = "auto-card-index-delete";

        public const string CardIndexExportToExcelButtonIdentifier = "auto-card-index-export-excel";
        public const string CardIndexExportToCsvButtonIdentifier = "auto-card-index-export-csv";
        public const string CardIndexCopyToClipboardButtonIdentifier = "auto-card-index-copy-clipboard";
    }
}
