﻿namespace Smt.Atomic.Presentation.Renderers.Consts
{
    public static class CardIndexStyles
    {
        public const string RecordFormBodyClass = "record-form";

        public const string RowClassSuccess = "success";
        public const string RowClassWarning = "warning";
        public const string RowClassDanger = "danger";
        public const string RowClassClosed = "closed";
    }
}