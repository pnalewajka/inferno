﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Services;

namespace Smt.Atomic.Presentation.Renderers
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register(Component.For<IExportService>().ImplementedBy<ExportService>().LifestyleTransient());
                    container.Register(Component.For<IImportService>().ImplementedBy<ImportService>().LifestyleTransient());
                    break;
            }
        }
    }
}