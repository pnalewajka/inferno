﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Attributes
{
    /// <summary>
    /// Indicates script associated with a given View Model
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ScriptAttribute : Attribute
    {
        public string ScriptPath { get; private set; }

        public ScriptAttribute(string scriptPath)
        {
            ScriptPath = scriptPath;
        }
    }
}
