﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class PasswordIssueRedirectAttribute : Attribute
    {
        public bool RedirectOnPasswordIssues { get; private set; }

        public PasswordIssueRedirectAttribute(bool redirectOnPasswordIssues)
        {
            RedirectOnPasswordIssues = redirectOnPasswordIssues;
        }
    }
}