﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Attributes
{
    /// <summary>
    /// Indicates that property will be ignored on Import (download template + importing)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ImportIgnoreAttribute : Attribute
    {
        /// <summary>
        /// Specifies if property will be ignored
        /// </summary>
        public bool ImportIgnore { get; private set; }

        public ImportIgnoreAttribute(bool importIgnore = true)
        {
            ImportIgnore = importIgnore;
        }
    }
}
