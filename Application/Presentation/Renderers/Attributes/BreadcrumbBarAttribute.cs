﻿using System;

namespace Smt.Atomic.Presentation.Renderers.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class BreadcrumbBarAttribute : Attribute
    {
        /// <summary>
        /// Path consisting of breadcrumb items,
        /// using short breadcrumb names, without prefix,
        /// separated with / (slash)
        /// </summary>
        public string BreadcrumbPath { get; private set; }

        public BreadcrumbBarAttribute(string breadcrumbPath)
        {
            BreadcrumbPath = breadcrumbPath;
        }
    }
}