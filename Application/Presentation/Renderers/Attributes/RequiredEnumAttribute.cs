﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Renderers.Attributes
{
    /// <summary>
    /// Indicates if enum value is required. 'Empty' enum value can be provisioned for testing
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredEnumAttribute : RequiredAttribute
    {
        private readonly Type _enumType;
        private readonly object _emptyEnumValue;

        /// <summary>
        /// Provide 'empty' enum value if it's desired to treat one of enum values as 'empty'
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="emptyEnumValue"></param>
        public RequiredEnumAttribute(Type enumType, object emptyEnumValue = null)
        {
            if (!enumType.IsEnum)
            {
                throw new NotSupportedException("Only enums are supported");
            }

            if (emptyEnumValue != null && enumType != emptyEnumValue.GetType())
            {
                throw new ArgumentException("Empty enum value is of different type than declared enum");
            }

            _enumType = enumType;
            _emptyEnumValue = emptyEnumValue;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (TypeHelper.IsFlaggedEnum(_enumType))
            {
                var flags = EnumHelper.GetFlags((Enum)value);

                return flags.Any();
            }

            if (!Enum.IsDefined(_enumType, value))
            {
                return false;
            }

            if (_emptyEnumValue == null)
            {
                return true;
            }

            var actualValue = value.ToString();
            var emptyValue = _emptyEnumValue.ToString();

            return actualValue != emptyValue;
        }
    }
}
