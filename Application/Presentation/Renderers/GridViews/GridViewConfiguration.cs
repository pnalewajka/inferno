﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Bootstrap.Enums;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.CardIndex.Attributes;
using Smt.Atomic.Presentation.Renderers.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.GridViews
{
    public class GridViewConfiguration<TViewModel> : IGridViewConfiguration
    {
        private const string ColumnMethodPrefix = "Get";
        private Dictionary<string, string> _notIncludedColumnReasons;

        public GridViewConfiguration([Localizable(true)]string name, string identifier)
        {
            ColumnConfiguration = GetColumns();

            Name = name;
            Identifier = identifier;
        }

        /// <summary>
        /// User friendly name.
        /// </summary>
        [Localizable(true)]
        public string Name { get; private set; }

        /// <summary>
        /// Identifier that shows up in URL parameter.
        /// </summary>
        public string Identifier { get; private set; }

        /// <summary>
        /// List of columns with their configurations
        /// </summary>
        public IList<IGridColumnViewConfiguration> ColumnConfiguration { get; private set; }

        /// <summary>
        /// Determines if this configuration is default
        /// </summary>
        public bool IsDefault { get; set; }

        public bool IsHidden { get; set; }

        public void AddColumnBefore(Expression<Func<TViewModel, object>> propertySelector, Expression<Func<TViewModel, object>> beforeColumnExpression)
        {
            var afterColumn = ResolveColumn(beforeColumnExpression);
            var afterColumnIndex = ColumnConfiguration.IndexOf(afterColumn);

            InsertColumn(afterColumnIndex, propertySelector);
        }

        public void AddColumn(Expression<Func<TViewModel, object>> propertySelector)
        {
            InsertColumn(null, propertySelector);
        }

        private void InsertColumn(int? index, Expression<Func<TViewModel, object>> propertySelector)
        {
            var column = new GridPropertyConfiguration<TViewModel>(propertySelector);
            column.IsVisible = true;
            column.IsSortable = false;

            ColumnConfiguration.Insert(index ?? ColumnConfiguration.Count, column);
        }

        public void IncludeColumn(Expression<Func<TViewModel, object>> propertyExpression)
        {
            var column = ResolveColumn(propertyExpression);

            column.IsVisible = true;
        }

        public void IncludeColumns(params Expression<Func<TViewModel, object>>[] propertyExpressions)
        {
            foreach (var propertyExpression in propertyExpressions)
            {
                IncludeColumn(propertyExpression);
            }
        }

        public void IncludeColumns(IEnumerable<Expression<Func<TViewModel, object>>> propertyExpressions)
        {
            foreach (var propertyExpression in propertyExpressions)
            {
                IncludeColumn(propertyExpression);
            }
        }

        public void IncludeAllColumns()
        {
            ChangeColumnsVisibility(ColumnConfiguration, true);
        }

        public void IncludeMethodColumn(string methodPath)
        {
            ChangeColumnsVisibility(ColumnConfiguration.Where(x => x.MemberPath == methodPath), true);
        }

        public void IncludeAllMethodColumns()
        {
            var methodNames = GetAllColumnMethods().Select(x => x.Name);
            var columnsConfiguration = ColumnConfiguration.Where(x => methodNames.Contains(x.MemberPath));
            ChangeColumnsVisibility(columnsConfiguration, true);
        }

        public void ExcludeColumn(Expression<Func<TViewModel, object>> propertyExpression)
        {
            var column = ResolveColumn(propertyExpression);

            column.IsVisible = false;
        }

        public void ExcludeMethodColumn(string methodPath)
        {
            ChangeColumnsVisibility(ColumnConfiguration.Where(x => x.MemberPath == methodPath), false);
        }

        public void ExcludeAllColumns()
        {
            foreach (var properyConfiguration in ColumnConfiguration)
            {
                properyConfiguration.IsVisible = false;
            }
        }

        public void ExcludeColumns(IEnumerable<Expression<Func<TViewModel, object>>> propertyExpressions)
        {
            foreach (var propertyExpression in propertyExpressions)
            {
                ExcludeColumn(propertyExpression);
            }
        }

        public void SetSorting(Expression<Func<TViewModel, object>> propertyExpression,
            SortingDirection sortingDirection)
        {
            var column = ResolveColumn(propertyExpression);

            column.SortingDirection = sortingDirection;
        }

        /// <summary>
        /// If default configuration is not defined, this one is used.
        /// </summary>
        public static GridViewConfiguration<TViewModel> DefaultGridViewConfiguration
        {
            get
            {
                var viewConfiguration = new GridViewConfiguration<TViewModel>(Resources.CardIndexResources.CardIndexDefaultGridView, "default");

                foreach (var properyConfiguration in viewConfiguration.ColumnConfiguration)
                {
                    properyConfiguration.IsVisible = true;
                }

                return viewConfiguration;
            }
        }

        private void ChangeColumnsVisibility(IEnumerable<IGridColumnViewConfiguration> columnConfiguration, bool isVisible)
        {
            foreach (var properyConfiguration in columnConfiguration)
            {
                properyConfiguration.IsVisible = isVisible;
            }
        }

        private IGridColumnViewConfiguration ResolveColumn(Expression<Func<TViewModel, object>> propertyExpression)
        {
            var columnPath = PropertyHelper.GetPropertyPath(propertyExpression);
            var column = ColumnConfiguration.SingleOrDefault(c => c.MemberPath == columnPath);

            if (column == null)
            {
                var columnProperty = FunctionalHelper.Try(
                    tryFunction: () => PropertyHelper.GetProperty(propertyExpression),
                    fallbackValue: null);

                if (columnProperty != null)
                {
                    column = GridPropertyConfiguration<TViewModel>.FromPropertyInfo(columnProperty);
                    ColumnConfiguration.Add(column);

                    return column;
                }

                string exceptionReason = _notIncludedColumnReasons.ContainsKey(columnPath)
                    ? _notIncludedColumnReasons[columnPath]
                    : "Column was not included in the grid configuration.";

                throw new InvalidOperationException(
                    $"Could not customize column '{columnPath}': {exceptionReason}");
            }

            return column;
        }

        private IList<IGridColumnViewConfiguration> GetColumns()
        {
            _notIncludedColumnReasons = new Dictionary<string, string>();

            var columns = new List<IGridColumnViewConfiguration>();
            columns.AddRange(GetPropertyColumns());
            columns.AddRange(GetColumnsBasedOnMethods());

            return columns;
        }

        private IEnumerable<IGridColumnViewConfiguration> GetPropertyColumns()
        {
            var rowType = typeof(TViewModel);
            var propertyInfos = TypeHelper.GetPublicInstanceProperties(rowType).ToArray();

            foreach (var propertyInfo in propertyInfos)
            {
                var isReadOnlyControlProperty = ViewModelPropertyHelper.IsReadOnlyControlProperty(propertyInfo, propertyInfos);

                if (isReadOnlyControlProperty)
                {
                    _notIncludedColumnReasons.Add(propertyInfo.Name, "This is read control property.");

                    continue;
                }

                var visibility = ViewModelPropertyHelper.ShouldBeVisible(propertyInfo, VisibilityScope.Grid);

                if (visibility != ControlVisibility.Show)
                {
                    _notIncludedColumnReasons.Add(
                        propertyInfo.Name,
                        $"Column does not have '{VisibilityScope.Grid}' visibility scope.");

                    continue;
                }

                yield return GridPropertyConfiguration<TViewModel>.FromPropertyInfo(propertyInfo);
            }
        }

        private IEnumerable<IGridColumnViewConfiguration> GetColumnsBasedOnMethods()
        {
            return GetAllColumnMethods()
                .Select(x => new GridMethodConfiguration<TViewModel>(x));
        }

        private IEnumerable<MethodInfo> GetAllColumnMethods()
        {
            var rowType = typeof(TViewModel);
            var desiredType = typeof(ICellContent);

            var matchingMethods = TypeHelper.GetPublicInstanceMethods(rowType)
                .Where(m => m.Name.StartsWith(ColumnMethodPrefix));

            foreach (var method in matchingMethods)
            {
                var returnType = method.ReturnType;

                if (desiredType.IsAssignableFrom(returnType)
                    || (TypeHelper.IsEnumeration(returnType)
                       && desiredType.IsAssignableFrom(TypeHelper.GetEnumerationElementType(returnType))))
                {
                    yield return method;
                }
            }
        }
    }
}
