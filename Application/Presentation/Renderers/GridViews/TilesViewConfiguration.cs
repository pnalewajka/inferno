using System.Collections.Generic;
using System.ComponentModel;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.GridViews
{
    public class TilesViewConfiguration<TViewModel> : GridViewConfiguration<TViewModel>, IInitializableViewConfiguration, ICustomViewConfiguration
    {
        public TilesViewConfiguration([Localizable(true)]string name, string identifier)
            : base(name, identifier)
        {
        }

        public string InitializeJavaScriptAction { get; set; }

        public string CustomViewName { get; set; }

        public GridDisplayMode DisplayMode => GridDisplayMode.Tiles;
    }
}