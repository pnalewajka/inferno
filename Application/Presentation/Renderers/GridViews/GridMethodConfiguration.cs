﻿using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.GridViews
{
    public interface IGridMethodColumnViewConfiguration : IGridColumnViewConfiguration
    {
        MethodInfo MethodInfo { get; }
    }

    public class GridMethodConfiguration<T> : IGridMethodColumnViewConfiguration
    {
        public GridMethodConfiguration(MethodInfo methodInfo)
        {
            MethodInfo = methodInfo;
        }

        public string MemberPath => MethodInfo.Name;
        public MethodInfo MethodInfo { get; private set; }
        public IEnumerable<MemberInfo> MemberChain => new[] { MemberInfo };
        public MemberInfo MemberInfo => MethodInfo;
        public bool IsVisible { get; set; }
        public bool IsSortable { get; set; } = true;
        public SortingDirection SortingDirection { get; set; } = SortingDirection.Unspecified;
    }
}