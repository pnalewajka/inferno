using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Interfaces;

namespace Smt.Atomic.Presentation.Renderers.GridViews
{
    public interface IGridPropertyColumnViewConfiguration : IGridColumnViewConfiguration
    {
        PropertyInfo PropertyInfo { get; }

        LambdaExpression MemberSelector { get; }

        Lazy<Delegate> CompiledMemberSelector { get; }
    }

    public class GridPropertyConfiguration<T> : IGridPropertyColumnViewConfiguration
    {
        public static GridPropertyConfiguration<T> FromPropertyInfo(PropertyInfo propertyInfo)
        {
            var selector = PropertyHelper.GetSelectorForProperty<T>(propertyInfo);

            return new GridPropertyConfiguration<T>(selector)
            {
                SortingDirection = SortingDirection.Unspecified,
            };
        }

        public GridPropertyConfiguration(Expression<Func<T, object>> propertySelector)
        {
            MemberSelector = propertySelector;
            CompiledMemberSelector = new Lazy<Delegate>(() => MemberSelector.Compile());

            // forbid sorting on multi-nested selectors
            IsSortable = PropertyHelper.GetMemberInfos(MemberSelector).Count() == 1;
        }

        public LambdaExpression MemberSelector { get; private set; }
        public Lazy<Delegate> CompiledMemberSelector { get; private set; }
        public string MemberPath => PropertyHelper.GetPropertyName(MemberSelector);
        public IEnumerable<MemberInfo> MemberChain => PropertyHelper.GetMemberInfos(MemberSelector);
        public MemberInfo MemberInfo => MemberChain.Last();
        public PropertyInfo PropertyInfo => (PropertyInfo)MemberInfo;
        public bool IsVisible { get; set; }
        public bool IsSortable { get; set; }
        public SortingDirection SortingDirection { get; set; }
    }
}