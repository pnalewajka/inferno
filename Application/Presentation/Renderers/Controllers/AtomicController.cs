﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models.Alerts;
using Smt.Atomic.Presentation.Renderers.Attributes;
using Smt.Atomic.Presentation.Renderers.Breadcrumbs;
using Smt.Atomic.Presentation.Renderers.Interfaces;
using Smt.Atomic.Presentation.Renderers.Models.Layout;
using Smt.Atomic.Presentation.Renderers.Resources;
using Smt.Atomic.Presentation.SiteMap.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.Renderers.Controllers
{
    public class AtomicController : Controller
    {
        private readonly IAlertService _alertService;
        private readonly IBreadcrumbService _breadcrumbService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISiteMapService _siteMapService;
        private readonly IWebApplicationSettings _webApplicationSettings;
        private readonly ISystemParameterService _systemParameters;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IUserChoreProvider _userChoresProvider;

        /// <summary>
        /// Gets the URL helper object that is used to generate URLs by using routing.
        /// </summary>
        /// 
        /// <returns>
        /// The URL helper object.
        /// </returns>
        public virtual new UrlHelper Url => new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

        protected virtual Type RestorePointControllerType => this.GetType();

        public LayoutViewModel Layout { get; private set; }

        public AtomicController(IBaseControllerDependencies baseDependencies)
        {
            _alertService = baseDependencies.AlertService;
            _breadcrumbService = baseDependencies.BreadcrumbService;
            _siteMapService = baseDependencies.SiteMapService;
            _webApplicationSettings = baseDependencies.WebApplicationSettings;
            _systemParameters = baseDependencies.SystemParameterService;
            _principalProvider = baseDependencies.PrincipalProvider;
            _settingsProvider = baseDependencies.SettingsProvider;
            _userChoresProvider = baseDependencies.UserChoresProvider;

            InitializeLayoutModel();
        }

        public void AddAlerts(BusinessResult businessResult)
        {
            foreach (var alertDto in businessResult.Alerts)
            {
                _alertService.AddAlert(alertDto.Type, alertDto.Message, alertDto.IsDismissable);
            }
        }

        public void AddAlerts(IEnumerable<AlertDto> alerts)
        {
            foreach (var alert in alerts)
            {
                _alertService.AddAlert(alert.Type, alert.Message, alert.IsDismissable);
            }
        }

        public void AddAlert(AlertType alertType, string message, bool isDismissable = true)
        {
            _alertService.AddAlert(alertType, message, isDismissable);
        }

        protected ActionResult RedirectToLandingPage(bool forAnonymousUser = false)
        {
            var targetPrincipal = forAnonymousUser
                ? AtomicPrincipal.Anonymous
                : GetCurrentPrincipal();

            return Redirect(Layout.Navigation.GetLandingPageUrl(Url, targetPrincipal));
        }

        protected ActionResult GeneratePrintout(string printoutViewPath, object printoutViewModel,
            string printoutCssPath = null, string printoutTitle = null)
        {
            Layout.MasterName = "~/Views/Shared/_Print.cshtml";
            Layout.Scripts.Add("~/Scripts/Atomic/Print.js");

            if (!string.IsNullOrEmpty(printoutCssPath))
            {
                Layout.Css.Add(printoutCssPath);
            }

            if (!string.IsNullOrEmpty(printoutTitle))
            {
                Layout.PageTitle = printoutTitle;
            }

            return View(printoutViewPath, printoutViewModel);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _breadcrumbService.Context = new BreadcrumbContext(null, null, _breadcrumbService.Context.SiteMap);
            base.OnActionExecuting(filterContext);

            Layout.SetAutomatedIdentifier(filterContext);

            ViewBag.LayoutViewModel = Layout;
            ViewBag.CurrentUserRoles = Layout.Navigation.CurrentUser.Roles;

            RedirectOnPasswordIssues(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Result is ViewResult)
            {
                PrepareBreadcrumbBar(filterContext);
            }

            base.OnActionExecuted(filterContext);
        }

        protected virtual IList<BreadcrumbItemViewModel> OnBuildBreadcrumbItemModels(BreadcrumbContext breadcrumbContext)
        {
            var breadcrumbBarViewModel = Layout.Navigation.BreadcrumbBar;

            if (breadcrumbBarViewModel.HasDefinition)
            {
                // build models based on the definition
                return _breadcrumbService.BuildBreadcrumbBar(breadcrumbBarViewModel.ItemDefinitions.ToArray());
            }

            var actionDescriptor = breadcrumbContext.ActionDescriptor;
            var breadcrumbBarAtrribute = actionDescriptor.GetAttributes<BreadcrumbBarAttribute>(true).FirstOrDefault();

            if (breadcrumbBarAtrribute != null && !string.IsNullOrEmpty(breadcrumbBarAtrribute.BreadcrumbPath))
            {
                // build models based on the attribute
                return _breadcrumbService.BuildBreadcrumbBar(breadcrumbBarAtrribute.BreadcrumbPath);
            }

            // build models based on the sitemap
            return _breadcrumbService.BuildDefaultBreadcrumbBar().ToList();
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var viewResult = filterContext.Result as ViewResult;

            if (viewResult != null && Layout.MasterName != null)
            {
                viewResult.MasterName = Layout.MasterName;
            }

            Layout.Alerts.AddRange(_alertService.GetAlerts());
            base.OnResultExecuting(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var viewResult = filterContext.Result as ViewResult;

            if (viewResult != null && viewResult.ViewData.ContainsKey(AlertViewModel.AlertDisplayedMarkerKey))
            {
                _alertService.ClearAlerts();
            }

            var partialViewResult = filterContext.Result as PartialViewResult;

            if (partialViewResult != null && partialViewResult.ViewData.ContainsKey(AlertViewModel.AlertDisplayedMarkerKey))
            {
                _alertService.ClearAlerts();
            }

            base.OnResultExecuted(filterContext);
        }

        protected IAtomicPrincipal GetCurrentPrincipal()
        {
            // for the sake of early-in-the-call exception handling
            return _principalProvider.IsSet
                ? _principalProvider.Current
                : AtomicPrincipal.Anonymous;
        }

        private void InitializeLayoutModel()
        {
            var currentPrincipal = GetCurrentPrincipal();
            var siteMap = GetSiteMapModel();

            Layout = new LayoutViewModel(
                siteMap,
                currentPrincipal,
                _userChoresProvider,
                _systemParameters,
                _webApplicationSettings,
                _settingsProvider);

            Layout.Resources.AddFrom<DialogClientResources>();
            Layout.Resources.AddFrom<LayoutClientResources>();
            Layout.Resources.AddFrom<RenderersResources>("Repeater.*");

            ConfigureRestorePoint();
        }

        private void ConfigureRestorePoint()
        {
            Layout.Scripts.ControllerName = RoutingHelper.GetControllerName(RestorePointControllerType);
            Layout.Scripts.ControllerAreaName = RoutingHelper.GetAreaName(RestorePointControllerType);
        }

        private SiteMapModel GetSiteMapModel()
        {
            var siteMap = _siteMapService.GetSiteMap();

            _breadcrumbService.Context = new BreadcrumbContext(null, null, siteMap);

            return siteMap;
        }

        private void RedirectOnPasswordIssues(ActionExecutingContext filterContext)
        {
            var methodInfo = filterContext.ActionDescriptor;
            var redirectionAttribute = methodInfo.GetAttributes<PasswordIssueRedirectAttribute>().FirstOrDefault();

            if (redirectionAttribute != null && !redirectionAttribute.RedirectOnPasswordIssues)
            {
                return;
            }

            var passwordStatus = GetCurrentPrincipal().PasswordStatus;
            string url = null;

            switch (passwordStatus)
            {
                case PasswordStatus.MustSet:
                    url = Url.Action("SetPassword", "Authentication", new { area = "Accounts" });
                    break;

                case PasswordStatus.MustChange:
                    url = Url.Action("ChangePassword", "Authentication", new { area = "Accounts" });
                    break;
            }

            if (!string.IsNullOrWhiteSpace(url))
            {
                filterContext.Result = Redirect(url);
            }
        }

        private void PrepareBreadcrumbBar(ActionExecutedContext filterContext)
        {
            var viewResult = filterContext.Result as ViewResult;
            var viewModel = viewResult?.Model;
            _breadcrumbService.Context = new BreadcrumbContext(filterContext, viewModel,
                _breadcrumbService.Context.SiteMap);
            var breadcrumbBarViewModel = Layout.Navigation.BreadcrumbBar;

            if (!breadcrumbBarViewModel.HasModel)
            {
                Layout.Navigation.BreadcrumbBar.Items = OnBuildBreadcrumbItemModels(_breadcrumbService.Context);
            }
        }

        internal ActionResult GetViewResult(string viewName, object model)
        {
            return View(viewName, model);
        }

        internal ActionResult GetPartialViewResult(string viewName, object model)
        {
            return PartialView(viewName, model);
        }

        /// <summary>
        /// Force model validation
        /// </summary>
        /// <param name="model"></param>
        public void RevalidateModel(object model)
        {
            ModelState.Clear();
            TryValidateModel(model);
        }
    }
}