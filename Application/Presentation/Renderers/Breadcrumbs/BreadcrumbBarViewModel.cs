﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Presentation.Renderers.Breadcrumbs
{
    public class BreadcrumbBarViewModel
    {
        public bool HasModel => !Items.IsNullOrEmpty();

        public bool HasDefinition => !ItemDefinitions.IsNullOrEmpty();

        /// <summary>
        /// Describes which BreadcrumbItems should be used to build the models
        /// </summary>
        public IList<Type> ItemDefinitions { get; set; }

        /// <summary>
        /// Breadcrumb models
        /// </summary>
        public IList<BreadcrumbItemViewModel> Items { get; set; }
    }
}