﻿using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Presentation.Renderers.Breadcrumbs
{
    public class BreadcrumbItemViewModel
    {
        public string Url { get; private set; }
        
        [LocalizationRequired]
        public string DisplayName { get; private set; }

        public BreadcrumbItemViewModel(string url, [LocalizationRequired]string displayName)
        {
            Url = url;
            DisplayName = displayName;
        }
    }
}