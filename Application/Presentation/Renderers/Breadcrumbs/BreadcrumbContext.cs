﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.Renderers.Breadcrumbs
{
    public class BreadcrumbContext
    {
        public ControllerBase ControllerBase {get; set; }
        public ActionDescriptor ActionDescriptor { get; set; }
        public object ViewModel { get; set; }
        public SiteMapModel SiteMap { get; set; }
        public ControllerBase Controller { get; set; }

        public BreadcrumbContext(ActionExecutedContext actionExecutedContext, object viewModel, SiteMapModel siteMapModel)
        {
            ViewModel = viewModel;
            SiteMap = siteMapModel;

            if (actionExecutedContext != null)
            {
                ActionDescriptor = actionExecutedContext.ActionDescriptor;
                Controller = actionExecutedContext.Controller;
                ControllerBase = actionExecutedContext.Controller;
            }
        }
    }
}