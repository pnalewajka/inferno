﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Validation
{
    public class PropertyValidationResult : ValidationResult
    {
        public PropertyValidationResult(string errorMessage, Expression<Func<object>> expression)
            : base(errorMessage, new[] {PropertyHelper.GetPropertyName(expression)})
        {
        }
    }
}
