﻿namespace Smt.Atomic.Presentation.Common.Interfaces
{
    /// <summary>
    /// Adds OnModelUpdated event
    /// </summary>
    public interface IValueAdjustingObject
    {
        /// <summary>
        /// Method executed after Mapping from Request
        /// </summary>
        void OnModelUpdated();
    }
}
