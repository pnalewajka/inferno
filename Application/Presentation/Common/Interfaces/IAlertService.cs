﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Models.Alerts;

namespace Smt.Atomic.Presentation.Common.Interfaces
{
    public interface IAlertService
    {
        void AddAlert(AlertType alertType, string message, bool isDismisable = true);

        void AddSuccess(string message, bool isDismisable = true);
        void AddInformation(string message, bool isDismisable = true);
        void AddWarning(string message, bool isDismisable = true);
        void AddError(string message, bool isDismisable = true);

        List<AlertViewModel> GetAlerts();
        void ClearAlerts();
    }
}