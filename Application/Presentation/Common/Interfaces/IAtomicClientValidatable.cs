﻿using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Common.Interfaces
{
    /// <summary>
    /// Marks a validation attribute as having a potentially custom client-side validation.
    /// Importantly, such attribute is expected to provide extra input tag attributes to tool-up such validation.
    /// </summary>
    public interface IAtomicClientValidatable
    {
        /// <summary>
        /// Populates the provided tag attribute collection with validation attributes.
        /// </summary>
        /// <param name="attributes">Tag attribute collection to populate</param>
        /// <param name="fieldName">Field name (could be used in validation messages)</param>
        void SetValidationAttributes(TagAttributes attributes, string fieldName);
    }
}