﻿using System;

namespace Smt.Atomic.Presentation.Common.Interfaces
{
    public interface IWebApplicationSettings
    {
        /// <summary>
        /// Logo to be displayed in the header
        /// </summary>
        string LogoUrl { get; }

        /// <summary>
        /// Project name that appears next to logo
        /// </summary>
        string ProjectName { get; }

        /// <summary>
        /// Default page title, a reasonable fallback (text only, used in title element)
        /// </summary>
        string DefaultPageTitle { get; }

        /// <summary>
        /// Name of the view used for rendering page header
        /// </summary>
        string DefaultPageHeaderViewName { get; }

        /// <summary>
        /// Default page header, a reasonable fallback (HTML-aware, in-body title)
        /// </summary>
        string DefaultPageHeader { get; }

        /// <summary>
        /// Resource-identifying path for the sitemap file
        /// </summary>
        string SiteMapPath { get; }

        /// <summary>
        /// Type of the resource used for menu localization
        /// </summary>
        Type SiteMapResources { get; }

        /// <summary>
        /// Layout file name
        /// </summary>
        string MasterName { get; }

        /// <summary>
        /// Default landing page URL for anonymous users
        /// </summary>
        string DefaultAnonymousLandingPageUrl { get; }

        /// <summary>
        /// Default landing page URL for authorized users
        /// </summary>
        string DefaultAuthorizedLandingPageUrl { get; }
    }
}