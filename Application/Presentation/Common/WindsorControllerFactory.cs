﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Common
{
    public class WindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel _kernel;

        public WindsorControllerFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        public override void ReleaseController(IController controller)
        {
            _kernel.ReleaseComponent(controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            const string pathCouldNotBeFound = "The controller for path '{0}' could not be found.";

            if (controllerType == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, string.Format(pathCouldNotBeFound, requestContext.HttpContext.Request.Path));
            }

            object areaValue;

            if (requestContext.RouteData.Values.TryGetValue("area", out areaValue))
            {
                var areaName = $"{areaValue}".ToUpperInvariant();

                if (areaName != RoutingHelper.GetAreaName(controllerType).ToUpperInvariant())
                {
                    throw new HttpException((int)HttpStatusCode.NotFound, string.Format(pathCouldNotBeFound, requestContext.HttpContext.Request.Path));
                }
            }

            return (IController)_kernel.Resolve(controllerType);
        }
    }
}