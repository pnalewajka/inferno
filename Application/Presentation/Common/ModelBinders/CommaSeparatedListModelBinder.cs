﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public class CommaSeparatedListModelBinder : IAtomicModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var collectionItemType = bindingContext.ModelType.GetGenericArguments().First();

            var resultList = CommaSeparatedBinderHelper.GetList(controllerContext, bindingContext);

            if (collectionItemType == typeof(int))
            {
                return resultList.Select(v => (int)v).ToList();
            }

            return resultList;
        }

        public void Register(ModelBinderDictionary binders)
        {
            binders.Add(typeof(IList<long>), this);
            binders.Add(typeof(List<long>), this);
            binders.Add(typeof(IList<int>), this);
            binders.Add(typeof(List<int>), this);
        }
    }
}