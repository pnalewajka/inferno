using System;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.SemanticTypes;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public class SemanticTypeModelBinder : IAtomicModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {            
            var modelName = bindingContext.ModelName;
            var underlyingTypeAsString = bindingContext.ValueProvider.GetValue(modelName).AttemptedValue;
            var underlyingType = TypeHelper.GetRawGenericArguments(bindingContext.ModelType, typeof (SemanticType<>))[0];
            var underlyingValue = Convert.ChangeType(underlyingTypeAsString, underlyingType);
            var instance = ReflectionHelper.CreateInstance(bindingContext.ModelType);
            var propertyInfo = instance.GetType().GetProperty("Value");

            try
            {
                propertyInfo.SetValue(instance, underlyingValue);
            }
            catch (Exception exception)
            {
                if (bindingContext.ModelState[modelName] == null)
                {
                    bindingContext.ModelState[modelName] = new ModelState();
                }

                bindingContext.ModelState[modelName].Errors.Add(exception);
            }            

            return instance;
        }

        public void Register(ModelBinderDictionary binders)
        {
            var types = TypeHelper.GetLoadedTypes();
            var semanticTypes = types.Where(t => t.BaseType != null 
                && t.BaseType.IsGenericType 
                && t.BaseType.GetGenericTypeDefinition() == typeof(SemanticType<>));
            
            foreach (var type in semanticTypes)
            {
                binders.Add(type, this);                
            }
        }
    }
}