﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public static class CommaSeparatedBinderHelper
    {      
        public static List<long> GetList(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var modelName = bindingContext.ModelName;
            var value = bindingContext.ValueProvider.GetValue(modelName) ??
                        bindingContext.ValueProvider.GetValue(modelName + "[]");

            var resultList = new List<long>();

            if (value != null)
            {
                resultList = GetValues(value.AttemptedValue);
            }
            else
            {
                var arrayAccessorFormat = modelName + "[{0}]";
                var arrayIndex = 0;

                value = bindingContext.ValueProvider.GetValue(string.Format(arrayAccessorFormat, arrayIndex));

                while (value != null)
                {
                    resultList.AddRange(GetValues(value.AttemptedValue));
                    arrayIndex++;
                    value = bindingContext.ValueProvider.GetValue(string.Format(arrayAccessorFormat, arrayIndex));
                }
            }

            return resultList;
        }

        private static List<long> GetValues(string value)
        {
            return value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Where(s => !s.Equals(ModelBinderConstants.EmptyValueMarker))
                .Select(s => Convert.ToInt64(s))
                .ToList();
        }

    }
}
