﻿using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    /// <summary>
    /// Latitude and longiture (in form of double, double) values to geographic location view model
    /// </summary>
    public class GeographicLocationViewModelModelBinder : IAtomicModelBinder
    {
        private const string ComplexTypeModelPropertyFieldNameFormat = "{0}.{1}";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var modelType = bindingContext.ModelType;
            var modelName = bindingContext.ModelName;
            var isNullable = TypeHelper.IsNullable(modelType);

            var latitudePropertyName = PropertyHelper.GetPropertyName<GeographicLocationViewModel>(g => g.Latitude);
            var longitudePropertyName = PropertyHelper.GetPropertyName<GeographicLocationViewModel>(g => g.Longitude);

            var latitudeValue =
                bindingContext.ValueProvider.GetValue(string.Format(ComplexTypeModelPropertyFieldNameFormat,
                    modelName, latitudePropertyName)).AttemptedValue;

            var longitudeValue =
                bindingContext.ValueProvider.GetValue(string.Format(ComplexTypeModelPropertyFieldNameFormat,
                    modelName, longitudePropertyName)).AttemptedValue;


            if (isNullable && string.IsNullOrWhiteSpace(latitudeValue) && string.IsNullOrWhiteSpace(longitudeValue))
            {
                return null;
            }

            var defaultModelBinder = new DefaultModelBinder();
            var actualBindingContext = bindingContext;
            var modelMetadata = ModelMetadataProviders.Current.GetMetadataForType(()=> new GeographicLocationViewModel(),
                typeof (GeographicLocationViewModel));

            if (isNullable)
            {
                actualBindingContext = new ModelBindingContext
                {
                    ModelMetadata = modelMetadata,
                    ModelState = new ModelStateDictionary(),
                    PropertyFilter = bindingContext.PropertyFilter,
                    ValueProvider = bindingContext.ValueProvider,
                    ModelName = bindingContext.ModelName
                };
            }

            var resultObject = defaultModelBinder.BindModel(controllerContext, actualBindingContext);
            var resultViewModel = resultObject as GeographicLocationViewModel?;

            if (resultViewModel == null)
            {
                return resultObject;
            }

            if (isNullable)
            {
                if (!actualBindingContext.ModelState.IsValid)
                {
                    if (bindingContext.ModelState[modelName] == null)
                    {
                        bindingContext.ModelState[modelName] = new ModelState();
                    }

                    foreach (var error in actualBindingContext.ModelState.SelectMany(k => k.Value.Errors))
                    {
                        bindingContext.ModelState[modelName].Errors.Add(error.ErrorMessage);
                    }
                }
            }

            return resultViewModel;
        }

        public void Register(ModelBinderDictionary binders)
        {
            binders.Add(typeof(GeographicLocationViewModel), this);
            binders.Add(typeof(GeographicLocationViewModel?), this);
        }
    }
}
