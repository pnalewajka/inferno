using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public static class ModelBinderConstants
    {
        public const string EmptyValueMarker = "NULL"; 
    }
}