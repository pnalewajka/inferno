using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public interface IAtomicModelBinder : IModelBinder
    {
        void Register(ModelBinderDictionary binders);
    }
}