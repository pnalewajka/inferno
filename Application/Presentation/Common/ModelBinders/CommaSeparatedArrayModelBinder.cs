﻿using System.Linq;
using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Common.ModelBinders
{
    public class CommaSeparatedArrayModelBinder : IAtomicModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var elementType = bindingContext.ModelType.GetElementType();

            var resultList = CommaSeparatedBinderHelper.GetList(controllerContext, bindingContext);

            if (elementType == typeof(int))
            {
                return resultList.Select(v => (int)v).ToArray();
            }

            return resultList.ToArray();
        }

        public void Register(ModelBinderDictionary binders)
        {
            binders.Add(typeof(long[]), this);
            binders.Add(typeof(int[]), this);
        }
    }
}