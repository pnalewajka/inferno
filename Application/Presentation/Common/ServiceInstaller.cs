﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Presentation.Common.Services;

namespace Smt.Atomic.Presentation.Common
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register(Component.For<IPropertyAccessProvider>().ImplementedBy<TabPropertyAccessProvider>().LifestyleTransient());
                    break;
            }
        }
    }
}
