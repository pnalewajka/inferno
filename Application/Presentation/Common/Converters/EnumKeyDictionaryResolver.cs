﻿using System;
using Newtonsoft.Json.Serialization;

namespace Smt.Atomic.Presentation.Common.Converters
{
    public class EnumKeyDictionaryResolver : DefaultContractResolver
    {
        protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
        {
            var contract = base.CreateDictionaryContract(objectType);
            var keyType = contract.DictionaryKeyType;

            if (keyType.IsEnum)
            {
                contract.DictionaryKeyResolver = propName => ((int)Enum.Parse(keyType, propName)).ToString();
            }

            return contract;
        }
    }
}
