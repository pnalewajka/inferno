﻿using Newtonsoft.Json.Converters;

namespace Smt.Atomic.Presentation.Common.Converters
{
    public class DateOnlyDateTimeConverter : IsoDateTimeConverter
    {
        public DateOnlyDateTimeConverter()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
