﻿using System;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.Common
{
    public class RequestAuthenticationHandler
    {
        private readonly WindsorContainer _container;

        public RequestAuthenticationHandler(WindsorContainer container)
        {
            _container = container;
        }

        public void HandlePostAuthenticateRequest(HttpApplication application)
        {
            var principalBuilder = _container.Resolve<IPrincipalBuilder>();
            
            var login = GetLoginFromAuthorizationCookie(application);
            
            SetPrincipal(login, principalBuilder, application);
        }

        public static void SetPrincipal(string login, IPrincipalBuilder principalBuilder, HttpApplication application)
        {
            var principal = principalBuilder.BuildPrincipal(login);
            Thread.CurrentPrincipal = principal;

            application.Context.User = principal;
        }

        private string GetLoginFromAuthorizationCookie(HttpApplication application)
        {
            string login = null;

            var cookie = application.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                var authTicket = FormsAuthentication.Decrypt(cookie.Value);

                if (authTicket == null)
                {
                    throw new InvalidOperationException("Failed to decrypt the cookie");
                }

                var serializer = new JavaScriptSerializer();
                var userData = serializer.Deserialize<FormsAuthenticationUserData>(authTicket.UserData);
                login = userData != null ? userData.Login : null;
            }

            return login;
        }
    }
}