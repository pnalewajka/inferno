﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Presentation.Common.HttpHeaders
{
    /// <summary>
    /// Content range parsing
    /// According to RFC spec
    /// https://tools.ietf.org/html/rfc7233#section-4.2
    /// </summary>
    public class ContentRangeHeader
    {
        public const string HeaderName = @"Content-Range";
        private const string BytesSection = @"bytes";
        private const string UnknownMarker = @"*";

        private ContentRangeHeader()
        {
        }

        public bool IsUnSatisfiedRange { get; private set; }
        public bool IsLengthUnknown { get; private set; }
        public long CompleteLength { get; private set; }
        public long FirstBytePos { get; private set; }
        public long LastBytePos { get; private set; }

        public static bool TryParse([NotNull] string headerBody, out ContentRangeHeader contentRangeHeader)
        {
            contentRangeHeader = null;

            if (string.IsNullOrWhiteSpace(headerBody))
            {
                return false;
            }

            headerBody = headerBody.Trim().Replace(BytesSection, "").Trim();
            var parts =
                headerBody.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                    .Select(p => p.Trim())
                    .ToList();

            if (parts.Count() != 2)
            {
                return false;
            }

            var parsedHeader = new ContentRangeHeader();

            if (!ParseRange(parsedHeader, parts.First()))
            {
                return false;
            }

            if (!ParseCompleteLength(parsedHeader, parts.Last()))
            {
                return false;
            }

            contentRangeHeader = parsedHeader;
            return true;
        }

        private static bool ParseRange(ContentRangeHeader header, string rangePart)
        {
            if (rangePart == UnknownMarker)
            {
                header.IsUnSatisfiedRange = true;

                return true;
            }

            var range =
                rangePart.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                    .Select(p => p.Trim())
                    .ToList();

            if (range.Count != 2)
            {
                return false;
            }

            long firstBytePos;

            if (!long.TryParse(range.First(), out firstBytePos))
            {
                return false;
            }

            header.FirstBytePos = firstBytePos;

            long lastBytePos;

            if (!long.TryParse(range.Last(), out lastBytePos))
            {
                return false;
            }

            header.LastBytePos = lastBytePos;

            return true;
        }

        private static bool ParseCompleteLength(ContentRangeHeader header, string lengthPart)
        {
            if (lengthPart == UnknownMarker)
            {
                header.IsLengthUnknown = true;

                return true;
            }

            long completeLength;

            if (!long.TryParse(lengthPart, out completeLength))
            {
                return false;
            }

            header.CompleteLength = completeLength;

            return true;
        }
    }
}