﻿using System;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public class ClientSideValueChangeOperation<TViewModel> : ClientSideOperation<TViewModel>
    {
        public override ClientSideOperationType OperationType => ClientSideOperationType.ValueChange;

        [JsonProperty("value")]
        public string Value { get; set; }

        public ClientSideValueChangeOperation(Expression<Func<TViewModel, object>> fieldExpression, string value)
            : base(fieldExpression)
        {
            Value = value;
        }
    }
}
