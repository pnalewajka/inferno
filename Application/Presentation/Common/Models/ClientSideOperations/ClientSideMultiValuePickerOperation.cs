﻿using System;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public class ClientSideMultiValuePickerOperation<TViewModel> : ClientSideOperation<TViewModel>
    {
        [JsonProperty("data")]
        public ValuePickerResponseViewModel[] Data { get; set; }

        public ClientSideMultiValuePickerOperation(Expression<Func<TViewModel, object>> fieldExpression, ValuePickerResponseViewModel[] data)
            : base(fieldExpression)
        {
            Data = data;
        }

        public override ClientSideOperationType OperationType => ClientSideOperationType.MultiValuePicker;
    }
}