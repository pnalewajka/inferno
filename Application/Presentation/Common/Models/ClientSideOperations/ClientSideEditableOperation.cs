﻿using System;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public class ClientSideEditableOperation<TViewModel> : ClientSideOperation<TViewModel>
    {
        public override ClientSideOperationType OperationType => ClientSideOperationType.Editable;

        [JsonProperty("isEditable")]
        public bool IsEditable { get; set; }

        public ClientSideEditableOperation(
            Expression<Func<TViewModel, object>> fieldExpression,
            bool isEditable)
            : base(fieldExpression)
        {
            IsEditable = isEditable;
        }
    }
}
