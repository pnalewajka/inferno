﻿using System;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public class ClientSideCustomOperation<TViewModel> : ClientSideOperation<TViewModel>
    {
        [JsonProperty("data")]
        public object Data { get; set; }

        [JsonProperty("action")]
        public string Action { get; set; }

        public ClientSideCustomOperation(Expression<Func<TViewModel, object>> fieldExpression, string jsAction)
            : this(fieldExpression, jsAction, null)
        {

        }

        public ClientSideCustomOperation(Expression<Func<TViewModel, object>> fieldExpression, string jsAction, object data)
            : base(fieldExpression)
        {
            Action = jsAction;
            Data = data;
        }

        public override ClientSideOperationType OperationType => ClientSideOperationType.CustomAction;
    }
}