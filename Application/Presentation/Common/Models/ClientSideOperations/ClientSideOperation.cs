﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public abstract class ClientSideOperation<TViewModel>
    {
        [JsonProperty("type")]
        public abstract ClientSideOperationType OperationType { get; }

        [JsonProperty("targetElementId")]
        public string TargetElementId { get; private set; }

        [JsonIgnore]
        public Expression<Func<TViewModel, object>> FieldExpression { get; private set; }

        protected ClientSideOperation(Expression<Func<TViewModel, object>> fieldExpression)
        {
            var propertyNames = PropertyHelper.GetPropertyNames(fieldExpression);
            TargetElementId = GetPropertyId(propertyNames);
            FieldExpression = fieldExpression;
        }

        private string GetPropertyId(IEnumerable<string> properties)
        {
            return string.Join(".", properties.Select(
                (p, i) =>
                    i < properties.Count() - 1
                        ? p
                        : NamingConventionHelper.ConvertPascalCaseToHyphenated(p)));
        }
    }
}
