﻿using Newtonsoft.Json;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public class ValuePickerResponseViewModel
    {
        [JsonProperty("id")]
        public long ElementId { get; set; }

        [JsonProperty("toString")]
        public string ElementDescription { get; set; }
    }
}