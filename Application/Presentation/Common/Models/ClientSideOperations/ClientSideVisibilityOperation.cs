﻿using System;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.ClientSideOperations
{
    public class ClientSideVisibilityOperation<TViewModel> : ClientSideOperation<TViewModel>
    {
        public override ClientSideOperationType OperationType => ClientSideOperationType.Visibility;

        [JsonProperty("isVisible")]
        public bool IsVisible { get; set; }

        public ClientSideVisibilityOperation(
            Expression<Func<TViewModel, object>> fieldExpression,
            bool isVisible)
            : base(fieldExpression)
        {
            IsVisible = isVisible;
        }
    }
}
