using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Models
{
    /// <summary>
    /// Container for tag attributes, used primarily for html mark-up refinement
    /// </summary>
    public class TagAttributes
    {
        private readonly IDictionary<string, string> _attributes = new Dictionary<string, string>();

        /// <summary>
        /// Adds a non-value attribute
        /// </summary>
        /// <example>readonly</example>
        /// <param name="name">Attribute name</param>
        public void Add(string name)
        {
            _attributes.Add(name, null);
        }

        /// <summary>
        /// Adds a value-bearing attribute
        /// </summary>
        /// <param name="name">Attribute name</param>
        /// <param name="value">Value of the attribute; if null, represented as empty string</param>
        /// <param name="duplicateBehavior">Hint how to cope with conflicts, defaults to throwing KeyNotFoundException</param>
        public void Add(string name, object value, TagAttributeDuplicateBehavior duplicateBehavior = TagAttributeDuplicateBehavior.ThrowException)
        {
            var stringValue = value == null ? string.Empty : GetStringValue(value);

            if (!_attributes.ContainsKey(name))
            {
                _attributes.Add(name, stringValue);
                return;
            }

            switch (duplicateBehavior)
            {
                case TagAttributeDuplicateBehavior.ThrowException:
                    throw new KeyNotFoundException();

                case TagAttributeDuplicateBehavior.Replace:
                    _attributes[name] = stringValue;
                    break;

                case TagAttributeDuplicateBehavior.Merge:
                    _attributes[name] = HtmlMarkupHelper.AddCssClass(_attributes[name], stringValue);
                    break;
            }
        }

        private static string GetStringValue(object value)
        {
            if (value is bool)
            {
                return HtmlMarkupHelper.ToInputValue((bool)value);
            }

            return value.ToString();
        }

        /// <summary>
        /// Checks if attribute with a given name already exists
        /// </summary>
        /// <param name="name">Attribute name</param>
        /// <returns>true if the attribute is present</returns>
        public bool Contains(string name)
        {
            return _attributes.ContainsKey(name);
        }

        /// <summary>
        /// Removes attribute with a given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if attribute got removed</returns>
        public bool Remove(string name)
        {
            return _attributes.Remove(name);
        }

        /// <summary>
        /// Returns a string representation of all attributes specified, which can be inserted into tag mark-up
        /// </summary>
        /// <remarks>When used in Razor templates, works best with @Raw() construct</remarks>
        /// <returns>String similar to: <value>attribute1="value1" attribute2 attribute3="value3"</value></returns>
        public string Render()
        {
            return string.Join
                (
                    " ",
                    _attributes.Select(p => p.Value == null
                                                ? p.Key
                                                : $"{p.Key}=\"{HttpUtility.HtmlAttributeEncode(p.Value)}\"")
                );
        }

        /// <summary>
        /// Allows to determine if the tag collection has any elements
        /// </summary>
        /// <returns>True if there are any tags defined</returns>
        public bool Any()
        {
            return _attributes.Any();
        }

        /// <summary>
        /// Merge tag attributes 
        /// </summary>
        /// 
        /// <param name="tagAttributes">Tag Attributes</param>
        public void Merge(TagAttributes tagAttributes)
        {
            foreach (var item in tagAttributes._attributes)
            {
                _attributes[item.Key] = item.Value;
            }
        }
    }
}