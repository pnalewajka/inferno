using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Models.Alerts
{
    [Serializable]
    public class AlertViewModel
    {
        public const string AlertDisplayedMarkerKey = "AlertDisplayed";

        public AlertType Type { get; set; }
        public string Message { get; set; }
        public bool IsDismissable { get; set; }

        public string GetCssClass()
        {
            switch (Type)
            {
                case AlertType.Success:
                    return "alert-success";

                case AlertType.Information:
                    return "alert-info";

                case AlertType.Warning:
                    return "alert-warning";

                case AlertType.Error:
                    return "alert-danger";

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public string GetDismissableCssClass()
        {
            if (IsDismissable)
            {
                return "alert-dismissible";
            }

            return null;
        }
    }
}