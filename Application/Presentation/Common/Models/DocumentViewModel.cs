﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.Presentation.Common.Models
{
    public class DocumentViewModel
    {
        public Guid? TemporaryDocumentId { get; set; }
        public long? DocumentId { get; set; }

        [StringLength(1024)]
        public string ContentType { get; set; }

        [StringLength(255)]
        public string DocumentName { get; set; }

        public override string ToString() => DocumentName;
    }
}
