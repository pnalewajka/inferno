﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Smt.Atomic.Presentation.Common.Models.ClientSideOperations;
using Smt.Atomic.Presentation.Common.Results;

namespace Smt.Atomic.Presentation.Common.Models
{
    public class OnValueChangeServerResponse<TViewModel>
    {
        [JsonProperty("operations")]
        public IList<ClientSideOperation<TViewModel>> Operations { get; set; }

        [JsonProperty("serverResultData")]
        public object ServerResultData { get; set; }

        public OnValueChangeServerResponse()
        {
            Operations = new List<ClientSideOperation<TViewModel>>();
            ServerResultData = null;
        }

        public JsonNetResult ToJsonNetResult()
        {
            return new JsonNetResult(this);
        }
    }
}
