﻿using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Models
{
    /// <summary>
    /// Representation of geographic point with additional data
    /// </summary>
    public struct GeographicLocationViewModel
    {
        /// <summary>
        /// Latitude of point
        /// </summary>
        [Range(-90.0, 90.0, ErrorMessageResourceName = "ValueOutOfRange",  ErrorMessageResourceType = typeof(GeographicLocationResources))]
        public decimal Latitude { get; set; }

        /// <summary>
        /// Longitude of point
        /// </summary>
        [Range(-180.0, 180.0, ErrorMessageResourceName = "ValueOutOfRange", ErrorMessageResourceType = typeof(GeographicLocationResources))]
        public decimal Longitude { get; set; }

        /// <summary>
        /// Radius of surrounding circle (buffer area)
        /// </summary>
        [Range(0, 1000, ErrorMessageResourceName = "ValueOutOfRange", ErrorMessageResourceType = typeof(GeographicLocationResources))]
        public int Radius { get; set; }

        /// <summary>
        /// Location name (initially from google location search)
        /// </summary>
        public string Name { get; set; }
    }
}
