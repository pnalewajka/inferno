﻿namespace Smt.Atomic.Presentation.Common.Models
{
    public class PerformanceTestViewModel
    {
        public string Name { get; set; }

        public string Group { get; set; }

        public int Timeout { get; set; }

        public string Url { get; set; }
    }
}
