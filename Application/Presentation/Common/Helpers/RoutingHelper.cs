﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    /// <summary>
    /// Routing helper methods
    /// </summary>
    public static class RoutingHelper
    {
        public const string AreaParameterName = "area";
        public const string IdParameterName = "id";
        public const string ParentIdParameterName = "parent-id";
        private const string ControllerSufix = "Controller";

        /// <summary>
        /// Converts a query-string-like set of parameters to a RouteValueDictionary object
        /// </summary>
        /// <param name="parameters">Ampersand-separated pairs of key=value</param>
        /// <returns>Route-value dictionary with specified values. Empty for empty string.</returns>
        public static RouteValueDictionary GetValueDictionary(string parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters));
            }

            var result = new RouteValueDictionary();

            if (parameters == string.Empty)
            {
                return result;
            }

            var items = HttpUtility.ParseQueryString(parameters);

            foreach (string key in items)
            {
                if (key == null)
                {
                    throw new ArgumentException("Bad format of parameters");
                }
                result[key] = items[key];
            }

            return result;
        }

        /// <summary>
        /// Gets controller name based on its type
        /// </summary>
        /// <typeparam name="TController">Type of the controller</typeparam>
        /// <returns>Controller name used in routing</returns>
        public static string GetControllerName<TController>() where TController : Controller
        {
            return GetControllerName(typeof(TController));
        }

        /// <summary>
        /// Gets controller name based on its type
        /// </summary>
        /// <returns>Controller name used in routing</returns>
        public static string GetControllerName(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.Name.TrimEnd(ControllerSufix);
        }

        /// <summary>
        /// Gets controller name based on its type
        /// </summary>
        /// <returns>Controller name used in routing</returns>
        public static string GetControllerName([NotNull] Controller controller)
        {
            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            return GetControllerName(controller.GetType());
        }

        /// <summary>
        /// Assures proper controller name as used for type lookups
        /// </summary>
        /// <param name="controllerName">Short-hand controller name (or full)</param>
        /// <returns>Type-appropriate controller type name</returns>
        public static string GetControllerTypeName(string controllerName)
        {
            if (controllerName == null)
            {
                throw new ArgumentNullException(nameof(controllerName));
            }

            return !controllerName.EndsWith(ControllerSufix)
                ? controllerName + ControllerSufix
                : controllerName;
        }

        /// <summary>
        /// Retrieves area name from the given controller.
        /// </summary>
        /// <param name="controller">Controller to examine.</param>
        /// <returns>Name of the area or null.</returns>
        public static string GetAreaName([NotNull] Controller controller)
        {
            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            return GetAreaName(controller.GetType());
        }

        /// <summary>
        /// Retrieves area name from the given controller.
        /// </summary>
        /// <typeparam name="TController">Controller type to examine</typeparam>
        /// <returns>Name of the area or null.</returns>
        public static string GetAreaName<TController>() where TController : Controller
        {
            return GetAreaName(typeof(TController));
        }

        /// <summary>
        /// Retrieves area name from the given controller type.
        /// </summary>
        /// <param name="controllerType">Controller type to examine.</param>
        /// <returns>Name of the area or null.</returns>
        public static string GetAreaName(Type controllerType)
        {
            if (controllerType == null)
            {
                throw new ArgumentNullException(nameof(controllerType));
            }

            if (controllerType.Namespace != null)
            {
                var nameSections = controllerType.Namespace.Split('.');

                for (var i = 1; i < nameSections.Length; i++)
                {
                    if (nameSections[i - 1] == "Areas")
                    {
                        return nameSections[i];
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Replaces tokens in a url parameter with their corresponding values
        /// </summary>
        /// <param name="urlParameter">Parameter string to use for replacement</param>
        /// <returns>Url parameter with tokens replaced</returns>
        public static string ReplaceKnownTokens([CanBeNull] string urlParameter)
        {
            if (string.IsNullOrWhiteSpace(urlParameter))
            {
                return urlParameter;
            }

            var resolvers = GetUrlTokenResolvers();

            foreach (IUrlTokenResolver resolver in resolvers)
            {
                foreach (var token in resolver.GetTokens())
                {
                    if (urlParameter.IndexOf(token, StringComparison.Ordinal) >= 0)
                    {
                        urlParameter = urlParameter.Replace(token, resolver.GetTokenValue(token));
                    }
                }
            }

            return urlParameter;
        }

        /// <summary>
        /// Returns a slightly prettier version of the URL
        /// </summary>
        /// <param name="url">URL to make prettier</param>
        /// <returns>Prettified URL</returns>
        public static string PrettifyUrl([NotNull]string url)
        {
            if (url == null)
            {
                throw new ArgumentNullException(nameof(url));
            }

            url = url.Replace("%20", "+");
            url = url.Replace("%2C", ",");

            return url;
        }

        /// <summary>
        /// Gets a dictionary of parameters and returns string query representation
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Query string</returns>
        public static string BuildQueryString(IDictionary<string, object> parameters)
        {
            return string.Join("&", parameters.Select(e => e.Key + "=" + e.Value));
        }

        private static IUrlTokenResolver[] GetUrlTokenResolvers()
        {
            const string urlTokenResolverCacheKey = "UrlTokenResolverCache";

            if (HttpContext.Current.Items[urlTokenResolverCacheKey] == null)
            {
                HttpContext.Current.Items[urlTokenResolverCacheKey] = ReflectionHelper.ResolveAll<IUrlTokenResolver>();
            }

            var resolvers = (IUrlTokenResolver[])HttpContext.Current.Items[urlTokenResolverCacheKey];

            return resolvers;
        }
    }
}
