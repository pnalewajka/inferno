﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    /// <summary>
    /// Extension methods for UrlHelper class
    /// </summary>
    public static class UrlHelperExtension
    {
        /// <summary>
        /// Generates a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static string ActionWithParams(this UrlHelper urlHelper,
                                              [NotNull, AspMvcAction] string actionName,
                                              [NotNull, AspMvcController] string controllerName,
                                              string parameters,
                                              params object[] args)
        {
            if (actionName == null)
            {
                throw new ArgumentNullException(nameof(actionName));
            }

            if (controllerName == null)
            {
                throw new ArgumentNullException(nameof(controllerName));
            }

            var resolvedParameters = string.Format(parameters, args);
            var parameterDictionary = HttpUtility.ParseQueryString(resolvedParameters).ToDictionary();
            var routeValueDictionary = new RouteValueDictionary(parameterDictionary);

            return urlHelper.Action(actionName, controllerName, routeValueDictionary);
        }

        /// <summary>
        /// Generates a fully qualified URL to an action method by using the specified action name, controller name, and route values.
        /// </summary>
        /// <returns>
        /// The fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        [StringFormatMethod("parameters")]
        public static string ActionWithParams(this UrlHelper urlHelper,
                                              [NotNull, AspMvcAction] string actionName,
                                              [NotNull, AspMvcController] string controllerName,
                                              string[] parameters)
        {
            if (actionName == null)
            {
                throw new ArgumentNullException(nameof(actionName));
            }

            if (controllerName == null)
            {
                throw new ArgumentNullException(nameof(controllerName));
            }

            var parameterDictionary = DictionaryHelper
                .ToDictionary(parameters ?? new string[0])
                .ToDictionary(k => k.Key, k => (object)k.Value);

            var routeValueDictionary = new RouteValueDictionary(parameterDictionary);

            return urlHelper.Action(actionName, controllerName, routeValueDictionary);
        }

        /// <summary>
        /// Generates a fully qualified URL to an action method by using the specified action name and route values.
        /// </summary>
        /// <returns>
        /// The fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="parameters">A string that contains the args for a route in url parameters format</param>
        /// <param name="args">Additional args in string.Format convention</param>
        [StringFormatMethod("parameters")]
        public static string ActionWithParams(this UrlHelper urlHelper,
                                              [NotNull, AspMvcAction] string actionName,
                                              string parameters,
                                              params object[] args)
        {
            if (actionName == null)
            {
                throw new ArgumentNullException(nameof(actionName));
            }

            var resolvedParameters = string.Format(parameters, args);
            var parameterDictionary = HttpUtility.ParseQueryString(resolvedParameters).ToDictionary();
            var routeValueDictionary = new RouteValueDictionary(parameterDictionary);

            return urlHelper.Action(actionName, routeValueDictionary);
        }

        /// <summary>
        /// Generates a fully qualified URL to an action method by using the specified action name and route values.
        /// </summary>
        /// <returns>
        /// The fully qualified URL to an action method.
        /// </returns>
        /// <param name="urlHelper">UrlHelper object</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="parameters">Additional parameters as key, value, key, value array of strings</param>
        [StringFormatMethod("parameters")]
        public static string ActionWithParams(this UrlHelper urlHelper,
                                              [NotNull, AspMvcAction] string actionName,
                                              string[] parameters = null)
        {
            if (actionName == null)
            {
                throw new ArgumentNullException(nameof(actionName));
            }

            var parameterDictionary = DictionaryHelper
                .ToDictionary(parameters ?? new string[0])
                .ToDictionary(k => k.Key, k => (object)k.Value);

            var routeValueDictionary = new RouteValueDictionary(parameterDictionary);

            return urlHelper.Action(actionName, routeValueDictionary);
        }
    }
}