﻿using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    /// <summary>
    /// Authentication and authorization methods common for WebApi and WebApp
    /// </summary>
    public static class AuthenticationHelper
    {
        private const int TicketVersion = 1;

        /// <summary>
        /// Create authentication cookie compatible with Forms Authentication
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="login"></param>
        /// <param name="rememberMe"></param>
        /// <returns></returns>
        public static HttpCookie CreateAuthenticationCookie(long userId, string login, bool rememberMe)
        {
            var sessionId = Guid.NewGuid().ToString("N");
#if PROD
            var expiration = DateTime.Now.AddHours(24);
#else
            var expiration = DateTime.Now.AddHours(24 * 365);
#endif
            var formsAuthenticationUserData = new FormsAuthenticationUserData(sessionId, userId, login);
            var javaScriptSerializer = new JavaScriptSerializer();
            var userData = javaScriptSerializer.Serialize(formsAuthenticationUserData);
            var authTicket = new FormsAuthenticationTicket(TicketVersion, login, DateTime.Now, expiration, rememberMe, userData);
            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
            {
                Expires = expiration,
                HttpOnly = true,
#if (PROD || STAGE)
                Secure = true,
#endif
            };

            return cookie;
        }

        /// <summary>
        /// Get user login based on the http request containing Form Authentication cookie
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <returns></returns>
        public static string GetLoginFromAuthorizationCookie(HttpRequest httpRequest)
        {
            string login = null;

            var cookie = httpRequest.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie != null)
            {
                var userData = GetFormsAuthenticationUserData(cookie);
                login = userData != null ? userData.Login : null;
            }

            return login;
        }

        public static string GetSessionIdFromAuthorizationCookie(HttpRequest httpRequest)
        {
            string sessionId = null;

            var cookie = httpRequest.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie != null)
            {
                var userData = GetFormsAuthenticationUserData(cookie);
                sessionId = userData != null ? userData.SessionId : null;
            }

            return sessionId;
        }

        private static FormsAuthenticationUserData GetFormsAuthenticationUserData(HttpCookie cookie)
        {
            var authTicket = FormsAuthentication.Decrypt(cookie.Value);

            if (authTicket == null)
            {
                throw new InvalidOperationException("Failed to decrypt the cookie");
            }

            if (authTicket.Version != TicketVersion)
            {
                throw new InvalidDataException("Unknown authentication ticket version");
            }

            return new JavaScriptSerializer().Deserialize<FormsAuthenticationUserData>(authTicket.UserData);
        }
    }
}