﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    /// <summary>
    /// Helper for rendering html markup
    /// </summary>
    public static class HtmlMarkupHelper
    {
        private const char AcceleratorMarker = '&';

        /// <summary>
        /// Gets the value for the boolean input
        /// </summary>
        public static string ToInputValue(bool boolean)
        {
            return boolean ? "true" : "false";
        }

        /// <summary>
        /// Returns a class string with the specified classes added.
        /// Ignores null or empty new class.
        /// </summary>
        /// <param name="inputCssClass">Existing css class definition</param>
        /// <param name="newCssClass">Css class to be added - or space separated classes, if multiple addition needed</param>
        /// <returns>Css class string with the specified class added (unless null).</returns>
        public static string AddCssClass(string inputCssClass, string newCssClass)
        {
            if (newCssClass == null || string.IsNullOrWhiteSpace(newCssClass))
            {
                return inputCssClass;
            }

            var classesToAdd = newCssClass
                .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();

            var existingClasses = (inputCssClass ?? string.Empty)
                .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .ToHashSet();

            foreach (var className in classesToAdd)
            {
                existingClasses.Add(className);
            }

            return string.Join(" ", existingClasses);
        }

        /// <summary>
        /// Retrieves accelerator from button text. Null if no access key found.
        /// </summary>
        /// <param name="buttonText">Button text, possibly with &amp;accelerator specified</param>
        /// <returns>Access key or null</returns>
        public static string GetAccelerator(string buttonText)
        {
            if (string.IsNullOrWhiteSpace(buttonText))
            {
                return null;
            }

            var markerIndex = buttonText.IndexOf(AcceleratorMarker);

            return markerIndex == -1
                       ? null
                       : buttonText.Substring(markerIndex + 1, 1);
        }

        /// <summary>
        /// Returns markup for a visual representation of the button text with accelerator highlighted
        /// </summary>
        /// <param name="buttonText">Button text, possibly with &amp;accelerator specified</param>
        /// <returns>Button text markup with accelerator highlighted</returns>
        public static string HighlightAccelerator(string buttonText)
        {
            if (string.IsNullOrWhiteSpace(buttonText))
            {
                return string.Empty;
            }

            var markerIndex = buttonText.IndexOf(AcceleratorMarker);

            if (markerIndex == -1)
            {
                return buttonText;
            }

            var before = buttonText.Substring(0, markerIndex);
            var accessKey = buttonText.Substring(markerIndex + 1, 1);
            var after = buttonText.Substring(markerIndex + 2);

            return $"{before}<span class=\"accelerator\">{accessKey}</span>{after}";
        }

        /// <summary>
        /// Returns plain-text version of the button text, i.e. with no accelerator markers
        /// </summary>
        /// <param name="buttonText">Button text</param>
        /// <returns>Button text with accelerator markers removed</returns>
        public static string RemoveAccelerator(string buttonText)
        {
            if (buttonText == null)
            {
                return buttonText;
            }

            return buttonText.Replace(AcceleratorMarker.ToString(), string.Empty);
        }
    }
}