﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    /// <summary>
    /// Extensions for ModelStateDictionary
    /// </summary>
    public static class ModelStateDictionaryExtensions
    {
        /// <summary>
        /// Adds error to model state using lambda expression to pass property name
        /// </summary>
        /// <typeparam name="T">Model type</typeparam>
        /// <param name="modelState">Model state</param>
        /// <param name="property">Name of the property passed as lambda expression</param>
        /// <param name="message">Message to be added</param>
        public static void AddModelError<T>(this ModelStateDictionary modelState, Expression<Func<T, object>> property, string message)
        {
            modelState.AddModelError(PropertyHelper.GetPropertyName(property), message);
        }

        /// <summary>
        /// Adds general error to model state
        /// </summary>
        /// <param name="modelState">Model state</param>
        /// <param name="message">Message to be added</param>
        public static void AddGeneralModelError(this ModelStateDictionary modelState, string message)
        {
            modelState.AddModelError(string.Empty, message);
        }

        /// <summary>
        /// Adds general-level error to model state that is not displayed. Good to stop model processing at validation.
        /// </summary>
        /// <param name="modelState">Model state</param>
        public static void Invalidate(this ModelStateDictionary modelState)
        {
            const string notExisitingPropertyName = "__missingProperty";

            modelState.AddModelError(notExisitingPropertyName, string.Empty);
        }
    }
}