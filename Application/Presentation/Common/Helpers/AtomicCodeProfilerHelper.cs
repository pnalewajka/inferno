﻿using System;
using System.Threading;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    public static class AtomicCodeProfilerHelper
    {
        public const string AtomicCodeProfilerCookieName = "MiniProfiler";

        public static bool IsProfilerEnabled =>
            HttpContext.Current.Request.Cookies[AtomicCodeProfilerCookieName] != null;

        public static bool IsUserAllowedToSeeMiniProfilerUi(HttpRequest request)
        {
            var profilerEnabled = request[AtomicCodeProfilerCookieName] != null;
            var principal = Thread.CurrentPrincipal as IAtomicPrincipal;

            if (principal == null)
            {
                return false;
            }

            return profilerEnabled && principal.IsInRole(SecurityRoleType.CanExecuteDeveloperTools);
        }

        public static void EnableProfiler()
        {
            if (!IsProfilerEnabled)
            {
                var cookie = new HttpCookie(AtomicCodeProfilerCookieName)
                {
                    HttpOnly = true,
#if (PROD || STAGE)
                    Secure = true,
#endif
                };

                HttpContext.Current.Response.AppendCookie(cookie);
            }
        }

        public static void DisableProfiler()
        {
            var cookie = new HttpCookie(AtomicCodeProfilerCookieName)
            {
                Expires = DateTime.Now.AddDays(-1),
                HttpOnly = true,
#if (PROD || STAGE)
                Secure = true,
#endif
                Value = null
            };

            HttpContext.Current.Response.AppendCookie(cookie);
        }
    }
}
