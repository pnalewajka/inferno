﻿using System;
using System.Web;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    public static class ActivityTrackingHelper
    {
        public const string ActivityTrackingCookieName = "Activity";

        public static HttpCookie CreateActivityTrackingCookie()
        {
            var activityId = Guid.NewGuid().ToString("N");

            var cookie = new HttpCookie(ActivityTrackingCookieName, activityId)
            {
                Expires = DateTime.MinValue,
                HttpOnly = true,
#if (PROD || STAGE)
                Secure = true,
#endif
            };

            return cookie;
        }

        public static string GetActivityIdFromActivityTrackingCookie(HttpRequest httpRequest)
        {
            string activityId = null;

            var cookie = httpRequest.Cookies[ActivityTrackingCookieName];

            if (cookie != null)
            {
                activityId = cookie.Value;
            }

            return activityId;
        }
    }
}
