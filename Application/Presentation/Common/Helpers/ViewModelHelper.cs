﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    public class ViewModelHelper
    {
        public const string IdPropertyName = "Id";

        /// <summary>
        /// Updates the specified model instance using values from the value provider,
        /// a prefix, a list of properties to exclude, and a list of properties to include.
        /// 
        /// Works around the generic method call and uses the actual model type.
        /// </summary>
        /// <param name="controller">The controller from which the generic TryUpdateModel method is called.</param>
        /// <param name="model">The model instance to update.</param>
        /// <param name="prefix">The prefix to use when looking up values in the value provider.</param>
        /// <param name="includeProperties">A list of properties of the model to update.</param>
        /// <param name="excludeProperties">A list of properties to explicitly exclude from the update. These are excluded even if they are listed in the includeProperties parameter list.</param>
        /// <param name="valueProvider">A dictionary of values that is used to update the model.</param>
        /// <returns>true if the update is successful; otherwise, false.</returns>
        public static bool TryUpdateModel(Controller controller, object model, string prefix = null,
            string[] includeProperties = null, string[] excludeProperties = null,
            IValueProvider valueProvider = null, bool clearValidationErrors = false)
        {
            const string tryUpdateModelMethodName = "TryUpdateModel";

            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            var controllerType = controller.GetType();

            // find the most suitable 'TryUpdateModel' override 
            // to work around MVC's generic type handling
            // using method with five params for best re-usability
            var tryUpdateModelMethod = TypeHelper
                .GetNonPublicInstanceMethods(controllerType)
                .SingleOrDefault(x => x.Name == tryUpdateModelMethodName
                                     && x.GetParameters().Count() == 5);

            if (tryUpdateModelMethod == null)
            {
                throw new NullReferenceException("Could not find the generic TryUpdateModel method in the controller provided.");
            }

            var methodInfo = tryUpdateModelMethod
                .MakeGenericMethod(new[] { model.GetType() });

            var result = (bool)methodInfo.Invoke(controller, new[]
                {
                    model,
                    prefix,
                    includeProperties,
                    excludeProperties,
                    valueProvider ?? controller.ValueProvider
                });

            if (clearValidationErrors)
            {
                foreach (var key in controller.ModelState.Keys)
                {
                    controller.ModelState[key].Errors.Clear();
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if the given conditional validation should be applied for a given view model
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool IsSuitableFor([NotNull] object viewModel, FormType target)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException(nameof(viewModel));
            }

            if (target == FormType.Always)
            {
                return true;
            }

            var viewModelType = viewModel.GetType();
            var propertyInfo = viewModelType.GetProperty("Id");

            if (propertyInfo == null)
            {
                throw new InvalidDataException("No id property detected");
            }

            var currentFormMode = FormType.None;

            if (propertyInfo.PropertyType == typeof(string))
            {
                var idValue = (string)propertyInfo.GetValue(viewModel);
                currentFormMode = idValue == null ? FormType.AddForm : FormType.EditForm;
            } 
            else if (propertyInfo.PropertyType == typeof (long))
            {
                var idValue = (long)propertyInfo.GetValue(viewModel);
                currentFormMode = idValue == 0 ? FormType.AddForm : FormType.EditForm;
            }

            if (currentFormMode == FormType.None)
            {
                throw new InvalidDataException("Invalid id property type detected");
            }

            return target.HasFlag(currentFormMode);            
        }

        public static long? GetModelIdValue([NotNull] object viewModel)
        {
            var viewModelType = viewModel.GetType();
            var propertyInfo = viewModelType.GetProperty(IdPropertyName);

            return (long?) propertyInfo.GetValue(viewModel);
        }
    }
}