﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Hosting;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    public static class WebResourceHelper
    {
        private static readonly IDictionary<string, string> ResolvedResourceUrls = new ConcurrentDictionary<string, string>();

        private static bool IsDebuggingEnabled => HttpContext.Current.IsDebuggingEnabled;

        /// <summary>
        /// Returns resource url with correct versioning info depending on debugging being enabled.
        /// Takes advantages of minifed resource, if possible.
        /// </summary>
        /// <param name="virtualResourcePath">Virtual resource path (~/)</param>
        /// <returns>Virtual path to the resource with version parameter if debugging enabled</returns>
        public static string ResolveResourceUrl(string virtualResourcePath)
        {
            if (IsDebuggingEnabled)
            {
                return virtualResourcePath;                
            }

            string result;

            if (!ResolvedResourceUrls.TryGetValue(virtualResourcePath, out result))
            {
                result = CalculateVersionedUrl(virtualResourcePath);
                ResolvedResourceUrls[virtualResourcePath] = result;
            }

            return result;
        }

        private static string CalculateVersionedUrl(string virtualResourcePath)
        {
            var result = virtualResourcePath;

            var extension = Path.GetExtension(virtualResourcePath);
            var minifiedVersionVirtualResourcePath = Path.ChangeExtension(virtualResourcePath, $"min{extension}");

            var physicalPath = HostingEnvironment.MapPath(minifiedVersionVirtualResourcePath);

            if (File.Exists(physicalPath))
            {
                result = minifiedVersionVirtualResourcePath;
            }
            else
            {
                physicalPath = HostingEnvironment.MapPath(virtualResourcePath);
            }

            if (physicalPath == null || !File.Exists(physicalPath))
            {
                return virtualResourcePath;
            }

            var fileContent = File.ReadAllText(physicalPath);
            var calculateMd5Hash = HashHelper.CalculateMd5Hash(fileContent);

            result += $"?v={calculateMd5Hash}";

            return result;
        }
    }
}