﻿using System;
using System.Linq;
using System.Net;
using System.Web;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    /// <summary>
    /// Helper methods
    /// </summary>
    public class IpAddressHelper
    {
        /// <summary>
        /// Extract client ip address (with possible proxy handling)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetClientIpAddress(HttpRequestBase request)
        {
            try
            {
                var userHostAddress = request.UserHostAddress;
                if (userHostAddress == null)
                {
                    return IPAddress.Any.ToString();
                }
                var address = IPAddress.Parse(userHostAddress);

                if (IPAddress.IsLoopback(address))
                {
                    userHostAddress = IPAddress.Loopback.ToString();
                }

                var xForward = request.ServerVariables["X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(xForward))
                {
                    return userHostAddress;
                }

                var publicForwardingIps = xForward.Split(',').Where(ip => !IsPrivate(ip)).ToList();
                return publicForwardingIps.Any() ? publicForwardingIps.Last() : userHostAddress;
            }
            catch (Exception)
            {
                return IPAddress.Any.ToString();
            }
        }

        /// <summary>
        /// Check if ip if from private subnet
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsPrivate(string ip)
        {
            return IsPrivate(IPAddress.Parse(ip));
        }

        /// <summary>
        /// Check if ip if from private subnet
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsPrivate(IPAddress ip)
        {
            var octets = ip.GetAddressBytes();

            var is24BitBlock = octets[0] == 10;
            if (is24BitBlock)
            {
                return true;
            }

            var is20BitBlock = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20BitBlock)
            {
                return true;
            }

            var is16BitBlock = octets[0] == 192 && octets[1] == 168;
            if (is16BitBlock)
            {
                return true;
            }

            var isLinkLocalAddress = octets[0] == 169 && octets[1] == 254;
            return isLinkLocalAddress;
        }
    }
}
