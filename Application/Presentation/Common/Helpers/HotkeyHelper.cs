﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    public static class HotkeyHelper
    {
        private const string Separator = "+";
        private static readonly Key[] SpecialKeys = new[] { Key.Alt, Key.Ctrl, Key.Shift };

        private static readonly Dictionary<Key, string> HotkeyNames = new Dictionary<Key, string>
        {
            {Key.Alt, "alt"},
            {Key.Ctrl, "ctrl"},
            {Key.Shift, "shift"},
            {Key.Zero, "0"},
            {Key.One, "1"},
            {Key.Two, "2"},
            {Key.Three, "3"},
            {Key.Four, "4"},
            {Key.Five, "5"},
            {Key.Six, "6"},
            {Key.Seven, "7"},
            {Key.Eight, "8"},
            {Key.Nine, "9"},
            {Key.A, "a"},
            {Key.B, "b"},
            {Key.C, "c"},
            {Key.D, "d"},
            {Key.E, "e"},
            {Key.F, "f"},
            {Key.G, "g"},
            {Key.H, "h"},
            {Key.I, "i"},
            {Key.J, "j"},
            {Key.K, "k"},
            {Key.L, "l"},
            {Key.M, "m"},
            {Key.N, "n"},
            {Key.O, "o"},
            {Key.P, "p"},
            {Key.Q, "q"},
            {Key.R, "r"},
            {Key.S, "s"},
            {Key.T, "t"},
            {Key.U, "u"},
            {Key.V, "v"},
            {Key.W, "w"},
            {Key.X, "x"},
            {Key.Y, "y"},
            {Key.Z, "z"},
            {Key.Backspace, "backspace"},
            {Key.Tab, "tab"},
            {Key.Enter, "return"},
            {Key.Esc, "esc"},
            {Key.Space, "space"},
            {Key.PageUp, "pageup"},
            {Key.PageDown, "pagedown"},
            {Key.End, "end"},
            {Key.Home, "home"},
            {Key.Left, "left"},
            {Key.Up, "up"},
            {Key.Right, "right"},
            {Key.Down, "down"},
            {Key.Insert, "insert"},
            {Key.Del, "del"},
            {Key.Pause, "pause"},
            {Key.CapsLock, "capslock"},
            {Key.NumLock, "numlock"},
            {Key.Scroll, "scroll"},
            {Key.F1, "f1"},
            {Key.F2, "f2"},
            {Key.F3, "f3"},
            {Key.F4, "f4"},
            {Key.F5, "f5"},
            {Key.F6, "f6"},
            {Key.F7, "f7"},
            {Key.F8, "f8"},
            {Key.F9, "f9"},
            {Key.F10, "f10"},
            {Key.F11, "f11"},
            {Key.F12, "f12"},
            {Key.Tilde, "~"},
            {Key.Tick, "`"},
            {Key.ExclamationPoint, "!"},
            {Key.At, "@"},
            {Key.Hash, "#"},
            {Key.Dollar, "$"},
            {Key.Percent, "%"},
            {Key.Caret, "^"},
            {Key.Ampersand, "&"},
            {Key.Asterisk, "*"},
            {Key.OpeningRoundBracket, "("},
            {Key.ClosingRoundBracket, ")"},
            {Key.Minus, "-"},
            {Key.Equals, "="},
            {Key.Underscore, "_"},
            {Key.Plus, "+"},
            {Key.OpeningSquareBracket, "["},
            {Key.ClosingSquareBracket, "]"},
            {Key.Backslash, "\\"},
            {Key.OpeningBrace, "{"},
            {Key.ClosingBrace, "}"},
            {Key.Pipe, "|"},
            {Key.Semicolon, ";"},
            {Key.Apostrophe, "'"},
            {Key.Colon, ":"},
            {Key.Quote, "\""},
            {Key.Dot, "."},
            {Key.Comma, ","},
            {Key.Slash, "/"},
            {Key.LessThan, "<"},
            {Key.MoreThan, ">"},
            {Key.QuestionMark, "?"},
        };

        public static string GetHotkey([NotNull] params Key[] keys)
        {
            if (keys == null)
            {
                throw new ArgumentNullException(nameof(keys));
            }

            var hotKeys = new List<Key>();
            var inputKeys = new HashSet<Key>(keys);

            foreach (var key in SpecialKeys)
            {
                if (inputKeys.Contains(key))
                {
                    inputKeys.Remove(key);
                    hotKeys.Add(key);
                }
            }

            hotKeys.AddRange(inputKeys);

            return string.Join(Separator, hotKeys.Select(k => HotkeyNames[k]));
        }
    }
}