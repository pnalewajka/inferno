﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Smt.Atomic.Presentation.Common.Helpers
{
    public class TransferUrlHelper<TController> : UrlHelper
        where TController : Controller
    {
        public TransferUrlHelper(RequestContext context)
            : base(new RequestContext(context.HttpContext, GetRouteData<TController>(context)))
        {
        }

        private static RouteData GetRouteData<T>(RequestContext context) where T : Controller
        {
            var data = new RouteData(context.RouteData.Route, context.RouteData.RouteHandler);
            data.Values["controller"] = RoutingHelper.GetControllerName<T>();
            data.Values["area"] = RoutingHelper.GetAreaName<T>();

            return data;
        }
    }
}
