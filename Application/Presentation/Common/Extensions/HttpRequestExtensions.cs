﻿using System;
using System.Linq;
using System.Web;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Extensions
{
    public static class HttpRequestExtensions
    {
        public static ActionType GetActionType(this HttpRequestBase request)
        {
            return (ActionType)Enum.Parse(typeof(ActionType), request.RequestType, true);
        }

        public static bool ContainsQueryParameter(this HttpRequestBase request, string parameterName)
        {
            return request.QueryString.AllKeys.Contains(parameterName);
        }

        public static TEnum GetQueryParameter<TEnum>(this HttpRequestBase request, string key, bool throwExceptionOnMissingEnumField = true)
            where TEnum : struct
        {
            var enumType = typeof(TEnum);

            if (!enumType.IsEnum)
            {
                throw new InvalidCastException();
            }

            var queryParameterValue = request.QueryString[key];
            var enumValue = EnumHelper.GetEnumValueOrDefault<TEnum>(queryParameterValue, NamingConventionHelper.ConvertPascalCaseToHyphenated);

            if (enumValue == null)
            {
                if (throwExceptionOnMissingEnumField)
                {
                    throw new ArgumentOutOfRangeException();
                }

                return default(TEnum);
            }

            return enumValue.Value;
        }
    }
}
