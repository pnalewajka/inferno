﻿using System;

namespace Smt.Atomic.Presentation.Common.Exceptions
{
    public class LengthValidationAttributeMismatchException : Exception
    {
        public LengthValidationAttributeMismatchException() :base (@"Attributes MinLength and MaxLength shouldn't be used along StringLength attribute")
        {
        }
    }
}
