﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Override default decimal precision of 2 digits
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class PrecisionAttribute : Attribute
    {
        public byte Precision { get; private set; }

        /// <summary>
        /// Override decimal precision
        /// </summary>
        /// <param name="precision">How many digits after decimal point are to accepted</param>
        public PrecisionAttribute(byte precision)
        {
            Precision = precision;
        }
    }
}
