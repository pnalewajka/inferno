﻿using System;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Max value validation of field, with localized message
    /// Format localized message with {0} for name,
    /// with {1} for max value
    /// </summary>
    public class MaxValueAttribute : LocalizedValidationAttribute
    {
        private const string DefaultResourceKey = "MaxValueAttributeValidationMessage";
        private static readonly Type DefaultResourceType = typeof (AttributeResources);

        /// <summary>
        /// Provide max value, validation message will be extracted from default resources
        /// </summary>
        /// <param name="maxValue"></param>
        public MaxValueAttribute(long maxValue)
            : this(maxValue, DefaultResourceKey, DefaultResourceType)
        {
        }

        /// <summary>
        /// Provide max value and localized validation message
        /// </summary>
        /// <param name="maxValue"></param>
        /// <param name="resourceKey"></param>
        /// <param name="resourceType"></param>
        public MaxValueAttribute(long maxValue, string resourceKey, Type resourceType) : this(Convert.ToDecimal(maxValue), resourceKey, resourceType)
        {
        }

        /// <summary>
        /// Provide max value, validation message will be extracted from default resources
        /// </summary>
        /// <param name="maxValue"></param>
        public MaxValueAttribute(decimal maxValue)
            : this(maxValue, DefaultResourceKey, DefaultResourceType)
        {
        }

        /// <summary>
        /// Provide max value and localized validation message
        /// </summary>
        /// <param name="maxValue"></param>
        /// <param name="resourceKey"></param>
        /// <param name="resourceType"></param>
        public MaxValueAttribute(decimal maxValue, string resourceKey, Type resourceType) : base(resourceKey, resourceType)
        {
            MaxValue = maxValue;
        }

        /// <summary>
        /// Max value
        /// </summary>
        public decimal MaxValue { get; private set; }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            decimal decimalValue;

            if (decimal.TryParse(value.ToString(), out decimalValue))
            {
                return decimalValue <= MaxValue;
            }

            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Description, name, MaxValue);
        }

    }
}