﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Base class for validation which are performed only for a given form type
    /// </summary>
    public class ConditionalValidationAttribute : ValidationAttribute
    {
        /// <summary>
        /// Context when the field should be validated
        /// </summary>
        public FormType FormType { get; private set; }

        public ConditionalValidationAttribute(FormType formType = FormType.Always)
        {
            FormType = formType;
        }

        public ConditionalValidationAttribute(FormType formType, Func<string> errorMessageAccessor) : base(errorMessageAccessor)
        {
            FormType = formType;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (ViewModelHelper.IsSuitableFor(validationContext.ObjectInstance, FormType))
            {
                return base.IsValid(value, validationContext);
            }

            return ValidationResult.Success;
        }
    }
}