﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public abstract class LocalizedStringValidationAttribute : LocalizedValidationAttribute
    {
        public LocalizedStringValidationAttribute(string resourceKey, Type resourceType) : base(resourceKey, resourceType)
        { }

        protected bool IsValid(object value, Func<string, bool> predicate)
        {
            var localizedString = value as LocalizedStringBase;

            if (localizedString == null)
            {
                return false;
            }

            return GetAllStringValues(localizedString).All(x => predicate(x));
        }

        private IEnumerable<string> GetAllStringValues(LocalizedStringBase value)
        {
            return TypeHelper.GetPublicInstancePropertiesOfType<string>(value.GetType())
                             .Select(x => (string)x.GetValue(value));
        }
    }
}
