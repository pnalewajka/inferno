﻿using System;
using System.Linq;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class AtomicAuthorizeAttribute : AuthorizeAttribute
    {
        public new SecurityRoleType[] Roles { get; protected set; }

        public AtomicAuthorizeAttribute()
        {
            Roles = new SecurityRoleType[] { };
        }

        public AtomicAuthorizeAttribute(params SecurityRoleType[] roles)
        {
            if (roles != null && roles.Length > 0)
            {
                base.Roles = string.Join(",", roles.Select(a => a.ToString()));
            }

            Roles = roles ?? new SecurityRoleType[] { };
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var isUserAuthenticated = filterContext.HttpContext.User.Identity.IsAuthenticated;
            var isAjaxRequest = filterContext.HttpContext.Request.IsAjaxRequest();
            
            if (isAjaxRequest || isUserAuthenticated)
            {
                var requiredRoles = string.Join(", ", Roles);

                var messageFormat = Roles.Length > 1
                    ? AttributeResources.SomeOfRolesAreMissing
                    : AttributeResources.OneRoleMissing;

                string message = string.Format(messageFormat, requiredRoles);

                throw new BusinessException(message);
            }
            
            //for non ajax call and non-authorized user we allow program to redirect to login screen
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
