﻿using System.IO;
using System.Web.Mvc;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();
            string filePath = ((FilePathResult)filterContext.Result).FileName;
            File.Delete(filePath);
        }
    }
}