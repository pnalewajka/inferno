﻿using System;
using System.Collections;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CannotBeEmptyAttribute : ConditionalValidationAttribute, IAtomicClientValidatable
    {
        /// <summary>
        /// Initializes a new instance of the CannotBeEmptyAttribute class.
        /// </summary>
        public CannotBeEmptyAttribute(FormType formType = FormType.Always)
            : base(formType, () => AttributeResources.CannotBeEmptyMessage)
        {
        }

        public override bool IsValid(object value)
        {
            var list = value as IEnumerable;
            return list != null && list.GetEnumerator().MoveNext();
        }

        public void SetValidationAttributes(TagAttributes attributes, string fieldName)
        {
            const string validationTagName = "data-val-cannotbeempty";

            attributes.Add(validationTagName, FormatErrorMessage(fieldName));
        }
    }
}