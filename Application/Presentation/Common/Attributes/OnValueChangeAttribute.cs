﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Allows to perform actions both on client and server when field value changes
    /// </summary>
    public class OnValueChangeAttribute : Attribute
    {
        /// <summary>
        /// Javascript body or function name
        /// </summary>
        public string ClientHandler { get; set; }

        /// <summary>
        /// Server action url (can be virtual "~/Test/MyTest")
        /// </summary>
        public string ServerHandler { get; set; }

        /// <summary>
        /// Javascript body or function name
        /// </summary>
        public string ServerResultClientHandler { get; set; }

        /// <summary>
        /// Will be executed on page load
        /// </summary>
        public bool ExecuteOnLoad { get; set; }
    }
}
