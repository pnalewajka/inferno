﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Enables model binding for polymorphic properties
    /// </summary>
    /// <example>
    /// For getting <see cref="Type"/> we can use property:
    /// <code>
    /// public Type ModelType { get; set; }
    /// 
    /// [TypeResolution(IndicatorName: nameof(ModelType))]
    /// public object Model { get; set; }
    /// </code>
    /// or method:
    /// <code>
    /// [TypeResolution(IndicatorName: nameof(GetModelType))]
    /// public object Model { get; set; }
    /// 
    /// public Type GetModelType()
    /// {
    ///     var type = typeof(SomeType);
    ///     
    ///     return type;
    /// }
    /// </code>
    /// </example>
    /// <remarks>
    /// Polymorphic property should be declared after its <see cref="Type"/> property
    /// or after all properties used by its <see cref="Type"/> method
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public class TypeResolutionAttribute : Attribute
    {
        private string _indicatorName;

        /// <summary>
        /// Enable polymorphic property model binding
        /// </summary>
        /// <param name="IndicatorName">Name of property or method returning <see cref="Type"/> of attributed property</param>
        public TypeResolutionAttribute(string IndicatorName)
        {
            _indicatorName = IndicatorName;
        }

        /// <summary>
        /// Get <see cref="Type"/> of attributed polymorphic property
        /// </summary>
        /// <param name="containingObject">Object containing both attributed property and property/method for getting its <see cref="Type"/></param>
        /// <returns><see cref="Type"/> of attributed property</returns>
        public Type GetPropertyType(object containingObject)
        {
            var returnType = typeof(Type);

            var typeProperty = containingObject.GetType().GetProperty(_indicatorName, returnType);

            if (typeProperty != null)
            {
                return (Type)typeProperty.GetValue(containingObject);
            }

            var typeMethod = containingObject.GetType().GetMethod(_indicatorName, Type.EmptyTypes);

            if (typeMethod != null && typeMethod.ReturnType == returnType)
            {
                return (Type)typeMethod.Invoke(containingObject, new object[0]);
            }

            throw new ArgumentException("Object does not contain specified property or method", nameof(containingObject));
        }
    }
}
