﻿using System;
using Smt.Atomic.Presentation.Common.Enums;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredWhenAttribute : ConditionalValidationAttribute
    {
        /// <summary>
        /// Gets or sets a value that indicates whether an empty string is allowed.
        /// </summary>
        /// 
        /// <returns>
        /// true if an empty string is allowed; otherwise, false. The default value is false.
        /// </returns>
        public bool AllowEmptyStrings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.ComponentModel.DataAnnotations.RequiredAttribute"/> class.
        /// </summary>
        /// <param name="formType">Context when the given field is required</param>
        public RequiredWhenAttribute(FormType formType)
            : base(formType, () => AttributeResources.RequiredWhenAttributeMessage)
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            var valueAsString = value as string;

            if (valueAsString != null && !AllowEmptyStrings)
            {
                return valueAsString.Trim().Length != 0;
            }

            return true;
        }
    }
}