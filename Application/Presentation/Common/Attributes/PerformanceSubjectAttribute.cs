﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Attribute used to mark controllers / actions to be subjects of a performance test run
    /// Both controllers and actions can have multiple performance test PerformanceAttributes declared
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public class PerformanceTestAttribute : Attribute
    {
        /// <summary>
        /// If attribute is used on a method (action) this value will be ignored if provided.
        /// If attribute is used on a controller level u can specify action name to run performance check on; every action requires new attribute. Example: 
        /// [PerformanceSubjectAtttribute("List")]
        /// [PerformanceSubjectAtttribute("Search")]
        /// public class SomeController...
        public string Action { get; }

        /// <summary>
        /// Request params to use
        /// </summary>
        public string Parameters { get; }

        /// <summary>
        /// Value in ms for request to timeout (1000ms = 1sec)
        /// </summary>
        public int Timeout { get; } = 3000;

        public PerformanceTestAttribute(string action = null) {
            Action = action;
        }

        public PerformanceTestAttribute(string parameters, string action = null)
        {
            Parameters = parameters;
        }

        public PerformanceTestAttribute(int timeout, string actions = null)
        {
            Timeout = timeout;
        }

        public PerformanceTestAttribute(int timeout, string parameters, string action = null)
        {
            Parameters = parameters;
            Timeout = timeout;
        }
    }
}