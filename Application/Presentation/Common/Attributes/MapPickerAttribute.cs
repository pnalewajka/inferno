﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Map picker customizations
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MapPickerAttribute : Attribute
    {

        /// <summary>
        /// Indicates if picker displays map in place rather than showing map picker in popup
        /// </summary>
        public bool IsInPlaceEditor { get; set; }

        /// <summary>
        /// Map height (editor or popup)
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Is name of location hidden? (editor)
        /// </summary>
        public bool EditorIsLocationNameHidden { get; set; }

        /// <summary>
        /// Is radius hidden? (editor)
        /// </summary>
        public bool EditorIsRadiusHidden { get; set; }

        /// <summary>
        /// Is latitude and longitude hidden? (editor)
        /// </summary>
        public bool EditorIsLatLngHidden { get; set; }

        /// <summary>
        /// Is search box hidden ? - Will be used with conjunction with parameters from config  (editor)
        /// </summary>
        public bool EditorIsSearchHidden { get; set; }

        /// <summary>
        /// Is name of location hidden? (popup)
        /// </summary>
        public bool PopupEditorIsLocationNameHidden { get; set; }

        /// <summary>
        /// Is radius hidden? (popup)
        /// </summary>
        public bool PopupEditorIsRadiusHidden { get; set; }

        /// <summary>
        /// Is latitude and longitude hidden? (popup)
        /// </summary>
        public bool PopupEditorIsLatLngHidden { get; set; }

        /// <summary>
        /// Is search box hidden ? - Will be used with conjunction with parameters from config  (popup)
        /// </summary>
        public bool PopupEditorIsSearchHidden { get; set; }

        /// <summary>
        /// Popup title
        /// </summary>
        public string PopupTitle { get; set; }

        public MapPickerAttribute()
        {
            IsInPlaceEditor = true;
        }
    }
}
