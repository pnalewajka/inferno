using System;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Describes the tab which will be rendered for a given view model 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class TabDescriptionAttribute : Attribute
    {
        private readonly Type _resourceType;

        /// <summary>
        /// Tab identifier
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// Tab name
        /// </summary>
        public string TabName { get; private set; }

        /// <summary>
        /// Order of the tab
        /// </summary>
        public long Order { get; private set; }

        /// <summary>
        /// Role required for read from properties in this tab
        /// </summary>
        public SecurityRoleType? RoleForRead { get; private set; }

        /// <summary>
        /// Role required for write to properties in this tab
        /// </summary>
        public SecurityRoleType? RoleForWrite { get; private set; }

        public TabDescriptionAttribute(long order, string id, string tabNamePropertyName)
            : this(order, id, tabNamePropertyName, null, null, null)
        {
        }

        public TabDescriptionAttribute(long order, string id, string tabNamePropertyName, SecurityRoleType securityRole)
            : this(order, id, tabNamePropertyName, securityRole, securityRole, null)
        {
        }

        public TabDescriptionAttribute(long order, string id, string tabNameResourceKey, Type resourceType)
            : this(order, id, tabNameResourceKey, null, null, resourceType)
        {
        }

        public TabDescriptionAttribute(
            long order,
            string id,
            string tabNameResourceKey,
            SecurityRoleType requiredRole,
            Type resourceType)
            : this(order, id, tabNameResourceKey, requiredRole, requiredRole, resourceType)
        {
        }

        public TabDescriptionAttribute(
            long order,
            string id,
            string tabNameResourceKey,
            SecurityRoleType roleForRead,
            SecurityRoleType roleForWrite,
            Type resourceType)
            : this(order, id, tabNameResourceKey, (SecurityRoleType?)roleForRead, roleForWrite, resourceType)
        {
        }

        private TabDescriptionAttribute(
            long order,
            string id,
            string tabName,
            SecurityRoleType? roleForRead,
            SecurityRoleType? roleForWrite,
            Type resourceType)
        {
            Id = id;
            TabName = tabName;
            Order = order;
            RoleForRead = roleForRead;
            RoleForWrite = roleForWrite;
            _resourceType = resourceType;
        }

        public string GetLocalizedTabName(object formModel)
        {
            if (_resourceType != null)
            {
                var resourceManager = ResourceManagerHelper.GetResourceManager(_resourceType);

                return resourceManager.GetString(TabName);
            }

            return PropertyHelper.GetPropertyValue<string>(formModel, TabName);
        }
    }
}