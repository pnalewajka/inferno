using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TabAttribute : Attribute
    {
        public string TabId { get; }

        public TabAttribute(string tabId)
        {
            TabId = tabId;
        }
    }
}