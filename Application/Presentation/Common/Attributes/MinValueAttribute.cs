﻿using System;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Min value validation of field, with localized message
    /// Format localized message with {0} for name,
    /// with {1} for min value
    /// </summary>
    public class MinValueAttribute : LocalizedValidationAttribute
    {
        private const string DefaultResourceKey = "MinValueAttributeValidationMessage";
        private static readonly Type DefaultResourceType = typeof (AttributeResources);

        /// <summary>
        /// Provide min value, validation message will be extracted from default resources
        /// </summary>
        /// <param name="minValue"></param>
        public MinValueAttribute(long minValue)
            : this(minValue, DefaultResourceKey, DefaultResourceType)
        {
        }

        /// <summary>
        /// Provide min value and localized validation message
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="resourceKey"></param>
        /// <param name="resourceType"></param>
        public MinValueAttribute(long minValue, string resourceKey, Type resourceType)
            : this(Convert.ToDecimal(minValue), resourceKey, resourceType)
        {
        }

        /// <summary>
        /// Provide min value, validation message will be extracted from default resources
        /// </summary>
        /// <param name="minValue"></param>
        public MinValueAttribute(decimal minValue)
            : this(minValue, DefaultResourceKey, DefaultResourceType)
        {
        }

        /// <summary>
        /// Provide min value and localized validation message
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="resourceKey"></param>
        /// <param name="resourceType"></param>
        public MinValueAttribute(decimal minValue, string resourceKey, Type resourceType)
            : base(resourceKey, resourceType)
        {
            MinValue = minValue;
        }


        /// <summary>
        /// Min value
        /// </summary>
        public decimal MinValue { get; private set; }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            decimal decimalValue;

            if (decimal.TryParse(value.ToString(), out decimalValue))
            {
                return decimalValue >= MinValue;
            }

            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Description, name, MinValue);
        }

    }
}