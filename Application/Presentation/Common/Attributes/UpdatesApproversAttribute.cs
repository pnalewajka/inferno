﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public class UpdatesApproversAttribute : OnValueChangeAttribute
    {
        public UpdatesApproversAttribute()
        {
            ServerHandler = "/Workflows/Request/GetApprovers";
            ServerResultClientHandler = "WorkflowRequests.updateApprovers";
        }
    }
}
