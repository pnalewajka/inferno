﻿using System.Web.Mvc;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public class CustomRequiredAttributeAdapter : RequiredAttributeAdapter
    {
        public CustomRequiredAttributeAdapter(ModelMetadata metadata, ControllerContext context, System.ComponentModel.DataAnnotations.RequiredAttribute attribute)
            : base(metadata, context, attribute)
        {
            attribute.ErrorMessageResourceType = typeof(AttributeResources);
            attribute.ErrorMessageResourceName = "RequiredWhenAttributeMessage";
        }
    }
}
