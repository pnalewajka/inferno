﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DocumentUploadAttribute : Attribute
    {
        /// <summary>
        /// Type used for resolving entity to document mapping
        /// </summary>
        public Type DocumentMappingType { get; private set; }

        /// <summary>
        /// List of comma separated mime types (wildcards available) for 'accept' attribute of input file
        /// example: image/*
        /// image/png,image/jpg
        /// </summary>
        public string AcceptableContentTypes { get; private set; }

        public string DownloadUrlTemplate { get; set; }

        public bool ShouldUseDownloadDialog { get; set; }

        public DocumentUploadAttribute()
        {            
        }

        public DocumentUploadAttribute(Type documentMappingType) : this()
        {
            DocumentMappingType = documentMappingType;
        }

        public DocumentUploadAttribute(Type documentMappingType, params string[] acceptableContentTypes) : this()
        {
            DocumentMappingType = documentMappingType;
            AcceptableContentTypes = string.Join(",", acceptableContentTypes ?? new string[0]);
        }
    }
}
