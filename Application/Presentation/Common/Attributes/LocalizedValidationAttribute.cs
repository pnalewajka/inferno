﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Base abstraction for 
    /// </summary>
    public abstract class LocalizedValidationAttribute : ValidationAttribute
    {
        private readonly string _resouceKey;
        private readonly Type _resourceType;

        protected LocalizedValidationAttribute(string resourceKey, Type resourceType)
        {
            _resouceKey = resourceKey;
            _resourceType = resourceType;
        }

        /// <summary>
        /// Localized description
        /// </summary>
        public string Description
        {
            get
            {
                if (_resourceType != null)
                {
                    var resourceManager = ResourceManagerHelper.GetResourceManager(_resourceType);
                    return resourceManager.GetString(_resouceKey);
                }

                return _resouceKey;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Description, name);
        }
    }
}