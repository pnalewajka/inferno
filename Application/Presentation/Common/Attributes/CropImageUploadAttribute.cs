﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Customize uploading and croping images
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CropImageUploadAttribute : DocumentUploadAttribute
    {
        private const string AllImagesTypes = "image/*";

        /// <summary>
        /// Image minimum crop width
        /// </summary>
        public long MinWidth { get; set; }

        /// <summary>
        /// Image maximum crop width
        /// </summary>
        public long MaxWidth { get; set; }

        /// <summary>
        /// Image minimum crop height
        /// </summary>
        public long MinHeight { get; set; }

        /// <summary>
        /// Image maximum crop height
        /// </summary>
        public long MaxHeight { get; set; }

        /// <summary>
        /// Image crop aspect ratio is to be maintained?
        /// </summary>
        public bool ShouldPreserveAspectRatio { get; set; }

        public CropImageUploadAttribute(Type documentMappingType, long minWidth, long maxWidth, long minHeight, long maxHeight, bool shouldPreserveAspectRatio, string acceptableContentTypes = AllImagesTypes)
            : base(documentMappingType, acceptableContentTypes)
        {
            MinWidth = minWidth;
            MaxWidth = maxWidth;
            MinHeight = minHeight;
            MaxHeight = maxHeight;
            ShouldPreserveAspectRatio = shouldPreserveAspectRatio;
        }
    }
}
