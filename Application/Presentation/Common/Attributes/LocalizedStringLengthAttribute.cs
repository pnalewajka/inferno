﻿using System;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public class LocalizedStringLengthAttribute : LocalizedStringValidationAttribute
    {
        private const string DefaultResourceKey = "StringLengthAttributeValidationMessage";
        private static readonly Type DefaultResourceType = typeof(AttributeResources);

        public int MaxLength { get; private set; }

        public LocalizedStringLengthAttribute(int maxLength)
            : this(maxLength, DefaultResourceKey, DefaultResourceType)
        {
        }

        public LocalizedStringLengthAttribute(int maxLength, string resourceKey, Type resourceType) : base(resourceKey, resourceType)
        {
            MaxLength = maxLength;
        }

        public override bool IsValid(object value)
        {
            return IsValid(value, x => string.IsNullOrEmpty(x) || x.Length <= MaxLength);
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Description, name, MaxLength);
        }
    }
}