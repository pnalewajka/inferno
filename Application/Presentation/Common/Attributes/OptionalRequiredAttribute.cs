﻿using System;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public class OptionalRequiredAttribute : ValidationAttribute
    {
        private const string DefaultResourceKey = nameof(AttributeResources.RequiredWhenAttributeMessage);
        private static readonly Type DefaultResourceType = typeof(AttributeResources);

        public OptionalRequiredAttribute()
            : this(DefaultResourceKey, DefaultResourceType)
        {
        }

        public OptionalRequiredAttribute(string resourceKey, Type resourceType)
        {
            ErrorMessageResourceName = resourceKey;
            ErrorMessageResourceType = resourceType;
        }

        public override bool IsValid(object value)
        {
            var valueType = value.GetType();

            if (!valueType.IsGenericType || valueType.GetGenericTypeDefinition() != typeof(Optional<>))
            {
                return false;
            }

            return !((IOption)value).IsSet || valueType.GetProperty("Value").GetValue(value, null) != null;
        }
    }
}
