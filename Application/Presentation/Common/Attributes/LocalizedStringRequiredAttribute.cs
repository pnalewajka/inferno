﻿using System;
using Smt.Atomic.Presentation.Common.Resources;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    public class LocalizedStringRequiredAttribute : LocalizedStringValidationAttribute
    {
        private const string DefaultResourceKey = "RequiredWhenAttributeMessage";
        private static readonly Type DefaultResourceType = typeof(AttributeResources);

        public LocalizedStringRequiredAttribute()
            : this(DefaultResourceKey, DefaultResourceType)
        {
        }

        public LocalizedStringRequiredAttribute(string resourceKey, Type resourceType) : base(resourceKey, resourceType)
        {
        }

        public override bool IsValid(object value)
        {
            return IsValid(value, x => !string.IsNullOrWhiteSpace(x));
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Description, name);
        }
    }
}
