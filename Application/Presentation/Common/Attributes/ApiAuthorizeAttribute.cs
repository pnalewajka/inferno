using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true)]
    public class ApiAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        public new SecurityRoleType[] Roles { get; protected set; }

        public ApiAuthorizeAttribute()
        {
            Roles = new SecurityRoleType[] { };
        }

        public ApiAuthorizeAttribute(params SecurityRoleType[] roles)
        {
            if (roles != null && roles.Length > 0)
            {
                base.Roles = string.Join(",", roles.Select(a => a.ToString()));
            }

            Roles = roles ?? new SecurityRoleType[] { };
        }
    }
}