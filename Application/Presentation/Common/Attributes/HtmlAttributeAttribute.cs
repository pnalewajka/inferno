﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Sets custom html attribute for an input of a given control
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class HtmlAttributeAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Value { get; private set; }

        public HtmlAttributeAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}