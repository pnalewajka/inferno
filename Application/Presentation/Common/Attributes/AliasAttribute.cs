﻿using System;

namespace Smt.Atomic.Presentation.Common.Attributes
{
    /// <summary>
    /// Attribute used to alias properties for view models
    /// E.g., having property named "property" with [Alias("alias")] we can bind property value from request using either of two names: "property" or "alias"
    /// Single property can have multiple aliases, all of them are supported by binder
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class AliasAttribute : Attribute
    {
        /// <summary>
        /// Property name alias
        /// </summary>
        public string Alias { get; private set; }

        public AliasAttribute(string alias)
        {
            Alias = alias;
        }
    }
}