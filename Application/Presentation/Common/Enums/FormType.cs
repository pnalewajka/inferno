﻿using System;

namespace Smt.Atomic.Presentation.Common.Enums
{
    [Flags]
    public enum FormType
    {
        None = 0,

        AddForm = 1 << 0,
        EditForm = 1 << 1,

        Always = AddForm | EditForm
    }
}