﻿namespace Smt.Atomic.Presentation.Common.Enums
{
    public enum ActionType
    {
        Get,
        Post,
        Head,
        Put,
        Delete,
        Connect,
        Options,
        Trace,
        Patch
    }
}
