namespace Smt.Atomic.Presentation.Common.Enums
{
    /// <summary>
    /// Tag attribute conflict resolve hint
    /// </summary>
    public enum TagAttributeDuplicateBehavior
    {
        /// <summary>
        /// Throw exception is element already exists. This is default behavior
        /// </summary>
        ThrowException,
        /// <summary>
        /// If element already exists, replace its value with new one
        /// </summary>
        Replace,
        /// <summary>
        /// If element already exists, extend its value by adding new one, prefixed with space at the end of current value
        /// </summary>
        Merge
    }
}