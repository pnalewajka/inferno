﻿namespace Smt.Atomic.Presentation.Common.Enums
{
    // Must be identical to enum in ClientSideOperations.ts
    public enum ClientSideOperationType
    {
        ValueChange = 1,
        Visibility = 2,
        Editable = 3,
        ValuePicker = 4,
        CustomAction = 5,
        MultiValuePicker = 6,
    }
}
