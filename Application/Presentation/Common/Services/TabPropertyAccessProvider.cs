﻿using System.Linq;
using System.Reflection;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.Presentation.Common.Services
{
    class TabPropertyAccessProvider : IPropertyAccessProvider
    {
        public int Priority => int.MaxValue;

        public AccessRoles GetAccessRoles(MemberInfo memberInfo)
        {
            var result = new AccessRoles();

            var tabAttribute = AttributeHelper.GetMemberAttribute<TabAttribute>(memberInfo);

            if (tabAttribute != null)
            {
                var tabDescription = AttributeHelper
                    .GetClassAttributes<TabDescriptionAttribute>(memberInfo.DeclaringType)
                    .FirstOrDefault(t => t.Id == tabAttribute.TabId);

                if (tabDescription != null)
                {
                    result.RoleForRead = tabDescription.RoleForRead;
                    result.RoleForWrite = tabDescription.RoleForWrite;
                }
            }

            return result;
        }
    }
}
