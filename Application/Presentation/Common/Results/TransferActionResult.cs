﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.Common.Results
{
    public class TransferActionResult : ActionResult
    {
        private readonly Type _controllerType;
        private readonly string _actionName;
        private readonly IDictionary<string, object> _extraParameters;

        public TransferActionResult(Type controllerType, string actionName)
            : this(controllerType, actionName, new Dictionary<string, object>())
        {
        }

        public TransferActionResult(Type controllerType, string actionName, IDictionary<string, object> extraParameters)
        {
            _controllerType = controllerType;
            _actionName = actionName;
            _extraParameters = extraParameters;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var controllerFactory = ControllerBuilder.Current.GetControllerFactory();
            var controllerName = RoutingHelper.GetControllerName(_controllerType);
            var controller = controllerFactory.CreateController(context.RequestContext, controllerName);

            var request = new RequestContext(context.HttpContext, context.RouteData);

            // Add custom parameters
            foreach (var parameter in _extraParameters)
            {
                request.RouteData.Values[parameter.Key] = parameter.Value;
            }

            // Rewrite route to use new controller
            request.RouteData.Values["controller"] = controllerName;
            request.RouteData.Values["action"] = _actionName;

            // Make it flow again (avoids the need to manually call MVC events)
            controller.Execute(request);
        }
    }
}
