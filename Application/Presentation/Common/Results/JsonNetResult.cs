﻿using System;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Presentation.Common.Results
{
    public class JsonNetResult : ActionResult
    {
        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }

        public JsonSerializerSettings SerializerSettings { get; set; }
        public JsonRequestBehavior JsonRequestBehavior { get; set; }

        public Formatting Formatting { get; set; }

        public JsonNetResult(object data, Formatting formatting) : this(data)
        {
            Formatting = formatting;
        }

        public JsonNetResult(object data) : this()
        {
            Data = data;
        }

        public JsonNetResult(string rawJson) : this()
        {
            Data = rawJson;
        }

        public JsonNetResult()
        {
            Formatting = Formatting.None;
            SerializerSettings = new JsonSerializerSettings();
            JsonRequestBehavior = JsonRequestBehavior.DenyGet;
        }

        public override void ExecuteResult([NotNull] ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
                ? ContentType
                : "application/json";

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data == null)
            {
                return;
            }

            using (var writer = new JsonTextWriter(response.Output) {Formatting = Formatting})
            {
                if (Data.GetType() == typeof(string))
                {
                    writer.WriteRaw(Data.ToString());
                }
                else
                {
                    var serializer = JsonSerializer.Create(SerializerSettings);
                    serializer.Serialize(writer, Data);
                }

                writer.Flush();
            }
        }
    }
}
