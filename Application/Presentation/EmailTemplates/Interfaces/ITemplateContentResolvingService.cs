﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.EmailTemplates.Dto;

namespace Smt.Atomic.Presentation.EmailTemplates.Interfaces
{
    public interface ITemplateContentResolvingService
    {
        IEnumerable<TemplateDefinitionDto> GetTemplateDefinitions();
    }
}
