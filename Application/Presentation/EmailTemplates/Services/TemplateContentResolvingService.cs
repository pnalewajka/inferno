﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.EmailTemplates.Dto;
using Smt.Atomic.Presentation.EmailTemplates.Interfaces;

namespace Smt.Atomic.Presentation.EmailTemplates.Services
{
    internal class TemplateContentResolvingService : ITemplateContentResolvingService
    {
        private const string templateLocation = "Smt.Atomic.Presentation.EmailTemplates.Templates.";

        public IEnumerable<TemplateDefinitionDto> GetTemplateDefinitions()
        {
            var assembly = this.GetType().Assembly;

            foreach(var fieldInfo in typeof(TemplateCodes).GetFields())
            {
                var templateDefinition = (TemplateDefinitionAttribute)fieldInfo.GetCustomAttributes(typeof(TemplateDefinitionAttribute), false).Single();

                using (var contentStream = assembly.GetManifestResourceStream(templateLocation + templateDefinition.TemplateLocation))
                {
                    string content = StreamHelper.StreamToString(contentStream, Encoding.UTF8);

                    yield return new TemplateDefinitionDto(
                        (string)fieldInfo.GetValue(null),
                        content,
                        templateDefinition.Description,
                        templateDefinition.ContentType);
                }
            }
        }
    }
}
