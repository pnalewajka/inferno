﻿using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.EmailTemplates.Helpers
{
    public class EmailTemplateHelper
    {
        public static string Shared(string resource)
        {
            var assembly = typeof(ServiceInstaller).Assembly;
            var resourceFullName = $"Smt.Atomic.Presentation.EmailTemplates.Templates.Shared.{resource}.html";

            return ResourceHelper.GetResourceAsString(resourceFullName, assembly);
        }

        public static string SharedHead => Shared("Head");
        
        public static string SharedLogo => Shared("Logo");
        
        public static string SharedLegal => Shared("Legal");
        
        public static string SharedSignature => Shared("Signature");
    }
}
