﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Presentation.EmailTemplates.Interfaces;
using Smt.Atomic.Presentation.EmailTemplates.Services;

namespace Smt.Atomic.Presentation.EmailTemplates
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<ITemplateContentResolvingService>().ImplementedBy<TemplateContentResolvingService>().LifestyleTransient());
        }
    }
}
