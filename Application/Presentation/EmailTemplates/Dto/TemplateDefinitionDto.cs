﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.EmailTemplates.Dto
{
    public class TemplateDefinitionDto
    {
        public TemplateDefinitionDto(string templateCode, string content, string description, MessageTemplateContentType contentType)
        {
            TemplateCode = templateCode;
            ContentType = contentType;
            Description = description;
            Content = content;
        }

        public MessageTemplateContentType ContentType { get; private set; }

        public string Description { get; private set; }

        public string Content { get; private set; }

        public string TemplateCode { get; private set; }
    }
}
