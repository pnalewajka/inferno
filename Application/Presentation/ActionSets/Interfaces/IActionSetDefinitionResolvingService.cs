﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.ActionSets.Dto;

namespace Smt.Atomic.Presentation.ActionSets.Interfaces
{
    public interface IActionSetDefinitionResolvingService
    {
        IEnumerable<ActionSetDefinitionDto> GetActionSetDefinitions();
    }
}
