﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Presentation.ActionSets.Interfaces;
using Smt.Atomic.Presentation.ActionSets.Services;

namespace Smt.Atomic.Presentation.ActionSets
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<IActionSetDefinitionResolvingService>().ImplementedBy<ActionSetDefinitionResolvingService>().LifestyleTransient());
        }
    }
}
