﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.ActionSets.Dto;
using Smt.Atomic.Presentation.ActionSets.Interfaces;

namespace Smt.Atomic.Presentation.ActionSets.Services
{
    public class ActionSetDefinitionResolvingService : IActionSetDefinitionResolvingService
    {
        private const string definitionLocation = "Smt.Atomic.Presentation.ActionSets.Definitions";

        public IEnumerable<ActionSetDefinitionDto> GetActionSetDefinitions()
        {
            var assembly = this.GetType().Assembly;
            var actionSetCodes = new ActionSetCodes();

            foreach (var fieldInfo in actionSetCodes.GetType().GetFields())
            {
                var code = (string)fieldInfo.GetValue(actionSetCodes);
                var definition = fieldInfo.GetCustomAttribute<ActionSetDefinitionAttribute>(false);

                using (var contentStream = assembly.GetManifestResourceStream($"{definitionLocation}.{code}.cshtml"))
                {
                    yield return new ActionSetDefinitionDto
                    {
                        Code = code,
                        Description = definition.Description,
                        TriggeringRequestStatuses = definition.TriggeringRequestStatuses,
                        TriggeringRequestType = definition.TriggeringRequestType,
                        Definition = StreamHelper.StreamToString(contentStream, Encoding.UTF8)
                    };
                }
            }
        }
    }
}
