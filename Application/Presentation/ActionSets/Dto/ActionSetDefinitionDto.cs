﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Presentation.ActionSets.Dto
{
    public class ActionSetDefinitionDto
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public RequestType TriggeringRequestType { get; set; }

        public RequestStatus[] TriggeringRequestStatuses { get; set; }

        public string Definition { get; set; }
    }
}
