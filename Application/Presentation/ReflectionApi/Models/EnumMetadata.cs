﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Presentation.ReflectionApi.Models
{
    public class EnumMetadata
    {
        public bool IsFlaggedEnum { get; set; }

        public IReadOnlyCollection<Field> Fields { get; set; }

        public class Field
        {
            public string Code { get; set; }

            public object Value { get; set; }

            public LocalizedString Description { get; set; }
        }
    }
}
