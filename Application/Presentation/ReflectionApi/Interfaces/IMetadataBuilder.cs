﻿using System;

namespace Smt.Atomic.Presentation.ReflectionApi.Interfaces
{
    public interface IMetadataBuilder<TMetadata>
    {
        TMetadata GetMetadata<T>();

        TMetadata GetMetadata(Type type);
    }
}
