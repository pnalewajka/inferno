﻿using System;
using Smt.Atomic.Presentation.ReflectionApi.Interfaces;

namespace Smt.Atomic.Presentation.ReflectionApi.Builders
{
    public abstract class MetadataBuilderBase<TMetadata>
        : IMetadataBuilder<TMetadata>
    {
        public abstract TMetadata GetMetadata(Type type);

        public TMetadata GetMetadata<TModel>()
        {
            return GetMetadata(typeof(TModel));
        }
    }
}
