﻿using System;

namespace Smt.Atomic.Presentation.ReflectionApi.Builders
{
    public class AtomicMetadataBuilder
        : MetadataBuilderBase<object>
    {
        public override object GetMetadata(Type type)
        {
            object metadata = null;

            // add more builders to the chain to extend functionality
            metadata = new EnumMetadataBuilder().GetMetadata(type)
                ?? null;

            return metadata;
        }
    }
}
