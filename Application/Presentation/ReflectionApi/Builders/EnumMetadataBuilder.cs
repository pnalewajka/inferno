﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.ReflectionApi.Models;

namespace Smt.Atomic.Presentation.ReflectionApi.Builders
{
    public class EnumMetadataBuilder
        : MetadataBuilderBase<EnumMetadata>
    {
        public override EnumMetadata GetMetadata(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                return null;
            }

            return new EnumMetadata
            {
                IsFlaggedEnum = TypeHelper.IsFlaggedEnum(enumType),
                Fields = EnumHelper.GetEnumValues(enumType).Select(e => new EnumMetadata.Field
                {
                    Code = e.ToString(),
                    Value = Convert.ChangeType(e, Enum.GetUnderlyingType(e.GetType())),
                    Description = e.GetLocalizedDescription(),
                }).ToList(),
            };
        }
    }
}
