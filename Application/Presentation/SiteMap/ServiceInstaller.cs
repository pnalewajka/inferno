﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Presentation.SiteMap.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Services;

namespace Smt.Atomic.Presentation.SiteMap
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register
                    (
                        Component.For<ISiteMapService>().ImplementedBy<SiteMapService>().LifestyleSingleton()
                    );
                    break;
            }
        }
    }
}
