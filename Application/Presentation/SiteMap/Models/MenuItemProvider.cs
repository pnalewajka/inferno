﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.SiteMap.Interfaces;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    public class MenuItemProvider : MenuItem
    {
        [XmlAttribute("provider")]
        public string ProviderIdentifier { get; set; }

        private IMenuItemProviderService _menuItemProviderService;

        private IMenuItemProviderService ProviderService
        {
            get
            {
                if (_menuItemProviderService == null)
                {
                    _menuItemProviderService =
                        ReflectionHelper.CreateInstanceByIdentifier<IMenuItemProviderService>(ProviderIdentifier);
                }

                return _menuItemProviderService;
            }
        }

        public MenuItemProvider()
        {
        }

        protected MenuItemProvider(MenuItemProvider original, IAtomicPrincipal principal)
            : base(original, principal)
        {
            ProviderIdentifier = original.ProviderIdentifier;
            _menuItemProviderService = original._menuItemProviderService;
        }

        internal override MenuItem CloneFor(IAtomicPrincipal principal)
        {
            return new MenuItemProvider(this, principal);
        }

        protected override IEnumerable<MenuItem> GetAvailableItems(IAtomicPrincipal principal)
        {
            if (!IsAvailableTo(principal))
            {
                return Array.Empty<MenuItem>();
            }

            var items = ProviderService
                .GetItems(ActionParameters)
                .Where(i => i.IsAvailableTo(principal))
                .ToList();

            if (!string.IsNullOrEmpty(GroupName))
            {
                items.ForEach(i => i.GroupName = GroupName);
            }

            return items;
        }
    }
}