﻿using System.Diagnostics;
using System.Resources;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Models;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    [DebuggerDisplay("{ToString()}")]
    public class NamedAction : ActionReference
    {
        [XmlAttribute("resourceKey")]
        public string ResourceKey { get; set; }

        [XmlIgnore]
        public string Text { get; protected set; }

        protected NamedAction(NamedAction original)
            : base(original)
        {
            ResourceKey = original.ResourceKey;
        }

        protected NamedAction()
        {
        }

        public string GetTextMarkup()
        {
            return HtmlMarkupHelper.HighlightAccelerator(Text);
        }

        public string GetPlainText()
        {
            return HtmlMarkupHelper.RemoveAccelerator(Text);
        }

        public string GetInputTagAttributes(int? menuPosition = null)
        {
            var tagAttributes = new TagAttributes();

            var accessKey = HtmlMarkupHelper.GetAccelerator(Text);

            if (accessKey != null)
            {
                tagAttributes.Add("accesskey", accessKey);
            }
            else if (menuPosition != null && menuPosition < 10)
            {
                tagAttributes.Add("accesskey", menuPosition);
            }

            return tagAttributes.Render();
        }

        public override string ToString()
        {
            // this is for debugging purposes only
            return StringHelper.Join(" -> ",
                                     (Text ?? ResourceKey),
                                     StringHelper.Join("?",
                                                       StringHelper.Join("/", AreaName, ControllerName, ActionName),
                                                       ActionParameters));
        }

        internal virtual void Localize(ResourceManager resourceManager)
        {
            Text = resourceManager.GetString(ResourceKey);
        }
    }
}