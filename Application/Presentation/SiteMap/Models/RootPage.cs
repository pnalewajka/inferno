﻿namespace Smt.Atomic.Presentation.SiteMap.Models
{
    public class RootPage : NamedAction
    {
        public RootPage()
        {
        }

        internal RootPage(RootPage original)
            : base(original)
        {
        }
    }
}