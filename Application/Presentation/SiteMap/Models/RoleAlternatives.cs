﻿using System;
using System.Linq;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    sealed class RoleAlternatives
    {
        public string[] Roles { get; }

        public RoleAlternatives(string roles)
        {
            Roles = (roles ?? string.Empty)
                .Split(',')
                .Select(r => r.Trim())
                .Where(r => r.Length > 0)
                .ToArray();
        }

        public RoleAlternatives(string[] roles)
        {
            Roles = roles ?? Array.Empty<string>();
        }

        public override bool Equals(object obj)
        {
            var roleAlternatives = obj as RoleAlternatives;

            return roleAlternatives != null && Roles.SequenceEqual(roleAlternatives.Roles);
        }

        public override int GetHashCode()
        {
            return Roles.Aggregate(43, (h, r) => h ^ r.GetHashCode());
        }
    }
}
