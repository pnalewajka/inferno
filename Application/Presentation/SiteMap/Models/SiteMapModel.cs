﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    [XmlRoot("SiteMap")]
    public class SiteMapModel
    {
        private SiteMapModel _originalModel;
        private IAtomicPrincipal _principal;

        public List<LandingPage> LandingPages { get; set; }
        public List<RootPage> RootPages { get; set; }

        public List<MenuItem> Menu { get; set; }

        public SiteMapModel()
        {
            Menu = new List<MenuItem>();

            RootPages = new List<RootPage>();
            LandingPages = new List<LandingPage>();
        }

        protected SiteMapModel(SiteMapModel original, IAtomicPrincipal principal)
        {
            _originalModel = original;
            _principal = principal;

            PopulateFromOriginal(principal);
        }

        public RootPage GetRootPageOrDefault()
        {
            return RootPages.FirstOrDefault();
        }

        public LandingPage GetLandingPageOrDefault(IAtomicPrincipal targetPrincipal = null)
        {
            if (targetPrincipal == null || _principal.Id == targetPrincipal.Id)
            {
                return LandingPages.FirstOrDefault();
            }

            return GetAvailableLandingPagesFromOriginal(targetPrincipal)
                .FirstOrDefault();
        }

        public SiteMapModel GetFilteredCopy(IAtomicPrincipal principal, ResourceManager resourceManager)
        {
            var siteMap = new SiteMapModel(this, principal);

            siteMap.Localize(resourceManager);

            return siteMap;
        }

        public IEnumerable<NamedAction> GetPath([NotNull] string controllerName, [NotNull] string actionName)
        {
            if (controllerName == null)
            {
                throw new ArgumentNullException(nameof(controllerName));
            }

            if (actionName == null)
            {
                throw new ArgumentNullException(nameof(actionName));
            }

            var rootPage = RootPages
                                    .FirstOrDefault(p => p.ActionName == actionName
                                                         && p.ControllerName == controllerName)
                                ?? RootPages.FirstOrDefault();

            if (rootPage != null)
            {
                yield return rootPage;
            }

            var references = GetPath(Menu, controllerName, actionName).ToList();

            if (references.Any())
            {
                foreach (var reference in references)
                {
                    yield return reference;
                }
            }
        }

        internal IEnumerable<ActionReference> GetAllActionReferences()
        {
            return GetAllMenuItems()
                .Union(RootPages)
                .Union(LandingPages);
        }

        private void Localize(ResourceManager resourceManager)
        {
            foreach (var item in Menu)
            {
                item.Localize(resourceManager);
            }

            foreach (var item in RootPages)
            {
                item.Localize(resourceManager);
            }
        }

        private IEnumerable<ActionReference> GetAllMenuItems()
        {
            return Menu.SelectMany(menuItem => menuItem.GetAllMenuItems());
        }

        private static IEnumerable<NamedAction> GetPath(
            [NotNull] IEnumerable<NamedAction> references, 
            [NotNull] string controllerName, 
            [NotNull] string actionName)
        {
            if (references == null)
            {
                throw new ArgumentNullException(nameof(references));
            }

            if (controllerName == null)
            {
                throw new ArgumentNullException(nameof(controllerName));
            }

            if (actionName == null)
            {
                throw new ArgumentNullException(nameof(actionName));
            }

            foreach (var actionReference in references)
            {
                if (actionReference.ActionName == actionName && actionReference.ControllerName == controllerName)
                {
                    yield return actionReference;
                }

                var menuItem = actionReference as MenuItem;

                if (menuItem == null)
                {
                    continue;
                }

                var actionReferences = GetPath(menuItem.Items, controllerName, actionName).ToList();

                if (actionReferences.Any())
                {
                    yield return actionReference;

                    foreach (var reference in actionReferences)
                    {
                        yield return reference;
                    }
                        
                    yield break;
                }
            }
        }

        private void PopulateFromOriginal(IAtomicPrincipal principal)
        {
            Menu = _originalModel
                .Menu
                .Where(m => m.IsAvailableTo(principal))
                .Select(i => i.CloneFor(principal))
                .Where(i => !i.IsEmpty)
                .ToList();

            RootPages = _originalModel
                .RootPages
                .Where(m => m.IsAvailableTo(principal))
                .Select(p => new RootPage(p))
                .ToList();

            LandingPages = GetAvailableLandingPagesFromOriginal(principal)
                .Select(p => new LandingPage(p))
                .ToList();
        }

        private IEnumerable<LandingPage> GetAvailableLandingPagesFromOriginal(IAtomicPrincipal principal)
        {
            return _originalModel
                .LandingPages
                .Where(m => m.IsAvailableTo(principal));
        }
    }
}
