using System.Resources;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    public class LocalizedMenuItem : MenuItem
    {
        public string Path { get; protected set; }

        public LocalizedMenuItem(string text)
        {
            Text = text;
        }

        public LocalizedMenuItem(string text, string path)
            : this(text)
        {
            Path = path;
        }

        private LocalizedMenuItem(LocalizedMenuItem original, IAtomicPrincipal principal)
            : base(original, principal)
        {
            Text = original.Text;
        }

        internal override MenuItem CloneFor(IAtomicPrincipal principal)
        {
            return new LocalizedMenuItem(this, principal);
        }

        internal override void Localize(ResourceManager resourceManager)
        {
        }
    }
}