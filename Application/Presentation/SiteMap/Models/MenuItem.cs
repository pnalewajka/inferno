﻿using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    public class MenuItem : NamedAction
    {
        [XmlElement("MenuItem", typeof(MenuItem))]
        [XmlElement("MenuItemProvider", typeof(MenuItemProvider))]
        public List<MenuItem> Items { get; set; }

        [XmlAttribute("group")]
        public string GroupName { get; set; }

        public MenuItem()
        {
            Items = new List<MenuItem>();
        }

        protected MenuItem(MenuItem original, IAtomicPrincipal principal)
            : base(original)
        {
            GroupName = original.GroupName;

            Items = new List<MenuItem>();

            var availableItems = original.Items.SelectMany(i => i.GetAvailableItems(principal));

            foreach (var item in availableItems)
            {
                var cloneFor = item.CloneFor(principal);

                if (cloneFor.IsAvailableTo(principal))
                {
                    Items.Add(cloneFor);
                }
            }

            RemoveEmptyBranches();
        }

        protected virtual IEnumerable<MenuItem> GetAvailableItems(IAtomicPrincipal principal)
        {
            if (IsAvailableTo(principal))
            {
                yield return this;
            }
        }

        internal virtual MenuItem CloneFor(IAtomicPrincipal principal)
        {
            return new MenuItem(this, principal);
        }

        internal override void Localize(ResourceManager resourceManager)
        {
            base.Localize(resourceManager);

            foreach (var item in Items)
            {
                item.Localize(resourceManager);
            }
        }

        internal IEnumerable<ActionReference> GetAllMenuItems()
        {
            yield return this;

            foreach (var item in Items)
            {
                var subItems = item.GetAllMenuItems();

                foreach (var subItem in subItems)
                {
                    yield return subItem;
                }
            }
        }

        internal bool IsEmpty => string.IsNullOrWhiteSpace(ActionName)
                                 && Items.Count == 0;

        private void RemoveEmptyBranches()
        {
            foreach (var item in Items)
            {
                item.RemoveEmptyBranches();
            }

            var emptyItems = Items.Where(i => i.IsEmpty).ToList();

            foreach (var item in emptyItems)
            {
                Items.Remove(item);
            }
        }
    }
}