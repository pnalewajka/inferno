﻿namespace Smt.Atomic.Presentation.SiteMap.Models
{
    public class LandingPage : ActionReference
    {
        public LandingPage()
        {
        }

        internal LandingPage(LandingPage original)
            : base(original)
        {
        }
    }
}