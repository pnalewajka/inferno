﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;

namespace Smt.Atomic.Presentation.SiteMap.Models
{
    [DebuggerDisplay("{ToString()}")] 
    public class ActionReference
    {
        private const string NoActionUrl = "#";

        private string _siteMapRoles;
        private List<RoleAlternatives> _allRoles;
        private readonly HashSet<RoleAlternatives> _controllerRoles = new HashSet<RoleAlternatives>();

        [XmlAttribute("area")]
        public string AreaName { get; set; }

        [XmlAttribute("controller")]
        public string ControllerName { get; set; }

        [XmlAttribute("action")]
        public string ActionName { get; set; }

        [XmlAttribute("params")]
        public string ActionParameters { get; set; }

        [XmlAttribute("profiles")]
        public string AllowedForProfiles { get; set; }

        [XmlAttribute("roles")]
        public string SiteMapRoles
        {
            get { return _siteMapRoles; }
            set
            {
                _siteMapRoles = value;
                _allRoles = null;
            }
        }

        [XmlAttribute("hideAfterLogin")]
        [DefaultValue(false)]
        public bool IsHiddenAfterLogin { get; set; }

        internal bool IsAuthorizationRequired { get; set; }
        
        internal bool HasActionReference => !string.IsNullOrWhiteSpace(ActionName);

        public bool IsAvailableTo(IAtomicPrincipal principal)
        {
            if (_allRoles == null)
            {
                _allRoles = _controllerRoles
                    .Union(GetRoles(_siteMapRoles))
                    .ToList();
            }

            if (!string.IsNullOrEmpty(AllowedForProfiles))
            {
                if (!principal.IsAuthenticated)
                {
                    return false;
                }

                var currentUserProfiles = new HashSet<string>(principal.Profiles);
                var allowedForProfileNames = AllowedForProfiles
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(p => p.Trim());

                if (!allowedForProfileNames.Any(p => currentUserProfiles.Contains(p)))
                {
                    return false;
                }
            }

            return _allRoles.All(a => a.Roles.Any(principal.IsInRole))
                   && (!IsAuthorizationRequired || principal.IsAuthenticated)
                   && !(IsHiddenAfterLogin && principal.IsAuthenticated);
        }

        public string GetUrl(UrlHelper urlHelper)
        {
            if (!HasActionReference)
            {
                return NoActionUrl;
            }

            var routeValues = RoutingHelper.GetValueDictionary(GetActionParameters());

            if (!string.IsNullOrWhiteSpace(AreaName))
            {
                routeValues[RoutingHelper.AreaParameterName] = AreaName;
            }

            var url = urlHelper.Action(ActionName, ControllerName, routeValues);

            if (url == null)
            {
                throw new NoNullAllowedException();
            }

            url = RoutingHelper.PrettifyUrl(url);

            return url;
        }

        public override string ToString()
        {
            // this is for debugging purposes only
            return StringHelper.Join("?",
                                     StringHelper.Join("/", AreaName, ControllerName, ActionName),
                                     ActionParameters);
        }

        internal void AddControllerRoles(RoleAlternatives roleAlternatives)
        {
            IsAuthorizationRequired = true;

            _controllerRoles.Add(roleAlternatives);
            _allRoles = null;
        }

        internal static RoleAlternatives[] GetRoles(IEnumerable<AuthorizeAttribute> attributes)
        {
            return attributes.Select(a => new RoleAlternatives(a.Roles)).Where(r => r.Roles.Any()).ToArray();
        }

        protected ActionReference(ActionReference original)
        {
            AreaName = original.AreaName;
            ControllerName = original.ControllerName;
            ActionName = original.ActionName;
            ActionParameters = original.ActionParameters;
            SiteMapRoles = original.SiteMapRoles;
            IsHiddenAfterLogin = original.IsHiddenAfterLogin;

            _controllerRoles = original._controllerRoles;
            _allRoles = original._allRoles;
        }

        protected ActionReference()
        {
        }

        private string GetActionParameters()
        {
            return RoutingHelper.ReplaceKnownTokens(ActionParameters ?? string.Empty);
        }

        private static IEnumerable<RoleAlternatives> GetRoles(string roleNames)
        {
            return (roleNames ?? string.Empty)
                .Split(',')
                .Select(r => r.Trim())
                .Where(r => r.Length > 0)
                .Select(r => new RoleAlternatives(r));
        }
    }
}