﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Exceptions;
using Smt.Atomic.Presentation.SiteMap.Interfaces;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.SiteMap.Services
{
    internal class SiteMapService : ISiteMapService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IWebApplicationSettings _webApplicationSettings;

        private readonly Dictionary<string, SiteMapModel> _siteMaps = new Dictionary<string, SiteMapModel>();
        private readonly object _lockObject = new object();

        public SiteMapService(IPrincipalProvider principalProvider, IWebApplicationSettings webApplicationSettings)
        {
            _principalProvider = principalProvider;
            _webApplicationSettings = webApplicationSettings;
        }

        public SiteMapModel GetSiteMap()
        {
            var siteMapResourcePath = _webApplicationSettings.SiteMapPath;
            var assembly = _webApplicationSettings.SiteMapResources.Assembly;

            if (string.IsNullOrWhiteSpace(siteMapResourcePath))
            {
                throw new ArgumentException(@"Must provide non-empty resource path.", nameof(siteMapResourcePath));
            }

            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }

            var siteMap = GetSiteMapModel(siteMapResourcePath, assembly);

            var principal = _principalProvider.Current;
            var resourceManager = ResourceManagerHelper.GetResourceManager(_webApplicationSettings.SiteMapResources);

            return siteMap.GetFilteredCopy(principal, resourceManager);
        }

        private SiteMapModel GetSiteMapModel(string siteMapResourcePath, Assembly assembly)
        {
            var key = $"{siteMapResourcePath}@{assembly.FullName}";

            lock (_lockObject)
            {
                if (_siteMaps.ContainsKey(key))
                {
                    return _siteMaps[key];
                }
            
                var siteMapResource = ResourceHelper.GetResourceAsString(siteMapResourcePath, assembly);
                var siteMap = XmlSerializationHelper.DeserializeFromXml<SiteMapModel>(siteMapResource);

                _siteMaps[key] = siteMap;
                ApplyRolesFromControllers(siteMap);

                return siteMap;
            }
        }

        private void ApplyRolesFromControllers(SiteMapModel siteMap)
        {
            var controllers = GetAllControllers();
            var references = siteMap.GetAllActionReferences();

            foreach (var reference in references.Where(r => r.HasActionReference))
            {
                ApplyRolesToActionReference(reference, controllers);
            }
        }

        private static void ApplyRolesToActionReference(ActionReference reference, IEnumerable<ControllerAuthorizationInfo> controllers)
        {
            var fullControllerName = RoutingHelper.GetControllerTypeName(reference.ControllerName);

            var controller = controllers
                .SingleOrDefault(c => c.Area == reference.AreaName
                                      && c.AssetName == fullControllerName);

            if (controller == null)
            {
                throw new ControllerNotFoundException(reference.AreaName, reference.ControllerName);
            }

            if (!controller.Methods.ContainsKey(reference.ActionName))
            {
                throw new ControllerActionNotFoundException(reference.AreaName, reference.ControllerName, reference.ActionName);
            }

            var action = controller.Methods[reference.ActionName];

            reference.IsAuthorizationRequired = action.IsAuthorizationRequired 
                || (controller.IsAuthorizationRequired && !action.AllowAnonymous);

            foreach (var role in controller.Roles)
            {
                reference.AddControllerRoles(role);
            }

            foreach (var role in action.Roles)
            {
                reference.AddControllerRoles(role);
            }
        }

        private IList<ControllerAuthorizationInfo> GetAllControllers()
        {
            var controllerTypes = TypeHelper.GetAllSubClassesOf(typeof (Controller), true);

            return controllerTypes
                .Select(t => new ControllerAuthorizationInfo(t))
                .ToList();
        }
    }
}
