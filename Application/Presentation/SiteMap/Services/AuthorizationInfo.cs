﻿using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.SiteMap.Services
{
    class AuthorizationInfo
    {
        public string AssetName { get; set; }

        public bool IsAuthorizationRequired { get; set; }

        public bool AllowAnonymous { get; set; }

        public RoleAlternatives[] Roles { get; set; }

        public AuthorizationInfo()
        {
            Roles = new RoleAlternatives[0];
        }
    }
}