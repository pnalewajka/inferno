﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.SiteMap.Services
{
    class ControllerAuthorizationInfo : AuthorizationInfo
    {
        public string Area { get; set; }

        public IDictionary<string, AuthorizationInfo> Methods { get; set; }


        public ControllerAuthorizationInfo(Type controllerType)
        {
            Methods = new Dictionary<string, AuthorizationInfo>();

            AssetName = controllerType.Name;
            Area = RoutingHelper.GetAreaName(controllerType);

            var controllerAttributes = AttributeHelper.GetClassAttributes<AuthorizeAttribute>(controllerType).ToList();

            if (controllerAttributes.Any())
            {
                IsAuthorizationRequired = true;
                Roles = ActionReference.GetRoles(controllerAttributes);
            }

            var actions = TypeHelper.GetPublicInstanceMethodsReturning<ActionResult>(controllerType);

            foreach (var action in actions)
            {
                if (AttributeHelper.GetMemberAttribute<HttpPostAttribute>(action) != null ||
                    AttributeHelper.GetMemberAttribute<HttpPutAttribute>(action) != null)
                {
                    continue;
                }

                var authorizeAttributes = AttributeHelper.GetMethodAttributes<AuthorizeAttribute>(action).ToList();
                var allowAnonymousAttribute = AttributeHelper.GetMethodAttribute<AllowAnonymousAttribute>(action);

                var isAuthorizationRequired = authorizeAttributes.Any();
                var allowAnnonymous = allowAnonymousAttribute != null;

                var roles = ActionReference.GetRoles(authorizeAttributes);

                StoreActionInfo(action, isAuthorizationRequired, roles, allowAnnonymous);
            }
        }

        private void StoreActionInfo(MethodInfo action, bool isAuthorizationRequired, RoleAlternatives[] roleAlternatives, bool allowAnnonymous)
        {
            if (Methods.ContainsKey(action.Name))
            {
                var authorizationInfo = Methods[action.Name];

                authorizationInfo.IsAuthorizationRequired |= isAuthorizationRequired;
                authorizationInfo.Roles = authorizationInfo.Roles.Union(roleAlternatives).ToArray();
                authorizationInfo.AllowAnonymous = allowAnnonymous;
            }
            else
            {
                var authorizationInfo = new AuthorizationInfo
                {
                    IsAuthorizationRequired = isAuthorizationRequired,
                    AssetName = action.Name,
                    Roles = roleAlternatives,
                    AllowAnonymous = allowAnnonymous
                };

                Methods.Add(action.Name, authorizationInfo);
            }
        }

    }
}