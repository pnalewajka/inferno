﻿using System.Collections.Generic;
using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.SiteMap.Interfaces
{
    public interface IMenuItemProviderService
    {
        IList<MenuItem> GetItems(string parameters);
    }
}