﻿using Smt.Atomic.Presentation.SiteMap.Models;

namespace Smt.Atomic.Presentation.SiteMap.Interfaces
{
    public interface ISiteMapService
    {
        SiteMapModel GetSiteMap();
    }
}
