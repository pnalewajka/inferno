﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.SiteMap.Exceptions
{
    /// <summary>
    /// Exception regarding a controller not found during site map initialization.
    /// </summary>
    [Serializable]
    public class ControllerNotFoundException : Exception
    {
        public string AreaName { get; protected set; }
        public string ControllerName { get; protected set; }

        public ControllerNotFoundException(string areaName, string controllerName)
            : base(GetMessage(areaName, controllerName))
        {
            AreaName = areaName;
            ControllerName = controllerName;
        }

        private static string GetMessage(string areaName, string controllerName)
        {
            return
                $"Could not find controller referenced in the sitemap. Area = '{areaName}', controller = '{controllerName}'.";
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected ControllerNotFoundException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
            AreaName = info.GetString(PropertyHelper.GetPropertyName<ControllerNotFoundException>(e => e.AreaName));
            ControllerName = info.GetString(PropertyHelper.GetPropertyName<ControllerNotFoundException>(e => e.ControllerName));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<ControllerNotFoundException>(e => e.AreaName), AreaName);
            info.AddValue(PropertyHelper.GetPropertyName<ControllerNotFoundException>(e => e.ControllerName), ControllerName);

            base.GetObjectData(info, context);
        }
    }
}
