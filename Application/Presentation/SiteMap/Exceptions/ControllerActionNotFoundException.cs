﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Presentation.SiteMap.Exceptions
{
    /// <summary>
    /// Exception regarding a controller action not found during site map initialization.
    /// </summary>
    [Serializable]
    public class ControllerActionNotFoundException : Exception
    {
        public string AreaName { get; protected set; }
        public string ControllerName { get; protected set; }
        public string ActionName { get; protected set; }

        public ControllerActionNotFoundException(string areaName, string controllerName, string actionName)
            : base(GetMessage(areaName, controllerName, actionName))
        {
            AreaName = areaName;
            ControllerName = controllerName;
            ActionName = actionName;
        }

        private static string GetMessage(string areaName, string controllerName, string actionName)
        {
            return
                $"Could not find controller action referenced in the sitemap. Area = '{areaName}', controller = '{controllerName}', action = {actionName}.";
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected ControllerActionNotFoundException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
            AreaName = info.GetString(PropertyHelper.GetPropertyName<ControllerActionNotFoundException>(e => e.AreaName));
            ControllerName = info.GetString(PropertyHelper.GetPropertyName<ControllerActionNotFoundException>(e => e.ControllerName));
            ActionName = info.GetString(PropertyHelper.GetPropertyName<ControllerActionNotFoundException>(e => e.ActionName));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(PropertyHelper.GetPropertyName<ControllerActionNotFoundException>(e => e.AreaName), AreaName);
            info.AddValue(PropertyHelper.GetPropertyName<ControllerActionNotFoundException>(e => e.ControllerName), ControllerName);
            info.AddValue(PropertyHelper.GetPropertyName<ControllerActionNotFoundException>(e => e.ActionName), ActionName);

            base.GetObjectData(info, context);
        }
    }
}
