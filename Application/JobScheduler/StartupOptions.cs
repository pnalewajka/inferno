﻿using CommandLine;

namespace Smt.Atomic.JobScheduler
{
    public class StartupOptions
    {
        [Option("console", HelpText = "Run as console application only")]
        public bool Console { get; set; }

        [Option("encryptConfigurationSection", HelpText = "Encrypt configuration section")]
        public bool EncryptConfigurationSection { get; set; }

        [Option("jobId", HelpText = "Run requested job only")]
        public long JobId { get; set; }

        [Option("jobCode", HelpText = "Run requested job only")]
        public string JobCode { get; set; }

        [Option("pipeline", HelpText = "Run only single pipeline")]
        public string Pipeline { get; set; }
    }
}
