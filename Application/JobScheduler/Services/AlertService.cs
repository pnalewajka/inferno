﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Models.Alerts;

namespace Smt.Atomic.JobScheduler.Services
{
    internal class AlertService : IAlertService
    {
        public void AddAlert(AlertType alertType, string message, bool isDismisable = true)
        {
        }

        public void AddSuccess(string message, bool isDismisable = true)
        {
        }

        public void AddInformation(string message, bool isDismisable = true)
        {
        }

        public void AddWarning(string message, bool isDismisable = true)
        {
        }

        public void AddError(string message, bool isDismisable = true)
        {
        }

        public List<AlertViewModel> GetAlerts()
        {
            return new List<AlertViewModel>();
        }

        public void ClearAlerts()
        {         
        }
    }
}