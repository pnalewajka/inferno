﻿Set-Location ..
$installUtil = [System.Runtime.InteropServices.RuntimeEnvironment]::GetRuntimeDirectory()  | Join-Path -ChildPath InstallUtil.exe
& $installUtil Smt.Atomic.JobScheduler.exe /ShowCallStack
Set-Location Scripts