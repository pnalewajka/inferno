using Castle.Core;
using Castle.Core.Logging;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Diagnostics;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using System;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.JobScheduler.Seeds;

namespace Smt.Atomic.JobScheduler
{
    internal class Program
    {
        private readonly JobSchedulerContainerConfiguration _configuration = new JobSchedulerContainerConfiguration();
        private IWindsorContainer _container;
        private ILogger _logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            new Program().Run(args);
        }

        private void Run(string[] args)
        {
            var options = ParseStartupOptions(args);

            if (options == null)
            {
                return;
            }

            SetupContainer();
            RunStartupTimeDiagnostics();
            SetupThreadPrincipal();
            RunSchedulerSeeds();

            try
            {
                ProcessStartingOptions(options, args);
            }
            catch (Exception e)
            {
                _logger.Error("Failure during service initialization", e);
                throw;
            }
        }

        private void RunSchedulerSeeds()
        {
            var unitOfWorkService = _container.Resolve<IUnitOfWorkService<ISchedulingDbScope>>();
            var logger = _container.Resolve<ILogger>();

            using (var unitOfWork = unitOfWorkService.Create())
            {
                PipelinesSeed.Seed(unitOfWork, logger);
                JobDefinitionsSeed.Seed(unitOfWork, logger);
            }
        }

        private void SetupThreadPrincipal()
        {
            var principalBuilder = _container.Resolve<IPrincipalBuilder>();
            var settingsProvider = _container.Resolve<ISettingsProvider>();
            var userName = settingsProvider.JobSchedulerSettings.UserName;
            var principal = principalBuilder.BuildPrincipal(userName);

            Thread.CurrentPrincipal = principal;
        }

        private void SetupContainer()
        {
            _container = _configuration.Configure(ContainerType.JobScheduler);
            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel));
            _logger = _container.Resolve<ILogger>();
        }

        private void RunStartupTimeDiagnostics()
        {
            WindsorContainerDiagnostics.Inspect(_container, _logger);
            CheckServicesRegistration();
        }

        private void CheckServicesRegistration()
        {
            var handlers = _container.Kernel.GetAssignableHandlers(typeof(object));
            var invalidHandlers = handlers
                .Where(h => h.ComponentModel.LifestyleType == LifestyleType.PerWebRequest);

            var invalidRequests = invalidHandlers
                .Select(h => h.ComponentModel.ComponentName)
                .ToList();

            if (invalidRequests.Any())
            {
                throw new BasicConfigurationException($"JobScheduler misconfiguration: {string.Join(", ", invalidRequests)}");
            }
        }

        private StartupOptions ParseStartupOptions(string[] args)
        {
            var result = CommandLine.Parser.Default.ParseArguments<StartupOptions>(args);
            
            if (result.Errors.Any())
            {
                Console.WriteLine(CommandLine.Text.HelpText.AutoBuild(result, null));

                return null;
            }
            
            return result.Value;
        }

        private void ProcessStartingOptions(StartupOptions options, string[] args)
        {
            if (options.EncryptConfigurationSection)
            {
                EncryptConfiguration(args);
                
                return;
            }
            
            var service = _container.Resolve<SchedulerWindowsService>();
            service.Options = options;
            
            if (options.Console)
            {
                service.RunConsole(args);
            }
            else
            {
                RunService(service);
            }
        }

        private void RunService(ServiceBase service)
        {
            var servicesToRun = new[]
            {
                service
            };
            ServiceBase.Run(servicesToRun);
        }

        private static void EncryptConfiguration(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine(@"Missing section parameter.");
            }

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            Console.WriteLine(@"Protecting: {0}", configuration.FilePath);

            var section = configuration.GetSection(args[1]);
            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(
                    "DataProtectionConfigurationProvider");

                configuration.Save();

                Console.WriteLine(@"Done");
            }
            else
            {
                Console.WriteLine(@"Section is missing or already encrypted");
            }
        }

    }
}
