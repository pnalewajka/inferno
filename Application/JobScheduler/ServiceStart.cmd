﻿IF [%1]==[] GOTO usage
IF [%2]==[] GOTO usage
IF [%3]==[] GOTO usage

echo Enabling %2 on \\%1
sc \\%1 config %2 start= demand
echo Starting %2 on \\%1
sc \\%1 start %2 %4