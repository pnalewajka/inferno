ECHO OFF
IF [%1]==[] GOTO usage
IF [%2]==[] GOTO usage
IF [%3]==[] GOTO usage
IF [%4]==[] GOTO usage

SET /a inc=1

:CheckLock
ECHO !!!
ECHO !!! Check number %inc%
ECHO !!!
copy %1\%2.%3 %1\%2.copy.%3 /Y /Z
IF errorlevel 1 GOTO CheckWait
copy %1\%2.copy.%3 %1\%2.%3 /Y /Z
IF errorlevel 1 GOTO CheckWait
del %1\%2.copy.%3 /F /Q
IF errorlevel 0 GOTO Ok
:CheckWait
SET /a inc=%inc%+1
IF %inc%==11 GOTO :Fail
%4\Sleeper.exe 10000
GOTO CheckLock

:Ok
ECHO !!!
echo !!! Access - OK
ECHO !!!
exit /B 0
GOTO:eof

:Fail
ECHO !!!
echo !!! Access - Fail
ECHO !!!
exit /B 1
GOTO:eof

:usage
echo Usage:
echo "WaitForAccess.exe [location] [fileName] [fileExtension] [tools directory]
GOTO:eof