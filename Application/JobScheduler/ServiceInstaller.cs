﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.JobScheduler.Services;
using Smt.Atomic.Presentation.Common.Interfaces;
using Smt.Atomic.WebApp;

namespace Smt.Atomic.JobScheduler
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<SchedulerWindowsService>().LifestylePerThread());
            container.Register(Component.For<IAlertService>().ImplementedBy<AlertService>().LifestyleTransient());

            container.Register(
                Classes.FromAssemblyContaining(typeof(WebAppContainerConfiguration))
                    .BasedOn(typeof(IClassMapping<,>))
                    .WithServiceSelf()
                    .WithServiceFirstInterface()
                    .LifestyleTransient());
        }
    }
}
