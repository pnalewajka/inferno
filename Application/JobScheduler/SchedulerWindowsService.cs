﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using System;
using System.ServiceProcess;
using System.Threading;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.JobScheduler
{
    public partial class SchedulerWindowsService : ServiceBase
    {
        private readonly ILogger _logger;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IPipelineManagerService _pipelineManagerService;
        private readonly IJobManagementService _jobManagementService;
        private readonly IPrincipalBuilder _principalBuilder;

        public SchedulerWindowsService(
            ILogger logger,
            ISettingsProvider settingsProvider,
            IPipelineManagerService pipelineManagerService,
            IJobManagementService jobManagementService,
            IPrincipalBuilder principalBuilder)
        {
            _logger = logger;
            _settingsProvider = settingsProvider;
            _pipelineManagerService = pipelineManagerService;
            _jobManagementService = jobManagementService;
            _principalBuilder = principalBuilder;
            InitializeComponent();
        }

        public StartupOptions Options { get; set; }

        protected override void OnStart(string[] args)
        {
            Init();

            if (Options.JobId != 0)
            {
                Options.Pipeline = _jobManagementService.ForceImmediateExecution(Options.JobId);
            }
            else if (!string.IsNullOrEmpty(Options.JobCode))
            {
                Options.Pipeline = _jobManagementService.ForceImmediateExecution(Options.JobCode);
            }

            if (Options.JobId != 0 && !string.IsNullOrEmpty(Options.JobCode))
            {
                throw new BusinessException("It not allowed to set jobId and jobCode at the same time");
            }

            try
            {
                if (string.IsNullOrEmpty(Options.Pipeline))
                {
                    foreach (var pipeline in _settingsProvider.JobSchedulerSettings.Pipelines)
                    {
                        _pipelineManagerService.CreateAndStartPipeline(pipeline);
                    }
                }
                else
                {
                    _pipelineManagerService.CreateAndStartPipeline(Options.Pipeline);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Failure during service startup", e);
                throw;
            }
        }

        protected override void OnStop()
        {
            try
            {
                _pipelineManagerService.Stop();
            }
            catch (Exception e)
            {
                _logger.Error("Failure during service shutdown", e);
                throw;
            }
        }

        // for testing and debugging
        public void RunConsole(string[] args)
        {
            OnStart(args);
            Console.WriteLine(@"Press any key to stop JobScheduler");
            Console.ReadKey(true);
            Console.WriteLine(@"JobScheduler is stopping");
            OnStop();
        }

        private void Init()
        {
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            SetupThreadPrincipal();
        }

        private void SetupThreadPrincipal()
        {
            var userName = _settingsProvider.JobSchedulerSettings.UserName;
            Thread.CurrentPrincipal = _principalBuilder.BuildPrincipal(userName);
        }
    }
}
