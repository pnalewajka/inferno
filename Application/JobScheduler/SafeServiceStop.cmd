::@echo off
:: This script originally authored by Eric Falsken
:: Pobrane z: http://stackoverflow.com/questions/1405372/stopping-starting-a-remote-windows-service-and-waiting-for-it-to-open-close

IF [%1]==[] GOTO usage
IF [%2]==[] GOTO usage
IF [%3]==[] GOTO usage

:: ping -n 1 -4 %1 | FIND "TTL="
:: IF errorlevel 1 GOTO SystemOffline
SC \\%1 query %2 | FIND "STATE"
IF errorlevel 1 GOTO SystemOffline

:ResolveInitialState
SC \\%1 query %2 | FIND "STATE" | FIND "RUNNING"
IF errorlevel 0 IF NOT errorlevel 1 GOTO StopService
SC \\%1 query %2 | FIND "STATE" | FIND "STOPPED"
IF errorlevel 0 IF NOT errorlevel 1 GOTO StopedService
SC \\%1 query %2 | FIND "STATE" | FIND "PAUSED"
IF errorlevel 0 IF NOT errorlevel 1 GOTO SystemOffline
echo Service State is changing, waiting for service to resolve its state before making changes
sc \\%1 query %2 | Find "STATE"
%3\Sleeper.exe 10000
GOTO ResolveInitialState

:StopService
echo Disabling %2 on \\%1
sc \\%1 config %2 start= disabled
echo Stopping %2 on \\%1
sc \\%1 stop %2 %4

GOTO StopingService
:StopingServiceDelay
echo Waiting for %2 to stop
%3\Sleeper.exe 10000
:StopingService
SC \\%1 query %2 | FIND "STATE" | FIND "STOPPED"
IF errorlevel 1 GOTO StopingServiceDelay

:StopedService
echo %2 on \\%1 is stopped and disabled
GOTO:eof

:SystemOffline
echo Server \\%1 or service %2 is not accessible or is offline
GOTO:eof

:usage
echo Will cause a remote service to STOP (if not already stopped).
echo This script will waiting for the service to enter the stpped state if necessary.
echo.
echo %0 [system name] [service name] [tools folder] {reason}
echo Example: %0 server1 MyService
echo.
echo For reason codes, run "sc stop"
GOTO:eof