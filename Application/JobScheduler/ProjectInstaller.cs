﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Settings.Models;

namespace Smt.Atomic.JobScheduler
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private readonly JobSchedulerSettings _settings;

        public ProjectInstaller()
        {
            AssemblyResolver.AddAssemblyLocation(Path.GetDirectoryName(GetType().Assembly.Location));
            _settings = GetSection<JobSchedulerSettings>();
            InitializeComponent();
        }

        private TSectionModel GetSection<TSectionModel>() where TSectionModel : class
        {
            var attribute = AttributeHelper.GetClassAttribute<XmlRootAttribute>(typeof(TSectionModel));
            var sectionName = attribute == null ? typeof (TSectionModel).Name : attribute.ElementName;

            var map = new ExeConfigurationFileMap
            {
                ExeConfigFilename = $"{GetType().Assembly.Location}.config"
            };

            var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            var section = config.GetSection(sectionName);
            var sectionHandlerType = Type.GetType(section.SectionInformation.Type);
            if (sectionHandlerType==null || !typeof(IConfigurationSectionHandler).IsAssignableFrom(sectionHandlerType))
            {
                return null;
            }

            var sectionHandler = Activator.CreateInstance(sectionHandlerType) as IConfigurationSectionHandler;
            if (sectionHandler == null)
            {
                return null;
            }

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(section.SectionInformation.GetRawXml());

            return sectionHandler.Create(null, null, xmlDocument) as TSectionModel;
        }

        private static class AssemblyResolver
        {
            private static readonly List<string> AssemblyPaths = new List<string>();

            static AssemblyResolver()
            {
                AppDomain.CurrentDomain.AssemblyResolve += ResolveHandler;
            }

            public static void AddAssemblyLocation(string path)
            {
                AssemblyPaths.Add(path);
            }

            private static Assembly ResolveHandler(object sender, ResolveEventArgs args)
            {
                var assemblyName = new AssemblyName(args.Name);
                foreach (var assemblyPath in AssemblyPaths)
                {
                    var file = Directory.GetFiles(assemblyPath).FirstOrDefault(f =>
                    {
                        var fileName = Path.GetFileName(f);
                        return fileName != null && fileName.StartsWith(assemblyName.Name);
                    });

                    if (file != null)
                    {
                        return Assembly.LoadFrom(file);
                    }

                }
                return null;
            }
        }
    }
}
