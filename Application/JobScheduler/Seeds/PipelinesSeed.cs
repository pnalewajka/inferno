﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.JobScheduler.Seeds
{
    public class PipelinesSeed
    {
        public static void Seed(IUnitOfWork<ISchedulingDbScope> unitOfWork, ILogger logger)
        {
            var existingPipelines = unitOfWork.Repositories.Pipelines.Select(s => s.Name).ToHashSet();
            var missingPipelines = GetPipelines().Where(s => !existingPipelines.Contains(s.Name)).ToList();

            if (missingPipelines.Any())
            {
                logger.Info("Adding new Pipelines...");

                unitOfWork.Repositories.Pipelines.AddRange(missingPipelines);
                unitOfWork.Commit();
            }
        }

        private static IEnumerable<Pipeline> GetPipelines()
        {
            var propertyInfos = typeof(PipelineCodes).GetFields();

            foreach (var propertyInfo in propertyInfos)
            {
                var name = propertyInfo.GetValue(null) as string;

                yield return new Pipeline
                {
                    Name = name
                };
            }
        }
    }
}
