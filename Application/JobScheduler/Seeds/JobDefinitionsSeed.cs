﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.JobScheduler.Seeds
{
    public class JobDefinitionsSeed
    {
        public static void Seed(IUnitOfWork<ISchedulingDbScope> unitOfWork, ILogger logger)
        {
            var existingJobDefinitions = unitOfWork.Repositories.JobDefinitions.Select(s => s.Code).ToHashSet();
            var missingJobDefinitions = GetJobDefinitions(unitOfWork, logger).Where(s => !existingJobDefinitions.Contains(s.Code)).ToList();

            if (missingJobDefinitions.Any())
            {
                logger.Info($"Adding {missingJobDefinitions.Count()} new JobDefinitions...");

                unitOfWork.Repositories.JobDefinitions.AddRange(missingJobDefinitions);
                unitOfWork.Commit();
            }
        }

        private static IEnumerable<JobDefinition> GetJobDefinitions(IUnitOfWork<ISchedulingDbScope> unitOfWork, ILogger logger)
        {
            var jobTypes = TypeHelper.GetImplementationsOf<IJob>();

            foreach (var jobType in jobTypes)
            {
                var identifierAttribute = AttributeHelper.GetClassAttribute<IdentifierAttribute>(jobType);
                var jobDefinitionAttribute = AttributeHelper.GetClassAttribute<JobDefinitionAttribute>(jobType);

                if (identifierAttribute == null || jobDefinitionAttribute == null)
                {
                    logger.Error($"No default definition was found for job: {jobType.Name}");
                    continue;
                }

                var pipelineId =
                    unitOfWork.Repositories.Pipelines.Where(p => p.Name == jobDefinitionAttribute.PipelineName)
                        .Select(p => p.Id)
                        .Single();

                yield return new JobDefinition
                {
                    Code = jobDefinitionAttribute.Code,
                    Description = jobDefinitionAttribute.Description,
                    ClassIdentifier = identifierAttribute.Value,
                    IsEnabled = jobDefinitionAttribute.IsEnabled,
                    ShouldRunNow = jobDefinitionAttribute.ShouldRunAfterCreation,
                    PipelineId = pipelineId,
                    MaxExecutionPeriodInSeconds = jobDefinitionAttribute.MaxExecutioPeriodInSeconds,
                    ScheduleSettings = jobDefinitionAttribute.ScheduleSettings,
                    MaxRunAttemptsInBulk = jobDefinitionAttribute.MaxRunAttemptsInBulk,
                    DelayBetweenBulkRunAttemptsInSeconds = jobDefinitionAttribute.DelayBetweenBulkRunAttemptsInSeconds,
                    MaxBulkRunAttempts = jobDefinitionAttribute.MaxBulkRunAttempts,
                };
            }
        }
    }
}
