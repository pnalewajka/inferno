﻿using System.Collections.Generic;
using Castle.MicroKernel.Releasers;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.JobScheduler
{
    public class JobSchedulerContainerConfiguration : ContainerConfiguration
    {
        public override WindsorContainer Configure(ContainerType containerType)
        {
            var windsorContainer = base.Configure(containerType);
#pragma warning disable 618
            windsorContainer.Kernel.ReleasePolicy = new NoTrackingReleasePolicy();
#pragma warning restore 618

            return windsorContainer;
        }

        protected override IEnumerable<AtomicInstaller> GetInstallers(ContainerType containerType)
        {
            yield return new Business.Accounts.ServiceInstaller();
            yield return new Business.BulkEdit.ServiceInstaller();
            yield return new Business.Common.ServiceInstaller();
            yield return new CrossCutting.Common.ServiceInstaller();
            yield return new CrossCutting.Settings.ServiceInstaller();
            yield return new Business.Scheduling.ServiceInstaller();
            yield return new Data.Entities.ServiceInstaller();
            yield return new Data.Exchange.ServiceInstaller();
            yield return new Data.Repositories.ServiceInstaller();
            yield return new CrossCutting.Security.ServiceInstaller();
            yield return new Business.Configuration.ServiceInstaller();
            yield return new Business.Runtime.ServiceInstaller();
            yield return new Business.Notifications.ServiceInstaller();
            yield return new Business.EventSourcing.ServiceInstaller();
            yield return new Data.Jira.ServiceInstaller();
            yield return new Data.Navision.ServiceInstaller();
            yield return new Data.ImportHours.ServiceInstaller();
            yield return new Business.Allocation.ServiceInstaller();
            yield return new Business.PeopleManagement.ServiceInstaller();
            yield return new Business.CustomerPortal.ServiceInstaller();
            yield return new Business.Organization.ServiceInstaller();
            yield return new Data.Crm.ServiceInstaller();
            yield return new Data.SharePoint.ServiceInstaller();
            yield return new Business.SkillManagement.ServiceInstaller();
            yield return new Business.Workflows.ServiceInstaller();
            yield return new Business.Dictionaries.ServiceInstaller();
            yield return new Business.Consents.ServiceInstaller();
            yield return new Business.TimeTracking.ServiceInstaller();
            yield return new Business.ActiveDirectory.ServiceInstaller();
            yield return new Business.Finance.ServiceInstaller();
            yield return new Business.Survey.ServiceInstaller();
            yield return new Business.BusinessTrips.ServiceInstaller();
            yield return new Business.Reporting.ServiceInstaller();
            yield return new Business.MeetMe.ServiceInstaller();
            yield return new Business.Compensation.ServiceInstaller();
            yield return new Business.Recruitment.ServiceInstaller();
            yield return new Business.CrmMigration.ServiceInstaller();
            yield return new ServiceInstaller();
        }
    }
}