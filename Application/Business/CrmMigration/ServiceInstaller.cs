﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.GDPR.Services;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.Business.Recruitment.Services.Notification;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessNoteServices;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessStepServices;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.CrmMigration
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<ICandidateCardIndexDataService>().ImplementedBy<CandidateCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IJobProfileService>().ImplementedBy<JobProfileService>().LifestylePerThread());
            container.Register(Component.For<ICandidateFileCardIndexDataService>().ImplementedBy<CandidateFileCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<ICandidateNoteCardIndexDataService>().ImplementedBy<CandidateNoteCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningCardIndexDataService>().ImplementedBy<JobOpeningCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningService>().ImplementedBy<JobOpeningService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningNoteService>().ImplementedBy<JobOpeningNoteService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningNoteCardIndexDataService>().ImplementedBy<JobOpeningNoteCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessCardIndexDataService>().ImplementedBy<RecruitmentProcessCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessService>().ImplementedBy<RecruitmentProcessService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessStepService>().ImplementedBy<RecruitmentProcessStepService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessNoteCardIndexDataService>().ImplementedBy<RecruitmentProcessNoteCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessStepCardIndexDataService>().ImplementedBy<RecruitmentProcessStepCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<ICandidateNotificationService>().ImplementedBy<CandidateNotificationService>().LifestylePerThread());
            container.Register(Component.For<ITechnicalReviewCardIndexDataService>().ImplementedBy<TechnicalReviewCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IJobApplicationCardIndexDataService>().ImplementedBy<JobApplicationCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IDataConsentCardIndexDataService>().ImplementedBy<CandidateDataConsentCardIndexDataService>().LifestylePerThread());
            container.Register(Component.For<IApplicationOriginService>().ImplementedBy<ApplicationOriginService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningNotificationService>().ImplementedBy<JobOpeningNotificationService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningRecipientsProvider>().ImplementedBy<JobOpeningRecipientsProvider>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessNoteNotificationService>().ImplementedBy<RecruitmentProcessNoteNotificationService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessNotificationService>().ImplementedBy<RecruitmentProcessNotificationService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessStepRecipientsProvider>().ImplementedBy<RecruitmentProcessStepRecipientsProvider>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessRecipientsProvider>().ImplementedBy<RecruitmentProcessRecipientsProvider>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningNoteNotificationService>().ImplementedBy<JobOpeningNoteNotificationService>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningNoteRecipientProvider>().ImplementedBy<JobOpeningNoteRecipientProvider>().LifestylePerThread());
            container.Register(Component.For<IJobOpeningHistoryService>().ImplementedBy<JobOpeningHistoryService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessHistoryService>().ImplementedBy<RecruitmentProcessHistoryService>().LifestylePerThread());
            container.Register(Component.For<IRecruitmentProcessNoteRecipientProvider>().ImplementedBy<RecruitmentProcessNoteRecipientsProvider>().LifestylePerThread());
        }
    }
}

