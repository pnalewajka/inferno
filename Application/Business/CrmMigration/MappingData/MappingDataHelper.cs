﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Business.CrmMigration.Consts;
using Smt.Atomic.Business.CrmMigration.Helpers;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.CrmMigration.MappingData
{
    public class MappingDataHelper
    {
        private readonly ILogger _logger;
        private readonly ConversionHelper _conversionHelper;

        public MappingDataHelper(ILogger logger)
        {
            _logger = logger;
            _conversionHelper = new ConversionHelper(_logger);
        }

        public CandidateEmploymentStatus MapperEmployeeStatus(int employeeStatus)
        {
            CandidateEmploymentStatus candidateEmploymentStatus = CandidateEmploymentStatus.NotEmployee;

            switch (employeeStatus)
            {
                case CrmEmployeeStatus.NotEmployee:
                    candidateEmploymentStatus = CandidateEmploymentStatus.NotEmployee;
                    break;
                case CrmEmployeeStatus.CurrentEmployee:
                    candidateEmploymentStatus = CandidateEmploymentStatus.CurrentEmployee;
                    break;
                case CrmEmployeeStatus.PastEmployee:
                    candidateEmploymentStatus = CandidateEmploymentStatus.PastEmployee;
                    break;
            }

            return candidateEmploymentStatus;
        }

        public bool MapperCandidateRelocation(int candidateRelocation)
        {
            bool canRelocate = false;

            switch (candidateRelocation)
            {
                case CrmRelocation.NoRelocation:
                    canRelocate = false;
                    break;
                case CrmRelocation.YesRelocation:
                    canRelocate = true;
                    break;
            }

            return canRelocate;
        }

        public List<CandidateLanguageDto> MapperLanguageCandidateSkill(List<new_languageskill> languagesSkill)
        {
            var languages = new List<CandidateLanguageDto>();

            languages.AddRange(languagesSkill.Where(s => s.new_Language != null).Select(l =>
               {
                   var languageId = l.new_Language?.Value;

                   return new CandidateLanguageDto
                   {
                       LanguageId = MapToDanteLanguage(languageId.Value).Value,
                       Level = l.new_Level != null ? MapperLanguageLevel(l.new_Level.Value) : MapperLanguageLevel(l.smt_LanguageLevel.Value)
                   };
               })
            .AsEnumerable());

            return languages;
        }

        public LanguageReferenceLevel MapperLanguageLevel(int? languageLevel)
        {
            var languageReferenceLevel = LanguageReferenceLevel.None;

            switch (languageLevel)
            {
                case CrmLanguageLevel.Basic:
                    languageReferenceLevel = LanguageReferenceLevel.A1;
                    break;
                case CrmLanguageLevel.Intermediate:
                    languageReferenceLevel = LanguageReferenceLevel.B1;
                    break;
                case CrmLanguageLevel.Advanced:
                    languageReferenceLevel = LanguageReferenceLevel.B2;
                    break;
                case CrmLanguageLevel.Fluent:
                    languageReferenceLevel = LanguageReferenceLevel.C1;
                    break;
                case CrmLanguageLevel.A1:
                    languageReferenceLevel = LanguageReferenceLevel.A1;
                    break;
                case CrmLanguageLevel.A1_A2:
                    languageReferenceLevel = LanguageReferenceLevel.A1;
                    break;
                case CrmLanguageLevel.A2:
                    languageReferenceLevel = LanguageReferenceLevel.A2;
                    break;
                case CrmLanguageLevel.A2_B1:
                    languageReferenceLevel = LanguageReferenceLevel.A2;
                    break;
                case CrmLanguageLevel.B1:
                    languageReferenceLevel = LanguageReferenceLevel.B1;
                    break;
                case CrmLanguageLevel.B1_B2:
                    languageReferenceLevel = LanguageReferenceLevel.B1;
                    break;
                case CrmLanguageLevel.B2:
                    languageReferenceLevel = LanguageReferenceLevel.B2;
                    break;
                case CrmLanguageLevel.B2_C1:
                    languageReferenceLevel = LanguageReferenceLevel.B2;
                    break;
                case CrmLanguageLevel.C1:
                    languageReferenceLevel = LanguageReferenceLevel.C1;
                    break;
                case CrmLanguageLevel.C1_C2:
                    languageReferenceLevel = LanguageReferenceLevel.C1;
                    break;
                case CrmLanguageLevel.C2:
                    languageReferenceLevel = LanguageReferenceLevel.C2;
                    break;
            }

            return languageReferenceLevel;
        }

        public RecruitmentProcessClosedReason? MapToRecruitmentProcessClosedReason(int statusCode)
        {
            switch (statusCode)
            {
                case CrmStatusCodeRa.CandidateResigned:
                case CrmStatusCodeRa.CandidateResignedBecauseOfTheProjectClientProces:
                case CrmStatusCodeRa.NoResponseFromCandidate:
                case CrmStatusCodeRa.NotInterested:
                case CrmStatusCodeRa.RejectedOffer:
                    return RecruitmentProcessClosedReason.CandidateResigned;
                case CrmStatusCodeRa.ClientResignedFromTheRecruitment:
                case CrmStatusCodeRa.ClientResigned:
                    return RecruitmentProcessClosedReason.ClientResigned;
                case CrmStatusCodeRa.HiredSd:
                case CrmStatusCodeRa.HiredTa:
                case CrmStatusCodeRa.HiredMs:
                case CrmStatusCodeRa.HiredPd:
                case CrmStatusCodeRa.HiredOtherBu:
                case CrmStatusCodeRa.HiredToOtherProject:
                case CrmStatusCodeRa.HiredExternal:
                case CrmStatusCodeRa.HiredOutsourcing:
                case CrmStatusCodeRa.HiredInternalRecruitment:
                case CrmStatusCodeRa.HiredEmbedded:
                case CrmStatusCodeRa.HiredCognitive:
                case CrmStatusCodeRa.HiredQa:
                case CrmStatusCodeRa.HiredMobile:
                case CrmStatusCodeRa.HiredEnterpriseApplications:
                case CrmStatusCodeRa.HiredDigitalPlatforms:
                case CrmStatusCodeRa.HiredProductManagementConsulting:
                case CrmStatusCodeRa.HiredDesignStudioPoland:
                case CrmStatusCodeRa.HiredAutomotive:
                case CrmStatusCodeRa.HiredHiTec:
                case CrmStatusCodeRa.HiredFinancialServices:
                case CrmStatusCodeRa.HiredTelcoMedia:
                case CrmStatusCodeRa.HiredIndustryHealthcare:
                case CrmStatusCodeRa.HiredConsumerServices:
                    return RecruitmentProcessClosedReason.Hired;
                case CrmStatusCodeRa.FailedTechnicalVerification:
                case CrmStatusCodeRa.CvRejectedByPm:    // ?
                case CrmStatusCodeRa.CvRejectedBySlm:   // ?
                case CrmStatusCodeRa.RejectedBySlm: // ?
                case CrmStatusCodeRa.RejectedBySdm: // ?
                case CrmStatusCodeRa.RejectedAfterMeeting:  // ?
                    return RecruitmentProcessClosedReason.RejectedByHiringManager;
                case CrmStatusCodeRa.RejectedByClientPm:
                case CrmStatusCodeRa.CvRejectedByClient:
                case CrmStatusCodeRa.HiredOtherCandidate:
                    return RecruitmentProcessClosedReason.RejectedByClient;
                case CrmStatusCodeRa.RejectedByRecruitment:
                    return RecruitmentProcessClosedReason.RejectedByRecruiter;

                case CrmStatusCodeRa.Active:
                case CrmStatusCodeRa.OnHold:
                    return null;

                // TODO
                //case ?:
                //    return RecruitmentProcessClosedReason.JobOpeningCancelled;

                default:
                    return null;
            }
        }

        public JobOpeningClosedReason? MapToJobOpeningClosedReason(int jobOpeningStatusCode)
        {
            switch (jobOpeningStatusCode)
            {
                case CrmNeededResourcesStatusCode.Fullfiled:
                    return JobOpeningClosedReason.Fullfied;

                case CrmNeededResourcesStatusCode.Lost:
                    return JobOpeningClosedReason.Lost;

                case CrmNeededResourcesStatusCode.LegacyCodeActive:
                case CrmNeededResourcesStatusCode.ClientResigned:
                case CrmNeededResourcesStatusCode.NotReady:
                case CrmNeededResourcesStatusCode.OnHold:
                    return JobOpeningClosedReason.PositionCanceled;

                default:
                    return null;
            }
        }

        // Specify valid contracts
        public long? MapToFinalContractType(int? contractType)
        {
            switch (contractType)
            {
                case CrmContractType.B2B:
                    return 1;
                case CrmContractType.StandardEmployment:
                    return 8;
                case CrmContractType.FeeForTask:
                    return 15;
                case CrmContractType.ForCommission:
                    return 21;
                default:
                    return null;
            }
        }

        public string MapToOfferContractType(int? contractType)
        {
            switch (contractType)
            {
                case CrmContractType.B2B:
                    return "b2b";
                case CrmContractType.StandardEmployment:
                    return "std. employment contract";
                case CrmContractType.FeeForTask:
                    return "fee for task";
                case CrmContractType.ForCommission:
                    return "contract for commission";
                default:
                    return null;
            }
        }

        public long? MapToJobTitleId(string jobPositionName, Dictionary<string, JobTitle> jobTitles)
        {
            if (jobPositionName == null)
            {
                return null;
            }

            return jobTitles.TryGetValue(jobPositionName, out var jobTitle) ? jobTitle.Id : (long?)null;
        }

        public AgencyEmploymentMode? MapToAgencyEmploymentMode(bool isAgencyModeAllowed, int? agencyEmploymentType)
        {
            if (!isAgencyModeAllowed)
            {
                return null;
            }

            switch (agencyEmploymentType)
            {
                case CrmAgencyEmploymentType.Outsourcing:
                    return AgencyEmploymentMode.Outsourcing;

                case CrmAgencyEmploymentType.PermanentRecruitment:
                    return AgencyEmploymentMode.PermanentRecruitment;

                case CrmAgencyEmploymentType.Both:
                    return AgencyEmploymentMode.Both;

                default:
                    return null;
            }

        }

        public int MapToJobOpeningPriority(int? priority)
        {
            return priority == null
                ? 0
                : priority.Value - 180580000;
        }

        public string MapToOrgUnitCode(string serviceLineName, int? verticalValue, Guid entityId)
        {
            var result = _conversionHelper.GetValue(CrmNeededResourcesServiceLine.OrgUnitCodes, serviceLineName, "Org Unit service line", entityId)
                ?? _conversionHelper.GetValue(CrmNeededResourcesVertical.OrgUnitCodes, verticalValue, "Org Unit Vertical", entityId);

            if (string.IsNullOrEmpty(result))
            {
                return "INTIVE";
            }

            return result;
        }

        public HiringMode MapToHiringMode(int? neededResourcesFormTemplate)
        {
            switch (neededResourcesFormTemplate)
            {
                case CrmNeededResourcesFormTemplate.HiringToProject:
                    return HiringMode.Project;

                case CrmNeededResourcesFormTemplate.HiringToIntive:
                default:
                    return HiringMode.Intive;
            }
        }

        public JobOpeningStatus MapToJobOpeningStatus(int? statuscode, smt_neededresourcesState? statecode)
        {
            switch (statuscode)
            {
                case CrmNeededResourcesStatusCode.Active:
                    return JobOpeningStatus.Active;

                case CrmNeededResourcesStatusCode.WaitingForAcceptance:
                    return JobOpeningStatus.PendingApproval;

                case CrmNeededResourcesStatusCode.NotReady:
                    return JobOpeningStatus.Draft;
                case CrmNeededResourcesStatusCode.Fullfiled:
                case CrmNeededResourcesStatusCode.ClientResigned:
                case CrmNeededResourcesStatusCode.LegacyCodeActive:
                case CrmNeededResourcesStatusCode.Lost:
                    return JobOpeningStatus.Closed;

                case CrmNeededResourcesStatusCode.OnHold:
                    return JobOpeningStatus.OnHold;

                default:
                    return JobOpeningStatus.Draft;
            }

            throw new CrmException($"Unrecognized needed resources status/state ({statuscode}/{statecode} ({(int)statecode})).");
        }

        public RecruitmentProcessStatus MapToRecruitmentProcessStatus(smt_recruitmentactivityState? statecode)
        {
            return statecode == null
                ? RecruitmentProcessStatus.Draft
                : statecode.Value == smt_recruitmentactivityState.Active
                    ? RecruitmentProcessStatus.Active
                    : RecruitmentProcessStatus.Closed;
        }

        public RecruitmentProcessStepStatus MapToRecruitmentProcessStepStatus(int statusCode)
        {
            switch (statusCode)
            {
                case CrmStatusCodeRa.Active:
                case CrmStatusCodeRa.OnHold:
                    return RecruitmentProcessStepStatus.ToDo;

                case CrmStatusCodeRa.HiredSd:
                case CrmStatusCodeRa.HiredTa:
                case CrmStatusCodeRa.HiredMs:
                case CrmStatusCodeRa.HiredPd:
                case CrmStatusCodeRa.HiredOtherBu:
                case CrmStatusCodeRa.HiredToOtherProject:
                case CrmStatusCodeRa.HiredExternal:
                case CrmStatusCodeRa.HiredOutsourcing:
                case CrmStatusCodeRa.HiredInternalRecruitment:
                case CrmStatusCodeRa.HiredEmbedded:
                case CrmStatusCodeRa.HiredCognitive:
                case CrmStatusCodeRa.HiredQa:
                case CrmStatusCodeRa.HiredMobile:
                case CrmStatusCodeRa.HiredEnterpriseApplications:
                case CrmStatusCodeRa.HiredDigitalPlatforms:
                case CrmStatusCodeRa.HiredProductManagementConsulting:
                case CrmStatusCodeRa.HiredDesignStudioPoland:
                case CrmStatusCodeRa.HiredAutomotive:
                case CrmStatusCodeRa.HiredHiTec:
                case CrmStatusCodeRa.HiredFinancialServices:
                case CrmStatusCodeRa.HiredTelcoMedia:
                case CrmStatusCodeRa.HiredIndustryHealthcare:
                case CrmStatusCodeRa.HiredConsumerServices:
                    return RecruitmentProcessStepStatus.Done;

                default:
                    return RecruitmentProcessStepStatus.Cancelled;
            }
        }

        public ApplicationOriginType MapToJobApplicationOriginType(int? contactSource)
        {
            switch (contactSource)
            {
                case CrmCandidateContactSource.Agency:
                    return ApplicationOriginType.Agency;

                case CrmCandidateContactSource.JobAdvertisment:
                    return ApplicationOriginType.JobAdvertisement;

                case CrmCandidateContactSource.ApplicationWithoutNotice:
                    return ApplicationOriginType.DropOff;

                case CrmCandidateContactSource.Event:
                    return ApplicationOriginType.Event;

                case CrmCandidateContactSource.SocialNetwork:
                    return ApplicationOriginType.SocialNetwork;

                case CrmCandidateContactSource.Recommendation:
                    return ApplicationOriginType.Recommendation;

                case CrmCandidateContactSource.Other:
                default:
                    return ApplicationOriginType.Other;
            }
        }

        public SeniorityLevelEnum MapToSeniorityLevel(int? seniorityLevel)
        {
            switch (seniorityLevel)
            {
                case CrmSeniorityLevel.L1:
                case CrmSeniorityLevel.L1_L2:
                default:
                    return SeniorityLevelEnum.Seniority1;

                case CrmSeniorityLevel.L2:
                case CrmSeniorityLevel.L2_L3:
                    return SeniorityLevelEnum.Seniority2;

                case CrmSeniorityLevel.L3:
                case CrmSeniorityLevel.L3_L4:
                    return SeniorityLevelEnum.Seniority3;

                case CrmSeniorityLevel.L4:
                case CrmSeniorityLevel.L4_L5:
                    return SeniorityLevelEnum.Seniority4;

                case CrmSeniorityLevel.L5:
                    return SeniorityLevelEnum.Seniority5;
            }
        }

        public long? MapToDanteLanguage(int? crmLanguage)
        {
            switch (crmLanguage)
            {
                case (CrmLanguage.English): return 1;
                case (CrmLanguage.French): return 7;
                case (CrmLanguage.German): return 4;
                case (CrmLanguage.Japanese): return 23;
                case (CrmLanguage.Polish): return 2;
                case (CrmLanguage.Portuguese): return 19;
                case (CrmLanguage.Russian): return 8;
                case (CrmLanguage.Spanish): return 5;
                default: return null;
            }
        }
    }
}
