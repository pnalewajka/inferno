﻿namespace Smt.Atomic.Business.CrmMigration.MappingData
{
    public static class CityMapping
    {
        public static long[] MapToDanteCityName(string crmCityName)
        {
            switch (crmCityName)
            {
                case "Biuro Spółki w Lublinie":
                    return new long[] { 9 };

                case "Amsterdam":
                case "Holandia":
                case "Holandia/Niemcy/UK":
                    return new long[] { 48 };

                case "Amsterdam/Wrocław":
                case "Holandia/Wrocław":
                    return new long[] { 48, 14 };

                case "Augsburg":
                    return new long[] { 119 };

                case "Bazylea i Frankfurt":
                    return new long[] { 32, 33 };

                case "Berlin":
                case "Berlin (Germany)":
                case "Berlin / ":
                case "Berlin, Germany":
                    return new long[] { 3 };

                case "Berlin Wrocław":
                    return new long[] { 3, 14 };

                case "Szwajcaria":
                    return new long[] { 120 };

                case "Białystok":
                case "Białystok (ewentualnie Wrocław lub Warszawa)":
                case "Białystok / ":
                case "Cała Polska / Białystok / ":
                    return new long[] { 6 };

                case "Białystok / Lublin / ":
                    return new long[] { 6, 9 };

                case "Białystok / Warszawa / ":
                case "Białystok/Warszawa":
                    return new long[] { 6, 13 };

                case "Białystok / Wrocław / Warszawa / ":
                    return new long[] { 6, 14, 13 };

                case "Białystok / Wrocław Kościuszki / ":
                    return new long[] { 6, 14 };

                case "Białystok / Wrocław / Szczecin / Warszawa / ":
                    return new long[] { 6, 14, 13, 12 };

                case "Białystok, Gliwice, Katowice":
                case "BIałystok/Katowice/Gliwice":
                    return new long[] { 6, 7, 56 };

                case "Bonn":
                case "Bonn (Germany)":
                    return new long[] { 40 };

                case "Bydgoszcz":
                    return new long[] { 85 };

                case "Chorzów":
                case "Chorzów + częściowo zdalnie":
                case "Chorzów lub biuro SMT":
                    return new long[] { 121 };

                case "Belga":
                case "Belgia":
                case "Brussels":
                case "Brussels, Luxembourg, Ispra, Seville, Petten, Geel, Karlsruhe, Dublin":
                    return new long[] { 49 };

                case "Cluj":
                case "Cluj, Romania":
                case "Cluj-Napoca (Rumunia)":
                case "Cluj-Napoca, Romania":
                case "Clujp-Napoca":
                    return new long[] { 52 };

                case "Cologne, Bonn":
                case "Köln, Meckenheim, Bonn":
                    return new long[] { 30, 40 };

                case "Copenhagen (Denmark)":
                case "Dania":
                    return new long[] { 53 };

                case "Darmstadt":
                case "Darmstadt, Germany":
                case "Darmstadt, Niemcy":
                    return new long[] { 122 };

                case "Baden-Dattwil, Switzerland":
                    return new long[] { 123 };

                case "Dublin":
                case "Dublin lub UK":
                case "Dublin/UK":
                case "Irlandia":
                    return new long[] { 124 };

                case "Dusseldorf":
                case "Düsseldorf":
                case "Düsseldorf, Essen":
                    return new long[] { 23 };

                case "Duisburg (Germany), Brno (Czec":
                    return new long[] { 125 };

                case "Dzierżoniów":
                    return new long[] { 126 };

                case "Luxemburg/Echternach":
                    return new long[] { 127 };

                case "Szkocja/UK":
                    return new long[] { 128 };

                case "Ennis":
                    return new long[] { 129 };

                case "Frankfurt":
                case "Kronberg (Germany)":
                case "Rhein Ryuhr area":
                case "Rhein-Mein area (Germany)":
                case "Rhein-Ruhr Area":
                case "Rhein-Ruhr area (Germany)":
                case "Rhine-Ruhr area":
                case "Rhine-Ruhr area, Niemcy":
                case "Rhine-Ruhr,Germany":
                case "Rhin-Main area (Germany)":
                case "Schöneck":
                case "Sulzbach":
                    return new long[] { 33 };

                case "Gdańsk":
                case "Trójmiasto/home office":
                    return new long[] { 82 };

                case "Gdańsk lub Poznań":
                    return new long[] { 82, 11 };

                case "Gdańsk, Kraków, Warszawa":
                    return new long[] { 82, 13, 8 };

                case "Gdynia":
                    return new long[] { 55 };

                case "Gdynia lub Łódź":
                    return new long[] { 55, 10 };

                case "Genova":
                    return new long[] { 130 };

                case "Gliwice":
                    return new long[] { 56 };

                case "Gliwice/Wrocław":
                    return new long[] { 56, 14 };

                case "Gliwice/Wrocław/Warszawa":
                    return new long[] { 56, 14, 13 };

                case "Görlitz, Germany":
                    return new long[] { 131 };

                case "Południowe Niemcy":
                    return new long[] { 37 };

                case "Hanower":
                    return new long[] { 25 };

                case "Heidelberg (Germany)":
                    return new long[] { 132 };

                case "Capital Area, Finland":
                case "Finlandia":
                case "Finlandia / Helsinki":
                case "Helsinki":
                case "Helsinki - Sanomatalo":
                case "Helsinki - Töölönlahdenkatu":
                    return new long[] { 58 };

                case "Helsinki/Wroclaw":
                case "Helsinki/Wrocław":
                    return new long[] { 58, 14 };

                case "Jelenia Góra":
                    return new long[] { 60 };

                case "Katowice":
                case "Katowice / ":
                case "Katowice lub inne":
                case "Katowice, ew. zdalnie":
                case "Śląsk":
                    return new long[] { 7 };

                case "Katowice / Kraków / ":
                    return new long[] { 7, 8 };

                case "Katowice / Lublin / Wrocław / ":
                    return new long[] { 7, 9, 14 };

                case "Katowice / Wrocław Prosta / ":
                    return new long[] { 7, 14 };

                case "Katowice i/lub Szczecin":
                    return new long[] { 7, 12 };

                case "Katowice, Gliwice":
                case "Katowice/Gliwice":
                    return new long[] { 7, 56 };

                case "Katowice/Gliwice/Warszawa":
                    return new long[] { 7, 56, 13 };

                case "Koblenz, Germany":
                    return new long[] { 133 };

                case "Kopenhaga":
                case "Middle Jutland":
                    return new long[] { 134 };

                case "W biurze Spółki w Koszalinie l":
                    return new long[] { 135 };

                case "Crackow":
                case "Karaków":
                case "Karków":
                case "Kolding (Dania) - siedziba klienta PDC}; lub Kraków - biuro PDC":
                case "Krakow":
                case "Kraków":
                case "Kraków ":
                case "Kraków - Zabierzów":
                case "Kraków (lub zdalnie + częste wyjazdy do Krakowa)":
                case "Kraków (ZABIERZÓW)":
                case "Kraków / ":
                case "Kraków lub inne ale najlepiej blisko Krakowa, inaczej konieczność częstych podróży do Krakowa":
                case "Krakw":
                case "Krkaów":
                case "Zabierzów":
                case "Kraków/Zabierzów":
                case "Kraków/ Zabierzów":
                case "Zabierzów/koło Krakowa":
                case "Kraków/Luton":
                    return new long[] { 8 };

                case "Kraków / Berlin / ":
                    return new long[] { 8, 3 };

                case "Kraków / Katowice / ":
                case "Kraków / Katowice / Cała Polska / ":
                    return new long[] { 8, 7 };

                case "Kraków / Wrocław / ":
                case "Kraków / Wrocław Kościuszki / ":
                    return new long[] { 8, 14 };

                case "Kraków / Wrocław / Warszawa / ":
                    return new long[] { 8, 14, 13 };

                case "Kraków, Gdańsk":
                case "Kraków/Gdańsk":
                    return new long[] { 8, 82 };

                case "Kraków, Gliwice, Katowice, zastanawiaja się na Wrocławiem":
                    return new long[] { 8, 7, 56, 14 };

                case "Kraków, Płock, biura SMT":
                    return new long[] { 8, 148 };

                case "Kraków, Wrocław lub Białysto":
                case "Kraków, Wrocław lub Białystok":
                    return new long[] { 8, 14, 6 };

                case "Krk,Wr,Pz,Kat":
                    return new long[] { 8, 14, 11, 7 };

                case "Kraków / Warszawa / ":
                case "Kraków/Warszawa":
                    return new long[] { 8, 13 };

                case "Leeds":
                    return new long[] { 136 };

                case "Południowy - wschód Polski, Rzeszów, Tarnów":
                    return new long[] { 91 };

                case "Anglia / ":
                case "Cambrdidge/jedno z biur Mobica w PL":
                case "Chiswick (UK)":
                case "London":
                case "London ":
                case "London - DK- SE":
                case "London (Central)":
                case "Londyn":
                case "Londyn , UK":
                case "UK":
                case "REading":
                case "United Kingdom":
                    return new long[] { 16 };


                case "Londyn/Wrocław":
                case "UK i PL":
                    return new long[] { 16, 14 };

                case "UK, Warszawa, Bydgoszcz, Łódź, Szczecin":
                    return new long[] { 16, 13, 85, 12, 10 };

                case "UK/Francja":
                    return new long[] { 16, 2 };

                case "UK/Holandia":
                    return new long[] { 16, 48 };

                case "UK/Łódź/":
                    return new long[] { 16, 10 };


                case "Los Angeles":
                    return new long[] { 67 };

                case "Lublin":
                case "Lublin / ":
                case "Lublin + wyjazdy":
                    return new long[] { 9 };

                case "Lublin / Białystok / Warszawa / Wrocław / ":
                    return new long[] { 9, 6, 13, 14 };

                case "Lublin lub Warszawa":
                    return new long[] { 9, 13 };

                case "Łódź":
                case "Łódż":
                case "Łódz / ":
                    return new long[] { 10 };

                case "Łódz / Katowice / ":
                    return new long[] { 10, 7 };

                case "Łódz / Kraków / ":
                    return new long[] { 10, 8 };

                case "Łódź, POZNAŃ":
                    return new long[] { 10, 11 };

                case "Łódz / Warszawa / Wrocław Prosta / Katowice / ":
                    return new long[] { 10, 13, 14, 7 };

                case "Łódz / Warszawa / ":
                case "Łódź lub Warszawa":
                case "Łódź, ew. Warszawa":
                case "Łódź, Warszawa":
                case "Łódź/ Warszawa":
                    return new long[] { 10, 13 };

                case "Madryt/Wrocław/Ljubljana":
                    return new long[] { 137, 14 };

                case "Malaga, Hiszpania":
                    return new long[] { 69 };

                case "Lund":
                case "Lund,Sweden":
                    return new long[] { 138 };

                case "Mannheim,Germany":
                case "Mannheim/Ludwigshafen":
                    return new long[] { 139 };

                case "Mediolan/Włochy":
                    return new long[] { 140 };

                case "Mons Belgia":
                    return new long[] { 141 };

                case "Moskwa":
                    return new long[] { 142 };

                case "greater area of Munich (German":
                case "Monachium":
                case "Monachium i Stuttgart":
                case "Munich, Germany":
                case "Niemcy":
                case "Niemcy (Munich)":
                case "NIEMCY ?":
                case "Niemcy, Rhine-Ruhr area":
                case "Niemcy/Heilbronn":
                    return new long[] { 4 };

                case "Niemcy i Wrocław":
                    return new long[] { 4, 14 };

                case "Naas":
                    return new long[] { 143 };

                case "Noordwijk":
                case "Noordwijk, Holandia":
                    return new long[] { 144 };

                case "Norymberga":
                case "Norymberga, Niemcy":
                case "Nurnberg area (Germany)":
                case "Greater Nuremberg":
                    return new long[] { 44 };

                case "Opole":
                    return new long[] { 90 };

                case "Norwegia":
                    return new long[] { 145 };

                case "Kanada":
                    return new long[] { 146 };

                case "Francja":
                    return new long[] { 2 };

                case "Francja/Łódź/Warszawa lub Bydg":
                    return new long[] { 2, 10, 13, 85 };

                case "Piła":
                    return new long[] { 147 };

                case "Płock":
                    return new long[] { 148 };

                case "Poznan":
                case "Poznań":
                case "Poznań (zmiana z Krakowa)":
                case "Poznań / ":
                case "Poznań/zdalnie":
                case "Poznań/Knutsford":
                    return new long[] { 11 };

                case "Poznań / Katowice / ":
                    return new long[] { 11, 7 };

                case "Poznań / Szczecin / ":
                    return new long[] { 11, 12 };

                case "Poznań / Szczecin / Kraków / Wrocław / ":
                    return new long[] { 11, 12, 8, 14 };

                case "Poznań lub Łódź":
                case "Poznań, Łódź":
                    return new long[] { 11, 10 };


                case "Poznań, Wrocław, Warszawa":
                case "Poznań/Warszawa/Wrocław":
                    return new long[] { 11, 13, 14 };

                case "Poznań lub Wrocław":
                case "Poznań/ Wrocław":
                case "Poznań / Wrocław / ":
                case "Poznań/Wrocław":
                    return new long[] { 11, 14 };

                case "Praga":
                    return new long[] { 79 };

                case "Ravensburg":
                    return new long[] { 5 };

                case "Włochy":
                    return new long[] { 92 };

                case "Korea":
                    return new long[] { 149 };

                case "Sofia- Bułgaria":
                case "Sofia(?)":
                    return new long[] { 150 };

                case "Sopot":
                    return new long[] { 94 };


                case "Southampton":
                case "Southampton, UK":
                case "Southamtpon":
                case "Southhampton":
                case "Sputhampton":
                    return new long[] { 118 };

                case "Southampton na 4-6 tygodni, potem praca z Polski":
                case "6 tygodni w Southampton, potem praca z Polski":
                case "6 tygodni w Southampton, potem Wrocław":
                case "Southampton ( 6 weeks), potem Polska (najlepiej Wrocław)":
                case "Southampton (6 tygodni), potem praca z Polski (najlepiej Wrocław)":
                case "Southampton (najpewniej od końca lutego), potem Polska (najlepiej Wrocław)":
                case "Southampton + Wrocław":
                    return new long[] { 118, 14 };

                case "Stockholm":
                case "Sztokholm":
                case "Szwecja":
                    return new long[] { 15 };

                case "Szwecja/Praga":
                    return new long[] { 15, 79 };

                case "Szwecja/Wrocław":
                    return new long[] { 15, 14 };

                case "Sindelfingen i/lub Lindau":
                case "Stuttgart":
                case "Stuttgart.":
                    return new long[] { 43 };

                case "Stuttgart/Monachium":
                    return new long[] { 43, 4 };

                case "Szczecin":
                case "Szczecin / ":
                case "Szczecin/zdalnie":
                case "Zdalnie + Szczecin":
                case "Szczecin i zdalnie":
                case "Szczecin, Królowej Korony Pols":
                    return new long[] { 12 };

                case "Szczecin / Poznań / ":
                    return new long[] { 12, 11 };

                case "Szczecin / Poznań / Białystok / Wrocław / Lublin / Warszawa / ":
                    return new long[] { 12, 11, 6, 13, 14, 9 };

                case "Szczecin / Poznań / Warszawa / Wrocław / ":
                case "Szczecin / Poznań / Wrocław / Warszawa / ":
                case "Szczecin / Wrocław / Poznań / Warszawa / ":
                    return new long[] { 12, 11, 13, 14 };

                case "Szczecin / Wrocław / ":
                case "Szczecin / Wrocław Prosta / Anywhere / ":
                case "Szczecin/Wrocław":
                    return new long[] { 12, 14 };

                case "Szczecin / Warszawa / ":
                case "Szczecin lub Warszawa":
                case "Szczecin, Warszawa":
                    return new long[] { 12, 13 };

                case "Szczecin/Koszalin/zdalnie":
                    return new long[] { 12, 135 };

                case "Kista/Szwecja":
                    return new long[] { 15 };

                case "Talin/Estonia":
                    return new long[] { 151 };

                case "Toruń":
                    return new long[] { 96 };

                case "Wangen D-88239 Germany":
                case "Wangen D-88239 Germany / Wroclaw (long term we Wrocławiu po kilku miesiącach pracy w Niemczech)":
                case "Wangen im Allgäu, Germany, for the first three months then Wroclaw Viessmann office":
                    return new long[] { 152 };

                case "Dowolna/Zdalnie - chętnie Warszawa":
                case "Pieńków":
                case "Preferowana Warszawa (inne lokalizacje do rozważenia )":
                case "tylko Warszawa":
                case "Warsaw":
                case "Warsaw / SMT office":
                case "najlepiej Warszawa, możliwe inne biuro SMT":
                case "Warszawa":
                case "Warszawa - biuro Philipsa":
                case "Warszawa - biuro SMT lub ich lokalizacja":
                case "Warszawa - nowy Dział DreamLab":
                case "Warszawa / ":
                case "Warszawa lub inne":
                case "Warszawa lub inne biuro SMT":
                case "Warszawa, ul. Ogrodowa oraz Jana Pawła":
                case "Warszawa lub zdalnie":
                case "Warszawa, zagranica":
                case "Warszawa/praca zdalna":
                case "Warszawa/Biuro SMT SA":
                case "Warszawa/Delegacje":
                case "Waw":
                case "Warszawa/zdalnie":
                    return new long[] { 13 };

                case "warszawa/Wrocław":
                case "Warszawa, Wroclaw":
                case "Warszawa, Wrocław":
                case "Warszawa / Wrocław":
                case "Warszawa Wrocław":
                case "Warszawa / Wrocław / ":
                case "Najlepiej Warszawa lub Wrocław":
                case "Warszawa / Wrocław Kościuszki / ":
                case "Warszawa lub Wrocław":
                    return new long[] { 13, 14 };

                case "Olsztyn/Warszawa":
                    return new long[] { 13, 89 };

                case "Warszawa + 1 osoba z W-wa, Wro lub Poz.":
                    return new long[] { 13, 14, 11 };

                case "Warsz/Łódź/Kraków":
                    return new long[] { 13, 10, 8 };

                case "Warszawa / Białystok / ":
                case "Warszawa lub Białystok":
                case "Warszawa, Białystok":
                case "Warszawa/Białystok":
                    return new long[] { 13, 6 };

                case "Warszawa, Wrocław lub Białystok":
                case "Warszawa / Białystok / Wrocław / ":
                    return new long[] { 13, 14, 6 };

                case "Warszawa / Białystok / Wrocław / Łódz / ":
                    return new long[] { 13, 14, 6, 10 };

                case "Warszawa / Białystok / Wrocław / Lublin / ":
                    return new long[] { 13, 14, 6, 9 };

                case "warszawa / Gliwice":
                    return new long[] { 13, 56 };

                case "Warszawa / Katowice":
                case "Warszawa/Katowice":
                    return new long[] { 13, 7 };

                case "Warszawa / Kraków / Wrocław":
                case "Warszawa Wrocław Kraków lub inne":
                case "Warszawa, Kraków lub Wrocław":
                    return new long[] { 13, 14, 8 };

                case "Warszawa / Łódź":
                case "Warszawa lub Łódź":
                case "Warszawa, Łodź":
                case "Warszawa, Łódź":
                    return new long[] { 13, 10 };

                case "Warszawa / Poznań / Katowice / Wrocław / Gdańsk / Gdynia / Sopot / ":
                case "Warszawa / Poznań / Wrocław / Katowice / Gdańsk / Gdynia / Sopot / ":
                case "Warszawa / Wrocław / Poznań / Katowice / Gdańsk / Gdynia / Sopot / ":
                    return new long[] { 13, 14, 82, 55, 11 };

                case "Warszawa / Wrocław / Francja / ":
                    return new long[] { 13, 14, 2 };

                case "Warszawa lub Kraków":
                case "Warszawa, Kraków":
                    return new long[] { 13, 8 };

                case "Warszawa lub Lublin":
                    return new long[] { 13, 9 };

                case "Warszawa lub Szczecin":
                    return new long[] { 13, 12 };

                case "Warszawa lub Toruń":
                case "Warszawa, Toruń":
                    return new long[] { 13, 96 };

                case "Warszawa. ew. Poznań":
                case "Warszawa/Poznań":
                    return new long[] { 13, 11 };

                case "Warszawa/Białystok/KAtowice/Gl":
                    return new long[] { 13, 6, 7, 56 };

                case "Warszawa/Białystok/Poznań":
                    return new long[] { 13, 6, 11 };

                case "Warszawa/Bydgoszcz":
                    return new long[] { 13, 85 };

                case "Warszawa/Bydgoszcz/Łódź":
                case "Warszawa/Łódź/Bydgoszcz":
                    return new long[] { 13, 85, 10 };

                case "Warszawa/Bydgoszcz/UK":
                    return new long[] { 13, 85, 16 };

                case "Warszawa/Dublin":
                    return new long[] { 13, 124 };

                case "Warszawa/Gliwice/Katowice":
                    return new long[] { 13, 56, 7 };

                case "Warszawa/Krakow/Wroclaw":
                    return new long[] { 13, 14, 8 };
                case "Warszawa/Łódź":
                case "Warszawa/Łódź/Belfast":
                    return new long[] { 13, 10 };

                case "warszawa/Wrocław/Gliwice":
                    return new long[] { 13, 14, 56 };

                case "Warszawa/Wrocław/Pozań":
                    return new long[] { 13, 14, 11 };

                case "Warszawa/zielona Góra":
                    return new long[] { 13, 101 };


                case "USA":
                case "USA/Europa":
                    return new long[] { 13, 110 };

                case "Austria":
                case "Wiedeń":
                case "Wien":
                    return new long[] { 153 };

                case "Wolfsburg":
                    return new long[] { 154 };

                case "Austria/Wrocław":
                case "Bielany Wrocławskie":
                case "biuro SMT":
                case "biuro SMT Wrocław":
                case "preferowany Wrocław":
                case "Wocław":
                case "Wrcław":
                case "Wrocław Prosta / ":
                case "Wrocław Prosta / Anywhere / ":
                case "Wroclaw, biuro SMT":
                case "Wrocław, biuro Viessmann (ul. Duńska 9, WPT)":
                case "Wrocław, GD":
                case "Wrocław, Green Day":
                case "Wrcoław":
                case "Wrocław (only)":
                case "Wrocław / ":
                case "Wrocław / Anywhere / ":
                case "Wroclaw":
                case "Wrocław":
                case "wrocłąw":
                case "Wrocław / Cała Polska / ":
                case "Wroclaw / dowolnie":
                case "Wrocław / dowolnie":
                case "Wrocław / Wrocław / ":
                case "Wrocław / Wrocław Prosta / ":
                case "Wrocław Kościuszki / ":
                case "Wrocław lub inne biuro SMT":
                case "Wrocław lub inne biuro w PL":
                case "Wrocław lub inne miasta":
                case "Wrocław/zagranica":
                case "Wrocław/zdalnie":
                case "Wrosław":
                case "Wrocław / TBD":
                case "Wrocław/or other":
                case "Wrocław, praca zdalna z biura":
                case "Wrocław, ul. Duńska (WPT)":
                case "Wrocław, ul. Duńska 9":
                case "Wrocław + Knudsford":
                    return new long[] { 14 };

                case "Wrocław (2 osoby)/Warszawa(1 osoba)":
                case "Wrocław / Warszawa":
                case "Wrocław / Warszawa / ":
                case "Wrocław, Warszawa":
                case "Wrocław ewentualnie Warszawa":
                case "Wrocław Kościuszki / Warszawa / ":
                case "Wrocław i Warszawa":
                case "Wroclaw/Warszawa":
                case "Wrocław-Warszawa (względnie inne)":
                case "Wrocław/Warszawa":
                case "Wrocław lub Warszawa":
                case "Wrocław/Warszawa/zdalnie":
                case "Wrocław lub Warszawa lub każde inne miasto":
                    return new long[] { 14, 13 };

                case "Wrocław (ewentualnie Białystok, w ostateczności Warszawa)":
                    return new long[] { 14, 13, 6 };

                case "Wrocław / Białystok / ":
                    return new long[] { 14, 6 };

                case "Wrcoław/Szczecin":
                    return new long[] { 14, 12 };

                case "Wrocław / Białystok / Katowice / Poznań / Kraków / ":
                    return new long[] { 14, 6, 7, 11, 8 };

                case "Wrocław / Białystok / Poznań / Kraków / Szczecin / ":
                    return new long[] { 14, 6, 7, 11, 12 };

                case "Wrocław / Katowice / ":
                case "Wrocław i Katowice":
                case "Wrocław/Katowice":
                case "Wrocław Prosta / Katowice / ":
                case "Wrocław, Katowice":
                case "Wrocław lub Katowice":
                case "Wrocław/ Katowice":
                    return new long[] { 14, 7 };

                case "Wrocław or Krakow":
                case "Wroclaw or Krakow (if must be - other)":
                case "Wrocław or Krakow (other if must be)":
                case "Wrocław or Kraków (other if must be)":
                case "Wrocław or Kraków (preffered, other if needed)":
                case "Wrocław / Kraków / ":
                    return new long[] { 14, 8 };

                case "Wrocław / Kraków / Katowice":
                    return new long[] { 14, 7 };

                case "Wrocław / Kraków / Katowice / Poznań / ":
                    return new long[] { 14, 7, 8, 11 };

                case "Wrocław / Kraków / Łódz / Szczecin / ":
                    return new long[] { 14, 8, 10, 12 };

                case "Wrocław / Kraków / Szczecin / ":
                    return new long[] { 14, 8, 12 };

                case "Wrocław / Kraków / USA / ":
                    return new long[] { 14, 8, 110 };

                case "Wrocław / Kraków / Warszawa / Białystok / ":
                    return new long[] { 14, 8, 13, 6 };

                case "Wrocław / Londyn":
                    return new long[] { 14, 16 };

                case "Wrocław / Poznań / ":
                    return new long[] { 14, 11 };

                case "Wrocław / Szczecin / ":
                case "Wrocław lub Szczecin":
                    return new long[] { 14, 12 };

                case "Wrocław / Szczecin / Kraków / ":
                    return new long[] { 14, 12, 8 };

                case "Wrocław / Szczecin / Lublin / ":
                    return new long[] { 14, 12, 9 };

                case "Wrocław / Szczecin / Poznań / ":
                    return new long[] { 14, 12, 9 };

                case "Wrocław / Szczecin / Poznań / Kraków / Warszawa / ":
                    return new long[] { 14, 12, 11, 8, 13 };

                case "Wrocław / USA / ":
                    return new long[] { 14, 110 };

                case "Wrocław / Warszawa / Anywhere / Białystok / Łódz / ":
                    return new long[] { 14, 13, 6, 10 };

                case "Wrocław / Warszawa / Białystok / ":
                    return new long[] { 14, 13, 6 };

                case "Wrocław / Warszawa / Kraków / ":
                    return new long[] { 14, 13, 8 };

                case "Wrocław / Warszawa / Szczecin / ":
                    return new long[] { 14, 13, 12 };

                case "Wrocław / Warszawa / Szczecin / Białystok / ":
                    return new long[] { 14, 13, 12, 6 };

                case "Wrocław + 2-3 miesiace w Niemczech":
                case "Wrocław + Niemcy":
                    return new long[] { 14, 3 };

                case "Wrocław + 6 tygodni w Southampton":
                    return new long[] { 14, 118 };

                case "Wrocław + Biuro klienta w NL":
                    return new long[] { 14, 48 };

                case "Wrocław + Poznań":
                case "Wrocław lub/i Poznań":
                case "Wrocław, Poznań":
                case "Wrocław/Poznań":
                case "Wrocław lub Poznań":
                    return new long[] { 14, 11 };


                case "Wrocław i Łódź":
                    return new long[] { 14, 10 };

                case "Wroclaw i Londyn":
                    return new long[] { 14, 16 };

                case "Wrocław i Szwecja":
                    return new long[] { 14, 15 };

                case "Wrocław Kościuszki / Kraków / Szczecin / ":
                    return new long[] { 14, 12, 8 };

                case "Wrocław Kościuszki / Kraków / Warszawa / Szczecin / Poznań / ":
                    return new long[] { 14, 8, 13, 12, 11 };

                case "Wrocław Kościuszki / Szczecin / ":
                    return new long[] { 14, 12 };

                case "Wrocław, Białystok, Kraków":
                    return new long[] { 14, 6, 8 };

                case "Wrocław, Gdańsk, Kraków, Warszawa":
                    return new long[] { 14, 8, 13, 82 };

                case "Wrocław, Katowice, Kraków, Poznan":
                    return new long[] { 14, 7, 8, 11 };

                case "Wrocław, Katowice, Kraków, Warszawa":
                    return new long[] { 14, 7, 8, 13 };

                case "Wrocław, Koszalin,Zielona Góra":
                    return new long[] { 14, 135, 101 };

                case "Wrocław, Kraków, Warszawa, Trójmiasto":
                    return new long[] { 14, 8, 13, 82 };

                case "Wroclaw, Poland / London, UK":
                    return new long[] { 14, 16 };

                case "Wrocław, Szczecin":
                    return new long[] { 14, 12 };


                case "Wrocław, Warszawa, Kraków":
                    return new long[] { 14, 13, 8 };

                case "Wrocław/ Bruksela":
                    return new long[] { 14, 49 };

                case "Wrocław/ Finladnia":
                    return new long[] { 14, 58 };

                case "Wrocław/Frankfurt/Monachim":
                case "Wrocław/Monachium/Frankfurt":
                case "Wrocław/Frankfurt/Monachium":
                    return new long[] { 14, 4, 33 };

                case "Wrocław/Gliwice":
                case "Wrocław/Gliwice (opcjonalnie)":
                    return new long[] { 14, 56 };


                case "Wrocław/Gliwice/Katowice":
                case "Wrocław/Kato/Gliwice":
                case "Wrocław/Katowice/Gliwcie":
                    return new long[] { 14, 56, 7 };

                case "Wrocław/Gliwice/Warszawa":
                case "Wrocław/Warszawa/Gliwice":
                    return new long[] { 14, 56, 13 };

                case "Wrocław/Katowice/Gliwice/Warsz":
                    return new long[] { 14, 56, 7, 13 };

                case "Wrocław/Katowice/Poznań":
                    return new long[] { 14, 11, 7 };

                case "Wrocław/Koszalin/Zielona Góra":
                    return new long[] { 14, 135, 101 };

                case "Wrocław/Kraków":
                    return new long[] { 14, 8 };

                case "Wrocław/Poznań/Warszawa":
                    return new long[] { 14, 13, 11 };

                case "Wrocław/Szczecin":
                    return new long[] { 14, 12 };

                case "Wrocław/Szczecin/Koszalin/Zielona Góra":
                    return new long[] { 14, 12, 135, 101 };

                case "Wrocław/Szwecja":
                    return new long[] { 14, 15 };

                case "Wrocław/Warszawa/Gliwice/Pozna":
                    return new long[] { 14, 13, 56, 11 };

                case "Wrocław/Warszawa/Katowice":
                    return new long[] { 14, 13, 7 };

                case "Wrocław/Warszawa/Katowice/Gliw":
                    return new long[] { 14, 13, 7, 56 };


                case "Wrocław/Zurich":
                    return new long[] { 14, 26 };



                case "Polska":
                case "SMT":
                case "SMT office in Poland":
                    return new long[] { 13 };

                case "Wuppertal":
                    return new long[] { 24 };

                case "Ząbkowice Śląskie":
                    return new long[] { 155 };

                case "Zduńska Wola":
                    return new long[] { 156 };

                case "Zurych":
                case "Zurych, Szwajcaria":
                case "Zurych/Szwajcaria":
                    return new long[] { 26 };

                default:
                    return null;
            }
        }
    }
}
