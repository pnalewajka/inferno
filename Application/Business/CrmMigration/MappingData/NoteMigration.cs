﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.CrmMigration.Jobs;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CrmMigration.MappingData
{
    public class NoteMigration
    {
        private const int NoteLength = 4000;

        public static void MigrateJobOpeningNote(
            IJobOpeningNoteCardIndexDataService cardIndexDataService,
            IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork,
            Annotation annotation,
            SystemUser noteCreator,
            long jobOpeningId)
        {
            var userId = BaseMigration.GetUserId(noteCreator?.smt_Id_Dante, extendedUnitOfWork);
            var NoteText = AppendCreatorNameIfNotInDante(!userId.HasValue, noteCreator) + annotation.NoteText;

            for (int i = 0; i < (NoteText.Length / NoteLength); i++)
            {
                var content = TruncateNote(NoteText, i);

                var noteDto = new JobOpeningNoteDto
                {
                    JobOpeningId = jobOpeningId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = content,
                    CreatedOn = annotation.CreatedOn ?? SqlDateTime.MinValue.Value,
                    ImpersonatedCreatedById = userId,
                    CreatedByFullName = annotation.CreatedBy.Name,
                };

                var addedRecord = cardIndexDataService.AddRecord(noteDto);

                UpdateCreationAndModificationData(userId, annotation.CreatedOn.Value, userId,
                    annotation.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "JobOpeningNote");
            }
        }

        public static void MigrateCandidateNote(
            ICandidateNoteCardIndexDataService cardIndexDataService,
            IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork,
            Annotation annotation,
            SystemUser noteCreator,
            long candidateId)
        {
            var userId = BaseMigration.GetUserId(noteCreator?.smt_Id_Dante, extendedUnitOfWork);
            var NoteText = AppendCreatorNameIfNotInDante(!userId.HasValue, noteCreator) + annotation.NoteText;

            for (int i = 0; i <= (NoteText.Length / NoteLength); i++)
            {
                var content = TruncateNote(NoteText, i);

                var noteDto = new CandidateNoteDto
                {
                    CandidateId = candidateId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = content,
                    ImpersonatedCreatedById = userId,
                    CreatedByFullName = annotation.CreatedBy.Name,
                };
                var addedRecord = cardIndexDataService.AddRecord(noteDto);

                UpdateCreationAndModificationData(userId, annotation.CreatedOn.Value, userId,
                    annotation.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "CandidateNote");
            }
        }

        public static List<AddRecordResult> AddCandidateNote(
            ICandidateNoteCardIndexDataService cardIndexDataService,
            long candidateId,
            string content)
        {
            var newCandidateNotes = new List<AddRecordResult>();
            var numberOfPartOfNote = (content.Length / NoteLength);

            for (int i = 0; i <= numberOfPartOfNote; i++)
            {
                var trimedContent = TruncateNote(content, i);

                var noteDto = new CandidateNoteDto
                {
                    CandidateId = candidateId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = trimedContent,
                };

                newCandidateNotes.Add(cardIndexDataService.AddRecord(noteDto));
            }

            return newCandidateNotes;
        }

        public static void MigrateRecruitmentProcessNote(
           IRecruitmentProcessNoteCardIndexDataService cardIndexDataService,
           IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork,
           Annotation annotation,
           SystemUser noteCreator,
           long recruitmentProcessId)
        {
            var userId = BaseMigration.GetUserId(noteCreator?.smt_Id_Dante, extendedUnitOfWork);
            var NoteText = AppendCreatorNameIfNotInDante(!userId.HasValue, noteCreator) + annotation.NoteText;

            for (int i = 0; i <= (NoteText.Length / NoteLength); i++)
            {
                var content = TruncateNote(NoteText, i);

                var noteDto = new RecruitmentProcessNoteDto
                {
                    RecruitmentProcessId = recruitmentProcessId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = content,
                    CreatedOn = annotation.CreatedOn ?? SqlDateTime.MinValue.Value,
                    ImpersonatedCreatedById = userId,
                    CreatedByFullName = annotation.CreatedBy.Name,
                };

                var addedRecord = cardIndexDataService.AddRecord(noteDto);

                UpdateCreationAndModificationData(userId, annotation.CreatedOn.Value, userId,
                    annotation.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "RecruitmentProcessNote");
            }
        }

        private static string AppendCreatorNameIfNotInDante(bool shouldAppend, SystemUser crmUser)
        {
            if (shouldAppend && crmUser != null)
            {
                return $"[{crmUser.FirstName},{crmUser.LastName}] ";
            }

            return string.Empty;
        }


        public static void MigrateCandidatePost(ICandidateNoteCardIndexDataService candidateNoteCardIndexDataService, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork, Post post, SystemUser postCreator, long danteCandidateId)
        {
            var userId = BaseMigration.GetUserId(postCreator?.smt_Id_Dante, extendedUnitOfWork);
            var postText = AppendCreatorNameIfNotInDante(!userId.HasValue, postCreator) + post.Text;

            for (int i = 0; i <= (postText.Length / NoteLength); i++)
            {
                var content = TruncateNote(postText, i);

                var noteDto = new CandidateNoteDto
                {
                    CandidateId = danteCandidateId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = content,
                    ImpersonatedCreatedById = userId,
                    CreatedByFullName = post.CreatedBy.Name,
                };
                var addedRecord = candidateNoteCardIndexDataService.AddRecord(noteDto);

                UpdateCreationAndModificationData(userId, post.CreatedOn.Value, userId,
                    post.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "CandidateNote");
            }
        }

        private static string TruncateNote(string note, int iteration)
        {
            return (iteration + 1) * NoteLength >= note.Length
                    ? note.Substring(iteration * NoteLength)
                    : note.Substring(iteration * NoteLength, NoteLength);
        }

        public static void UpdateCreationAndModificationData(long? userIdCreatedBy, DateTime? createdOn, long? userIdModifiedBy, DateTime? modifiedOn, long entityId,
            IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork, string tableName)
        {
            var sql = new StringBuilder($"UPDATE Recruitment.{tableName} SET CreatedOn = '{createdOn?.ToString("s")}', ModifiedOn = '{modifiedOn?.ToString("s")}'");

            if (userIdCreatedBy != null)
            {
                sql.Append($", UserIdCreatedBy = { userIdCreatedBy}");
            }

            if (userIdModifiedBy != null)
            {
                sql.Append($", UserIdModifiedBy = { userIdModifiedBy}");
            }

            sql.Append($" where id = { entityId } ");
            extendedUnitOfWork.ExecuteSqlCommand(sql.ToString());
        }

        public static void MigrateJobOpeningPost(IJobOpeningNoteCardIndexDataService jobOpeningNoteCardIndexDataService, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork, Post post, SystemUser postCreator, long jobOpeningId)
        {
            var userId = BaseMigration.GetUserId(postCreator?.smt_Id_Dante, extendedUnitOfWork);
            var postText = AppendCreatorNameIfNotInDante(!userId.HasValue, postCreator) + post.Text;

            for (int i = 0; i <= (postText.Length / NoteLength); i++)
            {
                var content = TruncateNote(postText, i);

                var noteDto = new JobOpeningNoteDto
                {
                    JobOpeningId = jobOpeningId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = content,
                    ImpersonatedCreatedById = userId,
                    CreatedByFullName = post.CreatedBy.Name,
                };
                var addedRecord = jobOpeningNoteCardIndexDataService.AddRecord(noteDto);

                UpdateCreationAndModificationData(userId, post.CreatedOn.Value, userId,
                    post.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "JobOpeningNote");
            }
        }

        public static void MigrateRecruitmentProcessPost(IRecruitmentProcessNoteCardIndexDataService recruitmentProcessNoteCardIndexDataService, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork, Post post, SystemUser postCreator, long recruitmentProcessId)
        {
            var userId = BaseMigration.GetUserId(postCreator?.smt_Id_Dante, extendedUnitOfWork);
            var postText = AppendCreatorNameIfNotInDante(!userId.HasValue, postCreator) + post.Text;

            for (int i = 0; i <= (postText.Length / NoteLength); i++)
            {
                var content = TruncateNote(postText, i);

                var noteDto = new RecruitmentProcessNoteDto
                {
                    RecruitmentProcessId = recruitmentProcessId,
                    NoteType = NoteType.UserNote,
                    NoteVisibility = NoteVisibility.RecruitmentTeamOnly,
                    Content = content,
                    ImpersonatedCreatedById = userId,
                    CreatedByFullName = post.CreatedBy.Name,
                };
                var addedRecord = recruitmentProcessNoteCardIndexDataService.AddRecord(noteDto);

                UpdateCreationAndModificationData(userId, post.CreatedOn.Value, userId,
                    post.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "RecruitmentProcessNote");
            }
        }
    }
}