﻿using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using Microsoft.Xrm.Client;
using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Business.CrmMigration.Helpers;
using Smt.Atomic.Business.CrmMigration.Jobs;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CrmMigration.MappingData
{
    public class RecruitmentProcessMapping
    {
        private readonly Dictionary<string, JobTitle> _jobTitles;
        private readonly Dictionary<string, Location> _locations;
        private readonly CrmOrganizationServiceContext _context;
        private readonly ConversionHelper _conversionHelper;
        private readonly MappingDataHelper _mappingDataHelper;

        public RecruitmentProcessMapping(
            Dictionary<string, JobTitle> jobTitles,
            Dictionary<string, Location> locations,
            CrmOrganizationServiceContext context,
            ConversionHelper conversionHelper,
            MappingDataHelper mappingDataHelper)
        {
            _jobTitles = jobTitles;
            _locations = locations;
            _context = context;
            _conversionHelper = conversionHelper;
            _mappingDataHelper = mappingDataHelper;
        }
        
        public RecruitmentProcessDto GetRecruitmentProcessDto(smt_recruitmentactivity recruitmentactivity, Candidate candidate, JobOpening jobOpening, IUnitOfWork<IRecruitmentDbScope> recruitmentScope, long defaultEmployeeId, RecruitmentProcessStepHelper recruitmentProcessHelper)
        {
            var crmJobPosition = recruitmentactivity.smt_jobposition?.Name;

            var crmCity = recruitmentactivity.smt_cityid?.Name;
            var contractType = _mappingDataHelper.MapToFinalContractType(recruitmentactivity.smt_contracttype?.Value);
            var crmOwningUser = _context.CreateQuery<SystemUser>().SingleOrDefault(u => u.Id == recruitmentactivity.OwningUser.Id);

            var recruitmentProcess = new RecruitmentProcessDto
            {
                CandidateId = candidate.Id,
                CandidateName = recruitmentactivity.smt_Candidate.Name,
                JobOpeningId = jobOpening.Id,
                RecruiterId = BaseMigration.GetEmployee(crmOwningUser?.smt_Id_Dante, recruitmentScope, defaultEmployeeId).Id,
                PositionName = crmJobPosition,
                CityId = CityMapping.MapToDanteCityName(crmCity)?.First(),
                Status = _mappingDataHelper.MapToRecruitmentProcessStatus(recruitmentactivity.statecode),
                ResumeShownToClientOn = recruitmentactivity.smt_DateIntroducedtotheClient,
                ClosedOn = recruitmentactivity.smt_D,
                ClosedReason = _mappingDataHelper.MapToRecruitmentProcessClosedReason(recruitmentactivity.statuscode.Value),
                ClosedComment = recruitmentactivity.smt_deactivatecomment,
                ModifiedOn = recruitmentactivity.ModifiedOn ?? SqlDateTime.MinValue.Value,
                WatcherIds = new long[0],
                CrmId = recruitmentactivity.Id
            };

            if (recruitmentactivity.smt_stagename == null)
            {
                return recruitmentProcess;
            }

            var currentStepType = _conversionHelper.GetValue(recruitmentProcessHelper.GetRecruitmentProcessStepDictionary(jobOpening.CustomerId), recruitmentactivity.smt_stagename, nameof(recruitmentactivity.smt_stagename), recruitmentactivity.Id);
            
            if ((int)currentStepType > (int)RecruitmentProcessStepType.HrInterview)
            {
                FillScrenningData(recruitmentProcess, recruitmentactivity);
            }

            if (recruitmentactivity.smt_stagename == CrmRecruitmentActivityStageName.Offer || recruitmentactivity.smt_stagename == CrmRecruitmentActivityStageName.Negotiation)
            {
                FillOfferData(recruitmentProcess, recruitmentactivity);
            }

            if (recruitmentactivity.smt_stagename == CrmRecruitmentActivityStageName.Negotiation)
            {
                FillFinalData(recruitmentProcess, recruitmentactivity);
            }

            return recruitmentProcess;
        }

        private void FillFinalData(RecruitmentProcessDto recruitmentProcess, smt_recruitmentactivity recruitmentactivity)
        {
            recruitmentProcess.FinalPositionId = recruitmentProcess.OfferPositionId;
            recruitmentProcess.FinalTrialContractTypeId = null;
            recruitmentProcess.FinalLongTermContractTypeId = _mappingDataHelper.MapToFinalContractType(recruitmentactivity.smt_contracttype?.Value);
            recruitmentProcess.FinalTrialSalary = null;
            recruitmentProcess.FinalLongTermSalary = recruitmentactivity.smt_sallary?.Value.ToInvariantString();
            recruitmentProcess.FinalStartDate = recruitmentactivity.smt_StartDate;
            recruitmentProcess.FinalLocationId = _conversionHelper.GetValue(_locations, recruitmentactivity.smt_cityid?.Name, nameof(recruitmentactivity.smt_cityid), recruitmentactivity.Id)?.Id;
            recruitmentProcess.FinalComment = null;

            if (recruitmentactivity.statuscode.Value != CrmStatusCodeRa.RejectedOffer)
            {
                return;
            };

            recruitmentProcess.ClosedComment = recruitmentactivity.smt_offerrejectedreason;
            recruitmentProcess.Status = RecruitmentProcessStatus.Closed;
            recruitmentProcess.ClosedReason = RecruitmentProcessClosedReason.CandidateResigned;
        }

        private void FillOfferData(RecruitmentProcessDto recruitmentProcess, smt_recruitmentactivity recruitmentactivity)
        {
            recruitmentProcess.OfferPositionId = _mappingDataHelper.MapToJobTitleId(recruitmentactivity.smt_jobposition?.Name, _jobTitles);
            recruitmentProcess.OfferContractType = _mappingDataHelper.MapToOfferContractType(recruitmentactivity.smt_contracttype?.Value);
            recruitmentProcess.OfferSalary = _conversionHelper.SubstringString(recruitmentactivity.smt_Offer, 150, nameof(recruitmentactivity.smt_Offer));
            recruitmentProcess.OfferStartDate = _conversionHelper.SubstringString(recruitmentactivity.smt_startdatetxt, 130, nameof(recruitmentactivity.smt_startdatetxt));
            recruitmentProcess.OfferLocationId = _conversionHelper.GetValue(_locations, recruitmentactivity.smt_cityid?.Name, nameof(recruitmentactivity.smt_cityid), recruitmentactivity.Id)?.Id;
            recruitmentProcess.OfferComment = _conversionHelper.SubstringString(recruitmentactivity.smt_Comments, 500, nameof(recruitmentactivity.smt_Comments));
        }

        private void FillScrenningData(RecruitmentProcessDto recruitmentProcess, smt_recruitmentactivity recruitmentactivity)
        {
            recruitmentProcess.ScreeningMotivation = _conversionHelper.SubstringString(recruitmentactivity.smt_Motivation, 500, nameof(recruitmentactivity.smt_Motivation));
            recruitmentProcess.ScreeningLanguageEnglish = _conversionHelper.SubstringString(recruitmentactivity.smt_English, 300, nameof(recruitmentactivity.smt_English));
            recruitmentProcess.ScreeningContractExpectations = _conversionHelper.SubstringString(recruitmentactivity.smt_Financialexpectations, 500, nameof(recruitmentactivity.smt_Financialexpectations));
            recruitmentProcess.ScreeningAvailability = _conversionHelper.SubstringString(recruitmentactivity.smt_WhenAvailable, 400, nameof(recruitmentactivity.smt_WhenAvailable));
            recruitmentProcess.ScreeningBusinessTripsCanDo = recruitmentactivity.smt_Travel == "yes";
            recruitmentProcess.ScreeningEveningWorkCanDo = recruitmentactivity.smt_EveningWork == "yes";
            recruitmentProcess.ScreeningWorkplaceExpectations = null;
            recruitmentProcess.ScreeningOtherProcesses = null;
            recruitmentProcess.ScreeningCounterOfferCriteria = null;
            recruitmentProcess.ScreeningRelocationCanDo = null;
            recruitmentProcess.ScreeningRelocationComment = null;
            recruitmentProcess.ScreeningBusinessTripsComment = null;
            recruitmentProcess.ScreeningWorkPermitNeeded = null;
            recruitmentProcess.ScreeningWorkPermitComment = null;
            recruitmentProcess.ScreeningLanguageGerman = null;
            recruitmentProcess.ScreeningLanguageOther = null;
            recruitmentProcess.ScreeningSkills = null;
            recruitmentProcess.ScreeningEveningWorkComment = null;
            recruitmentProcess.ScreeningGeneralOpinion = null;
        }
    }
}
