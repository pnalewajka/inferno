﻿namespace Smt.Atomic.Business.CrmMigration.MappingData
{
    public class CandidateFileMigrationDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public byte[] Content { get; set; }
    }
}
