﻿using System.IO;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.CrmMigration.MappingData
{
    public class FileMigration
    {
        private readonly ITemporaryDocumentPromotionService _temporaryDocumentPromotionService;

        public FileMigration(ITemporaryDocumentPromotionService temporaryDocumentPromotionService)
        {
            _temporaryDocumentPromotionService = temporaryDocumentPromotionService;
        }

        public CandidateFileDto MigrateCandidateFile(CandidateFileMigrationDto file, RecruitmentDocumentType documentType, long candidateId)
        {
            var temporaryDocumentId = _temporaryDocumentPromotionService.CreateTemporaryDocument(file.Name, file.Type, file.Size, null);

            using (var stream = new MemoryStream(file.Content))
            {
                _temporaryDocumentPromotionService.AddChunk(temporaryDocumentId, stream);
            }

            _temporaryDocumentPromotionService.CompleteTemporaryDocument(temporaryDocumentId);

            return new CandidateFileDto
            {
                CandidateId = candidateId,
                File = new DocumentDto[]
                {
                    new DocumentDto
                    {
                        ContentType = file.Type,
                        DocumentName = file.Name,
                        TemporaryDocumentId = temporaryDocumentId
                    }
                },
                FileSource = FileSource.Disk,
                DocumentType = documentType
            };
        }
    }
}
