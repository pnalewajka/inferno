﻿using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Business.CrmMigration.Steps
{
    public class HiringManagerResumeApprovalStep : IStep
    {
        public RecruitmentProcessStepType Type => RecruitmentProcessStepType.HiringManagerResumeApproval;

        public RecruitmentProcessStepDto Generate(smt_recruitmentactivity ra, bool lastStep)
        {
            var decision = ra.smt_slm_cv_decision?.Value == CrmDecision.SlmApproved;

            return new RecruitmentProcessStepDto
            {
                Type = Type,
                Status = lastStep
                    ? RecruitmentProcessStepStatus.ToDo
                    : decision
                        ? RecruitmentProcessStepStatus.Done
                        : RecruitmentProcessStepStatus.Cancelled,
                Decision = lastStep
                    ? RecruitmentProcessStepDecision.None
                    : decision
                        ? RecruitmentProcessStepDecision.Positive
                        : RecruitmentProcessStepDecision.Negative,
            };
        }

    }
}
