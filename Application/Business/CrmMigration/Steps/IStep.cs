﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Business.CrmMigration.Steps
{
    public interface IStep
    {
        RecruitmentProcessStepType Type { get; }

        RecruitmentProcessStepDto Generate(smt_recruitmentactivity ra, bool lastStep);

    }
}
