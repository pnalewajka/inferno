﻿using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Business.CrmMigration.Steps
{
    public class ContractNegotiationsStep : IStep
    {
        public RecruitmentProcessStepType Type => RecruitmentProcessStepType.ContractNegotiations;


        public RecruitmentProcessStepDto Generate(smt_recruitmentactivity ra, bool lastStep)
        {
            var decision = ra.smt_offeraccepted?.Value == CrmDecision.ClientApproved;

            return new RecruitmentProcessStepDto
            {
                Type = Type,
                Decision = decision ? RecruitmentProcessStepDecision.Positive :
                    RecruitmentProcessStepDecision.None,
                Status = decision ? RecruitmentProcessStepStatus.Done
                    : RecruitmentProcessStepStatus.ToDo,
                StepDetails = null
            };
        }

    }
}
