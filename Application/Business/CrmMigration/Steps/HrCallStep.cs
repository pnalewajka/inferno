﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Business.CrmMigration.Steps
{
    public class HrCallStep : IStep
    {
        public RecruitmentProcessStepType Type => RecruitmentProcessStepType.HrCall;

        public RecruitmentProcessStepDto Generate(smt_recruitmentactivity ra, bool lastStep)
        {
            return new RecruitmentProcessStepDto
            {
                Type = Type,
                Status = RecruitmentProcessStepStatus.Done,
                Decision = RecruitmentProcessStepDecision.Positive,
            };
        }

    }
}
