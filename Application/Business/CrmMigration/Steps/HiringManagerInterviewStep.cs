﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Business.CrmMigration.Steps
{
    public class HiringManagerInterviewStep : IStep
    {
        public RecruitmentProcessStepType Type => RecruitmentProcessStepType.HiringManagerInterview;

        public RecruitmentProcessStepDto Generate(smt_recruitmentactivity ra, bool lastStep)
        {
            return new RecruitmentProcessStepDto
            {
                Type = Type,
                Status = lastStep ? RecruitmentProcessStepStatus.ToDo : RecruitmentProcessStepStatus.Done,
                Decision = lastStep ? RecruitmentProcessStepDecision.None : RecruitmentProcessStepDecision.Positive,
            };
        }
    }
}
