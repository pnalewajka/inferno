﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Castle.Core.Logging;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.CrmMigration.Helpers;
using Smt.Atomic.Business.CrmMigration.MappingData;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CrmMigration.Jobs
{
    [Identifier("Job.Recruitment.JobOpeningMigration")]
    [JobDefinition(
            Code = "JobOpeningMigration",
            Description = "Migrates job openings from Dynamics Crm to Dante Recruitment Module",
            ScheduleSettings = "0 0 1 1 * *",
            PipelineName = PipelineCodes.Recruitment,
            MaxExecutioPeriodInSeconds = 86400)]
    public class JobOpeningMigration : BaseMigration, IJob
    {
        private readonly IJobOpeningNoteCardIndexDataService _jobOpeningNoteCardIndexDataService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _recruitmentUnitOfWorkService;
        private readonly IJobOpeningCardIndexDataService _jobOpeningCardIndexDataService;
        private readonly ISettingsProvider _settingsProvider;
        private readonly CrmConnection _crmConnection;
        private readonly ConversionHelper _conversionHelper;
        private readonly MappingDataHelper _mappingDataHelper;

        public JobOpeningMigration(ILogger logger,
            ICrmConnectionStringProvider crmConnectionStringProvider,
            IJobOpeningNoteCardIndexDataService jobOpeningNoteCardIndexDataService,
            IUnitOfWorkService<IRecruitmentDbScope> recruitmentUnitOfWorkService,
            IJobOpeningCardIndexDataService jobOpeningCardIndexDataService,
            ISettingsProvider settingsProvider) : base(logger, crmConnectionStringProvider)
        {
            _jobOpeningNoteCardIndexDataService = jobOpeningNoteCardIndexDataService;
            _recruitmentUnitOfWorkService = recruitmentUnitOfWorkService;
            _jobOpeningCardIndexDataService = jobOpeningCardIndexDataService;
            _settingsProvider = settingsProvider;

            var connectionString = _connectionStringProvider.GetConnectionString();
            _crmConnection = CrmConnection.Parse(connectionString);

            _conversionHelper = new ConversionHelper(_logger);
            _mappingDataHelper = new MappingDataHelper(_logger);
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                _logger.Info($"Create connections to services");

                using (var crmService = new OrganizationService(_crmConnection))
                using (var crmContext = new CrmOrganizationServiceContext(crmService))
                using (var recruitmentScope = _recruitmentUnitOfWorkService.Create())
                using (var extendedUnitOfWork = _recruitmentUnitOfWorkService.CreateExtended())

                {
                    _logger.Info($"Connected. Migrating...");

                    var crmNeededResources = crmContext.CreateQuery<smt_neededresources>();

                    foreach (var crmNeededResource in crmNeededResources)
                    {
                        try
                        {
                            var jobOpening = recruitmentScope.Repositories.JobOpenings.FirstOrDefault(s => s.CrmId == crmNeededResource.Id);

                            if (jobOpening == null)
                            {
                                _logger.Error($"RECRUITMENT_MIGRATION: Job opening with ID {jobOpening.Id} wasn't imported ");
                                continue;
                            }

                            var employee = GetEmployeeByCrmUserEntityReference(crmContext, recruitmentScope, crmNeededResource.CreatedBy);

                            if (employee == null || !employee.UserId.HasValue)
                            {
                                _logger.Error($"RECRUITMENT_MIGRATION: User from crm {crmNeededResource.CreatedBy.Id} wasn't found in dante");
                                continue;
                            }

                            UpdateCreatedBy(employee.UserId.Value, jobOpening.Id, extendedUnitOfWork);
                        }
                        catch (Exception exception)
                        {
                            var newNeededResources = new smt_neededresources
                            {
                                Id = crmNeededResource.Id,
                                new_dantemigrationlogerror = $"{DateTime.Now:s} {exception.Message}",
                            };
                            crmService.Update(newNeededResources);

                            _logger.Error($"RECRUITMENT_MIGRATION: During migration {crmNeededResource.Id} encounter error, {exception.Message}");
                        }
                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> exception)
            {
                LogExceptionWhileGettingNeededResources(exception);
                throw new CrmException(Resources.Exceptions.JobOpeningCouldNotBeFoundBecauseOfOrganizationServiceFault, exception);
            }
            catch (TimeoutException exception)
            {
                LogExceptionWhileGettingNeededResources(exception);
                throw new CrmException(Resources.Exceptions.JobOpeningCouldNotBeFoundBecauseOfTimeout, exception);
            }
            catch (Exception exception)
            {
                LogExceptionWhileGettingNeededResources(exception);
                throw new CrmException(Resources.Exceptions.JobOpeningCouldNotBeFound, exception);
            }

            return JobResult.Success();
        }

        public static void UpdateCreatedBy(long userIdCreatedBy, long entityId, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork)
        {
            var sql = new StringBuilder($"UPDATE Recruitment.JobOpening SET UserIdCreatedBy = { userIdCreatedBy} where id = { entityId } ");
            extendedUnitOfWork.ExecuteSqlCommand(sql.ToString());
        }

        private void LogExceptionWhileGettingNeededResources(Exception ex)
        {
            _logger.Error($"Error while getting recruitment activities.", ex);
        }
    }
}
