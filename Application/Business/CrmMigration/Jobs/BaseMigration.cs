﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CrmMigration.Jobs
{
    public class BaseMigration
    {
        protected readonly ILogger _logger;
        protected readonly ICrmConnectionStringProvider _connectionStringProvider;

        public BaseMigration(ILogger logger, ICrmConnectionStringProvider connectionStringProvider)
        {
            _logger = logger;
            _connectionStringProvider = connectionStringProvider;
        }


        protected SystemUser GetCrmUser(CrmOrganizationServiceContext crmContext, EntityReference entityReference)
        {
            var crmUser = entityReference == null
                ? null
                : crmContext.CreateQuery<SystemUser>()
                    .SingleOrDefault(u => u.Id == entityReference.Id);

            return crmUser;
        }

        protected SystemUser GetCrmTechnicalVeryficatorId(CrmOrganizationServiceContext crmContext, EntityReference entityReference)
        {
            var candidate = entityReference == null
                ? null
                : crmContext.CreateQuery<smt_candidate>()
                    .SingleOrDefault(u => u.Id == entityReference.Id);

            return candidate?.user_smt_candidate; 
        }

        protected void LogExceptionWhileGettingRecruitmentActivities(Exception ex)
        {
            _logger.Error($"Error while getting recruitment activities.", ex);
        }

        protected long[] GetIds<T>(List<T> entityList, string attributeName) where T : Entity
        {
            if (entityList.Count == 0)
            {
                return new long[0];
            }

            return entityList.Select(t => (long)t.GetAttributeValue<int>(attributeName)).ToArray();
        }

        protected bool IsConsentPresent(smt_candidate c)
        {
            return new[]
            {
                c.smt_ConsenttotheProcessingofPersonalData?.Value,
                c.smt_IntiveConsentNowFuture?.Value,
                c.smt_SMTsConsenttotheProcessingofPersonalData?.Value,
                c.smt_IntiveConsentOneTime?.Value
            }.Any(d => d.HasValue)
             || c.smt_NewCoconsenttotheprocessingdata == true;
        }

        protected CrmConnection GetCrmConnection()
        {
            return CrmConnection.Parse(_connectionStringProvider.GetConnectionString());
        }

        public Employee GetEmployeeByCrmUserEntityReference(CrmOrganizationServiceContext crmContext, IUnitOfWork<IRecruitmentDbScope> recruitmentScope, EntityReference userEntityReference, long? defaultEmployeeId = null)
        {
            var crmUser = GetCrmUser(crmContext, userEntityReference);
            var employeeId = crmUser?.smt_Id_Dante;

            if (employeeId == null && !String.IsNullOrEmpty(crmUser?.DomainName))
            {
                return recruitmentScope.Repositories.Employees.SingleOrDefault(e => e.Email == crmUser.DomainName);
            }

            return GetEmployee(employeeId, recruitmentScope, defaultEmployeeId);
        }

        public static Employee GetEmployee(long? employeeId, IUnitOfWork<IRecruitmentDbScope> recruitmentScope, long? defaultEmployeeId = null)
        {
            var employee = employeeId != null
                ? recruitmentScope.Repositories.Employees
                    .FirstOrDefault(e => e.Id == employeeId)
                : defaultEmployeeId == null
                    ? null
                    : recruitmentScope.Repositories.Employees
                        .FirstOrDefault(e => e.Id == defaultEmployeeId);

            return employee;
        }

        public static long? GetUserId(long? employeeId, IUnitOfWork<IRecruitmentDbScope> recruitmentScope)
        {
            return GetEmployee(employeeId, recruitmentScope)?.UserId;
        }
    }
}
