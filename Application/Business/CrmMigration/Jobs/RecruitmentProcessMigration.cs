﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Castle.Core.Logging;
using HtmlAgilityPack;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CrmMigration.Jobs
{
    [Identifier("Job.Recruitment.RecruitmentProcessMigration")]
    [JobDefinition(
            Code = "RecruitmentProcessMigration",
            Description = "Migrates recruitment processes from Dynamics Crm to Dante Recruitment Module",
            ScheduleSettings = "0 0 1 1 * *",
            PipelineName = PipelineCodes.Recruitment,
            MaxExecutioPeriodInSeconds = 86400)]
    public class RecruitmentProcessMigration : BaseMigration, IJob
    {
        private readonly IRecruitmentProcessNoteCardIndexDataService _recruitmentProcessNoteCardIndexDataService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _recruitmentUnitOfWorkService;
        private readonly CrmConnection _crmConnection;
        private const int NoteLength = 4000;

        public RecruitmentProcessMigration(ILogger logger,
            ICrmConnectionStringProvider crmConnectionStringProvider,
            IRecruitmentProcessNoteCardIndexDataService recruitmentProcessNoteCardIndexDataService,
            IUnitOfWorkService<IRecruitmentDbScope> recruitmentUnitOfWorkService) : base(logger, crmConnectionStringProvider)
        {
            _recruitmentProcessNoteCardIndexDataService = recruitmentProcessNoteCardIndexDataService;
            _recruitmentUnitOfWorkService = recruitmentUnitOfWorkService;

            var connectionString = _connectionStringProvider.GetConnectionString();
            _crmConnection = CrmConnection.Parse(connectionString);
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            CreateMigration();

            return JobResult.Success();
        }

        private void CreateMigration()
        {
            try
            {
                _logger.Info($"Create connections to services");

                using (var crmService = new OrganizationService(_crmConnection))
                using (var crmContext = new CrmOrganizationServiceContext(crmService))
                using (var extendedUnitOfWork = _recruitmentUnitOfWorkService.CreateExtended())
                {
                    _logger.Info($"Connected. Migrating...");


                    var crmRecruitmentActivities = crmContext.CreateQuery<smt_recruitmentactivity>();

                    foreach (var crmRecruitmentActivity in crmRecruitmentActivities)
                    {
                        try
                        {
                            var recruitmentProcess = extendedUnitOfWork.Repositories.RecruitmentProcesses.FirstOrDefault(s => s.CrmId == crmRecruitmentActivity.Id);
                            var activities = crmContext.CreateQuery<ActivityPointer>().Where(a => a.RegardingObjectId.Id == crmRecruitmentActivity.Id).ToList();

                            if (recruitmentProcess == null)
                            {
                                continue;
                            }

                            foreach (var activity in activities)
                            {
                                MigrateActivity(activity, recruitmentProcess.Id, extendedUnitOfWork);
                            }
                        }
                        catch (Exception exception)
                        {
                            var newRecruitmentActivity = new smt_recruitmentactivity
                            {
                                Id = crmRecruitmentActivity.Id,
                                new_dantemigrationlogerror = $"{DateTime.Now:s} {exception.Message}",
                            };
                            crmService.Update(newRecruitmentActivity);
                            _logger.Error($"RECRUITMENT_MIGRATION: During migration {crmRecruitmentActivity.Id} encounter error, {exception.Message}");
                        }
                    }
                }
            }
            catch (FaultException<OrganizationServiceFault> exception)
            {
                LogExceptionWhileGettingRecruitmentActivities(exception);
                throw new CrmException(Resources.Exceptions.RecruitmentProcessCouldNotBeFoundBecauseOfOrganizationServiceFault, exception);
            }
            catch (TimeoutException exception)
            {
                LogExceptionWhileGettingRecruitmentActivities(exception);
                throw new CrmException(Resources.Exceptions.RecruitmentProcessCouldNotBeFoundBecauseOfTimeout, exception);
            }
            catch (Exception exception)
            {
                LogExceptionWhileGettingRecruitmentActivities(exception);
                throw new CrmException(Resources.Exceptions.RecruitmentProcessCouldNotBeFound, exception);
            }
        }

        private void MigrateActivity(ActivityPointer activity, long id, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork)
        {
            var noteContent = GetNoteBasedOnActivity(activity);

            if (noteContent.Length < NoteLength)
            {
                return;
            }
            
            var sendOn = activity.SentOn;

            var note = new RecruitmentProcessNote
            {
                Content = noteContent,
                NoteType = NoteType.UserNote,
                NoteVisibility = NoteVisibility.Public,
                RecruitmentProcessId = id
            };

            extendedUnitOfWork.Repositories.RecruitmentProcessNotes.Add(note);

            if (sendOn.HasValue)
            {
                UpdateCreationAndModificationData(sendOn, note.Id, extendedUnitOfWork);
            }

            extendedUnitOfWork.Commit();
        }


        private string GetNoteBasedOnActivity(ActivityPointer activity)
        {
            var stringBuilder = new StringBuilder();

            var to = string.Join(", ", activity.allparties.Where(ap => ap.ParticipationTypeMask.Value == 2).Select(r => r.AddressUsed));
            stringBuilder.Append($"To: {to}{Environment.NewLine}");

            var cc = string.Join(", ", activity.allparties.Where(ap => ap.ParticipationTypeMask.Value == 3).Select(r => r.AddressUsed));
            stringBuilder.Append($"CC: {cc}{Environment.NewLine}");

            var bcc = string.Join(", ", activity.allparties.Where(ap => ap.ParticipationTypeMask.Value == 4).Select(r => r.AddressUsed));
            stringBuilder.Append($"BCC: {bcc}{Environment.NewLine}");

            var body = GetRawText(activity.Description);
            stringBuilder.Append($"Body: {body}{Environment.NewLine}");

            var subject = activity.Subject;
            stringBuilder.Append($"Subject: {body}");

            return stringBuilder.ToString();
        }

        private string GetRawText(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var stringBuilder = new StringBuilder();

            foreach (var node in doc.DocumentNode.SelectNodes("//text()"))
            {
                stringBuilder.Append(node.InnerText);
            }

            return stringBuilder.ToString();
        }
        public static void UpdateCreationAndModificationData(DateTime? sendOn, long entityId,
                 IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork)
        {
            var sql = $"UPDATE Recruitment.RecruitmentProcessNote SET CreatedOn = '{sendOn?.ToString("s")}', ModifiedOn = '{sendOn?.ToString("s")}'  where id = { entityId }";

            extendedUnitOfWork.ExecuteSqlCommand(sql);
        }
    }
}
