﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.ServiceModel;
using System.Web;
using Castle.Core.Logging;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Business.CrmMigration.Helpers;
using Smt.Atomic.Business.CrmMigration.MappingData;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Crm.CrmModel;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.SharePoint.Interfaces;

namespace Smt.Atomic.Business.CrmMigration.Jobs
{
    [Identifier("Job.Recruitment.CandidateMigration")]
    [JobDefinition(
            Code = "CandidateMigration",
            Description = "Migration candidates from Dynamics Crm to Dante Recruitment Module",
            ScheduleSettings = "0 0 1 1 * *",
            PipelineName = PipelineCodes.Recruitment,
            MaxExecutioPeriodInSeconds = 86400)]
    public class CandidateMigration : BaseMigration, IJob
    {
        private readonly ICandidateCardIndexDataService _candidateCardIndexDataService;
        private readonly ISharePointService _sharePointService;
        private readonly ICandidateFileCardIndexDataService _candidateFileCardIndexDataService;
        private readonly ITemporaryDocumentPromotionService _temporaryDocumentPromotionService;
        private readonly ICandidateNoteCardIndexDataService _candidateNoteCardIndexDataService;
        private readonly IJobApplicationCardIndexDataService _jobApplicationCardIndexDataService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkRecruitmentService;
        private readonly IUnitOfWorkService<IGDPRDbScope> _unitOfWorkGDPRService;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ITimeService _timeService;
        private readonly ConversionHelper _conversionHelper;
        private readonly MappingDataHelper _mappingDataHelper;
        private readonly FileMigration _fileMigration;

        public CandidateMigration(ILogger logger,
            ICrmConnectionStringProvider crmConnectionStringProvider,
            ITimeService timeService,
            ICandidateCardIndexDataService candidateCardIndexDataService,
            ICandidateFileCardIndexDataService candidateFileCardIndexDataService,
            IJobApplicationCardIndexDataService jobApplicationCardIndexDataService,
            ISharePointService sharePointService,
            ITemporaryDocumentPromotionService temporaryDocumentPromotionService,
            ICandidateNoteCardIndexDataService candidateNoteCardIndexDataService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkRecruitmentService,
            IUnitOfWorkService<IGDPRDbScope> unitOfWorkGDPRService,
            ISettingsProvider settingsProvider) : base(logger, crmConnectionStringProvider)
        {
            _candidateCardIndexDataService = candidateCardIndexDataService;
            _candidateFileCardIndexDataService = candidateFileCardIndexDataService;
            _jobApplicationCardIndexDataService = jobApplicationCardIndexDataService;
            _sharePointService = sharePointService;
            _temporaryDocumentPromotionService = temporaryDocumentPromotionService;
            _candidateNoteCardIndexDataService = candidateNoteCardIndexDataService;
            _unitOfWorkRecruitmentService = unitOfWorkRecruitmentService;
            _unitOfWorkGDPRService = unitOfWorkGDPRService;
            _settingsProvider = settingsProvider;
            _timeService = timeService;
            _conversionHelper = new ConversionHelper(_logger);
            _mappingDataHelper = new MappingDataHelper(_logger);
            _fileMigration = new FileMigration(_temporaryDocumentPromotionService);
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            //CreateNewElements();
            //VerifyAllCandidates();
            //AddValidDataConsent();
            AddAttachments();

            return JobResult.Success();
        }

        private void AddAttachments()
        {
            try
            {
                using (var service = new OrganizationService(GetCrmConnection()))
                using (var context = new CrmOrganizationServiceContext(service))
                using (var recruitmentScope = _unitOfWorkRecruitmentService.Create())
                {
                    _logger.Info($"Connected. Migrating...");

                    var _danteCandidates = recruitmentScope.Repositories.Candidates
                            .Where(c => c.CandidateFiles.Count == 0)
                            .Where(c => c.DataOwners.FirstOrDefault(d => d.CandidateId == c.Id).DataConsents.Count != 0)
                            .Where(c => c.CrmId.HasValue).ToList();

                    foreach (var danteCandidate in _danteCandidates)
                    {
                        try
                        {
                            var crmId = danteCandidate.CrmId.Value;
                            var crmCandidate = context.CreateQuery<smt_candidate>().FirstOrDefault(c => c.Id == crmId);

                            if (crmCandidate == null)
                            {
                                _logger.Error($"Candidate {crmId.ToString() } cannot find on CRM.");

                                continue;
                            }

                            var documentLocations = context
                                .CreateQuery<SharePointDocumentLocation>()
                                .Where(x => x.RegardingObjectId.Id == crmCandidate.Id)
                                .ToList();

                            var filedAbsolutePath = documentLocations.Select(documentLocation =>
                                new RetrieveAbsoluteAndSiteCollectionUrlRequest
                                {
                                    Target = documentLocation.ToEntityReference(),
                                })
                                .Select(retrieveRequest => (RetrieveAbsoluteAndSiteCollectionUrlResponse)service.Execute(retrieveRequest))
                                .Select(d => d.AbsoluteUrl)
                                .AsEnumerable();

                            CandidateFilesMigration(filedAbsolutePath, danteCandidate.Id);
                        }
                        catch (Exception exception)
                        {
                            _logger.Error($"RECRUITMENT_MIGRATION: During adding attachment {danteCandidate.Id}, {exception.Message}");
                        }
                    }
                }
            }
            #region Catching Fatal Exceptions
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
            #endregion

            _logger.Info($"Add candidates attachment DONE");
        }

        private void AddValidDataConsent()
        {
            try
            {
                using (var service = new OrganizationService(GetCrmConnection()))
                using (var context = new CrmOrganizationServiceContext(service))
                using (var recruitmentScope = _unitOfWorkRecruitmentService.Create())
                {
                    _logger.Info($"Connected. Migrating...");

                    var _candidates = context.CreateQuery<smt_candidate>();

                    var dataAdministratorId = recruitmentScope.Repositories.Companies
                        .Where(c => c.ActiveDirectoryCode == _settingsProvider.CrmImportSettings.DefaultDanteDataConsentAdministratorCompanyAdCode)
                        .Select(c => c.Id)
                        .Single();

                    foreach (var candidate in _candidates)
                    {
                        try
                        {
                            var danteCandidate = recruitmentScope.Repositories.Candidates.FirstOrDefault(c => c.CrmId == candidate.Id);

                            if (danteCandidate == null)
                            {
                                _logger.Error($"Candidate with email: {candidate.smt_emailaddress3} ({candidate.Id}) is NOT on DB!.");

                                continue;
                            }

                            if (IsConsentPresent(candidate))
                            {
                                using (var gdprScope = _unitOfWorkGDPRService.Create())
                                {
                                    if(!gdprScope.Repositories.DataOwners.Any(o => o.CandidateId == danteCandidate.Id))
                                    {
                                        _logger.Error($"Cannot find data owner for CandidateId: {danteCandidate.Id}");

                                        continue;
                                    }

                                    AddDataConsent(candidate, danteCandidate.Id, dataAdministratorId);
                                }
                            }
                            
                        }
                        catch (Exception exception)
                        {
                            var newCandidate = new smt_candidate
                            {
                                Id = candidate.Id,
                                smt_dantemigrationlogerror = $"{DateTime.Now:s} {exception.Message}"
                            };

                            service.Update(newCandidate);

                            _logger.Error($"RECRUITMENT_MIGRATION: During adding Data consent {candidate.Id}, {exception.Message}");
                        }
                    }
                }
            }
            #region Catching Fatal Exceptions
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
            #endregion

            _logger.Info($"Create candidates Consents DONE");
        }

            private void VerifyAllCandidates()
        {
            try
            {
                _logger.Info($"Create connections to services");

                using (var service = new OrganizationService(GetCrmConnection()))
                using (var context = new CrmOrganizationServiceContext(service))
                using (var recruitmentScope = _unitOfWorkRecruitmentService.Create())
                {
                    _logger.Info($"Connected. Checking existing candidates...");

                    var _candidates = context.CreateQuery<smt_candidate>()
                        .Where(s => s.Id == new Guid("cd5ed24b-4f63-e811-8138-e0071b6641c1")
                                 || s.Id == new Guid("cfabf600-60ab-e611-8242-3863bb347dd8")
                                 || s.Id == new Guid("e602478b-19e0-e211-93f6-000c294555ff"));

                    var defaultEmails = GenerateDefaultEmails();

                    foreach (var candidate in _candidates)
                    {
                        var existingCandidateInDBID = recruitmentScope.Repositories.Candidates.FirstOrDefault(c => c.CrmId == candidate.Id);

                        if (existingCandidateInDBID == null)
                        {
                            _logger.Error($"Candidate with ID is not in db {candidate.Id}");
                        }
                        try
                        {
                            using (var extendedUnitOfWork = _unitOfWorkRecruitmentService.CreateExtended())
                            {
                                MigrateTechnicalReviews(context, extendedUnitOfWork, candidate.Id, existingCandidateInDBID.Id);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error($"{ex.ToString()}");
                        }

                        //if (existingCandidateInDBID == null)
                        //{
                        //    var email = GetEmailForCandidate(candidate, defaultEmails);

                        //    var existingCandidateInDBEmail = recruitmentScope.Repositories.Candidates.FirstOrDefault(c => c.EmailAddress == candidate.EmailAddress);


                        //    else
                        //    {
                        //        _logger.Error($"Candidate with email: {email} ({candidate.Id}) is NOT on DB!.");
                        //    }

                        //    continue;
                        //}
                    }
                }
            }

            #region Catching Fatal Exceptions
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
            #endregion
        }

        private void CreateNewElements()
        {
            try
            {
                _logger.Info($"Create connections to services");

                using (var service = new OrganizationService(GetCrmConnection()))
                using (var context = new CrmOrganizationServiceContext(service))
                using (var recruitmentScope = _unitOfWorkRecruitmentService.Create())
                using (var extendedUnitOfWork = _unitOfWorkRecruitmentService.CreateExtended())
                {
                    _logger.Info($"Connected. Migrating...");

                    var _candidates = context.CreateQuery<smt_candidate>();
                    var _locations = context.CreateQuery<smt_citytowhichapplies>().ToList();
                    var _jobProfiles = context.CreateQuery<new_technologytagcloud>().ToList();

                    var dataAdministratorId = recruitmentScope.Repositories.Companies
                        .Where(c => c.ActiveDirectoryCode == _settingsProvider.CrmImportSettings.DefaultDanteDataConsentAdministratorCompanyAdCode)
                        .Select(c => c.Id)
                        .Single();

                    var applicationOrigin = recruitmentScope.Repositories.ApplicationOrigins
                        .Select(s => new ApplicationOriginDictionaryDto { Id = s.Id, Name = s.Name.ToLower().Trim(), OriginType = s.OriginType })
                        .ToList();

                    var defaultEmails = GenerateDefaultEmails();


                    foreach (var candidate in _candidates)
                    {
                        try
                        {
                            ClearCrmLog(service, candidate.Id);

                            var email = GetEmailForCandidate(candidate, defaultEmails);

                            var existingCandidateInDB = recruitmentScope.Repositories.Candidates.FirstOrDefault(c => c.EmailAddress == email);

                            if (existingCandidateInDB != null)
                            {
                                _logger.Error($"Candidate with email: {email} ({candidate.Id}) is already on DB.");

                                if (existingCandidateInDB.CrmId.HasValue && existingCandidateInDB.CrmId.Value != candidate.Id)
                                {
                                    _logger.Error($"There are TWO candidates on CRM with email: {email} ({candidate.Id}) amd ({existingCandidateInDB.Id}).");
                                }

                                continue;
                            }

                            long? consentCoordinatorEmployeeId = null;
                            long? contactCoordinatorEmployeeId = null;

                            var crmOwner = context.CreateQuery<SystemUser>()
                                .SingleOrDefault(u => u.Id == candidate.OwnerId.Id);

                            var ownerEmployeeId = crmOwner?.smt_Id_Dante;

                            if (ownerEmployeeId != null)
                            {
                                consentCoordinatorEmployeeId = contactCoordinatorEmployeeId = recruitmentScope.Repositories.Employees
                                    .FirstOrDefault(e => e.Id == ownerEmployeeId.Value)?.Id;
                            }

                            if (consentCoordinatorEmployeeId == null)
                            {
                                consentCoordinatorEmployeeId = recruitmentScope.Repositories.Employees
                                    .Single(u => u.Email == _settingsProvider.CrmImportSettings.DefaultDanteConsentCoordinatorEmployeeEmail).Id;
                                contactCoordinatorEmployeeId = recruitmentScope.Repositories.Employees
                                    .Single(u => u.Email == _settingsProvider.CrmImportSettings.DefaultDanteContactCoordinatorEmployeeEmail).Id;
                            }

                            var languagesSkill = context.CreateQuery<new_languageskill>()
                                .Where(u => u.new_CandidateId.Id == candidate.smt_candidateId)
                                .ToList();

                            var jobProfileIds = context.CreateQuery<new_new_technologytagcloud_smt_candidate>()
                                    .Where(t => t.smt_candidateid == candidate.Id)
                                    .Select(t => t.new_technologytagcloudid)
                                    .ToList();
                            var jobProfiles = new List<new_technologytagcloud>();

                            if (jobProfileIds.Count > 0)
                            {
                                jobProfiles = _jobProfiles.Where(c => jobProfileIds.Contains(c.Id)).ToList();
                            }

                            var locationIds = context.CreateQuery<smt_smt_citytowhichapplies_smt_candidate>()
                                    .Where(c => c.smt_candidateid == candidate.Id)
                                    .Select(c => c.smt_citytowhichappliesid)
                                    .ToList();
                            var locations = new List<smt_citytowhichapplies>();

                            if (locationIds.Count > 0)
                            {
                                locations = _locations.Where(c => locationIds.Contains(c.Id)).ToList();
                            }

                            var crmModifiedBy = GetCrmUser(context, candidate.ModifiedBy);

                            var candidateDto = new CandidateDto
                            {
                                FirstName = candidate.new_FirstName,
                                LastName = candidate.smt_LastName2.TrimStart(),
                                ContactRestriction = candidate.smt_DoNotContact.HasValue ? (candidate.smt_DoNotContact.Value ? ContactRestriction.None : ContactRestriction.AgencyRestrictions) : ContactRestriction.None,
                                MobilePhone = _conversionHelper.SubstringString(candidate.new_MobilePhone, 50, nameof(candidate.new_MobilePhone)),
                                OtherPhone = _conversionHelper.SubstringString(candidate.new_OtherPhone, 50, nameof(candidate.new_OtherPhone)),
                                EmailAddress = email,
                                SkypeLogin = candidate.smt_candidateloginskype,
                                EmployeeStatus = _mappingDataHelper.MapperEmployeeStatus(candidate.smt_Employee.Value),
                                CanRelocate = candidate.smt_relocation != null ? _mappingDataHelper.MapperCandidateRelocation(candidate.smt_relocation.Value) : false,
                                RelocateDetails = candidate.smt_relocationdescription,
                                ResumeTextContent = candidate.new_CV,
                                ConsentCoordinatorId = consentCoordinatorEmployeeId.Value,
                                ContactCoordinatorId = contactCoordinatorEmployeeId.Value,
                                CrmId = candidate.Id,
                                Languages = _mappingDataHelper.MapperLanguageCandidateSkill(languagesSkill),
                                JobProfileIds = GetIds(jobProfiles, "smt_iddantetechnology"),
                                CityIds = GetIds(locations, "smt_iddantecity"),
                                WatcherIds = new long[0],
                                ModifiedById = crmModifiedBy?.smt_Id_Dante,
                                ModifiedOn = candidate.ModifiedOn ?? SqlDateTime.MinValue.Value,
                                OriginalSourceId = MigrateOrginalSource(candidate.new_CvContactSource?.Value, candidate.smt_ContactSourceDescription, applicationOrigin),
                                OriginalSourceComment = _conversionHelper.SubstringString(candidate.smt_ContactSourceDescription, 400, nameof(candidate.smt_ContactSourceDescription))
                            };

                            var danteCandidateId = _candidateCardIndexDataService.AddRecord(candidateDto).AddedRecordId;

                            if (IsConsentPresent(candidate))
                            {
                                AddDataConsent(candidate, danteCandidateId, dataAdministratorId);
                            }

                            CandidateNotesMigration(context, extendedUnitOfWork, candidate.Id, danteCandidateId);

                            MigratePosts(context, extendedUnitOfWork, candidate.Id, danteCandidateId);

                            MigrateJobApplications(recruitmentScope, candidate, danteCandidateId);

                            MigrateTechnicalReviews(context, extendedUnitOfWork, candidate.Id, danteCandidateId);
                            // TODO: When Candidate.OriginalSource is added, set new_CvContactSource and smt_ContactSourceDescription there too (DANTER-363)

                            extendedUnitOfWork.Commit();

                            var documentLocations = context
                                .CreateQuery<SharePointDocumentLocation>()
                                .Where(x => x.RegardingObjectId.Id == candidate.Id)
                                .ToList();

                            var filedAbsolutePath = documentLocations.Select(documentLocation =>
                                new RetrieveAbsoluteAndSiteCollectionUrlRequest
                                {
                                    Target = documentLocation.ToEntityReference(),
                                })
                                .Select(retrieveRequest => (RetrieveAbsoluteAndSiteCollectionUrlResponse)service.Execute(retrieveRequest))
                                .Select(d => d.AbsoluteUrl)
                                .AsEnumerable();

                            CandidateFilesMigration(filedAbsolutePath, danteCandidateId);
                        }
                        catch (Exception exception)
                        {
                            var newCandidate = new smt_candidate
                            {
                                Id = candidate.Id,
                                smt_dantemigrationlogerror = $"{DateTime.Now:s} {exception.Message}"
                            };

                            service.Update(newCandidate);

                            _logger.Error($"RECRUITMENT_MIGRATION: During migration {candidate.Id}, {exception.Message}");
                        }
                    }
                }
            }

            #region Catching Fatal Exceptions
            catch (FaultException<OrganizationServiceFault> ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfOrganizationServiceFault, ex);
            }
            catch (TimeoutException ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFoundBecauseOfTimeout, ex);
            }
            catch (Exception ex)
            {
                LogExceptionWhileGettingRecruitmentActivities(ex);

                throw new CrmException(Resources.Exceptions.CandidateCouldNotBeFound, ex);
            }
            #endregion

            _logger.Info($"Create candidates DONE");
        }

        private void MigratePosts(CrmOrganizationServiceContext context, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork, Guid id, long danteCandidateId)
        {
            var posts = context.CreateQuery<Post>().Where(s => s.RegardingObjectId.Id == id && s.Text != null).ToList();

            foreach (var post in posts)
            {
                var noteCreator = GetCrmUser(context, post.CreatedBy);

                NoteMigration.MigrateCandidatePost(_candidateNoteCardIndexDataService, extendedUnitOfWork, post, noteCreator, danteCandidateId);
            }
        }

        private IList<string> GenerateDefaultEmails()
        {
            return Enumerable.Range(1, 1000).Select(s => $"default{s}@default.com").ToList();
        }

        private string GetEmailForCandidate(smt_candidate candidate, IList<string> defaultEmails)
        {
            if (!string.IsNullOrEmpty(candidate.smt_emailaddress3))
            {
                return candidate.smt_emailaddress3;
            }

            var defaultEmail = defaultEmails.First();
            defaultEmails.Remove(defaultEmail);

            return defaultEmail;
        }

        private void AddDataConsent(smt_candidate candidate, long danteCandidateId, long dataAdministratorId)
        {
            using (var gdprScope = _unitOfWorkGDPRService.Create())
            {
                var dataOwner = gdprScope.Repositories.DataOwners
                    .SingleOrDefault(o => o.CandidateId == danteCandidateId);

                if (candidate.smt_NewCoconsenttotheprocessingdata.HasValue && candidate.smt_NewCoconsenttotheprocessingdata.Value)
                {
                    if(!dataOwner.DataConsents.Any(c => c.DataOwnerId == dataOwner.Id && c.Type == DataConsentType.CandidateConsentLegacy))
                    {
                        var dataConsent = new DataConsent
                        {
                            Type = DataConsentType.CandidateConsentLegacy,
                            ExpiresOn = null,
                            DataAdministratorId = dataAdministratorId,
                            DataOwnerId = dataOwner.Id,
                        };
                        dataOwner.DataConsents.Add(dataConsent);
                    }
                }

                if (candidate.smt_SMTsConsenttotheProcessingofPersonalData?.Value == CrmConsent.Yes)
                {
                    if (!dataOwner.DataConsents.Any(c => c.DataOwnerId == dataOwner.Id && c.Type == DataConsentType.CandidateConsentSpecificPosition))
                    {
                        var dataConsent = new DataConsent
                        {
                            Type = DataConsentType.CandidateConsentSpecificPosition,
                            ExpiresOn = _timeService.GetCurrentDate().AddDays(75),
                            DataAdministratorId = dataAdministratorId,
                            DataOwnerId = dataOwner.Id,
                        };
                        dataOwner.DataConsents.Add(dataConsent);
                    }
                }

                if (candidate.smt_ConsenttotheProcessingofPersonalData?.Value == CrmConsent.Yes)
                {
                    if (!dataOwner.DataConsents.Any(c => c.DataOwnerId == dataOwner.Id && c.Type == DataConsentType.CandidateConsentSpecificPosition))
                    {
                        var dataConsent = new DataConsent
                        {
                            Type = DataConsentType.CandidateConsentSpecificPosition,
                            ExpiresOn = _timeService.GetCurrentDate().AddDays(75),
                            DataAdministratorId = dataAdministratorId,
                            DataOwnerId = dataOwner.Id,
                        };
                        dataOwner.DataConsents.Add(dataConsent);
                    }
                }

                if (candidate.smt_IntiveConsentNowFuture?.Value == CrmConsent.Yes)
                {
                    if (!dataOwner.DataConsents.Any(c => c.DataOwnerId == dataOwner.Id && c.Type == DataConsentType.CandidateConsentGeneral))
                    {
                        var dataConsent = new DataConsent
                        {
                            Type = DataConsentType.CandidateConsentGeneral,
                            ExpiresOn = null,
                            DataAdministratorId = dataAdministratorId,
                            DataOwnerId = dataOwner.Id,
                        };
                        dataOwner.DataConsents.Add(dataConsent);
                    }
                }

                if (candidate.smt_IntiveConsentOneTime?.Value == CrmConsent.Yes)
                {
                    if (!dataOwner.DataConsents.Any(c => c.DataOwnerId == dataOwner.Id && c.Type == DataConsentType.CandidateConsentSpecificPosition))
                    {
                        var dataConsent = new DataConsent
                        {
                            Type = DataConsentType.CandidateConsentSpecificPosition,
                            ExpiresOn = _timeService.GetCurrentDate().AddDays(75),
                            DataAdministratorId = dataAdministratorId,
                            DataOwnerId = dataOwner.Id,
                        };
                        dataOwner.DataConsents.Add(dataConsent);
                    }
                }

                gdprScope.Commit();
            }
        }

        private long MigrateOrginalSource(int? orginalSource, string comment, IList<ApplicationOriginDictionaryDto> applicationOrigin)
        {
            var other = applicationOrigin.First(s => s.OriginType == ApplicationOriginType.Other).Id;
            var trimedComment = string.IsNullOrEmpty(comment) ? string.Empty : comment.ToLower().Trim();

            if (orginalSource == null || orginalSource == CrmCvContactSource.Other)
            {
                return other;
            }

            switch (orginalSource)
            {
                case CrmCvContactSource.ApplicationWithoutNotice:
                    return applicationOrigin.First(s => s.OriginType == ApplicationOriginType.DropOff).Id;
                case CrmCvContactSource.CareersFairOrCollege:
                    return applicationOrigin.First(s => s.OriginType == ApplicationOriginType.Event).Id;

                case CrmCvContactSource.Recommendation:
                    return applicationOrigin.First(s => s.OriginType == ApplicationOriginType.Recommendation).Id;
            }

            if (orginalSource == CrmCvContactSource.Agency)
            {
                if (string.IsNullOrEmpty(comment))
                {
                    var originType = applicationOrigin.FirstOrDefault(s => s.OriginType == ApplicationOriginType.Agency && s.Name.Contains("Other"));
                    return originType == null ? other : originType.Id;


                }

                var origin = applicationOrigin.FirstOrDefault(s => s.Name.Contains(trimedComment));
                return origin == null ? other : origin.Id;
            }

            if (orginalSource == CrmCvContactSource.SocialNetwork)
            {
                if (string.IsNullOrEmpty(comment))
                {
                    var originType = applicationOrigin.FirstOrDefault(s => s.OriginType == ApplicationOriginType.SocialNetwork && s.Name.Contains("Other"));
                    return originType == null ? other : originType.Id;


                }

                var origin = applicationOrigin.FirstOrDefault(s => s.Name.Contains(trimedComment));
                return origin == null ? other : origin.Id;
            }
            if (orginalSource == CrmCvContactSource.JobAdvertisement)
            {
                if (string.IsNullOrEmpty(comment))
                {
                    var originType = applicationOrigin.FirstOrDefault(s => s.OriginType == ApplicationOriginType.JobAdvertisement && s.Name.Contains("Other"));
                    return originType == null ? other : originType.Id;
                }

                var origin = applicationOrigin.FirstOrDefault(s => s.Name.Contains(trimedComment));
                return origin == null ? other : origin.Id;
            }

            return other;
        }


        private void MigrateJobApplications(IUnitOfWork<IRecruitmentDbScope> recruitmentScope, smt_candidate crmCandidate, long candidateId)
        {
            var applicationOriginType = _mappingDataHelper.MapToJobApplicationOriginType(crmCandidate.new_CvContactSource?.Value);

            var applicationOrigins = recruitmentScope.Repositories.ApplicationOrigins
                .Where(ao => ao.OriginType == applicationOriginType)
                .ToList();

            if (!applicationOrigins.Any())
            {
                return;
            }

            var applicationOrigin = applicationOrigins.Count == 1
                ? applicationOrigins.Single()
                : applicationOrigins.FirstOrDefault(ao => ao.Name == crmCandidate.smt_ContactSourceDescription);

            if (applicationOrigin == null)
            {
                return;
            }

            var jobApplication = new JobApplicationDto
            {
                CandidateId = candidateId,
                ApplicationOriginId = applicationOrigin.Id,
                OriginType = applicationOriginType,
                OriginComment = _conversionHelper.SubstringString(crmCandidate.smt_ContactSourceDescription, 400, nameof(crmCandidate.smt_ContactSourceDescription)),
                JobProfileIds = new long[0],
                CityIds = new long[0],
            };

            _jobApplicationCardIndexDataService.AddRecord(jobApplication);
        }

        #region DuringJob

        private void CandidateNotesMigration(CrmOrganizationServiceContext crmContext, IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork, Guid crmCandidateGuid, long candidateId)
        {
            var annotations = crmContext.CreateQuery<Annotation>()
                .Where(a => a.ObjectId.Id == crmCandidateGuid);

            foreach (var annotation in annotations)
            {
                if (annotation.NoteText == null)
                {
                    continue;
                }

                var noteCreator = GetCrmUser(crmContext, annotation.CreatedBy);

                NoteMigration.MigrateCandidateNote(_candidateNoteCardIndexDataService, extendedUnitOfWork, annotation, noteCreator, candidateId);
            }
        }

        private void MigrateTechnicalReviews(
          CrmOrganizationServiceContext crmContext,
          IExtendedUnitOfWork<IRecruitmentDbScope> extendedUnitOfWork,
          Guid? crmCandidateGuid,
          long candidateId)
        {
            if (crmCandidateGuid == null)
            {
                return;
            }

            var crmTechnicalVerifications = crmContext.CreateQuery<new_technicalverification>()
                .Where(tr => tr.new_CandidateId.Id == crmCandidateGuid);

            foreach (var crmTechnicalVerification in crmTechnicalVerifications)
            {

                var notes = crmContext.CreateQuery<Annotation>().Where(a => a.ObjectId.Id == crmTechnicalVerification.Id).ToList();
                var attachmentNames = string.Join(", ", notes.Select(n => n.FileName));
                var notesText = string.Join(", ", notes.Select(n => n.NoteText));

                var seniorityLevel = _mappingDataHelper.MapToSeniorityLevel(crmTechnicalVerification.smt_Senioritylevelafterverification?.Value);
                var technicalVeryficationNoteContent = $"Technical Verification Note: \n Seniority level: {seniorityLevel.ToString()} " +
                       $"\n Opinion: {crmTechnicalVerification.new_Opinion} \n Notes text: {notesText} \n Attachments: {attachmentNames} \n Technical verificator: {crmTechnicalVerification?.smt_WhoVerified?.Name}";


                var crmTechnicalReviewer = GetCrmTechnicalVeryficatorId(crmContext, crmTechnicalVerification.smt_WhoVerified);

                var technicalReviewerUser = GetUserId(crmTechnicalReviewer?.smt_Id_Dante, extendedUnitOfWork);

                var files = notes
                    .Where(s => !string.IsNullOrEmpty(s.DocumentBody))
                    .Select(s =>
                    {
                        var content = Convert.FromBase64String(s.DocumentBody);
                        return _fileMigration.MigrateCandidateFile(new CandidateFileMigrationDto
                        {
                            Content = content,
                            Size = s.FileSize ?? content.Length,
                            Name = s.FileName,
                            Type = s.MimeType
                        }, RecruitmentDocumentType.TechnicalReview, candidateId);
                    });

                var candidateFilesName = extendedUnitOfWork.Repositories.CandidateFiles
                    .Where(s => s.CandidateId == candidateId)
                    .SelectMany(s => s.File)
                    .Select(d => d.Name)
                    .ToList();

                foreach (var file in files)
                {
                    if (!file.File.Any(s => candidateFilesName.Any(d => d == s.DocumentName)))
                    {
                        _candidateFileCardIndexDataService.AddRecord(file);
                    }
                }

                if (extendedUnitOfWork.Repositories.CandidateNotes.Any(s => s.CandidateId == candidateId && s.Content == technicalVeryficationNoteContent))
                {
                    continue;
                }

                var addedRecords = NoteMigration.AddCandidateNote(_candidateNoteCardIndexDataService, candidateId, technicalVeryficationNoteContent);

                foreach (var addedRecord in addedRecords)
                {
                    NoteMigration.UpdateCreationAndModificationData(technicalReviewerUser, crmTechnicalVerification.CreatedOn.Value, technicalReviewerUser,
                        crmTechnicalVerification.ModifiedOn.Value, addedRecord.AddedRecordId, extendedUnitOfWork, "CandidateNote");
                }
            }
        }


        private void CandidateFilesMigration(IEnumerable<string> documentLocationsPaths, long danteCandidateId)
        {
            if (documentLocationsPaths.Count() == 0)
            {
                return;
            }

            foreach (var documentPath in documentLocationsPaths)
            {
                try
                {
                    var candidateFiles = _sharePointService.GetCandidateDocuments(new List<string> { documentPath });

                    foreach (var file in candidateFiles)
                    {
                        var document = _sharePointService.GetCandidateDocument(file.UniqueId);

                        if (document.Content == null)
                        {
                            continue;
                        }

                        var candidateFileDto = _fileMigration.MigrateCandidateFile(
                            new CandidateFileMigrationDto
                            {
                                Content = document.Content,
                                Size = document.Content.Length,
                                Name = document.Name,
                                Type = MimeMapping.GetMimeMapping(document.Name)
                            }, RecruitmentDocumentType.Other, danteCandidateId);

                        _candidateFileCardIndexDataService.AddRecord(candidateFileDto);
                    }
                }
                catch
                {
                    _logger.Error($"RECRUITMENT_MIGRATION: Path in sharepoint not found {documentPath}");
                }
            }
        }

        #endregion
        protected void ClearCrmLog(OrganizationService crmService, Guid candidateId)
        {
            var newCandidate = new smt_candidate
            {
                Id = candidateId,
                smt_dantemigrationlogerror = null,
            };

            crmService.Update(newCandidate);
        }
    }

    class ApplicationOriginDictionaryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ApplicationOriginType OriginType { get; set; }
    }
}


