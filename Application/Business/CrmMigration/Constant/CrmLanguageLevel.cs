﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CrmMigration.Consts
{
    public static class CrmLanguageLevel
    {
        public const int Basic = 100000000;
        public const int Intermediate = 100000001;
        public const int Advanced = 100000002;
        public const int Fluent = 100000003;
        public const int A1 = 180580000;
        public const int A1_A2 = 180580006;
        public const int A2 = 180580001;
        public const int A2_B1 = 180580007;
        public const int B1 = 180580002;
        public const int B1_B2 = 180580008;
        public const int B2 = 180580003;
        public const int B2_C1 = 180580009;
        public const int C1 = 180580004;
        public const int C1_C2 = 180580010;
        public const int C2 = 180580005;
    }
}
