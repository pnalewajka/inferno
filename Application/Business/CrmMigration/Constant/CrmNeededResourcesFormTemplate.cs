﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public static class CrmNeededResourcesFormTemplate
    {
        public const int Information = 180580000;

        public const int HiringToIntive = 180580002;

        public const int HiringToProject = 180580001;
    }
}
