﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public static class CrmRecruitmentActivityStageName
    {
        public const string Contact = "Contact";

        public const string TechnicalReview = "Technical Review";

        public const string PreRecommendation = "Pre-Recommendation";

        public const string Recommendation = "Recommendation";

        public const string Interview = "Interview";

        public const string WaitingForDecision = "Waiting for decision";

        public const string Offer = "Offer";

        public const string Negotiation = "Negotiation";

        public const string Screen = "Screen";
    }
}
