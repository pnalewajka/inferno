﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CrmMigration.Consts
{
    public static class CrmRelocation
    {
        public const int NoRelocation = 180580000;
        public const int YesRelocation = 180580001;
    }
}
