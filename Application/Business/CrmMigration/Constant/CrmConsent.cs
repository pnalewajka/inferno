﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public class CrmConsent
    {
        public const int No = 180580000;

        public const int Yes = 180580001;

    }
}
