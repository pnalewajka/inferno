﻿using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public static class CrmNeededResourcesServiceLine
    {
        public const string Cognitive = "Cognitive";

        public const string DesignStudioPoland = "Design Studio Poland";

        public const string DigitalPlatforms = "Digital Platforms";

        public const string Embedded = "Embedded";

        public const string EnterpriseApplication = "Enterprise Application";

        public const string Mobile = "Mobile";

        public const string QaCyberSecurity = "QA & Cyber Security";


        public static Dictionary<string, string> OrgUnitCodes = new[]
        {
            new { name = Cognitive, code = "TBU SL C" },
            new { name = DesignStudioPoland, code = "PD BU DSP" },
            new { name = DigitalPlatforms, code = "TBU SL DP" },
            new { name = Embedded, code = "TBU SL E" },
            new { name = EnterpriseApplication, code = "TBU SL EA" },
            new { name = Mobile, code = "TBU PD M" },
            new { name = QaCyberSecurity, code = "TBU SL QA" },
        }
        .ToDictionary(x => x.name, x => x.code);

    }
}
