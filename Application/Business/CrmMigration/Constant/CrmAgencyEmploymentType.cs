﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public static class CrmAgencyEmploymentType
    {
        public const int Outsourcing = 180580000;

        public const int PermanentRecruitment = 180580001;

        public const int Both = 180580002;
    }
}
