﻿namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public class CrmCvContactSource
    {
        public const int Agency = 180580000;
        public const int JobAdvertisement = 100000000;
        public const int ApplicationWithoutNotice = 100000001;
        public const int CareersFairOrCollege = 100000002;
        public const int SocialNetwork = 100000003;
        public const int Recommendation = 100000004;
        public const int Other = 100000005;
    }
}
