﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public class CrmStatusCodeRa
    {
        public const int Active = 1;
        public const int RejectedOffer = 180580000;
        public const int RejectedBySdm = 180580003;
        public const int RejectedByRecruitment = 180580007;
        public const int RejectedByClientPm = 180580002;
        public const int RejectedAfterMeeting = 180580008;
        public const int RejectedBySlm = 180580042;
        public const int OnHold = 180580024;
        public const int NotInterested = 2;
        public const int NoResponseFromCandidate = 180580010;
        public const int FailedTechnicalVerification = 180580023;
        public const int HiredSd = 180580012;
        public const int HiredTa = 180580011;
        public const int HiredMs = 180580020;
        public const int HiredPd = 180580013;
        public const int HiredOtherBu = 180580014;
        public const int HiredToOtherProject = 180580021;
        public const int HiredOtherCandidate = 180580018;
        public const int HiredExternal = 180580004;
        public const int HiredOutsourcing = 180580005;
        public const int HiredInternalRecruitment = 180580006;
        public const int CvRejectedByClient = 180580019;
        public const int CvRejectedByPm = 180580022;
        public const int ClientResignedFromTheRecruitment = 180580017;
        public const int ClientResigned = 180580009;
        public const int CandidateResignedBecauseOfTheProjectClientProces = 180580016;
        public const int CandidateResigned = 180580015;
        public const int HiredEmbedded = 180580025;
        public const int HiredCognitive = 180580026;
        public const int HiredQa = 180580027;
        public const int HiredMobile = 180580028;
        public const int HiredEnterpriseApplications = 180580029;
        public const int HiredDigitalPlatforms = 180580030;
        public const int HiredProductManagementConsulting = 180580031;
        public const int HiredDesignStudioPoland = 180580032;
        public const int HiredAutomotive = 180580034;
        public const int HiredHiTec = 180580035;
        public const int HiredFinancialServices = 180580036;
        public const int HiredTelcoMedia = 180580037;
        public const int HiredIndustryHealthcare = 180580038;
        public const int HiredConsumerServices = 180580039;
        public const int CvRejectedBySlm = 180580040;
    }
}
