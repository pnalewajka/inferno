﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public static class CrmNeededResourcesStatusCode
    {
        public const int Active = 1;

        public const int WaitingForAcceptance = 180580003;

        public const int NotReady = 180580004;

        public const int Fullfiled = 2;

        public const int OnHold = 180580000;

        public const int ClientResigned = 180580002;

        public const int Lost = 180580005;

        public const int LegacyCodeActive = 180580001;
    }
}
