﻿
namespace Smt.Atomic.Business.CrmMigration.Consts
{
    public static class CrmSeniorityLevel
    {
        public const int NA = 180580000;

        public const int L1 = 180580001;

        public const int L1_L2 = 180580002;

        public const int L2 = 180580003;

        public const int L2_L3 = 180580004;

        public const int L3 = 180580005;

        public const int L3_L4 = 180580006;

        public const int L4 = 180580007;

        public const int L4_L5 = 180580008;

        public const int L5 = 180580009;

        public const int Pm1 = 180580015;

        public const int Pm2 = 180580016;
    }
}
