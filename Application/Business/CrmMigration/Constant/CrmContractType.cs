﻿
namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public class CrmContractType
    {
        public const int B2B = 180580000;

        public const int StandardEmployment = 180580001;

        public const int FeeForTask = 180580002;

        public const int ForCommission = 180580003;
    }
}
