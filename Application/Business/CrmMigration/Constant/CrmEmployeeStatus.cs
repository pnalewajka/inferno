﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CrmMigration.Consts
{
    public static class CrmEmployeeStatus
    {
        public const int NotEmployee = 180580000;
        public const int CurrentEmployee = 180580001;
        public const int PastEmployee = 180580002;
    }
}
