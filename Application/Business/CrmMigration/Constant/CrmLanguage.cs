﻿namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public class CrmLanguage
    {
        public const int English = 100000000;
        public const int French = 100000004;
        public const int German = 100000003;
        public const int Japanese = 100000007;
        public const int Polish = 180580000;
        public const int Portuguese = 100000005;
        public const int Russian = 100000002;
        public const int Spanish = 100000001;
    }
}
