﻿using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.CrmMigration.Constant
{
    public static class CrmNeededResourcesVertical
    {
        public const int Automotive = 180580005;

        public const int ConsumerServices = 180580003;

        public const int FinancialServices = 180580001;

        public const int HighTech = 180580000;

        public const int Industrial = 180580004;

        public const int MediaCommunications = 180580002;

        public const int Other = 180580006;

        public static Dictionary<int, string> OrgUnitCodes = new[]
        {
            new { vertical = Automotive, code = "SALES A" },
            new { vertical = ConsumerServices, code = "SALES CS" },
            new { vertical = FinancialServices, code = "SALES FS" },
            new { vertical = HighTech, code = "SALES HT" },
            new { vertical = Industrial, code = "SALESIN" },
            new { vertical = MediaCommunications, code = "SALES M&C" },
        }
        .ToDictionary(x => x.vertical, x => x.code);

    }
}
