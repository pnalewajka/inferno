﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.CrmMigration.Constant;
using Smt.Atomic.Business.CrmMigration.Steps;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.CrmMigration.Helpers
{
    public class RecruitmentProcessStepHelper
    {
        private StringComparer Comparer = StringComparer.OrdinalIgnoreCase;
        private IStep[] listOfInternalSteps;
        private IStep[] listOfClientsSteps;

        public RecruitmentProcessStepHelper()
        {
            InitializeSteps();
        }
       
        public IStep[] GetSteps(long? customerId)
        {
            if (customerId.HasValue)
            {
                return listOfClientsSteps;
            }
            else
            {
                return listOfInternalSteps;
            }
        }

        public Dictionary<string, RecruitmentProcessStepType> GetRecruitmentProcessStepDictionary(long? customerId)
        {
            if (customerId.HasValue)
            {
                return new Dictionary<string, RecruitmentProcessStepType>(Comparer)
                {
                    { CrmRecruitmentActivityStageName.Contact, default(RecruitmentProcessStepType) },
                    { CrmRecruitmentActivityStageName.Screen, RecruitmentProcessStepType.HrCall },
                    { CrmRecruitmentActivityStageName.PreRecommendation, RecruitmentProcessStepType.HrInterview },
                    { CrmRecruitmentActivityStageName.TechnicalReview, RecruitmentProcessStepType.TechnicalReview },
                    { CrmRecruitmentActivityStageName.Recommendation, RecruitmentProcessStepType.ClientResumeApproval },
                    { CrmRecruitmentActivityStageName.Interview, RecruitmentProcessStepType.ClientInterview },
                    { CrmRecruitmentActivityStageName.WaitingForDecision, RecruitmentProcessStepType.ClientInterview },
                    { CrmRecruitmentActivityStageName.Offer, RecruitmentProcessStepType.HiringDecision },
                    { CrmRecruitmentActivityStageName.Negotiation, RecruitmentProcessStepType.ContractNegotiations },
                };
            }
            else
            {
                return new Dictionary<string, RecruitmentProcessStepType>(Comparer)
                {
                    { CrmRecruitmentActivityStageName.Contact, default(RecruitmentProcessStepType) },
                    { CrmRecruitmentActivityStageName.Screen, RecruitmentProcessStepType.HrCall },
                    { CrmRecruitmentActivityStageName.PreRecommendation, RecruitmentProcessStepType.HrInterview },
                    { CrmRecruitmentActivityStageName.TechnicalReview, RecruitmentProcessStepType.TechnicalReview },
                    { CrmRecruitmentActivityStageName.Recommendation, RecruitmentProcessStepType.HiringManagerResumeApproval },
                    { CrmRecruitmentActivityStageName.Interview, RecruitmentProcessStepType.HiringManagerInterview },
                    { CrmRecruitmentActivityStageName.WaitingForDecision, RecruitmentProcessStepType.HiringManagerInterview },
                    { CrmRecruitmentActivityStageName.Offer, RecruitmentProcessStepType.HiringDecision },
                    { CrmRecruitmentActivityStageName.Negotiation, RecruitmentProcessStepType.ContractNegotiations },
                };
            }
        }

        private void InitializeSteps()
        {
            listOfInternalSteps = new IStep[]
            {
                new HrCallStep(),
                new HrInterviewStep(),
                new TechnicalReviewStep(),
                new HiringManagerResumeApprovalStep(),
                new HiringManagerInterviewStep(),
                new HiringDecisionStep(),
                new ContractNegotiationsStep(),
            };

            listOfClientsSteps = new IStep[]
            {
                new HrCallStep(),
                new HrInterviewStep(),
                new TechnicalReviewStep(),
                new ClientResumeApprovalStep(),
                new ClientInterviewStep(),
                new HiringDecisionStep(),
                new ContractNegotiationsStep(),
            };
        }
    }
}
