﻿using System;
using System.Collections.Generic;
using Castle.Core.Logging;

namespace Smt.Atomic.Business.CrmMigration.Helpers
{
    public class ConversionHelper
    {
        private readonly ILogger _logger;

        public ConversionHelper(ILogger logger)
        {
            _logger = logger;
        }

        public string SubstringString(string value, int length, string fieldName)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            value = value.Trim();

            if (value.Length <= length)
            {
                return value;
            }

            _logger.Warn($"RECRUITMENT_MIGRATION: Field {fieldName} with Length {value.Length} was truncated to {length}, value: {value}");

            return value.Substring(0, length);
        }

        public TResult GetValue<TKey, TResult>(IDictionary<TKey, TResult> dictionary, TKey? key, string fieldName, Guid entityId)
            where TKey : struct
        {
            if (key == null)
            {
                _logger.Warn($"RECRUITMENT_MIGRATION {entityId}: Field {fieldName} invalid key {key} was provided for IDictionary<{typeof(TKey)}, {typeof(TResult)}>");

                return default(TResult);
            }

            var result = dictionary.TryGetValue(key.Value, out var value);

            if (!result)
            {
                _logger.Warn($"RECRUITMENT_MIGRATION {entityId}: value for key {key} wasn't found");
            }

            return value;
        }

        public TResult GetValue<TResult>(IDictionary<string, TResult> dictionary, string key, string fieldName, Guid entityId)
        {
            if (key == null)
            {
                return default(TResult);
            }

            var result = dictionary.TryGetValue(key.Trim(), out var value);

            if (!result)
            {
                _logger.Warn($"RECRUITMENT_MIGRATION {entityId}: value for key {key} wasn't found");
            }

            return value;
        }

    }
}
