﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Accounts.Jobs
{
    [Identifier("Job.Accounts.UserPictureSyncJob")]
    [JobDefinition(
        Code = "UserPictureSyncJob",
        Description = "User picture sync job",
        ScheduleSettings = "0 0 0 * * *",
        MaxExecutioPeriodInSeconds = 2400,
        PipelineName = PipelineCodes.System)]
    internal class UserPictureSyncJob : IJob
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;
        private readonly ILogger _logger;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ISettingsProvider _settingsProvider;

        public UserPictureSyncJob(
            ITimeService timeService, 
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService,
            ILogger logger,
            ISystemParameterService systemParameterService,
            ISettingsProvider settingsProvider)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _logger = logger;
            _systemParameterService = systemParameterService;
            _settingsProvider = settingsProvider;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                SynchronizeUserPicturesWithActiveDirectory(executionContext.CancellationToken);

                return JobResult.Success();
            }
            catch
            {
                return JobResult.Fail();
            }
        }

        public void SynchronizeUserPicturesWithActiveDirectory(CancellationToken cancellationToken)
        {
            var login = _systemParameterService.GetParameter<string>(ParameterKeys.ExchangeSyncUser);
            var password = _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.Password;
            var credential = new NetworkCredential(login, password);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                foreach (var user in unitOfWork.Repositories.Users.Where(u => u.IsActive).TakeWhileNotCancelled(cancellationToken))
                {
                    var completeUrl = $"https://outlook.office365.com/ews/exchange.asmx/s/GetUserPhoto?email={user.Email}&size=HR240x240";

                    var request = WebRequest.Create(completeUrl);

                    request.Credentials = credential;

                    if (user.Picture != null)
                    {
                        request.Headers.Add("If-None-Match", user.Picture.ETag);
                    }

                    try
                    {
                        HttpWebResponse response = (HttpWebResponse)(request.GetResponse());

                        if ((response.StatusCode == HttpStatusCode.OK)
                        && response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                        {
                            byte[] buffer = null;

                            using (Image img = Image.FromStream(response.GetResponseStream()))
                            {
                                using (var pngStream = new MemoryStream())
                                {
                                    img.Save(pngStream, ImageFormat.Png);
                                    buffer = pngStream.ToArray();
                                }
                            }

                            UpdateUserPicture(user.Id, buffer, response.GetResponseHeader("ETag"));
                        }
                    }
                    catch (Exception ex)
                    {
                        var webException = ex as WebException;

                        var response = webException?.Response as HttpWebResponse;

                        if (response == null || (response.StatusCode != HttpStatusCode.NotFound && response.StatusCode != HttpStatusCode.NotModified))
                        {
                            _logger.Error($"Exception during synchronizing picture for user {user.Id}. Exception: {ex}");
                        }
                    }
                }
            }
        }

        public void UpdateUserPicture(long userId, byte[] pictureContent, string ETag)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.Single(u => u.Id == userId);

                if (user.Picture != null && user.Picture.DocumentContent != null)
                {
                    user.Picture.DocumentContent.Content = pictureContent;

                    user.Picture.ContentLength = pictureContent.Length;
                    user.Picture.ContentType = MimeHelper.PngImageFile;
                    user.Picture.ETag = ETag;
                    user.Picture.Name = user.Email;

                    unitOfWork.Repositories.UserPictureContent.Edit(user.Picture.DocumentContent);
                    unitOfWork.Repositories.UserPicture.Edit(user.Picture);
                    unitOfWork.Commit();
                }
                else
                {
                    var content = new UserPictureContent()
                    {
                        Content = pictureContent
                    };

                    var picture = new UserPicture()
                    {
                        ContentLength = pictureContent.Length,
                        ContentType = MimeHelper.PngImageFile,
                        ETag = ETag,
                        ModifiedOn = _timeService.GetCurrentTime(),
                        Name = user.Email,
                        DocumentContent = content
                    };

                    unitOfWork.Repositories.UserPictureContent.Add(content);
                    unitOfWork.Repositories.UserPicture.Add(picture);
                    user.Picture = picture;
                    unitOfWork.Commit();
                }
            }
        }
    }
}
