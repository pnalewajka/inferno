﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using System;
using System.Linq;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Interfaces;
using System.Linq.Expressions;
using Smt.Atomic.Data.Repositories.Scopes;
using System.IO;
using ImageProcessor;
using System.Drawing;
using ImageProcessor.Imaging.Formats;
using System.Threading;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Accounts.Jobs
{
    [Identifier("Job.Accounts.ThumbnailGenerationJob")]
    [JobDefinition(
        Code = "ThumbnailGenerationJob",
        Description = "Generating thumbnails for all users",
        ScheduleSettings = "0 0 0 * * *",
        PipelineName = PipelineCodes.System,
        MaxExecutioPeriodInSeconds = 2400)]
    public class ThumbnailGenerationJob : IJob
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;
        private const int _thumbnailWidth = 100;
        private const int _thumbnailHeight = 100;

        public ThumbnailGenerationJob(
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                GenerateThumbnails<UserPictureContent, IAccountsDbScope>(
                    uow => uow.Repositories.UserPictureContent,
                    e => e.Content,
                    e => e.Thumbnail,
                    e => e.Thumbnail == null,
                    _thumbnailWidth,
                    _thumbnailHeight,
                    executionContext.CancellationToken);

                return JobResult.Success();
            }
            catch (Exception)
            {
                return JobResult.Fail();
            }
        }

        private void GenerateThumbnails<TEntity, TDbScope>(
            Func<IUnitOfWork<TDbScope>, IRepository<TEntity>> repository,
            Func<TEntity, byte[]> inputImageFunc,
            Expression<Func<TEntity, byte[]>> thumbnailExpression,
            Func<TEntity, bool> processingContition,
            int width,
            int height,
            CancellationToken cancellationToken)
            where TDbScope : class, IDbScope
            where TEntity : class, IEntity
        {
            var smallImageSize = new Size(width, height);
            var smallImageFormat = new PngFormat();

            var thumbnailExpressionBody = (MemberExpression)thumbnailExpression.Body;
            var thumbnailPropertyName = thumbnailExpressionBody.Member.Name;

            using (var unitOfWorkRead = _unitOfWorkService.Create())
            {
                var repositoryRead = repository((IUnitOfWork<TDbScope>)unitOfWorkRead);

                var filteredEntitiesIds = processingContition != null 
                                          ? repositoryRead.Where(processingContition).Select(e => e.Id) 
                                          : repositoryRead.Select(e => e.Id);

                foreach (var entityId in filteredEntitiesIds.TakeWhileNotCancelled(cancellationToken))
                {
                    using (var unitOfWorkWrite = _unitOfWorkService.Create())
                    {
                        var repositoryWrite = repository((IUnitOfWork<TDbScope>)unitOfWorkWrite);

                        var entity = repositoryWrite.FirstOrDefault(e => e.Id == entityId);
                        var normalPictureContent = inputImageFunc(entity);
                        var thumbnailContent = GenerateThumbnail(normalPictureContent, smallImageSize, smallImageFormat);

                        entity.GetType().GetProperty(thumbnailPropertyName).SetValue(entity, thumbnailContent);
                        repositoryWrite.Edit(entity);

                        unitOfWorkWrite.Commit();
                    }
                }
            }
        }

        private byte[] GenerateThumbnail(byte[] inputImage, Size size, ISupportedImageFormat format)
        {
            using (MemoryStream inStream = new MemoryStream(inputImage))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        imageFactory.Load(inStream)
                            .Resize(size)
                            .Format(format)
                            .Save(outStream);
                    }

                    return outStream.ToArray();
                }
            }
        }
    }
}
