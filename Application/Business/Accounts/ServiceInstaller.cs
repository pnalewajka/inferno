﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Accounts.BusinessEventHandlers;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Accounts.ChoreProviders;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Services;
using Smt.Atomic.Business.Accounts.Services.SecurityProviders;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Services;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Accounts
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<AddUserToProfilesEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<RemoveUserFromProfilesEventHandler>().LifestyleTransient());
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IChoreProvider>().ImplementedBy<MissingAvatarChoreProvider>().LifestylePerWebRequest());
            }

            switch (containerType)
            {
                case ContainerType.JobScheduler:
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                case ContainerType.WebServices:
                case ContainerType.UnitTests:
                    container.Register(Component.For<ILoginService>().ImplementedBy<LoginService>().LifestyleTransient());
                    container.Register(Component.For<IUserService>().ImplementedBy<UserService>().LifestyleTransient());
                    container.Register(Component.For<ISecurityService>().ImplementedBy<SecurityService>().LifestyleTransient());
                    container.Register(Component.For<IUserCardIndexService>().ImplementedBy<UserCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ISecurityRoleCardIndexService>().ImplementedBy<SecurityRoleCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<IEntityTypeCardIndexService>().ImplementedBy<EntityTypeCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ISecurityProfileCardIndexService>().ImplementedBy<SecurityProfileCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<IProfileDataFilterCardIndexService>().ImplementedBy<ProfileDataFilterCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<IUserDataFilterCardIndexService>().ImplementedBy<UserDataFilterCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<IUserPermissionService>().ImplementedBy<UserPermissionService>().LifestyleTransient());
                    container.Register(Component.For<IPasswordGenerator>().ImplementedBy<SimplePasswordGenerator>().LifestyleTransient());
                    container.Register(Component.For<IDirectoryService>().ImplementedBy<DirectoryService>().LifestyleTransient());
                    container.Register(Classes.FromThisAssembly().BasedOn<IDocumentMapping>().LifestyleTransient());
                    container.Register(Classes.FromThisAssembly().BasedOn<IBusinessEventHandler>().WithService.FromInterface().LifestyleTransient());
                    container.Register(Component.For<ILocationService>().ImplementedBy<LocationService>().LifestyleTransient());
                    container.Register(Component.For<ISecurityProvider>().ImplementedBy<AtomicSecurityProvider>().Named("AtomicSecurityProvider").LifestyleTransient());
                    container.Register(Component.For<ISecurityProvider>().ImplementedBy<ActiveDirectorySecurityProvider>().Named("ActiveDirectorySecurityProvider").LifestyleTransient());
                    container.Register(Component.For<ISecurityProvider>().ImplementedBy<UnknownUserSecurityProvider>().Named("UnknownUserSecurityProvider").LifestyleTransient());
                    container.Register(Component.For<ISecurityProviderFactory>().AsFactory().LifestyleTransient());
                    container.Register(Component.For<ISecurityReportService>().ImplementedBy<SecurityReportService>().LifestyleTransient());
                    container.Register(Component.For<IUserBusinessLogic>().ImplementedBy<UserBusinessLogic>().LifestyleTransient());
                    container.Register(Component.For<IUserChoresCardIndexDataService>().ImplementedBy<UserChoresCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IProfileUserCardIndexService>().ImplementedBy<ProfileUserCardIndexService>().LifestyleTransient());
                    break;
            }
        }
    }
}