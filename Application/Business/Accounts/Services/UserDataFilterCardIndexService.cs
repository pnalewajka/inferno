using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class UserDataFilterCardIndexService : CardIndexDataService<DataFilterDto, DataFilter, IAccountsDbScope>, IUserDataFilterCardIndexService
    {
        private readonly IDataFilterService _dataFilterService;
        private readonly IEntityTypeCardIndexService _entityTypeCardIndexService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public UserDataFilterCardIndexService(
            ICardIndexServiceDependencies<IAccountsDbScope> dependencies,
            IDataFilterService dataFilterService,
            IEntityTypeCardIndexService entityTypeCardIndexService)
            : base(dependencies)
        {
            _dataFilterService = dataFilterService;
            _entityTypeCardIndexService = entityTypeCardIndexService;
            RelatedCacheAreas = new[] {CacheAreas.DataPermissions};
        }

        public override IQueryable<DataFilter> ApplyContextFiltering(IQueryable<DataFilter> records, object context)
        {
            var parentIdContext = (ParentIdContext)context;

            return records.Where(r => r.UserId == parentIdContext.ParentId);
        }

        protected override IEnumerable<Expression<Func<DataFilter, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Condition;
            yield return r => r.Entity;
            yield return r => r.Module;
        }

        public override void SetContext(DataFilter entity, object context)
        {
            var parentIdContext = (ParentIdContext)context;
            Debug.Assert(parentIdContext != null, "parentIdContext != null");
            entity.UserId = parentIdContext.ParentId;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<DataFilter, DataFilterDto, IAccountsDbScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);

            ValidateDataFilter(eventArgs.InputEntity, eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<DataFilter, DataFilterDto, IAccountsDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            ValidateDataFilter(eventArgs.InputEntity, eventArgs);
        }

        private void ValidateDataFilter(DataFilter dataFilter, CardIndexEventArgs eventArgs)
        {
            var entity = $"{dataFilter.Module}.{dataFilter.Entity}";

            foreach (var error in _dataFilterService.ValidateCondition(entity, dataFilter.Condition))
            {
                eventArgs.AddError(error);
            }
        }

        public override DataFilterDto GetDefaultNewRecord()
        {
            var defaultNewRecord = base.GetDefaultNewRecord();
            defaultNewRecord.Entity = _entityTypeCardIndexService.GetRecords().Rows[0].Type;

            return defaultNewRecord;
        }
    }
}