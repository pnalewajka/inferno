﻿using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    class ProfileUserCardIndexService : UserCardIndexService, IProfileUserCardIndexService
    {
        public ProfileUserCardIndexService(
            ICardIndexServiceDependencies<IAccountsDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IDataActivityLogService dataActivityLogService)
            : base(uploadedDocumentHandlingService, dependencies, dataActivityLogService)
        {
        }

        public override IQueryable<User> ApplyContextFiltering(IQueryable<User> records, object context)
        {
            var parentIdContext = (IParentIdContext)context;

            records = records.Where(u => u.Profiles.Any(p => p.Id == parentIdContext.ParentId));

            return base.ApplyContextFiltering(records, context);
        }
    }
}
