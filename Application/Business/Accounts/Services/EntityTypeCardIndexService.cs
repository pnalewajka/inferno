using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class EntityTypeCardIndexService : CustomDataCardIndexDataService<EntityTypeDto>, IEntityTypeCardIndexService
    {
        private static List<EntityTypeDto> _records;

        protected override IList<EntityTypeDto> GetAllRecords(object context = null)
        {
            if (_records == null)
            {
                var coreContextProperties = TypeHelper.GetPublicInstanceProperties(typeof(CoreEntitiesContext));

                _records = coreContextProperties
                    .Where(i => TypeHelper.IsSubclassOfRawGeneric(i.PropertyType, typeof(DbSet<>)))
                    .Select(GetDtoFromPropertyType)
                    .OrderBy(r => r.Type)
                    .ToList();

                for (int index = 0; index < _records.Count; index++)
                {
                    _records[index].Id = index + 1;
                }
            }

            return _records;
        }

        private EntityTypeDto GetDtoFromPropertyType(PropertyInfo propertyInfo)
        {
            var entityType = TypeHelper.GetRawGenericArguments(propertyInfo.PropertyType, typeof(DbSet<>))[0];
            Debug.Assert(entityType.Namespace != null, "entityType.Namespace != null");
            var namespaceElements = entityType.Namespace.Split(new[]{'.'});
        
            return new EntityTypeDto
            {
                Type = entityType.Name,
                Namespace = entityType.Namespace,
                Module = namespaceElements.Last()
            };
        }

        public override EntityTypeDto GetRecordById(long id)
        {
            return GetAllRecords().Single(r => r.Id == id);
        }

        public override EntityTypeDto GetRecordByIdOrDefault(long id)
        {
            return GetAllRecords().SingleOrDefault(r => r.Id == id);
        }

        protected override IEnumerable<Expression<Func<EntityTypeDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Type;
            yield return r => r.Module;
        }

        public string GetEntityTypeName(long entityTypeId)
        {
            if (entityTypeId != 0)
            {
                return GetRecordById(entityTypeId).Type;
            }

            return null;
        }

        public string GetEntityModule(long entityTypeId)
        {
            if (entityTypeId != 0)
            {
                return GetRecordById(entityTypeId).Namespace;
            }

            return null;
        }

        public long GetEntityTypeId(string entityTypeName)
        {
            return GetAllRecords().Single(r => r.Type == entityTypeName).Id;
        }
    }
}