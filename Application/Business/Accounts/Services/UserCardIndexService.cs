using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Internal;
using Smt.Atomic.Business.Accounts.BusinessEvents;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Accounts.Consts;
using Smt.Atomic.Business.Accounts.DocumentMappings;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    public class UserCardIndexService : CardIndexDataService<UserDto, User, IAccountsDbScope>, IUserCardIndexService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public UserCardIndexService(
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            ICardIndexServiceDependencies<IAccountsDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _dataActivityLogService = dataActivityLogService;
        }

        protected override IEnumerable<Expression<Func<User, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return u => u.FirstName;
                yield return u => u.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Login))
            {
                yield return u => u.Login;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Email))
            {
                yield return u => u.Email;
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<User, UserDto, IAccountsDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork, eventArgs.InputEntity.Documents, eventArgs.InputDto.Documents,
                p => new UserDocument
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });            

            eventArgs.InputEntity.Picture = _uploadedDocumentHandlingService.UpdateRelatedDocument(eventArgs.UnitOfWork, eventArgs.InputEntity.Picture, eventArgs.InputDto.Picture,
            p => new UserPicture
            {
                Name = p.DocumentName,
                ContentType = p.ContentType
            });
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<User, UserDto, IAccountsDbScope> eventArgs)
        {
            eventArgs.InputDto.ActiveDirectoryId = eventArgs.DbEntity.ActiveDirectoryId;
            eventArgs.InputEntity.ActiveDirectoryId = eventArgs.DbEntity.ActiveDirectoryId;

            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Documents, eventArgs.InputDto.Documents,
                p => new UserDocument
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });

            eventArgs.DbEntity.Picture = _uploadedDocumentHandlingService.UpdateRelatedDocument(eventArgs.UnitOfWork, eventArgs.DbEntity.Picture, eventArgs.InputDto.Picture,
            p => new UserPicture
            {
                Name = p.DocumentName,
                ContentType = p.ContentType
            });
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<User, UserDto> eventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<UserDocument, UserDocumentContent, UserDocumentMapping, IAccountsDbScope>(eventArgs.InputDto.Documents);

            if (eventArgs.InputDto.Picture != null)
            {
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<UserPicture, UserPictureContent, UserPictureMapping, IAccountsDbScope>(new[] { eventArgs.InputDto.Picture });
            }

            var alert = new AlertDto
            {
                Message = string.Format(UserCardIndexServiceResources.UserCardIndexServiceOnRecordAdded, eventArgs.InputEntity.Login),
                Type = AlertType.Success,
                IsDismissable = true
            };

            eventArgs.Alerts.Add(alert);

            _dataActivityLogService.LogUserDataActivity(eventArgs.InputEntity.Id, ActivityType.CreatedRecord);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<User, UserDto> eventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<UserDocument, UserDocumentContent, UserDocumentMapping, IAccountsDbScope>(eventArgs.InputDto.Documents);

            if (eventArgs.InputDto.Picture != null)
            {
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<UserPicture, UserPictureContent, UserPictureMapping, IAccountsDbScope>(new[] { eventArgs.InputDto.Picture });
            }
            
            var alert = new AlertDto
            {
                Message = string.Format(UserCardIndexServiceResources.UserCardIndexServiceOnRecordEdited, eventArgs.InputEntity.Login),
                Type = AlertType.Success,
                IsDismissable = true
            };

            eventArgs.Alerts.Add(alert);

            _dataActivityLogService.LogUserDataActivity(eventArgs.InputEntity.Id, ActivityType.RemovedRecord);
        }

        protected override IEnumerable<BusinessEvent> GetAddRecordEvents(AddRecordResult result, UserDto record)
        {
            var events = base.GetAddRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[]
                {
                    new UserAddedBusinessEvent
                    {
                        UserId = result.AddedRecordId,
                        RecordData = new UserBusinessEventData(record),
                    }
                });
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(
            EditRecordResult result,
            UserDto newRecord,
            UserDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[]
                {
                    new UserEditedBusinessEvent
                    {
                        UserId = result.EditedRecordId,
                        ModifiedById = newRecord.ModifiedById,
                        ModifiedOn = newRecord.ModifiedOn,
                        NewRecordData = new UserBusinessEventData(newRecord),
                        OriginalRecordData = new UserBusinessEventData(originalRecord),
                    }
                });
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, UserDto record)
        {
            var events = base.GetDeleteRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[]
                {
                    new UserDeletedBusinessEvent
                    {
                        UserId = record.Id,
                        RecordData = new UserBusinessEventData(record),
                    }
                });
            }

            return events;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<User, IAccountsDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);

            eventArgs.UnitOfWork.Repositories.UserDocuments.DeleteEach(p => p.UserId == eventArgs.DeletingRecord.Id);
            eventArgs.UnitOfWork.Repositories.Credentials.DeleteEach(c => c.UserId == eventArgs.DeletingRecord.Id);
            eventArgs.UnitOfWork.Repositories.DataOwners.Where(d => d.UserId == eventArgs.DeletingRecord.Id).ForEach(d => d.UserId = null);
        }

        protected override NamedFilters<User> NamedFilters
        {
            get
            {
                return new NamedFilters<User>(new[]
                {
                    new NamedFilter<User>(FilterCodes.UserStatusActive, u => u.IsActive),
                    new NamedFilter<User>(FilterCodes.UserStatusInactive, u => !u.IsActive),
                    new NamedFilter<User, DateTime, DateTime>(FilterCodes.UserLastModifiedBetweenDates,
                        (u, from, to) => !(u.LastLoggedOn < from || u.LastLoggedOn >= to)),
                    new NamedDtoFilter<User, UserLastLoggedOnBetweenDatesFilterDto>(
                        FilterCodes.UserLastModifiedBetweenDatesWithDto,
                        (e, d) => !(e.LastLoggedOn < d.From.Value || e.LastLoggedOn >= d.To.Value)),
                    new NamedFilter<User, long>(
                        FilterCodes.UserRoles,
                        (user, roleId) => user.Roles.Select(r => r.Id).Contains(roleId)
                        ),
                    new NamedFilter<User, long>(
                        FilterCodes.UserProfiles,
                        (user, profileId) => user.Profiles.Select(r => r.Id).Contains(profileId)
                        )
                }
                    .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<User>()));
            }
        }

        protected override Type GetSystemFilterTypeParameter()
        {
            return typeof(User);
        }

        public override IQueryable<User> ApplyContextFiltering(IQueryable<User> records, object context)
        {
            var userPickerContext = context as UserPickerContext;

            if (userPickerContext != null)
            {
                if (userPickerContext.IsActivelyWorking != null)
                {
                    records = records.Where(UserBusinessLogic.IsActivelyWorking.AsExpression()
                        .Xor(u => !userPickerContext.IsActivelyWorking.Value));
                }
            }

            return records;
        }

        protected override IQueryable<User> ConfigureIncludes(IQueryable<User> sourceQueryable)
        {
            return sourceQueryable
                .Include(x => x.Documents)
                .Include(x => x.Picture);
        }
    }
}