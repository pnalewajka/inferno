using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class SecurityRoleCardIndexService : EnumBasedCardIndexDataService<SecurityRoleDto, SecurityRoleType>, ISecurityRoleCardIndexService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public SecurityRoleCardIndexService(IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        protected override SecurityRoleDto ConvertToDto(SecurityRoleType enumValue)
        {
            return new SecurityRoleDto
            {
                Id = (long)enumValue,
                Name = enumValue.ToString(),
                Description = enumValue.GetDescription(),
                Module = GetModuleDescription(enumValue)
            };
        }

        private string GetModuleDescription(SecurityRoleType enumValue)
        {
            try
            {
                var moduleDescription = EnumHelper.GetValueAssignedToEnum<string, SecurityRoleDescriptionAttribute>(enumValue, a => a.Module.GetDescription());

                return moduleDescription;
            }
            catch (AttributeNotFoundException<SecurityRoleDescriptionAttribute>)
            {
                return string.Empty;
            }
        }

        protected override IEnumerable<Expression<Func<SecurityRoleDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return dto => dto.Name;
            yield return dto => dto.Description;
            yield return dto => dto.Module;
        }

        protected override IList<SecurityRoleDto> ApplyContextFiltering(IList<SecurityRoleDto> records, object context)
        {
            var userItemsContext = context as UserItemsContext;

            if (userItemsContext != null)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var user =
                        unitOfWork
                        .Repositories
                        .Users
                        .GetByIdOrDefault(userItemsContext.ParentId);

                    if (user != null)
                    {
                        var ids = user.Roles.GetIds();

                        return records.Where(r => userItemsContext.ShouldExclude ^ ids.Any(i => i == r.Id)).ToList();
                    }
                }
            }

            return records;
        }
    }
}