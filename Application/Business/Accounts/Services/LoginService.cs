﻿using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    public class LoginService : ILoginService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _accountsUnitOfWorkService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;

        public LoginService(
            IUnitOfWorkService<IAccountsDbScope> accountsUnitOfWorkService,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService)
        {
            _accountsUnitOfWorkService = accountsUnitOfWorkService;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
        }

        public string ResolveLogin(string identifier)
        {
            using (var accountsUnitOfWork = _accountsUnitOfWorkService.Create())
            {
                if (IsExistingLogin(accountsUnitOfWork, identifier))
                {
                    return identifier;
                }

                return GetLoginByEmail(accountsUnitOfWork, identifier) ??
                       GetLoginByAcronym(identifier) ??
                       identifier;
            }
        }

        private bool IsExistingLogin(IUnitOfWork<IAccountsDbScope> unitOfWork, string login)
        {
            return unitOfWork.Repositories.Users
                .Any(u => u.Login == login);
        }

        private string GetLoginByEmail(IUnitOfWork<IAccountsDbScope> unitOfWork, string email)
        {
            return unitOfWork.Repositories.Users
                .Where(u => u.Email == email)
                .Select(u => u.Login)
                .SingleOrDefault();
        }

        private string GetLoginByAcronym(string acronym)
        {
            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(e => e.Acronym.ToLower() == acronym.ToLower())
                    .Select(e => e.User.Login)
                    .SingleOrDefault();
            }
        }
    }
}
