﻿using System;
using System.Linq;
using System.Security.Cryptography;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services.SecurityProviders
{
    internal class AtomicSecurityProvider : BaseSecurityProvider, ISecurityProvider
    {
        private const int PasswordHashLength = 32;
        private const int SaltLength = 16;
        private const int MaximumPasswordAgeInDays = 30;
        private readonly CredentialDto _currentUserCredentialDto;
        private readonly UserDto _currentUserDto;

        private readonly string[] _roles;
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public AtomicSecurityProvider(IUnitOfWorkService<IAccountsDbScope> unitOfWorkService, ILogger logger,
            ITimeService timeService, UserDto currentUserDto, string[] roles)
            : base(logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _currentUserDto = currentUserDto;
            _roles = roles;
        }

        public AtomicSecurityProvider(IUnitOfWorkService<IAccountsDbScope> unitOfWorkService, ILogger logger, 
            ITimeService timeService, UserDto currentUserDto, CredentialDto currentUserCredentialDto, string[] roles)
            : this(unitOfWorkService, logger, timeService, currentUserDto, roles)
        {
            _currentUserCredentialDto = currentUserCredentialDto;
        }

        public bool CanChangePassword()
        {
            return true;
        }

        public BoolResult ChangePassword(string oldPassword, string password)
        {
            return SetPassword(password);
        }

        public BoolResult SetPassword(string password)
        {
            string base64PasswordHash, base64Salt;
            CreateUserPasswordCredentials(password, out base64PasswordHash, out base64Salt);
            UpdateUserCredentials(base64PasswordHash, base64Salt);

            return new BoolResult(true);
        }

        public UserValidationResult ValidatePassword(string password)
        {
            if (!_currentUserDto.IsActive)
            {
                Logger.WarnFormat($"User '{_currentUserDto.Login}' is not active.");

                return GetInvalidUserResult(SecurityServiceResources.UserIsInactiveMessage);
            }

            if (_currentUserCredentialDto == null || !ValidateCredential(password))
            {
                Logger.WarnFormat($"User '{_currentUserDto.Login}' has not provided correct credentials.");

                return GetInvalidUserResult(SecurityServiceResources.LoginOrPasswordInvalidMessage);
            }

            if (_currentUserDto.LoginTokenId != null && (_currentUserCredentialDto.IsExpired || !IsCreationDateValidated()))
            {
                Logger.WarnFormat($"Password for user '{_currentUserDto.Login}' is expired.");

                return GetExpiredPasswordResult(SecurityServiceResources.PasswordExpiredMessage);
            }

            return new UserValidationResult
            {
                UserId = _currentUserDto.Id,
                Login = _currentUserDto.Login,
                Roles = _roles,
                IsUserAuthenticated = true
            };
        }

        private bool IsCreationDateValidated()
        {
            var actualPasswordAgeInDays = DateTime.Today.Date.Subtract(_currentUserCredentialDto.CreatedOn.Date).Days;

            return actualPasswordAgeInDays <= MaximumPasswordAgeInDays;
        }

        private void CreateUserPasswordCredentials(string password, out string base64PasswordHash, out string base64Salt)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, SaltLength))
            {
                var generatedPasswordHash = pbkdf2.GetBytes(PasswordHashLength);
                var generatedSalt = pbkdf2.Salt;

                base64PasswordHash = Convert.ToBase64String(generatedPasswordHash);
                base64Salt = Convert.ToBase64String(generatedSalt);
            }
        }

        private void UpdateUserCredentials(string base64PasswordHash, string base64Salt)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                foreach (
                    var credential in
                        unitOfWork.Repositories.Credentials.Where(
                            c => c.UserId == _currentUserDto.Id && c.ProviderType == AuthorizationProviderType.Atomic))
                {
                    credential.IsExpired = true;
                }

                var user = unitOfWork.Repositories.Users.First(u => u.Id == _currentUserDto.Id);
                user.LoginToken = null;

                var newCredentials = new Credential
                {
                    PasswordHash = base64PasswordHash,
                    Salt = base64Salt,
                    CreatedOn = _timeService.GetCurrentTime(),
                    UserId = _currentUserDto.Id,
                    ProviderType = AuthorizationProviderType.Atomic,
                    Email = _currentUserDto.Email
                };

                unitOfWork.Repositories.Credentials.Add(newCredentials);

                unitOfWork.Commit();
            }
        }

        private bool ValidateCredential(string passwordToVerify)
        {
            var salt = Convert.FromBase64String(_currentUserCredentialDto.Salt);

            using (var pbkdf2 = new Rfc2898DeriveBytes(passwordToVerify, salt))
            {
                var generatedPasswordHash = pbkdf2.GetBytes(PasswordHashLength);
                var storedPasswordHash = Convert.FromBase64String(_currentUserCredentialDto.PasswordHash);
                var isValid = generatedPasswordHash.Length > 0
                              && generatedPasswordHash.SequenceEqual(storedPasswordHash);

                return isValid;
            }
        }
    }
}