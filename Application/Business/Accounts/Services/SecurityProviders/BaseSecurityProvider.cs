﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Accounts.Services.SecurityProviders
{
    internal abstract class BaseSecurityProvider
    {
        protected readonly ILogger Logger;

        protected BaseSecurityProvider(ILogger logger)
        {
            Logger = logger;
        }

        protected UserValidationResult GetInvalidUserResult(string message)
        {
            var validationResult = CreateValidationResult(message, false);

            return validationResult;
        }

        protected UserValidationResult GetExpiredPasswordResult(string message)
        {
            var validationResult = CreateValidationResult(message, true);

            return validationResult;
        }

        private static UserValidationResult CreateValidationResult(string message, bool isPasswordExpired)
        {
            var validationResult = new UserValidationResult
            {
                IsUserAuthenticated = false,
                IsPasswordExpired = isPasswordExpired
            };

            validationResult.AddAlert(AlertType.Error, message);
            return validationResult;
        }
    }
}