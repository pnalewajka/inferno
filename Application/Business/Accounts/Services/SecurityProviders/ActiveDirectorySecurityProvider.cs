﻿using System;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Net;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Settings.Interfaces;

namespace Smt.Atomic.Business.Accounts.Services.SecurityProviders
{
    internal class ActiveDirectorySecurityProvider : BaseSecurityProvider, ISecurityProvider
    {
        private readonly UserDto _currentUserDto;
        private readonly string[] _roles;
        private readonly ILogger _logger;
        private readonly ISettingsProvider _settingsProvider;
      
        public ActiveDirectorySecurityProvider(ILogger logger, ISettingsProvider settingsProvider,
            UserDto currentUserDto, string[] roles)
            : base(logger)
        {
            _logger = logger;
            _settingsProvider = settingsProvider;
            _currentUserDto = currentUserDto;
            _roles = roles;
        }

        private string LdapServerName
            => _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.ConnectionString;

        private string LdapDomain => _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.Domain;

        private string LdapPath => _settingsProvider.AuthorizationProviderSettings.ActiveDirectorySettings.Path;

        public bool CanChangePassword()
        {
            return true;
        }

        public BoolResult ChangePassword(string oldPassword, string password)
        {
            var errorMessage = SecurityServiceResources.ActiveDirectoryPasswordChangeFailedMessage;

            try
            {
                using (var directionEntry = new DirectoryEntry(LdapServerName, $"{LdapDomain}\\{_currentUserDto.Login}", oldPassword))
                {
                    directionEntry.Path = LdapPath;

                    using (var search = new DirectorySearcher(directionEntry)
                    {
                        Filter = $"(sAMAccountName={_currentUserDto.Login})"
                    })
                    {
                        var result = search.FindOne();
                        var userEntry = result?.GetDirectoryEntry();

                        if (userEntry != null)
                        {
                            userEntry.Invoke("ChangePassword", new object[] {oldPassword, password});
                            userEntry.CommitChanges();

                            return new BoolResult(true);
                        }

                        Logger.ErrorFormat(SecurityServiceResources.CoudntFindUserInActiveDirectory, _currentUserDto.Login);
                    }
                }
            }
            catch (Exception ex)
            {
                var directoryException = ex.InnerException as DirectoryServicesCOMException;

                if (directoryException != null)
                {
                    errorMessage += $" ({directoryException.Message})";
                }
                else
                {
                    Logger.WarnFormat(ex, SecurityServiceResources.CouldntChangePasswordForUserMessage, _currentUserDto.Login);
                }
            }

            return new BoolResult(false, new[]
            {
                new AlertDto
                {
                    Message = errorMessage,
                    Type = AlertType.Error
                }
            });
        }
        
        public BoolResult SetPassword(string password)
        {
            throw new NotImplementedException();
        }

        public UserValidationResult ValidatePassword(string password)
        {
            if (!_currentUserDto.IsActive)
            {
                Logger.WarnFormat($"User '{_currentUserDto.Login}' is not active.");

                return GetInvalidUserResult(SecurityServiceResources.UserIsInactiveMessage);
            }

            var masterPassword = _settingsProvider.MiscellaneousSettings?.MasterPassword;
            bool isAuthenticated;

#if !PROD
            if (!string.IsNullOrEmpty(masterPassword) && password == masterPassword)
            {
                _logger.Warn($"Master password used for user {_currentUserDto.Login}");
                isAuthenticated = true;
            }
            else
#endif
            {
                isAuthenticated = LdapAuthenticate(_currentUserDto.Login, password);
            }

            return !isAuthenticated
                ? GetInvalidUserResult(SecurityServiceResources.LoginOrPasswordInvalidMessage)
                : new UserValidationResult
                {
                    UserId = _currentUserDto.Id,
                    Login = _currentUserDto.Login,
                    Roles = _roles,
                    IsUserAuthenticated = true
                };
        }

        private bool LdapAuthenticate(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password)
                || string.IsNullOrWhiteSpace(LdapDomain) || string.IsNullOrWhiteSpace(LdapServerName))
            {
                return false;
            }

            return IsUserInLdap(login, password);
        }

        private bool IsUserInLdap(string login, string password)
        {
            try
            {
                var ldapDirectoryIdentifier = new LdapDirectoryIdentifier(LdapServerName, true, false);

                using (var ldapConnection = new LdapConnection(ldapDirectoryIdentifier))
                {
                    var networkCredential = new NetworkCredential(login, password, LdapDomain);
                    ldapConnection.Credential = networkCredential;
                    ldapConnection.AuthType = AuthType.Negotiate;
                    ldapConnection.Bind(networkCredential);
                    // user has authenticated at this point, as the credentials were used to login to the dc.

                    return true;
                }
            }
            catch (LdapException exception)
            {
                var message = $"LDAP authentication failed for user: {login}";
                Logger.Warn(message, exception);

                return false;
            }
        }
    }
}