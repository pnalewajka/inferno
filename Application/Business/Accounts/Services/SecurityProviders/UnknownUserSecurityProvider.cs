﻿using System;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Accounts.Services.SecurityProviders
{
    internal class UnknownUserSecurityProvider : BaseSecurityProvider, ISecurityProvider
    {
        private readonly string _login;

        public UnknownUserSecurityProvider(ILogger logger, string login) 
            : base(logger)
        {
            _login = login;
        }

        public bool CanChangePassword()
        {
            return false;
        }

        public BoolResult ChangePassword(string oldPassword, string password)
        {
            throw new InvalidOperationException();
        }

        public BoolResult SetPassword(string password)
        {
            throw new InvalidOperationException();
        }

        public UserValidationResult ValidatePassword(string password)
        {
            Logger.WarnFormat("User '{0}' does not exist or has not provided correct credentials.", _login);

            return GetInvalidUserResult(SecurityServiceResources.LoginOrPasswordInvalidMessage);
        }
    }
}