﻿using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.ActiveDirectory;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class DirectoryService : IDirectoryService
    {
        private readonly ISettingsProvider _settingsProvider;

        public DirectoryService(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public string GetUserEmail(string userName)
        {
            using (var context = new CoreDirectoryContext(_settingsProvider))
            {
                var user = context.Users.SingleOrDefault(u => u.UserName == userName);

                return user == null ? null : user.Email;
            }
        }
    }
}