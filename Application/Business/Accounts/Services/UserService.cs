using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Providers;
using Smt.Atomic.Data.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Castle.Core;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;

namespace Smt.Atomic.Business.Accounts.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class UserService : IUserService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;
        private readonly IClassMapping<User, UserDto> _userToUserDtoClassMapping;
        private readonly IClassMapping<UserDto, User> _userDtoToUserClassMapping;
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;
        private readonly ISecurityService _securityService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ILogger _logger;

        public UserService(
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService,
            IClassMapping<User, UserDto> userToUserDtoClassMapping,
            IClassMapping<UserDto, User> userDtoToUserClassMapping,
            IPasswordGenerator passwordGenerator,
            ISystemParameterService systemParameterService,
            ITimeService timeService,
            ISecurityService securityService,
            IMessageTemplateService messageTemplateService,
            IUserPermissionService userPermissionService,
            IReliableEmailService reliableEmailService,
            ILogger logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _userToUserDtoClassMapping = userToUserDtoClassMapping;
            _userDtoToUserClassMapping = userDtoToUserClassMapping;
            _passwordGenerator = passwordGenerator;
            _systemParameterService = systemParameterService;
            _timeService = timeService;
            _securityService = securityService;
            _messageTemplateService = messageTemplateService;
            _userPermissionService = userPermissionService;
            _reliableEmailService = reliableEmailService;
            _logger = logger;
        }

        public UserDto GetUserByLogin(string login)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork.Repositories.Users
                    .Single(u => u.Login == login);

                var otherDto = _userToUserDtoClassMapping.CreateFromSource(entity);

                return otherDto;
            }
        }

        public UserDto GetUserOrDefaultByLogin(string login)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork.Repositories.Users
                    .SingleOrDefault(u => u.Login == login);
                

                return entity == null ? null : _userToUserDtoClassMapping.CreateFromSource(entity);
            }
        }

        public UserDto GetUserOrDefaultByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork.Repositories.Users
                    .SingleOrDefault(u => u.Email == email);

                if (entity == null)
                {
                    return null;
                }

                var otherDto = _userToUserDtoClassMapping.CreateFromSource(entity);

                return otherDto;
            }
        }

        public UserDto GetUserOrDefaultByEmail(string email, AuthorizationProviderType? authorizationProviderType)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userCredentials =
                    unitOfWork.Repositories.Users.SelectMany(u => u.Credentials);

                if (authorizationProviderType.HasValue)
                {
                    userCredentials = userCredentials.Where(c => c.ProviderType == authorizationProviderType.Value);
                }

                var userCredential = userCredentials.FirstOrDefault(c => c.Email.ToLower() == email.ToLower());

                if (userCredential == null)
                {
                    return null;
                }

                var userDto = _userToUserDtoClassMapping.CreateFromSource(userCredential.User);

                return userDto;
            }
        }

        public UserDto FindUserByProviderDataOrDefault(string email, string externalId,
            AuthorizationProviderType? authorizationProviderType)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var credentialsQueryable =
                    unitOfWork.Repositories.Users.SelectMany(u => u.Credentials);

                if (!string.IsNullOrEmpty(email))
                {
                    credentialsQueryable = credentialsQueryable.Where(c => c.Email.ToLower() == email.ToLower());
                }

                if (!string.IsNullOrEmpty(externalId))
                {
                    credentialsQueryable = credentialsQueryable.Where(c => c.ExternalId.ToLower() == externalId.ToLower());
                }

                if (authorizationProviderType.HasValue)
                {
                    credentialsQueryable = credentialsQueryable.Where(c => c.ProviderType == authorizationProviderType.Value);
                }

                var userCredential = credentialsQueryable.FirstOrDefault();

                if (userCredential == null)
                {
                    return null;
                }

                var userDto = _userToUserDtoClassMapping.CreateFromSource(userCredential.User);

                return userDto;
            }
        }

        public IEnumerable<SecurityRoleType> GetUserRolesByLogin(string login)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.SingleOrDefault(u => u.Login.ToLower() == login.ToLower());

                return user?.GetAllRoleTypes().ToArray();
            }
        }

        [Cached(CacheArea = CacheAreas.FeaturePermissions, ExpirationInSeconds = 10)]
        public IEnumerable<SecurityRoleType> GetUserRolesById(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);

                return user?.GetAllRoleTypes().ToArray();
            }
        }

        public long CreateUser(UserDto userDto)
        {
            var userEntity = _userDtoToUserClassMapping.CreateFromSource(userDto);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                unitOfWork.Repositories.Users.Add(userEntity);
                unitOfWork.Commit();
                return userEntity.Id;
            }
        }

        public bool DoesUserCredentialExist(string email, AuthorizationProviderType authorizationProviderType)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Credentials.Any(
                    c => c.ProviderType == authorizationProviderType && c.Email.ToLower() == email.ToLower() && !c.IsExpired);
            }
        }

        public void CreateUserCredential(CredentialDto credentialDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var credentialsExists =
                    unitOfWork.Repositories.Credentials.Any(c => c.ProviderType == credentialDto.ProviderType &&
                                                                 ((c.Email != null &&
                                                                   c.Email.ToLower() == credentialDto.Email.ToLower())
                                                                  ||
                                                                  (c.ExternalId != null &&
                                                                   c.ExternalId.ToLower() ==
                                                                   credentialDto.ExternalId.ToLower())) && !c.IsExpired);

                if (credentialsExists)
                {
                    return;
                }

                var user = unitOfWork.Repositories.Users.Single(u => u.Id == credentialDto.UserId);

                var credential = new Credential
                {
                    ProviderType = credentialDto.ProviderType,
                    Email = credentialDto.Email,
                    User = user,
                    ExternalId = credentialDto.ExternalId,
                    CreatedOn = _timeService.GetCurrentTime()
                };

                unitOfWork.Repositories.Credentials.Add(credential);

                unitOfWork.Commit();
            }
        }

        public async Task<CreateUserResult> CreateUserAsync(UserDto userDto, SecurityRoleType[] securityRoles, UserAuthorizationValidationResult userAuthorizationValidationResult)
        {
            var result = new CreateUserResult();
            EmailMessageDto emailDto;
            string fullName;

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var userEntity = _userDtoToUserClassMapping.CreateFromSource(userDto);
                UserDto newUserDto;

                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    unitOfWork.Repositories.Users.Add(userEntity);

                    await unitOfWork.CommitAsync();

                    newUserDto = _userToUserDtoClassMapping.CreateFromSource(userEntity);
                }

                var token = _passwordGenerator.GenerateLoginToken();

                var expiresInHours = await _systemParameterService.GetParameterAsync<int>(ParameterKeys.AccountsLoginTokenExpirationInHours);
                var expiresOn = _timeService.GetCurrentTime().NextFullHour().AddHours(expiresInHours);

                _securityService.IssueLoginToken(userEntity.Id, token, expiresOn);
                _userPermissionService.GrantSecurityRolesToUser(userEntity.Id, securityRoles.Select(r => (long)r));

                fullName = newUserDto.GetFullName();
                emailDto = await PrepareEmail(newUserDto, token, expiresOn);

                if (userAuthorizationValidationResult != null)
                {
                    var credentialDto = new CredentialDto
                    {
                        Email = userAuthorizationValidationResult.Email,
                        ProviderType = userAuthorizationValidationResult.ProviderType,
                        UserId = userEntity.Id,
                        ExternalId = userAuthorizationValidationResult.ExternalId
                    };

                    CreateUserCredential(credentialDto);
                }

                result.IsSuccessful = true;
                result.UserDto = newUserDto;

                transactionScope.Complete();
            }

            if (emailDto != null)
            {
                _reliableEmailService.EnqueueMessage(emailDto);

                var resultMessage = string.Format(UserServiceResources.NewUserLoginTokenEmailSentMessageFormat, userDto.Login, fullName);

                result.AddAlert(AlertType.Success, resultMessage);
            }

            return result;
        }

        public async Task<bool> CanActivationEmailBeResendAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            email = email.ToLowerInvariant();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return await unitOfWork.Repositories.Users.AnyAsync(u => u.Email == email && u.LoginToken != null);
            }
        }

        public async Task<BoolResult> ResendActivationEmailAsync([NotNull] string email)
        {
            var result = new BoolResult();

            if (string.IsNullOrWhiteSpace(email))
            {
                result.IsSuccessful = false;
                result.AddAlert(AlertType.Error, UserServiceResources.UserEmailEmptyMessage);
                return result;
            }

            email = email.ToLowerInvariant();
            User userEntity;
            UserDto userDto;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userEntityList = await unitOfWork.Repositories.Users.Where(u => u.Email == email).ToListAsync();
                userEntity = userEntityList.Single();
                userDto = _userToUserDtoClassMapping.CreateFromSource(userEntity);
            }

            var token = _passwordGenerator.GenerateLoginToken();
            var expiresInHours = await _systemParameterService.GetParameterAsync<int>(ParameterKeys.AccountsLoginTokenExpirationInHours);
            var expiresOn = _timeService.GetCurrentTime().NextFullHour().AddHours(expiresInHours);

            _securityService.IssueLoginToken(userEntity.Id, token, expiresOn);

            var fullName = userDto.GetFullName();
            var emailDto = await PrepareEmail(userDto, token, expiresOn, true);

            result.IsSuccessful = true;

            if (emailDto != null)
            {
                _reliableEmailService.EnqueueMessage(emailDto);

                var resultMessage = string.Format(UserServiceResources.UserActivationEmailSentMessageFormat, fullName);
                result.AddAlert(AlertType.Success, resultMessage);
            }

            return result;
        }

        private async Task<EmailMessageDto> PrepareEmail(UserDto record, string token, DateTime expiresOn, bool isFromResendActivationEmail = false)
        {
            var emailAddress = record.Email;
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return null;
            }

            var serverAddressKey = ParameterKeys.ServerAddress;
            var tokenLoginUrlFormatKey = ParameterKeys.TokenLoginUrlFormat;
            var templateSubjectCode = TemplateCodes.AccountsNewUserLoginTokenEmailSubject;
            var templateBodyCode = TemplateCodes.AccountsNewUserLoginTokenEmailBody;
            var footerHtml = string.Empty;
            string senderName = null;
            
            var serverAddress = _systemParameterService.GetParameter<string>(serverAddressKey);
            var loginUrlFormat = _systemParameterService.GetParameter<string>(tokenLoginUrlFormatKey);
            var loginUrl = string.Format(loginUrlFormat, serverAddress, token);

            var messageModel = new UserLoginTokenEmailDto
            {
                FirstName = record.FirstName,
                LastName = record.LastName,
                ExpiresOn = expiresOn,
                LoginUrl = loginUrl,
                FooterHtml = footerHtml
            };

            var subjectTask = _messageTemplateService.ResolveTemplateAsync(templateSubjectCode, messageModel);
            var bodyTask = _messageTemplateService.ResolveTemplateAsync(templateBodyCode, messageModel);

            await Task.WhenAll(subjectTask, bodyTask);

            var message = new EmailMessageDto
            {
                Body = bodyTask.Result.Content,
                IsHtml = bodyTask.Result.IsHtml,
                Subject = subjectTask.Result.Content,
            };

            if (senderName != null)
            {
                message.From = new EmailRecipientDto
                {
                    EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                    FullName = senderName
                };
            }

            message.Recipients.Add(new EmailRecipientDto(emailAddress, record.GetFullName()));

            return message;
        }

        public bool TryCreateUserFromAuth(UserDto userDto, AuthorizationProviderType authorizationProviderType, SecurityRoleType[] securityRoles)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var userId = CreateUser(userDto);

                if (userId <= 0)
                {
                    return false;
                }

                _userPermissionService.GrantSecurityRolesToUser(userId, securityRoles.Select(r => (long)r));

                var credentialDto = new CredentialDto
                {
                    Email = userDto.Email,
                    ProviderType = authorizationProviderType,
                    UserId = userId
                };

                CreateUserCredential(credentialDto);
                transactionScope.Complete();

                return true;
            }
        }

        public void GeneratePasswordAndSendEmail(UserEmailDataDto data)
        {
            var password = _passwordGenerator.GeneratePassword();

            _securityService.CreateUserPasswordCredentials(data.UserId, password);

            if (_systemParameterService.GetParameter<bool>(ParameterKeys.AccountsNewUserStorePasswordInLog))
            {
                _logger.DebugFormat("Created password '{0}' for user '{1}' (id: {2})", password, data.Login, data.UserId);
            }

            SendEmailAboutPasswordIfPossible(data, password);
        }

        public void GenerateLoginTokenAndSendEmail(UserEmailDataDto data)
        {
            var token = _passwordGenerator.GenerateLoginToken();

            var expiresInHours =
                _systemParameterService.GetParameter<int>(ParameterKeys.AccountsLoginTokenExpirationInHours);
            var expiresOn = _timeService.GetCurrentTime().NextFullHour().AddHours(expiresInHours);

            _securityService.IssueLoginToken(data.UserId, token, expiresOn);

            SendEmailAboutTokenIfPossible(data, token, expiresOn);
        }

        public UserDto GetUserById(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork.Repositories.Users.GetById(userId);
                return _userToUserDtoClassMapping.CreateFromSource(entity);
            }
        }

        public UserDto[] GetUsersByIds(long[] userIds)
        {
            UserDto[] result;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entities = unitOfWork.Repositories.Users.GetByIds(userIds).ToArray();
                result = entities.Select(e => _userToUserDtoClassMapping.CreateFromSource(e)).ToArray();
            }

            return result;
        }

        public UserDto GetUserOrDefaultByActiveDirectoryId(Guid activeDirectoryId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork.Repositories.Users.SingleOrDefault(u => u.ActiveDirectoryId == activeDirectoryId);

                return entity != null
                    ? _userToUserDtoClassMapping.CreateFromSource(entity)
                    : null;
            }
        }

        public UserDto GetUserOrDefaultByCrmId(string crmId)
        {
            if (string.IsNullOrEmpty(crmId))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork.Repositories.Users.SingleOrDefault(u => u.CrmId == crmId);

                return entity != null
                    ? _userToUserDtoClassMapping.CreateFromSource(entity)
                    : null;
            }
        }

        public void SynchronizeWithActiveDirectory(ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.Single(u => u.ActiveDirectoryId == activeDirectoryUserDto.ActiveDirectoryId);

                user.IsActive = activeDirectoryUserDto.IsActive;
                user.Email = activeDirectoryUserDto.Email;
                user.FirstName = activeDirectoryUserDto.FirstName;
                user.LastName = activeDirectoryUserDto.LastName;
                user.Login = activeDirectoryUserDto.Login;
                user.ActiveDirectoryId = activeDirectoryUserDto.ActiveDirectoryId;
                user.CrmId = activeDirectoryUserDto.CrmId;

                unitOfWork.Commit();
            }
        }

        public void DeactivateUserMissingInActiveDirectory(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);

                // Deactivation of users from Active Directory not available in the system,
                // change Login to avoid problems with unique constrainsts

                user.IsActive = false;
                user.Login = $"AD_{user.ActiveDirectoryId}";
                user.Email = null;

                unitOfWork.Commit();
            }
        }

        private void SendEmailAboutPasswordIfPossible(UserEmailDataDto data, string password)
        {
            var emailAddress = data.Email;

            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return;
            }

            var messageModel = new UserPasswordEmailDto
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                Login = data.Login,
                Password = password
            };

            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.AccountsNewUserPasswordEmailSubject, messageModel);
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.AccountsNewUserPasswordEmailBody, messageModel);

            var message = new EmailMessageDto
            {
                Body = body.Content,
                IsHtml = body.IsHtml,
                Subject = subject.Content,
            };

            message.Recipients.Add(new EmailRecipientDto(emailAddress, data.GetFullName()));

            _reliableEmailService.EnqueueMessage(message);
        }

        private void SendEmailAboutTokenIfPossible(UserEmailDataDto data, string token, DateTime expiresOn)
        {
            var emailAddress = data.Email;

            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return;
            }

            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var loginUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.TokenLoginUrlFormat);
            var loginUrl = string.Format(loginUrlFormat, serverAddress, token);

            var messageModel = new UserLoginTokenEmailDto
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                ExpiresOn = expiresOn,
                LoginUrl = loginUrl
            };

            var batch = new EmailBatchDto
            {
                BodyTemplateCode = TemplateCodes.AccountsNewUserLoginTokenEmailBody,
                SubjectTemplateCode = TemplateCodes.AccountsNewUserLoginTokenEmailSubject,
                Model = messageModel,
            };

            batch.Recipients.Add(new EmailRecipientDto(emailAddress, data.GetFullName()));

            _reliableEmailService.EnqueueBatch(batch);
        }
    }
}