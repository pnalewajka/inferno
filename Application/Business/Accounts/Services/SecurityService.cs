﻿using System;
using System.Linq;
using System.Security.Cryptography;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class SecurityService : ISecurityService
    {
        private const int PasswordHashLength = 32;
        private const int SaltLength = 16;
        private readonly ILogger _logger;
        private readonly ITimeService _timeService;
        private readonly ISecurityProviderFactory _securityProviderFactory;
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IClassMapping<User, UserDto> _userClassMapping;
        private readonly IClassMapping<Credential, CredentialDto> _credentialClassMapping;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public SecurityService(
            ILogger logger,
            ITimeService timeService,
            ISecurityProviderFactory securityProviderFactory,
            ICacheInvalidationService cacheInvalidationService,
            IClassMapping<User, UserDto> userClassMapping,
            IClassMapping<Credential, CredentialDto> credentialClassMapping,
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService
            )
        {
            _logger = logger;
            _timeService = timeService;
            _securityProviderFactory = securityProviderFactory;
            _cacheInvalidationService = cacheInvalidationService;
            _userClassMapping = userClassMapping;
            _credentialClassMapping = credentialClassMapping;
            _unitOfWorkService = unitOfWorkService;
        }

        public UserValidationResult ValidateUser(string login, string password)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentUser = GetUserDtoByLogin(unitOfWork, login);
                var securityProvider = ResolveSecurityProfile(login);

                var validationResult = securityProvider.ValidatePassword(password);

                if (validationResult.IsUserAuthenticated)
                {
                    var user = unitOfWork.Repositories.Users.GetById(currentUser.Id);
                    user.LastLoggedOn = _timeService.GetCurrentTime();
                    unitOfWork.Commit();
                }

                return validationResult;
            }
        }

        public UserValidationResult ValidateUserAgainstDirectory(string email)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.SingleOrDefault(x => x.Email == email && x.IsActive);

                if (user == null)
                {
                    _logger.WarnFormat("User '{0}' does not exist or is not active.", email);

                    return GetInvalidUserResult(SecurityServiceResources.LoginOrPasswordInvalidMessage);
                }

                return new UserValidationResult
                {
                    UserId = user.Id,
                    Login = user.Login,
                    Roles = user.GetAllRolesNames().ToArray(),
                    IsUserAuthenticated = true
                };
            }
        }

        private User GetUserByLogin(IUnitOfWork<IAccountsDbScope> unitOfWork, string login)
        {
            return unitOfWork.Repositories.Users.SingleOrDefault(x => x.Login == login);
        }

        private UserDto GetUserDtoByLogin(IUnitOfWork<IAccountsDbScope> unitOfWork, string login)
        {
            var user = GetUserByLogin(unitOfWork, login);

            return user == null ? null : _userClassMapping.CreateFromSource(user);
        }

        private static UserValidationResult GetInvalidUserResult(string message)
        {
            var validationResult = new UserValidationResult
            {
                IsUserAuthenticated = false
            };

            validationResult.AddAlert(AlertType.Error, message);

            return validationResult;
        }

        public UserValidationResult ValidateUser(string token)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var now = _timeService.GetCurrentTime();

                var user = unitOfWork.Repositories.Users
                                          .SingleOrDefault(u => u.LoginToken != null
                                                                && u.LoginToken.Content == token
                                                                && u.LoginToken.ExpiresOn > now);

                if (user == null)
                {
                    _logger.WarnFormat("User with token '{0}' does not exist.", token);

                    return GetInvalidUserResult(SecurityServiceResources.LoginTokenInvalidMessage);
                }

                if (!user.IsActive)
                {
                    _logger.WarnFormat("User with token '{0}' is not active.", token);

                    return GetInvalidUserResult(SecurityServiceResources.UserIsInactiveMessage);
                }

                return new UserValidationResult
                {
                    UserId = user.Id,
                    Login = user.Login,
                    Roles = user.GetAllRolesNames().ToArray(),
                    IsUserAuthenticated = true
                };
            }
        }

        public BoolResult SetPassword(string login, string newPassword)
        {
            var result = ResolveSecurityProfile(login).SetPassword(newPassword);

            InvalidatePermissionCache();

            result.IsSuccessful = true;
            result.AddAlert(AlertType.Success, SecurityServiceResources.PasswordHasBeenSetMessage);

            return result;
        }

        public BoolResult ChangePassword(string login, string oldPassword, string newPassword)
        {
            var securityProvider = ResolveSecurityProfile(login);

            var userValidation = securityProvider.ValidatePassword(oldPassword);
            if (!userValidation.IsUserAuthenticated)
            {
                var failResult = new BoolResult();
                failResult.AddAlert(AlertType.Error, SecurityServiceResources.InvalidOldPasswordMessage);
                return failResult;
            }

            var result = securityProvider.ChangePassword(oldPassword, newPassword);

            if (result.IsSuccessful)
            {
                result.AddAlert(AlertType.Success, SecurityServiceResources.PasswordHasBeenChangedMessage);
            }

            InvalidatePermissionCache();

            return result;
        }

        /// <summary>
        /// Creates new Credential for user.
        /// Old Credentials are expired.
        /// </summary>              
        /// <param name="userId">user id to create credentials for</param>
        /// <param name="password">user password</param>
        public void CreateUserPasswordCredentials(long userId, string password)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);

                CreateUserPasswordCredentials(unitOfWork.Repositories.Credentials, user, password);

                unitOfWork.Commit();
            }

            InvalidatePermissionCache();
        }

        public void RevokeUserPasswordCredentials(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var credentials = unitOfWork.Repositories.Credentials.Where(u => u.UserId == userId).ToList();

                foreach (var entry in credentials)
                {
                    unitOfWork.Repositories.Credentials.Delete(entry);
                }

                unitOfWork.Commit();
            }

            InvalidatePermissionCache();
        }

        public void IssueLoginToken(long userId, string loginToken, DateTime expiresOn)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetByIdOrDefault(userId);

                if (user == null)
                {
                    throw new ArgumentOutOfRangeException(nameof(userId));
                }

                var credentials = user.Credentials.ToList();

                foreach (var credential in credentials)
                {
                    unitOfWork.Repositories.Credentials.Delete(credential);
                }

                if (user.LoginToken == null)
                {
                    user.LoginToken = new UserLoginToken();
                }

                user.LoginToken.Content = loginToken;
                user.LoginToken.ExpiresOn = expiresOn;

                unitOfWork.Commit();
            }

            InvalidatePermissionCache();
        }

        public BoolResult IsTokenValid(string token)
        {
            var result = new BoolResult();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var now = _timeService.GetCurrentTime();

                var user = unitOfWork.Repositories.Users
                                          .SingleOrDefault(u => u.LoginToken != null
                                                                && u.LoginToken.Content == token
                                                                && u.LoginToken.ExpiresOn > now);

                if (user == null)
                {
                    _logger.WarnFormat("User with token '{0}' does not exist.", token);

                    return result;
                }

                result.IsSuccessful = true;
                return result;
            }
        }
        
        private ISecurityProvider ResolveSecurityProfile(string userLogin)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.SingleOrDefault(u => u.Login == userLogin && u.IsActive && !u.IsDeleted);

                var userCredential = unitOfWork.Repositories.Credentials
                                                       .Where(x => x.User.Login == userLogin)
                                                       .OrderByDescending(x => x.CreatedOn)
                                                       .FirstOrDefault();

                if (user == null)
                {
                    return _securityProviderFactory.GetUnknownUserSecurityProvider(userLogin);
                }

                var userDto = _userClassMapping.CreateFromSource(user);
                var roles = user.GetAllRolesNames().ToArray();

                if (userCredential == null)
                {
                    return _securityProviderFactory.GetAtomicSecurityProvider(userDto, roles);
                }

                var credentialDto = _credentialClassMapping.CreateFromSource(userCredential);

                switch (userCredential.ProviderType)
                {
                    case AuthorizationProviderType.ActiveDirectory:

                        return _securityProviderFactory.GetActiveDirectorySecurityProvider(userDto, roles);

                    case AuthorizationProviderType.Atomic:

                        return _securityProviderFactory.GetAtomicSecurityProvider(userDto, credentialDto, roles);

                    default:
                        // TODO: unknown provider
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void InvalidatePermissionCache()
        {
            _cacheInvalidationService.InvalidateArea(CacheAreas.FeaturePermissions);
        }

        private void CreateUserPasswordCredentials(IRepository<Credential> credentialRepository, User user, string password)
        {
            string base64PasswordHash;
            string base64Salt;

            using (var pbkdf2 = new Rfc2898DeriveBytes(password, SaltLength))
            {
                byte[] generatedPasswordHash = pbkdf2.GetBytes(PasswordHashLength);
                byte[] generatedSalt = pbkdf2.Salt;

                base64PasswordHash = Convert.ToBase64String(generatedPasswordHash);
                base64Salt = Convert.ToBase64String(generatedSalt);
            }

            UpdateUserCredentials(credentialRepository, user, base64PasswordHash, base64Salt);
        }

        private void UpdateUserCredentials(IRepository<Credential> credentialRepository, User user, string base64PasswordHash, string base64Salt)
        {
            foreach (var credential in user.Credentials.Where(c => c.ProviderType == AuthorizationProviderType.Atomic))
            {
                credential.IsExpired = true;
            }

            var newCredentials = new Credential
            {
                Id = -1,
                PasswordHash = base64PasswordHash,
                Salt = base64Salt,
                CreatedOn = _timeService.GetCurrentTime(),
                UserId = user.Id,
                ProviderType = AuthorizationProviderType.Atomic,
                Email = user.Email
            };

            user.Credentials.Add(newCredentials);
            credentialRepository.Add(newCredentials);
        }
    }
}