using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.ActiveDirectory.BusinessEvents;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class UserPermissionService : IUserPermissionService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ILogger _logger;

        public UserPermissionService(
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService,
            ICacheInvalidationService cacheInvalidationService,
            IBusinessEventPublisher businessEventPublisher,
            IPrincipalProvider principalProvider,
            ILogger logger)
        {
            _logger = logger;
            _principalProvider = principalProvider;
            _unitOfWorkService = unitOfWorkService;
            _cacheInvalidationService = cacheInvalidationService;
            _businessEventPublisher = businessEventPublisher;
        }

        public void GrantSecurityRolesToUser(long userId, [NotNull] IEnumerable<long> roleIds)
        {
            if (roleIds == null)
            {
                throw new ArgumentNullException(nameof(roleIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);
                var roles = unitOfWork.Repositories.SecurityRoles.GetByIds(roleIds).ToList();

                foreach (var role in roles)
                {
                    if (!user.Roles.Contains(role))
                    {
                        user.Roles.Add(role);

                        LogPermissionsChange("User {0} (ID={1}) has granted to user {2} (ID={3}) security role {4}.", user, role.Name);
                    }
                }

                unitOfWork.Repositories.Users.Edit(user);
                unitOfWork.Commit();
                InvalidatePermissionCache();
            }
        }

        public void AssignSecurityProfilesToUser(long userId, [NotNull] IEnumerable<long> profileIds, bool removeOtherProfiles = false)
        {
            if (profileIds == null)
            {
                throw new ArgumentNullException(nameof(profileIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.Include(u => u.Profiles).GetById(userId);
                var profiles = unitOfWork.Repositories.SecurityProfiles.GetByIds(profileIds).ToList();

                if (removeOtherProfiles)
                {
                    var profilesToBeRemoved = user.Profiles.Except(profiles).Where(p => !string.IsNullOrEmpty(p.ActiveDirectoryGroupName)).ToList();

                    if (profilesToBeRemoved.Any())
                    {
                        foreach (var securityProfile in profilesToBeRemoved)
                        {
                            user.Profiles.Remove(securityProfile);

                            LogPermissionsChange("User {0} (ID={1}) has revoked from user {2} (ID={3}) security profile {4}.", user, securityProfile.Name);
                        }

                        PublishRemoveUserFromActiveDirectoryGroupsEvent(user, profilesToBeRemoved);
                    }
                }

                var profilesToBeAdded = profiles.Except(user.Profiles).ToList();

                AssignSecurityProfilesToUser(unitOfWork, user, profilesToBeAdded);
                PublishAddUserToActiveDirectoryGroupsEvent(user, profilesToBeAdded);
            }
        }

        public void AssignSecurityProfilesToUserBasedOnActiveDirectory(long userId, [NotNull] string activeDirectoriesNames)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var securityProfile = unitOfWork
                    .Repositories
                    .SecurityProfiles
                    .SingleOrDefault(sp => activeDirectoriesNames.Equals(sp.ActiveDirectoryGroupName));

                if (securityProfile == null)
                {
                    return;
                }

                var user = unitOfWork.Repositories.Users.GetById(userId);

                AssignSecurityProfilesToUser(unitOfWork, user, new[] { securityProfile });
            }
        }

        public void RevokeSecurityRolesFromUser(long userId, [NotNull] IEnumerable<long> roleIds)
        {
            if (roleIds == null)
            {
                throw new ArgumentNullException(nameof(roleIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);
                var roles = unitOfWork.Repositories.SecurityRoles.GetByIds(roleIds).ToList();

                foreach (var role in roles)
                {
                    user.Roles.Remove(role);

                    LogPermissionsChange("User {0} (ID={1}) has revoked from user {2} (ID={3}) security role {4}.", user, role.Name);
                }

                unitOfWork.Repositories.Users.Edit(user);
                unitOfWork.Commit();
                InvalidatePermissionCache();
            }
        }


        public void RemoveSecurityProfilesFromUser(long userId, [NotNull] IEnumerable<long> profileIds)
        {
            if (profileIds == null)
            {
                throw new ArgumentNullException(nameof(profileIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);
                var profiles = unitOfWork.Repositories.SecurityProfiles.GetByIds(profileIds).ToList();

                RemoveSecurityProfilesFromUser(unitOfWork, user, profiles);

                PublishRemoveUserFromActiveDirectoryGroupsEvent(user, profiles);
            }
        }

        public void RemoveSecurityProfilesFromUserBasedOnActiveDirectory(long userId, [NotNull] string activeDirectoryNames)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var securityProfile = unitOfWork
                    .Repositories
                    .SecurityProfiles
                    .SingleOrDefault(sp => activeDirectoryNames.Equals(sp.ActiveDirectoryGroupName));

                if (securityProfile == null)
                {
                    return;
                }

                var user = unitOfWork.Repositories.Users.GetById(userId);

                RemoveSecurityProfilesFromUser(unitOfWork, user, new[] { securityProfile });
            }
        }

        public void RemoveAllSecurityProfilesFromUser(long userId)
        {
            AssignSecurityProfilesToUser(userId, Enumerable.Empty<long>(), true);
        }

        public IEnumerable<long> GetSecurityProfileIdsFromActiveDirectoryGroups(IEnumerable<string> activeDirectoryGroupNames)
        {
            var groups = new HashSet<string>(activeDirectoryGroupNames);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var ids = unitOfWork.Repositories.SecurityProfiles
                    .Where(p => groups.Contains(p.ActiveDirectoryGroupName))
                    .Select(p => p.Id);

                foreach (var id in ids)
                {
                    yield return id;
                }
            }
        }

        private void InvalidatePermissionCache()
        {
            _cacheInvalidationService.InvalidateArea(CacheAreas.FeaturePermissions);
            _cacheInvalidationService.InvalidateArea(CacheAreas.EmployeeDeductibleCost);
        }


        private void AssignSecurityProfilesToUser(IUnitOfWork<IAccountsDbScope> unitOfWork, User user,
            [NotNull] IEnumerable<SecurityProfile> profiles)
        {
            foreach (var profile in profiles)
            {
                if (!user.Profiles.Contains(profile))
                {
                    user.Profiles.Add(profile);

                    LogPermissionsChange("User {0} (ID={1}) has granted to user {2} (ID={3}) security profile {4}.", user, profile.Name);
                }
            }

            unitOfWork.Repositories.Users.Edit(user);
            unitOfWork.Commit();
            InvalidatePermissionCache();
        }

        private void RemoveSecurityProfilesFromUser(IUnitOfWork<IAccountsDbScope> unitOfWork, User user,
            [NotNull] IEnumerable<SecurityProfile> profiles)
        {
            foreach (var profile in profiles)
            {
                if (!user.Profiles.Contains(profile))
                {
                    continue;
                }

                user.Profiles.Remove(profile);

                LogPermissionsChange("User {0} (ID={1}) has revoked from user {2} (ID={3}) security profile {4}.", user, profile.Name);
            }

            unitOfWork.Repositories.Users.Edit(user);
            unitOfWork.Commit();
            InvalidatePermissionCache();
        }

        private void PublishRemoveUserFromActiveDirectoryGroupsEvent(User user, IEnumerable<SecurityProfile> profiles)
        {
            var activeDirectoryGroupToRemove = GetActiveDirectoryNamesFromSecurityProfiles(profiles);

            if (user.ActiveDirectoryId.HasValue && activeDirectoryGroupToRemove.Any())
            {
                _businessEventPublisher.PublishBusinessEvent(
                    new RemoveUserFromActiveDirectoryGroupsEvent
                    {
                        UserId = user.ActiveDirectoryId.Value,
                        ActiveDirectoryGroupNames = activeDirectoryGroupToRemove
                    });
            }
        }

        private void PublishAddUserToActiveDirectoryGroupsEvent(User user, IEnumerable<SecurityProfile> profiles)
        {
            var activeDirectoryGroups = GetActiveDirectoryNamesFromSecurityProfiles(profiles);

            if (user.ActiveDirectoryId.HasValue && activeDirectoryGroups.Length != 0)
            {
                _businessEventPublisher.PublishBusinessEvent(
                    new AddUserToActiveDirectoryGroupsEvent
                    {
                        UserId = user.ActiveDirectoryId.Value,
                        ActiveDirectoryGroupNames = activeDirectoryGroups
                    });
            }
        }

        private string[] GetActiveDirectoryNamesFromSecurityProfiles(IEnumerable<SecurityProfile> profiles)
        {
            return profiles
                .Where(p => p.ActiveDirectoryGroupName != null)
                .Select(p => p.ActiveDirectoryGroupName)
                .ToArray();
        }

        private void LogPermissionsChange(string messageFormat, User user, string roleOrProfileName)
        {
            _logger.WarnFormat($"[Permissions] {messageFormat}",
                _principalProvider.Current?.Login, _principalProvider.Current?.Id, user.Login, user.Id, roleOrProfileName);
        }

        public void AssignUsersToProfile(long profileId, IEnumerable<long> userIds)
        {
            if (userIds == null)
            {
                throw new ArgumentNullException(nameof(userIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var profile = unitOfWork.Repositories.SecurityProfiles.GetById(profileId);
                var users = unitOfWork.Repositories.Users.GetByIds(userIds).ToList();

                foreach (var user in users)
                {
                    if (!profile.Users.Contains(user))
                    {
                        profile.Users.Add(user);

                        LogPermissionsChange("User {0} (ID={1}) has granted to profile {2} (ID={3}) security profile {4}.", user, profile.Name);
                    }
                }

                unitOfWork.Repositories.SecurityProfiles.Edit(profile);
                unitOfWork.Commit();
                InvalidatePermissionCache();
            }
        }

        public void RemoveUsersFromProfile(long profileId, IEnumerable<long> userIds)
        {
            if (userIds == null)
            {
                throw new ArgumentNullException(nameof(userIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var profile = unitOfWork.Repositories.SecurityProfiles.GetById(profileId);
                var users = unitOfWork.Repositories.Users.GetByIds(userIds).ToList();

                foreach (var user in users)
                {
                    profile.Users.Remove(user);

                    LogPermissionsChange("User {0} (ID={1}) has revoked from profile {2} (ID={3}) security profile {4}.", user, profile.Name);
                }

                unitOfWork.Repositories.SecurityProfiles.Edit(profile);
                unitOfWork.Commit();
                InvalidatePermissionCache();
            }
        }
    }
}