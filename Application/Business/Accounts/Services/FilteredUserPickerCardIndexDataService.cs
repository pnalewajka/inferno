﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Accounts.Consts;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Enums;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    public class FilteredUserPickerCardIndexDataService : CardIndexDataService<UserDto, User, IWorkflowsDbScope>, IFilteredUserPickerCardIndexDataService
    {
        private readonly IUserBusinessLogic _userBusinessLogic;

        protected override bool ShouldUseDownwardMappingForFieldResolution { get; }

        public FilteredUserPickerCardIndexDataService(
            IUserBusinessLogic userBusinessLogic,
            ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
            _userBusinessLogic = userBusinessLogic;
        }

        protected override IEnumerable<Expression<Func<User, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return u => u.FirstName;
                yield return u => u.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Login))
            {
                yield return u => u.Login;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Email))
            {
                yield return u => u.Email;
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<User, UserDto, IWorkflowsDbScope> eventArgs)
        {
            throw new NotSupportedException();
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<User, UserDto, IWorkflowsDbScope> eventArgs)
        {
            throw new NotSupportedException();
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<User, IWorkflowsDbScope> eventArgs)
        {
            throw new NotSupportedException();
        }

        protected override Type GetSystemFilterTypeParameter()
        {
            return typeof(User);
        }

        public override IQueryable<User> ApplyContextFiltering(IQueryable<User> records, object context)
        {
            var requestUserPickerContext = context as FilteredUserPickerContext;

            if (requestUserPickerContext?.FilterKey == null)
            {
                throw new ArgumentException("Missing arguments for context filtering");
            }

            var currentUserId = PrincipalProvider.Current.Id.Value;

            switch (requestUserPickerContext.FilterKey)
            {
                case nameof(FilteredUserPickerFilterKeys.UsersICanAdministrateAsHr):
                    var isMyHrEmployeeOrMyself = _userBusinessLogic.GetIsMyHrEmployeeOrMyselfBusinessLogic(currentUserId);

                    return records.Where(isMyHrEmployeeOrMyself.AsExpression());

                default:
                    throw new ArgumentException("FilteredUserPickerContext.FilterKey `{0}` is not implemented");
            }
        }
    }
}
