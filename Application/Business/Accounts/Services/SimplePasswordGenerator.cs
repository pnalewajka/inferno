﻿using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class SimplePasswordGenerator : IPasswordGenerator
    {
        private readonly ISystemParameterService _systemParameters;

        public SimplePasswordGenerator(ISystemParameterService systemParameters)
        {
            _systemParameters = systemParameters;
        }

        public string GeneratePassword()
        {
            var minLength = _systemParameters.GetParameter<int>(ParameterKeys.AccountsDefaultPasswordMinLength);
            var maxLength = _systemParameters.GetParameter<int>(ParameterKeys.AccountsDefaultPasswordMaxLength);
            var allowedCharacters = _systemParameters.GetParameter<string>(ParameterKeys.AccountsDefaultPasswordAllowedCharacters);

            var password = StringHelper.GetRandomString(minLength, maxLength, allowedCharacters);

            return password;
        }

        public string GenerateLoginToken()
        {
            var minLength = _systemParameters.GetParameter<int>(ParameterKeys.AccountsLoginTokenMinLength);
            var maxLength = _systemParameters.GetParameter<int>(ParameterKeys.AccountsLoginTokenMaxLength);
            var allowedCharacters = _systemParameters.GetParameter<string>(ParameterKeys.AccountsLoginTokenAllowedCharacters);

            var token = StringHelper.GetRandomString(minLength, maxLength, allowedCharacters);

            return token;
        }
    }
}