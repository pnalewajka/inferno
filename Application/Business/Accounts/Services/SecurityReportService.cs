﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    public class SecurityReportService : ISecurityReportService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public SecurityReportService(IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public IEnumerable<UserSecurityRolesDto> GetUserSecurityRoles(long[] ids)
        {
            return GetRolesPermissions(users => users.GetByIds(ids)
                       .Select(u => new UserSecurityRolesDto
                       {
                           UserFirstName = u.FirstName,
                           UserLastName = u.LastName,
                           UserEmail = u.Email,
                           RoleIds = u.Profiles.SelectMany(p => p.Roles).Union(u.Roles).Select(r => r.Id),
                       }).ToList());
        }

        public IEnumerable<UserSecurityProfilesDto> GetUserSecurityProfiles(long[] ids)
        {
            return GetProfilesPermissions(users => users.GetByIds(ids)
                       .Select(u => new UserSecurityProfilesDto
                       {
                           UserFirstName = u.FirstName,
                           UserLastName = u.LastName,
                           UserEmail = u.Email,
                           ProfileIds = u.Profiles.Select(p => p.Id)
                       }).ToList());
        }

        public IEnumerable<UserSecurityRolesDto> GetUsersSecurityRoles()
        {
            return GetRolesPermissions(users => users
                    .Select(u => new UserSecurityRolesDto
                    {
                        UserFirstName = u.FirstName,
                        UserLastName = u.LastName,
                        UserEmail = u.Email,
                        RoleIds = u.Profiles.SelectMany(p => p.Roles).Union(u.Roles).Select(r => r.Id),
                    }).ToList());
        }

        public IEnumerable<UserSecurityProfilesDto> GetUsersSecurityProfiles()
        {
            return GetProfilesPermissions(users => users
                   .Select(u => new UserSecurityProfilesDto
                   {
                       UserFirstName = u.FirstName,
                       UserLastName = u.LastName,
                       UserEmail = u.Email,
                       ProfileIds = u.Profiles.Select(p => p.Id)
                   }).ToList());
        }

        private IEnumerable<UserSecurityRolesDto> GetRolesPermissions(Func<IQueryable<User>, IEnumerable<UserSecurityRolesDto>> filter)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userQuery = unitOfWork.Repositories.Users
                       .Include(u => u.Roles)
                       .Include(u => u.Profiles);

                return filter(userQuery);
            }
        }

        private IEnumerable<UserSecurityProfilesDto> GetProfilesPermissions(Func<IQueryable<User>, IEnumerable<UserSecurityProfilesDto>> filter)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userQuery = unitOfWork.Repositories.Users
                       .Include(u => u.Roles)
                       .Include(u => u.Profiles);

                return filter(userQuery);
            }
        }
    }
}
