﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;

namespace Smt.Atomic.Business.Accounts.Services
{
    class UserChoresCardIndexDataService
        : CustomDataCardIndexDataService<ChoreDto>
        , IUserChoresCardIndexDataService
    {
        private readonly IUserChoreProvider _userChoresProvider;

        public UserChoresCardIndexDataService(
            IUserChoreProvider userChoresProvider)
        {
            _userChoresProvider = userChoresProvider;
        }

        public override ChoreDto GetRecordById(long id)
        {
            throw new NotSupportedException();
        }

        public override ChoreDto GetRecordByIdOrDefault(long id)
        {
            throw new NotSupportedException();
        }

        protected override IList<ChoreDto> GetAllRecords(object context = null)
        {
            return _userChoresProvider.GetActiveChores();
        }

        protected override IEnumerable<Expression<Func<ChoreDto, object>>> GetSearchColumns(
            SearchCriteria searchCriteria)
        {
            yield break;
        }
    }
}
