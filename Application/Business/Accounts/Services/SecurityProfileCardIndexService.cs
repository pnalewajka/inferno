using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Consts;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.Services
{
    internal class SecurityProfileCardIndexService : CardIndexDataService<SecurityProfileDto, SecurityProfile, IAccountsDbScope>, ISecurityProfileCardIndexService
    {
        private readonly ICacheInvalidationService _cacheInvalidationService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => true;

        public SecurityProfileCardIndexService(ICardIndexServiceDependencies<IAccountsDbScope> dependencies)
            : base(dependencies)
        {
            _cacheInvalidationService = dependencies.CacheInvalidationService;
        }

        protected override IEnumerable<Expression<Func<SecurityProfile, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return p => p.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Roles))
            {
                yield return p => p.Roles.Select(r => r.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodes.ActiveDirectoryGroup))
            {
                yield return p => p.ActiveDirectoryGroupName;
            }
        }

        public override IQueryable<SecurityProfile> ApplyContextFiltering(IQueryable<SecurityProfile> records, object context)
        {
            var userItemsContext = context as UserItemsContext;

            if (userItemsContext != null)
            {
                return records.Where(ExcludeUsers(userItemsContext));
            }

            return records;
        }

        public override EditRecordResult EditRecord(SecurityProfileDto record, object context = null)
        {
            var editRecordResult = base.EditRecord(record, context);

            if (editRecordResult.IsSuccessful)
            {
                InvalidatePermissionCache();
            }

            return editRecordResult;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<SecurityProfile, SecurityProfileDto, IAccountsDbScope> eventArgs)
        {
            eventArgs.InputEntity.Roles = EntityMergingService.CreateEntitiesFromIds<SecurityRole, IAccountsDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Roles.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<SecurityProfile, SecurityProfileDto, IAccountsDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Roles, eventArgs.InputEntity.Roles);
        }

        private void InvalidatePermissionCache()
        {
            _cacheInvalidationService.InvalidateArea(CacheAreas.FeaturePermissions);
        }

        private static Expression<Func<SecurityProfile,bool>> ExcludeUsers(UserItemsContext userItemsContext)
        {
            return p => userItemsContext.ShouldExclude ^ p.Users.Any(u => u.Id == userItemsContext.ParentId);
        }
    }
}