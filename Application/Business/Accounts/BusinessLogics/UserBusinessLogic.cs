﻿using System;
using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.BusinessLogics
{
    public class UserBusinessLogic : IUserBusinessLogic
    {
        public static readonly BusinessLogic<User, bool> IsActivelyWorking = new BusinessLogic
            <User, bool>(
            (user) => user.IsActive && user.Employees.Any(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated));

        private readonly IUserService _userService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;

        public UserBusinessLogic(
            IUserService userService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            ISystemParameterService systemParameterService)
        {
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
        }

        public BusinessLogic<User, bool> CanChangeUserDataOnBehalf(long onBehalfUserId)
        {
            var currentUserId = _principalProvider.Current.Id;

            if (!currentUserId.HasValue)
            {
                return new BusinessLogic<User, bool>(a => false);
            }

            var isMyHrEmployeeOrMyself = GetIsMyHrEmployeeOrMyselfBusinessLogic(currentUserId.Value);

            return new BusinessLogic<User, bool>(a => a.Id == onBehalfUserId && isMyHrEmployeeOrMyself.Call(a));
        }

        public BusinessLogic<User, bool> GetIsMyHrEmployeeOrMyselfBusinessLogic(long currentUserId)
        {
            var userRoles = _userService.GetUserRolesById(currentUserId);
            var isMyself = new BusinessLogic<User, bool>(u => u.Id == currentUserId);

            if (userRoles.Contains(SecurityRoleType.CanRequestAbsenceForOthers))
            {
                long employeeId;
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    employeeId = unitOfWork.Repositories.Employees.Single(e => e.UserId == currentUserId).Id;
                }

                var context = _systemParameterService.GetDefaultContext(employeeId);
                var companiesImAdministrating =
                    _systemParameterService.GetParameter<string[]>(ParameterKeys.CompaniesImAdministrating, context);

                return new BusinessLogic<User, bool>(
                    u => isMyself.Call(u) || (IsActivelyWorking.Call(u) && u.Employees.Any(e => companiesImAdministrating.Contains(e.Company.ActiveDirectoryCode))));
            }

            return isMyself;
        }

        public static readonly BusinessLogic<User, string> FullName = new BusinessLogic<User, string>
            (user => user.FirstName + " " + user.LastName);

        public static readonly BusinessLogic<User, DateTime, bool> HasValidConsent =
            new BusinessLogic<User, DateTime, bool>((u, today) => true);

        public static readonly BusinessLogic<User, SecurityRoleType, bool> HasRole =
            new BusinessLogic<User, SecurityRoleType, bool>((user, role) =>
                user.Roles.Any(r => r.Id == (int)role)
                || user.Profiles.Any(p => p.Roles.Any(pr => pr.Id == (int)role)));

        public static readonly BusinessLogic<User, string, bool> HasProfile = new BusinessLogic<User, string, bool>(
            (u, adGroupName) => u.Profiles.Any(s => s.ActiveDirectoryGroupName == adGroupName));
    }
}