﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface IUserPermissionService
    {
        void GrantSecurityRolesToUser(long userId, IEnumerable<long> roleIds);
        void RevokeSecurityRolesFromUser(long userId, IEnumerable<long> roleIds);
        void AssignSecurityProfilesToUser(long userId, IEnumerable<long> profileIds, bool removeOtherProfiles = false);
        void RemoveSecurityProfilesFromUser(long userId, IEnumerable<long> profileIds);
        IEnumerable<long> GetSecurityProfileIdsFromActiveDirectoryGroups(IEnumerable<string> activeDirectoryGroupNames);
        void AssignUsersToProfile(long profileId, IEnumerable<long> userIds);
        void RemoveUsersFromProfile(long profileId, IEnumerable<long> userIds);
    }
}