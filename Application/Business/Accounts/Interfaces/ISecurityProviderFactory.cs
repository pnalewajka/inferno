﻿using Smt.Atomic.Business.Accounts.Dto;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface ISecurityProviderFactory
    {
        ISecurityProvider GetAtomicSecurityProvider(UserDto currentUserDto, CredentialDto currentUserCredentialDto, string[] roles);

        ISecurityProvider GetAtomicSecurityProvider(UserDto currentUserDto, string[] roles);

        ISecurityProvider GetActiveDirectorySecurityProvider(UserDto currentUserDto, string[] roles);

        ISecurityProvider GetUnknownUserSecurityProvider(string login);
    }
}