﻿namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface ILoginService
    {
        string ResolveLogin(string identifier);
    }
}
