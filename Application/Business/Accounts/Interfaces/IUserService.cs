using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Providers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface IUserService
    {
        UserDto GetUserByLogin(string login);

        UserDto GetUserOrDefaultByLogin(string login);

        IEnumerable<SecurityRoleType> GetUserRolesByLogin(string login);

        IEnumerable<SecurityRoleType> GetUserRolesById(long userId);

        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="defaultRoleTypes"></param>
        /// <param name="userAuthorizationValidationResult"></param>
        Task<CreateUserResult> CreateUserAsync(UserDto userDto, SecurityRoleType[] defaultRoleTypes, UserAuthorizationValidationResult userAuthorizationValidationResult);

        /// <summary>
        /// Verifies if activation email can be sent
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<bool> CanActivationEmailBeResendAsync(string email);

        /// <summary>
        /// Resend activation email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<BoolResult> ResendActivationEmailAsync(string email);

        /// <summary>
        /// Gets user by its email and authorization provider
        /// </summary>
        /// <param name="email"></param>
        /// <param name="authorizationProvider"></param>
        /// <returns>User with given email or null</returns>
        UserDto GetUserOrDefaultByEmail(string email, AuthorizationProviderType? authorizationProvider);

        /// <summary>
        /// Gets user by its email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>User with given email or null</returns>
        UserDto GetUserOrDefaultByEmail(string email);

        /// <summary>
        /// Find user by provider data (email or external id)
        /// </summary>
        /// <param name="email"></param>
        /// <param name="externalId"></param>
        /// <param name="authorizationProviderType"></param>
        /// <returns></returns>
        UserDto FindUserByProviderDataOrDefault(string email, string externalId,
            AuthorizationProviderType? authorizationProviderType);

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="userDto"></param>
        long CreateUser(UserDto userDto);

        /// <summary>
        /// Check if user credentials for specific provider type exists
        /// </summary>
        /// <param name="email"></param>
        /// <param name="authorizationProviderType"></param>
        /// <returns></returns>
        bool DoesUserCredentialExist(string email, AuthorizationProviderType authorizationProviderType);

        /// <summary>
        /// Create credentials
        /// </summary>
        /// <param name="credentialDto"></param>
        void CreateUserCredential(CredentialDto credentialDto);

        /// <summary>
        /// Create user account from auth provider data
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="authorizationProviderType"></param>
        /// <param name="securityRoles"></param>
        /// <returns></returns>
        bool TryCreateUserFromAuth(UserDto userDto, AuthorizationProviderType authorizationProviderType, SecurityRoleType[] securityRoles);

        /// <summary>
        /// Generates new password and sends email to the user.
        /// </summary>
        /// <param name="data"></param>
        void GeneratePasswordAndSendEmail(UserEmailDataDto data);

        /// <summary>
        /// Generates new login token and sends email to the user.
        /// </summary>
        /// <param name="data"></param>
        void GenerateLoginTokenAndSendEmail(UserEmailDataDto data);

        /// <summary>
        /// Finds user by Id.
        /// </summary>
        /// <param name="userId"></param>
        UserDto GetUserById(long userId);

        /// <summary>
        /// Finds users by Ids.
        /// </summary>
        /// <param name="userIds">An array of users ids.</param>
        /// <returns>An array of users.</returns>
        UserDto[] GetUsersByIds(long[] userIds);

        /// <summary>
        /// Finds user by Active Directory Id.
        /// </summary>
        /// <param name="activeDirectoryId"></param>
        UserDto GetUserOrDefaultByActiveDirectoryId(Guid activeDirectoryId);

        /// <summary>
        /// Finds user by CRM Id.
        /// </summary>
        /// <param name="crmId"></param>
        UserDto GetUserOrDefaultByCrmId(string crmId);

        /// <summary>
        /// Synchronize user data with Active Directory.
        /// </summary>
        /// <param name="activeDirectoryUserDto"></param>
        void SynchronizeWithActiveDirectory(ActiveDirectoryUserDto activeDirectoryUserDto);

        /// <summary>
        /// Deactivate user missing in Active Directory.
        /// </summary>
        /// <param name="userId"></param>
        void DeactivateUserMissingInActiveDirectory(long userId);
    }
}
