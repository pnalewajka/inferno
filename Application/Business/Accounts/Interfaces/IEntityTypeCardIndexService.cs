﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface IEntityTypeCardIndexService : ICardIndexDataService<EntityTypeDto>
    {
        long GetEntityTypeId(string entityTypeName);
        string GetEntityTypeName(long entityTypeId);
        string GetEntityModule(long entityTypeId);
    }
}