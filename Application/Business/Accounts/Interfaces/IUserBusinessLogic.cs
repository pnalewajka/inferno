﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Accounts.BusinessLogics
{
    public interface IUserBusinessLogic
    {
        BusinessLogic<User, bool> GetIsMyHrEmployeeOrMyselfBusinessLogic(long currentUserId);

        BusinessLogic<User, bool> CanChangeUserDataOnBehalf(long onBehalfUserId);
    }
}