﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface IProfileUserCardIndexService : ICardIndexDataService<UserDto>
    {        
    }
}