﻿namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface IPasswordGenerator
    {
        string GeneratePassword();
        string GenerateLoginToken();
    }
}