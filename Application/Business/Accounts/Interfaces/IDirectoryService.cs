﻿namespace Smt.Atomic.Business.Accounts.Interfaces
{
    /// <summary>
    /// Active Directory access service
    /// </summary>
    public interface IDirectoryService
    {
        /// <summary>
        /// Get user email from AD
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        string GetUserEmail(string userName);
    }
}
