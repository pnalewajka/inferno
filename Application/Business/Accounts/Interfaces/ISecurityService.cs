﻿using System;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    /// <summary>
    /// Service responsible for operations on user credentials
    /// </summary>
    public interface ISecurityService
    {
        /// <summary>
        /// Login user authenticated via active directory
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        UserValidationResult ValidateUserAgainstDirectory(string email);

        UserValidationResult ValidateUser(string login, string password);

        UserValidationResult ValidateUser(string token);

        BoolResult SetPassword(string login, string newPassword);

        BoolResult ChangePassword(string login, string oldPassword, string newPassword);

        void CreateUserPasswordCredentials(long userId, string password);

        void RevokeUserPasswordCredentials(long userId);

        void IssueLoginToken(long userId, string token, DateTime expiresOn);

        BoolResult IsTokenValid(string token);
    }
}