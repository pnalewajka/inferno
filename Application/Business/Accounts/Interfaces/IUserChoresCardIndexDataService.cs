﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface IUserChoresCardIndexDataService
        : ICardIndexDataService<ChoreDto>
    {
    }
}
