﻿using System;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface ISecurityProvider
    {
        /// <summary>
        /// Does this provider support changing password
        /// </summary>
        /// <returns></returns>
        bool CanChangePassword();

        /// <summary>
        /// Change password for user
        /// </summary>
        /// <param name="oldPassword">Old password</param>
        /// <param name="password">New password</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException">If provider doesn't support seting password</exception>
        BoolResult ChangePassword(string oldPassword, string password);

        /// <summary>
        /// Set password for user
        /// </summary>
        /// <param name="password">New password</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException">If provider doesn't support seting password</exception>
        BoolResult SetPassword(string password);

        /// <summary>
        /// Validate if password is correct for user
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        UserValidationResult ValidatePassword(string password);
    }
}
