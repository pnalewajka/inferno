﻿using Smt.Atomic.Business.Accounts.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.Interfaces
{
    public interface ISecurityReportService
    {
        IEnumerable<UserSecurityRolesDto> GetUsersSecurityRoles();
        IEnumerable<UserSecurityRolesDto> GetUserSecurityRoles(long[] ids);
        IEnumerable<UserSecurityProfilesDto> GetUsersSecurityProfiles();
        IEnumerable<UserSecurityProfilesDto> GetUserSecurityProfiles(long[] ids);
    }
}
