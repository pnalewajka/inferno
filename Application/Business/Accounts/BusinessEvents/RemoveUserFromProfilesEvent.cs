﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Accounts.BusinessEvents
{
    [DataContract]
    public class RemoveUserFromProfilesEvent : BusinessEvent
    {
        [DataMember]
        public long[] UserIds { get; set; }

        [DataMember]
        public string ActiveDirectoryGroupName { get; set; }
    }
}
