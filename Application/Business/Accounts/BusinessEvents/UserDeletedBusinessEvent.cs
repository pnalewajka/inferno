﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Accounts.BusinessEvents
{
    [DataContract]
    public class UserDeletedBusinessEvent : BusinessEvent
    {
        [DataMember]
        public long UserId { get; set; }

        [DataMember]
        public UserBusinessEventData RecordData { get; set; }
    }
}
