﻿using System.Runtime.Serialization;
using Smt.Atomic.Business.Accounts.Dto;

namespace Smt.Atomic.Business.Accounts.BusinessEvents
{
    [DataContract]
    public class UserBusinessEventData
    {
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Email { get; set; }

        public UserBusinessEventData()
        {
        }

        public UserBusinessEventData(UserDto dto)
        {
            Login = dto.Login;
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            Email = dto.Email;
        }
    }
}