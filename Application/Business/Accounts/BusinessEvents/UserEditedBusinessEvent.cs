﻿using System;
using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Accounts.BusinessEvents
{
    [DataContract]
    public class UserEditedBusinessEvent : BusinessEvent
    {
        [DataMember]
        public long UserId { get; set; }

        [DataMember]
        public long? ModifiedById { get; set; }

        [DataMember]
        public DateTime ModifiedOn { get; set; }

        [DataMember]
        public UserBusinessEventData OriginalRecordData { get; set; }

        [DataMember]
        public UserBusinessEventData NewRecordData { get; set; }
    }
}
