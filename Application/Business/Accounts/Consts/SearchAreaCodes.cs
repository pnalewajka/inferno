﻿namespace Smt.Atomic.Business.Accounts.Consts
{
    public static class SearchAreaCodes
    {
        public const string Name = "name";
        public const string Email = "email";
        public const string Login = "login";
        public const string ActiveDirectoryGroup = "ad-group";
        public const string Roles = "roles";
    }
}