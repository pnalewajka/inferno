﻿namespace Smt.Atomic.Business.Accounts.Consts
{
    public static class FilterCodes
    {
        public const string UserStatusActive = "active";
        public const string UserStatusInactive = "inactive";
        public const string UserLastModifiedBetweenDates = "log";
        public const string UserLastModifiedBetweenDatesWithDto = "logd";
        public const string UserRoles = "user-role";
        public const string UserProfiles = "user-profile";
    }
}
