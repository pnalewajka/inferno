﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Accounts.ChoreProviders
{
    public class MissingAvatarChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;

        public MissingAvatarChoreProvider(
            IPrincipalProvider principalProvider,
            ISystemParameterService systemParameterService)
        {
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
        }

        public IQueryable<ChoreDto> GetActiveChores(
            IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (_principalProvider.Current == null 
                || _principalProvider.Current.Email == null
                || !_principalProvider.Current.Email.EndsWith(_systemParameterService.GetParameter<string>(ParameterKeys.UserDefaultEmailDomain)))
            {
                return null;
            }

            var missingAvatars = repositoryFactory
                .Create<User>()
                .Where(u => u.Id == _principalProvider.Current.Id && u.PictureId == null);

            var missingAvatarChores = missingAvatars
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.UserAvatarMissing,
                    DueDate = null,
                    Parameters = null                   
                });

            return missingAvatarChores;
        }
    }
}
