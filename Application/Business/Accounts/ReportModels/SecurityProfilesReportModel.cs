﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.ReportModels
{
    public class SecurityProfilesReportModel
    {
        public ICollection<UserProfilesReportModel> UserProfiles { get; set; }
    }
}
