﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.ReportModels
{
    public class SecurityRolesReportModel
    {
        public ICollection<UserRolesReportModel> UserRoles { get; set; }
    }
}
