﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.ReportModels
{
    public class UserProfilesReportModel
    {
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }

        public ICollection<DataColumn> Roles { get; set; }
    }
}
