﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.DocumentMappings
{
    [Identifier("Pictures.UserPictureMapping")]
    public class UserPictureMapping : DocumentMapping<UserPicture, UserPictureContent, IAccountsDbScope>
    {
        public UserPictureMapping(IUnitOfWorkService<IAccountsDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
