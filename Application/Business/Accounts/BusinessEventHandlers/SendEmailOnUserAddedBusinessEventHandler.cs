﻿using Smt.Atomic.Business.Accounts.BusinessEvents;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Accounts.BusinessEventHandlers
{
    public class SendEmailOnUserAddedBusinessEventHandler : BusinessEventHandler<UserAddedBusinessEvent>
    {
        private readonly IUserService _userService;
        private readonly ISystemParameterService _systemParameterService;

        public SendEmailOnUserAddedBusinessEventHandler(
            IUserService userService,
            ISystemParameterService systemParameterService)
        {
            _userService = userService;
            _systemParameterService = systemParameterService;
        }

        public override void Handle(UserAddedBusinessEvent businessEvent)
        {
            var emailDataDto = new UserEmailDataDto
            {
                UserId = businessEvent.UserId,
                FirstName = businessEvent.RecordData.FirstName,
                LastName = businessEvent.RecordData.LastName,
                Email = businessEvent.RecordData.Email,
                Login = businessEvent.RecordData.Login,
            };

            GenerateAuthMethodAndSendEmail(emailDataDto);
        }

        private void GenerateAuthMethodAndSendEmail(UserEmailDataDto emailDataDto)
        {
            if (_systemParameterService.GetParameter<bool>(ParameterKeys.AccountsNewUserSendPassword))
            {
                _userService.GeneratePasswordAndSendEmail(emailDataDto);
            }

            if (_systemParameterService.GetParameter<bool>(ParameterKeys.AccountsNewUserSendToken))
            {
                _userService.GenerateLoginTokenAndSendEmail(emailDataDto);
            }
        }
    }
}
