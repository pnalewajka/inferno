﻿using Smt.Atomic.Business.Accounts.BusinessEvents;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;

namespace Smt.Atomic.Business.Accounts.BusinessEventHandlers
{
    public class RemoveUserFromProfilesEventHandler : BusinessEventHandler<RemoveUserFromProfilesEvent>
    {
        private readonly IUserPermissionService _userPermissionService;

        public RemoveUserFromProfilesEventHandler(IUserPermissionService userPermissionService)
        {
            _userPermissionService = userPermissionService;
        }

        public override void Handle(RemoveUserFromProfilesEvent businessEvent)
        {
            foreach (var userId in businessEvent.UserIds)
            {
                _userPermissionService.RemoveSecurityProfilesFromUserBasedOnActiveDirectory(userId, businessEvent.ActiveDirectoryGroupName);
            }
        }
    }
}
