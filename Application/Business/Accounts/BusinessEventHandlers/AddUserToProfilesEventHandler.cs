﻿using Smt.Atomic.Business.Accounts.BusinessEvents;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;

namespace Smt.Atomic.Business.Accounts.BusinessEventHandlers
{
    public class AddUserToProfilesEventHandler : BusinessEventHandler<AddUserToProfilesEvent>
    {
        private readonly IUserPermissionService _userPermissionService;

        public AddUserToProfilesEventHandler(IUserPermissionService userPermissionService)
        {
            _userPermissionService = userPermissionService;
        }

        public override void Handle(AddUserToProfilesEvent businessEvent)
        {
            foreach (var userId in businessEvent.UserIds)
            {
                _userPermissionService.AssignSecurityProfilesToUserBasedOnActiveDirectory(userId, businessEvent.ActiveDirectoryGroupName);
            }
        }
    }
}
