﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserProfilesReportParametersDto
    {
        public long[] UserIds { get; set; }
        public long[] ProfileIds { get; set; }
    }
}
