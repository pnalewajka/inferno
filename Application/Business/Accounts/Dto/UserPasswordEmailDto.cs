﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserPasswordEmailDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}