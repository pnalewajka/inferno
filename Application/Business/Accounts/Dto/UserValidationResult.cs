﻿using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserValidationResult : BusinessResult
    {
        public bool IsUserAuthenticated { get; set; }
        public bool IsPasswordExpired { get; set; }
        public long UserId { get; set; }
        public string Login { get; set; }
        public string[] Roles { get; set; }
    }
}