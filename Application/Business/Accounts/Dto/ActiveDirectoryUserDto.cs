﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class ActiveDirectoryUserDto
    {
        public Guid ActiveDirectoryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Acronym { get; set; }
        public string Login { get; set; }
        public long? CompanyId { get; set; }
        public long? LocationId { get; set; }
        public PlaceOfWork? PlaceOfWork { get; set; }
        public long OrgUnitId { get; set; }
        public string Path { get; set; }
        public List<string> TreePath { get; set; }
        public string CrmId { get; set; }
        public Guid? ActiveDirectoryIdLineManager { get; set; }
        public HashSet<string> Groups { get; set; }
        public long? JobMatrixLevelId { get; set; }
        public long? JobProfileId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ContractType { get; set; }
        public bool IsActive { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool IsProjectContributor { get; set; }
    }
}
