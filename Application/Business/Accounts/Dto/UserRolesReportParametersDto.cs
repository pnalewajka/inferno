﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserRolesReportParametersDto
    {
        public long[] UserIds { get; set; }
        public long[] RoleIds { get; set; }
    }
}
