﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class SecurityRoleDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Module { get; set; }
    }
}
