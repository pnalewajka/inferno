﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class DataFilterDto
    {
        public long Id { get; set; }

        public long? UserId { get; set; }
        
        public long? ProfileId { get; set; }

        public string Entity { get; set; }
        
        public string Module { get; set; }

        public string Condition { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
