﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class DataFilterToDataFilterDtoMapping : ClassMapping<DataFilter, DataFilterDto>
    {
        public DataFilterToDataFilterDtoMapping()
        {
            Mapping = e => new DataFilterDto
            {
                Id = e.Id,
                UserId = e.UserId,
                ProfileId = e.ProfileId,
                Entity = e.Entity,
                Module = e.Module,
                Condition = e.Condition,
                Timestamp = e.Timestamp,
            };
        }
    }
}
