﻿using System;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserLastLoggedOnBetweenDatesFilterDto
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
