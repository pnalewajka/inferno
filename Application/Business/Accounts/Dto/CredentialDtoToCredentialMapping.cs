﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Accounts.Dto
{
    public class CredentialDtoToCredentialMapping : ClassMapping<CredentialDto, Credential>
    {
        public CredentialDtoToCredentialMapping()
        {
            Mapping = d => new Credential
            {
                PasswordHash = d.PasswordHash,
                Salt = d.Salt,
                CreatedOn = d.CreatedOn,
                LastUsedOn = d.LastUsedOn,
                IsExpired = d.IsExpired,
                UserId = d.UserId,
                Email = d.Email,
                ProviderType = d.ProviderType,
                Id = d.Id,
                Timestamp = d.Timestamp,
                ExternalId = d.ExternalId
            };
        }
    }
}
