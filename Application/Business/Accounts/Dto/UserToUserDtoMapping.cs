﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserToUserDtoMapping : ClassMapping<User, UserDto>
    {
        public UserToUserDtoMapping()
        {
            Mapping = e => new UserDto
            {
                Id = e.Id,
                Email = e.Email,
                FirstName = e.FirstName,
                ImpersonatedById = e.ImpersonatedById,
                IsActive = e.IsActive,
                LastName = e.LastName,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                LastLoggedOn = e.LastLoggedOn,
                Login = e.Login,
                Timestamp = e.Timestamp,
                Documents = e.Documents.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                    DocumentId = p.Id
                }),
                Picture = e.Picture == null
                    ? null
                    : new DocumentDto
                    {
                        ContentType = e.Picture.ContentType,
                        DocumentName = e.Picture.Name,
                        DocumentId = e.Picture.Id
                    },
                Location = e.Location == null
                    ? null
                    : new GeographyPointDto
                    {
                        Latitude = e.Location.Latitude.Value,
                        Longtitude = e.Location.Longitude.Value,
                    },
                LocationRadius = e.LocationRadius,
                IsDeleted = e.IsDeleted,
                ActiveDirectoryId = e.ActiveDirectoryId,
                CrmId = e.CrmId,
                LoginTokenId = e.LoginTokenId
            };
        }
    }
}