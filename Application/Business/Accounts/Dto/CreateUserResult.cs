﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class CreateUserResult : BoolResult
    {
        public UserDto UserDto { get; set; }

        public CreateUserResult()
        {
            
        }

        public CreateUserResult(bool isSuccessful, UserDto userDto, IEnumerable<AlertDto> alerts = null) : base(isSuccessful, alerts)
        {
            UserDto = userDto;
        }
    }
}
