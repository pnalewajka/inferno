﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class SecurityProfileToSecurityProfileDtoMapping : ClassMapping<SecurityProfile, SecurityProfileDto>
    {
        public SecurityProfileToSecurityProfileDtoMapping()
        {
            Mapping = e => new SecurityProfileDto
            {
                Id = e.Id,
                Name = e.Name,
                ActiveDirectoryGroupName = e.ActiveDirectoryGroupName,
                RoleIds = e.Roles.Select(r => r.Id).ToArray(),
                Timestamp = e.Timestamp,
            };
        }
    }
}
