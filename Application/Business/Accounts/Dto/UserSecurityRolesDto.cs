﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserSecurityRolesDto
    {
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }

        public IEnumerable<long> RoleIds { get; set; }
    }
}
