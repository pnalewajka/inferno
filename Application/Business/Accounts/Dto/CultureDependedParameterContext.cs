﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Accounts.Dto
{
    [JsonObject]
    [Identifier(nameof(CultureDependedParameterContext))]
    public class CultureDependedParameterContext
    {
        public string CurrentCulture { get; set; }
    }
}
