﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class SecurityProfileDtoToSecurityProfileMapping : ClassMapping<SecurityProfileDto, SecurityProfile>
    {
        public SecurityProfileDtoToSecurityProfileMapping()
        {
            Mapping = d => new SecurityProfile
            {
                Id = d.Id,
                Name = d.Name,
                ActiveDirectoryGroupName = d.ActiveDirectoryGroupName,
                Roles = new Collection<SecurityRole>(d.RoleIds.Select(i => new SecurityRole{Id = i}).ToList()),
                Timestamp = d.Timestamp,
            };
        }
    }
}
