﻿using System;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserLoginTokenEmailDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginUrl { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string FooterHtml { get; set; }
    }
}