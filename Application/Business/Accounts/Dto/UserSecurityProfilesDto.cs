﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserSecurityProfilesDto
    {
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }

        public IEnumerable<long> ProfileIds { get; set; }
    }
}
