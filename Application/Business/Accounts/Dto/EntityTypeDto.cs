﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class EntityTypeDto
    {
        public long Id { get; set; }

        public string Type { get; set; }

        public string Module { get; set; }
        
        public string Namespace { get; set; }
    }
}