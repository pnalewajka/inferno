﻿using System.Collections.ObjectModel;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserDtoToUserMapping : ClassMapping<UserDto, User>
    {
        public UserDtoToUserMapping()
        {
            Mapping = d => new User
            {
                Id = d.Id,
                Email = d.Email,
                FirstName = d.FirstName,
                ImpersonatedById = d.ImpersonatedById,
                IsActive = d.IsActive,
                LastName = d.LastName,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                LastLoggedOn = d.LastLoggedOn,
                Login = d.Login == null ? null : d.Login.ToLowerInvariant(),
                Timestamp = d.Timestamp,
                Documents = new Collection<UserDocument>(),
                Picture = null,
                Location = d.Location == null
                    ? null
                    : SpatialUtils.CreateDbPoint(d.Location.Latitude, d.Location.Longtitude),
                LocationRadius = d.LocationRadius,
                ActiveDirectoryId = d.ActiveDirectoryId,
                CrmId = d.CrmId,
                LoginTokenId = d.LoginTokenId
            };
        }
    }
}