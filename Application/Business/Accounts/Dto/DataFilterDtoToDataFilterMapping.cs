﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class DataFilterDtoToDataFilterMapping : ClassMapping<DataFilterDto, DataFilter>
    {
        public DataFilterDtoToDataFilterMapping()
        {
            Mapping = d => new DataFilter
            {
                Id = d.Id,
                UserId = d.UserId,
                ProfileId = d.ProfileId,
                Entity = d.Entity,
                Module = d.Module,
                Condition = d.Condition,
                Timestamp = d.Timestamp,
            };
        }
    }
}
