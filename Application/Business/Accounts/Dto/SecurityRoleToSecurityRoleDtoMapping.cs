﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Accounts.Dto
{
    public class SecurityRoleToSecurityRoleDtoMapping : ClassMapping<SecurityRole, SecurityRoleDto>
    {
        public SecurityRoleToSecurityRoleDtoMapping()
        {
            Mapping = e => new SecurityRoleDto
            {
                Id = e.Id,
                Name = e.Name,
            };
        }
    }
}
