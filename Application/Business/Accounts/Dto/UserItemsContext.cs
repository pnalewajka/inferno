﻿using Smt.Atomic.Business.Common.Services;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserItemsContext : ParentIdContext
    {
        /// <summary>
        /// Indicate if looking for items that belong to user specified (when false) or exclude such items (when true)
        /// </summary>
        public bool ShouldExclude { get; set; }
    }
}