﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserPickerContext
    {
        public bool? IsActivelyWorking { get; set; }
    }
}
