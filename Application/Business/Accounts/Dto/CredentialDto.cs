﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
namespace Smt.Atomic.Business.Accounts.Dto
{
    public class CredentialDto
    {
        public string PasswordHash { get; set; }

        public string Salt { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? LastUsedOn { get; set; }

        public bool IsExpired { get; set; }

        public long UserId { get; set; }

        public string Email { get; set; }

        public AuthorizationProviderType ProviderType { get; set; }

        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public string ExternalId { get; set; }
    }
}
