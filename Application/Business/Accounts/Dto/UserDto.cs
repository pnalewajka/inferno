﻿using System;
using System.Collections.Generic;
using System.Spatial;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public DateTime? LastLoggedOn { get; set; }

        public GeographyPointDto Location { get; set; }

        public int? LocationRadius { get; set; }
    
        public byte[] Timestamp { get; set; }

        public IEnumerable<DocumentDto> Documents { get; set; }

        public DocumentDto Picture { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? ActiveDirectoryId { get; set; }

        public string CrmId { get; set; }

        public long? LoginTokenId { get; set; }

        public string GetFullName()
        {
            return $"{FirstName} {LastName}".Trim();
        }
    }
}