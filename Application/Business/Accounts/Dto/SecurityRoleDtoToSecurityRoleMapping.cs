﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Accounts.Dto
{
    public class SecurityRoleDtoToSecurityRoleMapping : ClassMapping<SecurityRoleDto, SecurityRole>
    {
        public SecurityRoleDtoToSecurityRoleMapping()
        {
            Mapping = d => new SecurityRole
            {
                Id = d.Id,
                Name = d.Name,
            };
        }
    }
}
