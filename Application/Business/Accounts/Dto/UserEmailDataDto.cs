﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class UserEmailDataDto
    {
        public long UserId { get; set; }

        public string Login { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string GetFullName()
        {
            return $"{FirstName} {LastName}".Trim();
        }
    }
}
