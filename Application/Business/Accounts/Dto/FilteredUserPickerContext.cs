﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class FilteredUserPickerContext
    {
        /// <summary>
        /// Name of key in enum: FilteredUserPickerFilterKeys
        /// </summary>
        public string FilterKey { get; set; }
    }
}
