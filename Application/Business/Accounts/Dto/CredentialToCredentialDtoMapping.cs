﻿using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Accounts.Dto
{
    public class CredentialToCredentialDtoMapping : ClassMapping<Credential, CredentialDto>
    {
        public CredentialToCredentialDtoMapping()
        {
            Mapping = e => new CredentialDto
            {
                PasswordHash = e.PasswordHash,
                Salt = e.Salt,
                CreatedOn = e.CreatedOn,
                LastUsedOn = e.LastUsedOn,
                IsExpired = e.IsExpired,
                UserId = e.UserId,
                Email = e.Email,
                ProviderType = e.ProviderType,
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                ExternalId = e.ExternalId
            }
            ;
        }
    }
}
