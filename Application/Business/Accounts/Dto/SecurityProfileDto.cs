﻿namespace Smt.Atomic.Business.Accounts.Dto
{
    public class SecurityProfileDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ActiveDirectoryGroupName { get; set; }

        public long[] RoleIds { get; set; }

        public byte[] Timestamp { get; set; }
    }
}