﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.ReportModels;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Reporting;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Accounts.ReportDataSources
{
    [Identifier("DataSource.UserRolesReportDataSource")]
    [RequireRole(CrossCutting.Common.Enums.SecurityRoleType.CanViewUserRoles)]
    [DefaultReportDefinition(
        "UserRolesReport",
        nameof(ReportsResources.UserRolesReportName),
        nameof(ReportsResources.UserRolesReportDescription),
        MenuAreas.Administration,
        MenuGroups.AdministrationReports,
        ReportingEngine.MsExcel, 
        "Smt.Atomic.Business.Accounts.ReportTemplates.UserSecurityRolesReport.xlsx", 
        typeof(ReportsResources))]
    public class UserRolesReportDataSource : BaseReportDataSource<UserRolesReportParametersDto>
    {
        private readonly ISecurityReportService _securityReportService;
        private readonly ISecurityRoleCardIndexService _securityRoleCardIndexService;

        public UserRolesReportDataSource(
            ISecurityReportService securityReportService,
            ISecurityRoleCardIndexService securityRoleCardIndexService)
        {
            _securityReportService = securityReportService;
            _securityRoleCardIndexService = securityRoleCardIndexService;
        }

        public override object GetData(ReportGenerationContext<UserRolesReportParametersDto> reportGenerationContext)
        {
            var allRoles = GetAllRoles(reportGenerationContext.Parameters);
            var usersRoles = GetUserRoles(reportGenerationContext.Parameters);

            return new SecurityRolesReportModel
            {
                UserRoles = usersRoles.Select(
                    userRoles => CreateReportModel(userRoles, allRoles))
                    .ToList()
            };
        }

        public override UserRolesReportParametersDto GetDefaultParameters(ReportGenerationContext<UserRolesReportParametersDto> reportGenerationContext)
        {
            return new UserRolesReportParametersDto();
        }

        private IEnumerable<SecurityRoleDto> GetAllRoles(UserRolesReportParametersDto parameters)
        {
            return parameters.RoleIds == null || parameters.RoleIds.Length < 1
                ? _securityRoleCardIndexService.GetRecords().Rows
                : _securityRoleCardIndexService.GetRecordsByIds(parameters.RoleIds).Values;
        }

        private IEnumerable<UserSecurityRolesDto> GetUserRoles(UserRolesReportParametersDto parameters)
        {
            return parameters.UserIds.Any()
                ? _securityReportService.GetUserSecurityRoles(parameters.UserIds)
                : _securityReportService.GetUsersSecurityRoles();
        }

        private static UserRolesReportModel CreateReportModel(UserSecurityRolesDto userRoles, IEnumerable<SecurityRoleDto> roles)
        {
            return new UserRolesReportModel
            {
                UserFirstName = userRoles.UserFirstName,
                UserLastName = userRoles.UserLastName,
                UserEmail = userRoles.UserEmail,
                Roles = roles.Select(r => new DataColumn
                {
                    ColumnName = r.Name,
                    Value = (userRoles.RoleIds.Contains(r.Id) ? ReportConfiguration.ContainsMarker : string.Empty)
                }).ToList()
            };
        }
    }
}
