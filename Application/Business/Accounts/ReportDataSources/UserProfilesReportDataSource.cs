﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.ReportModels;
using Smt.Atomic.Business.Accounts.Resources;
using Smt.Atomic.Business.Reporting;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Accounts.ReportDataSources
{
    [Identifier("DataSource.UserProfilesReportDataSource")]
    [RequireRole(CrossCutting.Common.Enums.SecurityRoleType.CanViewUserProfiles)]
    [DefaultReportDefinition(
        "UserProfilesReport",
        nameof(ReportsResources.UserProfilesReportName),
        nameof(ReportsResources.UserProfilesReportDescription),
        MenuAreas.Administration,
        MenuGroups.AdministrationReports,
        ReportingEngine.MsExcel, 
        "Smt.Atomic.Business.Accounts.ReportTemplates.UserSecurityProfilesReport.xlsx", 
        typeof(ReportsResources))]
    public class UserProfilesReportDataSource : BaseReportDataSource<UserProfilesReportParametersDto>
    {
        private const int ProfilesPerPage = 200;
        private readonly ISecurityReportService _securityReportService;
        private readonly ISecurityProfileCardIndexService _securityProfileCardIndexService;

        public UserProfilesReportDataSource(
            ISecurityReportService securityReportService,
            ISecurityProfileCardIndexService securityProfileCardIndexService)
        {
            _securityReportService = securityReportService;
            _securityProfileCardIndexService = securityProfileCardIndexService;
        }

        public override object GetData(ReportGenerationContext<UserProfilesReportParametersDto> reportGenerationContext)
        {
            var parameters = reportGenerationContext.Parameters;
            var allProfiles = GetAllProfiles(parameters);
            var usersProfiles = GetUsersProfiles(parameters);

            return new SecurityProfilesReportModel
            {
                UserProfiles = usersProfiles.Select(
                    userProfiles => CreateReportModel(userProfiles, allProfiles))
                    .ToList()
            };
        }

        public override UserProfilesReportParametersDto GetDefaultParameters(ReportGenerationContext<UserProfilesReportParametersDto> reportGenerationContext)
        {
            return new UserProfilesReportParametersDto();
        }

        private IEnumerable<SecurityProfileDto> GetAllProfiles(UserProfilesReportParametersDto parameters)
        {
            var queryCriteria = new Common.Dto.QueryCriteria
            {
                PageSize = ProfilesPerPage
            };

            return parameters.ProfileIds == null || parameters.ProfileIds.Length < 1
                ? _securityProfileCardIndexService.GetRecords(queryCriteria).Rows
                : _securityProfileCardIndexService.GetRecordsByIds(parameters.ProfileIds).Values;
        }

        private IEnumerable<UserSecurityProfilesDto> GetUsersProfiles(UserProfilesReportParametersDto parameters)
        {
            return parameters.UserIds.Any()
                ? _securityReportService.GetUserSecurityProfiles(parameters.UserIds)
                : _securityReportService.GetUsersSecurityProfiles();
        }

        private static UserProfilesReportModel CreateReportModel(UserSecurityProfilesDto userProfiles, IEnumerable<SecurityProfileDto> profiles)
        {
            return new UserProfilesReportModel
            {
                UserFirstName = userProfiles.UserFirstName,
                UserLastName = userProfiles.UserLastName,
                UserEmail = userProfiles.UserEmail,
                Roles = profiles.Select(r => new DataColumn
                {
                    ColumnName = r.Name,
                    Value = (userProfiles.ProfileIds.Contains(r.Id) ? ReportConfiguration.ContainsMarker : string.Empty)
                }).ToList()
            };
        }
    }
}
