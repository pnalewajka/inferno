﻿namespace Smt.Atomic.Business.Reporting.Enums
{
    public enum ReportRelatedClassType
    {
        /// <summary>
        /// Classes implementing IReportingEngine
        /// </summary>
        ReportingEngine,

        /// <summary>
        /// Classes implementing IReportDataSource, marked with Identifier
        /// </summary>
        DataSource
    }
}
