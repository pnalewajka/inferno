﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTags;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Reporting
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register(Component.For<IReportRelatedClassCardIndexDataService>().ImplementedBy<ReportRelatedClassCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IReportDefinitionCardIndexDataService>().ImplementedBy<ReportDefinitionCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IPowerPointGenerationService>().ImplementedBy<PowerPointGenerationService>().LifestyleTransient());
                    container.Register(Component.For<IReportRegistrationService>().ImplementedBy<ReportRegistrationService>().LifestyleTransient());
                    container.Register(Component.For<IReportingService>().ImplementedBy<ReportingService>().LifestyleTransient());
                    container.Register(Component.For<IExcelGenerationService>().ImplementedBy<ExcelGenerationService>().LifestyleTransient());
                    container.Register(Component.For<IReportDesignService>().ImplementedBy<ReportDesignService>().LifestyleTransient());
                    container.Register(GetWordTemplateEngineComponentsRegistrations());
                    break;

                case ContainerType.UnitTests:
                    container.Register(Component.For<IReportRelatedClassCardIndexDataService>().ImplementedBy<ReportRelatedClassCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IReportDefinitionCardIndexDataService>().ImplementedBy<ReportDefinitionCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IReportRegistrationService>().ImplementedBy<ReportRegistrationService>().LifestyleTransient());
                    container.Register(Component.For<IReportingService>().ImplementedBy<ReportingService>().LifestyleTransient());
                    container.Register(GetWordTemplateEngineComponentsRegistrations());
                    break;

                case ContainerType.JobScheduler:
                    container.Register(Component.For<IReportingService>().ImplementedBy<ReportingService>().LifestyleTransient());
                    break;
            }
        }

        private IRegistration[] GetWordTemplateEngineComponentsRegistrations()
        {
            return new IRegistration[]
            {
                Component.For<IDocxTemplateEngine>().ImplementedBy<DocxTemplateEngine>().LifestyleTransient(),
                Component.For<IConditionalExpressionFactory>().ImplementedBy<ConditionalExpressionFactory>(),
                Classes.FromThisAssembly()
                    .BasedOn<TagProcessor>().WithServiceBase()
                    .Configure(c =>
                        c.Named(c.Implementation.Name)
                            .LifestyleTransient())
            };
        }
    }
}

