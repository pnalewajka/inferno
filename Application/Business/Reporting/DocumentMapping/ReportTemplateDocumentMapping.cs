﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Reporting.DocumentMapping
{
    [Identifier("Documents.ReportTemplateMapping")]
    public class ReportTemplateDocumentMapping : DocumentMapping<ReportTemplate, ReportTemplateContent, IReportingDbScope>
    {
        public ReportTemplateDocumentMapping(IUnitOfWorkService<IReportingDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
