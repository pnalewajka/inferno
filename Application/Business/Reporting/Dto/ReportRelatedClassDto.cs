﻿using System;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportRelatedClassDto
    {
        public long Id { get; set; }

        public string Identifier { get; set; }

        public Type ItemType { get; set; }
    }
}
