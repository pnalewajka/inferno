﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Reporting;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportDefinitionDtoToReportDefinitionMapping : ClassMapping<ReportDefinitionDto, ReportDefinition>
    {
        public ReportDefinitionDtoToReportDefinitionMapping()
        {
            Mapping = d => new ReportDefinition
            {
                Id = d.Id,
                Code = d.Code,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                DescriptionEn = d.Description.English,
                DescriptionPl = d.Description.Polish,
                OutputFileNameTemplate = d.OutputFileNameTemplate,
                ReportingEngineIdentifier = d.ReportingEngineIdentifier,
                DataSourceTypeIdentifier = d.DataSourceTypeIdentifier,
                ReportTemplates = new HashSet<ReportTemplate>(),
                Area = d.Area,
                Group = d.Group,
                AllowedForProfiles = new Collection<SecurityProfile>(d.AllowedForProfiles.Select(p => new SecurityProfile { Id = p }).ToList()),
                Timestamp = d.Timestamp
            };
        }
    }
}
