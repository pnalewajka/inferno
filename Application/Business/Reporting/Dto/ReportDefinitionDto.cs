﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportDefinitionDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public LocalizedString Name { get; set; }

        public LocalizedString Description { get; set; }

        public string OutputFileNameTemplate { get; set; }

        public string ReportingEngineIdentifier { get; set; }

        public string DataSourceTypeIdentifier { get; set; }

        public DocumentDto[] ReportTemplates { get; set; }

        public byte[] Timestamp { get; set; }

        public SecurityRoleType? RequiredRole { get; set; }

        public string Area { get; set; }

        public string Group { get; set; }

        public long[] AllowedForProfiles { get; set; }

        public string[] AllowedForProfileNames { get; set; }
    }
}
