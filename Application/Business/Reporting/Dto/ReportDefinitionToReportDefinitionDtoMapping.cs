﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Reporting;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportDefinitionToReportDefinitionDtoMapping : ClassMapping<ReportDefinition, ReportDefinitionDto>
    {
        public ReportDefinitionToReportDefinitionDtoMapping()
        {
            Mapping = e => new ReportDefinitionDto
            {
                Id = e.Id,
                Code = e.Code,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                Description = new LocalizedString
                {
                    English = e.DescriptionEn,
                    Polish = e.DescriptionPl
                },
                OutputFileNameTemplate = e.OutputFileNameTemplate,
                ReportingEngineIdentifier = e.ReportingEngineIdentifier,
                DataSourceTypeIdentifier = e.DataSourceTypeIdentifier,
                ReportTemplates = e.ReportTemplates.Select(t => new Common.Dto.DocumentDto
                {
                    ContentType = t.ContentType,
                    DocumentName = t.Name,
                    DocumentId = t.Id
                }).ToArray(),
                Area = e.Area,
                Group = e.Group,
                AllowedForProfiles = e.AllowedForProfiles.Select(p => p.Id).ToArray(),
                AllowedForProfileNames = e.AllowedForProfiles.Select(p => p.Name).ToArray(),
                Timestamp = e.Timestamp
            };
        }
    }
}
