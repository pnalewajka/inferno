﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportGenerationContext<TParametersDto> : IReportGenerationContext
    {
        public ReportDefinitionDto Definition { get; set; }

        public TParametersDto Parameters { get; set; }

        public Stream Content { get; set; }

        public string RendererIdentifier { get; set; }

        public IDictionary<long, byte[]> TemplateContents { get; set; }

        public Lazy<object> ReportData { get; }

        object IReportGenerationContext.Parameters
        {
            get
            {
                return Parameters;
            }
            set
            {
                Parameters = (TParametersDto)value;
            }
        }

        private readonly IReportDataSource _dataSource;

        public ReportGenerationContext(
            IReportDataSource dataSource,
            TParametersDto parametersDto,
            string rendererIdentifier,
            ReportDefinitionDto definition)
        {
            _dataSource = dataSource;
            Parameters = parametersDto;
            RendererIdentifier = rendererIdentifier;
            ReportData = new Lazy<object>(() => _dataSource.GetData(this));
            Definition = definition;
        }

        public ActionResult Render()
        {
            var renderer = ReflectionHelper.CreateInstanceByIdentifier<IReportRenderer>(RendererIdentifier);

            return renderer.Render(this);
        }

        public string GetTemplate(Func<DocumentDto, bool> documentPredicate)
        {
            var document = Definition.ReportTemplates.First(documentPredicate);
            var documentBytes = TemplateContents[document.DocumentId.Value];
            var documentContent = Encoding
                .UTF8.GetString(documentBytes)
                .TrimStart(Encoding.UTF8.GetChars(Encoding.UTF8.GetPreamble()));

            return documentContent;
        }

        public void AddOrUpdateTemplateFile(DocumentDto document, string content)
        {
            var documentIndex = Array.FindIndex(Definition.ReportTemplates, t => t.DocumentId == document.DocumentId);

            if (documentIndex > -1)
            {
                Definition.ReportTemplates[documentIndex] = document;
            }
            else
            {
                document.DocumentId = Definition.ReportTemplates.Select(t => t.DocumentId).DefaultIfEmpty(0).Max() + 1;
                Definition.ReportTemplates = Definition.ReportTemplates.Concat(new[] { document }).ToArray();
            }

            TemplateContents[(long)document.DocumentId] = Encoding.UTF8.GetBytes(content);
        }

        public void Dispose()
        {
            if (Content != null)
            {
                Content.Dispose();
            }
        }
    }

    public interface IReportGenerationContext : IDisposable
    {
        ReportDefinitionDto Definition { get; }

        object Parameters { get; set; }

        Stream Content { get; set; }

        string RendererIdentifier { get; }

        IDictionary<long, byte[]> TemplateContents { get; set; }

        Lazy<object> ReportData { get; }

        ActionResult Render();
    }
}