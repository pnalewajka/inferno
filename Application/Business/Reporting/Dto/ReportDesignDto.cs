﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportDesignDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public LocalizedString Name { get; set; }

        public string ManifestContent { get; set; }

        public string SqlContent { get; set; }

        public string ConfigContent { get; set; }
    }
}