﻿using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Reporting.Dto
{
    public class ReportResultDto : IArchivable
    {
        public string ContentType { get; set; }

        public byte[] Content { get; set; }

        public string FileName { get; set; }
    }
}
