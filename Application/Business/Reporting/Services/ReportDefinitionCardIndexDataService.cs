﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.DocumentMapping;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reporting.Services
{
    public class ReportDefinitionCardIndexDataService : CardIndexDataService<ReportDefinitionDto, ReportDefinition, IReportingDbScope>, IReportDefinitionCardIndexDataService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly ICacheInvalidationService _cacheInvalidationService;

        private static readonly IDictionary<string, SecurityRoleType?> RequiredRoles = new ConcurrentDictionary<string, SecurityRoleType?>();

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ReportDefinitionCardIndexDataService(
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            ICardIndexServiceDependencies<IReportingDbScope> dependencies)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _cacheInvalidationService = dependencies.CacheInvalidationService;
        }

        public override CardIndexRecords<ReportDefinitionDto> GetRecords(QueryCriteria criteria)
        {
            var records = base.GetRecords(criteria);

            AddRequiredRoleInformation(records);

            return records;
        }

        protected override IEnumerable<Expression<Func<ReportDefinition, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Code))
            {
                yield return r => r.Code;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return r => r.NameEn;
                yield return r => r.NamePl;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Description))
            {
                yield return r => r.DescriptionEn;
                yield return r => r.DescriptionPl;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Area))
            {
                yield return r => r.Area;
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<ReportDefinition, ReportDefinitionDto, IReportingDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                eventArgs.UnitOfWork,
                eventArgs.InputEntity.ReportTemplates,
                eventArgs.InputDto.ReportTemplates,
                rt => new ReportTemplate
                {
                    Name = rt.DocumentName,
                    ContentType = rt.ContentType
                });

            eventArgs.InputEntity.AllowedForProfiles = EntityMergingService.CreateEntitiesFromIds<SecurityProfile, IReportingDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.AllowedForProfiles.GetIds());
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<ReportDefinition, ReportDefinitionDto> recordAddedEventArgs)
        {
            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<ReportTemplate, ReportTemplateContent, ReportTemplateDocumentMapping, IReportingDbScope>(
                recordAddedEventArgs.InputDto.ReportTemplates);

            _cacheInvalidationService.InvalidateArea(CacheAreas.ReportDefinitions);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<ReportDefinition, ReportDefinitionDto, IReportingDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                eventArgs.UnitOfWork,
                eventArgs.DbEntity.ReportTemplates,
                eventArgs.InputDto.ReportTemplates,
                rt => new ReportTemplate
                {
                    Name = rt.DocumentName,
                    ContentType = rt.ContentType
                });

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.AllowedForProfiles, eventArgs.InputEntity.AllowedForProfiles);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<ReportDefinition, ReportDefinitionDto> recordEditedEventArgs)
        {
            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<ReportTemplate, ReportTemplateContent, ReportTemplateDocumentMapping, IReportingDbScope>(
                recordEditedEventArgs.InputDto.ReportTemplates);

            _cacheInvalidationService.InvalidateArea(CacheAreas.ReportDefinitions);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<ReportDefinition, IReportingDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);
            eventArgs.UnitOfWork.Repositories.ReportTemplates.DeleteEach(p => p.ReportDefinitionId == eventArgs.DeletingRecord.Id);

            _cacheInvalidationService.InvalidateArea(CacheAreas.ReportDefinitions);
        }

        private static void AddRequiredRoleInformation(CardIndexRecords<ReportDefinitionDto> records)
        {
            foreach (var record in records.Rows)
            {
                var sourceIdentifier = record.DataSourceTypeIdentifier;

                if (!RequiredRoles.ContainsKey(sourceIdentifier))
                {
                    var requiredRole = ReportingService.GetReportRequiredRole(sourceIdentifier);
                    RequiredRoles[sourceIdentifier] = requiredRole;
                }

                record.RequiredRole = RequiredRoles[sourceIdentifier];
            }
        }
    }
}
