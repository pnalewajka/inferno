﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using Smt.Atomic.Business.Reporting.Exceptions;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reporting.Services
{
    public class ReportRegistrationService : IReportRegistrationService
    {
        private readonly IUnitOfWorkService<IReportingDbScope> _unitOfWorkService;
        private readonly ICacheInvalidationService _cacheInvalidationService;

        public ReportRegistrationService(
            IUnitOfWorkService<IReportingDbScope> unitOfWorkService,
            ICacheInvalidationService cacheInvalidationService)
        {
            _unitOfWorkService = unitOfWorkService;
            _cacheInvalidationService = cacheInvalidationService;
        }

        private static Dictionary<Assembly, string[]> GetResourceAssemblyInfoes()
        {
            var assemblyInfoes = new Dictionary<Assembly, string[]>();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (var assembly in assemblies)
            {
                string[] resourceNames;

                try
                {
                    resourceNames = assembly.GetManifestResourceNames();
                }
                catch (Exception)
                {
                    continue;
                }

                assemblyInfoes.Add(assembly, resourceNames);
            }

            return assemblyInfoes;
        }

        private static IEnumerable<Type> GetReportDataSources()
        {
            return TypeHelper.GetLoadedTypes()
                .Where(t => t.GetCustomAttributes<DefaultReportDefinitionAttribute>().Any());
        }

        private static byte[] ReadReportTemplateFromResource(Assembly assembly, string reportTemplateResourceName)
        {
            using (var stream = assembly.GetManifestResourceStream(reportTemplateResourceName))
            {
                return StreamHelper.ReadBytes(stream);
            }
        }

        private static Assembly GetAssemblyByResourceName(Dictionary<Assembly, string[]> assemblyInfoes, string reportTemplateResourceName, string dataSourceTypeName)
        {
            var assemblyInfo = assemblyInfoes
                .Where(a => a.Value.Contains(reportTemplateResourceName))
                .ToList();

            if (assemblyInfo == null || assemblyInfo.Count < 1)
            {
                throw new ReportTemplateException($"Automatic report registration: Cannot find report template {reportTemplateResourceName} for data source {dataSourceTypeName}");
            }

            if (assemblyInfo.Count > 1)
            {
                throw new ReportTemplateException($"Automatic report registration: More than one report template found {reportTemplateResourceName} for data source {dataSourceTypeName}");
            }

            return assemblyInfo.First().Key;
        }

        private static ReportTemplate CreateReportTemplate(Dictionary<Assembly, string[]> assemblyInfoes, string reportTemplateResourceName, string dataSourceTypeName)
        {
            var assembly = GetAssemblyByResourceName(assemblyInfoes, reportTemplateResourceName, dataSourceTypeName);
            var reportTemplateContent = ReadReportTemplateFromResource(assembly, reportTemplateResourceName);

            var reportTemplate = new ReportTemplate
            {
                Name = GetReportTemplateName(reportTemplateResourceName),
                ContentType = MimeMapping.GetMimeMapping(reportTemplateResourceName),
                ContentLength = reportTemplateContent.Length,
                DocumentContent = new ReportTemplateContent
                {
                    Content = reportTemplateContent,
                }
            };

            return reportTemplate;
        }

        private static string GetReportTemplateName(string reportTemplateResourceName)
        {
            var resourceNameWithoutExtension = reportTemplateResourceName.Substring(0, reportTemplateResourceName.LastIndexOf('.'));
            var fileNameIndex = resourceNameWithoutExtension.LastIndexOf('.') + 1;

            return reportTemplateResourceName.Substring(fileNameIndex);
        }

        private static ReportDefinition CreateReportDefinition(DefaultReportDefinitionAttribute attribute, Type dataSourceType)
        {
            var dataSourceTypeIdentifier = AttributeHelper.GetClassAttribute<IdentifierAttribute>(dataSourceType);

            if (dataSourceTypeIdentifier == null)
            {
                throw new ReportDefinitionRegistrationException($"Data source not registered: {dataSourceType.FullName}");
            }

            return new ReportDefinition
            {
                Code = attribute.Code,
                NameEn = attribute.GetName(CultureInfo.GetCultureInfo(CultureCodes.English)),
                NamePl = attribute.GetName(CultureInfo.GetCultureInfo(CultureCodes.Polish)),
                DescriptionEn = attribute.GetDescription(CultureInfo.GetCultureInfo(CultureCodes.English)),
                DescriptionPl = attribute.GetDescription(CultureInfo.GetCultureInfo(CultureCodes.Polish)),
                ReportingEngineIdentifier = attribute.ReportingEngineIdentifier,
                OutputFileNameTemplate = attribute.FileName,
                DataSourceTypeIdentifier = dataSourceTypeIdentifier.Value,
                ReportTemplates = new HashSet<ReportTemplate>(),
                Area = attribute.Area,
                Group = attribute.Group
            };
        }

        public void RegisterNewReports()
        {
            var reportDefinitionInfoes =
                from dataSourceType in GetReportDataSources()
                from attribute in dataSourceType.GetCustomAttributes<DefaultReportDefinitionAttribute>()
                select new
                {
                    ReportDefinitionCode = attribute.Code,
                    DataSourceType = dataSourceType,
                    DataSourceTypeName = dataSourceType.FullName,
                    RegistrationInfo = attribute,
                    ReportTemplateResourceNames = attribute.ReportTemplateResources
                };

            var assemblyInfoes = GetResourceAssemblyInfoes();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var existingReportDefinitonCodes = unitOfWork.Repositories.ReportDefinitions.Select(rd => rd.Code).ToList();

                foreach (var reportDefinitionInfo in reportDefinitionInfoes)
                {
                    if (existingReportDefinitonCodes.Any(code => code == reportDefinitionInfo.ReportDefinitionCode))
                    {
                        var existingReportDefinitions = unitOfWork.Repositories.ReportDefinitions.Where(x =>
                            x.Code == reportDefinitionInfo.ReportDefinitionCode);

                        unitOfWork.Repositories.ReportTemplates.DeleteRange(existingReportDefinitions.SelectMany(x => x.ReportTemplates));
                        unitOfWork.Repositories.ReportDefinitions.DeleteRange(existingReportDefinitions);
                    }

                    var reportDefinition = CreateReportDefinition(reportDefinitionInfo.RegistrationInfo, reportDefinitionInfo.DataSourceType);

                    foreach (var reportTemplateResourceName in reportDefinitionInfo.ReportTemplateResourceNames)
                    {
                        var reportTemplate = CreateReportTemplate(
                            assemblyInfoes,
                            reportTemplateResourceName,
                            reportDefinitionInfo.DataSourceTypeName);

                        reportDefinition.ReportTemplates.Add(reportTemplate);
                    }

                    unitOfWork.Repositories.ReportDefinitions.Add(reportDefinition);
                }

                unitOfWork.Commit();
            }

            _cacheInvalidationService.InvalidateArea(CacheAreas.ReportDefinitions);
        }
    }
}
