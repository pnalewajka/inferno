﻿using System;
using System.Linq;
using System.Reflection;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Exceptions;
using Smt.Atomic.Business.Reporting.Helpers;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reporting.Services
{
    public class ReportingService : IReportingService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUnitOfWorkService<IReportingDbScope> _unitOfWorkService;
        private readonly IClassMapping<ReportDefinition, ReportDefinitionDto> _reportDefinitionMapping;

        public ReportingService(
            IPrincipalProvider principalProvider,
            IUnitOfWorkService<IReportingDbScope> unitOfWorkService,
            IClassMapping<ReportDefinition, ReportDefinitionDto> reportDefinitionMapping)
        {
            _principalProvider = principalProvider;
            _unitOfWorkService = unitOfWorkService;
            _reportDefinitionMapping = reportDefinitionMapping;
        }

        private object GetDtoFromParameters(object parameters, Type dataSourceType)
        {
            var providedParametersType = parameters.GetType();
            var expectedParametersType = dataSourceType.GetInterfaces()
                .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IReportDataSource<>))
                .Select(i => i.GetGenericArguments().Single())
                .SingleOrDefault();

            if (expectedParametersType != null && expectedParametersType.IsAssignableFrom(providedParametersType))
            {
                return parameters;
            }

            var classMapper = GetClassMapperForType(providedParametersType);

            return classMapper.CreateFromSource(parameters);
        }

        private static IClassMapping GetClassMapperForType(Type type)
        {
            var classMapperType = GetClassMapperTypeForType(type);

            return ReflectionHelper.CreateInstance<IClassMapping>(classMapperType);
        }

        private static Type GetClassMapperTypeForType(Type type)
        {
            var mappers = TypeHelper.GetLoadedTypes().Where(t => TypeHelper.IsSubclassOfRawGeneric(t, typeof(ClassMapping<,>)));
            var classMapperTypes = mappers.Where(t => TypeHelper.GetRawGenericArguments(t, typeof(ClassMapping<,>)).FirstOrDefault() == type).ToList();

            if (classMapperTypes == null || classMapperTypes.Count != 1)
            {
                throw new ReportExecutionException($"ClassMapping not found for type: {(type).FullName}");
            }

            return classMapperTypes.Single();
        }

        private IReportGenerationContext CreateReportGenerationContext(IReportDataSource reportDataSource, object parametersDto, string rendererIdentifier, ReportDefinition reportDefinition)
        {
            var reportDefinitionDto = _reportDefinitionMapping.CreateFromSource(reportDefinition);
            var parametersDtoType = parametersDto.GetType();
            var reportGenerationContextType = typeof(ReportGenerationContext<>).MakeGenericType(parametersDtoType);
            var reportGenerationContext = (IReportGenerationContext)reportGenerationContextType
                .GetConstructor(new Type[] { typeof(IReportDataSource), parametersDtoType, typeof(string), typeof(ReportDefinitionDto) })
                .Invoke(new object[] { reportDataSource, parametersDto, rendererIdentifier, reportDefinitionDto });

            reportGenerationContext.TemplateContents = reportDefinition.ReportTemplates.ToDictionary(t => t.Id, t => t.DocumentContent.Content);

            return reportGenerationContext;
        }

        private object GetReportData(ReportDefinition reportDefinition, object parameters)
        {
            var dataSource = ReflectionHelper.CreateInstanceByIdentifier<IReportDataSource>(reportDefinition.DataSourceTypeIdentifier);
            var parametersDto = GetDtoFromParameters(parameters, dataSource.GetType());

            CheckPermissions(reportDefinition, dataSource, parametersDto);

            var reportGenerationContext = CreateReportGenerationContext(dataSource, parametersDto, null, reportDefinition);

            return reportGenerationContext.ReportData;
        }

        private IReportGenerationContext GenerateReport(ReportDefinition reportDefinition, object parameters)
        {
            var dataSource = ReflectionHelper.CreateInstanceByIdentifier<IReportDataSource>(reportDefinition.DataSourceTypeIdentifier);
            var reportingEngine = ReflectionHelper.CreateInstanceByIdentifier<IReportingEngine>(reportDefinition.ReportingEngineIdentifier);
            var parametersDto = GetDtoFromParameters(parameters, dataSource.GetType());

            CheckPermissions(reportDefinition, dataSource, parametersDto);

            var reportGenerationContext = CreateReportGenerationContext(dataSource, parametersDto, reportingEngine.RendererIdentifier, reportDefinition);
            reportGenerationContext.Content = reportingEngine.GenerateContent(reportGenerationContext);

            return reportGenerationContext;
        }

        private object GetReportViewModel(ReportDefinition reportDefinition)
        {
            var dataSource = GetReportDataSource(reportDefinition);
            var defaultParametersMethod = GetDefaultParametersMethodInfo(dataSource);

            var parametersDtoType = GetParametersDtoTypeFromDefaultParametersMethod(defaultParametersMethod);
            var reportGenerationContext = CreateReportGenerationContext((IReportDataSource)dataSource, Activator.CreateInstance(parametersDtoType), null, reportDefinition);

            var paramsDto = defaultParametersMethod.Invoke(dataSource, new object[] { reportGenerationContext });
            var toViewModelClassMapper = GetClassMapperForType(parametersDtoType);

            return toViewModelClassMapper.CreateFromSource(paramsDto);
        }

        private Type GetReportViewModelType(ReportDefinition reportDefinition)
        {
            var dataSource = GetReportDataSource(reportDefinition);
            var defaultParametersMethod = GetDefaultParametersMethodInfo(dataSource);

            var parametersDtoType = GetParametersDtoTypeFromDefaultParametersMethod(defaultParametersMethod);
            var toViewModelClassMapperType = GetClassMapperTypeForType(parametersDtoType);

            return TypeHelper.GetRawGenericArguments(toViewModelClassMapperType, typeof(ClassMapping<,>)).LastOrDefault();
        }

        private Type GetParametersDtoTypeFromDefaultParametersMethod(MethodInfo defaultParametersMethod)
        {
            var defaultParametersMethodParameter = defaultParametersMethod.GetParameters().First();

            return defaultParametersMethodParameter.ParameterType.GetGenericArguments().Single();
        }

        private MethodInfo GetDefaultParametersMethodInfo(object dataSource)
        {
            var dataSourceType = dataSource.GetType();

            return dataSourceType.GetMethod(nameof(IReportDataSource<object>.GetDefaultParameters));
        }

        private object GetReportDataSource(ReportDefinition reportDefinition)
        {
            var dataSource = ReflectionHelper.CreateInstanceByIdentifier<object>(reportDefinition.DataSourceTypeIdentifier);
            CheckPermissions(reportDefinition, (IReportDataSource)dataSource);

            return dataSource;
        }

        internal static SecurityRoleType? GetReportRequiredRole(string dataSourceIdentifier)
        {
            var dataSourceType = IdentifierHelper.GetTypeByIdentifier(dataSourceIdentifier);
            var requireRoleAttribute = AttributeHelper.GetClassAttribute<RequireRoleAttribute>(dataSourceType);

            var requiredRole = requireRoleAttribute?.Role;

            return requiredRole;
        }

        public object GetReportViewModel(string reportCode)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(r => r.Code == reportCode);

                return GetReportViewModel(reportDefinition);
            }
        }

        public Type GetReportViewModelType(string reportCode)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(r => r.Code == reportCode);

                return GetReportViewModelType(reportDefinition);
            }
        }

        public IReportGenerationContext PerformReport(string reportCode, object parameters)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(r => r.Code == reportCode);

                return GenerateReport(reportDefinition, parameters);
            }
        }

        public object GetReportData(string reportCode, object parameters)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(r => r.Code == reportCode);

                return GetReportData(reportDefinition, parameters);
            }
        }

        public LocalizedString GetReportName(string reportCode)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(r => r.Code == reportCode);

                return new LocalizedString
                {
                    English = reportDefinition.NameEn,
                    Polish = reportDefinition.NamePl
                };
            }
        }

        public void CheckPermissions(ReportDefinition reportDefinition, IReportDataSource dataSource, object parametersDto = null)
        {
            if (parametersDto != null)
            {
                RequiredRolesHelper.CheckRolesForReportExecution(_principalProvider.Current, dataSource, parametersDto);
            }
            else
            {
                RequiredRolesHelper.CheckRolesForReportConfiguration(_principalProvider.Current, dataSource);
            }

            var allowedForProfileIds = reportDefinition.AllowedForProfiles.Select(p => p.Id);
            var currentUserId = _principalProvider.Current.Id;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var availableProfileIds = unitOfWork.Repositories.SecurityProfiles.Where(p => p.Users.Any(u => u.Id == currentUserId)).Select(p => p.Id);
               
                if (allowedForProfileIds.Any() && availableProfileIds.All(x => !allowedForProfileIds.Contains(x)))
                {
                    throw new MissingAllowedForProfileException();
                }
            }
        }
    }
}
