﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Reporting.DocumentMapping;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reporting.Services
{
    public class ReportDesignService : IReportDesignService
    {
        private const string ManifestSuffix = ".manifest";
        private const string SqlSuffix = ".sql";
        private const string ConfigSuffix = "-config.js";

        private readonly IUnitOfWorkService<IReportingDbScope> _unitOfWorkService;
        private readonly ITemporaryDocumentService _temporaryDocumentService;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;

        public ReportDesignService(IUnitOfWorkService<IReportingDbScope> unitOfWorkService,
            ITemporaryDocumentService temporaryDocumentService,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService)
        {
            _unitOfWorkService = unitOfWorkService;
            _temporaryDocumentService = temporaryDocumentService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
        }

        public ReportDesignDto GetReportDesignById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(rd => rd.Id == id);

                return GetReportDesignDto(reportDefinition);
            }
        }

        public void UpdateReportDesign(ReportDesignDto reportDesign)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reportDefinition = unitOfWork.Repositories.ReportDefinitions.Single(rd => rd.Id == reportDesign.Id);

                var temporaryDocuments = CreateTemplates(reportDefinition, reportDesign);

                _uploadedDocumentHandlingService.MergeDocumentCollections(unitOfWork, reportDefinition.ReportTemplates, temporaryDocuments, d => new ReportTemplate
                {
                    Name = d.DocumentName,
                    ContentType = d.ContentType
                });

                unitOfWork.Commit();

                _uploadedDocumentHandlingService
                    .PromoteTemporaryDocuments<ReportTemplate, ReportTemplateContent, ReportTemplateDocumentMapping, IReportingDbScope>(temporaryDocuments);
            }
        }

        private ReportDesignDto GetReportDesignDto(ReportDefinition reportDefinition)
        {
            return new ReportDesignDto
            {
                Id = reportDefinition.Id,
                Code = reportDefinition.Code,
                Name = new LocalizedString { English = reportDefinition.NameEn, Polish = reportDefinition.NamePl },
                ManifestContent = GetDocumentContent(reportDefinition, ManifestSuffix),
                SqlContent = GetDocumentContent(reportDefinition, SqlSuffix),
                ConfigContent = GetDocumentContent(reportDefinition, ConfigSuffix),
            };
        }

        private string GetDocumentContent(ReportDefinition definition, string documentExtension)
            => Encoding.UTF8.GetString(definition.ReportTemplates.SingleOrDefault(rt => rt.Name.EndsWith(documentExtension))?.DocumentContent?.Content ?? new Byte[0]);

        private IEnumerable<DocumentDto> CreateTemplates(ReportDefinition reportDefinition, ReportDesignDto reportDesign)
        {
            var temporaryDocuments = new List<DocumentDto>();

            temporaryDocuments.Add(CreateTemplate(reportDefinition, ManifestSuffix, MimeHelper.BinaryFile, reportDesign.ManifestContent));
            temporaryDocuments.Add(CreateTemplate(reportDefinition, SqlSuffix, MimeHelper.BinaryFile, reportDesign.SqlContent));
            temporaryDocuments.Add(CreateTemplate(reportDefinition, ConfigSuffix, MimeHelper.JavaScriptFile, reportDesign.ConfigContent));

            return temporaryDocuments;
        }

        private DocumentDto CreateTemplate(ReportDefinition reportDefinition, string documentSuffix, string defaultContentType, string content)
        {
            var template = reportDefinition.ReportTemplates.SingleOrDefault(rt => rt.Name.EndsWith(documentSuffix));
            var templateName = template?.Name ?? $"{reportDefinition.Code}{documentSuffix}";
            var contentType = template?.ContentType ?? defaultContentType;

            return CreateTemporaryDocument(templateName, contentType, content);
        }

        private DocumentDto CreateTemporaryDocument(string documentName, string contentType, string content)
        {
            var contentBytes = Encoding.UTF8.GetBytes(content);
            Guid documentGuid;

            using (var stream = new MemoryStream(contentBytes))
            {
                documentGuid = _temporaryDocumentService.CreateTemporaryDocument(
                    documentName,
                    contentType,
                    contentBytes.LongLength,
                    null);

                _temporaryDocumentService.AddChunk(documentGuid, stream);
                _temporaryDocumentService.CompleteTemporaryDocument(documentGuid);
            }

            return new DocumentDto
            {
                TemporaryDocumentId = documentGuid,
                ContentType = contentType,
                DocumentName = documentName
            };
        }
    }
}