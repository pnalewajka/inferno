﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Xml.Linq;

namespace Smt.Atomic.Business.Reporting.Services
{
    [Identifier("DataSource.SqlBasedDataSource")]
    public class SqlBasedDataSource : BaseReportDataSource<DynamicDto>
    {
        private readonly IUnitOfWorkService<IDbScope> _unitOfWork;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public SqlBasedDataSource(
            IUnitOfWorkService<IDbScope> unitOfWork,
            IPrincipalProvider principalProvider,
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
        {
            _unitOfWork = unitOfWork;
            _principalProvider = principalProvider;
            _unitOfWorkService = unitOfWorkService;
        }

        public override object GetData(ReportGenerationContext<DynamicDto> reportGenerationContext)
        {
            var sqlQuery = reportGenerationContext.GetTemplate(t => t.DocumentName.EndsWith(".sql", StringComparison.InvariantCultureIgnoreCase));
            var parameters = reportGenerationContext.Parameters.Data;

            return ExecuteSqlQuery(sqlQuery, parameters);
        }

        public override DynamicDto GetDefaultParameters(ReportGenerationContext<DynamicDto> reportGenerationContext)
        {
            var manifest = reportGenerationContext.GetTemplate(t => t.DocumentName.EndsWith(".manifest", StringComparison.InvariantCultureIgnoreCase));
            var manifestXDocument = XDocument.Parse(manifest);
            string[] requiredRoleNames = ExtractCommaSeparatedValues(GetInnerNodeAsStringOrDefault(manifestXDocument, "RequiredRoles")); 
            string[] allowedForProfileNames = ExtractCommaSeparatedValues(GetInnerNodeAsStringOrDefault(manifestXDocument, "AllowedForProfiles"));

            if (requiredRoleNames.Any(r => !_principalProvider.Current.IsInRole(r)))
            {
                throw new MissingRequiredRoleException(requiredRoleNames, true);
            }

            var currentUserProfiles = GetUserProfiles(_principalProvider.Current.Id.Value);

            if (allowedForProfileNames.Any() && currentUserProfiles.All(p => !allowedForProfileNames.Contains(p)))
            {
                throw new MissingAllowedForProfileException();
            }

            string itemsDefinition = GetInnerNodeAsStringOrDefault(manifestXDocument, "Parameters");
            var definition = $"<DynamicModelItems xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.datacontract.org/2004/07/Smt.Atomic.Presentation.Renderers.Models.Dynamic\">" +
                $"{itemsDefinition}" +
                $"</DynamicModelItems >";

            return new DynamicDto(definition);
        }

        private static string[] ExtractCommaSeparatedValues(string valueAsString)
        {
            return (valueAsString ?? string.Empty).Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        private static string GetInnerNodeAsStringOrDefault(XDocument manifestXDocument, string nodeName)
        {
            XElement element = manifestXDocument.Descendants().SingleOrDefault(p => p.Name.LocalName == nodeName);

            if (element == null)
            {
                return null;
            }

            var definitionReader = element.CreateReader();
            definitionReader.MoveToContent();
            var content = definitionReader.ReadInnerXml().Trim();

            return content;
        }

        private HashSet<string> GetUserProfiles(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);

                return new HashSet<string>(user.Profiles.Select(p => p.Name));
            }
        }

        private object ExecuteSqlQuery(string sqlQuery, IDictionary<string, object> parameters)
        {
            parameters["@CurrentUserId"] = _principalProvider.Current.Id;

            using (var extendedUnitOfWork = _unitOfWork.CreateExtended())
            {
                var queryResult = extendedUnitOfWork.SqlQueryToDataSet(sqlQuery, parameters);

                dynamic result = new ExpandoObject();
                var resultAsDictionary = ((IDictionary<string, object>)result);
                long? tableCount = null;

                foreach (DataTable dataTable in queryResult.Tables)
                {
                    var list = new List<ExpandoObject>();

                    foreach (DataRow row in dataTable.Rows)
                    {
                        var record = (IDictionary<string, object>)new ExpandoObject();

                        for (int i = 0; i < dataTable.Columns.Count; i++)
                        {
                            DataColumn column = dataTable.Columns[i];
                            record[column.ColumnName] = row[column.ColumnName];
                        }

                        list.Add((ExpandoObject)record);
                    }

                    resultAsDictionary["Records" + tableCount ?? string.Empty] = list;
                    tableCount = (tableCount ?? 0) + 1;
                }

                return result;
            }
        }
    }
}