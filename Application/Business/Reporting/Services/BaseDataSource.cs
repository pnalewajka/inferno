﻿using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;

namespace Smt.Atomic.Business.Reporting.Services
{
    public abstract class BaseReportDataSource<TParametersDto> : IReportDataSource<TParametersDto>
    {
        public abstract object GetData(ReportGenerationContext<TParametersDto> reportGenerationContext);
        public abstract TParametersDto GetDefaultParameters(ReportGenerationContext<TParametersDto> reportGenerationContext);

        public object GetData(IReportGenerationContext reportGenerationContext)
        {
            return GetData((ReportGenerationContext<TParametersDto>)reportGenerationContext);
        }
    }
}
