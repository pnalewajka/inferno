﻿using Smt.Atomic.Business.Reporting.Enums;

namespace Smt.Atomic.Business.Reporting.Services
{
    public class ReportRelatedClassContext
    {
        public ReportRelatedClassType ClassType { get; set; }
    }
}