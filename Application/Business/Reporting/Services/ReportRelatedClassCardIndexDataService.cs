﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Enums;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Reporting.Services
{
    public class ReportRelatedClassCardIndexDataService : CustomDataCardIndexDataService<ReportRelatedClassDto>, IReportRelatedClassCardIndexDataService
    {
        private static IList<ReportRelatedClassDto> _records;

        public override ReportRelatedClassDto GetRecordById(long id)
        {
            return GetConfigurationItems().Single(r => r.Id == id);
        }

        public override ReportRelatedClassDto GetRecordByIdOrDefault(long id)
        {
            return GetConfigurationItems().SingleOrDefault(r => r.Id == id);
        }

        public long GetId(string itemCode)
        {
            return GetConfigurationItems().Single(r => r.Identifier == itemCode).Id;
        }

        public string GetIdentifierOrDefault(long id)
        {
            return GetRecordByIdOrDefault(id)?.Identifier;
        }

        protected override IList<ReportRelatedClassDto> GetAllRecords(object context = null)
        {
            var reportRelatedClassType = GetConfigurationTypeOrDefault(context);

            return GetConfigurationItems(reportRelatedClassType);
        }

        protected override IEnumerable<Expression<Func<ReportRelatedClassDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return dto => dto.Identifier;
        }

        private static ReportRelatedClassType? GetConfigurationTypeOrDefault(object context)
        {
            var reportRelatedClassContext = (ReportRelatedClassContext)context;

            return reportRelatedClassContext?.ClassType;
        }

        private bool IsTypeOf(ReportRelatedClassType? configurationType, Type type)
        {
            switch (configurationType)
            {
                case ReportRelatedClassType.ReportingEngine:
                    return typeof(IReportingEngine).IsAssignableFrom(type);

                case ReportRelatedClassType.DataSource:
                    return type.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IReportDataSource<>));

                default:
                    return true;
            }
        }

        private IList<ReportRelatedClassDto> GetConfigurationItems(ReportRelatedClassType? configurationType = null)
        {
            if (_records == null)
            {
                _records = TypeHelper.GetLoadedTypes()
                                    .Where(type => AttributeHelper.GetClassAttribute<IdentifierAttribute>(type) != null)
                                    .Select(type => new
                                    {
                                        Identifier = AttributeHelper.GetClassAttribute<IdentifierAttribute>(type).Value,
                                        Type = type
                                    })
                                    .OrderBy(identity => identity.Identifier)
                                    .Select((identity, index) => new ReportRelatedClassDto
                                    {
                                        Id = index + 1,
                                        Identifier = identity.Identifier,
                                        ItemType = identity.Type
                                    }).ToList();
            }

            return _records
                .Where(identity => IsTypeOf(configurationType, identity.ItemType))
                .ToList();
        }
    }
}
