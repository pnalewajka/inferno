﻿using System;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions
{
    [ExpressionName(Name = "IsNullOrWhiteSpaceString")]
    public class IsNullOrWhiteSpaceStringExpression : ConditionalExpression
    {
        public IsNullOrWhiteSpaceStringExpression(object argument) 
            : base(argument)
        {
        }

        public override Predicate<object> Condition => argument =>
            argument is string && string.IsNullOrWhiteSpace((string)argument);
    }
}
