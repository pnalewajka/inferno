﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    public class TagProcessorContext
    {
        public object Model { get; }
        public DocumentMap DocumentMap { get; }
        public DocumentBuilder DocumentBuilder { get; }

        public TagProcessorContext(
            object model, 
            DocumentMap documentMap, 
            DocumentBuilder documentBuilder)
        {
            Model = model;
            DocumentMap = documentMap;
            DocumentBuilder = documentBuilder;
        }
    }
}
