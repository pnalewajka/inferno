﻿using Novacode;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions;
using System;
namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag
{
    public class IfTag
    {
        public InsertBeforeOrAfter OpeningElement { get; set; }
        public InsertBeforeOrAfter ClosingElement { get; set; }
        public ConditionalExpression Expression { get; set; }
        public Action ActionToEvaluate { get; set; }

        public void Evaluate()
        {
            if (Expression.IsTrue)
            {
                ActionToEvaluate();
            }
        }
    }
}
