﻿using System;
using System.Collections;
using System.Linq;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions
{
    [ExpressionName(Name = "IsEmptyCollection")]
    public class IsEmptyCollectionExpression : ConditionalExpression
    {
        public IsEmptyCollectionExpression(object argument)
            : base(argument)
        {
        }

        public override Predicate<object> Condition => argument =>
            argument is IEnumerable && !((IEnumerable)argument).Cast<object>().Any();
    }
}