﻿using System;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions
{
    [ExpressionName(Name = "IsFalse")]
    public class IsFalseExpression : ConditionalExpression
    {
        public IsFalseExpression(object argument)
            : base(argument)
        { }

        public override Predicate<object> Condition => arg => arg is bool && (!(bool)arg);
    }
}
