﻿using System.Text.RegularExpressions;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.TagRegexDescriptors
{
    public class IfTagParsingResult
    {
        public string ExpressionName { get; set; }
        public string ExpressionArgumentName { get; set; }

        public IfTagParsingResult(GroupCollection regexGroupingResult)
        {
            ExpressionName = regexGroupingResult[1].Value;
            ExpressionArgumentName = regexGroupingResult[2].Value;
        }
    }
}
