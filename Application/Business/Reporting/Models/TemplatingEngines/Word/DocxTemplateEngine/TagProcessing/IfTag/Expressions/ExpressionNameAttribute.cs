﻿using System;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ExpressionNameAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
