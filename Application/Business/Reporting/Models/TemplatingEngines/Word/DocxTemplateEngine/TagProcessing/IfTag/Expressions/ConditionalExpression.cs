﻿using System;

namespace Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions
{
    public abstract class ConditionalExpression
    {
        protected ConditionalExpression(object argument)
        {
            Argument = argument;
        }

        public abstract Predicate<object> Condition { get; }
        public object Argument { get; set; }
        public bool IsTrue => Condition(Argument);
    }
}
