using System.Collections.Generic;
using System.Linq;
using Novacode;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Reporting.Helpers.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine
{
    public class DocumentMap
    {
        public Dictionary<InsertBeforeOrAfter, ElementDescription> Map { get; private set; }

        public DocumentMap(DocX docX)
        {
            BuildTemplateMap(docX);
        }

        public ElementDescription GetElementDescription(InsertBeforeOrAfter element)
        {
            return Map[element];
        }

        public List<KeyValuePair<InsertBeforeOrAfter, ElementDescription>> GetLoopStartElements(int nestingLevel)
        {
            return Map
                .Where(e => e.Value.ForeachClause != null && e.Value.LoopNestingLevel == nestingLevel)
                .Where(e => e.Key is Paragraph)
                .ToList();
        }

        public Dictionary<InsertBeforeOrAfter, ElementDescription> GetDocumentPart(InsertBeforeOrAfter loopStartElement, InsertBeforeOrAfter loopEndElement)
        {
            return Map
                .SkipWhile(e => e.Key != loopStartElement)
                .Skip(1)
                .TakeWhile(e => e.Key != loopEndElement)
                .ToDictionary(e => e.Key, e => e.Value);
        }

        public void SetDocumentPartDescription(IEnumerable<InsertBeforeOrAfter> elements, ElementDescription elementDescription)
        {
            foreach (InsertBeforeOrAfter element in elements)
            {
                Map[element].LoopIterator = elementDescription.LoopIterator;
                Map[element].LoopIteratorValue = elementDescription.LoopIteratorValue;
                Map[element].LoopProperty = elementDescription.LoopProperty;
            }
        }

        public void AddElement(InsertBeforeOrAfter element, ElementDescription elementDescription)
        {
            Map[element] = elementDescription;
        }

        public ElementDescription GetOuterLoopDescription(KeyValuePair<InsertBeforeOrAfter, ElementDescription> loopStartMap)
        {
            int loopElementIndex = Map.Keys.ToList().IndexOf(loopStartMap.Key);

            return Map
                .Take(loopElementIndex)
                .Reverse()
                .SkipWhile(e => e.Value.LoopNestingLevel != loopStartMap.Value.LoopNestingLevel - 1)
                .Take(1)
                .Single().Value;
        }

        public InsertBeforeOrAfter FindMatchingEndForeachElement(KeyValuePair<InsertBeforeOrAfter, ElementDescription> foreachElement)
        {
            var nestingLevel = foreachElement.Value.LoopNestingLevel;

            return Map
                .SkipWhile(e => e.Key != foreachElement.Key)
                .SkipWhile(e => !(e.Value.IsEndForeach && e.Value.LoopNestingLevel == nestingLevel - 1))
                .First()
                .Key;
        }

        public InsertBeforeOrAfter FindMatchingEndIfElement(KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap)
        {
            return Map
                .SkipWhile(e => e.Key != elementMap.Key)
                .SkipWhile(e => !(IsEndIfElement(e)))
                .Take(1)
                .Single()
                .Key;
        }

        private bool IsEndIfElement(KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap)
        {
            var paragraph = elementMap.Key as Paragraph;

            return paragraph != null
                   && IfTagProcessor.IsClosingIfTagRegexMatch(paragraph.Text);
        }

        public void BuildTemplateMap(DocX docX)
        {
            var result = new Dictionary<InsertBeforeOrAfter, ElementDescription>();
            IEnumerable<Paragraph> paragraphsOutsideTables = docX.Paragraphs
                .Where(p => p.ParentContainer == ContainerType.Body || p.ParentContainer == ContainerType.None);
            int currentNestingLevel = 0;

            foreach (Paragraph paragraph in paragraphsOutsideTables)
            {
                ElementDescription elementDescription = BuildElementDescription(paragraph, currentNestingLevel);
                currentNestingLevel = elementDescription.LoopNestingLevel;
                result[paragraph] = elementDescription;

                if (paragraph.FollowingTable != null)
                {
                    var tableElementDescription = new ElementDescription {LoopNestingLevel = currentNestingLevel};
                    result[paragraph.FollowingTable] = tableElementDescription;
                }
            }

            Map = result;
        }

        private static ElementDescription BuildElementDescription(Paragraph paragraph, int nestingLevel)
        {
            var elementDescription = new ElementDescription();
            var match = ForEachTagProcessor.GetOpeningForEachRegexMatch(paragraph.Text);

            if (match != null)
            {
                elementDescription = new ElementDescription(match.Groups) {LoopNestingLevel = nestingLevel + 1};
            }           
            else if (ForEachTagProcessor.IsClosingForEachRegexMatch(paragraph.Text))
            {
                elementDescription.IsEndForeach = true;
                elementDescription.LoopNestingLevel = nestingLevel - 1;
            }
            else
            {
                elementDescription.LoopNestingLevel = nestingLevel;
            }

            return elementDescription;
        }

        public void MarkForRemoval(Dictionary<InsertBeforeOrAfter, ElementDescription> documentPart)
        {
            foreach (ElementDescription templateElementDescription in documentPart.Values)
            {
                templateElementDescription.MarkedForRemoval = true;
            }
        }

        public void MarkForRemoval(InsertBeforeOrAfter element)
        {
            Map[element].MarkedForRemoval = true;
        }

        public void RemoveMarkedElements()
        {
            var elementsMarkedForRemoval = Map.Where(e => e.Value.MarkedForRemoval).Select(m => m.Key).ToList();

            foreach (var element in elementsMarkedForRemoval)
            {
                DocumentBuilder.RemoveElement(element);
                Map.Remove(element);
            }
        }
    }
}