using System.Collections.Generic;
using System.IO;
using System.Linq;
using Novacode;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine
{
    public class DocxTemplateEngine : IDocxTemplateEngine
    {
        private readonly IReadOnlyCollection<TagProcessor> _tagProcessors;

        public DocxTemplateEngine(TagProcessor[] tagProcessors)
        {
            _tagProcessors = tagProcessors;
        }

        public byte[] GenerateDocumentBytes(string templatePath, object viewModel)
        {
            MemoryStream memoryStream = GenerateDocumentStream(templatePath, viewModel);

            return memoryStream.ToArray();
        }

        public MemoryStream GenerateDocumentStream(Stream templateStream, object viewModel)
        {
            using (var docX = DocX.Load(templateStream))
            {
                return GenerateDocumentStream(viewModel, docX);
            }
        }

        public MemoryStream GenerateDocumentStream(string templatePath, object viewModel)
        {
            using (var docX = DocX.Load(templatePath))
            {
                return GenerateDocumentStream(viewModel, docX);
            }
        }

        private MemoryStream GenerateDocumentStream(object viewModel, DocX docX)
        {
            ProcessTemplate(viewModel, docX);

            return SaveAsStream(docX);
        }

        private void ProcessTemplate(object viewModel, DocX docX)
        {
            var documentMap = new DocumentMap(docX);
            var documentBuilder = new DocumentBuilder(documentMap, docX);
            var context = new TagProcessorContext(viewModel, documentMap, documentBuilder);

            ExecuteProcessors(context);
        }

        private void ExecuteProcessors(TagProcessorContext context)
        {
            foreach (var tagProcessor in _tagProcessors.OrderBy(tp => tp.ExecutionOrder))
            {
                tagProcessor.Process(context);
            }
        }

        private MemoryStream SaveAsStream(DocX docX)
        {
            var memoryStream = new MemoryStream();
            docX.SaveAs(memoryStream);

            return memoryStream;
        }
    }
}