﻿using System;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine
{
    public class DocxTemplateException : Exception
    {
        public DocxTemplateException(string message) : base(message)
        {
        }
    }
}