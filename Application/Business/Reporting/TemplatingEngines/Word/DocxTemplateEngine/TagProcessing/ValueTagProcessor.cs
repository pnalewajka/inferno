﻿using Novacode;
using Smt.Atomic.Business.Reporting.Helpers.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    public class ValueTagProcessor : TableAndParagraphTagsProcessor
    {
        private const string RegexValue = @"{{[_a-zA-Z][_a-zA-Z0-9]*(\.[_a-zA-Z][_a-zA-Z0-9]*)*}}";
        private static string OpenMarkup = "{{";
        private static string CloseMarkup = "}}";
        private static string CollectionPropertySeperator = ".";

        public override int ExecutionOrder => 2;

        protected override void ProcessTable(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Table table)
        {
            table.Paragraphs.ForEach(x => ReplaceTagsWithData(context, x, elementMap));
        }

        protected override void ProcessParagrah(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Paragraph paragraph)
        {
            ReplaceTagsWithData(context, paragraph, elementMap);
        }

        private void ReplaceTagsWithData(TagProcessorContext context, Paragraph paragraph, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap)
        {
            var patternMatches = Regex.Matches(paragraph.Text, RegexValue);

            foreach (Match match in patternMatches)
            {
                ReplaceTagValue(context, paragraph, elementMap, match);
            }
        }

        private void ReplaceTagValue(TagProcessorContext context, Paragraph paragraph, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Match match)
        {
            var tagValue = GetTagValue(context, elementMap, match);
            paragraph.ReplaceText(match.Value, tagValue);
        }

        private string GetTagValue(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Match match)
        {
            var tagValue = elementMap.Value.IsLoopIterator
                ? TagValueHelper.GetLoopTagValue<object>(elementMap.Value, match.Value)
                : TagValueHelper.GetTagValue<object>(context.Model, match.Value);

            return tagValue?.ToString() ?? string.Empty;
        }

        public static bool IsLoopIteratorProperty(string tagText, string propertyName)
        {
            return tagText.StartsWith($"{propertyName}{CollectionPropertySeperator}");
        }

        public static string GetExpressionFromTag(string tagText)
        {
            var openMarkupLength = OpenMarkup.Length;
            var openAndCloseMarkupsLength = openMarkupLength + CloseMarkup.Length;

            return tagText.Substring(
                openMarkupLength,
                tagText.Length - openAndCloseMarkupsLength);
        }

        public static string CreateValueTag(string propertyName)
        {
            return $"{OpenMarkup}{propertyName}{CloseMarkup}";
        }

        public static string GetLoopIteratorProperty(string tagText, string loopIteratorName)
        {
            return tagText.Substring(loopIteratorName.Length + CollectionPropertySeperator.Length);
        }

    }
}
