﻿using Novacode;
using Smt.Atomic.Business.Reporting.Helpers.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    public class ForEachTagProcessor : TagProcessor
    {
        public override int ExecutionOrder => 1;        
        private const string OpeningTagRegexValue = @"^{{foreach ([_a-zA-Z][_a-zA-Z0-9]*) in (([_a-zA-Z][_a-zA-Z0-9]*)(\.[_a-zA-Z][_a-zA-Z0-9]*)*)}}$";
        private const string ClosingTagRegexValue = @"^{{end foreach}}$";

        public override void Process(TagProcessorContext context)
        {
            for (var i = 1; ; i++)
            {
                var loopStartElements = context.DocumentMap.GetLoopStartElements(i);

                if (!loopStartElements.Any())
                {
                    break;
                }

                foreach (var loopStartElement in loopStartElements)
                {
                    ProcessForeachClause(context, loopStartElement);
                }
            }

            context.DocumentMap.RemoveMarkedElements();
        }

        private void ProcessForeachClause(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> loopStartMap)
        {
            var paragraph = loopStartMap.Key as Paragraph;

            if (paragraph?.ParentContainer == ContainerType.Table)
            {
                throw new DocxTemplateException("Foreach is supported outside tables only");
            }

            var loopEndElement = context.DocumentMap.FindMatchingEndForeachElement(loopStartMap);
            var documentPart = context.DocumentMap.GetDocumentPart(loopStartMap.Key, loopEndElement);
            var loopCollection = GetLoopCollection(context, loopStartMap).ToList();

            if (loopCollection.Any())
            {
                loopStartMap.Value.LoopIteratorValue = loopCollection[0];
                AddLoopDataToDocument(context, loopStartMap, documentPart, loopCollection);
            }
            else
            {
                context.DocumentMap.MarkForRemoval(documentPart);
            }

            context.DocumentMap.MarkForRemoval(loopStartMap.Key);
            context.DocumentMap.MarkForRemoval(loopEndElement);
        }

        private void AddLoopDataToDocument(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> loopStartMap, Dictionary<InsertBeforeOrAfter, ElementDescription> documentPart, IList<object> loopCollection)
        {
            var nonLoopElements = GetNonLoopElements(documentPart);
            context.DocumentMap.SetDocumentPartDescription(nonLoopElements, loopStartMap.Value);
            context.DocumentBuilder.InsertDocumentParts(documentPart, loopCollection);
        }

        public ICollection<object> GetLoopCollection(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> loopStartMap)
        {
            ElementDescription outerLoopStartDescription = context.DocumentMap.GetOuterLoopDescription(loopStartMap);

            return outerLoopStartDescription.LoopIterator != null
                ? GetOuterLoopCollection(loopStartMap, outerLoopStartDescription)
                : TagValueHelper.GetTagCollectionValue<object>(context.Model, loopStartMap.Value.LoopProperty);
        }

        public static ICollection<object> GetOuterLoopCollection(KeyValuePair<InsertBeforeOrAfter, ElementDescription> loopStartMap, ElementDescription outerLoopStartDescription)
        {
            var reducedLoopProperty = ValueTagProcessor.GetLoopIteratorProperty(loopStartMap.Value.LoopProperty, outerLoopStartDescription.LoopIterator);

            return TagValueHelper.GetTagCollectionValue<object>(outerLoopStartDescription.LoopIteratorValue, reducedLoopProperty);
        }

        public static Match GetOpeningForEachRegexMatch(string tagText)
        {
            return GetTagRegexMatch(tagText, OpeningTagRegexValue);
        }

        public static bool IsClosingForEachRegexMatch(string tagText)
        {
            var match = GetTagRegexMatch(tagText, ClosingTagRegexValue);

            return match != null;
        }

        private static IEnumerable<InsertBeforeOrAfter> GetNonLoopElements(Dictionary<InsertBeforeOrAfter, ElementDescription> documentPart)
        {
            return documentPart
                .Where(e => e.Value.ForeachClause == null
                    && !e.Value.IsEndForeach)
                .Select(e => e.Key);
        }
    }
}
