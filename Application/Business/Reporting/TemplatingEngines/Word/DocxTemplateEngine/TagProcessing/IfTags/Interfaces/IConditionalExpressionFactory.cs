﻿using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTags
{
    public interface IConditionalExpressionFactory
    {
        ConditionalExpression Resolve(string expressionName, object epxressionArgument);
    }
}
