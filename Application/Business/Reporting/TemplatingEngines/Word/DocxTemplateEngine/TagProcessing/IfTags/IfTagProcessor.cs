﻿using System.Text.RegularExpressions;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    using Novacode;   
    using System.Collections.Generic;
    using IfTags;
    using Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag;
    using Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
    using Helpers.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
    using CrossCutting.Common.Helpers;
    using Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.TagRegexDescriptors;
    using System;

    public class IfTagProcessor : TableAndParagraphTagsProcessor
    {
        public override int ExecutionOrder => 3;

        private const string OpeningTagRegexValue = @"^{{if ([_a-zA-Z][_a-zA-Z0-9]*)\(([_a-zA-Z][_a-zA-Z0-9]*\.?[_a-zA-Z][_a-zA-Z0-9]*)*\)}}$";
        private const string ClosingTagRegexValue = @"^{{end if}}$";

        private readonly IConditionalExpressionFactory _expressionFactory;
        private readonly IList<IfTag> _ifTags = new List<IfTag>();

        public IfTagProcessor(IConditionalExpressionFactory expressionFactory)
        {
            _expressionFactory = expressionFactory;
        }

        public override void Process(TagProcessorContext context)
        {
            base.Process(context);
            Evaluate(context.DocumentMap, _ifTags);
        }

        protected override void ProcessParagrah(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Paragraph paragraph)
        {
            FindIfTag(context, elementMap, paragraph);
        }

        protected override void ProcessTable(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Table table)
        {
            table.Paragraphs.ForEach(x => FindIfTag(context, elementMap, x));
        }

        public static Match GetOpeningIfTagRegexMatch(string tagText)
        {
            return GetTagRegexMatch(tagText, OpeningTagRegexValue);
        }

        public static bool IsClosingIfTagRegexMatch(string tagText)
        {
            var match = GetTagRegexMatch(tagText, ClosingTagRegexValue);

            return match != null;
        }

        private void FindIfTag(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Paragraph paragraph)
        {
            var tag = GetTag(context.DocumentMap, context, elementMap, paragraph);

            if (tag != null)
            {
                _ifTags.Add(tag);
            }
        }

        private IfTag GetTag(DocumentMap documentMap, TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Paragraph paragraph)
        {
            var match = GetOpeningIfTagRegexMatch(paragraph.Text);

            return match != null
                ? CreateTag(documentMap, context, elementMap, match)
                : null;
        }

        private IfTag CreateTag(DocumentMap documentMap, TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Match match)
        {
            var ifTagParsingResult = new IfTagParsingResult(match.Groups);
            var expressionArgument = GetIfTagExpressionArgument(ifTagParsingResult, elementMap, context.Model);

            var openingElement = elementMap.Key;
            var closingElement = documentMap.FindMatchingEndIfElement(elementMap);

            return new IfTag
            {
                ClosingElement = closingElement,
                OpeningElement = openingElement,
                ActionToEvaluate = GetRemoveIfTagContentAction(documentMap, elementMap),
                Expression = _expressionFactory.Resolve(ifTagParsingResult.ExpressionName, expressionArgument)
            };
        }

        private static object GetIfTagExpressionArgument(IfTagParsingResult ifTagParsingResult, KeyValuePair<InsertBeforeOrAfter, ElementDescription> containingElement, object model)
        {
            var argumentNameAsValueTag = ValueTagProcessor.CreateValueTag(ifTagParsingResult.ExpressionArgumentName);

            return containingElement.Value.LoopIterator != null
                ? TagValueHelper.GetLoopTagValue<object>(containingElement.Value, argumentNameAsValueTag)
                : TagValueHelper.GetTagValue<object>(model, argumentNameAsValueTag);
        }

        private void Evaluate(DocumentMap map, IList<IfTag> tags)
        {
            foreach (var tag in tags)
            {
                tag.Evaluate();
                map.MarkForRemoval(tag.OpeningElement);
                map.MarkForRemoval(tag.ClosingElement);
            }

            map.RemoveMarkedElements();
        }

        private Action GetRemoveIfTagContentAction(
            DocumentMap map,
            KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap)
        {
            return
                () =>
                {
                    var openingElement = elementMap.Key;
                    var closingElement = map.FindMatchingEndIfElement(elementMap);
                    var contentBetweenIfTag = map.GetDocumentPart(openingElement, closingElement);

                    map.MarkForRemoval(contentBetweenIfTag);
                };
        }
    }
}
