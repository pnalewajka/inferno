﻿using System;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTag.Expressions;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing.IfTags
{
    public class ConditionalExpressionFactory : IConditionalExpressionFactory
    {
        public ConditionalExpression Resolve(string expressionName, object expressionArgument)
        {
            var expressions = GetAllExpressionTypes();
            var expression = FindExpressionByName(expressionName, expressionArgument, expressions);

            if (expression == null)
            {
                throw new Exception($"Expression {expressionName} not found");
            }

            return expression;

        }

        private static ConditionalExpression FindExpressionByName(string expressionName, object expressionArgument, IEnumerable<Type> expressions)
        {
            ConditionalExpression resultExpression = null;

            foreach (var expression in expressions)
            {
                var expressionNameAttribute = expression.GetCustomAttributes<ExpressionNameAttribute>().FirstOrDefault();

                if (expressionNameAttribute != null)
                {
                    if (expressionName == expressionNameAttribute.Name)
                    {
                        resultExpression =  ReflectionHelper.CreateInstance<ConditionalExpression>(expression, new[] { expressionArgument });
                    }
                }
            }

            return resultExpression;
        }

        private static IEnumerable<Type> GetAllExpressionTypes()
        {
            var epxressionType = typeof(ConditionalExpression);

            return TypeHelper.GetSubclassesOf(epxressionType)
                .Where(t => !t.IsAbstract);
        }
    }
}
