﻿using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    public abstract class TagProcessor
    {
        public abstract int ExecutionOrder { get; }
        public abstract void Process(TagProcessorContext context);

        protected static Match GetTagRegexMatch(string textToExamine, string regex)
        {
            var patternMatches = Regex.Matches(textToExamine, regex);

            if (patternMatches.Count > 1)
            {
                throw new DocxTemplateException("Only one tag statement per paragraph is permitted");
            }

            return patternMatches
                .Cast<Match>()
                .FirstOrDefault();
        }
    }
}
