﻿using System.Collections.Generic;
using Novacode;
using Smt.Atomic.Business.Reporting.Models.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    public abstract class TableAndParagraphTagsProcessor : TagProcessor
    {
        public override void Process(TagProcessorContext context)
        {
            foreach (var elementMap in context.DocumentMap.Map)
            {
                var table = elementMap.Key as Table;

                if (table != null)
                {
                    ProcessTable(context, elementMap, table);
                }

                var paragraph = elementMap.Key as Paragraph;

                if (paragraph != null)
                {
                    ProcessParagrah(context, elementMap, paragraph);
                }                
            }
        }

        protected abstract void ProcessParagrah(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Paragraph paragraph);
        protected abstract void ProcessTable(TagProcessorContext context, KeyValuePair<InsertBeforeOrAfter, ElementDescription> elementMap, Table table);
    }
}
