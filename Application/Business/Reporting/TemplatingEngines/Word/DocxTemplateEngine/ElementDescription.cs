using System.Text.RegularExpressions;
namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine
{
    public class ElementDescription
    {
        public ElementDescription(GroupCollection regexGroupingResult)
        {
            ForeachClause = regexGroupingResult[0].Value;
            LoopIterator = regexGroupingResult[1].Value;
            LoopProperty = regexGroupingResult[2].Value;
        }

        public ElementDescription() { }

        private const int LoopMaxNestingLevel = 2;

        private int _loopNestingLevel;
        public string LoopProperty { get; set; }
        public string LoopIterator { get; set; }
        public object LoopIteratorValue { get; set; }
        public string ForeachClause { get; set; }
        public bool IsEndForeach { get; set; }
        public bool MarkedForRemoval { get; set; }
        public int LoopNestingLevel
        {
            get { return _loopNestingLevel; }
            set
            {
                if (value > LoopMaxNestingLevel)
                {
                    throw new DocxTemplateException(
                        $"Only {LoopMaxNestingLevel} levels of foreach loop are supported");
                }

                _loopNestingLevel = value;
            }
        }

        public bool IsLoopIterator => LoopIterator != null;

        public ElementDescription Clone()
        {
            return new ElementDescription
            {
                LoopNestingLevel = LoopNestingLevel,
                LoopProperty = LoopProperty,
                LoopIterator = LoopIterator,
                ForeachClause = ForeachClause,
                IsEndForeach = IsEndForeach
            };
        }
    }
}