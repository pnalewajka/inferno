﻿using System.IO;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine
{
    public interface IDocxTemplateEngine
    {
        byte[] GenerateDocumentBytes(string templatePath, object viewModel);
        MemoryStream GenerateDocumentStream(Stream templateStream, object viewModel);
        MemoryStream GenerateDocumentStream(string templatePath, object viewModel);
    }
}