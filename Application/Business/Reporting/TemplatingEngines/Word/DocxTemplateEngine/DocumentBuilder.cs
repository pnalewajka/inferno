using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Novacode;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine
{
    public class DocumentBuilder
    {
        private readonly DocumentMap _documentMap;
        private readonly DocX _docX;

        public DocumentBuilder(DocumentMap documentMap, DocX docX)
        {
            _documentMap = documentMap;
            _docX = docX;
        }

        public static void RemoveElement(InsertBeforeOrAfter element)
        {
            var paragraph = element as Paragraph;

            if (paragraph != null)
            {
                if (paragraph.Xml.Parent != null)
                {
                    paragraph.Remove(trackChanges: false);
                }

                return;
            }

            var table = element as Table;

            if (table != null)
            {
                table.Remove();

                return;
            }

            throw new DocxTemplateException("Only paragraphs and tables are supported");
        }

        public void InsertDocumentParts(Dictionary<InsertBeforeOrAfter, ElementDescription> documentPart, IList<object> loopCollection)
        {
            var insertAfter = documentPart.Last().Key;

            for (var i = 1; i < loopCollection.Count; i++)
            {
                insertAfter = InsertDocumentPartAfter(insertAfter, documentPart.Keys, loopCollection[i]);
            }
        }

        public InsertBeforeOrAfter InsertDocumentPartAfter(InsertBeforeOrAfter afterElement,
            IEnumerable<InsertBeforeOrAfter> elements, object iteratorValue)
        {
            return elements.Aggregate(afterElement,
                (current, element) => InsertElementAfter(current, element, iteratorValue));
        }

        private InsertBeforeOrAfter InsertElementAfter(InsertBeforeOrAfter insertAfter, InsertBeforeOrAfter elementToInsert, object iteratorValue)
        {
            var newElement = InsertElementAfterSelf(insertAfter, elementToInsert);
            var elementDescription = _documentMap.GetElementDescription(elementToInsert);
            var clonedElement = CloneElement(elementDescription, iteratorValue);
            _documentMap.AddElement(newElement, clonedElement);

            return newElement;
        }

        private ElementDescription CloneElement(ElementDescription elementDescription, object iteratorValue)
        {
            var clonedElement = elementDescription.Clone();
            clonedElement.LoopIteratorValue = iteratorValue;

            return clonedElement;
        }

        private InsertBeforeOrAfter InsertElementAfterSelf(InsertBeforeOrAfter element, InsertBeforeOrAfter elementToInsert)
        {
            var paragraph = elementToInsert as Paragraph;

            if (paragraph != null)
            {
                var tableElement = element as Table;

                if (tableElement != null)
                {
                    return InsertParagraphAfterTable(tableElement, paragraph);
                }

                return element.InsertParagraphAfterSelf(paragraph);
            }

            var table = elementToInsert as Table;

            if (table != null)
            {
                return element.InsertTableAfterSelf(table);
            }

            throw new DocxTemplateException("Only paragraphs and tables are supported");
        }

        private InsertBeforeOrAfter InsertParagraphAfterTable(Table table, Paragraph paragraphToInsert)
        {
            // WARNING: this is a workaround - original Novacode.InsertParagraphAfterSelf should be used,
            // but it doesn't create a new Paragraph like it should, just returns the argument instead.
            // I believe they will fix it some day, and then it's better to remove this method.
            // Contact me for details: ivan.sanzcarasa

            // Create paragraph wrapper
            var newParagraph = _docX.InsertParagraph(); // Create paragraph instance + add \n in the end of doc
            _docX.RemoveParagraph(newParagraph); // Remove \n but preserve the reference

            // Attach content to the paragraph
            table.Xml.AddAfterSelf(paragraphToInsert.Xml);
            newParagraph.Xml = table.Xml.ElementsAfterSelf().First();

            return newParagraph;
        }
    }
}