﻿using System.IO;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word
{
    public interface IWordGenerationService
    {
        byte[] GenerateDocumentBytes(string templatePath, object viewModel);
        MemoryStream GenerateDocumentStream(string templatePath, object viewModel);
    }
}
