﻿using System.IO;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using Smt.Atomic.Business.Reporting.Dto;
using System.Linq;
using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.Consts;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Word
{
    [Identifier(ReportingEngine.MsWord)]
    public class WordGenerationService : IWordGenerationService, IReportingEngine
    {
        public string RendererIdentifier => "ReportRenderer.FileContent";

        private readonly IDocxTemplateEngine _docxTemplateEngine;

        public WordGenerationService(IDocxTemplateEngine docxTemplateEngine)
        {
            _docxTemplateEngine = docxTemplateEngine;
        }

        public byte[] GenerateDocumentBytes(string templatePath, object viewModel)
        {
            return _docxTemplateEngine.GenerateDocumentBytes(templatePath, viewModel);
        }

        public MemoryStream GenerateDocumentStream(string templatePath, object viewModel)
        {
            return _docxTemplateEngine.GenerateDocumentStream(templatePath, viewModel);
        }

        public Stream GenerateContent(IReportGenerationContext context)
        {
            var template = context.Definition.ReportTemplates.First();
            var templateContent = context.TemplateContents[template.DocumentId.Value];

            var reportContent = new MemoryStream();
            reportContent.Write(templateContent, 0, templateContent.Length);

            return _docxTemplateEngine.GenerateDocumentStream(reportContent, context.ReportData.Value);
        }
    }
}