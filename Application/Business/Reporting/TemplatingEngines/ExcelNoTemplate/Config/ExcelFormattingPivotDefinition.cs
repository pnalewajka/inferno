using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Extensions;

public class ExcelFormattingPivotDefinition
{
    public string DataSourceTableName { get; set; }

    public string Columns { get; set; }

    public string Rows { get; set; }

    public string Values { get; set; }

    public IEnumerable<string> GetColumnNames()
    {
        return Columns.ParseCommaSeparatedList<string>();
    }

    public IEnumerable<string> GetRowNames()
    {
        return Rows.ParseCommaSeparatedList<string>();
    }

    public IEnumerable<string> GetValueNames()
    {
        return Values.ParseCommaSeparatedList<string>();
    }
}