using System.Xml.Serialization;

[XmlRoot(ElementName = "ExcelFormatting")]
public class ExcelPivotTableFormattingModel
{
    [XmlElement("PivotDefinition")]
    public ExcelFormattingPivotDefinition PivotDefinition { get; set; }
}