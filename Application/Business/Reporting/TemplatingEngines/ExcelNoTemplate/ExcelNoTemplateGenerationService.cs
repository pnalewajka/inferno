﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.Reporting.TemplatingEngines.ExcelNoTemplate.Model;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.ExcelNoTemplate
{
    [Identifier(ReportingEngine.MsExcelNoTemplate)]
    public class ExcelNoTemplateGenerationService : IReportingEngine
    {
        private class PropertyDescription
        {
            public string PropertyName { get; set; }

            public Type PropertyType { get; set; }

            public bool IsDataColumn
            {
                get
                {
                    if (PropertyType == null)
                    {
                        return false;
                    }

                    return typeof(DataColumn).IsAssignableFrom(TypeHelper.GetEnumerationElementType(PropertyType));
                }
            }

            public object GetValue(object o)
            {
                if (o is IDictionary<string, object> dictionary)
                {
                    return dictionary[PropertyName];
                }

                return o.GetType().GetProperties().Single(p => p.Name == PropertyName).GetValue(o);
            }
        }

        private const string DefaultDateTimeFormat = "yyyy-MM-dd HH:mm";
        private List<ExcelPivotTableFormattingModel> PivotFormattingOptions = new List<ExcelPivotTableFormattingModel>();
        public string RendererIdentifier => "ReportRenderer.ReportExcelFileRenderer";

        public Stream GenerateContent(IReportGenerationContext context)
        {
            var reportDataValue = context.ReportData.Value;
            PopulatePivotFormattingOptions(context);

            using (var reportFile = new ExcelPackage())
            {
                ProcessReport(reportFile, reportDataValue);

                return SaveAsStream(reportFile);
            }
        }

        private void PopulatePivotFormattingOptions(IReportGenerationContext context)
        {
            var reportTemplates = context.Definition.ReportTemplates;
            var pivotFormattingTemplates = reportTemplates.Where(t => t.DocumentName.EndsWith("-formatting.xml"));
            var pivotFormattingTemplateDocumentIds = pivotFormattingTemplates.Where(t => t.DocumentId.HasValue).Select(t => t.DocumentId.Value);

            foreach (var documentId in pivotFormattingTemplateDocumentIds)
            {
                PivotFormattingOptions.Add(GetPivotTableFormattingOptionsFromFileContent(context.TemplateContents[documentId]));
            }
        }

        private ExcelPivotTableFormattingModel GetPivotTableFormattingOptionsFromFileContent(byte[] fileContent)
        {
            using (var reader = new StreamReader(new MemoryStream(fileContent)))
            {
                var serializer = new XmlSerializer(typeof(ExcelPivotTableFormattingModel));

                return (ExcelPivotTableFormattingModel)serializer.Deserialize(reader);
            }
        }

        private void ProcessReport(ExcelPackage reportFile, object viewModel)
        {
            foreach (var propertyName in GetEnumerableProperties(viewModel))
            {
                var workSheetName = GetWorkSheetName(propertyName);
                var worksheet = CreateWorksheet(reportFile, workSheetName);
                var underlyingModel = GetPropertyUnderlyingModel(viewModel, propertyName);
                var formattingModel = ProccessModel(worksheet, viewModel, underlyingModel);
                FormatWorksheet(worksheet, formattingModel);
                CreatePivotWorksheetIfFound(reportFile, worksheet, workSheetName);
            }
        }

        private void CreatePivotWorksheetIfFound(ExcelPackage reportFile, ExcelWorksheet worksheet, string workSheetName)
        {
            var pivotDefinition = PivotFormattingOptions.Find(o => o.PivotDefinition.DataSourceTableName == workSheetName)?.PivotDefinition;

            if (pivotDefinition == null)
            {
                return;
            }

            var pivotTableName = workSheetName + "PivotTable";
            var pivotWorksheet = reportFile.Workbook.Worksheets.Add(pivotTableName);
            var pivotTable = pivotWorksheet.PivotTables.Add(pivotWorksheet.Cells["A1"], worksheet.Cells[worksheet.Dimension.Address.ToString()], pivotTableName);

            AddColumnFieldsToPivotTable(pivotTable, pivotDefinition);
            AddRowFieldsToPivotTable(pivotTable, pivotDefinition);
            AddDataFieldsToPivotTable(pivotTable, pivotDefinition);

            pivotTable.DataOnRows = false;
        }

        private void AddColumnFieldsToPivotTable(ExcelPivotTable pivotTable, ExcelFormattingPivotDefinition pivotDefinition)
        {
            foreach (var column in pivotDefinition.GetColumnNames())
            {
                var field = pivotTable.Fields[column];

                if (field != null)
                {
                    pivotTable.ColumnFields.Add(field);
                }
            }
        }

        private void AddRowFieldsToPivotTable(ExcelPivotTable pivotTable, ExcelFormattingPivotDefinition pivotDefinition)
        {
            foreach (var row in pivotDefinition.GetRowNames())
            {
                var field = pivotTable.Fields[row];

                if (field != null)
                {
                    pivotTable.RowFields.Add(field);
                }
            }
        }

        private void AddDataFieldsToPivotTable(ExcelPivotTable pivotTable, ExcelFormattingPivotDefinition pivotDefinition)
        {
            foreach (var value in pivotDefinition.GetValueNames())
            {
                var field = pivotTable.Fields[value];

                if (field != null)
                {
                    pivotTable.DataFields.Add(field);
                }
            }
        }

        private FormatingModel ProccessModel(ExcelWorksheet worksheet, object viewModel, object underlyingModel)
        {
            var records = underlyingModel is PropertyInfo ? (((PropertyInfo)underlyingModel).GetValue(viewModel) as IEnumerable) : (underlyingModel as IEnumerable);
            var recordType = TypeHelper.GetEnumerationElementType(records.GetType());
            var recordPropertyDescriptions = typeof(IDictionary<string, object>).IsAssignableFrom(recordType)
                ? GetDictionaryProperties((IDictionary<string, object>)records.Cast<object>().FirstOrDefault())
                : GetRelevantRecordProperties(recordType);

            var dataColumnsIndexOffset = GenerateColumnHeaders(worksheet, recordPropertyDescriptions);
            var dateTimeColumnsIndices = new List<int>();
            var knownDataColumns = new Dictionary<string, int>();
            var rowIndex = 2;

            foreach (var record in records)
            {
                var columnIndex = 1;

                foreach (var propertyDescription in recordPropertyDescriptions)
                {
                    var propertyValue = propertyDescription.GetValue(record);

                    if (rowIndex == 2
                        && (propertyDescription.PropertyType == typeof(DateTime) || propertyDescription.PropertyType == typeof(DateTime?)))
                    {
                        dateTimeColumnsIndices.Add(columnIndex);
                    }

                    if (propertyValue is IEnumerable dataColumns
                        && typeof(DataColumn).IsAssignableFrom(TypeHelper.GetEnumerationElementType(propertyValue.GetType())))
                    {
                        foreach (DataColumn dataColumn in dataColumns)
                        {
                            var columnKey = dataColumn.ColumnName;
                            var columnValue = dataColumn.Value;

                            if (knownDataColumns.TryGetValue(columnKey, out var existingColumnIndex))
                            {
                                SetCellValue(worksheet, rowIndex, existingColumnIndex, columnValue);
                            }
                            else
                            {
                                var newDataColumnIndex = dataColumnsIndexOffset++;
                                knownDataColumns[columnKey] = newDataColumnIndex;

                                CreateWorkSheetHeader(worksheet, newDataColumnIndex, columnKey);
                                SetCellValue(worksheet, rowIndex, newDataColumnIndex, columnValue);
                            }

                            if (rowIndex == 2
                                && (columnValue.GetType() == typeof(DateTime) || columnValue.GetType() == typeof(DateTime?))
                                && knownDataColumns.TryGetValue(columnKey, out var datetimeColumnIndex))
                            {
                                dateTimeColumnsIndices.Add(datetimeColumnIndex);
                            }
                        }
                    }
                    else
                    {
                        SetCellValue(worksheet, rowIndex, columnIndex, propertyValue);
                    }

                    ++columnIndex;
                }

                ++rowIndex;
            }

            return new FormatingModel { RowIndex = rowIndex, DateTimeColumnsIndices = dateTimeColumnsIndices };
        }

        private object GetPropertyUnderlyingModel(object viewModel, string propertyName)
        {
            if (viewModel is IDictionary<string, object> dictionary)
            {
                return dictionary.Single(p => p.Key == propertyName).Value;
            }

            return viewModel.GetType().GetProperties().Single(p => p.Name == propertyName);
        }

        private IEnumerable<string> GetEnumerableProperties(object viewModel)
        {
            var enumerablePropertiesExpression = new Func<Type, bool>(t => t != typeof(string) && typeof(IEnumerable).IsAssignableFrom(t));

            if (viewModel is IDictionary<string, object>)
            {
                var properties = viewModel as IDictionary<string, object>;
                return properties.Where(p => enumerablePropertiesExpression(p.Value.GetType())).Select(p => p.Key);
            }

            return viewModel.GetType().GetProperties().Where(p => enumerablePropertiesExpression(p.PropertyType)).Select(p => p.Name);
        }

        private string GetWorkSheetName(string modelName)
        {
            var result = modelName;
            result = result.TrimEnd("ReportModel");
            result = result.TrimEnd("ViewModel");
            result = result.TrimEnd("Model");

            return result;
        }

        private ExcelWorksheet CreateWorksheet(ExcelPackage reportFile, string workSheetName)
        {
            return reportFile.Workbook.Worksheets.Add(workSheetName);
        }

        private static void FormatWorksheet(ExcelWorksheet worksheet, FormatingModel formatingModel)
        {
            var column = 1;

            while (!string.IsNullOrWhiteSpace(worksheet.Cells[1, column].GetValue<string>()))
            {
                worksheet.Cells[1, column].Style.Font.Bold = true;
                worksheet.Cells[1, column].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, column].Style.Font.Color.SetColor(Color.White);
                worksheet.Cells[1, column].Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);

                if (formatingModel.DateTimeColumnsIndices.Any(c => c == column))
                {
                    worksheet.Column(column).Style.Numberformat.Format = DefaultDateTimeFormat;
                }

                column++;
            }

            if (column > 1)
            {
                var headerAddress = new ExcelAddress(1, 1, formatingModel.RowIndex, column - 1);
                worksheet.Cells[headerAddress.Address].AutoFitColumns();
                worksheet.Cells[headerAddress.Address].AutoFilter = true;
            }
        }

        private int GenerateColumnHeaders(ExcelWorksheet worksheet, PropertyDescription[] propertyDescriptions)
        {
            int columnIndex = 1;

            foreach (var propertyDescription in propertyDescriptions.Where(p => !p.IsDataColumn))
            {
                CreateWorkSheetHeader(worksheet, columnIndex++, propertyDescription.PropertyName);
            }

            return columnIndex;
        }

        private PropertyDescription[] GetDictionaryProperties(IDictionary<string, object> dictionary)
        {
            if (dictionary == null)
            {
                return new PropertyDescription[] { };
            }

            return dictionary
                .Select(p => new PropertyDescription
                {
                    PropertyName = p.Key,
                    PropertyType = dictionary[p.Key]?.GetType(),
                })
                .ToArray();
        }

        private PropertyDescription[] GetRelevantRecordProperties(Type recordType)
        {
            return recordType.GetProperties()
                .Where(p => p.PropertyType == typeof(string)
                    || (!typeof(IEnumerable).IsAssignableFrom(p.PropertyType)
                    || typeof(DataColumn).IsAssignableFrom(TypeHelper.GetEnumerationElementType(p.PropertyType))))
                .Select(i => new PropertyDescription
                {
                    PropertyName = i.Name,
                    PropertyType = i.PropertyType
                })
                .ToArray();
        }

        private void CreateWorkSheetHeader(ExcelWorksheet worksheet, int columnIndex, object columnValue)
        {
            worksheet.Cells[1, columnIndex].Style.Font.Bold = true;
            SetCellValue(worksheet, 1, columnIndex, columnValue);
        }

        private void SetCellValue(ExcelWorksheet worksheet, int rowIndex, int columnIndex, object columnValue)
        {
            worksheet.Cells[rowIndex, columnIndex].Value = columnValue;
        }

        private static MemoryStream SaveAsStream(ExcelPackage excelPackage)
        {
            var fileResultStream = new MemoryStream();
            excelPackage.SaveAs(fileResultStream);

            return fileResultStream;
        }
    }
}
