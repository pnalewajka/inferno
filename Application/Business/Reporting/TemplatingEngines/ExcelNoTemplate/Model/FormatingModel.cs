﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.ExcelNoTemplate.Model
{
    public class FormatingModel
    {
        public int RowIndex { get; set; }

        public ICollection<int> DateTimeColumnsIndices { get; set; }
    }
}
