﻿using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using System.Linq;
using Smt.Atomic.Business.Reporting.Dto;
using System.IO;
using Smt.Atomic.Business.Reporting.Consts;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Pivot
{
    [Identifier(ReportingEngine.PivotTable)]
    public class PivotTableReportingEngine : IReportingEngine
    {
        public string RendererIdentifier => "ReportRenderer.PivotTable";

        public Stream GenerateContent(IReportGenerationContext context)
        {
            var template = context.Definition.ReportTemplates.First(t => t.DocumentName.EndsWith("-config.js"));
            var templateContent = context.TemplateContents[template.DocumentId.Value];

            return new MemoryStream(templateContent);
        }
    }
}
