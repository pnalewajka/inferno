﻿using OfficeOpenXml;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using System.IO;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.TagProcessing;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Data.Entities.Modules.Reporting;
using System.Linq;
using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.Consts;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel
{
    [Identifier(ReportingEngine.MsExcel)]
    public class ExcelGenerationService : IExcelGenerationService, IReportingEngine
    {
        public string RendererIdentifier => "ReportRenderer.FileContent";

        public Stream GenerateContent(IReportGenerationContext context)
        {
            var template = context.Definition.ReportTemplates.First();
            var templateContent = context.TemplateContents[template.DocumentId.Value];

            var reportContent = new MemoryStream();
            reportContent.Write(templateContent, 0, templateContent.Length);

            using (var templateFile = new ExcelPackage(reportContent))
            {
                ProcessTemplate(templateFile, context.ReportData.Value);

                return SaveAsStream(templateFile);
            }
        }

        public byte[] GenerateDocumentBytes(string templatePath, object viewModel)
        {
            return GenerateDocumentStream(templatePath, viewModel).ToArray();
        }

        public MemoryStream GenerateDocumentStream(string templatePath, object viewModel)
        {
            using (var templateFile = new ExcelPackage(new FileInfo(templatePath)))
            {
                ProcessTemplate(templateFile, viewModel);

                return SaveAsStream(templateFile);
            }
        }

        public void ProcessTemplate(ExcelPackage templateFile, object viewModel)
        {
            foreach (var excelWorksheet in templateFile.Workbook.Worksheets)
            {
                var workSheetModel = new WorkSheetModel(excelWorksheet);
                new RowExtendingTagProcessor(workSheetModel, viewModel).Process();
                new ColumnExtendingTagProcessor(workSheetModel, viewModel).Process();
                new ValueReplacingTagProcessor(workSheetModel, viewModel).Process();
                workSheetModel.AutoFitColums();
            }
        }

        private static MemoryStream SaveAsStream(ExcelPackage excelPackage)
        {
            var fileResultStream = new MemoryStream();
            excelPackage.SaveAs(fileResultStream);

            return fileResultStream;
        }
    }
}