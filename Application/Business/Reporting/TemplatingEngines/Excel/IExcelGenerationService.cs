﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel
{
    public interface IExcelGenerationService
    {
        byte[] GenerateDocumentBytes(string templatePath, object viewModel);

        MemoryStream GenerateDocumentStream(string templatePath, object viewModel);
    }
}
