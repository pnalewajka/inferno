﻿using System;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel
{
    public class ExcelTemplateException : Exception
    {
        public ExcelTemplateException(Exception ex) : base(ex.Message, ex)
        {
        }

        public ExcelTemplateException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
