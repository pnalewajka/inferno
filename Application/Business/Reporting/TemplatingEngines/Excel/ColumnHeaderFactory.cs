﻿namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel
{
    public delegate string ColumnHeaderFactory(string templateHeader, string dataColumnName);
}
