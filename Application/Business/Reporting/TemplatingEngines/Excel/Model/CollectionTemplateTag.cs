﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.MemberAccess;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model
{
    internal class CollectionTemplateTag : TemplateTag
    {
        private static readonly Regex TemplatePattern = new Regex(
            @"^{{foreach[ ]+(?<ElementPath>[A-Za-z0-9_\(\)\.\[\]]+)[ ]+in[ ]+(?<Path>[A-Za-z0-9_\.\[\]\(\)]+)[ ]*}}$",
            RegexOptions.Compiled);

        public MemberAccessPath ElementPath { get; }

        public CollectionTemplateTag(int columnId, int rowId, MemberAccessPath path, MemberAccessPath elementPath)
            : base(columnId, rowId, path)
        {
            ElementPath = elementPath;
        }

        public new IEnumerable<object> GetValue(object model)
        {
            return ((IEnumerable)base.GetValue(model)).Cast<object>();
        }

        public ObjectTemplateTag GetRowIndexedTag(int index, int rowId)
        {
            var path = Path
                .Join(new IndexerAccessElement(index))
                .Join(ElementPath);

            return new ObjectTemplateTag(ColumnId, rowId, path);
        }

        public override string ToString()
        {
            return "{{foreach " + ElementPath + " in " + Path + "}}";
        }

        public static CollectionTemplateTag TryParse(int columnId, int rowId, string template)
        {
            var match = TemplatePattern.Match(template.Trim());

            if (!match.Success)
            {
                return null;
            }

            var path = match.Groups["Path"].Value;
            var elementPath = match.Groups["ElementPath"].Value;

            return new CollectionTemplateTag(
                columnId,
                rowId,
                new MemberAccessPath(path),
                new MemberAccessPath(elementPath));
        }
    }
}
