﻿using System.Text.RegularExpressions;
using Smt.Atomic.CrossCutting.Common.MemberAccess;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model
{
    internal class ObjectTemplateTag : TemplateTag
    {
        public static readonly Regex TemplatePattern = new Regex(@"^{{(?<Path>[A-Za-z0-9_\(\)\.\[\]]+)}}$", RegexOptions.Compiled);

        public ObjectTemplateTag(int columnId, int rowId, MemberAccessPath path) : base(columnId, rowId, path)
        {
        }

        public ObjectTemplateTag GetColumnIndexedTag(int index, int columnId)
        {
            var path = Path.Join(new IndexerAccessElement(index));

            return new ObjectTemplateTag(columnId, RowId, path);
        }

        public override string ToString()
        {
            return "{{" + Path + "}}";
        }

        public static ObjectTemplateTag TryParse(int columnId, int rowId, string template)
        {
            var match = TemplatePattern.Match(template.Trim());

            if (!match.Success)
            {
                return null;
            }

            var path = match.Groups["Path"].Value;

            return new ObjectTemplateTag(columnId, rowId, new MemberAccessPath(path));
        }
    }
}
