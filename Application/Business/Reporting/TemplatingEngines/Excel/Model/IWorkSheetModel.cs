﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model
{
    internal interface IWorkSheetModel
    {
        IEnumerable<TemplateTag> GetTemplateTags();

        object GetValue(int columnId, int rowId);

        void SetValue(int columnId, int rowId, object value);

        void SetHeaderValue(int columnId, int rowId, string value);

        void InsertColumns(int columnId, int count);

        void InsertRows(int rowId, int count);

        void RemoveColumn(int columnId);

        void RemoveRow(int rowId);
    }
}