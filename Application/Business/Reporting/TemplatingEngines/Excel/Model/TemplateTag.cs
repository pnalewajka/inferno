﻿using Smt.Atomic.CrossCutting.Common.MemberAccess;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model
{
    internal abstract class TemplateTag
    {
        public int ColumnId { get; }

        public int RowId { get; }

        public MemberAccessPath Path { get; }

        protected TemplateTag(int columnId, int rowId, MemberAccessPath path)
        {
            ColumnId = columnId;
            RowId = rowId;
            Path = path;
        }

        public object GetValue(object model)
        {
            var value = Path.GetValueOf(model);
            var dataColumn = value as DataColumn;

            if (dataColumn != null)
            {
                value = dataColumn.Value;
            }

            return value;
        }
    }
}
