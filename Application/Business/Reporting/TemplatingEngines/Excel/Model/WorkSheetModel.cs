﻿using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model
{
    internal class WorkSheetModel : IWorkSheetModel
    {
        private readonly ExcelWorksheet _worksheet;
        private readonly List<TableNameWithAddress> _excelTables;

        public WorkSheetModel(ExcelWorksheet worksheet)
        {
            _worksheet = worksheet;
            _excelTables = worksheet.Tables.Select(t => new TableNameWithAddress
            {
                Name = t.Name,
                Address = t.Address,
            }).ToList();
        }

        public IEnumerable<TemplateTag> GetTemplateTags()
        {
            return
                _worksheet.Cells
                    .Select(c => new
                    {
                        c.Text,
                        ColumnId = c.Start.Column,
                        RowId = c.Start.Row,
                    })
                    .Where(c => !string.IsNullOrEmpty(c.Text))
                    .Select(c =>
                        ((TemplateTag)ObjectTemplateTag.TryParse(c.ColumnId, c.RowId, c.Text)) ??
                        CollectionTemplateTag.TryParse(c.ColumnId, c.RowId, c.Text))
                    .Where(t => t != null)
                    .ToList();
        }

        public void RemoveColumn(int columnId)
        {
            _worksheet.DeleteColumn(columnId);
        }

        public void RemoveRow(int rowId)
        {
            _worksheet.DeleteRow(rowId);
        }

        public void InsertColumns(int columnId, int count)
        {
            _worksheet.InsertColumn(columnId, count, columnId);
        }

        public void InsertRows(int rowId, int count)
        {
            _worksheet.InsertRow(rowId, count, rowId + count);
        }

        public object GetValue(int columnId, int rowId)
        {
            return _worksheet.Cells[rowId, columnId].Value;
        }

        public void SetValue(int columnId, int rowId, object value)
        {
            _worksheet.Cells[rowId, columnId].Value = value;
        }

        public void SetHeaderValue(int columnId, int rowId, string value)
        {
            SetValue(columnId, rowId, value);
            _worksheet.Column(columnId).BestFit = true;
        }

        public void AutoFitColums()
        {
            if (_worksheet.Dimension != null)
            {
                _worksheet.Cells[_worksheet.Dimension.Address].AutoFitColumns();
            }
        }

        private struct TableNameWithAddress
        {
            public string Name { get; set; }
            public ExcelAddressBase Address { get; set; }
        }
    }
}
