﻿using Newtonsoft.Json;
using System;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel
{
    public class DataColumn
    {
        [JsonIgnore]
        public static readonly ColumnHeaderFactory OverrideHeader =
            (templateHeader, dataColumnName) => dataColumnName;

        [JsonIgnore]
        public static readonly ColumnHeaderFactory AppendColumnNameToHeader =
            (templateHeader, dataColumnName) => templateHeader + dataColumnName;

        [JsonProperty(PropertyName = "Key")]
        public string ColumnName { get; set; }

        public object Value { get; set; }

        [JsonIgnore]
        public ColumnHeaderFactory ColumnHeaderFactory { get; set; }

        public DataColumn()
        {
            ColumnHeaderFactory = OverrideHeader;
        }
    }
}
