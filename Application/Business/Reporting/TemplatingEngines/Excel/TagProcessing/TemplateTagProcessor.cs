﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.TagProcessing
{
    internal abstract class TemplateTagProcessor
    {
        protected readonly IWorkSheetModel WorkSheet;
        protected readonly object Model;

        protected TemplateTagProcessor(IWorkSheetModel workSheet, object model)
        {
            WorkSheet = workSheet;
            Model = model;
        }

        public abstract void Process();
    }
}
