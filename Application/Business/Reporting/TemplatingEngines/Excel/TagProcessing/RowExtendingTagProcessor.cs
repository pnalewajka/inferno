﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.TagProcessing
{
    internal class RowExtendingTagProcessor : TemplateTagProcessor
    {
        private int _rowsInserted = 0;

        public RowExtendingTagProcessor(IWorkSheetModel workSheet, object model) : base(workSheet, model)
        {
        }

        public override void Process()
        {
            var templateTagsByRow = WorkSheet.GetTemplateTags()
                .OfType<CollectionTemplateTag>()
                .GroupBy(t => t.RowId)
                .OrderBy(g => g.Key)
                .ToList();

            foreach (var row in templateTagsByRow)
            {
                var tagsWithRows = row
                    .OrderBy(t => t.ColumnId)
                    .Select(t => new TagWithRowCount
                    {
                        Tag = t,
                        RowCount = ComputeNumberOfRows(t)
                    });

                ProcessRow(row.Key + _rowsInserted, tagsWithRows.ToList());
            }
        }

        private void ProcessRow(int rowId, IReadOnlyCollection<TagWithRowCount> tagsWithRows)
        {
            var maxNumberOfRows = tagsWithRows.Max(t => t.RowCount);

            if (maxNumberOfRows > 1)
            {
                WorkSheet.InsertRows(rowId, maxNumberOfRows - 1);
            }

            PopulateTags(rowId, tagsWithRows);

            if (maxNumberOfRows > 1)
            {
                _rowsInserted += maxNumberOfRows - 1;
            }
        }

        private void PopulateTags(int rowId, IEnumerable<TagWithRowCount> tagsWithRows)
        {
            foreach (var tagWithRows in tagsWithRows)
            {
                PopulateTags(rowId, tagWithRows);
            }
        }

        private void PopulateTags(int rowId, TagWithRowCount tagWithRows)
        {
            if (tagWithRows.RowCount == 0)
            {
                WorkSheet.SetValue(tagWithRows.Tag.ColumnId, rowId, null);

                return;
            }

            for (var i = 0; i < tagWithRows.RowCount; i++)
            {
                var value = tagWithRows.Tag.GetRowIndexedTag(i, rowId + i).ToString();
                WorkSheet.SetValue(tagWithRows.Tag.ColumnId, rowId + i, value);
            }
        }

        private int ComputeNumberOfRows(CollectionTemplateTag templateTag)
        {
            var collection = templateTag.GetValue(Model);

            return collection?.Count() ?? 0;
        }

        private struct TagWithRowCount
        {
            public CollectionTemplateTag Tag { get; set; }

            public int RowCount { get; set; }
        }
    }
}
