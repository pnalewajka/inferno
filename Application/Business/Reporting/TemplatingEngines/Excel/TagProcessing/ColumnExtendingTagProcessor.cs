﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.TagProcessing
{
    internal class ColumnExtendingTagProcessor : TemplateTagProcessor
    {
        private int _columnsInserted = 0;

        public ColumnExtendingTagProcessor(IWorkSheetModel workSheet, object model) : base(workSheet, model)
        {
        }

        public override void Process()
        {
            var templateTagsByColumn = WorkSheet.GetTemplateTags()
                .OfType<ObjectTemplateTag>()
                .GroupBy(t => t.ColumnId)
                .OrderBy(g => g.Key)
                .ToList();

            foreach (var column in templateTagsByColumn)
            {
                var tagsWithColumns = column
                    .OrderBy(t => t.RowId)
                    .Select(t => new TagWithColums
                    {
                        Tag = t,
                        Columns = GetColumns(t)
                    })
                    .Where(t => t.Columns != null)
                    .ToList();

                if (tagsWithColumns.Any())
                {
                    ProcessColumn(column.Key + _columnsInserted, tagsWithColumns);
                }
            }
        }

        private void ProcessColumn(int columnId, IReadOnlyCollection<TagWithColums> tagsWithColumns)
        {
            var maxColumnCount = CountMaxColumns(tagsWithColumns);

            if (maxColumnCount == 0)
            {
                WorkSheet.RemoveColumn(columnId);
            }
            else
            {
                foreach (var tagWithColumns in tagsWithColumns)
                {
                    WorkSheet.SetValue(columnId, tagWithColumns.Tag.RowId, null);
                }

                WorkSheet.InsertColumns(columnId, maxColumnCount - 1);
                PopulateTags(columnId, tagsWithColumns);
            }

            _columnsInserted += maxColumnCount - 1;
        }

        private static IEnumerable<IReadOnlyCollection<TagWithColums>> SplitToRowRanges(IEnumerable<TagWithColums> tags)
        {
            var result = new List<TagWithColums>();

            foreach (var tag in tags)
            {
                if (result.Count > 0 && result.Last().Tag.RowId < tag.Tag.RowId - 1)
                {
                    yield return result;
                    result = new List<TagWithColums>();
                }

                result.Add(tag);
            }

            if (result.Count > 0)
            {
                yield return result;
            }
        }

        private void PopulateHeaders(int columnId, int rowId, IReadOnlyList<ColumnHeader> headers)
        {
            var templateHeaderName = WorkSheet.GetValue(columnId + 1, rowId)?.ToString() ?? string.Empty;

            for (var i = 0; i < headers.Count; i++)
            {
                var headerName = headers[i].GetHeaderName(templateHeaderName);
                WorkSheet.SetHeaderValue(columnId + i, rowId, headerName);
            }
        }

        private void PopulateTags(int columnId, IEnumerable<TagWithColums> tagsWithColumns)
        {
            foreach (var rowRange in SplitToRowRanges(tagsWithColumns))
            {
                var headers = CombineColumns(rowRange.SelectMany(t => t.Columns)).ToList();
                var headerRowId = rowRange.First().Tag.RowId - 1;

                if (headerRowId > 0)
                {
                    PopulateHeaders(columnId, headerRowId, headers);
                }

                foreach (var tagWithColumns in rowRange)
                {
                    PopulateTags(columnId, tagWithColumns, headers);
                }
            }
        }

        private void PopulateTags(int columnId, TagWithColums tagWithColumns, IReadOnlyCollection<ColumnHeader> headers)
        {
            var index = 0;

            foreach (var column in tagWithColumns.Columns.Select(AdjustColumnPostitions(headers)))
            {
                var value = tagWithColumns.Tag.GetColumnIndexedTag(index, columnId + column.Position).ToString();
                WorkSheet.SetValue(columnId + column.Position, tagWithColumns.Tag.RowId, value);
                index++;
            }
        }

        private List<ColumnHeader> GetColumns(TemplateTag templateTag)
        {
            var value = templateTag.GetValue(Model);
            var columnsCollection = value as IEnumerable<DataColumn>;

            return columnsCollection?.Select((c, i) => new ColumnHeader
            {
                Position = i,
                ColumnName = c.ColumnName,
                ColumnHeaderFactory = c.ColumnHeaderFactory,
            }).ToList();
        }

        private static Func<ColumnHeader, ColumnHeader> AdjustColumnPostitions(IEnumerable<ColumnHeader> headers)
        {
            return c => new ColumnHeader
            {
                Position = headers.First(h => h.ColumnName == c.ColumnName).Position,
                ColumnName = c.ColumnName,
                ColumnHeaderFactory = c.ColumnHeaderFactory,
            };
        }
        
        private static IEnumerable<ColumnHeader> CombineColumns(IEnumerable<ColumnHeader> columns)
        {
            var result = columns.DistinctBy(h => h.ColumnName).Select((h, i) => new ColumnHeader
            {
                Position = i,
                ColumnName = h.ColumnName,
                ColumnHeaderFactory = h.ColumnHeaderFactory,
            });

            return result;
        }

        private static int CountMaxColumns(IEnumerable<TagWithColums> tagsWithColumns)
        {
            return SplitToRowRanges(tagsWithColumns)
                .Select(r => r.SelectMany(t => t.Columns))
                .Select(CombineColumns)
                .Max(c => c.Count());
        }

        private struct ColumnHeader
        {
            public int Position { get; set; }

            public string ColumnName { get; set; }

            public ColumnHeaderFactory ColumnHeaderFactory { get; set; }

            public string GetHeaderName(string templateHeaderName)
            {
                return ColumnHeaderFactory(templateHeaderName, ColumnName);
            }
        }

        private struct TagWithColums
        {
            public ObjectTemplateTag Tag { get; set; }

            public List<ColumnHeader> Columns { get; set; }
        }
    }
}
