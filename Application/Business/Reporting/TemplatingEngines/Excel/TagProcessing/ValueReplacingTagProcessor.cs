﻿using System.Linq;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.TagProcessing
{
    internal class ValueReplacingTagProcessor : TemplateTagProcessor
    {
        public ValueReplacingTagProcessor(IWorkSheetModel workSheet, object model) : base(workSheet, model)
        {
        }

        public override void Process()
        {
            var templateTags = WorkSheet.GetTemplateTags().Cast<ObjectTemplateTag>().ToList();

            foreach (var tag in templateTags)
            {
                var value = tag.GetValue(Model);
                WorkSheet.SetValue(tag.ColumnId, tag.RowId, value);
            }
        }
    }
}
