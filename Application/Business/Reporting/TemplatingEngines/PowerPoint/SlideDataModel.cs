﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint
{
    internal class SlideDataModel
    {
        public object DataSource { get; set; }
        public IEnumerable<string> TextPropertyNames { get; set; }
        public IEnumerable<string> PicturePropertyNames { get; set; }
    }
}
