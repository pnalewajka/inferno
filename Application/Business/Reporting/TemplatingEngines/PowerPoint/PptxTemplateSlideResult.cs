﻿using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint.PptxTemplater;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint
{
    public class PptxTemplateSlideResult
    {
        public PptxSlide Slide { get; set; }
        public IEnumerable<object> MatchedDataSource { get; set; }
    }
}
