﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint.PptxTemplater;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Consts;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint
{
    [Identifier(ReportingEngine.MsPowerPoint)]
    public class PowerPointGenerationService : IPowerPointGenerationService, IReportingEngine
    {
        public string RendererIdentifier => "ReportRenderer.FileContent";

        protected const string SlideNoteForEachRegex = @"^{{foreach:(.*?)}}$";
        protected const string TemplateTagFormatString = "{{{{{0}}}}}";

        public Stream GenerateContent(IReportGenerationContext context)
        {
            var template = context.Definition.ReportTemplates.First();
            var templateContent = context.TemplateContents[template.DocumentId.Value];

            var reportContent = new MemoryStream();
            reportContent.Write(templateContent, 0, templateContent.Length);

            InjectPresentationData(reportContent, context.ReportData.Value);

            return reportContent;
        }

        public byte[] GeneratePresentationBytes(string templatePath, object viewModel)
        {
            return GeneratePresentationStream(templatePath, viewModel).ToArray();
        }

        public MemoryStream GeneratePresentationStream(string templatePath, object viewModel)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (FileStream fileStream = File.OpenRead(templatePath))
                {
                    fileStream.CopyTo(memoryStream);
                }

                InjectPresentationData(memoryStream, viewModel);

                return memoryStream;
            }
        }

        protected void InjectPresentationData(Stream templateFileStream, object viewModel)
        {
            using (var pptx = new Pptx(templateFileStream, FileAccess.ReadWrite))
            {
                PopulateSlideNotesWithData(pptx.GetSlides(), viewModel);

                var templateSlides = FindTemplateSlides(pptx, viewModel);

                foreach (var templateSlide in templateSlides)
                {
                    PopulateTemplateSlideWithData(templateSlide);
                }

                foreach (var slide in pptx.GetSlides())
                {
                    slide.RemoveNoteParagraphsWithTags();
                }

                PopulateSlidesWithData(pptx.GetSlides(), viewModel);
            }
        }

        private void PopulateTemplateSlideWithData(PptxTemplateSlideResult templateSlideData)
        {
            var dataSourceItemCount = templateSlideData.MatchedDataSource.Count();
            var dataSourceItemType = TypeHelper.GetCollectionElementType(templateSlideData.MatchedDataSource.GetType());
            var textPropertiesToInject = GetAllPropertyNamesOfType<string>(dataSourceItemType).ToList();
            var picturePropertiesToInject = GetAllPropertyNamesOfType<byte[]>(dataSourceItemType).ToList();

            var slideData = new SlideDataModel
            {
                TextPropertyNames = textPropertiesToInject,
                PicturePropertyNames = picturePropertiesToInject
            };

            for (var i = dataSourceItemCount - 1; i >= 0; i--)
            {
                var slideClone = templateSlideData.Slide.Clone();
                PptxSlide.InsertAfter(slideClone, templateSlideData.Slide);

                slideData.DataSource = templateSlideData.MatchedDataSource.ElementAt(i);
                InjectSlideData(slideClone, slideData);
                PopulateSlideNotesWithData(slideClone, slideData.DataSource);
            }

            templateSlideData.Slide.Remove();
        }

        private void PopulateSlidesWithData(IEnumerable<PptxSlide> slides, object viewModel)
        {
            var viewModelType = viewModel.GetType();
            var textPropertiesToInject = GetAllPropertyNamesOfType<string>(viewModelType).ToList();
            var picturePropertiesToInject = GetAllPropertyNamesOfType<byte[]>(viewModelType).ToList();

            var slideData = new SlideDataModel
            {
                DataSource = viewModel,
                TextPropertyNames = textPropertiesToInject,
                PicturePropertyNames = picturePropertiesToInject
            };

            foreach (var slide in slides)
            {
                InjectSlideData(slide, slideData);
            }
        }

        private void PopulateSlideNotesWithData(PptxSlide slide, object viewModel)
        {
            PopulateSlideNotesWithData(new[] { slide }, viewModel);
        }

        private void PopulateSlideNotesWithData(IEnumerable<PptxSlide> slides, object viewModel)
        {
            var viewModelType = viewModel.GetType();
            var textPropertiesToInject = GetAllPropertyNamesOfType<string>(viewModelType).ToList();

            foreach (var textPropertyName in textPropertiesToInject)
            {
                var valueToInject = PropertyHelper.GetPropertyValue<string>(viewModel, textPropertyName);

                foreach (var slide in slides)
                {
                    slide.ReplaceNoteTag(
                        tag: BuildTag(textPropertyName),
                        newText: valueToInject);
                }
            }
        }

        private void InjectSlideData(PptxSlide slide, SlideDataModel slideData)
        {
            foreach (var textPropertyName in slideData.TextPropertyNames)
            {
                var valueToInject = PropertyHelper.GetPropertyValue<string>(slideData.DataSource, textPropertyName);
                slide.ReplaceTag(
                    tag: BuildTag(textPropertyName),
                    newText: StringHelper.TruncateAtWords(valueToInject, 200),
                    replacementType: PptxSlide.ReplacementType.Global);
            }

            foreach (var picturePropertyName in slideData.PicturePropertyNames)
            {
                var pictureData = PropertyHelper.GetPropertyValue<byte[]>(slideData.DataSource, picturePropertyName);
                var pictureTag = BuildTag(picturePropertyName);

                if (pictureData != null)
                {
                    using (var memoryStream = new MemoryStream(pictureData))
                    {
                        var pictureToInject = Image.FromStream(memoryStream);
                        slide.ReplacePicture(
                        tag: pictureTag,
                        newPicture: pictureToInject.ToByteArray(),
                        contentType: pictureToInject.GetContentType());
                    }
                }
                else
                {
                    slide.RemovePictue(pictureTag);
                }
            }
        }

        private IEnumerable<PptxTemplateSlideResult> FindTemplateSlides(Pptx presentation, object viewModel)
        {
            var presentationSlides = presentation.GetSlides();

            foreach (var slide in presentationSlides)
            {
                var notes = slide.GetNotes();

                var loopPropertyName = notes
                    .Select(note => FindLoopPropertyNameInText(note))
                    .FirstOrDefault(propertyName => propertyName != null);

                if (!string.IsNullOrWhiteSpace(loopPropertyName))
                {
                    var propertyValue = PropertyHelper.GetPropertyValue<IEnumerable<object>>(viewModel, loopPropertyName);

                    if (propertyValue != null)
                    {
                        yield return new PptxTemplateSlideResult
                        {
                            Slide = slide,
                            MatchedDataSource = propertyValue
                        };
                    }
                }
            }
        }

        protected static string BuildTag(string tagName)
        {
            return string.Format(TemplateTagFormatString, tagName);
        }

        protected static string FindLoopPropertyNameInText(string text)
        {
            string result = null;

            if (!string.IsNullOrEmpty(text))
            {
                var patternMatches = Regex.Matches(text, SlideNoteForEachRegex);

                if (patternMatches.Count > 0)
                {
                    var match = patternMatches[0];

                    if (match.Groups.Count > 1)
                    {
                        result = match.Groups[1].Value;
                    }
                }
            }

            return result;
        }

        protected static IEnumerable<string> GetAllPropertyNamesOfType<TPropType>(Type sourceType) where TPropType : class
        {
            if (sourceType == null)
            {
                throw new ArgumentNullException(nameof(sourceType));
            }

            return TypeHelper.GetPublicInstanceProperties(sourceType)
                .Where(x => x.PropertyType == typeof(TPropType))
                .Select(p => p.Name)
                .ToArray();
        }
    }
}
