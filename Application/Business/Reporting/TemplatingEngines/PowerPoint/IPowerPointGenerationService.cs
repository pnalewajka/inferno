﻿using System.IO;

namespace Smt.Atomic.Business.Reporting.TemplatingEngines.PowerPoint
{
    public interface IPowerPointGenerationService
    {
        byte[] GeneratePresentationBytes(string templatePath, object viewModel);
        MemoryStream GeneratePresentationStream(string templatePath, object viewModel);
    }
}
