﻿namespace Smt.Atomic.Business.Reporting.Consts
{
    public static class SearchAreaCodes
    {
        public const string Code = "code";
        public const string Name = "name";
        public const string Description = "description";
        public const string Area = "area";
    }
}
