﻿namespace Smt.Atomic.Business.Reporting.Consts
{
    public static class MenuAreas
    {
        public const string Administration = "Administration";
        public const string Competence = "Competence";
        public const string Organization = "Organization";
        public const string TimeTracking = "TimeTracking";
        public const string Allocations = "Allocations";
        public const string Compensation = "Compensation";
        public const string Workflow = "Workflow";
        public const string Recruitment = "Recruitment";
    }
}
