﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Reporting.Consts
{
    public static class ReportingEngine
    {
        public const string MsExcelNoTemplate = "ReportingEngine.MsExcelNoTemplate";

        public const string MsExcel = "ReportEngine.MsExcel";

        public const string MsWord = "ReportEngine.MsWord";

        public const string MsPowerPoint = "ReportEngine.MsPowerPoint";

        public const string PivotTable = "ReportEngine.PivotTable";

        public const string Chart = "ReportEngine.Chart";
    }
}
