﻿namespace Smt.Atomic.Business.Reporting.Consts
{
    public static class MenuGroups
    {
        public const string AdministrationReports = "AdministrationReports";
        public const string CompetenceReports = "CompetenceReports";
        public const string OrgUnit = "OrgUnit";
        public const string PeopleManagement = "PeopleManagement";
        public const string Project = "Project";
        public const string TimeTrackingReportsHR = "TimeTrackingReportsHR";
        public const string TimeTrackingReportsTT = "TimeTrackingReportsTT";
        public const string TimeTrackingReportsUtilization = "TimeTrackingReportsUtilization";
        public const string AllocationsReports = "AllocationsReports";
        public const string CompensationReports = "CompensationReports";
        public const string WorkflowReports = "WorkflowReports";
        public const string RecruitmentReports = "RecruitmentReports";
        public const string KpiReports = "KpiReports";
    }
}
