﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Reporting.Helpers
{
    internal static class RequiredRolesHelper
    {
        public static void CheckRolesForReportConfiguration(IAtomicPrincipal principal, object reportDataSource)
        {
            var requiredRolesProvider = reportDataSource as IRequiredRolesProvider;

            var requiredRoles = requiredRolesProvider == null
                ? GetDefaultRoles(reportDataSource)
                : requiredRolesProvider.GetRolesForReportConfiguration();

            CheckRoles(principal, requiredRoles);
        }

        public static void CheckRolesForReportExecution(IAtomicPrincipal principal, object reportDataSource, object parameters)
        {
            var requiredRolesProvider = reportDataSource as IRequiredRolesProvider;

            var requiredRoles = requiredRolesProvider == null
                ? GetDefaultRoles(reportDataSource)
                : requiredRolesProvider.GetRolesForReportExecution(parameters);

            CheckRoles(principal, requiredRoles);
        }

        private static IEnumerable<SecurityRoleType> GetDefaultRoles(object reportDataSource)
        {
            return AttributeHelper.GetClassAttributes<RequireRoleAttribute>(reportDataSource).Select(a => a.Role);
        }

        private static void CheckRoles(IAtomicPrincipal principal, IEnumerable<SecurityRoleType> roles)
        {
            var securityRoleTypes = roles as IList<SecurityRoleType> ?? roles.ToList();

            if (securityRoleTypes.Any() && !securityRoleTypes.Any(principal.IsInRole))
            {
                throw new MissingRequiredRoleException(securityRoleTypes.Select(r => r.ToString()), false);
            }
        }
    }
}
