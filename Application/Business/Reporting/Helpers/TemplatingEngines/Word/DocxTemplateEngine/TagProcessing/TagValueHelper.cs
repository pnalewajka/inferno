﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing;
using System;

namespace Smt.Atomic.Business.Reporting.Helpers.TemplatingEngines.Word.DocxTemplateEngine.TagProcessing
{
    /// <summary>
    /// Helper methods for extracting value from word template tags
    /// </summary>
    public static class TagValueHelper
    { 
        /// <summary>
        /// Returns evaluated expression's value based on given model
        /// </summary>
        /// <typeparam name="TValueType">Expected model's property type</typeparam>
        /// <param name="model">Model to expamine</param>
        /// <param name="tagText">Word template tag to evaluate</param>
        /// <returns>Model's property value matching given tag</returns>
        public static TValueType GetTagValue<TValueType>(object model, string tagText) where TValueType : class
        {
            var tagExpression = ValueTagProcessor.GetExpressionFromTag(tagText);

            return GetTagExpressionValue<TValueType>(model, tagExpression);           
        }


        /// <summary>
        /// Returns evaluated expression's value based on given element description
        /// </summary>
        /// <typeparam name="TValueType">Expected model's property type</typeparam>
        /// <param name="model">elementDescription containing loop tag</param>
        /// <param name="tagText">Word template tag to evaluate</param>
        /// <returns>loop's property value matching given tag</returns>
        public static TValueType GetLoopTagValue<TValueType>(ElementDescription elementDescription, string tagText) where TValueType : class
        {
            var tagExpression = ValueTagProcessor.GetLoopIteratorProperty(tagText, elementDescription.LoopIterator);
            var reducedTagExpression = ValueTagProcessor.GetExpressionFromTag(tagExpression);

            return GetTagExpressionValue<TValueType>(elementDescription.LoopIteratorValue, reducedTagExpression);
        }

        /// <summary>
        /// Returns evaluated expression's collection value based on given element description
        /// </summary>
        /// <typeparam name="T">Expected model's property type</typeparam>
        /// <param name="model">elementDescription containing loop tag</param>
        /// <param name="tagText">Word template tag to evaluate</param>
        /// <returns>loop's collection property value matching given tag</returns>
        public static ICollection<TValueType> GetTagCollectionValue<TValueType>(object model, string tagText)
            where TValueType : class
        {
            var collectionObject = GetTagExpressionValue<ICollection>(model, tagText);

            return collectionObject?.Cast<TValueType>().ToList() ?? new List<TValueType>();
        }

        private static TValueType GetTagExpressionValue<TValueType>(object model, string tagExpression) where TValueType : class
        {
            var nonListPropertyNames = GetPublicPropertyNamesExcludingType<IList>(model.GetType());
            var collectionPropertyName = GetCollectionPropertyName(tagExpression, nonListPropertyNames);

            if (collectionPropertyName != null)
            {
                return GetCollectionPropertyValue<TValueType>(model, tagExpression, collectionPropertyName);
            }

            return GetNonCollectionPropertyValue<TValueType>(model, tagExpression, nonListPropertyNames);
        }

        private static IEnumerable<string> GetPublicPropertyNamesExcludingType<T>(Type type)
        {
            var publicProperties = TypeHelper.GetPublicInstanceProperties(type).Where(x => x.PropertyType != typeof(T));

            return publicProperties.Select(i => i.Name);
        }

        private static TValueType GetCollectionPropertyValue<TValueType>(object model, string tagExpression, string collectionPropertyName) where TValueType : class
        {
            var collectionProperty = PropertyHelper.GetPropertyValue<object>(model, collectionPropertyName);
            var reducedTagExpression = ValueTagProcessor.GetLoopIteratorProperty(tagExpression, collectionPropertyName);

            return GetTagExpressionValue<TValueType>(collectionProperty, reducedTagExpression);
        }

        private static TValueType GetNonCollectionPropertyValue<TValueType>(object model, string tagExpression, IEnumerable<string> nonListPropertyNames) where TValueType : class
        {
            var propertyName = nonListPropertyNames.SingleOrDefault(p => p == tagExpression);

            return propertyName != null 
                ? PropertyHelper.GetPropertyValue<TValueType>(model, propertyName)
                : null;
        }

        private static string GetCollectionPropertyName(string tagExpression, IEnumerable<string> nonListPropertyNames)
        {
            return nonListPropertyNames.SingleOrDefault(p => ValueTagProcessor.IsLoopIteratorProperty(tagExpression, p));
        }
    }
}
