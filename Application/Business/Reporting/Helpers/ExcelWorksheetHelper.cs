﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Reporting.Helpers
{
    public static class ExcelWorksheetHelper
    {
        /// <summary>
        /// Set the correct values to a model ICollection DataColumn header names
        /// </summary>
        /// <typeparam name="TModel">The type of the rows</typeparam>
        /// <param name="rows">Rows of the model</param>
        /// <param name="columnsGetter">Extensible column method getter</param>
        /// <param name="uniqueElementHeaderName">Header name for unique column</param>
        /// <param name="multipleElementsHeaderNameFormat">Header name format for multiple columns, with {0} being the number of the column</param>
        public static void PrepareExtensibleColumn<TModel>(
            ICollection<TModel> rows,
            Func<TModel, ICollection<DataColumn>> columnsGetter,
            string uniqueElementHeaderName,
            string multipleElementsHeaderNameFormat)
        {
            if (!rows.Any())
            {
                return;
            }

            var extensibleColumn = rows.Select(columnsGetter);

            var maxColumnCount = extensibleColumn.Max(e => e.Count);

            if (maxColumnCount == 0)
            {
                maxColumnCount = 1;
            }

            var headerNames = maxColumnCount == 1
                ? new[] { uniqueElementHeaderName }
                : Enumerable.Range(1, maxColumnCount)
                    .Select(n => string.Format(multipleElementsHeaderNameFormat, n))
                    .ToArray();

            foreach (var column in extensibleColumn)
            {
                AdjustExtensibleColumnCount(column, maxColumnCount, headerNames);
            }
        }

        private static void AdjustExtensibleColumnCount(
            ICollection<DataColumn> extensibleColumn,
            int targetCount,
            IList<string> headerNames)
        {
            var currentIndex = 0;

            foreach (var subColumn in extensibleColumn)
            {
                subColumn.ColumnName = headerNames[currentIndex++];
            }

            while (currentIndex < targetCount)
            {
                extensibleColumn.Add(new DataColumn
                {
                    ColumnName = headerNames[currentIndex++],
                    Value = string.Empty,
                });
            }
        }
    }
}
