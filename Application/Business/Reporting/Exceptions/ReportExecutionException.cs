﻿using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Reporting.Exceptions
{

    [System.Serializable]
    public class ReportExecutionException : BusinessException
    {
        public ReportExecutionException() { }
        public ReportExecutionException(string message) : base(message) { }
        public ReportExecutionException(string message, System.Exception inner) : base(message, inner) { }
        protected ReportExecutionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
