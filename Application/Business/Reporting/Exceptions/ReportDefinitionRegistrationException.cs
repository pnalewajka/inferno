﻿using Smt.Atomic.CrossCutting.Common.Exceptions;
using System;

namespace Smt.Atomic.Business.Reporting.Exceptions
{

    [Serializable]
    public class ReportDefinitionRegistrationException : BusinessException
    {
        public ReportDefinitionRegistrationException() { }
        public ReportDefinitionRegistrationException(string message) : base(message) { }
        public ReportDefinitionRegistrationException(string message, Exception inner) : base(message, inner) { }
        protected ReportDefinitionRegistrationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
