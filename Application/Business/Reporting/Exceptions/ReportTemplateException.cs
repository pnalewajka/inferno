﻿using Smt.Atomic.CrossCutting.Common.Exceptions;
using System;

namespace Smt.Atomic.Business.Reporting.Exceptions
{

    [Serializable]
    public class ReportTemplateException : BusinessException
    {
        public ReportTemplateException() { }
        public ReportTemplateException(string message) : base(message) { }
        public ReportTemplateException(string message, Exception inner) : base(message, inner) { }
        protected ReportTemplateException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
