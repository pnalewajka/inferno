﻿using Smt.Atomic.Business.Reporting.Dto;
using System.Collections.Generic;
using System.IO;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IReportingEngine
    {
        string RendererIdentifier { get; }

        Stream GenerateContent(IReportGenerationContext context);
    }
}
