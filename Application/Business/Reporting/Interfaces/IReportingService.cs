﻿using System;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IReportingService
    {
        object GetReportViewModel(string reportCode);

        Type GetReportViewModelType(string reportCode);

        IReportGenerationContext PerformReport(string reportCode, object parameters);

        object GetReportData(string reportCode, object parameters);

        LocalizedString GetReportName(string reportCode);
    }
}
