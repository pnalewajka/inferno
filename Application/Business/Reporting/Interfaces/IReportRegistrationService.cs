﻿namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IReportRegistrationService
    {
        void RegisterNewReports();
    }
}
