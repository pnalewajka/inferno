﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IRequiredRolesProvider
    {
        IEnumerable<SecurityRoleType> GetRolesForReportConfiguration();

        IEnumerable<SecurityRoleType> GetRolesForReportExecution(object parameters);
    }
}
