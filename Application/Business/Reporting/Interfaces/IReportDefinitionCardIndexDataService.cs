﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Reporting.Dto;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IReportDefinitionCardIndexDataService : ICardIndexDataService<ReportDefinitionDto>
    {
    }
}
