﻿using Smt.Atomic.Business.Reporting.Dto;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IReportDesignService
    {
        ReportDesignDto GetReportDesignById(long id);

        void UpdateReportDesign(ReportDesignDto reportDesign);
    }
}