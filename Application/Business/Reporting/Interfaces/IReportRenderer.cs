﻿using Smt.Atomic.Business.Reporting.Dto;
using System.Web.Mvc;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    public interface IReportRenderer
    {
        ActionResult Render(IReportGenerationContext context);
    }
}
