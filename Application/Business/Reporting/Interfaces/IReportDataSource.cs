﻿using Smt.Atomic.Business.Reporting.Dto;

namespace Smt.Atomic.Business.Reporting.Interfaces
{
    /// <summary>
    /// Represents data source which can be used to create the report
    /// </summary>
    /// <typeparam name="TParametersDto">Type of the report input parameters</typeparam>
    public interface IReportDataSource<TParametersDto> : IReportDataSource
    {
        /// <summary>
        /// Method returing the default values of report input parameters
        /// </summary>
        /// <returns></returns>
        TParametersDto GetDefaultParameters(ReportGenerationContext<TParametersDto> reportGenerationContext);

        /// <summary>
        /// Method generating data for the report based on the input parameters
        /// </summary>
        /// <param name="reportGenerationContext"></param>
        /// <returns></returns>
        object GetData(ReportGenerationContext<TParametersDto> reportGenerationContext);
    }

    public interface IReportDataSource
    {
        /// <summary>
        /// Method generating data for the report based on the input parameters
        /// </summary>
        /// <param name="reportGenerationContext"></param>
        /// <returns></returns>
        object GetData(IReportGenerationContext reportGenerationContext);
    }
}
