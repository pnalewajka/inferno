﻿using Smt.Atomic.CrossCutting.Common.Enums;
using System;

namespace Smt.Atomic.Business.Reporting.Attributes
{
    /// <summary>
    /// Assign security role needed for a given report generation
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class RequireRoleAttribute : Attribute
    {
        public RequireRoleAttribute(SecurityRoleType role)
        {
            Role = role;
        }

        /// <summary>
        /// Security role required for a given report
        /// </summary>
        public SecurityRoleType Role { get; }
    }
}
