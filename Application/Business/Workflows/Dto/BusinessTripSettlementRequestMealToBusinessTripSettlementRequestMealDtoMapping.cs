﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestMealToBusinessTripSettlementRequestMealDtoMapping : ClassMapping<BusinessTripSettlementRequestMeal, BusinessTripSettlementRequestMealDto>
    {
        public BusinessTripSettlementRequestMealToBusinessTripSettlementRequestMealDtoMapping()
        {
            Mapping = e => new BusinessTripSettlementRequestMealDto
            {
                CountryId = e.CountryId,
                CountryName = e.CountryId.HasValue ? e.Country.Name : null,
                Day = e.Day,
                BreakfastCount = e.BreakfastCount,
                LunchCount = e.LunchCount,
                DinnerCount = e.DinnerCount,
            };
        }
    }
}
