﻿using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripAdvancedPaymentRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public decimal Amount { get; set; }

        public long CurrencyId { get; set; }

        public string CurrencySymbol { get; set; }

        public long BusinessTripId { get; set; }

        public RequestStatus Status { get; set; }

        public string RequestorFirstName { get; set; }

        public string RequestorLastName { get; set; }

        public string ParticipantFullName { get; set; }
    }
}
