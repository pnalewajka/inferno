﻿namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestStatusDto
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
}
