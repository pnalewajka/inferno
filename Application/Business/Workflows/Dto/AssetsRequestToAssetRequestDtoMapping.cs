﻿using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AssetsRequestToAssetRequestDtoMapping : ClassMapping<AssetsRequest, AssetsRequestDto>
    {
        public AssetsRequestToAssetRequestDtoMapping()
        {
            Mapping = e => new AssetsRequestDto
            {
                Id = e.Id,
                OnboardingRequestId = e.OnboardingRequestId,
                EmployeeId = e.EmployeeId,
                AssetTypeHardwareSetId = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.HardwareSet).Select(a => (long?)a.Id).FirstOrDefault(),
                AssetTypeHardwareSetName = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.HardwareSet).Select(GetAssetTypeName).FirstOrDefault(),
                AssetTypeHardwareSetComment = e.AssetTypeHardwareSetComment,
                AssetTypeHeadsetId = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Headset).Select(a => (long?)a.Id).FirstOrDefault(),
                AssetTypeHeadsetName = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Headset).Select(GetAssetTypeName).FirstOrDefault(),
                AssetTypeHeadsetComment = e.AssetTypeHeadsetComment,
                AssetTypeBackpackId = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Backpack).Select(a => (long?)a.Id).FirstOrDefault(),
                AssetTypeBackpackName = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Backpack).Select(GetAssetTypeName).FirstOrDefault(),
                AssetTypeBackpackComment = e.AssetTypeBackpackComment,
                AssetTypeOptionalHardwareIds = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.OptionalHardware).Select(a => a.Id).ToArray(),
                AssetTypeOptionalHardwareNames = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.OptionalHardware).Select(GetAssetTypeName).ToArray(),
                AssetTypeOptionalHardwareComment = e.AssetTypeOptionalHardwareComment,
                AssetTypeSoftwareId = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Software).Select(a => (long?)a.Id).FirstOrDefault(),
                AssetTypeSoftwareName = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Software).Select(GetAssetTypeName).FirstOrDefault(),
                AssetTypeSoftwareComment = e.AssetTypeSoftwareComment,
                AssetTypeOtherIds = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Other).Select(a => a.Id).ToArray(),
                AssetTypeOtherNames = e.AssetTypes.Where(a => a.Category == AssetTypeCategory.Other).Select(GetAssetTypeName).ToArray(),
                AssetTypeOtherComment = e.AssetTypeOtherComment,
                CommentForIT = e.CommentForIT
            };
        }

        private static LocalizedString GetAssetTypeName(AssetType assetType)
        {
            return new LocalizedString
            {
                English = assetType.NameEn,
                Polish = assetType.NamePl
            };
        }
    }
}
