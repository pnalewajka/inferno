﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestVehicleMileageToBusinessTripSettlementRequestVehicleMileageDtoMapping : ClassMapping<BusinessTripSettlementRequestVehicleMileage, BusinessTripSettlementRequestVehicleMileageDto>
    {
        public BusinessTripSettlementRequestVehicleMileageToBusinessTripSettlementRequestVehicleMileageDtoMapping()
        {
            Mapping = e => new BusinessTripSettlementRequestVehicleMileageDto
            {
                VehicleType = e.VehicleType,
                IsCarEngineCapacityOver900cc = e.IsCarEngineCapacityOver900cc,
                RegistrationNumber = e.RegistrationNumber,
                OutboundFromCityId = e.OutboundFromCityId,
                OutboundToCityId = e.OutboundToCityId,
                OutboundKilometers = e.OutboundKilometers,
                InboundFromCityId = e.InboundFromCityId,
                InboundToCityId = e.InboundToCityId,
                InboundKilometers = e.InboundKilometers,
            };
        }
    }
}
