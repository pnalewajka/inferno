﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApprovalGroupDtoToApprovalGroupMapping : ClassMapping<ApprovalGroupDto, ApprovalGroup>
    {
        public ApprovalGroupDtoToApprovalGroupMapping(IClassMapping<ApprovalDto, Approval> approvalMapping)
        {
            Mapping = d => new ApprovalGroup
            {
                Id = d.Id,
                Mode = d.Mode,
                Approvals = d.Approvals.Select(approvalMapping.CreateFromSource).ToList(),
            };
        }
    }
}
