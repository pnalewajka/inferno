﻿using System;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentTerminationBackupDataDto
    {
        public DateTime? TerminationDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? TerminationReasonId { get; set; }
    }
}
