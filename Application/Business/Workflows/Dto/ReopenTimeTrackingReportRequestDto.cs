﻿using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ReopenTimeTrackingReportRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public long[] ProjectIds { get; set; }

        public string Reason { get; set; }
    }
}
