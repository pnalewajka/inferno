﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApprovalDtoToApprovalMapping : ClassMapping<ApprovalDto, Approval>
    {
        public ApprovalDtoToApprovalMapping()
        {
            Mapping = d => new Approval
            {
                Id = d.Id,
                Status = d.Status,
                UserId = d.UserId,
                ForcedByUserId = d.ForcedByUserId,
            };
        }
    }
}
