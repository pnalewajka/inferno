﻿using Smt.Atomic.Business.Workflows.Interfaces;
using System;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentTerminationRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public DateTime TerminationDate { get; set; }

        public DateTime EndDate { get; set; }

        public long ReasonId { get; set; }

        public string BackupData { get; set; }
    }
}
