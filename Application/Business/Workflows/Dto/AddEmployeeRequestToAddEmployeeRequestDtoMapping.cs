﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AddEmployeeRequestToAddEmployeeRequestDtoMapping : ClassMapping<AddEmployeeRequest, AddEmployeeRequestDto>
    {
        public AddEmployeeRequestToAddEmployeeRequestDtoMapping()
        {
            Mapping = e => new AddEmployeeRequestDto
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Email = e.Email,
                Login = e.Login,
                JobTitleId = e.JobTitleId,
                CrmId = e.CrmId,
                JobProfileId = e.JobProfileId,
                OrgUnitId = e.OrgUnitId,
                LocationId = e.LocationId,
                PlaceOfWork = e.PlaceOfWork,
                CompanyId = e.CompanyId,
                LineManagerId = e.LineManagerId,
                ContractFrom = e.ContractFrom,
                ContractTo = e.ContractTo,
                ContractSignDate = e.ContractSignDate,
                MondayHours = e.MondayHours,
                TuesdayHours = e.TuesdayHours,
                WednesdayHours = e.WednesdayHours,
                ThursdayHours = e.ThursdayHours,
                FridayHours = e.FridayHours,
                SaturdayHours = e.SaturdayHours,
                SundayHours = e.SundayHours,
                ContractTypeId = e.ContractTypeId,
                ContractDuration = e.ContractDuration,
                VacationHourToDayRatio = e.VacationHourToDayRatio,
                TimeReportingMode = e.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = e.AbsenceOnlyDefaultProjectId,
                CalendarId = e.CalendarId
            };
        }
    }
}
