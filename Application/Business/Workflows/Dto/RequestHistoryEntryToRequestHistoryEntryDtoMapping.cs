﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestHistoryToRequestHistoryDtoMapping : ClassMapping<RequestHistoryEntry, RequestHistoryEntryDto>
    {
        public RequestHistoryToRequestHistoryDtoMapping()
        {
            Mapping = e => new RequestHistoryEntryDto
            {
                Id = e.Id,
                Date = e.Date,
                Type = e.Type,
                ExecutingUserId = e.ExecutingUserId,
                ApprovingUserId = e.ApprovingUserId,
                ActionIdentifier = e.ActionIdentifier,
                JiraIssueKey = e.JiraIssueKey,
                EmailSubject = e.EmailSubject,
                EmailRecipients = e.EmailRecipients,
                WatcherNames = e.WatcherNames,
                RejectionReason = e.RejectionReason,
                IsActionActive = e.IsActionActive,
                UpdatesOn = e.UpdatesOn,
                RequestId = e.RequestId,
                ExecutingUserFullName = e.ExecutingUser != null ? e.ExecutingUser.FirstName + " " + e.ExecutingUser.LastName : null,
                ApprovingUserFullName = e.ApprovingUser != null ? e.ApprovingUser.FirstName + " " + e.ApprovingUser.LastName : null,
                Timestamp = e.Timestamp
            };
        }
    }
}
