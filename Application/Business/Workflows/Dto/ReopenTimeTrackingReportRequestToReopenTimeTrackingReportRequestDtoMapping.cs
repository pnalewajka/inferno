﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Helpers;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ReopenTimeTrackingReportRequestToReopenTimeTrackingReportRequestDtoMapping : ClassMapping<ReopenTimeTrackingReportRequest, ReopenTimeTrackingReportRequestDto>
    {
        public ReopenTimeTrackingReportRequestToReopenTimeTrackingReportRequestDtoMapping()
        {
            Mapping = e => new ReopenTimeTrackingReportRequestDto
            {
                Id = e.Id,
                Month = e.Month,
                Year = e.Year,
                ProjectIds = e.Projects.GetIds(),
                Reason = e.Reason,
            };
        }
    }
}
