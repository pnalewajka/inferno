﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ActionSetDtoToActionSetMapping : ClassMapping<ActionSetDto, ActionSet>
    {
        public ActionSetDtoToActionSetMapping()
        {
            Mapping = d => new ActionSet
            {
                Id = d.Id,
                Code = d.Code,
                Description = d.Description,
                TriggeringRequestType = d.TriggeringRequestType,
                TriggeringRequestStatuses = d.TriggeringRequestStatusIds
                    .Select(s => new ActionSetTriggeringStatus { RequestStatus = (RequestStatus)s }).ToList(),
                Definition = d.Definition,
                Timestamp = d.Timestamp
            };
        }
    }
}
