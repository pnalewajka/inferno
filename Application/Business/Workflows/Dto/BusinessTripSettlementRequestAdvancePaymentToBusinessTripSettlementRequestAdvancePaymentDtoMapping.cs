﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestAdvancePaymentToBusinessTripSettlementRequestAdvancePaymentDtoMapping : ClassMapping<BusinessTripSettlementRequestAdvancePayment, BusinessTripSettlementRequestAdvancePaymentDto>
    {
        public BusinessTripSettlementRequestAdvancePaymentToBusinessTripSettlementRequestAdvancePaymentDtoMapping()
        {
            Mapping = e => new BusinessTripSettlementRequestAdvancePaymentDto
            {
                CurrencyId = e.CurrencyId,
                Amount = e.Amount,
                WithdrawnOn = e.WithdrawnOn,
            };
        }
    }
}
