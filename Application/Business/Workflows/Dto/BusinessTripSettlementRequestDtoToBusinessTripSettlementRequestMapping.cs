﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestDtoToBusinessTripSettlementRequestMapping
        : ClassMapping<BusinessTripSettlementRequestDto, BusinessTripSettlementRequest>
    {
        public BusinessTripSettlementRequestDtoToBusinessTripSettlementRequestMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = d => new BusinessTripSettlementRequest
            {
                Id = d.Id,
                BusinessTripParticipantId = d.BusinessTripParticipantId,
                IsEmpty = d.IsEmpty,
                SettlementDate = d.SettlementDate,
                DepartureDateTime = d.DepartureDateTime,
                ArrivalDateTime = d.ArrivalDateTime,
                HoursWorkedForClient = d.HoursWorkedForClient,
                SimplifiedPrivateVehicleMileage = d.SimplifiedPrivateVehicleMileage,
                AbroadTimeEntries = d.AbroadTimeEntries.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAbroadTimeEntryDto,
                    BusinessTripSettlementRequestAbroadTimeEntry>().CreateFromSource).ToArray(),
                Meals = d.Meals.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestMealDto,
                    BusinessTripSettlementRequestMeal>().CreateFromSource).ToArray(),
                OwnCosts = d.OwnCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestOwnCostDto,
                    BusinessTripSettlementRequestOwnCost>().CreateFromSource).ToArray(),
                CompanyCosts = d.CompanyCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestCompanyCostDto,
                    BusinessTripSettlementRequestCompanyCost>().CreateFromSource).ToArray(),
                AdditionalAdvancePayments = d.AdditionalAdvancePayments.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAdvancePaymentDto,
                    BusinessTripSettlementRequestAdvancePayment>().CreateFromSource).ToArray(),
                PrivateVehicleMileage = d.PrivateVehicleMileage == null ? null
                    : mappingFactory.CreateMapping<
                        BusinessTripSettlementRequestVehicleMileageDto,
                        BusinessTripSettlementRequestVehicleMileage>().CreateFromSource(d.PrivateVehicleMileage),
                ControllingStatus = d.ControllingStatus,
                PreferredDistanceUnit = d.PreferredDistanceUnit,
            };
        }
    }
}