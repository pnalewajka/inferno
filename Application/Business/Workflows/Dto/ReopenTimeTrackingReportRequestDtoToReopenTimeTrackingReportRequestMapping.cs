﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ReopenTimeTrackingReportRequestDtoToReopenTimeTrackingReportRequestMapping : ClassMapping<ReopenTimeTrackingReportRequestDto, ReopenTimeTrackingReportRequest>
    {
        public ReopenTimeTrackingReportRequestDtoToReopenTimeTrackingReportRequestMapping()
        {
            Mapping = d => new ReopenTimeTrackingReportRequest
            {
                Id = d.Id,
                Month = d.Month,
                Year = d.Year,
                Projects = d.ProjectIds != null ? d.ProjectIds.Select(p => new Project { Id = p }).ToList() : new List<Project>(),
                Reason = d.Reason,
            };
        }
    }
}
