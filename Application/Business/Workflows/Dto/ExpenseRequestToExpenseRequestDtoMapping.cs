﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ExpenseRequestToExpenseRequestDtoMapping : ClassMapping<ExpenseRequest, ExpenseRequestDto>
    {
        public ExpenseRequestToExpenseRequestDtoMapping()
        {
            Mapping = e => new ExpenseRequestDto
            {
                Id = e.Id,
                ProjectId = e.ProjectId,
                Date = e.Date,
                EmployeeId = e.EmployeeId,
                Amount = e.Amount,
                CurrencyId = e.CurrencyId,
                CurrencyDisplayName = e.Currency.IsoCode,
                Justification = e.Justification
            };
        }
    }
}
