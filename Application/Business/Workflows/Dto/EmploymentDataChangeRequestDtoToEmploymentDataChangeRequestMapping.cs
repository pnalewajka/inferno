﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentDataChangeRequestDtoToEmploymentDataChangeRequestMapping : ClassMapping<EmploymentDataChangeRequestDto, EmploymentDataChangeRequest>
    {
        public EmploymentDataChangeRequestDtoToEmploymentDataChangeRequestMapping()
        {
            Mapping = d => new EmploymentDataChangeRequest
            {
                Id = d.Id,
                EffectiveOn = d.EffectiveOn,
                EmployeeId = d.EmployeeId,
                IsSetSalary = d.Salary.IsSet,
                Salary = d.Salary.Value,
                IsSetContractType = d.ContractType.IsSet,
                ContractTypeId = d.ContractType.Value,
                IsSetContractDurationType = d.ContractDuration.IsSet,
                ContractDurationType = d.ContractDuration.Value,
                IsSetCompany = d.Company.IsSet,
                CompanyId = d.Company.Value,
                IsSetJobTitle = d.JobTitle.IsSet,
                JobTitleId = d.JobTitle.Value,
                CommentsForPayroll = d.CommentsForPayroll,
                Justification = d.Justification,
            };
        }
    }
}
