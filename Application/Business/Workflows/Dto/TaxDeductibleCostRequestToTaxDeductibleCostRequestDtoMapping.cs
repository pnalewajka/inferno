﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class TaxDeductibleCostRequestToTaxDeductibleCostRequestDtoMapping : ClassMapping<TaxDeductibleCostRequest, TaxDeductibleCostRequestDto>
    {
        public TaxDeductibleCostRequestToTaxDeductibleCostRequestDtoMapping()
        {
            Mapping = e => new TaxDeductibleCostRequestDto
            {
                Id = e.Id,
                AffectedUserId = e.AffectedUserId,
                Month = e.Month,
                Year = e.Year,
                ProjectId = e.ProjectId,
                WorkName = e.WorkName,
                Hours = e.Hours,
                Description = e.Description,
                UrlField = e.UrlField
            };
        }
    }
}