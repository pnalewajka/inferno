﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestCompanyCostDto : IBusinessTripSettlementRequestCost
    {
        public string Description { get; set; }

        public DateTime TransactionDate { get; set; }

        public long CurrencyId { get; set; }

        public decimal Amount { get; set; }

        public BusinessTripSettlementPaymentMethod PaymentMethod { get; set; }

        public long? CardHolderEmployeeId { get; set; }

        public bool HasInvoiceIssued { get; set; }
    }
}
