﻿using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AssetsRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long? OnboardingRequestId { get; set; }

        public long EmployeeId { get; set; }

        public long? AssetTypeHardwareSetId { get; set; }

        public LocalizedString AssetTypeHardwareSetName { get; set; }

        public string AssetTypeHardwareSetComment { get; set; }

        public long? AssetTypeHeadsetId { get; set; }

        public LocalizedString AssetTypeHeadsetName { get; set; }

        public string AssetTypeHeadsetComment { get; set; }

        public long? AssetTypeBackpackId { get; set; }

        public LocalizedString AssetTypeBackpackName { get; set; }

        public string AssetTypeBackpackComment { get; set; }

        public long[] AssetTypeOptionalHardwareIds { get; set; }

        public LocalizedString[] AssetTypeOptionalHardwareNames { get; set; }

        public string AssetTypeOptionalHardwareComment { get; set; }

        public long? AssetTypeSoftwareId { get; set; }

        public LocalizedString AssetTypeSoftwareName { get; set; }

        public string AssetTypeSoftwareComment { get; set; }

        public long[] AssetTypeOtherIds { get; set; }

        public LocalizedString[] AssetTypeOtherNames { get; set; }

        public string AssetTypeOtherComment { get; set; }

        public string CommentForIT { get; set; }

        public bool IsCreatedAutomatically { get; set; }
    }
}
