﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SkillChangeRequestToSkillChangeRequestDtoMapping : ClassMapping<SkillChangeRequest, SkillChangeRequestDto>
    {
        public SkillChangeRequestToSkillChangeRequestDtoMapping()
        {
            Mapping = e => new SkillChangeRequestDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                SkillIds = e.Skills.Select(s => s.Id).ToArray(),
            };
        }
    }
}
