﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AdvancedPaymentRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long BusinessTripId { get; set; }

        public long ParticipantId { get; set; }

        public decimal Amount { get; set; }

        public long CurrencyId { get; set; }

        public string CurrencyDisplayName { get; set; }
    }
}
