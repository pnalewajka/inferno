﻿using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class FeedbackRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long EvaluatorEmployeeId { get; set; }

        public string EvaluatorEmployeeFullName { get; set; }

        public long EvaluatedEmployeeId { get; set; }

        public string EvaluatedEmployeeFullName { get; set; }

        public string FeedbackContent { get; set; }

        public string Comment { get; set; }

        public FeedbackVisbility Visibility { get; set; }

        public FeedbackOriginScenario OriginScenario { get; set; }

        public long[] EvaluatorEmployeeIds { get; set; }

        public string RequestURL { get; set; }
    }
}
