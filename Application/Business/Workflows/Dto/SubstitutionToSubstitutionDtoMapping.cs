﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SubstitutionToSubstitutionDtoMapping : ClassMapping<Substitution, SubstitutionDto>
    {
        public SubstitutionToSubstitutionDtoMapping()
        {
            Mapping = e => new SubstitutionDto
            {
                Id = e.Id,
                ReplacedUserId = e.ReplacedUserId,
                SubstituteUserId = e.SubstituteUserId,
                ReplacedUserFullName = e.ReplacedUser != null ? e.ReplacedUser.FullName : string.Empty,
                SubstituteUserFullName = e.SubstituteUser != null ? e.SubstituteUser.FullName : string.Empty,
                From = e.From,
                To = e.To,
                RequestId = e.RequestId,
                Timestamp = e.Timestamp
            };
        }
    }
}
