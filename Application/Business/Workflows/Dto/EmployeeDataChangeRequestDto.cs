﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmployeeDataChangeRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long[] EmployeeIds { get; set; }

        public DateTime? EffectiveOn { get; set; }

        public Optional<long?> LineManager { get; set; }

        public Optional<long?> OrgUnit { get; set; }

        public string OrgUnitNameTooltip { get; set; }

        public Optional<long?> JobProfile { get; set; }

        public Optional<long?> Location { get; set; }

        public Optional<PlaceOfWork?> PlaceOfWork { get; set; }

        public EmployeeDataChangeRequestDto()
        {
            EmployeeIds = new long[0];
        }
    }
}
