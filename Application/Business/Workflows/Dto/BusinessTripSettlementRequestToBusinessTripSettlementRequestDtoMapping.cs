﻿using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestToBusinessTripSettlementRequestDtoMapping : ClassMapping<BusinessTripSettlementRequest, BusinessTripSettlementRequestDto>
    {
        public BusinessTripSettlementRequestToBusinessTripSettlementRequestDtoMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = e => new BusinessTripSettlementRequestDto
            {
                Id = e.Id,
                BusinessTripId = e.BusinessTripParticipant.BusinessTripId,
                BusinessTripParticipantId = e.BusinessTripParticipantId,
                IsEmpty = e.IsEmpty,
                DefaultCurrencyId = e.BusinessTripParticipant.Employee.Company == null ? (long?)null
                    : e.BusinessTripParticipant.Employee.Company.DefaultAdvancedPaymentCurrencyId,
                DefaultCurrencyName = e.BusinessTripParticipant.Employee.Company == null ? null
                    : e.BusinessTripParticipant.Employee.Company.DefaultAdvancedPaymentCurrency.DisplayName,
                DefaultCardHolderEmployeeId = e.BusinessTripParticipant.Employee == null ? (long?)null
                    : e.BusinessTripParticipant.Employee.Id,
                DefaultCardHolderEmployeeName = e.BusinessTripParticipant.Employee == null ? null
                    : e.BusinessTripParticipant.Employee.DisplayName,
                SettlementDate = e.SettlementDate,
                DepartureDateTime = e.DepartureDateTime,
                ArrivalDateTime = e.ArrivalDateTime,
                HoursWorkedForClient = e.HoursWorkedForClient,
                SimplifiedPrivateVehicleMileage = e.SimplifiedPrivateVehicleMileage,
                CalculationResult = e.CalculationResult,
                AbroadTimeEntries = e.AbroadTimeEntries.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAbroadTimeEntry,
                    BusinessTripSettlementRequestAbroadTimeEntryDto>().CreateFromSource).ToArray(),
                Meals = e.Meals.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestMeal,
                    BusinessTripSettlementRequestMealDto>().CreateFromSource).ToArray(),
                OwnCosts = e.OwnCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestOwnCost,
                    BusinessTripSettlementRequestOwnCostDto>().CreateFromSource).ToArray(),
                CompanyCosts = e.CompanyCosts.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestCompanyCost,
                    BusinessTripSettlementRequestCompanyCostDto>().CreateFromSource).ToArray(),
                AdditionalAdvancePayments = e.AdditionalAdvancePayments.Select(mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestAdvancePayment,
                    BusinessTripSettlementRequestAdvancePaymentDto>().CreateFromSource).ToArray(),
                PrivateVehicleMileage = e.PrivateVehicleMileage == null ? null : mappingFactory.CreateMapping<
                    BusinessTripSettlementRequestVehicleMileage,
                    BusinessTripSettlementRequestVehicleMileageDto>().CreateFromSource(e.PrivateVehicleMileage),
                ControllingStatus = e.ControllingStatus,
                PreferredDistanceUnit = e.PreferredDistanceUnit,
            };
        }
    }
}