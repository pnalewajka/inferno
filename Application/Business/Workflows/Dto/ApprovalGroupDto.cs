﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApprovalGroupDto
    {
        public long Id { get; set; }

        public long RequestId { get; set; }

        public ApprovalGroupMode Mode { get; set; }

        public IList<ApprovalDto> Approvals { get; set; }
    }
}
