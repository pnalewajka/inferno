﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AbsenceRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long AffectedUserId { get; set; }

        public long AbsenceTypeId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal Hours { get; set; }

        public long? SubstituteUserId { get; set; }

        public string Comment { get; set; }

        public RequestStatus RequestStatus { get; set; }
    }
}
