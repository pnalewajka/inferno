﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApproverGroupDto
    {
        public ApprovalGroupMode Mode { get; set; }

        public IEnumerable<ApproverDto> Approvers { get; set; }

        public ApproverGroupDto(ApprovalGroupMode mode, IEnumerable<long> employeeIds)
        {
            Mode = mode;
            Approvers = employeeIds.Distinct().Select(e => new ApproverDto(e)).ToList();
        }
    }
}
