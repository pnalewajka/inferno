﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestDtoTRequestMapping : ClassMapping<RequestDto, Request>
    {
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly IClassMapping<SkillChangeRequestDto, SkillChangeRequest> _skillChangeRequestMapping;
        private readonly IClassMapping<AddEmployeeRequestDto, AddEmployeeRequest> _addEmployeeRequestMapping;
        private readonly IClassMapping<AbsenceRequestDto, AbsenceRequest> _absenceRequestMapping;
        private readonly IClassMapping<AddHomeOfficeRequestDto, AddHomeOfficeRequest> _homeOfficeRequestMapping;
        private readonly IClassMapping<OvertimeRequestDto, OvertimeRequest> _overtimeRequestMapping;
        private readonly IClassMapping<EmploymentTerminationRequestDto, EmploymentTerminationRequest> _employmentTerminationRequestMapping;
        private readonly IClassMapping<ReopenTimeTrackingReportRequestDto, ReopenTimeTrackingReportRequest> _reopenTimeReportRequestMapping;
        private readonly IClassMapping<ProjectTimeReportApprovalRequestDto, ProjectTimeReportApprovalRequest> _projectTimeReportApprovalRequestMapping;
        private readonly IClassMapping<EmployeeDataChangeRequestDto, EmployeeDataChangeRequest> _employeeDataChangeRequestMapping;
        private readonly IClassMapping<EmploymentDataChangeRequestDto, EmploymentDataChangeRequest> _employmentDataChangeRequestMapping;
        private readonly IClassMapping<BusinessTripRequestDto, BusinessTripRequest> _businessTripRequestMapping;
        private readonly IClassMapping<AdvancedPaymentRequestDto, AdvancedPaymentRequest> _advancedPaymentRequestMapping;
        private readonly IClassMapping<OnboardingRequestDto, OnboardingRequest> _onboardingRequestMapping;
        private readonly IClassMapping<AssetsRequestDto, AssetsRequest> _assetsRequestMapping;
        private readonly IClassMapping<FeedbackRequestDto, FeedbackRequest> _feedbackRequestMapping;
        private readonly IClassMapping<BusinessTripSettlementRequestDto, BusinessTripSettlementRequest> _settlementRequestMapping;
        private readonly IClassMapping<TaxDeductibleCostRequestDto, TaxDeductibleCostRequest> _taxDeductibleCostRequestMapping;
        private readonly IClassMapping<BonusRequestDto, BonusRequest> _bonusRequestMapping;
        private readonly IClassMapping<ExpenseRequestDto, ExpenseRequest> _expenseRequestMapping;

        public RequestDtoTRequestMapping(
            IRequestServiceResolver requestServiceResolver,
            IClassMapping<SkillChangeRequestDto, SkillChangeRequest> skillChangeRequestMapping,
            IClassMapping<AddEmployeeRequestDto, AddEmployeeRequest> addEmployeeRequestMapping,
            IClassMapping<AbsenceRequestDto, AbsenceRequest> absenceRequestMapping,
            IClassMapping<AddHomeOfficeRequestDto, AddHomeOfficeRequest> homeOfficeRequestMapping,
            IClassMapping<OvertimeRequestDto, OvertimeRequest> overtimeRequestMapping,
            IClassMapping<EmploymentTerminationRequestDto, EmploymentTerminationRequest> employmentTerminationRequestMapping,
            IClassMapping<ReopenTimeTrackingReportRequestDto, ReopenTimeTrackingReportRequest> reopenTimeReportRequestMapping,
            IClassMapping<ProjectTimeReportApprovalRequestDto, ProjectTimeReportApprovalRequest> projectTimeReportApprovalRequestMapping,
            IClassMapping<EmployeeDataChangeRequestDto, EmployeeDataChangeRequest> employeeDataChangeRequestMapping,
            IClassMapping<EmploymentDataChangeRequestDto, EmploymentDataChangeRequest> employmentDataChangeRequestMapping,
            IClassMapping<BusinessTripRequestDto, BusinessTripRequest> businessTripRequestMapping,
            IClassMapping<AdvancedPaymentRequestDto, AdvancedPaymentRequest> advancedPaymentRequestMapping,
            IClassMapping<OnboardingRequestDto, OnboardingRequest> onboardingRequestMapping,
            IClassMapping<AssetsRequestDto, AssetsRequest> assetsRequestMapping,
            IClassMapping<FeedbackRequestDto, FeedbackRequest> feedbackRequestMapping,
            IClassMapping<BusinessTripSettlementRequestDto, BusinessTripSettlementRequest> settlementRequestMapping,
            IClassMapping<TaxDeductibleCostRequestDto, TaxDeductibleCostRequest> taxDeductibleCostRequestMapping,
            IClassMapping<BonusRequestDto, BonusRequest> bonusRequestMapping,
            IClassMapping<ExpenseRequestDto, ExpenseRequest> expenseRequestMapping)
        {
            _requestServiceResolver = requestServiceResolver;
            _skillChangeRequestMapping = skillChangeRequestMapping;
            _addEmployeeRequestMapping = addEmployeeRequestMapping;
            _absenceRequestMapping = absenceRequestMapping;
            _homeOfficeRequestMapping = homeOfficeRequestMapping;
            _overtimeRequestMapping = overtimeRequestMapping;
            _employmentTerminationRequestMapping = employmentTerminationRequestMapping;
            _reopenTimeReportRequestMapping = reopenTimeReportRequestMapping;
            _projectTimeReportApprovalRequestMapping = projectTimeReportApprovalRequestMapping;
            _employeeDataChangeRequestMapping = employeeDataChangeRequestMapping;
            _employmentDataChangeRequestMapping = employmentDataChangeRequestMapping;
            _businessTripRequestMapping = businessTripRequestMapping;
            _advancedPaymentRequestMapping = advancedPaymentRequestMapping;
            _onboardingRequestMapping = onboardingRequestMapping;
            _assetsRequestMapping = assetsRequestMapping;
            _feedbackRequestMapping = feedbackRequestMapping;
            _settlementRequestMapping = settlementRequestMapping;
            _taxDeductibleCostRequestMapping = taxDeductibleCostRequestMapping;
            _bonusRequestMapping = bonusRequestMapping;
            _expenseRequestMapping = expenseRequestMapping;

            Mapping = d => CreateRequest(d);
        }

        private Request CreateRequest(RequestDto d)
        {
            var description = new LocalizedString(() => _requestServiceResolver.GetByServiceType(d.RequestType).GetRequestDescription(d));

            var request = new Request
            {
                Id = d.Id,
                Status = d.Status,
                RequestType = d.RequestType,
                DescriptionEn = description.English,
                DescriptionPl = description.Polish,
                RequestingUserId = d.RequestingUserId,
                Watchers = d.WatchersUserIds == null ? new List<User>() : d.WatchersUserIds.Distinct().Select(i => new User { Id = i }).ToList(),
                DeferredUntil = d.DeferredUntil,
                RejectionReason = d.RejectionReason,
                Exception = d.Exception,
                Timestamp = d.Timestamp,
                Documents = new Collection<RequestDocument>(),
            };

            switch (d.RequestType)
            {
                case RequestType.SkillChangeRequest:
                    request.SkillChangeRequest = _skillChangeRequestMapping.CreateFromSource((SkillChangeRequestDto)d.RequestObject);
                    break;

                case RequestType.AddEmployeeRequest:
                    request.AddEmployeeRequest = _addEmployeeRequestMapping.CreateFromSource((AddEmployeeRequestDto)d.RequestObject);
                    break;

                case RequestType.AbsenceRequest:
                    request.AbsenceRequest = _absenceRequestMapping.CreateFromSource((AbsenceRequestDto)d.RequestObject);
                    break;

                case RequestType.AddHomeOfficeRequest:
                    request.AddHomeOfficeRequest = _homeOfficeRequestMapping.CreateFromSource((AddHomeOfficeRequestDto)d.RequestObject);
                    break;

                case RequestType.OvertimeRequest:
                    request.OvertimeRequest = _overtimeRequestMapping.CreateFromSource((OvertimeRequestDto)d.RequestObject);
                    break;

                case RequestType.EmploymentTerminationRequest:
                    request.EmploymentTerminationRequest = _employmentTerminationRequestMapping.CreateFromSource((EmploymentTerminationRequestDto)d.RequestObject);
                    break;

                case RequestType.ReopenTimeTrackingReportRequest:
                    request.ReopenTimeTrackingReportRequest = _reopenTimeReportRequestMapping.CreateFromSource((ReopenTimeTrackingReportRequestDto)d.RequestObject);
                    break;

                case RequestType.ProjectTimeReportApprovalRequest:
                    request.ProjectTimeReportApprovalRequest = _projectTimeReportApprovalRequestMapping.CreateFromSource((ProjectTimeReportApprovalRequestDto)d.RequestObject);
                    break;

                case RequestType.EmployeeDataChangeRequest:
                    request.EmployeeDataChangeRequest = _employeeDataChangeRequestMapping.CreateFromSource((EmployeeDataChangeRequestDto)d.RequestObject);
                    break;

                case RequestType.EmploymentDataChangeRequest:
                    request.EmploymentDataChangeRequest = _employmentDataChangeRequestMapping.CreateFromSource((EmploymentDataChangeRequestDto)d.RequestObject);
                    break;

                case RequestType.BusinessTripRequest:
                    request.BusinessTripRequest = _businessTripRequestMapping.CreateFromSource((BusinessTripRequestDto)d.RequestObject);
                    break;

                case RequestType.AdvancedPaymentRequest:
                    request.AdvancedPaymentRequest = _advancedPaymentRequestMapping.CreateFromSource((AdvancedPaymentRequestDto)d.RequestObject);
                    break;

                case RequestType.OnboardingRequest:
                    request.OnboardingRequest = _onboardingRequestMapping.CreateFromSource((OnboardingRequestDto)d.RequestObject);
                    break;

                case RequestType.AssetsRequest:
                    request.AssetsRequest = _assetsRequestMapping.CreateFromSource((AssetsRequestDto)d.RequestObject);
                    break;

                case RequestType.FeedbackRequest:
                    request.FeedbackRequest = _feedbackRequestMapping.CreateFromSource((FeedbackRequestDto)d.RequestObject);
                    break;

                case RequestType.BusinessTripSettlementRequest:
                    request.BusinessTripSettlementRequest = _settlementRequestMapping.CreateFromSource((BusinessTripSettlementRequestDto)d.RequestObject);
                    break;

                case RequestType.TaxDeductibleCostRequest:
                    request.TaxDeductibleCostRequest = _taxDeductibleCostRequestMapping.CreateFromSource((TaxDeductibleCostRequestDto)d.RequestObject);
                    break;

                case RequestType.BonusRequest:
                    request.BonusRequest = _bonusRequestMapping.CreateFromSource((BonusRequestDto)d.RequestObject);
                    break;

                case RequestType.ExpenseRequest:
                    request.ExpenseRequest = _expenseRequestMapping.CreateFromSource((ExpenseRequestDto)d.RequestObject);
                    break;

                default:
                    throw new InvalidOperationException($"RequestData mapping not found for type: {d.RequestType}");
            }

            return FillRequest(request, d);
        }

        private Request FillRequest(Request request, RequestDto d)
        {
            request.Id = d.Id;
            request.Status = d.Status;
            request.RequestType = d.RequestType;
            request.RequestingUserId = d.RequestingUserId;
            request.Watchers = d.WatchersUserIds == null ? new List<User>() : d.WatchersUserIds.Distinct().Select(i => new User { Id = i }).ToList();
            request.DeferredUntil = d.DeferredUntil;
            request.RejectionReason = d.RejectionReason;
            request.Exception = d.Exception;
            request.Timestamp = d.Timestamp;

            return request;
        }
    }
}