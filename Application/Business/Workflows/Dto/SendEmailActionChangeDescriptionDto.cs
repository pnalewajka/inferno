﻿namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SendEmailActionChangeDescriptionDto
    {
        public string AuthorFullName { get; set; }

        public string ChangeDescription { get; set; }
    }
}
