﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripRequestToBusinessTripRequestDtoMapping : ClassMapping<BusinessTripRequest, BusinessTripRequestDto>
    {
        public BusinessTripRequestToBusinessTripRequestDtoMapping()
        {
            Mapping = e => new BusinessTripRequestDto
            {
                Id = e.Id,
                ProjectIds = e.Projects.Select(p => p.Id).ToArray(),
                ParticipantEmployeeIds = e.Participants.Select(p => p.Id).ToArray(),
                StartedOn = e.StartedOn,
                EndedOn = e.EndedOn,
                DepartureCityIds = e.DepartureCities.Select(p => p.Id).ToArray(),
                DestinationCityId = e.DestinationCityId,
                TravelExplanation = e.TravelExplanation,
                AreTravelArrangementsNeeded = e.AreTravelArrangementsNeeded,
                TransportationPreferences = e.TransportationPreferences,
                AccommodationPreferences = e.AccommodationPreferences,
                DepartureCityTaxiVouchers = e.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = e.DestinationCityTaxiVouchers,
                LuggageDeclarations = e.LuggageDeclarations.Select(l => new DeclaredLuggageDto
                {
                    Id = l.Id,
                    EmployeeId = l.EmployeeId,
                    HandLuggageItems = l.HandLuggageItems,
                    RegisteredLuggageItems = l.RegisteredLuggageItems
                }).ToArray(),
                ItineraryDetails = e.ItineraryDetails
            };
        }
    }
}
