﻿using Smt.Atomic.Business.Workflows.Interfaces;
using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ProjectTimeReportApprovalRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public long ProjectId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public IList<TimeReportTaskApprovalDto> Tasks { get; set; }

        public ProjectTimeReportApprovalRequestDto()
        {
            Tasks = new List<TimeReportTaskApprovalDto>();
        }
    }
}
