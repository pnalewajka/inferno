﻿using System;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class OnboardingRequestDto : IRequestObject
    {
        public OnboardingRequestDto()
        {
            WelcomeAnnounceEmailPhotos = new DocumentDto[0];
            PayrollDocuments = new DocumentDto[0];
            ForeignEmployeeDocuments = new DocumentDto[0];
        }

        public long Id { get; set; }

        public long? LineManagerEmployeeId { get; set; }

        public long RecruiterEmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public DateTime? SignedOn { get; set; }

        public DateTime StartDate { get; set; }

        public long LocationId { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public long CompanyId { get; set; }

        public long JobTitleId { get; set; }

        public long JobProfileId { get; set; }

        public long ContractTypeId { get; set; }

        public long ProbationPeriodContractTypeId { get; set; }

        public string Compensation { get; set; }

        public string ProbationPeriodCompensation { get; set; }

        public string CommentsForPayroll { get; set; }

        public long OrgUnitId { get; set; }

        public string CommentsForHiringManager { get; set; }

        public bool IsForeignEmployee { get; set; }

        public bool IsComingFromReferralProgram { get; set; }

        public string ReferralProgramDetails { get; set; }

        public bool IsRelocationPackageNeeded { get; set; }

        public string RelocationPackageDetails { get; set; }

        public DateTime? OnboardingDate { get; set; }

        public long? OnboardingLocationId { get; set; }

        public string WelcomeAnnounceEmailInformation { get; set; }

        public DocumentDto[] WelcomeAnnounceEmailPhotos { get; set; }

        public DocumentDto[] PayrollDocuments { get; set; }

        public DocumentDto[] ForeignEmployeeDocuments { get; set; }

        public Guid? WelcomeAnnounceEmailPhotosSecret { get; set; }

        public Guid? PayrollDocumentsSecret { get; set; }

        public Guid? ForeignEmployeeDocumentsSecret { get; set; }

        public long? ResultingUserId { get; set; }

        public bool IsCreatedAutomatically { get; set; }
    }
}
