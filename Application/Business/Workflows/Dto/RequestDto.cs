﻿using System;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestDto : IRequesterData
    {
        public long Id { get; set; }

        public RequestStatus Status { get; set; }

        public RequestType RequestType { get; set; }

        public IRequestObject RequestObject { get; set; }

        public long RequestingUserId { get; set; }

        public LocalizedString Description { get; set; }

        public string RequesterFullName { get; set; }

        public string RequesterEmail { get; set; }

        public ApprovalGroupDto[] ApprovalGroups { get; set; }

        public long? CurrentApprovalGroupId { get; set; }

        public long[] WatchersUserIds { get; set; }

        public DateTime? DeferredUntil { get; set; }

        public string RejectionReason { get; set; }

        public string Exception { get; set; }

        public DocumentDto[] Documents { get; set; }

        public string Company { get; set; }

        public string OrgUnit { get; set; }

        public LocalizedString ContractType { get; set; }

        public long[] AffectedUserIds { get; set; }

        public string[] AffectedUserFullNames { get; set; }

        public bool HasHistoryEntries { get; set; }

        public RequestSaveAction SaveActionType { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public RequestDto()
        {
            Documents = new DocumentDto[0];
            ApprovalGroups = new ApprovalGroupDto[0];
            WatchersUserIds = new long[0];
        }
    }
}