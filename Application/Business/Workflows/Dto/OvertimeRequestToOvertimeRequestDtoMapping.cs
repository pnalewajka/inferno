﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class OvertimeRequestToOvertimeRequestDtoMapping : ClassMapping<OvertimeRequest, OvertimeRequestDto>
    {
        public OvertimeRequestToOvertimeRequestDtoMapping()
        {
            Mapping = e => new OvertimeRequestDto
            {
                Id = e.Id,
                ProjectId = e.ProjectId,
                From = e.From,
                To = e.To,
                HourLimit = e.HourLimit,
                Comment = e.Comment,
            };
        }
    }
}
