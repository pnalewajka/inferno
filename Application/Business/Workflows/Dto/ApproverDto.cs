﻿namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApproverDto
    {
        public long EmployeeId { get; set; }

        public long? ForcedByEmployeeId { get; set; }

        public ApproverDto(long employeeId, long? forcedByEmployeeId = null)
        {
            EmployeeId = employeeId;
            ForcedByEmployeeId = forcedByEmployeeId;
        }
    }
}
