﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ExpenseRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long ProjectId { get; set; }

        public long EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public long CurrencyId { get; set; }

        public string CurrencyDisplayName { get; set; }

        public string Justification { get; set; }
    }
}
