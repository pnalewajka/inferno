﻿namespace Smt.Atomic.Business.Workflows.Dto
{
    public class DeclaredLuggageDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public int HandLuggageItems { get; set; }

        public int RegisteredLuggageItems { get; set; }
    }
}
