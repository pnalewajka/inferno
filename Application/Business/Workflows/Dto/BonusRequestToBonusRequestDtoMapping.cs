﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BonusRequestToBonusRequestDtoMapping : ClassMapping<BonusRequest, BonusRequestDto>
    {
        public BonusRequestToBonusRequestDtoMapping()
        {
            Mapping = e => new BonusRequestDto
            {
                Id = e.Id,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                ProjectId = e.ProjectId,
                EmployeeId = e.EmployeeId,
                EmployeeFullName = e.Employee.FullName,
                BonusAmount = e.BonusAmount,
                CurrencyId = e.CurrencyId,
                Justification = e.Justification,
                Type = e.Type,
                Reinvoice = e.Reinvoice
            };
        }
    }
}