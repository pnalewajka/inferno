﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AdvancedPaymentRequestToAdvancedPaymentRequestDtoMapping : ClassMapping<AdvancedPaymentRequest, AdvancedPaymentRequestDto>
    {
        public AdvancedPaymentRequestToAdvancedPaymentRequestDtoMapping()
        {
            Mapping = e => new AdvancedPaymentRequestDto
            {
                Id = e.Id,
                Amount = e.Amount,
                CurrencyId = e.CurrencyId,
                CurrencyDisplayName = $"{e.Currency.Symbol} ({e.Currency.IsoCode})",
                BusinessTripId = e.BusinessTripId,
                ParticipantId = e.ParticipantId,
            };
        }
    }
}
