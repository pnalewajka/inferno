﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AbsenceRequestToAbsenceRequestDtoMapping : ClassMapping<AbsenceRequest, AbsenceRequestDto>
    {
        public AbsenceRequestToAbsenceRequestDtoMapping()
        {
            Mapping = e => new AbsenceRequestDto
            {
                Id = e.Id,
                AffectedUserId = e.AffectedUserId,
                AbsenceTypeId = e.AbsenceTypeId,
                From = e.From,
                To = e.To,
                Hours = e.Hours,
                SubstituteUserId = e.SubstituteUserId,
                Comment = e.Comment,
                RequestStatus = e.Request.Status,
            };
        }

        public override IEnumerable<string> NavigationProperties
        {
            get
            {
                // Optimize description
                return base.NavigationProperties.Union(
                    new[] 
                    {
                        nameof(AbsenceRequest.AbsenceType),
                        nameof(AbsenceRequest.AffectedUser)
                    });
            }
        }
    }
}
