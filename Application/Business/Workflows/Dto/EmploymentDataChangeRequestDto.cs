﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentDataChangeRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public DateTime EffectiveOn { get; set; }

        public Optional<string> Salary { get; set; }

        public Optional<long?> ContractType { get; set; }

        public Optional<ContractDurationType?> ContractDuration { get; set; }

        public Optional<long?> Company { get; set; }

        public Optional<long?> JobTitle { get; set; }

        public string CommentsForPayroll { get; set; }

        public string Justification { get; set; }
    }
}
