﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ProjectTimeReportApprovalRequestDtoToProjectTimeReportApprovalRequestMapping : ClassMapping<ProjectTimeReportApprovalRequestDto, ProjectTimeReportApprovalRequest>
    {
        public ProjectTimeReportApprovalRequestDtoToProjectTimeReportApprovalRequestMapping()
        {
            Mapping = d => new ProjectTimeReportApprovalRequest
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                ProjectId = d.ProjectId,
                From = d.From,
                To = d.To,
                Tasks = JsonHelper.Serialize(d.Tasks, false),
            };
        }
    }
}
