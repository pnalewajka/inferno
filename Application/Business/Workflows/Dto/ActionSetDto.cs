﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ActionSetDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public RequestType? TriggeringRequestType { get; set; }

        public long[] TriggeringRequestStatusIds { get; set; }

        public string Definition { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
