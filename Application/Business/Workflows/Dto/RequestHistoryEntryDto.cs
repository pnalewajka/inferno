﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestHistoryEntryDto
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public RequestHistoryEntryType Type { get; set; }

        public long? ExecutingUserId { get; set; }

        public long? ApprovingUserId { get; set; }

        public string ActionIdentifier { get; set; }

        public string JiraIssueKey { get; set; }

        public string EmailSubject { get; set; }

        public string EmailRecipients { get; set; }

        public string WatcherNames { get; set; }

        public string RejectionReason { get; set; }

        public bool? IsActionActive { get; set; }

        public string UpdatesOn { get; set; }

        public long RequestId { get; set; }

        public string ExecutingUserFullName { get; set; }

        public string ApprovingUserFullName { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
