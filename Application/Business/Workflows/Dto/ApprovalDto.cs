﻿using Smt.Atomic.CrossCutting.Business.Enums;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApprovalDto
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public ApprovalStatus Status { get; set; }

        public string ApproverEmail { get; set; }

        public string ApproverFullName { get; set; }

        public long? ForcedByUserId { get; set; }

        public string ForcedByUserEmail { get; set; }

        public string ForcedByUserFullName { get; set; }
    }
}
