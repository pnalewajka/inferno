﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentDataChangeRequestToEmploymentDataChangeRequestDtoMapping : ClassMapping<EmploymentDataChangeRequest, EmploymentDataChangeRequestDto>
    {
        public EmploymentDataChangeRequestToEmploymentDataChangeRequestDtoMapping()
        {
            Mapping = e => new EmploymentDataChangeRequestDto
            {
                Id = e.Id,
                EffectiveOn = e.EffectiveOn,
                EmployeeId = e.EmployeeId,
                Salary = new Optional<string>(e.IsSetSalary, e.Salary),
                ContractType = new Optional<long?>(e.IsSetContractType, e.ContractTypeId),
                ContractDuration = new Optional<ContractDurationType?>(e.IsSetContractDurationType, e.ContractDurationType),
                Company = new Optional<long?>(e.IsSetCompany, e.CompanyId),
                JobTitle = new Optional<long?>(e.IsSetJobTitle, e.JobTitleId),
                CommentsForPayroll = e.CommentsForPayroll,
                Justification = e.Justification,
            };
        }

        public override IEnumerable<string> NavigationProperties
        {
            get
            {
                // Optimize description
                return base.NavigationProperties.Union(
                    new[]
                    {
                        nameof(EmploymentDataChangeRequest.Employee),
                    });
            }
        }
    }
}
