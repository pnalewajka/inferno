﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class FeedbackRequestToFeedbackRequestDtoMapping : ClassMapping<FeedbackRequest, FeedbackRequestDto>
    {
        public FeedbackRequestToFeedbackRequestDtoMapping()
        {
            Mapping = e => new FeedbackRequestDto
            {
                Id = e.Id,
                EvaluatedEmployeeId = e.EvaluatedEmployeeId,
                EvaluatorEmployeeId = e.EvaluatorEmployeeId,
                FeedbackContent = e.FeedbackContent,
                Comment = e.Comment,
                Visibility = e.Visibility,
                OriginScenario = e.OriginScenario,
                EvaluatorEmployeeIds = e.EvaluatorEmployees != null ? e.EvaluatorEmployees.Select(v => v.Id).ToArray() : null,
                EvaluatorEmployeeFullName = e.EvaluatorEmployee.FullName,
                EvaluatedEmployeeFullName = e.EvaluatedEmployee.FullName
            };
        }
    }
}
