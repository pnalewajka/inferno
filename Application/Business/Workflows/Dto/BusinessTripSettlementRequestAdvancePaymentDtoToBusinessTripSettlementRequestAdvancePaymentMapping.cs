﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestAdvancePaymentDtoToBusinessTripSettlementRequestAdvancePaymentMapping : ClassMapping<BusinessTripSettlementRequestAdvancePaymentDto, BusinessTripSettlementRequestAdvancePayment>
    {
        public BusinessTripSettlementRequestAdvancePaymentDtoToBusinessTripSettlementRequestAdvancePaymentMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestAdvancePayment
            {
                CurrencyId = d.CurrencyId,
                Amount = d.Amount,
                WithdrawnOn = d.WithdrawnOn,
            };
        }
    }
}
