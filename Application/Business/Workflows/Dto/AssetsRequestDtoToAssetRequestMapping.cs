﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AssetsRequestDtoToAssetRequestMapping : ClassMapping<AssetsRequestDto, AssetsRequest>
    {
        public AssetsRequestDtoToAssetRequestMapping()
        {
            Mapping = d => new AssetsRequest
            {
                Id = d.Id,
                OnboardingRequestId = d.OnboardingRequestId,
                EmployeeId = d.EmployeeId,
                AssetTypes = GetAssetTypes(d).ToList(),
                AssetTypeHardwareSetComment = d.AssetTypeHardwareSetComment,
                AssetTypeHeadsetComment = d.AssetTypeHeadsetComment,
                AssetTypeBackpackComment = d.AssetTypeBackpackComment,
                AssetTypeOptionalHardwareComment = d.AssetTypeOptionalHardwareComment,
                AssetTypeSoftwareComment = d.AssetTypeSoftwareComment,
                AssetTypeOtherComment = d.AssetTypeOtherComment,
                CommentForIT = d.CommentForIT
            };
        }

        private static IEnumerable<AssetType> GetAssetTypes(AssetsRequestDto dto)
        {
            if (dto.AssetTypeHardwareSetId.HasValue)
            {
                yield return new AssetType { Id = dto.AssetTypeHardwareSetId.Value };
            }

            if (dto.AssetTypeHeadsetId.HasValue)
            {
                yield return new AssetType { Id = dto.AssetTypeHeadsetId.Value };
            }

            if (dto.AssetTypeBackpackId.HasValue)
            {
                yield return new AssetType { Id = dto.AssetTypeBackpackId.Value };
            }

            foreach (var id in dto.AssetTypeOptionalHardwareIds ?? Enumerable.Empty<long>())
            {
                yield return new AssetType { Id = id };
            }

            if (dto.AssetTypeSoftwareId.HasValue)
            {
                yield return new AssetType { Id = dto.AssetTypeSoftwareId.Value };
            }

            foreach (var id in dto.AssetTypeOtherIds ?? Enumerable.Empty<long>())
            {
                yield return new AssetType { Id = id };
            }
        }
    }
}
