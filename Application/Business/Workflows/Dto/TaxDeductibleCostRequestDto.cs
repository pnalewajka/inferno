﻿using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class TaxDeductibleCostRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long AffectedUserId { get; set; }

        public string AffectedUserFullName { get; set; }

        public string Description { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public long ProjectId { get; set; }

        public string WorkName { get; set; }

        public decimal Hours { get; set; }

        public string UrlField { get; set; }
    }
}