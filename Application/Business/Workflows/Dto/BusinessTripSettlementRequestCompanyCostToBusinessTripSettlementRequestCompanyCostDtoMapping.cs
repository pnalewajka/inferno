﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestCompanyCostToBusinessTripSettlementRequestCompanyCostDtoMapping : ClassMapping<BusinessTripSettlementRequestCompanyCost, BusinessTripSettlementRequestCompanyCostDto>
    {
        public BusinessTripSettlementRequestCompanyCostToBusinessTripSettlementRequestCompanyCostDtoMapping()
        {
            Mapping = e => new BusinessTripSettlementRequestCompanyCostDto
            {
                Description = e.Description,
                TransactionDate = e.TransactionDate,
                CurrencyId = e.CurrencyId,
                Amount = e.Amount,
                PaymentMethod = e.PaymentMethod,
                CardHolderEmployeeId = e.CardHolderEmployeeId,
                HasInvoiceIssued = e.HasInvoiceIssued,
            };
        }
    }
}
