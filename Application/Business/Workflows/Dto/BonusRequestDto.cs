﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BonusRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long ProjectId { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFullName { get; set; }

        public decimal BonusAmount { get; set; }

        public long CurrencyId { get; set; }

        public string Justification { get; set; }

        public BonusType Type { get; set; }

        public bool Reinvoice { get; set; }
    }
}