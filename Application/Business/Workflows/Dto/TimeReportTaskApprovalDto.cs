﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class TimeReportTaskApprovalDto
    {
        public string TaskName { get; set; }

        public HourlyRateType OvertimeVariant { get; set; }

        public decimal Hours { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public bool IsBillable { get; set; }

        public string[] Attributes { get; set; }

        public TimeReportTaskApprovalDto()
        {
            Attributes = new string[0];
        }
    }
}
