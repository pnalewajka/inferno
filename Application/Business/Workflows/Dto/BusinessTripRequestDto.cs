﻿using System;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long[] ProjectIds { get; set; }

        public long[] ParticipantEmployeeIds { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime EndedOn { get; set; }

        public long[] DepartureCityIds { get; set; }

        public long DestinationCityId { get; set; }

        public string TravelExplanation { get; set; }

        public bool AreTravelArrangementsNeeded { get; set; }

        public MeansOfTransport TransportationPreferences { get; set; }

        public AccommodationType AccommodationPreferences { get; set; }

        public int DepartureCityTaxiVouchers { get; set; }

        public int DestinationCityTaxiVouchers { get; set; }

        public DeclaredLuggageDto[] LuggageDeclarations { get; set; }

        public string ItineraryDetails { get; set; }
    }
}
