﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SkillChangeRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public long[] SkillIds { get; set; }
    }
}
