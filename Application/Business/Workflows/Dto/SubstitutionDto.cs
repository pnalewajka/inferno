﻿using System;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SubstitutionDto
    {
        public long Id { get; set; }

        public long ReplacedUserId { get; set; }

        public long SubstituteUserId { get; set; }

        public string ReplacedUserFullName { get; set; }

        public string SubstituteUserFullName { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public long? RequestId { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
