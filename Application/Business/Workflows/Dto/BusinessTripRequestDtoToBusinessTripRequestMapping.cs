﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripRequestDtoToBusinessTripRequestMapping : ClassMapping<BusinessTripRequestDto, BusinessTripRequest>
    {
        public BusinessTripRequestDtoToBusinessTripRequestMapping()
        {


            Mapping = d => new BusinessTripRequest
            {
                Id = d.Id,
                Projects = d.ProjectIds != null ? d.ProjectIds.Select(id => new Project { Id = id }).ToList() : new List<Project>(),
                Participants = d.ParticipantEmployeeIds != null ? d.ParticipantEmployeeIds.Select(id => new Employee { Id = id }).ToList() : new List<Employee>(),
                StartedOn = d.StartedOn,
                EndedOn = d.EndedOn,
                DepartureCities = d.DepartureCityIds != null ? d.DepartureCityIds.Select(id => new City { Id = id }).ToList() : new List<City>(),
                DestinationCityId = d.DestinationCityId,
                TravelExplanation = d.TravelExplanation,
                AreTravelArrangementsNeeded = d.AreTravelArrangementsNeeded,
                TransportationPreferences = d.TransportationPreferences,
                AccommodationPreferences = d.AccommodationPreferences,
                DepartureCityTaxiVouchers = d.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = d.DestinationCityTaxiVouchers,
                LuggageDeclarations = d.LuggageDeclarations.Select(l => new DeclaredLuggage
                {
                    Id = l.Id,
                    RequestId = d.Id,
                    EmployeeId = l.EmployeeId,
                    HandLuggageItems = l.HandLuggageItems,
                    RegisteredLuggageItems = l.RegisteredLuggageItems
                }).ToList(),
                ItineraryDetails = d.ItineraryDetails
            };
        }
    }
}
