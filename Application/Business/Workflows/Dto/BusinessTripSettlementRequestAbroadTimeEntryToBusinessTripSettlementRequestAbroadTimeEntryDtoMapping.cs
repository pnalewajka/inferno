﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestAbroadTimeEntryToBusinessTripSettlementRequestAbroadTimeEntryDtoMapping : ClassMapping<BusinessTripSettlementRequestAbroadTimeEntry, BusinessTripSettlementRequestAbroadTimeEntryDto>
    {
        public BusinessTripSettlementRequestAbroadTimeEntryToBusinessTripSettlementRequestAbroadTimeEntryDtoMapping()
        {
            Mapping = e => new BusinessTripSettlementRequestAbroadTimeEntryDto
            {
                DepartureDateTime = e.DepartureDateTime,
                DepartureCountryId = e.DepartureCountryId,
                DepartureCityId = e.DepartureCityId,
                DestinationType = e.DestinationType,
            };
        }
    }
}
