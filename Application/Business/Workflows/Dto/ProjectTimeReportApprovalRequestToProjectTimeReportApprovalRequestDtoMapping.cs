﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ProjectTimeReportApprovalRequestToProjectTimeReportApprovalRequestDtoMapping : ClassMapping<ProjectTimeReportApprovalRequest, ProjectTimeReportApprovalRequestDto>
    {
        public ProjectTimeReportApprovalRequestToProjectTimeReportApprovalRequestDtoMapping()
        {
            Mapping = e => new ProjectTimeReportApprovalRequestDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                ProjectId = e.ProjectId,
                From = e.From,
                To = e.To,
                Tasks = JsonHelper.Deserialize<List<TimeReportTaskApprovalDto>>(e.Tasks),
            };
        }

        public override IEnumerable<string> NavigationProperties
        {
            get
            {
                // Optimize description
                return base.NavigationProperties.Union(
                    new[]
                    {
                        nameof(ProjectTimeReportApprovalRequest.Project),
                        nameof(ProjectTimeReportApprovalRequest.Employee)
                    });
            }
        }
    }
}
