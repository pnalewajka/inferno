﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestToRequestDtoMapping : ClassMapping<Request, RequestDto>
    {
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly IClassMapping<ApprovalGroup, ApprovalGroupDto> _approvalClassMapping;
        private readonly IClassMapping<SkillChangeRequest, SkillChangeRequestDto> _skillChangeRequestMapping;
        private readonly IClassMapping<AddEmployeeRequest, AddEmployeeRequestDto> _addEmployeeRequestMapping;
        private readonly IClassMapping<AbsenceRequest, AbsenceRequestDto> _absenceRequestMapping;
        private readonly IClassMapping<AddHomeOfficeRequest, AddHomeOfficeRequestDto> _homeOfficeRequestMapping;
        private readonly IClassMapping<OvertimeRequest, OvertimeRequestDto> _overtimeRequestMapping;
        private readonly IClassMapping<EmploymentTerminationRequest, EmploymentTerminationRequestDto> _employmentTerminationRequestMapping;
        private readonly IClassMapping<ReopenTimeTrackingReportRequest, ReopenTimeTrackingReportRequestDto> _reopenTimeReportRequestMapping;
        private readonly IClassMapping<ProjectTimeReportApprovalRequest, ProjectTimeReportApprovalRequestDto> _projectTimeReportApprovalRequestMapping;
        private readonly IClassMapping<EmployeeDataChangeRequest, EmployeeDataChangeRequestDto> _employeeDataChangeRequestMapping;
        private readonly IClassMapping<EmploymentDataChangeRequest, EmploymentDataChangeRequestDto> _employmentDataChangeRequestMapping;
        private readonly IClassMapping<BusinessTripRequest, BusinessTripRequestDto> _businessTripRequestMapping;
        private readonly IClassMapping<AdvancedPaymentRequest, AdvancedPaymentRequestDto> _advancedPaymentRequestMapping;
        private readonly IClassMapping<OnboardingRequest, OnboardingRequestDto> _onboardingRequestMapping;
        private readonly IClassMapping<AssetsRequest, AssetsRequestDto> _assetsRequestMapping;
        private readonly IClassMapping<FeedbackRequest, FeedbackRequestDto> _feedbackRequestMapping;
        private readonly IClassMapping<BusinessTripSettlementRequest, BusinessTripSettlementRequestDto> _settlementRequestMapping;
        private readonly IClassMapping<TaxDeductibleCostRequest, TaxDeductibleCostRequestDto> _taxDeductibleCostRequestMapping;
        private readonly IClassMapping<BonusRequest, BonusRequestDto> _bonusRequestMapping;
        private readonly IClassMapping<ExpenseRequest, ExpenseRequestDto> _expenseRequestMapping;

        public RequestToRequestDtoMapping(
            IRequestServiceResolver requestServiceResolver,
            IClassMapping<ApprovalGroup, ApprovalGroupDto> approvalClassMapping,
            IClassMapping<SkillChangeRequest, SkillChangeRequestDto> skillChangeRequestMapping,
            IClassMapping<AddEmployeeRequest, AddEmployeeRequestDto> addEmployeeRequestMapping,
            IClassMapping<AbsenceRequest, AbsenceRequestDto> absenceRequestMapping,
            IClassMapping<AddHomeOfficeRequest, AddHomeOfficeRequestDto> homeOfficeRequestMapping,
            IClassMapping<OvertimeRequest, OvertimeRequestDto> overtimeRequestMapping,
            IClassMapping<EmploymentTerminationRequest, EmploymentTerminationRequestDto> employmentTerminationRequestMapping,
            IClassMapping<ReopenTimeTrackingReportRequest, ReopenTimeTrackingReportRequestDto> reopenTimeReportRequestMapping,
            IClassMapping<ProjectTimeReportApprovalRequest, ProjectTimeReportApprovalRequestDto> projectTimeReportApprovalRequestMapping,
            IClassMapping<EmployeeDataChangeRequest, EmployeeDataChangeRequestDto> employeeDataChangeRequestMapping,
            IClassMapping<EmploymentDataChangeRequest, EmploymentDataChangeRequestDto> employmentDataChangeRequestMapping,
            IClassMapping<BusinessTripRequest, BusinessTripRequestDto> businessTripRequestMapping,
            IClassMapping<AdvancedPaymentRequest, AdvancedPaymentRequestDto> advancedPaymentRequestMapping,
            IClassMapping<OnboardingRequest, OnboardingRequestDto> onboardingRequestMapping,
            IClassMapping<AssetsRequest, AssetsRequestDto> assetsRequestMapping,
            IClassMapping<FeedbackRequest, FeedbackRequestDto> feedbackRequestMapping,
            IClassMapping<BusinessTripSettlementRequest, BusinessTripSettlementRequestDto> settlementRequestMapping,
            IClassMapping<TaxDeductibleCostRequest, TaxDeductibleCostRequestDto> taxDeductibleCostRequestMapping,
            IClassMapping<BonusRequest, BonusRequestDto> bonusRequestMapping,
            IClassMapping<ExpenseRequest, ExpenseRequestDto> expenseRequestMapping)
        {
            _requestServiceResolver = requestServiceResolver;
            _approvalClassMapping = approvalClassMapping;
            _skillChangeRequestMapping = skillChangeRequestMapping;
            _addEmployeeRequestMapping = addEmployeeRequestMapping;
            _absenceRequestMapping = absenceRequestMapping;
            _homeOfficeRequestMapping = homeOfficeRequestMapping;
            _overtimeRequestMapping = overtimeRequestMapping;
            _employmentTerminationRequestMapping = employmentTerminationRequestMapping;
            _reopenTimeReportRequestMapping = reopenTimeReportRequestMapping;
            _projectTimeReportApprovalRequestMapping = projectTimeReportApprovalRequestMapping;
            _employeeDataChangeRequestMapping = employeeDataChangeRequestMapping;
            _employmentDataChangeRequestMapping = employmentDataChangeRequestMapping;
            _businessTripRequestMapping = businessTripRequestMapping;
            _advancedPaymentRequestMapping = advancedPaymentRequestMapping;
            _onboardingRequestMapping = onboardingRequestMapping;
            _assetsRequestMapping = assetsRequestMapping;
            _feedbackRequestMapping = feedbackRequestMapping;
            _settlementRequestMapping = settlementRequestMapping;
            _taxDeductibleCostRequestMapping = taxDeductibleCostRequestMapping;
            _bonusRequestMapping = bonusRequestMapping;
            _expenseRequestMapping = expenseRequestMapping;

            var currentDate = DateTime.Now;

            Mapping = e => new RequestDto
            {
                Id = e.Id,
                Status = e.Status,
                RequestType = e.RequestType,
                Description = new LocalizedString { English = e.DescriptionEn, Polish = e.DescriptionPl },
                RequestObject = CreateObjectDto(e),
                RequestingUserId = e.RequestingUserId,
                RequesterFullName = e.RequestingUser.FullName,
                RequesterEmail = e.RequestingUser.Email,
                ApprovalGroups = e.ApprovalGroups.OrderBy(g => g.Order).Select(_approvalClassMapping.CreateFromSource).ToArray(),
                CurrentApprovalGroupId = RequestBusinessLogic.CurrentApprovalGroup.Call(e) == null ? (long?)null
                    : RequestBusinessLogic.CurrentApprovalGroup.Call(e).Id,
                WatchersUserIds = e.Watchers.Select(u => u.Id).ToArray(),
                DeferredUntil = e.DeferredUntil,
                RejectionReason = e.RejectionReason,
                Exception = e.Exception,
                Company = e.RequestingUser.Employees.Any()
                    ? e.RequestingUser.Employees.FirstOrDefault().Company != null
                        ? e.RequestingUser.Employees.FirstOrDefault().Company.Name
                        : ""
                    : "",
                OrgUnit = e.RequestingUser.Employees.Any()
                    ? e.RequestingUser.Employees.FirstOrDefault().OrgUnit != null
                        ? e.RequestingUser.Employees.FirstOrDefault().OrgUnit.Name
                        : ""
                    : "",
                ContractType = RequestBusinessLogic.GetAffectedUsers.Call(e).Select(u => u.Employees).Any()
                    ? RequestBusinessLogic.GetAffectedUsers.Call(e).Select(u => u.Employees.FirstOrDefault().EmploymentPeriods
                        .Where(ep => ep.StartDate <= currentDate && (!ep.EndDate.HasValue || ep.EndDate >= currentDate))
                        .Select(p => p.ContractType.Name).FirstOrDefault()
                        ).FirstOrDefault()
                    : null,
                AffectedUserIds = RequestBusinessLogic.GetAffectedUsers.Call(e).Select(u => u.Id).ToArray(),
                AffectedUserFullNames = RequestBusinessLogic.GetAffectedUsers.Call(e).Select(u => u.FullName).ToArray(),
                HasHistoryEntries = e.HistoryEntries.Any(),
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                Documents = e.Documents.Select(p => new DocumentDto
                {
                    DocumentId = p.Id,
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                }).ToArray(),
            };
        }

        public override IDictionary<string, List<string>> GetFieldMappings(Type sourceType, Type destinationType)
        {
            var mappings = base.GetFieldMappings(sourceType, destinationType);

            var requestDataMappings = new Dictionary<Type, IClassMapping>
            {
                { typeof(SkillChangeRequest), _skillChangeRequestMapping },
                { typeof(AddEmployeeRequest), _addEmployeeRequestMapping },
                { typeof(AbsenceRequest), _absenceRequestMapping },
                { typeof(AddHomeOfficeRequest), _homeOfficeRequestMapping },
                { typeof(OvertimeRequest), _overtimeRequestMapping },
                { typeof(EmploymentTerminationRequest), _employmentTerminationRequestMapping },
                { typeof(ReopenTimeTrackingReportRequest), _reopenTimeReportRequestMapping },
                { typeof(ProjectTimeReportApprovalRequest), _projectTimeReportApprovalRequestMapping },
                { typeof(EmployeeDataChangeRequest), _employeeDataChangeRequestMapping },
                { typeof(EmploymentDataChangeRequest), _employmentDataChangeRequestMapping },
                { typeof(BusinessTripRequest), _businessTripRequestMapping },
                { typeof(AdvancedPaymentRequest), _advancedPaymentRequestMapping },
                { typeof(OnboardingRequest), _onboardingRequestMapping },
                { typeof(FeedbackRequest), _feedbackRequestMapping },
                { typeof(BusinessTripSettlementRequest), _settlementRequestMapping },
                { typeof(TaxDeductibleCostRequest), _taxDeductibleCostRequestMapping },
                { typeof(BonusRequest), _bonusRequestMapping },
                { typeof(ExpenseRequest), _expenseRequestMapping },
            };

            if (requestDataMappings.ContainsKey(sourceType))
            {
                mappings = mappings.Union(requestDataMappings[sourceType].GetFieldMappings(sourceType, sourceType)).ToDictionary(s => s.Key, s => s.Value);
            }

            return mappings;
        }

        private IRequestObject CreateObjectDto(Request e)
        {
            IRequestObject requestObject;

            switch (e.RequestType)
            {
                case RequestType.SkillChangeRequest:
                    requestObject = _skillChangeRequestMapping.CreateFromSource(e.SkillChangeRequest);
                    break;

                case RequestType.AddEmployeeRequest:
                    requestObject = _addEmployeeRequestMapping.CreateFromSource(e.AddEmployeeRequest);
                    break;

                case RequestType.AbsenceRequest:
                    requestObject = _absenceRequestMapping.CreateFromSource(e.AbsenceRequest);
                    break;

                case RequestType.AddHomeOfficeRequest:
                    requestObject = _homeOfficeRequestMapping.CreateFromSource(e.AddHomeOfficeRequest);
                    break;

                case RequestType.OvertimeRequest:
                    requestObject = _overtimeRequestMapping.CreateFromSource(e.OvertimeRequest);
                    break;

                case RequestType.EmploymentTerminationRequest:
                    requestObject = _employmentTerminationRequestMapping.CreateFromSource(e.EmploymentTerminationRequest);
                    break;

                case RequestType.ReopenTimeTrackingReportRequest:
                    requestObject = _reopenTimeReportRequestMapping.CreateFromSource(e.ReopenTimeTrackingReportRequest);
                    break;

                case RequestType.ProjectTimeReportApprovalRequest:
                    requestObject = _projectTimeReportApprovalRequestMapping.CreateFromSource(e.ProjectTimeReportApprovalRequest);
                    break;

                case RequestType.EmployeeDataChangeRequest:
                    requestObject = _employeeDataChangeRequestMapping.CreateFromSource(e.EmployeeDataChangeRequest);
                    break;

                case RequestType.EmploymentDataChangeRequest:
                    requestObject = _employmentDataChangeRequestMapping.CreateFromSource(e.EmploymentDataChangeRequest);
                    break;

                case RequestType.BusinessTripRequest:
                    requestObject = _businessTripRequestMapping.CreateFromSource(e.BusinessTripRequest);
                    break;

                case RequestType.AdvancedPaymentRequest:
                    requestObject = _advancedPaymentRequestMapping.CreateFromSource(e.AdvancedPaymentRequest);
                    break;

                case RequestType.OnboardingRequest:
                    requestObject = _onboardingRequestMapping.CreateFromSource(e.OnboardingRequest);
                    break;

                case RequestType.AssetsRequest:
                    requestObject = _assetsRequestMapping.CreateFromSource(e.AssetsRequest);
                    break;

                case RequestType.FeedbackRequest:
                    requestObject = _feedbackRequestMapping.CreateFromSource(e.FeedbackRequest);
                    break;

                case RequestType.BusinessTripSettlementRequest:
                    requestObject = _settlementRequestMapping.CreateFromSource(e.BusinessTripSettlementRequest);
                    break;

                case RequestType.TaxDeductibleCostRequest:
                    requestObject = _taxDeductibleCostRequestMapping.CreateFromSource(e.TaxDeductibleCostRequest);
                    break;

                case RequestType.BonusRequest:
                    requestObject = _bonusRequestMapping.CreateFromSource(e.BonusRequest);
                    break;

                case RequestType.ExpenseRequest:
                    requestObject = _expenseRequestMapping.CreateFromSource(e.ExpenseRequest);
                    break;

                default:
                    throw new InvalidOperationException($"RequestData mapping not found for type: {e.RequestType}");
            }

            requestObject.Id = e.Id;

            return requestObject;
        }
    }
}